﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

void std_mem_checker_start();
void std_mem_checker_stop();

#ifdef TARGET_OS_WINDOWS
extern uint32_t g_last_error_index = 0;

//--------------------------------------
// dllmain.cpp : Defines the entry point for the DLL application.
//--------------------------------------
BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
#if defined(DEBUG_MEMORY)
		std_mem_checker_initialize();
		std_mem_checker_start();
#endif
		rtl::res_counter_t::initialize();
		break;
	case DLL_THREAD_ATTACH:
		g_last_error_index = TlsAlloc();
		break;
	case DLL_THREAD_DETACH:
		TlsFree(g_last_error_index);
		break;
	case DLL_PROCESS_DETACH:
#if defined(DEBUG_MEMORY)		
		std_mem_checker_stop();
#endif
		break;
	}
	return TRUE;
}
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
uintptr_t __thread g_last_error = 0;
//--------------------------------------
// for linux we use __attribute__((constructor))
//--------------------------------------
__attribute__((constructor))
void dll_load()
{
#if defined(DEBUG_MEMORY)
		std_mem_checker_initialize();
		std_mem_checker_start();
#endif
		rtl::res_counter_t::initialize();
}
__attribute__((destructor))
void dll_unload()
{
#if defined(DEBUG_MEMORY)
	void std_mem_checker_stop()
#endif
}
#endif
//--------------------------------------
