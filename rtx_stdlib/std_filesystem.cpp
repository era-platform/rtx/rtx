﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

//#include "std_filesystem.h"

#if defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#endif

//--------------------------------------
//
//--------------------------------------
RTX_STD_API bool fs_is_file_exist(const char* path)
{
	if (path == nullptr || path[0] == 0)
		return false;

#if defined (TARGET_OS_WINDOWS)	
	DWORD attr = GetFileAttributesA(path);
	if (attr == INVALID_FILE_ATTRIBUTES)
		return false;
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	struct stat st;
	if (stat(path, &st) != 0)
		return false;
#endif
	
	return true;
}
//--------------------------------------
//
//--------------------------------------
RTX_STD_API bool fs_delete_file(const char* path)
{
	if (path == nullptr || path[0] == 0)
		return false;

#if defined (TARGET_OS_WINDOWS)	
	if (!DeleteFileA(path))
		return false;
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	if (unlink(path) != 0)
		return false;
#endif
	
	return true;
}
//--------------------------------------
//
//--------------------------------------
RTX_STD_API bool fs_directory_exists(const char* path)
{
	return fs_is_file_exist(path);
}
//--------------------------------------
//
//--------------------------------------
RTX_STD_API bool fs_directory_create(const char* path)
{
	if (path == nullptr || path[0] == 0)
		return false;
#if defined (TARGET_OS_WINDOWS)	
	if (!CreateDirectoryA(path, NULL))
		return false;
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	if (mkdir(path, S_IRWXU | S_IRWXG | S_IRWXO) != 0) {
		int error = errno;
		if (error != EEXIST) {
			return false;
		}
	}
#endif
	return true;
}
//--------------------------------------
//
//--------------------------------------
RTX_STD_API bool fs_directory_delete(const char* path)
{
	if (path == nullptr || path[0] == 0)
		return false;
#if defined (TARGET_OS_WINDOWS)
	if (!RemoveDirectoryA(path))
		return false;
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	if (rmdir(path) != 0)
		return false;
#endif
	return true;
}
//--------------------------------------
// empty directories return 0
//--------------------------------------
RTX_STD_API int fs_directory_get_entries(const char* path_m, rtl::ArrayT<fs_entry_info_t>& list)
{
	list.clear();

	if (path_m == nullptr || path_m[0] == 0)
		return false;

#if defined (TARGET_OS_WINDOWS)	
	int path_len = (int)strlen(path_m);
	char* path_mask = (char*)MALLOC(path_len + 16);
	strcpy(path_mask, path_m);
	
	WIN32_FIND_DATAA fd = { 0 };
	HANDLE handle = FindFirstFileA(path_mask, &fd);
	
	FREE(path_mask);
	
	if (handle == INVALID_HANDLE_VALUE)
		return 0;
	do
	{
		if ((strcmp(fd.cFileName, ".") == 0) || (strcmp(fd.cFileName, "..") == 0))
			continue;

		fs_entry_info_t entry;

		entry.fs_attributes = fd.dwFileAttributes;
		strncpy(entry.fs_filename, fd.cFileName, FS_MAX_PATH - 1);
		entry.fs_filename[FS_MAX_PATH - 1] = 0;
		entry.fs_directory = (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0;
		entry.fs_filesize = (!entry.fs_directory) ? uint64_t(fd.nFileSizeLow) | ((uint64_t)fd.nFileSizeHigh << 32) : 0;

		list.add(entry);
	}
	while (FindNextFileA(handle, &fd));

	FindClose(handle);

#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)

	int path_len = std_strlen(path_m);
	const char* ptr = strpbrk(path_m, "\\/");
	if (ptr == NULL)
	{
		return 0;
	}
	const char* last = ptr;
	while (ptr)
	{
		last = ptr;
		const char* next = ptr + 1;
		ptr = strpbrk(next, "\\/");
	}

	char dir_name[MAX_PATH];
	int len = last - path_m + 1;
	strncpy(dir_name, path_m, len);
	dir_name[len] = 0;
	char name[MAX_PATH];
	int len_name = path_len - len;
	strncpy(name, path_m + len, len_name);
	name[len_name] = 0;
	ptr = strpbrk(name, "*");
	if (ptr != NULL)
	{
		int index = ptr - name;
		name[index] = 0;
	}

	DIR* pdir = opendir(dir_name);
	if (pdir == NULL)
	{
		//Log.log_error("sys", "get_dir_entries return %d", errno);
		return false;
	}

	struct stat statbuf;
	struct dirent* entry;

	while ((entry = readdir(pdir)) != nullptr)
	{
		fs_entry_info_t fs_entry;

		if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
			continue;

		char cmp_name[FS_MAX_PATH];
		int path_len_len_name = std_strlen(name);
		strncpy(cmp_name, entry->d_name, path_len_len_name);
		cmp_name[path_len_len_name]=0;
		if (strncmp(cmp_name,name,path_len_len_name) != 0)
		{
			continue;
		}

		char f_path[FS_MAX_PATH];
		strncpy(f_path, dir_name, len);
		int path_len_name = std_strlen(entry->d_name);
		strncpy(f_path+len, entry->d_name, path_len_name);
		f_path[len+path_len_name] = 0;
		//printf("fs_entry name: f_path3 %s\n", f_path);

		strcpy(fs_entry.fs_filename, entry->d_name);

		if (stat(f_path, &statbuf) == 0)
		{
			fs_entry.fs_directory = S_ISDIR(statbuf.st_mode);

			if (!fs_entry.fs_directory)
			{
				// calc filesize
				fs_entry.fs_filesize = statbuf.st_size;
			}

			list.add(fs_entry);
		}
	}
	
	closedir(pdir);
#endif

	return list.getCount();
}
//--------------------------------------
//
//--------------------------------------
RTX_STD_API bool fs_check_path_and_create(const char* path)
{
	char dir_name[MAX_PATH];
	const char* last = path;

	/* убираем '\\?\' */
	if (strncmp(last, "\\\\?\\", 4) == 0)
	{
		last += 4;
	}

	/* убираем 'D:' */
	if (last[1] == ':')
	{
		last += 2;
	}

	// убираем ведущий слеш
	if (last[0] == '\\' || last[0] == '/')
	{
		last++;
	}

	const char* ptr = strpbrk(last, "\\/");

	if (ptr == nullptr)
		return false;

	do
	{
		int index = PTR_DIFF(ptr, path);
		strncpy(dir_name, path, index);
		dir_name[index] = 0;
		if (!fs_directory_exists(dir_name))
		{
			if (!fs_directory_create(dir_name))
				return false;
		}

		last = ++ptr;

		ptr = strpbrk(last, "\\/");
	} while (ptr);

	return true;
}
//--------------------------------------
//
//--------------------------------------
