﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_module.h"

namespace rtl
{
	//--------------------------------------
	//
	//--------------------------------------
	Module::Module() : m_lib(nullptr)
	{
	}
	//--------------------------------------
	//
	//--------------------------------------
	Module::~Module()
	{
		if (m_lib != nullptr)
		{
			unload();
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Module::load(const char* module_path)
	{
		if (m_lib != nullptr)
		{
			return false;
		}

	#if defined (TARGET_OS_WINDOWS)
		m_name = module_path;
		m_lib = LoadLibrary(module_path);
	#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		m_lib = dlopen(module_path, RTLD_GLOBAL | RTLD_LAZY);
		if (m_lib == nullptr)
		{
			LOG_ERROR("std-mod", "dlopen returns %s", dlerror());
			printf("RTX TEST dlopen returns: %s %d\n", dlerror(), errno);
		}
	#endif

		return m_lib != nullptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Module::unload()
	{
		if (m_lib == nullptr)
			return;

	#if defined (TARGET_OS_WINDOWS)
		FreeLibrary(m_lib);
	#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		dlclose(m_lib);
	#endif

		m_lib = nullptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void* Module::bindSymbol(const char* symbol)
	{
		if (m_lib == nullptr)
		{
			return nullptr;
		}

		void* sym_ptr = nullptr;

	#if defined (TARGET_OS_WINDOWS)
		sym_ptr = GetProcAddress(m_lib, symbol);
		if (sym_ptr == nullptr)
		{
			LOG_ERR_MSG("std", GetLastError(), "Binding name %s in module %s failed", symbol, (const char*)m_name);
		}
	#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		sym_ptr = dlsym(m_lib, symbol);
	#endif

		return sym_ptr;
	}
}
//--------------------------------------
