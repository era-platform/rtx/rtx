﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_res_counter.h"
#include "std_synchro.h"

namespace rtl
{
	//--------------------------------------
	//
	//--------------------------------------
	Mutex res_counter_t::s_sync;
	//--------------------------------------
	//
	//--------------------------------------
	res_counter_t::res_counter_entry_t res_counter_t::s_res[STD_RES_COUNTER_MAX_INDEX] = { 0 };
	//--------------------------------------
	//
	//--------------------------------------
	void res_counter_t::initialize()
	{
		//memset(s_res, 0, sizeof s_res);
	}
	//--------------------------------------
	//
	//--------------------------------------
	int res_counter_t::add(const char* res_name)
	{
		MutexLock lock(s_sync);

		// search 
		for (int i = 0; i < STD_RES_COUNTER_MAX_INDEX; i++)
		{
			res_counter_entry_t& entry = s_res[i];
			if (entry.res_name[0] != 0 && strcmp(res_name, entry.res_name) == 0)
			{
				return i;
			}
		}

		// new entry
		for (int i = 0; i < STD_RES_COUNTER_MAX_INDEX; i++)
		{
			res_counter_entry_t& entry = s_res[i];
			if (entry.res_name[0] == 0)
			{
				int name_len = std_strlen(res_name);
				int to_copy = MIN(name_len, 31);
				strncpy(entry.res_name, res_name, to_copy);
				if (to_copy < 31)
				{
					memset(entry.res_name + to_copy, '.', 31 - to_copy);
					entry.res_name[31] = 0;
				}

				entry.counter = 0;
				entry.max_value = 0;
				return i;
			}
		}

		return -1;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void res_counter_t::add_ref(int type)
	{
		if (type < 0 || type >= STD_RES_COUNTER_MAX_INDEX)
			return;

		res_counter_entry_t& entry = s_res[type];

		if (entry.res_name == nullptr)
			return;

		uint64_t value = std_interlocked_inc(&entry.counter);

		if (entry.max_value < value)
			entry.max_value = value;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void res_counter_t::release(int type)
	{
		if (type < 0 || type >= STD_RES_COUNTER_MAX_INDEX)
			return;

		res_counter_entry_t& entry = s_res[type];

		if (entry.res_name == nullptr)
			return;

		std_interlocked_dec(&entry.counter);
	}
	//--------------------------------------
	//
	//--------------------------------------
	int res_counter_t::get_statistics(char* buffer, int size)
	{
		int written = 0;

		for (int i = 0; i < STD_RES_COUNTER_MAX_INDEX; i++)
		{
			res_counter_entry_t& entry = s_res[i];

			if (entry.res_name[0] == 0)
				continue;

			if (size <= written + 1)
				break;

			written += std_snprintf(buffer + written, size - written, "\r\n\t%s : %" PRIu64 "/%" PRIu64, entry.res_name, entry.counter, entry.max_value);
		}

		buffer[size - 1] = 0;
		return written;
	}
}
//--------------------------------------
