﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "std_lock_watcher.h"

namespace rtl
{
#define LOCK_BUFFER_SIZE	(32 * 1024)
	//--------------------------------------
	// информация о блокировке
	//--------------------------------------
	struct locker_info_t
	{
		const char* funcName;
		ThreadId threadId;
	};
	//---
	class lock_object_t
	{
		char m_name[32];
		ArrayT<locker_info_t> m_standby_list;
		Stack<locker_info_t> m_lock_stack;

		volatile uint32_t m_lock_timestamp;

		volatile bool m_blocked;

	public:
		lock_object_t(const char* lockName);

		const char* getName() { return m_name; }
		void set_name(const char* name) { STR_COPY(m_name, name); }

		void locking(const char* funcName);
		void locked(const char* funcName);
		void unlock();
		void cancel(const char* funcName);

		int get_locker_count() { return m_lock_stack.getCount(); }
		locker_info_t* get_locker_at(int index) { return &m_lock_stack[index]; }

		int get_standby_count() { return m_standby_list.getCount(); }
		locker_info_t* get_standby_at(int index) { return &m_standby_list[index]; }

		bool is_locking() { return m_standby_list.getCount() > 0 && !m_blocked; }
		uint32_t get_lock_timestamp() { return m_lock_timestamp; }
		void set_blocked() { m_blocked = true; }
	};
	//--------------------------------------
	// определение массива локов
	//--------------------------------------
	typedef lock_object_t* lockptr_t;
	typedef SortedArrayT<lockptr_t, uintptr_t> lockptr_list_t;
	//--------------------------------------
	// часовой родины
	//--------------------------------------
	class lock_watcher_t : ITimerEventHandler
	{
		bool m_started;
		Timer* m_timer;
		lockptr_list_t m_locks;
		Mutex m_sync;
		char* m_buffer;
		int m_period;

	public:
		lock_watcher_t();
		~lock_watcher_t();

		bool start(int period);
		void stop();

		uintptr_t register_lock(const char* lockName);
		int unregister_lock(uintptr_t lock);
		void set_lock_name(uintptr_t lock, const char* lockName);
		const char* get_lock_name(uintptr_t lock);

		void locking(uintptr_t lock, const char* funcName);
		void locked(uintptr_t lock, const char* funcName);
		void unlock(uintptr_t lock);
		void cancel(uintptr_t lock, const char* funcName);

	private:
		lock_object_t* get_lock_object(uintptr_t lock_id);
		lock_object_t* register_internal(const char* lockName);
		virtual void timer_elapsed_event(Timer* sender, int tid);
	};

	static lock_watcher_t* watcher_ptr = nullptr;

	//--------------------------------------
	// интерфейс часового родины
	//--------------------------------------
	RTX_STD_API bool Watch_Start(int period)
	{
		if (watcher_ptr == nullptr)
		{
			watcher_ptr = NEW lock_watcher_t();
		}
		return watcher_ptr->start(period);
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API void Watch_Stop()
	{
		if (watcher_ptr != nullptr)
		{
			watcher_ptr->stop();
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API void Watch_Destroy()
	{
		if (watcher_ptr != nullptr)
		{
			watcher_ptr->stop();
			DELETEO(watcher_ptr);
			watcher_ptr = nullptr;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API uintptr_t Watch_RegisterLock(const char* lockName)
	{
		if (watcher_ptr == nullptr)
		{
			watcher_ptr = NEW lock_watcher_t();
		}
		return watcher_ptr->register_lock(lockName);
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API int Watch_UnregisterLock(uintptr_t lock)
	{
		if (watcher_ptr != nullptr)
			return watcher_ptr->unregister_lock(lock);
		return 0;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API void Watch_SetLockName(uintptr_t lockId, const char* lockName)
	{
		if (watcher_ptr == nullptr || lockId == 0 || lockName == nullptr || lockName[0] == 0)
			return;

		watcher_ptr->set_lock_name(lockId, lockName);
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API const char* Watch_GetLockName(uintptr_t lockId)
	{
		return watcher_ptr != nullptr && lockId != 0 ? watcher_ptr->get_lock_name(lockId) : "";
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API void Watch_Locking(uintptr_t lock, const char* funcName)
	{
		if (watcher_ptr != nullptr)
			watcher_ptr->locking(lock, funcName);
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API void Watch_Locked(uintptr_t lock, const char* funcName)
	{
		if (watcher_ptr != nullptr)
			watcher_ptr->locked(lock, funcName);
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API void Watch_Unlock(uintptr_t lock)
	{
		if (watcher_ptr != nullptr)
			watcher_ptr->unlock(lock);
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API void Watch_Cancel(uintptr_t lock, const char* funcName)
	{
		if (watcher_ptr != nullptr)
			watcher_ptr->cancel(lock, funcName);
	}
	//--------------------------------------
	//
	//--------------------------------------
	lock_object_t::lock_object_t(const char* lockName) : m_standby_list(32), m_lock_stack(32)
	{
		STR_COPY(m_name, lockName);
		m_lock_timestamp = 0;
		m_blocked = false;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void lock_object_t::locking(const char* funcName)
	{
		m_lock_timestamp = DateTime::getTicks();

		locker_info_t locker = { funcName, Thread::get_current_tid() };
		m_standby_list.add(locker);

		int i = m_standby_list.getCount() - 1;
		locker_info_t* l = &m_standby_list[i];
		if (l->funcName == nullptr || l->funcName[0] == 0 || l->threadId == 0)
		{
			LOG_ERROR("watcher", "VERY DANGEROES ERROR : lock watcher(%s) found broken lock record while LOCKING function %s sl:%d/%d!", m_name, funcName, m_standby_list.getCount(), i);
			m_standby_list.removeAt(i);
			return;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void lock_object_t::locked(const char* funcName)
	{
		locker_info_t* locker = nullptr;
		int i = 0;

		for (i = m_standby_list.getCount() - 1; i >= 0; i--)
		{
			locker_info_t* l = &m_standby_list[i];
			if (l->funcName == nullptr || l->funcName[0] == 0 || l->threadId == 0)
			{
				LOG_ERROR("watcher", "VERY DANGEROES ERROR : lock watcher(%s) found broken lock record while LOCKED function %s sl:%d/%d!", m_name, funcName, m_standby_list.getCount(), i);
				m_standby_list.removeAt(i);
				return;
			}

			if (strcmp(l->funcName, funcName) == 0)
			{
				locker = l;
				break;
			}
		}

		if (locker != nullptr)
		{
			m_lock_stack.push(*locker);
			m_standby_list.removeAt(i);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void lock_object_t::unlock()
	{
		m_lock_stack.pop();

		m_blocked = false;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void lock_object_t::cancel(const char* funcName)
	{
		//locker_info_t* locker = nullptr;


		for (int i = m_standby_list.getCount() - 1; i >= 0; i--)
		{
			locker_info_t* l = &m_standby_list[i];

			if (l->funcName == nullptr || l->funcName[0] == 0 || l->threadId == 0)
			{
				LOG_ERROR("watcher", "VERY DANGEROES ERROR : lock watcher(%s) found broken lock record while CANCELING function %s sl:%d/%d!", m_name, funcName, m_standby_list.getCount(), i);
				m_standby_list.removeAt(i);
				return;
			}

			if (strcmp(l->funcName, funcName) == 0)
			{
				m_standby_list.removeAt(i);
				break;
			}
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	static int compare_by_lock(const lockptr_t& left, const lockptr_t& right)
	{
		return int((uintptr_t)left - (uintptr_t)right);
	}
	//--------------------------------------
	//
	//--------------------------------------
	static int compare_by_key(const uintptr_t& left, const lockptr_t& right)
	{
		return int(left - (uintptr_t)right);
	}
	//--------------------------------------
	//
	//--------------------------------------
	lock_watcher_t::lock_watcher_t() : m_started(false), m_timer(nullptr), m_locks(compare_by_lock, compare_by_key)
	{
		m_buffer = (char*)MALLOC(LOCK_BUFFER_SIZE);
	}
	//--------------------------------------
	//
	//--------------------------------------
	lock_watcher_t::~lock_watcher_t()
	{
		stop();
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool lock_watcher_t::start(int period)
	{
		if (m_started)
			return true;

		LOG_CALL("watcher", "starting : period:%d...", period);

		bool res = false;

		MutexLock lock(m_sync);

		m_timer = NEW Timer(false, this, Log, "lock-watcher");
		res = m_timer->start(period / 2);
		m_period = period;
		m_started = true;

		LOG_CALL("watcher", "started");

		return res;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void lock_watcher_t::stop()
	{
		if (!m_started)
			return;

		if (m_timer != nullptr)
		{
			m_timer->stop();
		}

		MutexLock lock(m_sync);

		if (m_timer != nullptr)
		{
			DELETEO(m_timer);
			m_timer = nullptr;
		}

		FREE(m_buffer);

		for (int i = 0; i < m_locks.getCount(); i++)
		{
			lock_object_t* ptr = m_locks.getAt(i);

			if (ptr != nullptr)
			{
				DELETEO(ptr);
			}
		}

		m_locks.clear();

		m_started = false;
	}
	//--------------------------------------
	//
	//--------------------------------------
	uintptr_t lock_watcher_t::register_lock(const char* lockName)
	{
		uintptr_t lock_id = 0;

		MutexLock lock(m_sync);

		lock_id = (uintptr_t)register_internal(lockName);

		return lock_id;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int lock_watcher_t::unregister_lock(uintptr_t lock_id)
	{
		if (lock_id == 0)
			return 0;

		int lockCount = 0;

		MutexLock lock(m_sync);

		lock_object_t* lockInfo = get_lock_object(lock_id);

		if (lockInfo != nullptr)
		{
			lockCount = lockInfo->get_locker_count() + lockInfo->get_standby_count();
			m_locks.remove(lockInfo);
			DELETEO(lockInfo);
		}
		else
		{
			LOG_WARN("watcher", "lock id %d is invalid while unregister", lock_id);
		}

		return lockCount;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void lock_watcher_t::set_lock_name(uintptr_t lock_id, const char* lockName)
	{
		MutexLock lock(m_sync);

		lock_object_t* lockInfo = get_lock_object(lock_id);

		if (lockInfo != nullptr)
		{
			lockInfo->set_name(lockName);
		}
		else
		{
			LOG_WARN("watcher", "lock id %d is invalid while changing name %s", lock_id, lockName);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	const char* lock_watcher_t::get_lock_name(uintptr_t lock_id)
	{
		const char* name = "";

		MutexLock lock(m_sync);

		lock_object_t* lockInfo = get_lock_object(lock_id);

		if (lockInfo != nullptr)
		{
			name = lockInfo->getName();
		}
		else
		{
			LOG_WARN("watcher", "lock id %d is invalid while getting name", lock_id);
		}

		return name;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void lock_watcher_t::locking(uintptr_t lock_id, const char* funcName)
	{
		MutexLock lock(m_sync);

		lock_object_t* lockInfo = get_lock_object(lock_id);

		if (lockInfo != nullptr)
		{
			lockInfo->locking(funcName);
		}
		else
		{
			LOG_WARN("watcher", "lock id %d is invalid while locking %s function", lock_id, funcName);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void lock_watcher_t::locked(uintptr_t lock_id, const char* funcName)
	{
		MutexLock lock(m_sync);

		lock_object_t* lockInfo = get_lock_object(lock_id);

		if (lockInfo != nullptr)
		{
			lockInfo->locked(funcName);
		}
		else
		{
			LOG_WARN("watcher", "lock id %d is invalid while locked %s function", lock_id, funcName);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void lock_watcher_t::unlock(uintptr_t lock_id)
	{
		MutexLock lock(m_sync);

		lock_object_t* lockInfo = get_lock_object(lock_id);

		if (lockInfo != nullptr)
		{
			lockInfo->unlock();
		}
		else
		{
			LOG_WARN("watcher", "lock id %d is invalid while unlocked", lock_id);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void lock_watcher_t::cancel(uintptr_t lock_id, const char* funcName)
	{
		MutexLock lock(m_sync);

		lock_object_t* lockInfo = get_lock_object(lock_id);

		if (lockInfo != nullptr)
		{
			lockInfo->cancel(funcName);
		}
		else
		{
			LOG_WARN("watcher", "lock id %d is invalid while cancel", lock_id);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	lock_object_t* lock_watcher_t::get_lock_object(uintptr_t lock_id)
	{
		lock_object_t** addr = m_locks.find(lock_id);

		if (addr != nullptr)
		{
			return *addr;
		}

		return nullptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	lock_object_t* lock_watcher_t::register_internal(const char* lockName)
	{
		lock_object_t* lockInfo = nullptr;

		lockInfo = NEW lock_object_t(lockName);
		m_locks.add(lockInfo);

		return lockInfo;
	}
	//--------------------------------------
	// самая главная функция -- только ради нее живет остальной код в этом файле!!!
	//--------------------------------------
	void lock_watcher_t::timer_elapsed_event(Timer* sender, int tid)
	{
		lock_object_t* lock_ptr;
		int len = 0;

		MutexLock lock(m_sync);

		uint32_t current = DateTime::getTicks();

		for (int i = 0; i < m_locks.getCount(); i++)
		{
			lock_ptr = m_locks.getAt(i);
			// если некая блокировка висит больше минуты то логируем ее!
			if (lock_ptr->is_locking() && int(current - lock_ptr->get_lock_timestamp()) > m_period)
			{
				locker_info_t* l;

				lock_ptr->set_blocked();

				int dif = int(current - lock_ptr->get_lock_timestamp());
				int min, sec, msec, tmp;

				min = dif / 60000;
				tmp = dif - min * 60000;
				sec = tmp / 1000;
				msec = tmp % 1000;


				len = std_snprintf(m_buffer, LOCK_BUFFER_SIZE, "FOUND LONG LOCK -----> '%s' blocked for %d min %d sec %d msec\n", lock_ptr->getName(), min, sec, msec);
				len += std_snprintf(m_buffer + len, LOCK_BUFFER_SIZE - len, "\t\tLocker stack:\n");
				int lockerCount = lock_ptr->get_locker_count();

				for (int j = 0; j < lockerCount; j++)
				{
					l = lock_ptr->get_locker_at(j);
					len += std_snprintf(m_buffer + len, LOCK_BUFFER_SIZE - len, "\t\t\tthread id %6u function %s\n", l->threadId, l->funcName);

					if (len >= LOCK_BUFFER_SIZE)
					{
						len = LOCK_BUFFER_SIZE - 1;
						m_buffer[len] = 0;
						break;
					}
				}

				len += std_snprintf(m_buffer + len, LOCK_BUFFER_SIZE - len, "\t\tStandby queue:\n");
				int standbyCount = lock_ptr->get_standby_count();
				for (int j = 0; j < standbyCount; j++)
				{
					l = lock_ptr->get_standby_at(j);
					len += std_snprintf(m_buffer + len, LOCK_BUFFER_SIZE - len, "\t\t\tthread id %6u function %s\n", l->threadId, l->funcName);

					if (len >= LOCK_BUFFER_SIZE)
					{
						len = LOCK_BUFFER_SIZE - 1;
						m_buffer[len] = 0;
						break;
					}
				}

				m_buffer[len] = 0;
				break;
			}
		}

		if (len > 0)
			LOG_WARN("watcher", m_buffer);
	}
}
//--------------------------------------
