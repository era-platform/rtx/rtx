﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_mem_checker.h"
#include "std_res_counter.h"
#include <limits.h>

namespace rtl
{
	//--------------------------------------
	//
	//--------------------------------------
	static int s_mem_obj_counter = -1;
	static int s_mem_buf_counter = -1;
	static volatile uint64_t s_mem_usage = 0;
	static volatile uint64_t s_buf_usage = 0;
	static volatile uint64_t s_obj_usage = 0;
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API void std_mem_checker_initialize()
	{
		s_mem_obj_counter = res_counter_t::add("NEW-OBJ");
		s_mem_buf_counter = res_counter_t::add("MALLOC-BUF");
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API uint64_t std_mem_checker_get_usage()
	{
		return s_mem_usage;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API void* std_mallocz(size_t size)
	{
		void* ptr = MALLOC(size);
		if (ptr != nullptr)
		{
			memset(ptr, 0, size);
		}

		return ptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API void std_freep(void* arg)
	{
		void **ptr = (void **)arg;
		FREE(*ptr);
		*ptr = NULL;
	}
	//--------------------------------------
	//
	//--------------------------------------
#ifdef DEBUG_MEMORY
//--------------------------------------
//
//--------------------------------------
	struct SIGN_HEADER_T
	{
		uint64_t	mem_sign;
		uint64_t	mem_size;
		char		source_name[64];
		uint32_t	source_line;
	};
	//----
#define SIGN_CTRL_HEADER_SIZE	sizeof(SIGN_HEADER_T)
#define SIGN_CTRL_FOOTER_SIZE	(2 * sizeof(uint32_t))
#define SIGN_CTRL_SIZE			(SIGN_CTRL_HEADER_SIZE + SIGN_CTRL_FOOTER_SIZE)
#define SIGN_HEADER				0xc0c0c0c0c0c0c0c0
#define SIGN_FOOTER1			0xaaaaaaaa
#define SIGN_FOOTER2			0xcdcdcdcd
//--------------------------------------
//
//--------------------------------------
	static volatile uint32_t s_mem_ops = 0;
	static volatile bool s_mem_init = false;
	static FILE* s_mem_log = nullptr;
	static Mutex s_mem_lock;
	static const char* s_mem_sign[] = { "NEW  ", "NEW[]", "ALLOC", "DEL  ", "DEL[]", "FREE " };
	//--------------------------------------
	//
	//--------------------------------------
	void* operator new (size_t size, char* file, int line)
	{
		return std_mem_checker_malloc(size, file, line, rc_malloc_new_e);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void* operator new[](size_t size, char* file, int line)
	{
		return std_mem_checker_malloc(size, file, line, rc_malloc_newar_e);
	}
		//--------------------------------------
		//
		//--------------------------------------
		void operator delete (void* mem_ptr)
	{
		std_mem_checker_free(mem_ptr, rc_malloc_delete_e);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void operator delete[](void* mem_ptr)
	{
		std_mem_checker_free(mem_ptr, rc_malloc_deletear_e);
	}
		//--------------------------------------
		//
		//--------------------------------------
		void std_mem_checker_print(const char* fmt, ...)
	{
		if (s_mem_log != nullptr)
		{
			char mem_buf[1024];
			DateTime sysTime;
			va_list args;
			va_start(args, fmt);

			uint32_t tId = Thread::get_current_tid();

			MutexLock lock(s_mem_lock);

			int len = std_snprintf(mem_buf, 1024, "%02d:%02d:%02d:%03d %- 8u ", sysTime.getHour(), sysTime.getMinute(), sysTime.setSecond(), sysTime.getMilliseconds(), tId);
			mem_buf[len] = 0;
			len += std_vsnprintf(mem_buf + len, 1024 - len, fmt, args);
			mem_buf[len] = 0;

			fputs(mem_buf, s_mem_log);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void std_mem_checker_comment(const char* fmt, ...)
	{
		if (s_mem_log != nullptr)
		{
			char mem_buf[1024];

			va_list args;
			va_start(args, fmt);

			MutexLock lock(s_mem_lock);

			int len = std_vsnprintf(mem_buf, 1024, fmt, args);
			mem_buf[len] = 0;
			fputs(mem_buf, s_mem_log);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void std_mem_checker_start()
	{
		MutexLock lock(s_mem_lock);

		if (!s_mem_init) // init
		{
			s_mem_log = fopen("memory.log", "w");
			if (s_mem_log != nullptr)
			{
				//#pragma warning(suppress: 28125)
				std_mem_checker_comment(";----------\n");
				std_mem_checker_comment("; memory log\n");
				std_mem_checker_comment(";----------\n");
				std_mem_checker_comment(";   time    | thread | op  | pointer|  size  | file line\n");
			}

			s_mem_init = true;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void std_mem_checker_stop()
	{
		MutexLock lock(s_mem_lock);

		// destroy

		if (s_mem_init && s_mem_log != nullptr)
		{
			std_mem_checker_comment(";----------\n");
			std_mem_checker_comment(";ops   %u\n;usage %d\n", s_mem_ops, s_mem_usage);
			std_mem_checker_comment(";----------\n");

			fflush(s_mem_log);
			fclose(s_mem_log);
			s_mem_log = nullptr;

			s_mem_init = false;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API void* std_mem_checker_malloc(size_t size, char* file, int line, rc_malloc_type_t type)
	{
		size_t full_size = size + SIGN_CTRL_SIZE;
		uint8_t* mem_ptr = (uint8_t*)malloc(full_size);
		uint8_t* ptr = nullptr;

		const char* filename = strrchr(file, FS_PATH_DELIMITER);

		if (filename == nullptr)
			filename = file;
		else
			filename++;

		if (mem_ptr != nullptr)
		{
			ptr = mem_ptr + SIGN_CTRL_HEADER_SIZE;
			SIGN_HEADER_T* begin = (SIGN_HEADER_T*)mem_ptr;
			uint32_t* end_ptr = (uint32_t*)(ptr + size);

			// пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅ
			begin->mem_sign = SIGN_HEADER;
			// пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅ
			begin->mem_size = size;
			// пїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅ
			strncpy(begin->source_name, filename, 63);
			begin->source_name[63] = 0;
			// пїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅ
			begin->source_line = line;

			end_ptr[0] = SIGN_FOOTER1;		// пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅ 1
			end_ptr[1] = SIGN_FOOTER2;		// пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅ 2

			std_interlocked_add(&s_mem_usage, (uint64_t)size);
			std_interlocked_inc(&s_mem_ops);

			if (type == rc_malloc_new_e)
			{
				res_counter_t::add_ref(s_mem_obj_counter);
				std_interlocked_add(&s_obj_usage, (uint64_t)size);
			}
			else // all other is buffer routines
			{
				res_counter_t::add_ref(s_mem_buf_counter);
				std_interlocked_add(&s_buf_usage, (uint64_t)size);
			}

			if (s_mem_init)
			{
				std_mem_checker_print("%s %016" PRIx64 " %-" PRIu64 " %s %u\n", s_mem_sign[type], ptr, begin->mem_size, filename, line);
				//std_mem_checker_print("%s %p %-8u %s %d\n", s_mem_sign[type], mem_ptr, (uint32_t)size, filename, line);
			}
		}

		return ptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API void std_mem_checker_free(void *mem_ptr, rc_malloc_type_t type)
	{
		if (mem_ptr == nullptr)
			return;

		SIGN_HEADER_T* begin = (SIGN_HEADER_T*)((uint8_t*)mem_ptr - SIGN_CTRL_HEADER_SIZE);
		uint8_t* data = (uint8_t*)mem_ptr;
		intptr_t size = begin->mem_size;

		char source_safe_buffer[64];
		memcpy(source_safe_buffer, begin->source_name, 64);
		source_safe_buffer[63] = 0;

		if (begin->mem_sign != SIGN_HEADER)
		{
			Log.log_error("MEMCHECK", "%s -- MEMORY CORRUPTED AT BEGIN -- memory not freed!!!\n\
\t\t\tptr        %p\n\
\t\t\tsign       %16" PRIX64 "\n\
\t\t\tsize       %" PRIu64 "\n\
\t\t\tfile       %s\n\
\t\t\tline       %u\n\
\t\t\thdr dump   %02x %02x %02x %02x %02x %02x %02x %02x\n",
s_mem_sign[type],
mem_ptr,
begin->mem_sign,
begin->mem_size,
source_safe_buffer,
begin->source_line,
data[0], data[1], data[2], data[3],
data[4], data[5], data[6], data[7]);

			return;
		}
		else
		{
			uint32_t* end = (uint32_t*)((uint8_t*)mem_ptr + begin->mem_size);
			uint8_t* edata = (uint8_t*)(end - 2);
			if (end[0] != SIGN_FOOTER1 || end[1] != SIGN_FOOTER2)
			{
				Log.log_error("MEMCHECK", "%s -- MEMORY CORRUPTED AT END!!!\n\
\t\t\tptr        %p\n\
\t\t\tsign       %16" PRIX64 "\n\
\t\t\tsize       %" PRIu64 "\n\
\t\t\tfile       %s\n\
\t\t\tline       %u\n\
\t\t\thdr dump   %02x %02x %02x %02x %02x %02x %02x %02x\n\
\t\t\tend dump   %02x %02x %02x %02x %02x %02x %02x %02x\n\
\t\t\tfooter[0]  %08X\n\
\t\t\tfooter[1]  %08X\n",
s_mem_sign[type],
mem_ptr,
begin->mem_sign,
begin->mem_size,
source_safe_buffer,
begin->source_line,
data[0], data[1], data[2], data[3],
data[4], data[5], data[6], data[7],
edata[0], edata[1], edata[2], edata[3],
edata[4], edata[5], edata[6], edata[7],
end[0], end[1]);
			}

			size = begin->mem_size;
			//InterlockedExchangeSubstract(&s_mem_usage, size);
			std_interlocked_sub(&s_mem_usage, size);
			std_interlocked_dec(&s_mem_ops);

			if (type == rc_malloc_delete_e)
			{
				res_counter_t::release(s_mem_obj_counter);
				std_interlocked_sub(&s_obj_usage, size);
				//InterlockedExchangeAdd(&s_obj_usage, (uint64_t)-size);
			}
			else // all other buffer routines
			{
				res_counter_t::release(s_mem_buf_counter);
				std_interlocked_sub(&s_buf_usage, size);
				//InterlockedExchangeAdd(&s_buf_usage, (uint64_t)-size);
			}

			if (s_mem_init)
			{
				std_mem_checker_print("%s %016" PRIx64 "\n", s_mem_sign[type], mem_ptr);
			}
		}

		free(begin);
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API void* std_mem_checker_realloc(void* mem_ptr, size_t size, char* file, int line)
	{
		if (mem_ptr == nullptr)
			return std_mem_checker_malloc(size, file, line, rc_malloc_malloc_e);

		SIGN_HEADER_T* begin = (SIGN_HEADER_T*)((uint8_t*)mem_ptr - 16);
		uint8_t* data = (uint8_t*)mem_ptr;

		char source_safe_buffer[64];
		memcpy(source_safe_buffer, begin->source_name, 64);
		source_safe_buffer[63] = 0;

		if (begin->mem_sign != SIGN_HEADER)
		{
			Log.log_error("MEMCHECK", "REALLOC -- MEMORY CORRUPTED AT BEGIN -- memory not freed!!!\n\
\t\t\tptr        %p\n\
\t\t\tsign       %" PRIX64 "\n\
\t\t\tsize       %" PRIu64 "\n\
\t\t\tfile       %s\n\
\t\t\tline       %u\n\
\t\t\thdr dump   %02x %02x %02x %02x %02x %02x %02x %02x\n",
mem_ptr,
begin->mem_sign,
begin->mem_size,
source_safe_buffer,
begin->source_line,
data[0], data[1], data[2], data[3],
data[4], data[5], data[6], data[7]);

			return nullptr;
		}
		else
		{
			uint32_t* end = (uint32_t*)((uint8_t*)mem_ptr + begin->mem_size);
			uint8_t* edata = (uint8_t*)(end - 2);
			if (end[0] != SIGN_FOOTER1 || end[1] != SIGN_FOOTER2)
			{
				Log.log_error("MEMCHECK", "REALLOC -- MEMORY CORRUPTED AT END!!!\n\
\t\t\tptr        %p\n\
\t\t\tsign		 %" PRIX64 "\n\
\t\t\tsize       %" PRIu64 "\n\
\t\t\tfile       %s\n\
\t\t\tline       %u\n\
\t\t\thdr dump   %02x %02x %02x %02x %02x %02x %02x %02x\n\
\t\t\tend dump   %02x %02x %02x %02x %02x %02x %02x %02x\n\
\t\t\tfooter[0]  %08X\n\
\t\t\tfooter[1]  %08X\n",
mem_ptr,
begin->mem_sign,
begin->mem_size,
source_safe_buffer,
begin->source_line,
data[0], data[1], data[2], data[3],
data[4], data[5], data[6], data[7],
edata[0], edata[1], edata[2], edata[3],
edata[4], edata[5], edata[6], edata[7],
end[0], end[1]);
			}

			intptr_t old_size = begin->mem_size;
			std_interlocked_sub(&s_mem_usage, old_size);
			//InterlockedExchangeAdd(&s_mem_usage, (uint64_t)-old_size);
			std_interlocked_dec(&s_mem_ops);

			std_interlocked_sub(&s_buf_usage, old_size);
			//InterlockedExchangeAdd(&s_buf_usage, (uint64_t)-old_size);
			res_counter_t::release(s_mem_buf_counter);

			if (s_mem_init)
			{
				std_mem_checker_print("FREE  %016" PRIx64 "\n", begin);
			}
		}

		uint8_t* new_ptr = (uint8_t*)realloc(begin, size + SIGN_CTRL_SIZE);
		uint8_t* ptr = nullptr;
		begin = (SIGN_HEADER_T*)new_ptr;

		const char* filename = strrchr(file, FS_PATH_DELIMITER);

		if (filename == nullptr)
			filename = file;
		else
			filename++;

		if (begin != nullptr)
		{
			ptr = (uint8_t*)(new_ptr + SIGN_CTRL_HEADER_SIZE);
			uint32_t* end_ptr = (uint32_t*)(ptr + size);
			begin->mem_sign = SIGN_HEADER;
			begin->mem_size = size;
			// пїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅ
			strncpy(begin->source_name, filename, 63);
			begin->source_name[63] = 0;
			// пїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅ
			begin->source_line = line;

			end_ptr[0] = SIGN_FOOTER1;		// пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅ 1
			end_ptr[1] = SIGN_FOOTER2;		// пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅ 2

			std_interlocked_add(&s_mem_usage, (uint64_t)size);
			std_interlocked_inc(&s_mem_ops);

			std_interlocked_add(&s_buf_usage, (uint64_t)size);
			res_counter_t::add_ref(s_mem_buf_counter);

			if (s_mem_init)
			{
				std_mem_checker_print("ALLOC %016" PRIx64 " %-" PRIu64 " %s %u\n", ptr, size, filename, line);
			}
		}

		return ptr;
	}
#endif // DEBUG_MEMORY
}
//--------------------------------------
