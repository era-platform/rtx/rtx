﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "stdafx.h"

#include <ctype.h>
#include "std_string.h"

namespace rtl
{
	//--------------------------------------
	//
	//--------------------------------------
	static uint32_t ROT13Hash(const char* str, int len);
	//--------------------------------------
	// 
	//--------------------------------------
	string_t__ptr* string_t__allocate(int str_len)
	{
		int real_len = str_len;

		if ((real_len & GROW_MASK) != 0)
		{
			real_len &= ~GROW_MASK;
			real_len += STR_GROW_SIZE;
		}

		string_t__ptr* real = (string_t__ptr*)MALLOC((real_len)+sizeof(string_t__ptr));
		string_t__ptr* ptr = (string_t__ptr*)(((uintptr_t)real) & 0xFFFFFFFFFFFFFFFC);
		ptr->address = real;
		ptr->ref = 1;
		ptr->size = real_len;
		ptr->length = 0;
		ptr->string[0] = 0;

		return ptr;
	}
	//--------------------------------------
	// 
	//--------------------------------------
	string_t__ptr* string_t__reallocate(string_t__ptr* ptr, int new_str_len)
	{
		if (ptr == nullptr)
			return string_t__allocate(new_str_len);

		if (ptr->ref > 1)
		{
			string_t__ptr* ptr_new = string_t__allocate(new_str_len);
			ptr_new->length = ptr->length;
			strcpy(ptr_new->string, ptr->string);
			if (std_interlocked_dec(&ptr->ref) == 0)
			{
				FREE(ptr->address);
			}

			return ptr_new;
		}
		else if (ptr->size < new_str_len)
		{
			string_t__ptr* ptr_new = string_t__allocate(new_str_len);
			ptr_new->length = ptr->length;
			strcpy(ptr_new->string, ptr->string);
			FREE(ptr->address);
			return ptr_new;
		}

		return ptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void string_t__addref(char* strptr)
	{
		if (strptr == nullptr)
			return;

		string_t__ptr* ptr = STR_PTR(strptr);
		std_interlocked_inc(&ptr->ref);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void string_t__release(char* strptr)
	{
		if (strptr == nullptr)
			return;

		string_t__ptr* ptr = STR_PTR(strptr);
		if (std_interlocked_dec(&ptr->ref) == 0)
		{
			FREE(ptr->address);
		}
	}
	//--------------------------------------
	// 
	//--------------------------------------
	char* string_t__allocate_new(int capacity)
	{
		string_t__ptr* ptr = string_t__allocate(capacity);
		return ptr->string;
	}
	//--------------------------------------
	// 
	//--------------------------------------
	void string_t__set_length(char* strptr, int length)
	{
		if (strptr != nullptr)
		{
			string_t__ptr* ptr = STR_PTR(strptr);
			if (ptr->ref == 1 && length < ptr->size)
			{
				ptr->length = length;
				ptr->string[length] = 0;
			}
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	char* string_t__copy(char* strptr)
	{
		if (strptr == nullptr)
			return nullptr;

		string_t__ptr* ptr_src = STR_PTR(strptr);
		if (ptr_src->ref == 1)
			return strptr;

		string_t__ptr* ptr_dst = string_t__allocate(ptr_src->length);
		ptr_dst->length = ptr_src->length;
		strcpy(ptr_dst->string, ptr_src->string);
		if (std_interlocked_dec(&ptr_src->ref) == 0)
		{
			FREE(ptr_src->address);
		}

		return ptr_dst->string;
	}
	//--------------------------------------
	// присвоение новой строки
	//--------------------------------------
	char* string_t__set(char* strptr, const char* str, int str_len)
	{
		string_t__ptr* ptr;
		string_t__ptr* ptr_new;

		// если нужно просто обнулить
		if (str == nullptr || str[0] == 0 || str_len == 0)
		{
			if (strptr != nullptr)
			{
				// отвязка!
				string_t__release(strptr);
			}
			return nullptr;
		}

		// вычислим длину
		if (str_len == STR_BADINDEX)
			str_len = std_strlen(str);

		// ничего небыло - создадим новую строку
		if (strptr == nullptr)
		{
			ptr = string_t__allocate(str_len);
			strncpy(ptr->string, str, str_len);
			ptr->length = str_len;
			ptr->string[ptr->length] = 0;
			return ptr->string;
		}

		// получим указатель на структуру
		ptr = STR_PTR(strptr);

		if (ptr->ref > 1)	// отвязка и создание новой строки
		{
			if (std_interlocked_dec(&ptr->ref) == 0)
			{
				FREE(ptr->address);
			}
			ptr_new = string_t__allocate(str_len);
		}
		else if (ptr->size < str_len) // получение большего буфера
		{
			FREE(ptr->address);
			ptr_new = string_t__allocate(str_len);
		}
		else // буфер сответствует ожиданиям
		{
			ptr_new = ptr;
		}

		// копируем строку в буфер
		strncpy(ptr_new->string, str, str_len);
		ptr_new->string[ptr_new->length = str_len] = 0;

		return ptr_new->string;
	}
	//--------------------------------------
	// добавление строки
	//--------------------------------------
	char* string_t__add(char* strptr, const char* str, int str_len)
	{
		string_t__ptr* ptr = nullptr;
		string_t__ptr* ptr_new;

		// если нечего добавлять вернем старый указатель
		if (str == nullptr || str[0] == 0 || str_len == 0)
		{
			return strptr;
		}

		// вычислим длину
		if (str_len == STR_BADINDEX)
			str_len = std_strlen(str);

		// ничего небыло - просто присвоим
		if (strptr == nullptr)
		{
			ptr = string_t__allocate(str_len);
			strncpy(ptr->string, str, str_len);
			ptr->string[ptr->length = str_len] = 0;
			return ptr->string;
		}

		// получим указатель на структуру
		ptr = STR_PTR(strptr);

		if (ptr->ref > 1)	// отвязка и создание новой строки
		{
			ptr_new = string_t__allocate(ptr->length + str_len);
			strcpy(ptr_new->string, ptr->string);
			ptr_new->length = ptr->length;
			if (std_interlocked_dec(&ptr->ref) == 0)
			{
				FREE(ptr->address);
			}
		}
		else if (ptr->size < ptr->length + str_len) // получение большего буфера
		{
			ptr_new = string_t__allocate(ptr->length + str_len);
			strcpy(ptr_new->string, ptr->string);
			ptr_new->length = ptr->length;
			FREE(ptr->address);
		}
		else // буфер сответствует ожиданиям
		{
			ptr_new = ptr;
		}

		strncpy(ptr_new->string + ptr_new->length, str, str_len);
		ptr_new->string[ptr_new->length += str_len] = 0;
		return ptr_new->string;
	}
	//--------------------------------------
	// вставка строки
	//--------------------------------------
	char* string_t__insert(char* strptr, int index, const char* str, int str_len)
	{
		string_t__ptr* ptr = nullptr;
		string_t__ptr* ptr_new;

		// нечего вставлять
		if (str == nullptr || str[0] == 0 || str_len == 0)
		{
			return strptr;
		}

		// вычислим длину
		if (str_len == STR_BADINDEX)
			str_len = std_strlen(str);

		// ничего небыло
		if (strptr == nullptr)
		{
			ptr = string_t__allocate(str_len);
			strncpy(ptr->string, str, str_len);
			ptr->string[ptr->length = str_len] = 0;
			return ptr->string;
		}

		// получим указатель на структуру
		ptr = STR_PTR(strptr);

		if (ptr->ref > 1)	// отвязка и создание новой строки
		{
			ptr_new = string_t__allocate(ptr->length + str_len);
			strcpy(ptr_new->string, ptr->string);
			ptr_new->length = ptr->length;
			if (std_interlocked_dec(&ptr->ref) == 0)
			{
				FREE(ptr->address);
			}
		}
		else if (ptr->size < ptr->length + str_len) // получение большего буфера
		{
			ptr_new = string_t__allocate(ptr->length + str_len);
			strcpy(ptr_new->string, ptr->string);
			ptr_new->length = ptr->length;
			FREE(ptr->address);
		}
		else // буфер сответствует ожиданиям
		{
			ptr_new = ptr;
		}

		// сдвинем строку
		if (index >= ptr_new->length)
		{
			strncpy(ptr_new->string + ptr_new->length, str, str_len);
			ptr_new->string[ptr_new->length += str_len] = 0;
		}
		else
		{
			memmove(ptr_new->string + (index + str_len), ptr_new->string + index, (ptr_new->length - index + 1));
			memcpy(ptr_new->string + index, str, str_len);
			ptr_new->length += str_len;
		}

		return ptr_new->string;
	}
	//--------------------------------------
	// удаление строки
	//--------------------------------------
	char* string_t__remove(char* strptr, int start, int count)
	{
		string_t__ptr* ptr = nullptr;
		string_t__ptr* ptr_new;

		// если строка пустая то ничего не делаем
		if (strptr == nullptr)
			return nullptr;

		ptr = STR_PTR(strptr);

		// если удаление всей строки то отвязка
		if (start == 0 && count >= ptr->length)
		{
			string_t__release(strptr);
			return nullptr;
		}

		// вычислим кол-во символов для удаления
		if (count > ptr->length)
			count = ptr->length - start;

		// получим указатель на структуру
		ptr = STR_PTR(strptr);

		if (ptr->ref > 1)	// отвязка и создание новой строки
		{
			ptr_new = string_t__allocate(ptr->length);
			strcpy(ptr_new->string, ptr->string);
			ptr_new->length = ptr->length;
			if (std_interlocked_dec(&ptr->ref) == 0)
			{
				FREE(ptr->address);
			}
		}
		else // буфер сответствует ожиданиям
		{
			ptr_new = ptr;
		}

		if (start + count == ptr->length)
		{
			ptr_new->string[start] = 0;
		}
		else
		{
			memmove(ptr_new->string + start, ptr_new->string + start + count, (ptr->length - (start + count) + 1));
		}

		ptr->length -= count;

		return ptr_new->string;
	}
	//--------------------------------------
	//
	//--------------------------------------
	const String String::empty;
	//--------------------------------------
	//
	//--------------------------------------
	String& String::assign(const String& string)
	{
		string_t__release(m_ptr);
		string_t__addref(string.m_ptr);
		m_ptr = string.m_ptr;

		return *this;
	}
	//--------------------------------------
	//
	//--------------------------------------
	String& String::assign(const char* str, int length)
	{
		m_ptr = string_t__set(m_ptr, str, length);

		return *this;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void String::allocate_new(int capacity)
	{
		string_t__release(m_ptr);
		m_ptr = string_t__allocate_new(capacity);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void String::setLength(int length)
	{
		string_t__set_length(m_ptr, length);
	}
	//--------------------------------------
	//
	//--------------------------------------
	String& String::append(const String& str)
	{
		m_ptr = string_t__add(m_ptr, str, str.getLength());

		return *this;
	}
	//--------------------------------------
	//
	//--------------------------------------
	String String::append(const String& str) const
	{
		String t = *this;

		return t.append(str);
	}
	//--------------------------------------
	//
	//--------------------------------------
	String& String::append(const char* str, int length)
	{
		m_ptr = string_t__add(m_ptr, str, length);

		return *this;
	}
	//--------------------------------------
	//
	//--------------------------------------
	String String::append(const char* str, int length) const
	{
		String t = *this;

		return t.append(str, length);
	}
	//--------------------------------------
	//
	//--------------------------------------
	String& String::setAt(int index, char ch)
	{
		if (m_ptr == nullptr || m_ptr[0] == 0 || index >= STR_LEN(m_ptr))
			return *this;

		m_ptr = string_t__copy(m_ptr);
		m_ptr[index] = ch;

		return *this;
	}
	//--------------------------------------
	//
	//--------------------------------------
	String String::setAt(int index, char ch) const
	{
		String t = *this;

		return t.setAt(index, ch);
	}
	//--------------------------------------
	//
	//--------------------------------------
	String String::substring(int start, int count) const
	{
		String result;

		if (m_ptr == nullptr || m_ptr[0] == 0 || count == 0 || start >= STR_LEN(m_ptr))
			return result;

		int length = STR_LEN(m_ptr);

		// подсчет подстроки.
		if (start + count >= length || count == STR_BADINDEX)
			count = length - start;

		// подготовим буфер
		return result.assign(m_ptr + start, count);
	}
	//--------------------------------------
	//
	//--------------------------------------
	String& String::remove(int start, int count)
	{
		if (m_ptr == nullptr || m_ptr[0] == 0 || count == 0 || start >= STR_LEN(m_ptr))
			return *this;

		int length = STR_LEN(m_ptr);
		// подсчет подстроки.
		if (start + count > length || count == STR_BADINDEX)
			count = length - start;

		m_ptr = string_t__remove(m_ptr, start, count);

		return *this;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API String String::remove(int start, int count) const
	{
		String t = *this;

		return t.remove(start, count);
	}
	//--------------------------------------
	//
	//--------------------------------------
	String& String::insert(int index, const String& str)
	{
		if (index >= getLength())
			m_ptr = string_t__add(m_ptr, str, str.getLength());
		else
			m_ptr = string_t__insert(m_ptr, index, str, str.getLength());

		return *this;
	}
	//--------------------------------------
	//
	//--------------------------------------
	String String::insert(int index, const String& str) const
	{
		String t = *this;

		return t.insert(index, str);
	}
	//--------------------------------------
	//
	//--------------------------------------
	String& String::insert(int index, const char* str, int length)
	{
		if (index >= getLength())
			m_ptr = string_t__add(m_ptr, str, length);
		else
			m_ptr = string_t__insert(m_ptr, index, str, length);

		return *this;
	}
	//--------------------------------------
	//
	//--------------------------------------
	String String::insert(int index, const char* str, int length) const
	{
		String t = *this;

		return t.insert(index, str, length);
	}
	//--------------------------------------
	//
	//--------------------------------------
	String& String::insert(int index, char ch)
	{
		if (index >= getLength())
			m_ptr = string_t__add(m_ptr, &ch, 1);
		else
			m_ptr = string_t__insert(m_ptr, index, &ch, 1);

		return *this;
	}
	//--------------------------------------
	//
	//--------------------------------------
	String String::insert(int index, char ch) const
	{
		String t = *this;

		return t.insert(index, ch);
	}
	//--------------------------------------
	//
	//--------------------------------------
	int String::indexOf(const char* str, int start) const
	{
		if (m_ptr != nullptr && m_ptr[0] != 0 && str != nullptr && start < STR_LEN(m_ptr))
		{
			const char* found = strstr(m_ptr + start, str);
			return found != nullptr ? PTR_DIFF(found, m_ptr) : STR_BADINDEX;
		}

		return STR_BADINDEX;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int String::indexOf(char ch, int start) const
	{
		if (m_ptr != nullptr && m_ptr[0] != 0 && start < STR_LEN(m_ptr))
		{
			const char* found = ::strchr(m_ptr + start, ch);
			return found != nullptr ? PTR_DIFF(found, m_ptr) : STR_BADINDEX;
		}

		return STR_BADINDEX;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int String::indexOfAny(const char* charSet, int start) const
	{
		if (m_ptr != nullptr && m_ptr[0] != 0 && charSet != nullptr && start < STR_LEN(m_ptr))
		{
			const char* found = strpbrk(m_ptr + start, charSet);
			return found != nullptr ? PTR_DIFF(found, m_ptr) : STR_BADINDEX;
		}

		return STR_BADINDEX;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int String::lastIndexOf(const char* str, int start) const
	{
		if (m_ptr != nullptr && m_ptr[0] != 0 && str != 0 && str[0] != 0)
		{
			// поиск в обратную сторону
			int slen = STR_LEN(m_ptr);
			int len = std_strlen(str);
			if (start < 0 || start >= slen)
				start = slen - 1;

			for (char* cp = m_ptr + start; cp >= m_ptr; cp--)
			{
				if (*cp == str[0])
				{
					if (strncmp(cp, str, len) == 0)
						return PTR_DIFF(cp, m_ptr);
				}
			}
		}

		return STR_BADINDEX;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int String::lastIndexOf(char ch, int start) const
	{
		if (m_ptr != nullptr && m_ptr[0] != 0)
		{
			// поиск в обратную сторону
			int slen = STR_LEN(m_ptr);
			if (start < 0 || start > slen)
				start = slen - 1;

			for (char* cp = m_ptr + start; cp >= m_ptr; cp--)
			{
				if (*cp == ch)
					return PTR_DIFF(cp, m_ptr);
			}
		}

		return STR_BADINDEX;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API void String::split(StringList& arr, const char* delimeter) const
	{
		arr.clear();

		if (delimeter == nullptr || delimeter[0] == 0 || m_ptr == nullptr)
			return;

		const char* lp = m_ptr, *rp;
		int d_len = std_strlen(delimeter);
		int sub_index = 0;
		int sub_len = 0;

		while ((rp = strstr(lp, delimeter)))
		{
			sub_len = PTR_DIFF(rp, lp);

			arr.add(substring(sub_index, sub_len));

			sub_index += sub_len + d_len;
			lp = rp + d_len;
		}

		if (m_ptr)
		{
			arr.add(lp);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API void String::split(StringList& arr, char delimeter) const
	{
		arr.clear();

		if (m_ptr == nullptr)
			return;

		const char* lp = m_ptr, *rp;
		int sub_index = 0;
		int sub_len = 0;

		while ((rp = ::strchr(lp, delimeter)))
		{
			sub_len = PTR_DIFF(rp, lp);

			arr.add(substring(sub_index, sub_len));

			sub_index += sub_len + 1;
			lp = rp + 1;
		}

		if (m_ptr)
		{
			arr.add(lp);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	String& String::trim(const char* charSet)
	{
		if (m_ptr != nullptr && m_ptr[0] != 0 && STR_LEN(m_ptr) > 0)
		{
			m_ptr = string_t__copy(m_ptr);

			int len = STR_LEN(m_ptr);
			int llen = 0;
			int rlen = 0;
			char* ptr = m_ptr;

			while (*ptr != 0 && ::strchr(charSet, *ptr++) != nullptr);
			llen = PTR_DIFF(ptr, m_ptr) - 1;
			if (llen > 0)
				remove(0, llen);

			len = STR_LEN(m_ptr);
			char* end = m_ptr + len - 1;
			ptr = end;

			while (ptr >= m_ptr && ::strchr(charSet, *ptr--) != nullptr);
			rlen = PTR_DIFF(end, ptr) - 1;
			if (rlen > 0)
				remove(len - rlen, rlen);
		}

		return *this;
	}
	RTX_STD_API String String::trim(const char* charSet) const
	{
		String t = *this; return t.trim(charSet);
	}
	//--------------------------------------
	//
	//--------------------------------------
	String& String::toUpper()
	{
		if (m_ptr != nullptr && m_ptr[0] != 0)
		{
			m_ptr = string_t__copy(m_ptr);
			strupr(m_ptr);
		}

		return *this;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API String String::toUpper() const
	{
		String t = *this;

		return t.toUpper();
	}
	//--------------------------------------
	//
	//--------------------------------------
	String& String::toLower()
	{
		if (m_ptr != nullptr && m_ptr[0] != 0)
		{
			m_ptr = string_t__copy(m_ptr);
			strlwr(m_ptr);
		}

		return *this;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API String String::toLower() const
	{
		String t = *this;
		return t.toLower();
	}
	//--------------------------------------
	//
	//--------------------------------------
	String& String::replace(const char* toFind, const char* toReplace)
	{
		if (m_ptr != nullptr && m_ptr[0] != 0 && toFind != nullptr && toFind[0] != 0)
		{
			m_ptr = string_t__copy(m_ptr);

			int slen = std_strlen(toFind);
			int index = indexOf(toFind);
			int rlen = toReplace ? std_strlen(toReplace) : 0;

			while (index != (int)BAD_INDEX)
			{
				remove(index, slen);
				if (toReplace != nullptr && rlen)
					insert(index, toReplace);
				index = indexOf(toFind, index + rlen);
			}
		}

		return *this;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API String String::replace(const char* toFind, const char* toReplace) const
	{
		String t = *this;

		return t.replace(toFind, toReplace);
	}
	//--------------------------------------
	//
	//--------------------------------------
	String& String::replace(char toFind, char toReplace)
	{
		if (m_ptr != nullptr && m_ptr[0] != 0)
		{
			m_ptr = string_t__copy(m_ptr);

			for (char* cp = m_ptr; *cp != 0; cp++)
				if (*cp == toFind)
					*cp = toReplace;
		}

		return *this;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API String String::replace(char toFind, char toReplace) const
	{
		String t = *this;

		return t.replace(toFind, toReplace);
	}
	//--------------------------------------
	//
	//--------------------------------------
	String& String::setEmpty()
	{
		string_t__release(m_ptr);
		m_ptr = nullptr;
		return *this;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int String::compare(const String& left, const String& right, bool ignoreCase)
	{
		const char* l = left, *r = right;

		if (ignoreCase)
		{
			return l != nullptr && r != nullptr ? std_stricmp(l, r) : PTR_DIFF(l, r);
		}
		else
		{
			return l != nullptr && r != nullptr ? strcmp(l, r) : PTR_DIFF(l, r);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	int String::compare(const String& left, const char* right, bool ignoreCase)
	{
		const char* l = left, *r = right;

		if (ignoreCase)
		{
			return l != nullptr && r != nullptr ? std_stricmp(l, r) : PTR_DIFF(l, r);
		}
		else
		{
			return l != nullptr && r != nullptr ? strcmp(l, r) : PTR_DIFF(l, r);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	int String::compare(const char* left, const String& right, bool ignoreCase)
	{
		const char* l = left, *r = right;

		if (ignoreCase)
		{
			return l != nullptr && r != nullptr ? std_stricmp(l, r) : PTR_DIFF(l, r);
		}
		else
		{
			return l != nullptr && r != nullptr ? strcmp(l, r) : PTR_DIFF(l, r);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	uint32_t String::calc_string_hash(const char* key, int length)
	{
		if (length < 0 && key != nullptr)
			length = std_strlen(key);

		return key != nullptr ? ROT13Hash(key, length) : 0;
	}
	//--------------------------------------
	////////////////////////////////////////
	//--------------------------------------
	void StringList::init(int newSize)
	{
		m_array = NULL;
		m_size = m_count = 0;

		if (newSize > 0)
			resize(newSize);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void StringList::setAt(int index, const String& str)
	{
		if (index >= 0 && index < m_count)
		{
			string_t__addref(str.m_ptr);
			string_t__release(m_array[index]);
			m_array[index] = str.m_ptr;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void StringList::setAt(int index, const char* str)
	{
		if (index >= 0 && index < m_count)
		{
			m_array[index] = string_t__set(m_array[index], str, STR_BADINDEX);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	StringList& StringList::assign(const StringList& array)
	{
		clear();

		for (int i = 0; i < array.getCount(); i++)
		{
			add(array[i]);
		}

		return *this;
	}
	//--------------------------------------
	//
	//--------------------------------------
	StringList& StringList::assign(const String* array, int count)
	{
		clear();

		for (int i = 0; i < count; i++)
		{
			add(array[i]);
		}

		return *this;
	}
	//--------------------------------------
	//
	//--------------------------------------
	StringList& StringList::assign(const char** array, int count)
	{
		clear();

		for (int i = 0; i < count; i++)
		{
			add(array[i]);
		}

		return *this;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void StringList::append(const StringList& array)
	{
		for (int i = 0; i < array.getCount(); i++)
		{
			add(array[i]);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void StringList::append(const String* pStrings, int count)
	{
		for (int i = 0; i < count; i++)
		{
			add(pStrings[i]);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void StringList::append(const char** pStrings, int count)
	{
		for (int i = 0; i < count; i++)
		{
			add(pStrings[i]);
		}
	}
	int StringList::add(const String& str)
	{
		if (m_count >= m_size)
		{
			resize(m_count + 1);
		}
		string_t__addref(str.m_ptr);
		m_array[m_count] = str.m_ptr;
		return m_count++;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int StringList::add(const char* str)
	{
		if (m_count >= m_size)
		{
			resize(m_count + 1);
		}

		m_array[m_count] = string_t__set(NULL, str, STR_BADINDEX);
		return m_count++;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void StringList::insert(int index, const String& str)
	{
		if (index >= m_count)
		{
			add(str);
			return;
		}

		if (m_count >= m_size)
		{
			resize(m_count + 1);
		}

		// сдвинем часть массива на одну позицию
		memmove(m_array + index + 1, m_array + index, (m_count - index) * sizeof(char*));
		string_t__addref(str.m_ptr);
		m_array[index] = str.m_ptr;
		m_count++;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void StringList::insert(int index, const char* str)
	{
		if (index >= m_count)
		{
			add(str);
			return;
		}

		if (m_count >= m_size)
		{
			resize(m_count + 1);
		}

		// сдвинем часть массива на одну позицию
		memmove(m_array + index + 1, m_array + index, (m_count - index) * sizeof(char*));
		m_array[index] = string_t__set(NULL, str, STR_BADINDEX);
		m_count++;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void StringList::removeAt(int index)
	{
		if (index >= 0 && index < m_count)
		{
			string_t__release(m_array[index]);

			if (index >= m_count)
				return;
			if (index < m_count - 1)
				memmove(m_array + index, m_array + index + 1, (m_count - index - 1) * sizeof(char*));
			m_array[--m_count] = NULL;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	int StringList::indexOf(const String& str, bool ignoreCase) const
	{
		for (int i = 0; i < m_count; i++)
		{
			if (String::compare(m_array[i], str, ignoreCase) == 0)
				return i;
		}

		return STR_BADINDEX;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int StringList::indexOf(const char* str, bool ignoreCase) const
	{
		for (int i = 0; i < m_count; i++)
		{
			if (String::compare(*(String*)&m_array[i], str, ignoreCase) == 0)
				return i;
		}

		return STR_BADINDEX;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void StringList::clear()
	{
		for (int i = 0; i < m_count; i++)
		{
			string_t__release(m_array[i]);
		}

		if (m_array != NULL)
		{
			FREE(m_array);
			m_array = NULL;
			m_size = m_count = 0;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void StringList::resize(int newSize)
	{
		if (newSize == 0)
		{
			clear();
			return;
		}

		if (m_size < newSize)
		{
			if ((newSize & GROW_MASK) != 0)
			{
				newSize &= ~STR_GROW_MASK;
				newSize += STR_GROW_SIZE;
			}

			char** pArray = (char**)MALLOC(newSize * sizeof(char*));
			memset(pArray + m_count, 0, (newSize - m_count) * sizeof(char*));

			if (m_array != NULL)
			{
				if (m_count > 0)
					memcpy(pArray, m_array, m_count * sizeof(char*));
				FREE(m_array);
			}
			m_array = pArray;
			m_size = newSize;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	uint32_t ROT13Hash(const char* str, int len)
	{
		//  ROT13
		uint32_t hash = 0;

	/*	for (int i = 0; i < len; str++, i++)
		{
			hash += (BYTE)(*str);
			hash -= (hash << 13) | (hash >> 19);
		}*/

		return hash;
	}
}
//--------------------------------------
