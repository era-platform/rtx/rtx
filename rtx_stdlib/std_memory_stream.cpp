﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_memory_stream.h"

namespace rtl {
	//-----------------------------------------------
	//
	//-----------------------------------------------
	MemoryStream::MemoryStream(uint32_t mem_block_size)
	{
		if (mem_block_size == 0)
			mem_block_size = DEFAULT_MEMORY_BLOCK_SIZE;

		m_mem = (uint8_t*)MALLOC(mem_block_size);
		m_mem_pos = 0;
		m_mem_length = 0;
		m_block_size = mem_block_size;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void MemoryStream::close()
	{
		if (m_mem != nullptr)
		{
			FREE(m_mem);
			m_mem = nullptr;
		}

		m_mem_length = 0;
		m_mem_pos = 0;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	size_t MemoryStream::getLength()
	{
		return m_mem_length;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	size_t MemoryStream::setLength(size_t length)
	{
		realloc(length);

		return m_mem_length;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	size_t MemoryStream::getPosition()
	{
		return m_mem_pos;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	size_t MemoryStream::seek(SeekOrigin origin, intptr_t pos)
	{
		if (pos == 0)
		{
			return m_mem_pos;
		}

		if (origin == SeekOrigin::Begin)
		{
			intptr_t new_pos = pos > 0 ? pos : 0;

			if ((size_t)pos > m_mem_length)
			{
				realloc(pos);
			}
			m_mem_pos = new_pos;
		}
		else if (origin == SeekOrigin::Current)
		{
			intptr_t new_pos = m_mem_pos + pos;

			if (new_pos > 0)
			{
				realloc(m_mem_pos = new_pos);
			}
			else if (new_pos <= 0)
			{
				m_mem_pos = 0;
			}
		}
		else if (origin == SeekOrigin::End)
		{
			intptr_t new_pos = m_mem_length + pos;

			if (new_pos > 0)
			{
				realloc(m_mem_pos = new_pos);
			}
			else if (new_pos <= 0)
			{
				m_mem_pos = 0;
			}
		}

		return m_mem_pos;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	size_t MemoryStream::write(const void* data, size_t len)
	{
		if ((data == nullptr) || (len == 0))
			return 0;

		if (!realloc(m_mem_pos + len))
			return 0;

		memcpy(m_mem + m_mem_pos, data, len);

		m_mem_pos += len;

		return len;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	size_t MemoryStream::read(void* buffer, size_t size)
	{
		if (buffer == nullptr || size == 0 || m_mem == nullptr || m_mem_pos == m_mem_length)
		{
			return 0;
		}

		size_t new_pos = m_mem_pos + size;

		if (new_pos > m_mem_length)
			size -= new_pos - m_mem_length;

		memcpy(buffer, m_mem + m_mem_pos, size);

		return size;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool MemoryStream::realloc(size_t new_size)
	{
		// for zstring 
		new_size += 1;

		if (new_size <= m_mem_length)
			return true;

		int remain = new_size % m_block_size;

		if (remain > 0)
		{
			size_t bc = new_size / m_block_size;
			new_size = bc * m_block_size + m_block_size;
		}

		uint8_t* new_mem = (uint8_t*)MALLOC(new_size);

		if (new_mem == nullptr)
			return false;

		if (m_mem != nullptr)
		{
			memcpy(new_mem, m_mem, m_mem_length);
			FREE(m_mem);
		}

		memset(new_mem + m_mem_length, 0, new_size - m_mem_length);

		m_mem = new_mem;
		m_mem_length = new_size;

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	MemoryStream& MemoryStream::operator << (int32_t value)
	{
		char buf[32];
		int len = std_snprintf(buf, 32, "%d", value);
		write(buf, len);

		return *this;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	MemoryStream& MemoryStream::operator << (uint32_t value)
	{
		char buf[32];
		int len = std_snprintf(buf, 32, "%u", value);
		write(buf, len);

		return *this;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	MemoryStream& MemoryStream::operator << (int64_t value)
	{
		char buf[32];
		int len = std_snprintf(buf, 32, "%" PRId64, value);
		write(buf, len);

		return *this;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	MemoryStream& MemoryStream::operator << (uint64_t value)
	{
		char buf[32];
		int len = std_snprintf(buf, 32, "%" PRIu64, value);
		write(buf, len);

		return *this;
	}

	MemoryReader::~MemoryReader() 
	{
	}

	void MemoryReader::close()
	{
	}

	size_t MemoryReader::getLength()
	{
		return m_mem_length;
	}

	size_t MemoryReader::setLength(size_t length)
	{
		return BAD_INDEX;
	}

	size_t MemoryReader::getPosition()
	{
		return m_mem_pos;
	}
	
	size_t MemoryReader::seek(SeekOrigin origin, intptr_t pos)
	{
		size_t newPos = m_mem_pos;
		
		switch (origin)
		{
		case SeekOrigin::Begin:
			newPos = pos;
			break;
		case SeekOrigin::Current:
			newPos = m_mem_pos + pos;
			break;
		case SeekOrigin::End:
			newPos = m_mem_length + pos;
			break;
		}

		if (newPos < 0)
			newPos = 0;
		if (newPos > m_mem_length)
			newPos = m_mem_length;

		return m_mem_pos = newPos;
	}

	size_t MemoryReader::write(const void* data, size_t len)
	{
		return BAD_INDEX;
	}
	
	size_t MemoryReader::read(void* buffer, size_t size)
	{
		size_t to_copy = m_mem_pos + size > m_mem_length ? m_mem_length - m_mem_pos : size;

		if (to_copy > 0)
		{
			memcpy(buffer, m_mem + m_mem_pos, to_copy);

			m_mem_pos += to_copy;
		}

		return to_copy;
	}
}
//-----------------------------------------------

