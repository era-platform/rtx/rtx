﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_date_time.h"

namespace rtl
{
	//--------------------------------------
	//
	//--------------------------------------
#define DELTA_EPOCH_IN_MICROSECS	11644473600000000Ui64
#define __epoch						2208988800ULL
#define __ntp_scale_frac			4294967295ULL
#if defined (TARGET_OS_WINDOWS)
#ifndef timeval
	struct timeval
	{
		long tv_sec;
		long tv_usec;
	};
#endif
#endif
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API DateTime& DateTime::assign(const tm& tm)
	{
		m_year = tm.tm_year + 1900;
		m_month = tm.tm_mon + 1;
		m_day = tm.tm_mday;
		m_hour = tm.tm_hour;
		m_minute = tm.tm_min;
		m_second = tm.tm_sec;
		m_millisecond = 0;

		return *this;
	}
	uint64_t DateTime::getGTS() const
	{
		time_t t;
		tm tms = { 0 };

		tms.tm_year = int(m_year - 1900);
		tms.tm_mon = int(m_month - 1);
		tms.tm_mday = m_day;
		tms.tm_hour = m_hour;
		tms.tm_min = m_minute;
		tms.tm_sec = m_second;

		t = mktime(&tms);

		return (uint64_t)t * 1000 + m_millisecond;
	}
	//--------------------------------------
	//
	//--------------------------------------
	const DateTime DateTime::now()
	{
		uint64_t ts;
		DateTime& dt = *(DateTime*)&ts;

#if defined (TARGET_OS_WINDOWS)

		SYSTEMTIME system_time;
		GetLocalTime(&system_time);

		dt.m_year = system_time.wYear;
		dt.m_month = system_time.wMonth;
		dt.m_day = system_time.wDay;
		dt.m_hour = system_time.wHour;
		dt.m_minute = system_time.wMinute;
		dt.m_second = system_time.wSecond;
		dt.m_millisecond = system_time.wMilliseconds;

#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)

		timeval tim;
		int res = gettimeofday(&tim, NULL);
		if (res != 0)
		{
			int err = errno;
			LOG_ERROR("datetime", "date_time.now -- gettimeofday() failed. error = %d.", err);
		}

		tm* t = localtime(&tim.tv_sec);

		dt.m_year = t->tm_year + 1900;
		dt.m_month = t->tm_mon + 1;
		dt.m_day = t->tm_mday;
		dt.m_hour = t->tm_hour;
		dt.m_minute = t->tm_min;
		dt.m_second = t->tm_sec;
		dt.m_millisecond = tim.tv_usec / 1000;

#endif

		return dt;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API uint64_t DateTime::getCurrentGTS()
	{
#if defined(TARGET_OS_WINDOWS)
		FILETIME ft = { 0 };
		GetSystemTimeAsFileTime(&ft);

		uint64_t ft_ns = uint64_t(ft.dwHighDateTime) << 32;
		ft_ns |= ft.dwLowDateTime;

		/*converting file time to unix epoch*/
		ft_ns /= 10;  /*convert into microseconds*/
		ft_ns -= DELTA_EPOCH_IN_MICROSECS;

		return ft_ns / 1000;

		//uint32_t tv_sec = (uint32_t)(ft_ns / 1000000UL);
		//uint32_t tv_usec = (uint32_t)(ft_ns % 1000000UL);

		//uint64_t ntp_sec = tv_sec + __epoch;
		//uint64_t ntp_frac = ((uint64_t)tv_usec * (uint64_t)__ntp_scale_frac) / (uint64_t)1000000;

		//return (ntp_sec << 32) | ntp_frac;
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		timeval tv = { 0, 0 };
		int res = gettimeofday(&tv, NULL);
		if (res != 0)
		{
			int err = errno;
			LOG_ERROR("timer", "performance_counter.get_elapsed_ms -- gettimeofday() failed. error = %d.", err);
		}

		// 
		return ((uint64_t)tv.tv_sec) * 1000 + ((uint64_t)tv.tv_usec / 1000);
#endif
	}
	//--------------------------------------
	// Unix time
	//--------------------------------------
	RTX_STD_API DateTime DateTime::parseGTS(uint64_t gtt)
	{
		DateTime dt;
		time_t tv_sec = long(gtt / 1000);
		uint32_t tv_msec = long(gtt % 1000);

		tm* t = localtime(&tv_sec);

		dt.m_year = t->tm_year + 1900;
		dt.m_month = t->tm_mon + 1;
		dt.m_day = t->tm_mday;
		dt.m_hour = t->tm_hour;
		dt.m_minute = t->tm_min;
		dt.m_second = t->tm_sec;
		dt.m_millisecond = tv_msec;

		return dt;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API int64_t DateTime::subtractGTS(uint64_t gttA, uint64_t gttB)
	{
		return gttA - gttB;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API int64_t DateTime::addGTS(uint64_t gttA, uint64_t gttB)
	{
		return gttA + gttB;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API int DateTime::compareGTS(uint64_t gttA, uint64_t gttB)
	{
		int64_t res = gttA - gttB;
		return res > 0 ? 1 : res < 0 ? -1 : 0;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API uint32_t DateTime::getTicks()
	{
#if defined(TARGET_OS_WINDOWS)
		return ::GetTickCount();
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		timeval tv = { 0, 0 };
		int res = gettimeofday(&tv, NULL);
		if (res != 0)
		{
			int err = errno;
			LOG_ERROR("timer", "performance_counter.get_elapsed_ms -- gettimeofday() failed. error = %d.", err);
		}

		return (tv.tv_sec * 1000) + (tv.tv_usec / 1000.0);
#endif
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API uint64_t DateTime::getTicks64()
	{
#if defined(TARGET_OS_WINDOWS)
		return ::GetTickCount64();
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		timeval tv = { 0, 0 };
		int res = gettimeofday(&tv, NULL);
		if (res != 0)
		{
			int err = errno;
			LOG_ERROR("timer", "performance_counter.get_elapsed_ms -- gettimeofday() failed. error = %d.", err);
		}

		return (tv.tv_sec * 1000) + (tv.tv_usec / 1000.0);

#endif
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API uint64_t DateTime::getNTP()
	{
#if defined(TARGET_OS_WINDOWS)
		FILETIME ft = { 0 };
		GetSystemTimeAsFileTime(&ft);

		uint64_t ft_ns = uint64_t(ft.dwHighDateTime) << 32;
		ft_ns |= ft.dwLowDateTime;

		/*converting file time to unix epoch*/
		ft_ns /= 10;  /*convert into microseconds*/
		ft_ns -= DELTA_EPOCH_IN_MICROSECS;

		uint32_t tv_sec = (uint32_t)(ft_ns / 1000000UL);
		uint32_t tv_usec = (uint32_t)(ft_ns % 1000000UL);

		uint64_t ntp_sec = tv_sec + __epoch;
		uint64_t ntp_frac = ((uint64_t)tv_usec * (uint64_t)__ntp_scale_frac) / (uint64_t)1000000;

		return (ntp_sec << 32) | ntp_frac;
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		struct timeval tv;
		gettimeofday(&tv, NULL);
		uint64_t tv_ntp, tv_usecs;

		tv_ntp = tv.tv_sec + __epoch;
		tv_usecs = (__ntp_scale_frac * tv.tv_usec) / 1000000UL;

		return (tv_ntp << 32) | tv_usecs;
#endif
	}

}
//--------------------------------------
