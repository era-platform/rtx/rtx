﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_crypto.h"
//--------------------------------------
// Constants for md5_transform routine.
//--------------------------------------
#define S11 7
#define S12 12
#define S13 17
#define S14 22
#define S21 5
#define S22 9
#define S23 14
#define S24 20
#define S31 4
#define S32 11
#define S33 16
#define S34 23
#define S41 6
#define S42 10
#define S43 15
#define S44 21
//--------------------------------------
//
//--------------------------------------
static void md5_transform(uint32_t [4], const uint8_t [64]);
static void md5_encode(uint8_t*, uint32_t*, uint32_t);
static void md5_decode(uint32_t*, const uint8_t*, int);
static void hex_to_string(uint8_t* bin, char* hex);
//--------------------------------------
//
//--------------------------------------
static uint8_t PADDING[64] =
{
	0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};
//--------------------------------------
// F, G, H and I are basic MD5 functions.
//--------------------------------------
#define F(x, y, z) (((x) & (y)) | ((~x) & (z)))
#define G(x, y, z) (((x) & (z)) | ((y) & (~z)))
#define H(x, y, z) ((x) ^ (y) ^ (z))
#define I(x, y, z) ((y) ^ ((x) | (~z)))
//--------------------------------------
// ROTATE_LEFT rotates x left n bits.
//--------------------------------------
#define ROTATE_LEFT(x, n) (((x) << (n)) | ((x) >> (32-(n))))
//--------------------------------------
// FF, GG, HH, and II transformations for rounds 1, 2, 3, and 4.
// Rotation is separate from addition to prevent recomputation.
//--------------------------------------
#define FF(a, b, c, d, x, s, ac) { \
 (a) += F ((b), (c), (d)) + (x) + (uint32_t)(ac); \
 (a) = ROTATE_LEFT ((a), (s)); \
 (a) += (b); \
  }
#define GG(a, b, c, d, x, s, ac) { \
 (a) += G ((b), (c), (d)) + (x) + (uint32_t)(ac); \
 (a) = ROTATE_LEFT ((a), (s)); \
 (a) += (b); \
  }
#define HH(a, b, c, d, x, s, ac) { \
 (a) += H ((b), (c), (d)) + (x) + (uint32_t)(ac); \
 (a) = ROTATE_LEFT ((a), (s)); \
 (a) += (b); \
  }
#define II(a, b, c, d, x, s, ac) { \
 (a) += I ((b), (c), (d)) + (x) + (uint32_t)(ac); \
 (a) = ROTATE_LEFT ((a), (s)); \
 (a) += (b); \
  }
//--------------------------------------
// MD5 initialization. Begins an MD5 operation, writing a New context.
//--------------------------------------
RTX_STD_API md5_context_t::md5_context_t()
{
	m_count[0] = m_count[1] = 0;
	
	// Load magic initialization constants.
	m_state[0] = 0x67452301;
	m_state[1] = 0xefcdab89;
	m_state[2] = 0x98badcfe;
	m_state[3] = 0x10325476;

	memset(m_buffer, 0, sizeof(m_buffer));
}
//--------------------------------------
// MD5 initialization. Begins an MD5 operation, writing a New context.
//--------------------------------------
RTX_STD_API void md5_context_t::init()
{
	m_count[0] = m_count[1] = 0;
	
	// Load magic initialization constants.
	m_state[0] = 0x67452301;
	m_state[1] = 0xefcdab89;
	m_state[2] = 0x98badcfe;
	m_state[3] = 0x10325476;

	memset(m_buffer, 0, sizeof(m_buffer));
}
//--------------------------------------
// MD5 block update operation. Continues an MD5 message-digest
// operation, processing another message block, and updating the context.
//--------------------------------------
RTX_STD_API void md5_context_t::update(const void *data, int length)
{
	int i, index, partLen;
	const uint8_t* input = (const uint8_t*)data;
	// Compute number of bytes mod 64
	index = ((uint32_t)(m_count[0] >> 3) & 0x3F);
	
	// update number of bits
	if ((m_count[0] += ((uint32_t)length << 3)) < ((uint32_t)length << 3))
	{
		m_count[1]++;
	}
	m_count[1] += ((uint32_t)length >> 29);

	partLen = 64 - index;

	// Transform as many times as possible.

	if (length >= partLen)
	{
		memcpy(m_buffer+index, input, partLen);
		md5_transform(m_state, m_buffer);

		for (i = partLen; i + 63 < length; i += 64)
		{
			md5_transform(m_state, &input[i]);
		}

		index = 0;
	}
	else
		i = 0;

	// Buffer remaining input
	memcpy(&m_buffer[index], &input[i], length-i);
}
//--------------------------------------
// MD5 finalization. Ends an MD5 message-digest operation, writing the
//  the message digest and zeroizing the context.
//--------------------------------------
RTX_STD_API void md5_context_t::final(uint8_t digest[MD5_HASH_SIZE])
{
	uint8_t bits[8];
	uint32_t index, padLen;

	// Save number of bits
	md5_encode(bits, m_count, 8);

	// Pad out to 56 mod 64.

	index = ((uint32_t)(m_count[0] >> 3) & 0x3f);
	padLen = (index < 56) ? (56 - index) : (120 - index);
	update(PADDING, padLen);

	// Append length (before padding)
	update(bits, 8);
	// Store state in digest
	md5_encode(digest, m_state, 16);

	// Zeroize sensitive information.
	m_state[0] = m_state[1] = m_state[2] = m_state[3] = 0;
	m_count[0] = m_count[1] = 0;
	memset(m_buffer, 0, sizeof(m_buffer));
}
//--------------------------------------
// MD5 finalization. Ends an MD5 message-digest operation, writing the
//  the message digest and zeroizing the context.
//--------------------------------------
RTX_STD_API void md5_context_t::final(char hash[MD5_HASH_HEX_SIZE])
{
	uint8_t bits[8];
	uint32_t index, padLen;
	uint8_t digest[MD5_HASH_SIZE];

	// Save number of bits
	md5_encode(bits, m_count, 8);

	// Pad out to 56 mod 64.

	index = ((uint32_t)(m_count[0] >> 3) & 0x3f);
	padLen = (index < 56) ? (56 - index) : (120 - index);
	update(PADDING, padLen);

	// Append length (before padding)
	update(bits, 8);
	// Store state in digest
	md5_encode(digest, m_state, 16);

	// Zeroize sensitive information.
	m_state[0] = m_state[1] = m_state[2] = m_state[3] = 0;
	m_count[0] = m_count[1] = 0;
	memset(m_buffer, 0, sizeof(m_buffer));

	hex_to_string(digest, hash);
}
//--------------------------------------
// MD5 basic transformation. Transforms state based on block.
//--------------------------------------
static void md5_transform(uint32_t state[4], const uint8_t block[64])
{
	uint32_t a = state[0], b = state[1], c = state[2], d = state[3], x[16];

	md5_decode(x, block, 64);

	/* Round 1 */
	FF (a, b, c, d, x[ 0], S11, 0xd76aa478); /* 1 */
	FF (d, a, b, c, x[ 1], S12, 0xe8c7b756); /* 2 */
	FF (c, d, a, b, x[ 2], S13, 0x242070db); /* 3 */
	FF (b, c, d, a, x[ 3], S14, 0xc1bdceee); /* 4 */
	FF (a, b, c, d, x[ 4], S11, 0xf57c0faf); /* 5 */
	FF (d, a, b, c, x[ 5], S12, 0x4787c62a); /* 6 */
	FF (c, d, a, b, x[ 6], S13, 0xa8304613); /* 7 */
	FF (b, c, d, a, x[ 7], S14, 0xfd469501); /* 8 */
	FF (a, b, c, d, x[ 8], S11, 0x698098d8); /* 9 */
	FF (d, a, b, c, x[ 9], S12, 0x8b44f7af); /* 10 */
	FF (c, d, a, b, x[10], S13, 0xffff5bb1); /* 11 */
	FF (b, c, d, a, x[11], S14, 0x895cd7be); /* 12 */
	FF (a, b, c, d, x[12], S11, 0x6b901122); /* 13 */
	FF (d, a, b, c, x[13], S12, 0xfd987193); /* 14 */
	FF (c, d, a, b, x[14], S13, 0xa679438e); /* 15 */
	FF (b, c, d, a, x[15], S14, 0x49b40821); /* 16 */

	/* Round 2 */
	GG (a, b, c, d, x[ 1], S21, 0xf61e2562); /* 17 */
	GG (d, a, b, c, x[ 6], S22, 0xc040b340); /* 18 */
	GG (c, d, a, b, x[11], S23, 0x265e5a51); /* 19 */
	GG (b, c, d, a, x[ 0], S24, 0xe9b6c7aa); /* 20 */
	GG (a, b, c, d, x[ 5], S21, 0xd62f105d); /* 21 */
	GG (d, a, b, c, x[10], S22,  0x2441453); /* 22 */
	GG (c, d, a, b, x[15], S23, 0xd8a1e681); /* 23 */
	GG (b, c, d, a, x[ 4], S24, 0xe7d3fbc8); /* 24 */
	GG (a, b, c, d, x[ 9], S21, 0x21e1cde6); /* 25 */
	GG (d, a, b, c, x[14], S22, 0xc33707d6); /* 26 */
	GG (c, d, a, b, x[ 3], S23, 0xf4d50d87); /* 27 */
	GG (b, c, d, a, x[ 8], S24, 0x455a14ed); /* 28 */
	GG (a, b, c, d, x[13], S21, 0xa9e3e905); /* 29 */
	GG (d, a, b, c, x[ 2], S22, 0xfcefa3f8); /* 30 */
	GG (c, d, a, b, x[ 7], S23, 0x676f02d9); /* 31 */
	GG (b, c, d, a, x[12], S24, 0x8d2a4c8a); /* 32 */

	/* Round 3 */
	HH (a, b, c, d, x[ 5], S31, 0xfffa3942); /* 33 */
	HH (d, a, b, c, x[ 8], S32, 0x8771f681); /* 34 */
	HH (c, d, a, b, x[11], S33, 0x6d9d6122); /* 35 */
	HH (b, c, d, a, x[14], S34, 0xfde5380c); /* 36 */
	HH (a, b, c, d, x[ 1], S31, 0xa4beea44); /* 37 */
	HH (d, a, b, c, x[ 4], S32, 0x4bdecfa9); /* 38 */
	HH (c, d, a, b, x[ 7], S33, 0xf6bb4b60); /* 39 */
	HH (b, c, d, a, x[10], S34, 0xbebfbc70); /* 40 */
	HH (a, b, c, d, x[13], S31, 0x289b7ec6); /* 41 */
	HH (d, a, b, c, x[ 0], S32, 0xeaa127fa); /* 42 */
	HH (c, d, a, b, x[ 3], S33, 0xd4ef3085); /* 43 */
	HH (b, c, d, a, x[ 6], S34,  0x4881d05); /* 44 */
	HH (a, b, c, d, x[ 9], S31, 0xd9d4d039); /* 45 */
	HH (d, a, b, c, x[12], S32, 0xe6db99e5); /* 46 */
	HH (c, d, a, b, x[15], S33, 0x1fa27cf8); /* 47 */
	HH (b, c, d, a, x[ 2], S34, 0xc4ac5665); /* 48 */

	/* Round 4 */
	II (a, b, c, d, x[ 0], S41, 0xf4292244); /* 49 */
	II (d, a, b, c, x[ 7], S42, 0x432aff97); /* 50 */
	II (c, d, a, b, x[14], S43, 0xab9423a7); /* 51 */
	II (b, c, d, a, x[ 5], S44, 0xfc93a039); /* 52 */
	II (a, b, c, d, x[12], S41, 0x655b59c3); /* 53 */
	II (d, a, b, c, x[ 3], S42, 0x8f0ccc92); /* 54 */
	II (c, d, a, b, x[10], S43, 0xffeff47d); /* 55 */
	II (b, c, d, a, x[ 1], S44, 0x85845dd1); /* 56 */
	II (a, b, c, d, x[ 8], S41, 0x6fa87e4f); /* 57 */
	II (d, a, b, c, x[15], S42, 0xfe2ce6e0); /* 58 */
	II (c, d, a, b, x[ 6], S43, 0xa3014314); /* 59 */
	II (b, c, d, a, x[13], S44, 0x4e0811a1); /* 60 */
	II (a, b, c, d, x[ 4], S41, 0xf7537e82); /* 61 */
	II (d, a, b, c, x[11], S42, 0xbd3af235); /* 62 */
	II (c, d, a, b, x[ 2], S43, 0x2ad7d2bb); /* 63 */
	II (b, c, d, a, x[ 9], S44, 0xeb86d391); /* 64 */

	state[0] += a;
	state[1] += b;
	state[2] += c;
	state[3] += d;

	// Zeroize sensitive information.
	memset(x, 0, sizeof (x));
}

//--------------------------------------
// Encodes input (uint32_t) into output (unsigned char). Assumes len is
// a multiple of 4.
//--------------------------------------
static void md5_encode(uint8_t *output, uint32_t *input, uint32_t len)
{
	uint32_t i, j;

	for (i = 0, j = 0; j < len; i++, j += 4)
	{
		output[j]   = (unsigned char)(input[i] & 0xff);
		output[j+1] = (unsigned char)((input[i] >> 8) & 0xff);
		output[j+2] = (unsigned char)((input[i] >> 16) & 0xff);
		output[j+3] = (unsigned char)((input[i] >> 24) & 0xff);
	}
}
//--------------------------------------
// Decodes input (unsigned char) into output (uint32_t). Assumes len is
// a multiple of 4.
//--------------------------------------
static void md5_decode(uint32_t *output, const uint8_t *input, int len)
{
	int i, j;

	for (i = 0, j = 0; j < len; i++, j += 4)
		output[i] = ((uint32_t)input[j]) | (((uint32_t)input[j+1]) << 8) |
			(((uint32_t)input[j+2]) << 16) | (((uint32_t)input[j+3]) << 24);
}
//--------------------------------------
// bnary to hex text
//--------------------------------------
static void hex_to_string(uint8_t* bin, char* hex)
{
    uint8_t j;

    for (int i = 0; i < MD5_HASH_SIZE; i++)
	{
        j = (bin[i] >> 4) & 0xf;
        if (j <= 9)
		{
			hex[i*2] = (j + '0');
		}
		else
		{
			hex[i*2] = (j + 'a' - 10);
		}
        
		j = bin[i] & 0xf;
        if (j <= 9)
		{
			hex[i*2+1] = (j + '0');
		}
		else
		{
			hex[i*2+1] = (j + 'a' - 10);
		}
    };

    hex[MD5_HASH_HEX_SIZE] = '\0';
};
//--------------------------------------
// calculate H(A1) as per spec
//--------------------------------------
RTX_STD_API void DigestCalcHA1(const char* alg, const char* userName, const char* realm, const char* pswd, char* hash1)
{
	md5_context_t Md5Ctx;
	uint8_t HA1[MD5_HASH_SIZE];

	Md5Ctx.update((uint8_t*)userName, std_strlen(userName));
	Md5Ctx.update((uint8_t*)":", 1);
	Md5Ctx.update((uint8_t*)realm, std_strlen(realm));
	Md5Ctx.update((uint8_t*)":", 1);
	Md5Ctx.update((uint8_t*)pswd, std_strlen(pswd));
	Md5Ctx.final((uint8_t*)HA1);

	hex_to_string(HA1, hash1);
};
//--------------------------------------
// calculate H(A1) as per spec
//--------------------------------------
RTX_STD_API void DigestCalcHA1_Sess(const char* alg, const char* userName, const char* realm, const char* pswd,
									 const char* nonce, const char* cnonce, char* hash1)
{
	md5_context_t Md5Ctx;
	uint8_t HA1[MD5_HASH_SIZE];

	Md5Ctx.update((uint8_t*)userName, std_strlen(userName));
	Md5Ctx.update((uint8_t*)":", 1);
	Md5Ctx.update((uint8_t*)realm, std_strlen(realm));
	Md5Ctx.update((uint8_t*)":", 1);
	Md5Ctx.update((uint8_t*)pswd, std_strlen(pswd));
	Md5Ctx.final((uint8_t*)HA1);

	if (std_stricmp(alg, "md5-sess") == 0)
	{
		Md5Ctx.init();
		Md5Ctx.update((uint8_t*)HA1, MD5_HASH_SIZE);
		Md5Ctx.update((uint8_t*)":", 1);
		Md5Ctx.update((uint8_t*)nonce, std_strlen(nonce));
		Md5Ctx.update((uint8_t*)":", 1);
		Md5Ctx.update((uint8_t*)cnonce, std_strlen(cnonce));
		Md5Ctx.final((uint8_t*)HA1);
	};

	hex_to_string(HA1, hash1);
};
//--------------------------------------
// calculate request-digest/response-digest as per HTTP Digest spec
//--------------------------------------
RTX_STD_API void DigestCalcHA2(const char* method, const char* digestUri, char* hash2)
{
	md5_context_t Md5Ctx;
	uint8_t HA2[MD5_HASH_SIZE];
	uint8_t colon = ':';

	memset(HA2, 0, sizeof HA2);

	// calculate H(A2)
	Md5Ctx.update((uint8_t*)method, std_strlen(method));
	Md5Ctx.update(&colon, 1);
	Md5Ctx.update((uint8_t*)digestUri, std_strlen(digestUri));
	
	Md5Ctx.final((uint8_t*)HA2);
	
	hex_to_string(HA2, hash2);
};
//--------------------------------------
// calculate request-digest/response-digest as per HTTP Digest spec
//--------------------------------------
RTX_STD_API void DigestCalcHA2_Int(const char* method, const char* digestUri, const char* qop,
								const char* hentity, char* hash2)
{
	md5_context_t Md5Ctx;
	uint8_t HA2[MD5_HASH_SIZE];
	uint8_t colon = ':';

	memset(HA2, 0, sizeof HA2);

	// calculate H(A2)
	Md5Ctx.update((uint8_t*)method, std_strlen(method));
	Md5Ctx.update(&colon, 1);
	Md5Ctx.update((uint8_t*)digestUri, std_strlen(digestUri));
	
	if (std_stricmp(qop, "auth-int") == 0)
	{
		Md5Ctx.update(&colon, 1);
		Md5Ctx.update((uint8_t*)hentity, MD5_HASH_HEX_SIZE);
	};

	Md5Ctx.final((uint8_t*)HA2);

	hex_to_string(HA2, hash2);
};
//--------------------------------------
// calculate request-digest/response-digest as per HTTP Digest spec
//--------------------------------------
RTX_STD_API void DigestCalcResponse(const char* HA1, const char* nonce, const char* HA2, char* response)
{
	md5_context_t Md5Ctx;
	uint8_t respHash[MD5_HASH_SIZE];
	uint8_t colon = ':';

	memset(respHash, 0, sizeof respHash);

	// calculate response
	Md5Ctx.init();
	Md5Ctx.update((uint8_t*)HA1, MD5_HASH_HEX_SIZE);
	Md5Ctx.update(&colon, 1);
	Md5Ctx.update((uint8_t*)nonce, std_strlen(nonce));
	Md5Ctx.update(&colon, 1);
	Md5Ctx.update((uint8_t*)HA2, MD5_HASH_HEX_SIZE);
	Md5Ctx.final((uint8_t*)respHash);
	
	hex_to_string(respHash, response);
};
//--------------------------------------
// calculate request-digest/response-digest as per HTTP Digest spec
//--------------------------------------
RTX_STD_API void DigestCalcResponse_Qop(const char* HA1, const char* nonce, const char* nonceCount,
									 const char* cnonce, const char* qop, const char* HA2, char* response)
{
	md5_context_t Md5Ctx;
	uint8_t respHash[MD5_HASH_SIZE];
	uint8_t colon = ':';

	memset(respHash, 0, sizeof respHash);

	// calculate response
	Md5Ctx.init();
	Md5Ctx.update((uint8_t*)HA1, MD5_HASH_HEX_SIZE);
	Md5Ctx.update(&colon, 1);
	Md5Ctx.update((uint8_t*)nonce, std_strlen(nonce));
	Md5Ctx.update(&colon, 1);
	
	if (*qop)
	{
		Md5Ctx.update((uint8_t*)nonceCount, std_strlen(nonceCount));
		Md5Ctx.update(&colon, 1);
		Md5Ctx.update((uint8_t*)cnonce, std_strlen(cnonce));
		Md5Ctx.update(&colon, 1);
		Md5Ctx.update((uint8_t*)qop, std_strlen(qop));
		Md5Ctx.update(&colon, 1);
	};

	Md5Ctx.update((uint8_t*)HA2, MD5_HASH_HEX_SIZE);

	Md5Ctx.final((uint8_t*)respHash);
	
	hex_to_string(respHash, response);
};
//--------------------------------------
// PPP in HDLC-like Framing (RFC 1662).
//--------------------------------------
//static uint32_t fcstab_32[256] =
//{
//	0x00000000, 0x77073096, 0xee0e612c, 0x990951ba,	0x076dc419, 0x706af48f, 0xe963a535, 0x9e6495a3,
//	0x0edb8832, 0x79dcb8a4, 0xe0d5e91e, 0x97d2d988,	0x09b64c2b, 0x7eb17cbd, 0xe7b82d07, 0x90bf1d91,
//	0x1db71064, 0x6ab020f2, 0xf3b97148, 0x84be41de,	0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7,
//	0x136c9856, 0x646ba8c0, 0xfd62f97a, 0x8a65c9ec,	0x14015c4f, 0x63066cd9, 0xfa0f3d63, 0x8d080df5,
//	0x3b6e20c8, 0x4c69105e, 0xd56041e4, 0xa2677172,	0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b,
//	0x35b5a8fa, 0x42b2986c, 0xdbbbc9d6, 0xacbcf940,	0x32d86ce3, 0x45df5c75, 0xdcd60dcf, 0xabd13d59,
//	0x26d930ac, 0x51de003a, 0xc8d75180, 0xbfd06116,	0x21b4f4b5, 0x56b3c423, 0xcfba9599, 0xb8bda50f,
//	0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924,	0x2f6f7c87, 0x58684c11, 0xc1611dab, 0xb6662d3d,
//	0x76dc4190, 0x01db7106, 0x98d220bc, 0xefd5102a,	0x71b18589, 0x06b6b51f, 0x9fbfe4a5, 0xe8b8d433,
//	0x7807c9a2, 0x0f00f934, 0x9609a88e, 0xe10e9818,	0x7f6a0dbb, 0x086d3d2d, 0x91646c97, 0xe6635c01,
//	0x6b6b51f4, 0x1c6c6162, 0x856530d8, 0xf262004e,	0x6c0695ed, 0x1b01a57b, 0x8208f4c1, 0xf50fc457,
//	0x65b0d9c6, 0x12b7e950, 0x8bbeb8ea, 0xfcb9887c,	0x62dd1ddf, 0x15da2d49, 0x8cd37cf3, 0xfbd44c65,
//	0x4db26158, 0x3ab551ce, 0xa3bc0074, 0xd4bb30e2,	0x4adfa541, 0x3dd895d7, 0xa4d1c46d, 0xd3d6f4fb,
//	0x4369e96a, 0x346ed9fc, 0xad678846, 0xda60b8d0,	0x44042d73, 0x33031de5, 0xaa0a4c5f, 0xdd0d7cc9,
//	0x5005713c, 0x270241aa, 0xbe0b1010, 0xc90c2086,	0x5768b525, 0x206f85b3, 0xb966d409, 0xce61e49f,
//	0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4,	0x59b33d17, 0x2eb40d81, 0xb7bd5c3b, 0xc0ba6cad,
//	0xedb88320, 0x9abfb3b6, 0x03b6e20c, 0x74b1d29a,	0xead54739, 0x9dd277af, 0x04db2615, 0x73dc1683,
//	0xe3630b12, 0x94643b84, 0x0d6d6a3e, 0x7a6a5aa8,	0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1,
//	0xf00f9344, 0x8708a3d2, 0x1e01f268, 0x6906c2fe,	0xf762575d, 0x806567cb, 0x196c3671, 0x6e6b06e7,
//	0xfed41b76, 0x89d32be0, 0x10da7a5a, 0x67dd4acc,	0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5,
//	0xd6d6a3e8, 0xa1d1937e, 0x38d8c2c4, 0x4fdff252,	0xd1bb67f1, 0xa6bc5767, 0x3fb506dd, 0x48b2364b,
//	0xd80d2bda, 0xaf0a1b4c, 0x36034af6, 0x41047a60,	0xdf60efc3, 0xa867df55, 0x316e8eef, 0x4669be79,
//	0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236,	0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f,
//	0xc5ba3bbe, 0xb2bd0b28, 0x2bb45a92, 0x5cb36a04,	0xc2d7ffa7, 0xb5d0cf31, 0x2cd99e8b, 0x5bdeae1d,
//	0x9b64c2b0, 0xec63f226, 0x756aa39c, 0x026d930a,	0x9c0906a9, 0xeb0e363f, 0x72076785, 0x05005713,
//	0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0x0cb61b38,	0x92d28e9b, 0xe5d5be0d, 0x7cdcefb7, 0x0bdbdf21,
//	0x86d3d2d4, 0xf1d4e242, 0x68ddb3f8, 0x1fda836e,	0x81be16cd, 0xf6b9265b, 0x6fb077e1, 0x18b74777,
//	0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c,	0x8f659eff, 0xf862ae69, 0x616bffd3, 0x166ccf45,
//	0xa00ae278, 0xd70dd2ee, 0x4e048354, 0x3903b3c2,	0xa7672661, 0xd06016f7, 0x4969474d, 0x3e6e77db,
//	0xaed16a4a, 0xd9d65adc, 0x40df0b66, 0x37d83bf0,	0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9,
//	0xbdbdf21c, 0xcabac28a, 0x53b39330, 0x24b4a3a6,	0xbad03605, 0xcdd70693, 0x54de5729, 0x23d967bf,
//	0xb3667a2e, 0xc4614ab8, 0x5d681b02, 0x2a6f2b94,	0xb40bbe37, 0xc30c8ea1, 0x5a05df1b, 0x2d02ef8d
//};
////--------------------------------------
//// Calculates a new fcs given the current fcs and the new data.
////--------------------------------------
//uint32_t calculate_crc32(uint32_t fcs, const uint8_t* cp, int len)
//{
//	while (len--)
//	{
//		fcs = (((fcs) >> 8) ^ fcstab_32[((fcs) ^ (*cp++)) & 0xff]);
//	}
//
//	return fcs;
//}


// This implementation is based on the sample implementation in RFC 1952.

// CRC32 polynomial, in reversed form.
// See RFC 1952, or http://en.wikipedia.org/wiki/Cyclic_redundancy_check
static const uint32_t kCrc32Polynomial = 0xEDB88320;
static uint32_t kCrc32Table[256] = { 0 };

static void EnsureCrc32TableInited()
{
	const int crc32_table_size = sizeof(kCrc32Table)/sizeof(uint32_t);

	if (kCrc32Table[crc32_table_size - 1])
		return;  // already inited
	
	for (uint32_t i = 0; i < crc32_table_size; ++i)
	{
		uint32_t c = i;
		for (size_t j = 0; j < 8; ++j)
		{
			if (c & 1)
			{
				c = kCrc32Polynomial ^ (c >> 1);
			}
			else
			{
				c >>= 1;
			}
		}
		
		kCrc32Table[i] = c;
	}

	//Log.Binary("CRC32", (const uint8_t*)kCrc32Table, sizeof(kCrc32Table), "CRC32 calculated");
}

uint32_t calculate_crc32(uint32_t start, const uint8_t* data, int len)
{
	EnsureCrc32TableInited();

	uint32_t c = start ^ 0xFFFFFFFF;
	
	for (int i = 0; i < len; ++i)
	{
		c = kCrc32Table[(c ^ data[i]) & 0xFF] ^ (c >> 8);
	}
  
	return c ^ 0xFFFFFFFF;
}

//--------------------------------------
//
//--------------------------------------
bool crypto_get_random(uint8_t* buffer, uint32_t length)
{
	uint32_t *dst = (uint32_t*)buffer;
	int int_len = length / 4;
	int byte_len = length % 4;
	uint32_t val = 0;

	srand(rtl::DateTime::getTicks());

	while (int_len)
	{
		val = rand();
		*dst++ = val;
		int_len--;
	}

	if (byte_len > 0)
	{
		uint8_t* bytes = (uint8_t*)dst;
		val = rand();

		while (byte_len)
		{
			*bytes++ = val >> (--byte_len * 8);
		}
	}

	return true;
	///* Generic C-library (rand()) version */
	///* This is a random source of last resort */
	//uint8_t *dst = (uint8_t *)buffer;
	//while (length)
	//{
	//	int val = rand();
	//	/* rand() returns 0-32767 (ugh) */
	//	/* Is this a good enough way to get random bytes?
	//	It is if it passes FIPS-140... */
	//	*dst++ = val & 0xff;
	//	length--;
	//}
}
//--------------------------------------

