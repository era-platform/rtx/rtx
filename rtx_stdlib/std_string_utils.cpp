/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_string_utils.h"

namespace rtl
{
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API char* strdup(const char* src, int len)
	{
		if (src == nullptr || src[0] == 0 || len == 0)
			return nullptr;

		int llen = len < 0 ? (int)strlen(src) : len;

		char* dst = (char*)MALLOC(llen + 1);
		strncpy(dst, src, llen);
		dst[llen] = 0;

		return dst;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API char* strupdate(char*& ptr, const char* src, int len)
	{
		if (ptr != nullptr)
		{
			FREE(ptr);
			ptr = nullptr;
		}

		if (src == nullptr || src[0] == 0 || len == 0)
			return nullptr;

		int llen = len < 0 ? (int)strlen(src) : len;

		ptr = (char*)MALLOC(llen + 1);
		strncpy(ptr, src, llen);
		ptr[llen] = 0;

		return ptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int strupdatel(char*& ptr, const char* src, int len)
	{
		if (ptr != nullptr)
		{
			FREE(ptr);
			ptr = nullptr;
		}

		if (src == nullptr || src[0] == 0 || len == 0)
			return 0;

		int llen = len < 0 ? (int)strlen(src) : len;

		ptr = (char*)MALLOC(llen + 1);
		strncpy(ptr, src, llen);
		ptr[llen] = 0;

		return llen;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API char* strupr(char* str)
	{
		char* p = str;
		while (*p != 0)
		{
			*p = toupper(*p);
			p++;
		}
		return str;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API char* strlwr(char* str)
	{
		char* p = str;
		while (*p != 0)
		{
			*p = tolower(*p);
			p++;
		}
		return str;
	}//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API char* strtrim(char* text, const char* char_set)
	{
		if (text != nullptr && text[0] != 0)
		{
			int len = (int)strlen(text);
			int llen = 0;

			char* ptr = text;

			while (*ptr != 0 && ::strchr(char_set, *ptr++) != nullptr);

			llen = PTR_DIFF(ptr, text) - 1;
			if (llen > 0)
				memmove(text, ptr - 1, len - llen);

			len -= llen;

			char* end = text + len - 1;
			ptr = end;

			while (ptr >= text && ::strchr(char_set, *ptr--) != nullptr);
			ptr += 2;

			*ptr = 0;
		}

		return text;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API int sprintf(char*& ptr, const char* format, ...)
	{
		int len;
		va_list ap;

		/* initialize variable arguments */
		va_start(ap, format);
		/* compute */
		len = vsprintf(ptr, format, &ap);
		/* reset variable arguments */
		va_end(ap);

		return len;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API int vsprintf(char*& ptr, const char* format, va_list* ap)
	{
		int len = 0;
		/* FREE previous value */
		if (ptr != nullptr)
		{
			FREE(ptr);
		}

		/* needed for 64bit platforms where vsnprintf will change the va_list */
		/* compute destination len for windows mobile */
		len = vsnprintf(0, 0, format, *ap);
		if (len > 0)
		{
			ptr = (char*)MALLOC(len + 1);
			len = vsnprintf(ptr, len, format, *ap);
			ptr[len] = 0;
		}

		return len;
	}
	//--------------------------------------
	// ���������� ��������� �� ��������� ������ ����� �����������
	//--------------------------------------
	RTX_STD_API const char* strtok(const char* str, int delimeter)
	{
		const char* cp;

		if ((cp = ::strchr(str, delimeter)) == nullptr)
			return nullptr;

		while (*cp == delimeter) cp++;

		return cp;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API int strcmp(const char* l, const char* r)
	{
		if (l == nullptr || l[0] == 0)
		{
			return r == nullptr || r[0] == 0 ? 0 : -1;
		}

		if (r == nullptr || r[0] == 0)
		{
			return 1;
		}

		return ::strcmp(l, r);
	}
	//--------------------------------------
	//
	//--------------------------------------
	static uint8_t hex_to_byte(char ch, bool lo);
	//static char byte_to_hex(uint8_t ch, bool lo);
	//--------------------------------------
	// LWSP = *(WSP / COMMENT / EOL)
	//--------------------------------------
	RTX_STD_API const char* skip_LWSP(const char* text)
	{
		while (*text == ' ' || *text == '\t' || *text == '\r' || *text == '\n' || *text == ';')
		{
			if (*text == ';') // comment!
			{
				text = ::strchr(text, '\n');
				continue;
			}
			else
			{
				text++;
			}
		}

		return text;
	}
	//--------------------------------------
	// SEP = (WSP / EOL / COMMENT) LWSP
	//--------------------------------------
	RTX_STD_API const char* skip_SEP(const char* text)
	{
		while (*text == ' ' || *text == '\t' || *text == '\r' || *text == '\n' || *text == ';')
		{
			if (*text == ';') // comment!
			{
				text = ::strchr(text, '\n');
				continue;
			}
			else
			{
				text++;
			}
		}

		return text;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API const char* next_LWSP(const char* text)
	{
		while (*text != 0 && *text != ' ' && *text != '\t' && *text != '\r' && *text != '\n' && *text != ';')
		{
			text++;
		}

		return text;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API const char* next_SEP(const char* text)
	{
		while (*text != 0 && *text != ' ' && *text != '\t' && *text != '\r' && *text != '\n' && *text != ';')
		{
			text++;
		}

		return text;
	}
	//--------------------------------------
	// string of max 8 hexdigits to uint32_t
	//--------------------------------------
	RTX_STD_API uint32_t hex_to_uint(const char* text, int length)
	{
		uint32_t result = 0;
		uint8_t* res_ptr = (uint8_t*)&result;
		if (length > 8)
		{
			length = 8;
		}

		bool even_len = (length & 1) == 0;

		for (int i = length - 1; i >= 0; i--)
		{

			bool lo = even_len ? (i & 1) != 0 : (i & 1) == 0;
			*res_ptr |= hex_to_byte(text[i], lo);
			if (!lo)
				res_ptr++;
		}

		return result;
	}
	//--------------------------------------
	// ABCDEF01234567890 1234567ABCD
	//--------------------------------------
	RTX_STD_API int hex_to_bytes(const char* text, int length, uint8_t* buffer, int size)
	{
		if (size * 2 < length)
			return 0;

		bool even_len = (length & 1) == 0;
		uint8_t* res_ptr = buffer;
		*res_ptr = 0;

		for (int i = 0; i < length; i++)
		{
			bool lo = even_len ? (i & 1) != 0 : (i & 1) == 0;

			*res_ptr |= hex_to_byte(text[i], lo);

			if (lo)
			{
				*++res_ptr = 0;
			}
		}

		return PTR_DIFF(res_ptr, buffer);
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API int bytes_to_hex(char* stream, int size, const uint8_t* buffer, int length)
	{
		if (length * 2 > size)
			return 0;

		char* ptr = stream;

		for (int i = 0; i < length; i++)
		{
			uint8_t ch = (buffer[i] & 0xF0) >> 4;
			*ptr++ = ch < 10 ? ch + '0' : (ch - 10) + 'A';
			ch = buffer[i] & 0x0F;
			*ptr++ = ch < 10 ? ch + '0' : (ch - 10) + 'A';
		}

		return length * 2;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API int utf8_to_locale(char* buffer, int size, const char* str, int length)
	{
		if (length == -1)
			length = (int)strlen(str);

		int b_len = 0;

		wchar_t* ubuffer = (wchar_t*)MALLOC((length + 1) * 2);

		int u_len = utf8_to_wcs(ubuffer, length + 1, str, length);

		Log.log("IVR", "utf8 to locale wcs len %d", u_len);

		if (u_len <= 0)
		{
			FREE(ubuffer);
			buffer[0] = 0;
			return 0;
		}

		ubuffer[u_len] = 0;

		setlocale(LC_ALL, "ru-RU");

		b_len = (int)wcstombs(buffer, ubuffer, u_len);

		Log.log("IVR", "utf8 to locale b_len %d", b_len);

		if (b_len > 0)
		{
			buffer[b_len] = 0;
		}

		FREE(ubuffer);

		Log.log("IVR", "utf8 to locale -> %s", buffer);

		return b_len;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API String utf8_to_locale(const String& str)
	{
		String result;

		//Log.log("IVR", "utf8 to locale %s", (const char*)str);

		int str_length = str.getLength();

		wchar_t* ubuffer = (wchar_t*)MALLOC((str_length + 1) * sizeof(wchar_t));

		int u_len = utf8_to_wcs(ubuffer, str_length + 1, str, str.getLength());

		//Log.log("IVR", "wcs len %d", u_len);

		if (u_len <= 0)
		{
			FREE(ubuffer);
			return String::empty;
		}

		ubuffer[u_len] = 0;

		//Log.log("IVR", "wcs %S", ubuffer);

		char* buffer = (char*)MALLOC(str_length + 1);

		int b_len = (int)wcstombs(buffer, ubuffer, str_length + 1);

		//Log.log("IVR", "locale len %d", b_len);

		if (b_len < 0)
		{
			FREE(ubuffer);
			FREE(buffer);

			return result;
		}

		buffer[b_len] = 0;

		//Log.log("IVR", "locale %s", buffer);

		result = buffer;

		FREE(ubuffer);
		FREE(buffer);


		//Log.log("IVR", "result %s", (const char*)result);

		return result;
	}
	//--------------------------------------
	// UTF8 to UNICODE (16LE)
	//--------------------------------------
	RTX_STD_API int utf8_to_wcs(wchar_t* dest, int size, const char* utf8, int length)
	{
		int written = 0;
		int read = 0;

		if (length == -1)
			length = (int)strlen(utf8);

		while (written < size && read < length)
		{
			char ch = utf8[read++];

			// end of string
			if (ch == 0)
			{
				dest[written] = 0;
				return written;
			}

			if ((ch & 0x80) == 0)  // 1 byte
			{
				dest[written++] = ch; // as is
			}
			else if ((ch & 0xE0) == 0xC0) // two bytes : 110xxxxx 10xxxxxx
			{
				if (read >= length)
				{
					dest[written] = 0;
					return written;
				}

				char ch2 = utf8[read++];

				if ((ch2 & 0xC0) != 0x80) // not second byte
				{
					return -1;
				}

				dest[written++] = ((ch & 0x1F) << 6) | (ch2 & 0x3F);
			}
			else if ((ch & 0xF0) == 0xE0) // three bytes : 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
			{
				if (read >= length - 1)
				{
					dest[written] = 0;
					return written;
				}

				char ch2 = utf8[read++];
				char ch3 = utf8[read++];

				if (((ch2 & 0xC0) != 0x80) || ((ch3 & 0xC0) != 0x80)) // not sequense bytes
				{
					return -1;
				}

				dest[written++] = ((ch & 0x0F) << 12) | ((ch2 & 0x3F) << 6) | (ch3 & 0x3F);
			}
		}

		return written;
	}
	//--------------------------------------
	// UNICODE (16LE) to UTF8
	//--------------------------------------
	RTX_STD_API int wcs_to_utf8(char* utf8, int size, const wchar_t* wcs, int length)
	{
		char* b = utf8;
		int idx = 0;
		intptr_t wcs_len = length != BAD_INDEX ? length : wcslen(wcs);
		const wchar_t* end = wcs + wcs_len;

		utf8[size - 1] = 0;

		for (const wchar_t* p = wcs; p < end; p++)
		{
			wchar_t uc = *p;

			if (uc < 0x80)
			{
				if (idx++ < size) *b++ = (char)uc;
			}
			else if (uc < 0x800)
			{
				idx += 2;

				if (idx >= size)
				{
					return -1;
				}

				*b++ = 192 + uc / 64;
				*b++ = 128 + uc % 64;
			}
			else if (uc - 0xd800u < 0x800)
			{
				return -1;
			}
			else if (uc < 0x10000)
			{
				idx += 3;

				if (idx >= size)
				{
					return -1;
				}

				*b++ = 224 + uc / 4096;
				*b++ = 128 + uc / 64 % 64;
				*b++ = 128 + uc % 64;
			}
			else
			{
				return -1;
			}
		}

		return PTR_DIFF(b, utf8);
	}
	//--------------------------------------
	//
	//--------------------------------------
	static uint8_t hex_to_byte(char ch, bool lo)
	{
		uint8_t result = 0;

		if (ch >= '0' && ch <= '9')
		{
			result = ch - '0';
		}
		else if (ch >= 'A' && ch <= 'F')
		{
			result = ch - 'A' + 10;
		}
		else if (ch >= 'a' && ch <= 'f')
		{
			result = ch - 'a' + 10;
		}

		if (!lo)
		{
			result <<= 4;
		}

		return result;
	}
	//--------------------------------------
	//
	//--------------------------------------
	static bool is_char_rfc3986(char ch)
	{
		/*

		����        "$" | "-" | "_" | "." | "!" | "*" | "'" | "(" | ")" ,
		�� rfc3986  "-" | "_" | "." | "~"

		. - 0x2E
		- - 0x2D
		_ - 0x5F
		~ - 0x7E
		*/

		if (ch >= 'A' && ch <= 'Z')
			return true;
		if (ch >= 'a' && ch <= 'z')
			return true;
		if (ch >= '0' && ch <= '9')
			return true;
		if (ch == '-' || ch == '_' || ch == '.' || ch == '~')
			return true;

		return false;
	}
	//--------------------------------------
	// 4 byte buffer!
	//--------------------------------------
	static char* escape_char_rfc3986(char ch, char buf[4])
	{
		uint8_t t1 = ((uint8_t)ch) >> 4;
		uint8_t t2 = ((uint8_t)ch) & 0x0F;

		buf[0] = '%';
		buf[1] = t1 < 0xA ? t1 + '0' : (t1 - 0x0A) + 'A';
		buf[2] = t2 < 0xA ? t2 + '0' : (t2 - 0x0A) + 'A';
		//	buf[3] = 0;

		return buf;
	}
	//--------------------------------------
	// 4 byte buffer!
	//--------------------------------------
	static char tetrad_to_char(int digit, bool high)
	{
		char result = 0;

		if (digit >= '0' && digit <= '9')
			result = digit - '0';
		else if (digit >= 'A' && digit <= 'F')
			result = digit - 'A' + 10;
		else if (digit >= 'a' && digit <= 'f')
			result = digit - 'a' + 10;

		if (high)
			result <<= 4;

		return result;
	}
	//--------------------------------------
	// 4 byte buffer!
	//--------------------------------------
	//static char hex_to_char(const char* str, int lenght)
	//{
	//	if (lenght < 3)
	//		return 0;
	//
	//	return tetrad_to_char(str[2], false) | tetrad_to_char(str[1], true);
	//}
	//--------------------------------------
	//
	//--------------------------------------
	String escape_rfc3986(const String& str)
	{
		String result;
		//const char* xlat = str;
		char buf[4] = { 0 };

		for (const char* it = str; *it != 0; it++)
		{
			if (is_char_rfc3986(*it))
				result << *it;
			else
				result << escape_char_rfc3986(*it, buf);
		}

		return result;
	}
	//--------------------------------------
	//
	//--------------------------------------
	String unescape_rfc3986(const String& str)
	{
		String xlat = str;

		int pos;

		pos = (int)-1;

		while ((pos = xlat.indexOf('%', pos + 1)) != STR_BADINDEX)
		{
			int digit1 = xlat[pos + 1];
			int digit2 = xlat[pos + 2];

			if (isxdigit(digit1) && isxdigit(digit2))
			{
				xlat.setAt(pos, (char)(
					(isdigit(digit2) ? (digit2 - '0') : (toupper(digit2) - 'A' + 10)) +
					((isdigit(digit1) ? (digit1 - '0') : (toupper(digit1) - 'A' + 10)) << 4)));
				xlat.remove(pos + 1, 2);
			}
		}

		return xlat;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int escape_rfc3986(char* buffer, int size, const char* str, int length)
	{
		if (length == -1)
		{
			length = (int)strlen(str);
		}

		int idx = 0;

		for (int i = 0; i < length; i++)
		{
			const char* it = str + i;

			if (is_char_rfc3986(*it))
			{
				if (idx <= size)
				{
					buffer[idx++] = *it;
				}
				else
				{
					break;
				}
			}
			else
			{
				if (idx < size - 3)
				{
					escape_char_rfc3986(*it, buffer + idx);
					idx += 3;
				}
				else
				{
					break;
				}
			}
		}

		return idx;
	}
	//--------------------------------------
	//
	//--------------------------------------
	char* unescape_rfc3986(char* buffer, int size, const char* str, int length)
	{
		if (length > size)
		{
			buffer[0] = 0;
			return buffer;
		}

		if (length == -1)
		{
			length = (int)strlen(str);
		}

		int r_idx = 0;

		for (int i = 0; i < length; i++)
		{
			if (str[i] == '%')
			{
				if (length - i < 3)
				{
					buffer[r_idx] = 0;
					return buffer;
				}

				buffer[r_idx++] = tetrad_to_char(str[i + 2], false) | tetrad_to_char(str[i + 1], true);//hex_to_char(str + i, length - i);
				i += 2;
			}
			else
			{
				buffer[r_idx++] = str[i];
			}
		}

		buffer[r_idx] = 0;

		return buffer;
	}

	//--------------------------------------
	//
	//--------------------------------------
	//struct str_scanner_t
	//{
	//	const char* ptr;
	//	int nvd; // name value div
	//	int pd;	// pair div
	//	char name[128];
	//	char value[128];
	//};
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API bool str_scanner_init(str_scanner_t* ctx, const char* str, char nvd, char pv)
	{
		if (str == nullptr || *str == 0)
			return false;

		*ctx = { str, nvd, pv, { 0 }, { 0 } };

		return str_scanner_next(ctx);
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API bool str_scanner_next(str_scanner_t* ctx)
	{
		if (ctx == nullptr || ctx->ptr == nullptr || *ctx->ptr == 0)
			return false;

		char pbrk[3] = { (char)ctx->nvd, (char)ctx->pd, 0 };
		const char* cp = strpbrk(ctx->ptr, pbrk);

		if (cp == nullptr)
		{
			strncpy(ctx->name, ctx->ptr, 128);
			ctx->name[127] = 0;
			ctx->ptr = nullptr;
			strtrim(ctx->name, " \t\r\n");
			return true;
		}

		if (*cp == ';')
		{
			// �������� ��� ��������!
			int ncp = (int)MIN(127, cp - ctx->ptr);
			strncpy(ctx->name, ctx->ptr, ncp);
			ctx->name[ncp] = 0;
			strtrim(ctx->name, " \t\r\n");
			ctx->value[0] = 0;
			ctx->ptr = cp + 1;
			return true;
		}

		// cp == '='
		int ncp = (int)MIN(127, cp - ctx->ptr);
		strncpy(ctx->name, ctx->ptr, ncp);
		ctx->name[ncp] = 0;
		strtrim(ctx->name, " \t\r\n");
		ctx->ptr = cp + 1;
		cp = ::strchr(ctx->ptr, ';');

		if (cp == nullptr)
		{
			strncpy(ctx->value, ctx->ptr, 127);
			ctx->value[127] = 0;
			strtrim(ctx->value, " \t\r\n");
			ctx->ptr = cp;
		}
		else
		{
			ncp = (int)MIN(127, cp - ctx->ptr);
			strncpy(ctx->value, ctx->ptr, ncp);
			ctx->value[ncp] = 0;
			strtrim(ctx->value, " \t\r\n");
			ctx->ptr = cp + 1;
		}

		return true;
	}

	//-----------------------------------------------
	//
	//-----------------------------------------------
	static bool backToInt(const char* str, const char* bstr, Number& result);
	static bool backToInt(const wchar_t* str, const wchar_t* bstr, Number& result);
	//-----------------------------------------------
	//
	//-----------------------------------------------
	#define ISDIGIT(ch) (ch >= '0' && ch <= '9')
	#define DIGIT(ch) (ch - '0')
		//-----------------------------------------------
		//
		//-----------------------------------------------
	NumberType StrToNumber(const char* str, const char** end, Number& result)
	{
		// extract integer part
		bool minus = false;
		const char* dec_point = nullptr;
		const char* exp_sign = nullptr;

		// check sign
		if (*str == '-' || *str == '+')
		{
			minus = *str++ == '-';
		}

		char* cp = (char*)str;

		// check legal symbols
		for (;;)
		{
			if (ISDIGIT(*cp))
			{
				cp++;
			}
			else if (*cp == '.' && !dec_point)
			{
				dec_point = cp++;
			}
			else if ((*cp == 'e' || *cp == 'E') && !exp_sign)
			{
				exp_sign = cp++;

				if (*cp == '-' || *cp == '+')
					cp++;
			}
			else
			{
				if (end != nullptr)
					*end = cp;
				if (cp == str || cp - 1 == exp_sign)
					return NumberType::Error;
				cp--;
				break;
			}
		}

		if (!dec_point && !exp_sign)
		{
			if (backToInt(str, cp, result))
			{
				result.integer *= minus ? -1 : 1;
				return NumberType::Integer;
			}
			return NumberType::Error;
		}

		result.real = strtod(str, (char**)end);

		return NumberType::Real;
	}
	//----------------------
	NumberType StrToNumber(const wchar_t* str, wchar_t** end, Number& result)
	{
		// extract integer part
		bool minus = false;
		const wchar_t* dec_point = nullptr;
		const wchar_t* exp_sign = nullptr;

		// check sign
		if (*str == '-' || *str == '+')
		{
			minus = *str++ == '-';
		}

		wchar_t* cp = (wchar_t*)str;

		// check legal symbols
		for (;;)
		{
			if (ISDIGIT(*cp))
			{
				cp++;
			}
			else if (*cp == '.' && !dec_point)
			{
				dec_point = cp++;
			}
			else if ((*cp == 'e' || *cp == 'E') && !exp_sign)
			{
				exp_sign = cp++;

				if (*cp == '-' || *cp == '+')
					cp++;
			}
			else
			{
				if (end != nullptr)
					*end = cp;
				if (cp == str || cp - 1 == exp_sign)
					return NumberType::Error;
				cp--;
				break;
			}
		}

		if (!dec_point && !exp_sign)
		{
			if (backToInt(str, cp, result))
			{
				result.integer *= minus ? -1 : 1;
				return NumberType::Integer;
			}
			return NumberType::Error;
		}

		result.real = wcstod(str, end);
		return NumberType::Real;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	static bool backToInt(const char* str, const char* bstr, Number& result)
	{
		int64_t mult = 1;
		int64_t minmax = 0;

		result.integer = DIGIT(*(bstr));

		if (str == bstr)
		{
			return true;
		}

		for (const char* it = bstr - 1; it >= str; it--)
		{
			result.integer += DIGIT(*it) * (mult * 10);
			mult *= 10;
			if (result.integer > minmax)
				minmax = result.integer;
		}

		return result.integer == minmax;
	}
	//----------------------
	static bool backToInt(const wchar_t* str, const wchar_t* bstr, Number& result)
	{
		int64_t mult = 1;
		int64_t minmax = 0;

		result.integer = DIGIT(*(bstr));

		for (const wchar_t* it = bstr - 1; it >= str; it--)
		{
			result.integer += DIGIT(*it) * (mult * 10);
			mult *= 10;
			if (result.integer > minmax)
				minmax = result.integer;
		}

		return result.integer == minmax;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool StrToInt64(const char* str, char** end, int64_t& int_ref)
	{
		bool sign = *str == '-';

		if (*str == '-' || *str == '+')
			str++;

		const char* int_ptr = str;

		while (ISDIGIT(*int_ptr)) int_ptr++;

		if (int_ptr == str)
		{
			return false;
		}

		int64_t mult = 1;
		int_ref = DIGIT(*(int_ptr - 1));
		int64_t minmax = 0;
		for (const char* it = int_ptr - 2; it >= str; it--)
		{
			int_ref += DIGIT(*it) * (mult * 10);
			mult *= 10;

			if (int_ref > minmax)
				minmax = int_ref;
		}

		if (int_ref != minmax)
			return false;

		if (sign)
			int_ref *= BAD_INDEX;

		return true;
	}
	//----------------------
	bool StrToInt64(const wchar_t* str, wchar_t** end, int64_t& int_ref)
	{
		bool sign = *str == '-';

		if (*str == '-' || *str == '+')
			str++;

		const wchar_t* int_ptr = str;

		while (ISDIGIT(*int_ptr)) int_ptr++;

		if (int_ptr == str)
		{
			return false;
		}

		int64_t mult = 1;
		int_ref = DIGIT(*(int_ptr - 1));
		int64_t minmax = 0;
		for (const wchar_t* it = int_ptr - 2; it >= str; it--)
		{
			int_ref += DIGIT(*it) * (mult * 10);
			mult *= 10;

			if (int_ref > minmax)
				minmax = int_ref;
		}

		if (int_ref != minmax)
			return false;

		if (sign)
			int_ref *= BAD_INDEX;

		return true;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool StrToInt32(const char* str, char** end, int32_t& int_ref)
	{
		bool sign = *str == '-';

		if (*str == '-' || *str == '+')
			str++;

		const char* int_ptr = str;

		while (ISDIGIT(*int_ptr)) int_ptr++;

		if (int_ptr == str)
		{
			return false;
		}

		int32_t mult = 1;
		int_ref = DIGIT(*(int_ptr - 1));
		int32_t minmax = 0;
		for (const char* it = int_ptr - 2; it >= str; it--)
		{
			int_ref += DIGIT(*it) * (mult * 10);
			mult *= 10;

			if (int_ref > minmax)
				minmax = int_ref;
		}

		if (int_ref != minmax)
			return false;

		if (sign)
			int_ref *= BAD_INDEX;

		return true;
	}
	//----------------------
	bool StrToInt32(const wchar_t* str, wchar_t** end, int32_t& int_ref)
	{
		bool sign = *str == '-';

		if (*str == '-' || *str == '+')
			str++;

		const wchar_t* int_ptr = str;

		while (ISDIGIT(*int_ptr)) int_ptr++;

		if (int_ptr == str)
		{
			return false;
		}

		int32_t mult = 1;
		int_ref = DIGIT(*(int_ptr - 1));
		int32_t minmax = 0;
		for (const wchar_t* it = int_ptr - 2; it >= str; it--)
		{
			int_ref += DIGIT(*it) * (mult * 10);
			mult *= 10;

			if (int_ref > minmax)
				minmax = int_ref;
		}

		if (int_ref != minmax)
			return false;

		if (sign)
			int_ref *= BAD_INDEX;

		return true;
	}
}
