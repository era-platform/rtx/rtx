﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_synchro.h"
#include "std_async_call.h"
#include "std_res_counter.h"

namespace rtl
{
	//--------------------------------------
	// asynchronius call manager
	//--------------------------------------
	static async_call_manager_t s_async_caller;
	static bool s_async_call_manager_initialized = false;
	static int s_async_call_counter = -1;
	static int s_async_thread_counter = -1;
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API void async_call_manager_t::createAsyncCall(int initial_thread_count, int max_threads)
	{
		if (!s_async_call_manager_initialized)
		{
			s_async_call_counter = res_counter_t::add("ASYNC_CALL");
			s_async_thread_counter = res_counter_t::add("ASYNC_THREAD");

			s_async_caller.create(initial_thread_count, max_threads);
			s_async_caller.start();
			s_async_call_manager_initialized = true;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API void async_call_manager_t::destroyAsyncCall()
	{
		if (s_async_call_manager_initialized)
		{
			s_async_caller.destroy();
			s_async_call_manager_initialized = false;
		}
	}
	bool async_call_manager_t::callAsync(async_call_target_t* target, uint16_t call_id, uintptr_t int_param, void* ptr_param)
	{
		s_async_caller.call(target, call_id, int_param, ptr_param);
		return true;
	}
	bool async_call_manager_t::callAsync(pfn_async_callback_t callback, void* userdata, uint16_t call_id, uintptr_t int_param, void* ptr_param)
	{
		s_async_caller.call(callback, userdata, call_id, int_param, ptr_param);
		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	async_call_manager_t::async_call_manager_t() :
		m_created(false), m_started(false), m_thread_max(0),
		m_thread_waiting_count(0), m_priority(ThreadPriority::Normal)
	{
	}
	//--------------------------------------
	//
	//--------------------------------------
	async_call_manager_t::~async_call_manager_t()
	{
		destroy();
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API bool async_call_manager_t::create(int initial_thread_count, int max_threads)
	{
		MutexLock lock(m_sync);

		if (!m_created)
		{
			LOG_CALL("async", "create async call manager initial_threads:%d max_thread:%d...", initial_thread_count, max_threads);
			m_call_queue_sem.create(0, INT_MAX);

			m_thread_max = max_threads;

			if (m_thread_max < 0 || m_thread_max > ASYNC_THREADS_MAX)
				m_thread_max = ASYNC_THREADS_MAX;

			if (m_thread_max == 0)
				m_thread_max = ASYNC_THREADS * 4;

			if (initial_thread_count <= 0 || initial_thread_count > m_thread_max)
				initial_thread_count = ASYNC_THREADS;




			for (int i = 0; i < initial_thread_count; i++)
			{
				async_call_thread_t* thread = NEW async_call_thread_t(*this);
				m_thread_pool.add(thread);
			}

			LOG_CALL("async", "async call manager created initial_threads:%d max_threads:%d", m_thread_pool.getCount(), m_thread_max);

			m_created = true;
		}

		return m_created;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API void async_call_manager_t::destroy()
	{
		MutexLock lock(m_sync);

		if (m_created)
		{
			LOG_CALL("async", "destroy async call manager...");

			stop();

			for (int i = 0; i < m_thread_pool.getCount(); i++)
			{
				async_call_thread_t* thread = m_thread_pool[i];
				thread->stop();

				DELETEO(thread);
			}

			m_thread_pool.clear();

			m_call_queue.clear(true);

			m_call_queue_sem.close();

			m_created = false;

			LOG_CALL("async", "async call manager destroyed");
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool async_call_manager_t::start(ThreadPriority priority)
	{
		if (!m_created)
			return false;

		MutexLock lock(m_sync);

		if (!m_started)
		{
			LOG_CALL("async", "starting async call manager t:%d max:%d...", m_thread_pool.getCount(), m_thread_max);

			for (int i = 0; i < m_thread_pool.getCount(); i++)
			{
				async_call_thread_t* thread = m_thread_pool[i];
				thread->start(priority);
			}

			m_started = true;
			LOG_CALL("async", "async call manager started");
		}

		return m_started;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void async_call_manager_t::stop()
	{
		if (!m_created)
			return;

		if (m_started)
		{
			LOG_CALL("async", "Stopping async call manager : thread-count:%d wait-count:%d call-queue:%d...", m_thread_pool.getCount(), m_thread_waiting_count, m_call_queue.getCount());

			m_started = false;

			{
				MutexLock lock(m_sync);
				for (int i = 0; i < m_thread_pool.getCount(); i++)
				{
					async_call_info_t call;
					memset(&call, 0, sizeof call);
					m_call_queue.push(call);
					m_call_queue_sem.signal();
				}
			}

			for (int i = 0; i < m_thread_pool.getCount(); i++)
			{
				async_call_thread_t* thread = m_thread_pool[i];
				thread->stop();
			}

			{
				MutexLock lock(m_sync);
				m_call_queue.clear();
			}

			LOG_CALL("async", "async call manager stopped : call_queue:%d", m_call_queue.getCount());
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void  async_call_manager_t::check_thread_count()
	{
		// check thread count and add new if needed and can
		if (m_thread_pool.getCount() < m_thread_max && m_thread_waiting_count == 0 && m_call_queue.getCount() > ASYNC_CALL_QUEUE_RED_LINE)
		{
			LOG_CALL("async", "Async call manager : add New thread thread-count:%d wait-count:%d call-queue:%d...", m_thread_pool.getCount(), m_thread_waiting_count, m_call_queue.getCount());

			async_call_thread_t* thread = NEW async_call_thread_t(*this);
			m_thread_pool.add(thread);

			thread->start(m_priority);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool async_call_manager_t::call(async_call_target_t* target, uint16_t call_id, uintptr_t int_param, void* ptr_param)
	{
		if (!m_created || !m_started)
		{
			LOG_ERROR("async", "async call canceled! --> call manager not started!");
			return false;
		}

		if (target == nullptr)
		{
			LOG_ERROR("async", "async call canceled! --> null target cannot be called!");
			return false;
		}

		async_call_info_t call = { { target }, nullptr, { 0, call_id }, int_param, ptr_param };

		MutexLock lock(m_sync);

		LOG_ASYNC("async", "async call pushed to queue(size:%d) %s(%p, %u, %u, %p)", m_call_queue.getCount(), target->async_get_name(), target, call_id, int_param, ptr_param);

		res_counter_t::add_ref(s_async_call_counter);
		m_call_queue.push(call);

		m_call_queue_sem.signal();

		// add threads if queue size grows more than red line
		check_thread_count();

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool async_call_manager_t::call(pfn_async_callback_t callback, void* userdata, uint16_t call_id, uintptr_t int_param, void* ptr_param)
	{
		if (!m_created || !m_started)
		{
			LOG_ERROR("async", "async call canceled! --> call manager not started!");
			return false;
		}

		if (callback == nullptr)
		{
			LOG_ERROR("async", "async call canceled! --> null callback cannot be called!");
			return false;
		}

		async_call_info_t call = { { nullptr }, userdata, { 1, call_id}, int_param, ptr_param };
		call.callback = callback;

		MutexLock lock(m_sync);

		LOG_ASYNC("async", "async call pushed to queue(size:%d) CALLBACK(%p, %u, %u, %p)", m_call_queue.getCount(), userdata, call_id, int_param, ptr_param);

		res_counter_t::add_ref(s_async_call_counter);
		m_call_queue.push(call);

		m_call_queue_sem.signal();

		// add threads if queue size grows more than red line
		check_thread_count();

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API void async_call_manager_t::get_statistics(async_call_statistics_t& stat)
	{
		stat.all_thread_count = m_thread_pool.getCount();
		stat.queue_size = m_call_queue.getCount();
		stat.waiting_thread_count = m_thread_waiting_count;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool async_call_manager_t::get_call(async_call_info_t& call_info)
	{
		bool result = false;

		std_interlocked_inc(&m_thread_waiting_count);

		m_call_queue_sem.wait(INFINITE);

		std_interlocked_dec(&m_thread_waiting_count);

		if (!m_started)
		{
			return false;
		}

		{
			MutexLock lock(m_sync);

			call_info = m_call_queue.pop();
			res_counter_t::release(s_async_call_counter);
			if (call_info.target != nullptr)
			{
				result = true;
			}
		}

		return result;
	}
	//--------------------------------------
	//
	//--------------------------------------
	async_call_thread_t::async_call_thread_t(async_call_manager_t& owner) : m_owner(owner)
	{
		res_counter_t::add_ref(s_async_thread_counter);
	}
	//--------------------------------------
	//
	//--------------------------------------
	async_call_thread_t::~async_call_thread_t()
	{
		stop();

		res_counter_t::release(s_async_thread_counter);
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool async_call_thread_t::start(ThreadPriority priority)
	{
		if (m_thread.isStarted())
			return true;

		LOG_ASYNC("async", "async New thread : starting...");
		m_thread.set_priority(priority);
		if (!m_thread.start(this))
		{
			LOG_ERROR("async", "async New thread : start thread failed");
			return false;
		}

		LOG_ASYNC("async", "async thread %u : started", m_thread.get_id());

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void async_call_thread_t::stop()
	{
		if (m_thread.isStarted())
		{
			LOG_ASYNC("async", "async thread %u : stopping...", m_thread.get_id());

			m_thread.stop();

			m_thread.wait(INFINITE);

			LOG_ASYNC("async", "async thread : stopped");
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void async_call_thread_t::thread_run(Thread* thread)
	{
		async_call_info_t call;

		LOG_ASYNC("async", "async thread : started");

		while (!thread->get_stopping_flag())
		{
			if (m_owner.get_call(call))
			{
				if (call.target == nullptr)
				{
					// ?
					LOG_ASYNC("async", "async thread : stop processing - empty call");
					break;
				}

				if (call.call_id[0] == 1 && call.callback != nullptr)
				{
					LOG_ASYNC("async", "async call for CALLBACK(%p, %u, %u, %p)...", call.userdata, call.call_id[1], call.int_param, call.ptr_param);
					call.callback(call.userdata, call.call_id[1], call.int_param, call.ptr_param);
					LOG_ASYNC("async", "async call returns");
				}
				else if (call.call_id[0] == 0 && call.target != nullptr)
				{
					LOG_ASYNC("async", "async call for %s(%p, %u, %u, %u)...", call.target->async_get_name(), call.target, call.call_id[1], call.int_param, call.ptr_param);
					call.target->async_handle_call(call.call_id[1], call.int_param, call.ptr_param);
					LOG_ASYNC("async", "async call returns");
				}
			}
			else
			{
				LOG_ASYNC("async", "async thread : stop processing - stop signal read");

				break;
			}
		}

		LOG_ASYNC("async", "async thread : exit");
	}
	//--------------------------------------
	//
	//--------------------------------------
	MessageQueue::MessageQueue() : m_started(false), m_priority(ThreadPriority::Normal)
	{
		m_queueSemaphore.create(0, INT_MAX);
		m_startEvent.create(true, false);
	}
	//--------------------------------------
	//
	//--------------------------------------
	MessageQueue::~MessageQueue()
	{

	}
	//--------------------------------------
	//
	//--------------------------------------
	bool MessageQueue::start(ThreadPriority priority)
	{
		if (m_handlingThread.isStarted())
			return true;

		MutexLock lock(m_sync);

		LOG_MQ("MQ", "Starting message queue...");

		m_handlingThread.set_priority(priority);

		if (m_handlingThread.start(this))
		{
			m_startEvent.wait();
		}

		return m_started;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void MessageQueue::stop()
	{
		if (m_started)
		{
			m_started = false;

			{
				MutexLock lock(m_sync);
				async_call_info_t call;
				memset(&call, 0, sizeof call);
				m_queue.push(call);
				m_queueSemaphore.signal();
			}

			m_handlingThread.stop();

			{
				MutexLock lock(m_sync);
				m_queue.clear();
			}

			LOG_MQ("MQ", "Message queue stopped");
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool MessageQueue::push(pfnQueueHandlerCallback callback, void* userData, uint16_t messageId, uintptr_t intParam, void* ptrParam)
	{
		if (!m_started)
		{
			LOG_ERROR("MQ", "Not started!");
			return false;
		}

		if (callback == nullptr)
		{
			LOG_ERROR("MQ", "Invalid callback parameter!");
			return false;
		}

		async_call_info_t call = { { nullptr }, userData, { 1, messageId}, intParam, ptrParam };
		call.callback = callback;

		MutexLock lock(m_sync);

		LOG_MQ("MQ", "Message pushed to queue(size:%d) CALLBACK(%p, %u, %u, %p)", m_queue.getCount(), userData, messageId, intParam, ptrParam);

		res_counter_t::add_ref(s_async_call_counter);
		m_queue.push(call);

		m_queueSemaphore.signal();

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool MessageQueue::getCall(async_call_info_t& msg)
	{
		m_queueSemaphore.wait(INFINITE);

		if (!m_started)
		{
			return false;
		}

		{
			MutexLock lock(m_sync);

			msg = m_queue.pop();
		}

		return msg.target != nullptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void MessageQueue::thread_run(Thread* thread)
	{
		async_call_info_t call;

		LOG_MQ("MQ", "Queue thread : started");

		m_started = true;

		m_startEvent.signal();

		while (!thread->get_stopping_flag())
		{
			if (getCall(call))
			{
				if (call.call_id[0] == 1 && call.callback != nullptr)
				{
					LOG_MQ("MQ", "Message to CALLBACK(%p, %u, %u, %p)...", call.userdata, call.call_id[1], call.int_param, call.ptr_param);
					call.callback(call.userdata, call.call_id[1], call.int_param, call.ptr_param);
					LOG_MQ("MQ", "callback returns");
				}
				else if (call.call_id[0] == 0 && call.target != nullptr)
				{
					LOG_MQ("MQ", "Message for %s(%p, %u, %u, %u)...", call.target->async_get_name(), call.target, call.call_id[1], call.int_param, call.ptr_param);
					call.target->async_handle_call(call.call_id[1], call.int_param, call.ptr_param);
					LOG_MQ("MQ", "handler returns");
				}
			}
			else
			{
				LOG_MQ("MQ", "Queue thread : stop processing - stop signal read");

				break;
			}
		}

		LOG_MQ("MQ", "Queue thread : exit");
	}
}
//--------------------------------------
