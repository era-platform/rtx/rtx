﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_config.h"
//--------------------------------------
//
//--------------------------------------
RTX_STD_API rtl::std_config_t Config;

namespace rtl
{
	//--------------------------------------
	//
	//--------------------------------------
	inline static const char* skip_space(const char* text)
	{
		while (*text != 0 && (*text == ' ' || *text == '\t')) text++;

		return text;
	}
	//--------------------------------------
	//
	//--------------------------------------
	inline static const char* skip_back_spaces(char* text, int len)
	{
		for (int i = len - 1; i >= 0; i--)
		{
			char ch = text[i];
			if (ch != ' ' && ch != '\t' && ch != '\n' && ch != '\r')
				break;
			else
				text[i] = 0;
		}

		return text;
	}
	//--------------------------------------
	//
	//--------------------------------------
	std_config_t::std_config_t() : m_param_list(compare_param, compare_key)
	{
	}
	//--------------------------------------
	//
	//--------------------------------------
	std_config_t::~std_config_t()
	{
		close();
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool std_config_t::read(const char* config_file)
	{
		/*
		формат конфигурационного файла
		коментарий = ';' текст CRLF
		значение = key '=' value CRLF
		key, value = буквы без учета регистра цифры и любые печатные символы за сключением символов ";="
		*/

		MutexLock lock(m_sync);

		close();

		char line[1024];
		FILE* file = fopen(config_file, "r");

		if (file == nullptr)
		{
			printf("config file opening error -- %s\n", config_file);
			return false;
		}

		bool first_read = true;

		while (fgets(line, 1024, file) != nullptr)
		{
			char* line_ptr = line;
			if (first_read)
			{
				uint32_t sign = *(uint32_t*)line;
				sign &= 0x00FFFFFF;

				if (sign == 0x00BFBBEF)
				{
					// utf8 sign!
					line_ptr += 3;
				}
				first_read = false;
			}
			skip_back_spaces(line_ptr, std_strlen(line_ptr));
			const char* cp = skip_space(line_ptr);

			// skip comments and empty lines
			if (*cp == 0 || *cp == ';' || *cp == '\r' || *cp == '\n')
				continue;

			add_param(cp);
		}

		fclose(file);

		return m_param_list.getCount() > 0;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void std_config_t::close()
	{
		MutexLock lock(m_sync);

		for (int i = 0; i < m_param_list.getCount(); i++)
		{
			config_param_t& item = m_param_list.getAt(i);

			FREE(item.name);
			if (item.value != nullptr)
			{
				FREE(item.value);
			}
		}

		m_param_list.clear();
	}
	//--------------------------------------
	//
	//--------------------------------------
	const char* std_config_t::get_config_value(const char* key, bool* result)
	{
		MutexLock lock(m_sync);

		config_param_t* param = m_param_list.find(key);

		if (param != nullptr)
		{
			if (result != nullptr)
			{
				*result = true;
			}

			return param->value != nullptr ? param->value : "";
		}

		if (result != nullptr)
		{
			*result = false;
		}

		return nullptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool std_config_t::set_config_value(const char* key, const char* value)
	{
		MutexLock lock(m_sync);

		config_param_t* param = m_param_list.find(key);

		if (param != nullptr)
		{
			update_param(param, value);
		}

		add_new_param(key, value);

		return nullptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void std_config_t::add_param(const char* line)
	{
		const char* comment = ::strchr(line, ';');
		int line_length = comment != nullptr ? PTR_DIFF(comment, line) : std_strlen(line);

		const char* eq = strchr(line, '=', line_length);


		config_param_t param = { nullptr, nullptr };

		if (eq != nullptr)
		{
			int len = PTR_DIFF(eq, line);

			param.name = (char*)MALLOC(len + 1);
			strncpy(param.name, line, len);
			param.name[len] = 0;
			skip_back_spaces(param.name, len);

			const char* value = skip_space(eq + 1);
			len = line_length - PTR_DIFF(value, line);
			param.value = (char*)MALLOC(len + 1);
			strncpy(param.value, value, len);
			param.value[len] = 0;
			skip_back_spaces(param.value, len);
		}
		else
		{
			int len = line_length;
			param.name = (char*)MALLOC(len + 1);
			strncpy(param.name, line, len);
			param.name[len] = 0;
			skip_back_spaces(param.name, len);
		}

		m_param_list.add(param);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void std_config_t::add_new_param(const char* key, const char* value)
	{
		config_param_t param = { nullptr, nullptr };

		if (key != nullptr && key[0] != 0)
		{
			int len = (int)strlen(key);
			param.name = (char*)MALLOC(len + 1);
			strcpy(param.name, key);
			skip_back_spaces(param.name, len);

			if (value != nullptr && value[0] != 0)
			{
				len = (int)strlen(value);
				param.value = (char*)MALLOC(len + 1);
				strcpy(param.value, value);
				skip_back_spaces(param.value, len);
			}

			m_param_list.add(param);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void std_config_t::update_param(config_param_t* param, const char* new_value)
	{
		if (param->value != nullptr)
		{
			FREE(param->value);
			param->value = nullptr;
		}

		if (new_value != nullptr && new_value[0] != 0)
		{
			int len = (int)strlen(new_value);
			param->value = (char*)MALLOC(len + 1);
			strcpy(param->value, new_value);
			skip_back_spaces(param->value, len);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API bool std_config_t::print(FILE* out)
	{
		MutexLock lock(m_sync);

		for (int i = 0; i < m_param_list.getCount(); i++)
		{
			config_param_t& param = m_param_list[i];
			fprintf(out, "%s=%s\r\n", param.name, param.value);
		}

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int std_config_t::compare_param(const config_param_t& left, const config_param_t& right)
	{
		return std_stricmp(left.name, right.name);
	}
	//--------------------------------------
	//
	//--------------------------------------
	int std_config_t::compare_key(const ZString& key, const config_param_t& item)
	{
		return std_stricmp(key, item.name);
	}
}
//--------------------------------------
