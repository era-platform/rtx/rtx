/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_json.h"

namespace rtl {
	//-----------------------------------------------
	//
	//-----------------------------------------------
#define SYM_OBJECT_BEGIN '{'
#define SYM_OBJECT_END '}'
#define SYM_ARRAY_BEGIN '[' 
#define SYM_ARRAY_END ']' 
#define SYM_NAME_SEPARATOR ':'
#define SYM_NEXT ','
#define SYM_QUOTE '\"'
#define SYM_ZERO '\0'
//-----------------------------------------------
//
//-----------------------------------------------
	RTX_STD_API JsonParser::JsonParser() :
		m_json(nullptr), m_readPtr(nullptr), m_state(JsonState::Initial),
		m_fieldType(JsonTypes::Null), m_b_value(false), m_i_value(0), m_f_value(0.0)
	{
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	RTX_STD_API JsonParser::~JsonParser()
	{
		close();
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	RTX_STD_API bool JsonParser::start(const char* text)
	{
		close();

		m_json = m_readPtr = text;

		return text != nullptr;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	RTX_STD_API void JsonParser::close()
	{
		m_json = m_readPtr = nullptr;
		m_state = JsonState::Initial;
		m_fieldName.setEmpty();
		m_fieldType = JsonTypes::Null;
		m_b_value = false;
		m_i_value = 0;
		m_f_value = 0.0;
		m_s_value.setEmpty();
		m_stateStack.clear();
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	RTX_STD_API JsonState JsonParser::read()
	{
		switch (m_state)
		{
		case JsonState::Object:
			return readMember();
		case JsonState::Array:
			return readArrayValue();
		case JsonState::ObjectEnd:
		case JsonState::ArrayEnd:
			m_state = m_stateStack.pop();
			break;
		case JsonState::Error:
		case JsonState::Eof:
			return m_state;
		case JsonState::ArrayValue:
		case JsonState::Member:
			if (m_fieldType == JsonTypes::Object)
			{
				m_stateStack.push(m_state);
				return m_state = JsonState::Object;
			}

			if (m_fieldType == JsonTypes::Array)
			{
				m_stateStack.push(m_state);
				return m_state = JsonState::Array;
			}
			// ���� �������� ������ ���� ��� ������ ��� ������
			break;
		}

		if ((m_readPtr = skip_LWSP(m_readPtr)) == nullptr)
		{
			return m_state = JsonState::Eof;
		}

		char ch = *m_readPtr;
		m_readPtr++;

		switch (ch)
		{
		case SYM_OBJECT_BEGIN:
			m_stateStack.push(m_state);
			return m_state = JsonState::Object;
		case SYM_OBJECT_END:
			return m_state = JsonState::ObjectEnd;
		case SYM_ARRAY_BEGIN:
			m_stateStack.push(m_state);
			return m_state = JsonState::Array;
		case SYM_ARRAY_END:
			return m_state = JsonState::ArrayEnd;
		case SYM_NEXT:
			return m_state == JsonState::Member ? readMember() :
				m_state == JsonState::ArrayValue ? readArrayValue() :
				JsonState::Error;
		}

		return m_state = (m_state == JsonState::Initial ? JsonState::Eof : JsonState::Error);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	String JsonParser::getValueAsString()
	{
		if (m_state == JsonState::Member)
		{
			switch (m_fieldType)
			{
			case JsonTypes::String:
				return m_s_value;
			case JsonTypes::Boolean:
				return STR_BOOL(m_b_value);
			case JsonTypes::Integer:
				return int_to_string(m_i_value);
			case JsonTypes::Float:
				return float_to_string(m_f_value);
			}

			return "object";
		}

		return "null";
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	JsonState JsonParser::readMember()
	{
		m_state = JsonState::Member;

		/* name : value */
		const char* cp = skip_LWSP(m_readPtr);

		if (cp == nullptr || *cp != SYM_QUOTE)
		{
			return m_state = JsonState::Error;
		}

		m_readPtr = ++cp;
		const char* cpEnd = ::strchr(m_readPtr, SYM_QUOTE);

		if (cpEnd == nullptr)
		{
			return m_state = JsonState::Error;
		}

		// we have the fieldName name
		m_fieldName.assign(cp, (int)(cpEnd - m_readPtr));

		m_readPtr = skip_LWSP(cpEnd + 1);

		if (m_readPtr == nullptr || *m_readPtr != SYM_NAME_SEPARATOR)
		{
			return m_state = JsonState::Error;
		}

		// we have the member an value
		m_readPtr++;

		return readValue();
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	JsonState JsonParser::readArrayValue()
	{
		m_state = JsonState::ArrayValue;

		return readValue();
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	JsonState JsonParser::readValue()
	{
		/* [:, value ,}] */

		m_s_value.setEmpty();
		m_i_value = 0;
		m_f_value = .0;
		m_b_value = false;

		bool result = false;
		m_readPtr = skip_LWSP(m_readPtr);

		if (*m_readPtr == SYM_ZERO)
		{
			return m_state = JsonState::Error;
		}

		// object
		if (*m_readPtr == SYM_OBJECT_BEGIN)
		{
			m_fieldType = JsonTypes::Object;
			m_readPtr++;
			return m_state;
		}

		// array
		if (*m_readPtr == SYM_ARRAY_BEGIN)
		{
			m_fieldType = JsonTypes::Array;
			m_readPtr++;
			return m_state;
		}

		// string
		if (*m_readPtr == SYM_QUOTE)
		{
			return readStringValue();
		}

		// number
		if (*m_readPtr == '.' || *m_readPtr == '-' || isdigit(*m_readPtr))
		{
			return readNumberValue();
		}

		if (::strncmp(m_readPtr, "true", 4) == 0)
		{
			m_fieldType = JsonTypes::Boolean;
			m_b_value = true;
			m_readPtr += 4;
		}
		else if (::strncmp(m_readPtr, "false", 5) == 0)
		{
			m_fieldType = JsonTypes::Boolean;
			m_b_value = false;
			m_readPtr += 5;
		}
		else if (::strncmp(m_readPtr, "null", 4) == 0)
		{
			m_fieldType = JsonTypes::Null;
			m_readPtr += 4;
		}
		else
		{
			m_state = JsonState::Error;
		}

		return m_state;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	JsonState JsonParser::readStringValue()
	{
		const char* value = ++m_readPtr;
		const char* end = ::strchr(value, '\"');

		if (end == nullptr)
		{
			return JsonState::Error;
		}

		m_s_value.assign(value, int(end - value));
		m_fieldType = JsonTypes::String;
		m_readPtr = end + 1;

		return m_state;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	JsonState JsonParser::readNumberValue()
	{
		Number num;
		const char** end = &m_readPtr;

		NumberType result = StrToNumber(m_readPtr, end, num);

		switch (result)
		{
		case NumberType::Integer:
			m_fieldType = JsonTypes::Integer;
			m_i_value = num.integer;
			m_readPtr = *end;
			break;
		case NumberType::Real:
			m_fieldType = JsonTypes::Float;
			m_f_value = num.real;
			m_readPtr = *end;
			break;
		case NumberType::Error:
			m_state = JsonState::Error;
			break;
		}

		return m_state;
	}
}
//-----------------------------------------------
