﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "stdafx.h"
#include "std_mutex.h"
#include "std_lock_watcher.h"

namespace rtl
{
	//--------------------------------------
	//
	//--------------------------------------
	static bool s_watcher_enabled = false;
	//--------------------------------------
	//
	//--------------------------------------
	#if defined(TARGET_OS_WINDOWS)
	//--------------------------------------
	//
	//--------------------------------------
	MutexWatch::MutexWatch(const char* name) : m_id(0)
	{
	#pragma warning(suppress: 28125)
		InitializeCriticalSection(&m_cs);
		m_id = s_watcher_enabled ? Watch_RegisterLock(name) : 0;
	}
	//--------------------------------------
	//
	//--------------------------------------
	MutexWatch::~MutexWatch()
	{
		DeleteCriticalSection(&m_cs);

		if (m_id != 0)
			Watch_UnregisterLock(m_id);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void MutexWatch::wait(const char* proc) const
	{
		if (m_id != 0)
			Watch_Locking(m_id, proc);

		EnterCriticalSection(&m_cs);

		if (m_id != 0)
			Watch_Locked(m_id, proc);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void MutexWatch::signal() const
	{
		LeaveCriticalSection(&m_cs);

		if (m_id != 0)
			Watch_Unlock(m_id);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void MutexWatch::set_name(const char* name)
	{
		if (m_id != 0)
			Watch_SetLockName(m_id, name);
	}
	//--------------------------------------
	//
	//--------------------------------------
	#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	Mutex::Mutex()
	{
		pthread_mutexattr_t attr;
		int res = pthread_mutexattr_init(&attr);
		if (res != 0)
		{
			LOG_ERROR("mutex", "mutex.mutex -- pthread_mutexattr_init() failed. error = %d.", res);
		}

		res = pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
		if (res != 0)
		{
			LOG_ERROR("mutex", "mutex.mutex -- pthread_mutexattr_settype() failed. error = %d.", res);
		}

		res = pthread_mutex_init(&m_mutex, &attr);
		if (res != 0)
		{
			LOG_ERROR("mutex", "mutex.mutex -- pthread_mutex_init() failed. error = %d.", res);
		}

		res = pthread_mutexattr_destroy(&attr);
		if (res != 0)
		{
			LOG_ERROR("mutex", "mutex.mutex -- pthread_mutexattr_destroy() failed. error = %d.", res);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	Mutex::~Mutex()
	{
		int res = pthread_mutex_destroy(&m_mutex);
		if (res != 0)
		{
			LOG_ERROR("mutex", "mutex.~mutex -- pthread_mutex_destroy() failed. error = %d.", res);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void Mutex::wait(uint32_t timeout) const
	{
		int res = pthread_mutex_lock(&m_mutex);
		if (res != 0)
		{
			LOG_ERROR("mutex", "mutex.lock -- pthread_mutex_lock() failed. error = %d.", res);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	//bool Mutex::try_lock()
	//{
	//	int res = pthread_mutex_trylock(&m_mutex);
	//	if (res == 0)
	//		return true;
	//	if (res == EBUSY)
	//		return false;
	//
	//	G_PLOG_ERROR("mutex.try_lock -- pthread_mutex_trylock() failed. error = %d.", res);
	//
	//	return false;
	//}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void Mutex::signal() const
	{
		int res = pthread_mutex_unlock(&m_mutex);
		if (res != 0)
		{
			LOG_ERROR("mutex", "mutex.unlock -- pthread_mutex_unlock() failed. error = %d.", res);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	MutexWatch::MutexWatch(const char* name)
	{
		pthread_mutexattr_t attr;
		int res = pthread_mutexattr_init(&attr);
		if (res != 0)
		{
			LOG_ERROR("mutex", "mutex.mutex -- pthread_mutexattr_init() failed. error = %d.", res);
		}

		res = pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
		if (res != 0)
		{
			LOG_ERROR("mutex", "mutex.mutex -- pthread_mutexattr_settype() failed. error = %d.", res);
		}

		res = pthread_mutex_init(&m_mutex, &attr);
		if (res != 0)
		{
			LOG_ERROR("mutex", "mutex.mutex -- pthread_mutex_init() failed. error = %d.", res);
		}

		res = pthread_mutexattr_destroy(&attr);
		if (res != 0)
		{
			LOG_ERROR("mutex", "mutex.mutex -- pthread_mutexattr_destroy() failed. error = %d.", res);
		}

		m_id = s_watcher_enabled ? Watch_RegisterLock(name) : 0;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	MutexWatch::~MutexWatch()
	{
		int res = pthread_mutex_destroy(&m_mutex);
		if (res != 0)
		{
			LOG_ERROR("mutex", "mutex.~mutex -- pthread_mutex_destroy() failed. error = %d.", res);
		}

		if (m_id != 0)
			Watch_UnregisterLock(m_id);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void MutexWatch::wait(const char* proc) const
	{
		if (m_id != 0)
			Watch_Locking(m_id, proc);
	
		int res = pthread_mutex_lock(&m_mutex);
		if (res != 0)
		{
			LOG_ERROR("mutex", "mutex.lock -- pthread_mutex_lock() failed. error = %d.", res);
			if (m_id != 0)
				Watch_Cancel(m_id, proc);
		}
		else if (m_id != 0)
			Watch_Locked(m_id, proc);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	//bool MutexWatch::try_lock()
	//{
	//	int res = pthread_mutex_trylock(&m_mutex);
	//	if (res == 0)
	//		return true;
	//	if (res == EBUSY)
	//		return false;
	//
	//	G_PLOG_ERROR("mutex.try_lock -- pthread_mutex_trylock() failed. error = %d.", res);
	//
	//	return false;
	//}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void MutexWatch::signal() const
	{
		int res = pthread_mutex_unlock(&m_mutex);
		if (res != 0)
		{
			LOG_ERROR("mutex", "mutex.unlock -- pthread_mutex_unlock() failed. error = %d.", res);
		}
	
		if (m_id != 0)
			Watch_Unlock(m_id);
	}
	#endif

	//--------------------------------------
	//
	//--------------------------------------
	void MutexWatch::setup_watcher(bool watcherEnable, int timerPeriod)
	{
		s_watcher_enabled = watcherEnable;

		if (watcherEnable)
			Watch_Start(timerPeriod);
		else
			Watch_Stop();
	}
}
//--------------------------------------
