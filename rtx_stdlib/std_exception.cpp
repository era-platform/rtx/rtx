﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_exception.h"
#include "std_seh_winnt.h"

namespace rtl
{
	//--------------------------------------
	//
	//--------------------------------------
	rtx_exception_callback_t Exception::g_callback = nullptr;
	//--------------------------------------
	//
	//--------------------------------------
	Exception::Exception(const char* text, uint32_t code, Exception* inner) : m_code(code), m_text(nullptr), m_inner(inner)
	{
		size_t innerLen = 0;
		const char* innerText = nullptr;

		if (inner != nullptr)
		{
			innerText = inner->get_message();
			innerLen = (int)strlen(innerText);
		}

		if (text != nullptr && text[0] != 0)
		{
			size_t len = strlen(text) + 128 + innerLen;

			m_text = (char*)malloc(len);

			if (innerText != nullptr)
				sprintf(m_text, "Exception '%s' code %d. Exception trace:\n%s.", text, code, innerText);
			else
				sprintf(m_text, "Exception '%s' code %d.", text, code);
			return;
		}

		m_text = (char*)malloc(128 + innerLen);

		if (!m_text)
		{
			if (innerText != nullptr)
				sprintf(m_text, "Exception code %d. Exception trace: %s.", code, innerText);
			else
				sprintf(m_text, "Exception code %d.", code);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	Exception::~Exception()
	{
		if (m_text != nullptr)
		{
			free(m_text);
			m_text = nullptr;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Exception::raise_notify(const char* module, const char* catcher, void* user_data)
	{
		if (g_callback != nullptr)
			g_callback(module, catcher, user_data, m_text);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Exception::set_callback(rtx_exception_callback_t callback)
	{
		g_callback = callback;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Exception::kill()
	{

#if defined (TARGET_OS_WINDOWS)
		PROCESSENTRY32 entry;
		entry.dwSize = sizeof(PROCESSENTRY32);

		HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

		if (Process32First(snapshot, &entry) == TRUE)
		{
			while (Process32Next(snapshot, &entry) == TRUE)
			{
				if (std_stricmp(entry.szExeFile, "rtx_mg3.exe") == 0)
				{
					HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, entry.th32ProcessID);
					if (hProcess != NULL)
					{
						TerminateProcess(hProcess, 9);
						CloseHandle(hProcess);
					}
				}
			}
		}

		CloseHandle(snapshot);

#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)

#endif

	}
#if defined (TARGET_OS_WINDOWS)
	//--------------------------------------
	//
	//--------------------------------------
	_purecall_handler pure_call_exception_t::s_old_handler = nullptr;
	//--------------------------------------
	//
	//--------------------------------------
	pure_call_exception_t::pure_call_exception_t() : Exception("Pure virtual call detected")
	{
	}
#pragma warning(push)
#pragma warning(disable : 4722)

	pure_call_exception_t::~pure_call_exception_t()
	{
		Log.log_error("EXCEPT", "pure_call_exception_t : DESTRUCTOR ENDS PROCESS!!!");
		exit(-1);
	}
#pragma warning(pop) 
	//--------------------------------------
	//
	//--------------------------------------
	void pure_call_exception_t::start_handling()
	{
		if (s_old_handler == nullptr)
			s_old_handler = _set_purecall_handler(pure_call_exception_t::translate);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void pure_call_exception_t::stop_handling()
	{
		_set_purecall_handler(s_old_handler);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void pure_call_exception_t::translate()
	{
		Log.log_error("EXCEPT", "!!! PURE VIRTUAL CALL DETECTED !!!");
		throw pure_call_exception_t();
	}
	//--------------------------------------
	//
	//--------------------------------------
	Mutex SEH_Exception::s_lock;
	//--------------------------------------
	//
	//--------------------------------------
#define EXCEPTION_TEXT_LEN 8192
	SEH_Exception::SEH_Exception(EXCEPTION_POINTERS* pointer) : Exception(nullptr, EX_SEH)
	{
		MutexLock lock(s_lock);

		construct(pointer);
	}
	void SEH_Exception::construct(EXCEPTION_POINTERS* pointer)
	{
		__try
		{
			// копируем информацию
			PEXCEPTION_RECORD exception = pointer->ExceptionRecord;
			PCONTEXT pContext = pointer->ContextRecord;

			// буфер под текст
			if (m_text != nullptr)
			{
				free(m_text);
			}

			m_text = (char*)malloc(EXCEPTION_TEXT_LEN);


			//ERR_ERROR(L"SEH", L"...catched... see to logs...");

			// версия ос
			OSVERSIONINFOEXW info;
			memset(&info, 0, sizeof info);
			info.dwOSVersionInfoSize = sizeof info;
			GetVersionExW((OSVERSIONINFOW*)&info);

			int len = _snprintf(m_text, EXCEPTION_TEXT_LEN, "SEH Exception\n\t\t");
			if (len >= EXCEPTION_TEXT_LEN)
				__leave;//пїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅ try

			len += GetFaultReason(pointer, m_text + len, EXCEPTION_TEXT_LEN - len);
			if (len >= EXCEPTION_TEXT_LEN)
				__leave;

			len += _snprintf(m_text + len, EXCEPTION_TEXT_LEN - len, "\n\t\t");
			if (len >= EXCEPTION_TEXT_LEN)
				__leave;

			uint32_t temp = 0;
			temp = GetFirstStackTraceString(/*GSTSO_PARAMS |*/ GSTSO_MODULE | GSTSO_SYMBOL | GSTSO_SRCLINE, pointer, m_text + len, EXCEPTION_TEXT_LEN - len);
			if (temp < 1)
				__leave;
			temp += _snprintf(m_text + (len + temp), EXCEPTION_TEXT_LEN - (len + temp), "\n\t\t");

			while (temp > 0)
			{
				len += temp;
				if (len >= EXCEPTION_TEXT_LEN)
					break;
				temp = GetNextStackTraceString(/*GSTSO_PARAMS |*/ GSTSO_MODULE | GSTSO_SYMBOL | GSTSO_SRCLINE, pointer, m_text + len, EXCEPTION_TEXT_LEN - len);
				if (temp > 0)
					temp += _snprintf(m_text + (len + temp), EXCEPTION_TEXT_LEN - (len + temp), "\n\t\t");
			}
			if (len >= EXCEPTION_TEXT_LEN)
				__leave;
#if defined (TARGET_ARCH_X64)
			len += _snprintf(m_text + len, EXCEPTION_TEXT_LEN - len, "Windows Version : %d.%d.%d SP: %d.%d\n\
		App Version     : %d.%d.%d.%d\n\
		ContextFlags    : 0x%08X\n\
		ExceptionCode   : 0x%08X\n\
		CONTEXT_DEBUG_REGISTERS:\n\
		Dr0   : 0x%016I64X    Dr1   : 0x%016I64X    Dr2   : 0x%016I64X\n\
		Dr3   : 0x%016I64X    Dr6   : 0x%016I64X    Dr7   : 0x%016I64X\n\
		CONTEXT_SEGMENTS:\n\
		SegGs : 0x%08X    SegFs : 0x%08X\n\
		SegEs : 0x%08X    SegDs : 0x%08X\n\
		SegCS : 0x%08X    SegSS : 0x%08X\n\
		CONTEXT_INTEGER\n\
		RDI   : 0x%016I64X    RSI   : 0x%016I64X    RBX   : 0x%016I64X\n\
		RDX   : 0x%016I64X    RCX   : 0x%016I64X    RAX   : 0x%016I64X\n\
		RBP   : 0x%016I64X    RSP   : 0x%016I64X    RIP   : 0x%016I64X\n\
		CONTEXT_CONTROL\n\
		EFlags: 0x%08X\n\n",
				info.dwMajorVersion, info.dwMinorVersion, info.dwBuildNumber, info.wServicePackMajor, info.wServicePackMinor,
				API_VERSION_MAJOR, API_VERSION_MINOR, API_VERSION_TEST, API_VERSION_BUILD,
				pContext->ContextFlags, exception->ExceptionCode, pContext->Dr0, pContext->Dr1, pContext->Dr2, pContext->Dr3, pContext->Dr6, pContext->Dr7,
				pContext->SegGs, pContext->SegFs, pContext->SegEs, pContext->SegDs, pContext->SegCs, pContext->SegSs,
				pContext->Rdi, pContext->Rsi, pContext->Rbx, pContext->Rdx, pContext->Rcx,
				pContext->Rax, pContext->Rbp, pContext->Rsp, pContext->Rip, pContext->EFlags);
#else
			len += _snprintf(m_text + len, EXCEPTION_TEXT_LEN - len, "Windows Version : %d.%d.%d SP: %d.%d\n\
		HAL Version     : %d.%d.%d.%d\n\
		ContextFlags    : 0x%08X\n\
		ExceptionCode   : 0x%08X\n\
		CONTEXT_DEBUG_REGISTERS:\n\
		Dr0   : 0x%08X    Dr1   : 0x%08X    Dr2   : 0x%08X\n\
		Dr3   : 0x%08X    Dr6   : 0x%08X    Dr7   : 0x%08X\n\
		CONTEXT_SEGMENTS:\n\
		SegGs : 0x%08X    SegFs : 0x%08X\n\
		SegEs : 0x%08X    SegDs : 0x%08X\n\
		CONTEXT_INTEGER\n\
		EDI   : 0x%08X    ESI   : 0x%08X    EBX   : 0x%08X\n\
		EDX   : 0x%08X    ECX   : 0x%08X    EAX   : 0x%08X\n\
		CONTEXT_CONTROL\n\
		EBP   : 0x%08X    EIP   : 0x%08X    SegCS : 0x%08X\n\
		EFlags: 0x%08X    ESP   : 0x%08X    SegSS : 0x%08X\n\n",
				info.dwMajorVersion, info.dwMinorVersion, info.dwBuildNumber, info.wServicePackMajor, info.wServicePackMinor,
				API_VERSION_MAJOR, API_VERSION_MINOR, API_VERSION_TEST, API_VERSION_BUILD,
				pContext->ContextFlags, exception->ExceptionCode, pContext->Dr0, pContext->Dr1, pContext->Dr2, pContext->Dr3, pContext->Dr6, pContext->Dr7,
				pContext->SegGs, pContext->SegFs, pContext->SegEs, pContext->SegDs,
				pContext->Edi, pContext->Esi, pContext->Ebx, pContext->Edx, pContext->Ecx, pContext->Eax,
				pContext->Ebp, pContext->Eip, pContext->SegCs, pContext->EFlags, pContext->Esp, pContext->SegSs);
#endif
		}
		__except (EXCEPTION_EXECUTE_HANDLER)
		{
			LOG_ERROR("SEH", "DOUBLE EXCEPTION HANDLED!!!");
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void SEH_Exception::start_handling()
	{
		_set_se_translator(translate);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void SEH_Exception::stop_handling()
	{
		_set_se_translator(nullptr);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void SEH_Exception::translate(uint32_t dwEC, EXCEPTION_POINTERS* pSEH)
	{
		throw SEH_Exception(pSEH);
	}
#else
	// static void rtx_sig_handler(int signum)
	// {
	// 	if (signum == SIGSEGV)
	// 	{
	// 		exit(-1);
	// 	}
	// }

	void intialize_signal_handlers()
	{
		/*struct sigaction act;
		struct sigaction oldact;
		int signum = SIGSEGV;

		act.sa_handler = rtx_sig_handler;
		act.sa_flags = 0;
		act.sa_mask = 0;
		act.sa_restorer = nullptr;

		int err = sigaction(signum, &act, &oldact);*/
	}
#endif
}
//--------------------------------------------------------------------------
