﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_timer.h"

namespace rtl
{
	//--------------------------------------
	//
	//--------------------------------------
	enum TimerCallbackMode { TimerCallbackFunction, TimerCallbackInterface };
	//--------------------------------------
	//
	//--------------------------------------
	struct TimerUser
	{
		TimerCallbackMode callbackMode;
		volatile bool repeatingTimer;
		volatile bool disabled;

		volatile uint32_t timeStart;
		volatile uint32_t timeToElapse;

		uint32_t timerId; // для использования множества таймеров одним объектом

		union
		{
			ITimerEventHandler* timerHandler;
			TimerEventCallback timerCallback;
		};

		void* userData;
		TimerUser* next;
	};
	//--------------------------------------
	//
	//--------------------------------------
	static MutexWatch g_timer_sync("timer-man");
	static TimerUser* g_timer_first_user = nullptr;
	static TimerUser* g_timer_last_user = nullptr;
	static volatile bool g_timer_thread_enter = false;
	static Thread g_timer_thread;
	static Event g_timer_event;
	//--------------------------------------
	//
	//--------------------------------------
	static void timer_thread_proc(Thread* thread, void* context);
	static TimerUser* timer_find_user(ITimerEventHandler* user, uint32_t tid);
	static TimerUser* timer_find_user(TimerEventCallback user, void* userData, uint32_t tid);
	static void timer_check_users();
	//--------------------------------------
	//
	//--------------------------------------
	Timer::Timer(bool realtime, ITimerEventHandler* handler, Logger& log, const char* name) :
		m_resolution(0),
		m_started(false),
		m_stopping(false),
		m_pause(false),
		m_idealTime(0),
		m_realtime(realtime),
		m_handler(handler),
		m_callback(nullptr),
		m_userdata(nullptr),
		m_log(log)
	{
		if (name != nullptr && name[0] != 0)
		{
			strncpy(m_name, name, sizeof(m_name));
			m_name[sizeof(m_name) - 1] = 0;
		}
		else
		{
			strcpy(m_name, realtime ? "timer-rt" : "timer-n");
		}

		LOG_TIMER("timer", "%s timer '%s' created", !m_realtime ? "normal" : "realtime", name);
	}
	//--------------------------------------
	//
	//--------------------------------------
	Timer::Timer(bool realtime, TimerEventCallback handler, void* userdata, Logger& log, const char* name) :
		m_resolution(0),
		m_started(false),
		m_stopping(false),
		m_pause(false),
		m_idealTime(0),
		m_realtime(realtime),
		m_handler(nullptr),
		m_callback(handler),
		m_userdata(userdata),
		m_log(log)
	{
		if (name != nullptr && name[0] != 0)
		{
			strncpy(m_name, name, sizeof(m_name));
			m_name[sizeof(m_name) - 1] = 0;
		}
		else
		{
			strcpy(m_name, realtime ? "timer-rt" : "timer-n");
		}

		LOG_TIMER("timer", "%s timer '%s' created", !m_realtime ? "normal" : "realtime", name);
	}
	//--------------------------------------
	//
	//--------------------------------------
	Timer::~Timer()
	{
		stop();

		LOG_TIMER("timer", "%s timer '%s' destroyed", !m_realtime ? "normal" : "realtime", m_name);
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Timer::start(int resolution)
	{
		if (m_stopping)
			return false;

		if (m_started && resolution == m_resolution)
			return true;

		MLOG_TIMER("timer", "timer '%s' : starting res:%u ms...", m_name, resolution);

		stop();

		if (resolution == 0)
		{
			MLOG_TIMER("timer", "timer '%s' : start disabled -- resolution is 0", m_name);
			return false;
		}

		m_resolution = resolution;
		m_pause = false;

		if (!m_stopEvent.create(true, false))
		{
			MLOG_ERROR("timer", "timer '%s' : start failed : CreateEvent() failed", m_name); // GetLastError()
			return false;
		}

		// создадим поток таймера

		if (!m_thread.start(this))
		{
			stop();
			MLOG_ERROR("timer", "timer '%s' : Start failed : Thread.start() failed", m_name); // GetLastError(), 
			return false;
		}

		MLOG_TIMER("timer", "timer '%s' : started", m_name);

		return m_started = true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Timer::stop()
	{
		// остановим поток
		if (!m_stopEvent.is_valid())
			return;

		MLOG_TIMER("timer", "timer '%s' : stopping...", m_name);

		m_pause = false;
		m_stopping = true;

		m_stopEvent.signal();

		if (m_thread.isStarted())
		{
			MLOG_TIMER("timer", "timer '%s' : waiting thread stop...", m_name);

			m_thread.wait();

			MLOG_TIMER("timer", "timer '%s' : thread stopped", m_name);

		}

		if (m_stopEvent.is_valid())
		{
			m_stopEvent.close();
		}

		m_idealTime = 0;
		m_resolution = 0;
		m_started = false;
		m_stopping = false;

		MLOG_TIMER("timer", "timer '%s' : stopped", m_name);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Timer::thread_run(Thread* thread)
	{
		uint32_t current;
		uint32_t wait_time = m_resolution;
		int diff;

		m_thread.set_priority(m_realtime ? ThreadPriority::Realtime : ThreadPriority::Normal);

		MLOG_CALL("timer", "timer '%s' : Thread enter resolution %u...", m_name, m_resolution);

		m_idealTime = DateTime::getTicks() + m_resolution;

		while (!m_stopping)
		{
			// 1. ждем событие
			// если таймаут то таймер сработал
			//MLOG_TIMER("timer", "timer '%s' : Timer wait...", m_name);

			if (!m_stopEvent.wait(wait_time))
			{
				//MLOG_TIMER("timer", "timer '%s' : Timer tick...", m_name);

				if (m_stopping)
				{
					//MLOG_TIMER("timer", "timer '%s' : Timer stop...", m_name);
					break;
				}

				current = DateTime::getTicks();

				diff = (int)(current - m_idealTime);


				if (m_handler != nullptr)
				{
					m_handler->timer_elapsed_event(this, 0);
				}
				else if (m_callback != nullptr)
				{
					m_callback(this, diff, m_userdata, 0);
				}

				m_idealTime += ((diff / m_resolution) * m_resolution) + m_resolution;

				wait_time = m_idealTime - current;

				if (wait_time <= 3)
				{
					wait_time += m_resolution;
					m_idealTime += m_resolution;
				}
			}
			else
			{
				break;
			}
		}

		MLOG_TIMER("timer", "timer '%s' : Thread exit", m_name);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Timer::initializeGlobalTimer(bool high_res)
	{
		g_timer_event.create(true, false);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Timer::destroyGlobalTimer()
	{
		g_timer_thread.wait();
		g_timer_event.close();

		MutexWatchLock lock(g_timer_sync, __FUNCTION__);

		TimerUser* next, *current = g_timer_first_user;

		while (current != nullptr)
		{
			next = current->next;
			DELETEO(current);
			current = next;
		}

		g_timer_first_user = g_timer_last_user = nullptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	static void timer_thread_proc(Thread* thread, void* context)
	{
		uint32_t next_time;
		uint32_t current_time;

		uint32_t elapsed;
		uint32_t next_tick;

		while (!thread->get_stopping_flag())
		{
			// собираем пользователей
			TimerUser* current = g_timer_first_user;
			current_time = DateTime::getTicks();
			next_time = INFINITE;

			g_timer_thread_enter = true;

			next_tick = INFINITE;

			while (current != nullptr)
			{
				//DBG_LOG("timer", "........%p...check user %u %u", current->userData, current->timeStart + current->timeToElapse, current_time);

				if (!current->disabled)
				{
					if (current->timeStart + current->timeToElapse <= current_time)
					{
						elapsed = current_time - current->timeStart;

						if (!current->repeatingTimer)
						{
							current->disabled = true;
						}
						else
						{
							current->timeStart = current_time;
						}

						if (current->callbackMode == TimerCallbackFunction)
						{
							//LOG_TIMER("timer", "........timer elapsed for %p-%p time:%d ms rep:%s", current->timerCallback, current->timerId, time, STR_BOOL(current->repeatingTimer));
							current->timerCallback(nullptr, elapsed, current->userData, current->timerId);
						}
						else if (current->callbackMode == TimerCallbackInterface)
						{
							//LOG_TIMER("timer", "........timer elapsed for %p-%p time:%d ms rep:%s", current->timerHandler, current->timerId, time, STR_BOOL(current->repeatingTimer));
							current->timerHandler->timer_elapsed_event(nullptr, current->timerId);
						}
					}

					if (!current->disabled)
					{
						uint32_t next_fire = current->timeStart + current->timeToElapse;
						if (next_fire > current_time)
						{
							if (next_fire < next_tick)
								next_tick = next_fire;

							//DBG_LOG("timer", "........%p...calc timeout %u/%u", current->userData, next_fire, next_tick);
						}
					}
				}

				current = current->next;
			}

			g_timer_thread_enter = false;

			if (next_tick < next_time)
				next_time = next_tick;

			timer_check_users();

			if (g_timer_event.wait(next_tick > current_time ? next_tick - current_time : 0) && thread->get_stopping_flag())
				break;

			g_timer_event.reset();
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	static TimerUser* timer_find_user(ITimerEventHandler* handler, uint32_t tid)
	{
		TimerUser* current = g_timer_first_user;

		while (current != nullptr)
		{
			if (current->callbackMode == TimerCallbackInterface && current->timerHandler == handler && current->timerId == tid)
			{
				return current;
			}

			current = current->next;
		}

		return nullptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	static TimerUser* timer_find_user(TimerEventCallback func, void* userData, uint32_t tid)
	{
		TimerUser* current = g_timer_first_user;

		while (current != nullptr)
		{
			if (current->callbackMode == TimerCallbackFunction && current->timerCallback == func && current->userData == userData && current->timerId == tid)
			{
				return current;
			}

			current = current->next;
		}

		return nullptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	static void timer_check_users()
	{
		//MutexLock lock(g_timer_sync);
		MutexWatchLock lock(g_timer_sync, __FUNCTION__);

		TimerUser* current = g_timer_first_user;
		TimerUser* prev = nullptr;
		TimerUser* to_delete = nullptr;

		while (current != nullptr)
		{
			if (current->disabled)
			{
				// нашли
				if (prev != nullptr)
				{
					prev->next = current->next;
					if (current == g_timer_last_user)
						g_timer_last_user = prev;
				}
				else
				{
					g_timer_first_user = current->next;

					// если единственный то ссылка на последнего тоже обнуляется
					if (g_timer_first_user == nullptr)
					{
						g_timer_last_user = nullptr;
					}
				}

				if (Logger::check_trace(TRF_TIMER))
				{
					if (current->callbackMode == TimerCallbackFunction)
						Log.log("timer", "........auto delete timer %p-%p-%d", current->timerCallback, current->userData, current->timerId);
					else
						Log.log("timer", "........auto delete timer %p-%d", current->timerHandler, current->timerId);
				}

				to_delete = current;
				current = current->next;
				DELETEO(to_delete);
			}
			else
			{
				prev = current;
				current = current->next;
			}
		}

	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API void Timer::setTimer(ITimerEventHandler* handler, uint32_t tid, uint32_t time, bool repeat)
	{
		//MutexLock lock(g_timer_sync);
		MutexWatchLock lock(g_timer_sync, __FUNCTION__);

		TimerUser* user = timer_find_user(handler, tid);

		if (user == nullptr)
		{
			LOG_TIMER("timer", "........add timer:%p-%d time:%d ms rep:%s", handler, tid, time, STR_BOOL(repeat));

			user = NEW TimerUser;
			user->callbackMode = TimerCallbackInterface;
			user->disabled = false;
			user->repeatingTimer = repeat;
			user->timerHandler = handler;
			user->userData = nullptr;
			user->timerId = tid;
			user->timeStart = DateTime::getTicks();
			user->timeToElapse = time;
			user->next = nullptr;

			LOG_TIMER("timer", "........add timer 2");

			if (g_timer_last_user == nullptr)
			{
				LOG_TIMER("timer", "........add timer 3");
				g_timer_last_user = g_timer_first_user = user;
			}
			else
			{
				LOG_TIMER("timer", "........add timer 4");
				g_timer_last_user->next = user;
				g_timer_last_user = user;
			}

			LOG_TIMER("timer", "........timer added");
		}
		else
		{
			LOG_TIMER("timer", "........update timer:%p-%d time:%d ms rep:%s", handler, tid, time, STR_BOOL(repeat));
			user->disabled = false;
			user->repeatingTimer = repeat;
			user->timeStart = DateTime::getTicks();
			user->timeToElapse = time;
			LOG_TIMER("timer", "........timer updated");
		}

		if (!g_timer_thread.isStarted())
		{
			g_timer_thread.set_priority(ThreadPriority::High);
			g_timer_thread.start(timer_thread_proc);
		}
		else
		{
			g_timer_event.signal();
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API void  Timer::setTimer(TimerEventCallback callback, void* userData, uint32_t tid, uint32_t time, bool repeat)
	{
		MutexWatchLock lock(g_timer_sync, __FUNCTION__);

		TimerUser* user = timer_find_user(callback, userData, tid);

		if (user == nullptr)
		{
			LOG_TIMER("timer", "........add timer:%p-%p-%d time:%u ms rep:%s", callback, userData, tid, time, STR_BOOL(repeat));

			user = NEW TimerUser;
			user->callbackMode = TimerCallbackFunction;
			user->disabled = false;
			user->repeatingTimer = repeat;
			user->timerCallback = callback;
			user->userData = userData;
			user->timerId = tid;
			user->timeStart = DateTime::getTicks();
			user->timeToElapse = time;
			user->next = nullptr;

			if (g_timer_last_user == nullptr)
			{
				g_timer_last_user = g_timer_first_user = user;
			}
			else
			{
				g_timer_last_user->next = user;
				g_timer_last_user = user;
			}

			LOG_TIMER("timer", "........timer added");
		}
		else
		{
			LOG_TIMER("timer", "........update timer:%p-%p-%d time:%u ms rep:%s", callback, userData, tid, time, STR_BOOL(repeat));

			user->disabled = false;
			user->repeatingTimer = repeat;
			user->timeStart = DateTime::getTicks();
			user->timeToElapse = time;

			LOG_TIMER("timer", "........timer updated");
		}

		if (!g_timer_thread.isStarted())
		{
			g_timer_thread.set_priority(ThreadPriority::Highest);
			g_timer_thread.start(timer_thread_proc);
		}
		else
		{
			g_timer_event.signal();
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API void  Timer::stopTimer(ITimerEventHandler* handler, uint32_t tid)
	{
		bool res = false; // 0 - not found, 1 - stopped

		//MutexLock lock(g_timer_sync);
		MutexWatchLock lock(g_timer_sync, __FUNCTION__);

		TimerUser* current = g_timer_first_user, *prev = nullptr;

		LOG_TIMER("timer", "........stop timer %p-%d", handler, tid);

		// ищем линк
		while (current != nullptr)
		{
			if (current->callbackMode == TimerCallbackInterface && current->timerHandler == handler && current->timerId == tid)
			{
				res = true;
				break;
			}

			prev = current;
			current = current->next;
		}

		// нашли
		if (current != nullptr)
		{
			if (prev != nullptr)
			{
				prev->next = current->next;
				if (current == g_timer_last_user)
					g_timer_last_user = prev;
			}
			else
			{
				g_timer_first_user = current->next;

				// если единственный то ссылка на последнего тоже обнуляется
				if (g_timer_first_user == nullptr)
				{
					g_timer_last_user = nullptr;
				}
			}

			// ждем окончания работы таймера
			uint32_t start_ticks = DateTime::getTicks();

			// ждем 1 секунду после чего ругаемся
			while (g_timer_thread_enter)
			{
				Thread::sleep(5);

				uint32_t delay = DateTime::getTicks() - start_ticks;

				if (delay > 1200) // ругаемся!!! и выходим
				{
					LOG_ERROR("timer", "std timer deleting timeout (%u ms) error! ", delay);
					break;
				}
			}

			DELETEO(current);
		}

		LOG_TIMER("timer", "........timer %s", res ? "stopped" : "not found");
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API void Timer::stopTimer(TimerEventCallback callback, void* userData, uint32_t tid)
	{
		bool res = false;

		//MutexLock lock(g_timer_sync);
		MutexWatchLock lock(g_timer_sync, __FUNCTION__);

		TimerUser* current = g_timer_first_user, *prev = nullptr;

		LOG_TIMER("timer", "........stop timer %p-%p-%d", callback, userData, tid);

		// ищем линк
		while (current != nullptr)
		{
			if (current->callbackMode == TimerCallbackFunction && current->timerCallback == callback && current->userData == userData && current->timerId == tid)
			{
				res = true;
				break;
			}

			prev = current;
			current = current->next;
		}

		// нашли
		if (current != nullptr)
		{
			if (prev != nullptr)
			{
				prev->next = current->next;
				if (current == g_timer_last_user)
					g_timer_last_user = prev;
			}
			else
			{
				g_timer_first_user = current->next;

				// если единственный то ссылка на последнего тоже обнуляется
				if (g_timer_first_user == nullptr)
				{
					g_timer_last_user = nullptr;
				}
			}

			// ждем окончания работы таймера
			uint32_t start_ticks = DateTime::getTicks();

			// ждем 3 секунды после чего ругаемся
			while (g_timer_thread_enter)
			{
				Thread::sleep(5);

				uint32_t delay = DateTime::getTicks() - start_ticks;

				if (delay > 3000) // ругаемся!!! и выходим
				{
					break;
				}
			}

			DELETEO(current);
		}

		LOG_TIMER("timer", "........timer %s", res ? "stopped" : "not found");
	}
}
//--------------------------------------
