﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /*
 *  sha1.c
 *
 *  Description:
 *      This file implements the Secure Hashing Algorithm 1 as
 *      defined in FIPS PUB 180-1 published April 17, 1995.
 *
 *      The SHA-1, produces a 160-bit message digest for a given
 *      data stream.  It should take about 2**n steps to find a
 *      message with the same digest as a given message and
 *      2**(n/2) to find any two messages with the same digest,
 *      when n is the digest size in bits.  Therefore, this
 *      algorithm can serve as a means of providing a
 *      "fingerprint" for a message.
 *
 *  Portability Issues:
 *      SHA-1 is defined in terms of 32-bit "words".  This code
 *      uses <stdint.h> (included via "sha1.h" to define 32 and 8
 *      bit unsigned integer types.  If your C compiler does not
 *      support 32 bit unsigned integers, this code is not
 *      appropriate.
 *
 *  Caveats:
 *      SHA-1 is designed to work with messages less than 2^64 bits
 *      long.  Although SHA-1 allows a message digest to be generated
 *      for messages of any number of bits less than 2^64, this
 *      implementation only works with messages with a length that is
 *      a multiple of the size of an 8-bit character.
 *
 */
//--------------------------------------
#include "stdafx.h"

#include "std_sha1.h"
//--------------------------------------
//  Define the SHA1 circular left shift macro
//--------------------------------------
#define SHA1CircularShift(bits,word) (((word) << (bits)) | ((word) >> (32-(bits))))
//--------------------------------------
/*
 *  SHA1Reset
 *
 *  Description:
 *      This function will initialize the SHA1Context in preparation
 *      for computing a New SHA1 message digest.
 *
 *  Parameters:
 *      context: [in/out]
 *          The context to reset.
 *
 *  Returns:
 *      sha Error Code.
 *
 */
//--------------------------------------
sha1_context_t::sha1_context_t()
{
    m_length_low			= 0;
    m_length_high			= 0;
    m_message_block_index	= 0;

    m_intermediate_hash[0]   = 0x67452301;
    m_intermediate_hash[1]   = 0xEFCDAB89;
    m_intermediate_hash[2]   = 0x98BADCFE;
    m_intermediate_hash[3]   = 0x10325476;
    m_intermediate_hash[4]   = 0xC3D2E1F0;

    m_computed   = 0;
    m_corrupted  = 0;
}
//--------------------------------------
//
//--------------------------------------
int sha1_context_t::reset()
{
    m_length_low             = 0;
    m_length_high            = 0;
    m_message_block_index    = 0;

    m_intermediate_hash[0]   = 0x67452301;
    m_intermediate_hash[1]   = 0xEFCDAB89;
    m_intermediate_hash[2]   = 0x98BADCFE;
    m_intermediate_hash[3]   = 0x10325476;
    m_intermediate_hash[4]   = 0xC3D2E1F0;

    m_computed   = 0;
    m_corrupted  = 0;

    return shaSuccess;
}
//--------------------------------------
/*
 *  SHA1Result
 *
 *  Description:
 *      This function will return the 160-bit message digest into the
 *      Message_Digest array  provided by the caller.
 *      NOTE: The first octet of hash is stored in the 0th element,
 *            the last octet of hash in the 19th element.
 *
 *  Parameters:
 *      context: [in/out]
 *          The context to use to calculate the SHA-1 hash.
 *      Message_Digest: [out]
 *          Where the digest is returned.
 *
 *  Returns:
 *      sha Error Code.
 *
 */
//--------------------------------------
int sha1_context_t::result(uint8_t digest[SHA1_HASH_SIZE])
{
    int i;

    if (!digest)
    {
        return shaNull;
    }

    if (m_corrupted)
    {
        return m_corrupted;
    }

    if (!m_computed)
    {
        sha1_pad_message();
        
		for (i = 0; i < 64; ++i)
        {
            /* message may be sensitive, clear it out */
            m_message_block[i] = 0;
        }
        m_length_low = 0;    /* and clear length */
        m_length_high = 0;
        m_computed = 1;
    }

    for(i = 0; i < SHA1_HASH_SIZE; ++i)
    {
        digest[i] = m_intermediate_hash[i>>2] >> 8 * ( 3 - ( i & 0x03 ) );
    }

    return shaSuccess;
}
//--------------------------------------
/*
 *  SHA1Input
 *
 *  Description:
 *      This function accepts an array of octets as the next portion
 *      of the message.
 *
 *  Parameters:
 *      context: [in/out]
 *          The SHA context to update
 *      message_array: [in]
 *          An array of characters representing the next portion of
 *          the message.
 *      length: [in]
 *          The length of the message in message_array
 *
 *  Returns:
 *      sha Error Code.
 *
 */
//--------------------------------------
int sha1_context_t::input(const uint8_t* message_array, int length)
{
    if (!length)
    {
        return shaSuccess;
    }

    if (!message_array)
    {
        return shaNull;
    }

    if (m_computed)
    {
        m_corrupted = shaStateError;
        return shaStateError;
    }

    if (m_corrupted)
    {
         return m_corrupted;
    }
    
	while(length-- && !m_corrupted)
    {
		m_message_block[m_message_block_index++] = (*message_array & 0xFF);

		m_length_low += 8;
		if (m_length_low == 0)
		{
			m_length_high++;
			if (m_length_high == 0)
			{
				/* Message is too long */
				m_corrupted = 1;
			}
		}

		if (m_message_block_index == 64)
		{
			sha1_process_message_block();
		}

		message_array++;
    }

    return shaSuccess;
}
//--------------------------------------
/*
 *  SHA1ProcessMessageBlock
 *
 *  Description:
 *      This function will process the next 512 bits of the message
 *      stored in the Message_Block array.
 *
 *  Parameters:
 *      None.
 *
 *  Returns:
 *      Nothing.
 *
 *  Comments:
 *      Many of the variable names in this code, especially the
 *      single character names, were used because those were the
 *      names used in the publication.
 *
 *
 */
//--------------------------------------
void sha1_context_t::sha1_process_message_block()
{
    const uint32_t K[] =    {       /* Constants defined in SHA-1   */
                            0x5A827999,
                            0x6ED9EBA1,
                            0x8F1BBCDC,
                            0xCA62C1D6
                            };
    int           t;                 /* Loop counter                */
    uint32_t      temp;              /* Temporary word value        */
    uint32_t      W[80];             /* Word sequence               */
    uint32_t      A, B, C, D, E;     /* Word buffers                */

    /*
     *  Initialize the first 16 words in the array W
     */
    for(t = 0; t < 16; t++)
    {
        W[t] = m_message_block[t * 4] << 24;
        W[t] |= m_message_block[t * 4 + 1] << 16;
        W[t] |= m_message_block[t * 4 + 2] << 8;
        W[t] |= m_message_block[t * 4 + 3];
    }

    for(t = 16; t < 80; t++)
    {
       W[t] = SHA1CircularShift(1,W[t-3] ^ W[t-8] ^ W[t-14] ^ W[t-16]);
    }

    A = m_intermediate_hash[0];
    B = m_intermediate_hash[1];
    C = m_intermediate_hash[2];
    D = m_intermediate_hash[3];
    E = m_intermediate_hash[4];

    for(t = 0; t < 20; t++)
    {
        temp =  SHA1CircularShift(5,A) +
                ((B & C) | ((~B) & D)) + E + W[t] + K[0];
        E = D;
        D = C;
        C = SHA1CircularShift(30,B);
        B = A;
        A = temp;
    }

    for(t = 20; t < 40; t++)
    {
        temp = SHA1CircularShift(5,A) + (B ^ C ^ D) + E + W[t] + K[1];
        E = D;
        D = C;
        C = SHA1CircularShift(30,B);
        B = A;
        A = temp;
    }

    for(t = 40; t < 60; t++)
    {
        temp = SHA1CircularShift(5,A) +
               ((B & C) | (B & D) | (C & D)) + E + W[t] + K[2];
        E = D;
        D = C;
        C = SHA1CircularShift(30,B);
        B = A;
        A = temp;
    }

    for(t = 60; t < 80; t++)
    {
        temp = SHA1CircularShift(5,A) + (B ^ C ^ D) + E + W[t] + K[3];
        E = D;
        D = C;
        C = SHA1CircularShift(30,B);
        B = A;
        A = temp;
    }

    m_intermediate_hash[0] += A;
    m_intermediate_hash[1] += B;
    m_intermediate_hash[2] += C;
    m_intermediate_hash[3] += D;
    m_intermediate_hash[4] += E;

    m_message_block_index = 0;
}
//--------------------------------------
/*
 *  SHA1PadMessage
 *
 *  Description:
 *      According to the standard, the message must be padded to an even
 *      512 bits.  The first padding bit must be a '1'.  The last 64
 *      bits represent the length of the original message.  All bits in
 *      between should be 0.  This function will pad the message
 *      according to those rules by filling the Message_Block array
 *      accordingly.  It will also call the ProcessMessageBlock function
 *      provided appropriately.  When it returns, it can be assumed that
 *      the message digest has been computed.
 *
 *  Parameters:
 *      context: [in/out]
 *          The context to pad
 *      ProcessMessageBlock: [in]
 *          The appropriate SHA*ProcessMessageBlock function
 *  Returns:
 *      Nothing.
 *
 */
//--------------------------------------
void sha1_context_t::sha1_pad_message()
{
    /*
     *  Check to see if the current message block is too small to hold
     *  the initial padding bits and length.  If so, we will pad the
     *  block, process it, and then continue padding into a second
     *  block.
     */
    if (m_message_block_index > 55)
    {
        m_message_block[m_message_block_index++] = 0x80;
        while(m_message_block_index < 64)
        {
            m_message_block[m_message_block_index++] = 0;
        }

        sha1_process_message_block();

        while(m_message_block_index < 56)
        {
            m_message_block[m_message_block_index++] = 0;
        }
    }
    else
    {
        m_message_block[m_message_block_index++] = 0x80;
        while(m_message_block_index < 56)
        {
            m_message_block[m_message_block_index++] = 0;
        }
    }

    //  Store the message length as the last 8 octets
    m_message_block[56] = m_length_high >> 24;
    m_message_block[57] = m_length_high >> 16;
    m_message_block[58] = m_length_high >> 8;
    m_message_block[59] = m_length_high;
    m_message_block[60] = m_length_low >> 24;
    m_message_block[61] = m_length_low >> 16;
    m_message_block[62] = m_length_low >> 8;
    m_message_block[63] = m_length_low;

	sha1_process_message_block();
}
//--------------------------------------
