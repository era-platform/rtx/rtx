﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_crypto.h"
//--------------------------------------
//
//--------------------------------------
static const char base64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
//--------------------------------------
//
//--------------------------------------
RTX_STD_API int Base64_CalcDecodedSize(int encoded)
{
	return encoded - (encoded / 4);
}
//--------------------------------------
//
//--------------------------------------
RTX_STD_API int Base64_CalcEncodedSize(int original)
{
	original += 3 - (original % 3);
	return original + (original / 3);
}
//--------------------------------------
//
//--------------------------------------
RTX_STD_API int Base64Encode(const uint8_t* in, int in_len, char* out, int out_size, int lf)
{
	if (in == NULL || in_len < 1 || out == NULL || out_size < 1)
		return -1;

	int len = in_len;
	
	if ((in_len % 3) != 0)
		len += (3 - (in_len % 3));

	if (out_size < len + (len/3))
		return len + (len/3);

	char* out_base = out;

	uint8_t byte1, byte2, byte3, pos = 1;

	for (int i = 0; i < len; i += 3)
	{
		byte1 = i < in_len ? in[i] : 0;
		byte2 = i+1 < in_len ? in[i+1] : 0;
		byte3 = i+2 < in_len ? in[i+2] : 0;

		*out++ = base64[byte1 >> 2];	pos++;
		*out++ = base64[((byte1 & 0x03) << 4) | ((byte2 & 0xf0) >> 4)];		pos++;
		*out++ = i + 1 < in_len ? base64[((byte2 & 0x0f) << 2) | ((byte3 & 0xc0) >> 6)] : '=';		pos++;
		*out++ = i + 2 < in_len ? base64[byte3 & 0x3f] : '=';	pos++;

		if (pos >= lf && lf > 0)
		{
			*out++ = '\n';
			*out++ = '\r';
			pos = 1;
		}
	}

	int ll = PTR_DIFF(out, out_base);
	
	if (ll < out_size)
		*out = 0;

	return ll;
}
//--------------------------------------
//
//--------------------------------------
static uint8_t decodeChar(char ch)
{
	if (ch >= 'A' && ch <= 'Z') return ch - 'A';
	if (ch >= 'a' && ch <= 'z') return ch - 'a' + 26;
	if (ch >= '0' && ch <= '9') return ch - '0' + 52;
	if (ch == '+')              return 62;
	if (ch == '/')              return 63;

	return 0xFF;
}
//--------------------------------------
//
//--------------------------------------
RTX_STD_API int Base64Decode(const char* in, int in_len, uint8_t* out, int out_size)
{
	if (in == NULL || in_len == 0 || out == NULL || out_size < 1)
		return -1;

	if (in_len < 0)
		in_len = std_strlen(in);

	int len = in_len - (in_len / 4);

	if (out_size < len - (len/4))
		return len - (len/4);

	uint8_t* out_base = out;
	uint8_t index;
	int op = 0;

	for (int i = 0; i < in_len; i++)
	{
		if ((index = decodeChar(in[i])) != 0xFF)
		{
			switch (op)
			{
			case 0:
				*out = index << 2;
				op++;
				break;
			case 1:
				*out++ |= index >> 4;
				*out = index << 4;
				op++;
				break;
			case 2:
				*out++ |= index >> 2;
				*out = index << 6;
				op++;
				break;
			case 3:
				*out++ |= index;
				op = 0;
				break;
			}
		}
	}

	*out = 0;

	return PTR_DIFF(out, out_base);
}
//--------------------------------------
