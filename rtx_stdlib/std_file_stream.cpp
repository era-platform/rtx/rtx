﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "stdafx.h"

#if defined(TARGET_OS_FREEBSD)
#include <sys/stat.h>
#endif

#include "std_file_stream.h"

namespace rtl
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	FileStream::FileStream()
	{
		m_handle = INVALID_FILE_HANDLE;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	FileStream::~FileStream()
	{
		close();
		//delete m_implementation;
		//m_implementation = nullptr;
	}
	//-----------------------------------------------
	// ��������� ���� ��� ������.
	// ���� ���� �� ���������� - ���������� false.
	//-----------------------------------------------
	bool FileStream::open(const char* filename)
	{
		if (isOpened())
			return false;

		if (STRING_IS_EMPTY(filename))
			return false;
#if defined(TARGET_OS_WINDOWS)
		m_handle = CreateFileA(filename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_WRITE_THROUGH, NULL);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		m_handle = ::open(filename, O_RDONLY);
#endif
		return m_handle != INVALID_FILE_HANDLE;
	}
	//-----------------------------------------------
	// ��������� ���� ��� ������.
	// ���� ���� �� ����������, ������� �����.
	// ���� ���� �� ����������, �������������� ���, ������� ������.
	//-----------------------------------------------
	bool FileStream::createNew(const char* filename)
	{
		if (isOpened())
			return false;

		if (STRING_IS_EMPTY(filename))
			return false;

#if defined(TARGET_OS_WINDOWS)
		m_handle = CreateFileA(filename, GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		m_handle = ::open(filename, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH);
#endif

		return m_handle != INVALID_FILE_HANDLE;
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	bool FileStream::create_or_append(const char* filename)
	{
		if (isOpened())
			return false;

		if (STRING_IS_EMPTY(filename))
			return false;

#if defined(TARGET_OS_WINDOWS)
		m_handle = CreateFileA(filename, GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		m_handle = ::open(filename, O_WRONLY | O_CREAT | O_APPEND, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH);
#endif

		if (m_handle != INVALID_FILE_HANDLE)
			_seek_position_end(0);

		return m_handle != INVALID_FILE_HANDLE;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void FileStream::close()
	{
		if (isOpened())
		{
#if defined(TARGET_OS_WINDOWS)
			CloseHandle(m_handle);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
			::close(m_handle);
#endif
		}
		m_handle = INVALID_FILE_HANDLE;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	size_t FileStream::getLength()
	{
		size_t length = 0;

		if (isOpened())
		{
#if defined(TARGET_OS_WINDOWS)
			LARGE_INTEGER fileSize = { 0 };

			if (!GetFileSizeEx(m_handle, &fileSize))
				return 0;
#if defined (TARGET_ARCH_X86)
			length = fileSize.LowPart;
#elif defined (TARGET_ARCH_X64)
			length = int64_t(fileSize.QuadPart);
#endif
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
			// �������� ������� �������
			off_t oldpos = ::lseek(m_handle, 0, SEEK_CUR);
			if (oldpos == -1)
				return 0;

			// ��������� ������� � ����� �����
			off_t endpos = ::lseek(m_handle, 0, SEEK_END);
			if (endpos == -1)
				return 0;

			// ������ ��� ����
			::lseek(m_handle, oldpos, SEEK_SET);

			length = (uintptr_t)endpos;
#endif
		}

		return length;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	size_t FileStream::setLength(size_t length)
	{
		if (!isOpened())
			return 0;

		bool result = _set_file_size(length);

		length = getLength();

		if (!result)
			return length;

		seek(SeekOrigin::End, 0);

		return length;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	size_t FileStream::getPosition()
	{
		if (!isOpened())
			return 0;

		size_t result = 0;

#if defined (TARGET_OS_WINDOWS)
		LARGE_INTEGER distanceToMove = { 0 };
		LARGE_INTEGER newFilePointer = { 0 };

		if (SetFilePointerEx(m_handle, distanceToMove, &newFilePointer, FILE_CURRENT))
		{
#if defined (TARGET_ARCH_X86)
			result = newFilePointer.LowPart;
#elif defined (TARGET_ARCH_X64)
			result = newFilePointer.QuadPart;
#endif
		}
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		off_t pos = ::lseek(m_handle, 0, SEEK_CUR);

		if (pos != -1)
		{
			result = pos;
		}
#endif
		return result;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	size_t FileStream::seek(SeekOrigin origin, intptr_t pos)
	{
		if (!isOpened())
			return 0;

		intptr_t length = getLength();
		intptr_t curpos = getPosition();

		if (origin == SeekOrigin::Begin)
		{
			if (pos > length)
				pos = length;
			else if (pos < 0)
				pos = 0;

			if (pos != curpos)
			{
				_seek_position_begin(pos);
				curpos = getPosition();
			}
		}
		else if (origin == SeekOrigin::Current)
		{
			if (pos < 0)
			{
				int64_t expected = curpos + pos;
				if (expected < 0)
					pos = -curpos;
			}
			else if (pos > 0)
			{
				int64_t expected = curpos + pos;

				// ��������� �� ������ int64 ��� �� length?
				if ((expected < 0) || (expected > length))
					pos = length - curpos;
			}

			if (pos != 0)
			{
				_seek_position_current(pos);
				curpos = getPosition();
			}
		}
		else
		{
			if (pos > 0)
				pos = 0;

			int64_t expected = length + pos;
			if (expected < 0)
				pos = -length;

			expected = length + pos;
			if (expected != curpos)
			{
				_seek_position_end(pos);
				curpos = getPosition();
			}
		}

		return curpos;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	size_t FileStream::write(const void* data, size_t len)
	{
		if (!isOpened())
			return 0;

		if ((data == NULL) || (len == 0))
			return 0;

#if defined (TARGET_OS_WINDOWS)
		DWORD towrite = DWORD(len);
		DWORD written = 0;

		if (!WriteFile(m_handle, data, towrite, &written, NULL))
			return 0;
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		int written = ::write(m_handle, data, size_t(len));
		if (written == -1)
			return 0;
#endif

		return written;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	size_t FileStream::read(void* buffer, size_t size)
	{
		if (!isOpened())
			return 0;

		if ((buffer == NULL) || (size == 0))
			return 0;

#if defined (TARGET_OS_WINDOWS)
		DWORD toread = DWORD(size);
		DWORD read = 0;

		if (!ReadFile(m_handle, buffer, toread, &read, NULL))
			return 0;
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		int read = ::read(m_handle, buffer, size_t(size));
		if (read == -1)
			return 0;
#endif

		return read;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool FileStream::_set_file_size(size_t size)
	{
#if defined(TARGET_OS_WINDOWS)
		LARGE_INTEGER temppos = { 0 };
		temppos.QuadPart = LONGLONG(size);
		if (!SetFilePointerEx(m_handle, temppos, NULL, FILE_BEGIN))
			return 0;
		if (!SetEndOfFile(m_handle))
			return false;
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		off_t off = off_t(size);
		if (::ftruncate(m_handle, off) != 0)
			return false;
#endif
		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool FileStream::_seek_position_begin(intptr_t pos)
	{
#if defined(TARGET_OS_WINDOWS)
		LARGE_INTEGER distanceToMove = { 0 };
		distanceToMove.QuadPart = LONGLONG(pos);
		if (!SetFilePointerEx(m_handle, distanceToMove, NULL, FILE_BEGIN))
			return false;
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		off_t offset = off_t(pos);
		off_t res = ::lseek(m_handle, offset, SEEK_SET);
		if (res == -1)
			return false;
#endif
		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool FileStream::_seek_position_current(intptr_t pos)
	{
#if defined(TARGET_OS_WINDOWS)
		LARGE_INTEGER distanceToMove = { 0 };
		distanceToMove.QuadPart = LONGLONG(pos);

		if (!SetFilePointerEx(m_handle, distanceToMove, NULL, FILE_CURRENT))
			return false;
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		off_t offset = off_t(pos);

		off_t res = ::lseek(m_handle, offset, SEEK_CUR);
		if (res == -1)
			return false;
#endif
		return true;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool FileStream::_seek_position_end(intptr_t pos)
	{
#if defined(TARGET_OS_WINDOWS)
		LARGE_INTEGER distanceToMove = { 0 };
		distanceToMove.QuadPart = LONGLONG(pos);

		return SetFilePointerEx(m_handle, distanceToMove, NULL, FILE_END) != FALSE;
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		off_t offset = off_t(pos);
		off_t res = ::lseek(m_handle, offset, SEEK_END);
		return res != -1;
#endif
	}
}
//-----------------------------------------------
