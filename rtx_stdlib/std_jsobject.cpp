/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_jsobject.h"

namespace rtl {

	//-----------------------------------------------
	//
	//-----------------------------------------------
	void JValue::makeNew(JsonTypes type)
	{
		setNull();

		switch (m_type = type)
		{
		case JsonTypes::Object: m_o_value = NEW JObject(); break;
		case JsonTypes::Array: m_a_value = NEW JArray(); break;
		}
	};
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void JValue::setValue(const JValue& value)
	{
		setNull();

		switch (m_type = value.m_type)
		{
		case JsonTypes::Boolean:
			m_b_value = value.m_b_value;
			break;
		case JsonTypes::Integer:
			m_i_value = value.m_i_value;
			break;
		case JsonTypes::Float:
			m_f_value = value.m_f_value;
			break;
		case JsonTypes::String:
			m_s_value = ::strdup(value.m_s_value);
			break;
		case JsonTypes::Object:
			m_o_value = value.m_o_value ? value.m_o_value->clone() : nullptr;
			break;
		case JsonTypes::Array:
			m_a_value = value.m_a_value ? value.m_a_value->clone() : nullptr;
			break;
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void JValue::setNull()
	{
		switch (m_type)
		{
		case JsonTypes::String: FREE(m_s_value); break;
		case JsonTypes::Object: DELETEO(m_o_value); break;
		case JsonTypes::Array: DELETEO(m_a_value); break;
		}

		m_type = JsonTypes::Null;
		m_i_value = 0;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void JValue::setObject(const JObject* o)
	{
		setNull();
		m_type = JsonTypes::Object;
		m_o_value = o ?
			o->clone() :
			nullptr;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void JValue::setArray(const JArray* a)
	{
		setNull();
		m_type = JsonTypes::Array;
		m_a_value = a ?
			a->clone() :
			nullptr;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	int JObject::compare_jobj(const JFieldPtr& l, const JFieldPtr& r)
	{
		return String::compare(l->getName(), r->getName(), false);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	int JObject::compare_jobjkey(const ZString& l, const JFieldPtr& r)
	{
		return String::compare(l, r->getName(), false);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool JObject::read(const char* text)
	{
		return false;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void JObject::write(Stream* stream) const
	{
		//...
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	JObject& JObject::assign(const JObject& object)
	{
		for (int i = 0; i < object.m_fields.getCount(); i++)
		{
			addField(*object.m_fields[i]);
		}

		return *this;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool addNullField(const String& name);
	//-----------------------------------------------
	//
	//-----------------------------------------------
	JObject* addNewObject(const String& name);
	//-----------------------------------------------
	//
	//-----------------------------------------------
	JArray* addNewArray(const String& name);
	//-----------------------------------------------
	//
	//-----------------------------------------------
	JField* addNew(const String& name, JsonTypes type);
	//-----------------------------------------------
	//
	//-----------------------------------------------
	JArray* JArray::clone() const
	{
		return nullptr;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void JArray::clean()
	{
	}
}
//-----------------------------------------------
