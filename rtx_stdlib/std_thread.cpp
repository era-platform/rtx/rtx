﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_thread.h"

//--------------------------------------
//
//--------------------------------------
#if defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
static uint32_t get_tid()
{
#if defined(TARGET_OS_LINUX)
	return (uint32_t)syscall(SYS_gettid);
#elif defined(TARGET_OS_FREEBSD)
	return (uint32_t)syscall(SYS_getpid);
#endif
}
extern uintptr_t __thread g_last_error;
#elif defined (TARGET_OS_WINDOWS)
extern uint32_t g_last_error_index;
#endif

#ifndef megafunc
static uint32_t megafunc()
{
	printf("megafunc called\n");
	return 0;
}
#endif


namespace rtl
{
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API Thread::Thread() :
		m_sync("thread-sync"),
		m_handle(0),
		m_id(0),
		m_stop_flag(true),
		m_user_function(nullptr),
		m_user_context(nullptr),
		m_handler(nullptr),
		m_priority(ThreadPriority::Normal)
	{
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API void Thread::set_priority(ThreadPriority priority)
	{
		m_priority = priority;

		if (m_handle != 0)
		{
			int os_priority = map_to_OS_priority(priority);
	#if defined (TARGET_OS_WINDOWS)		
			SetThreadPriority(m_handle, os_priority);
	#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
			sched_param par;
			par.sched_priority = os_priority;
			int res = pthread_setschedparam(m_handle, SCHED_OTHER, &par);
			if (res != 0)
			{
				LOG_ERROR("thread", "thread.set_thread_priority -- pthread_setschedparam() failed. error = %d.", res);
			}
	#endif
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API bool Thread::start(ThreadCallback user_function, void* user_context)
	{
		//MutexLock sync(m_sync);
		MutexWatchLock lock(m_sync, __FUNCTION__);

		if (m_handle == 0)
		{
			m_user_function = user_function;
			m_user_context = user_context;
			m_handler = nullptr;

			start_thread();

			m_stop_flag = m_handle == 0;
		}
		else
		{
			LOG_ERROR("thread", "error: Thread::start already started");
		}

		return m_handle != 0;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API bool Thread::start(IThreadUser* handler)
	{
		//MutexLock sync(m_sync);
		MutexWatchLock lock(m_sync, __FUNCTION__);

		if (m_handle == 0)
		{
			m_handler = handler;
			m_user_function = nullptr;
			m_user_context = nullptr;

			start_thread();

			m_stop_flag = m_handle == 0;
		}
		else
			LOG_ERROR("thread", "error: Thread::start already started");

		return m_handle != 0;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API void Thread::stop()
	{
		m_stop_flag = true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API bool Thread::wait(int period)
	{
		m_stop_flag = true;

	#if defined(TARGET_OS_WINDOWS)
		unsigned long result = WAIT_OBJECT_0;
		bool wait_thread = false;
		HANDLE handle = nullptr;

		{
			//MutexLock lock(m_sync);
			MutexWatchLock lock(m_sync, __FUNCTION__);

			if (m_handle != nullptr)
			{
				unsigned long exit_code;
				handle = m_handle;
				wait_thread = GetExitCodeThread(m_handle, &exit_code) && exit_code == STILL_ACTIVE;
				m_handle = nullptr;
			}
		}

		if (handle != nullptr)
		{
			if (wait_thread)
			{
				result = WaitForSingleObject(handle, period);
			}

			CloseHandle(handle);
		}

		return result == WAIT_OBJECT_0;
	#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		int res = 0;
		if (m_handle != 0)
		{
			res = pthread_join(m_handle, NULL);
			if (res != 0)
			{
				LOG_ERR_MSG("thread", res, "thread.join -- pthread_join() failed");
			}

			m_handle = 0;
		}

		return res == 0;
	#endif
	}
	//--------------------------------------
	//
	//--------------------------------------
	ThreadResult THREAD_CALL Thread::thread_proc(void* thread_context)
	{
		Thread* thread = (Thread*)thread_context;

		thread->set_priority(thread->m_priority);

	#if defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		thread->m_id = get_tid();
	#endif

		EXCEPTION_ENTER;

	#if defined(TARGET_OS_WINDOWS)
		try
		{
	#endif
			if (thread->m_user_function != nullptr)
			{
				thread->m_user_function(thread, thread->m_user_context);
			}
			else if (thread != nullptr && thread->m_handler != nullptr)
			{
				thread->m_stop_flag = false;
				thread->m_handler->thread_run(thread);
				thread->m_stop_flag = true;
			}
	#if defined(TARGET_OS_WINDOWS)
		}
		catch (Exception& eh)
		{
			LOG_ERROR("thread", "Exception raised %s!", eh.get_message());
			Log.destroy();
			Exception::kill();
		}
	#endif

		EXCEPTION_LEAVE;

	#if defined(TARGET_OS_WINDOWS)
		_endthreadex(0);
	#endif

		return 0;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Thread::start_thread()
	{
		int res = 0;
	#if defined(TARGET_OS_WINDOWS)
		if ((m_handle = (HANDLE)_beginthreadex(nullptr, 0, thread_proc, this, 0, &m_id)) == nullptr)
		{
			LOG_ERROR("thread", "error: _beginthreadex return %d/%d", errno, GetLastError());
			res = -1;
		}
	#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		res = pthread_create(&m_handle, NULL, thread_proc, this);
		if (res != 0)
		{
			LOG_ERROR("thread", "thread.create_thread -- pthread_create() failed. error = %d.", res);
		}
	#endif

		return res == 0;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	ThreadId Thread::get_current_tid()
	{
		ThreadId ret_value = 0;
	#if defined (TARGET_OS_WINDOWS)
		ret_value = GetCurrentThreadId();
	#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		ret_value = get_tid();
	#endif
		return ret_value;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	RTX_STD_API void Thread::sleep(uint32_t timeout)
	{
	#if defined(TARGET_OS_WINDOWS)
		Sleep(timeout);
	#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)

		// ��������� ��������� ��������� � �������������

		long int sec = timeout / 1000;
		long int ms = timeout % 1000;
		long int ns = ms * 1000 * 1000;

		timespec need, remaining;
		need.tv_sec = sec;
		need.tv_nsec = ns;

		while (true)
		{
			// nanosleep() ����� ���� �������, ���� ������� ������� �����-�� ������.
			// � ���� ������ � ��������� remaining ����� ������� �������.
			// � ���� �������� �������� �������� nanosleep()

			// ��� ���������� ������ ��������� -1, � errno ������ ���� ����� EINTR
			int res = ::nanosleep(&need, &remaining);
			if (res == 0)
				break;

			int err = errno;

			if (err != EINTR)//���� �����-��
			{
				LOG_ERROR("thread", "thread.sleep -- nanosleep() failed. error = %d.", err);
				return;
			}

			need.tv_sec = remaining.tv_sec;
			need.tv_nsec = remaining.tv_nsec;
		}
	#endif
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	RTX_STD_API void Thread::set_last_error(uintptr_t error)
	{
	#if defined(TARGET_OS_WINDOWS)
		TlsSetValue(g_last_error_index, (void*)error);
	#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		g_last_error = error;
	#endif
		return;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	RTX_STD_API void Thread::reset_error()
	{
#if defined(TARGET_OS_WINDOWS)
		TlsSetValue(g_last_error_index, nullptr);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		g_last_error = 0;
#endif
		return;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	RTX_STD_API uintptr_t Thread::get_last_error()
	{
	#if defined(TARGET_OS_WINDOWS)
		return (uintptr_t)TlsGetValue(g_last_error_index);
	#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		return g_last_error;
	#endif
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	int Thread::map_to_OS_priority(ThreadPriority priority)
	{
	#if defined (TARGET_OS_WINDOWS)
		switch (priority)
		{
		case ThreadPriority::Lowest:
			return THREAD_PRIORITY_LOWEST;

		case ThreadPriority::Low:
			return THREAD_PRIORITY_BELOW_NORMAL;

		case ThreadPriority::Normal:
			return THREAD_PRIORITY_NORMAL;

		case ThreadPriority::High:
			return THREAD_PRIORITY_ABOVE_NORMAL;

		case ThreadPriority::Highest:
			return THREAD_PRIORITY_HIGHEST;

		case ThreadPriority::Realtime:
			return THREAD_PRIORITY_TIME_CRITICAL;
		}

		return THREAD_PRIORITY_NORMAL;

	#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)

		int pmin = sched_get_priority_min(SCHED_OTHER);
		int pmax = sched_get_priority_max(SCHED_OTHER);

		switch (priority)
		{
		case ThreadPriority::Lowest:
			return pmin;
		case ThreadPriority::Low:
			return pmin + (pmax - pmin) / 4;
		case ThreadPriority::Normal:
			return pmin + (pmax - pmin) / 2;
		case ThreadPriority::High:
			return pmin + 3 * (pmax - pmin) / 4;
		case ThreadPriority::Highest:
		case ThreadPriority::Realtime:
			return pmax;
		}

		return -1;
	#endif
	}
}
//-----------------------------------------------
