﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "std_seh_winnt.h"

#if defined(TARGET_OS_WINDOWS)
//--------------------------------------
//
//--------------------------------------
#define SYM_BUFF_SIZE 512
//--------------------------------------
// The static symbol lookup buffer
//--------------------------------------
static char g_stSymbol[SYM_BUFF_SIZE];
//--------------------------------------
// The static source file and line number structure
//--------------------------------------
static IMAGEHLP_LINE64 g_stLine;
//--------------------------------------
// The stack frame used in walking the stack
//--------------------------------------
static STACKFRAME64 g_stFrame;
static BOOL g_firstStFrame = FALSE;
//--------------------------------------
// The original version of this code changed the CONTEXT structure when
// passed through the stack walking code.  Therefore, if the user
// utilized the containing  EXCEPTION_POINTERS to write a mini dump, the
// dump wasn't correct.  I now save off the CONTEXT as a global, much
// like the stack frame.
//--------------------------------------
static CONTEXT g_stContext;
//--------------------------------------
//
//--------------------------------------
const char* ConvertSimpleException(DWORD dwExcept)
{
	switch (dwExcept)
	{
	case EXCEPTION_ACCESS_VIOLATION:				return "EXCEPTION_ACCESS_VIOLATION";
	case EXCEPTION_DATATYPE_MISALIGNMENT:			return "EXCEPTION_DATATYPE_MISALIGNMENT";
	case EXCEPTION_BREAKPOINT:						return "EXCEPTION_BREAKPOINT";
	case EXCEPTION_SINGLE_STEP:						return "EXCEPTION_SINGLE_STEP";
	case EXCEPTION_ARRAY_BOUNDS_EXCEEDED:			return "EXCEPTION_ARRAY_BOUNDS_EXCEEDED";
	case EXCEPTION_FLT_DENORMAL_OPERAND:			return "EXCEPTION_FLT_DENORMAL_OPERAND";
	case EXCEPTION_FLT_DIVIDE_BY_ZERO:				return "EXCEPTION_FLT_DIVIDE_BY_ZERO";
	case EXCEPTION_FLT_INEXACT_RESULT:				return "EXCEPTION_FLT_INEXACT_RESULT";
	case EXCEPTION_FLT_INVALID_OPERATION:			return "EXCEPTION_FLT_INVALID_OPERATION";
	case EXCEPTION_FLT_OVERFLOW:					return "EXCEPTION_FLT_OVERFLOW";
	case EXCEPTION_FLT_STACK_CHECK:					return "EXCEPTION_FLT_STACK_CHECK";
	case EXCEPTION_FLT_UNDERFLOW:					return "EXCEPTION_FLT_UNDERFLOW";
	case EXCEPTION_INT_DIVIDE_BY_ZERO:				return "EXCEPTION_INT_DIVIDE_BY_ZERO";
	case EXCEPTION_INT_OVERFLOW:					return "EXCEPTION_INT_OVERFLOW";
	case EXCEPTION_PRIV_INSTRUCTION:				return "EXCEPTION_PRIV_INSTRUCTION";
	case EXCEPTION_IN_PAGE_ERROR:					return "EXCEPTION_IN_PAGE_ERROR";
	case EXCEPTION_ILLEGAL_INSTRUCTION:				return "EXCEPTION_ILLEGAL_INSTRUCTION";
	case EXCEPTION_NONCONTINUABLE_EXCEPTION:		return "EXCEPTION_NONCONTINUABLE_EXCEPTION";
	case EXCEPTION_STACK_OVERFLOW:					return "EXCEPTION_STACK_OVERFLOW";
	case EXCEPTION_INVALID_DISPOSITION:				return "EXCEPTION_INVALID_DISPOSITION";
	case EXCEPTION_GUARD_PAGE:						return "EXCEPTION_GUARD_PAGE";
	case EXCEPTION_INVALID_HANDLE:					return "EXCEPTION_INVALID_HANDLE";
	case 0xE06D7363:								return "Microsoft C++ Exception";
	}

	return "Unknown Exception Code";
}
//--------------------------------------
// EXCEPTION_POINTER Translation Functions Implementation
//--------------------------------------
int GetFaultReason(EXCEPTION_POINTERS* pExPtrs, char* wzStr, int iSize)
{
	if (IsBadReadPtr(pExPtrs, sizeof(EXCEPTION_POINTERS)))
		return 0;

	if (wzStr == nullptr)
		return 0;

	int BUFF_SIZE = iSize;
	// The current position in the buffer
	int iCurr = 0;

	// The variable that holds the return value

	__try
	{
		HANDLE process = GetCurrentProcess();
		SymInitialize(process, nullptr, TRUE);


		int iTemp = 0;
		// A temporary value holder. This holder keeps the stack usage
		// to a minimum.
		DWORD dwTemp;

		iCurr += GetModuleBaseName(process, nullptr, wzStr, BUFF_SIZE);

		iTemp = _snprintf(wzStr + iCurr, BUFF_SIZE - iCurr, " caused an ");
		if (iTemp >= 0)
			iCurr += iTemp;
		else
			__leave;

		const char*  strTemp = ConvertSimpleException(pExPtrs->ExceptionRecord->ExceptionCode);
		if (strTemp != nullptr)
		{
			iTemp = _snprintf(wzStr + iCurr, BUFF_SIZE - iCurr, "%s(%08X)", strTemp, pExPtrs->ExceptionRecord->ExceptionCode);

			if (iTemp >= 0)
				iCurr += iTemp;
			else
				__leave;
		}
		else
		{
			dwTemp = FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_FROM_HMODULE;
			iTemp = FormatMessageA(dwTemp, GetModuleHandle("NTDLL.DLL"), pExPtrs->ExceptionRecord->ExceptionCode, 0, wzStr + iCurr, BUFF_SIZE - iCurr, 0);
			iCurr += iTemp;
		}

		iTemp = _snprintf(wzStr + iCurr, BUFF_SIZE - iCurr, " in module ");
		if (iTemp >= 0)
			iCurr += iTemp;
		else
			__leave;




		//имя модуля где упало
		DWORD64 dwTemp64 = SymGetModuleBase64(process, (DWORD64)pExPtrs->ExceptionRecord->ExceptionAddress);
		if (dwTemp64 == 0)
		{
			iTemp = _snprintf(wzStr + iCurr, BUFF_SIZE - iCurr, "<UNKNOWN>");

			if (iTemp >= 0)
				iCurr += iTemp;
			else
				__leave;
		}
		else
			iCurr += GetModuleBaseName(process, (HINSTANCE)dwTemp64, wzStr + iCurr, BUFF_SIZE - iCurr);




		//адрес памяти, где упало
#if defined (TARGET_ARCH_X64)
		iTemp = _snprintf(wzStr + iCurr, BUFF_SIZE - iCurr, " at %p", pExPtrs->ExceptionRecord->ExceptionAddress);
#else
		iTemp = _snprintf(wzStr + iCurr, BUFF_SIZE - iCurr, " at %08X", pExPtrs->ExceptionRecord->ExceptionAddress);
#endif
		if (iTemp >= 0)
			iCurr += iTemp;
		else
			__leave;
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		LOG_ERROR("SEH", "GetFaultReason failed with exception!!!");
	}
	return iCurr;
}
//--------------------------------------
// EXCEPTION_POINTER Translation Functions Implementation
//--------------------------------------
void FillInStackFrame(PCONTEXT pCtx)
{
	// Initialize the STACKFRAME structure.
	memset(&g_stFrame, 0, sizeof(STACKFRAME64));
#if defined (TARGET_ARCH_X64)
	g_stFrame.AddrPC.Offset = pCtx->Rip;
	g_stFrame.AddrPC.Mode = AddrModeFlat;
	g_stFrame.AddrStack.Offset = pCtx->Rsp;
	g_stFrame.AddrStack.Mode = AddrModeFlat;
	g_stFrame.AddrFrame.Offset = pCtx->Rbp;
	g_stFrame.AddrFrame.Mode = AddrModeFlat;
#else
	g_stFrame.AddrPC.Offset = pCtx->Eip;
	g_stFrame.AddrPC.Mode = AddrModeFlat;
	g_stFrame.AddrStack.Offset = pCtx->Esp;
	g_stFrame.AddrStack.Mode = AddrModeFlat;
	g_stFrame.AddrFrame.Offset = pCtx->Ebp;
	g_stFrame.AddrFrame.Mode = AddrModeFlat;
#endif
}
//--------------------------------------
//
//--------------------------------------
BOOL CALLBACK CH_ReadProcessMemory(HANDLE, DWORD64 qwBaseAddress, PVOID lpBuffer, DWORD nSize, LPDWORD lpNumberOfBytesRead)
{
#if defined (TARGET_ARCH_X64)
	return ReadProcessMemory(GetCurrentProcess(), (LPCVOID)qwBaseAddress, lpBuffer, nSize, (SIZE_T*)lpNumberOfBytesRead);
#else
	return ReadProcessMemory(GetCurrentProcess(), (LPCVOID)qwBaseAddress, lpBuffer, nSize, lpNumberOfBytesRead);
#endif
}
//--------------------------------------
// The internal function that does all the stack walking
//--------------------------------------
int InternalGetStackTraceString(EXCEPTION_POINTERS* pExPtrs, DWORD dwOpts, char* wzStr, int iSize)
{

	// The value that is returned
	// The module base address. I look this up right after the stack
	// walk to ensure that the module is valid.
	DWORD64 dwModBase;

	int BUFF_SIZE = iSize;
	int iCurr = 0;

	HANDLE process = GetCurrentProcess();

	__try
	{
		// Initialize the symbol engine in case it isn't initialized.
		//InitSymEng ( ) ;

		// Note:  If the source file and line number functions are used,
		//        StackWalk can cause an access violation.
#if defined (TARGET_ARCH_X64)
		BOOL bSWRet = StackWalk64(IMAGE_FILE_MACHINE_AMD64,
#else
		BOOL bSWRet = StackWalk64(IMAGE_FILE_MACHINE_I386,
#endif
			GetCurrentProcess(),
			GetCurrentThread(),
			&g_stFrame,
			&g_stContext,
			CH_ReadProcessMemory,
			SymFunctionTableAccess64,
			SymGetModuleBase64,
			nullptr);

		if ((bSWRet == FALSE) || (g_stFrame.AddrFrame.Offset == 0))
			__leave;

		//имя функции и смещение от нее

		DWORD64 dwFuncOffset = 0;
		PIMAGEHLP_SYMBOL64 pSym = nullptr;
		BOOL bGetSym = FALSE;
		// Output the symbol name?
		if ((dwOpts & GSTSO_SYMBOL) == GSTSO_SYMBOL)
		{
			// Start looking up the exception address.
			pSym = (PIMAGEHLP_SYMBOL64)&g_stSymbol;
			ZeroMemory(pSym, SYM_BUFF_SIZE);
			pSym->SizeOfStruct = sizeof(IMAGEHLP_SYMBOL64);
			pSym->MaxNameLength = SYM_BUFF_SIZE - sizeof(IMAGEHLP_SYMBOL64);
			pSym->Address = g_stFrame.AddrPC.Offset;

			bGetSym = SymGetSymFromAddr64(process, g_stFrame.AddrPC.Offset, &dwFuncOffset, pSym);
		}

		// Before I get too carried away and start calculating
		// everything, I need to double-check that the address returned
		// by StackWalk really exists. I've seen cases in which
		// StackWalk returns TRUE but the address doesn't belong to
		// a module in the process.
		dwModBase = SymGetModuleBase64(process, g_stFrame.AddrPC.Offset);
		if (dwModBase == 0)
			__leave;

		//Базовый адрес модуля
		int iTemp = 0;
		iTemp = _snprintf(wzStr + iCurr, BUFF_SIZE - iCurr, "%p", (void*)dwModBase);//g_stFrame.AddrPC.Offset);
		if (iTemp >= 0)
			iCurr += iTemp;
		else
			__leave;
		//если получили функции в модуле, то запишем смещение в модуле
		if (bGetSym)
		{
			iTemp = _snprintf(wzStr + iCurr, BUFF_SIZE - iCurr, ":%llx",
				(g_firstStFrame ? (DWORD64)pExPtrs->ExceptionRecord->ExceptionAddress : g_stFrame.AddrPC.Offset) - dwModBase);//g_stFrame.AddrPC.Offset);
			if (iTemp >= 0)
				iCurr += iTemp;
			else
				__leave;
		}

		//!!!!!!!!!!!!!!!!!!!!!!
		//смещение относительно базы
		//!!!!!!!!!!!!!!!!!!

		// Параметры функции, если надо
		if ((dwOpts & GSTSO_PARAMS) == GSTSO_PARAMS)
		{
#if defined (TARGET_ARCH_X64)
			iTemp = _snprintf(wzStr + iCurr, BUFF_SIZE - iCurr, " (0x%0llx 0x%0llx 0x%0llx 0x%0llx)",
#else
			iTemp = _snprintf(wzStr + iCurr, BUFF_SIZE - iCurr, " (0x%08X 0x%08X 0x%08X 0x%08X)",
#endif
				g_stFrame.Params[0], g_stFrame.Params[1], g_stFrame.Params[2], g_stFrame.Params[3]);

			if (iTemp >= 0)
				iCurr += iTemp;
			else
				__leave;
		}

		// Output the module name.
		if ((dwOpts & GSTSO_MODULE) == GSTSO_MODULE)
		{
			iTemp = _snprintf(wzStr + iCurr, BUFF_SIZE - iCurr, " ");
			if (iTemp >= 0)
				iCurr += iTemp;
			else
				__leave;

			iCurr += GetModuleBaseName(process, (HINSTANCE)dwModBase, wzStr + iCurr, BUFF_SIZE - iCurr);
		}

		// Output the symbol name?
		if (((dwOpts & GSTSO_SYMBOL) == GSTSO_SYMBOL) && bGetSym)
		{
			// Start looking up the exception address.
			/* PIMAGEHLP_SYMBOL64 pSym = (PIMAGEHLP_SYMBOL64) &g_stSymbol;
			ZeroMemory(pSym, SYM_BUFF_SIZE);
			pSym->SizeOfStruct = sizeof(IMAGEHLP_SYMBOL64);
			pSym->MaxNameLength = SYM_BUFF_SIZE - sizeof(IMAGEHLP_SYMBOL64);
			pSym->Address = g_stFrame.AddrPC.Offset;*/

			// if (SymGetSymFromAddr64(process, g_stFrame.AddrPC.Offset, &dwModBase, pSym))
			{
				if (dwOpts & ~GSTSO_SYMBOL)
				{
					iTemp = _snprintf(wzStr + iCurr, BUFF_SIZE - iCurr, ",");
					if (iTemp >= 0)
						iCurr += iTemp;
					else
						__leave;
				}

				//имя функции и смещение
				if (dwModBase > 0)
					iTemp = _snprintf(wzStr + iCurr, BUFF_SIZE - iCurr, " %s()+%04llX byte(s)",
						pSym->Name, dwFuncOffset);
				else
					iTemp = _snprintf(wzStr + iCurr, BUFF_SIZE - iCurr, " %s()", pSym->Name);

				if (iTemp >= 0)
					iCurr += iTemp;
				else
					__leave;
			}
			/*else
			__leave ;*/
		}


		// Output the source file and line number information?
		if ((dwOpts & GSTSO_SRCLINE) == GSTSO_SRCLINE)
		{
			ZeroMemory(&g_stLine, sizeof(IMAGEHLP_LINE64));
			g_stLine.SizeOfStruct = sizeof(IMAGEHLP_LINE64);

			DWORD dwLineDisp;
			if (SymGetLineFromAddr64(process, g_stFrame.AddrPC.Offset, &dwLineDisp, &g_stLine))
			{
				if (dwOpts & ~GSTSO_SRCLINE)
				{
					iTemp = _snprintf(wzStr + iCurr, BUFF_SIZE - iCurr, ",");
					if (iTemp >= 0)
						iCurr += iTemp;
					else
						__leave;
				}

				iTemp = _snprintf(wzStr + iCurr, BUFF_SIZE - iCurr, " %s, line %04d",
					g_stLine.FileName, g_stLine.LineNumber);

				if (iTemp >= 0)
					iCurr += iTemp;
				else
					__leave;
			}
		}

		//wzRet = g_wzBuff ;
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		LOG_ERROR("SEH", "InternalGetStackTraceString failed with exception");
	}
	return iCurr;
}
//--------------------------------------
//
//--------------------------------------
int GetFirstStackTraceString(DWORD dwOpts, EXCEPTION_POINTERS* pExPtrs, char* wzStr, int iSize)
{
	if (IsBadReadPtr(pExPtrs, sizeof(EXCEPTION_POINTERS)))
		return 0;

	// Get the stack frame filled in.
	FillInStackFrame(pExPtrs->ContextRecord);
	g_firstStFrame = TRUE;

	// Copy over the exception pointers fields so I don't corrupt the
	// real one.
	g_stContext = *(pExPtrs->ContextRecord);

	return InternalGetStackTraceString(pExPtrs, dwOpts, wzStr, iSize);
}
//--------------------------------------
//
//--------------------------------------
int GetNextStackTraceString(DWORD dwOpts, EXCEPTION_POINTERS* pExPtrs, char* wzStr, int iSize)
{
	g_firstStFrame = FALSE;
	// All error checking is in InternalGetStackTraceString.
	// Assume that GetFirstStackTraceString has already initialized the
	// stack frame information.
	return InternalGetStackTraceString(pExPtrs, dwOpts, wzStr, iSize);
}
#endif
//--------------------------------------
