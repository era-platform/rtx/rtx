﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "stdafx.h"

#include "std_stream_base.h"

namespace rtl {
	//-----------------------------------------------
	//
	//-----------------------------------------------
	Stream::~Stream()
	{
	}
	void Stream::writeFormatArgs(const char* fmt, va_list args)
	{
		int writeSize = std_vsnprintf(nullptr, 0, fmt, args);

		if (writeSize > 0)
		{
			char* buff = (char*)MALLOC(writeSize + 1);

			writeSize = std_vsnprintf(buff, writeSize + 1, fmt, args);

			if (writeSize > 0)
				write(buff, writeSize);

			FREE(buff);
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void Stream::writeFormat(const char* fmt, ...)
	{
		va_list args;
		va_start(args, fmt);

		int writeSize = std_vsnprintf(nullptr, 0, fmt, args);

		if (writeSize > 0)
		{
			char* buff = (char*)MALLOC(writeSize + 1);

			writeSize = std_vsnprintf(buff, writeSize + 1, fmt, args);

			if (writeSize > 0)
				write(buff, writeSize);

			FREE(buff);
		}
	}
}
//-----------------------------------------------
