﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_event.h"

#if defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
namespace rtl
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	Event::Event()
	{
		m_autoreset = false;
		m_signaled = false;
		m_created = false;

		//	pthread_mutex_t m_mutex;
		//	pthread_cond_t m_cond;


	}
	bool Event::create(bool manual, bool signaled)
	{
		if (m_created)
			return true;

		m_autoreset = !manual;
		m_signaled = signaled;

		int res = pthread_mutex_init(&m_mutex, NULL);
		if (res != 0)
		{
			LOG_ERROR("evt", "event.event -- pthread_mutex_init() failed. error = %d.", res);
			return false;
		}

		res = pthread_cond_init(&m_cond, NULL);
		if (res != 0)
		{
			pthread_mutex_destroy(&m_mutex);
			LOG_ERROR("evt", "event.event -- pthread_cond_init() failed. error = %d.", res);
		}

		return (m_created = (res == 0));
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void Event::close()
	{
		if (!m_created)
			return;

		int res = pthread_cond_destroy(&m_cond);
		if (res != 0)
		{
			LOG_ERROR("evt", "event.event -- pthread_cond_destroy() failed. error = %d.", res);
		}

		res = pthread_mutex_destroy(&m_mutex);
		if (res != 0)
		{
			LOG_ERROR("evt", "event.~event -- pthread_mutex_destroy() failed. error = %d.", res);
		}

		m_created = false;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool Event::signal()
	{
		if (!m_created)
			return false;

		int res = pthread_mutex_lock(&m_mutex);
		if (res != 0)
		{
			LOG_ERROR("evt", "event.set -- pthread_mutex_lock() failed. error = %d.", res);
		}

		if (m_signaled)
		{
			res = pthread_mutex_unlock(&m_mutex);
			if (res != 0)
			{
				LOG_ERROR("evt", "event.set -- pthread_mutex_unlock() failed. error = %d.", res);
			}

			return true;
		}

		m_signaled = true;

		res = pthread_cond_broadcast(&m_cond);
		if (res != 0)
		{
			LOG_ERROR("evt", "event.set -- pthread_cond_broadcast() failed. error = %d.", res);
		}

		res = pthread_mutex_unlock(&m_mutex);
		if (res != 0)
		{
			LOG_ERROR("evt", "event.set -- pthread_mutex_unlock() failed. error = %d.", res);
		}

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool Event::reset()
	{
		if (!m_created)
			return false;

		int res = pthread_mutex_lock(&m_mutex);
		if (res != 0)
		{
			LOG_ERROR("evt", "event.reset -- pthread_mutex_lock() failed. error = %d.", res);
		}

		m_signaled = false;

		res = pthread_mutex_unlock(&m_mutex);
		if (res != 0)
		{
			LOG_ERROR("evt", "event.reset -- pthread_mutex_unlock() failed. error = %d.", res);
		}

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	/*bool Event::wait()
	{
		int res = pthread_mutex_lock(&m_mutex);
		if (res != 0)
		{
			LOG_ERROR("evt", "event.wait -- pthread_mutex_lock() failed. error = %d.", res);
		}

		bool result = true;

		while (!m_state)
		{
			res = pthread_cond_wait(&m_cond, &m_mutex);
			if (res != 0)
			{
				LOG_ERROR("evt", "event.wait -- pthread_cond_wait() failed. error = %d.", res);
				result = false;
				break;
			}
		}

		if (result && m_autoreset)
			m_state = false;

		res = pthread_mutex_unlock(&m_mutex);
		if (res != 0)
		{
			LOG_ERROR("evt", "event.wait -- pthread_mutex_unlock() failed. error = %d.", res);
		}

		return true;
	}*/
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool Event::wait(uint32_t timeout)
	{
		if (!m_created)
			return false;

		/*if (timeout == INFINITE)
			return wait();*/

		timeval tv;
		timespec abstime;
		int res;

		if (timeout != INFINITE)
		{
			res = gettimeofday(&tv, NULL);
			if (res != 0)
			{
				int err = errno;
				LOG_ERROR("evt", "event.wait -- gettimeofday() failed. error = %d.", err);
			}


			abstime.tv_sec = tv.tv_sec + timeout / 1000;
			abstime.tv_nsec = tv.tv_usec * 1000 + (timeout % 1000) * 1000000;
			if (abstime.tv_nsec >= 1000000000)
			{
				abstime.tv_nsec -= 1000000000;
				abstime.tv_sec++;
			}
		}

		bool result = true;

		res = pthread_mutex_lock(&m_mutex);
		if (res != 0)
		{
			LOG_ERROR("evt", "event.wait -- pthread_mutex_lock() failed. error = %d.", res);
		}

		while (!m_signaled)
		{
			if (timeout == INFINITE)
			{
				res = pthread_cond_wait(&m_cond, &m_mutex);
				if (res != 0)
				{
					LOG_ERROR("evt", "event.wait -- pthread_cond_wait() failed. error = %d.", res);
					result = false;
					break;
				}
			}
			else
			{
				res = pthread_cond_timedwait(&m_cond, &m_mutex, &abstime);

				if (res == ETIMEDOUT)
				{
					result = false;
					break;
				}

				if (res != 0)
				{
					LOG_ERROR("evt", "event.wait -- pthread_cond_timedwait() failed. error = %d.", res);
					result = false;
					break;
				}
			}
		}

		if (result && m_autoreset)
			m_signaled = false;

		res = pthread_mutex_unlock(&m_mutex);
		if (res != 0)
		{
			LOG_ERROR("evt", "event.wait -- pthread_mutex_unlock() failed. error = %d.", res);
		}

		return result;
	}
}
#endif

