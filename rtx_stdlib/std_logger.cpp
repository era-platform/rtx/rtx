﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "stdafx.h"

#include "std_logger.h"
#include "std_thread.h"
#include "std_filesystem.h"

//--------------------------------------
// Логи доступные всей системе
//--------------------------------------
RTX_STD_API rtl::Logger Log;
RTX_STD_API rtl::Logger Err;


namespace rtl
{
	/* linux error codes!
	#define	EPERM		 1	// Operation not permitted
	#define	ENOENT		 2	// No such file or directory
	#define	ESRCH		 3	// No such process
	#define	EINTR		 4	// Interrupted system call
	#define	EIO			 5	// I/O error
	#define	ENXIO		 6	// No such device or address
	#define	E2BIG		 7	// Argument list too long
	#define	ENOEXEC		 8	// Exec format error
	#define	EBADF		 9	// Bad file number
	#define	ECHILD		10	// No child processes
	#define	EAGAIN		11	// Try again
	#define	ENOMEM		12	// Out of memory
	#define	EACCES		13	// Permission denied
	#define	EFAULT		14	// Bad address
	#define	ENOTBLK		15	// Block device required
	#define	EBUSY		16	// Device or resource busy
	#define	EEXIST		17	// File exists
	#define	EXDEV		18	// Cross-device link
	#define	ENODEV		19	// No such device
	#define	ENOTDIR		20	// Not a directory
	#define	EISDIR		21	// Is a directory
	#define	EINVAL		22	// Invalid argument
	#define	ENFILE		23	// File table overflow
	#define	EMFILE		24	// Too many open files
	#define	ENOTTY		25	// Not a typewriter
	#define	ETXTBSY		26	// Text file busy
	#define	EFBIG		27	// File too large
	#define	ENOSPC		28	// No space left on device
	#define	ESPIPE		29	// Illegal seek
	#define	EROFS		30	// Read-only file system
	#define	EMLINK		31	// Too many links
	#define	EPIPE		32	// Broken pipe
	#define	EDOM		33	// Math argument out of domain of func
	#define	ERANGE		34	// Math result not representable
	*/
	const char* get_error_string(int lastError);
	//--------------------------------------
	//
	//--------------------------------------
	#define LOG_ONE_MB (1024 * 1024)
	#define LOG_TIMER_TICK (1000 * 60 * 60)
	//--------------------------------------
	//
	//--------------------------------------
	static const char* format_tag(char* buffer, int length, const char* text);
	static void log_check_flag(uint32_t* flags, const char* word);
	Mutex s_log_list_sync;
	ArrayT<Logger*> s_log_list;
	static void timer_check_log_expired_day(Timer* sender, uint32_t elapsed, void* user_data, int tid);
	static void async_check_log_expired_day(void* userData, uint16_t callId, uintptr_t intParam, void* ptrParam);
	static void add_log(Logger* log);
	static void remove_log(Logger* log);
	//-------------------------------------
	//
	//-------------------------------------
	uint64_t Logger::s_log_max_size = 10 * LOG_ONE_MB;
	uint64_t Logger::s_log_part_size = 10 * LOG_ONE_MB;
	uint64_t Logger::s_log_part_count = 10;
	uint32_t Logger::g_trace_flags = 0;
	//-------------------------------------
	//
	//-------------------------------------
	Logger::Logger(const Logger& log) :
		m_created(false),
		m_flags(0),
		m_utf8(nullptr),
		m_buffer(nullptr),
		m_buffer_length(0),
		m_log_written(0),
		m_part_number(0)
	{
		memset(m_folder, 0, sizeof m_folder);
		memset(m_log_name, 0, sizeof m_log_name);
	}
	//-------------------------------------
	//
	//-------------------------------------
	Logger& Logger::operator = (const Logger& log)
	{
		return *this;
	}
	//-------------------------------------
	//
	//-------------------------------------
	Logger::Logger() : m_created(false), m_flags(0), m_utf8(nullptr), m_buffer(nullptr), m_buffer_length(0), m_log_written(0), m_part_number(0)
	{
		// m_open_time already DateTime::now()
		memset(m_folder, 0, sizeof m_folder);
		memset(m_log_name, 0, sizeof m_log_name);
		m_buffer = (char*)MALLOC(LOG_BUFFER_SIZE * sizeof(char));
		memset(m_buffer, 0, LOG_BUFFER_SIZE * sizeof(char));
		m_utf8 = (char*)MALLOC(LOG_BUFFER_SIZE * 2);
		memset(m_utf8, 0, LOG_BUFFER_SIZE * 2);
	}
	//-------------------------------------
	//
	//-------------------------------------
	Logger::~Logger()
	{
		destroy();

		if (m_buffer != nullptr)
			FREE(m_buffer);

		if (m_utf8 != nullptr)
			FREE(m_utf8);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Logger::write_buffer()
	{
		if (!check_and_reopen_file(m_log_written + (m_buffer_length * sizeof(char))))
				return;

		if (m_buffer_length > 0 && m_out.isOpened())
		{
			m_log_written += m_out.write(m_buffer, m_buffer_length);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Logger::write_format(const char* tag, const char* type, const char* format, va_list argptr)
	{
		DateTime sysTime;

		MutexLock lock(m_sync);

		ThreadId thr_id = Thread::get_current_tid();

		if (type == nullptr || type[0] == 0)
		{
			m_buffer_length = std_snprintf(m_buffer, LOG_BUFFER_LEN, "%02u:%02u:%02u.%03u  %8u  %s -- ", sysTime.getHour(), sysTime.getMinute(), sysTime.setSecond(), sysTime.getMilliseconds(), thr_id, tag);
		}
		else
		{
			m_buffer_length = std_snprintf(m_buffer, LOG_BUFFER_LEN, "%02u:%02u:%02u.%03u  %8u  %s %s! ", sysTime.getHour(), sysTime.getMinute(), sysTime.setSecond(), sysTime.getMilliseconds(), thr_id, tag, type);
		}
		
	
		if (format != nullptr && format[0] != 0)
		{
			m_buffer_length += vsnprintf(m_buffer + m_buffer_length, LOG_BUFFER_LEN - m_buffer_length, format, argptr);
		}

		if (m_buffer_length < LOG_BUFFER_END)
		{
			m_buffer[m_buffer_length++] = '\r';
			m_buffer[m_buffer_length++] = '\n';
			m_buffer[m_buffer_length] = 0;
		}

		write_buffer();
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Logger::write_format_message(const char* tag, const char* type, int lastError, const char* format, va_list argptr)
	{
		DateTime sysTime;

		MutexLock lock(m_sync);

		ThreadId thr_id = Thread::get_current_tid();

		if (type == nullptr || type[0] == 0)
			m_buffer_length = std_snprintf(m_buffer, LOG_BUFFER_LEN, "%02u:%02u:%02u.%03u  %8u  %s -- ",
					sysTime.getHour(), sysTime.getMinute(), sysTime.setSecond(), sysTime.getMilliseconds(), thr_id, tag);
		else
			m_buffer_length = std_snprintf(m_buffer, LOG_BUFFER_LEN, "%02u:%02u:%02u.%03u  %8u  %s %s! ",
					sysTime.getHour(), sysTime.getMinute(), sysTime.setSecond(), sysTime.getMilliseconds(), thr_id, tag, type);
		
	
		if (format != nullptr && format[0] != 0)
		{
			m_buffer_length += vsnprintf(m_buffer + m_buffer_length, LOG_BUFFER_LEN - m_buffer_length, format, argptr);
		}

		if (lastError != 0)
		{
	#if defined (TARGET_OS_WINDOWS)
			char* errBuffer = nullptr;
		
			if (FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
				nullptr, lastError, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (char*)&errBuffer, 0, nullptr ) <= 0)
			{
				// Handle the error.
				m_buffer_length += std_snprintf(m_buffer + m_buffer_length, LOG_BUFFER_LEN - m_buffer_length, " (%d) no error information", lastError);
			}
			else
			{
				char* cp = strrchr(errBuffer, '\r');
				if (cp != nullptr)
					*cp = 0;
				m_buffer_length += std_snprintf(m_buffer + m_buffer_length, LOG_BUFFER_LEN - m_buffer_length, " (%d) '%s'", lastError, errBuffer);
			}

			if (errBuffer)
				LocalFree(errBuffer);
	#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
			const char* errBuffer = get_error_string(lastError);

			if (errBuffer == nullptr || errBuffer[0] == 0)
			{
				// Handle the error.
				m_buffer_length += std_snprintf(m_buffer + m_buffer_length, LOG_BUFFER_LEN - m_buffer_length, " (%d) no error information", lastError);
			}
			else
			{
				m_buffer_length += std_snprintf(m_buffer + m_buffer_length, LOG_BUFFER_LEN - m_buffer_length, " (%d) '%s'", lastError, errBuffer);
			}
	#endif
		}

		if (m_buffer_length < LOG_BUFFER_END)
		{
			m_buffer[m_buffer_length++] = '\r';
			m_buffer[m_buffer_length++] = '\n';
			m_buffer[m_buffer_length] = 0;
		}

		write_buffer();
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Logger::write_binary(const char* tag, const uint8_t* data, int datalen, const char* format, va_list argptr)
	{
		DateTime sysTime;
	
		MutexLock lock(m_sync);

		ThreadId thr_id = Thread::get_current_tid();

		m_buffer_length = std_snprintf(m_buffer, LOG_BUFFER_LEN, "%02u:%02u:%02u.%03u  %8u  %s -- ",
				sysTime.getHour(), sysTime.getMinute(), sysTime.setSecond(), sysTime.getMilliseconds(), thr_id, tag);

		if (format != nullptr && format[0] != 0)
		{
			m_buffer_length += vsnprintf(m_buffer + m_buffer_length, LOG_BUFFER_LEN - m_buffer_length, format, argptr);
		}
		else
		{
			m_buffer_length += std_snprintf(m_buffer + m_buffer_length, LOG_BUFFER_LEN - m_buffer_length, "Memory dump from: 0x%p dump size: %d bytes:", data, datalen);
		}

		if (m_buffer_length < LOG_BUFFER_END)
		{
			m_buffer[m_buffer_length++] = '\r';
			m_buffer[m_buffer_length++] = '\n';
			m_buffer[m_buffer_length] = 0;
		}

		write_buffer();

		if (data == nullptr)
			datalen = 0;

		// дамп двоичных данных
		//  0000: 00 01 02 03  04 05 06 07  08 09 0a 0b  0c 0d 0e 0f | ................
		//  0010: 10 11 12 13  14 15 16 17  18 19 1a 1b  1c 1d 1e 1f | ................
		// ...

		// write binary dump to output
		int rows = (datalen / 16) ;
		if (datalen % 16)
			rows ++;

		int j = 0;

		for (int i = 0; i < rows; i++)
		{
			m_buffer_length = std_snprintf(m_buffer, LOG_BUFFER_END, "  %08x: ", i * 16);
			for (j = 0; j < 16; j++)
			{
				int index = (i * 16) + j;

				uint8_t ch = data[index];

				if (m_buffer_length > LOG_BUFFER_END - 3)
					break;

				m_buffer[m_buffer_length++] = ' ';

				if (index < datalen)
				{
					uint8_t lt = ch & 0x0F;
					uint8_t ht = (ch & 0xF0) >> 4;
					
					if (!(j % 4) && j != 0)
					{
						m_buffer[m_buffer_length++] = ' ';
					}
			
					m_buffer[m_buffer_length++] = (char)(ht > 9 ? ht - 10 + 'a' : ht + '0');
					m_buffer[m_buffer_length++] = (char)(lt > 9 ? lt - 10 + 'a' : lt + '0');
				}
				else
				{
					if (!(j % 4) && j != 0)
					{
						m_buffer[m_buffer_length++] = ' ';
					}

					m_buffer[m_buffer_length++] = ' ';
					m_buffer[m_buffer_length++] = ' ';
				}
			}

			m_buffer[m_buffer_length++] = ' ';
			m_buffer[m_buffer_length++] = '|';
			m_buffer[m_buffer_length++] = ' ';

			for (j = 0; j < 16; j++)
			{
				int index = (i * 16) + j;
				if (index >= datalen)
					break;
				uint8_t ch = data[index];
				if (m_buffer_length > LOG_BUFFER_END - 3)
					break;

				if (ch < 32 || ch == 127 || ch == 255)
					m_buffer[m_buffer_length++] = '.';
				else
					m_buffer[m_buffer_length++] = ch;
			}

			m_buffer[m_buffer_length++] = '\r';
			m_buffer[m_buffer_length++] = '\n';
			m_buffer[m_buffer_length] = 0;

			write_buffer();
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Logger::open_file()
	{
		close_file();

		char name[MAX_PATH];
	
		m_open_time = DateTime::now();

		generate_file_name(name, MAX_PATH);

		fs_check_path_and_create(name);

		if (m_out.create_or_append(name))
		{
			char head[MAX_PATH];
			int len = std_snprintf(head, MAX_PATH, "---------- Log started  at %04u.%02u.%02u %02u:%02u:%02u ----------\n",
				m_open_time.getYear(),
				m_open_time.getMonth(),
				m_open_time.getDay(),
				m_open_time.getHour(),
				m_open_time.getMinute(),
				m_open_time.setSecond());
		
			head[len] = 0;
			size_t written = m_out.write(head, len);
			m_log_written += written;

			return true;
		}

		return false;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Logger::close_file()
	{
		if (!m_created)
			return;

		if (m_out.isOpened())
		{
			DateTime st;

			char tail[MAX_PATH];
			int tail_length = std_snprintf(tail, MAX_PATH, "---------- Log closed at %04u.%02u.%02u %02u:%02u:%02u ----------\n",
				st.getYear(),
				st.getMonth(),
				st.getDay(),
				st.getHour(),
				st.getMinute(),
				st.setSecond());

			tail[tail_length] = 0;
			m_out.write(tail, tail_length);

			m_out.close();
		}
	}
	//--------------------------------------
	// if no files found returns false
	//--------------------------------------
	bool Logger::get_existing_filename(const char* fmask, int* fpart, uint64_t* filesize, char* fname_buffer, int size)
	{
		ArrayT<fs_entry_info_t> file_list;

		fs_directory_get_entries(fmask, file_list);

		int start_part_no = *fpart;
		int part_no = 0;
		size_t fname_offset, fname_len, mask_offset;

		const char* cp = strrchr(fmask, FS_PATH_DELIMITER);
	
		fname_offset = (cp != nullptr) ? (cp - fmask) + 1 : 0;
		mask_offset = strlen(fmask) - 5;
		fname_len = mask_offset - fname_offset;

		bool original_found = false;
		uint64_t original_size = 0;
		bool parts_found = false;
		uint64_t part_size = 0;
		bool free_parts = false;
		uint64_t free_part_size = 0;
		int free_part_no = 0;

		for (int i = 0; i < file_list.getCount(); i++)
		{
			fs_entry_info_t& fs_entry = file_list[i];

			if (fs_entry.fs_directory)
				continue;

			if (*(fs_entry.fs_filename + fname_len) != L'.')
			{
				if (s_log_part_size == 0)
					continue;

				// part file found
				if (strncmp(fs_entry.fs_filename + fname_len, "_p", 2) != 0)
					continue;

				if ((part_no = atoi(fs_entry.fs_filename + (fname_len + 2))) >= *fpart)
				{
					parts_found = true;

					*fpart = part_no;
					part_size = fs_entry.fs_filesize;

					if (s_log_part_count == 0)
					{
						strncpy(fname_buffer, fs_entry.fs_filename, size);
						fname_buffer[size - 1] = 0;
					}
					else
					{
						if (uint64_t(part_no) >= s_log_part_count)
						{
							continue;
						}

						if (part_size >= s_log_part_size - 1024)
						{
							continue;
						}

						free_parts = true;
						free_part_size = fs_entry.fs_filesize;
						free_part_no = part_no;

						strncpy(fname_buffer, fs_entry.fs_filename, size);
						fname_buffer[size - 1] = 0;
					}
				}
			}
			else
			{
				original_found = true;
				original_size = fs_entry.fs_filesize;
			}
		}

		if (parts_found)
		{
			if (s_log_part_count == 0)
			{
				// проверяем размер
				if (part_size >= s_log_part_size - 1024)
				{
					int inert = *fpart;
					inert++;
					*fpart = inert;
					fname_buffer[0] = 0;
					return false;
				}

				*filesize = part_size;
				return true;
			}
			else
			{
				if (free_parts)
				{
					*fpart = free_part_no;
					*filesize = free_part_size;
					return true;
				}
			
				*fpart = start_part_no;
				fname_buffer[0] = 0;
				return false;
			}
		}

		// используем оригинальное имя
		fname_buffer[0] = 0;

		if (original_found)
		{
			if (original_size >= s_log_part_size - 1024)
			{
				int inert = *fpart;
				if ((s_log_part_count != 0) && (uint64_t(inert) >= s_log_part_count))
				{
					inert = 0;
				}
				// используем новое имя
				*fpart = inert;
			}

			*filesize = original_size;
		}

		// оригинальный файл
		return false;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Logger::generate_file_name(char* fpath, int size)
	{
		char part_name[32];
		char pre_part_name[32];
		char dir[MAX_PATH];
		char fdir[MAX_PATH];
		char fmask[MAX_PATH];
		char fname[MAX_PATH];

	#if defined (TARGET_OS_WINDOWS)
	#define LOG_PATH_TEMPLATE_1 "%s\\%04u-%02u-%02u\\%s*.log"
	#define LOG_PATH_TEMPLATE_2 "%04u-%02u-%02u\\%s*.log"
	#define LOG_PATH_TEMPLATE_3 "%s\\%s_%04u-%02u-%02u*.log"
	#define LOG_PATH_TEMPLATE_4 "%s_%04u-%02u-%02u*.log"
	#define LOG_PATH_TEMPLATE_5	"%s\\%04u-%02u-%02u\\%s"
	#define LOG_PATH_TEMPLATE_6 "%04u-%02u-%02u\\%s"
	#define LOG_PATH_TEMPLATE_7 "%s\\%s"
	#define LOG_PATH_TEMPLATE_8 	"%s\\%04u-%02u-%02u\\%s%s.log"
	#define LOG_PATH_TEMPLATE_9 	"%04u-%02u-%02u\\%s%s.log"
	#define LOG_PATH_TEMPLATE_10	"%s\\%s_%04u-%02u-%02u%s.log"
	#define LOG_PATH_TEMPLATE_11	"%s_%04u-%02u-%02u%s.log"
	#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	#define LOG_PATH_TEMPLATE_1 	"%s/%04u-%02u-%02u/%s*.log"
	#define LOG_PATH_TEMPLATE_2 	"%04u-%02u-%02u/%s*.log"
	#define LOG_PATH_TEMPLATE_3 	"%s/%s_%04u-%02u-%02u*.log"
	#define LOG_PATH_TEMPLATE_4 	"%s_%04u-%02u-%02u*.log"
	#define LOG_PATH_TEMPLATE_5		"%s/%04u-%02u-%02u/%s"
	#define LOG_PATH_TEMPLATE_6 	"%04u-%02u-%02u/%s"
	#define LOG_PATH_TEMPLATE_7 	"%s/%s"
	#define LOG_PATH_TEMPLATE_8 	"%s/%04u-%02u-%02u/%s%s.log"
	#define LOG_PATH_TEMPLATE_9 	"%04u-%02u-%02u/%s%s.log"
	#define LOG_PATH_TEMPLATE_10	"%s/%s_%04u-%02u-%02u%s.log"
	#define LOG_PATH_TEMPLATE_11	"%s_%04u-%02u-%02u%s.log"
	#endif

		memcpy(dir, m_folder, sizeof(m_folder));

		if (m_flags & LOG_CREATE_TIMEDIR)
		{
			if (m_folder[0] != 0)
				std_snprintf(fmask, MAX_PATH, LOG_PATH_TEMPLATE_1, m_folder, m_open_time.getYear(), m_open_time.getMonth(), m_open_time.getDay(), m_log_name);
			else
				std_snprintf(fmask, MAX_PATH, LOG_PATH_TEMPLATE_2, m_open_time.getYear(), m_open_time.getMonth(), m_open_time.getDay(), m_log_name);
		}
		else
		{
			if (m_folder[0] != 0)
				std_snprintf(fmask, MAX_PATH, LOG_PATH_TEMPLATE_3, m_folder, m_log_name, m_open_time.getYear(), m_open_time.getMonth(), m_open_time.getDay());
			else
				std_snprintf(fmask, MAX_PATH, LOG_PATH_TEMPLATE_4, m_log_name, m_open_time.getYear(), m_open_time.getMonth(), m_open_time.getDay());
		}

		/// check directory
		if (get_existing_filename(fmask, (int*)&m_part_number, (uint64_t*)&m_log_written, fname, size))
		{
			if (m_flags & LOG_CREATE_TIMEDIR)
			{
				if (m_folder[0] != 0)
					std_snprintf(fpath, MAX_PATH, LOG_PATH_TEMPLATE_5, m_folder, m_open_time.getYear(), m_open_time.getMonth(), m_open_time.getDay(), fname);
				else
					std_snprintf(fpath, MAX_PATH, LOG_PATH_TEMPLATE_6, m_open_time.getYear(), m_open_time.getMonth(), m_open_time.getDay(), fname);
			}
			else
			{
				if (m_folder[0] != 0)
					std_snprintf(fpath, MAX_PATH, LOG_PATH_TEMPLATE_7, m_folder, fname);
				else
					strncpy(fpath, fname, MAX_PATH);
			}

			return;
		}

		if (m_part_number == 0)
		{
			memset(part_name, 0, sizeof pre_part_name);
			memset(pre_part_name, 0, sizeof pre_part_name);
		}
		else
		{
			// начинаем новый файл
			std_snprintf(part_name, 32, "_p%02d(%02d-%02d)", m_part_number, m_open_time.getHour(), m_open_time.getMinute());
			std_snprintf(pre_part_name, 32, "_p%02d*", m_part_number);
			m_log_written = 0;
		}

		if (s_log_part_count != 0)
		{
			if (m_flags & LOG_CREATE_TIMEDIR)
			{
				// 
				if (m_folder[0] != 0)
					std_snprintf(fmask, MAX_PATH, LOG_PATH_TEMPLATE_8, m_folder, m_open_time.getYear(), m_open_time.getMonth(), m_open_time.getDay(), m_log_name, pre_part_name);
				else
					std_snprintf(fmask, MAX_PATH, LOG_PATH_TEMPLATE_9, m_open_time.getYear(), m_open_time.getMonth(), m_open_time.getDay(), m_log_name, pre_part_name);
			}
			else
			{
				if (m_folder[0] != 0)
					std_snprintf(fmask, MAX_PATH, LOG_PATH_TEMPLATE_10, m_folder, m_log_name, m_open_time.getYear(), m_open_time.getMonth(), m_open_time.getDay(), pre_part_name);
				else
					std_snprintf(fmask, MAX_PATH, LOG_PATH_TEMPLATE_11, m_log_name, m_open_time.getYear(), m_open_time.getMonth(), m_open_time.getDay(), pre_part_name);
			}

			ArrayT<fs_entry_info_t> file_list;

			fs_directory_get_entries(fmask, file_list);

			for (int i = 0; i < file_list.getCount(); i++)
			{
				fs_entry_info_t& fs_entry = file_list[i];

				if (fs_entry.fs_directory)
					continue;

				if (fs_entry.fs_filesize >= s_log_part_size - 1024)
				{
					if (dir[0] == 0)
					{
						fs_delete_file(fs_entry.fs_filename);
					}
					else
					{
						::sprintf(fdir, LOG_PATH_TEMPLATE_7, dir, fs_entry.fs_filename);
						fs_delete_file(fdir);
					}
				
					m_log_written = 0;
				}
			}
		}

		if (m_flags & LOG_CREATE_TIMEDIR)
		{
			// имя файла
			if (m_folder[0] != 0)
				std_snprintf(fpath, MAX_PATH, LOG_PATH_TEMPLATE_8, m_folder, m_open_time.getYear(), m_open_time.getMonth(), m_open_time.getDay(), m_log_name, part_name);
			else
				std_snprintf(fpath, MAX_PATH, LOG_PATH_TEMPLATE_9, m_open_time.getYear(), m_open_time.getMonth(), m_open_time.getDay(), m_log_name, part_name);
		}
		else
		{
			if (m_folder[0] != 0)
				std_snprintf(fpath, MAX_PATH, LOG_PATH_TEMPLATE_10, m_folder, m_log_name, m_open_time.getYear(), m_open_time.getMonth(), m_open_time.getDay(), part_name);
			else
				std_snprintf(fpath, MAX_PATH, LOG_PATH_TEMPLATE_11, m_log_name, m_open_time.getYear(), m_open_time.getMonth(), m_open_time.getDay(), part_name);
		}

	}
	//--------------------------------------
	// если фальш то ничего не пишем
	//--------------------------------------
	bool Logger::check_and_reopen_file(uint64_t file_size)
	{
		if (!m_out.isOpened())
			return open_file();

		if (s_log_part_size != 0 && file_size >= s_log_part_size)
		{
			// переоткрываем файл
			m_part_number++;
			return open_file();
		}

		DateTime local_time;

		if (m_open_time.getDay() != local_time.getDay() || m_open_time.getMonth() != local_time.getMonth())
		{
			m_part_number = 0;
			m_log_written = 0;

			return open_file();
		}

		if (s_log_part_size == 0 && file_size >= s_log_max_size)
		{
			return false;
		}

		// file ready to write data
		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Logger::check_log_expired_day(const DateTime& current_time)
	{
		MutexLock lock(m_sync);

		if (!m_created || !m_out.isOpened())
			return;

		// проверяем переход на следующий день. достаточно сравнить день месяца
		if (current_time.getDay() != m_open_time.getDay())
		{
			Log.log("log", "Logger::timer_event -> close log %s by timer", m_log_name);
			close_file();
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	static void add_log(Logger* log)
	{
		MutexLock lock(s_log_list_sync);

		Log.log("log", "Logger::add_log %p %s", log, log->getName());

		s_log_list.add(log);

		if (s_log_list.getCount() == 1)
		{
			Log.log("log", "Logger::start_timer()");
			Timer::setTimer(timer_check_log_expired_day, nullptr, 0, LOG_TIMER_TICK, true);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void remove_log(Logger* log)
	{
		MutexLock lock(s_log_list_sync);

		Log.log("log", "Logger::remove_log %p %s", log, log->getName());

		for (int i = 0; i < s_log_list.getCount(); i++)
		{
			if (s_log_list[i] == log)
			{
				s_log_list.removeAt(i);
				break;
			}
		}

		if (s_log_list.getCount() == 0)
		{
			Log.log("log", "Logger::stop_timer()");
			Timer::stopTimer(timer_check_log_expired_day, nullptr, 0);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void timer_check_log_expired_day(Timer* sender, uint32_t elapsed, void* user_data, int tid)
	{
		async_call_manager_t::callAsync(async_check_log_expired_day, nullptr, 0, elapsed, nullptr);
	}
	//--------------------------------------
	//
	//--------------------------------------
	static void async_check_log_expired_day(void* userData, uint16_t callId, uintptr_t intParam, void* ptrParam)
	{
		MutexLock lock(s_log_list_sync);

		Log.log("log", "Logger::timer_event");

		DateTime now = DateTime::now();

		for (int i = 0; i < s_log_list.getCount(); i++)
		{
			Logger* log = s_log_list.getAt(i);
			if (log == nullptr)
			{
				continue;
			}
			log->check_log_expired_day(now);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Logger::create(const char* folder, const char* logName, uint32_t flags)
	{
		if (m_created)
			return;

		if (folder != nullptr && folder[0] != 0)
			strncpy(m_folder, folder, MAX_PATH);
		else
			m_folder[0] = 0;

		if (logName != nullptr && logName[0] != 0)
			strncpy(m_log_name, logName, LOG_MAXNAME);
		else
			strcpy(m_log_name, "at");

		m_flags = flags;
		m_created = true;

		add_log(this);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Logger::destroy()
	{
		remove_log(this);

		MutexLock lock(m_sync);

		if (m_created)
		{
			close_file();
		}
	
		m_created = false;
	}
	//--------------------------------------
	// типизированный вывод ошибок
	//--------------------------------------
	void Logger::log_error(const char* tag, const char* format, ...)
	{
		if (!m_created)
			return;

		va_list args;
		va_start(args, format);

		char buffer[9];
		write_format(format_tag(buffer, 8, tag), "E", format, args);
	}
	//--------------------------------------
	// типизированный вывод системных ошибок с описанием
	//--------------------------------------
	void Logger::log_error_message(const char* tag, int last_error, const char* format, ...)
	{
		if (!m_created)
			return;

		va_list args;
		va_start(args, format);

		char buffer[9];
		write_format_message(format_tag(buffer, 8, tag), "E", last_error, format, args);
	}
	//--------------------------------------
	// типизированный вывод ошибок?
	//--------------------------------------
	void Logger::log_error_formatted(const char* tag, const char* message, const char* format, va_list argptr)
	{
		if (!m_created)
			return;

		char buffer[9];
		write_format(format_tag(buffer, 8, tag), message, format, argptr);
	}
	//--------------------------------------
	// типизированный вывод сообщений
	//--------------------------------------
	void Logger::log(const char* tag, const char*  format, ...)
	{
		if (!m_created)
			return;

		va_list args;
		va_start(args, format);

		char buffer[9];
		write_format(format_tag(buffer, 8, tag), nullptr, format, args);
	}
	//--------------------------------------
	// типизированный вывод событий
	//--------------------------------------
	void Logger::log_event(const char* tag, const char* format, ...)
	{
		if (!m_created)
			return;

		va_list args;
		va_start(args, format);

		char buffer[9];
		write_format(format_tag(buffer, 8, tag), "-----------> ", format, args);
	}
	//--------------------------------------
	// типизированный вывод предупреждений
	//--------------------------------------
	void Logger::log_warning(const char* tag, const char* format, ...)
	{
		if (!m_created)
			return;

		va_list args;
		va_start(args, format);

		char buffer[9];
		write_format(format_tag(buffer, 8, tag), "W", format, args);
	}
	//--------------------------------------
	// типизированный вывод бинарных данных
	//--------------------------------------
	void Logger::log_binary(const char* tag, const uint8_t* data, int data_len, const char* format, ...)
	{
		if (!m_created)
			return;

		va_list args;
		va_start(args, format);

		char buffer[9];
		write_binary(format_tag(buffer, 8, tag), data, data_len, format, args);
	}
	//--------------------------------------
	// типизированный вывод сообщений с параметрами
	//--------------------------------------
	void Logger::log_format(const char* tag, const char* format, va_list argptr)
	{
		if (!m_created)
			return;

		char buffer[9];
		write_format(format_tag(buffer, 8, tag), nullptr, format, argptr);
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API void Logger::set_log_sizes(uint64_t max_log_size, uint64_t max_part_size, uint64_t max_part_count)
	{
		s_log_max_size = (max_log_size <= 0) ? 10 * LOG_ONE_MB : max_log_size * LOG_ONE_MB;
		s_log_part_size = (max_part_size <= 0) ? 10 * LOG_ONE_MB : max_part_size * LOG_ONE_MB;
		s_log_part_count = (max_part_count <= 0) ? 10 : max_part_count;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API int Logger::get_log_max_size()
	{
		return (int)(s_log_max_size / LOG_ONE_MB);
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API int Logger::get_log_part_size()
	{
		return (int)(s_log_part_size / LOG_ONE_MB);
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API int Logger::get_log_part_count()
	{
		return (int)(s_log_part_count);
	}
	//--------------------------------------
	// считывание флагов вида "1001010011100" или "NET IP SESSION INFO CALL MEDIA STREAM MAIN TRUNK TRANSACTION PROTO"
	//--------------------------------------
	RTX_STD_API uint32_t Logger::set_trace_flags(const char* str_flags)
	{
		if (str_flags == nullptr || str_flags[0] == 0)
			return g_trace_flags = 0;

		uint32_t flags = 0;
		char word[128];

		// проверим если битовый набор то сюда
		// ищем только нули и единицы которые разделены пробелами табами или запятыми.
		// если встречается любой другой символ то прекращаем обработку
		// биты идут -- слева старший с права младший пример '1100 0011' это С3
		if (str_flags[0] == '0' || str_flags[0] == '1')
		{
			const char* ch = str_flags;
			uint32_t bit_pos = 31, mask = 1;
			bool stop = false;

			while (*ch != 0)
			{
				switch (*ch++)
				{
				case '1':
					flags |= mask << bit_pos;
				case '0':
					bit_pos--;
					break;
				case ' ':
				case '\t':
				case ',':
					break;
				default:
					stop = true;
					break;
				}

				if (stop)
					break;
			}

			if (bit_pos > 0 && flags != 0)
				flags >>= (bit_pos+1); // bit_pos был индексом теперь количество
		}
		else // иначе именованный набор флагов
		{
			const char* cp = str_flags, *cpp;

			while ((cpp = strpbrk(cp, " \t,")) != nullptr)
			{
				size_t len = cpp - cp;

				if (len < 127)
				{
					strncpy(word, cp, len);
					word[len] = 0;

					log_check_flag(&flags, word);
				}

				while (*cpp != 0 && (*cpp == ' ' || *cpp == ',' || *cpp == '\t')) cpp++;

				cp = cpp;
			}

			size_t len = strlen(cp);

			if (len < 127)
			{
				strncpy(word, cp, len);
				word[len] = 0;
				log_check_flag(&flags, word);
			}
		}

		return g_trace_flags = flags;
	}
	//--------------------------------------
	// считывание флагов вида "1001010011100" или "NET IP SESSION INFO CALL MEDIA STREAM MAIN TRUNK TRANSACTION PROTO"
	//--------------------------------------
	RTX_STD_API bool Logger::check_trace(uint32_t flags)
	{
		return (g_trace_flags & flags) == flags;
	}
	//--------------------------------------
	// запись вида "1001010101" или "NET FLAG9 VOICE"
	//--------------------------------------
	struct log_flag_name_t
	{
		const char* name;
		uint32_t value;
	};
	//--------------------------------------
	// запись вида "1001010101" или "NET FLAG9 VOICE"
	//--------------------------------------
	const char* Logger::trace_to_string(char* buffer, int size, bool bits)
	{
		static log_flag_name_t flags[] =
		{
			{ "CALL", 4 },		{ "EVENT", 5 },  	{ "MEDIA-FLOW", 10 },	{ "NET", 3 },
			{ "VAD", 3 },		{ "PROTO", 5 },		{ "TIMER", 5 },			{ "SESSION", 7 },
			{ "TRANS", 5 },		{ "TRUNK", 5 },		{ "STREAM", 6 },		{ "WARNING", 7 },
			{ "ERROR", 5 },	    { "FLAG1", 5 },		{ "FLAG2", 5 },			{ "FLAG3", 5 },
			{ "FLAG4", 5 },		{ "FLAG5", 5 },		{ "FLAG6", 5 },			{ "FLAG7", 5 },
			{ "FLAG8", 5 },		{ "FLAG9", 5 },		{ "FLAG10", 6 },		{ "STAT", 6 },
			{ "FLAG12", 6 },	{ "FLAG13", 6 },	{ "FLAG14", 6 },		{ "RTP-FLOW", 8 },
			{ "BANNED", 6 },	{ "RTP", 3 },		{ "ASYNC", 5 },			{ "FAX", 3 }
		};

		if (buffer == nullptr || size <= 32)
			return "";

		buffer[0] = 0;

		char* p = buffer;

		if (bits)
		{
			uint32_t mask = 1;

			for (int i = 0; i < 32; i++)
			{
				if (g_trace_flags & mask)
				{
					p[i] = '1';
				}
				else
				{
					p[i] = '0';
				}

				mask <<= 1;
			}

			p[31] = 0;

			// сзади затираем нули	

			for (int i = 31; i >= 0; i--)
			{
				if (buffer[i] == '1')
					break;
				buffer[i] = 0;
			}
		}
		else
		{
			uint32_t	mask = 1;
			int len = 0;

			for (int i = 0; i < 32; i++)
			{
				if (g_trace_flags & mask)
				{
					len += flags[i].value + 1;

					if (len >= size)
						return buffer;

					*p++ = ' ';
					strcpy(p, flags[i].name);
					p += flags[i].value;
				}

				mask <<= 1;
			}
		}

		return buffer;
	}
	//-------------------------------------
	// запись в буфер с заполнением пробелами остатка
	//-------------------------------------
	static const char* format_tag(char* buffer, int length, const char* text)
	{
		size_t text_length = strlen(text);
		size_t buffer_length = text_length < (size_t)length ? text_length : length;
	
		strncpy(buffer, text, buffer_length);
	
		if (buffer_length < (size_t)length)
			memset (buffer + buffer_length, ' ', length - buffer_length);
	
		buffer[length] = 0;

		return buffer;
	}
	//--------------------------------------
	// парсинг флагов логирования
	//--------------------------------------
	static void log_check_flag(uint32_t* flags, const char* word)
	{
		static log_flag_name_t s_named_flags[] =
		{
			{ "ASYNC",		(uint32_t)TRF_ASYNC		},		// 31
			{ "BANNED",		(uint32_t)TRF_BANNED  	},		// 29
			{ "CALL",		(uint32_t)TRF_CALL		},		// 1
			{ "ERROR",		(uint32_t)TRF_ERRORS		},		// 13
			{ "EVENT",		(uint32_t)TRF_EVENTS		},		// 2
			{ "FAX",		(uint32_t)TRF_FAX				},		// 32
			{ "FLAG1",		(uint32_t)TRF_FLAG1		},		// 14
			{ "FLAG2",		(uint32_t)TRF_FLAG2		},		// 15
			{ "FLAG3",		(uint32_t)TRF_FLAG3		},		// 16
			{ "FLAG4",		(uint32_t)TRF_FLAG4		},		// 17
			{ "FLAG5",		(uint32_t)TRF_FLAG5		},		// 18
			{ "FLAG6",		(uint32_t)TRF_FLAG6		},		// 19
			{ "FLAG7",		(uint32_t)TRF_FLAG7		},		// 20
			{ "FLAG10",		(uint32_t)TRF_FLAG10		},		// 23
			{ "FLAG12",		(uint32_t)TRF_FLAG12		},		// 25
			{ "FLAG13",		(uint32_t)TRF_FLAG13		},		// 26
			{ "FLAG14",		(uint32_t)TRF_FLAG14		},		// 27
			{ "MEDIA-FLOW",	(uint32_t)TRF_MEDIA_FLOW	},		// 4
			{ "MIXER",		(uint32_t)TRF_MIXER		},		// 21
			{ "MQ",			(uint32_t)TRF_MQ		},		// 22
			{ "NET",		(uint32_t)TRF_NET			},		// 4
			{ "PROTO",		(uint32_t)TRF_PROTO		},		// 6
			{ "RTP",		(uint32_t)TRF_RTP			},		// 30
			{ "RTP-FLOW",	(uint32_t)TRF_RTP_FLOW	},		// 28
			{ "SESSION",	(uint32_t)TRF_SESSION 	},		// 8
			{ "STAT",		(uint32_t)TRF_STAT		},		// 24
			{ "STREAM",		(uint32_t)TRF_STREAM		},		// 11
			{ "TIMER",		(uint32_t)TRF_TIMER		},		// 7
			{ "TRANS",		(uint32_t)TRF_TRANS		},		// 9
			{ "TRUNK",		(uint32_t)TRF_TRUNK		},		// 10
			{ "VAD",		(uint32_t)TRF_VAD		},		// 5
			{ "WARNING",	(uint32_t)TRF_WARNING		},		// 12
		};

		for (int i = 0; i < (int)(sizeof(s_named_flags) / sizeof(log_flag_name_t)); i++)
		{
			log_flag_name_t& result = s_named_flags[i];
			if (std_stricmp(result.name, word) == 0)
			{
				*flags |= result.value;
				return;
			}
		}
	}
	/* linux error codes!
	#define	EPERM		 1	// Operation not permitted
	#define	ENOENT		 2	// No such file or directory
	#define	ESRCH		 3	// No such process
	#define	EINTR		 4	// Interrupted system call
	#define	EIO			 5	// I/O error
	#define	ENXIO		 6	// No such device or address
	#define	E2BIG		 7	// Argument list too long
	#define	ENOEXEC		 8	// Exec format error
	#define	EBADF		 9	// Bad file number
	#define	ECHILD		10	// No child processes
	#define	EAGAIN		11	// Try again
	#define	ENOMEM		12	// Out of memory
	#define	EACCES		13	// Permission denied
	#define	EFAULT		14	// Bad address
	#define	ENOTBLK		15	// Block device required
	#define	EBUSY		16	// Device or resource busy
	#define	EEXIST		17	// File exists
	#define	EXDEV		18	// Cross-device link
	#define	ENODEV		19	// No such device
	#define	ENOTDIR		20	// Not a directory
	#define	EISDIR		21	// Is a directory
	#define	EINVAL		22	// Invalid argument
	#define	ENFILE		23	// File table overflow
	#define	EMFILE		24	// Too many open files
	#define	ENOTTY		25	// Not a typewriter
	#define	ETXTBSY		26	// Text file busy
	#define	EFBIG		27	// File too large
	#define	ENOSPC		28	// No space left on device
	#define	ESPIPE		29	// Illegal seek
	#define	EROFS		30	// Read-only file system
	#define	EMLINK		31	// Too many links
	#define	EPIPE		32	// Broken pipe
	#define	EDOM		33	// Math argument out of domain of func
	#define	ERANGE		34	// Math result not representable
	*/
	const char* get_error_string(int lastError)
	{
		static const char* error_names[] = {
			"No error",
			"Operation not permitted",
			"No such file or directory",
			"No such process",
			"Interrupted system call",
			"I/O error",
			"No such device or address",
			"Argument list too long",
			"Exec format error",
			"Bad file number",
			"No child processes",
			"Try again",
			"Out of memory",
			"Permission denied",
			"Bad address",
			"Block device required",
			"Device or resource busy",
			"File exists",
			"Cross-device link",
			"No such device",
			"Not a directory",
			"Is a directory",
			"Invalid argument",
			"File table overflow",
			"Too many open files",
			"Not a typewriter",
			"Text file busy",
			"File too large",
			"No space left on device",
			"Illegal seek",
			"Read-only file system",
			"Too many links",
			"Broken pipe",
			"Math argument out of domain of func",
			"Math result not representable"
		};

		return lastError > 0 && lastError <= 34 ? error_names[lastError] : nullptr;
	}
}
//--------------------------------------
