﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "stdafx.h"
#include <ctype.h>
#include "std_sys_env.h"
#include "whereami.c"
//--------------------------------------
//
//--------------------------------------
uint32_t std_get_processors_count()
{
	uint32_t count = 0;

#if defined(TARGET_OS_WINDOWS)
	SYSTEM_INFO si;
	GetSystemInfo(&si);
	count = uint32_t(si.dwNumberOfProcessors);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	int res = sysconf(_SC_NPROCESSORS_ONLN);
	if (res <= 0)
	{
		LOG_WARN("sys-env", "environment.get_processors_count -- sysconf() returned %d.", res);
		count = 1;
	}
	else
	{
		count = res;
	}
#endif

	return count;
}
//--------------------------------------
//
//--------------------------------------
bool std_get_application_startup_path(char* buffer, int size) // rtl::String
{
	bool result = false;

#if defined(TARGET_OS_WINDOWS)
	DWORD res = GetModuleFileNameA(NULL, buffer, size);
	if (res != 0)
	{
		if (res == size)
			res--;

		for (int i = res; i >= 0; --i)
		{
			if (buffer[i] == '\\')
			{
				buffer[i] = 0;
				break;
			}
		}

		result = true;
	}
	else
	{
		DWORD gle = GetLastError();
		LOG_ERROR("env", "environment.get_application_startup_path -- GetModuleFileNameA() failed. error = %u.", gle);
		buffer[0] = 0;
	}
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
    int dirname_length;
	int res = wai_getExecutablePath(buffer, size, &dirname_length);
	if (res > 0)
	{
        if (dirname_length < size)
        {
            buffer[dirname_length+1] = 0;
            result = true;
        }
	}
	else if (res < 0)
	{
		int err = errno;
		LOG_ERROR("sys-env", "environment.get_application_startup_path -- wai_getExecutablePath() failed. error = %u.", err);
		buffer[0] = 0;
	}
#endif

	return result;
}
//--------------------------------------
//
//--------------------------------------
RTX_STD_API guid_t std_generate_guid()
{
	guid_t guid = {0x55DD55DD, 0x55DD, 0x55DD,
		       0x55, 0xDD, 0x55, 0xDD, 0x55, 0xDD, 0x55, 0xDD};
#if defined(TARGET_OS_FREEBSD)
    uint32_t status;
#endif
#if defined(TARGET_OS_WINDOWS)
	CoCreateGuid((GUID*)&guid);
#elif defined(TARGET_OS_LINUX)
	uuid_generate((unsigned char*)(&guid));
#elif defined(TARGET_OS_FREEBSD)
	uuid_create((uuid_t*)(&guid), &status);
    // XXX no status check!!!
    // if (status != uuid_s_ok) { WHAT_TO_DO? crash? };
#endif
	return guid;
}
//--------------------------------------
//
//--------------------------------------
static uint8_t ch2byte(char ch)
{
	uint8_t res = 0;

	if (ch >= '0' && ch <= '9')
		res = ch - '0';
	if (ch >= 'A' && ch <= 'Z')
		res = (ch - 'A') + 10;
	if (ch >= 'a' && ch <= 'z')
		res = (ch - 'a') + 10;

	return res;
}
//--------------------------------------
//
//--------------------------------------
RTX_STD_API bool std_parse_guid(guid_t& guid, const char* guid_str, int length)
{
	if (guid_str == nullptr || guid_str[0] == 0)
	{
		memset(&guid, 0, sizeof(guid_t));
		return false;
	}

	if (length < 0)
		length = std_strlen(guid_str);

	//  format string presentation
	// {01234567-8901-2345-6789-012345678901}

	//  GUID structure
	//  01234567 8901 2345 93995738F7DBDECF

	//  GUID string presentation
	//  01234567 8901 2345 6789 012345678901

	//  Sample
	//  E0A59057 B084 4F3D 93995738F7DBDECF
	//  E0A59057-B084-4F3D-9399-5738F7DBDECF
	// {E0A59057-B084-4F3D-9399-5738F7DBDECF}

	if (length < 32)
		return false;

	const char* cp = guid_str;
	int idx = 0, idx1 = 3, idx2 = 1;
	memset(&guid, 0, sizeof guid);
	// пройдемся по строке
	while (idx < 16 && *cp != 0 && *(cp + 1) != 0)
	{
		if (!isxdigit(*cp))
		{
			cp++;
			continue;
		}

		char ch[2] = { *cp, *(cp + 1) };
		cp += 2;

		uint8_t b = (ch2byte(ch[0]) << 4) | ch2byte(ch[1]);

		if (idx < 4)
		{
			guid._l_data1 |= (uint32_t(b) << (idx1 * 8));
			idx++;
			idx1--;
		}
		else if (idx < 6)
		{
			guid._s_data2 |= uint16_t(b) << (idx2)* 8;
			idx++;
			idx2--;
		}
		else if (idx < 8)
		{
			if (idx == 6)
				idx2 = 1;
			guid._s_data3 |= uint16_t(b) << (idx2)* 8;
			idx++;
			idx2--;
		}
		else
		{
			guid._ch_data4[idx - 8] = b;
			idx++;
		}
	}
	//
	return true;
}
//--------------------------------------
//
//--------------------------------------
RTX_STD_API const char* std_guid_to_string(const guid_t& guid, char* buffer, int size)
{
	int len = std_snprintf(buffer, size, "%08X-%04X-%04X-%02X%02X-%02X%02X%02X%02X%02X%02X",
		guid._l_data1, guid._s_data2, guid._s_data3, guid._ch_data4[0], guid._ch_data4[1],
		guid._ch_data4[2], guid._ch_data4[3], guid._ch_data4[4], guid._ch_data4[5],
		guid._ch_data4[6], guid._ch_data4[7]);

	if (len > 0)
	{
		buffer[len] = 0;
		return buffer;
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
RTX_STD_API bool std_is_guid_empty(const guid_t& guid)
{
	const int64_t* pData4 = (const int64_t*)&guid._ch_data4;
	return guid._l_data1 == 0 && guid._s_data2 == 0 && guid._s_data3 == 0 && *pData4 == 0;
}
//--------------------------------------
//
//--------------------------------------
RTX_STD_API bool std_is_guid_equal(const guid_t& l_guid, const guid_t& r_guid)
{
	return  memcmp(&l_guid, &r_guid, sizeof(guid_t)) == 0;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
