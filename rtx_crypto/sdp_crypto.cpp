﻿/* RTX H.248 Media Gate. Crypto Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sdp_crypto.h"
#include "std_crypto.h"
//--------------------------------------
//
//--------------------------------------
RTX_CRYPTO_API sdp_crypto_parameter_t::sdp_crypto_parameter_t(int tag) :
	m_tag(tag), m_suite(sdp_crypto_none_e),
	m_KDR(0), m_UNENCRYPTED_SRTCP(false), m_UNENCRYPTED_SRTP(false),
	m_UNAUTHENTICATED_SRTP(false), m_FEC_ORDER(0), m_FEC_KEY(nullptr), m_WSH(0)
{
}
//--------------------------------------
//
//--------------------------------------
RTX_CRYPTO_API sdp_crypto_parameter_t::sdp_crypto_parameter_t(const sdp_crypto_parameter_t& param) :
	m_tag(param.m_tag), m_suite(param.m_suite),
	m_KDR(param.m_KDR), m_UNENCRYPTED_SRTCP(param.m_UNENCRYPTED_SRTCP), m_UNENCRYPTED_SRTP(param.m_UNENCRYPTED_SRTP),
	m_UNAUTHENTICATED_SRTP(param.m_UNAUTHENTICATED_SRTP), m_FEC_ORDER(param.m_FEC_ORDER), m_WSH(param.m_WSH)
{
	if (param.m_FEC_KEY != nullptr)
	{
		m_FEC_KEY = NEW sdp_crypto_key_param_t;
		memcpy(m_FEC_KEY, param.m_FEC_KEY, sizeof(sdp_crypto_key_param_t));
	}
	else
	{
		m_FEC_KEY = nullptr;
	}
}
//--------------------------------------
//
//--------------------------------------
RTX_CRYPTO_API sdp_crypto_parameter_t::~sdp_crypto_parameter_t()
{
	if (m_FEC_KEY != nullptr)
		DELETEO(m_FEC_KEY);

	m_key_params.clear();
}
//--------------------------------------
//
//--------------------------------------
int sdp_crypto_parameter_t::encode_key_params(const sdp_crypto_key_param_t& key_param, char* buffer, int size) const
{
	const char* initial_ptr = buffer;

	if (size > 7)
	{
		strcpy(buffer, "inline:");
		size -= 7;
		buffer += 7;
	}

	int len = Base64Encode(key_param.key, 30, buffer, size);
	buffer += len;
	size -= len;

	if (key_param.lifetime > 0)
	{
		// не самое простй алгоритм -- запись в виде 2^n
		int bit_pos = -1, bit_mask = 1;
		for (int i = 0; i < 32; i++)
		{
			if (bit_mask & key_param.lifetime)
			{
				if (bit_pos != -1)
				{
					bit_pos = -1;
					break;
				}
				bit_pos = i;
			}
			// сдвигаем маску
			bit_mask = bit_mask << 1;
		}

		if (bit_pos == -1)
		{
			len = std_snprintf(buffer, size, "|%d", key_param.lifetime);
			size -= len;
			buffer += len;
		}
		else
		{
			len = std_snprintf(buffer, size, "|2^%d", bit_pos);
			size -= len;
			buffer += len;
		}
	}

	if (key_param.mki > 0 && key_param.mki_size > 0)
	{
		len = std_snprintf(buffer, size, "|%d:%d", key_param.mki, key_param.mki_size);
		size -= len;
		buffer += len;
	}

	return int(buffer - initial_ptr);
}
//--------------------------------------
// запись в буфер по форме a=crypto:<tag> <crypto-suite> <key-params> [<session-params>]
//--------------------------------------
RTX_CRYPTO_API int sdp_crypto_parameter_t::encode(char* buffer, int size) const
{
	if (m_suite == sdp_crypto_none_e || m_key_params.getCount() == 0)
		return 0;

	const char* initial_ptr = buffer;

	// write tag
	int len = std_snprintf(buffer, size, "a=crypto:%d %s ", m_tag, get_crypto_suite_name(m_suite));
	size -= len;
	buffer += len;

	// write key info
	for (int i = 0; i < m_key_params.getCount(); i++)
	{
		if (i > 0 && size > 2)
		{
			*buffer++ = ';';
			size--;
		}
		const sdp_crypto_key_param_t& key = m_key_params[i];
		len = encode_key_params(key, buffer, size);
		size -= len;
		buffer += len;
	}

	if (m_KDR > 0)
	{
		len = std_snprintf(buffer, size, " KDR=%d", m_KDR);
		size -= len;
		buffer += len;
	}

	if (m_UNENCRYPTED_SRTP)
	{
		len = std_snprintf(buffer, size, " UNENCRYPTED_SRTP");
		size -= len;
		buffer += len;
	}

	if (m_UNENCRYPTED_SRTCP)
	{
		len = std_snprintf(buffer, size, " UNENCRYPTED_SRTCP");
		size -= len;
		buffer += len;
	}

	if (m_UNAUTHENTICATED_SRTP)
	{
		len = std_snprintf(buffer, size, " UNAUTHENTICATED_SRTP");
		size -= len;
		buffer += len;
	}

	if (m_FEC_ORDER != 0)
	{
		len = std_snprintf(buffer, size, " FEC_ORDER=%s", m_FEC_ORDER == 1 ? "FEC_SRTP" : "SRTP_FEC");
		size -= len;
		buffer += len;
	}

	if (m_FEC_KEY != nullptr)
	{
		len = std_snprintf(buffer, size, " FEC_KEY=inline:");
		size -= len;
		buffer += len;

		len = Base64Encode(m_FEC_KEY->key, 30, buffer, size);
		buffer += len;
		size -= len;

		if (m_FEC_KEY->lifetime > 0)
		{
			len = std_snprintf(buffer, size, "|%d", m_FEC_KEY->lifetime);
			size -= len;
			buffer += len;
		}

		if (m_FEC_KEY->mki > 0 && m_FEC_KEY->mki_size > 0)
		{
			len = std_snprintf(buffer, size, "|%d:%d", m_FEC_KEY->mki, m_FEC_KEY->mki_size);
			size -= len;
			buffer += len;
		}
	}

	*buffer++ = '\r';
	*buffer++ = '\n';

	return int(buffer - initial_ptr);
}
//--------------------------------------
// декодирование lifetime и MKI:MKI_size
//--------------------------------------
const char* sdp_crypto_parameter_t::decode_key_param(sdp_crypto_key_param_t& param, const char* str)
{
	const char* colon = strchr(str, ':');
	const char* cp = strpbrk(str, "| ");

	if (cp == nullptr)
		cp = str + strlen(str);

	// если есть '|' или отсутствует ':' или ':' дальше '|' то lifetime
	if (colon == nullptr || (*cp == '|' && colon > cp))
	{
		if (strncmp(str, "2^", 2) == 0)
		{
			int lf_exp = atoi(str + 2);
			param.lifetime = 1 << lf_exp;
		}
		else
		{
			param.lifetime = atoi(str);
		}
	}
	else if (colon != nullptr)
	{
		param.mki = atoi(str);
		param.mki_size = atoi(colon+1);
	}

	return cp;
}
//--------------------------------------
// перевода строки нет т.к. заменяется на терминатор '\0'
//--------------------------------------
bool sdp_crypto_parameter_t::decode_key_params(sdp_crypto_key_param_t& key_param, const char* str)
{
	const char* cp;

	// первый ключ всегда должен быть
	// ищем конец ключа это знак '|' или ' ' возможно конец строки
	
	if ((cp = strpbrk(str, "| ;")) == nullptr)
		cp = str + strlen(str);

	int key_len = int(cp-str);

	if (key_len != 40)
		return false;

	// нашли конец ключа
	memset(&key_param, 0, sizeof key_param);

	int len = Base64Decode(str, key_len, key_param.key, 30);

	if (len != 30)
		return false;

	if (*cp == '|')
	{
		// есть параметры -- разбор параметров лайфтайм и MKI:MKI_SIZE. Отличие: в мки есть знак ':'
		str = cp+1;
		cp = strchr(str, '|');

		if (cp == nullptr)
			cp = str + strlen(str);

		if (*cp == '|')
		{
			decode_key_param(key_param, str); // есть оба параметра
			cp = decode_key_param(key_param, cp+1); // есть оба параметра
		}
		else
		{
			cp = decode_key_param(key_param, str); // есть оба параметра
		}
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
const char* sdp_crypto_parameter_t::decode_session_params(const char* str)
{
	const char* equal = strchr(str, '=');
	const char* eol = strchr(str, ' ');

	if (eol == nullptr)
		eol = str + strlen(str);

	/*
	KDR
    UNENCRYPTED_SRTP
    UNENCRYPTED_SRTCP
    UNAUTHENTICATED_SRTP
    FEC_ORDER
    FEC_KEY
    WSH
	*/

	if (std_strnicmp(str, "KDR", 3) == 0 && equal != nullptr)
	{
		m_KDR = atoi(equal+1);
	}
	else if (std_strnicmp(str, "UNENCRYPTED_SRTP", 16) == 0)
	{
		m_UNENCRYPTED_SRTP = true;
	}
	else if (std_strnicmp(str, "UNENCRYPTED_SRTCP", 17) == 0)
	{
		m_UNENCRYPTED_SRTCP = true;
	}
	else if (std_strnicmp(str, "UNAUTHENTICATED_SRTP", 20) == 0)
	{
		m_UNAUTHENTICATED_SRTP = true;
	}
	else if (std_strnicmp(str, "FEC_ORDER", 9) == 0 && equal != nullptr)
	{
		m_FEC_ORDER = std_strnicmp(equal+1, "FEC_SRTP", 8) == 0 ? 1 :
			std_strnicmp(equal+1, "SRTP_FEC", 8) == 0 ? 2 : 0;
	}
	else if (std_strnicmp(str, "FEC_KEY", 7) == 0 && equal != nullptr)
	{
		//
		m_FEC_KEY = NEW sdp_crypto_key_param_t;
		if (!decode_key_params(*m_FEC_KEY, equal+1))
		{
			DELETEO(m_FEC_KEY);
			m_FEC_KEY = nullptr;
		}
	}
	else if (std_strnicmp(str, "WSH", 3) == 0 && equal != nullptr)
	{
		m_WSH = atoi(equal+1);
	}

	return eol;
}
//--------------------------------------
// разбор параметров шифпования:
//	"a=crypto:" tag 1*WSP crypto-suite 1*WSP key-params *(1*WSP session-param)
//--------------------------------------
RTX_CRYPTO_API bool sdp_crypto_parameter_t::decode(const char* str)
{
	// разбор по формату: tag SP crypto-suite SP crypto-params SP session-params

	const char* cp;

	if ((cp = strchr(str, ' ')) == nullptr)
		return false;

	// save the tag parameter
	m_tag = atoi(str);
	str = cp+1;

	// get crypto-suite
	if ((cp = strchr(str, ' ')) == nullptr)
		return false;

	if ((m_suite = ::get_crypto_suite(str, int(cp-str))) == sdp_crypto_error_e)
		return false;
	str = cp+1;

	sdp_crypto_key_param_t key_param;
	do
	{
		// get crypto-params
		// ищем следующий разделитель или конец строки
		if ((cp = strpbrk(str, " ;")) == nullptr)
			cp = str + strlen(str);

		if (strncmp(str, "inline:", 7) != 0)
		{
			return false;
		}

		if (decode_key_params(key_param, str + 7))
		{
			m_key_params.add(key_param);
		}
		str = cp+1;
	}
	while (*cp == ';');

	// параметров сессии нет
	if (*cp == 0)
		return true;

	do
	{
		cp = decode_session_params(cp+1);
	}
	while (*cp == ' ');

	// выход
	return true;
}
//--------------------------------------
//
//--------------------------------------
RTX_CRYPTO_API bool sdp_crypto_parameter_t::add_key(const uint8_t* key, int length, int lifetime, int mki, int mki_size)
{
	sdp_crypto_key_param_t key_param;
	memset(&key_param, 0, sizeof key_param);
	memcpy(key_param.key, key, MIN(length, 30));
	key_param.lifetime = lifetime;
	key_param.mki = mki;
	key_param.mki_size = mki_size;

	return m_key_params.add(key_param) != BAD_INDEX;
}
//--------------------------------------
//
//--------------------------------------
RTX_CRYPTO_API const char* get_crypto_suite_name(sdp_crypto_suite_t suite)
{
	static const char* names[] = { "0", "AES_CM_128_HMAC_SHA1_80", "AES_CM_128_HMAC_SHA1_32", "F8_128_HMAC_SHA1_80" };

	return suite >= sdp_crypto_none_e && suite <= sdp_crypto_f8_128_hmac_sha1_80_e ? names[suite] : "ERR";
}
//--------------------------------------
//
//--------------------------------------
RTX_CRYPTO_API sdp_crypto_suite_t get_crypto_suite(const char* str, int length)
{
	if (length == 23 && strncmp(str, "AES_CM_128_HMAC_SHA1_80", 23) == 0)
		return sdp_crypto_aes_cm_128_hmac_sha1_80_e;
	if (length == 23 && strncmp(str, "AES_CM_128_HMAC_SHA1_32", 23) == 0)
		return sdp_crypto_aes_cm_128_hmac_sha1_32_e;
	if (length == 19 && strncmp(str, "F8_128_HMAC_SHA1_80", 19) == 0)
		return sdp_crypto_f8_128_hmac_sha1_80_e;

	return sdp_crypto_error_e;
}
//--------------------------------------
