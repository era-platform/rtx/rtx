﻿/* RTX H.248 Media Gate. Crypto Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_crypto.h"

 //------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
enum fingerprint_role_t
{
	active,
	passive,
	actpass
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class RTX_CRYPTO_API sdp_fingerprint_t
{
	char m_hash_function[16];
	uint8_t* m_hash_data;
	int m_hash_length;

	char m_setup[16];
	fingerprint_role_t m_role;

public:
	sdp_fingerprint_t();
	~sdp_fingerprint_t();

	void set_hash(const char* hash_function, const uint8_t* hash, int length);
	
	const char* get_hash_function() const { return m_hash_function; }
	const uint8_t* get_hash() const { return m_hash_data; }
	int get_hash_length() const { return m_hash_length; }


	void parse(const char* fingerprint);
	int write_to(char* buffer, int size);

	bool compare(sdp_fingerprint_t* fingerprint);

	bool copy_from(const sdp_fingerprint_t* fingerprint);

	//a=setup:actpass
	//a=setup:passive
	//a=setup:active
	bool read_role(const char* role);
	const char* write_role();
	fingerprint_role_t get_role();
	void set_role(fingerprint_role_t role);

private:
	void cleanup();
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
