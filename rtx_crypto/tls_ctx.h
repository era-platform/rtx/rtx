/* RTX H.248 Media Gate. Crypto Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

 //-----------------------------------------------
// ������������� ���������� OpenSSl ���������� � ���� DTLS...
// �������� ������������ ����� ���������� ���������� ���� ��� � ����...
//-----------------------------------------------

//-----------------------------------------------
// ���� TLS. ������ ��� ������.
//-----------------------------------------------
enum tls_connection_role_t
{
	tls_connection_role_client_e,
	tls_connection_role_server_e
};
//-----------------------------------------------
// ������ ��������� TLS.
//-----------------------------------------------
enum tls_version_t
{
	TLSv1 = 0x0103,
	TLSv1_1 = 0x0203,
	TLSv1_2 = 0x0303
};
//-----------------------------------------------
// 
//-----------------------------------------------
class tls_ctx_t
{
	tls_ctx_t(const tls_ctx_t&);
	tls_ctx_t& operator = (const tls_ctx_t&);
	tls_ctx_t(tls_ctx_t&&);
	tls_ctx_t& operator = (tls_ctx_t&&);

public:
	RTX_CRYPTO_API tls_ctx_t(rtl::Logger& log, tls_connection_role_t role);
	RTX_CRYPTO_API virtual ~tls_ctx_t();

	RTX_CRYPTO_API bool init(tls_version_t version);
	RTX_CRYPTO_API void destroy();

	// ���������� ���� ������ � OPENSSL. (�������� ������)
	RTX_CRYPTO_API int rx_push(const char* buff, int buff_len);
	// ���������� ���� ������ � OPENSSL. (��������� ������)
	RTX_CRYPTO_API int tx_push(const char* buff, int buff_len);
	// �������� ������������ ������ �� OPENSSL. (�������� ������)
	RTX_CRYPTO_API int rx_pull(char* buff, int buff_len);
	// �������� ������������ ������ �� OPENSSL. (��������� ������)
	RTX_CRYPTO_API int tx_pull(char* buff, int buff_len);

	bool is_handshake_done() { return m_handshake_done; }
	bool is_initialized() { return m_ssl != nullptr; }

	RTX_CRYPTO_API int get_state();
	RTX_CRYPTO_API const char* get_state_string();
	RTX_CRYPTO_API const char* get_state_string_long();
	rtl::Logger& get_log() { return m_log; }

private:
	bool init_CTX(tls_version_t version);
	bool init_BIO();
	bool init_SSL();
	bool check_cert();

private:
	rtl::Logger& m_log;
	tls_connection_role_t m_role;
	bool m_handshake_done;

	struct ssl_ctx_st* m_ctx;
	struct ssl_st* m_ssl;
	struct bio_st* m_web;
	struct bio_st* m_out;
};
//-----------------------------------------------
