﻿/* RTX H.248 Media Gate. Crypto Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_filesystem.h"
#include "tls_certificate_manager.h"
#include "cert.h"
//------------------------------------------------------------------------------
#include <openssl/ssl.h>
#include <openssl/pkcs12.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <openssl/conf.h>
#include <openssl/x509v3.h>
#include <openssl/bio.h>
#include <openssl/rand.h>
//------------------------------------------------------------------------------
#include <memory.h>
//#include <WinNls.h>
//#include <WinBase.h>
//#include <WinCrypt.h>
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
#define LOG_PREFIX "TLSCM"
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// #if defined (TARGET_OS_WINDOWS)
// #pragma comment(lib, "crypt32.lib")
// #endif
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
static volatile bool		g_use_cert_from_res_for_dtls = false;
//------------------------------------------------------------------------------
tls_certificate_manager_t*	g_tls_certificate_manager = nullptr;
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void						init_openssl_lib(const char* pfx, const char* password, rtl::Logger* log)
{
	SSL_library_init();

	//SSL_load_error_strings();
	//OpenSSL_add_all_algorithms();
	//ERR_load_crypto_strings();
	//OpenSSL_add_all_ciphers();
	//CRYPTO_malloc_init();

	g_tls_certificate_manager = NEW tls_certificate_manager_t();

	if (!g_tls_certificate_manager->init_pfx(pfx, password))
	{
		DELETEO(g_tls_certificate_manager);
		g_tls_certificate_manager = nullptr;
		if (log != nullptr)
		{
			log->log_error(LOG_PREFIX, "init_openssl_lib -- init_pfx fail!");
			fprintf(stderr, "init_openssl_lib -- init_pfx fail!\n");
		}
	}
}
void						init_openssl_lib(const char* crt, const char* kye, const char* password, rtl::Logger* log)
{
	SSL_library_init();

	g_tls_certificate_manager = NEW tls_certificate_manager_t();

	if (!g_tls_certificate_manager->init_pem(crt, kye, password))
	{
		DELETEO(g_tls_certificate_manager);
		g_tls_certificate_manager = nullptr;
		if (log != nullptr)
		{
			log->log_error(LOG_PREFIX, "init_openssl_lib -- init_pem fail!");
			fprintf(stderr, "init_openssl_lib -- init_pem fail!\n");
		}
	}
}
//------------------------------------------------------------------------------
/*void						init_openssl_lib(const char* CN)
{
	SSL_library_init();

	g_tls_certificate_manager = NEW tls_certificate_manager_t();

	if (!g_tls_certificate_manager->init_ca(CN))
	{
		DELETEO(g_tls_certificate_manager);
		g_tls_certificate_manager = nullptr;
	}
}*/
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void						destroy_openssl_lib()
{
	//ERR_remove_state(0);
	////ENGINE_cleanup();
	//CONF_modules_unload(1);
	//ERR_free_strings();
	////EVP_cleanup();
	//CRYPTO_cleanup_all_ex_data();

	if (g_tls_certificate_manager != nullptr)
	{
		DELETEO(g_tls_certificate_manager);
		g_tls_certificate_manager = nullptr;
	}
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void						set_flag_use_cert_from_res_for_dtls(bool use)
{
	g_use_cert_from_res_for_dtls = use;
}
//------------------------------------------------------------------------------
bool						use_cert_from_res_for_dtls()
{
	return g_use_cert_from_res_for_dtls;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
tls_certificate_manager_t::tls_certificate_manager_t()
{
	m_p12 = nullptr;

	m_pkey = nullptr;
	m_cert = nullptr;
	m_pkey_res = nullptr;
	m_cert_res = nullptr;
	m_ca = nullptr;

	m_from_resource = false;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
tls_certificate_manager_t::~tls_certificate_manager_t()
{
	destroy();
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool tls_certificate_manager_t::init_pfx(const char* path_pfx, const char* password)
{
	if ((m_pkey != nullptr) && (m_cert != nullptr))
	{
		return false;
	}
	if ((path_pfx == nullptr) || (path_pfx[0] == 0) || (!contains_file(path_pfx)))
	{
		m_from_resource = true;
		return init_pem(nullptr, nullptr, nullptr);
	}
	//FILE* fp = fopen(path_pfx, "rb");
	//if (fp == nullptr)
	//{
	//	//fprintf(stderr, "Error opening file %s\n", path_pfx);
	//	return false;
	//}
	
	BIO* bio = BIO_new_file(path_pfx, "r");
	if (bio == nullptr)
	{
		return false;
	}

	m_p12 = d2i_PKCS12_bio(bio, nullptr);

	/*PKCS12* p12 = d2i_PKCS12_fp(fp, nullptr);
	m_p12 = d2i_PKCS12_fp(fp, nullptr);
	fclose(fp);*/

	if (m_p12 == nullptr)
	{
		//fprintf(stderr, "Error reading PKCS#12 file\n");
		return false;
	}

	STACK_OF(X509) *ca = nullptr;
	if (!PKCS12_parse((PKCS12*)m_p12, password, &m_pkey, &m_cert, &ca))
	{
		//fprintf(stderr, "Error parsing PKCS#12 file\n");
		return false;
	}
	m_ca = ca;

	if (use_cert_from_res_for_dtls())
	{
		return from_resources(password);
	}

	return true;
}
//------------------------------------------------------------------------------
bool tls_certificate_manager_t::init_pem(const char* cert_file, const char* key_file, const char* password)
{
	if ((m_pkey != nullptr) && (m_cert != nullptr))
	{
		return false;
	}

	if (!m_from_resource)
	{
		if (((cert_file == nullptr) || (cert_file[0] == 0) || (!contains_file(cert_file))) ||
			((key_file == nullptr) || (key_file[0] == 0) || (!contains_file(key_file))))
		{
			m_from_resource = true;
		}
	}
	
	if (m_from_resource)
	{
		return from_resources(password);
	}
	else
	{
		BIO* cbio = BIO_new_file(cert_file, "r");
		m_cert = PEM_read_bio_X509(cbio, nullptr, 0, nullptr);
		if (m_cert == nullptr)
		{
			//PLOG_CRYPTO_ERROR(LOG_PREFIX, "load_certificate --- cert == nullptr");
			return false;
		}

		BIO* kbio = BIO_new_file(key_file, "r");
		m_pkey = PEM_read_bio_PrivateKey(kbio, nullptr, 0, (void*)password);
		//RSA* rsa = PEM_read_bio_RSAPrivateKey(kbio, nullptr, 0, "hello");
		//RSA* rsa = PEM_read_bio_RSAPrivateKey(kbio, nullptr, 0, "");
		if (m_pkey == nullptr)
		{
			//PLOG_CRYPTO_ERROR(LOG_PREFIX, "load_certificate --- rsa == nullptr");
			return false;
		}

		if (use_cert_from_res_for_dtls())
		{
			return from_resources(password);
		}
	}

	return true;
}
//------------------------------------------------------------------------------
/*bool tls_certificate_manager_t::init_ca(const char* CN)
{
	if ((m_pkey != nullptr) && (m_cert != nullptr))
	{
		return false;
	}

	HCERTSTORE cs;
	PCCERT_CONTEXT ctx = nullptr;
	char buf[128];

	//X509_STORE* x509_store = X509_STORE_new();
	cs = CertOpenStore(CERT_STORE_PROV_SYSTEM, 0, 0, CERT_SYSTEM_STORE_LOCAL_MACHINE, L"MY");
	//cs = CertOpenSystemStoreA(0, store_name);
	if (cs == nullptr)
	{
		//PLOG_CRYPTO_ERROR(LOG_PREFIX, "init_ca --- CryptoAPI: failed to open system cert store '%s': error=%d\n", store_name, (int)GetLastError());
		return false;
	}

	while ((ctx = CertEnumCertificatesInStore(cs, ctx)))
	{
		m_cert = d2i_X509(nullptr, (const uint8_t**)&ctx->pbCertEncoded, ctx->cbCertEncoded);
		if (m_cert == nullptr)
		{
			//PLOG_CRYPTO_ERROR(LOG_PREFIX, "init_ca --- CryptoAPI: Could not process X509 DER encoding for CA cert\n");
			continue;
		}

		m_pkey = X509_get_pubkey(m_cert);
		if (m_pkey == nullptr)
		{
			//PLOG_CRYPTO_ERROR(LOG_PREFIX, "init_ca --- CryptoAPI: Could not process X509 DER encoding for CA cert\n");
			X509_free(m_cert);
			continue;
		}

		X509_NAME_oneline(X509_get_subject_name((X509*)m_cert), buf, sizeof(buf));
		const char* cn = get_CN(buf);
		if (cn == nullptr)
		{
			continue;
		}

		if (_stricmp(cn, CN) == 0)
		{
			break;
		}

		//RSA *rsa_key = EVP_PKEY_get1_RSA(public_key);
		//int key_length = RSA_size(rsa_key);

		//
		////PLOG_CRYPTO_WRITE(LOG_PREFIX, "init_ca --- OpenSSL: Loaded CA certificate for system certificate store:\n subject='%s'\n\n", buf);

		//if (!X509_STORE_add_cert(ssl_ctx->cert_store, cert))
		//{
		//	//PLOG_CRYPTO_ERROR(LOG_PREFIX, "init_ca --- Failed to add ca_cert to OpenSSL certificate store");
		//}

		//X509_free(cert);
	}

	if (!CertCloseStore(cs, 0))
	{
		//PLOG_CRYPTO_ERROR(LOG_PREFIX, "init_ca --- failed to close system cert store '%s': error=%d", store_name, (int)GetLastError());
	}

	return true;
}*/
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool tls_certificate_manager_t::change_pfx(const char* path_pfx, const char* password)
{
	if ((path_pfx == nullptr) || (path_pfx[0] == 0) || (!contains_file(path_pfx)))
	{
		return false;
	}

	destroy();

	return init_pfx(path_pfx, password);
}
//------------------------------------------------------------------------------
bool tls_certificate_manager_t::change_pem(const char* cert_file, const char* key_file, const char* password)
{
	if (((cert_file == nullptr) || (cert_file[0] == 0) || (!contains_file(cert_file))) ||
		((key_file == nullptr) || (key_file[0] == 0) || (!contains_file(key_file))))
	{
		return false;
	}

	destroy();

	return init_pem(cert_file, key_file, password);
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void tls_certificate_manager_t::destroy()
{
	if (m_ca != nullptr)
	{
		sk_X509_pop_free(m_ca, X509_free);
		m_ca = nullptr;
	}

	if (m_cert != nullptr)
	{
		X509_free(m_cert);
		m_cert = nullptr;
	}

	if (m_pkey != nullptr)
	{
		EVP_PKEY_free(m_pkey);
		m_pkey = nullptr;
	}

	if (m_cert_res != nullptr)
	{
		X509_free(m_cert_res);
		m_cert_res = nullptr;
	}

	if (m_pkey_res != nullptr)
	{
		EVP_PKEY_free(m_pkey_res);
		m_pkey_res = nullptr;
	}

	if (m_p12 != nullptr)
	{
		PKCS12_free((PKCS12*)m_p12);
		m_p12 = nullptr;
	}
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool tls_certificate_manager_t::create(char* key_file, char* cert_file)
{
	FILE* fp = fopen(key_file, "w");
	if (fp == nullptr)
	{
		//fprintf(stderr, "Error opening file %s\n", argv[1]);
		return false;
	}
	if (m_pkey)
	{
		//fprintf(fp, "***Private Key***\n");
		PEM_write_PrivateKey(fp, m_pkey, nullptr, nullptr, 0, nullptr, nullptr);
	}
	fclose(fp);

	fp = fopen(cert_file, "w");
	if (fp == nullptr)
	{
		//fprintf(stderr, "Error opening file %s\n", argv[1]);
		return false;
	}
	if (m_cert)
	{
		//fprintf(fp, "***User Certificate***\n");
		PEM_write_X509_AUX(fp, m_cert);
	}

	if (m_ca && sk_X509_num(m_ca))
	{
		//fprintf(fp, "***Other Certificates***\n");
		for (int i = 0; i < sk_X509_num(m_ca); i++)
		{
			PEM_write_X509_AUX(fp, sk_X509_value(m_ca, i));
		}
	}

	fclose(fp);

	return true;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
evp_pkey_st* tls_certificate_manager_t::get_key()
{
	return m_from_resource ? m_pkey_res : m_pkey;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
x509_st* tls_certificate_manager_t::get_cert()
{
	return m_from_resource ? m_cert_res : m_cert;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
evp_pkey_st* tls_certificate_manager_t::get_key_res()
{
	return m_pkey_res;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
x509_st* tls_certificate_manager_t::get_cert_res()
{
	return m_cert_res;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool tls_certificate_manager_t::create_certificate(const char* key_file, const char* cert_file)
{
	//// проверяем наличие файлов.
	//if (!contains_file(cert_file) || !contains_file(key_file))
	//{
	//	CRYPTO_mem_ctrl(CRYPTO_MEM_CHECK_ON);

	//	/* длина ключа в битах */
	//	unsigned long bits = 2048;

	//	/* указатель на структуру для хранения ключей */
	//	RSA * rsa = nullptr;

	//	/* Генерируем ключи */
	//	rsa = RSA_generate_key(bits, RSA_F4, nullptr, nullptr);

	//	/* указатель на структуру ключа */
	//	EVP_PKEY* pk = EVP_PKEY_new();
	//	EVP_PKEY_assign_RSA(pk, rsa);

	//	/* указатель на сертификат*/
	//	X509* x509 = X509_new();

	//	// дней действие сертификата.
	//	int days = 365;

	//	X509_set_version(x509, NID_X509); // установка версии.
	//	ASN1_INTEGER_set(X509_get_serialNumber(x509), 0x00000000); //set serial number
	//	X509_gmtime_adj(X509_get_notBefore(x509), 0);
	//	X509_gmtime_adj(X509_get_notAfter(x509), (long)60 * 60 * 24 * days);
	//	X509_set_pubkey(x509, pk);

	//	X509_NAME* name = X509_get_subject_name(x509);

	//	// параметры: 

	//	//	Common Name	--
	//	//	Полное доменное имя для вашего веб - сервера. Оно должно в точности совпадать!!!
	//	//	Если вы предполагаете использовать следующий URL : https ://www.yourdomain.com, 
	//	//	то "Common Name" должно быть www.yourdomain.com
	//	//	Для WildCard - сертификатов указывайте со звездочкой.Пример : *.yourdomain.com
	//	const uint8_t commonName[] = "www.oktell.ru";
	//	X509_NAME_add_entry_by_txt(name, "CN", MBSTRING_ASC, commonName, -1, -1, 0);

	//	//	Organization --
	//	//	Наименование организации.
	//	const uint8_t organization[] = "TelSystem";
	//	X509_NAME_add_entry_by_txt(name, "O", MBSTRING_ASC, organization, -1, -1, 0);

	//	//	Organization Unit --
	//	//	Наименование отдела, подразделения(на английском языке).
	//	//X509_NAME_add_entry_by_txt(name, "OU", MBSTRING_ASC, "", -1, -1, 0);

	//	//	City or Locality --
	//	//	Город (на английском языке).
	//	//X509_NAME_add_entry_by_txt(name, "CL", MBSTRING_ASC, "", -1, -1, 0);

	//	//	State or Province --
	//	//	Область (на английском языке).
	//	//X509_NAME_add_entry_by_txt(name, "SP", MBSTRING_ASC, "", -1, -1, 0);

	//	//	Country --
	//	//	Страна, в виде двухсимвольного ISO - кода. Для России : RU
	//	const uint8_t country[] = "RU";
	//	X509_NAME_add_entry_by_txt(name, "C", MBSTRING_ASC, country, -1, -1, 0);

	//	//X509_set_subject_name(x509, name); //save name fields to certificate
	//	X509_set_issuer_name(x509, name); //save name fields to certificate

	//	X509_EXTENSION *ex;

	//	// хз.
	//	// что то для netscape
	//	ex = X509V3_EXT_conf_nid(nullptr, nullptr, NID_netscape_cert_type, "server");
	//	X509_add_ext(x509, ex, -1);
	//	X509_EXTENSION_free(ex);

	//	ex = X509V3_EXT_conf_nid(nullptr, nullptr, NID_netscape_comment, "example comment extension");
	//	X509_add_ext(x509, ex, -1);
	//	X509_EXTENSION_free(ex);

	//	ex = X509V3_EXT_conf_nid(nullptr, nullptr, NID_netscape_ssl_server_name, "www.oktell.ru");
	//	X509_add_ext(x509, ex, -1);
	//	X509_EXTENSION_free(ex);

	//	ex = X509V3_EXT_conf_nid(nullptr, nullptr, NID_basic_constraints, "critical,CA:TRUE");
	//	X509_add_ext(x509, ex, -1);
	//	X509_EXTENSION_free(ex);

	//	/*ex = X509V3_EXT_conf_nid(nullptr, nullptr, NID_subject_key_identifier, "hash");
	//	X509_add_ext(x509, ex, -1);
	//	X509_EXTENSION_free(ex);*/

	//	/*ex = X509V3_EXT_conf_nid(nullptr, nullptr, NID_authority_key_identifier, "keyid:always");
	//	X509_add_ext(x509, ex, -1);
	//	X509_EXTENSION_free(ex);*/

	//	//X509_sign(x509, pk, EVP_sha1()); //sign x509 certificate
	//	X509_sign(x509, pk, EVP_md_null()); //sign x509 certificate

	//	//Секретный ключ шифруем с помощью парольной фразы "hello"
	//	uint8_t password[] = "hello";

	//	FILE* priv_key_file = fopen(key_file, "wb");
	//	if (!PEM_write_PrivateKey(
	//		priv_key_file,		/* write the key to the file we've opened */
	//		pk,					/* our key from earlier */
	//		EVP_des_ede3_cbc(),	/* default cipher for encrypting the key on disk */
	//		password,			/* passphrase required for decrypting the key on disk */
	//		sizeof(password),	/* length of the passphrase string */
	//		nullptr,				/* callback for requesting a password */
	//		nullptr				/* data to pass to the callback */
	//		))
	//	{
	//		handle_openssl_error();
	//		return false;
	//	}

	//	FILE* certificate_file = fopen(cert_file, "wb");
	//	if (!PEM_write_X509(
	//		certificate_file,	/* write the certificate to the file we've opened */
	//		x509				/* our certificate */
	//		))
	//	{
	//		handle_openssl_error();
	//		return false;
	//	}

	//	/* Освобождаем память, выделенную под структуру rsa */
	//	RSA_free(rsa);

	//	CRYPTO_mem_ctrl(CRYPTO_MEM_CHECK_OFF);


	//	// Другой вариант записи ключа :

	//	///* контекст алгоритма шифрования */
	//	//const EVP_CIPHER *cipher = nullptr;

	//	///* Формируем контекст алгоритма шифрования */
	//	//OpenSSL_add_all_ciphers();
	//	//cipher = EVP_get_cipherbyname("bf-ofb");

	//	///* Получаем из структуры rsa открытый и секретный ключи и сохраняем в файлах.
	//	//* Секретный ключ шифруем с помощью парольной фразы "hello"
	//	//*/
	//	//PEM_write_RSAPrivateKey(priv_key_file, rsa, cipher, nullptr, 0, nullptr, password);

	//	///* Освобождаем память, выделенную под структуру rsa */
	//	////RSA_free(rsa);
	//}
	
	//return true;

	return false;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool tls_certificate_manager_t::contains_file(const char* filename)
{
	return fs_is_file_exist(filename);
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool tls_certificate_manager_t::load_certificate(SSL_CTX* ctx, rtl::Logger* m_log)
{
	X509* cert = get_cert();
	if ((cert == nullptr) || (!SSL_CTX_use_certificate(ctx, cert)))
	{
		PLOG_CRYPTO_ERROR(LOG_PREFIX, "load_certificate --- no certificate found!");
		return false;
	}

	EVP_PKEY* key = get_key();
	if ((key == nullptr) || (!SSL_CTX_use_PrivateKey(ctx, key)))
	{
		PLOG_CRYPTO_ERROR(LOG_PREFIX, "load_certificate --- no private key found!");
		return false;
	}

	if (!SSL_CTX_check_private_key(ctx))
	{
		PLOG_CRYPTO_ERROR(LOG_PREFIX, "load_certificate --- invalid private key!");
		return false;
	}
	else
	{
		PLOG_CRYPTO_WRITE(LOG_PREFIX, "load_certificate --- private key - OK!");
	}

	return true;
}
//------------------------------------------------------------------------------
bool tls_certificate_manager_t::load_certificate(SSL* ssl, rtl::Logger* m_log)
{
	X509* cert = get_cert();
	if ((cert == nullptr) || (!SSL_use_certificate(ssl, cert)))
	{
		PLOG_CRYPTO_ERROR(LOG_PREFIX, "load_certificate --- no certificate found!");
		return false;
	}

	EVP_PKEY* key = get_key();
	if ((key == nullptr) || (!SSL_use_PrivateKey(ssl, key)))
	{
		PLOG_CRYPTO_ERROR(LOG_PREFIX, "load_certificate --- no private key found!");
		return false;
	}

	if (!SSL_check_private_key(ssl))
	{
		PLOG_CRYPTO_ERROR(LOG_PREFIX, "load_certificate --- invalid private key!");
		return false;
	}
	else
	{
		PLOG_CRYPTO_WRITE(LOG_PREFIX, "load_certificate --- private key - OK!");
	}

	return true;
}
//------------------------------------------------------------------------------
bool tls_certificate_manager_t::load_certificate_resource(struct ssl_ctx_st* ctx, rtl::Logger* m_log)
{
	X509* cert = get_cert_res();
	if (cert == nullptr)
	{
		if (!from_resources(""))
		{
			PLOG_CRYPTO_ERROR(LOG_PREFIX, "load_certificate_resource --- from_resources fail!");
			return false;
		}
	}

	cert = get_cert_res();
	if ((cert == nullptr) || (!SSL_CTX_use_certificate(ctx, cert)))
	{
		PLOG_CRYPTO_ERROR(LOG_PREFIX, "load_certificate_resource --- no certificate found!");
		return false;
	}

	EVP_PKEY* key = get_key_res();
	if ((key == nullptr) || (!SSL_CTX_use_PrivateKey(ctx, key)))
	{
		PLOG_CRYPTO_ERROR(LOG_PREFIX, "load_certificate_resource --- no private key found!");
		return false;
	}

	if (!SSL_CTX_check_private_key(ctx))
	{
		PLOG_CRYPTO_ERROR(LOG_PREFIX, "load_certificate_resource --- invalid private key!");
		return false;
	}
	else
	{
		PLOG_CRYPTO_WRITE(LOG_PREFIX, "load_certificate_resource --- private key - OK!");
	}

	return true;
}
//------------------------------------------------------------------------------
bool tls_certificate_manager_t::load_certificate_resource(struct ssl_st* ssl, rtl::Logger* m_log)
{
	X509* cert = get_cert_res();
	if (cert == nullptr)
	{
		if (!from_resources(""))
		{
			PLOG_CRYPTO_ERROR(LOG_PREFIX, "load_certificate_resource --- from_resources fail!");
			return false;
		}
	}

	cert = get_cert_res();
	if ((cert == nullptr) || (!SSL_use_certificate(ssl, cert)))
	{
		PLOG_CRYPTO_ERROR(LOG_PREFIX, "load_certificate_resource --- no certificate found!");
		return false;
	}

	EVP_PKEY* key = get_key_res();
	if ((key == nullptr) || (!SSL_use_PrivateKey(ssl, key)))
	{
		PLOG_CRYPTO_ERROR(LOG_PREFIX, "load_certificate_resource --- no private key found!");
		return false;
	}

	if (!SSL_check_private_key(ssl))
	{
		PLOG_CRYPTO_ERROR(LOG_PREFIX, "load_certificate_resource --- invalid private key!");
		return false;
	}
	else
	{
		PLOG_CRYPTO_WRITE(LOG_PREFIX, "load_certificate_resource --- private key - OK!");
	}

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool tls_certificate_manager_t::from_resources(const char* password)
{
	BIO* cbio = BIO_new_mem_buf((void*)g_mg_cert, G_MG_CERT_SIZE);
	m_cert_res = PEM_read_bio_X509(cbio, nullptr, 0, nullptr);
	if (m_cert_res == nullptr)
	{
		//PLOG_CRYPTO_ERROR(LOG_PREFIX, "load_certificate --- cert == nullptr");
		return false;
	}

	BIO* kbio = BIO_new_mem_buf((void*)g_mg_key, G_MG_KEY_SIZE);
	if ((password == nullptr) || (password[0] == 0))
	{
		m_pkey_res = PEM_read_bio_PrivateKey(kbio, nullptr, 0, (void*)"hello");
	}
	else
	{
		m_pkey_res = PEM_read_bio_PrivateKey(kbio, nullptr, 0, (void*)password);
	}

	//RSA* rsa = PEM_read_bio_RSAPrivateKey(kbio, nullptr, 0, "hello");
	//RSA* rsa = PEM_read_bio_RSAPrivateKey(kbio, nullptr, 0, "");
	if (m_pkey_res == nullptr)
	{
		//PLOG_CRYPTO_ERROR(LOG_PREFIX, "load_certificate --- rsa == nullptr");
		return false;
	}

	return true;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
const char*	tls_certificate_manager_t::get_CN(char* params) const
{
	char* cp = params;
	char* begin = nullptr;
	int count = 0;
	bool go = true;
	while (go)
	{
		cp = strchr(cp, '/');
		if (cp == nullptr)
		{
			go = false;
		}
		else
		{
			if (strncmp(cp + 1, "CN", 2) == 0)
			{
				begin = cp + 4;
				const char* next = strchr(begin, '/');
				if (next == nullptr)
				{
					return begin;
				}
				count = int(next - cp - 4);
				begin[count] = 0;
				return begin;
			}
			else
			{
				cp++;
			}
		}
	}

	return nullptr;
};
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
