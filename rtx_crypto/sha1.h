﻿/* RTX H.248 Media Gate. Crypto Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  sha1.h
 *
 *  Description:
 *      This is the header file for code which implements the Secure
 *      Hashing Algorithm 1 as defined in FIPS PUB 180-1 published
 *      April 17, 1995.
 *
 *      Many of the variable names in this code, especially the
 *      single character names, were used because those were the names
 *      used in the publication.
 *
 *      Please read the file sha1.c for more information.
 *
 */


#pragma once

////--------------------------------------
////
////--------------------------------------
//enum
//{
//    shaSuccess = 0,
//    shaNull,            /* Null pointer parameter */
//    shaInputTooLong,    /* input data too long */
//    shaStateError       /* called Input after Result */
//};
////--------------------------------------
////
////--------------------------------------
//#define SHA1_HASH_SIZE 20
////--------------------------------------
////  This structure will hold context information for the SHA-1 hashing operation
////--------------------------------------
//class sha1_context_t
//{
//    uint32_t m_intermediate_hash[SHA1_HASH_SIZE/4];	/* Message Digest  */
//    uint32_t m_length_low;							/* Message length in bits      */
//    uint32_t m_length_high;							/* Message length in bits      */
//
//													/* Index into message block array   */
//    int_least16_t m_message_block_index;
//    uint8_t m_message_block[64];					/* 512-bit message blocks      */
//
//    int m_computed;									/* Is the digest computed?         */
//    int m_corrupted;								/* Is the message digest corrupted? */
//
//	void sha1_pad_message();
//	void sha1_process_message_block();
//
//public:
//	sha1_context_t();
//
//	int reset();
//	int input(const uint8_t* data, int length);
//	int result(uint8_t digest[SHA1_HASH_SIZE]);
//};
