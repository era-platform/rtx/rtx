﻿/* RTX H.248 Media Gate. Crypto Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_crypto.h"

 //------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define SDP_CRYPTO_KEY_METHOD		"inline"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
enum sdp_crypto_suite_t
{
	sdp_crypto_none_e = 0,
	sdp_crypto_aes_cm_128_hmac_sha1_80_e = 1,
	sdp_crypto_aes_cm_128_hmac_sha1_32_e = 2,
	sdp_crypto_f8_128_hmac_sha1_80_e = 3,
	sdp_crypto_error_e = -1,
};
//------------------------------------------------------------------------------
RTX_CRYPTO_API const char* get_crypto_suite_name(sdp_crypto_suite_t suite);
RTX_CRYPTO_API sdp_crypto_suite_t get_crypto_suite(const char* str, int length);
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
struct sdp_crypto_key_param_t
{
	uint8_t key[30];
	int lifetime;
	int mki;
	int mki_size;
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class sdp_crypto_parameter_t
{
	int m_tag;
	sdp_crypto_suite_t m_suite;
	rtl::ArrayT<sdp_crypto_key_param_t> m_key_params;
	int m_KDR;
	bool m_UNENCRYPTED_SRTCP;
	bool m_UNENCRYPTED_SRTP;
	bool m_UNAUTHENTICATED_SRTP;
	int m_FEC_ORDER; // 0 - nothing, 1 - FEC_SRTP(default), 2 - SRTP_FEC
	sdp_crypto_key_param_t* m_FEC_KEY; // ??? FEC_KEY=key-params
	int m_WSH; // min(64)

	int encode_key_params(const sdp_crypto_key_param_t& param, char* buffer, int size) const;
	const char* decode_key_param(sdp_crypto_key_param_t& param, const char* str);
	bool decode_key_params(sdp_crypto_key_param_t& param, const char* str);
	const char* decode_session_params(const char* str);

public:
	RTX_CRYPTO_API sdp_crypto_parameter_t(int tag);
	RTX_CRYPTO_API sdp_crypto_parameter_t(const sdp_crypto_parameter_t& param);
	RTX_CRYPTO_API ~sdp_crypto_parameter_t();

	RTX_CRYPTO_API int encode(char* buffer, int size) const;
	RTX_CRYPTO_API bool decode(const char* str);

	int get_crypto_tag() const { return m_tag; }
	void set_crypto_tag(int tag) { m_tag = tag; }

	sdp_crypto_suite_t get_crypto_suite() const { return m_suite; }
	void set_crypto_suite(sdp_crypto_suite_t suite) { m_suite = suite; }

	int get_key_count() const { return m_key_params.getCount(); }
	const sdp_crypto_key_param_t& get_key_at(int index) const { return m_key_params.getAt(index); }
	void remove_key_at(int index) { m_key_params.removeAt(index); }
	void remove_all_keys() { m_key_params.clear(); }

	RTX_CRYPTO_API bool add_key(const uint8_t* key, int length, int lifetime, int mki, int mki_size);
	bool add_key(const sdp_crypto_key_param_t& key_param) { return m_key_params.add(key_param) != BAD_INDEX; }

	int get_kdr() const { return m_KDR; }
	void set_kdr(int kdr) { m_KDR = kdr; }
	bool get_unencrypted_srtcp_flag() const { return m_UNENCRYPTED_SRTCP; }
	void set_unencrypted_srtcp_flag(bool flag) { m_UNENCRYPTED_SRTCP = flag; }
	bool get_unencrypted_srtp_flag() const { return m_UNENCRYPTED_SRTP; }
	void set_unencrypted_srtp_flag(bool flag) { m_UNENCRYPTED_SRTP = flag; }
	bool get_unauthenticated_srtp_flag() const { return m_UNAUTHENTICATED_SRTP; }
	void set_unauthenticated_srtp_flag(bool flag) { m_UNAUTHENTICATED_SRTP = flag; }
	int get_wsh() const { return m_WSH; }
	void get_wsh(int wsh) { m_WSH = wsh; }

	// только получаем но никогда не используем сами, т.к. у нас нет реализации FEC
	int get_fec_order() const { return m_FEC_ORDER; }
	sdp_crypto_key_param_t* get_fec_key() const { return m_FEC_KEY; }
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
