﻿/* RTX H.248 Media Gate. Crypto Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "dtls.h"
#include "srtp_define.h"
#include "tls_certificate_manager.h"
#include "net/ip_address.h"
#include "net/socket_address.h"
//------------------------------------------------------------------------------
#include <openssl/pem.h>
#include <openssl/conf.h>
#include <openssl/x509v3.h>
#include <openssl/ssl.h>
#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/rand.h>
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
#define LOG_PREFIX "DTLS"
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
enum dtls_state_e
{
	DS_HANDSHAKE,
	DS_SETUP,
	DS_READY,
	DS_FAIL,
	DS_INVALID,
	DS_STARTED
};
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
struct dtls_ssl_fields
{
public:
	dtls_ssl_fields()
	{
		read_bio = nullptr;
		write_bio = nullptr;
		ssl = nullptr;
		ctx = nullptr;
		state = DS_INVALID;
		last_state = DS_INVALID;
		count_msg = 0;
	}
	virtual ~dtls_ssl_fields()
	{
		if (ssl != nullptr)
		{
			SSL_free(ssl);
			ssl = nullptr;
		}

		// BIO_free не делаем т.к. эта память дложна освободится при SSL_free
		if (read_bio != nullptr)
		{
			//BIO_free(read_bio);
			read_bio = nullptr;
		}

		if (write_bio != nullptr)
		{
			//BIO_free(write_bio);
			write_bio = nullptr;
		}
		
		if (ctx != nullptr)
		{
			SSL_CTX_free(ctx);
			ctx = nullptr;
		}

		state = DS_INVALID;
		last_state = DS_INVALID;

		count_msg = 0;
	}

	BIO* read_bio;  /* OpenSSL will read its incoming SSL packets from here */
	BIO* write_bio; /* OpenSSL will write its outgoing SSL packets to here */
	SSL* ssl;
	SSL_CTX* ctx;
	dtls_state_e state;
	dtls_state_e last_state;

	uint32_t count_msg;
};
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
const char *dtls_state_names(dtls_state_e s)
{
	if (s == DS_HANDSHAKE)
	{
		return "HANDSHAKE";
	}
	else if (s == DS_SETUP)
	{
		return "SETUP";
	}
	else if (s == DS_READY)
	{
		return "READY";
	}
	else if (s == DS_FAIL)
	{
		return "FAIL";
	}
	else if (s == DS_INVALID)
	{
		return "INVALID";
	}
	else if (s == DS_STARTED)
	{
		return "STARTED";
	}
	else
	{
		return "INVALID";
	}
}
//------------------------------------------------------------------------------
void dtls_set_state(dtls_ssl_fields* ssl_fields, dtls_state_e state, rtl::Logger* m_log)
{
	PLOG_CRYPTO_WRITE(LOG_PREFIX, "dtls_set_state --- Changing DTLS state from %s to %s", dtls_state_names(ssl_fields->state), dtls_state_names(state));

	ssl_fields->last_state = ssl_fields->state;
	ssl_fields->state = state;
}
//------------------------------------------------------------------------------
const EVP_MD *dtls_get_evp_by_name(const char *name)
{
	if (std_stricmp(name, "md5") == 0)
	{
		return EVP_md5();
	}
	else if (std_stricmp(name, "sha1") == 0)
	{
		return EVP_sha1();
	}
	else if (std_stricmp(name, "sha-1") == 0)
	{
		return EVP_sha1();
	}
	else if (std_stricmp(name, "sha-256") == 0)
	{
		return EVP_sha256();
	}
	else if (std_stricmp(name, "sha-512") == 0)
	{
		return EVP_sha512();
	}

	return nullptr;
}
//------------------------------------------------------------------------------
int  dtls_cert_extract_and_check_fingerprint(X509* x509, sdp_fingerprint_t *fp)
{
	const EVP_MD *evp;
//	unsigned int i, j;

	evp = dtls_get_evp_by_name(fp->get_hash_function());

	uint8_t hash[64] = { 0 };// (uint8_t*)fp->get_hash();
	uint32_t len = 64;// fp->get_hash_length();

	int res = X509_digest(x509, evp, hash, &len);

	if ((res != 1) || (len == 0))
	{
		//switch_MLOG__printf(SWITCH_CHANNEL_MLOG_, SWITCH_MLOG__ERROR, "FP DIGEST ERR!\n");
		return -1;
	}

	if (len != (uint32_t)fp->get_hash_length())
	{
		return -1;
	}

	for (uint32_t i = 0; i < len; i++)
	{
		if (hash[i] != (fp->get_hash()[i]))
		{
			return -1;
		}
	}

	return 0;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
dtls::dtls(rtl::Logger* log, net_sender_handler* rtp_handler) :
m_log(log), m_rtp(rtp_handler)
{
	m_local_fingerprint = nullptr;
	m_remote_fingerprint = nullptr;

	m_connection_role = dtls_connection_role_t::dtls_connection_role_invalid_e;
		
	m_ssl_fields = nullptr;

	m_set_crypto = 0;

	m_crypto_in.crypto_suite = 0;
	m_crypto_out.crypto_suite = 0;

	memset(m_crypto_in.key, 0, sizeof(m_crypto_in.key));
	memset(m_crypto_out.key, 0, sizeof(m_crypto_in.key));

	m_crypto_in.null_rtcp_cipher = false;
	m_crypto_in.null_rtp_auth = false;
	m_crypto_in.null_rtp_cipher = false;

	m_crypto_out.null_rtcp_cipher = false;
	m_crypto_out.null_rtp_auth = false;
	m_crypto_out.null_rtp_cipher = false;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
dtls::~dtls()
{
	dispose();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void dtls::dispose()
{
	if (m_local_fingerprint != nullptr)
	{
		DELETEO(m_local_fingerprint);
		m_local_fingerprint = nullptr;
	}
	if (m_remote_fingerprint != nullptr)
	{
		DELETEO(m_remote_fingerprint);
		m_remote_fingerprint = nullptr;
	}

	if (m_ssl_fields != nullptr)
	{
		DELETEO(m_ssl_fields);
		m_ssl_fields = nullptr;
	}

	m_connection_role = dtls_connection_role_t::dtls_connection_role_invalid_e;

	m_crypto_in.crypto_suite = 0;
	m_crypto_out.crypto_suite = 0;

	memset(m_crypto_in.key, 0, sizeof(m_crypto_in.key));
	memset(m_crypto_out.key, 0, sizeof(m_crypto_in.key));

	m_crypto_in.null_rtcp_cipher = false;
	m_crypto_in.null_rtp_auth = false;
	m_crypto_in.null_rtp_cipher = false;

	m_crypto_out.null_rtcp_cipher = false;
	m_crypto_out.null_rtp_auth = false;
	m_crypto_out.null_rtp_cipher = false;

	m_set_crypto = 0;
}
//------------------------------------------------------------------------------
// Подготовка отпечатка по сертификату.
//------------------------------------------------------------------------------
bool dtls::prepare_fingerprint(const char* algorithm)
{
	/*if (g_dtls_cert_file == nullptr)
	{
		return false;
	}*/

	if ((m_local_fingerprint != nullptr) && (strcmp(m_local_fingerprint->get_hash_function(), algorithm) == 0))
	{
		return true;
	}

	unsigned char fprint[EVP_MAX_MD_SIZE] = {0};
	unsigned int fprint_size = 0;

	// creates BIO buffer
	/*BIO* bio = BIO_new_mem_buf(g_dtls_cert_file, g_dtls_length_cert_file);
	if (bio == nullptr)
	{
		return false;
	}
*/
	X509* x509 = (g_tls_certificate_manager == nullptr) ? nullptr : g_tls_certificate_manager->get_cert();
	// decodes buffer
	/*if (!(x509 = PEM_read_bio_X509(bio, nullptr, 0L, nullptr)))
	{
		BIO_free(bio);
		return false;
	};*/
	if (x509 == nullptr)
	{
		return false;
	}
	
	//const EVP_MD* digest = EVP_get_digestbyname("sha1");
	size_t size = strlen(algorithm);
	char buff[20] = { 0 };
	buff[19] = '\0';
	int j = 0;
	for (size_t i = 0; i < 19; i++)
	{
		if (i >= size)
		{
			break;
		}

		if (algorithm[i] != '-')
		{
			buff[j] = algorithm[i];
			j++;
		}
	}
	const EVP_MD* digest = EVP_get_digestbyname(buff);
	if (digest == nullptr)
	{
		return false;
	}
	
	int err = X509_digest(x509, digest, fprint, &fprint_size);
	// frees memory
	//BIO_free(bio);
	if (err < 0)
	{
		return false;
	}

	if (m_local_fingerprint == nullptr)
	{
		m_local_fingerprint = NEW sdp_fingerprint_t();
	}
	else
	{
		DELETEO(m_local_fingerprint);
		m_local_fingerprint = NEW sdp_fingerprint_t();
	}

	m_local_fingerprint->set_hash(algorithm, fprint, fprint_size);

	return true;
}
//------------------------------------------------------------------------------
bool dtls::prepare_fingerprint()
{
	return prepare_fingerprint(m_remote_fingerprint == nullptr ? "sha-256" : m_remote_fingerprint->get_hash_function());
}
//------------------------------------------------------------------------------
const uint8_t* dtls::get_fingerprint()
{
	if (m_local_fingerprint == nullptr)
	{
		return nullptr;
	}

	return m_local_fingerprint->get_hash();
}
//------------------------------------------------------------------------------
uint32_t dtls::get_fingerprint_length()
{
	if (m_local_fingerprint == nullptr)
	{
		return 0;
	}

	return m_local_fingerprint->get_hash_length();
}
//------------------------------------------------------------------------------
const char* dtls::get_fingerprint_algorithm()
{
	if (m_local_fingerprint == nullptr)
	{
		return nullptr;
	}

	return m_local_fingerprint->get_hash_function();
}
//------------------------------------------------------------------------------
bool dtls::set_remote_fingerprint(const char* algorithm, const uint8_t* fingerprint, int length)
{
	if (m_remote_fingerprint == nullptr)
	{
		m_remote_fingerprint = NEW sdp_fingerprint_t();
	}

	m_remote_fingerprint->set_hash(algorithm, fingerprint, length);

	return true;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
dtls_connection_role_t dtls::get_dtls_connection_role()
{
	return m_connection_role;
}
//------------------------------------------------------------------------------
void dtls::set_dtls_connection_role(dtls_connection_role_t role)
{
	m_connection_role = role;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool dtls::chek_dtls()
{
	if ((m_ssl_fields == nullptr) || (m_ssl_fields->state == DS_INVALID))
	{
		return true;
	}

	return m_ssl_fields->state == DS_READY;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool dtls::setup_dtls()
{
	if (m_ssl_fields != nullptr && m_ssl_fields->state != DS_INVALID)
	{
		return true;
	}

	/*if ((g_dtls_cert_file == nullptr) || (g_dtls_key_file == nullptr))
	{
	return false;
	}*/

	m_ssl_fields = NEW dtls_ssl_fields();

	dtls_set_state(m_ssl_fields, DS_STARTED, m_log);

	m_ssl_fields->ctx = SSL_CTX_new(DTLS_method());
	if (m_ssl_fields->ctx == nullptr)
	{
		return false;
	}

	SSL_CTX_set_mode(m_ssl_fields->ctx, SSL_MODE_AUTO_RETRY);

	//SSL_CTX_set_verify(dtls->ssl_ctx, SSL_VERIFY_PEER | SSL_VERIFY_FAIL_IF_NO_PEER_CERT, nullptr);
	SSL_CTX_set_verify(m_ssl_fields->ctx, SSL_VERIFY_NONE, nullptr);

	SSL_CTX_set_cipher_list(m_ssl_fields->ctx, "ALL");

	SSL_CTX_set_tlsext_use_srtp(m_ssl_fields->ctx, "SRTP_AES128_CM_SHA1_80");

	m_ssl_fields->read_bio = BIO_new(BIO_s_mem());
	if (m_ssl_fields->read_bio == nullptr)
	{
		return false;
	}

	m_ssl_fields->write_bio = BIO_new(BIO_s_mem());
	if (m_ssl_fields->write_bio == nullptr)
	{
		return false;
	}

	BIO_set_mem_eof_return(m_ssl_fields->read_bio, -1);
	BIO_set_mem_eof_return(m_ssl_fields->write_bio, -1);

	//if (!dtls_load_certificate(m_ssl_fields->ctx, (const char*)g_dtls_cert_file, (const char*)g_dtls_key_file, true, m_MLOG_))
	if (g_tls_certificate_manager == nullptr)
	{
		return false;
	}
	bool res = use_cert_from_res_for_dtls()
		? g_tls_certificate_manager->load_certificate_resource(m_ssl_fields->ctx, m_log)
		: g_tls_certificate_manager->load_certificate(m_ssl_fields->ctx, m_log);
	if (!res)
	{
		return false;
	}

	m_ssl_fields->ssl = SSL_new(m_ssl_fields->ctx);

	SSL_set_bio(m_ssl_fields->ssl, m_ssl_fields->read_bio, m_ssl_fields->write_bio);
	SSL_set_mode(m_ssl_fields->ssl, SSL_MODE_AUTO_RETRY);
	SSL_set_read_ahead(m_ssl_fields->ssl, 1);
	//SSL_set_verify(dtls->ssl, (SSL_VERIFY_PEER | SSL_VERIFY_FAIL_IF_NO_PEER_CERT), cb_verify_peer);
	SSL_set_verify(m_ssl_fields->ssl, SSL_VERIFY_NONE, nullptr);
	SSL_set_app_data(m_ssl_fields->ssl, m_ssl_fields);

	BIO_ctrl(m_ssl_fields->read_bio, BIO_CTRL_DGRAM_SET_MTU, 1400, nullptr);
	BIO_ctrl(m_ssl_fields->write_bio, BIO_CTRL_DGRAM_SET_MTU, 1400, nullptr);
	SSL_set_mtu(m_ssl_fields->ssl, 1400);
	BIO_ctrl(m_ssl_fields->write_bio, BIO_C_SET_BUFF_SIZE, 1400, nullptr);
	BIO_ctrl(m_ssl_fields->read_bio, BIO_C_SET_BUFF_SIZE, 1400, nullptr);

	if (m_connection_role == dtls_connection_role_passive_e)
	{
		SSL_set_accept_state(m_ssl_fields->ssl);
	}
	else
	{
		SSL_set_connect_state(m_ssl_fields->ssl);
	}
	
	dtls_set_state(m_ssl_fields, DS_HANDSHAKE, m_log);

	return true;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
int dtls::dtls_state_setup()
{
	m_set_crypto = 0;

	const int cr_keylen = 16;
	const int cr_saltlen = 14;
	X509 *cert = nullptr;
	uint8_t raw_key_data[sizeof(m_crypto_in.key) * 2] = { 0 };
	unsigned char *local_key, *remote_key, *local_salt, *remote_salt;

	if (m_connection_role == dtls_connection_role_active_e)
	{
		if (m_remote_fingerprint == nullptr)
		{
			return -1;
		}

		cert = SSL_get_peer_certificate(m_ssl_fields->ssl);
		if (cert == nullptr)
		{
			return -1;
		}

		int res = dtls_cert_extract_and_check_fingerprint(cert, m_remote_fingerprint);
		X509_free(cert);
		if (res != 0)
		{
			return -1;
		}
	}

	if (!SSL_export_keying_material(m_ssl_fields->ssl, raw_key_data, sizeof(raw_key_data), "EXTRACTOR-dtls_srtp", 19, nullptr, 0, 0))
	{
		PLOG_CRYPTO_ERROR(LOG_PREFIX, "dtls_state_setup --- Key material export failure");
		dtls_set_state(m_ssl_fields, DS_FAIL, m_log);
		return -1;
	}

	if (m_connection_role == dtls_connection_role_passive_e)
	{
		remote_key = raw_key_data;
		local_key = remote_key + cr_keylen;
		remote_salt = local_key + cr_keylen;
		local_salt = remote_salt + cr_saltlen;
	}
	else
	{
		local_key = raw_key_data;
		remote_key = local_key + cr_keylen;
		local_salt = remote_key + cr_keylen;
		remote_salt = local_salt + cr_saltlen;
	}

	/*
	Если у нас серверная роль, то удаленный ключ - это ключ клиента, а мы его используем для дешифровки его сообщений. И наоборот.
	*/

	memcpy(m_crypto_out.key, local_key, cr_keylen);
	memcpy(m_crypto_out.key + cr_keylen, local_salt, cr_saltlen);

	memcpy(m_crypto_in.key, remote_key, cr_keylen);
	memcpy(m_crypto_in.key + cr_keylen, remote_salt, cr_saltlen);

	m_crypto_in.crypto_suite = srtp_profile_aes128_cm_sha1_80;
	m_crypto_out.crypto_suite = srtp_profile_aes128_cm_sha1_80;

	m_set_crypto = 1;

	dtls_set_state(m_ssl_fields, DS_READY, m_log);

	return 0;
}
//------------------------------------------------------------------------------
int dtls::dtls_state_handshake()
{
	int ret;

	if ((ret = SSL_do_handshake(m_ssl_fields->ssl)) != 1)
	{
		switch ((ret = SSL_get_error(m_ssl_fields->ssl, ret)))
		{
		case SSL_ERROR_WANT_READ:
		case SSL_ERROR_WANT_WRITE:
		case SSL_ERROR_NONE:
			break;
		default:
			PLOG_CRYPTO_ERROR(LOG_PREFIX, "dtls_state_handshake --- Handshake failure %d", ret);
			//char buff[1000] = { 0 };
			//MLOG__ERROR(LOG_PREFIX, "dtls_state_handshake --- Handshake failure ERR_get_error : %s", ERR_error_string(ERR_get_error(), buff));

			//dtls_set_state(m_ssl_fields, DS_FAIL, m_MLOG_);
			return -1;
		}
	}

	if (SSL_is_init_finished(m_ssl_fields->ssl))
	{
		dtls_set_state(m_ssl_fields, DS_SETUP, m_log);
	}
	return 0;
}
//------------------------------------------------------------------------------
int dtls::do_dtls(void *bufer, int size, const ip_address_t* addr, uint16_t port)
{
	PLOG_CRYPTO_WRITE(LOG_PREFIX, "do_dtls --- STATE(%s): start...", dtls_state_names(m_ssl_fields->state));

	if (m_ssl_fields->state == DS_READY)
	{
		PLOG_CRYPTO_WRITE(LOG_PREFIX, "do_dtls --- STATE(%s): ehd.", dtls_state_names(m_ssl_fields->state));
		return size;
	}

	if ((bufer == nullptr) || (size == 0))
	{
		// значит мы из понга пришли...
		// проверяем если мы уже отправляли сообщение то значит обрабатывать ничего не надо...
		// т.к. из понга мы запускаем DTLS сессию, а она уже запущена...
		if (m_ssl_fields->count_msg != 0)
		{
			return size;
		}
	}

	//unsigned char* inbuf = (unsigned char*)bufer;
	//int insize = size;
	int r = 0, ret = 0, len;
	size_t bytes;
	unsigned char buf[4096] = "";

	if ((ret = BIO_write(m_ssl_fields->read_bio, bufer, size)) != size && size > 0)
	{
		ERR_error_string(SSL_get_error(m_ssl_fields->ssl, ret), (char*)&buf);
		PLOG_CRYPTO_ERROR(LOG_PREFIX, "do_dtls --- STATE(%s): DTLS packet read err %s", dtls_state_names(m_ssl_fields->state), buf);
	}
	PLOG_CRYPTO_WRITE(LOG_PREFIX, "do_dtls --- STATE(%s): BIO_write : %d", dtls_state_names(m_ssl_fields->state), ret);

	if (m_ssl_fields->state == dtls_state_e::DS_HANDSHAKE)
	{
		r = dtls_state_handshake();
	}
	else if (m_ssl_fields->state == dtls_state_e::DS_READY)
	{
	}
	else if (m_ssl_fields->state == dtls_state_e::DS_FAIL)
	{
	}
	else if (m_ssl_fields->state == dtls_state_e::DS_INVALID)
	{
	}

	if (m_ssl_fields->state == dtls_state_e::DS_SETUP)
	{
		r = dtls_state_setup();
	}

	len = 0;
	if ((len = BIO_read(m_ssl_fields->write_bio, buf, sizeof(buf))) > 0)
	{
		PLOG_CRYPTO_WRITE(LOG_PREFIX, "do_dtls --- STATE(%s): BIO_read : %d", dtls_state_names(m_ssl_fields->state), len);

		bytes = len;

		socket_address saddr(addr, port);
		if ((m_rtp == nullptr) || (!m_rtp->send_data(buf, uint32_t(bytes), &saddr)))
		{
			PLOG_CRYPTO_ERROR(LOG_PREFIX, "do_dtls --- STATE(%s): DTLS packet send_to FAIL. send(%d), addr(%s), port(%d), error: r/n/",
				dtls_state_names(m_ssl_fields->state), bytes, addr->to_string(), port, std_get_error);
		}
		else
		{
			// считаем колличество отправленных сообщений (для таймаута)
			m_ssl_fields->count_msg++;

			if (m_ssl_fields->count_msg > 10)
			{
				dtls_set_state(m_ssl_fields, DS_INVALID, m_log);
			}
		}
		PLOG_CRYPTO_WRITE(LOG_PREFIX, "do_dtls --- STATE(%s): DTLS packet send_to: send(%d), addr(%s), port(%d)", 
			dtls_state_names(m_ssl_fields->state), bytes, addr->to_string(), port);
	}
	else
	{
		PLOG_CRYPTO_WRITE(LOG_PREFIX, "do_dtls --- STATE(%s): BIO_read : %d", dtls_state_names(m_ssl_fields->state), len);
	}

	PLOG_CRYPTO_WRITE(LOG_PREFIX, "do_dtls --- STATE(%s): ehd.", dtls_state_names(m_ssl_fields->state));
	return r;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool dtls::chek_crypto()
{
	if (m_set_crypto == 1)
	{
		return true;
	}
	return false;
}
//------------------------------------------------------------------------------
rtp_crypto_param_t* dtls::get_crypto_in()
{
	return &m_crypto_in;
}
//------------------------------------------------------------------------------
rtp_crypto_param_t* dtls::get_crypto_out()
{
	return &m_crypto_out;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void dtls::reset()
{
	if (m_ssl_fields == nullptr)
	{
		return;
	}

	dtls_set_state(m_ssl_fields, DS_HANDSHAKE, m_log);
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
