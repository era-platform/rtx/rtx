/* RTX H.248 Media Gate. Crypto Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "tls_ctx.h"
#include "tls_certificate_manager.h"

#include <openssl/ssl.h>
#include <openssl/bio.h>
#include <openssl/err.h>
//-----------------------------------------------
// 
//-----------------------------------------------
tls_ctx_t::tls_ctx_t(rtl::Logger& log, tls_connection_role_t role) : m_log(log)
{
	m_ctx = nullptr;
	m_web = nullptr;
	m_out = nullptr;
	m_ssl = nullptr;
	m_role = role;
	m_handshake_done = false;
}
//-----------------------------------------------
// 
//-----------------------------------------------
tls_ctx_t::~tls_ctx_t()
{
	destroy();
}
//-----------------------------------------------
//
//-----------------------------------------------
bool tls_ctx_t::init(tls_version_t version)
{
	if (!init_CTX(version))
	{
		return false;
	}

	if (!init_BIO())
	{
		return false;
	}

	if (!init_SSL())
	{
		return false;
	}

	return true;
}
//-----------------------------------------------
// 
//-----------------------------------------------
void tls_ctx_t::destroy()
{
	if (m_ssl != nullptr)
	{
		SSL_free(m_ssl);
		m_ssl = nullptr;
	}
	if (m_ctx != nullptr)
	{
		SSL_CTX_free(m_ctx);
		m_ctx = nullptr;
	}
	if (m_out != nullptr)
	{
		//BIO_free(m_out);
		m_out = nullptr;
	}
	if (m_web != nullptr)
	{
		//BIO_free_all(m_web);
		m_web = nullptr;
	}
	m_handshake_done = false;
}
//-----------------------------------------------
// 
//-----------------------------------------------
bool tls_ctx_t::init_CTX(tls_version_t version)
{
	// ���������������� ��� � ����. �������� ���...

	const SSL_METHOD* method = nullptr;
	if (m_role == tls_connection_role_t::tls_connection_role_server_e)
	{
		switch (version)
		{
		case TLSv1:
			method = TLSv1_server_method();
			break;
		case TLSv1_1:
			method = TLSv1_1_server_method();
			break;
		case TLSv1_2:
			method = TLSv1_2_server_method();
			break;
		}
	}
	else
	{
		switch (version)
		{
		case TLSv1:
			method = TLSv1_client_method();
			break;
		case TLSv1_1:
			method = TLSv1_1_client_method();
			break;
		case TLSv1_2:
			method = TLSv1_2_client_method();
			break;
		}
	}

	if (method == nullptr)
	{
		int err = 0;// SSL_get_error(nullptr, 0);
		char buf[1024] = { 0 };
		int err2;
		char* cp = ERR_error_string(err2 = ERR_get_error(), buf);
		MLOG_ERROR("TLS", "tls_ctx_t::init_CTX --- method v%d fail (%d/%d) %s.", version, err, err2, cp);
		return false;
	}

	m_ctx = SSL_CTX_new(method);
	if (m_ctx == nullptr)
	{
		int err = SSL_get_error(nullptr, 0);
		char buf[1024] = { 0 };
		ERR_error_string(ERR_get_error(), buf);
		MLOG_ERROR("TLS", "tls_ctx_t::init_CTX --- SSL_CTX_new fail(%d) -- %s", err, buf);
		return false;
	}

	if ((g_tls_certificate_manager == nullptr) || (!g_tls_certificate_manager->load_certificate(m_ctx, &m_log)))
	{
		MLOG_ERROR("TLS", "tls_ctx_t::init_CTX --- load_certificates fail. (%p)", g_tls_certificate_manager);
		return false;
	}

	/* Cannot fail ??? */
	SSL_CTX_set_verify(m_ctx, SSL_VERIFY_PEER, NULL);

	/* Cannot fail ??? */
	SSL_CTX_set_verify_depth(m_ctx, 4);

	/* Cannot fail ??? */
	const long flags = SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3 | SSL_OP_NO_COMPRESSION | SSL_OP_NO_TICKET | SSL_OP_NO_SESSION_RESUMPTION_ON_RENEGOTIATION;
	SSL_CTX_set_options(m_ctx, flags);

	SSL_CTX_set_mode(m_ctx, SSL_MODE_AUTO_RETRY);

	//SSL_CTX_set_info_callback(m_ctx, apps_ssl_info_callback_client);

	return true;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool tls_ctx_t::init_BIO()
{
	m_web = BIO_new(BIO_s_mem());
	if (m_web == nullptr)
	{
		MLOG_ERROR("TLS", "tls_ctx_t::init_BIO --- BIO_new fail.");
		return false;
	}

	m_out = BIO_new(BIO_s_mem());
	if (m_out == nullptr)
	{
		MLOG_ERROR("TLS", "tls_ctx_t::init_BIO --- BIO_new fail.");
		return false;
	}

	BIO_set_mem_eof_return(m_web, -1);
	BIO_set_mem_eof_return(m_out, -1);

	return true;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool tls_ctx_t::init_SSL()
{
	if (m_ctx == nullptr)
	{
		return false;
	}

	m_ssl = SSL_new(m_ctx);
	if (m_ssl == nullptr)
	{
		MLOG_ERROR("TLS", "tls_ctx_t::init_SSL --- SSL_new fail.");
		return false;
	}

	SSL_set_bio(m_ssl, m_web, m_out);
	SSL_set_mode(m_ssl, SSL_MODE_AUTO_RETRY);
	SSL_set_read_ahead(m_ssl, 1);
	SSL_set_verify(m_ssl, SSL_VERIFY_NONE, NULL);

	if (m_role == tls_connection_role_t::tls_connection_role_server_e)
	{
		SSL_set_accept_state(m_ssl);
	}
	else
	{
		SSL_set_connect_state(m_ssl);
	}

	return true;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool tls_ctx_t::check_cert()
{
	/* Step 1: verify a server certificate was presented during the negotiation */
	X509* cert = SSL_get_peer_certificate(m_ssl);
	if (cert)
	{
		X509_free(cert);/* Free immediately */
	}
	else if (cert == NULL)
	{
		MLOG_ERROR("TLS", "tls_ctx_t::check_cert --- SSL_get_peer_certificate fail.");
		return false;
	}

	/* Step 2: verify the result of chain verification */
	if (SSL_get_verify_result(m_ssl) != X509_V_OK)
	{
		MLOG_ERROR("TLS", "tls_ctx_t::check_cert --- SSL_get_verify_result fail.");
		return false;
	}

	return true;
}
//------------------------------------------------------------------------------
// ���������� ���� ������ � OPENSSL.
//------------------------------------------------------------------------------
int tls_ctx_t::rx_push(const char* buff, int buff_len)
{
	int len = 0;

	if (m_role == tls_connection_role_t::tls_connection_role_server_e)
	{
		len = BIO_write(m_web, buff, buff_len);
	}
	else
	{
		len = BIO_write(m_web, buff, buff_len);
	}

	return len;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
int tls_ctx_t::tx_push(const char* buff, int buff_len)
{
	int len = 0;
	//MLOG_CALL(L"tls", L"tls_ctx_t::tx_push(%d, %p, %d)", m_role, buff, buff_len);
	if (m_role == tls_connection_role_t::tls_connection_role_server_e)
	{
		if (m_handshake_done)
		{
			len = SSL_write(m_ssl, buff, buff_len);
		}
		else
		{
			len = BIO_write(m_web, buff, buff_len);
		}
	}
	else
	{
		if (m_handshake_done)
		{
			len = SSL_write(m_ssl, buff, buff_len);
		}
		else
		{
			len = BIO_write(m_web, buff, buff_len);
			SSL_do_handshake(m_ssl);
		}
	}

	return len;
}
//------------------------------------------------------------------------------
// �������� ������������ ������ �� OPENSSL.
//------------------------------------------------------------------------------
int tls_ctx_t::rx_pull(char* buff, int buff_len)
{
	int len = 0;
	if (m_role == tls_connection_role_t::tls_connection_role_server_e)
	{
		if (m_handshake_done)
		{
			len = (int)BIO_ctrl_pending(m_web);
			if (len > 0)
			{
				//len = SSL_read(m_ssl, buff, buff_len);

				int res = SSL_read(m_ssl, buff, buff_len);
				if (res < 0)
				{
					int err = SSL_get_error(m_ssl, res);

					if (err != SSL_ERROR_WANT_READ)
					{
						char buf[1024] = { 0 };
						ERR_error_string(ERR_get_error(), buf);
						MLOG_ERROR("tls", "tls_ctx_t::rx_pull error %d %s", err, buf);
						return -1;
					}
					else
					{
						res = 0;
					}
				}

				len = res;
			}
		}
		else
		{
			if (SSL_do_handshake(m_ssl) == 1)
			{
				m_handshake_done = true;
			}

			len = BIO_read(m_out, buff, buff_len);
		}
	}
	else
	{
		if (m_handshake_done)
		{
			len = (int)BIO_ctrl_pending(m_web);
			if (len > 0)
			{
				int to_read = MIN(buff_len, len);
				len = SSL_read(m_ssl, buff, to_read);
			}
		}
		else
		{
			len = BIO_read(m_out, buff, buff_len);

			const char* cstr = SSL_state_string(m_ssl);
			if (strcmp("SSLOK ", cstr) == 0)
			{
				m_handshake_done = true;
			}
		}
	}

	return len;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
int tls_ctx_t::tx_pull(char* buff, int buff_len)
{
	int len = 0;

	if (m_role == tls_connection_role_t::tls_connection_role_server_e)
	{
		if (m_handshake_done)
		{
			len = (int)BIO_ctrl_pending(m_out);
			if (len > 0)
			{
				len = BIO_read(m_out, buff, buff_len);
			}
		}
		else
		{
			if (SSL_do_handshake(m_ssl) == 1)
			{
				m_handshake_done = true;
			}
			len = BIO_read(m_out, buff, buff_len);
		}
	}
	else
	{
		if (m_handshake_done)
		{
			len = (int)BIO_ctrl_pending(m_out);
			if (len > 0)
			{
				len = BIO_read(m_out, buff, buff_len);
			}
		}
		else
		{
			len = BIO_read(m_out, buff, buff_len);

			const char* cstr = SSL_state_string(m_ssl);
			if (strcmp("SSLOK ", cstr) == 0)
			{
				m_handshake_done = true;
			}
		}
	}

	return len;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
RTX_CRYPTO_API int tls_ctx_t::get_state()
{
	if (m_ssl != nullptr)
	{
		return SSL_get_state(m_ssl);
	}

	return 0;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
RTX_CRYPTO_API const char* tls_ctx_t::get_state_string()
{
	if (m_ssl != nullptr)
	{
		return SSL_state_string(m_ssl);
	}

	return 0;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
RTX_CRYPTO_API const char* tls_ctx_t::get_state_string_long()
{
	if (m_ssl != nullptr)
	{
		return SSL_state_string_long(m_ssl);
	}

	return 0;
}
//-----------------------------------------------
