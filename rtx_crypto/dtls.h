﻿/* RTX H.248 Media Gate. Crypto Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_crypto.h"
#include "std_logger.h"
#include "sdp_fingerprint.h"
#include "dtls_define.h"
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
class ip_address_t;
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
class RTX_CRYPTO_API dtls
{
								dtls(const dtls&);
	dtls&						operator = (const dtls&);
								dtls(dtls&&);
	dtls&						operator = (dtls&&);

public:
	dtls(rtl::Logger* log, net_sender_handler* rtp_handler);
	virtual						~dtls();

	void						dispose();

	void						reset();

	// Подготавливаем фингерпринт по сертификату и записываем его в буфер.
	// Подготавливаем фингерпринт на основании сертификата из ресурса.
	bool						prepare_fingerprint(const char* algorithm);
	// Подготавливаем фингерпринт на основании сертификата из ресурса и алгоритма шифрования из удаленного фингерпринта.
	bool						prepare_fingerprint();
	const uint8_t*				get_fingerprint();
	uint32_t					get_fingerprint_length();
	const char*					get_fingerprint_algorithm();
	bool						set_remote_fingerprint(const char* algorithm, const uint8_t* fingerprint, int length);

	dtls_connection_role_t		get_dtls_connection_role();
	void						set_dtls_connection_role(dtls_connection_role_t role);

	// Проверяем этап передачи ключей. Если true значит ключи сформированы.
	bool						chek_crypto();
	// DTLS отработал прошло успешно.
	bool						chek_dtls();

	// Получить крипто параметры для расшифрования входящего трафика.
	rtp_crypto_param_t*			get_crypto_in();
	// Получить крипто параметры для шифрования исходящего трафика.
	rtp_crypto_param_t*			get_crypto_out();

	// Подготавливаем параметры для запуска DTLS. Файлы сертификатов грузятся из ресурсов.
	bool						setup_dtls();

	// Обработка очередного пакета DTLS.
	int							do_dtls(void *buf, int size, const ip_address_t* addr, uint16_t port);

private:
	int							dtls_state_setup();
	int							dtls_state_handshake();

private:
	rtl::Logger*						m_log;
	net_sender_handler*			m_rtp;

	sdp_fingerprint_t*			m_local_fingerprint;
	sdp_fingerprint_t*			m_remote_fingerprint;
	
	// Показатель роли в каком режиме работает DTLS. Сервер или клиент.
	dtls_connection_role_t		m_connection_role;

	// Структура с дополнителными полями которые используются в OpenSSL.
	struct dtls_ssl_fields*		m_ssl_fields;

	// Крипто параметры для расшифрования входящего трафика.
	rtp_crypto_param_t			m_crypto_in;
	// Крипто параметры для шифрования исходящего трафика.
	rtp_crypto_param_t			m_crypto_out;

	// Счетчик формирования крипто параметров. Если 1 то параметры сформированы и мы 
	// можем их передавать в канал для шифровки и расшифровки трафика.
	uint32_t					m_set_crypto;
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
