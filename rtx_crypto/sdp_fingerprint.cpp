﻿/* RTX H.248 Media Gate. Crypto Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sdp_fingerprint.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
sdp_fingerprint_t::sdp_fingerprint_t() : m_hash_data(nullptr), m_hash_length(0)
{
	memset(m_hash_function, 0, sizeof(m_hash_function));
	memset(m_setup, 0, sizeof(m_setup));

	m_role = fingerprint_role_t::actpass;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
sdp_fingerprint_t::~sdp_fingerprint_t()
{
	cleanup();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void sdp_fingerprint_t::set_hash(const char* hash_function, const uint8_t* hash, int length)
{
	cleanup();

	if (hash_function == nullptr || *hash_function == 0 || hash == nullptr || length <= 0)
	{
		return;
	}

	size_t copy_len = MIN(sizeof(m_hash_function) - 1, strlen(hash_function));
	strncpy(m_hash_function, hash_function, copy_len);

	m_hash_data = (uint8_t*)MALLOC(length);
	memcpy(m_hash_data, hash, length);

	m_hash_length = length;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void sdp_fingerprint_t::parse(const char* fingerprint)
{
	cleanup();

	const char* ras = strchr(fingerprint, ':');

	if (ras == nullptr)
		return;

	if (*(ras + 1) == ' ')
	{
		ras++;
	}
	ras++;

	const char* s = strchr(ras, ' ');
	if (s == nullptr)
		return;
	
	// копируем имя алгоритма 
	size_t copy_len = MIN(sizeof(m_hash_function) - 1, size_t(s - ras));
	strncpy(m_hash_function, ras, copy_len);
	m_hash_function[copy_len] = 0;

	s++;

	int length = int(strlen(s));

	// расчет длины хеш строки

	m_hash_length = (length + 1) / 3;

	m_hash_data = (uint8_t*)MALLOC(m_hash_length);

	for (int i = 0; i < m_hash_length; i++)
	{
		char ch = *s++;
		uint8_t val = 0;

		if (ch >= '0' && ch <= '9')
		{
			val = ch - '0';
		}
		else if (ch >= 'A' && ch <= 'F')
		{
			val = ch - 'A' + 10;
		}
		else if (ch >= 'a' && ch <= 'f')
		{
			val = ch - 'a' + 10;
		}
		else
		{
			cleanup();
			return;
		}

		val <<= 4;

		ch = *s++;

		if (ch >= '0' && ch <= '9')
		{
			val |= ch - '0';
		}
		else if (ch >= 'A' && ch <= 'F')
		{
			val |= ch - 'A' + 10;
		}
		else if (ch >= 'a' && ch <= 'f')
		{
			val |= ch - 'a' + 10;
		}
		else
		{
			cleanup();
			return;
		}

		m_hash_data[i] = val;

		// передвинем указатель вперед (пропустим ':')
		s++;
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int sdp_fingerprint_t::write_to(char* buffer, int size)
{
	if (m_hash_data == nullptr || m_hash_function[0] == 0 || size < (20 + m_hash_length * 3))
		return 0;
	
	char* initial_ptr = buffer;

	int written = std_snprintf(buffer, size, "a=fingerprint:%s ", m_hash_function);

	buffer += written;
	size -= written;

	for (int i = 0; i < m_hash_length; i++)
	{
		written = std_snprintf(buffer, size, "%02X", m_hash_data[i]);
		buffer += written;
		size -= written;

		if (i < m_hash_length - 1)
		{
			*buffer++ = ':';
			size--;
		}
	}

	*buffer++ = '\r';
	*buffer++ = '\n';

	return int(buffer - initial_ptr);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool sdp_fingerprint_t::compare(sdp_fingerprint_t* fingerprint)
{
	if (fingerprint == nullptr)
	{
		return false;
	}

	if ((m_hash_data == nullptr) || (m_hash_length == 0))
	{
		return false;
	}

	if (fingerprint->get_hash_length() != m_hash_length)
	{
		return false;
	}

	const uint32_t size_buff = 120;

	char buff1[size_buff] = { 0 };
	char buff2[size_buff] = { 0 };

	uint32_t size1 = write_to(buff1, size_buff);
	uint32_t size2 = fingerprint->write_to(buff2, size_buff);

	if (size1 != size2)
	{
		return false;
	}

	if (memcmp(buff1, buff2, size2) != 0)
	{
		return false;
	}

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool sdp_fingerprint_t::copy_from(const sdp_fingerprint_t* fingerprint)
{
	cleanup();

	if (fingerprint == nullptr)
	{
		return false;
	}

	memcpy(m_hash_function, fingerprint->m_hash_function, 16);

	m_hash_length = fingerprint->m_hash_length;
	m_hash_data = (uint8_t*)MALLOC(m_hash_length);
	memcpy(m_hash_data, fingerprint->m_hash_data, m_hash_length);

	memcpy(m_setup, fingerprint->m_setup, 16);
	m_role = fingerprint->m_role;

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void sdp_fingerprint_t::cleanup()
{
	memset(m_hash_function, 0, sizeof(m_hash_function));

	if (m_hash_data != nullptr)
	{
		FREE(m_hash_data);
		m_hash_data = nullptr;
		m_hash_length = 0;
	}

	memset(m_setup, 0, sizeof(m_setup));

	m_role = fingerprint_role_t::actpass;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool sdp_fingerprint_t::read_role(const char* role)
{
	if (role == nullptr)
	{
		return false;
	}

	size_t length = MIN(sizeof(m_setup), strlen(role));
	strncpy(m_setup, role, length);

	rtl::String role_str(role);
	if (role_str.indexOf("active") != BAD_INDEX)
	{
		m_role = fingerprint_role_t::active;
	}
	else if (role_str.indexOf("passive") != BAD_INDEX)
	{
		m_role = fingerprint_role_t::passive;
	}
	else
	{
		m_role = fingerprint_role_t::actpass;
	}

	return true;
}
//------------------------------------------------------------------------------
const char* sdp_fingerprint_t::write_role()
{
	//a=setup:actpass
	//a=setup:passive
	//a=setup:active

	rtl::String role_str;
	if (m_role == fingerprint_role_t::actpass)
	{
		role_str.append("a=setup:actpass");
	}
	else if (m_role == fingerprint_role_t::passive)
	{
		role_str.append("a=setup:passive");
	}
	else
	{
		role_str.append("a=setup:active");
	}

	size_t length = MIN(sizeof(m_setup), (size_t)role_str.getLength());
	strncpy(m_setup, role_str, length);
	m_setup[length] = 0;

	return m_setup;
}
//------------------------------------------------------------------------------
fingerprint_role_t sdp_fingerprint_t::get_role()
{
	return m_role;
}
//------------------------------------------------------------------------------
void sdp_fingerprint_t::set_role(fingerprint_role_t role)
{
	m_role = role;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
