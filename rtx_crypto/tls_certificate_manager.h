﻿/* RTX H.248 Media Gate. Crypto Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_crypto.h"

 //------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
#define MG_PATH_TO_CERT "mg-cert-path"
#define MG_PATH_TO_KEY "mg-key-path"
#define MG_CERT_PASS "mg-cert-pass"
//------------------------------------------------------------------------------
// Инициализация библиотек OpenSSL. Передаем путь до pfx сертификата.
//------------------------------------------------------------------------------
RTX_CRYPTO_API void init_openssl_lib(const char* path_pfx, const char* password_pfx, rtl::Logger* log);
//------------------------------------------------------------------------------
// Инициализация библиотек OpenSSL. Передаем путь до crt сертификата.
//------------------------------------------------------------------------------
RTX_CRYPTO_API void init_openssl_lib(const char* path_crt, const char* path_kye, const char* password_crt, rtl::Logger* log);
//------------------------------------------------------------------------------
// Инициализация библиотек OpenSSL. Сертификат извлекается из хранилища сетификатов windows.
//------------------------------------------------------------------------------
//RTX_CRYPTO_API void init_openssl_lib(const char* CN);
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
RTX_CRYPTO_API void destroy_openssl_lib();
//------------------------------------------------------------------------------
// Выставить флаг использования сертификата из ресурсов для DTLS.
//------------------------------------------------------------------------------
RTX_CRYPTO_API void set_flag_use_cert_from_res_for_dtls(bool use);
//------------------------------------------------------------------------------
// Проверка использования сертификата из ресурсов для DTLS.
//------------------------------------------------------------------------------
RTX_CRYPTO_API bool use_cert_from_res_for_dtls();
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
struct evp_pkey_st;
struct x509_st;
struct stack_st_X509;
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
class tls_certificate_manager_t
{
	tls_certificate_manager_t(const tls_certificate_manager_t&);
	tls_certificate_manager_t&	operator = (const tls_certificate_manager_t&);
	tls_certificate_manager_t(tls_certificate_manager_t&&);
	tls_certificate_manager_t&	operator = (tls_certificate_manager_t&&);

public:
	tls_certificate_manager_t();
	virtual						~tls_certificate_manager_t();

	// Инициализация менеджера на основе сертификата pfx.
	bool						init_pfx(const char* path_pfx, const char* password);
	// Инициализация менеджера на основе PEM-ключа и PEM-сертификата.
	bool						init_pem(const char* cert_file, const char* key_file, const char* password);
	// Инициаллизация на основе хранилища сертификатоф Windiws
	//bool						init_ca(const char* CN);

	void						destroy();

	// Смена текущего сертификата на основе pfx..
	bool						change_pfx(const char* path_pfx, const char* password);
	// Смена текущего сертификата на основе PEM-ключа и PEM-сертификата.
	bool						change_pem(const char* cert_file, const char* key_file, const char* password);

	// Создание PEM-ключа и PEM-сертификата.
	bool						create(char* key_file, char* cert_file);

	// Получение ключа в структуре OpenSSl.
	evp_pkey_st*				get_key();
	// Получение сертификата в структуре OpenSSl.
	x509_st*					get_cert();
	// Получение ключа в структуре OpenSSl. (из ресурсов)
	evp_pkey_st*				get_key_res();
	// Получение сертификата в структуре OpenSSl. (из ресурсов)
	x509_st*					get_cert_res();

	bool						load_certificate(struct ssl_ctx_st* ctx, rtl::Logger* m_log);
	bool						load_certificate(struct ssl_st* ssl, rtl::Logger* m_log);

	bool						load_certificate_resource(struct ssl_ctx_st* ctx, rtl::Logger* m_log);
	bool						load_certificate_resource(struct ssl_st* ssl, rtl::Logger* m_log);

private:
	// Создание PEM-ключа и PEM-сертификата с нуля. (Пока не реализовано. Просто пример кода.)
	bool						create_certificate(const char* key_file, const char* cert_file);
	bool						contains_file(const char* filename);
	bool						from_resources(const char* password);

	const char*					get_CN(char* params) const;

private:
	void*						m_p12;

	evp_pkey_st*				m_pkey;
	x509_st*					m_cert;
	evp_pkey_st*				m_pkey_res;
	x509_st*					m_cert_res;
	stack_st_X509*				m_ca;

	bool						m_from_resource;
};
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
extern tls_certificate_manager_t*	g_tls_certificate_manager;
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
