/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_value_auth.h"
//--------------------------------------
//
//--------------------------------------
inline const char* SKIP_WS(const char* text)
{
	char ch = *text;

	while (ch != 0 && ch == ' ' && ch == '\t' && ch == '\r' && ch == '\n') ch = *(++text);

	return text;
}
//--------------------------------------
//
//--------------------------------------
sip_value_auth_t::~sip_value_auth_t()
{
	cleanup();
}
//--------------------------------------
//
//--------------------------------------
rtl::String& sip_value_auth_t::prepare(rtl::String& stream) const
{
	stream << m_auth_scheme << ' ';
	for (int i = 0; i < m_auth_params.getCount(); i++)
	{
		const mime_param_t* param = m_auth_params.getAt(i);
		stream << param->getName() << '=' << param->getValue();
		if (i < m_auth_params.getCount() - 1)
			stream << ", ";
	}

	return stream;
}
bool sip_value_auth_t::parse()
{
	bool result;

	read_from(m_value, result);

	return result;

	/*
	int ptr = 0;
	int eol = 0;
	mbstring_t entry_name;
	mbstring_t entry_value;

	m_auth_params.remove_all();
	m_auth_scheme.setEmpty();

	//proxy-authenticate: Digest realm=\"Realm\",nonce=\"MTQ4NDUzMzI5MzU3NTc0YjY1ZTEyOGZkYTdjYTUyZTA2NThjYzJhNGQ0ZTdh\",stale=false,algorithm=MD5,qop=\"auth,auth-int\"\r\n\

	// schema : Digest
	ptr = m_value.indexOf(' ');

	if (ptr == -1)
	{
	m_auth_scheme = m_value;
	return true;
	}
	else
	{
	m_auth_scheme = m_value.substring(0, ptr).trim();
	}


	while ((eol = m_value.indexOfAny("=,\r\n", eol)) != -1)
	{
	entry_name = m_value.substring(ptr, eol - ptr);
	entry_value = read_next_value(m_value, eol);

	m_auth_params.set(entry_name, entry_value);

	// scroll whitespaces ' ', '\t', "\r ', '\n\t', e.t.c

	while (m_value[eol] != 0 && m_value[eol] != ' ' && m_value[eol] != '\t' && m_value[eol] != '\r' && m_value[eol] != '\n')
	{
	eol++;
	}

	ptr = ++eol;
	}
	*/
}
//--------------------------------------
//
//--------------------------------------
void sip_value_auth_t::cleanup()
{
	m_auth_scheme.setEmpty();
	m_auth_params.cleanup();

	sip_value_t::cleanup();
}
//--------------------------------------
//
//--------------------------------------
sip_value_auth_t& sip_value_auth_t::assign(const sip_value_auth_t& v)
{
	sip_value_t::assign(v);

	m_auth_params = v.m_auth_params;
	m_auth_scheme = v.m_auth_scheme;

	return *this;
}
//--------------------------------------
//
//--------------------------------------
sip_value_t* sip_value_auth_t::clone() const
{
	return NEW sip_value_auth_t(*this);
}
//--------------------------------------
//
//--------------------------------------
const char* sip_value_auth_t::read_parameter(const char* stream, bool& result)
{
	// =\"Realm\", ...
	// ^-^------^^

	// =auth, ...
	// ^----^

	// =auth\r\n ...
	// ^----^

	// , ...
	// ^

	// \r\n ...
	// ^

	const char* ptr = SKIP_WS(stream);
	const char* eq = strchr(ptr, '=');

	if (eq == nullptr)
	{
		result = false;
		return nullptr;
	}

	rtl::String name, value;
	name.assign(ptr, int(eq - ptr)).trim();
	eq++;

	const char* cp = strpbrk(eq, "\",\r\n");

	if (cp == nullptr)
	{
		value.assign(eq);
	}
	else if (*cp == '\"')
	{
		const char* cnp = strchr(cp+1, '\"');
		if (cnp == nullptr)
		{
			result = false;
			return nullptr;
		}
		
		value.assign(cp, int(cnp - cp) + 1);
		cp = cnp + 1;
	}
	else
	{
		value.assign(eq, int(cp - eq));
	}

	result = true;
	m_auth_params.set(name, value);

	return cp;
}
//--------------------------------------
//
//--------------------------------------
const char* sip_value_auth_t::read_from(const char* stream, bool& result)
{
	const char* ptr = stream;
	rtl::String entry_name;
	rtl::String entry_value;

	m_value.setEmpty();
	m_auth_params.cleanup();
	m_auth_scheme.setEmpty();

	ptr = strpbrk(stream, " =,\r\n");

	if (ptr == nullptr || *ptr != ' ')
	{
		result = false;
		return nullptr;
	}

	m_auth_scheme.assign(stream, PTR_DIFF(ptr, stream));
	++ptr;

	ptr = SKIP_WS(ptr);

	while (ptr != nullptr && *ptr != 0)
	{
		ptr = read_parameter(ptr, result);

		// check end of value!
		if (ptr == nullptr)
		{
			return nullptr;
		}
		
		// we can optimize it
		if (*ptr == '\r' && *(ptr + 1) == '\n')
		{
			ptr += 2;

			if (*ptr != ' ' && *ptr != '\t')
			{
				return ptr;
			}
		}

		if (*ptr == ',')
		{
			ptr++;
		}

		ptr = SKIP_WS(ptr);
	}

	return ptr;
}
//--------------------------------------
