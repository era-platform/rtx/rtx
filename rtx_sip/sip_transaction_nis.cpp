/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "sip_transaction_nis.h"
#include "sip_transaction_manager.h"
//--------------------------------------
//
//--------------------------------------
static const char* GetStateName(int state)
{
	static const char* names[] = { "Idle", "Trying", "Proceeding", "Completed", "Terminated" };

	return state >= 0 && state <= sip_transaction_nis_t::nis_transaction_state_terminated_e ? names[state] : "Unknown";
}
//--------------------------------------
//
//--------------------------------------
sip_transaction_nis_t::sip_transaction_nis_t(const trans_id& transactionId, sip_transaction_manager_t& manager) :
	sip_transaction_t(transactionId, sip_transaction_nist_e, manager),	m_has_sent_provisional_response(false)
{
	SLOG_TRANS(get_type_name(), "%s Created with Id: %s", m_internal_id, (const char*)transactionId.AsString());
}
//--------------------------------------
//
//--------------------------------------
sip_transaction_nis_t::~sip_transaction_nis_t()
{
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_nis_t::process_sip_timer_event(sip_transaction_timer_t timerType, uint32_t interval)
{
	SLOG_TRANS(get_type_name(), "%s Timer event %s state %s",
			m_internal_id, sip_timer_manager_t::GetTimerName(timerType), GetStateName(m_state));

	if (timerType == sip_timer_J_e)
	{
		SLOG_TRANS(get_type_name(), "%s Timer G :  Terminating", m_internal_id);
		set_state(nis_transaction_state_terminated_e);
		destroy();
	}
	else if (timerType == sip_timer_life_span_e)
	{
		set_state(nis_transaction_state_terminated_e);
		m_manager.process_timer_expire_event(timerType, *this);
		destroy();
	}
	else
	{
		SLOG_WARN(get_type_name(), "%s %s : Invalid timer tick received!", m_internal_id, sip_timer_manager_t::GetTimerName(timerType));
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_nis_t::process_sip_transaction_event(sip_message_t& message)
{
	/*if (m_call_id.isEmpty())
	{
		m_call_id = message.CallId();
	}*/

	SLOG_TRANS(get_type_name(), "%s State.%s", m_internal_id, GetStateName(m_state));

	switch(m_state)
	{
	case nis_transaction_state_idle_e:
		process_idle_state(message);
		break;
	case nis_transaction_state_trying_e:
		process_trying_state(message);
		break;
	case nis_transaction_state_proceeding_e:
		process_proceeding_state(message);
		break;
	case nis_transaction_state_completed_e:
		process_completed_state(message);
		break;
	case nis_transaction_state_terminated_e:
		break;
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_nis_t::process_idle_state(sip_message_t& message)
{

	/*  The state machine is initialized in the "Trying" state and is passed
	a request other than INVITE or ACK when initialized.  This request is
	passed up to the TU.  Once in the "Trying" state, any further request
	retransmissions are discarded.  A request is a retransmission if it
	matches the same server transaction, using the rules specified in
	Section 17.2.3.*/


	if (!message.is_request())
	{
		SLOG_TRANS(get_type_name(), "%s State.Idle : message is request!", m_internal_id);
		return;
	}

	set_opening_request(message);
	
	SLOG_TRANS(get_type_name(), "%s State.Idle --> State.Trying", m_internal_id);
	
	set_state(nis_transaction_state_trying_e);

	m_manager.process_received_message_event(message, *this);
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_nis_t::process_trying_state(sip_message_t& message)
{
	/*
	While in the "Trying" state, if the TU passes a provisional response
	to the server transaction, the server transaction MUST enter the
	"Proceeding" state.  The response MUST be passed to the transport
	layer for transmission.  Any further provisional responses that are
	received from the TU while in the "Proceeding" state MUST be passed
	to the transport layer for transmission.  If a retransmission of the
	request is received while in the "Proceeding" state, the most
	recently sent provisional response MUST be passed to the transport
	layer for retransmission.  If the TU passes a final response (status
	codes 200-699) to the server while in the "Proceeding" state, the
	transaction MUST enter the "Completed" state, and the response MUST
	be passed to the transport layer for transmission.
	*/

	if (!message.is_request())
	{
		m_last_response = message;

		STR_COPY(m_ua_core_name, m_last_response.GetUACoreName());

		if (message.Is1xx())
		{
			SLOG_TRANS(get_type_name(), "%s (1xx) State.Trying --> State.Proceeding", m_internal_id);
			set_state(nis_transaction_state_proceeding_e);

			m_has_sent_provisional_response = true;
			send_message_to_transport(m_last_response);
		}
		else
		{
			m_last_response = message;
			SLOG_TRANS(get_type_name(), "%s (!1xx) State.Trying --> State.Completed", m_internal_id);
			send_message_to_transport(m_last_response);

			if (!is_reliable_transport())
			{
				set_state(nis_transaction_state_completed_e);
				start_timer(sip_timer_J_e, SIP_INTERVAL_J);
			}
			else
			{
				set_state(nis_transaction_state_terminated_e);
				destroy();
			}
		}
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_nis_t::process_proceeding_state(sip_message_t& message)
{
	if (message.is_request() && m_has_sent_provisional_response)
	{
		/// retrnasmit the last provisional response we sent
		SLOG_TRANS(get_type_name(), "%s State.Proceeding : send message to transport", m_internal_id);
		send_message_to_transport(m_last_response);
	}
	else if (!message.is_request())
	{

		m_last_response = message;


		if (message.Is1xx())
		{
			SLOG_TRANS(get_type_name(), "%s (1xx) State.Proceeding : send last response", m_internal_id);

			m_has_sent_provisional_response = true;
			send_message_to_transport(m_last_response);
		}
		else
		{
			///this is a 2xx-6xx response
			send_message_to_transport(m_last_response);

			if (!is_reliable_transport())
			{
				SLOG_TRANS(get_type_name(), "%s (2xx-6xx) State.Proceeding --> State.Completed", m_internal_id);

				set_state(nis_transaction_state_completed_e);
				start_timer(sip_timer_J_e, SIP_INTERVAL_J);
			}
			else
			{
				SLOG_TRANS(get_type_name(), "%s (2xx-6xx) State.Proceeding --> State.Terminating", m_internal_id);

				set_state(nis_transaction_state_terminated_e);
				destroy();
			}
		}
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_nis_t::process_completed_state(sip_message_t& message)
{
	/*
	17.2.2 Non-INVITE Server Transaction

	The state machine for the non-INVITE server transaction is shown in
	Figure 8.

	The state machine is initialized in the "Trying" state and is passed
	a request other than INVITE or ACK when initialized.  This request is
	passed up to the TU.  Once in the "Trying" state, any further request
	retransmissions are discarded.  A request is a retransmission if it
	matches the same server transaction, using the rules specified in
	Section 17.2.3.

	While in the "Trying" state, if the TU passes a provisional response
	to the server transaction, the server transaction MUST enter the
	"Proceeding" state.  The response MUST be passed to the transport
	layer for transmission.  Any further provisional responses that are
	received from the TU while in the "Proceeding" state MUST be passed
	to the transport layer for transmission.  If a retransmission of the
	request is received while in the "Proceeding" state, the most
	recently sent provisional response MUST be passed to the transport
	layer for retransmission.  If the TU passes a final response (status
	codes 200-699) to the server while in the "Proceeding" state, the
	transaction MUST enter the "Completed" state, and the response MUST
	be passed to the transport layer for transmission.

	When the server transaction enters the "Completed" state, it MUST set
	Timer J to fire in 64*T1 seconds for unreliable transports, and zero
	seconds for reliable transports.  While in the "Completed" state, the
	server transaction MUST pass the final response to the transport
	layer for retransmission whenever a retransmission of the request is
	received.  Any other final responses passed by the TU to the server
	transaction MUST be discarded while in the "Completed" state.  The
	server transaction remains in this state until Timer J fires, at
	which point it MUST transition to the "Terminated" state.

	The server transaction MUST be destroyed the instant it enters the
	"Terminated" state.

	*/

	if (message.is_request())
	{
		/// retrnasmit the last final response we sent
		SLOG_TRANS(get_type_name(), "%s State.Complated : send last response", m_internal_id);
		send_message_to_transport(m_last_response);
	}
}
//--------------------------------------
