/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_transport_listener_tcp.h"
#include "sip_transport_tcp.h"
//--------------------------------------
// ����� �� ���������� ������ 64*T1 = 32 �������, �� �������� �� ������
//--------------------------------------
#define SIP_TRANSPORT_LIFESPAN (1000 * 60)
//#define MAKE_KEY64(a, p, t) uint64_t((uint64_t(a.s_addr) << 32) + (uint64_t(p) << 16) + t)
//--------------------------------------
//
//--------------------------------------
sip_transport_listener_tcp_t::sip_transport_listener_tcp_t() :
	m_tcp_listener(), sip_transport_listener_t(m_tcp_listener)
{
	update_name("TCP-L");
}
//--------------------------------------
//
//--------------------------------------
sip_transport_listener_tcp_t::~sip_transport_listener_tcp_t()
{
}
//--------------------------------------
//
//--------------------------------------
bool sip_transport_listener_tcp_t::create(const sip_transport_point_t& iface)
{
	SLOG_NET("tcp-l", "%s(%d) : create on %s:%u...", m_name, get_ref_count(), inet_ntoa(iface.address), iface.port);

	if (!m_socket.open(AF_INET, SOCK_STREAM, IPPROTO_TCP))
	{
		int last_error = std_get_error;
		SLOG_ERR_MSG("net-tcp", last_error, "%s(%d) : socket() function failed", m_name, get_ref_count());
		return false;
	}

	m_if = iface;

	sockaddr_in addr;
	memset(&addr, 0, sizeof addr);
	addr.sin_family = AF_INET;
	addr.sin_addr = m_if.address;
	addr.sin_port = htons(m_if.port);

	bool result = m_socket.bind((sockaddr*)&addr, sizeof addr);

	if (!result)
	{
		int last_error = std_get_error;
		SLOG_ERR_MSG("net-tcp", last_error, "%s(%d) : bind() function failed -- if:%s:%u", m_name, get_ref_count(), inet_ntoa(m_if.address), m_if.port);
		return false;
	}

	int bsize = 64 * 1024;
	m_socket.set_option(SOL_SOCKET, SO_RCVBUF, &bsize, sizeof(int));
	m_socket.set_option(SOL_SOCKET, SO_SNDBUF, &bsize, sizeof(int));

	m_socket.set_tag(this);

	SLOG_NET("net-tcp", "%s(%d) : created", m_name, get_ref_count());
	return true;
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_listener_tcp_t::destroy()
{
	sip_transport_listener_t::destroy();

	if (m_socket.get_handle() != INVALID_SOCKET)
	{
		SLOG_NET("net-tcp", "%s(%d) : closing socket", m_name, get_ref_count());
		m_socket.close();
		SLOG_NET("net-tcp", "%s(%d) : socket closed", m_name, get_ref_count());
	}
}
//--------------------------------------
//
//--------------------------------------
bool sip_transport_listener_tcp_t::start_listen()
{
	SLOG_NET("net-tcp", "%s(%d) : start listen New connections...", m_name, get_ref_count());
	
	bool result = m_socket.listen(SOMAXCONN);

	if (!result)
	{
		int last_error = std_get_error;
		SLOG_ERR_MSG("net-tcp", last_error, "%s(%d) : listen() function failed!", m_name, get_ref_count());
	}

	return result;
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_listener_tcp_t::data_ready()
{
	sockaddr_in addr;
	sockaddrlen_t addr_len = sizeof(addr);
	SOCKET handle = m_socket.accept((sockaddr*)&addr, &addr_len);

	if (handle == INVALID_SOCKET)
	{
		int last_error = std_get_error;
		SLOG_ERR_MSG("net-tcp", last_error, "%s(%d) : accept() failed for New connection!", m_name, get_ref_count());
		// ����� ��������� �� ���� ������, �������� ����� ����� ��������� ���������!
		//m_manager.sip_listener_stopped(this, last_error);
		return;
	}

	int type = 0;
	sockaddrlen_t typelen = sizeof(type);

	if (::getsockopt(handle, SOL_SOCKET, SO_TYPE, (char*)&type, &typelen) != 0)
	{
		SLOG_ERR_MSG("net-tcp", std_get_error, "%s(%d) : getsockopt failed", m_name, get_ref_count());
		return;
	}

	if (type != SOCK_STREAM)
	{
		SLOG_ERROR("net-tcp", "%s(%d) : socket handle type is not TCP/IP", m_name, get_ref_count());
		return;
	}

	if (sip_transport_manager_t::sip_listener_is_address_banned(this, addr.sin_addr))
	{
		SLOG_NET_WARN("net-tcp", "%s(%d) : PACKET NOT READ -- ADDRESS %s:%u BANNED!", m_name, get_ref_count(), inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));
		return;
	}

	SLOG_NET_EVENT("net-tcp", "%s(%d) : NEW CONNECTION for %s:%u", m_name, get_ref_count(), inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));

	sip_transport_point_t endpoint = { m_if.type, addr.sin_addr, ntohs(addr.sin_port) };

	sip_transport_tcp_t* transport = NEW sip_transport_tcp_t(m_if, endpoint, handle);

	sip_transport_manager_t::sip_listener_new_connection(this, transport);
}
//--------------------------------------
