﻿/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_session_manager.h"
//--------------------------------------
//
//--------------------------------------
sip_session_manager_t::sip_session_manager_t(sip_user_agent_t& ua, int sessionThreadCount, const char* mgrName) :
	m_sync("sip-sess-manager"), m_ua(ua), m_session_sync("sip-sess-list"),
	m_session_list("SLIST"),
	m_session_ref_list("RLIST")
{
	m_log_tag = "SESSM";
	STR_COPY(m_ua_core_name, mgrName);

	m_stop_flag = FALSE;

	rtl::Timer::setTimer(this, 0, 1000, true);
}
//--------------------------------------
//
//--------------------------------------
sip_session_manager_t::~sip_session_manager_t()
{
	stop();

	rtl::Timer::stopTimer(this, 0);

	sip_session_t* session;

	SLOG_CALL(m_log_tag, "DELETING SESSION MANAGER %d", m_session_list.getCount());

	for (int i = 0; i < m_session_list.getCount(); i++)
	{
		session = m_session_list.getAt(i);
		if (session != nullptr)
		{
			SLOG_CALL(m_log_tag, "DELETING SESSION %s by DESTRUCTOR from session list", session->getName());
			DELETEO(session);
		}
		else
		{
			SLOG_ERROR(m_log_tag, "DELETING nullptr SESSION! INDEX(%d)", i);
		}
	}

	m_session_ref_list.clear();
	m_session_list.clear();

	cleanup_purgatory();
}
//--------------------------------------
//
//--------------------------------------
void sip_session_manager_t::stop()
{
	m_stop_flag = TRUE;

	SLOG_CALL(m_log_tag, "Stopping session manager...");

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	for (int i = 0; i < m_session_list.getCount(); i++)
	{
		sip_session_t* session = m_session_list.getAt(i);
		if (session != nullptr)
		{
			SLOG_CALL(m_log_tag, "DELETING SESSION %s by DESTRUCTOR from session list", session->getName());
			session->destroy_session(false);
		}
		else
		{
			SLOG_ERROR(m_log_tag, "DELETING NULL SESSION! INDEX(%d)", i);
		}
	}

	cleanup_purgatory();

	SLOG_CALL(m_log_tag, "Session manager stopped");
}
//--------------------------------------
//
//--------------------------------------
sip_session_t* sip_session_manager_t::create_client_session(const sip_profile_t& profile, const char* callId)
{
	if (m_stop_flag)
		return nullptr;

	sip_session_t* session = create_new_client_session(profile, callId);

	if (session == nullptr)
		return nullptr;

	rtl::MutexWatchLock lock(m_session_sync, __FUNCTION__);

	SLOG_CALL(m_log_tag, "ADD SESSION %s TO LIST", session->getName());
	m_session_list.add(session, session->get_call_id());
	m_session_ref_list.add(session, session->getName());

	return session;
}
//--------------------------------------
//
//--------------------------------------
sip_session_t* sip_session_manager_t::create_new_client_session(const sip_profile_t& /*profile*/, const char* /*callId*/)
{
	// since we dont know what client session to create
	// we must force implementors to return their own descendant here
	//PAssertAlways(PUnimplementedFunction);
	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
sip_session_t* sip_session_manager_t::create_server_session(sip_message_t& request)
{
	if (m_stop_flag)
	{
		return nullptr;
	}

	sip_session_t* session = create_new_server_session(request);

	if (session == nullptr)
		return nullptr;

	rtl::MutexWatchLock lock(m_session_sync, __FUNCTION__);
	SLOG_CALL(m_log_tag, "ADD SESSION %s TO LIST", session->getName());
	m_session_list.add(session, session->get_call_id());
	m_session_ref_list.add(session, session->getName());

	return session;
}
//--------------------------------------
//
//--------------------------------------
sip_session_t* sip_session_manager_t::create_new_server_session(sip_message_t& request)
{
	return m_stop_flag ? nullptr : NEW sip_session_t(*this, request);
}
//--------------------------------------
//
//--------------------------------------
void sip_session_manager_t::queue_for_destruction(const char* callId)
{
	if (m_stop_flag)
		return;

	sip_session_t* session = nullptr;

	SLOG_CALL(m_log_tag, "QUEUE FOR DESTRUCTION BY CALL-ID '%s'", callId);
		
	{
		rtl::MutexWatchLock lock(m_session_sync, __FUNCTION__);

		int index;
		session = m_session_list.find(callId, index);

		if (session != nullptr)
		{
			SLOG_SESS(m_log_tag, "QueueForDestruction ----> session %s found by callid '%s'", session->getName(), callId);

			session->m_ready_to_destroy = false;
			session->m_purgatory_time = rtl::DateTime::getTicks();

			m_session_list.removeAt(index);
			m_session_ref_list.remove(session->getName());

			SLOG_SESS(m_log_tag, "QueueForDestruction ----> Session '%s' QUEUED FOR DESTRUCTION", session->getName());
		}
		else
		{
			SLOG_WARN(m_log_tag, "QueueForDestruction ----> Session '%s' NOT FOUND!", callId);
		}
	}

	if (session != nullptr)
		add_to_purgatory(session);
}
//--------------------------------------
//
//--------------------------------------
void sip_session_manager_t::start_session_timer(sip_session_t* session, uint32_t id, uint32_t period, bool single)
{
	SLOG_CALL(m_log_tag, "Start session timer for %s...", session->getName());

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	/*SLOG_TIMER(m_log_tag, "Start session timer (%d) for session %s ", id, session->getName(),
		currentTime, period, single ? "true" : "false");*/

	uint32_t currentTime = rtl::DateTime::getTicks();

	for (int i = 0; i < m_timer_users.getCount(); i++)
	{
		SESSION_TIMER& info = m_timer_users[i];
			
		if (info.tm_session == session && info.tm_id == id)
		{
			SLOG_TIMER(m_log_tag, "Update session timer (%d) for session %s start:%u period:%u single:%s",
				id, session->getName(), currentTime, period, single ? "true" : "false");

			info.tm_start = currentTime;
			info.tm_period = period;
			info.tm_single = single;
			info.tm_bad_handle = false;
				
			return;
		}
	}

	SLOG_TIMER(m_log_tag, "Add session timer (%d) for session %s start:%u period:%u single:%s",
				id, session->getName(), currentTime, period, single ? "true" : "false");

	SESSION_TIMER info;

	info.tm_session = session;
	info.tm_id = id;
	info.tm_bad_handle = false;
	info.tm_start = currentTime;
	info.tm_period = period;
	info.tm_single = single;

	m_timer_users.add(info);

	SLOG_CALL(m_log_tag, "Session timer for %s started", session->getName());
}
//--------------------------------------
//
//--------------------------------------
void sip_session_manager_t::update_session_timer(sip_session_t* session, uint32_t id, uint32_t period, bool single)
{
	SLOG_CALL(m_log_tag, "Update session timer for %s...", session->getName());
	
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	uint32_t currentTime = rtl::DateTime::getTicks();

	for (int i = 0; i < m_timer_users.getCount(); i++)
	{
		SESSION_TIMER& info = m_timer_users[i];
			
		if (info.tm_session == session && info.tm_id == id)
		{
			SLOG_TIMER(m_log_tag, "Update session timer (%d) for session %s start:%u period:%u single:%s",
				id, session->getName(), currentTime, period, single ? "true" : "false");
			info.tm_start = currentTime;
			info.tm_period = period;
			info.tm_single = single;
			info.tm_bad_handle = false;
			break;
		}
	}

	SLOG_CALL(m_log_tag, "Session timer for %s updated", session->getName());
}
//--------------------------------------
//
//--------------------------------------
void sip_session_manager_t::stop_session_timer(sip_session_t* session, uint32_t id)
{
	SLOG_CALL(m_log_tag, "Stop session timer for %s...", session->getName());

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	//SLOG_TIMER(m_log_tag, "Stop session timer (%d) for session %s : count", id, session->getName(), m_timer_users.getCount());

	int removed = 0;

	for (int i = m_timer_users.getCount() - 1; i >= 0; i--)
	{
		SESSION_TIMER& info = m_timer_users[i];

		if (info.tm_session == session)
		{
			if (id != SESSION_TIMER_ALL)
			{
				if (info.tm_id == id)
				{
					SLOG_TIMER(m_log_tag, "Session timer (%d) for %d session %s removed", id, i, session->getName());
					m_timer_users.removeAt(i);
					removed++;
					break;
				}
			}
			else
			{
				SLOG_TIMER(m_log_tag, "Session all timers for %d session %s removed", i, session->getName());
				m_timer_users.removeAt(i);
				removed++;
			}
		}
	}

	//SLOG_TIMER(m_log_tag, "Session timer(%d) for session %s %d timers removed", id, session->getName(), removed);

	SLOG_CALL(m_log_tag, "Session timer for %s stopped", session->getName());
}
//--------------------------------------
//
//--------------------------------------
const char* sip_session_manager_t::async_get_name()
{
	return m_log_tag;
}
//--------------------------------------
//
//--------------------------------------
void sip_session_manager_t::async_handle_call(uint16_t call_id, uintptr_t int_param, void* ptr_param)
{
	if (call_id == 0)
	{
		uint32_t currentTime = rtl::DateTime::getTicks();

		check_session_timers(currentTime);
		check_purgatory(currentTime);
	}
	else if (call_id == 1001)
	{
		EventQueue_OnDequeue(ptr_param);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_session_manager_t::timer_elapsed_event(rtl::Timer* sender, int tid)
{
	rtl::async_call_manager_t::callAsync(this, 0, tid, nullptr);
}
//--------------------------------------
//
//--------------------------------------
void sip_session_manager_t::check_session_timers(uint32_t currentTime)
{
	bool signal_raised = false;
	try
	{
		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		if (m_timer_users.getCount() > 0)
		{
			//SLOG_TIMER(m_log_tag, "Session's timer check ts:%u count:%d", currentTime, m_timer_users.getCount());

			for (int i = m_timer_users.getCount() - 1; i >= 0; i--)
			{
				SESSION_TIMER info = m_timer_users[i];

				uint32_t signal_time = info.tm_start + info.tm_period;
				uint32_t old_start_time = info.tm_start;

				int times = int(currentTime - signal_time);

				if (times > 0)
				{
					signal_raised = true;
					if (info.tm_single)
					{
						SLOG_TIMER(m_log_tag, "Session timer (%d) for %d session %s removed", info.tm_id, i, info.tm_session->getName());
						m_timer_users.removeAt(i);
					}
					else
					{
						m_timer_users[i].tm_start = currentTime;
					}

					try
					{
						SLOG_TIMER(m_log_tag, "Session timer (%d) for session %s signaled. current:%u start:%u period:%u diff:%u ms",
							info.tm_id, info.tm_session->getName(), currentTime, old_start_time, info.tm_period, times);

						if (!info.tm_bad_handle)
							rtl::async_call_manager_t::callAsync(info.tm_session, 1, info.tm_id, nullptr);
					}
					catch (rtl::Exception& ex)
					{
						SLOG_ERROR(m_log_tag, "Calling session timer handler(%p) failed with exception %s", info.tm_session, ex.get_message());
						info.tm_bad_handle = true;
					}
				}
			}

			if (signal_raised)
				SLOG_CALL(m_log_tag, "Session timers checked");
		}
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR(m_log_tag, "CheckSessionTimers failed with exception %s", ex.get_message());
	}
}
//--------------------------------------
//
//--------------------------------------
bool sip_session_manager_t::is_valid_session(sip_session_t* sess)
{
	rtl::MutexWatchLock lock(m_session_sync, __FUNCTION__);
	
	for (int i = 0; i < m_session_list.getCount(); i++)
	{
		if (m_session_list.getAt(i) == sess)
			return true;
	}

	return false;
}
//--------------------------------------
//
//--------------------------------------
void sip_session_manager_t::check_purgatory(uint32_t currentTime)
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (m_purgatory.getCount() > 0)
	{
		for (int i = m_purgatory.getCount()-1; i >= 0 ; i--)
		{
			sip_session_t* session = m_purgatory.getAt(i);

			if (session != nullptr)
			{
				if (session->m_ready_to_destroy && currentTime - session->m_destroy_time > 10000)
				{
					SLOG_TIMER(m_log_tag, "TIMER -> DestroySession %s", session->getName());
					m_purgatory.removeAt(i);
					session->process_destroy_event();
					DELETEO(session);
				}
				else if (!session->m_ready_to_destroy && currentTime - session->m_purgatory_time > 5000)
				{
					SLOG_TIMER(m_log_tag, "TIMER -> ReadyToDestroy %s", session->getName());

					session->m_destroy_time = currentTime;
					session->m_ready_to_destroy = true;
				}
			}
		}
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_session_manager_t::add_to_purgatory(sip_session_t* session)
{
	SLOG_CALL(m_log_tag, "Add %s to purgatory...", session->getName());

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	m_purgatory.add(session);

	SLOG_CALL(m_log_tag, "%s added to purgatory", session->getName());
}
//--------------------------------------
//
//--------------------------------------
void sip_session_manager_t::cleanup_purgatory()
{
	SLOG_CALL(m_log_tag, "cleanup purgatory...");

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	sip_session_t* session;

	for (int i = 0; i < m_purgatory.getCount(); i++)
	{
		if ((session = m_purgatory.getAt(i)) != nullptr)
		{
			SLOG_CALL(m_log_tag, "DELETING SESSION %s by STOP from purgatory", session->getName());
			DELETEO(session);
		}
	}

	m_purgatory.clear();

	SLOG_CALL(m_log_tag, "purgatory cleaned");
}
//--------------------------------------
//
//--------------------------------------
sip_session_t* sip_session_manager_t::find_session_by_call_id(const char* callId)
{
	if (m_stop_flag || IS_STRING_EMPTY(callId))
		return nullptr;

	SLOG_SESS(m_log_tag, "Find Session By CallId %s", callId);

	sip_session_t* session = nullptr;

	rtl::MutexWatchLock lock(m_session_sync, __FUNCTION__);

	if ((session = m_session_list.find(callId)) != nullptr)
	{
		SLOG_CALL(m_log_tag, "SESSION %s found", session->getName());
	}
	else
	{
		SLOG_CALL(m_log_tag, "SESSION NOT FOUND BY CALLID %s", callId);
	}

	return session;
}
//--------------------------------------
//
//--------------------------------------
sip_session_t* sip_session_manager_t::find_session_by_ref(const char* sessionRef)
{
	if (m_stop_flag || IS_STRING_EMPTY(sessionRef))
		return nullptr;

	SLOG_SESS(m_log_tag, "Find Session By Ref %s", sessionRef);

	sip_session_t* session = nullptr;

	rtl::MutexWatchLock lock(m_session_sync, __FUNCTION__);

	if ((session = m_session_ref_list.find(sessionRef)) != nullptr)
	{
		SLOG_CALL(m_log_tag, "SESSION %s found", session->getName());
	}
	else
	{
		SLOG_CALL(m_log_tag, "SESSION NOT FOUND BY REF %s", sessionRef);
	}	

	return session;
}
//--------------------------------------
//
//--------------------------------------
void sip_session_manager_t::push_session_event(sip_session_t& session, sip_session_event_t* eventObject)
{
	if (!m_stop_flag && session.will_process_events())
	{
		SLOG_CALL(m_log_tag, "Enqueue session event %p for session %s", eventObject, session.getName());
		rtl::async_call_manager_t::callAsync(this, 1001, 0, eventObject);
	}
	else if (eventObject != nullptr)
	{
		SLOG_WARN(m_log_tag, "session event %p filtered by dead session %p", eventObject, session.getName());
		DELETEO(eventObject);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_session_manager_t::EventQueue_OnDequeue(void* item)
{
	if (item != nullptr && !m_stop_flag)
	{
		sip_session_event_t* eventObject = (sip_session_event_t*)item;
		
		sip_session_t* session = find_session_by_ref(eventObject->get_ref());

		SLOG_CALL(m_log_tag, "Dequeue session event %p %s", item, eventObject->get_ref());

		if (session != nullptr && session->will_process_events())
		{
			session->process_session_event(eventObject->get_event(), eventObject->get_message());
		}
		else
		{
			process_orphaned_message(eventObject->get_message());
		}

		DELETEO((sip_session_event_t*)eventObject);
		SLOG_CALL(m_log_tag, "Session event %p handled", item, session);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_session_manager_t::process_stack_event(sip_stack_event_t* sip_event)
{
	if (m_stop_flag)
	{
		return;
	}
	
	sip_session_t* session = find_session_by_call_id(sip_event->get_call_id());

	process_stack_event(session, sip_event);
}
//--------------------------------------
//
//--------------------------------------
void sip_session_manager_t::process_stack_event(const sip_transport_route_t& route, sip_stack_event_t* sip_event)
{
	if (sip_event->getType() == sip_stack_event_t::sip_event_transport_failure_e)
	{
		SLOG_SESS(m_log_tag, "EVENT '%s'", (const char*)sip_event->to_string());
		sip_event_transport_failure_t* transp_fault = (sip_event_transport_failure_t*)sip_event;
		process_transport_failure(transp_fault->getRoute(), transp_fault->get_net_error());
	}
	
	if (sip_event->getType() == sip_stack_event_t::sip_event_transport_disconnected_e)
	{
		SLOG_SESS(m_log_tag, "EVENT '%s'", (const char*)sip_event->to_string());
		sip_event_transport_disconnected_t* transp_fault = (sip_event_transport_disconnected_t*)sip_event;
		process_transport_disconnected(transp_fault->getRoute());
	}
}
//--------------------------------------
// базовая обработка
//--------------------------------------
void sip_session_manager_t::process_stack_event(sip_session_t* session, sip_stack_event_t* sip_event)
{
	if (m_stop_flag)
	{
		return;
	}

	try
	{
		if (sip_event->getType() == sip_stack_event_t::sip_event_tx_e)
		{
			SLOG_SESS(m_log_tag, "EVENT message sent event session %p call-id %s", session, (const char*)sip_event->get_call_id());
		}
		
		if (sip_event->getType() == sip_stack_event_t::sip_event_unknown_trn_e)
		{
			SLOG_SESS(m_log_tag, "EVENT UnknownTransaction for '%s' method %s", (const char*)sip_event->get_call_id(), (const char*)sip_event->get_method());
			process_unknown_transaction((sip_event_unknown_transaction_t&)*sip_event);
		}
		else
		{
			// just discard this event.  no session to handle it
			if (session == nullptr && sip_event->getType() != sip_stack_event_t::sip_event_message_e)
			{
				//SLOG_WARN(m_log_tag, "NULL Session Event and event is not message type! ---> %s(%d)", sip_event_get_type_name(sip_event->getType()), sip_event->getType());
				return;
			}

			switch (sip_event->getType())
			{
			case sip_stack_event_t::sip_event_message_e:
				SLOG_SESS(m_log_tag, "EVENT Inbound for '%s' %s", (const char*)sip_event->get_call_id(), (const char*)((sip_event_message_t&)*sip_event).get_message().GetStartLine());
				process_incoming_message((sip_event_message_t&)*sip_event, session);
				break;
			
			case sip_stack_event_t::sip_event_timer_e:
				{
					sip_event_timer_expires_t* pTimer = (sip_event_timer_expires_t*)sip_event;
					uint32_t tt = pTimer->get_timer();
					SLOG_SESS(m_log_tag, "EVENT TimerExpire(%d) for '%s' method %s", tt, (const char*)sip_event->get_call_id(), (const char*)sip_event->get_method());
					if (session != nullptr)
					{
						session->process_timer_expires_event((sip_event_timer_expires_t&)*sip_event);
					}
					else
					{
						SLOG_ERROR(m_log_tag, "EVENT TimerExpire has nullptr session object!");
					}
				}
				break;
			
			case sip_stack_event_t::sip_event_transport_failure_e:
				SLOG_SESS(m_log_tag, "EVENT TransportError for '%s' method %s", (const char*)sip_event->get_call_id(), (const char*)sip_event->get_method());
				if (session != nullptr)
				{
					session->process_transport_failure_event(sip_stack_event_t::sip_event_transport_failure_e);
				}
				else
				{
					SLOG_ERROR(m_log_tag, "EVENT TransportError has nullptr session object!");
				}
				break;
			
			case sip_stack_event_t::sip_event_tx_e:
				SLOG_SESS(m_log_tag, "EVENT TransportWrite for '%s' method %s", (const char*)sip_event->get_call_id(), (const char*) sip_event->get_method());

				if (session != nullptr)
				{
					session->process_sip_message_sent_event((sip_event_tx_t&)*sip_event);
				}
				break;
			default:
				SLOG_SESS(m_log_tag, "Unusable EVENT '%s' callid %s method %s", sip_event_get_type_name(sip_event->getType()),
						(const char*)sip_event->get_call_id(), (const char*)sip_event->get_method());
			}

		}
	}
	catch (rtl::Exception& ex )
	{
		SLOG_ERROR(m_log_tag, "Process Stack Event failed with exception: %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_session_manager_t::process_incoming_message(sip_event_message_t& messageEvent, sip_session_t* session)
{
	if (m_stop_flag)
	{
		return;
	}

	//const trans_id& transactionId = messageEvent.get_transaction_id();

	sip_message_t request = messageEvent.get_message();

	if (!request.IsValid())
	{
		return;
	}

	if (session != nullptr )
	{
		if (!session->will_process_events())
		{
			SLOG_SESS(m_log_tag, "MESSAGE ARRIVAL ---> Session %s stopped. try to create New for %s", session->getName(), (const char*)request.GetStartLine());
			//session = nullptr;
			process_orphaned_message(request);
		}
		else if (!session->process_incoming_sip_message(messageEvent))
		{
			SLOG_SESS(m_log_tag, "MESSAGE ARRIVAL ---> Session %s declined message %s", session->getName(), (const char*)request.GetStartLine());
			process_orphaned_message(request);
			return;
		}
	}
	else if (request.is_request())
	{
		if (!request.IsAck() && !request.IsOptions())
		{
			session = create_server_session(request);

			if (session != nullptr)
			{
				sip_event_message_t messageEvent2(request, messageEvent.get_transaction_id());
				session->process_incoming_sip_message(messageEvent2);
				return;
			}
			else
			{
				SLOG_SESS(m_log_tag, "MESSAGE ARRIVAL ---> No Session available to handle %s", (const char*)request.GetStartLine());
				process_orphaned_message(request, sip_404_NotFound);
			}
		}
		else
		{
			process_orphaned_message(request);
		}
	}
	else  // остальное - ответы
	{
		SLOG_SESS(m_log_tag, "MESSAGE ARRIVAL ---> Received a response for deleted session '%s'", (const char*)request.GetStartLine());
		process_orphaned_message(request);
		return;
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_session_manager_t::process_unknown_transaction(sip_event_unknown_transaction_t& eventObject)
{
	if (m_stop_flag)
		return;

	SLOG_WARN(m_log_tag, "ON UNHANDLED TRANSACTION %s", eventObject.get_type_name()); // (const char*)strm
}
//--------------------------------------
//
//--------------------------------------
void sip_session_manager_t::process_orphaned_message(const sip_message_t& message, sip_status_t scode)
{
	if (m_stop_flag)
		return;

	SLOG_WARN(m_log_tag, "ON ORPHANED REQUEST %s", (const char*)message.GetStartLine());

	if (message.is_request())
	{
		sip_message_t response;
		message.make_response(response, scode);
		sip_transport_route_t route = response.getRoute();
		send_message_to_transport(response);
		sip_transport_manager_t::release_route(route, "sip-mgr-orphaned");
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_session_manager_t::process_transport_disconnected(const sip_transport_route_t& route)
{
	if (m_stop_flag)
		return;

	rtl::MutexWatchLock lock(m_session_sync, __FUNCTION__);

	for (int i = 0; i < m_session_list.getCount(); i++)
	{
		sip_session_t* session = m_session_list.getAt(i);
		if (session != nullptr)
		{
			session->check_transport_route_disconnected(route);
		}
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_session_manager_t::process_transport_failure(const sip_transport_route_t& route, int net_error)
{
	if (m_stop_flag)
		return;

	rtl::MutexWatchLock lock(m_session_sync, __FUNCTION__);

	for (int i = 0; i < m_session_list.getCount(); i++)
	{
		sip_session_t* session = m_session_list.getAt(i);
		if (session != nullptr)
		{
			session->check_transport_route_failure(route, net_error);
		}
	}
}
//--------------------------------------
//
//--------------------------------------
bool sip_session_manager_t::send_message_to_transport(const sip_message_t& request, sip_session_t* session)
{
	trans_id tid;
	return get_user_agent().push_message_to_transaction(request, tid);
}
//--------------------------------------
