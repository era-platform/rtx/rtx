﻿/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_reg_session_client.h"
//--------------------------------------
//
//--------------------------------------
static const char* GetSessionEventName(int eventId);

#define SIP_EXPIRES_DELTA 20
//--------------------------------------
// конструктор клиентской сессии
//--------------------------------------
sip_reg_session_client_t::sip_reg_session_client_t(sip_reg_session_manager_t& sessionManager, const sip_profile_t& profile, const char* callId) :
	sip_session_t(sessionManager, profile, callId),
	m_registered(false),
	m_for_unregister(false),
	m_last_expires(0),
	m_authorization_attempts(0),
	m_event_handler(nullptr),
	m_register_qop_count(0)
{
	if (m_profile.get_expires() < 30)
	{
		m_profile.set_expires(30);
	}

	m_log_tag = "REG";

	construct_register(m_register);

	SLOG_SESS(m_log_tag, "%s -- Created Client from CallId: %s", m_session_ref, (const char*)m_call_id);

	//RC_IncrementObject(RC_REG_SESSION);

	memset(m_auth_nonce, 0, sizeof m_auth_nonce);
}
//--------------------------------------
// конструктор серверной сессии
//--------------------------------------
sip_reg_session_client_t::sip_reg_session_client_t(sip_reg_session_manager_t& sessionManager, const sip_message_t& request) :
	sip_session_t(sessionManager, request),
	m_registered(false),
	m_for_unregister(false),
	m_last_expires(0),
	m_authorization_attempts(0),
	m_event_handler(nullptr),
	m_register_qop_count(0)
{
	m_log_tag = "REG";

	//Renat+Peter 21.06.2011
	//  Иначе не удаляется сессия, если ей авторизационную инфу не прислали
	StartSessionExpireTimer(120, false);

	SLOG_SESS(m_log_tag, "%s -- Created Server from CallId: %s", m_session_ref, (const char*)m_call_id);

	//RC_IncrementObject(RC_REG_SESSION);
}
//--------------------------------------
// деструктор
//--------------------------------------
sip_reg_session_client_t::~sip_reg_session_client_t()
{
	//RC_DecrementObject(RC_REG_SESSION);
}
//--------------------------------------
// отправка начального запроса регистрации
//--------------------------------------
void sip_reg_session_client_t::send_register(sip_reg_session_event_handler_t* handler)
{
	SLOG_SESS(m_log_tag, "%s : Send register", m_session_ref);

	StopAutoDestructTimer();

	sip_message_t request = m_register;
	
	m_event_handler = handler;

	SLOG_SESS(m_log_tag, "%s : Send register : Starting Registration on %s:%u", m_session_ref, inet_ntoa(m_route.remoteAddress), m_route.remotePort);

	m_for_unregister = false;

	request.Via_Branch(sip_utils::GenBranchParameter());

	push_message_to_transaction(request);
}
//--------------------------------------
// отмена регистрации
//--------------------------------------
void sip_reg_session_client_t::send_unregister()
{
	SLOG_SESS(m_log_tag, "%s : Send Unregister", m_session_ref);

	StopNATKeepAlive();
	
	StopSessionExpireTimer();

	StartAutoDestructTimer(10);

	m_for_unregister = true;
	m_event_handler = nullptr;

	if (!m_registered)
	{
		SLOG_WARN(m_log_tag, "%s : Send Unregister : Sending UNREGISTER Aborted because Session is not registered", m_session_ref);
		return;
	}

	m_registered = false;

	// 18.03.2013 renat. FMC. Защита построения объекта локом.
	sip_message_t unregister = GetCurrentUACRequestCopy();

	/// increment the cseq;
	unregister.CSeq_Increment();

	unregister.remove_std_header(sip_Contact_e);
	unregister.make_Contact_value()->set_asterisk_flag(true);
	unregister.make_Expires_value()->set_number(0);
	unregister.Via_Branch(sip_utils::GenBranchParameter());

	push_message_to_transaction(unregister);
}
//--------------------------------------
// выставляем тип и время отправки пакетов для нат шлюзов
//--------------------------------------
void sip_reg_session_client_t::set_nat_keep_alive(uint32_t seconds)
{
	SLOG_SESS(m_log_tag, "%s : set nat keep-alive: expires:%d sec", m_session_ref, seconds);

	m_profile.set_keep_alive_interval(seconds);
	
	if (seconds == 0)
	{
		StopNATKeepAlive();
		StartSessionExpireTimer(m_last_expires - SIP_EXPIRES_DELTA, false);
	}
	else //if (type == sip_profile_t::keep_alive_send)
	{
		StartNATKeepAlive(seconds);
		StartSessionExpireTimer(m_last_expires - SIP_EXPIRES_DELTA, false);
	}
}
//--------------------------------------
// выставляем время жизни клиентской сессии
//--------------------------------------
void sip_reg_session_client_t::set_expires(int expires)
{
	m_profile.set_expires(m_last_expires = expires);
	
	// обновим состояние для исходящей регистрации
	if (m_type == sip_session_client_e && m_registered)
	{
		process_session_expire_event();
	}
}
//--------------------------------------
// сортировка входящих сип пакетов
//--------------------------------------
bool sip_reg_session_client_t::process_incoming_sip_message(sip_event_message_t& messageEvent)
{
	sip_session_t::process_incoming_sip_message(messageEvent);

	sip_message_t msg = messageEvent.get_message();

	if (!msg.is_request() && m_type == sip_session_client_e)
	{
		rtl::String method = msg.CSeq_Method();

		if (method *= "REGISTER")
		{
			process_response(msg);
		}
		else if (method *= "OPTIONS")
		{
			if (m_event_handler != nullptr)
			{
				m_event_handler->sip_reg_session__options_response(*this, msg);
			}
		}
	}
	else if (msg.IsOptions())
	{
		push_session_event(NEW sip_session_event_t(*this, OptionsRequest, msg));
	}

	return true;
}
//--------------------------------------
// обработка событий сессии
//--------------------------------------
void sip_reg_session_client_t::process_session_event(int eventId, const sip_message_t& eventMsg)
{
	SLOG_SESS(m_log_tag, "%s : session event '%s' message %s", m_session_ref, GetSessionEventName(eventId), (const char*)eventMsg.GetStartLine());

	switch(eventId)
	{
		// Base transport events
	case sip_session_t::TransportFault:
	case sip_session_t::TransportDisconnected:
		if (m_registered)
			process_transport_failure();
		break;
	case OptionsRequest:
		process_options(eventMsg);
		break;
	}
}
//--------------------------------------
// обработка события конца времени жизни запроса
//--------------------------------------
void sip_reg_session_client_t::process_timer_expires_event(sip_event_timer_expires_t& /*timerEvent*/)
{
	SLOG_SESS(m_log_tag, "%s : request transaction timeout", m_session_ref);
	
	if (m_event_handler != nullptr)
		m_event_handler->sip_reg_session__timeout(*this);
	
	destroy_session();
}
//--------------------------------------
// событие таймера конца времени жизни сессии
//--------------------------------------
void sip_reg_session_client_t::process_session_expire_event()
{
	if (m_type == sip_session_client_e)
	{
		SLOG_SESS(m_log_tag, "%s : session timer expire (client) send next registration...", m_session_ref);

		sip_message_t request = m_register;

		/// increment the cseq;
		/// 19.03.2013 renat. Наиболее актуальный CSeq сейчас находится в CurrentUACRequest, а не в m_register.
		/// request.GetCSeq(cseq);
		sip_message_t currentRequest = GetCurrentUACRequestCopy();
		uint32_t cseq = currentRequest.CSeq_Number();
		request.CSeq_Number(++cseq);
		m_register.CSeq_Number(cseq);

		uint32_t expires = m_profile.get_expires();
		request.make_Expires_value()->set_number(expires);
		m_register.make_Expires_value()->set_number(expires);

		request.Via_Branch(sip_utils::GenBranchParameter());

		m_new_session = false;

		if (push_message_to_transaction(request))
		{
			int expires = m_profile.get_expires();
			/// renew the timer
			StartSessionExpireTimer(expires, false);
		}
	}
}
//--------------------------------------
// событие смерти сессии
//--------------------------------------
void sip_reg_session_client_t::process_destroy_event()
{
	SLOG_SESS(m_log_tag, "%s : destroy session", m_session_ref);

	if (m_event_handler != nullptr)
	{
		m_event_handler->sip_reg_session__destroying(*this);
		m_event_handler = nullptr;
	}

	sip_session_t::process_destroy_event();
}
//--------------------------------------
// обработка ответа на запрос
//--------------------------------------
void sip_reg_session_client_t::process_response(const sip_message_t& response)
{
	SLOG_SESS(m_log_tag, "%s : handling response %s", m_session_ref, (const char*)response.GetStartLine());

	if (response.Is1xx())
		return;

	if (response.Is2xx())
	{
		m_authorization_attempts = 0;
		process_2xx_response(response);
	}
	else if (response.get_response_code() == sip_401_Unauthorized || 
		response.get_response_code() == sip_407_ProxyAuthenticationRequired)
	{
		if (++m_authorization_attempts > 2)
			process_failure(response);
		else
			process_authenticate(response);
	}
	else if (response.get_response_code() == sip_423_IntervalTooBrief)
	{
		process_interval_to_brief(response);
	}
	else
	{
		process_failure(response);
	}

}
//--------------------------------------
// обработка ошибок отправки/ответов с ошибкой
//--------------------------------------
void sip_reg_session_client_t::process_failure(const sip_message_t& response)
{
	m_registered = false;

	if (m_event_handler != nullptr)
		m_event_handler->sip_reg_session__rejected(*this, response);
}
//--------------------------------------
// обработка ответов 2xx
//--------------------------------------
void sip_reg_session_client_t::process_2xx_response(const sip_message_t& response)
{
	// 18.03.2013 renat. FMC. Защита построения объекта локом.
	//sip_message_t request = GetCurrentUACRequest();
	sip_message_t request = GetCurrentUACRequestCopy();

	if (!(request.IsUnregister() || m_for_unregister))
	{
		InitializeDialogAsUAC(m_register, response);

		calc_expires_from_response(response);
		start_expires_timer(-SIP_EXPIRES_DELTA);
		
		int interval = m_last_expires;

		
		StopNATKeepAlive();

		//Start the NAT keep-alive heartbeat
		uint32_t natKeepAlive = m_profile.get_keep_alive_interval();
		if (natKeepAlive > 0)
		{
			StartNATKeepAlive(natKeepAlive);
		}
		
		m_registered = TRUE;

		SLOG_SESS(m_log_tag, "%s : On successful : Registration Accepted", m_session_ref);

		if (m_event_handler != nullptr)
			m_event_handler->sip_reg_session__accepted(*this, request);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_reg_session_client_t::process_authenticate(const sip_message_t& response)
{
	sip_message_t auth;

	// 18.03.2013 renat. FMC. Защита построения объекта локом.
//	sip_message_t request = GetCurrentUACRequest();
	sip_message_t request = GetCurrentUACRequestCopy();
	bool ok = false;

	SLOG_SESS(m_log_tag, "%s : Authenticate : Registration being authenticated", m_session_ref);

	if (response.get_response_code() == sip_401_Unauthorized)
	{
		ok = construct_authorization(request, response, auth);
	}
	else if (response.get_response_code() == sip_407_ProxyAuthenticationRequired)
	{
		ok = construct_authentication(request, response, auth);
	}

	if (ok)
	{
		push_message_to_transaction(auth);
	}
	else
	{
		process_failure(response);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_reg_session_client_t::process_interval_to_brief(const sip_message_t& response)
{
	// 18.03.2013 renat. FMC. Защита построения объекта локом.

	sip_message_t repost = GetCurrentUACRequestCopy();

	SLOG_SESS(m_log_tag, "%s : Authenticate : Registration being authenticated", m_session_ref);

	// изменяем expires, cseq

	/// increment cseq
	repost.CSeq_Increment();
	m_last_expires = response.get_Expires_value()->get_number();
	repost.make_Expires_value()->set_number(m_last_expires);

	m_profile.set_expires(m_last_expires);

	start_expires_timer(-SIP_EXPIRES_DELTA);

	push_message_to_transaction(repost);
}
//--------------------------------------
// выставляем время жизни от ответа сервера если интервал меньше запрошенного нами
//--------------------------------------
bool sip_reg_session_client_t::calc_expires_from_response(const sip_message_t& response)
{
	const sip_value_number_t* expires = response.get_Expires_value();

	if (!expires)
	{
		if (response.get_Contact()->get_value_count() == 0)
		{
			SLOG_WARN(m_log_tag, "%s : Response has NO contact field!", m_session_ref);
			return false;
		}

		const sip_value_contact_t* contact = response.get_Contact_value();
		if (contact)
		{
			m_last_expires = contact->get_expires();
			if ((uint32_t)m_last_expires == CONTACT_HAS_NO_EXPORES)
			{
				SLOG_WARN(m_log_tag, "%s : Response has NO expores header or parameter in contact field!", m_session_ref);
				m_last_expires = 0;
				return false;
			}
			SLOG_SESS(m_log_tag, "%s : Calc expires from contact : %u sec", m_session_ref, m_last_expires);
		}
	}
	else
	{
		m_last_expires = expires->get_number();
		SLOG_SESS(m_log_tag, "%s : Calc expires from field : %u sec", m_session_ref, m_last_expires);
	}

	return true;
}
//--------------------------------------
// выставление времени жизни сессии по входящему сип пакету
//--------------------------------------
void sip_reg_session_client_t::start_expires_timer(int delta)
{
	StopSessionExpireTimer();

	int expires = m_last_expires + delta;

	if (expires < 10)
		expires = 10;

	StartSessionExpireTimer(expires, false);

	SLOG_SESS(m_log_tag, "%s : Starting expires timer for %d seconds", m_session_ref, expires);
}
//--------------------------------------
// подготовка и отправка ответа на OPTIONS
//--------------------------------------
void sip_reg_session_client_t::process_options(const sip_message_t& request)
{
	/// we are about to be deleted by the useragent
	SLOG_SESS(m_log_tag, "%s : OPTIONS request", m_session_ref);

	sip_message_t answer;
	request.make_response(answer, sip_200_OK, "");
	const sip_header_t* c = request.get_std_header(sip_Contact_e);
	answer.set_std_header(c);

	sip_header_t* allowed = answer.make_Allow();

	allowed->add_value("INVITE");
	allowed->add_value("BYE");
	allowed->add_value("ACK");
	allowed->add_value("REFER");
	allowed->add_value("MESSAGE");
	allowed->add_value("INFO");
	allowed->add_value("SUBSCRIBE");
	allowed->add_value("NOTIFY");
	allowed->add_value("OPTIONS");

	bool sdp_required = true;

	if (request.get_Accept()->get_value_count() > 0)
	{
		const sip_value_t* accept = request.get_Accept_value();

		if (accept->value() *= "application/sdp")
			sdp_required = true;
		else
			sdp_required = false;
		
	}

	if (sdp_required && m_event_handler != nullptr)
		m_event_handler->sip_reg_session__options_request(*this, request, answer);

	push_message_to_transaction(answer);
}
//--------------------------------------
// 
//--------------------------------------
void sip_reg_session_client_t::process_transport_failure()
{
	if (m_event_handler != nullptr)
	{
		m_event_handler->sip_reg_session__transport_failure(*this);
		m_registered = false;
	}
	
	destroy_session();
}
//--------------------------------------
// подготовка пакета REGISTER
//--------------------------------------
bool sip_reg_session_client_t::construct_register(sip_message_t& regMsg)
{
	/*
	10.2 Constructing the REGISTER Request

	REGISTER requests add, remove, and query bindings.  A REGISTER
	request can add a NEW binding between an address-of-record and one or
	more contact addresses.  Registration on behalf of a particular
	address-of-record can be performed by a suitably authorized third
	party.  A client can also remove previous bindings or query to
	determine which bindings are currently in place for an address-of-
	record.

	Except as noted, the construction of the REGISTER request and the
	behavior of clients sending a REGISTER request is identical to the
	general UAC behavior described in Section 8.1 and Section 17.1.

	A REGISTER request does not establish a dialog.  A UAC MAY include a
	Route header field in a REGISTER request based on a pre-existing
	route set as described in Section 8.1.  The Record-Route header field
	has no meaning in REGISTER requests or responses, and MUST be ignored
	if present.  In particular, the UAC MUST NOT create a NEW route set
	based on the presence or absence of a Record-Route header field in
	any response to a REGISTER request.

	The following header fields, except Contact, MUST be included in a
	REGISTER request.  A Contact header field MAY be included:

	Request-URI: The Request-URI names the domain of the location
	service for which the registration is meant (for example,
	"sip:chicago.com").  The "userinfo" and "@" components of the
	SIP URI MUST NOT be present.

	To: The To header field contains the address of record whose
	registration is to be created, queried, or modified.  The To
	header field and the Request-URI field typically differ, as
	the former contains a user name.  This address-of-record MUST
	be a SIP URI or SIPS URI.

	From: The From header field contains the address-of-record of the
	person responsible for the registration.  The value is the
	same as the To header field unless the request is a third-
	party registration.

	Call-ID: All registrations from a UAC SHOULD use the same Call-ID
	header field value for registrations sent to a particular
	registrar.

	If the same client were to use different Call-ID values, a
	registrar could not detect whether a delayed REGISTER request
	might have arrived out of order.

	CSeq: The CSeq value guarantees proper ordering of REGISTER
	requests.  A UA MUST increment the CSeq value by one for each
	REGISTER request with the same Call-ID.

	Contact: REGISTER requests MAY contain a Contact header field with
	zero or more values containing address bindings.

	expires: The "expires" parameter indicates how long the UA would
	like the binding to be valid.  The value is a number
	indicating seconds.  If this parameter is not provided, the
	value of the Expires header field is used instead.
	Implementations MAY treat values larger than 2**32-1
	(4294967295 seconds or 136 years) as equivalent to 2**32-1.
	Malformed values SHOULD be treated as equivalent to 3600.

	UAs MUST NOT send a NEW registration (that is, containing NEW Contact
	header field values, as opposed to a retransmission) until they have
	received a final response from the registrar for the previous one or
	the previous REGISTER request has timed out.

	*/
	char tmp[128];
	SLOG_SESS(m_log_tag, "%s -- construct register request...route %s", m_session_ref, route_to_string(tmp, 128, m_route));

	regMsg.set_route(m_route);

	/// Create the start line
//	RequestLine requestLine;
	sip_uri_t registrarAddress;
	
	registrarAddress.set_scheme("sip");
	registrarAddress.set_user(m_profile.get_username());
	registrarAddress.set_host(m_profile.get_domain());

	regMsg.set_request_line(sip_REGISTER_e, registrarAddress);

	//SLOG_SESS(m_log_tag, "%s -- construct register request. step 0.1", m_session_ref);

	/// Create the via
	sip_value_via_t* via = regMsg.make_Via_value();

	via->set_address(inet_ntoa(get_via_address()));
	via->set_port(get_via_port());
	
	SLOG_SESS(m_log_tag, "%s -- construct register request. via %s", m_session_ref, (const char*)via->to_string());

	via->set_protocol(m_profile.get_transport());
	via->set_branch(sip_utils::GenBranchParameter());
	if (m_profile.get_interface_port() == m_profile.get_listener_port()) 
		via->drop_rport();

	//SLOG_SESS(m_log_tag, "%s -- construct register request. step 0.3", m_session_ref);

	//SLOG_SESS(m_log_tag, "%s -- construct register request. step 1", m_session_ref);

	/// Create the from uri
	rtl::String domain = m_profile.get_domain();

	//SLOG_SESS(m_log_tag, "%s -- construct register request. step 1.1", m_session_ref);

	sip_value_user_t* from = regMsg.make_From_value();
	sip_uri_t& fromURI = from->get_uri();
	from->set_display_name(m_profile.get_display_name());
	fromURI.set_user(m_profile.get_username());
	fromURI.set_host(domain);

	//SLOG_SESS(m_log_tag, "%s -- construct register request. step 1.2", m_session_ref);

	/// always add a NEW tag
	from->set_tag(sip_utils::GenTagParameter());

	//SLOG_SESS(m_log_tag, "%s -- construct register request. step 1.3", m_session_ref);

	//SLOG_SESS(m_log_tag, "%s -- construct register request. step 2", m_session_ref);

	/// Create the to uri
	/// To and from for registrations are identical
	sip_value_uri_t* to = regMsg.make_To_value();
	to->set_display_name(m_profile.get_display_name());
	to->set_uri(fromURI);

	//SLOG_SESS(m_log_tag, "%s -- construct register request. step 3", m_session_ref);

	/// Create the CSeq
	regMsg.CSeq(1, "REGISTER");

	//SLOG_SESS(m_log_tag, "%s -- construct register request. step 4", m_session_ref);

	/// set the call-id
	regMsg.CallId(m_call_id);

	regMsg.make_Max_Forwards_value()->set_number(SIP_DEFAULT_MAX_FORWARDS);

	/// Set the expires
	regMsg.make_Expires_value()->set_number(m_profile.get_expires());

	/// Set the content length to zero
	regMsg.make_Content_Length_value()->set_number(0);

	//SLOG_SESS(m_log_tag, "%s -- construct register request. step 5", m_session_ref);

	/// Set the allow header for Requests we support
	sip_header_t* allowed = regMsg.make_Allow();
	allowed->add_value("INVITE");
	allowed->add_value("BYE");
	allowed->add_value("ACK");
	allowed->add_value("REFER");
	allowed->add_value("MESSAGE");
	allowed->add_value("INFO");
	allowed->add_value("SUBSCRIBE");
	allowed->add_value("NOTIFY");
	allowed->add_value("OPTIONS");

	//SLOG_SESS(m_log_tag, "%s -- construct register request. step 6", m_session_ref);

	sip_uri_t tempUri;

	tempUri.set_user(m_profile.get_username());
	tempUri.set_host(via->get_address());
	tempUri.set_port(rtl::int_to_string(via->get_port()));
	tempUri.get_parameters().set("transport", TransportType_toString(m_profile.get_transport()));

	//SLOG_SESS(m_log_tag, "%s -- construct register request. step 7", m_session_ref);

	sip_value_contact_t* contact = regMsg.make_Contact_value();
	contact->set_display_name(m_profile.get_display_name() );
	contact->set_uri(tempUri);

	SLOG_SESS(m_log_tag, "%s -- request constructed", m_session_ref);

	return true;
}
//--------------------------------------
// добавление авторизации в пакет
//--------------------------------------
bool sip_reg_session_client_t::construct_authorization(const sip_message_t& regMsg, const sip_message_t& challenge, sip_message_t& auth)
{
	rtl::String auth_id = m_profile.get_auth_id();
	rtl::String auth_pwd = m_profile.get_auth_pwd();

	if (!challenge.has_WWW_Authenticate())
		return false;

	const sip_value_auth_t* wwwAuth = challenge.get_WWW_Authenticate_value();

	/// clone the original register message
	auth = regMsg;

	auth.From_Tag(sip_utils::GenTagParameter());

	/// increment cseq
	auth.CSeq_Increment();

	rtl::String uriText = auth.get_request_uri().to_string(false);

	rtl::String realm, alg;
	rtl::String nonce;
	rtl::String qop, opaque;
	rtl::String entity = "";

	wwwAuth->get_auth_param("realm", realm);
	wwwAuth->get_auth_param("nonce", nonce);
	wwwAuth->get_auth_param("qop", qop);
	wwwAuth->get_auth_param("opaque", opaque);
	wwwAuth->get_auth_param("algorithm", alg);

	sip_utils::UnQuote(realm);
	sip_utils::UnQuote(nonce);
	sip_utils::UnQuote(qop);
	sip_utils::UnQuote(opaque);

	if (nonce != m_nonce)
	{
		m_register_qop_count = 0;
		m_nonce = nonce;
	}

	bool hasQop = !qop.isEmpty();

	sip_value_auth_t* authorization = auth.make_Authorization_value();

	if (!hasQop)
	{
		/*MD5::A1Hash a1(auth_id, realm, auth_pwd);
		MD5::A2Hash a2(startLine.GetRequestLine()->GetMethod(), uriText, qop, entity);
		rtl::String hResponse = MD5::MD5Authorization::Construct(a1.AsString(), nonce, a2.AsString());*/

		char ha1[MD5_HASH_HEX_SIZE+1];
		char ha2[MD5_HASH_HEX_SIZE+1];
		char response[MD5_HASH_HEX_SIZE+1];

		DigestCalcHA1_Sess(alg, auth_id, realm, auth_pwd, nonce, "", ha1);
		DigestCalcHA2(auth.get_request_method_name(), uriText, ha2);
		DigestCalcResponse(ha1, nonce, ha2, response);
		rtl::String hResponse = response;

		rtl::String st;

		authorization->set_scheme("Digest");
		authorization->set_auth_param("username", sip_utils::Quote(auth_id));
		authorization->set_auth_param("realm", sip_utils::Quote(realm));
		authorization->set_auth_param("nonce", sip_utils::Quote(nonce));
		authorization->set_auth_param("uri", sip_utils::Quote(uriText));
		authorization->set_auth_param("response", sip_utils::Quote(hResponse));
		
		if (!alg.isEmpty())
			authorization->set_auth_param("algorithm", alg);
	}
	else
	{
		char cnonce[MD5_HASH_HEX_SIZE+1];
		DigestCalcHA2(qop, uriText, cnonce);

		char ha1[MD5_HASH_HEX_SIZE+1];
		char ha2[MD5_HASH_HEX_SIZE+1];
		char response[MD5_HASH_HEX_SIZE+1];
		char nc[16];

		int len = std_snprintf(nc, 16, "%08x", ++m_register_qop_count);
		nc[len] = 0;

		DigestCalcHA1_Sess(alg, auth_id, realm, auth_pwd, nonce, cnonce, ha1);

		if (qop.indexOf(',') > 0)
		{
			qop = "auth";
		}

		DigestCalcHA2_Int(auth.get_request_method_name(), uriText, qop, entity, ha2);
		DigestCalcResponse_Qop(ha1, nonce, nc, cnonce, qop, ha2, response);
		rtl::String hResponse = response;

		rtl::String st;

		authorization->set_scheme("Digest");
		authorization->set_auth_param("username", sip_utils::Quote(auth_id));
		authorization->set_auth_param("realm", sip_utils::Quote(realm));
		authorization->set_auth_param("nonce", sip_utils::Quote(nonce));
		authorization->set_auth_param("uri", sip_utils::Quote(uriText));
		authorization->set_auth_param("response", sip_utils::Quote(hResponse));
		authorization->set_auth_param("cnonce", sip_utils::Quote(cnonce));
		authorization->set_auth_param("nc", nc);
		authorization->set_auth_param("qop", qop);//sip_utils::Quote(qop));
		
		if (!alg.isEmpty())
			authorization->set_auth_param("algorithm", alg);
		if (!opaque.isEmpty())
			authorization->set_auth_param("opaque", sip_utils::Quote(opaque));
	}

	auth.Via_Branch(sip_utils::GenBranchParameter());

	return true;
}
//--------------------------------------
// добавление авторизации в пакет
//--------------------------------------
bool sip_reg_session_client_t::construct_authentication(const sip_message_t& regMsg, const sip_message_t& challenge, sip_message_t& auth)
{
	rtl::String auth_id = m_profile.get_auth_id();
	rtl::String auth_pwd = m_profile.get_auth_pwd();

	if (!challenge.has_Proxy_Authenticate())
		return false;

	const sip_value_auth_t* proxyAuth = challenge.get_Proxy_Authenticate_value();

	/// clone the original register message
	auth = regMsg;

	auth.From_Tag(sip_utils::GenTagParameter());

	/// increment cseq
	auth.CSeq_Increment();

	rtl::String uriText = auth.get_request_uri().to_string(false);

	rtl::String realm, alg;
	rtl::String nonce;
	rtl::String qop, opaque;
	rtl::String entity = "";

	proxyAuth->get_auth_param("realm", realm);
	proxyAuth->get_auth_param("nonce", nonce);
	proxyAuth->get_auth_param("qop", qop);
	proxyAuth->get_auth_param("opaque", opaque);
	proxyAuth->get_auth_param("algorithm", alg);

	sip_utils::UnQuote(realm);
	sip_utils::UnQuote(nonce);
	sip_utils::UnQuote(qop);
	sip_utils::UnQuote(opaque);

	if (nonce != m_nonce)
	{
		m_register_qop_count = 0;
		m_nonce = nonce;
	}

	bool hasQop = !qop.isEmpty();

	sip_value_auth_t* authorization = auth.make_Proxy_Authorization_value();

	if (!hasQop)
	{
		/*MD5::A1Hash a1(auth_id,realm, auth_pwd);
		MD5::A2Hash a2(startLine.GetRequestLine()->GetMethod(), uriText);
		rtl::String hResponse = MD5::MD5Authorization::Construct(a1.AsString(), nonce, a2.AsString());*/

		char ha1[MD5_HASH_HEX_SIZE+1];
		char ha2[MD5_HASH_HEX_SIZE+1];
		char response[MD5_HASH_HEX_SIZE+1];

		DigestCalcHA1_Sess(alg, auth_id, realm, auth_pwd, nonce, "", ha1);
		DigestCalcHA2(auth.get_request_method_name(), uriText, ha2);
		DigestCalcResponse(ha1, nonce, ha2, response);
		rtl::String hResponse = response;

		authorization->set_scheme("Digest");
		authorization->set_auth_param("username", sip_utils::Quote(auth_id));
		authorization->set_auth_param("realm", sip_utils::Quote(realm));
		authorization->set_auth_param("nonce", sip_utils::Quote(nonce));
		authorization->set_auth_param("uri", sip_utils::Quote(uriText));
		authorization->set_auth_param("response", sip_utils::Quote(hResponse));
		
		if (!alg.isEmpty())
			authorization->set_auth_param("algorithm", alg);
		if (!opaque.isEmpty())
			authorization->set_auth_param("opaque", sip_utils::Quote(opaque));
	}
	else
	{
		char cnonce[MD5_HASH_HEX_SIZE+1];
		DigestCalcHA2(qop, uriText, cnonce);

		char ha1[MD5_HASH_HEX_SIZE+1];
		char ha2[MD5_HASH_HEX_SIZE+1];
		char response[MD5_HASH_HEX_SIZE+1];
		char nc[16];

		int len = std_snprintf(nc, 16, "%08x", ++m_register_qop_count);
		nc[len] = 0;

		DigestCalcHA1_Sess(alg, auth_id, realm, auth_pwd, nonce, cnonce, ha1);

		if (qop.indexOf(',') > 0)
		{
			qop = "auth";
		}

		DigestCalcHA2_Int(auth.get_request_method_name(), uriText, qop, entity, ha2);
		DigestCalcResponse_Qop(ha1, nonce, nc, cnonce, qop, ha2, response);
		rtl::String hResponse = response;

		rtl::String st;

		authorization->set_scheme("Digest");
		authorization->set_auth_param("username", sip_utils::Quote(auth_id));
		authorization->set_auth_param("realm", sip_utils::Quote(realm));
		authorization->set_auth_param("nonce", sip_utils::Quote(nonce));
		authorization->set_auth_param("uri", sip_utils::Quote(uriText));
		authorization->set_auth_param("response", sip_utils::Quote(hResponse));
		authorization->set_auth_param("cnonce", sip_utils::Quote(cnonce));
		authorization->set_auth_param("nc", nc);
		authorization->set_auth_param("qop", qop);//sip_utils::Quote(qop));
		if (!alg.isEmpty())
			authorization->set_auth_param("algorithm", alg);
		if (!opaque.isEmpty())
			authorization->set_auth_param("opaque", sip_utils::Quote(opaque));
	}

	auth.Via_Branch(sip_utils::GenBranchParameter());

	return true;
}
//--------------------------------------
// 
//--------------------------------------
in_addr sip_reg_session_client_t::get_via_address()
{
	return m_route.if_address;
}
//--------------------------------------
// 
//--------------------------------------
uint16_t sip_reg_session_client_t::get_via_port()
{
	return m_route.if_port;
}
//--------------------------------------
// добавление авторизации в пакет
//--------------------------------------
static const char* GetSessionEventName(int eventId)
{
	static const char* names[] = { "NewRegistration", "Registration", "RegistrationExpire", "Unregister", "Options" };
	size_t idx = eventId - 101;
	return idx >= 0 && idx < sizeof(names) / sizeof(const char*)? names[idx] : "UnknownEvent";
}
//--------------------------------------
