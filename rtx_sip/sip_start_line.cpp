/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_start_line.h"
//--------------------------------------
//
//--------------------------------------
void sip_start_line_t::cleanup()
{
	if (m_request != nullptr)
	{
		DELETEO(m_request);
		m_request = nullptr;
	}

	if (m_response != nullptr)
	{
		DELETEO(m_response);
		m_response = nullptr;
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_start_line_t::set_request_line(const rtl::String& method, const sip_uri_t& uri, const rtl::String& version)
{
	cleanup();

	m_request = NEW sip_request_line_t(method, uri, version);
}
//--------------------------------------
//
//--------------------------------------
void sip_start_line_t::set_status_line(const rtl::String& version, uint16_t code, const rtl::String& reason)
{
	cleanup();

	m_response = NEW sip_status_line_t(version, code, reason);
}
//--------------------------------------
//--------------------------------------
//--------------------------------------
//
//--------------------------------------
//void sip_start_line2_t::cleanup()
//{
//	if (m_request != nullptr)
//	{
//		DELETEO(m_request);
//		m_request = nullptr;
//	}
//
//	if (m_response != nullptr)
//	{
//		DELETEO(m_response);
//		m_response = nullptr;
//	}
//}
////--------------------------------------
////
////--------------------------------------
//void sip_start_line2_t::set_request_line(const char* method, const sip_uri2_t uri, const char* version)
//{
//	cleanup();
//
//	m_request = NEW sip_request_line2_t(method, uri, version);
//}
////--------------------------------------
////
////--------------------------------------
//void sip_start_line2_t::set_status_line(const char* version, uint16_t code, const char* reason)
//{
//	cleanup();
//
//	m_response = NEW sip_status_line2_t(version, code, reason);
//}
//--------------------------------------
