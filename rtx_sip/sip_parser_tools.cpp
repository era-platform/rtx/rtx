/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "std_sys_env.h"
#include "sip_parser_tools.h"
//--------------------------------------
//
//--------------------------------------
#define GS_HASH_KEY_1 0x5060
#define GS_HASH_KEY_2 0x50

uint16_t sip_utils::m_DefaultHashKey1 = GS_HASH_KEY_1;
uint8_t sip_utils::m_DefaultHashKey2 = GS_HASH_KEY_2;

static const char* s_known_headers[] = 
{
	"accept",
	"accept-encoding",
	"accept-language",
	"alert-info",
	"allow",
	"allow-events",
	"authentication-info",
	"authorization",
	"call-id",
	"call-info",
	"contact",
	"content-disposition",
	"content-encoding",
	"content-language",
	"content-length",
	"content-type",
	"cseq",
	"date",
	"error-info",
	"event",
	"expires",
	"from",
	"in-reply-to",
	"max-forwards",
	"mime-version",
	"min-expires",
	"min-se",
	"organization",
	"priority",
	"proxy-authenticate",
	"proxy-authorization",
	"proxy-require",
	"record-route",
	"refer-to",
	"reply-to",
	"require",
	"retry-after",
	"route",
	"server",
	"session-expires",
	"sip-etag",
	"sip-if-match",
	"subject",
	"subscription-state",
	"supported",
	"TimeStamp",
	"to",
	"unsupported",
	"user-agent",
	"via",
	"warning",
	"www-authenticate",
};

static int strcompare(const void* l, const void* r)
{
	return strcmp((const char*)l, (const char*)r);
}

bool sip_utils::IsKnownHeader(const rtl::String& header)
{
	rtl::String h = header.toLower();

	void* result = bsearch(h, s_known_headers, sizeof(s_known_headers)/sizeof(const char*), sizeof(const char*), strcompare);

	return result != NULL;
}


void sip_utils::SliceMIME(const rtl::String& mime, rtl::String& name, rtl::String& value)
{
	if (mime.isEmpty())
		return;

	int colon = mime.indexOf(": ");

	if (colon != BAD_INDEX)
	{
		name = mime.substring(0, colon);
		value = mime.substring(colon + 2);
	}
	else
	{
		colon = mime.indexOf(':');
		if (colon != BAD_INDEX)
		{
			name = mime.substring(colon);
			if (sip_utils::IsKnownHeader(name))
			{
				value = mime.substring(colon + 1);
			}
			else
			{
				name = rtl::String::empty;
				value = mime;
			}
		}
		else
		{
			value = mime;
		}
	}
}

bool sip_utils::IsNumeric(const rtl::String& value)
{
	int size = value.getLength();

	for (int j = 0; j < size; j++)
		if (::isalpha(value[j]) != 0)
			return false;

	return true;
}

//rtl::String sip_utils::EscapeAsRFC2396(const rtl::String& str)
//{
//	rtl::String xlat = str;
//
//	const char * safeChars = "abcdefghijklmnopqrstuvwxyz"
//		"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
//		"0123456789$-_.!*'(),";
//
//	int pos = (int)-1;
//
//	while ((pos += (int)(1 + strspn(&xlat[pos+1], safeChars))) < xlat.getLength())
//		xlat.Splice(psprintf("%%%02X", (uint8_t)xlat[pos]), pos, 1);
//
//	return (const char *)xlat;
//}
//
//
//rtl::String sip_utils::UnescapeAsRFC2396(const rtl::String & str)
//{
//	rtl::String xlat = str;
//	xlat.MakeUnique();
//
//	int pos;
//
//
//	pos = (int)-1;
//	while ((pos = xlat.Find('%', pos+1)) != P_MAX_INDEX) {
//		int digit1 = xlat[pos+1];
//		int digit2 = xlat[pos+2];
//		if (isxdigit(digit1) && isxdigit(digit2)) {
//			xlat[pos] = (char)(
//				(isdigit(digit2) ? (digit2-'0') : (toupper(digit2)-'A'+10)) +
//				((isdigit(digit1) ? (digit1-'0') : (toupper(digit1)-'A'+10)) << 4));
//			xlat.Delete(pos+1, 2);
//		}
//	}
//
//	return (const char *)xlat;
//}
//
//
rtl::String sip_utils::GenGUID()
{
	guid_t guid = std_generate_guid();

	char gs[64];

	std_guid_to_string(guid, gs, 64);

	return rtl::String(gs);
}
//
////rtl::String sip_utils::GetRFC1123Date()
////{
////	/// Date: Sat, 13 Nov 2010 23:29:00 GMT
////	PTime now;
////	return (const char *)now.AsString(PTime::RFC1123, PTime::GMT);
////}
//
rtl::String sip_utils::GenTagParameter()
{
	char tag[128];

	static volatile uint32_t counter = 135747;

	uint32_t l = std_interlocked_inc(&counter);

	srand(rtl::DateTime::getTicks());

	uint32_t m = rand();

	if ((m & 0x000000FF) < 0xFF)
		m |= 0x5D;

	if ((l & 0x8000) == 0)
		l |= 0x8000;

	if ((l & 0x00000000) == 0)
		l |= 0xD0000000;


	int len = std_snprintf(tag, 128, "%u%u%u", (l & 0x0000FFFF), m, (l >> 16));
	tag[len] = 0;

	return rtl::String(tag);
}
//
rtl::String sip_utils::GenCallId(const char* cookie)
{
	uint32_t rr;
	static volatile uint32_t s_call_id_gen = 18273;
	char tmp[256];
	srand(rtl::DateTime::getTicks());
	rr = rand();
	int32_t call_id_val = std_interlocked_inc(&s_call_id_gen);

	rr ^= 0x5DAD5D9A;
	int len = std_snprintf(tmp, 255, "%u%u%u@%s", (call_id_val & 0xFFFF), rr, (call_id_val >> 16), cookie ? cookie : "aS1k2d3u323eD");
	tmp[len] = 0;
	return rtl::String(tmp);
}
//
rtl::String sip_utils::GenBranchParameter()
{
	static const rtl::String magicCookie = "z9hG4bK";
	return magicCookie + sip_utils::GenTagParameter();
}
//
//rtl::String sip_utils::GetReasonPhrase(uint16_t statusCode)
//{
//	static rtl::String s100("Trying");
//	static rtl::String s180("Ringing");
//	static rtl::String s181("Call Is Being Forwarded");
//	static rtl::String s182("Queued");
//	static rtl::String s183("Session Progress");
//	static rtl::String s200("OK");
//	static rtl::String s202("Accepted");
//	static rtl::String s300("Multiple Choices");
//	static rtl::String s301("Moved Permanently");
//	static rtl::String s302("Moved Temporarily");
//	static rtl::String s305("Use Proxy");
//	static rtl::String s380("Alternative Service");
//	static rtl::String s400("Bad Request");
//	static rtl::String s401("Unauthorized");
//	static rtl::String s402("Payment Required");
//	static rtl::String s403("Forbidden");
//	static rtl::String s404("Not Found");
//	static rtl::String s405("Method Not Allowed");
//	static rtl::String s406("Not Acceptable");
//	static rtl::String s407("Proxy Authentication Required");
//	static rtl::String s408("Request Timeout");
//	static rtl::String s409("Conflict");
//	static rtl::String s410("Gone");
//	static rtl::String s411("Length Required");
//	static rtl::String s412("Conditional Request Failed");
//	static rtl::String s413("Request Entity Too Large");
//	static rtl::String s414("Request-URI Too Large");
//	static rtl::String s415("Unsupported Media Type");
//	static rtl::String s416("Unsupported Uri Scheme");
//	static rtl::String s420("Bad Extension");
//	static rtl::String s422("Session Interval Too Small");
//	static rtl::String s423("Interval Too Short");
//	static rtl::String s480("Temporarily not available");
//	static rtl::String s481("Call Leg/Transaction Does Not Exist");
//	static rtl::String s482("Loop Detected");
//	static rtl::String s483("Too Many Hops");
//	static rtl::String s484("Address Incomplete");
//	static rtl::String s485("Ambiguous");
//	static rtl::String s486("Busy Here");
//	static rtl::String s487("Request Cancelled");
//	static rtl::String s488("Not Acceptable Here");
//	static rtl::String s489("Bad Request");
//	static rtl::String s491("Request Pending");
//	static rtl::String s500("Internal Server Error");
//	static rtl::String s501("Not Implemented");
//	static rtl::String s502("Bad Gateway");
//	static rtl::String s503("Service Unavailable");
//	static rtl::String s504("Gateway Time-out");
//	static rtl::String s505("SIP Version not supported");
//	static rtl::String s600("Busy Everywhere");
//	static rtl::String s603("Decline");
//	static rtl::String s604("Does not exist anywhere");
//	static rtl::String s606("Not Acceptable");
//
//	static rtl::String s999("Reason Unknown");
//
//	int i = statusCode / 100;
//
//	if (i == 1)
//	{				/* 1xx  */
//		if (statusCode == 100)
//			return s100;
//		if (statusCode == 180)
//			return s180;
//		if (statusCode == 181)
//			return s181;
//		if (statusCode == 182)
//			return s182;
//		if (statusCode == 183)
//			return s183;
//	}else if (i == 2)
//	{	if (statusCode == 200)
//	return s200;
//	if (statusCode == 202)
//		return s202;
//	}else if (i == 3)
//	{				/* 3xx */
//		if (statusCode == 300)
//			return s300;
//		if (statusCode == 301)
//			return s301;
//		if (statusCode == 302)
//			return s302;
//		if (statusCode == 305)
//			return s305;
//		if (statusCode == 380)
//			return s380;
//	}else if (i == 4)
//	{				/* 4xx */
//		if (statusCode == 400)
//			return s400;
//		if (statusCode == 401)
//			return s401;
//		if (statusCode == 402)
//			return s402;
//		if (statusCode == 403)
//			return s403;
//		if (statusCode == 404)
//			return s404;
//		if (statusCode == 405)
//			return s405;
//		if (statusCode == 406)
//			return s406;
//		if (statusCode == 407)
//			return s407;
//		if (statusCode == 408)
//			return s408;
//		if (statusCode == 409)
//			return s409;
//		if (statusCode == 410)
//			return s410;
//		if (statusCode == 411)
//			return s411;
//		if (statusCode == 413)
//			return s413;
//		if (statusCode == 414)
//			return s414;
//		if (statusCode == 415)
//			return s415;
//		if (statusCode == 416)
//			return s416;
//		if (statusCode == 420)
//			return s420;
//		if (statusCode == 422)
//			return s422;
//		if (statusCode == 423)
//			return s423;
//		if (statusCode == 480)
//			return s480;
//		if (statusCode == 481)
//			return s481;
//		if (statusCode == 482)
//			return s482;
//		if (statusCode == 483)
//			return s483;
//		if (statusCode == 484)
//			return s484;
//		if (statusCode == 485)
//			return s485;
//		if (statusCode == 486)
//			return s486;
//		if (statusCode == 487)
//			return s487;
//		if (statusCode == 488)
//			return s488;
//		if (statusCode == 489)
//			return s489;
//		if (statusCode == 491)
//			return s491;
//	}else if (i == 5)
//	{				/* 5xx */
//		if (statusCode == 500)
//			return s500;
//		if (statusCode == 501)
//			return s501;
//		if (statusCode == 502)
//			return s502;
//		if (statusCode == 503)
//			return s503;
//		if (statusCode == 504)
//			return s504;
//		if (statusCode == 505)
//			return s505;
//	}else if (i == 6)
//	{				/* 6xx */
//		if (statusCode == 600)
//			return s600;
//		if (statusCode == 603)
//			return s603;
//		if (statusCode == 604)
//			return s604;
//		if (statusCode == 606)
//			return s606;
//	}
//
//	return s999;
//}
//
//BOOL sip_utils::MsgTokenize(const rtl::String & msg, mbstring_tArray & finalTokens)
//{
//	mbstring_tArray tokens;
//
//	int lastOffSet  = 0;
//	int offSet = 0;
//	BOOL ok = FALSE;
//
//	///got start of message
//	BOOL gotSOM = FALSE;
//
//	BOOL crLFCompliant = msg.Find("\r\n") != P_MAX_INDEX;
//
//	for(;;)
//	{
//		lastOffSet = offSet;
//
//		if(!crLFCompliant)
//			offSet = msg.FindOneOf("\r\n", offSet);
//		else
//			offSet = msg.Find("\r\n", offSet);
//
//		if(offSet != P_MAX_INDEX)
//		{
//			//
//			//  lastOffSet++;
//			rtl::String token = msg.Mid(lastOffSet, ++offSet - lastOffSet);
//
//
//			char firstChar = token[1];
//			token = token.Trim();
//
//			if (firstChar >= 32 && ::isspace(firstChar) && !token.IsEmpty())
//				token = "  " + token;
//
//			if(!gotSOM && token.IsEmpty())
//				continue;
//			else
//			{
//				tokens.AppendString(token);
//				gotSOM = TRUE;
//			}
//
//			ok = TRUE;
//		}else
//		{
//			break;
//		}
//
//
//	}
//
//	/// now check if there are tokens that has embedded CRLF
//
//	rtl::String buff, buff2;
//	int size = tokens.GetSize();
//	for(int i = 0; i < size; i++)
//	{
//		buff2 = rtl::String::Empty();
//		buff += tokens[i];
//
//		if(i + 1 == size)
//		{
//			finalTokens.AppendString(buff);
//			break;
//		}
//
//		buff2 = tokens[i+1];
//		char firstChar = buff2[0];
//		if (firstChar >= 32 && ::isspace(firstChar))
//		{
//			buff += "\r\n";
//		}
//		else
//		{
//			buff = buff.Trim();
//			finalTokens.AppendString(buff);
//			buff = rtl::String::Empty();
//		}
//	}
//
//	return ok;
//}
//
//BOOL sip_utils::MsgTokenize(const PBYTEArray & _bytes, mbstring_tArray & tokens)
//{
//	PBYTEArray bytes = _bytes;
//	bytes.SetSize(_bytes.GetSize() + 1);
//	bytes[_bytes.GetSize()]='\0';
//	rtl::String msg((const char*)bytes.GetPointer());
//	msg.MakeUnique();
//	return sip_utils::MsgTokenize(msg, tokens);
//}
//
//rtl::String sip_utils::GetExpandedHeader(const rtl::String & compactHeader)
//{
//	if(compactHeader.IsEmpty() || compactHeader.GetLength() > 1)
//		return compactHeader;
//
//	char c = compactHeader[0];
//
//	if(c == 'a')
//		return "Accept-Contact";
//	else if(c == 'b')
//		return "Referred-By";
//	else if(c == 'c')
//		return "Content-Type";
//	else if(c == 'e')
//		return "Content-Encoding";
//	else if(c == 'f')
//		return "From";
//	else if(c == 'i')
//		return "Call-ID";
//	else if(c == 'k')
//		return "Supported";
//	else if(c == 'l')
//		return "Content-Length";
//	else if(c == 'm')
//		return "Contact";
//	else if(c == 'o')
//		return "Event";
//	else if(c == 'r')
//		return "Refer-To";
//	else if(c == 's')
//		return "Subject";
//	else if(c == 't')
//		return "To";
//	else if(c == 'u')
//		return "Allow-Events";
//	else if(c == 'v')
//		return "Via";
//
//	return compactHeader;
//}
//
//const char* sip_utils::get_expanded_header(const char* compactHeader)
//{
//	if(compactHeader == NULL || compactHeader[0] == 0 || compactHeader[1] != 0)
//		return compactHeader;
//
//	char c = compactHeader[0];
//
//	switch (c)
//	{
//	case 'a':
//		return "Accept-Contact";
//	case 'b':
//		return "Referred-By";
//	case 'c':
//		return "Content-Type";
//	case 'e':
//		return "Content-Encoding";
//	case 'f':
//		return "From";
//	case 'i':
//		return "Call-ID";
//	case 'k':
//		return "Supported";
//	case 'l':
//		return "Content-Length";
//	case 'm':
//		return "Contact";
//	case 'o':
//		return "Event";
//	case 'r':
//		return "Refer-To";
//	case 's':
//		return "Subject";
//	case 't':
//		return "To";
//	case 'u':
//		return "Allow-Events";
//	case 'v':
//		return "Via";
//	}
//
//	return compactHeader;
//}
//
//rtl::String sip_utils::GetCompactedHeader(const rtl::String & expandedHeader)
//{
//	rtl::String h = expandedHeader.ToLower();
//
//	if(h == "accept-contact")
//		return "a";
//	else if(h == "referred-by")
//		return "b";
//	else if(h == "content-type")
//		return "c";
//	else if(h == "content-encoding")
//		return "e";
//	else if(h == "from")
//		return "f";
//	else if(h == "call-id")
//		return "i";
//	else if(h == "supported")
//		return "k";
//	else if(h == "content-length")
//		return "l";
//	else if(h == "contact")
//		return "m";
//	else if(h == "event")
//		return "o";
//	else if(h == "refer-to")
//		return "r";
//	else if(h == "subject")
//		return "s";
//	else if(h == "to")
//		return "t";
//	else if(h == "allow-events")
//		return "u";
//	else if(h == "via")
//		return "v";
//
//	return expandedHeader;
//
//}
//
//rtl::String sip_utils::AsHex(const uint8_t* digest)
//{
//	uint8_t j;
//	char hex[33];
//
//    for (int i = 0; i < 16; i++)
//	{
//        j = (digest[i] >> 4) & 0xf;
//        if (j <= 9)
//		{
//			hex[i*2] = (j + '0');
//		}
//		else
//		{
//			hex[i*2] = (j + 'a' - 10);
//		}
//        
//		j = digest[i] & 0xf;
//        if (j <= 9)
//		{
//			hex[i*2+1] = (j + '0');
//		}
//		else
//		{
//			hex[i*2+1] = (j + 'a' - 10);
//		}
//    };
//
//    hex[32] = 0;
//
//	return rtl::String(hex);
//}
//
rtl::String& sip_utils::Quote(rtl::String& str)
{
	if (str[0] != '\"')
		str.insert(0, '"');
	
	if (str[str.getLength() - 1] != '\"')
		str += '\"';
	
	return str;
}
rtl::String sip_utils::Quote(const rtl::String& str)
{
	rtl::String strm;
	
	if (str[0] != '\"')
		strm << '"';
	
	strm << str;

	if (str[str.getLength() - 1] != '\"')
		strm << '\"';

	return strm;
}
//
rtl::String& sip_utils::UnQuote(rtl::String& str)
{
	return str.trim("\"");
}

rtl::String sip_utils::UnQuote(const rtl::String& str)
{
	return str.trim("\"");
}


//BOOL sip_utils::WildCardCompare(ZString wild, ZString string) 
//{
//	ZString cp = NULL, mp = NULL;
//
//	while ((*string) && (*wild != '*'))
//	{
//		if ((*wild != *string) && (*wild != '?'))
//		{
//			return 0;
//		}
//		wild++;
//		string++;
//	}
//
//	while (*string)
//	{
//		if (*wild == '*')
//		{
//			if (!*++wild)
//			{
//				return 1;
//			}
//			mp = wild;
//			cp = string+1;
//		}
//		else if ((*wild == *string) || (*wild == '?'))
//		{
//			wild++;
//			string++;
//		}
//		else
//		{
//			wild = mp;
//			string = cp++;
//		}
//	}
//
//	while (*wild == '*')
//	{
//		wild++;
//	}
//
//	return !*wild;
//}
//
//void sip_utils::EndianSwap(uint16_t& x)
//{
//	x = (x>>8) | 
//		(x<<8);
//}
//
//void sip_utils::EndianSwap(DWORD& x)
//{
//	int i = (x>>24) | 
//		((x<<8) & 0x00FF0000) |
//		((x>>8) & 0x0000FF00) |
//		(x<<24);
//
//	x = (DWORD)i;
//}
//
//#ifdef P_HAS_INT64
//void sip_utils::EndianSwap(PUInt64& x)
//{
//	x = (x>>56) | 
//		((x<<40) & 0x00FF000000000000) |
//		((x<<24) & 0x0000FF0000000000) |
//		((x<<8)  & 0x000000FF00000000) |
//		((x>>8)  & 0x00000000FF000000) |
//		((x>>24) & 0x0000000000FF0000) |
//		((x>>40) & 0x000000000000FF00) |
//		(x<<56);
//}
//#endif
//
//
//rtl::String sip_utils::StripHTMLTags(const rtl::String& htmlPart)
//{
//	rtl::String plainText;
//	BOOL insideTag = FALSE;
//
//	for(int i = 0; i < htmlPart.GetLength(); i++)
//	{
//		rtl::String c = htmlPart[i];
//		if(c == "<")
//			insideTag = TRUE;
//		else if(c == ">")
//			insideTag = FALSE;
//		else
//			if(!insideTag)
//			{
//				char ch = c[0];
//				if(ch >= 32 && ::isspace(ch))
//					c == " ";
//
//				if(c[0] == '\r' || c[0] == '\n')
//					c = "";
//
//				plainText += c;
//			}
//	}
//
//	return plainText.Trim();
//}

//int sip_utils::STLTokenize(const char * _str,
//              vector<string>& tokens,
//              const char * _delimiters)
//{
//  string str = _str;
//  string delimiters = _delimiters;
//  
//    // Skip delimiters at beginning.
//  string::size_type lastPos = str.find_first_not_of(delimiters, 0);
//    // Find first "non-delimiter".
//  string::size_type pos     = str.find_first_of(delimiters, lastPos);
//
//  while (string::npos != pos || string::npos != lastPos)
//  {
//        // Found a token, add it to the vector.
//    tokens.push_back(str.substr(lastPos, pos - lastPos));
//        // Skip delimiters.  Note the "not_of"
//    lastPos = str.find_first_not_of(delimiters, pos);
//        // Find next "non-delimiter"
//    pos = str.find_first_of(delimiters, lastPos);
//  }
//  
//  int size = tokens.size();
//  if(size == 0)
//    tokens.push_back(str);
//  
//  return size;
//}
//
//int sip_utils::STLTokenize(
//  const string & str,
//  vector<string>& tokens,
//  const char * delimiters
//)
//{
//  return sip_utils::STLTokenize(str, tokens, delimiters);
//}
//
//
//
//
//

//--------------------------------------
