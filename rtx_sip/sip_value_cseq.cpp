/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_value_cseq.h"
//--------------------------------------
//
//--------------------------------------
inline const char* SKIP_WS(const char* text)
{
	char ch = *text;

	while (ch != 0 && (ch == ' ' || ch == '\t' || ch == '\r' || ch == '\n'))
	{
		ch = *(++text);
	}

	return text;
}
//--------------------------------------
//
//--------------------------------------
sip_value_cseq_t::~sip_value_cseq_t()
{
	cleanup();
}
//--------------------------------------
//
//--------------------------------------
void sip_value_cseq_t::cleanup()
{
	m_cseq = 0;
	m_method.setEmpty();
}
//--------------------------------------
//
//--------------------------------------
rtl::String& sip_value_cseq_t::prepare(rtl::String& stream) const
{
	return stream << m_cseq << ' ' << m_method;
}
//--------------------------------------
//
//--------------------------------------
bool sip_value_cseq_t::parse()
{
	int index = m_value.indexOf(L' ');

	if (index > 0)
	{
		m_cseq = strtoul(m_value, nullptr, 10);
		m_method = m_value.substring(index+1).trim();
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
sip_value_cseq_t& sip_value_cseq_t::assign(const sip_value_cseq_t& value)
{
	sip_value_t::assign(value);

	m_cseq = value.m_cseq;
	m_method = value.m_method;
	
	return *this;
}
//--------------------------------------
//
//--------------------------------------
sip_value_t* sip_value_cseq_t::clone() const
{
	return NEW sip_value_cseq_t(*this);
}
//--------------------------------------
