/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "sip_transaction_nic.h"
#include "sip_transaction_manager.h"
//--------------------------------------
//
//--------------------------------------
static const char* GetStateName(int state)
{
	static const char* names[] = { "Idle", "Trying", "Proceeding", "Completed", "Terminated" };

	return state >= 0 && state <= sip_transaction_nic_t::nic_transaction_state_terminated_e ? names[state] : "Unknown";
}
//--------------------------------------
//
//--------------------------------------
sip_transaction_nic_t::sip_transaction_nic_t(const rtl::String& id, sip_transaction_manager_t& manager) :
sip_transaction_t(id, sip_transaction_nict_e, manager)
{
	m_has_sent_initial_request = false;

	SLOG_TRANS(get_type_name(), "%s Created with Id: %s", m_internal_id, (const char*)id);
}
//--------------------------------------
//
//--------------------------------------
sip_transaction_nic_t::~sip_transaction_nic_t()
{
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_nic_t::process_sip_transaction_event(sip_message_t& message)
{
	//if (m_call_id.isEmpty())
	//{
	//	m_call_id = message.CallId();
	//}

	SLOG_TRANS(get_type_name(), "%s  State.%s", m_internal_id, GetStateName(m_state));
	switch (m_state)
	{
	case nic_transaction_state_idle_e:
		process_idle_state(message);
		break;
	case nic_transaction_state_trying_e:
		process_trying_state(message);
		break;
	case nic_transaction_state_proceeding_e:
		process_proceeding_state(message);
		break;
	case nic_transaction_state_completed_e:
		process_completed_state(message);
		break;
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_nic_t::process_sip_timer_event(sip_transaction_timer_t timerType, uint32_t dwInterval)
{
	SLOG_TRANS(get_type_name(), "%s Timer event %s state %s",
			m_internal_id, sip_timer_manager_t::GetTimerName(timerType), GetStateName(m_state));

	switch (timerType)
	{
	case sip_timer_life_span_e:
		set_state(nic_transaction_state_terminated_e);
		m_manager.process_timer_expire_event(timerType, *this);
		destroy();
		break;
	case sip_timer_E_e:
		if (m_state == nic_transaction_state_trying_e)
		{
			/*If timer E fires while still in this state,
			the timer is reset, but this time with a value of MIN(2*T1, T2).
			When the timer fires again, it is reset to a MIN(4*T1, T2).*/

			send_message_to_transport(m_opening_request);
			uint32_t interval = dwInterval * 2 > SIP_INTERVAL_T2 ? SIP_INTERVAL_T2 : dwInterval * 2;

			SLOG_TRANS(get_type_name(), "%s Timer E : retransmit message and restart timer E (%d ms).", m_internal_id, interval);

			start_timer(sip_timer_E_e, interval);
		}
		else if (m_state == nic_transaction_state_proceeding_e)
		{
			/*
			If Timer E fires while in the "Proceeding" state, the request MUST be
			passed to the transport layer for retransmission, and Timer E MUST be
			reset with a value of T2 seconds. 
			*/
			///m_TransportSender(m_opening_request, (INT)this);
			send_message_to_transport(m_opening_request);

			SLOG_TRANS(get_type_name(), "%s Timer E : retransmit message and restart timer E (%d ms).", m_internal_id, SIP_INTERVAL_T2);

			start_timer(sip_timer_E_e, SIP_INTERVAL_T2);
		}
		break;
	case sip_timer_F_e:
		if (m_state == nic_transaction_state_trying_e)
		{
			/* If Timer F fires while the client transaction is still in the
			"Trying" state, the client transaction SHOULD inform the TU about the
			timeout, and then it SHOULD enter the "Terminated" state. */
			
			SLOG_TRANS(get_type_name(), "%s Timer F : Terminating.", m_internal_id);

			set_state(nic_transaction_state_terminated_e);
			stop_timer(sip_timer_E_e);

			m_manager.process_timer_expire_event(timerType, *this);
			destroy();
		}
		else if (m_state == nic_transaction_state_proceeding_e)
		{
			/*If timer F fires while in the
			"Proceeding" state, the TU MUST be informed of a timeout, and the
			client transaction MUST transition to the terminated state.
			*/
			SLOG_TRANS(get_type_name(), "%s Timer F : Terminating.", m_internal_id);

			set_state(nic_transaction_state_terminated_e);
			stop_timer(sip_timer_E_e);
			
			m_manager.process_timer_expire_event(timerType, *this);
			destroy();
		}
		break;
	case sip_timer_K_e:
		/*Once the client transaction enters the "Completed" state, it MUST set
		Timer K to fire in T4 seconds for unreliable transports, and zero
		seconds for reliable transports.*/

		SLOG_TRANS(get_type_name(), "%s Timer K : Terminating.", m_internal_id);

		set_state(nic_transaction_state_terminated_e);
		destroy();
		break;
	default:
		SLOG_WARN(get_type_name(), "%s %s : Invalid timer tick received!", m_internal_id, sip_timer_manager_t::GetTimerName(timerType));
		break;
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_nic_t::process_idle_state(sip_message_t& message)
{
	/*
	17.1.2 Non-INVITE Client Transaction

	17.1.2.1 Overview of the non-INVITE Transaction

	Non-INVITE transactions do not make use of ACK.  They are simple
	request-response interactions.  For unreliable transports, requests
	are retransmitted at an interval which starts at T1 and doubles until
	it hits T2.  If a provisional response is received, retransmissions
	continue for unreliable transports, but at an interval of T2.  The
	server transaction retransmits the last response it sent, which can
	be a provisional or final response, only when a retransmission of the
	request is received.  This is why request retransmissions need to
	continue even after a provisional response; they are to ensure
	reliable delivery of the final response.

	Unlike an INVITE transaction, a non-INVITE transaction has no special
	handling for the 2xx response.  The result is that only a single 2xx
	response to a non-INVITE is ever delivered to a UAC.

	17.1.2.2 Formal Description

	The state machine for the non-INVITE client transaction is shown in
	Figure 6.  It is very similar to the state machine for INVITE.

	The "Trying" state is entered when the TU initiates a NEW client
	transaction with a request.  When entering this state, the client
	transaction SHOULD set timer F to fire in 64*T1 seconds.  The request
	MUST be passed to the transport layer for transmission.  If an
	unreliable transport is in use, the client transaction MUST set timer
	E to fire in T1 seconds.  If timer E fires while still in this state,
	the timer is reset, but this time with a value of MIN(2*T1, T2).
	When the timer fires again, it is reset to a MIN(4*T1, T2).  This
	process continues so that retransmissions occur with an exponentially
	increasing interval that caps at T2.  The default value of T2 is 4s,
	and it represents the amount of time a non-INVITE server transaction
	will take to respond to a request, if it does not respond
	immediately.  For the default values of T1 and T2, this results in
	intervals of 500 ms, 1 s, 2 s, 4 s, 4 s, 4 s, etc.

	If Timer F fires while the client transaction is still in the
	"Trying" state, the client transaction SHOULD inform the TU about the
	timeout, and then it SHOULD enter the "Terminated" state.  If a
	provisional response is received while in the "Trying" state, the
	response MUST be passed to the TU, and then the client transaction
	SHOULD move to the "Proceeding" state.  If a final response (status
	codes 200-699) is received while in the "Trying" state, the response
	MUST be passed to the TU, and the client transaction MUST transition
	to the "Completed" state.

	If Timer E fires while in the "Proceeding" state, the request MUST be
	passed to the transport layer for retransmission, and Timer E MUST be
	reset with a value of T2 seconds.  If timer F fires while in the
	"Proceeding" state, the TU MUST be informed of a timeout, and the
	client transaction MUST transition to the terminated state.  If a
	final response (status codes 200-699) is received while in the
	"Proceeding" state, the response MUST be passed to the TU, and the
	client transaction MUST transition to the "Completed" state.

	Once the client transaction enters the "Completed" state, it MUST set
	Timer K to fire in T4 seconds for unreliable transports, and zero
	seconds for reliable transports.  The "Completed" state exists to
	buffer any additional response retransmissions that may be received
	(which is why the client transaction remains there only for
	unreliable transports).  T4 represents the amount of time the network
	will take to clear messages between client and server transactions.
	The default value of T4 is 5s.  A response is a retransmission when
	it matches the same transaction, using the rules specified in Section
	17.1.3.  If Timer K fires while in this state, the client transaction
	MUST transition to the "Terminated" state.

	Once the transaction is in the terminated state, it MUST be destroyed
	immediately.
	*/


	/*
	The state machine for the INVITE client transaction is shown in
	Figure 5.  The initial state, "calling", MUST be entered when the TU
	initiates a NEW client transaction with an INVITE request.  The
	client transaction MUST pass the request to the transport layer for
	transmission (see Section 18).  If an unreliable transport is being
	used, the client transaction MUST start timer A with a value of T1.
	If a reliable transport is being used, the client transaction SHOULD
	NOT start timer A (Timer A controls request retransmissions).  For
	any transport, the client transaction MUST start timer B with a value
	of 64*T1 seconds (Timer B controls transaction timeouts).
	*/


	if (message.is_request())
	{
		m_has_sent_initial_request = true;
		SLOG_TRANS(get_type_name(), "%s State.Idle -> State.Trying", m_internal_id);
		set_state(nic_transaction_state_trying_e);

		set_opening_request(message);

		if (!is_reliable_transport())
			start_timer(sip_timer_E_e, SIP_INTERVAL_E);

		start_timer(sip_timer_F_e, SIP_INTERVAL_F);

		STR_COPY(m_ua_core_name, message.GetUACoreName());
		
		rtl::String transport;

		if (!message.is_request())
			SLOG_WARN(get_type_name(), "%s State.Idle : sip_transaction_nic_t got none request event on State Idle", m_internal_id);

		send_message_to_transport(message);
	}
	else if (!message.Is1xx() && !message.is_request())  /// we received a 2xx while in state idle ?
	{
		SLOG_TRANS(get_type_name(), "%s (!1xx & !request) State.Trying -> State.Completed", m_internal_id);
		set_state(nic_transaction_state_completed_e);

		if (!is_reliable_transport())
			start_timer(sip_timer_E_e, SIP_INTERVAL_E);

		stop_timer(sip_timer_F_e);
		m_manager.process_received_message_event(message, *this);

#if NICT_USE_TIMER_K
		if (!is_reliable_transport())
		{
			set_state(nic_transaction_state_completed_e);
			start_timer(sip_timer_K_e, SIP_INTERVAL_K);
		}
		else
		{
			set_state(nic_transaction_state_terminated_e);
			destroy();
		}
#else
		set_state(Terminated);
		destroy();
#endif
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_nic_t::process_trying_state(sip_message_t& message)
{

	/*If Timer F fires while the client transaction is still in the
	"Trying" state, the client transaction SHOULD inform the TU about the
	timeout, and then it SHOULD enter the "Terminated" state.  If a
	provisional response is received while in the "Trying" state, the
	response MUST be passed to the TU, and then the client transaction
	SHOULD move to the "Proceeding" state.  If a final response (status
	codes 200-699) is received while in the "Trying" state, the response
	MUST be passed to the TU, and the client transaction MUST transition
	to the "Completed" state.*/

	if (message.is_request())
	{
		SLOG_WARN(get_type_name(), "%s State.Trying : message is request!", m_internal_id);
		return;
	}

	if (message.Is1xx())
	{
		SLOG_TRANS(get_type_name(), "%s (1xx) State.Trying --> State.Proceeding", m_internal_id);
		set_state(nic_transaction_state_proceeding_e);
		m_manager.process_received_message_event(message, *this);
	}
	else
	{
		SLOG_TRANS(get_type_name(), "%s (!1xx) State.Trying --> State.Completed", m_internal_id);

		stop_timer(sip_timer_E_e);
		stop_timer(sip_timer_F_e);
		m_manager.process_received_message_event(message, *this);

#if NICT_USE_TIMER_K
		if (!is_reliable_transport())
		{
			set_state(nic_transaction_state_completed_e);
			start_timer(sip_timer_K_e, SIP_INTERVAL_K);
		}
		else
		{
			set_state(nic_transaction_state_terminated_e);
			destroy();
		}
#else
		set_state(Terminated);
		destroy();
#endif
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_nic_t::process_proceeding_state(sip_message_t& message)
{

	/*If Timer E fires while in the "Proceeding" state, the request MUST be
	passed to the transport layer for retransmission, and Timer E MUST be
	reset with a value of T2 seconds.  If timer F fires while in the
	"Proceeding" state, the TU MUST be informed of a timeout, and the
	client transaction MUST transition to the terminated state.  If a
	final response (status codes 200-699) is received while in the
	"Proceeding" state, the response MUST be passed to the TU, and the
	client transaction MUST transition to the "Completed" state.
	*/

	if (message.is_request() )
	{
		SLOG_TRANS(get_type_name(), "%s Handle State.Proceeding : message is request!", m_internal_id);
		return;
	}

	m_manager.process_received_message_event(message, *this);

	if (!message.Is1xx())  /// final response
	{
		SLOG_TRANS(get_type_name(), "%s (!1xx) State.Proceeding --> State.Completed", m_internal_id);

		set_state(nic_transaction_state_completed_e);
		stop_timer(sip_timer_E_e);
		stop_timer(sip_timer_F_e);
#if NICT_USE_TIMER_K
		if (!is_reliable_transport())
		{
			start_timer(sip_timer_K_e, SIP_INTERVAL_K);
		}
		else
		{
			set_state(nic_transaction_state_terminated_e);
			destroy();
		}
#else
		set_state(Terminated);
		destroy();
#endif
	}

}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_nic_t::process_completed_state(sip_message_t& message)
{
	/*
	Once the client transaction enters the "Completed" state, it MUST set
	Timer K to fire in T4 seconds for unreliable transports, and zero
	seconds for reliable transports.  The "Completed" state exists to
	buffer any additional response retransmissions that may be received
	(which is why the client transaction remains there only for
	unreliable transports).  T4 represents the amount of time the network
	will take to clear messages between client and server transactions.
	The default value of T4 is 5s.  A response is a retransmission when
	it matches the same transaction, using the rules specified in Section
	17.1.3.  If Timer K fires while in this state, the client transaction
	MUST transition to the "Terminated" state.

	Once the transaction is in the terminated state, it MUST be destroyed
	immediately.
	*/

	SLOG_TRANS(get_type_name(), "%s State.Completed : pass to stack", m_internal_id);

	// 29.07.2013
	// ��������� ������ ������ ������������!

	/*if (!message.IsRequest())
	{
		m_manager.OnReceivedMessage(message, *this);
	}*/
}
//--------------------------------------
