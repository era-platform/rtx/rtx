/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "sip_transport_pool.h"
#include "sip_transport_manager.h"
#include "sip_transport.h"
#include "sip_transport_tcp.h"
#include "sip_transport_binding.h"
#include "sip_transport_reader.h"
#include "sip_message.h"
//--------------------------------------
//
//--------------------------------------
#define ASYNC_CALL_PACKET_ARRIVAL			0
#define ASYNC_CALL_PARSER_ERROR				1
#define ASYNC_CALL_DISCONNECTED				2
#define ASYNC_CALL_TRANSPORT_SEND_ERROR		3
#define ASYNC_CALL_TRANSPORT_RECV_ERROR		4
#define ASYNC_CALL_CHECK_LIFE_TIMES			5
#define ASYNC_SEND_MESSAGE					6
//--------------------------------------
// ������� ��� ��������� ����������
//--------------------------------------
sip_transport_manager_event_handler_t* sip_transport_manager_t::m_event_handler = nullptr;

/// ������ ��� ������ �� ����
rtl::Mutex sip_transport_manager_t::m_readerSync;
rtl::ArrayT<sip_transport_reader_t*> sip_transport_manager_t::m_reader_list;

/// ��������� ����
rtl::Mutex sip_transport_manager_t::m_binding_list_sync;
rtl::ArrayT<sip_transport_binding_t*> sip_transport_manager_t::m_binding_list;

/// ����� �����������
sip_transport_pool_t sip_transport_manager_t::m_pool;

volatile bool sip_transport_manager_t::m_discard_subscribes;
//--------------------------------------
//
//--------------------------------------
static void update_message(sip_message_t& message, const sip_transport_route_t& route);
//--------------------------------------
//
//--------------------------------------
void sip_transport_manager_t::create(sip_transport_manager_event_handler_t* event_handler)
{
	SLOG_NET("net-man", "Create transport manager %p", event_handler);

	m_event_handler = event_handler;

	rtl::Timer::setTimer(timer_elapsed_event, nullptr, 0, 1000, true);

#if defined (TARGET_OS_WINDOWS)
	WSAData d;
	int net_result = WSAStartup(MAKEWORD(2, 0), &d);

	if (net_result != 0)
	{
		LOG_NET_ERROR("net-man", "starting : failed : WSAStartup() failed %d", std_get_error);
	}
#endif
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_manager_t::destroy()
{
	// ����� ������� ��� ���������, ������, �������

	SLOG_NET("net-man", "destroying transport manager...");

	rtl::Timer::stopTimer(timer_elapsed_event, nullptr, 0);

	stop_readers();

	destroy_bindings();

	m_pool.cleanup();

#if defined (TARGET_OS_WINDOWS)
	WSACleanup();
#endif

	SLOG_NET("net-man", "transport manager destroyed");
}
//--------------------------------------
//
//--------------------------------------
bool sip_transport_manager_t::start_listener(const sip_transport_point_t& point)
{
	SLOG_NET("net-man", "opening binding to %s interface %s:%u...", TransportType_toString(point.type), inet_ntoa(point.address), point.port);

	bool result = false;
	// ���� ��������� � �������
	// ���� �������, �� ������ �� ������
	// ���� ��� �� �������

	sip_transport_binding_t* binding = get_binding(point);

	if (binding != nullptr)
	{
		result = binding->open();
	}

	SLOG_NET("net-man", "binding %s", result ? "opened" : "not opened");

	return result;
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_manager_t::stop_listener(const sip_transport_point_t& point)
{
	SLOG_NET("net-man", "closing binding to %s interface %s:%u...", TransportType_toString(point.type), inet_ntoa(point.address), point.port);
	// ���� ��������� � �������
	// ���� �������, ������������� � �������
	rtl::MutexLock lock(m_binding_list_sync);
	
	for (int i = m_binding_list.getCount() - 1; i >= 0; i--)
	{
		sip_transport_binding_t* binding = m_binding_list[i];

		if (binding->get_listen_address().s_addr == point.address.s_addr &&
			binding->get_listen_port() == point.port &&
			compare_listener_type(binding->getType(), point.type))
		{
			SLOG_NET("net-man", "closing and destroying binding %s", binding->get_id());
			binding->close();
			binding->destroy();
			DELETEO(binding);
			
			m_binding_list.removeAt(i);

			SLOG_NET("net-man", "binding closed and destroyed");

			return;
		}
	}

	SLOG_NET("net-man", "binding not found");
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_manager_t::send_message_async(const sip_message_t& message)
{
	sip_message_t* copy = NEW sip_message_t(message);

	SLOG_NET("net-man", "sending message async %p...", copy);

	rtl::async_call_manager_t::callAsync(async_handle_call, nullptr, ASYNC_SEND_MESSAGE, 0, copy);
}
//--------------------------------------
//
//--------------------------------------
int sip_transport_manager_t::send_message(sip_message_t& message)
{
	int result = 0;

	SLOG_NET("net-man", "sending message %p...", &message);

	// capture changes route!
	sip_transport_route_t route = message.getRoute();

	sip_transport_t* transport = capture_transport(route);

	if (transport != nullptr)
	{
		if (transport->connect())
		{
			if (message.is_request() && !message.get_over_nat_flag())
				update_message(message, transport->getRoute());

			result = transport->send_message(message);
		}
		else
		{
			SLOG_WARN("net-man", "send_message() failed : transport connect return error");
		}

		transport->release_("sip_transport_manager_t::send_message");

		SLOG_NET("net-man", "message sent %d bytes", result > 0 ? result : 0);
	}
	else
	{
		SLOG_WARN("net-man", "send_message() failed : no suitable transport found");
	}

	return result;
}
//--------------------------------------
// ������ ����������
//--------------------------------------
bool sip_transport_manager_t::open_route(sip_transport_route_t& route, const char* user)
{
	char tmp[64];
	SLOG_NET("net-man", "open route %s...%s", route_to_string(tmp, 64, route), user);

	sip_transport_t* transport =  capture_transport(route);

	if (transport != nullptr)
	{
		route.if_address = transport->getRoute().if_address;
		route.if_port = transport->getRoute().if_port;

		SLOG_NET("net-man", "route opened %s...", route_to_string(tmp, 64, route));
		return true;
	}

	SLOG_NET("net-man", "route not accessible.");
	return false;
}
//--------------------------------------
// ��������� ����������� ������ � ��������� ������� ��������� (keep-alive)
//--------------------------------------
bool sip_transport_manager_t::set_route_keep_alive_timer(sip_transport_route_t& route, int keep_alive_value)
{
	char tmp[128];
	SLOG_NET("net-man", "set_route_keep_alive -> %s %d", route_to_string(tmp, 128, route), keep_alive_value);

	sip_transport_t* transport = m_pool.find_transport(route);

	if (transport != nullptr)
	{
		SLOG_NET("net-man", "set_route_keep_alive -> found existing");

		transport->set_keep_alive_timer(keep_alive_value * 1000);

		return true;
	}

	// ������� �� ��������!
	SLOG_NET("net-man", "set_route_keep_alive -> no route found!");
	
	return false;
}
//--------------------------------------
// ������������ ����������
//--------------------------------------
void sip_transport_manager_t::release_route(const sip_transport_route_t& route, const char* user)
{
	sip_transport_t* transport = m_pool.find_transport(route);

	char tmp[64];
	SLOG_NET("net-man", "close route %s...%s", route_to_string(tmp, 64, route), user);

	if (transport != nullptr)
	{
		transport->release_("sip_transport_manager_t::release_route");
		if (transport->getRoute().type == sip_transport_udp_e && transport->get_user_count() == 1)
			transport->release_("sip_transport_manager_t::release_route(1)");
		return;
	}

	SLOG_NET_WARN("net-man", "close_route -> not found");
}
//--------------------------------------
// ������� �� ���������� : �� ������ ������ ��� �����
//--------------------------------------
void sip_transport_manager_t::sip_transport_packet_read(sip_transport_t* sender, raw_packet_t* packet)
{
	if (m_discard_subscribes && packet->get_start_line().is_request())
	{
		const char* method = packet->get_start_line().get_method();
		if (std_stricmp(method, "SUBSCRIBE") == 0 || std_stricmp(method, "PUBLISH") == 0 || std_stricmp(method, "NOTIFY") == 0)
		{
			raw_packet_t::release(packet);
			return;
		}
	}

	sender->add_ref();

	rtl::async_call_manager_t::callAsync(async_handle_call, sender, ASYNC_CALL_PACKET_ARRIVAL, 0, packet);
}
//--------------------------------------
// ������� �� ���������� : ����� ���������
//--------------------------------------
void sip_transport_manager_t::sip_transport_packet_sent(sip_transport_t* sender, const sip_message_t& message)
{
	if (m_event_handler != nullptr)
	{
		m_event_handler->sip_transport_man_packet_sent(message);
	}
}
//--------------------------------------
// ������� �� ���������� : ��������� ������ ������� ������
//--------------------------------------
void sip_transport_manager_t::sip_transport_parser_error(sip_transport_t* sender)
{
	SLOG_NET("net-man", "transport %s parser error -- close connection", sender->get_id());

	sender->add_ref();

	rtl::async_call_manager_t::callAsync(async_handle_call, sender, ASYNC_CALL_PARSER_ERROR, 0, nullptr);
}
//--------------------------------------
// ������� �� ���������� : ��������� ������ ��������
//--------------------------------------
void sip_transport_manager_t::sip_transport_send_error(sip_transport_t* sender, const sip_message_t& message, int net_error)
{
	SLOG_NET("net-man", "Raise send error event %p", &message);

	sender->add_ref();

	rtl::async_call_manager_t::callAsync(async_handle_call, sender, ASYNC_CALL_TRANSPORT_SEND_ERROR, net_error, NEW sip_message_t(message));
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_manager_t::sip_transport_recv_error(sip_transport_t* sender, int net_error)
{
	long ref = sender->add_ref();

	SLOG_NET("net-man", "!!!sip_transport_manager_t::sip_transport_recv_error red:%d", ref);

	rtl::async_call_manager_t::callAsync(async_handle_call, sender, ASYNC_CALL_TRANSPORT_RECV_ERROR, net_error, nullptr);
}
//--------------------------------------
// ������� �� ��������� : ���������� ���������� (TCP)
//--------------------------------------
void sip_transport_manager_t::sip_transport_disconnected(sip_transport_t* sender)
{
	sender->add_ref();

	SLOG_NET("net-man", "... raise transport_disconnected event %s(%p)", sender->get_id(), sender);

	rtl::async_call_manager_t::callAsync(async_handle_call, sender, ASYNC_CALL_DISCONNECTED, 0, nullptr);
}
//--------------------------------------
// ������� �� ��������� : ����� ����������
//--------------------------------------
void sip_transport_manager_t::sip_listener_new_connection(sip_transport_listener_t* sender, sip_transport_t* new_transport)
{
	SLOG_NET("net-man", "accept New connection for %s address:%s:%u...", TransportType_toString(sender->getType()),
		inet_ntoa(new_transport->getRoute().remoteAddress), new_transport->getRoute().remotePort);
	
	m_pool.add_transport(new_transport);
	
	new_transport->capture_("sip_transport_manager_t::sip_listener_new_connection");
	
	start_reading(new_transport);

	SLOG_NET("net-man", "New connection accepted...");
}
//--------------------------------------
// ������� �� ���������� : ������ ���������
//--------------------------------------
void sip_transport_manager_t::sip_listener_transport_failure(sip_transport_listener_t* sender, int net_error, const sip_transport_route_t& route)
{
	sip_transport_route_t* fault_route = NEW sip_transport_route_t;
	*fault_route = route;

	rtl::async_call_manager_t::callAsync(async_handle_call, nullptr, ASYNC_CALL_TRANSPORT_RECV_ERROR, net_error, fault_route);
}
//--------------------------------------
// ������� �� ���������� : ����� ���������
//--------------------------------------
void sip_transport_manager_t::sip_listener_transport_started(sip_transport_listener_t* sender)
{
	sip_transport_point_t point = { sender->getType(), sender->get_interface_address(), sender->get_interface_port() };
	if (m_event_handler != nullptr)
	{
		m_event_handler->sip_transport_man_new_interface(point);
	}
}
//--------------------------------------
// ������� �� ���������� : ��������� ����� �����������
//--------------------------------------
bool sip_transport_manager_t::sip_listener_is_address_banned(sip_transport_listener_t* sender, in_addr remoteAddress)
{
	return (m_event_handler != nullptr) ? m_event_handler->sip_transport_man_is_address_banned(remoteAddress) : false;
}
//--------------------------------------
// ������� �� ���������� : ��������� ������ �����������
//--------------------------------------
bool sip_transport_manager_t::sip_listener_is_useragent_banned(sip_transport_listener_t* sender, const char* useragent)
{
	return (m_event_handler != nullptr) ? m_event_handler->sip_transport_man_is_useragent_banned(useragent) : false;
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_manager_t::async_call(async_call_target_t* caller, uint16_t command, uintptr_t int_param, void* ptr_param)
{
	rtl::async_call_manager_t::callAsync(caller, command, int_param, ptr_param);
}
//--------------------------------------
//
//--------------------------------------
sip_transport_binding_t* sip_transport_manager_t::bind_listener(const sip_transport_point_t& point)
{
	// ���� ��������� � �������
	// ���� �������, �� ������ �� ������
	// ���� ��� �� �������

	sip_transport_binding_t* binding = nullptr;

	SLOG_NET("net-man", "bind listener p:%s if:%s:%u...", TransportType_toString(point.type), inet_ntoa(point.address), point.port);

	if ((binding = find_binding(point)) == nullptr)
	{
		//in_addr dummy = { 0 };
		binding = NEW sip_transport_binding_t(point.type);
		if (!binding->create(point.address, point.port))
		{
			SLOG_WARN("net-man", "binding listener to interface %s:%u failed", inet_ntoa(point.address), point.port);
			DELETEO(binding);
			return nullptr;
		}

		rtl::MutexLock lock(m_binding_list_sync);
		m_binding_list.add(binding);
		
		SLOG_NET("net-man", "new bind %s created", binding->get_id());
	}

	return binding;
}
//--------------------------------------
//
//--------------------------------------
sip_transport_binding_t* sip_transport_manager_t::find_binding(const sip_transport_point_t& point)
{
	SLOG_NET("net-man", "search binded listener on %s:%u %s", inet_ntoa(point.address), point.port, TransportType_toString(point.type));

	rtl::MutexLock lock(m_binding_list_sync);

	for (int i = 0; i < m_binding_list.getCount(); i++)
	{
		sip_transport_binding_t* binding = m_binding_list[i];
		
		// if == ANY || if == if_address
		in_addr binded_iface = binding->get_listen_address();

		if (compare_listener_type(binding->getType(), point.type) &&
			binding->get_listen_port() == point.port &&
			(binded_iface.s_addr == INADDR_ANY || binded_iface.s_addr == point.address.s_addr))
		{
			SLOG_NET("net-man", "binded listener found %s", binding->get_id());
			return binding;
		}
	}

	SLOG_NET("net-man", "binded listener not found");

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
sip_transport_binding_t* sip_transport_manager_t::get_binding(const sip_transport_point_t& point)
{
	SLOG_NET("net-man", "get binded listener on %s:%u %s", inet_ntoa(point.address), point.port, TransportType_toString(point.type));

	rtl::MutexLock lock(m_binding_list_sync);

	for (int i = 0; i < m_binding_list.getCount(); i++)
	{
		sip_transport_binding_t* binding = m_binding_list[i];
		
		// if == ANY || if == if_address
		in_addr binded_iface = binding->get_listen_address();

		if (compare_listener_type(binding->getType(), point.type) &&
			(point.port == 0 || binding->get_listen_port() == point.port) &&
			(binded_iface.s_addr == INADDR_ANY || binded_iface.s_addr == point.address.s_addr))
		{
			SLOG_NET("net-man", "binded listener found %s", binding->get_id());
			return binding;
		}
	}

	SLOG_NET("net-man", "binded listener not found -- create New");
	
	// ���� �� ����� �� �������
	return bind_listener(point);
}
//--------------------------------------
// ����� ���������� �� ����������
//--------------------------------------
sip_transport_t* sip_transport_manager_t::capture_transport(sip_transport_route_t& route)
{
	char tmp[64];
	SLOG_NET("net-man", "capture route %s...", route_to_string(tmp, 64, route));

	sip_transport_t* transport = m_pool.find_transport(route);

	if (transport != nullptr)
	{
		if (!transport->is_valid()/* && transport->getRoute().type == sip_transport_udp_e*/)
		{
			sip_transport_route_t route = transport->getRoute();
			m_pool.remove_transport(transport);
			//transport = nullptr;
			char buf[128];
			SLOG_NET("net-man", "route captured....remove invalid transport route %s", route_to_string(buf, 128, route));
		}
		else
		{
			// �������� ����� ��������� ���������
			if (transport->getRoute().if_address.s_addr != route.if_address.s_addr)
			{
				//...
				//transport->reconnect(route);
			}

			transport->capture_("sip_transport_manager_t::capture_transport");

			SLOG_NET("net-man", "route captured.");

			return transport;
		}
	}

	return create_transport(route);
}
//--------------------------------------
// �������� ������ ���������� �� ����������
//--------------------------------------
sip_transport_t* sip_transport_manager_t::create_transport(sip_transport_route_t& route)
{
	char tmp[64];
	SLOG_NET("net-man", "create transport %s...", route_to_string(tmp, 64, route));

	sip_transport_point_t iface = { route.type, route.if_address, route.if_port };
	sip_transport_t* transport = nullptr;

	if (route.type == sip_transport_udp_e)
	{
		// ����� ����� ���������� ��������� � ���� ���� �� ������������
		if (route.if_address.s_addr == INADDR_ANY)
		{
			// 0 - ����� ��������� -> ����� ����� ����� best_interface()
			if (!net_t::get_best_interface(route.remoteAddress, route.if_address))
				return nullptr;
		}
		
		sip_transport_binding_t* binding = get_binding(iface);

		if (binding != nullptr)
		{
			route.if_port = binding->get_listen_port();
			transport = binding->create_transport(route);
		}
	}
	else
	{
		sip_transport_point_t endpoint = { route.type, route.remoteAddress, route.remotePort };
		transport = NEW sip_transport_tcp_t(iface, endpoint);
	}

	if (transport != nullptr)
	{
		m_pool.add_transport(transport);

		transport->capture_("sip_transport_manager_t::create_transport");

		SLOG_NET("net-man", "transport created...");
	}
	else
	{
		SLOG_NET_WARN("net-man", "create transport failed");
	}

	return transport;
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_manager_t::async_handle_call(void* userdata, uint16_t command, uintptr_t int_param, void* ptr_param)
{
	//SLOG_NET("net-man", "async function #%d call ...", command);

	switch (command)
	{
	case ASYNC_CALL_PACKET_ARRIVAL:
		sip_async_call_packet_arrival((sip_transport_t*)userdata, (raw_packet_t*)ptr_param);
		break;
	case ASYNC_CALL_PARSER_ERROR:
		sip_async_call_parser_error((sip_transport_t*)userdata);
		break;
	case ASYNC_CALL_DISCONNECTED:
		sip_async_call_transport_disconnected((sip_transport_t*)userdata);
		break;
	case ASYNC_CALL_TRANSPORT_SEND_ERROR:
		sip_async_call_transport_send_error((sip_transport_t*)userdata, (sip_message_t*)ptr_param, (int)int_param);
		break;
	case ASYNC_CALL_TRANSPORT_RECV_ERROR:
		sip_async_call_transport_recv_error((sip_transport_t*)userdata, (int)int_param, (sip_transport_route_t*)ptr_param);
		break;
	case ASYNC_CALL_CHECK_LIFE_TIMES:
		sip_async_call_check_lifetimes();
		break;
	case ASYNC_SEND_MESSAGE:
		{
			sip_message_t* message = (sip_message_t*)ptr_param;
			send_message(*message);
			DELETEO(message);
		}
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_manager_t::sip_async_call_packet_arrival(sip_transport_t* sender, raw_packet_t* packet)
{
	SLOG_EVENT("net-man", "sip_async_call_packet_arrival...");

	sip_message_t* message = NEW sip_message_t(*packet);
	bool filtered = false;

	sip_transport_route_t route = packet->getRoute();

	if (packet->get_start_line().is_request() && m_event_handler != nullptr)
	{
		filtered = !m_event_handler->sip_transport_man_check_account(route.remoteAddress, route.remotePort, *message);
	}

	if (rtl::Logger::check_trace(TRF_PROTO))
	{
		uint8_t* a = (uint8_t*)&route.remoteAddress.s_addr;
		uint8_t* b = (uint8_t*)&route.if_address.s_addr;
		const int buffer_size = 16 * 1024;
		char buffer[buffer_size + 1];
		memset(buffer, 0, buffer_size + 1);
		int b_len = packet->to_string(buffer, buffer_size);
		buffer[b_len] = 0;

		if (!filtered)
		{
			TransLog.log("RECV", "%s %d Bytes FROM %d.%d.%d.%d:%u IFACE %d.%d.%d.%d:%u\r\n%s",
				TransportType_toString(route.type), b_len,
				a[0], a[1], a[2], a[3], route.remotePort, b[0], b[1], b[2], b[3], route.if_port, buffer);
		}
		else if (rtl::Logger::check_trace(TRF_BANNED))
		{
			TransLog.log("RECV", "%s %d Bytes FROM %d.%d.%d.%d:%u IFACE %d.%d.%d.%d:%u ---- FILTERED : WRONG USER OR DOMAIN!\r\n",
				TransportType_toString(route.type), b_len, a[0], a[1], a[2], a[3], route.remotePort, b[0], b[1], b[2], b[3], route.if_port);
		}
	}

	if (!filtered && m_event_handler != nullptr)
	{

		m_event_handler->sip_transport_man_packet_arrival(message);
	}
	else
	{
		DELETEO(message);
	}

	raw_packet_t::release(packet);

	sender->release_ref();
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_manager_t::sip_async_call_parser_error(sip_transport_t* transport)
{
	transport->disconnect();

	transport->release_ref();
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_manager_t::sip_async_call_transport_disconnected(sip_transport_t* transport)
{
	sip_transport_route_t route = transport->getRoute();
	char buf[256];
	SLOG_NET("net-man", "... transport_disconnected EVENT (%p) route %s", transport, route_to_string(buf, 256, route));

	if (m_event_handler != nullptr)
	{
		m_event_handler->sip_transport_man_disconnected(transport->getRoute());
	}
	// ��������� ��� �� ������� ��� ����������
	transport->disconnect();

	transport->release_ref();
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_manager_t::sip_async_call_transport_send_error(sip_transport_t* transport, sip_message_t* message, int net_error)
{
	if (m_event_handler != nullptr)
	{
		SLOG_NET("net-man", "Handle send error event %p", message);

		m_event_handler->sip_transport_man_send_error(*message, net_error);
	}

	if (message != nullptr)
		DELETEO(message);

	// ��������� ��� �� ������� ��� ����������
	transport->disconnect();

	transport->release_ref();
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_manager_t::sip_async_call_transport_recv_error(sip_transport_t* transport, int net_error, sip_transport_route_t* route)
{
	//sip_transport_route_t* f_route = nullptr;

	if (transport != nullptr)
	{
		sip_transport_route_t route = transport->getRoute();
		if (m_event_handler != nullptr)
		{
			m_event_handler->sip_transport_man_recv_error(route, net_error);
		}
		// ��������� ��� �� ������� ��� ����������
		transport->disconnect();
		transport->release_ref();
	}
	else if (route != nullptr)
	{
		if (m_event_handler != nullptr)
		{
			m_event_handler->sip_transport_man_recv_error(*route, net_error);
		}

		DELETEO(route);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_manager_t::sip_async_call_check_lifetimes()
{
	// �������� ����� � ��������� �����������
	update_interface_table();

	// ������ �� ����������� � ������������ ��� ��� ��� ����� �� ��������,
	// � ����� ������������ ������ ������

	if (m_binding_list.getCount() > 0)
	{
		rtl::MutexLock lock(m_binding_list_sync);

		for (int i = 0; i < m_binding_list.getCount(); i++)
		{
			sip_transport_binding_t* binding = m_binding_list[i];
			binding->check_transport_life_times();
		}
	}

	rtl::ArrayT<sip_transport_reader_t*> removed_list;

	if (m_reader_list.getCount() > 0)
	{
		rtl::MutexLock lock(m_readerSync);
		for (int i = m_reader_list.getCount() - 1; i >= 0; i--)
		{
			sip_transport_reader_t* reader = m_reader_list[i];

			if (reader->getCount() == 0)
			{
				removed_list.add(reader);
				m_reader_list.removeAt(i);
			}
		}
	}

	for (int i = 0; i < removed_list.getCount(); i++)
	{
		sip_transport_reader_t* reader = removed_list[i];

		if (reader->getCount() == 0)
		{
			reader->stop();
			DELETEO(reader);
		}
	}

	removed_list.clear();

	m_pool.check_transport_timeouts();
}
//--------------------------------------
// ��������� ITimerEvents
//--------------------------------------
void sip_transport_manager_t::timer_elapsed_event(rtl::Timer* sender, uint32_t elapsed, void* user_data, int tid)
{
	rtl::async_call_manager_t::callAsync(async_handle_call, nullptr, ASYNC_CALL_CHECK_LIFE_TIMES, 0, nullptr);
}
//--------------------------------------
//
//--------------------------------------
bool sip_transport_manager_t::start_reading(sip_transport_object_t* object)
{
	sip_transport_reader_t* reader = nullptr;

	rtl::MutexLock lock(m_readerSync);

	for (int i = 0; i < m_reader_list.getCount(); i++)
	{
		if (m_reader_list[i]->get_free_slots() > 0)
		{
			reader = m_reader_list[i];
			break;
		}
	}

	if (reader == nullptr)
	{
		reader = NEW sip_transport_reader_t;
		m_reader_list.add(reader);
	}

	return reader->add_object(object);
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_manager_t::destroy_bindings()
{
	rtl::MutexLock lock(m_binding_list_sync);

	if (m_binding_list.getCount() > 0)
	{
		SLOG_NET("net-man", "destroying %d bindings", m_binding_list.getCount());

		for (int i = 0; i < m_binding_list.getCount(); i++)
		{
			sip_transport_binding_t* binding = m_binding_list[i];
			if (binding != nullptr)
			{
				SLOG_NET("net-man", "bind %s destroyed", binding->get_id());
				binding->destroy();
				DELETEO(binding);
			}
		}

		m_binding_list.clear();
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_manager_t::stop_readers()
{
	rtl::ArrayT<sip_transport_reader_t*> removed_list;

	{
		rtl::MutexLock lock(m_readerSync);

		if (m_reader_list.getCount() > 0)
		{
			SLOG_NET("net-man", "removing %d readers", m_reader_list.getCount());
		
			for (int i = 0; i < m_reader_list.getCount(); i++)
			{
				sip_transport_reader_t* reader = m_reader_list[i];
				if (reader != nullptr)
				{
					removed_list.add(reader);
				}
			}

			m_reader_list.clear();
		}
	}

	SLOG_NET("net-man", "stopping %d readers", removed_list.getCount());

	for (int i = removed_list.getCount()-1; i >= 0; i--)
	{
		sip_transport_reader_t* reader = removed_list[i];

		SLOG_NET("net-man", "stopping reader %p", reader);

		if (reader != nullptr)
		{
			reader->stop();
			DELETEO(reader);
		}
	}

	removed_list.clear();

	SLOG_NET("net-man", "readers stopped and destroyed");
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_manager_t::update_interface_table()
{
	rtl::ArrayT<in_addr> iface_table;
	
	if (!net_t::get_interface_table(iface_table))
	{
		return;
	}

	rtl::MutexLock lock(m_binding_list_sync);

	for (int i = 0; i < m_binding_list.getCount(); i++)
	{
		m_binding_list[i]->update_interface_table(iface_table);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_manager_t::destroy_sip_transports(sip_transport_point_t& iface)
{
	m_pool.disable_transports(iface);
}
//--------------------------------------
//
//--------------------------------------
//void sip_transport_manager_t::net_transport_network_failed()
//{
//	synchro_t<rtl::Mutex> lock(m_binding_list_sync);
//
//	for (int i = 0; i < m_binding_list.getCount(); i++)
//	{
//		sip_transport_binding_t* binding = m_binding_list[i];
//		binding->event_network_fault();
//	}
//}
////--------------------------------------
////
////--------------------------------------
//void sip_transport_manager_t::net_transport_addresses_added(const rtl::ArrayT<in_addr>& list)
//{
//	synchro_t<rtl::Mutex> lock(m_binding_list_sync);
//
//	for (int i = 0; i < m_binding_list.getCount(); i++)
//	{
//		m_binding_list[i]->event_interface_added(list);
//	}
//}
////--------------------------------------
////
////--------------------------------------
//void sip_transport_manager_t::net_transport_addresses_removed(const rtl::ArrayT<in_addr>& list)
//{
//	synchro_t<rtl::Mutex> lock(m_binding_list_sync);
//
//	for (int i = 0; i < m_binding_list.getCount(); i++)
//	{
//		m_binding_list[i]->event_interface_removed(list);
//	}
//}
//--------------------------------------
// ��������� ���� VIA � CONTACT
//--------------------------------------
static void update_message(sip_message_t& message, const sip_transport_route_t& route)
{
	//bool update_via = false;
	sip_value_via_t* v = message.get_Via_value();

	if (v == nullptr)
	{
		return;
	}

	const rtl::String& via_addr = v->get_address();
	in_addr v_addr = net_parse(via_addr);
	uint16_t v_port = v->get_port();

	if (!net_t::ip_is_valid(v_addr))
	{
		v->set_address(inet_ntoa(route.if_address));
	}

	if (v_port == 0)
	{
		v->set_port(route.if_port);

		char tmp[128];
		SLOG_NET("net-man", "Update message via %s:%u to route %s", (const char*)via_addr, v_port, route_to_string(tmp, 128, route));
	}

	sip_value_contact_t* c = message.get_Contact_value();
	
	if (!c)
		return;

	sip_uri_t& s_uri = c->get_uri();
	
	in_addr c_addr;
	c_addr.s_addr = inet_addr(s_uri.get_host());
	uint16_t c_port = (uint16_t)strtoul(s_uri.get_port(), nullptr, 10);

	if (!net_t::ip_is_valid(c_addr))
	{
		s_uri.set_host(inet_ntoa(route.if_address));
	}

	if (c_port == 0)
	{
		char tmp[32];
		std_snprintf(tmp, 32, "%u", route.if_port);
		s_uri.set_port(tmp);
	}

	if (message.get_request_method_name() *= "CANCEL")
	{
		rtl::String ptr = s_uri.to_string();
		SLOG_CALL("net-man", "Update message is CANCEL Contact URI : %s", (const char*)ptr);
	}
}
//--------------------------------------
//
//--------------------------------------
//void sip_transport_manager_t::net_transport_network_failed()
//{
//	synchro_t<rtl::Mutex> lock(m_binding_list_sync);
//
//	for (int i = 0; i < m_binding_list.getCount(); i++)
//	{
//		sip_transport_binding_t* binding = m_binding_list[i];
//		binding->event_network_fault();
//	}
//}
////--------------------------------------
////
////--------------------------------------
//void sip_transport_manager_t::net_transport_addresses_added(const rtl::ArrayT<in_addr>& list)
//{
//	synchro_t<rtl::Mutex> lock(m_binding_list_sync);
//
//	for (int i = 0; i < m_binding_list.getCount(); i++)
//	{
//		m_binding_list[i]->event_interface_added(list);
//	}
//}
////--------------------------------------
////
////--------------------------------------
//void sip_transport_manager_t::net_transport_addresses_removed(const rtl::ArrayT<in_addr>& list)
//{
//	synchro_t<rtl::Mutex> lock(m_binding_list_sync);
//
//	for (int i = 0; i < m_binding_list.getCount(); i++)
//	{
//		m_binding_list[i]->event_interface_removed(list);
//	}
//}
//--------------------------------------
