﻿/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_stack.h"
#include "sip_transport_manager.h"
#include "sip_registrar_account.h"
#include "sip_subscribe_session.h"
#include "sip_parser_tools.h"
//--------------------------------------
//
//--------------------------------------
sip_subscribe_session_t::sip_subscribe_session_t(sip_registrar_account_t& account) :
	m_account(account),
	m_call_id(nullptr),
	m_from_tag(nullptr),
	m_to_tag(nullptr),
	m_cseq_in(0),
	m_cseq_out(0),
	m_over_nat(false),
	m_last_time(0)
{
	memset(&m_route, 0, sizeof(m_route));

	m_via_type = sip_transport_udp_e;
	m_via_address.s_addr = INADDR_ANY;
	m_via_port = 0;

	m_contact_type = sip_transport_udp_e;
	m_contact_address.s_addr = INADDR_ANY;
	m_contact_port = 0;
}
//--------------------------------------
//
//--------------------------------------
sip_subscribe_session_t::~sip_subscribe_session_t()
{
	cleanup();
}
//--------------------------------------
//
//--------------------------------------
bool sip_subscribe_session_t::check_transport_disconnected(const sip_transport_route_t& route)
{
	return route.type == m_route.type && m_route.remoteAddress.s_addr == route.remoteAddress.s_addr && m_route.remotePort == route.remotePort;
}
//--------------------------------------
//
//--------------------------------------
bool sip_subscribe_session_t::process_request(const sip_message_t& request, bool over_nat)
{
	//CSeq cseq;

	rtl::String call_id = request.CallId();
	rtl::String from_tag = request.From_Tag();
	rtl::String to_tag = request.To_Tag();
	rtl::String method = request.CSeq_Method();
	uint32_t seq_no = request.CSeq_Number();

	// проверки
	if (call_id.isEmpty())
	{
		SLOG_WARN("sub-sess", "%s : failed : request has no Call-Id header", m_call_id);
		return false;
	}

	if (seq_no == 0 || method.isEmpty())
	{
		SLOG_WARN("sub-sess", "%s : failed : request has no CSeq header or 0 value", m_call_id);
		return false;
	}

	if (from_tag.isEmpty())
	{
		SLOG_WARN("sub-sess", "%s : failed : request has no From tag parameter", m_call_id);
		return false;
	}

	m_from_uri = request.From_URI();
	m_to_uri = request.To_URI();

	m_over_nat = over_nat;

	// call-id в первую очередь
	if (m_call_id == nullptr)
	{
		m_call_id = rtl::strdup((const char*)call_id);
	}

	if (m_from_tag == nullptr)
	{
		m_from_tag = rtl::strdup((const char*)from_tag);
	}

	SLOG_CALL("sub-sess", "%s : processing request CSeq:%d %s Call-Id:%s...", (const char*)call_id, seq_no, (const char*)method, (const char*)call_id);

	if (seq_no < m_cseq_in)
	{
		SLOG_CALL("sub-sess", "%s : packet with CSeq:%d older than current CSeq:%d", m_call_id, seq_no, m_cseq_in);
		return true;
	}

	m_cseq_in = seq_no; // обновим cseq;

	m_route = request.getRoute();

	const sip_value_via_t* via = request.get_Via_value();
	if (!via)
	{
		SLOG_WARN("sub-sess", "%s : failed : request has no Via headers", m_call_id);
		return false;
	}

	m_via_address = net_t::get_host_by_name(via->get_address());
	m_via_port = via->get_port();
	m_via_type = via->get_protocol();

	// обязательная часть прошла!
	// если есть паспорт то требуется авторизация
	// иначе сразу 200 ok

	m_last_time = time(nullptr);

	// в любом случае захватим транспорт

	const sip_value_number_t* expires = request.get_Expires_value(); // Expires
	uint32_t max_expires = m_account.get_max_expires();

	if (expires)
	{
		m_expires_value = expires->get_number();

		if (max_expires != 0 && m_expires_value > max_expires)
		{
			m_expires_value = max_expires;
		}
	}

	//SLOG_CALL("sub-sess", "%s : set contacts...", m_call_id);

	// обработка списка контактов!
	const sip_value_contact_t* contact;

	int contact_count = request.get_Contact()->get_value_count();

	//bool leave_cycle = false;

	for (int i = 0; i < contact_count; i++)
	{
		contact = request.get_Contact_value(i);
		if (contact)
		{
			//SLOG_CALL("sub-sess", "%s : contact[%d] %s...", m_call_id, 0, (const char*)contact.AsString());

			const sip_uri_t& contact_uri = contact->get_uri();
			if (contact_uri.get_scheme() *= "sip")
			{
				uri_to_contact(*contact);
				break;
			}
		}
	}

	SLOG_CALL("sub-sess", "%s : request done : expires %u sec", m_call_id, m_expires_value);

	// вернется ответ 200 Ok
	return true;
}
//--------------------------------------
//
//--------------------------------------
void sip_subscribe_session_t::update_response(sip_message_t& response)
{
	if (m_to_tag == nullptr)
	{
		m_to_tag = rtl::strdup(response.To_Tag());
	}

	// проверить Subscribe-State на terminated

	const sip_value_t* ss = response.get_Subscription_State_value();
	if (ss != nullptr && ss->value().indexOf("terminated") != BAD_INDEX)
	{
		m_expires_value = 0;
	}

	response.make_Expires_value()->set_number(m_expires_value);
	
}
//--------------------------------------
// true - alive, false - expired
//--------------------------------------
bool sip_subscribe_session_t::check_expire_timer(time_t current_time)
{
	uint32_t expires = (int32_t)(current_time - m_last_time);

	if (m_expires_value == 0)
	{
		SLOG_CALL("sub-sess", "contact %s:%u set expires(%u) by zero", inet_ntoa(m_contact_address), m_contact_port, m_expires_value);
		return false;
	}

	if (expires < m_expires_value)
	{
		return true;
	}

	uint64_t overtime = expires - m_expires_value;

	if (overtime < 10)
	{
		SLOG_CALL("sub-sess", "contact %s:%u add expires %u overtime", inet_ntoa(m_contact_address), m_contact_port, overtime);
		return true;
	}

	SLOG_CALL("sub-sess", "contact %s:%u set expires by timer", inet_ntoa(m_contact_address), m_contact_port);

	return false;
}
//--------------------------------------
//
//--------------------------------------
static void update_request_from_info(sip_message_t& request, const sip_registrar_subscribe_info_t* info)
{
	rtl::String s;

	if (info->content_disposition[0] != 0)
	{
		//SIPHeader cd("Content-Disposition", info->content_disposition);
		//request.SetContentDisposition(cd);
		s = info->content_disposition;
		request.make_Content_Disposition_value()->assign(s);
	}

	if (info->content_encoding[0] != 0)
	{
		/*ContentEncoding ce(info->content_encoding);
		request.AppendContentEncoding(ce);*/
		s = info->content_encoding;
		request.make_Content_Encoding_value()->assign(s);
	}

	if (info->content_language[0] != 0)
	{
		/*SIPHeader cl("Content-Language", info->content_language);
		request.SetContentLanguage(cl);*/
		s = info->content_language;
		request.make_Content_Language_value()->assign(s);
	}

	if (info->content_type[0] != 0)
	{
		//request.SetContentType(info->content_type);
		s = info->content_type;
		request.make_Content_Type_value()->assign(s);
	}

	if (info->event[0] != 0)
	{
		/*SIP_Event e(info->event);
		request.SetEvent(e);*/
		s = info->event;
		request.make_Event_value()->assign(s);
	}

	if (info->subscription_state[0] != 0)
	{
		//SIPHeader ss("Subscription-State", info->subscription_state);
		//request.SetSubscriptionState(ss);
		s = info->subscription_state;
		request.make_Subscription_State_value()->assign(s);
	}

	if (info->content != nullptr && info->content_length > 0)
	{
		request.set_body(info->content, info->content_length);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_subscribe_session_t::send_notify(const sip_registrar_subscribe_info_t* notify_info)
{
	sip_message_t request;

	/// Create the start line
	sip_uri_t uri;
	uri.set_user(m_account.get_username());
	uri.set_host(inet_ntoa(m_contact_address));
	uri.set_port(rtl::int_to_string(m_contact_port));

	request.set_request_line(sip_NOTIFY_e, uri);

	// Create the from uri
	sip_value_user_t* from = request.make_From_value();
	from->set_uri(m_to_uri);
	from->set_tag(m_to_tag);

	/// Create the to uri
	sip_value_user_t* to = request.make_To_value();
	to->set_uri(m_from_uri);
	to->set_tag(m_from_tag);

	/// create the via
	sip_value_via_t* via = request.make_Via_value();
	via->set_protocol(m_route.type);
	via->set_address(inet_ntoa(m_route.if_address));
	via->set_port(m_route.if_port);
	via->set_branch(sip_utils::GenBranchParameter());
	

	/// only generate a Cseq for nonce-CANCEL and none-ACK requests

	/*
	Requests within a dialog MUST contain strictly monotonically
	increasing and contiguous CSeq sequence numbers (increasing-by-one)
	in each direction (excepting ACK and CANCEL of course, whose numbers
	equal the requests being acknowledged or cancelled).  Therefore, if
	the local sequence number is not empty, the value of the local
	sequence number MUST be incremented by one, and this value MUST be
	placed into the CSeq header field.  If the local sequence number is
	empty, an initial value MUST be chosen using the guidelines of
	Section 8.1.1.5.  The method field in the CSeq header field value
	MUST match the method of the request.
	*/

	/// Create the CSeq
	request.CSeq(++m_cseq_out, "NOTIFY");

	/// set the call-id
	request.CallId(m_call_id);

	/// Set the contact URI
	sip_uri_t curi = m_to_uri;
	// GEORGE : есди нет юзера то берем из фром

	curi.set_host(inet_ntoa(m_route.if_address));
	curi.set_port(rtl::int_to_string(m_route.if_port));

	request.make_Contact_value()->set_uri(curi);

	request.set_body(NULL, 0);

	if (m_route.type != sip_transport_udp_e)
	{
		request.set_route(m_route);
	}
	else
	{
		request.set_route(m_route); //@todo Нужно разобюратся ?????? m_contact_address, m_contact_port, m_contact_type
	}

	update_request_from_info(request, notify_info);

	SLOG_CALL("sub-sess", "%s : send NOTIFY request", m_call_id);

	// проверить Subscribe-State на terminated

	m_account.send_request(request);

	// нужно проверить ответ на ниличие терминатед

	//if (notify_info->content != nullptr && notify_info->content_length > 0)
	//{
	//	rtl::String body((const char*)notify_info->content, notify_info->content_length);

	//	int index = body.indexOf("<state>terminated</state>");
	//	if (index != BAD_INDEX)
	//	{
	//		// нужно выставлять удалить подписку!
	//		m_expires_value = 0;
	//	}
	//}

	if (request.has_Subscription_State())
	{
		sip_value_t* ss = request.get_Subscription_State_value();
		if (ss)
		{
			if (ss->value().indexOf("terminated") != BAD_INDEX)
			{
				m_expires_value = 0;
			}
		}
	}
}
//--------------------------------------
//
//--------------------------------------
bool sip_subscribe_session_t::uri_to_contact(const sip_value_contact_t& contact_uri)
{
	const sip_uri_t& uri = contact_uri.get_uri();

	const rtl::String& address = uri.get_host();
	const rtl::String& port = uri.get_port();
	bool b;
	const rtl::String& transport = uri.get_parameters().get("transport", b);

	// если не указан транспорт то UDP
	if (!transport.isEmpty())
	{
		m_contact_type = parse_transport_type(transport);
	}

	if (!address.isEmpty())
	{
		m_contact_address = net_t::get_host_by_name(address);
	}
	else
	{
		return false;
	}

	if (!port.isEmpty())
	{
		m_contact_port = (uint16_t)strtoul(port, nullptr, 10);
	}
	else
	{
		m_contact_port = 5060;
	}

	// определимся с адресом и портом по принципу:

	// r_contact == invalid (обычно для TCP)
	// contact = r_recv (отправляем запросы на контакт)

	if (m_contact_address.s_addr == INADDR_NONE || m_contact_address.s_addr == INADDR_ANY)
	{
		m_contact_type = m_route.type;
		m_contact_address = m_route.remoteAddress;
		m_contact_port = m_route.remotePort;
	}

	if (m_via_address.s_addr != m_route.remoteAddress.s_addr && m_via_address.s_addr == m_contact_address.s_addr && m_via_port == m_contact_port)
	{
		m_contact_type = m_route.type;
		m_contact_address = m_route.remoteAddress;
		m_contact_port = m_route.remotePort;
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
void sip_subscribe_session_t::cleanup()
{
	if (m_call_id != nullptr)
	{
		FREE(m_call_id);
		m_call_id = nullptr;
	}

	if (m_from_tag != nullptr)
	{
		FREE(m_from_tag);
		m_from_tag = nullptr;
	}

	if (m_to_tag != nullptr)
	{
		FREE(m_to_tag);
		m_to_tag = nullptr;
	}

	memset(&m_route, 0, sizeof(m_route));
}
//--------------------------------------
