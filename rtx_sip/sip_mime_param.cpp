/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_mime_param.h"
//--------------------------------------
//
//--------------------------------------
const mime_param_t* mime_param_list_t::find(const char* name) const
{
	for (int i = 0; i < m_array.getCount(); i++)
	{
		mime_param_t* param = m_array[i];
		if (rtl::String::compare(param->getName(), name, true) == 0)
			return param;
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
mime_param_t* mime_param_list_t::find(const char* name)
{
	for (int i = 0; i < m_array.getCount(); i++)
	{
		mime_param_t* param = m_array[i];
		if (rtl::String::compare(param->getName(), name, true) == 0)
			return param;
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
mime_param_list_t& mime_param_list_t::assign(const mime_param_list_t& paramsArray)
{
	mime_param_t* param;

	for (int i = 0; i < m_array.getCount(); i++)
	{
		param = m_array[i];
		DELETEO(param);
	}

	m_array.clear();

	for (int i = 0; i < paramsArray.m_array.getCount(); i++)
	{
		param = paramsArray.m_array[i];
		if (param != nullptr)
		{
			m_array.add(NEW mime_param_t(*param));
		}
	}

	return *this;
}
//--------------------------------------
//
//--------------------------------------
const rtl::String& mime_param_list_t::get(const char* name, bool& result) const
{
	const mime_param_t* param = find(name);
	
	result = param != nullptr;

	return result ? param->getValue() : rtl::String::empty;
}
//--------------------------------------
//
//--------------------------------------
const rtl::String& mime_param_list_t::get(const char* name) const
{
	const mime_param_t* param = find(name);
	return param != nullptr ? param->getValue() : rtl::String::empty;
}
//--------------------------------------
//
//--------------------------------------
bool mime_param_list_t::get(const char* name, rtl::String& result) const
{
	const mime_param_t* param = find(name);

	if (param != nullptr)
	{
		result = param->getValue();
		return true;
	}
	
	result = rtl::String::empty;

	return false;
}
//--------------------------------------
//
//--------------------------------------
void mime_param_list_t::set(const char* name, const rtl::String& value)
{
	mime_param_t* param = find(name);

	if (param != nullptr)
	{
		param->setValue(value);
	}
	else
	{
		m_array.add(NEW mime_param_t(name, value));
	}
}
//--------------------------------------
//
//--------------------------------------
void mime_param_list_t::remove(const char* name)
{
	for (int i = 0; i < m_array.getCount(); i++)
	{
		mime_param_t* param = m_array[i];
		if (rtl::String::compare(param->getName(), name, true) == 0)
		{
			m_array.removeAt(i);
			DELETEO(param);
			break;
		}
	}
}
//--------------------------------------
//
//--------------------------------------
void mime_param_list_t::removeAt(int index)
{
	if (index >= 0 && index < m_array.getCount())
	{
		mime_param_t* param = m_array.getAt(index);
		m_array.removeAt(index);

		if (param != nullptr)
		{
			DELETEO(param);
		}
	}
}
//--------------------------------------
//
//--------------------------------------
void mime_param_list_t::cleanup()
{
	for (int i = 0; i < m_array.getCount(); i++)
	{
		mime_param_t* param = m_array[i];
		DELETEO(param);
	}

	m_array.clear();
}
//--------------------------------------
//
//--------------------------------------
int mime_param_list_t::read_from(const char* stream, mime_param_list_t& list)
{
	return 0;
}
//--------------------------------------
