﻿/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_call_session_manager.h"
#include "sip_call_session.h"

#pragma region Invite Server Transaction 200 OK retransmission
//--------------------------------------
//
//--------------------------------------
retransmit_record_t::retransmit_record_t(const sip_message_t& msg, const rtl::String& call_id, sip_call_session_manager_t& manager) : m_manager(manager)
{
	m_200_ok = msg;
	m_200_ok.set_retransmission();

	m_is_expired = false;
	m_start_time = rtl::DateTime::getTicks(); // ms
	m_timer_value = SIP_INTERVAL_T1;   // ms
	m_retransmit_count = 0;
	m_call_id = call_id;
}
//--------------------------------------
//
//--------------------------------------
retransmit_record_t::~retransmit_record_t()
{
}
//--------------------------------------
//
//--------------------------------------
bool retransmit_record_t::retransmit()
{
	if (m_is_expired)
		return false;

	bool result = true;

	try
	{
		uint32_t interval = rtl::DateTime::getTicks() - m_start_time;

		if (interval >= m_timer_value)
		{
			++m_retransmit_count;

			if (m_retransmit_count < 7)
			{
				SLOG_CALL("CALLM", "RETRANSMIT(%d):  200 Ok - %s", m_retransmit_count, (const char*)m_call_id);
				m_manager.get_user_agent().push_message_to_transport(m_200_ok);
				m_timer_value = m_timer_value * 2;
				if (m_timer_value > SIP_INTERVAL_T2)
					m_timer_value = SIP_INTERVAL_T2;
			}

			///Timer H  64*T1            Section 17.2.1       Wait time for ACK receipt
			/// only retransmit 6 times (500, 1000, 2000, 4000, 4000, 4000, 500) total = 16000 MS
			if (m_retransmit_count == 6)
			{
				m_timer_value = SIP_INTERVAL_T1;  /// wait 500 more ms before we expire post an expire
			}
			else if (m_retransmit_count == 7)
			{
				result = false;

				sip_call_session_t * session = (sip_call_session_t*)m_manager.find_session_by_call_id(m_call_id); 
				SLOG_CALL("CALLM", "RETRANSMIT(%d)  200 Ok - %s : time expired for session %s", m_retransmit_count, (const char*)m_call_id,
					session != NULL ? session->getName() : "(null)");
				if (session != NULL)
				{
					session->OnAckFor200OkExpire(m_200_ok);
				}
			}
		}
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR("CALLM", "RETRANSMIN: failed with exception %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	return result;
}
#pragma endregion

#pragma region Invite Server Transaction 200 OK retransmission List
//--------------------------------------
//
//--------------------------------------
static int ret_compare(const retransmit_record_list_t::SESS_KEY& left, const retransmit_record_list_t::SESS_KEY& right)
{
	return std_stricmp(left.call_id, right.call_id);
}
//--------------------------------------
//
//--------------------------------------
static int ret_key_compare(const ZString& left, const retransmit_record_list_t::SESS_KEY& right)
{
	return std_stricmp(left, right.call_id);
}
//--------------------------------------
//
//--------------------------------------
retransmit_record_list_t::retransmit_record_list_t() : m_pool(ret_compare, ret_key_compare, 512)
{
}
//--------------------------------------
//
//--------------------------------------
retransmit_record_list_t::~retransmit_record_list_t()
{
	for (int i = 0; i < m_pool.getCount(); i++)
	{
		SESS_KEY key = m_pool[i];
		FREE(key.call_id);
		DELETEO(key.record);
	}

	m_pool.clear();
}
//--------------------------------------
//
//--------------------------------------
bool retransmit_record_list_t::add(const char* key, retransmit_record_t* session)
{
	bool res = false;

	if (session != NULL && key != NULL && key[0] != 0)
	{
		SESS_KEY trn_rec = { NULL, session };

		int len = (int)strlen(key);
		trn_rec.call_id = (char*)MALLOC(len+1);
		strcpy(trn_rec.call_id, key);
		res = m_pool.add(trn_rec) != BAD_INDEX;
	}

	return res;
}
//--------------------------------------
//
//--------------------------------------
void retransmit_record_list_t::removeAt(int index)
{
	if (index >= 0 && index < m_pool.getCount())
	{
		SESS_KEY& sess_ptr = m_pool.getAt(index);
		FREE(sess_ptr.call_id);
		sess_ptr.call_id = NULL;
		DELETEO(sess_ptr.record);
		sess_ptr.record = NULL;
		m_pool.removeAt(index);
	}
}
//--------------------------------------
//
//--------------------------------------
retransmit_record_t* retransmit_record_list_t::find(const char* key) const
{
	if (key != NULL && key[0] != 0)
	{
		const SESS_KEY* sess_ptr = m_pool.find(key);
		if (sess_ptr != NULL)
			return sess_ptr->record;
	}

	return NULL;
}
#pragma endregion

#pragma region Retransmission thread
//--------------------------------------
//
//--------------------------------------
retransmit_thread_t::retransmit_thread_t(sip_call_session_manager_t& manager)	: m_manager(manager), m_ret_sync("call-man-ret-thr")
{
	m_exit_flag = false;
	m_event.create(false, false);
	m_thread.start(this);
}
//--------------------------------------
//
//--------------------------------------
retransmit_thread_t::~retransmit_thread_t()
{
	m_exit_flag = true;

	m_event.signal();
	m_thread.stop();

	m_event.close();
}
//--------------------------------------
//
//--------------------------------------
void retransmit_thread_t::stop_retransmission(const char* call_id)
{
	try
	{
		rtl::MutexWatchLock lock(m_ret_sync, __FUNCTION__);
		
		retransmit_record_t* ret = m_retransmit_list.find(call_id);
		if (ret != NULL)
		{
			SLOG_CALL("CALLM", "Retransmitt thread : stop 200 OK retransmission for %s", call_id);
			ret->set_expired();
		}
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR("CALLM", "Retransmitt thread : expire failed with exception %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}
	
	m_event.signal();
}
//--------------------------------------
//
//--------------------------------------
void retransmit_thread_t::start_retransmission(const char* call_id, const sip_message_t& msg)
{
	retransmit_record_t* retran = NEW retransmit_record_t(msg, call_id, m_manager);

	try
	{
		rtl::MutexWatchLock lock(m_ret_sync, __FUNCTION__);
		SLOG_CALL("CALLM", "Retransmitt thread : start 200 OK retransmission for %s", call_id);
		m_retransmit_list.add(call_id, retran);
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR("CALLM", "Retransmitt thread : start failed with exception %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	// непонятно! зачем будить поток ретрансмиттера? мы же только что отправили 200 ок!
	m_event.signal();
}
//--------------------------------------
//
//--------------------------------------
void retransmit_thread_t::handle_retransmission()
{
	try
	{
		rtl::MutexWatchLock lock(m_ret_sync, __FUNCTION__);

		for (int i = m_retransmit_list.getCount()-1; i >= 0; i--)
		{
			retransmit_record_t* retran = m_retransmit_list.getAt(i);
			/// this is where we retransmit
			if (!retran->retransmit())
			{
				SLOG_CALL("CALLM", "Retransmitt thread : remove retransmission for %s", retran->get_call_id());
				m_retransmit_list.removeAt(i);
			}
		}
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR("CALLM", "Retransmit 200 Ok thread failed with exception %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}
}
//--------------------------------------
//
//--------------------------------------
void retransmit_thread_t::thread_run(rtl::Thread* thread)
{
	do
	{
		try
		{
			m_event.wait(SIP_INTERVAL_T1/2);

			if (thread->get_stopping_flag())
				break;

			handle_retransmission();
		}
		catch (rtl::Exception& ex)
		{
			SLOG_ERROR("CALLM", "sip_call_session_manager_t::retransmit_thread_t catches exception: %s", ex.get_message());
			ex.raise_notify(__FILE__, __FUNCTION__);
		}
	}
	while (!thread->get_stopping_flag());
}
#pragma endregion

//--------------------------------------
//
//--------------------------------------
sip_call_session_manager_t::sip_call_session_manager_t(sip_user_agent_t& ua, int sessionThreadCount) :
	sip_session_manager_t(ua, sessionThreadCount, "Call Manager")
{
	m_log_tag = "CALLM";
	m_200_ok_retransmit_thread = NEW retransmit_thread_t(*this);
}
//--------------------------------------
//
//--------------------------------------
sip_call_session_manager_t::~sip_call_session_manager_t()
{
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_manager_t::stop()
{
	if (m_200_ok_retransmit_thread != NULL)
	{
		DELETEO(m_200_ok_retransmit_thread);
		m_200_ok_retransmit_thread = NULL;
	}

	sip_session_manager_t::stop();
}
//--------------------------------------
//
//--------------------------------------
sip_session_t* sip_call_session_manager_t::create_new_server_session(sip_message_t& request)
{
	sip_call_session_t * callSession = NULL;

	try
	{
		if (request.IsInvite())
		{
			callSession = NEW sip_call_session_t(*this, request);
		}
		else
		{
			SLOG_WARN(m_log_tag, "Server Session got a None-INVITE request");
		}

		
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR(m_log_tag, "Create Server Session failed with exception %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	return callSession;
}
//--------------------------------------
//
//--------------------------------------
sip_session_t* sip_call_session_manager_t::create_new_client_session(const sip_profile_t& profile, const char* callId)
{
	return NEW sip_call_session_t(*this, profile, callId);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_manager_t::process_unknown_transaction(sip_event_unknown_transaction_t& event)
{
	const sip_message_t& msg = event.get_message();
	/// find a session
	
	SLOG_WARN(m_log_tag, "Unknown transaction received CallId: %s!", (const char*)event.get_call_id());
	
	sip_session_t* ref = find_session_by_call_id(msg.CallId());
	if (ref == NULL)
	{
		SLOG_CALL(m_log_tag, "No Session found for Unknown transaction CallId: %s!", (const char*)event.get_call_id());
		return;
	}

	sip_call_session_t * session = (sip_call_session_t *)ref/*.GetObject()*/;

	rtl::String method = msg.CSeq_Method();

	if (event.is_from_transport())
	{
		if (msg.IsAck())
		{
			session->IST_OnReceivedAck(msg);
		}
		else if (msg.Is2xx())
		{
			/// this is a 200 ok retransmision
			if (msg.CSeq_Method() *= "INVITE")
				session->ICT_OnReceived2xx(msg);
		}
	}
	else
	{
		/// its not from transport.  Check if its 200 Ok retransmision
		if (msg.Is2xx())
		{
			if (method *= "INVITE")
			{
				session->IST_On2xxSent(msg);
			}
		}
	}
}
//--------------------------------------
// returning FALSE here will automatically reject the incoming call
//--------------------------------------
call_session_event_handler_t* sip_call_session_manager_t::process_incoming_connection(const sip_message_t& invite, sip_call_session_t& session)
{
	return NULL;
}
//--------------------------------------
//
//--------------------------------------
bool sip_call_session_manager_t::process_incoming_im_message(const sip_message_t& message)
{
	return false;
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_manager_t::process_orphaned_message(const sip_message_t& message, sip_status_t scode)
{
	SLOG_CALL("CALLM", "On Orphaned Message... %s", (const char*)message.GetStartLine());

	if (message.is_request())
	{
		if (message.IsOptions())
		{
			process_received_options(message);
			return;
		}
		else if (message.IsMessage())
		{
			process_received_message(message);
			return;
		}
	}

	sip_session_manager_t::process_orphaned_message(message, scode);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_manager_t::process_received_options(const sip_message_t& options)
{
	sip_message_t ok;
	options.make_response(ok, sip_200_OK);

	sip_header_t* allow = ok.make_Allow();
	allow->add_value("INVITE");
	allow->add_value("ACK");
	allow->add_value("CANCEL");
	allow->add_value("OPTIONS");
	allow->add_value("INFO");
	allow->add_value("MESSAGE");
	allow->add_value("BYE");
	allow->add_value("REFER");
	allow->add_value("NOTIFY");

	// если требуется сдп то найдем акаунт и запросим описание медиа ресурсов

	const sip_header_t* accept = options.get_Accept();

	if (accept != nullptr && accept->get_value_count() > 0)
	{
		for (int j = 0; j < accept->get_value_count(); j++)
		{
			const rtl::String& value = accept->get_value_at(j)->value();
		
			if (value *= "application/sdp")
			{
				options_set_sdp_info(options, ok);
				goto send_label;
			}
		}
	}

send_label:
	//sip_transport_route_t route = options.getRoute();
	send_message_to_transport(ok);
	//sip_transport_manager_t::release_route(route, "call-mgr-orphaned");
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_manager_t::process_received_message(const sip_message_t& message)
{
	sip_message_t response;

	if (process_incoming_im_message(message))
	{
		message.make_response(response, sip_200_OK);
	}
	else
	{
		message.make_response(response, sip_404_NotFound);
	}
	
	//response.set_remote_address(message.get_remote_address(), message.get_remote_port(), message.get_transport_type());
	//sip_transport_route_t route = message.getRoute();
	send_message_to_transport(response);
	//sip_transport_manager_t::release_route(route, "call-mgr-orphaned");
}
//--------------------------------------
//
//--------------------------------------
bool sip_call_session_manager_t::options_set_sdp_info(const sip_message_t& request, sip_message_t& response)
{
	return false;
}
//--------------------------------------
