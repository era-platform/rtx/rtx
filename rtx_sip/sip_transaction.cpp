/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "sip_transaction.h"
#include "sip_transaction_stage.h"
#include "sip_transaction_manager.h"
#include "sip_transport_manager.h"
//--------------------------------------
//
//--------------------------------------
volatile int32_t sip_transaction_t::m_internal_id_gen = 1;
//--------------------------------------
//
//--------------------------------------
const char* get_sip_transaction_type_name(sip_transaction_type_t type)
{
	static const char* names[] = { "ICT", "IST", "NICT", "NIST" };
	return names[type];
}
//--------------------------------------
//
//--------------------------------------
sip_transaction_t::sip_transaction_t(const trans_id& tid, sip_transaction_type_t type, sip_transaction_manager_t& manager) :
	sip_timer_user_t(manager),
	m_manager(manager),
	m_type(type),
	m_sync("trans"),
	m_ref_count(0),
	m_event_queue_lock("trans-que"),
	m_id(tid),
	m_state(0),
	m_terminating(false),
	m_terminated(false),
	m_reliable_transport(false),
	m_send_lock("trans-send")
{
	memset(m_ua_core_name, 0, sizeof m_ua_core_name);
	m_id_str = m_id.AsString();
	int32_t id = std_interlocked_inc(&m_internal_id_gen);
	sprintf(m_internal_id, "%s(%d)", get_type_name(), id);
	start_lifespan_timer(SIP_MAX_TRANSACTION_LIFE_SPAN);
	//RC_IncrementObject(RC_TRANSACTIONS);
}
//--------------------------------------
//
//--------------------------------------
sip_transaction_t::~sip_transaction_t()
{
	SLOG_TRANS(get_type_name(), "%s --> DESTROYED", m_internal_id);

	stop_all_timers();

	//RC_DecrementObject(RC_TRANSACTIONS);
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_t::push_event(sip_transaction_event_t sip_event, uintptr_t param1, uintptr_t param2)
{
	rtl::MutexWatchLock lock(m_event_queue_lock, __FUNCTION__);

	if (rtl::Logger::check_trace(TRF_TRANS))
	{
		SLOG_EVENT(get_type_name(), "%s Enqueue event %s", m_internal_id, get_sip_transaction_event_name(sip_event));
	}

	if (m_terminating && sip_event != sip_transaction_event_final_e)
	{
		if (sip_event == sip_transaction_event_message_e)
			DELETEO((sip_message_t*)param1);
	}
	else
	{
		sip_transaction_event_info_t evt = { sip_event, param1, param2 };

		//RC_IncrementObject(RC_TRN_EVENTS);

		m_event_queue.push(evt);
		m_manager.m_stage->push_transaction(this);
	}
}
//--------------------------------------
//
//--------------------------------------
bool sip_transaction_t::pop_event(sip_transaction_event_info_t* sip_event)
{
	bool res = false;
	
	rtl::MutexWatchLock lock(m_event_queue_lock, __FUNCTION__);
	
	if (m_event_queue.getCount() > 0)
	{
		*sip_event = m_event_queue.pop();
		
		//RC_DecrementObject(RC_TRN_EVENTS);

		res = true;

		if (rtl::Logger::check_trace(TRF_TRANS))
		{
			SLOG_EVENT(get_type_name(), "%s Dequeue event %s", m_internal_id, get_sip_transaction_event_name(sip_event->type));
		}
	}

	return res;
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_t::flush_events()
{
	while (m_event_queue.getCount() > 0)
	{
		sip_transaction_event_info_t& sip_event = m_event_queue.pop();

		if (sip_event.type == sip_transaction_event_message_e)
			DELETEO((sip_message_t*)sip_event.param1);
	}

	m_event_queue.clear();
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_t::process_events()
{
	try
	{
		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		sip_transaction_event_info_t sip_event = { sip_transaction_event_unknown_e, 0, 0 };
		
		if (!pop_event(&sip_event) || m_terminated)
		{
			SLOG_TRANS(get_type_name(), "%s Event Processing : terminated", m_internal_id);

			if (sip_event.type == sip_transaction_event_message_e)
			{
				DELETEO((sip_message_t*)sip_event.param1);
			}

			return;
		}

		switch (sip_event.type)
		{
		case sip_transaction_event_timer_e:
			{
				sip_transaction_timer_t ttype = (sip_transaction_timer_t)sip_event.param1;
				SLOG_TRANS(get_type_name(), "%s Event Processing : Timer %s", m_internal_id, sip_timer_manager_t::GetTimerName(ttype));
				process_sip_timer_event(ttype, (uint32_t)sip_event.param2);
			}
			break;	
		case sip_transaction_event_message_e:
			{
				sip_message_t* msg = (sip_message_t*)sip_event.param1;
				rtl::String startLine = msg->GetStartLine();

				SLOG_TRANS(get_type_name(), "%s Event Processing : Message %s", m_internal_id, (const char*)startLine);

				process_sip_transaction_event(*msg);

				DELETEO(msg);
			}
			break;
		case sip_transaction_event_final_e:
			SLOG_TRANS(get_type_name(), "%s Event Processing : FinalEvent", m_internal_id);
			process_sip_transaction_final_event();
			m_manager.m_stage->terminate_transaction(this);
			m_terminated = true;
			break;
		case sip_transaction_event_cancel_e:
			SLOG_TRANS(get_type_name(), "%s Event Processing : Cancel", m_internal_id);
			process_sip_transaction_cancel_event();
			break;
		default:
			SLOG_WARN(get_type_name(), "%s Event Processing : RECEIVED UNKNOWN EVENT '%s'", m_internal_id, get_sip_transaction_event_name(sip_event.type));
			break;
		}
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR(get_type_name(), "Event Processing : catches exception: %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_t::process_sip_timer_event(sip_transaction_timer_t timerType, uint32_t dwInterval)
{
	SLOG_ERROR(get_type_name(), "pure virtual call -- %s", __FUNCTION__);
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_t::process_sip_transaction_event(sip_message_t& message/*, BOOL isSendEvent*/)
{
	SLOG_ERROR(get_type_name(), "pure virtual call -- %s", __FUNCTION__);
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_t::process_sip_transaction_final_event()
{
	m_terminating = true;
	m_manager.remove_transaction(this);
	stop_all_timers();
	flush_events();
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_t::process_sip_transaction_cancel_event()
{
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_t::sip_timer_elapsed(sip_transaction_timer_t type, uint32_t dwInterval)
{
	SLOG_TRANS(get_type_name(), "%s Enqueue timer event %s(%d ms)", m_internal_id, sip_timer_manager_t::GetTimerName(type), dwInterval);

	push_event(sip_transaction_event_timer_e, type, dwInterval);
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_t::destroy()
{
	SLOG_TRANS(get_type_name(), "%s Terminating...", m_internal_id);

	m_terminating = true;
	push_event(sip_transaction_event_final_e, 0, 0);
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_t::set_opening_request(const sip_message_t& message)
{ 
	m_reliable_transport = message.getRoute().type != sip_transport_udp_e;

	m_opening_request = message;
}
//--------------------------------------
//
//--------------------------------------
bool sip_transaction_t::send_message_to_transport(sip_message_t& msg)
{
	bool res = false;

	try
	{
		rtl::MutexWatchLock lock(m_send_lock, __FUNCTION__);

		if (!m_terminating)
		{

			if (msg.IsValid())
			{
				m_manager.send_message_to_transport(msg, *this);
			}
			else
			{
				SLOG_WARN(get_type_name(), "%s : sending message to transport failed : invalid message \n%s!", m_internal_id, (const char*)msg.to_string());
			}
		}
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR(get_type_name(), "%s : sending message to transport failed : failed with exception %s", m_internal_id, ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	return res;
}
//--------------------------------------
//
//--------------------------------------
//void sip_transaction_t::set_call_id(const char* call_id)
//{
//	m_call_id = call_id;
//}
//--------------------------------------
//
//--------------------------------------
static int transaction_compare(const sip_transaction_list_t::TRN_KEY& left, const sip_transaction_list_t::TRN_KEY& right)
{
	return std_stricmp(left.key, right.key);
}
//--------------------------------------
//
//--------------------------------------
static int transaction_key_compare(const ZString& left, const sip_transaction_list_t::TRN_KEY& right)
{
	return std_stricmp(left, right.key);
}
//--------------------------------------
//
//--------------------------------------
sip_transaction_list_t::sip_transaction_list_t() : m_pool(transaction_compare, transaction_key_compare, 512)
{
}
//--------------------------------------
//
//--------------------------------------
sip_transaction_list_t::~sip_transaction_list_t()
{
	for (int i = 0; i < m_pool.getCount(); i++)
	{
		TRN_KEY key = m_pool[i];
		FREE(key.key);
	}

	m_pool.clear();
}
//--------------------------------------
//
//--------------------------------------
bool sip_transaction_list_t::add(sip_transaction_t* transaction, ZString key)
{
	bool res = false;

	if (transaction != NULL && key != NULL)
	{
		TRN_KEY trn_rec = { NULL, transaction };

		int len = (int)strlen(key);
		trn_rec.key = (char*)MALLOC(len+1);
		strcpy(trn_rec.key, key);
		res = m_pool.add(trn_rec) != BAD_INDEX;
	}

	return res;
}
//--------------------------------------
//
//--------------------------------------
sip_transaction_t* sip_transaction_list_t::remove(ZString key)
{
	sip_transaction_t* transaction = NULL;

	if (key != NULL && key[0] != 0)
	{
		int index;
		TRN_KEY* trans_ptr = m_pool.find(key, index);
		if (trans_ptr != NULL)
		{
			transaction = trans_ptr->transaction;
			FREE(trans_ptr->key);
			m_pool.removeAt(index);
		}
	}

	return transaction;
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_list_t::remove(sip_transaction_t* transaction)
{
	for (int i = 0; i < m_pool.getCount(); i++)
	{
		TRN_KEY& trans_ptr = m_pool[i];
		if (trans_ptr.transaction == transaction)
		{
			FREE(trans_ptr.key);
			m_pool.removeAt(i);
			return;
		}
	}
}
//--------------------------------------
//
//--------------------------------------
sip_transaction_t* sip_transaction_list_t::find(ZString key)
{
	if (key != NULL)
	{
		TRN_KEY* trans_ptr = m_pool.find(key);
		if (trans_ptr != NULL)
			return trans_ptr->transaction;
	}

	return NULL;
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_list_t::clear()
{
	m_pool.clear();
}
//--------------------------------------
//
//--------------------------------------
const char* get_sip_transaction_event_name(sip_transaction_event_t type)
{
	static const char* names[] = { "Unknown", "Timer", "Message", "Final", "Cancel" };

	if ((int)type >= 0 && (int)type < sizeof(names)/sizeof(const char*))
		return names[type];
	
	return "Unknown";
}
//--------------------------------------
