/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_uri.h"
//--------------------------------------
//
//--------------------------------------
const sip_uri_t sip_uri_t::empty;
//--------------------------------------
//
//--------------------------------------
void sip_uri_t::cleanup()
{
	m_scheme = rtl::String::empty;
	m_user = rtl::String::empty;
	m_password = rtl::String::empty;
	m_host = rtl::String::empty;
	m_port = rtl::String::empty;
	m_params.cleanup();
	m_headers.clear();
}
//--------------------------------------
//
//--------------------------------------
void sip_uri_t::parse_colon_pairs(const char* text, int length, rtl::String& left, rtl::String& right)
{
	const char* colon = (const char*)memchr(text, ':', length);

	if (colon != nullptr)
	{
		int left_length = PTR_DIFF(colon, text);
		left.assign(text, left_length);
		right.assign(colon + 1, length - (left_length + 1));
	}
	else
	{
		left.assign(text, length);
		right.setEmpty();
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_uri_t::parse_param_set(const char* param_set, int length)
{
	const char* cp = param_set;
	const char* next = (const char*)memchr(cp, ';', length);
	int plen;

	while (next != nullptr)
	{
		plen = PTR_DIFF(next, cp);
		parse_param(cp, plen);
		cp = next+1;
		length -= plen+1;
		next = (const char*)memchr(cp, ';', length);
	}

	parse_param(cp, length);
}
//--------------------------------------
//
//--------------------------------------
void sip_uri_t::parse_param(const char* param, int length)
{
	const char* eq = (const char*)memchr(param, '=', length);

	rtl::String name;
	rtl::String value;

	if (eq != nullptr)
	{
		int plen = PTR_DIFF(eq, param);
		name.assign(param, plen);
		value.assign(eq+1, length - (plen+1));
	}
	else
	{
		name.assign(param, length);
	}

	m_params.set(name, sip_unescape_rfc3986(value));
}
//--------------------------------------
//
//--------------------------------------
void sip_uri_t::parse_header_set(const char* header_set, int length)
{
	const char* cp = header_set;
	const char* next = (const char*)memchr(cp, '&', length);
	int plen;

	while (next != nullptr)
	{
		plen = PTR_DIFF(next, cp);
		parse_header(cp, plen);
		cp = next+1;
		length -= plen+1;
		next = (const char*)memchr(cp, '&', length);
	}

	parse_header(cp, length);
}
//--------------------------------------
//
//--------------------------------------
void sip_uri_t::parse_header(const char* header, int length)
{
	const char* eq = (const char*)memchr(header, '=', length);

	rtl::String name;
	rtl::String value;

	if (eq != nullptr)
	{
		int plen = PTR_DIFF(eq, header);
		name.assign(header, plen);
		value.assign(eq+1, length - (plen+1));
	}
	else
	{
		name.assign(header, length);
	}

	m_headers.add(name, sip_unescape_rfc3986(value));
}
//--------------------------------------
//
//--------------------------------------
rtl::String& sip_uri_t::params_to_string(rtl::String& stream) const
{
	for (int i = 0; i < m_params.getCount(); i++)
	{
		const mime_param_t* param = m_params.getAt(i);
		stream << ';' << param->getName();
		if (!param->getValue().isEmpty())
			stream << '=' << sip_escape_rfc3986(param->getValue());
	}

	return stream;
}
//--------------------------------------
//
//--------------------------------------
rtl::String& sip_uri_t::headers_to_string(rtl::String& stream) const
{
	int header_count = m_headers.getCount();

	if (header_count > 0)
	{
		stream << '?';

		for (int i = 0; i < header_count; i++)
		{
			const mime_param_t* header = m_headers.getAt(i);
			stream << header->getName() << '=' << sip_escape_rfc3986(header->getValue());

			if (i < header_count - 1)
				stream << '&';
		}
	}

	return stream;
}
//--------------------------------------
//
//--------------------------------------
sip_uri_t& sip_uri_t::operator = (const sip_uri_t& uri)
{
	m_scheme = uri.m_scheme;
	m_user = uri.m_user;
	m_password = uri.m_password;
	m_host = uri.m_host;
	m_port = uri.m_port;

	m_params = uri.m_params;
	m_headers = uri.m_headers;
	
	return *this;
}
//--------------------------------------
//	sip:alice@atlanta.com
//	sip:alice:secretword@atlanta.com;transport=tcp
//	sips:alice@atlanta.com?subject=project%20x&priority=urgent
//	sip:+1-212-555-1212:1234@gateway.com;user=phone
//	sips:1212@gateway.com
//	sip:alice@192.0.2.4
//	sip:atlanta.com;method=REGISTER?to=alice%40atlanta.com
//	sip:alice;day=tuesday@atlanta.com
//--------------------------------------
bool sip_uri_t::assign(const char* uri, int length)
{
	cleanup();

	if (length == 0 || uri == nullptr || uri[0] == 0)
	{
		return false;
	}

	if (length < 0)
	{
		length = (int)strlen(uri);
	}

	/*
	sip:user:password@host:port;uri-parameters?headers
	*/

	const char* cp;
	int found_length;

	// extact scheme
	cp = (const char*)memchr(uri, ':', length);

	if (cp == nullptr)
	{
		return false;
	}

	if (*uri == '<')
	{
		LOG_ERROR("sip-uri", "Invalid uri '%s'", uri);
	}

	found_length = PTR_DIFF(cp, uri);
	m_scheme.assign(uri, found_length);

	// user:password
	uri = cp + 1;
	length -= found_length + 1;

	// �������� ��������� � ��������� �� ������
	const char* params = (const char*)memchr(uri, ';', length);
	int param_length = 0;
	const char* headers = nullptr;
	int header_length = 0;

	found_length = length;

	if (params == nullptr)
	{
		headers = (const char*)memchr(uri, '?', length);

		if (headers != nullptr)
		{
			found_length = PTR_DIFF(headers, uri);
			header_length = length - PTR_DIFF(headers, uri);
		}
	}
	else
	{
		param_length = length - PTR_DIFF(params, uri);
		found_length = PTR_DIFF(params, uri);

		headers = (const char*)memchr(params, '?', param_length);
		if (headers != nullptr)
		{
			header_length = param_length - PTR_DIFF(headers, params);
			param_length -= header_length;
		}
	}

	cp = (const char*)memchr(uri, '@', found_length);

	if (cp != nullptr)
	{
		parse_colon_pairs(uri, PTR_DIFF(cp, uri), m_user, m_password);

		length -= PTR_DIFF(cp, uri) + 1;
		found_length -= PTR_DIFF(cp, uri) + 1;
		uri = cp + 1;
	}

	parse_colon_pairs(uri, found_length, m_host, m_port);

	// ������ ��������� � ������
	if (params != nullptr && param_length > 0)
	{
		parse_param_set(params + 1, param_length - 1);
	}

	if (headers != nullptr && header_length > 0)
	{
		parse_header_set(headers + 1, header_length - 1);
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
rtl::String sip_uri_t::to_string(bool includeURIParams, bool includeUser, bool includePort) const
{
	rtl::String result;

	if (m_user.isEmpty() && m_host.isEmpty())
		return rtl::String::empty;

	///sip:user:password@host:port;uri-parameters?headers
	///sip:alice:secretword@atlanta.com;transport=tcp
	result << m_scheme << ':';

	if (!m_user.isEmpty() && includeUser)
	{
		result << m_user;

		if (!m_password.isEmpty())
			result << ':' << m_password;

		result << '@';
	}

	result << m_host;

	if (!m_port.isEmpty() && includePort)
		result << ':' << m_port;

	if (includeURIParams)
	{
		params_to_string(result);

		headers_to_string(result);
	}

	return result;
}
//--------------------------------------
//
//--------------------------------------
rtl::String& sip_uri_t::write_to(rtl::String& stream, bool includeURIParams, bool includeUser, bool includePort) const
{
	if (m_user.isEmpty() && m_host.isEmpty())
		return stream;

	///sip:user:password@host:port;uri-parameters?headers
	///sip:alice:secretword@atlanta.com;transport=tcp
	stream << m_scheme << ':';

	if (!m_user.isEmpty() && includeUser)
	{
		stream << m_user;

		if (!m_password.isEmpty())
			stream << ':' << m_password;

		stream << '@';
	}

	stream << m_host;

	if (!m_port.isEmpty() && includePort)
		stream << ':' << m_port;

	if (includeURIParams)
	{
		params_to_string(stream);

		headers_to_string(stream);
	}

	return stream;
}
//--------------------------------------
//
//--------------------------------------
sip_uri_header_list_t& sip_uri_header_list_t::assign(const sip_uri_header_list_t& list)
{
	clear();

	for (int i = 0; i < list.getCount(); i++)
	{
		const mime_param_t* param = list.getAt(i);
		add(param->getName(), param->getValue());
	}

	return *this;
}
//--------------------------------------
//
//--------------------------------------
const mime_param_t* sip_uri_header_list_t::find(const rtl::String& name, int index) const
{
	int f_index = 0;

	for (int i = 0; i < m_array.getCount(); i++)
	{
		mime_param_t* param = m_array[i];
		
		if (rtl::String::compare(param->getName(), name, true) == 0)
		{
			if (f_index == index)
			{
				index = f_index;
				return param;
			}
			
			f_index++;
		}
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
mime_param_t* sip_uri_header_list_t::find(const rtl::String& name, int index)
{
	int f_index = 0;

	for (int i = 0; i < m_array.getCount(); i++)
	{
		mime_param_t* param = m_array[i];

		if (rtl::String::compare(param->getName(), name, true) == 0)
		{
			if (f_index == index)
			{
				index = f_index;
				return param;
			}

			f_index++;
		}
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
const rtl::String& sip_uri_header_list_t::get(const rtl::String& name, bool& found, int index) const
{
	const mime_param_t* uri_header = find(name, index);
	
	found = uri_header != nullptr;

	return found ? uri_header->getValue() : rtl::String::empty;
}
//--------------------------------------
//
//--------------------------------------
void sip_uri_header_list_t::add(const rtl::String& name, const rtl::String& value)
{
	mime_param_t* uri_header = NEW mime_param_t(name, value);

	m_array.add(uri_header);
}
//--------------------------------------
//
//--------------------------------------
bool sip_uri_header_list_t::set(const rtl::String& name, const rtl::String& value, int index)
{
	mime_param_t* uri_header = find(name, index);
	bool found = uri_header != nullptr;

	if (found)
	{
		uri_header->setValue(value);
	}

	return found;
}
//--------------------------------------
//
//--------------------------------------
bool sip_uri_header_list_t::remove(const rtl::String& name, int index)
{
	for (int i = 0; i < m_array.getCount(); i++)
	{
		mime_param_t* param = m_array[i];
		if (rtl::String::compare(param->getName(), name, true) == 0)
		{
			m_array.removeAt(i);
			DELETEO(param);

			return true;
		}
	}

	return false;
}
//--------------------------------------
//
//--------------------------------------
void sip_uri_header_list_t::clear()
{
	for (int i = 0; i < m_array.getCount(); i++)
	{
		mime_param_t* param = m_array[i];
		DELETEO(param);
	}

	m_array.clear();
}
//--------------------------------------
//
//--------------------------------------
void sip_uri_header_list_t::removeAt(int index)
{
	if (index >= 0 && index < m_array.getCount())
	{
		mime_param_t* param = m_array.getAt(index);
		m_array.removeAt(index);

		if (param != nullptr)
		{
			DELETEO(param);
		}
	}
}
//--------------------------------------
