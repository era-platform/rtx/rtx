/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_transport.h"
#include "sip_transport_pool.h"
//--------------------------------------
//
//--------------------------------------
sip_transport_pool_t::sip_transport_pool_t() : m_list(compare_transport, compare_key)
{
}
//--------------------------------------
//
//--------------------------------------
sip_transport_pool_t::~sip_transport_pool_t()
{
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_pool_t::cleanup()
{
	rtl::MutexLock lock(m_sync);

	for (int i = 0; i < m_list.getCount(); i++)
	{
		transport_ptr transport = m_list.getAt(i);

		transport->release_ref();

		transport->destroy();
	}

	m_list.clear();
}
//--------------------------------------
//
//--------------------------------------
bool sip_transport_pool_t::add_transport(sip_transport_t* transport)
{
	rtl::MutexLock lock(m_sync);

	SLOG_NET("net-pool", "add transport %s (%016I64x)...", transport->get_id(), transport->get_key());
	
	transport->add_ref();
	bool result = m_list.add(transport) != BAD_INDEX;
	
	SLOG_NET("net-pool", "transport %s", result ? "added" : "NOT added");

	return result;
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_pool_t::remove_transport(sip_transport_t* transport)
{
	{
		rtl::MutexLock lock(m_sync);
		SLOG_NET("net-pool", "remove transport %s (%016I64x)...", transport->get_id(), transport->get_key());
		int index = BAD_INDEX;
		uint64_t key = transport->get_key();
		const transport_ptr* found = m_list.find(key, index);
		
		if (found == nullptr || *found == nullptr || index == BAD_INDEX)
		{
			SLOG_NET("net-pool", "transport not match stored pointer %p != %p", transport, found);
			return;
		}

		m_list.removeAt(index);

		long ref_cnt = transport->release_ref();

		if (ref_cnt <= 0)
		{
			transport = nullptr;
		}
	}
	
	if (transport != nullptr)
	{
		transport->destroy();
	}

	SLOG_NET("net-pool", "transport removed");
}
//--------------------------------------
//
//--------------------------------------
sip_transport_t* sip_transport_pool_t::find_transport(const sip_transport_route_t& route)
{
	int index = -1;
	uint64_t key = MAKE_KEY64(route.remoteAddress, route.remotePort, route.type);
	
	rtl::MutexLock lock(m_sync);
	
	const transport_ptr* info = m_list.find(key, index);

	return info != nullptr ? *info : nullptr;
}
//--------------------------------------
//
//--------------------------------------
sip_transport_t* sip_transport_pool_t::find_transport(const sip_transport_point_t& endpoint)
{
	int index = -1;
	uint64_t key = MAKE_KEY64(endpoint.address, endpoint.port, endpoint.type);

	rtl::MutexLock lock(m_sync);

	const transport_ptr* info = m_list.find(key, index);

	return info != nullptr ? *info : nullptr;
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_pool_t::reset_transport_type(sip_transport_t* transport, sip_transport_type_t new_type)
{
	rtl::MutexLock list(m_sync);

	int index = -1;
	uint64_t key = transport->get_key();
	const transport_ptr* info = m_list.find(key, index);

	if (info == nullptr)
	{
		const sip_transport_route_t& route = transport->getRoute();
		SLOG_NET_WARN("net-pool", "replacing transport type (%s:%u %s -> %s) failed : no transport found!",
			inet_ntoa(route.remoteAddress), route.remotePort, TransportType_toString(route.type), TransportType_toString(new_type));
	}
	else
	{
		if (index != -1)
		{
			// transport refcount not changed addref and release not called
			m_list.removeAt(index);
			transport->reset_type(new_type);
			m_list.add(transport);
		}
		else
		{
			const sip_transport_route_t& route = transport->getRoute();
			SLOG_NET_WARN("net-pool", "replacing transport type (%s:%u %s -> %s) failed : transport removed!",
				inet_ntoa(route.remoteAddress), route.remotePort, TransportType_toString(route.type), TransportType_toString(new_type));
		}
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_pool_t::check_transport_timeouts()
{
	//rtl::ArrayT<sip_transport_t*> list;

	//{
	//	rtl::MutexLock lock(m_sync);

	//	uint32_t ticks = rtl::DateTime::getTicks();

	//	for (int i = m_list.getCount() - 1; i >= 0; i--)
	//	{
	//		sip_transport_t* transport = m_list.getAt(i);
	//		if (transport->get_user_count() == 0)
	//		{
	//			int diff = ticks - transport->timestamp();
	//			if (diff > 64 * 500)
	//			{
	//				m_list.removeAt(i);
	//				list.add(transport);
	//			}
	//		}
	//	}
	//}

	//for (int i = 0; i < list.getCount(); i++)
	//{
	//	list[i]->disconnect();
	//}

	//list.clear();
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_pool_t::disable_transports(sip_transport_point_t& iface)
{
	rtl::MutexLock lock(m_sync);

	for (int i = m_list.getCount() - 1; i >= 0; i--)
	{
		sip_transport_t* transport = m_list.getAt(i);
		if (transport->getRoute().if_address.s_addr == iface.address.s_addr)
		{
			remove_transport(transport);
		}
	}
}
//--------------------------------------
//
//--------------------------------------
int sip_transport_pool_t::compare_key(const uint64_t& left, const transport_ptr& right)
{
	return left < right->get_key() ? -1 : left > right->get_key() ? 1 : 0;
}
//--------------------------------------
//
//--------------------------------------
int sip_transport_pool_t::compare_transport(const transport_ptr& left, const transport_ptr& right)
{
	return left->get_key() < right->get_key() ? -1 : left->get_key() > right->get_key() ? 1 : 0;
}
//--------------------------------------
