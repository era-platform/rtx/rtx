/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_value.h"
#include "sip_value_uri.h"
#include "sip_value_auth.h"
#include "sip_value_num.h"
#include "sip_value_user.h"
#include "sip_value_cseq.h"
#include "sip_value_via.h"
#include "sip_value_contact.h"
//--------------------------------------
//
//--------------------------------------
rtl::String& sip_value_t::prepare(rtl::String& stream) const
{
	return stream << m_value;
}
//--------------------------------------
//
//--------------------------------------
bool sip_value_t::parse()
{
	return true;
}
//--------------------------------------
// ����� ����������: ����� ������������ ��� ��� ��������� � ������� ('\"' '<')
//--------------------------------------
void sip_value_t::extract_parameters()
{
	int index = -1;
	const char* ptr;

	do
	{
		if ((index = m_value.indexOfAny("<\";", index+1)) != -1)
		{
			// ���� ���-�� �������!
			switch (m_value[index])
			{
			case '<':
				index = m_value.indexOf('>', index + 1);
				break;
			case '\"':
				index = m_value.indexOf('\"', index + 1);
				break;
			case ';':
				ptr = m_value;
				parse_parameters(ptr + (index + 1));
				m_value.remove(index, m_value.getLength() - index);
				index = -1;
				break;
			}
		}
	}
	while (index != -1);
}
//--------------------------------------
//
//--------------------------------------
void sip_value_t::parse_parameters(const char* params)
{
	const char* param = params;
	const char* delim;
	const char* eq;

	rtl::String name, value;

	while ((delim = strchr(param, ';')) != nullptr)
	{
		if ((eq = (const char*)memchr(param, '=', delim - param)) != nullptr)
		{
			name.assign(param, PTR_DIFF(eq, param));
			value.assign(eq + 1, PTR_DIFF(delim, (eq + 1)));
		}
		else
		{
			name.assign(param, PTR_DIFF(delim, param));
			value.setEmpty();
		}

		m_params.set(name, value);
		param = delim + 1;
	}

	if ((eq = strchr(param, '=')) != nullptr)
	{
		name.assign(param, PTR_DIFF(eq, param));
		value.assign(eq+1);
	}
	else
	{
		name.assign(param);
		value.setEmpty();
	}
	
	m_params.set(name, sip_unescape_rfc3986(value));
}
//--------------------------------------
//
//--------------------------------------
sip_value_t::~sip_value_t()
{
	m_value.setEmpty();
	m_params.cleanup();
}
//--------------------------------------
// ������� ��� ����������� ������ ���� ��������,
// ���� �������� ��������� ����������� �������� �� ���� ���� ������� ��������
//--------------------------------------
sip_value_t& sip_value_t::assign(const rtl::String& value)
{
	m_value = value.trim();
	m_params.cleanup();
	extract_parameters();
	parse();
	return *this;
}
//--------------------------------------
//
//--------------------------------------
sip_value_t& sip_value_t::assign(const sip_value_t& value)
{
	m_value = value.m_value;
	m_params = value.m_params;
	return *this;
}

//--------------------------------------
//
//--------------------------------------
void sip_value_t::cleanup()
{
	m_value.setEmpty();
	m_params.cleanup();
}
//--------------------------------------
//
//--------------------------------------
sip_value_t* sip_value_t::clone() const
{
	return NEW sip_value_t(*this);
}
//--------------------------------------
//
//--------------------------------------
bool sip_value_t::read_from(const char* string, int length)
{
	m_value.assign(string, length);
	m_value.trim();
	m_params.cleanup();
	extract_parameters();
	// ���� ���� ��������� ���� �� ������ ������ ����� ������ ����
	return parse();
}
//--------------------------------------
// �������� ������ ���� �������� (����� �� �������, ������� ������ � ��������)
// �������: nullptr ��������� ��������, ����� ������ ���� ��� �������� �� ����� �� ���������
//--------------------------------------
const char* sip_value_t::read_from(const char* stream, bool& result)
{
	const char* ptr = stream;
	const char* eol = stream;
	bool next_value = false;
	
	while ((eol = strpbrk(eol, ",\r")) != nullptr)
	{
		if (*eol == ',')
		{
			// new value
			eol++;
			next_value = true;
			break;
		}

		// ���� ������� ������������� ����� ��� ���� ������ ���
		// ������� �������� �� ������� ������ �������
		//if (*eol == '\r')

		if (*(++eol) == '\n') eol++;
		
		if (*eol != ' ' && *eol != '\t')
		{
			// new header!
			break;
		}

		// this is whitespace
		// \r\n\x20 -> single SP
		// \r\n\t -> single SP
	}

	m_value.assign(ptr, eol != nullptr ? PTR_DIFF((eol-1), ptr) : -1);
	m_value.trim();

	m_params.cleanup();
	extract_parameters();
	
	result = parse();

	// ���� ���� ��������� ���� �� ������ ������ ����� ������ ����
	return next_value ? eol : nullptr;
}
//--------------------------------------
//
//--------------------------------------
rtl::String& sip_value_t::write_to(rtl::String& stream) const
{
	prepare(stream);

	for (int i = 0; i < m_params.getCount(); i++)
	{
		const mime_param_t* param = m_params.getAt(i);

		stream << ';' << param->getName();
		if (!param->getValue().isEmpty())
		{
			stream << '=' << sip_escape_rfc3986(param->getValue());
		}
	}

	return stream;
}
//--------------------------------------
// ���������� "������������" ������������
//--------------------------------------
typedef sip_value_t* (*value_constructor_t)();
//--------------------------------------
// ������� ��� ���������, ��� : ������
//--------------------------------------
sip_value_t* create_std_sip_value()
{
	return NEW sip_value_t();
}
//--------------------------------------
// ��������� � �������
//--------------------------------------
sip_value_t* create_std_sip_value_uri()
{
	return NEW sip_value_uri_t();
}
//--------------------------------------
// ��������� � ������� �����
//--------------------------------------
//sip_value_t* create_std_sip_value_c()
//{
//	return NEW sip_value_c_t();
//}
//--------------------------------------
// ��������� � ������� ���������� ����������� �������
//--------------------------------------
sip_value_t* create_std_sip_value_auth()
{
	return NEW sip_value_auth_t();
}
//--------------------------------------
// ��������� � �������
//--------------------------------------
sip_value_t* create_std_sip_value_number()
{
	return NEW sip_value_number_t();
}
//--------------------------------------
// ��������� � ������� �������
//--------------------------------------
sip_value_t* create_std_sip_value_user()
{
	return NEW sip_value_user_t();
}
//--------------------------------------
//
//--------------------------------------
sip_value_t* create_std_sip_value_cseq()
{
	return NEW sip_value_cseq_t();
}
//--------------------------------------
//
//--------------------------------------
sip_value_t* create_std_sip_value_contact()
{
	return NEW sip_value_contact_t();
}
//--------------------------------------
//
//--------------------------------------
sip_value_t* create_std_sip_value_via()
{
	return NEW sip_value_via_t();
}
//--------------------------------------
//
//--------------------------------------
static value_constructor_t s_ctor_list[] =
{
	create_std_sip_value, //{ sip_std_Accept_e, "Accept", },
	create_std_sip_value, //{ sip_std_Accept_Contact_e, "Accept-Contact", },
	create_std_sip_value, //{ sip_std_Accept_Encoding_e, "Accept-Encoding", },
	create_std_sip_value, //{ sip_std_Accept_Language_e, "Accept-Language", },
	create_std_sip_value, //{ sip_std_Alert_Info_e, "Alert-Info", },
	create_std_sip_value, //{ sip_std_Allow_e, "Allow", },
	create_std_sip_value, //{ sip_std_Allow_Events_e, "Allow-Events", },
	create_std_sip_value, //{ sip_std_Authentication_Info_e, "Authentication-Info", },
	create_std_sip_value_auth, //{ sip_std_Authorization_e, "Authorization", },
	create_std_sip_value, //{ sip_std_Call_ID_e, "Call-ID", },
	create_std_sip_value, //{ sip_std_Call_Info_e, "Call-Info", },
	create_std_sip_value_contact, //{ sip_std_Contact_e, "Contact", },
	create_std_sip_value, //{ sip_std_Content_Disposition_e, "Content-Disposition", },
	create_std_sip_value, //{ sip_std_Content_Encoding_e, "Content-Encoding", },
	create_std_sip_value, //{ sip_std_Content_Language_e, "Content-Language", },
	create_std_sip_value_number, //{ sip_std_Content_Length_e, "Content-Length", },
	create_std_sip_value, //{ sip_std_Content_Type_e, "Content-Type", },
	create_std_sip_value_cseq, //{ sip_std_CSeq_e, "CSeq", },
	create_std_sip_value, //{ sip_std_Date_e, "Date", },
	create_std_sip_value, //{ sip_std_Diversion_e, "Diversion", },
	create_std_sip_value, //{ sip_std_Error_Info_e, "Error-Info", },
	create_std_sip_value, //{ sip_std_Event_e, "Event", },
	create_std_sip_value_number, //{ sip_std_Expires_e, "Expires", },
	create_std_sip_value_user, //{ sip_std_From_e, "From", },
	create_std_sip_value, //{ sip_std_History_Info_e, "History-Info", },
	create_std_sip_value, //{ sip_std_Identity_e, "Identity", },
	create_std_sip_value, //{ sip_std_Identity_Info_e, "Identity-Info", },
	create_std_sip_value, //{ sip_std_In_Reply_To_e, "In-Reply-To", },
	create_std_sip_value, //{ sip_std_Info_Package_e, "Info-Package", },
	create_std_sip_value, //{ sip_std_Join_e, "Join", },
	create_std_sip_value_number, //{ sip_std_Max_Forwards_e, "Max-Forwards", },
	create_std_sip_value_number, //{ sip_std_Min_Expires_e, "Min-Expires", },
	create_std_sip_value, //{ sip_std_Min_SE_e, "Min-SE", },
	create_std_sip_value, //{ sip_std_MIME_Version_e, "MIME-Version", },
	create_std_sip_value, //{ sip_std_Organization_e, "Organization", },
	create_std_sip_value, //{ sip_std_P_Access_Network_Info_e, "P-Access-Network-Info", },
	create_std_sip_value, //{ sip_std_P_Asserted_Identity_e, "P-Asserted-Identity", },
	create_std_sip_value, //{ sip_std_P_Asserted_Service_e, "P-Asserted-Service", },
	create_std_sip_value, //{ sip_std_P_Associated_URI_e, "P-Associated-URI", },
	create_std_sip_value, //{ sip_std_P_Called_Party_ID_e, "P-Called-Party-ID", },
	create_std_sip_value, //{ sip_std_P_Charging_Function_Addresses_e, "P-Charging-Function-Addresses", },
	create_std_sip_value, //{ sip_std_P_Charging_Vector_e, "P-Charging-Vector", },
	create_std_sip_value, //{ sip_std_P_DCS_BILLING_INFO_e, "P-DCS-BILLING-INFO", },
	create_std_sip_value, //{ sip_std_P_DCS_LAES_e, "P-DCS-LAES", },
	create_std_sip_value, //{ sip_std_P_DCS_OSPS_e, "P-DCS-OSPS", },
	create_std_sip_value, //{ sip_std_P_DCS_REDIRECT_e, "P-DCS-REDIRECT", },
	create_std_sip_value, //{ sip_std_P_DCS_Trace_Party_ID_e, "P-DCS-Trace-Party-ID", },
	create_std_sip_value, //{ sip_std_P_Early_Media_e, "P-Early-Media", },
	create_std_sip_value, //{ sip_std_P_Preferred_Service_e, "P-Preferred-Service", },
	create_std_sip_value, //{ sip_std_P_Profile_Key_e, "P-Profile-Key", },
	create_std_sip_value, //{ sip_std_P_Refused_URI_List_e, "P-Refused-URI-List", },
	create_std_sip_value, //{ sip_std_P_Served_User_e, "P-Served-User", },
	create_std_sip_value, //{ sip_std_P_Visited_Network_ID_e, "P-Visited-Network-ID", },
	create_std_sip_value_uri, //{ sip_std_Path_e, "Path", },
	create_std_sip_value, //{ sip_std_Priority_e, "Priority", },
	create_std_sip_value_auth, //{ sip_std_Proxy_Authenticate_e, "Proxy-Authenticate", },
	create_std_sip_value_auth, //{ sip_std_Proxy_Authorization_e, "Proxy-Authorization", },
	create_std_sip_value, //{ sip_std_Proxy_Require_e, "Proxy-Require", },
	create_std_sip_value, //{ sip_std_RAck_e, "RAck", },
	create_std_sip_value, //{ sip_std_Reason_e, "Reason", },
	create_std_sip_value_uri, //{ sip_std_Record_Route_e, "Record-Route", },
	create_std_sip_value, //{ sip_std_Recv_Info_e, "Recv-Info", },
	create_std_sip_value_uri, //{ sip_std_Refer_to_e, "Refer-To", },
	create_std_sip_value_uri, //{ sip_std_Referred_By_e, "Referred-By", },
	create_std_sip_value, //{ sip_std_Reject_Contact_e, "Reject-Contact", },
	create_std_sip_value, //{ sip_std_Request_Disposition_e, "Request-Disposition", },
	create_std_sip_value, //{ sip_std_Require_e, "Require", },
	create_std_sip_value, //{ sip_std_Replaces_e, "Replaces", },
	create_std_sip_value, //{ sip_std_Reply_To_e, "Reply-To", },
	create_std_sip_value, //{ sip_std_Retry_After_e, "Retry-After", },
	create_std_sip_value_number, //{ sip_std_RSeq_e, "RSeq", },
	create_std_sip_value_uri, //{ sip_std_Route_e, "Route", },
	create_std_sip_value, //{ sip_std_Security_Client_e, "Security-Client", },
	create_std_sip_value, //{ sip_std_Security_Server_e, "Security-Server", },
	create_std_sip_value, //{ sip_std_Security_Verify_e, "Security-Verify", },
	create_std_sip_value, //{ sip_std_Server_e, "Server", },
	create_std_sip_value_uri, //{ sip_std_Service_Route_e, "Service-Route", },
	create_std_sip_value, //{ sip_std_Session_Expires_e, "Session-Expires", },
	create_std_sip_value, //{ sip_std_SIP_ETag_e, "SIP-ETag", },
	create_std_sip_value, //{ sip_std_SIP_If_Match_e, "SIP-If-Match", },
	create_std_sip_value, //{ sip_std_Subject_e, "Subject", },
	create_std_sip_value, //{ sip_std_Subscription_State_e, "Subscription-State", },
	create_std_sip_value, //{ sip_std_Supported_e, "Supported", },
	create_std_sip_value, //{ sip_std_Suppress_If_Match_e, "Suppress-If-Match", },
	create_std_sip_value, //{ sip_std_Target_Dialog_e, "Target-Dialog", },
	create_std_sip_value_number, //{ sip_std_Timestamp_e, "Timestamp", },
	create_std_sip_value_user, //{ sip_std_To_e, "To", },
	create_std_sip_value, //{ sip_std_Unsupported_e, "Unsupported", },
	create_std_sip_value, //{ sip_std_User_Agent_e, "User-Agent", },
	create_std_sip_value_via, //{ sip_std_Via_e, "Via", },
	create_std_sip_value, //{ sip_std_Warning_e, "Warning", },
	create_std_sip_value_auth, //{ sip_std_WWW_Authenticate_e, "WWW-Authenticate", },
};

//--------------------------------------
// virtual constructor for typed value
//--------------------------------------
sip_value_t* sip_value_t::create(sip_header_type_t type)
{
	return (type >= sip_Accept_e && type < sip_custom_header_e) ?
		s_ctor_list[type]() :
		create_std_sip_value();
}
//--------------------------------------
