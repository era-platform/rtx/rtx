/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_transport_tcp.h"
#include "sip_transport_listener.h"
#include "sha1.h"
#include "std_crypto.h"
//--------------------------------------
//
//--------------------------------------
#define ASYNC_PROCESS_HANDSHAKE		ASYNC_TRANSP_BASE + 1
#define MAX_TLS_FRAME (1024 * 16)
//--------------------------------------
//
//--------------------------------------
#pragma pack(push, 1)
struct tls_message_header_t
{
	uint8_t type;
	uint16_t version;
	uint16_t length;
};
struct tls_handshake_header_t
{
	uint8_t type;
	uint8_t length[3];
};
struct tls_random_t
{
	uint32_t gmt_unix_time;
	uint8_t random_bytes[28];
};
struct tls_clent_hello_header_t
{
	uint16_t ver;
	tls_random_t random;
};
#pragma pack(pop)
//--------------------------------------
//
//--------------------------------------
static void unmask_packet(uint8_t* packet, int packet_length, const uint8_t* mask);
//static void unmask_buffer_to(uint8_t* dst, const uint8_t* src, int length, uint32_t mask);
static const char* check_handshake_request(raw_packet_t* packet);
//--------------------------------------
//
//--------------------------------------
sip_transport_tcp_t::sip_transport_tcp_t(const sip_transport_point_t& iface, const sip_transport_point_t& endpoint, SOCKET handle) :
	m_tcp_socket(handle),
	sip_transport_t(iface, endpoint, m_tcp_socket),
	m_state(handle == INVALID_SOCKET ? sip_transport_tcp_state_idle: sip_transport_tcp_state_connected),
	m_type(handle == INVALID_SOCKET ? sip_transport_tcp_client : sip_transport_tcp_server),
	m_tls(SLog, handle == INVALID_SOCKET ? tls_connection_role_client_e : tls_connection_role_server_e)
{
	initialize();
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_tcp_t::initialize()
{
	m_ws_proto = (m_route.type == sip_transport_ws_e || m_route.type == sip_transport_wss_e);
	m_wait_first_packet_in = false;

	m_packet = nullptr;

	m_rx_buffer_size = 0;
	m_rx_buffer_read_ptr = 0;

	m_tcp_read_headers = true;
	m_tcp_body_read = 0;

	m_tls_initial_packet = true;
	m_tls_rx_buffer_length = 0;

	if (m_route.type == sip_transport_tls_e)
	{
		m_tls_tx_buffer = (uint8_t*)MALLOC(MAX_TLS_FRAME);
		m_tls_rx_buffer = (uint8_t*)MALLOC(MAX_TLS_FRAME);
	}
	else
	{
		m_tls_tx_buffer = nullptr;
		m_tls_rx_buffer = nullptr;
	}

	m_ws_close_sent = false;
	m_ws_read_state = ws_read_header_e;

	m_ws_payload_length_high = 0;
	m_ws_payload_length = 0;
	m_ws_payload_offset = 0;
	m_ws_packet_buffer = nullptr;
	m_ws_packet_written = 0;

	memset(m_rx_buffer, 0, sizeof m_rx_buffer);

	memset(&m_ws_header, 0, sizeof m_ws_header);
	m_ws_header.opcode = ws_opcode_ctrl_reserver;

	memset(m_ws_payload_mask, 0, 4);

	// ����-����� ������� �� ���� ������
	if (m_route.type == sip_transport_tls_e)
	{
		writer = &sip_transport_tcp_t::write_tls_socket; // : &sip_transport_tcp_t::write_tcp_socket;
		receiver = &sip_transport_tcp_t::read_tls_socket; // : &sip_transport_tcp_t::write_tcp_socket;
	}
	else
	{
		writer = &sip_transport_tcp_t::write_tcp_socket;
		receiver = &sip_transport_tcp_t::read_tcp_socket;
	}

	m_wait_first_packet_in = true;

	if (m_socket.get_handle() != INVALID_SOCKET)
	{
		m_socket.set_tag(this);

		int bsize = 64 * 1024;

		m_socket.set_option(SOL_SOCKET, SO_RCVBUF, &bsize, sizeof(int));
		m_socket.set_option(SOL_SOCKET, SO_SNDBUF, &bsize, sizeof(int));

		sockaddr_in addr;
		sockaddrlen_t addr_len = sizeof addr;
		memset(&addr, 0, sizeof addr);

		char route_tmp[64];

		if (m_socket.get_interface((sockaddr*)&addr, &addr_len))
		{
			m_route.if_address = addr.sin_addr;
			m_route.if_port = ntohs(addr.sin_port);

			SLOG_NET("net-tcp", "%s(%d) : socket route %s...", m_name, get_ref_count(), route_to_string(route_tmp, 64, m_route));
		}
		else
		{
			SLOG_ERR_MSG("net-tcp", std_get_error, "%s(%d) : socket name error ...", m_name, get_ref_count());
		}
	}
}
//--------------------------------------
//
//--------------------------------------
sip_transport_tcp_t::~sip_transport_tcp_t()
{
	if (m_packet != nullptr)
	{
		raw_packet_t::release(m_packet);
		m_packet = nullptr;
	}

	m_tls.destroy();

	if (m_tls_tx_buffer != nullptr)
	{
		FREE(m_tls_tx_buffer);
		m_tls_tx_buffer = nullptr;
	}

	if (m_socket.get_handle() != INVALID_SOCKET)
	{
		m_socket.close();
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_tcp_t::destroy()
{
	rtl::MutexLock lock(m_sender_sync);

	sip_transport_t::destroy();
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_tcp_t::data_ready()
{
	m_last_activity = rtl::DateTime::getTicks();

	int written = 0; 
	int read_result = read_socket(m_rx_buffer + m_rx_buffer_size, SIP_PACKET_MAX - m_rx_buffer_size, written);

	if (read_result <= 0)
	{
		return;
	}

	m_rx_buffer_size += written;

	if (!m_ws_proto) // ...
	{
		tcp_process_data();
	}
	else
	{
		ws_process_data();
	}

	if (m_rx_buffer_read_ptr == m_rx_buffer_size)
	{
		m_rx_buffer_read_ptr = m_rx_buffer_size = 0;
	}

	SLOG_NET("net-tcp", "%s(%d) : data read. rx_ptr:%d rx_size:%d", m_name, get_ref_count(), m_rx_buffer_read_ptr, m_rx_buffer_size);
}
//--------------------------------------
//
//--------------------------------------
bool sip_transport_tcp_t::rx_buffer_read(void* buffer, int size)
{
	if (size <= 0 || m_rx_buffer_read_ptr + size > m_rx_buffer_size)
		return false;

	// �������� ������
	memcpy(buffer, m_rx_buffer + m_rx_buffer_read_ptr, size);
	m_rx_buffer_read_ptr += size;

	return true;
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_tcp_t::tcp_process_data()
{
	while (m_rx_buffer_read_ptr < m_rx_buffer_size)
	{
		int tmp_m_rx_buffer_read_ptr = m_rx_buffer_read_ptr;

		if (m_packet == nullptr)
		{
			// ������ ��������� ������ PING
			if (*(uint32_t*)(m_rx_buffer + m_rx_buffer_read_ptr) == 0x0A0D0A0D)
			{
				// ������ ����!
				SLOG_NET("net-tcp", "%s(%d) : ping received", m_name, get_ref_count());
				m_rx_buffer_read_ptr += 4;
				continue;
			}
			// ������ ��������� ������ PONG
			else if (*(uint16_t*)(m_rx_buffer + m_rx_buffer_read_ptr) == 0x0A0D)
			{
				// ������ �o��!
				SLOG_NET("net-tcp", "%s(%d) : pong received", m_name, get_ref_count());
				m_rx_buffer_read_ptr += 2;
				continue;
			}

			m_packet = raw_packet_t::get_free(false);

			if (m_packet == nullptr)
			{
				raise_parser_error();
				return;
			}

			m_packet->set_route(m_route);
		}

		if (m_tcp_read_headers)
		{
			tcp_read_sip_headers();
		}
		else
		{
			tcp_read_sip_body();
		}

		//���� ������ ������ �������� �� �������
		if (tmp_m_rx_buffer_read_ptr == m_rx_buffer_read_ptr)
			break;
	}
}
//--------------------------------------
//
//--------------------------------------
bool sip_transport_tcp_t::tcp_read_sip_headers()
{
	char* buffer = (char*)(m_rx_buffer + m_rx_buffer_read_ptr);
	char* term;
	int remain_size = m_rx_buffer_size - m_rx_buffer_read_ptr;

	// ������� �� ����� ��������
	while ((term = (char*)memchr(buffer, '\r', remain_size)) != nullptr && (term - buffer < remain_size - 1) && (*(term + 1) == '\n'))
	{
		if (term == buffer)
		{
			// ����� ������ ������
			remain_size -= 2;
			term += 2;
			m_rx_buffer_read_ptr += 2;

			//������� ��� ������ � ������ ������� - �� �� ��� ��������� ��������� ���������� ������, �� �� ��� ���-�����
			if (m_packet->get_packet_length() == 0)
			{
				// KEEP-ALIVE
				raw_packet_t::release(m_packet);
				m_packet = nullptr;
				return true;
			}

			// ��������� �����
			m_tcp_read_headers = false;

			int body_length = m_packet->parse_headers();

			if (body_length == 0)
			{
				// ����� ����� � ��������!
				raise_packet_read(m_packet);
				m_packet = nullptr;
				m_tcp_read_headers = true;

				return true;
			}
			else if (body_length > 0)
			{
				return tcp_read_sip_body();
			}

			// �������� ���� ������ ���� ����� ���� ��������� ������������� 8)

			SLOG_NET("net-tcp", "transport %s : parser error body length %d", m_name, body_length);

			raise_parser_error();

			raw_packet_t::release(m_packet);
			m_tcp_read_headers = true;
			m_packet = nullptr;
			m_rx_buffer_read_ptr = 0;
			m_rx_buffer_size = 0;

			return false;
		}
		else
		{
			// ������� ������
			term += 2;
			int append_size = PTR_DIFF(term, buffer);
			m_packet->add_line(buffer, append_size);
			buffer = term;
			m_rx_buffer_read_ptr += append_size;

			remain_size -= append_size;
		}
	}

	// �� �� ����� ��������� ������	-- ���� ��� ������
	return true;
}
//--------------------------------------
//
//--------------------------------------
bool sip_transport_tcp_t::tcp_read_sip_body()
{
	int written = m_packet->add_body(m_rx_buffer + m_rx_buffer_read_ptr, m_rx_buffer_size - m_rx_buffer_read_ptr);

	m_rx_buffer_read_ptr += written;

	if ((m_tcp_body_read += written) == m_packet->get_body_length())
	{
		// ����� ����� � ��������!
		raise_packet_read(m_packet);

		m_packet = nullptr;
		m_tcp_read_headers = true;
		m_tcp_body_read = 0;
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_tcp_t::ws_process_data()
{
	if (rtl::Logger::check_trace(TRF_FLAG1))
	{
		SLOG_NET_BINARY("net-tcp", m_rx_buffer, m_rx_buffer_size, "%s(%d) : READING WS FRAME", m_name, get_ref_count());
	}

	sip_transport_read_state_t read_state = sip_transport_read_continue_e;

	while (read_state == sip_transport_read_continue_e)
	{
		if (m_packet == nullptr)
		{
			m_packet = raw_packet_t::get_free(false);

			if (m_packet == nullptr)
			{
				raise_parser_error();
				return;
			}

			m_ws_packet_buffer = (uint8_t*)m_packet->get_buffer();
			m_ws_packet_written = 0;
			m_packet->set_route(m_route);
		}

		switch (m_ws_read_state)
		{
		case ws_read_header_e:
			read_state = ws_read_header();
			break;
		case ws_read_length_e:
			read_state = ws_read_length();
			break;
		case ws_read_mask_e:
			read_state = ws_read_mask();
			break;
		case ws_read_frame_e:
			read_state = ws_read_frame();
			break;
		}
	}

}
//--------------------------------------
//
//--------------------------------------
/*
short frame:
	 0                   1                   2                   3
	 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
	(0)             (1)             (2)             (3)
	+-+-+-+-+-------+-+-------------+-------------------------------+
	|F|R|R|R| opcode|M| Payload len | Masking-key, if MASK set to 1 |
	|I|S|S|S|  (4)  |A|     (7)     |                               |
	|N|V|V|V|       |S|             |                               |
	| |1|2|3|       |K|             |                               |
	+-+-+-+-+-------+-+-------------+-------------------------------+
	| Masking-key (continued)       |          Payload Data         |
	+-------------------------------+ - - - - - - - - - - - - - - - +
	:                     Payload Data continued ...                :
	+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +
	|                     Payload Data continued ...                |
	+---------------------------------------------------------------+

middle frame:
	 0                   1                   2                   3
	 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
	(0)             (1)             (2)             (3)
	+-+-+-+-+-------+-+-------------+-------------------------------+
	|F|R|R|R| opcode|M| Payload len |    Extended payload length    |
	|I|S|S|S|  (4)  |A|     (7)     |             (16/64)           |
	|N|V|V|V|       |S|             |   (if payload len==126/127)   |
	| |1|2|3|       |K|             |                               |
	+-+-+-+-+-------+-+-------------+-------------------------------+
4   |                  Masking-key, if MASK set to 1                |
	+---------------------------------------------------------------+
8	|                          Payload Data                         |
	+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +
	:                     Payload Data continued ...                :
	+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +
	|                     Payload Data continued ...                |
	+---------------------------------------------------------------+

long frame:
	 0                   1                   2                   3
	 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
	(0)             (1)             (2)             (3)
	+-+-+-+-+-------+-+-------------+-------------------------------+
0	|F|R|R|R| opcode|M| Payload len |    Extended payload length    |
	|I|S|S|S|  (4)  |A|     (7)     |             (16/64)           |
	|N|V|V|V|       |S|             |   (if payload len==126/127)   |
	| |1|2|3|       |K|             |                               |
	+-+-+-+-+-------+-+-------------+ - - - - - - - - - - - - - - - +
4	|     Extended payload length continued, if payload len == 127  |
	+ - - - - - - - - - - - - - - - +-------------------------------+
8	|                               |Masking-key, if MASK set to 1  |
	+-------------------------------+-------------------------------+
12	| Masking-key (continued)       |          Payload Data         |
	+-------------------------------- - - - - - - - - - - - - - - - +
16	:                     Payload Data continued ...                :
	+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +
	|                     Payload Data continued ...                |
	+---------------------------------------------------------------+
*/
sip_transport_read_state_t sip_transport_tcp_t::ws_read_header()
{
	if (m_rx_buffer_size - m_rx_buffer_read_ptr < (int)sizeof(ws_header_t))
	{
		return sip_transport_read_need_more_data_e;
	}

	m_ws_header = *(ws_header_t*)(m_rx_buffer + m_rx_buffer_read_ptr);
	m_ws_read_state = ws_read_length_e;
	
	m_rx_buffer_read_ptr += sizeof(ws_header_t);

	return sip_transport_read_continue_e;
}
//--------------------------------------
//
//--------------------------------------
sip_transport_read_state_t sip_transport_tcp_t::ws_read_length()
{
	if (m_ws_header.p_len_7 < 126) // ����� 2 �����
	{
		// ����� � ����� 1, ����� � ������ 2-5, ������ ���������� � ����� �6
		m_ws_payload_length = m_ws_header.p_len_7;
		m_ws_payload_offset = 6;
	}
	else if (m_ws_header.p_len_7 == 126)
	{
		// ����� � 3 � 4 ������, ����� �� 5 �����, ������ � 8 �����

		m_ws_payload_length_high = 0;
		m_ws_payload_length = ntohs(*(short*)(m_rx_buffer + m_rx_buffer_read_ptr));
		m_ws_payload_offset = 8;

		m_rx_buffer_read_ptr += 2;
	}
	else
	{
		// ����� � ������ 2-9, ����� �� 10 �����, ������ � 14 �����

		m_ws_payload_length_high = ntohl(*(uint32_t*)(m_rx_buffer + m_rx_buffer_read_ptr));
		m_ws_payload_length = ntohl(*(uint32_t*)(m_rx_buffer + m_rx_buffer_read_ptr + 4));
		m_ws_payload_offset = 14;

		m_rx_buffer_read_ptr += 8;
	}

	m_ws_read_state = ws_read_mask_e;

	return sip_transport_read_continue_e;
}
//--------------------------------------
//
//--------------------------------------
sip_transport_read_state_t sip_transport_tcp_t::ws_read_mask()
{
	if (!m_ws_header.f_mask)
	{
		m_ws_read_state = ws_read_frame_e;
		return sip_transport_read_continue_e;
	}

	if (m_rx_buffer_size - m_rx_buffer_read_ptr < 4)
	{
		return sip_transport_read_need_more_data_e;
	}

	memcpy(m_ws_payload_mask, m_rx_buffer + m_rx_buffer_read_ptr, 4);

	m_rx_buffer_read_ptr += 4;

	m_ws_read_state = ws_read_frame_e;

	return sip_transport_read_continue_e;
}
//--------------------------------------
//
//--------------------------------------
sip_transport_read_state_t sip_transport_tcp_t::ws_read_frame()
{
	if (m_ws_payload_length == 0)
	{
		// ��������� ������
		ws_process_frame();
		m_ws_read_state = ws_read_header_e;
		return sip_transport_read_continue_e;
	}

	// ���� ����� �� ������� �� ���������� ����������

	if ((int)m_ws_payload_length > m_rx_buffer_size - m_rx_buffer_read_ptr)
	{
		return sip_transport_read_need_more_data_e;
	}

	// ��������� ������

	//unmask_buffer_to(m_ws_packet_buffer + m_ws_packet_written, m_rx_buffer + m_rx_buffer_read_ptr, m_ws_payload_length, m_ws_payload_mask);

	memcpy(m_ws_packet_buffer + m_ws_packet_written, m_rx_buffer + m_rx_buffer_read_ptr, m_ws_payload_length);
	unmask_packet(m_ws_packet_buffer + m_ws_packet_written, m_ws_payload_length, m_ws_payload_mask);

	m_ws_packet_written += m_ws_payload_length;

	ws_process_frame();

	m_ws_read_state = ws_read_header_e;
	m_rx_buffer_read_ptr += m_ws_payload_length;

	return sip_transport_read_continue_e;
}
//--------------------------------------
//
//--------------------------------------
bool sip_transport_tcp_t::ws_process_frame()
{
	switch (m_ws_header.opcode)
	{
	case ws_opcode_frame_continue:
		break;
	case ws_opcode_frame_text:
	case ws_opcode_frame_binary:
		// ���� ���� ������� ��������� �������� �� ������������ �� ��� ������
		if (m_ws_header.f_fin)
			ws_process_sip_packet();
		break;
	case ws_opcode_ctrl_close:
		ws_process_close();
		break;
	case ws_opcode_ctrl_ping:
		ws_process_ping();
		break;
	case ws_opcode_ctrl_pong:
		ws_process_pong();
		break;
	}

	return true;
}
//--------------------------------------
// ���������� �������� ������ � ������� �������� ��� ������
// ���� ��� ��������� ��������� �������. ���� ��� ����� �� ������ �� ���� ����������
//--------------------------------------
bool sip_transport_tcp_t::ws_process_sip_packet()
{
	SLOG_NET("net-tcp", "%s(%d) : WS SIP PACKET RECEIVED %d bytes", m_name, get_ref_count(), m_ws_packet_written);

	// ��������� �����
	if (m_packet->parse(m_ws_payload_length))
	{
		raise_packet_read(m_packet);

		m_packet = nullptr;

		m_ws_packet_buffer = nullptr;
		m_ws_packet_written = 0;

		return true;
	}

	raise_parser_error();
		
	m_ws_packet_buffer = nullptr;
	m_ws_packet_written = 0;
	
	return false;
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_tcp_t::ws_process_handshake_request(raw_packet_t* packet)
{
	SLOG_NET("net-tcp", "%s(%d) : process handshake request...", m_name, get_ref_count());
	const char* key = check_handshake_request(packet);

	if (key != nullptr && key[0] != 0)
	{
		m_ws_proto = true;
		sip_transport_type_t new_type = m_route.type == sip_transport_tcp_e ? sip_transport_ws_e : sip_transport_wss_e;
		SLOG_NET("net-tcp", "%s(%d) : change transport type to %s...", m_name, get_ref_count(), TransportType_toString(new_type));
		sip_transport_manager_t::get_pool().reset_transport_type(this, new_type);
		update_name(TransportType_toString(m_route.type));
		SLOG_NET("net-tcp", "%s(%d) : send handshake response...", m_name, get_ref_count());
		ws_send_handshake_accept(key);
		SLOG_NET("net-tcp", "%s(%d) : handshake response sent", m_name, get_ref_count());
	}
	else
	{
		SLOG_NET("net-tcp", "%s(%d) : send handshake reject", m_name, get_ref_count());
		
		ws_send_handshake_reject();

		disconnect();
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_tcp_t::ws_send_handshake_accept(const char* key)
{
	const char* response_template =
		"HTTP/1.1 101 Switching Protocols\r\n\
Upgrade: websocket\r\n\
Connection: Upgrade\r\n\
Sec-WebSocket-Accept: %s\r\n\
Sec-WebSocket-Protocol: sip\r\n\r\n";

	char buffer[256];
	uint8_t accept[20];
	char response[1024];

	// ������� ������
	int len = std_snprintf(buffer, 256, "%s258EAFA5-E914-47DA-95CA-C5AB0DC85B11", key);
	buffer[len] = 0;

	// ����������� �������������
	sha1_context_t sha1;
	memset(&sha1, 0, sizeof sha1);

	sha1.reset();
	sha1.input((const uint8_t*)buffer, (int)strlen(buffer));
	sha1.result(accept);

	Base64Encode(accept, 20, buffer, 256);

	len = std_snprintf(response, 1024, response_template, buffer);
	response[len] = 0;

	SLOG_NET("net-tcp", "%s(%d) : WS SEND RESPONSE\n%s", m_name, get_ref_count(), response);

	int res = write_socket(response, (int)strlen(response));

	if (res >= 0)
	{
		SLOG_NET("net-tcp", "%s(%d) : WS handshake response sent", m_name, get_ref_count());
	}
	else if (res < 0)
	{
		SLOG_ERR_MSG("net-tcp", std_get_error, "%s(%d) : WS handshake response not sent", m_name, get_ref_count());
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_tcp_t::ws_send_handshake_reject()
{
	const char* response =
		"HTTP/1.1 400 Bad Request\r\n\
Upgrade: websocket\r\n\
Connection: Upgrade\r\n\
Sec-WebSocket-Protocol: sip\r\n\r\n";

	SLOG_NET("net-tcp", "%s(%d) : WS SEND REJECT\n%s", m_name, get_ref_count(), response);

	int res = write_socket(response, (int)strlen(response));

	if (res >= 0)
	{	SLOG_NET("net-tcp", "%s(%d) : WS handshake reject sent", m_name, get_ref_count());
	}
	else if (res < 0)
	{
		SLOG_ERR_MSG("net-tcp", std_get_error, "%s(%d) : WS handshake reject not sent", m_name, get_ref_count());
	}
}
//--------------------------------------
// ��������� ������ �� �������� ����������
//--------------------------------------
bool sip_transport_tcp_t::ws_process_close()
{
	if (!m_ws_close_sent)
	{
		uint16_t code = m_ws_payload_length > 0 ? ntohs(*(uint16_t*)(m_rx_buffer + m_ws_payload_offset)) : 0;
		SLOG_NET("net-tcp", "%s(%d) : WS CLOSE RECEIVED reason %u %s -> SEND CLOSE REPLY", m_name, get_ref_count(), code, m_ws_payload_length > 2 ? (char*)(m_rx_buffer + m_ws_payload_offset + 2) : "(empty)");

		m_ws_close_sent = true;

		ws_send_frame(ws_opcode_ctrl_close, m_rx_buffer + m_ws_payload_offset, m_ws_payload_length);

		disconnect();
	}

	return true;
}
//--------------------------------------
// ��������� �����
//--------------------------------------
bool sip_transport_tcp_t::ws_process_ping()
{
	SLOG_NET("net-tcp", "%s(%d) : WS PING RECEIVED -> SEND PONG", m_name, get_ref_count());
	ws_send_frame(ws_opcode_ctrl_pong, m_rx_buffer + m_ws_payload_offset, m_ws_payload_length);
	return true;
}
//--------------------------------------
// ��������� �����
//--------------------------------------
bool sip_transport_tcp_t::ws_process_pong()
{
	SLOG_NET("net-tcp", "%s(%d) : WS PONG RECEIVED", m_name, get_ref_count());
	return true;
}
//--------------------------------------
// �������� ��� ���������
//--------------------------------------
int sip_transport_tcp_t::ws_send_frame(ws_opcode_e op_code, const void* packet, int packet_length)
{
	SLOG_NET("net-tcp", "%s(%d) : SENDING WS SIP MESSAGE %d bytes", m_name, get_ref_count(), packet_length);

	// ����� ����� ������������� ��������, ��� ��� �������� ������ ���� � ��� ������
	uint8_t ws_header[14];
	int header_length = 0;
	memset(ws_header, 0, sizeof ws_header);

	ws_header[0] = 0x80;		// �� ������ ���������� ����� ���������
	ws_header[0] |= op_code;	// opcode
	if (packet_length < 126)
	{
		ws_header[1] = packet_length;
		header_length = 2;
	}
	else if (packet_length < 65536)
	{
		ws_header[1] = 126;
		ws_header[2] = uint8_t((packet_length & 0x0000ff00) >> 8);
		ws_header[3] = uint8_t(packet_length & 0x000000ff);
		header_length = 4;
	}
	else if (packet_length >= 65536)
	{
		ws_header[1] = 127;
		// ws_packet[2,3,4,5] = 0;
		ws_header[6] = uint8_t((packet_length & 0xff000000) >> 24);
		ws_header[7] = uint8_t((packet_length & 0x00ff0000) >> 16);
		ws_header[8] = uint8_t((packet_length & 0x0000ff00) >> 8);
		ws_header[9] = uint8_t(packet_length & 0x000000ff);
		header_length = 10;
	}

	if (rtl::Logger::check_trace(TRF_FLAG1))
	{
		SLOG_NET_BINARY("net-tcp", ws_header, header_length, "%s(%d) : SENDING WS FRAME HEADER", m_name, get_ref_count());
	}

	int result = write_socket(ws_header, header_length);

	if (result > 0 && packet_length > 0)
	{
		if (rtl::Logger::check_trace(TRF_FLAG1))
		{
			SLOG_NET_BINARY("net-tcp", (uint8_t*)packet, packet_length, "%s(%d) : SENDING WS FRAME", m_name, get_ref_count());
		}

		int frame_res = write_socket(packet, packet_length);

		if (frame_res > 0)
			result += frame_res;
		else
			result = frame_res;
	}

	return result;
}
//--------------------------------------
// �������� ��� ��������� ����� ��� �����
//--------------------------------------
int sip_transport_tcp_t::ws_send_message(const sip_message_t& message)
{
	if (m_socket.get_handle() == INVALID_SOCKET)
	{
		SLOG_ERROR("net-tcp", "%s(%d) : has no socket!", m_name, get_ref_count());
		return 0;
	}

	rtl::String msg_string = message.to_string();
	const char* packet = (const char*)msg_string;
	int packet_len = msg_string.getLength();
	// ���������� � ��� ��������
	if (rtl::Logger::check_trace(TRF_NET))
	{
		uint8_t* a = (uint8_t*)&m_route.remoteAddress;
		uint8_t* b = (uint8_t*)&m_route.if_address;

		SLog.log("net-tcp", "%s(%d) : SENDING PACKET(len: %d) IFACE %d.%d.%d.%d:%u TO %d.%d.%d.%d:%u",
			m_name, get_ref_count(), packet_len,
			a[0], a[1], a[2], a[3], m_route.if_port,
			b[0], b[1], b[2], b[3], m_route.remotePort);
	}

	int result = ws_send_frame(ws_opcode_frame_text, (const uint8_t*)packet, packet_len);

	if (result >= 0)
	{
		SLOG_NET("net-tcp", "%s(%d) : PACKET SENT (%d bytes)", m_name, get_ref_count(), result);

		if (rtl::Logger::check_trace(TRF_PROTO) && !message.IsKeepAlive())
		{
			uint8_t* a = (uint8_t*)&m_route.remoteAddress;
			uint8_t* b = (uint8_t*)&m_route.if_address;

			TransLog.log("SEND", "%s %d Bytes TO %d.%d.%d.%d:%u IFACE %d.%d.%d.%d:%u\r\n%s",
				TransportType_toString(m_route.type), packet_len,
				a[0], a[1], a[2], a[3], m_route.remotePort, b[0], b[1], b[2], b[3], m_route.if_port,
				packet);
		}

		raise_packet_sent(message);
	}
	else //result == SOCKET_ERROR)
	{
		int net_error = std_get_error;
		SLOG_ERR_MSG("net-tcp", net_error, "%s(%d) : PACKET NOT SENT!", m_name, get_ref_count());
		
		raise_send_error(message, net_error);
	}

	return result;
}
//--------------------------------------
//
//--------------------------------------
int sip_transport_tcp_t::write_tcp_socket(const void* data, int length)
{
	int res;

	if ((res = m_socket.send(data, length)) < 0)
	{
		m_socket.close();
	}

	return res;
}
//--------------------------------------
//
//--------------------------------------
int sip_transport_tcp_t::write_tls_socket(const void* data, int length)
{
	int result = 0;

	SLOG_NET("tls-net", "%s(%d) : (type %s) sending TLS frame size %d bytes", m_name, get_ref_count(), TransportType_toString(m_route.type), length);

	if (m_route.type == sip_transport_tls_e || m_route.type == sip_transport_wss_e)
	{
		m_tls.tx_push((const char*)data, length);

		int tx_size = m_tls.tx_pull((char*)m_tls_tx_buffer, MAX_TLS_FRAME);

		if (rtl::Logger::check_trace(TRF_FLAG3))
		{
			SLOG_BINARY("tls-net", m_tls_tx_buffer, tx_size, "%s(%d) : sending encoded data len:%d", m_name, get_ref_count(), tx_size);
		}

		result = m_socket.send(m_tls_tx_buffer, tx_size);
	}

	return result;
}
//--------------------------------------
//
//--------------------------------------
int sip_transport_tcp_t::read_tcp_socket(uint8_t* buffer, int size, int& written)
{
	int amount;
	written = m_socket.io_control(FIONREAD, (uint32_t*)&amount);

	if (written == SOCKET_ERROR)
	{
		signal_transport_failure(written, "ioctlsocket");
		return -1;
	}

	if (amount == 0)
	{
		signal_transport_failure(written = 0, "ioctlsocket");
		return -1;
	}

	SLOG_NET("net-tcp", "%s(%d) : READING DATA %d bytes. rx_ptr:%d rx_size:%d", m_name, get_ref_count(), amount, m_rx_buffer_read_ptr, m_rx_buffer_size);

	written = m_socket.receive(buffer, MIN(amount, SIP_PACKET_MAX));

	return written > 0 ? 1 : -1;
}
//--------------------------------------
// ����� ���������� �� ������ ����� �� � �������� ���������
// 0 - �� ����� ������! � ���������� ������!
//--------------------------------------
int sip_transport_tcp_t::read_tls_socket(uint8_t* buffer, int size, int& written)
{
	int amount;
	written = m_socket.io_control(FIONREAD, (uint32_t*)&amount);

	if (written == SOCKET_ERROR)
	{
		signal_transport_failure(written, "ioctlsocket");
		return -1;
	}

	if (amount == 0)
	{
		signal_transport_failure(written = 0, "ioctlsocket");
		return -1;
	}

	SLOG_NET("tls-net", "%s(%d) : READING DATA %d bytes. tls_rx_len:%d", m_name, get_ref_count(), amount, m_tls_rx_buffer_length);

	int to_read = MIN(amount, MAX_TLS_FRAME);

	int socket_result = m_socket.receive(m_tls_rx_buffer + m_tls_rx_buffer_length, to_read, 0);

	if (socket_result <= 0)
	{
		written = socket_result;
		return -1;
	}

	m_tls_rx_buffer_length += socket_result;

	written = 0;

	int result = tls_process_incoming_data(buffer, size, written);

	if (result == m_tls_rx_buffer_length)
	{
		m_tls_rx_buffer_length = 0;
	}
	else if (result > 0)
	{
		int to_move = m_tls_rx_buffer_length - result;
		SLOG_NET("tls-net", "%s(%d) : MOVE UNREAD DATA. tls_read:%d, tls_rx_len:%d, tls_rx_move:%d", m_name, get_ref_count(), result, m_tls_rx_buffer_length, to_move);
		memmove(m_tls_rx_buffer, m_tls_rx_buffer + result, to_move);
		m_tls_rx_buffer_length = to_move;
	}

	return result > 0;
}
//--------------------------------------
// ��������� ������ ����� � (m_tls_rx_buffer + m_tls_rx_buffer_size) ������ read_size
//--------------------------------------
int sip_transport_tcp_t::tls_process_incoming_data(uint8_t* buffer, int size, int& written)
{
	tls_message_header_t* tls_header = (tls_message_header_t*)(m_tls_rx_buffer);

	if (m_tls_initial_packet && !m_tls.is_handshake_done())
	{

		tls_handshake_header_t* tls_handshake = (tls_handshake_header_t*)(m_tls_rx_buffer + sizeof(tls_message_header_t));
		tls_clent_hello_header_t* tls_client_hello = (tls_clent_hello_header_t*)(m_tls_rx_buffer + sizeof(tls_message_header_t) + sizeof(tls_handshake_header_t));

		SLOG_NET("tls-net", "%s(%d) : message header: type(%x) ver(%x) length(%d)", m_name, get_ref_count(), tls_header->type, ntohs(tls_header->version), ntohs(tls_header->length));
		uint32_t handshake_length = tls_handshake->length[2] + ((uint16_t)tls_handshake->length[1] << 8) + ((uint32_t)tls_handshake->length[0] << 16);
		SLOG_NET("tls-net", "%s(%d) : handshake header: type(%x) length(%u)", m_name, get_ref_count(), tls_handshake->type, handshake_length);
		SLOG_NET("tls-net", "%s(%d) : client hello header: ver(%x)", m_name, get_ref_count(), ntohs(tls_client_hello->ver));

		if (!m_tls.init((tls_version_t)tls_client_hello->ver)) // (tls_version_t)*((uint16_t*)(m_rx_buffer + 1))
		{
			signal_transport_failure(0, "tls_init");
			return -1;
		}

		m_tls_initial_packet = false;
	}

	int frame_length = int(ntohs(tls_header->length) + sizeof(tls_message_header_t));

	// ������ ������������ �������
	if (rtl::Logger::check_trace(TRF_FLAG3))
	{
		SLOG_BINARY("tls-net", m_tls_rx_buffer, m_tls_rx_buffer_length, "%s(%d) : incoming tls buffer size (%d bytes) --> frame length %d",
			m_name, get_ref_count(), m_tls_rx_buffer_length, frame_length);
	}

	int pushed = m_tls.rx_push((const char*)m_tls_rx_buffer, m_tls_rx_buffer_length);

	if (!m_tls.is_handshake_done())
	{
		int amount = m_tls.rx_pull((char*)m_tls_tx_buffer, MAX_TLS_FRAME);

		if (amount < 0)
		{
			SLOG_ERROR("tls-net", "%s(%d) : handshake rx_pull() failed with error %d %s", m_name, get_ref_count(), m_tls.get_state(), m_tls.get_state_string_long());
			signal_transport_failure(0, "tls:rx_pull");
			return -1;
		}

		if (rtl::Logger::check_trace(TRF_FLAG3))
		{
			SLOG_BINARY("tls-net", m_tls_tx_buffer, amount, "%s(%d) : sending server handshake packet (%d bytes)", m_name, get_ref_count(), amount);
		}

		int res = m_socket.send(m_tls_tx_buffer, amount);
		if (res == SOCKET_ERROR)
		{
			signal_transport_failure(res, "send");
			return -1;
		}

		return pushed;
	}

	int read = 0;

	do
	{
		read = m_tls.rx_pull((char*)buffer + written, size - written);

		if (read < 0)
		{
			SLOG_ERROR("tls-net", "%s(%d) : rx_pull() -> %d:%d  failed with error %d %s", m_name, get_ref_count(), read, written, m_tls.get_state(), m_tls.get_state_string_long());
			signal_transport_failure(0, "tls:rx_pull");
			return -1;
		}

		written += read;
	}
	while (read > 0);
	
	return pushed;
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_tcp_t::raise_packet_read(raw_packet_t* packet)
{
	if (!packet->is_valid())
	{
		SLOG_WARN("net-tcp", "transport %s : invalid packet received", m_name);

		raw_packet_t::release(packet);

		disconnect();

		return;
	}

	// check_for web_socket handshake
	if (m_wait_first_packet_in && std_stricmp(packet->get_start_line().get_version(), "HTTP/1.1") == 0)
	{
		/*capture();
		m_release_on_ws_connect = true;*/

		SLOG_NET("net-tcp", "%s(%d) : it is web socket handshake", m_name, get_ref_count());
		rtl::async_call_manager_t::callAsync(this, ASYNC_PROCESS_HANDSHAKE, 0, packet);
	}
	else
	{
		packet->set_route(m_route);
		sip_transport_t::raise_packet_read(packet);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_tcp_t::async_handle_call(uint16_t command, uintptr_t int_param, void* ptr_param)
{
	if (command == ASYNC_PROCESS_HANDSHAKE && ptr_param != 0)
	{
		raw_packet_t* packet = (raw_packet_t*)ptr_param;

		SLOG_NET("net-tcp", "%s(%d) : web socket handshake starts...", m_name, get_ref_count());
		// handshake starts!
		ws_process_handshake_request(packet);
		raw_packet_t::release(packet);
	}
	else
	{
		sip_transport_t::async_handle_call(command, int_param, ptr_param);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_tcp_t::signal_transport_failure(int socket_result, const char* func)
{
	if (socket_result == 0)
	{
		SLOG_NET("net-tcp", "%s(%d) : %s() read stop flag (%d) or network level error ", m_name, get_ref_count(), func, socket_result);

		m_readable = false;
		if (m_socket.get_state() != net_socket_state_t::net_shutdown_send)
		{
			disconnect();
		}
		
		raise_stopped();
	}
	else
	{
		int net_error = std_get_error;

		SLOG_ERR_MSG("net-tcp", net_error, "%s(%d) : %s() returns socket error %d", m_name, get_ref_count(), func, net_error);

		raise_stopped();

		m_socket.close();
	}

	raise_recv_error(socket_result);

	if (m_packet != nullptr)
	{
		raw_packet_t::release(m_packet);
		m_packet = nullptr;
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_tcp_t::disconnect()
{
	if (m_state != sip_transport_tcp_state_connected)
		return;

	SLOG_NET("net-tcp", "%s(%d) : disconnect transport", m_name, get_ref_count());

	stop_keep_alive();

	stop_reading();

	if (m_socket.get_handle() != INVALID_SOCKET)
	{
		if (m_socket.get_state() != net_socket_state_t::net_shutdown_send)
		{
			SLOG_NET("net-tcp", "%s(%d) : send shutdown", m_name, get_ref_count());
			m_socket.shutdown(SD_SEND);

			release_("sip_transport_tcp_t::disconnect");
		}
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_tcp_t::send_keep_alive()
{
	int result;

	rtl::MutexLock lock(m_sender_sync);

	if (m_ws_proto)
	{
		result = ws_send_frame(ws_opcode_frame_text, "\r\n\r\n", 4);
	}
	else
	{
		if (m_socket.get_handle() == INVALID_SOCKET)
		{
			SLOG_ERROR("net", "transport %s has no socket!", m_name, get_ref_count());
			return;
		}

		if (rtl::Logger::check_trace(TRF_NET))
		{
			uint8_t* b0 = (uint8_t*)&m_route.remoteAddress;
			uint8_t* b1 = (uint8_t*)&m_route.if_address;

			SLog.log("net-udp", "%s(%d) : SENDING KEEP-ALIVE IFACE %d.%d.%d.%d:%u TO %d.%d.%d.%d:%u",
				m_name, get_ref_count(),
				b1[0], b1[1], b1[2], b1[3], m_route.if_port,
				b0[0], b0[1], b0[2], b0[3], m_route.remotePort);
		}

		const char* packet = "\r\n\r\n";
		int packet_len = 4;

		result = write_socket((const uint8_t*)packet, packet_len);

		if (result >= 0)
		{
			SLOG_NET("net", "%s(%d) : PACKET SENT (%d bytes)", m_name, get_ref_count(), result);
		}
		else //result == SOCKET_ERROR)
		{
			int net_error = std_get_error;
			SLOG_ERR_MSG("net", net_error, "%s(%d) : PACKET NOT SENT!", m_name, get_ref_count());
		}
	}
}
//--------------------------------------
//
//--------------------------------------
bool sip_transport_tcp_t::connect()
{
	/*
		! ���� ��������� ��� ���������� �� ����� ���������� ����������
		! ��� ��� ������������ �� ����� � ����� ��������� ��������� ��������� (������� �� ��������� ��� ������ ��� ��������� ����������)
	*/

	SLOG_NET("net-tcp", "%s(%d) : connecting to (state:%d, m_type:%d)", m_name, get_ref_count(), m_state, m_type);

	if (m_state == sip_transport_tcp_state_connected)
	{
		return true;
	}

	if (m_type == sip_transport_tcp_server || m_state != sip_transport_tcp_state_idle)
	{
		SLOG_WARN("net-tcp", "%s(%d) : transport is server or state not idle(state:%d)", m_name, get_ref_count(), m_state);
		return true;
	}

	rtl::MutexLock lock(m_sender_sync);

	m_state = sip_transport_tcp_state_connecting;

	// �������
	if (m_socket.get_handle() != INVALID_SOCKET)
	{
		SLOG_WARN("net-tcp", "%s(%d) : socket already opened (state:%d)?", m_name, get_ref_count(), m_state);
		return true;
	}

	if (!m_socket.open(AF_INET, SOCK_STREAM, IPPROTO_TCP))
	{
		SLOG_ERR_MSG("net-tcp", std_get_error, "%s(%d) : socket() function failed", m_name, get_ref_count());
		return false;
	}

	// try to bind
	if (m_route.if_address.s_addr != INADDR_ANY || m_route.if_port != 0)
	{
		try_to_bind();
	}

	SLOG_NET("net-tcp", "%s(%d) : connecting to %s:%u...(state:%d, m_type:%d)", m_name, get_ref_count(), inet_ntoa(m_route.remoteAddress), m_route.remotePort, m_state, m_type);

	sockaddr_in addr;
	memset(&addr, 0, sizeof addr);
	addr.sin_addr = m_route.remoteAddress;
	addr.sin_port = htons(m_route.remotePort);
	addr.sin_family = AF_INET;

	//bool result = false;
	int rx_result = m_socket.connect((sockaddr*)&addr, sizeof addr);

	if (rx_result == 0)
	{
		m_state = sip_transport_tcp_state_connected;
		m_socket.set_tag(this);

		char if_addr_buf[32];
		char r_addr_buf[32];

		SLOG_NET("net-tcp", "%s(%d) : connect on iface %s:%u raddr %s:%u...", m_name, get_ref_count(),
			net_to_string(if_addr_buf, 32, m_route.if_address), m_route.if_port,
			net_to_string(r_addr_buf, 32, m_route.remoteAddress), m_route.remotePort);

		if (m_route.type != sip_transport_tls_e  || tls_connect())
		{
			initialize();
			capture_("sip_transport_tcp_t::connect");
			start_keep_alive();

			sip_transport_manager_t::start_reading(this);

			return true;
		}
		
		SLOG_ERROR("net-tcp", "%s(%d) : tls_connect() failed", m_name, get_ref_count());
	}
	else
	{
		int last_error = std_get_error;
		SLOG_ERR_MSG("net-tcp", last_error, "%s(%d) : connect() failed", m_name, get_ref_count());
	}
	
	m_socket.close();
	
	m_state = sip_transport_tcp_state_closed;

	raise_stopped();

	return false;
}
//--------------------------------------
//
//--------------------------------------
bool sip_transport_tcp_t::try_to_bind()
{
	sockaddr_in addr;
	memset(&addr, 0, sizeof addr);
	addr.sin_addr = m_route.if_address;
	addr.sin_port = htons(m_route.if_port);
	addr.sin_family = AF_INET;
	//bool result = false;

	return m_socket.bind((sockaddr*)&addr, sizeof(addr)) == 0;
}
//--------------------------------------
//
//--------------------------------------
bool sip_transport_tcp_t::tls_connect()
{
	SLOG_NET("tls-net", "%s(%d) : start tls client handshake...", m_name, get_ref_count());

	// TLS client hello
	m_tls.init(TLSv1_2);

	m_tls_initial_packet = false;

	if (m_tls_tx_buffer == nullptr)
	{
		m_tls_tx_buffer = (uint8_t*)MALLOC(MAX_TLS_FRAME);
	}

	int rx_result = 0, tx_result = 0;
	int tx_size = m_tls.tx_push((const char*)m_rx_buffer, 0);

	if (tx_size <= 0)
	{
		SLOG_WARN("net", "%s(%d) : initial handshake %d! '%s'", m_name, get_ref_count(), tx_size, m_tls.get_state_string_long());
		//m_socket.close();
		//return false;
	}

	do
	{
		tx_size = m_tls.tx_pull((char*)m_tls_tx_buffer, MAX_TLS_FRAME);

		if (tx_size > 0)
		{
			if (rtl::Logger::check_trace(TRF_FLAG3))
			{
				SLOG_BINARY("tls-net", m_tls_tx_buffer, tx_size, "%s(%d) : sending handshake len:%d", m_name, get_ref_count(), tx_size);
			}

			if ((tx_result = m_socket.send(m_tls_tx_buffer, tx_size)) == SOCKET_ERROR)
			{
				int net_error = std_get_error;
				SLOG_ERR_MSG("tls-net", net_error, "%s(%d) : handshake NOT SENT!", m_name, get_ref_count());
				m_socket.close();
				return false;
			}
		}
		else
		{
			SLOG_WARN("tls-net", "%s(%d) : preparing handshake failed! '%s'", m_name, get_ref_count(), m_tls.get_state_string_long());
			//m_socket.close();
			//return false;
		}
		
		if (m_tls.is_handshake_done() || tx_size == 0)
		{
			SLOG_NET("tls-net", "%s(%d) : break handshake! done(%s) or tx_size == %d", m_name, get_ref_count(), STR_BOOL(m_tls.is_handshake_done()), tx_size);
			break;
		}

		rtl::ArrayT<net_socket_t*> rd_list;
		rd_list.add(&m_socket);

		if ((rx_result = net_socket_t::select(rd_list, 1000)) == SOCKET_ERROR || rx_result == 0)
		{
			SLOG_ERROR("tls-net", "%s(%d) : handshake select failed!", m_name, get_ref_count());
			m_socket.close();
			return false;
		}

		int amount;
		if ((rx_result = m_socket.io_control(FIONREAD, (uint32_t*)&amount)) == SOCKET_ERROR)
		{
			SLOG_ERROR("tls-net", "%s(%d) : handshake ioctl failed!'", m_name, get_ref_count());
			m_socket.close();
			return false;
		}

		// ����� ������!
		if ((rx_result = m_socket.receive(m_rx_buffer, amount)) == SOCKET_ERROR)
		{
			SLOG_ERROR("tls-net", "%s(%d) : handshake recv failed!'", m_name, get_ref_count());
			m_socket.close();
			return false;
		}

		if (rtl::Logger::check_trace(TRF_FLAG3))
		{
			SLOG_NET("tls-net", "%s(%d) : handshake reply received %d bytes!", m_name, get_ref_count(), rx_result);
		}

		rx_result = m_tls.tx_push((const char*)m_rx_buffer, rx_result);
	}
	while (!m_tls.is_handshake_done());

	SLOG_NET("tls-net", "%s(%d) : handshake %s!", m_name, get_ref_count(), m_tls.is_handshake_done() ? L"done" : L"failed");

	return m_tls.is_handshake_done();
}
//--------------------------------------
//
//--------------------------------------
int sip_transport_tcp_t::send_message(const sip_message_t& message)
{
	int result;
	
	rtl::MutexLock lock(m_sender_sync);

	if (m_state != sip_transport_tcp_state_connected)
	{
		SLOG_ERROR("net-tcp", "%s(%d) : has no connected socket!", m_name, get_ref_count());
		return 0;
	}

	m_last_activity = rtl::DateTime::getTicks();

	if (m_ws_proto)
	{
		result = ws_send_message(message);
	}
	else
	{
		rtl::String msg_string = message.to_string();
		const char* packet = (const char*)msg_string;
		int packet_len = msg_string.getLength();

		if (message.IsKeepAlive())
		{
			if (rtl::Logger::check_trace(TRF_NET))
			{
				uint8_t* b0 = (uint8_t*)&m_route.remoteAddress;
				uint8_t* b1 = (uint8_t*)&m_route.if_address;

				SLog.log("net-tcp", "%s(%d) : SENDING KEEP-ALIVE IFACE %d.%d.%d.%d:%u TO %d.%d.%d.%d:%u",
					m_name, get_ref_count(),
					b1[0], b1[1], b1[2], b1[3], m_route.if_port,
					b0[0], b0[1], b0[2], b0[3], m_route.remotePort);
			}

			packet = "\r\n\r\n";
			packet_len = 4;
		}
		else
		{
			// ���������� � ��� ��������
			if (rtl::Logger::check_trace(TRF_NET))
			{
				uint8_t* b = (uint8_t*)&m_route.remoteAddress;
				uint8_t* a = (uint8_t*)&m_route.if_address;

				SLog.log("net-tcp", "%s(%d) : SENDING PACKET(len: %d) IFACE %d.%d.%d.%d:%u TO %d.%d.%d.%d:%u",
					m_name, get_ref_count(), packet_len,
					a[0], a[1], a[2], a[3], m_route.if_port,
					b[0], b[1], b[2], b[3], m_route.remotePort);
			}
		}

		result = write_socket((const uint8_t*)packet, packet_len);

		if (result >= 0)
		{
			SLOG_NET("net-tcp", "%s(%d) : PACKET SENT (%d bytes)", m_name, get_ref_count(), result);

			if (rtl::Logger::check_trace(TRF_PROTO) && !message.IsKeepAlive())
			{
				uint8_t* a = (uint8_t*)&m_route.remoteAddress;
				uint8_t* b = (uint8_t*)&m_route.if_address;

				TransLog.log("SEND", "%s %d Bytes TO %d.%d.%d.%d:%u IFACE %d.%d.%d.%d:%u\r\n%s",
					TransportType_toString(m_route.type), packet_len,
					a[0], a[1], a[2], a[3], m_route.remotePort, b[0], b[1], b[2], b[3], m_route.if_port,
					packet);
			}

			raise_packet_sent(message);
		}
		else //result == SOCKET_ERROR)
		{
			int net_error = std_get_error;
			SLOG_ERR_MSG("net-tcp", net_error, "%s(%d) : PACKET NOT SENT!", m_name, get_ref_count());

			raise_send_error(message, net_error);
		}
	}

	return result;
}
//--------------------------------------
//
//--------------------------------------
bool sip_transport_tcp_t::is_valid()
{
	return m_tcp_socket != INVALID_SOCKET && m_socket.get_state() == net_shutdown_closed;
}
//--------------------------------------
// ������������� ������ ���������
//--------------------------------------
static void unmask_packet(uint8_t* packet, int packet_length, const uint8_t* mask)
{
	for (int i = 0; i < packet_length; i++)
	{
		packet[i] ^= mask[i % 4];
	}
}
//--------------------------------------
// ������������� ������ ���������
//--------------------------------------
/*static void unmask_buffer_to(uint8_t* dst, const uint8_t* src, int length, uint32_t mask)
{
	const uint8_t* bmask = (const uint8_t*)&mask;
	for (int i = 0; i < length; i++)
	{
		dst[i] = src[i] ^ bmask[i % 4];
	}
}*/
//--------------------------------------
//
//--------------------------------------
static const char* check_handshake_request(raw_packet_t* packet)
{
	SLOG_NET("net-tcp", "ws transport : validating handshake request");

	/*
	GET / HTTP/1.1					// �������� ������ ������
	Host: sip-ws.example.com		// ����������
	Upgrade: websocket				// ���������
	Connection: Upgrade			// ���������
	Sec-WebSocket-Key: dGhlIHNhbXBsZSBub25jZQ==	// ��������� � ����������
	Origin: http://www.example.com	// ����������
	Sec-WebSocket-Protocol: sip	// ���������
	Sec-WebSocket-Version: 13		// ���������
	*/

	const raw_header_t* header = packet->get_header("Upgrade");
	if (header == nullptr || std_stricmp(header->value, "websocket") != 0)
	{
		SLOG_NET_WARN("net-tcp", "ws transport : handshake request has no 'Upgrade' field or it is not 'websocket'");
		return nullptr;
	}

	header = packet->get_header("Connection");
	if (header == nullptr || strstr(header->value, "Upgrade") == 0)
	{
		SLOG_NET_WARN("net-tcp", "ws transport : handshake request has no 'Connection' field or it is not 'Upgrade'");
		return nullptr;
	}

	header = packet->get_header("Sec-WebSocket-Protocol");
	if (header == nullptr || std_stricmp(header->value, "sip") != 0)
	{
		SLOG_NET_WARN("net-tcp", "ws transport : handshake request has no 'Sec-WebSocket-Protocol' field or it is not 'sip'");
		return nullptr;
	}

	header = packet->get_header("Sec-WebSocket-Version");
	if (header == nullptr || std_stricmp(header->value, "13") != 0)
	{
		SLOG_NET_WARN("net-tcp", "ws transport : handshake request has no 'Sec-WebSocket-Version' field or it is not '13'");
		return nullptr;
	}

	header = packet->get_header("Sec-WebSocket-Key");
	if (header == nullptr)
	{
		SLOG_NET_WARN("net-tcp", "ws transport : handshake request has no 'Sec-WebSocket-Key' field");
		return nullptr;
	}

	SLOG_NET("net-tcp", "ws transport : handshake request is valid : 'Sec-WebSocket-key' value '%s'", header->value);

	return header->value;
}
//--------------------------------------
