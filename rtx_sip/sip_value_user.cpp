/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_value_user.h"
//--------------------------------------
//
//--------------------------------------
rtl::String& sip_value_user_t::prepare(rtl::String& stream) const
{
	sip_value_uri_t::prepare(stream);

	if (!m_tag.isEmpty())
		stream << ";tag=" << m_tag;

	return stream;
}
//--------------------------------------
//
//--------------------------------------
bool sip_value_user_t::parse()
{
	sip_value_uri_t::parse();

	bool b;
	const rtl::String& tag = m_params.get("tag", b);

	if (b)
	{
		m_tag = tag;
		m_params.remove("tag");
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
sip_value_user_t::~sip_value_user_t()
{
	cleanup();
}
//--------------------------------------
//
//--------------------------------------
void sip_value_user_t::cleanup()
{
	sip_value_uri_t::cleanup();

	m_tag.setEmpty();
}
//--------------------------------------
//
//--------------------------------------
sip_value_user_t& sip_value_user_t::assign(const sip_value_user_t& value)
{
	sip_value_uri_t::assign(value);
	m_tag = value.m_tag;

	return *this;
}
//--------------------------------------
//
//--------------------------------------
sip_value_t* sip_value_user_t::clone() const
{
	return NEW sip_value_user_t(*this);
}
//--------------------------------------
