﻿/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_stack.h"
#include "sip_user_agent.h"
#include "sip_profile.h"
#include "sip_registrar.h"
#include "sip_registrar_account.h"
//--------------------------------------
//
//--------------------------------------
sip_registrar_t::sip_registrar_t(sip_user_agent_t& useragent) :
	m_account_list(compare_by_obj, compare_by_key),
	m_user_agent(useragent),
	m_serializer(nullptr),
	m_started(false),
	m_write_changes_to_regdb(true)
{
}
//--------------------------------------
// деструктор
//--------------------------------------
sip_registrar_t::~sip_registrar_t()
{
	stop();

	rtl::MutexLock lock(m_sync);

	SLOG_CALL("reg-man", "destroying registrar...");

	for (int i = 0; i < m_account_list.getCount(); i++)
	{
		m_account_list[i]->release();
	}

	m_account_list.clear();

	SLOG_CALL("reg-man", "registrar destroyed");
}
//--------------------------------------
//
//--------------------------------------
void sip_registrar_t::setup(sip_registrar_serializer_t* serializer)
{
	SLOG_CALL("reg-man", "setup registrar");

	rtl::MutexLock lock(m_sync);

	m_serializer = serializer;

	SLOG_CALL("reg-man", "setup done");
}
//--------------------------------------
//
//--------------------------------------
void sip_registrar_t::start()
{
	SLOG_CALL("reg-man", "starting registrar...");

	rtl::MutexLock lock(m_sync);

	m_started = true;

	rtl::Timer::setTimer(this, 0, 1000, true);

	SLOG_CALL("reg-man", "registrar started");
}
//--------------------------------------
//
//--------------------------------------
void sip_registrar_t::stop()
{
	SLOG_CALL("reg-man", "stopping registrar...");

	rtl::Timer::stopTimer(this, 0);

	rtl::MutexLock lock(m_sync);

	m_started = false;

	SLOG_CALL("reg-man", "registrar stopped");
}
//--------------------------------------
// добавление или изменение аккаунта
// изменять можно только auth_id  и password
//--------------------------------------
bool sip_registrar_t::set_account(sip_registrar_event_handler_t* event_handler, const char* username, const char* auth_id, const char* password, int max_expires)
{
	SLOG_CALL("reg-man", "updating account %s...", username != nullptr ? username : "(null)");

	if (username == nullptr || username[0] == 0)
	{
		SLOG_WARN("reg-man", "invalid account username! not updated!");
	}

	sip_registrar_account_t* account = find_account(username);

	if (account == nullptr)
	{
		account = NEW sip_registrar_account_t(*this, event_handler);
		account->setup(username, auth_id, password, max_expires);

		{
			rtl::MutexLock lock(m_sync);
			m_account_list.add(account);
		}

		SLOG_CALL("reg-man", "new account added");
	}
	else
	{
		SLOG_CALL("reg-man", "existing account updating");
		account->setup(username, auth_id, password, max_expires);
		account->release();
	}

	SLOG_CALL("reg-man", "account updated");

	return true;
}
//--------------------------------------
// удаление аккаунта
//--------------------------------------
void sip_registrar_t::remove_account(const char* username)
{
	SLOG_CALL("reg-man", "removing account %s...", username ? username : "(null)");

	if (username == nullptr || username[0] == 0)
	{
		SLOG_WARN("reg-man", "invalid account username! not removed!");
	}

	int index = BAD_INDEX;
	sip_registrar_account_t* account = find_account(username, &index);

	if (account != nullptr)
	{
		{
			rtl::MutexLock lock(m_sync);
			m_account_list.removeAt(index);
			account->release();
		}

		account->release();

		SLOG_CALL("reg-man", "account removed");
	}
	else
	{
		SLOG_CALL("reg-man", "account not found!");
	}
}
//--------------------------------------
// включение аккаунта (информация о контактах отсутствует)
//--------------------------------------
void sip_registrar_t::enable_account(const char* username)
{
	SLOG_CALL("reg-man", "enabling account %s...", username != nullptr ? username : "(null)");

	if (username == nullptr || username[0] == 0)
	{
		SLOG_WARN("reg-man", "invalid account username! not enabled!");
	}

	// find_account() содержит лок а регистрар в этой функции не меняется
	// rtl::MutexLock lock(m_sync);

	sip_registrar_account_t* account = find_account(username);

	if (account != nullptr)
	{
		account->set_enabled();
		account->release();
		SLOG_CALL("reg-man", "account enabled");
	}
	else
	{
		SLOG_CALL("reg-man", "account not found!");
	}
}
//--------------------------------------
// выключение аккаунта (информация о контактах удаляется)
//--------------------------------------
void sip_registrar_t::disable_account(const char* username)
{
	SLOG_CALL("reg-man", "disabling account %s...", username);

	// find_account() содержит лок а регистрар в этой функции не меняется
	// rtl::MutexLock lock(m_sync);

	sip_registrar_account_t* account = find_account(username);

	if (account != nullptr)
	{
		account->set_enabled(false);
		account->release();

		SLOG_CALL("reg-man", "account disabled");
	}
	else
	{
		SLOG_CALL("reg-man", "account not found!");
	}
}
//--------------------------------------
// изменение свойств аккаунта
//--------------------------------------
void sip_registrar_t::set_max_expires(const char* username, int max_expires)
{
	SLOG_CALL("reg-man", "setting max expires (%d) for account %s...", max_expires, username);

	sip_registrar_account_t* account = find_account(username);

	if (account != nullptr)
	{
		account->set_max_expires(max_expires);
		account->release();

		SLOG_CALL("reg-man", "account set");
	}
	else
	{
		SLOG_CALL("reg-man", "account not found!");
	}
}
//--------------------------------------
// поиск акаунта для изменения свойств 
//--------------------------------------
//sip_registrar_account_t* sip_registrar_t::get_account(const char* username)
//{
//	SLOG_CALL("reg-man", "get account %s...", username);
//
//	// find_account() содержит лок а регистрар в этой функции не меняется
//	// rtl::MutexLock lock(m_sync);
//
//	sip_registrar_account_t* account = find_account(username);
//
//	SLOG_CALL("reg-man", "account %s", account != nullptr ? L"found" : L"not found");
//
//	return account;
//}
//--------------------------------------
// получение информации об активных контактах акаунта
//--------------------------------------
int sip_registrar_t::get_contacts(const char* username, rtl::PonterArrayT<sip_registrar_contact_info_t>& contacts)
{
	SLOG_CALL("reg-man", "retrieving account %s contacts...", username != nullptr ? username : "(null)");

	if (username == nullptr || username[0] == 0)
	{
		SLOG_WARN("reg-man", "invalid account username! not disabled!");
	}

	// find_account() содержит лок а регистрар в этой функции не меняется
	// rtl::MutexLock lock(m_sync);

	sip_registrar_account_t* account = find_account(username);

	SLOG_CALL("reg-man", "account found!");

	int count = 0;
	
	if (account != nullptr)
	{
		count = account->get_contacts(contacts);
		account->release();
		SLOG_CALL("reg-man", "account have %d contacts", count);
	}
	else
	{
		SLOG_CALL("reg-man", "account not found!");
	}

	return count;
}
//--------------------------------------
//
//--------------------------------------
bool sip_registrar_t::check_contact(const char* username, sip_registrar_contact_info_t* contact)
{
	SLOG_CALL("reg-man", "checking account %s contact for %s:%u", username, inet_ntoa(contact->get_contact().address), contact->get_contact().port);

	if (username == nullptr || username[0] == 0)
	{
		SLOG_WARN("reg-man", "invalid account username! not disabled!");
	}

	sip_registrar_account_t* account = find_account(username);
	bool result = false;

	if (account != nullptr)
	{
		result = account->check_contact(contact);
		account->release();
	}
	
	return result;
}
//--------------------------------------
//
//--------------------------------------
void sip_registrar_t::send_subscription_notify(const char* username, const sip_registrar_subscribe_info_t* notify_info)
{
	SLOG_CALL("reg-man", "send notify for account %s...", username);

	if (username == nullptr || username[0] == 0)
	{
		SLOG_WARN("reg-man", "invalid account username!");
	}

	// find_account() содержит лок а регистрар в этой функции не меняется
	// rtl::MutexLock lock(m_sync);

	sip_registrar_account_t* account = find_account(username);

	if (account != nullptr)
	{
		account->send_sub_notify(notify_info);
		account->release();
		SLOG_CALL("reg-man", "done");
	}
	else
	{
		SLOG_CALL("reg-man", "account not found!");
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_registrar_t::process_transport_disconnected(const sip_transport_route_t& route)
{
	rtl::MutexLock lock(m_sync);

	for (int i = 0; i < m_account_list.getCount(); i++)
	{
		m_account_list[i]->process_transport_disconnected(route);
	}
}
//--------------------------------------
// обработка входящего сип пакета регистрации
//--------------------------------------
void sip_registrar_t::process_incoming_request(sip_stack_event_t* sip_event)
{
	if (!m_started)
	{
		SLOG_CALL("reg-man", "process incoming REGISTER request : packet dropped becouse service not started");
		return;
	}

	sip_event_message_t& msg_event = *(sip_event_message_t*)sip_event;
	const sip_message_t& request = msg_event.get_message();

	const sip_value_uri_t* to = request.get_To_value();
	const sip_value_uri_t* from = request.get_From_value();

	SLOG_CALL("reg-man", "process incoming REGISTER request to:%s", (const char*)request.To_URI().to_string());

	if (to && from)
	{
		if (request.IsRegister())
		{
			const rtl::String& to_username = to->get_uri().get_user();
			const rtl::String& from_username = from->get_uri().get_user();

			if (::strcmp(to_username, from_username) == 0)
			{
				sip_registrar_account_t* account = find_account(to_username);

				if (account != nullptr && account->is_enabled())
				{
					if (!account->process_incoming_request(request))
					{
						// запрос был не принят обработчиком по причине неправильно
						// сформированного запроса
						// если были проблемы другого характера то функция сама отправляет ответ
						// например 500 Internal Error
						send_response(request, sip_400_BadRequest);
					}
					account->release();
				}
				else // акаунт не найден в базе
				{
					send_response(request, sip_404_NotFound);
				}
			}
			else // акаунт может менятся только самим пользователем!
			{
				send_response(request, sip_403_Forbidden);
			}
		}
		else if (request.IsSubscribe())
		{
			const rtl::String& from_username = from->get_uri().get_user();

			sip_registrar_account_t* account = find_account(from_username);

			if (account != nullptr && account->is_enabled())
			{
				if (!account->process_incoming_request(request))
				{
					// запрос был не принят обработчиком по причине неправильно
					// сформированного запроса
					// если были проблемы другого характера то функция сама отправляет ответ
					// например 500 Internal Error
					send_response(request, sip_400_BadRequest);
				}
				account->release();
			}
			else // акаунт не найден в базе
			{
				send_response(request, sip_404_NotFound);
			}
		}
	}
	else // отсутствуют To From
	{
		send_response(request, sip_400_BadRequest);
	}

	SLOG_CALL("reg-man", "request handled");
}
//--------------------------------------
// таймер для обнавления состояния акаунтов и сброс последнего состояния регистратора
//--------------------------------------
void sip_registrar_t::timer_elapsed_event(rtl::Timer* sender, int tid)
{
	rtl::async_call_manager_t::callAsync(this, 0, 0, nullptr);
}
//--------------------------------------
//
//--------------------------------------
const char* sip_registrar_t::async_get_name()
{
	return "sip-registrar";
}
//--------------------------------------
//
//--------------------------------------
void sip_registrar_t::async_handle_call(uint16_t call_id, uintptr_t int_param, void* ptr_param)
{
	if (!m_started)
	{
		// ...
		return;
	}

	time_t current_time = time(nullptr);

	rtl::MutexLock lock(m_sync);

	for (int i = 0; i < m_account_list.getCount(); i++)
	{
		m_account_list[i]->check_contacts(current_time);
	}
}
//--------------------------------------
//
//--------------------------------------
int sip_registrar_t::compare_by_obj(const sip_registrar_account_ptr& left, const sip_registrar_account_ptr& right)
{
	return strcmp(left->get_username(), right->get_username());
}
//--------------------------------------
//
//--------------------------------------
int sip_registrar_t::compare_by_key(const ZString& left, const sip_registrar_account_ptr& right)
{
	return strcmp(left, right->get_username());
}
//--------------------------------------
// алгоритм:
//   ищем сначала среди акаунтов с доменами, а потом без
//--------------------------------------
sip_registrar_account_t* sip_registrar_t::find_account(const char* username, int* pindex)
{
	rtl::MutexLock lock(m_sync);

	int index = BAD_INDEX;
	sip_registrar_account_ptr* account_ptr = m_account_list.find(username, index);

	if (pindex != nullptr)
		*pindex = index;

	if (account_ptr != nullptr)
	{
		sip_registrar_account_t* account = *account_ptr;
		if (account != nullptr)
		{
			account->add_ref();
		}

		return account;
	}
	
	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
void sip_registrar_t::send_response(const sip_message_t& request,  sip_status_t answer_code)
{
	sip_message_t response;
	request.make_response(response, answer_code);

	send_response(response);
}
//--------------------------------------
//
//--------------------------------------
void sip_registrar_t::send_response(sip_message_t& response)
{
	if (!response.is_request() && !response.has_Server())
	{
		rtl::String uaName = m_user_agent.get_UA_name();
		response.make_Server_value()->assign(uaName); 
	}

	const sip_transport_route_t& r_route = response.getRoute();

	if (r_route.type == sip_transport_udp_e)
	{
		sip_value_via_t* via = response.get_Via_value();

		in_addr via_address = net_t::get_host_by_name(via->get_address());
		uint16_t via_port = via->get_port();

		if (via_port == 0)
			via_port = 5060;

		//in_addr recv_address = response.get_remote_address();
		//uint16_t recv_port = response.get_remote_port();
		//rtl::String rport, raddr;

		uint16_t rport = via->get_rport();
		in_addr raddr = via->get_received_address();

		//if (via->get("rport", rport) && via->get("received", raddr))
		if (rport != 0 && net_t::ip_is_valid(raddr))
		{
			if (via_address.s_addr != r_route.remoteAddress.s_addr)
			{
				via_address = r_route.remoteAddress;
				via_port = r_route.remotePort;
			}
		}

		if (via_address.s_addr == r_route.remoteAddress.s_addr && via_port != r_route.remotePort)
		{
			// отправляем на другой порт но по тому же протоколу
			// получение запроса по UDP и отправка по TCP не поддерживается
			// как и обратная ситуация
			// во всех остальных случаях отправляем откуда пришло (NAT, WS)
			//------------------

			// @spec: transport: 1, 6, 7 : отправка ответа по Via. только для UDP
			sip_transport_route_t back_route = { r_route.type, r_route.if_address, r_route.if_port, via_address, via_port };
			response.set_route(back_route);
		}
	}

	if (rtl::Logger::check_trace(TRF_CALL))
	{
		SLog.log("reg-man", "send response %d %s to %s:%u",
			response.get_response_code(),
			(const char*)response.get_response_reason(),
			inet_ntoa(response.get_remote_address()),
			response.get_remote_port());
	}

	trans_id tid;
	m_user_agent.push_message_to_transaction(response, tid);
}
//--------------------------------------
//
//--------------------------------------
void sip_registrar_t::send_request(sip_message_t& request)
{
	if (!request.is_request() && !request.has_User_Agent())
	{
		request.make_User_Agent()->assign(m_user_agent.get_UA_name());
	}

	if (!request.has_Max_Forwards())
	{
		request.make_Max_Forwards_value()->set_number(SIP_DEFAULT_MAX_FORWARDS);
	}

	trans_id tid;
	m_user_agent.push_message_to_transaction(request, tid);
}
//--------------------------------------
//
//--------------------------------------
sip_stack_t& sip_registrar_t::get_stack()
{
	return m_user_agent.get_sip_stack();
}
//--------------------------------------
//
//--------------------------------------
bool sip_registrar_t::is_over_nat(const char* request_host, in_addr iface, in_addr r_addr)
{
	return m_serializer != nullptr ? m_serializer->sip_registrar_is_over_nat(request_host, iface, r_addr) : false;
}
//--------------------------------------
//
//--------------------------------------
const char* get_sip_unregister_event_reason(sip_unregister_event_reason_t reason)
{
	const char* names[] = { "unregister_request", "unregister_replaced", "unregister_disabled", "unregister_reset", "unregister_failed", "unregister_expires" };

	return reason >= sip_unregister_request_e && reason <= sip_unregister_expires_e ? names[reason] : "unregister_unknown";
}
//--------------------------------------
//
//--------------------------------------
sip_registrar_contact_info_t::sip_registrar_contact_info_t() :
	m_expires(0), m_is_over_nat(false)
{
	memset(&m_route, 0, sizeof(m_route));
	memset(&m_via, 0, sizeof(m_via));
	memset(&m_contact, 0, sizeof(m_contact));
	m_real_address.s_addr = INADDR_ANY;
}
//--------------------------------------
//
//--------------------------------------
sip_registrar_contact_info_t::sip_registrar_contact_info_t(const sip_registrar_contact_info_t& info)
{
	m_route = info.m_route;
	m_real_address = info.m_real_address;
	m_expires = info.m_expires;
	m_is_over_nat = info.m_is_over_nat;
	m_via = info.m_via;
	m_contact = info.m_contact;
	m_contact_params = info.m_contact_params;
	m_callid = info.m_callid;
	m_user = info.m_user;
}
//--------------------------------------
//
//--------------------------------------
sip_registrar_contact_info_t& sip_registrar_contact_info_t::operator = (const sip_registrar_contact_info_t& info)
{
	m_route = info.m_route;
	m_real_address = info.m_real_address;
	m_expires = info.m_expires;
	m_is_over_nat = info.m_is_over_nat;
	m_via = info.m_via;
	m_contact = info.m_contact;
	m_contact_params = info.m_contact_params;
	m_callid = info.m_callid;
	m_user = info.m_user;

	return *this;
}
//--------------------------------------
