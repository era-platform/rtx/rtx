/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_transport_binding.h"
#include "sip_transport_listener_udp.h"
#include "sip_transport_listener_tcp.h"
//--------------------------------------
//
//--------------------------------------
#define ASYNC_REMOVE_LISTENER	1
#define ASYNC_START_LISTENER	2
//--------------------------------------
//
//--------------------------------------
volatile int32_t sip_transport_binding_t::s_id_gen = 0;
//--------------------------------------
//
//--------------------------------------
sip_transport_binding_t::sip_transport_binding_t(sip_transport_type_t type) :
	m_type(type), m_listen_all(false), m_listen_port(0), m_disable_time(0)
{
	m_listen_address.s_addr = INADDR_ANY;

	int32_t id = std_interlocked_inc(&s_id_gen);

	int len = std_snprintf(m_id, 32, "B(%d)", id);

	m_id[len] = 0;
}
//--------------------------------------
//
//--------------------------------------
sip_transport_binding_t::~sip_transport_binding_t()
{
	destroy();
}
//--------------------------------------
//
//--------------------------------------
bool sip_transport_binding_t::create(in_addr if_address, uint16_t if_port)
{
	if (if_port == 0 || if_address.s_addr == INADDR_NONE)
		return false;

	m_listen_address = if_address;
	m_listen_port = if_port;
	m_listen_all = m_listen_address.s_addr == INADDR_ANY;
	
	if (m_listen_all)
	{
		rtl::ArrayT<in_addr> iftable;
		if (net_t::get_interface_table(iftable))
		{
			for (int i = 0; i < iftable.getCount(); i++)
			{
				add_listener(iftable[i]);
			}
		}
	}
	else
	{
		add_listener(if_address);
	}

	return m_list.getCount() > 0;
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_binding_t::destroy()
{
	rtl::MutexLock lock(m_list_sync);
	
	for (int i = 0; i < m_list.getCount(); i++)
	{
		sip_transport_listener_t* listener = m_list[i];
 		listener->stop_listen();
		listener->stop_reading();
		listener->destroy();
		listener->release_ref();
	}

	m_if_list.clear();

	m_list.clear();
}
//--------------------------------------
//
//--------------------------------------
bool sip_transport_binding_t::open()
{
	rtl::MutexLock lock(m_list_sync);

	int started = 0;

	for (int i = 0; i < m_list.getCount(); i++)
	{
		if (m_list[i]->start_listen())
			started++;
	}

	return started > 0;
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_binding_t::close()
{
	rtl::MutexLock lock(m_list_sync);
	
	for (int i = 0; i < m_list.getCount(); i++)
	{
		m_list[i]->stop_listen();
	}

	m_if_list.clear();
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_binding_t::check_transport_life_times()
{
	//rtl::MutexLock lock(m_list_sync);
	//
	//for (int i = 0; i < m_list.getCount(); i++)
	//{
	//	m_list[i]->check_transport_life_times();
	//}
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_binding_t::update_interface_table(const rtl::ArrayT<in_addr>& iftable)
{
	if (m_listen_all)
	{
		update_zero_interface(iftable);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_binding_t::event_interface_added(const rtl::ArrayT<in_addr>& list)
{
	rtl::MutexLock lock(m_list_sync);

	// ���� ������ �� ������������ ��������� � ����� ��������� �� ������� ���������
	if (!m_listen_all)
	{
		for (int i = 0; i < list.getCount(); i++)
		{
			if (list[i].s_addr == m_listen_address.s_addr)
			{
				add_listener(m_listen_address);
				break;
			}
		}

		return;
	}

	// ���� �� ���� �� ������ ������� �����
	for (int i = 0; i < list.getCount(); i++)
	{
		add_listener(list[i]);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_binding_t::event_interface_removed(const rtl::ArrayT<in_addr>& list)
{
	rtl::MutexLock lock(m_list_sync);

	// ���� ������ �� ������������ ��������� � ����� ��������� �� �������� ���������
	if (!m_listen_all)
	{
		//...
		return;
	}

	// ���� �� ���� �� ������ ������

	for (int i = 0; i < m_list.getCount(); i++)
	{
		sip_transport_listener_t* listener = m_list[i];
		for (int j = 0; j < list.getCount(); j++)
		{
			if (listener->get_interface_address().s_addr == list[j].s_addr)
			{
				listener->stop_listen();
				rtl::async_call_manager_t::callAsync(this, ASYNC_REMOVE_LISTENER, 0, listener);
				break;
			}
		}
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_binding_t::event_network_fault()
{
	rtl::MutexLock lock(m_list_sync);

	// ������ ��� ��� ���� � ��� �������� ��������

	for (int i = 0; i < m_list.getCount(); i++)
	{
		m_list[i]->stop_listen();
	}

	rtl::async_call_manager_t::callAsync(this, ASYNC_REMOVE_LISTENER, 0, nullptr);
}
//--------------------------------------
//
//--------------------------------------
sip_transport_t* sip_transport_binding_t::create_transport(const sip_transport_route_t& route)
{
	if (route.type != sip_transport_udp_e)
		return nullptr;

	sip_transport_listener_t* listener = nullptr;

	{
		rtl::MutexLock lock(m_list_sync);

		for (int i = 0; i < m_list.getCount(); i++)
		{
			if (m_list[i]->get_interface_address().s_addr == route.if_address.s_addr &&
				compare_listener_type(route.type, m_list[i]->getType()))
			{
				listener = m_list[i];
				break;
			}
		}

		// �������� ��� ���������� ���������
		if (listener == nullptr)
		{
			listener = add_listener(route.if_address);
		}
	}

	if (listener != nullptr)
	{
		sip_transport_point_t endpoint = { route.type, route.remoteAddress, route.remotePort };
		return ((sip_transport_listener_udp_t*)listener)->create_transport(endpoint);
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
sip_transport_listener_t* sip_transport_binding_t::add_listener(in_addr if_address)
{
	sip_transport_listener_t* listener = nullptr;
	switch (m_type)
	{
	case sip_transport_udp_e:
		listener = NEW sip_transport_listener_udp_t();
		break;
	case sip_transport_tcp_e:
	case sip_transport_tls_e:
	case sip_transport_ws_e:
	case sip_transport_wss_e:
		listener = NEW sip_transport_listener_tcp_t();
		break;
	}

	sip_transport_point_t iface = { m_type, if_address, m_listen_port };

	if (listener == nullptr)
	{
		SLOG_WARN("net-bind", "start listener failed unknown type(%d)", m_type);
	}
	else if (!listener->create(iface))
	{
		SLOG_WARN("net-bind", "start %s listener failed on interface %s:%u", TransportType_toString(m_type), inet_ntoa(if_address), m_listen_port);

		listener->release_ref();
		listener = nullptr;
	}
	else
	{
		rtl::MutexLock lock(m_list_sync);
		
		m_if_list.add(if_address);
		m_list.add(listener);
		
		sip_transport_manager_t::start_reading(listener);

		SLOG_NET("net-bind", "new listener %s added", listener->get_id());
	}

	return listener;
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_binding_t::update_zero_interface(const rtl::ArrayT<in_addr>& iftable)
{
	rtl::ArrayT<in_addr> if_new;
	rtl::ArrayT<sip_transport_listener_t*> if_delete;

	rtl::MutexLock lock(m_list_sync);

	for (int i = 0; i < iftable.getCount(); i++)
	{
		bool found = false;
		for (int j = 0; j < m_if_list.getCount(); j++)
		{
			
			if (iftable[i].s_addr == m_if_list[j].s_addr)
			{
				found = true;
				break;
			}
		}

		if (!found)
			if_new.add(iftable[i]);
	}

	for (int i = m_list.getCount() - 1; i >= 0; i--)
	{
		bool found = false;
		sip_transport_listener_t* listener = m_list[i];
		
		for (int j = 0; j < iftable.getCount(); j++)
		{
			if (listener->get_interface_address().s_addr == iftable[j].s_addr)
			{
				found = true;
				break;
			}
		}

		if (!found)
		{
			if_delete.add(listener);
		}
	}

	// ������������� ��������� � ���������� � ����������
	for (int i = 0; i < if_delete.getCount(); i++)
	{
		SLOG_NET("net-bind", "found removed interface listener %s", if_delete[i]->get_id());
		rtl::async_call_manager_t::callAsync(this, ASYNC_REMOVE_LISTENER, 0, if_delete[i]);
	}

	// ��������� �����
	for (int i = 0; i < if_new.getCount(); i++)
	{
		SLOG_NET("net-bind", "found new interface %s", inet_ntoa(if_new[i]));
		rtl::async_call_manager_t::callAsync(this, ASYNC_START_LISTENER, if_new[i].s_addr, nullptr);
	}
}
//--------------------------------------
//
//--------------------------------------
const char* sip_transport_binding_t::async_get_name()
{
	return "listener-binding";
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_binding_t::async_handle_call(uint16_t call_id, uintptr_t int_param, void* ptr_param)
{
	switch (call_id)
	{
	case ASYNC_REMOVE_LISTENER:
		async_remove_listener((sip_transport_listener_t*)ptr_param);
		break;
	case ASYNC_START_LISTENER:
		async_add_listener(int_param);
		break;
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_binding_t::async_remove_listener(sip_transport_listener_t* listener)
{
	rtl::MutexLock lock(m_list_sync);

	if (listener != nullptr)
	{
		for (int i = m_list.getCount() - 1; i >= 0; i--)
		{
			sip_transport_listener_t* checking = m_list[i];

			if (checking == listener)
			{
				SLOG_NET("net-bind", "destroy listener %s...", checking->get_id());

				m_if_list.removeAt(i);
				m_list.removeAt(i);

				checking->stop_listen(true);
				checking->stop_reading();
				checking->destroy();
				checking->release_ref();

				SLOG_NET("net-bind", "listener destroyed");
			
				return;
			}
		}
	}
	else // ������� ����
	{
		for (int i = 0; i < m_list.getCount(); i++)
		{
			sip_transport_listener_t* l = m_list[i];

			SLOG_NET("net-bind", "destroy listener %s...", l->get_id());

			l->stop_listen(true);
			l->stop_reading();
			l->destroy();
			l->release_ref();

			SLOG_NET("net-bind", "listener destroyed");
		}

		m_if_list.clear();
		m_list.clear();
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_binding_t::async_add_listener(uintptr_t addr)
{
	in_addr ifaddr;
	ifaddr.s_addr = (uint32_t)addr;
	sip_transport_listener_t* listener = add_listener(ifaddr);

	if (listener != nullptr)
	{
		bool result = listener->start_listen();
		if (result)
		{
			sip_transport_manager_t::sip_listener_transport_started(listener);
		}
		SLOG_NET("net-bind", "new interface %s", result ? L"started" : L"failed");
	}
	else
	{
		SLOG_NET("net-bind", "new interface failed");
	}
}
//--------------------------------------
