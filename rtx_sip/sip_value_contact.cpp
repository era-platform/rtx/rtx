/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_value_contact.h"
//--------------------------------------
//
//--------------------------------------
rtl::String& sip_value_contact_t::prepare(rtl::String& stream) const
{
	if (m_all_contact)
		return stream << "*";
	
	sip_value_user_t::prepare(stream);
	
	return stream;
}
//--------------------------------------
//
//--------------------------------------
bool sip_value_contact_t::parse()
{
	if (m_value == "*")
	{
		m_all_contact = true;
		m_display_name = rtl::String::empty;
		m_uri.cleanup();
		m_params.cleanup();
	}
	else
	{
		sip_value_user_t::parse();

		m_expires = CONTACT_HAS_NO_EXPORES;

		for (int i = m_params.getCount() - 1; i >= 0; i--)
		{
			const mime_param_t* param = m_params.getAt(i);

			if (std_stricmp(param->getName(), "expires") == 0)
			{
				m_expires = strtoul(param->getValue(), nullptr, 10);
				m_params.removeAt(i);
			}
		}
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
void sip_value_contact_t::cleanup()
{
	m_all_contact = false;
	m_expires = CONTACT_HAS_NO_EXPORES;

	sip_value_user_t::cleanup();
}
//--------------------------------------
//
//--------------------------------------
sip_value_contact_t& sip_value_contact_t::assign(const sip_value_contact_t& v)
{
	sip_value_user_t::assign(v);

	m_all_contact = v.m_all_contact;
	m_expires = v.m_expires;

	return *this;
}
//--------------------------------------
//
//--------------------------------------
sip_value_t* sip_value_contact_t::clone() const
{
	return NEW sip_value_contact_t(*this);
}
//--------------------------------------
