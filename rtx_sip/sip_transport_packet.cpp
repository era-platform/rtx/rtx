/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "sip_transport_packet.h"
#include "sip_transport.h"
//--------------------------------------
//
//--------------------------------------
#define SIP_PACKET_START_COUNT	4
//--------------------------------------
//
//--------------------------------------
rtl::Mutex raw_packet_t::s_pool_sync;
rtl::QueueT<raw_packet_t*> raw_packet_t::s_pool_queue;
//--------------------------------------
//
//--------------------------------------
raw_packet_t* raw_packet_t::get_free(bool is_udp)
{
	/*raw_packet_t* packet;

	{
		critical_section_lock_t lock(s_pool_sync);
		packet = s_pool_queue.pop();
	}
	
	if (packet == nullptr)
		packet = NEW raw_packet_t;

	return packet;*/

	return NEW raw_packet_t(is_udp);
}
//--------------------------------------
//
//--------------------------------------
void raw_packet_t::release(raw_packet_t* packet)
{
	/*packet->cleanup();

	critical_section_lock_t lock(s_pool_sync);
	s_pool_queue.push(packet);*/

	DELETEO(packet);
}
//--------------------------------------
//
//--------------------------------------
/*void raw_packet_t::create_pool()
{
	critical_section_lock_t lock(s_pool_sync);
	for (int i = 0; i < SIP_PACKET_START_COUNT; i++)
	{
		raw_packet_t* packet = NEW raw_packet_t;
		s_pool_queue.push(packet);
	}
}*/
//--------------------------------------
//
//--------------------------------------
/*void raw_packet_t::destroy_pool()
{
	raw_packet_t* packet;

	critical_section_lock_t lock(s_pool_sync);
	while (s_pool_queue.getCount() > 0)
	{
		if ((packet = s_pool_queue.pop()) != nullptr)
			DELETEO(packet);
	}
}*/
//--------------------------------------
//
//--------------------------------------
static char* skip_ws(char* text)
{
	if (text == nullptr || *text == 0)
		return nullptr;

	while (*text == ' ' || *text == '\t' || *text == '\r' || *text == '\n')
		text++;

	int length = (int)strlen(text);
	char* zero = text + (length > 0 ? length - 1 : 0);

	while (zero >= text && (*zero == ' ' || *zero == '\t' || *zero == '\r' || *zero == '\n'))
	{
		*zero = 0;
		zero--;
	}

	return text;
}
//--------------------------------------
// ������ ��������� ������ ���������
//--------------------------------------
char* raw_start_line_t::parse(char* stream)
{
	cleanup();

	char* p1, *p2, *p3;

	if (stream == nullptr || *stream == 0)
		return nullptr;

	p1 = stream;
	char* cp = strchr(stream, ' ');

	if (cp == nullptr)
	{
		return nullptr;
	}

	*cp++ = 0;
	
	p2 = cp;

	cp = strchr(cp, ' ');

	if (cp == nullptr)
	{
		return nullptr;
	}

	*cp++ = 0;

	p3 = cp;

	cp = strchr(cp, '\r');

	if (cp == nullptr || *(cp+1) != '\n')
	{
		return nullptr;
	}
	
	*cp++ = 0;
	*cp++ = 0;

	m_part1 = p1;
	m_part2 = p2;
	m_part3 = p3;

	m_request = std_strnicmp(m_part1, "SIP/", 4) != 0;

	if (m_part1[0] == 0 || m_part2[0] == 0 || m_part3[0] == 0)
	{
		SLOG_ERROR("sip-par", "Parser error : invalid %s line M:%s U:%s V:%s", m_request ? "Request" : "Status", m_part1, m_part2, m_part3);
		return nullptr;
	}

	if (!m_request && std_strnicmp(m_part1, "SIP/", 4) != 0)
	{
		SLOG_ERROR("sip-par", "Parser error : invalid %s line M:%s U:%s V:%s", m_request ? "Request" : "Status", m_part1, m_part2, m_part3);
		return nullptr;
	}

	return cp;
}
//--------------------------------------
//
//--------------------------------------
int raw_start_line_t::to_string(char* buffer, int size)
{
	if (m_part1 == nullptr || m_part2 == nullptr || m_part3 == nullptr)
		return 0;

	return std_snprintf(buffer, size, "%s %s %s\r\n", m_part1, m_part2, m_part3);
}
//--------------------------------------
//
//--------------------------------------
raw_packet_t::raw_packet_t(bool is_udp) : m_buffer(nullptr)
{
	memset(&m_route, 0, sizeof(m_route));
	cleanup();

	if (!is_udp)
	{
		m_buffer = (char*)MALLOC(SIP_PACKET_MAX + 4);
	}

//	RC_IncrementObject(RC_RAW_MSG);
}
//--------------------------------------
//
//--------------------------------------
raw_packet_t::~raw_packet_t()
{
	if (m_buffer != nullptr)
	{
		FREE(m_buffer);
		m_buffer = nullptr;
	}

	//RC_DecrementObject(RC_RAW_MSG);
}
//--------------------------------------
//
//--------------------------------------
void raw_packet_t::set_route(const sip_transport_route_t& transport)
{
	m_route = transport;
}
//--------------------------------------
//
//--------------------------------------
void raw_packet_t::set_route(sip_transport_type_t type, in_addr ifaddr, uint16_t ifport, in_addr raddr, uint16_t rport)
{
	m_route.type = type;
	m_route.if_address = ifaddr;
	m_route.if_port = ifport;
	m_route.remoteAddress = raddr;
	m_route.remotePort = rport;
}
//--------------------------------------
//
//--------------------------------------
char* raw_packet_t::parse_next_header(raw_header_t& header, char* stream)
{
	header.name = nullptr;
	header.value = nullptr;

	if (stream == nullptr || stream[0] == 0)
		return nullptr;

	char* name = nullptr;
	char* value = nullptr;
	char* eol;

	// �������� ��������� ����
	if ((value = strchr(stream, ':')) == nullptr)
	{
		// ����� ������!
		return nullptr;
	}

	// ������ ����� ������
	if ((eol = strchr(stream, '\r')) == nullptr || *(eol+1) != '\n' || value > eol)
	{
		// ����� ������!
		return nullptr;
	}

	name = stream;
	*value++ = 0;

	// ���� ����� ��������
	while (*(eol+2) == ' ' || *(eol+2) == '\t')
	{
		if ((eol = strchr(eol+2, '\r')) == nullptr || *(eol+1) != '\n')
		{
			// ����� ������!
			return nullptr;
		}
	}

	*eol++ = 0;
	*eol++ = 0;

	header.name = skip_ws(name);
	header.value = skip_ws(value);

	return eol;
}
//--------------------------------------
//
//--------------------------------------
void raw_packet_t::add_header(raw_header_t& header)
{
	if (header.name == nullptr || header.name[0] == 0)
		return;

	m_headers.add(header);

	if (std_stricmp(header.name, "content-length") == 0)
	{
		m_body_length = atoi(header.value);
	}
	else if (std_stricmp(header.name, "user-agent") == 0)
	{
		m_useragent = header.value;
	}
}
//--------------------------------------
//
//--------------------------------------
void raw_packet_t::cleanup()
{
	//m_route_type = sip_transport_udp_e;

	memset(&m_route, 0, sizeof(m_route));

	//m_receive_address.s_addr = INADDR_ANY;
	//m_receive_port = 0;
	//m_interface_address.s_addr = INADDR_ANY;
	//m_interface_port = 0;

	m_start_line.cleanup();
	m_headers.clear();

	m_useragent = "";

	m_body = nullptr;
	m_body_length = 0;
	m_body_written = 0;

	if (m_buffer != nullptr)
	{
		memset(m_buffer, 0, m_buffer_length);
	}

	m_buffer_length = 0;
}
//--------------------------------------
//
//--------------------------------------
const raw_header_t* raw_packet_t::get_header(const char* name)
{
	raw_header_t* header = nullptr;

	for (int i = 0; i < m_headers.getCount(); i++)
	{
		if (std_stricmp(m_headers[i].name, name) == 0)
		{
			header = &m_headers[i];
			break;
		}
	}

	return header;
}
//--------------------------------------
// ��������� ����� ��� ������ ��������
//--------------------------------------
char* raw_packet_t::get_buffer()
{
	cleanup();

	if (m_buffer == nullptr)
	{
		m_buffer = (char*)MALLOC(SIP_PACKET_MAX + 4);
	}

	return m_buffer;
}
//--------------------------------------
//
//--------------------------------------
bool raw_packet_t::parse(int length)
{
	if (length <= 0)
		return false;

	m_buffer[m_buffer_length = length] = 0;

	char* ptr;
	
	if ((ptr = m_start_line.parse(m_buffer)) == nullptr)
		return false;

	raw_header_t header;
	while (*ptr != 0 && *ptr != '\r')
	{
		if ((ptr = parse_next_header(header, ptr)) != nullptr)
		{
			add_header(header);
		}
		else
		{
			cleanup();
			return false;
		}
	}

	// ������� ���� �� ���������
	if (*ptr == 0)
	{
		// ����� �����
		cleanup();
		return false;
	}

	if (*ptr++ != '\r')
	{
		return false;
	}
	
	if (*ptr++ != '\n')
	{
		cleanup();
		return false;
	}

	m_body = (uint8_t*)ptr;
	m_body_length = m_buffer_length - PTR_DIFF(ptr, m_buffer);

	return is_valid();
}
//--------------------------------------
//
//--------------------------------------
bool raw_packet_t::parse(const uint8_t* packet, int length)
{
	if (length <= 0)
		return false;

	cleanup();

	m_buffer = (char*)MALLOC(length+4);

	memcpy(m_buffer, packet, length);

	return parse(length);
}
//--------------------------------------
// ���������� ���������� ��� ������
//--------------------------------------
int raw_packet_t::add_line(const char* line, int length)
{
	if (length <= 0)
		return 0;

	int free_space = SIP_PACKET_MAX - m_buffer_length;
	int copy_size = free_space < length ? free_space : length;

	if (copy_size <= 0)
		return 0;

	memcpy(m_buffer + m_buffer_length, line, copy_size);
	m_buffer_length += copy_size;
	m_buffer[m_buffer_length] = 0;
	return copy_size;
}
//--------------------------------------
// ������ ���������� � ��������� ����� ���� ���������
//--------------------------------------
int raw_packet_t::parse_headers()
{
	if (m_buffer_length == 0)
		return -1;

	// ping packet
	if (m_buffer[0] == '\r' && m_buffer[1] == '\n')
	{
		return 0;
	}

	char* ptr;
	
	if ((ptr = m_start_line.parse(m_buffer)) == nullptr)
		return -1;

	raw_header_t header;

	m_body = nullptr;
	m_body_length = 0;

	while (*ptr != 0 && *ptr != '\r')
	{
		if ((ptr = parse_next_header(header, ptr)) != nullptr)
		{
			add_header(header);
		}
		else
		{
			cleanup();
			return -1;
		}
	}

	if (*ptr == '\r')
	{
		ptr += 2;
	}

	// ������� ���� �� ���������
	if (m_body_length < 0)
	{
		m_body_length = 0;
		return -1;
	}

	m_body = (uint8_t*)ptr;
	m_body_written = 0;
	return m_body_length;
}
//--------------------------------------
// ������ ��� ���������� ������ ���� ���������
//--------------------------------------
int raw_packet_t::add_body(const uint8_t* body, int length)
{
	if (m_body_written == m_body_length)
		return 0;

	int append_size = m_body_length - m_body_written;
	int copy_size = length <= append_size? length : append_size;

	memcpy(m_body + m_body_written, body, copy_size);
	m_body_written += copy_size;

	return copy_size;
}
//--------------------------------------
//
//--------------------------------------
int raw_packet_t::to_string(char* buffer, int size)
{
	char* initial_ptr = buffer;

	if (!m_start_line.is_valid() && m_headers.getCount() == 0)
		return 0;

	int len = m_start_line.to_string(buffer, size);

	buffer += len;
	size -= len;

	for (int i = 0; i < m_headers.getCount(); i++)
	{
		raw_header_t& header = m_headers.getAt(i);
		len = std_snprintf(buffer, size, "%s: %s\r\n", header.name, header.value);
		buffer += len;
		size -= len;
	}

	*buffer++ = '\r';
	*buffer++ = '\n';
	size -= 2;

	if (m_body_length > 0)
	{
		int mlen = MIN(size, m_body_length);
		strncpy(buffer, (char*)m_body, mlen);
		buffer[mlen] = 0;

		buffer += mlen;
		size -= mlen;
	}

	*buffer = 0;

	return PTR_DIFF(buffer, initial_ptr);
}
//--------------------------------------
