/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_transport_udp.h"
#include "sip_transport_listener.h"
#include "sip_message.h"
//--------------------------------------
//
//--------------------------------------
sip_transport_udp_t::sip_transport_udp_t(const sip_transport_point_t& iface, const sip_transport_point_t& endpoint, net_socket_t& socket) :
	sip_transport_t(iface, endpoint, socket)
{
	//capture_("sip_transport_udp_t::sip_transport_udp_t");
}
//--------------------------------------
//
//--------------------------------------
sip_transport_udp_t::~sip_transport_udp_t()
{
	// ��� ������������ ������!
	// release_("sip_transport_udp_t::~sip_transport_udp_t");
	SLOG_NET("net-udp", "%s(%d) : dtor", m_name, get_ref_count());

}
//--------------------------------------
//
//--------------------------------------
bool sip_transport_udp_t::connect()
{
	return true;
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_udp_t::disconnect()
{
}
//--------------------------------------
//
//--------------------------------------
int sip_transport_udp_t::send_message(const sip_message_t& message)
{
	if (m_socket.get_handle() == INVALID_SOCKET)
	{
		SLOG_ERROR("net-udp", "%s(%d) : has no socket!", m_name, get_ref_count());
		return 0;
	}

	int result = 0;

	rtl::String msg_string = message.to_string();
	const char* packet = (const char*)msg_string;
	int packet_len = msg_string.getLength();

	m_last_activity = rtl::DateTime::getTicks();

	if (message.IsKeepAlive())
	{
		if (rtl::Logger::check_trace(TRF_NET))
		{
			uint8_t* b0 = (uint8_t*)&m_route.remoteAddress;
			uint8_t* b1 = (uint8_t*)&m_route.if_address;

			SLog.log("net-udp", "%s(%d) : SENDING KEEP-ALIVE IFACE %d.%d.%d.%d:%u TO %d.%d.%d.%d:%u",
				m_name, get_ref_count(),
				b1[0], b1[1], b1[2], b1[3], m_route.if_port,
				b0[0], b0[1], b0[2], b0[3], m_route.remotePort);
		}

		result = m_socket.send_to("\r\n\r\n", 4, m_route.remoteAddress, m_route.remotePort);
	}
	else
	{
		// ���������� � ��� ��������
		if (rtl::Logger::check_trace(TRF_NET))
		{
			uint8_t* b0 = (uint8_t*)&m_route.remoteAddress;
			uint8_t* b1 = (uint8_t*)&m_route.if_address;

			SLog.log("net-udp", "%s(%d) : SENDING PACKET(len: %d) IFACE %d.%d.%d.%d:%u TO %d.%d.%d.%d:%u",
				m_name, get_ref_count(), packet_len,
				b1[0], b1[1], b1[2], b1[3], m_route.if_port,
				b0[0], b0[1], b0[2], b0[3], m_route.remotePort);
		}

		result = m_socket.send_to(packet, packet_len, m_route.remoteAddress, m_route.remotePort);
	}

	if (result >= 0)
	{
		SLOG_NET("net-udp", "%s(%d) : PACKET SENT (%d bytes)", m_name, get_ref_count(), result);

		if (rtl::Logger::check_trace(TRF_PROTO) && !message.IsKeepAlive())
		{
			uint8_t* a = (uint8_t*)&m_route.remoteAddress;
			uint8_t* b = (uint8_t*)&m_route.if_address;

			TransLog.log("SEND", "%s %d Bytes TO %d.%d.%d.%d:%u IFACE %d.%d.%d.%d:%u\r\n%s",
				TransportType_toString(m_route.type), packet_len,
				a[0], a[1], a[2], a[3], m_route.remotePort, b[0], b[1], b[2], b[3], m_route.if_port,
				packet);
		}

		if (!message.IsKeepAlive())
			raise_packet_sent(message);
	}
	else // result == SOCKET_ERROR
	{
		int net_error = std_get_error;
		SLOG_ERR_MSG("net-udp", net_error, "%s(%d) : PACKET NOT SENT!", m_name, get_ref_count());
		raise_send_error(message, net_error);
	}

	return result;
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_udp_t::send_keep_alive()
{
	if (m_socket.get_handle() == INVALID_SOCKET)
	{
		SLOG_ERROR("net-udp", "%s(%d) : keep-alive -> has no socket!", m_name, get_ref_count());
		return;
	}

	int result = 0;

	if (rtl::Logger::check_trace(TRF_NET))
	{
		uint8_t* b0 = (uint8_t*)&m_route.remoteAddress;
		uint8_t* b1 = (uint8_t*)&m_route.if_address;

		SLog.log("net-udp", "%s(%d) : SENDING KEEP-ALIVE IFACE %d.%d.%d.%d:%u TO %d.%d.%d.%d:%u",
			m_name, get_ref_count(),
			b1[0], b1[1], b1[2], b1[3], m_route.if_port,
			b0[0], b0[1], b0[2], b0[3], m_route.remotePort);
	}

	result = m_socket.send_to("\r\n\r\n", 4, m_route.remoteAddress, m_route.remotePort);

	if (result >= 0)
	{
		SLOG_NET("net-udp", "%s(%d) : PACKET SENT (%d bytes)", m_name, get_ref_count(), result);
	}
	else // result == SOCKET_ERROR
	{
		int net_error = std_get_error;
		SLOG_ERR_MSG("net-udp", net_error, "%s(%d) : PACKET NOT SENT!", m_name, get_ref_count());
	}
}
//--------------------------------------
//
//--------------------------------------
bool sip_transport_udp_t::is_valid()
{
	return m_socket != INVALID_SOCKET;
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_udp_t::rx_packet(const uint8_t* data, int data_length, in_addr remoteAddress, uint16_t remotePort)
{
	m_last_activity = rtl::DateTime::getTicks();

	raw_packet_t* packet = raw_packet_t::get_free();

	if (packet == nullptr)
	{
		SLOG_ERROR("net-udp", "%s(%d) : no packet allocated for incoming packet!", m_name, get_ref_count());
		return;
	}

	SLOG_NET("net-udp", "%s(%d) : PARSING PACKET...", m_name, get_ref_count());

	if (packet->parse(data, data_length))
	{
		packet->set_route(m_route);

		if (rtl::Logger::check_trace(TRF_NET))
		{
			uint8_t* b0 = (uint8_t*)&remoteAddress;
			uint8_t* b1 = (uint8_t*)&m_route.if_address;
			SLog.log("net-udp", "%s(%d) : RECEIVED PACKET(len: %d) IFACE %d.%d.%d.%d:%u FROM %d.%d.%d.%d:%u",
				m_name, get_ref_count(), data_length, b1[0], b1[1], b1[2], b1[3], m_route.if_port, b0[0], b0[1], b0[2], b0[3], remotePort);
		}

		raise_packet_read(packet);
	}
	else
	{
		if (rtl::Logger::check_trace(TRF_NET))
		{
			SLOG_WARN("net-udp", "%s(%d) : PACKET RECEIVED FROM %s:%u IS NOT SIP PACKET!", m_name, get_ref_count(), inet_ntoa(remoteAddress), remotePort);
			SLOG_BINARY("net-udp", data, data_length, "%s(%d) : packet dump", m_name, get_ref_count());
		}

		raw_packet_t::release(packet);
		
		//raise_parser_error();
		// ������ ����� ����� -- �������� ��������������� ���������� ��������� � ������!
	}
}
//--------------------------------------
// !!! ����� �� ��������� ����� ������� �� ��������� � ����� ������� �� �������,
// �� ���� � ��� ��� ����� �� ��������� �������� ������� ������!!!
//--------------------------------------
void sip_transport_udp_t::rx_error(int net_error, in_addr remoteAddress, uint16_t remotePort)
{
	SLOG_ERR_MSG("net-udp", net_error, "%s(%d) : data not sent! addr %s:%u", m_name, get_ref_count(), inet_ntoa(remoteAddress), remotePort);
	// ����� ����������� ������
	//raise_recv_error(net_error);
}
//--------------------------------------
