/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_header.h"
//--------------------------------------
//
//--------------------------------------
rtl::String& sip_header_t::write_values(rtl::String& stream) const
{
	for (int i = 0; i < m_values.getCount(); i++)
	{
		const sip_value_t* value = m_values[i];
		value->write_to(stream);

		if (i < m_values.getCount() - 1)
			stream << ",\r\n  ";
	}

	return stream << "\r\n";
}
sip_value_t* sip_header_t::create_value()
{
	sip_value_t* value = sip_value_t::create(m_type);
	m_values.add(value);
	return value;
}
//--------------------------------------
//
//--------------------------------------
sip_header_t::~sip_header_t()
{
	cleanup();
}
//--------------------------------------
//
//--------------------------------------
sip_header_t& sip_header_t::assign(const sip_header_t& header)
{
	remove_all_values();

	m_type = header.m_type;
	m_name = header.m_name;
	
	for (int i = 0; i < header.m_values.getCount(); i++)
	{
		sip_value_t* value = header.m_values.getAt(i)->clone();
		m_values.add(value);
	}

	return *this;
}
//--------------------------------------
//
//--------------------------------------
void sip_header_t::cleanup()
{
	for (int i = 0; i < m_values.getCount(); i++)
	{
		sip_value_t* value = m_values[i];
		DELETEO(value);
	}

	m_values.clear();
}
//--------------------------------------
//
//--------------------------------------
sip_header_t* sip_header_t::clone() const
{
	return NEW sip_header_t(*this);
}
//--------------------------------------
//
//--------------------------------------
rtl::String sip_header_t::to_string() const
{
	rtl::String stream = getName();
	stream += ": ";
	
	write_values(stream);

	return stream;
}
//--------------------------------------
//
//--------------------------------------
rtl::String& sip_header_t::write_to(rtl::String& stream) const
{
	stream << getName() << ": ";
	write_values(stream);
	return stream;
}
//--------------------------------------
//
//--------------------------------------
sip_value_t* sip_header_t::make_value(int index)
{
	if (index >= 0 && index < m_values.getCount())
		return m_values.getAt(index);

	return create_value();
}
//--------------------------------------
//
//--------------------------------------
void sip_header_t::remove_value_at(int index)
{
	if (index >= 0 && index < m_values.getCount())
	{
		sip_value_t* value = m_values[index];
		m_values.removeAt(index);
		DELETEO(value);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_header_t::remove_all_values()
{
	for (int i = 0; i < m_values.getCount(); i++)
	{
		sip_value_t* value = m_values[i];
		DELETEO(value);
	}

	m_values.clear();
}
//--------------------------------------
//
//--------------------------------------
bool sip_header_t::add_value(const char* stream)
{
	if (stream == nullptr || stream[0] == 0)
	{
		return false;
	}

	bool result = false;

	while (stream != nullptr && stream[0] != 0)
	{
		sip_value_t* value = sip_value_t::create(m_type);

		stream = value->read_from(stream, result);

		if (result)
		{
			m_values.add(value);
		}
		else
		{
			DELETEO(value);
		}
	}

	return result;
}
//--------------------------------------
//
//--------------------------------------
bool sip_header_t::insert_value(int index, const char* stream)
{
	bool result = false;

	while (stream != nullptr)
	{
		sip_value_t* value = sip_value_t::create(m_type);
		
		stream = value->read_from(stream, result);
		
		if (result)
		{
			m_values.insert(index, value);
		}
		else
		{
			DELETEO(value);
		}
	}

	return result;
}
//--------------------------------------
//
//--------------------------------------
bool sip_header_t::add_value(sip_value_t* value)
//bool sip_header_t::add_value(const char* stream)
{
	if (value == nullptr)
	{
		return false;
	}

	m_values.add(value);

	return true;
}
//--------------------------------------
