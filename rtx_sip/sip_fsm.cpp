/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "sip_fsm.h"
#include "sip_transaction_ic.h"
#include "sip_transaction_is.h"
#include "sip_transaction_nic.h"
#include "sip_transaction_nis.h"
//--------------------------------------
//
//--------------------------------------
sip_transaction_fsm_t::sip_transaction_fsm_t()
{
}
//--------------------------------------
//
//--------------------------------------
sip_transaction_t * sip_transaction_fsm_t::create_new_transaction(const sip_message_t& message, const trans_id& trnId, sip_transaction_type_t type)
{
	if (m_terminating)
		return NULL;

	rtl::String id = trnId.AsString();

	SLOG_TRANS("SFM", "Create Transaction %s ID %s", get_sip_transaction_type_name(type), (const char*)id);

	switch (type)
	{
	case sip_transaction_ict_e:
		return NEW sip_transaction_ic_t(id, *this);
	case sip_transaction_ist_e:
		return NEW sip_transaction_is_t(id, *this);
	case sip_transaction_nict_e:
		return NEW sip_transaction_nic_t(id, *this);
	case sip_transaction_nist_e:
		return NEW sip_transaction_nis_t(id, *this);
	}

	SLOG_WARN("SFM", "Unknown transaction type requested (%d)", type);
	return NULL;
}
//--------------------------------------
