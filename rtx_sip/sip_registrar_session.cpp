﻿/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_stack.h"
#include "sip_transport_manager.h"
#include "sip_registrar_account.h"
#include "sip_registrar_session.h"
//--------------------------------------
//
//--------------------------------------
sip_registrar_session_t::sip_registrar_session_t(sip_registrar_account_t& account) :
	m_account(account),
	m_over_nat(false)
{
	memset(&m_route, 0, sizeof(m_route));

	m_via.type = sip_transport_udp_e;
	m_via.address.s_addr = INADDR_ANY;
	m_via.port = 0;

	// информация о последнем запросе
	m_cseq = 0;
	m_last_time = time(nullptr);
	m_real_sender_address.s_addr = INADDR_ANY;
}
//--------------------------------------
//
//--------------------------------------
sip_registrar_session_t::~sip_registrar_session_t()
{
	clear_contacts();

	char tmp2[128];

	SLOG_CALL("reg-sess", "%s : releasing route %s!", (const char*)m_call_id, route_to_string(tmp2, 128, m_route));

	m_account.get_registrar().get_stack().release_route(m_route, "reg-session");

	memset(&m_route, 0, sizeof(m_route));
}
//--------------------------------------
//
//--------------------------------------
int sip_registrar_session_t::get_top_contact(char* buffer, int size, sip_transport_type_t type)
{
	// есть ли контакты?
	if (m_contact_list.getCount() == 0)
	{
		buffer[0] = 0;
		return 0;
	}

	sip_registrar_contact_t* contact = m_contact_list[0];
	// по умолчанию udp и адрес берем из контакта
	sip_transport_point_t c_point = contact->get_point();

	// если тип соединения другой (TCP, WS) то берем адрес откуда пришло
	if (c_point.type != sip_transport_udp_e)
	{
		c_point.address = m_route.remoteAddress;
		c_point.port = m_route.remotePort;
	}

	int len = 0;

	if (c_point.port == 0 || c_point.port == 5060)
		len = std_snprintf(buffer, size, "%s\r\nsip:%s@%s", (const char*)m_call_id, m_account.get_username(), inet_ntoa(c_point.address));
	else
		len = std_snprintf(buffer, size, "%s\r\nsip:%s@%s:%u", (const char*)m_call_id, m_account.get_username(), inet_ntoa(c_point.address), c_point.port);

	buffer[len] = 0;

	return len;
}
//--------------------------------------
//
//--------------------------------------
int sip_registrar_session_t::get_contacts(rtl::PonterArrayT<sip_registrar_contact_info_t>& contacts) const
{
	for (int i = 0; i < m_contact_list.getCount(); i++)
	{
		sip_registrar_contact_info_t* info = m_contact_list[i]->get_info();

		info->set_over_nat_flag(m_over_nat);
		info->set_real_address(m_real_sender_address);
		info->set_route(m_route);
		info->set_via(m_via);
		info->set_callid(m_call_id);

		contacts.add(info);
	}

	return contacts.getCount();
}
//--------------------------------------
//
//--------------------------------------
bool sip_registrar_session_t::check_contact(sip_registrar_contact_info_t* info) const 
{
	/// найдем по адресу и получим контакт

	for (int i = 0; i < m_contact_list.getCount(); i++)
	{
		const sip_registrar_contact_t* contact = m_contact_list[i];

		const sip_transport_point_t& cpoint = contact->get_point();

		/*if (cpoint.type != sip_transport_udp_e)
		{*/
			const sip_transport_route_t& info_route = info->getRoute();

			char tmp1[128];
			char tmp2[128];
			SLOG_CALL("reg-sess", "%s : check contact %s and %s!", (const char*)m_call_id, point_to_string(tmp1, 128, cpoint), route_to_string(tmp2, 128, info_route));

			if (cpoint.address.s_addr == info_route.remoteAddress.s_addr && cpoint.port == info_route.remotePort && cpoint.type == info_route.type)
			{
				info->set_over_nat_flag(m_over_nat);
				return true;
			}
		//}
		//else
		//{
		//	char tmp1[128];
		//	char tmp2[128];

		//	/*in_addr info_address = info->get_via().address;
		//	uint16_t info_port = info->get_via().port;*/
		//	const sip_transport_point_t& vpoint = info->get_via();
		//	const sip_transport_route_t& info_route = info->getRoute();

		//	SLOG_CALL("reg-sess", "%s : check contact %s and via %s!", m_call_id, point_to_string(tmp1, 128, cpoint), point_to_string(tmp2, 128, vpoint));
		//	SLOG_CALL("reg-sess", "%s : check contact %s and route %s!", m_call_id, point_to_string(tmp1, 128, cpoint), route_to_string(tmp2, 128, info_route));

		//	if (cpoint.address.s_addr == vpoint.address.s_addr && cpoint.port == vpoint.port && cpoint.type == vpoint.type)
		//	{
		//		info->set_over_nat_flag(m_over_nat);
		//		return true;
		//	}
		//}

	}

	return false;
}
//--------------------------------------
//
//--------------------------------------
sip_registrar_contact_info_t* sip_registrar_session_t::get_sip_contact() const 
{
	sip_registrar_contact_info_t* info = nullptr;

	if (m_contact_list.getCount() > 0)
	{
		info = m_contact_list[0]->get_info();

		info->set_user(m_account.get_username());
		info->set_over_nat_flag(m_over_nat);
		info->set_real_address(m_real_sender_address);
		info->set_route(m_route);
		info->set_via(m_via);
		info->set_callid(m_call_id);
	}

	return info;
}
//--------------------------------------
//
//--------------------------------------
void sip_registrar_session_t::reset_expires()
{
	for (int i = 0; i < m_contact_list.getCount(); i++)
	{
		m_contact_list[i]->set_expires(0);
	}
}
//--------------------------------------
//
//--------------------------------------
bool sip_registrar_session_t::check_transport_disconnected(const sip_transport_route_t& route)
{
	if (route.type != m_route.type)
		return false;
	
	/*char r1[64];
	char r2[64];*/

	//SLOG_CALL("reg-sess", "%s : check_transport_disconnected '%s' with %s", m_call_id, route_to_string(r1, 64, route), route_to_string(r2, 64, m_route));

	if (route.type == sip_transport_udp_e)
	{
		// если интерфейс совпадает то истина для всех
		if (route.if_address.s_addr == m_route.if_address.s_addr)
		{
			// 0 - свалился слушатель!
			if (route.remoteAddress.s_addr == 0 || route.remoteAddress.s_addr == m_route.remoteAddress.s_addr)
			{
				SLOG_CALL("reg-sess", "%s : transport disconnected!", (const char*)m_call_id);
				return true;
			}
		}
	}
	else if (memcmp(&m_route, &route, sizeof(m_route)) == 0)
	{
		SLOG_CALL("reg-sess", "%s : transport disconnected!", (const char*)m_call_id);
		return true;
	}

	return false;
}
//--------------------------------------
// новая сессия
//--------------------------------------
bool sip_registrar_session_t::process_request(const sip_message_t& request, bool over_nat)
{
	rtl::String call_id = request.CallId();
	rtl::String method;
	int seq_no = -1;

	if (call_id.isEmpty())
	{
		SLOG_WARN("reg-sess", "%s : failed : request has no Call-Id header", (const char*)call_id);
		return false;
	}

	m_over_nat = over_nat;

	// call-id в первую очередь
	if (m_call_id.isEmpty())
	{
		m_call_id = call_id;
	}

	seq_no = request.CSeq_Number();
	method = request.CSeq_Method();

	SLOG_CALL("reg-sess", "%s : processing request CSeq:%d %s...", (const char*)m_call_id, seq_no, (const char*)method);

	if (seq_no == -1 || method.isEmpty())
	{
		SLOG_WARN("reg-sess", "%s : failed : request has no CSeq header", (const char*)call_id);
		return false;
	}
	
	if (uint32_t(seq_no) < m_cseq)
	{
		SLOG_CALL("reg-sess", "%s : packet with CSeq:%d olfder than current CSeq:%d", (const char*)m_call_id, seq_no, m_cseq);
		return true;
	}

	m_cseq = seq_no; // обновим cseq;

	// обновим маршрут
	const sip_transport_route_t& r_route = request.getRoute();
	if (m_route != r_route)
	{
		//@spec: transport: 1, 6 : захват транспорта сессией
		if (!is_route_empty(m_route))
		{
			char tmp2[128];
			SLOG_CALL("reg-sess", "%s : releasing route %s!!", (const char*)m_call_id, route_to_string(tmp2, 128, m_route));
			m_account.get_registrar().get_stack().release_route(m_route, "reg-session");
		}

		m_route = r_route;
		m_account.get_registrar().get_stack().open_route(m_route, "reg-session");
	}

	const sip_value_via_t* via = request.get_Via_value();
	if (!via)
	{
		SLOG_WARN("reg-sess", "%s : failed : request has no Via headers", (const char*)m_call_id);
		return false;
	}

	m_via.address = net_t::get_host_by_name(via->get_address());
	m_via.port = via->get_port();
	m_via.type = via->get_protocol();

	const sip_header_t* OktellProxy = request.get_custom_header("OktellProxy-RcvFromAddr"); // OktellProxy-RcvFromAddr

	if (OktellProxy)
	{
		m_real_sender_address.s_addr = inet_addr(OktellProxy->get_top_value()->value());
	}

	m_last_time = time(nullptr);

	// удаляем старые контакты!
	char event_contact_uri[256];
	get_top_contact(event_contact_uri, 256);

	clear_contacts();

	const sip_value_number_t* expires = request.get_Expires_value();
	uint32_t expires_value = REG_NO_EXPIRES_VALUE;
	uint32_t max_expires = (uint32_t)m_account.get_max_expires();

	if (expires)
	{
		expires_value = expires->get_number();

		if (max_expires != 0 && expires_value > max_expires)
		{
			expires_value = max_expires;
		}
	}

	SLOG_CALL("reg-sess", "%s : set contacts...", (const char*)m_call_id);

	// обработка списка контактов!
	const sip_value_contact_t* contact;
	bool event_contact_defined = false;

	int contact_count = request.get_Contact()->get_value_count();

	for (int i = 0; i < contact_count; i++)
	{
		if (contact = request.get_Contact_value(i))
		{
			SLOG_CALL("reg-sess", "%s : contact[%d] %s...", (const char*)m_call_id, 0, (const char*)contact->to_string());

			const sip_uri_t& contact_uri = contact->get_uri();

			SLOG_CALL("reg-sess", "%s : add contact URI[%d] %s...", (const char*)m_call_id, 0, (const char*)contact_uri.to_string());

			if (contact_uri.get_scheme() *= "sip")
			{
				sip_registrar_contact_t* record = NEW sip_registrar_contact_t();

				if (uri_to_contact(*contact, *record))
				{
					if (record->get_expires() == REG_NO_EXPIRES_VALUE)
					{
						record->set_expires((uint32_t)expires_value == REG_NO_EXPIRES_VALUE ? REG_DEF_EXPIRES_VALUE : expires_value);
					}

					if (record->get_point().port == 0)
					{
						record->set_port(REG_DEF_SIP_PORT);
					}

					SLOG_CALL("reg-sess", "%s : add contact[%d,%d] %s %s:%u exp:%u", (const char*)m_call_id, 0, 0, TransportType_toString(record->get_point().type), inet_ntoa(record->get_point().address), record->get_point().port, record->get_expires());

					if (record->get_expires() > 0)
					{
						if (max_expires != 0 && record->get_expires() > max_expires)
						{
							record->set_expires(max_expires);
						}

						if (expires_value == REG_NO_EXPIRES_VALUE)
						{
							expires_value = record->get_expires();
						}

						m_contact_list.add(record);

						if (!event_contact_defined)
						{
							int len = contact_to_string(*record, event_contact_uri, 256);
							event_contact_uri[len] = 0;
							event_contact_defined = true;
						}

						record = nullptr;
					}
				}

				if (record != nullptr)
				{
					DELETEO(record);
				}

				break;
			}
		}
	}

	SLOG_CALL("reg-sess", "%s : %d contacts added (expires:%d)...", (const char*)m_call_id, m_contact_list.getCount(), expires_value);

	// вернется ответ 200 Ok
	m_account.send_reg_200_ok(*this, request, expires_value);

	return true;
}
//--------------------------------------
//
//--------------------------------------
sip_registrar_contact_state_t sip_registrar_session_t::check_expired_time(const sip_registrar_contact_t& contact, time_t current_time)
{
	intptr_t expires = current_time - m_last_time;

	const sip_transport_point_t& cpoint = contact.get_point();

	if (contact.get_expires() == 0)
	{
		SLOG_CALL("reg-sess", "contact %s:%u set expires by zero", inet_ntoa(cpoint.address), cpoint.port, contact.get_expires());
		return sip_registrar_contact_expires_e;
	}

	if (expires < contact.get_expires())
	{
		return sip_registrar_contact_live_e;
	}

	uint64_t overtime = expires - contact.get_expires();

	if (overtime < 10)
	{
		SLOG_CALL("reg-sess", "contact %s:%u add expires %u overtime", inet_ntoa(cpoint.address), cpoint.port, overtime);
		return sip_registrar_contact_overtime_e;
	}

	SLOG_CALL("reg-sess", "contact %s:%u set expires by timer", inet_ntoa(cpoint.address), cpoint.port);

	return sip_registrar_contact_expires_e;
}
//--------------------------------------
//
//--------------------------------------
bool sip_registrar_session_t::check_contacts(time_t current_time)
{
	char contact_uri[256];
	int len = get_top_contact(contact_uri, 256);
	contact_uri[len] = 0;

	for (int i = m_contact_list.getCount() - 1; i >= 0 ; i--)
	{
		sip_registrar_contact_t* contact = m_contact_list[i];

		if (check_expired_time(*contact, current_time) == sip_registrar_contact_expires_e)
		{
			const sip_transport_point_t& cpoint = contact->get_point();
			SLOG_CALL("reg-sess", "%s : contact %s:%u expired", (const char*)m_call_id, inet_ntoa(cpoint.address), cpoint.port);
			m_contact_list.removeAt(i);
			// удалить из списка у освободить память!
			DELETEO(contact);
		}
	}

	return m_contact_list.getCount() > 0;
}
//--------------------------------------
//
//--------------------------------------
void sip_registrar_session_t::clear_contacts()
{
	for (int i = 0; i < m_contact_list.getCount(); i++)
	{
		sip_registrar_contact_t* contact = m_contact_list[i];
		DELETEO(contact);
	}

	m_contact_list.clear();
}
//--------------------------------------
//
//--------------------------------------
bool sip_registrar_session_t::uri_to_contact(const sip_value_contact_t& contact_uri, sip_registrar_contact_t& record)
{
	/*
	// контактная информация
	sip_transport_type_t type;
	char display_name[MAX_PATH+1];
	char address[MAX_PATH+1];
	uint16_t port;

	// время начала и продолжительность жизни записи
	uint64_t reg_start;
	uint32_t reg_expires;
	*/

	const sip_uri_t& uri = contact_uri.get_uri();

	rtl::String display_name = contact_uri.get_display_name();
	sip_utils::UnQuote(display_name);
	const rtl::String& address = uri.get_host();
	const rtl::String& port = uri.get_port();
	uint32_t expires = contact_uri.get_expires();
	const rtl::String& transport = uri.get_parameters().get("transport");
	sip_transport_point_t cp = {sip_transport_udp_e, 0, 0};

	// если не указан транспорт то UDP
	if (!transport.isEmpty())
	{
		cp.type = parse_transport_type(transport);
	}

	if (!display_name.isEmpty())
	{
		record.set_display_name(display_name);
	}

	if (!address.isEmpty())
	{
		cp.address = net_t::get_host_by_name(address);
	}
	else
	{
		return false;
	}

	if (!port.isEmpty())
	{
		cp.port = (uint16_t)strtoul(port, nullptr, 10);
	}
	else
	{
		cp.port = 5060;
	}

	record.set_point(cp);
	record.set_expires(expires);

	const mime_param_list_t& params = uri.get_parameters();

	rtl::String params_str;

	int params_count = params.getCount();

	for (int i = 0; i < params_count; i++)
	{
		const mime_param_t* mp = params.getAt(i);
		const rtl::String& name = mp->getName();
		const rtl::String& value = mp->getValue();
		
		params_str << name;

		if (!value.isEmpty())
			params_str << '=' << value;

		if (i < params_count - 1)
		{
			params_str << ';';
		}
	}

	if (!params_str.isEmpty())
		record.set_uri_params(params_str);

	// определимся с адресом и портом по принципу:

	// r_contact == invalid (обычно для TCP)
	// contact = r_recv (отправляем запросы на контакт)

	const sip_transport_point_t& cpoint = record.get_point();

	if (m_route.type != sip_transport_udp_e)// || m_cisco_phone)
	{
		record.set_point(m_route.type, m_route.remoteAddress, m_route.remotePort);
	}
	else
	{
		if (cpoint.address.s_addr == INADDR_NONE || cpoint.address.s_addr == INADDR_ANY)
		{
			record.set_point(m_route.type, m_route.remoteAddress, m_route.remotePort);
		}

		// r_via == r_recv
		// contact = r_contact (отправляем запросы на контакт)

		// r_via != r_recv && r_contact == r_via
		// contact = r_recv (отправляем запросы на NAT адрес)
		if (m_via.address.s_addr != m_route.remoteAddress.s_addr && m_via.address.s_addr == cpoint.address.s_addr && m_via.port == cpoint.port)
		{
			record.set_point(m_route.type, m_route.remoteAddress, m_route.remotePort);
		}
	}

	// r_via != r_recv && r_contact != r_via
	// contact = r_recv (отправляем запросы на контакт на свой страх и риск)

	return true;
}
//--------------------------------------
//
//--------------------------------------
int sip_registrar_session_t::contact_to_string(const sip_registrar_contact_t& contact, char* buffer, int size)
{
	// по умолчанию udp и адрес берем из контакта
	sip_transport_point_t cpoint = contact.get_point();

	// если тип соединения другой (TCP, WS) то берем адрес откуда пришло
	if (cpoint.type != sip_transport_udp_e)// || m_cisco_phone)
	{
		cpoint.address = m_route.remoteAddress;
		cpoint.port = m_route.remotePort;
	}

	int len = 0;

	if (cpoint.port == 0 || cpoint.port == 5060)
		len = std_snprintf(buffer, size, "%s\r\nsip:%s@%s", (const char*)m_call_id, m_account.get_username(), inet_ntoa(cpoint.address));
	else
		len = std_snprintf(buffer, size, "%s\r\nsip:%s@%s:%u", (const char*)m_call_id, m_account.get_username(), inet_ntoa(cpoint.address), cpoint.port);

	buffer[len] = 0;

	return len;
}
//--------------------------------------
//
//--------------------------------------
sip_registrar_contact_t::sip_registrar_contact_t() : m_reg_expires(REG_NO_EXPIRES_VALUE)
{
	m_cp.type = sip_transport_udp_e;
	m_cp.port = 0,
	m_cp.address.s_addr = INADDR_ANY;
}
//--------------------------------------
//
//--------------------------------------
sip_registrar_contact_t::sip_registrar_contact_t(const sip_registrar_contact_t& contact)
{
	m_display_name = contact.m_display_name;
	m_cp = contact.m_cp;
	m_reg_expires = contact.m_reg_expires;
	m_uri_params = contact.m_uri_params;
}
//--------------------------------------
//
//--------------------------------------
sip_registrar_contact_t::~sip_registrar_contact_t()
{
}
//--------------------------------------
//
//--------------------------------------
sip_registrar_contact_t& sip_registrar_contact_t::operator = (const sip_registrar_contact_t& contact)
{
	m_display_name = contact.m_display_name;
	m_cp = contact.m_cp;
	m_reg_expires = contact.m_reg_expires;
	m_uri_params = contact.m_uri_params;

	return *this;
}
//--------------------------------------
//
//--------------------------------------
sip_registrar_contact_info_t* sip_registrar_contact_t::get_info()
{
	sip_registrar_contact_info_t* info = NEW sip_registrar_contact_info_t();

	info->set_contact(m_cp);
	info->set_contact_params(m_uri_params);
	info->set_expires(m_reg_expires);

	return info;
}
//--------------------------------------
