/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_parser_tools.h"
#include "sip_message.h"
//--------------------------------------
//
//--------------------------------------
sip_message_t::sip_message_t() :
	m_request(false), m_request_method(sip_custom_method_e), m_response_code(sip_000), m_body_length(0), m_body(nullptr)
{
	memset(m_std_headers, 0, sizeof m_std_headers);
	memset(&m_route, 0, sizeof(m_route));
	m_over_nat = false;
	m_has_trans = false;
	m_ua_name = "RTX";
	m_version = "SIP/2.0";
	m_retransmission = false;
}
//--------------------------------------
//
//--------------------------------------
sip_message_t::sip_message_t(const sip_message_t& sip_packet) :
m_request(false), m_request_method(sip_custom_method_e), m_response_code(sip_000), m_body_length(0), m_body(nullptr)
	
{
	memset(m_std_headers, 0, sizeof m_std_headers);
	memset(&m_route, 0, sizeof(m_route));
	m_over_nat = false;
	m_has_trans = false;
	m_ua_name = "RTX";
	m_retransmission = false;

	assign(sip_packet);
}
//--------------------------------------
//
//--------------------------------------
sip_message_t::sip_message_t(const raw_packet_t& raw_packet) :
m_request(false), m_request_method(sip_custom_method_e), m_response_code(sip_000), m_body_length(0), m_body(nullptr)
{
	memset(m_std_headers, 0, sizeof m_std_headers);
	memset(&m_route, 0, sizeof(m_route));
	m_over_nat = false;
	m_has_trans = false;
	m_ua_name = "RTX";
	m_retransmission = false;

	assign(raw_packet);
}
//--------------------------------------
//
//--------------------------------------
sip_message_t::~sip_message_t()
{
	cleanup();
}
//--------------------------------------
//
//--------------------------------------
sip_message_t&  sip_message_t::assign(const sip_message_t& sip_packet)
{
	cleanup();

	/// ��������� �����
	m_request = sip_packet.m_request;
	
	m_request_method = sip_packet.m_request_method;
	m_request_uri = sip_packet.m_request_uri;
	
	//m_version = sip_packet.m_version;
	m_version = sip_packet.m_version.isEmpty() ? rtl::String("SIP/2.0") : sip_packet.m_version;
	
	m_response_code = sip_packet.m_response_code;
	m_response_reason = sip_packet.m_response_reason;

	/// ��������� �� �������� � ����������� �������������� �����
	for (int i = 0; i < sip_header_max_e; i++)
	{
		const sip_header_t* header = sip_packet.m_std_headers[i];

		if (header != nullptr)
		{
			m_std_headers[i] = NEW sip_header_t(*header);
		}
	}

	/// ������������� ����
	for (int i = 0; i < sip_packet.m_custom_headers.getCount(); i++)
	{
		const sip_header_t* header = sip_packet.m_custom_headers[i];

		if (header != nullptr)
		{
			m_custom_headers.add(NEW sip_header_t(*header));
		}
	}

	/// ���� ���������
	if (sip_packet.m_body_length > 0 && sip_packet.m_body != nullptr)
	{
		m_body_length = sip_packet.m_body_length;;
		m_body = (uint8_t*)MALLOC(m_body_length+1);
		memcpy(m_body, sip_packet.m_body, m_body_length);
		m_body[m_body_length] = 0;
	}

	/// ��������� ������
	m_route = sip_packet.m_route;
	m_over_nat = sip_packet.m_over_nat;
	m_trans_id = sip_packet.m_trans_id;
	m_has_trans = sip_packet.m_has_trans;
	m_ua_name = sip_packet.m_ua_name;
	m_retransmission = sip_packet.m_retransmission;

	return *this;

}
//--------------------------------------
//
//--------------------------------------
sip_message_t& sip_message_t::assign(const raw_packet_t& raw_packet)
{
	cleanup();

	/// ��������� �����
	
	m_version = raw_packet.get_start_line().get_version();
	if (m_version.isEmpty())
		m_version = "SIP/2.0";
	

	if ((m_request = raw_packet.get_start_line().is_request()))
	{
		m_request_method = sip_parse_method_type(raw_packet.get_start_line().get_method());
		m_request_uri = raw_packet.get_start_line().get_request_uri();
		if (m_request_method == sip_custom_method_e)
			m_request_method_name = raw_packet.get_start_line().get_method();
	}
	else
	{
		m_response_code = (sip_status_t)strtoul(raw_packet.get_start_line().get_code(), nullptr, 10);
		m_response_reason = raw_packet.get_start_line().get_reason();
	}

	// ������� �� �����
	for (int i = 0; i < raw_packet.get_header_count(); i++)
	{
		add_header(raw_packet.get_header_at(i));
	}

	m_body_length = raw_packet.get_body_length();

	if (m_body_length > 0)
	{
		m_body = (uint8_t*)MALLOC(m_body_length+1);
		memcpy(m_body, raw_packet.get_body(), m_body_length);
		m_body[m_body_length] = 0;
	}

	/// ��������� ������
	m_route = raw_packet.getRoute();
	m_over_nat = false;
	m_trans_id.setEmpty();
	m_has_trans = false;
	m_ua_name.setEmpty();
	m_retransmission = false;

	return *this;
}
//--------------------------------------
//
//--------------------------------------
void sip_message_t::cleanup()
{
	m_request = false;
	
	m_request_method = sip_custom_method_e;
	m_request_uri.cleanup();

	m_version.setEmpty();

	m_response_code = sip_000;
	m_response_reason.setEmpty();

	m_body_length = 0;

	if (m_body != nullptr)
	{
		FREE(m_body);
		m_body = nullptr;
	}
	
	for (int i = 0; i < sip_header_max_e; i++)
	{
		sip_header_t* hdr = m_std_headers[i];
		if (hdr != nullptr)
		{
			DELETEO(hdr);
		}
	}

	memset(m_std_headers, 0, sizeof m_std_headers);

	for (int i = 0; i < m_custom_headers.getCount(); i++)
	{
		sip_header_t* header = m_custom_headers[i];
		if (header != nullptr)
			DELETEO(header);
	}

	m_custom_headers.clear();

	memset(&m_route, 0, sizeof(m_route));
	m_over_nat = false;
	m_has_trans = false;
	m_ua_name = "RTX";
	m_version = "SIP/2.0";
	m_retransmission = false;
}
//--------------------------------------
// helper method
//--------------------------------------
void sip_message_t::set_request_method(sip_method_type_t type)
{
	m_request_method = type;
	m_request_method_name.setEmpty();
	CSeq_Method(sip_get_method_name(type));
}
//--------------------------------------
// helper method
//--------------------------------------
void sip_message_t::set_request_method(const rtl::String& method)
{
	m_request_method = sip_parse_method_type(method);
	
	if (m_request_method == sip_custom_method_e)
	{
		m_request_method_name = method;
		CSeq_Method(method);
	}
	else
	{
		m_request_method_name.setEmpty();
		CSeq_Method(sip_get_method_name(m_request_method));
	}
}
//--------------------------------------
//
//--------------------------------------
rtl::String& sip_message_t::write_to(rtl::String& stream) const
{
	if (m_request)
	{
		stream << get_request_method_name();
		stream << ' ';
		m_request_uri.write_to(stream);
		stream << " SIP/2.0\r\n";
	}
	else
	{
		stream << "SIP/2.0 ";
		stream << m_response_code << ' ';
		stream << m_response_reason << "\r\n";
	}

	// Via
	write_header(stream, m_std_headers[sip_Via_e]);

	// From
	write_header(stream, m_std_headers[sip_From_e]);
	
	// To
	write_header(stream, m_std_headers[sip_To_e]);

	// Call-Id
	write_header(stream, m_std_headers[sip_Call_ID_e]);

	// CSeq
	write_header(stream, m_std_headers[sip_CSeq_e]);

	// Contact
	write_header(stream, m_std_headers[sip_Contact_e]);

	// standard headers
	for (int i = 0; i < sip_header_max_e; i++)
	{
		sip_header_t* header = m_std_headers[i];
		
		if (header == nullptr)
			continue;
		
		sip_header_type_t type = (sip_header_type_t)i;
		if (type == sip_Via_e || type == sip_From_e || type == sip_To_e || type == sip_CSeq_e || type == sip_Call_ID_e || type == sip_Contact_e)
			continue;

		write_header(stream, header);
	}

	// custom headers
	for (int i = 0; i < m_custom_headers.getCount(); i++)
	{
		write_header(stream, m_custom_headers[i]);
	}

	stream << "\r\n";

	// ������ ����� (sdp)!
	if (m_body != nullptr && m_body_length > 0)
	{
		stream.append((const char*)m_body, m_body_length);
	}

	return stream;
}
//--------------------------------------
// helper method
//--------------------------------------
rtl::String& sip_message_t::write_header(rtl::String& stream, const sip_header_t* header) const
{
	if (header != nullptr && header->get_value_count() > 0)
	{
		if (header->getType() == sip_Record_Route_e || header->getType() == sip_Route_e)
			write_header_milti(stream, *header);
		else
			write_header_single(stream, *header);
	}
	return stream;
}
rtl::String& sip_message_t::write_header_milti(rtl::String& stream, const sip_header_t& header) const
{
	int value_count = header.get_value_count();
	for (int i = 0; i < value_count; i++)
	{
		const sip_value_t* value = header.get_value_at(i);
		if (value != nullptr)
		{
			stream << header.getName() << ": ";
			value->write_to(stream);
			stream << "\r\n";
		}
	}

	return stream;
}
rtl::String& sip_message_t::write_header_single(rtl::String& stream, const sip_header_t& header) const
{
	int value_count = header.get_value_count();

	if (value_count > 0)
	{
		stream << header.getName() << ": ";
		for (int i = 0; i < value_count; i++)
		{
			const sip_value_t* value = header.get_value_at(i);
			value->write_to(stream);
			if (i < (value_count-1))
				stream << ", ";
		}
	}

	stream << "\r\n";

	return stream;
}
//--------------------------------------
//
//--------------------------------------
void sip_message_t::set_request_line(sip_method_type_t method, sip_uri_t& request_uri, const rtl::String& version)
{
	m_request = true;
	m_request_method = method;
	m_request_method_name = rtl::String::empty;
	m_request_uri = request_uri;
	m_version = version.isEmpty() ? rtl::String("SIP/2.0") : version;
}
//--------------------------------------
//
//--------------------------------------
void sip_message_t::set_request_line(const rtl::String& custom, sip_uri_t& request_uri, const rtl::String& version)
{
	m_request = true;
	m_request_method = sip_parse_method_type(custom);
	m_request_method_name = m_request_method == sip_custom_method_e ? custom : rtl::String::empty;
	m_request_uri = request_uri;
	m_version = version.isEmpty() ? rtl::String("SIP/2.0") : version;
}
//--------------------------------------
//
//--------------------------------------
void sip_message_t::set_response_line(sip_status_t code, const rtl::String& reason, const rtl::String& version)
{
	m_request = false;

	m_response_code = code;
	m_response_reason = reason.isEmpty() ? sip_get_reason_text(code) : reason;
	m_version = version.isEmpty() ? rtl::String("SIP/2.0") : version;

	m_request_method = sip_custom_method_e;
	m_request_method_name.setEmpty();
	m_request_uri.cleanup();
}
//--------------------------------------
//
//--------------------------------------
sip_header_t* sip_message_t::make_std_header(sip_header_type_t type)
{
	if (m_std_headers[type] == nullptr)
	{
		m_std_headers[type] = NEW sip_header_t(type);
	}

	return m_std_headers[type];
}
//--------------------------------------
//
//--------------------------------------
sip_header_t* sip_message_t::get_custom_header(const char* name)
{
	for (int i = 0; i < m_custom_headers.getCount(); i++)
	{
		sip_header_t* header = m_custom_headers[i];

		if (std_stricmp(header->getName(), name) == 0)
			return header;
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
const sip_header_t* sip_message_t::get_custom_header(const char* name) const
{
	for (int i = 0; i < m_custom_headers.getCount(); i++)
	{
		sip_header_t* header = m_custom_headers[i];

		if (std_stricmp(header->getName(), name) == 0)
			return header;
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
void sip_message_t::set_std_header(const sip_header_t* h)
{
	if (h == nullptr)
		return;

	sip_header_t** sh = &m_std_headers[h->getType()];

	if (*sh == nullptr)
		*sh = NEW sip_header_t(*h);
	else
		**sh = *h;
}
//--------------------------------------
//
//--------------------------------------
void sip_message_t::set_std_header(const sip_header_t& h)
{
	sip_header_t** sh = &m_std_headers[h.getType()];

	if (*sh == nullptr)
		*sh = NEW sip_header_t(h);
	else
		**sh = h;
}
//--------------------------------------
//
//--------------------------------------
void sip_message_t::set_custom_header(const sip_header_t& h_name)
{
	sip_header_t* hdr = get_custom_header(h_name.getName());

	if (hdr != nullptr)
	{
		*hdr = h_name;
	}
	else
	{
		hdr = h_name.clone();
		m_custom_headers.add(hdr);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_message_t::remove_std_header(sip_header_type_t type)
{
	if (m_std_headers[type] != nullptr)
	{
		DELETEO(m_std_headers[type]);
		m_std_headers[type] = nullptr;
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_message_t::remove_custom_header(const char* name)
{
	for (int i = 0; i < m_custom_headers.getCount(); i++)
	{
		sip_header_t* header = m_custom_headers[i];

		if (std_stricmp(header->getName(), name) == 0)
		{
			m_custom_headers.removeAt(i);
			DELETEO(header);
			return;
		}
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_message_t::set_body(const void* body, int length)
{
	if (m_body != nullptr)
	{
		FREE(m_body);
		m_body = nullptr;
		m_body_length = 0;

		make_Content_Length_value()->set_number(0);
	}

	if (body != nullptr && length > 0)
	{
		m_body_length = length;
		m_body = (uint8_t*)MALLOC(m_body_length + 1);

		memcpy(m_body, body, length);
		m_body[m_body_length] = 0;

		make_Content_Length_value()->set_number(length);
	}
}
void sip_message_t::set_body(const char* body)
{
	set_body((const void*)body, (int)strlen(body));
}
//--------------------------------------
//
//--------------------------------------
bool sip_message_t::has_sdp() const
{
	if (m_body != nullptr && m_body_length > 0)
	{
		const sip_value_t* c = get_Content_Type_value();
		return (c != nullptr) && (c->value() *= "application/sdp");
	}

	return false;
}
//--------------------------------------
//
//--------------------------------------
sip_value_t* sip_message_t::make_std_value(sip_header_type_t h_name)
{
	if (m_std_headers[h_name] == nullptr)
	{
		m_std_headers[h_name] = NEW sip_header_t(h_name);
	}
	
	return m_std_headers[h_name]->make_value(0);
}
//--------------------------------------
//
//--------------------------------------
bool sip_message_t::IsValid(bool minimal) const
{
	if (m_version != "SIP/2.0")
		return false;

	if (m_request)
	{
		if (m_request_method == sip_method_type_t::sip_custom_method_e && m_request_method_name.isEmpty())
			return false;
	}
	else
	{
		if (m_response_code == sip_000 || m_response_code == sip_MaxCode)
			return false;
	}

	const sip_value_cseq_t* cseq = get_CSeq_value();

	if (cseq == nullptr || cseq->get_method().isEmpty())
		return false;

	if (m_request && !(cseq->get_method() *= get_request_method_name()))
		return false;

	const sip_value_t* callid = get_Call_ID_value();
	if (callid == nullptr || callid->to_string().isEmpty())
		return false;

	if (!minimal)
	{
		const sip_value_uri_t* from = get_From_value();
		if (!from || from->get_uri().get_host().isEmpty())
			return false;

		const sip_value_uri_t* to = get_To_value();
		if (!to || to->get_uri().get_host().isEmpty())
			return false;

		const sip_header_t* via = get_Via();
		if (via == nullptr || via->get_value_count() == 0)
			return false;

	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool sip_message_t::IsUnregister() const
{
	if (m_request_method != sip_REGISTER_e)
		return false;

	const sip_value_number_t* expires = get_Expires_value();

	if (expires != nullptr && expires->get_number() == 0)
		return true;

	const sip_value_contact_t* contact = get_Contact_value();

	if (contact != nullptr && contact->get_expires() == 0)
		return true;

	return false;
}
//--------------------------------------
//
//--------------------------------------
void sip_message_t::add_header(const raw_header_t& raw_header)
{
	sip_header_type_t header_type = sip_parse_header_type(raw_header.name);

	if (header_type == sip_custom_header_e)
	{
		sip_header_t* header = get_custom_header(raw_header.name);
		
		if (header == nullptr)
		{
			header = NEW sip_header_t(raw_header.name);
			m_custom_headers.add(header);
		}

		header->add_value(raw_header.value);
	}
	else
	{
		if (m_std_headers[header_type] == nullptr)
		{
			m_std_headers[header_type] = NEW sip_header_t(header_type);
		}

		m_std_headers[header_type]->add_value(raw_header.value);
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
bool sip_message_t::FixViaNATParameters()
{
	sip_value_via_t* via = get_Via_value();

	if (!via)
		return false;

	if (net_t::ip_is_valid(m_route.remoteAddress))
	{
		via->set_received_address(m_route.remoteAddress);
		via->set_rport(m_route.remotePort);
	}

	return true;
}

bool sip_message_t::make_response(sip_message_t& response, uint16_t statusCode, const rtl::String& reasonPhrase) const
{
	response.set_response_line((sip_status_t)statusCode, reasonPhrase);


	/** Clone the From header */
	response.set_std_header(m_std_headers[sip_From_e]);

	/** Clone the CSEQ */
	response.set_std_header(m_std_headers[sip_CSeq_e]);

	/** Clone the call ID */
	response.set_std_header(m_std_headers[sip_Call_ID_e]);

	/** Clone the To header */
	response.set_std_header(m_std_headers[sip_To_e]);
	
	sip_value_user_t* to = response.get_To_value();
	
	if (statusCode != sip_100_Trying)
	{
		if ((IsInvite() || IsRegister() || IsSubscribe()) && To_Tag().isEmpty())
		{
			/// only set the tag parameter if its empty
			to->set_tag(sip_utils::GenTagParameter());
		}
	}

	response.set_route(m_route);

	response.set_std_header(m_std_headers[sip_Via_e]);

	/// clone the record route if there is any
	response.set_std_header(m_std_headers[sip_Record_Route_e]);

	return true;
}
//--------------------------------------
