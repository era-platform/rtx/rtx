/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "sip_transaction_is.h"
#include "sip_transaction_manager.h"
#include "sip_transport_manager.h"
//--------------------------------------
//
//--------------------------------------
static const char* GetStateName(int state)
{
	static const char* names[] = { "Idle", "Trying", "Proceeding", "Completed", "Confirmed", "Terminated" };

	return state >= 0 && state <= sip_transaction_is_t::is_transaction_state_terminated_e ? names[state] : "Unknown";
}
//--------------------------------------
//
//--------------------------------------
sip_transaction_is_t::sip_transaction_is_t(const trans_id& transactionId, sip_transaction_manager_t& manager) :
sip_transaction_t(transactionId, sip_transaction_ist_e, manager)
{
	SLOG_TRANS(get_type_name(), "%s Created with Id: %s", m_internal_id, (const char*)transactionId.AsString());
}
//--------------------------------------
//
//--------------------------------------
sip_transaction_is_t::~sip_transaction_is_t()
{
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_is_t::process_sip_transaction_event(sip_message_t & message)
{
	//if (m_call_id.isEmpty())
	//{
	//	m_call_id = message.CallId();
	//}

	SLOG_TRANS(get_type_name(), "%s State.%s", m_internal_id, GetStateName(m_state));

	switch (m_state)
	{
	case is_transaction_state_idle_e:
		process_idle_state(message);
		break;
	case is_transaction_state_proceeding_e:
		process_proceeding_state(message);
		break;
	case is_transaction_state_completed_e:
		process_completed_state(message);
		break;
	case is_transaction_state_confirmed_e:
		process_confirmed_state(message);
		break;
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_is_t::process_idle_state(sip_message_t & message)
{
	/*
	When 
	a server transaction is constructed for a request, it enters the
	"Proceeding" state.  The server transaction MUST generate a 100
	(Trying) response unless it knows that the TU will generate a
	provisional or final response within 200 ms, in which case it MAY
	generate a 100 (Trying) response.  This provisional response is
	needed to quench request retransmissions rapidly in order to avoid
	network congestion.  The 100 (Trying) response is constructed
	according to the procedures in Section 8.2.6, except that the
	insertion of tags in the To header field of the response (when none
	was present in the request) is downgraded from MAY to SHOULD NOT.
	The request MUST be passed to the TU.
	*/

	/// double check via before sending a reply
	const sip_value_via_t* via = message.get_Via_value();

	if (message.getRoute().type != sip_transport_ws_e && message.getRoute().type != sip_transport_wss_e)
	{
		if (via->get_address().isEmpty() || via->get_address() == "0.0.0.0")
		{
			SLOG_WARN(get_type_name(), "%s Handle State.Idle : terminating -- invalid Via header (%s)", m_internal_id, (const char*)via->to_string());

			set_state(is_transaction_state_terminated_e);
			destroy();
			return;
		}
		else 
		{
			/// check if the address is valid
			rtl::String via_address = via->get_address();
			in_addr viaAddr;
			viaAddr.s_addr = inet_addr(via_address);
			if (!net_t::ip_is_valid(viaAddr))
			{
				/// still not valid, check if it might be a domain
				/// very light check.  Just get a hint if it start with 
				/// a number and assume an invalid ip address
				char ch = via_address[0];
				if (ch >= 0 && ch < 128 && ::isdigit(ch))
				{
					SLOG_WARN(get_type_name(), "%s Handle State.Idle : terminating -- invalid Via address (%s)", m_internal_id, (const char*)via_address);

					set_state(is_transaction_state_terminated_e);
					destroy();
					return;
				}
			}
		}
	}

	set_opening_request(message);

	// check if XOR hash is enabled

	m_manager.process_received_message_event(message, *this);
	set_state(is_transaction_state_proceeding_e);
	send_proceeding(message);

	SLOG_TRANS(get_type_name(), "%s State.Idle -> State.Proceeding", m_internal_id);
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_is_t::process_proceeding_state(sip_message_t & message)
{
	/*The TU passes any number of provisional responses to the server
	transaction.  So long as the server transaction is in the
	"Proceeding" state, each of these MUST be passed to the transport
	layer for transmission.  They are not sent reliably by the
	transaction layer (they are not retransmitted by it) and do not cause
	a change in the state of the server transaction.  If a request
	retransmission is received while in the "Proceeding" state, the most
	recent provisional response that was received from the TU MUST be
	passed to the transport layer for retransmission.  A request is a
	retransmission if it matches the same server transaction based on the
	rules of Section 17.2.3.

	If, while in the "Proceeding" state, the TU passes a 2xx response to
	the server transaction, the server transaction MUST pass this
	response to the transport layer for transmission.  It is not
	retransmitted by the server transaction; retransmissions of 2xx
	responses are handled by the TU.  The server transaction MUST then
	transition to the "Terminated" state.

	While in the "Proceeding" state, if the TU passes a response with
	status code from 300 to 699 to the server transaction, the response
	MUST be passed to the transport layer for transmission, and the state
	machine MUST enter the "Completed" state.  For unreliable transports,
	timer G is set to fire in T1 seconds, and is not set to fire for
	reliable transports.

	This is a change from RFC 2543, where responses were always
	retransmitted, even over reliable transports.

	When the "Completed" state is entered, timer H MUST be set to fire in
	64*T1 seconds for all transports.  Timer H determines when the server
	transaction abandons retransmitting the response.  Its value is
	chosen to equal Timer B, the amount of time a client transaction will
	continue to retry sending a request.  If timer G fires, the response
	is passed to the transport layer once more for retransmission, and
	timer G is set to fire in MIN(2*T1, T2) seconds.  From then on, when
	timer G fires, the response is passed to the transport again for
	transmission, and timer G is reset with a value that doubles, unless
	that value exceeds T2, in which case it is reset with the value of
	T2.  This is identical to the retransmit behavior for requests in the
	"Trying" state of the non-INVITE client transaction.  Furthermore,
	while in the "Completed" state, if a request retransmission is
	received, the server SHOULD pass the response to the transport for
	retransmission.

	If an ACK is received while the server transaction is in the
	"Completed" state, the server transaction MUST transition to the
	"Confirmed" state.  As Timer G is ignored in this state, any
	retransmissions of the response will cease.

	If timer H fires while in the "Completed" state, it implies that the
	ACK was never received.  In this case, the server transaction MUST
	transition to the "Terminated" state, and MUST indicate to the TU
	that a transaction failure has occurred.

	The purpose of the "Confirmed" state is to absorb any additional ACK
	messages that arrive, triggered from retransmissions of the final
	response.  When this state is entered, timer I is set to fire in T4
	seconds for unreliable transports, and zero seconds for reliable
	transports.  Once timer I fires, the server MUST transition to the
	"Terminated" state.

	Once the transaction is in the "Terminated" state, it MUST be
	destroyed immediately.  As with client transactions, this is needed
	to ensure reliability of the 2xx responses to INVITE.
	*/


	if (message.is_request() && !message.IsAck())
	{
		/// retrnasmit the last response sent by the TU
		//m_TransportSender(m_last_response, (INT)this);

		SLOG_TRANS(get_type_name(), "%s (!ACK) State.Proceeding : send message to transport", m_internal_id);

		send_message_to_transport(m_last_response);
	}
	else
	{
		m_last_response = message;

		STR_COPY(m_ua_core_name, m_last_response.GetUACoreName());

		uint16_t statusCode = message.get_response_code();

		//// 1xx
		if (statusCode >= 100 && statusCode < 200) 
		{
			SLOG_TRANS(get_type_name(), "%s (1xx) State.Proceeding : send message to transport", m_internal_id);
			send_message_to_transport(m_last_response);
			//// 2xx Response
		}
		else if (statusCode >= 200 && statusCode < 300)
		{
			/// act as if we have received an ACK for none-2xx response
			stop_timer(sip_timer_G_e);
			stop_timer(sip_timer_H_e);
			send_message_to_transport(m_last_response);

			if (!is_reliable_transport())
			{
				SLOG_TRANS(get_type_name(), "%s (2xx) State.Proceeding --> State.Confirmed", m_internal_id);
				set_state(is_transaction_state_confirmed_e);
				start_timer(sip_timer_I_e, SIP_INTERVAL_I);
			}
			else
			{
				SLOG_TRANS(get_type_name(), "%s (2xx) State.Proceeding --> State.Terminated", m_internal_id);
				set_state(is_transaction_state_terminated_e);
				destroy();
			}
			return;

			//// 3xx, 4xx, 5xx Response
		}
		else if (statusCode >= 300)
		{
			SLOG_TRANS(get_type_name(), "%s (>= 3xx) State.Proceeding --> State.Completed", m_internal_id);
			set_state(is_transaction_state_completed_e);

			start_timer(sip_timer_H_e, SIP_INTERVAL_H);


			/// check if via needs a reliable transport
			const sip_value_via_t* via = message.get_Via_value();
			if (!via)
			{
				SLOG_WARN(get_type_name(), "%s State.Proceeding : message has no via!", m_internal_id);
				return;
			}

			rtl::String proto = TransportType_toString(via->get_protocol());

			if (!is_reliable_transport())
				start_timer(sip_timer_G_e, SIP_INTERVAL_G);


			SLOG_TRANS(get_type_name(), "%s (2xx) State.Proceeding : send last response", m_internal_id);

			/// tell the manager that we are expecting an ACK
			m_manager.add_ack_transaction(m_last_response, this);
			send_message_to_transport(m_last_response);
		}
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_is_t::process_completed_state(sip_message_t & message)
{
	if (message.is_request())
	{
		/// check if its an ACK
		if (message.IsAck())
		{
			stop_timer(sip_timer_G_e);
			stop_timer(sip_timer_H_e);

			if (!is_reliable_transport())
			{
				SLOG_TRANS(get_type_name(), "%s (ACK) State.Proceeding --> State.Confirmed", m_internal_id);
				set_state(is_transaction_state_confirmed_e);
				start_timer(sip_timer_I_e, SIP_INTERVAL_I);
			}
			else
			{
				SLOG_TRANS(get_type_name(), "%s (ACK) State.Proceeding --> State.Terminated", m_internal_id);
				m_manager.remove_ack_transaction(m_last_response, this);
				set_state(is_transaction_state_terminated_e);
				destroy();
			}
		}
		else
		{
			/// Not an ACK
			/// retrnasmit the last response sent by the TU
			///m_TransportSender(m_last_response, (INT)this);
			SLOG_TRANS(get_type_name(), "%s (!ACK) State.Proceeding : send last response", m_internal_id);
			
			send_message_to_transport(m_last_response);
		}
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_is_t::process_confirmed_state(sip_message_t & /*message*/)
{
	/// we are not expecting any event here
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_is_t::process_sip_timer_event(sip_transaction_timer_t timerType, uint32_t dwInterval)
{
	SLOG_TRANS(get_type_name(), "%s Timer event %s state %s",
			m_internal_id, sip_timer_manager_t::GetTimerName(timerType), GetStateName(m_state));

	switch (timerType)
	{
	case sip_timer_life_span_e:
		set_state(is_transaction_state_terminated_e);
		m_manager.process_timer_expire_event(timerType, *this);
		destroy();
		break;
	case sip_timer_G_e:
		if (m_state == is_transaction_state_completed_e)
		{
			/// retransmit
			///m_TransportSender(m_last_response, (INT)this);
			send_message_to_transport(m_last_response);

			/// retstart timer with compounded interval 

			uint32_t newInterval = dwInterval < SIP_INTERVAL_T2 ? dwInterval * 2 : SIP_INTERVAL_T2;

			start_timer(sip_timer_G_e, newInterval);

			SLOG_TRANS(get_type_name(), "%s Timer G : retransmit response and restart timer G (%d ms).", m_internal_id, newInterval);

		}
		break;
	case sip_timer_H_e:
		if (m_state == is_transaction_state_completed_e)
		{
			stop_timer(sip_timer_G_e);
			SLOG_TRANS(get_type_name(), "%s Timer H : Terminating", m_internal_id);
			set_state(is_transaction_state_terminated_e);
			m_manager.process_timer_expire_event(timerType, *this);
			m_manager.remove_ack_transaction(m_last_response, this);
			destroy();
		}
		break;
	case sip_timer_I_e:
		if (m_state == is_transaction_state_confirmed_e)
		{
			SLOG_TRANS(get_type_name(), "%s Timer I : Terminating", m_internal_id);

			m_manager.remove_ack_transaction(m_last_response, this);
			destroy();
			set_state(is_transaction_state_terminated_e);
		}
		break;
	default:
		SLOG_WARN(get_type_name(), "%s %s : Invalid timer tick received!", m_internal_id, sip_timer_manager_t::GetTimerName(timerType));
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_is_t::send_proceeding(const sip_message_t & request)
{
	sip_message_t proceeding;
	request.make_response(proceeding, sip_100_Trying);

	m_last_response = proceeding;

	/// DO NOT SEND.  The transport should have take cared of sending it
	///send_message_to_transport(proceeding);
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_is_t::process_sip_transaction_cancel_event()
{
	if (m_state != is_transaction_state_terminated_e)
	{
		SLOG_TRANS(get_type_name(), "%s CancelEvent --> State.Terminated", m_internal_id);
		set_state(is_transaction_state_completed_e);

		start_timer(sip_timer_H_e, SIP_INTERVAL_H);

		sip_message_t response;
		m_opening_request.make_response(response, sip_487_RequestTerminated);

		m_last_response = response;
		/// tell the manager that we are expecting an ACK
		m_manager.add_ack_transaction(response, this);
		///m_TransportSender(response, (INT)this);
		send_message_to_transport(response);
	}
}
//--------------------------------------
