/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_transport.h"
#include "sip_transport_listener.h"
#include "sip_transport_packet.h"
//--------------------------------------
//
//--------------------------------------
sip_transport_t::sip_transport_t(const sip_transport_point_t& iface, const sip_transport_point_t& endpoint, net_socket_t& sock) :
sip_transport_object_t(sock), m_user_count(0), m_keep_alive_started(false), m_keep_alive_value(0)
{
	m_route.type = endpoint.type;
	m_route.if_address = iface.address;
	m_route.if_port = iface.port;
	m_route.remoteAddress = endpoint.address;
	m_route.remotePort = endpoint.port;

	m_key = MAKE_KEY64(m_route.remoteAddress, m_route.remotePort, m_route.type);
	m_last_activity = rtl::DateTime::getTicks();

	update_name(TransportType_toString(m_route.type));

	//RC_IncrementObject(RC_TRANSPORT);
}
//--------------------------------------
//
//--------------------------------------
sip_transport_t::~sip_transport_t()
{
	SLOG_NET("net", "%s(%d) : destructor -> user:%d", m_name, get_ref_count(), m_user_count);

	//RC_DecrementObject(RC_TRANSPORT);
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_t::data_ready()
{
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_t::raise_packet_read(raw_packet_t* packet)
{
	bool filtered = filter_packet(packet);
	
	if (!filtered)
	{
		SLOG_NET("net", "%s(%d) : raise packet arrival -- PUSHED TO UA", m_name, get_ref_count());
		
		sip_transport_manager_t::sip_transport_packet_read(this, packet);
	}
	else
	{
		SLOG_NET("net", "%s(%d) : raise packet arrival -- FILTERED", m_name, get_ref_count());

		raw_packet_t::release(packet);

		sip_transport_manager_t::sip_transport_parser_error(this);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_t::raise_packet_sent(const sip_message_t& message)
{
	sip_transport_manager_t::sip_transport_packet_sent(this, message);
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_t::raise_stopped()
{
	sip_transport_manager_t::sip_transport_disconnected(this);
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_t::raise_parser_error()
{
	sip_transport_manager_t::sip_transport_parser_error(this);
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_t::raise_send_error(const sip_message_t& message, int net_error)
{
	sip_transport_manager_t::sip_transport_send_error(this, message, net_error);
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_t::raise_recv_error(int net_error)
{
	sip_transport_manager_t::sip_transport_recv_error(this, net_error);
}
//--------------------------------------
// ��������� sip_async_object_t
//--------------------------------------
const char* sip_transport_t::async_get_name()
{
	return "sip-transport";
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_t::async_handle_call(uint16_t command, uintptr_t int_param, void* ptr_param)
{
	if (command == ASYNC_TRANSP_KEEPALIVE)
	{
		send_keep_alive();
	}
}
//--------------------------------------
//
//--------------------------------------
bool sip_transport_t::filter_packet(raw_packet_t* packet)
{
	bool filtered = sip_transport_manager_t::sip_listener_is_useragent_banned(nullptr, packet->get_useragent());
	
	if (filtered)
	{
		if (rtl::Logger::check_trace(TRF_BANNED | TRF_PROTO))
		{
			sip_transport_route_t msg_route = packet->getRoute();

			uint8_t* a = (uint8_t*)&msg_route.remoteAddress.s_addr;
			uint8_t* b = (uint8_t*)&msg_route.if_address.s_addr;
		
			TransLog.log("RECV", "%s %d Bytes FROM %d.%d.%d.%d:%u IFACE %d.%d.%d.%d:%u ---- BANNED BY USER-AGENT '%s'",
				TransportType_toString(msg_route.type), packet->get_packet_length(),
				a[0], a[1], a[2], a[3], msg_route.remotePort,
				b[0], b[1], b[2], b[3], msg_route.if_port, packet->get_useragent());
		}

		SLOG_NET_WARN("net", "%s(%d) : PACKET NOT PROCESSED -- USER-AGENT '%s' BANNED!", m_name, get_ref_count(), packet->get_useragent());
	}

	return filtered;
}
//--------------------------------------
//
//--------------------------------------
bool sip_transport_t::connect()
{
	return false;
}
//--------------------------------------
//
//--------------------------------------
//bool sip_transport_t::reconnect(const sip_transport_route_t& route)
//{
//	//...
//
//}
//--------------------------------------
//
//--------------------------------------
void sip_transport_t::disconnect()
{
	SLOG_NET_WARN("net", "%s(%d) : Abstract function disconnect called!!!", m_name, get_ref_count());
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_t::set_keep_alive_timer(int value)
{
	if (value == m_keep_alive_value)
		return;

	bool restart = m_keep_alive_started;

	stop_keep_alive();

	m_keep_alive_value = value;

	if (restart)
		start_keep_alive();
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_t::timer_event_callback(rtl::Timer* sender, uint32_t elapsed, void* user_data, int tid)
{
	rtl::async_call_manager_t::callAsync((sip_transport_t*)user_data, ASYNC_TRANSP_KEEPALIVE, 0, nullptr);
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_t::start_keep_alive()
{
	if (m_keep_alive_started || m_keep_alive_value <= 0)
		return;

	add_ref();
	rtl::Timer::setTimer(timer_event_callback, this, 0, m_keep_alive_value, true);
	m_keep_alive_started = true;
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_t::stop_keep_alive()
{
	if (!m_keep_alive_started)
		return;

	// disable keepalive
	m_keep_alive_value = 0;
	rtl::Timer::stopTimer(timer_event_callback, this, 0);
	release_ref();
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_t::send_keep_alive()
{
	// do nothing!
}
//--------------------------------------
//
//--------------------------------------
bool sip_transport_t::is_valid()
{
	return m_socket.get_handle() != INVALID_SOCKET;
}
//--------------------------------------
//
//--------------------------------------
int sip_transport_t::send_message(const sip_message_t& message)
{
	return false;
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_t::capture_(const char* user)
{
	add_ref();
	// ������� �������������
	long user_count = std_interlocked_inc(&m_user_count);

	SLOG_NET("net", "%s(%d) : captured : %d <- %s", m_name, get_ref_count(), user_count, user);
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_t::release_(const char* user)
{
	long user_count = std_interlocked_dec(&m_user_count);

	release_ref();

	if (user_count == 0)
	{
		// ���� ������������� ������ ��� �� ��������� �����
		SLOG_NET("net", "%s(%d) : released to 0. remove from pool <- %s", m_name, get_ref_count(), user);

		sip_transport_manager_t::get_pool().remove_transport(this);
	}
	else
	{
		SLOG_NET("net", "%s(%d) : released. users:%d <- %s", m_name, get_ref_count(), user_count, user);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_t::destroy()
{
	SLOG_NET("net", "%s(%d) : destroy. users:%d", m_name, get_ref_count(), m_user_count);

	stop_keep_alive();

	if (m_user_count > 0)
		return;

	m_readable = false;

	// ��������� ������� ������������ � ������������
	// ������ ������� ���������� ���� ���������� ������ == 0
	release_ref();

	SLOG_NET("net", "transport destroyed");
}
//--------------------------------------
