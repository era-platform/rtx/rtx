/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_trans_id.h"
#include "sip_message.h"
//--------------------------------------
//
//--------------------------------------
#define MAGIC_COOKIE "z9hG4bK"
//--------------------------------------
//
//--------------------------------------
trans_id::trans_id() : m_is_rfc3261_compliant(false)
{
}
//--------------------------------------
//
//--------------------------------------
trans_id::trans_id(const trans_id& tId) : m_is_rfc3261_compliant(false)
{
	operator = (tId);
}

trans_id::trans_id(const rtl::String& tId) : m_is_rfc3261_compliant(false)
{
	operator = (tId);
}

trans_id::trans_id(const sip_message_t& msg) : m_is_rfc3261_compliant(false)
{
	operator = (msg);
}

trans_id& trans_id::operator = (const trans_id& tId)
{
	m_state_machine = tId.m_state_machine;
	m_callid = tId.m_callid;
	m_branch = tId.m_branch;
	m_method = tId.m_method;

	m_is_rfc3261_compliant = tId.m_is_rfc3261_compliant;
	m_from_tag = tId.m_from_tag;
	m_cseq_number = tId.m_cseq_number;

	return *this;
}

trans_id& trans_id::operator = (const rtl::String& tId)
{

	rtl::StringList tokens;
	tId.split(tokens, '|');
	int size = tokens.getCount();

	if (size == 3)
	{
		m_callid = tokens[0];
		m_branch = tokens[1];
		m_method = tokens[2];
	}
	else if (size == 4)
	{
		m_state_machine = tokens[0];
		m_callid = tokens[1];
		m_branch = tokens[2];
		m_method = tokens[3];
	}
	else if (size == 5)
	{
		m_callid = tokens[0];
		m_branch = tokens[1];
		m_method = tokens[2];
		m_from_tag = tokens[3];
		m_cseq_number = tokens[4];
	}
	else if (size == 6)
	{
		m_state_machine = tokens[0];
		m_callid = tokens[1];
		m_branch = tokens[2];
		m_method = tokens[3];
		m_from_tag = tokens[4];
		m_cseq_number = tokens[5];
	}

	if (m_branch.indexOf(MAGIC_COOKIE) == BAD_INDEX)
		m_is_rfc3261_compliant = false;

	return *this;
}

rtl::String trans_id::AsString() const
{
	rtl::String strm;
	PrintOn(strm);
	return strm;
}

rtl::String& trans_id::PrintOn(rtl::String& strm) const
{
	if (!m_state_machine.isEmpty())
		strm << m_state_machine << "|";

	strm << m_callid << "|";
	strm << m_branch << "|";
	strm << m_method;

	if (!m_is_rfc3261_compliant)
	{
		if (!m_from_tag.isEmpty() && !m_cseq_number.isEmpty())
			strm << "|" << m_from_tag << "|" << m_cseq_number;
	}

	return strm;
}

trans_id& trans_id::operator = (const sip_message_t& msg)
{
	/*
	17.1.3 Matching Responses to Client Transactions

	1.  If the response has the same value of the branch parameter in
	the top Via header field as the branch parameter in the top
	Via header field of the request that created the transaction.

	2.  If the method parameter in the CSeq header field matches the
	method of the request that created the transaction.  The
	method is needed since a CANCEL request constitutes a
	different transaction, but shares the same value of the branch
	parameter.
	*/

	m_callid = msg.CallId();
	m_branch = msg.Via_Branch();

	if (m_branch.indexOf(MAGIC_COOKIE) != BAD_INDEX)
		m_is_rfc3261_compliant = true;

	m_method = msg.CSeq_Method();

	if (!m_is_rfc3261_compliant)
	{
		m_cseq_number = msg.CSeq_Number();
		m_from_tag = msg.From_Tag();
	}

	return *this;
}
//--------------------------------------
