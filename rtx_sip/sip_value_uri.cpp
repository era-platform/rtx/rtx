/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_value_uri.h"
//--------------------------------------
//
//--------------------------------------
sip_value_uri_t::~sip_value_uri_t()
{
	cleanup();
}
//--------------------------------------
//
//--------------------------------------
rtl::String& sip_value_uri_t::prepare(rtl::String& stream) const
{
	if (!m_display_name.isEmpty())
	{
		stream << '\"' << m_display_name << "\" ";
	}

	return stream << '<' << m_uri.to_string() << '>';
}
//--------------------------------------
//
//--------------------------------------
bool sip_value_uri_t::parse()
{
	m_display_name = rtl::String::empty;
	m_uri = rtl::String::empty;

	int index = m_value.indexOfAny("\"<");

	if (index == -1)
	{
		m_uri.assign(m_value, m_value.getLength());
		return true;
	}

	if (m_value[index] == '\"')
	{
		int last = m_value.indexOf('\"', index + 1);

		if (last == -1)
			return false;

		// "1234567890"
		// |          |
		m_display_name = m_value.substring(index + 1, last - (index + 1));

		index = m_value.indexOf('<', last + 1);

		if (index == -1)
		{
			m_uri.assign((const char*)m_value + (last + 1), m_value.getLength() - (last + 1));
		}
		else
		{
			last = m_value.indexOf('>', index+1);

			if (last == -1)
				return false;

			m_uri.assign((const char*)m_value + (index + 1), last - (index + 1));
		}
	}
	else
	{
		int last = m_value.indexOf('>', index+1);

		if (last == -1)
			return false;

		m_uri.assign((const char*)m_value + (index + 1), last - (index + 1));
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
void sip_value_uri_t::cleanup()
{
	m_display_name.setEmpty();
	m_uri.cleanup();

	sip_value_t::cleanup();
}
//--------------------------------------
//
//--------------------------------------
sip_value_uri_t& sip_value_uri_t::assign(const sip_value_uri_t& value)
{
	sip_value_t::assign(value);
	m_display_name = value.m_display_name;
	m_uri = value.m_uri;
	return *this;
}
//--------------------------------------
//
//--------------------------------------
sip_value_t* sip_value_uri_t::clone() const
{
	return NEW sip_value_uri_t(*this);
}
//--------------------------------------
