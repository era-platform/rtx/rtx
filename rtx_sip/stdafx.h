/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "std_basedef.h"
#include "std_typedef.h"
#include "std_funcdef.h"

#if defined (TARGET_OS_WINDOWS)
//--------------------------------------
// ��� ������������� � VS2005, VS2008, VS2010
//--------------------------------------
//#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#define _CRT_NON_CONFORMING_SWPRINTFS
//#define _USE_32BIT_TIME_T
#define __STDC_LIMIT_MACROS
#define _CRT_RAND_S
#pragma setlocale("ru-RU")
#endif
//--------------------------------------
// Standard Header Files:
//--------------------------------------
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <assert.h>
#include <inttypes.h>
#include <limits.h>
#include <string.h>
#include <ctype.h>
#include <wchar.h>
//--------------------------------------
//
//--------------------------------------
#include "std_mem_checker.h"
//--------------------------------------
//
//--------------------------------------
#if defined (DEBUG_MEMORY)
#pragma warning(disable : 4291)

void* operator new(size_t size, char* file, int line);
void* operator new[](size_t size, char* file, int line);
void operator delete (void* mem_ptr);
void operator delete[](void* mem_ptr);

#define NEW new (__FILE__, __LINE__)
#define DELETEO(ptr) delete (ptr)
#define DELETEAR(ptr) delete[] (ptr)
#define MALLOC(size) std_mem_checker_malloc(size, __FILE__, __LINE__, rc_malloc_malloc_e)
#define FREE(ptr) std_mem_checker_free(ptr, rc_malloc_free_e)
#define REALLOC(ptr, size) std_mem_checker_realloc(ptr, size, __FILE__, __LINE__)
#else
#define NEW new
#define DELETEO(ptr) delete ptr
#define DELETEAR(ptr) delete[] ptr
#define MALLOC(size) malloc(size)
#define FREE(ptr) free(ptr)
#define REALLOC(ptr, size) realloc(ptr, size)
#endif

#if defined (TARGET_OS_WINDOWS)
//--------------------------------------
// Exclude rarely-used stuff from Windows headers
//--------------------------------------
#define WIN32_LEAN_AND_MEAN
#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0600
#endif						
#ifndef _WIN32_IE
#define _WIN32_IE 0x0600
#endif
#include <process.h>
#include <io.h>
//--------------------------------------
// Windows Header Files:
//--------------------------------------
#include <winsock2.h>
#include <ws2tcpip.h>
#include <ws2ipdef.h>
#include <iphlpapi.h>
#include <windows.h>
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#if defined(TARGET_OS_LINUX)
#include <sys/epoll.h>
#elif defined(TARGET_OS_FREEBSD)
#include <sys/event.h>
#endif
#include <netdb.h>
#endif
//--------------------------------------
//
//--------------------------------------
#include "rtx_stdlib.h"
#include "rtx_netlib.h"
#include "rtx_crypto.h"

extern rtl::Logger SLog;
extern rtl::Logger TransLog;

#define THREAD_PING_TIMEOUT 300000

#define SLOG_CALL		if (rtl::Logger::check_trace(TRF_CALL)) SLog.log
#define SLOG_ASYNC		if (rtl::Logger::check_trace(TRF_ASYNC)) SLog.log
#define SLOG_EVENT		if (rtl::Logger::check_trace(TRF_EVENTS)) SLog.log_event
#define SLOG_WARN		/*if (rtl::Logger::check_trace(TRF_WARNING))*/ SLog.log_warning
#define SLOG_ERROR		SLog.log_error
#define SLOG_ERR_MSG	SLog.log_error_message
#define SLOG_TRANS		if (rtl::Logger::check_trace(TRF_TRANS)) SLog.log
#define SLOG_SESS		if (rtl::Logger::check_trace(TRF_SESSION)) SLog.log
#define SLOG_NET		if (rtl::Logger::check_trace(TRF_NET)) SLog.log
#define SLOG_NET_WARN	if (rtl::Logger::check_trace(TRF_NET | TRF_WARNING)) SLog.log_warning
#define SLOG_NET_EVENT	if (rtl::Logger::check_trace(TRF_NET)) SLog.log_event
#define SLOG_TIMER		if (rtl::Logger::check_trace(TRF_TIMER)) SLog.log
#define SLOG_BINARY		if (rtl::Logger::check_trace(TRF_CALL)) SLog.log_binary
#define SLOG_NET_BINARY	if (rtl::Logger::check_trace(TRF_NET)) SLog.log_binary

/************************************************************************/
/// TRANSACTION definitions
#define ICT_MAX_REDIRECT_ATTEMPTS 10
#define SIP_MAX_TRANSACTION_LIFE_SPAN 300000 /// 5 minutes

/// STACK definitions
#define LOG_INSTANCE_COUNTER 0
#define FSM_THREAD_COUNT 10
#define NICT_USE_TIMER_K 1
#define SIP_UDP_DEFAULT_BUFFER_SIZE 32768
#define SIP_DEFAULT_MAX_FORWARDS 70
#define CLIENT_TRANSACTION_RETRANSMISSION_UNIT 20 /// retransmission unit for Timer B and F
/************************************************************************/
#define GS_HASH_KEY_1 0x5060
#define GS_HASH_KEY_2 0x50
/************************************************************************/

#define IS_STRING_EMPTY(s) (s == nullptr || s[0] == 0)

#include "sip_transport_manager.h"
#include "sip_registrar.h"
//--------------------------------------
