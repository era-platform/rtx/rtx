﻿/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_call_session.h"
#include "sip_call_session_manager.h"
#include "std_crypto.h"
//--------------------------------------
//
//--------------------------------------
const char* sip_call_session_t::GetSessionStateName(SessionState state)
{
	static const char* stateNames[] = 
	{
		"Idle",			"NewCall",			"Proceeding",
		"Alerting",		"Queued",			"MaxAnswerCall",
		"Redirecting",	"Cancelling",		"Connected",
		"CallTransfer",	"Disconnected",		"Final",
	};

	return state >= 0 && state < (sizeof(stateNames)/sizeof(const char*)) ? stateNames[state] : "UNKNOWN";
}
//--------------------------------------
// транк для замещения нулевых указателей
//--------------------------------------
class sip_null_trunk_t : public call_session_event_handler_t
{
public:
	sip_null_trunk_t() { }

	virtual ~sip_null_trunk_t();

	virtual void call_session__recv_1xx(sip_call_session_t& session, const sip_message_t& message);
	virtual void call_session__call_established(sip_call_session_t& session, const sip_message_t& message);
	virtual void call_session__call_disconnected(sip_call_session_t& session, const sip_message_t& message);

	virtual void call_session__ack_sent(sip_call_session_t& session);
	virtual void call_session__recv_ack(sip_call_session_t& session, const sip_message_t& msg);

	virtual reinvite_response_t call_session__recv_reinvite(sip_call_session_t& session, const sip_message_t& offer);
	virtual void call_session__call_reestablished(sip_call_session_t& session, const sip_message_t& message);

	virtual bool call_session__apply_sdp_offer(sip_call_session_t& session, const sip_message_t& offer);
	virtual bool call_session__prepare_sdp_answer(sip_call_session_t& session, sip_message_t& answer);
	virtual bool call_session__prepare_sdp_offer(sip_call_session_t& session, sip_message_t& offer);
	virtual bool call_session__apply_sdp_answer(sip_call_session_t& session, const sip_message_t& answer);

	virtual bool call_session__apply_sdp_reoffer(sip_call_session_t& session, const sip_message_t& offer);
	virtual bool call_session__prepare_sdp_reanswer(sip_call_session_t& session, sip_message_t& answer);
	virtual bool call_session__prepare_sdp_reoffer(sip_call_session_t& session, sip_message_t& offer, bool hold);
	virtual bool call_session__apply_sdp_reanswer(sip_call_session_t& session, const sip_message_t& anser);

	//virtual void call_session__recv_dtmf(sip_call_session_t& session, const rtl::String& digit, int duration);
	virtual bool call_session__recv_ni_request(sip_call_session_t& session, const sip_message_t& request, sip_message_t& response);
	virtual void call_session__recv_ni_response(sip_call_session_t& session, const sip_message_t& msg);

	virtual void call_session__failure(sip_call_session_t& session);
};
//--------------------------------------
//
//--------------------------------------
static sip_null_trunk_t g_null_trunk;
#define NULL_TRUNK (&g_null_trunk)
//--------------------------------------
// UAS, does NOT use profiles
//--------------------------------------
sip_call_session_t::sip_call_session_t(sip_call_session_manager_t& sessionManager, const sip_message_t& request) :
	sip_session_t((sip_session_manager_t &)sessionManager, request),
	m_call_manager(sessionManager),
	m_event_mutex("event-call-lock"),
	m_answer_call_mutex("answer-call-lock"),
	m_custom_headers_lock("sess-cust-hdr"),	
	m_proxy_auth_lock("call-auth-lock"),
	m_reinvite_lock("call-reinvite-lock")/*,
	m_session_timer_mutex("call-timer")*/
{
	m_call_end_reason = 0;

	m_current_uas_invite = request;
	m_trunk = NULL_TRUNK;
	m_log_tag = "CALL";
	m_last_ist_request_cseq = 0;
	m_last_ict_response_cseq = 0;

	m_state = StateIdle;

	m_has_sent_proxy_auth = false;
	m_can_send_cancel = false;
	m_send_cancel = false;
	m_cancel_param = false;
	//m_min_session_expire = 90;
	//m_max_session_expire = 3600;

	//m_enable_session_timer = false;
	m_ack_received = false;

	m_reinvite_state = ReinviteNone;

	m_has_auth = false;
	m_has_www_auth = false;
	m_invite_qop_count = 0;

	SLOG_SESS(m_log_tag, "%s -- Created Server from Call-Id: %s", m_session_ref, (const char*)m_call_id);

	//RC_IncrementObject(RC_CALL_SESSION);
}
//--------------------------------------
//
//--------------------------------------
sip_call_session_t::sip_call_session_t(sip_call_session_manager_t& manager, const sip_profile_t& profile, const char* callId) :
	sip_session_t((sip_session_manager_t&)manager, profile, callId),
	m_call_manager(manager),
	m_event_mutex("event-call-lock"),
	m_answer_call_mutex("answer-call-lock"),
	m_custom_headers_lock("sess-cust-hdr"),
	m_proxy_auth_lock("call-auth-lock"),
	m_reinvite_lock("call-reinvite-lock")/*,
	m_session_timer_mutex("call-timer")*/
{
	m_call_end_reason = 0;

	m_trunk = NULL_TRUNK;

	m_last_ist_request_cseq = 0;
	m_last_ict_response_cseq = 0;

	m_state = StateIdle;

	m_has_sent_proxy_auth = false;

	m_can_send_cancel = false;
	m_send_cancel = false;
	m_cancel_param = false;

	m_log_tag = "CALL";

	//m_min_session_expire = 90;
	//m_max_session_expire = 3600;
	//m_enable_session_timer = false;

	m_ack_received = false;

	m_reinvite_state = ReinviteNone;

	m_has_auth = false;
	m_has_www_auth = false;
	m_invite_qop_count = 0;

	SLOG_SESS(m_log_tag, "%s -- Created Client from Call-Id: %s", m_session_ref, (const char*)m_call_id);

	//RC_IncrementObject(RC_CALL_SESSION);
}
//--------------------------------------
//
//--------------------------------------
sip_call_session_t::~sip_call_session_t()
{
	clear_custom_headers();

	//RC_DecrementObject(RC_CALL_SESSION);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::process_destroy_event()
{
	rtl::MutexWatchLock lock(m_event_mutex, __FUNCTION__);

	call_session_event_handler_t* trunk = m_trunk;
	m_trunk = NULL_TRUNK;

	sip_session_t::process_destroy_event();

	SLOG_SESS(m_log_tag, "%s : destroying call session : disable trunk %p/%p", m_session_ref, trunk, m_trunk);
}
//--------------------------------------
//
//--------------------------------------
bool sip_call_session_t::make_call(call_session_event_handler_t* trunk)
{
	SLOG_SESS(m_log_tag, "%s : make call...", m_session_ref);

	if (trunk == nullptr)
	{
		SLOG_WARN(m_log_tag, "%s : make call : TRUNK is null !!!", m_session_ref);
		return false;
	}

	bool result = false;

	try
	{
		sip_message_t invite;

		/// create the request line
		sip_uri_t requestURI;
		
		requestURI.set_user(m_profile.get_request_user());
		requestURI.set_host(m_profile.get_domain());

		invite.set_request_line(sip_INVITE_e, requestURI);

		m_trunk = trunk;

		/// put the proxy as the static route
		invite.set_route(m_route);

		SLOG_SESS(m_log_tag, "%s : make call : set remote address to %s:%u %s",
			m_session_ref,
			inet_ntoa(m_profile.get_proxy_address()),
			m_profile.get_proxy_port(),
			TransportType_toString(m_profile.get_transport()));

		sip_value_via_t* via = invite.make_Via_value();
		
		via->set_address(inet_ntoa(m_profile.get_listener()));
		via->set_port(m_profile.get_listener_port());
		via->set_protocol(m_profile.get_transport());
		via->set_branch(sip_utils::GenBranchParameter());
		via->set_rport();

		SLOG_SESS(m_log_tag, "%s : make call via top address is %s:%u",
			m_session_ref,
			(const char*)via->get_address(),
			via->get_port());

		/// Set From header

		sip_value_user_t* from = invite.make_From_value();

		const rtl::String& displayName = m_profile.get_display_name();
		if (!displayName.isEmpty())
			from->set_display_name(displayName);

		sip_uri_t& from_uri = from->get_uri();

		from_uri.set_scheme("sip");
		from_uri.set_user(m_profile.get_username());
		from_uri.set_host(m_profile.get_domain());

		from->set_tag(sip_utils::GenTagParameter());

		/// Set To header
		//23.11.2010 Peter
		//  вообще в случае регистрации на сервере в TO следовало бы ставить IP адрес нашего сервера, а не его.
		sip_value_user_t* to = invite.make_To_value();
		sip_uri_t& to_uri = to->get_uri();
		to_uri.set_scheme("sip");
		to_uri.set_user(m_profile.get_to_user());
		to_uri.set_host(m_profile.get_domain());

		/// Set the call Id
		invite.CallId(m_call_id);

		/// Set the CSeq
		invite.CSeq(1, "INVITE");

		/// Set the contact
		sip_uri_t& curi = invite.make_Contact_value()->get_uri();

		//если есть контакты, используем верхний
		curi.set_scheme("sip");
		curi.set_user(m_profile.get_username());
		curi.set_host(via->get_address());
		curi.set_port(rtl::int_to_string(via->get_port()));
		curi.get_parameters().set("transport", TransportType_toString(m_profile.get_transport()));

		/// Set the allow header for Requests we support
		// INVITE, ACK, CANCEL, OPTIONS, BYE, REFER, NOTIFY, MESSAGE, SUBSCRIBE, INFO
		sip_header_t* allow = invite.make_Allow();
		allow->add_value("INVITE");
		allow->add_value("ACK");
		allow->add_value("CANCEL");
		allow->add_value("OPTIONS");
		allow->add_value("BYE");
		allow->add_value("REFER");
		allow->add_value("NOTIFY");
		allow->add_value("MESSAGE");

		/// Do not return false here.  The higher level applications
		/// might intentionally want to send media in ACK
		
		if (m_trunk == NULL_TRUNK || !m_trunk->call_session__prepare_sdp_offer(*this, invite))
		{
			SLOG_WARN(m_log_tag, "%s : make call : TRUNK HAS NO MEDIA!!!", m_session_ref);
			goto the_end;
		}

		SetCurrentUACRequest(invite);

		m_current_uac_invite = invite;


		{
			rtl::MutexWatchLock l_custom_headers(m_custom_headers_lock, __FUNCTION__);
			for (int i = 0; i < m_custom_headers.getCount(); i++)
			{
				sip_header_t* header = m_custom_headers[i];
				if (header != nullptr)
					invite.set_custom_header(*header);
			}
		}

		result = push_message_to_transaction(invite);
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR(m_log_tag, "%s : make call : Make call failed with exception %s", m_session_ref, ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
		result = false;
	}

the_end:
	return result;
}
bool sip_call_session_t::make_call_to_target(call_session_event_handler_t* trunk, const sip_value_uri_t& referTo, const sip_value_uri_t* referredBy)
{
	SLOG_SESS(m_log_tag, "%s : make call...", m_session_ref);

	if (trunk == nullptr)
	{
		SLOG_WARN(m_log_tag, "%s : make call : TRUNK is null !!!", m_session_ref);
		return false;
	}

	bool result = false;

	sip_value_t replaces;

	bool found;
	rtl::String rstr = referTo.get_uri().get_headers().get("Replaces", found);
	if (found)
		replaces.assign(rstr);

	try
	{
		sip_message_t invite;

		/// create the request line
		sip_uri_t requestURI = referTo.get_uri();
		requestURI.get_headers().clear();
		invite.set_request_line(sip_INVITE_e, requestURI);
		m_trunk = trunk;

		/// put the proxy as the static route
		invite.set_route(m_route);

		SLOG_SESS(m_log_tag, "%s : make call : set remote address to %s:%u %s",
			m_session_ref,
			inet_ntoa(m_profile.get_proxy_address()),
			m_profile.get_proxy_port(),
			TransportType_toString(m_profile.get_transport()));

		sip_value_via_t* via = invite.make_Via_value();

		via->set_address(inet_ntoa(m_profile.get_listener()));
		via->set_port(m_profile.get_listener_port());
		via->set_protocol(m_profile.get_transport());
		via->set_branch(sip_utils::GenBranchParameter());
		via->set_rport();

		SLOG_SESS(m_log_tag, "%s : make call via top address is %s:%u",
			m_session_ref,
			(const char*)via->get_address(),
			via->get_port());

		/// Set From header

		sip_value_user_t* from = invite.make_From_value();

		const rtl::String& displayName = m_profile.get_display_name();
		if (!displayName.isEmpty())
			from->set_display_name(displayName);

		sip_uri_t& from_uri = from->get_uri();

		from_uri.set_scheme("sip");
		from_uri.set_user(m_profile.get_username());
		from_uri.set_host(m_profile.get_domain());

		from->set_tag(sip_utils::GenTagParameter());

		/// Set To header
		//23.11.2010 Peter
		//  вообще в случае регистрации на сервере в TO следовало бы ставить IP адрес нашего сервера, а не его.
		sip_value_user_t* to = invite.make_To_value();
		to->get_uri() = invite.get_request_uri();

		/// Set the call Id
		invite.CallId(m_call_id);

		/// Set the CSeq
		invite.CSeq(1, "INVITE");

		/// Set the contact
		sip_uri_t& curi = invite.make_Contact_value()->get_uri();

		//если есть контакты, используем верхний
		curi.set_scheme("sip");
		curi.set_user(m_profile.get_username());
		curi.set_host(via->get_address());
		curi.set_port(rtl::int_to_string(via->get_port()));
		curi.get_parameters().set("transport", TransportType_toString(m_profile.get_transport()));

		/// Set the allow header for Requests we support
		// INVITE, ACK, CANCEL, OPTIONS, BYE, REFER, NOTIFY, MESSAGE, SUBSCRIBE, INFO
		sip_header_t* allow = invite.make_Allow();
		allow->add_value("INVITE");
		allow->add_value("ACK");
		allow->add_value("CANCEL");
		allow->add_value("OPTIONS");
		allow->add_value("BYE");
		allow->add_value("REFER");
		allow->add_value("NOTIFY");
		allow->add_value("MESSAGE");

		/// Do not return false here.  The higher level applications
		/// might intentionally want to send media in ACK

		if (m_trunk == NULL_TRUNK || !m_trunk->call_session__prepare_sdp_offer(*this, invite))
		{
			SLOG_WARN(m_log_tag, "%s : make call : TRUNK HAS NO MEDIA!!!", m_session_ref);
			goto the_end;
		}

		SetCurrentUACRequest(invite);

		m_current_uac_invite = invite;


		{
			rtl::MutexWatchLock l_custom_headers(m_custom_headers_lock, __FUNCTION__);
			for (int i = 0; i < m_custom_headers.getCount(); i++)
			{
				sip_header_t* header = m_custom_headers[i];
				if (header != nullptr)
					invite.set_custom_header(*header);
			}
		}

		result = push_message_to_transaction(invite);
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR(m_log_tag, "%s : make call : Make call failed with exception %s", m_session_ref, ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
		result = false;
	}

the_end:
	return result;
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::unbind_trunk_handler()
{
	m_trunk = NULL_TRUNK;
}
//--------------------------------------
//
//--------------------------------------
bool sip_call_session_t::process_incoming_sip_message(sip_event_message_t& messageEvent)
{
	/// make sure we never get deleted while we are doing something
	const sip_message_t& msg = messageEvent.get_message();
	trans_id tid = messageEvent.get_transaction_id();
	rtl::String fsm = tid.GetStateMachine();

	if (fsm == "IST" && msg.IsInvite() && m_state > sip_call_session_t::StateNewCall && m_state < sip_call_session_t::StateConnected)
	{
		SLOG_WARN(m_log_tag, "%s : Incoming Invite banned (404) : Rejecting", m_session_ref);

		sip_message_t response;
		// {SETTINGS}
		msg.make_response(response, sip_409_Conflict);
		push_message_to_transaction(response);
		return false;
	}

	bool result = sip_session_t::process_incoming_sip_message(messageEvent);

	if (fsm == "ICT")
	{
		if (msg.Is1xx())
			EnqueueMessageEvent(ICT_Recv1xx, msg);
		else if (msg.Is2xx())
			EnqueueMessageEvent(ICT_Recv2xx, msg);
		else if (msg.Is3xx())
			EnqueueMessageEvent(ICT_Recv3xx, msg);
		else if (msg.Is4xx())
			EnqueueMessageEvent(ICT_Recv4xx, msg);
		else if (msg.Is5xx())
			EnqueueMessageEvent(ICT_Recv5xx, msg);
		else if (msg.Is6xx())
			EnqueueMessageEvent(ICT_Recv6xx, msg);
		else
			EnqueueMessageEvent(ICT_RecvUnknown, msg);
	}
	else if (fsm == "NICT")
	{
		if (msg.Is1xx())
			EnqueueMessageEvent(NICT_Recv1xx, msg);
		else if (msg.Is2xx())
			EnqueueMessageEvent(NICT_Recv2xx, msg);
		else if (msg.Is3xx())
			EnqueueMessageEvent(NICT_Recv3xx, msg);
		else if (msg.Is4xx())
			EnqueueMessageEvent(NICT_Recv4xx, msg);
		else if (msg.Is5xx())
			EnqueueMessageEvent(NICT_Recv5xx, msg);
		else if (msg.Is6xx())
			EnqueueMessageEvent(NICT_Recv6xx, msg);
		else
			EnqueueMessageEvent(NICT_RecvUnknown, msg);
	}
	else if (fsm == "IST")
	{
		if (msg.IsInvite())
		{
			if (m_last_ist_request_cseq == 0 && m_type == sip_session_server_e)
			{
				/// this is a NEW invite
				m_last_ist_request_cseq = msg.CSeq_Number();

				EnqueueMessageEvent(IST_RecvInvite, msg);
			}
			else if (m_last_ist_request_cseq < msg.CSeq_Number())
			{
				// send trying
				SendTrying(msg);

				bool can_reinvite;

				{
					rtl::MutexWatchLock lock(m_reinvite_lock, __FUNCTION__);
					can_reinvite = m_reinvite_state == ReinviteNone;
					if (can_reinvite)
					{
						m_reinvite_state = ReinviteIncoming;
					}
				}

				if (!can_reinvite)
				{
					sip_session_t::SendAcceptByRejection(msg, sip_491_RequestPending);
				}
				else
				{
					// Renat+Peter 21.06.2011
					m_last_ist_request_cseq = msg.CSeq_Number();

					// если можно то стартуем обработку входящего реинвайта
					// если нет то ответ 491 RequestPending

					/// check if CSeq is higher
					EnqueueMessageEvent(IST_RecvMoreInvite, msg);
				}
			}
		}
		else if (msg.IsAck())
		{
			EnqueueMessageEvent(IST_RecvAck, msg);
		}
		else
		{
			EnqueueMessageEvent(IST_RecvUnknown, msg);
		}
	}
	else if (fsm == "NIST")
	{
		if (msg.IsBye())
		{
			EnqueueMessageEvent(NIST_RecvBye, msg);
		}
		else if (msg.IsOptions())
			EnqueueMessageEvent(NIST_RecvOptions, msg);
		else if (msg.IsInfo())
			EnqueueMessageEvent(NIST_RecvInfo, msg);
		else if (msg.IsCancel())
			EnqueueMessageEvent(NIST_RecvCancel, msg);
		else if (msg.IsNotify())
			EnqueueMessageEvent(NIST_RecvNotify, msg);
		else if (msg.IsSubscribe())
			EnqueueMessageEvent(NIST_RecvSubscribe, msg);
		else if (msg.IsRefer())
			EnqueueMessageEvent(NIST_RecvRefer, msg);
		else if (msg.IsMessage())
			EnqueueMessageEvent(NIST_RecvMessage, msg);
		else if (msg.IsUpdate())
			EnqueueMessageEvent(NIST_RecvUpdate, msg);
		else
			EnqueueMessageEvent(NIST_RecvUnknown, msg);
	}
	else
	{
		SLOG_WARN(m_log_tag, "%s : Incoming SIP message failed : message has Unknown state machine %s!", m_session_ref, (const char*)fsm);
		//PAssertAlways(PLogicError);
	}

	return result;
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::process_sip_message_sent_event(sip_event_tx_t& messageEvent)
{
	/// make sure we never get deleted while we are doing something

	sip_session_t::process_sip_message_sent_event(messageEvent);

	const sip_message_t& msg = messageEvent.get_message();
	trans_id tid = messageEvent.get_transaction_id();
	rtl::String fsm = tid.GetStateMachine();

	SLOG_SESS(m_log_tag, "%s : On SIP Message Sent %s %s", m_session_ref, (const char*)fsm, (const char*)tid.AsString());

	if (fsm == "ICT")
	{
		if (msg.IsInvite())
			EnqueueMessageEvent(ICT_InviteSent, msg);
		else if (msg.IsAck())
			EnqueueMessageEvent(ICT_AckSent, msg);
		else
			EnqueueMessageEvent(ICT_UnknownSent, msg);
	}
	else if (fsm == "NICT")
	{
		if (msg.IsBye())
			EnqueueMessageEvent(NICT_ByeSent, msg);
		else if (msg.IsOptions())
			EnqueueMessageEvent(NICT_OptionsSent, msg);
		else if (msg.IsInfo())
			EnqueueMessageEvent(NICT_InfoSent, msg);
		else if (msg.IsCancel())
			EnqueueMessageEvent(NICT_CancelSent, msg);
		else if (msg.IsNotify())
			EnqueueMessageEvent(NICT_NotifySent, msg);
		else if (msg.IsSubscribe())
			EnqueueMessageEvent(NICT_SubscribeSent, msg);
		else
			EnqueueMessageEvent(NICT_UnknownSent, msg);

	}
	else if (fsm == "IST")
	{
		if (msg.Is1xx())
			EnqueueMessageEvent(IST_1xxSent, msg);
		else if (msg.Is2xx())
			EnqueueMessageEvent(IST_2xxSent, msg);
		else if (msg.Is3xx())
			EnqueueMessageEvent(IST_3xxSent, msg);
		else if (msg.Is4xx())
			EnqueueMessageEvent(IST_4xxSent, msg);
		else if (msg.Is5xx())
			EnqueueMessageEvent(IST_5xxSent, msg);
		else if (msg.Is6xx())
			EnqueueMessageEvent(IST_6xxSent, msg);
		else
			EnqueueMessageEvent(IST_UnknownSent, msg);

	}
	else if (fsm == "NIST")
	{
		if (msg.Is1xx())
			EnqueueMessageEvent(NIST_1xxSent, msg);
		else if (msg.Is2xx())
			EnqueueMessageEvent(NIST_2xxSent, msg);
		else if (msg.Is3xx())
			EnqueueMessageEvent(NIST_3xxSent, msg);
		else if (msg.Is4xx())
			EnqueueMessageEvent(NIST_4xxSent, msg);
		else if (msg.Is5xx())
			EnqueueMessageEvent(NIST_5xxSent, msg);
		else if (msg.Is6xx())
			EnqueueMessageEvent(NIST_6xxSent, msg);
		else
			EnqueueMessageEvent(NIST_UnknownSent, msg);
	}
	else
	{
		if (msg.IsAck())
			EnqueueMessageEvent(ICT_AckSent, msg);
		if (msg.IsBye())
			EnqueueMessageEvent(NICT_ByeSent, msg);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::process_session_event(int event, const sip_message_t& eventMsg)
{
	SLOG_SESS(m_log_tag, "%s : session event %s message %s", m_session_ref, GetEventName(event), (const char*)eventMsg.GetStartLine());

	/// Handle one event at a time
	try
	{
		rtl::MutexWatchLock lock(m_event_mutex, __FUNCTION__);

		switch(event)
		{
			/// Base transport events
		case sip_session_t::TransportFault:
		case sip_session_t::TransportDisconnected:
			TransportFailure();
			break;
			/// Invite Client Transaction FEvents
		case ICT_Recv1xx:
			ICT_OnReceived1xx(eventMsg);
			break;
		case ICT_Recv2xx:
			ICT_OnReceived2xx(eventMsg);
			break;
		case ICT_Recv3xx:
			m_call_end_reason = ICT_Recv3xx;
			ICT_OnReceived3xx(eventMsg);
			break;
		case ICT_Recv4xx:
			m_call_end_reason = ICT_Recv4xx;
			ICT_OnReceived4xx(eventMsg);
			break;
		case ICT_Recv5xx:
			m_call_end_reason = ICT_Recv5xx;
			ICT_OnReceived5xx(eventMsg);
			if (m_state < StateConnected)
			{
				m_state = StateDisconnected;
			}
			break;
		case ICT_Recv6xx:
			m_call_end_reason = ICT_Recv6xx;
			ICT_OnReceived6xx(eventMsg);
			if (m_state < StateConnected)
			{
				m_state = StateDisconnected;
			}
			break;
		case ICT_RecvUnknown:
			ICT_OnReceivedUnknown(eventMsg);
			break;
		case ICT_InviteSent:
			ICT_OnInviteSent(eventMsg);
			break;
		case ICT_AckSent:
			ICT_OnAckSent(eventMsg);
			break;
		case ICT_UnknownSent:
			ICT_OnUnknownSent(eventMsg);
			break;
		case ICT_DisconnectCall:
			ICT_OnDisconnectCall();
			break;

			/// Invite Server Transaction Events
		case IST_RecvInvite:
			if (m_state == StateIdle)
			{
				m_state = StateNewCall;
				IST_OnReceivedInvite(eventMsg);
			}
			else if (m_state == StateNewCall)
			{
				SLOG_WARN(m_log_tag, "%s : Incoming Invite banned (404) : Rejecting",
					m_session_ref);

				sip_message_t response;
				eventMsg.make_response(response, sip_406_NotAcceptable);
				CheckAndReplaceTransport(eventMsg, response);
				push_message_to_transaction(response);
			}
			break;
		case IST_RecvMoreInvite:
			IST_OnReceivedMoreInvite(eventMsg);
			break;
		case IST_RecvAck:
			IST_OnReceivedAck(eventMsg);
			break;
		case IST_RecvUnknown:
			IST_OnReceivedUnknown(eventMsg);
			break;
		case IST_200OkRetransmitTimeout:
			IST_200OkRetransmitTimeoutReceived(eventMsg);
			break;
		case IST_1xxSent:
			IST_On1xxSent(eventMsg);
			break;
		case IST_2xxSent:
			m_state = StateConnected;
			IST_On2xxSent(eventMsg);
			break;
		case IST_3xxSent:
			m_call_end_reason = IST_3xxSent;
			IST_On3xxSent(eventMsg);
			break;
		case IST_4xxSent:
			m_call_end_reason = IST_4xxSent;
			IST_On4xxSent(eventMsg);
			break;
		case IST_5xxSent:
			m_call_end_reason = IST_5xxSent;
			IST_On5xxSent(eventMsg);
			break;
		case IST_6xxSent:
			m_call_end_reason = IST_6xxSent;
			IST_On6xxSent(eventMsg);
			break;
		case IST_UnknownSent:
			IST_OnUnknownSent(eventMsg);
			break;
		case IST_AnswerCallNow:                // Sends 200 OK back with SDP after receipt of INVITE
			InternalAnswerCall(AnswerCallNow, m_current_uas_invite);
			m_state = StateConnected;
			break;
		case IST_AnswerCallDeferred:           // Answers with 180 ringing
			InternalAnswerCall(AnswerCallDeferred, m_current_uas_invite);
			m_state = StateAlerting;
			break;
		case IST_AnswerCallDeferredWithMedia:  // Like AnswerCallDefered Only media is sent in 183 progress
			InternalAnswerCall(AnswerCallDeferredWithMedia, m_current_uas_invite);
			m_state = StateProceeding;
			break;
		//case IST_AnswerCallRedirect:           // Redirect the call to another address
		//	InternalAnswerCall(AnswerCallRedirect, m_current_uas_invite);
		//	m_state = StateRedirecting;
		//	break;
		//case IST_AnswerCallRedirectToProxy:    // Redirect the call to a proxy
		//	InternalAnswerCall(AnswerCallRedirectToProxy, m_current_uas_invite);
		//	m_state = StateRedirecting;
		//	break;
		case IST_AnswerCallQueued:             // Tell remote that the call is queued
			InternalAnswerCall(AnswerCallQueued, m_current_uas_invite);
			m_state = StateQueued;
			break;
		case IST_AnswerCallDenied:              // Reject the call
			InternalAnswerCall(AnswerCallDenied, m_current_uas_invite);
			if (m_state < StateConnected)
				m_state = StateDisconnected;
			break;
			/// None Invite Server Transaction Events
		case NIST_RecvBye:
			m_call_end_reason = NIST_RecvBye;
			if (m_state < StateConnected)
				m_state = StateDisconnected;
			NIST_OnReceivedBye(eventMsg);
			break;
		case NIST_RecvOptions:
			NIST_OnReceivedOptions(eventMsg);
			break;
		case NIST_RecvInfo:
			NIST_OnReceivedInfo(eventMsg);
			break;
		case NIST_RecvCancel:
			m_call_end_reason = NIST_RecvCancel;
			NIST_OnReceivedCancel(eventMsg);
			break;
		case NIST_RecvNotify:
			NIST_OnReceivedNotify(eventMsg);
			break;
		case NIST_RecvSubscribe:
			NIST_OnReceivedSubscribe(eventMsg);
			break;
		case NIST_RecvRefer:
			NIST_OnReceivedRefer(eventMsg);
			break;
		case NIST_RecvMessage:
			NIST_OnReceivedMessage(eventMsg);
			break;
		case NIST_RecvUpdate:
			NIST_OnReceivedUpdate(eventMsg);
			break;
		case NIST_RecvUnknown:
			NIST_OnReceivedUnknown(eventMsg);
			break;
		case NIST_1xxSent:
			NIST_On1xxSent(eventMsg);
			break;
		case NIST_2xxSent:
			NIST_On2xxSent(eventMsg);
			break;
		case NIST_3xxSent:
			NIST_On3xxSent(eventMsg);
			break;
		case NIST_4xxSent:
			NIST_On4xxSent(eventMsg);
			break;
		case NIST_5xxSent:
			NIST_On5xxSent(eventMsg);
			break;
		case NIST_6xxSent:
			NIST_On6xxSent(eventMsg);
			break;
		case NIST_UnknownSent:
			NIST_OnUnknownSent(eventMsg);
			break;
			/// None Invite Client Transaction Events
		case NICT_Recv1xx:
			NICT_OnReceived1xx(eventMsg);
			break;
		case NICT_Recv2xx:
			NICT_OnReceived2xx(eventMsg);
			break;
		case NICT_Recv3xx:
			NICT_OnReceived3xx(eventMsg);
			break;
		case NICT_Recv4xx:
			NICT_OnReceived4xx(eventMsg);
			break;
		case NICT_Recv5xx:
			NICT_OnReceived5xx(eventMsg);
			break;
		case NICT_Recv6xx:
			NICT_OnReceived6xx(eventMsg);
			break;
		case NICT_RecvUnknown:
			NICT_OnReceivedUnknown(eventMsg);
			break;
		case NICT_ByeSent:
			m_call_end_reason = NICT_ByeSent;
			NICT_OnByeSent(eventMsg);
			break;
		case NICT_OptionsSent:
			NICT_OnOptionsSent(eventMsg);
			break;
		case NICT_InfoSent:
			NICT_OnInfoSent(eventMsg);
			break;
		case NICT_CancelSent:
			m_call_end_reason = NICT_CancelSent;
			NICT_OnCancelSent(eventMsg);
			break;
		case NICT_NotifySent:
			NICT_OnNotifySent(eventMsg);
			break;
		case NICT_SubscribeSent:
			NICT_OnSubscribeSent(eventMsg);
			break;
		case NICT_UnknownSent:
			NICT_OnUnknownSent(eventMsg);
			break;
		default:
			break;
		}
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR(m_log_tag, "%s failed while handling event %s: %s", m_session_ref, GetEventName(event), ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}
}
//--------------------------------------
// получен инвайт
//--------------------------------------
void sip_call_session_t::IST_OnReceivedInvite(const sip_message_t& msg)
{
	try
	{
		rtl::MutexWatchLock lock(m_answer_call_mutex, __FUNCTION__);

		// как он суда попал?
		// реинвайт -- изменение параметров сессии
		if (m_state == StateConnected)
		{
			IST_OnReceivedMoreInvite(msg);
			return;
		}
		else if (m_state == StateCancelling)
		{
			if (m_trunk != NULL_TRUNK)
				m_trunk->call_session__call_disconnected(*this, msg);

			StartAutoDestructTimer(30);
			return;
		}

		sip_message_t response;

		m_current_uas_invite = msg;

		SLOG_SESS(m_log_tag, "%s : Incoming connection!", m_session_ref);

		m_trunk = m_call_manager.process_incoming_connection(msg, *this);

		if (m_trunk == nullptr)
		{
			m_trunk = NULL_TRUNK;
		}

		// событие входящего звонка приходит именно NULL
		if (m_trunk == NULL_TRUNK)
		{
			SLOG_WARN(m_log_tag, "%s : Incoming Session failed (404) : Rejecting incoming INVITE : %s", m_session_ref, (const char*)msg.GetStartLine());

			msg.make_response(response, sip_404_NotFound);

			CheckAndReplaceTransport(msg, response);

			push_message_to_transaction(response);

			StartAutoDestructTimer(30);
			m_trunk = NULL_TRUNK;
			return;
		}

		/// IncomingConnection Allowed this call to process
		/// check for presence of SDP

		m_route.type = m_profile.get_transport();
		m_route.if_address = m_profile.get_interface();
		m_route.if_port = m_profile.get_interface_port();
		m_route.remoteAddress = m_profile.get_proxy_address();
		m_route.remotePort = m_profile.get_proxy_port();

		// маршрут уже открыт

		bool hasSDP = msg.has_sdp();

		if (hasSDP)
		{
			if (m_trunk == NULL_TRUNK || !m_trunk->call_session__apply_sdp_offer(*this, msg))
			{
				SLOG_WARN(m_log_tag, "%s : Incoming SDP offer failed (415) : Rejecting incoming INVITE %s", m_session_ref, (const char*)msg.GetStartLine());

				msg.make_response(response, sip_415_UnsupportedMediaType);

				CheckAndReplaceTransport(msg, response);

				push_message_to_transaction(response);

				m_trunk->call_session__call_disconnected(*this, msg);

				StartAutoDestructTimer(30);
			}
		}
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR(m_log_tag, "IST On Received Invite failed with exception %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::IST_OnReceivedMoreInvite(const sip_message_t& reinvite)
{
	if (m_state == StateDisconnected || m_trunk == NULL_TRUNK)
	{
		SLOG_WARN(m_log_tag, "%s : Incoming Reinvite while disconnecting! : Rejecting", m_session_ref);

		sip_message_t response;
		reinvite.make_response(response, sip_481_TransactionDoesNotExist);
		CheckAndReplaceTransport(reinvite, response);
		push_message_to_transaction(response);
		return;
	}

	if (reinvite.CSeq_Number() <= m_current_uas_invite.CSeq_Number())
	{
		return;   /// ignore, this is a retransmission
	}

	OnReinvite(reinvite);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::IST_OnReceivedAck(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : IST received ACK!", m_session_ref);

	if (m_state < StateConnected)
		return;

	if (msg.has_sdp())
	{
		if (m_trunk != NULL_TRUNK)
		{
			if (m_ack_received)
			{
				m_trunk->call_session__call_reestablished(*this, msg);

				m_trunk->call_session__apply_sdp_reanswer(*this, msg);
			}
			else if (m_trunk->call_session__apply_sdp_answer(*this, msg))
			{
				m_trunk->call_session__call_established(*this, msg);
			}
		}
	}

	if (m_trunk != NULL_TRUNK)
		m_trunk->call_session__recv_ack(*this, msg);

	m_ack_received = true;

	if (msg.getRoute().type == sip_transport_udp_e)
	{
		retransmit_thread_t * retran = m_call_manager.get_retransmit_thread();
		retran->stop_retransmission(msg.CallId());
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::IST_On1xxSent(const sip_message_t& msg)
{
	sip_status_t code = msg.get_response_code();
	SLOG_SESS(m_log_tag, "%s : IST 1xx sent! trunk:%p code:%d", m_session_ref, m_trunk, code);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::IST_On2xxSent(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : IST 2xx sent!", m_session_ref);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::IST_On3xxSent(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : IST 3xx sent!", m_session_ref);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::IST_On4xxSent(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : IST 4xx sent!", m_session_ref);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::IST_On5xxSent(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : IST 5xx sent!", m_session_ref);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::IST_On6xxSent(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : IST 6xx sent!", m_session_ref);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::IST_OnUnknownSent(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : IST Unknown sent %s", m_session_ref, (const char*)msg.GetStartLine());
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::IST_OnReceivedUnknown(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : IST received Unknown request %s", m_session_ref, (const char*)msg.GetStartLine());
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::IST_200OkRetransmitTimeoutReceived(const sip_message_t& msg)
{
	SLOG_WARN(m_log_tag, "%s : ACK not received!", m_session_ref);

	rtl::MutexWatchLock lock(m_event_mutex, __FUNCTION__);

	m_call_end_reason = sip_call_session_t::ICT_Recv4xx;

	if (m_state >= sip_call_session_t::StateConnected)
	{
		SLOG_WARN(m_log_tag, "%s : ACK not received! send BYE to peer", m_session_ref);
		SendBye();
	}

	sip_message_t response;
	m_current_uac_invite.make_response(response, sip_408_RequestTimeout);
	SLOG_WARN(m_log_tag, "%s : ACK not received! notify trunk %p", m_session_ref, m_trunk);
	if (m_trunk != NULL_TRUNK)
		m_trunk->call_session__call_disconnected(*this, response);
}
//--------------------------------------
//
//--------------------------------------
/** NIST Callbacks */
void sip_call_session_t::NIST_OnReceivedRegister(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NIST received REGISTER!", m_session_ref);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NIST_OnReceivedBye(const sip_message_t& msg)
{
	m_call_end_reason = sip_call_session_t::NIST_RecvBye;
	sip_message_t ok;
	msg.make_response(ok, sip_200_OK);

	CheckAndReplaceTransport(msg, ok);

	push_message_to_transaction(ok);

	SLOG_SESS(m_log_tag, "%s : NIST received BYE!", m_session_ref);
	
	if (m_trunk != NULL_TRUNK)
		m_trunk->call_session__call_disconnected(*this, msg);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NIST_OnReceivedOptions(const sip_message_t& request)
{
	SLOG_SESS(m_log_tag, "%s : NIST received OPTIONS!", m_session_ref);

	sip_message_t answer;
	request.make_response(answer, sip_200_OK, "");
	const sip_header_t* c = request.get_std_header(sip_Contact_e);
	answer.set_std_header(c);

	sip_header_t* allowed = answer.make_Allow();
	allowed->add_value("INVITE");
	allowed->add_value("BYE");
	allowed->add_value("ACK");
	allowed->add_value("REFER");
	allowed->add_value("MESSAGE");
	allowed->add_value("INFO");
	allowed->add_value("SUBSCRIBE");
	allowed->add_value("NOTIFY");
	allowed->add_value("OPTIONS");

	bool sdp_required = true;

	for (int i = 0; i < request.get_Accept()->get_value_count(); i++)
	{
		const sip_value_t* accept = request.get_Accept_value(i);

		if (accept->value() *= "application/sdp")
		{
			sdp_required = true;
			break;
		}
		else
		{
			sdp_required = false;
		}
	}

	if (sdp_required && m_trunk != NULL_TRUNK)
		m_trunk->call_session__recv_ni_request(*this, request, answer);

	push_message_to_transaction(answer);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NIST_OnReceivedInfo(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NIST received INFO!", m_session_ref);

	sip_message_t response;
	msg.make_response(response, sip_200_OK);
	m_trunk->call_session__recv_ni_request(*this, msg, response);
	push_message_to_transaction(response);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NIST_OnReceivedCancel(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NIST received CANCEL!", m_session_ref);

	if (m_state < StateConnected)
	{
		m_state = sip_call_session_t::StateCancelling;
		m_call_end_reason = sip_call_session_t::NIST_RecvCancel;
		sip_message_t ok;
		msg.make_response(ok, sip_200_OK);
		push_message_to_transaction(ok);

		if (m_current_uas_invite.IsValid())
		{
			SendReject(m_current_uas_invite, sip_487_RequestTerminated);

			if (m_trunk != NULL_TRUNK)
				m_trunk->call_session__call_disconnected(*this, msg);
		}
	}
	else
	{
		sip_message_t reject;
		msg.make_response(reject, sip_481_TransactionDoesNotExist);
		push_message_to_transaction(reject);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NIST_OnReceivedNotify(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NIST received NOTIFY!", m_session_ref);

	if (msg.has_Event())
	{
		const sip_value_t* event = msg.get_Event_value();

		sip_message_t response;
		msg.make_response(response, sip_200_OK);

		if (event != nullptr)
		{
			if (event->value() *= "refer")
			{
				/// get the subscription state
				const sip_value_t* state = msg.get_Subscription_State_value();
				if (state == nullptr)
				{
					response.set_response_line(sip_400_BadRequest);
					response.make_Reason_value()->assign("Request has no Subscribe-state field");
					return;
				}



				/// get the SIP fragment from the body
				rtl::String sipFrag((const char*)msg.get_body(), msg.get_body_length());

				(m_log_tag, "%s : NIST pass NOTIFY reques to trunk!", m_session_ref);

				m_trunk->call_session__recv_ni_request(*this, msg, response);
			}
		}

		push_message_to_transaction(response);
	}
	else
	{
		SLOG_WARN(m_log_tag, "%s : NIST received NOTIFY has no Event header!", m_session_ref);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NIST_OnReceivedSubscribe(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NIST received SUBSCRIBE!", m_session_ref);
	sip_message_t response;
	msg.make_response(response, sip_200_OK);
	m_trunk->call_session__recv_ni_request(*this, msg, response);
	push_message_to_transaction(response);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NIST_OnReceivedRefer(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NIST received REFER!", m_session_ref);

	sip_message_t response;
	msg.make_response(response, sip_202_Accepted);
	m_trunk->call_session__recv_ni_request(*this, msg, response);
	CheckAndReplaceTransport(msg, response);
	push_message_to_transaction(response);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NIST_OnReceivedMessage(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NIST received message!", m_session_ref, (const char*)msg.GetStartLine());
	sip_message_t response;
	msg.make_response(response, sip_200_OK);
	m_trunk->call_session__recv_ni_request(*this, msg, response);
	push_message_to_transaction(response);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NIST_OnReceivedUpdate(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NIST received UPDATE!", m_session_ref);

	sip_message_t ok;
	msg.make_response(ok, sip_200_OK);

	push_message_to_transaction(ok);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NIST_OnReceivedUnknown(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NIST received Request '%s'", m_session_ref, (const char*)msg.GetStartLine());
	sip_message_t response;
	msg.make_response(response, sip_200_OK);
	m_trunk->call_session__recv_ni_request(*this, msg, response);
	push_message_to_transaction(response);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NIST_On1xxSent(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NIST 1xx sent!", m_session_ref);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NIST_On2xxSent(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NIST 2xx sent!", m_session_ref);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NIST_On3xxSent(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NIST 3xx sent!", m_session_ref);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NIST_On4xxSent(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NIST 4xx sent!", m_session_ref);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NIST_On5xxSent(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NIST 5xx sent!", m_session_ref);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NIST_On6xxSent(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NIST 6xx sent!", m_session_ref);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NIST_OnUnknownSent(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NIST unknown sent %s", m_session_ref, (const char*)msg.GetStartLine());
}
//--------------------------------------
//
//--------------------------------------
/** NICT CALLBACKS */
void sip_call_session_t::NICT_OnRegisterSent(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NICT REGISTER sent!", m_session_ref);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NICT_OnByeSent(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NICT BYE sent! notify trunk %p", m_session_ref, m_trunk);

	m_call_end_reason = sip_call_session_t::NICT_ByeSent;

	m_trunk = NULL_TRUNK;
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NICT_OnOptionsSent(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NICT OPTIONS sent!", m_session_ref);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NICT_OnInfoSent(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NICT INFO sent!", m_session_ref);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NICT_OnCancelSent(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NICT CANCEL sent!", m_session_ref);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NICT_OnNotifySent(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NICT NOTIFY sent!", m_session_ref);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NICT_OnSubscribeSent(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NICT SUBSCRIBE sent!", m_session_ref);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NICT_OnUnknownSent(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NICT Unknown sent %s", m_session_ref, (const char*)msg.GetStartLine());
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NICT_OnReceived1xx(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NICT received 1xx!", m_session_ref);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NICT_OnReceived2xx(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NICT received 2xx!", m_session_ref);

	if (m_trunk != NULL_TRUNK)
		m_trunk->call_session__recv_ni_response(*this, msg);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NICT_OnReceived3xx(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NICT received 3xx!", m_session_ref);

	/// check if this is a 3xx Redirect for refer
	if (m_trunk != nullptr)
		m_trunk->call_session__recv_ni_response(*this, msg);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NICT_OnReceived4xx(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NICT received 4xx!", m_session_ref);

	/// check if this is a 4xx Reject for refer
	if (m_trunk != nullptr)
		m_trunk->call_session__recv_ni_response(*this, msg);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NICT_OnReceived5xx(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NICT received 5xx!", m_session_ref);

	/// check if this is a 5xx Failure for refer
	if (m_trunk != nullptr)
		m_trunk->call_session__recv_ni_response(*this, msg);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NICT_OnReceived6xx(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NICT received 6xx!", m_session_ref);

	/// check if this is a 6xx Failure for refer
	if (m_trunk != nullptr)
		m_trunk->call_session__recv_ni_response(*this, msg);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::NICT_OnReceivedUnknown(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : NICT received Unknown %s", m_session_ref, (const char*)msg.GetStartLine());
}
//--------------------------------------
//
//--------------------------------------
/** ICT CALLBACKS */
void sip_call_session_t::ICT_OnInviteSent(const sip_message_t& msg)
{
	m_route = msg.getRoute();
	char buf[256];
	SLOG_SESS(m_log_tag, "%s : NICT INVITE Sent! route -> %s", m_session_ref, route_to_string(buf, 256, m_route));
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::ICT_OnAckSent(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : ICT ACK Sent!", m_session_ref);

	if (m_state == StateConnected && m_trunk != NULL_TRUNK)
		m_trunk->call_session__ack_sent(*this);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::ICT_OnReceived1xx(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : ICT 1xx received", m_session_ref);

	if (m_state == StateDisconnected )
		return;

	m_can_send_cancel = true;

	if (m_send_cancel)
	{
		SendCancel(false);
		m_send_cancel = false;
		return;
	}

	/*if (IsReferredCall())
	{
		SendCallTransferNotification(msg);
	}*/

	if (msg.has_sdp())
	{
		if (m_trunk == NULL_TRUNK || !m_trunk->call_session__apply_sdp_answer(*this, msg))
			return;
	}

	if (m_trunk != NULL_TRUNK)
		m_trunk->call_session__recv_1xx(*this, msg);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::ICT_OnReceived2xx(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : ICT 2xx received!", m_session_ref);

	if (m_state == StateCancelling)
	{
		SLOG_WARN(m_log_tag, "%s : 200 ok on canceling state! send ack, bye ans start auto destructor timer for 30 seconds", m_session_ref);

		/*
			сначала остановим CANCEL транзакцию
			отправим ACK
			отправим BYE
		*/ 

		if (m_trunk != NULL_TRUNK)
		{
			m_trunk = NULL_TRUNK;
		}

		m_call_manager.get_user_agent().get_sip_stack().stop_cancel_invite_client_transaction(m_current_uac_invite);

		InitializeDialogAsUAC(m_current_uac_invite, msg);

		SendAck(msg);

		m_state = StateConnected;

		SendBye();

		StartAutoDestructTimer(30);

		return;
	}

	if (m_state == StateDisconnected)
	{
		SLOG_WARN(m_log_tag, "%s : state is disconnected! ignored", m_session_ref);

		return;
	}

	uint32_t cseqNo = msg.CSeq_Number();
	if (cseqNo <= (uint32_t)m_last_ict_response_cseq)
	{
		// we must send last ack message
		SendAck(msg);
		return;  // this is a retransmission
	}

	m_last_ict_response_cseq = cseqNo;

	m_reinvite_state = ReinviteNone;

	m_can_send_cancel = false;

	if (m_state == StateCancelling)
	{
		//SendBye();
		return;
	}

	// иначе отправка BYE в момент принятия 200 ок не отрабатывает
	InitializeDialogAsUAC(m_current_uac_invite, msg);

	if (msg.has_sdp())
	{
		if (m_trunk != NULL_TRUNK)
		{
			if (m_state == StateConnected)
			{
				m_trunk->call_session__apply_sdp_reanswer(*this, msg);

				// если реинвайт то просто сообщим
				m_trunk->call_session__call_reestablished(*this, msg);
			}
			else if (m_trunk->call_session__apply_sdp_answer(*this, msg))
			{
				m_state = StateConnected;
				// если успешно обработанно то коннектед!
				m_trunk->call_session__call_established(*this, msg);
			}
		}
	}

	/// clear out the proxy auth flag to allow reinvites to be 
	/// authenticated
	m_has_sent_proxy_auth = false;

	/// Clear out the redirecting flag
	//m_redirecting_call = false;

	SLOG_SESS(m_log_tag, "%s : ICT 2xx received : Remote is CONNECTED", m_session_ref);

	sip_message_t connect = msg;

	SendAck(msg);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::ICT_OnReceived3xx(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : ICT 3xx received!", m_session_ref);

	//m_uac_cancel_sync.Signal();

	if (m_state == StateCancelling || m_state == StateDisconnected )
		return;

	if (msg.CSeq_Method() *= "INVITE")
		m_state = StateDisconnected;

	if (m_trunk != NULL_TRUNK)
		m_trunk->call_session__call_disconnected(*this, msg);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::ICT_OnReceived4xx(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : ICT 4xx received!", m_session_ref);

	if (m_state == StateCancelling || m_state == StateDisconnected )
		return;

	if ((msg.get_response_code() == sip_407_ProxyAuthenticationRequired ||
		msg.get_response_code() == sip_401_Unauthorized) && m_state != StateCancelling)
	{
		if (!OnProxyAuthentication(msg))
		{
			m_state = StateDisconnected;
			//OnDisconnected();
			if (m_trunk != NULL_TRUNK)
				m_trunk->call_session__call_disconnected(*this, msg);
		}
	}
	else if (msg.get_response_code() == sip_488_NotAcceptableHere || msg.get_response_code() == sip_408_RequestTimeout)
	{
		if (m_trunk != NULL_TRUNK)
			m_trunk->call_session__call_reestablished(*this,msg);
	}
	else if (msg.get_response_code() == sip_491_RequestPending)
	{
		if (m_trunk != NULL_TRUNK)
			m_trunk->call_session__call_reestablished(*this,msg);
	}
	else
	{
		if (msg.CSeq_Method() *= "INVITE")
			m_state = StateDisconnected;

		if (m_trunk != NULL_TRUNK)
			m_trunk->call_session__call_disconnected(*this,msg);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::ICT_OnReceived5xx(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : ICT 5xx received!", m_session_ref);

	if (msg.CSeq_Method() *= "INVITE")
		m_state = StateDisconnected;

	if (m_trunk != NULL_TRUNK)
		m_trunk->call_session__call_disconnected(*this, msg);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::ICT_OnReceived6xx(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : ICT 6xx received!", m_session_ref);

	if (msg.CSeq_Method() *= "INVITE")
		m_state = StateDisconnected;

	if (m_trunk != NULL_TRUNK)
		m_trunk->call_session__call_disconnected(*this, msg);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::ICT_OnReceivedUnknown(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : ICT Unknown received %s", m_session_ref, (const char*)msg.GetStartLine());
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::ICT_OnUnknownSent(const sip_message_t& msg)
{
	SLOG_SESS(m_log_tag, "%s : ICT unknown sent %s", m_session_ref, (const char*)msg.GetStartLine());
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::TransportFailure()
{
	SLOG_SESS(m_log_tag, "%s : Transport failure!", m_session_ref);

	m_call_end_reason = ICT_Recv5xx;
	m_state = StateDisconnected;

	if (m_trunk != NULL_TRUNK)
		m_trunk->call_session__failure(*this);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::EnqueueMessageEvent(int event, const sip_message_t& msg)
{
	push_session_event(NEW sip_session_event_t(*this, event, msg));
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::process_session_expire_event()
{
	SLOG_SESS(m_log_tag, "%s : Session Timer Expired!!!", m_session_ref);

	if (m_state == StateConnected)
	{
		SendBye();
	}
}
//--------------------------------------
//
//--------------------------------------
bool sip_call_session_t::OnReinvite(const sip_message_t& invite)
{
	// проверим наличие тела
	if (!invite.has_sdp())
	{
		m_reinvite_state = ReinviteNone;
		return SendConnect(invite);
	}

	// уведомим о получении реинвайта!
	// 0 - async response
	// 1 - send response

	if (m_trunk == NULL_TRUNK)
		return false;

	m_current_uas_invite = invite;
	reinvite_response_t response = m_trunk->call_session__recv_reinvite(*this, invite);

	// 1. просто реинвайт для смены медиа стрима
	// 2. флеш

	if (!invite.has_body() && response == reinvite_response_ok_e)
	{
		m_reinvite_state = ReinviteNone;
		return SendConnect(invite);
	}

	if (response == reinvite_response_not_accepted_e)
	{
		SLOG_SESS(m_log_tag, "%s : Incoming SDP Offer dropped (488) : IGNORING incoming INVITE.", m_session_ref);

		sip_message_t response;
		sip_status_t answer = sip_488_NotAcceptableHere;

		invite.make_response(response, answer);

		m_reinvite_state = ReinviteNone;
		CheckAndReplaceTransport(invite, response);
		push_message_to_transaction(response);

		return true;
	}
	else if (response == reinvite_response_error_e)
	{
		SLOG_SESS(m_log_tag, "%s : Incoming SDP Offer dropped (500) : IGNORING incoming INVITE.", m_session_ref);

		sip_message_t response;
		sip_status_t answer = sip_500_ServerInternalError;

		invite.make_response(response, answer);
		CheckAndReplaceTransport(invite, response);
		m_reinvite_state = ReinviteNone;
		push_message_to_transaction(response);

		return true;
	}

	rtl::String sdpOffer((const char*)invite.get_body(), invite.get_body_length());
	
	if (!m_trunk->call_session__apply_sdp_reoffer(*this, invite))
	{
		SLOG_SESS(m_log_tag, "%s : Incoming SDP Offer dropped (488) : IGNORING incoming INVITE.", m_session_ref);

		sip_message_t response;
		sip_status_t answer = sip_488_NotAcceptableHere;

		invite.make_response(response, answer);
		m_reinvite_state = ReinviteNone;
		CheckAndReplaceTransport(invite, response);
		push_message_to_transaction(response);
	}
	else if (response == reinvite_response_ok_e) // now lets answer the call
	{
		m_reinvite_state = ReinviteNone;
		SendConnect(invite);
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool sip_call_session_t::OnProxyAuthentication(const sip_message_t& challenge)
{
	//GCVERIFYREF("sip_call_session_t::OnProxyAuthentication", false);

	SLOG_SESS(m_log_tag, "%s : Proxy Authentication %s", (const char*)m_call_id, (const char*)challenge.GetStartLine());

	if (challenge.get_response_code() == sip_401_Unauthorized)
		return OnWWWAuthentication(challenge);

	bool result = false;

	try
	{
		rtl::MutexWatchLock lock(m_proxy_auth_lock, __FUNCTION__);

		if (m_has_sent_proxy_auth)
		{
			SLOG_WARN(m_log_tag, "%s : Double Proxy Authentication!", m_session_ref);
			goto the_end;
		}

		const rtl::String& auth_id = m_profile.get_auth_id();
		const rtl::String& auth_pwd = m_profile.get_auth_pwd();

		if (auth_id.isEmpty())
		{
			SLOG_WARN(m_log_tag, "%s : Proxy Authentication User is EMPTY!!! Unable to authenticate Call!!!", m_session_ref);
			goto the_end;
		}

		if (!challenge.has_Proxy_Authenticate())
		{
			SLOG_WARN(m_log_tag, "%s : Proxy Authentication : challenge has no proxy authenticate", m_session_ref);
			goto the_end;
		}

		m_auth = *challenge.get_Proxy_Authenticate_value();
		m_has_auth = true;

		/// clear the internal headers from the previous transaction
//		m_current_uac_invite.ClearInternalHeaders();

		/// increment cseq
		m_current_uac_invite.CSeq_Increment();

		rtl::String uriText = m_current_uac_invite.get_request_uri().to_string(false);

		rtl::String realm, alg;
		rtl::String nonce, n;
		rtl::String qop, opaque;
		rtl::String entity = "";

		m_auth.get_auth_param("realm", realm);
		m_auth.get_auth_param("nonce", n);
		m_auth.get_auth_param("qop", qop);
		m_auth.get_auth_param("opaque", opaque);
		m_auth.get_auth_param("algorithm", alg);

		sip_utils::UnQuote(realm);
		sip_utils::UnQuote(nonce = n);
		sip_utils::UnQuote(qop);
		sip_utils::UnQuote(opaque);

		bool hasQop = !qop.isEmpty();

		sip_value_auth_t* authorization = m_current_uac_invite.make_Proxy_Authorization_value();

		if (!hasQop)
		{
			char ha1[MD5_HASH_HEX_SIZE+1];
			char ha2[MD5_HASH_HEX_SIZE+1];
			char response[MD5_HASH_HEX_SIZE+1];

			DigestCalcHA1_Sess(alg, auth_id, realm, auth_pwd, nonce, "", ha1);
			DigestCalcHA2(m_current_uac_invite.get_request_method_name(), uriText, ha2);
			DigestCalcResponse(ha1, nonce, ha2, response);
			rtl::String hResponse = response;

			authorization->set_scheme("Digest");
			authorization->set_auth_param("username", sip_utils::Quote(auth_id));
			authorization->set_auth_param("realm", sip_utils::Quote(realm));
			authorization->set_auth_param("nonce", sip_utils::Quote(nonce));
			authorization->set_auth_param("uri", sip_utils::Quote(uriText));
			authorization->set_auth_param("response", sip_utils::Quote(hResponse));
			if (!alg.isEmpty())
				authorization->get_auth_param("algorithm", alg);
		}
		else
		{
			char cnonce[MD5_HASH_HEX_SIZE+1];
			DigestCalcHA2(qop, uriText, cnonce);

			char ha1[MD5_HASH_HEX_SIZE+1];
			char ha2[MD5_HASH_HEX_SIZE+1];
			char response[MD5_HASH_HEX_SIZE+1];
			char nc[16];

			int len = std_snprintf(nc, 16, "%08x", ++m_invite_qop_count);
			nc[len] = 0;

			DigestCalcHA1_Sess(alg, auth_id, realm, auth_pwd, nonce, cnonce, ha1);

			if (qop.indexOf(',') > 0)
			{
				qop = "auth";
			}

			DigestCalcHA2_Int(m_current_uac_invite.get_request_method_name(), uriText, qop, entity, ha2);
			DigestCalcResponse_Qop(ha1, nonce, nc, cnonce, qop, ha2, response);
			rtl::String hResponse = response;

			rtl::String st;

			authorization->set_scheme("Digest");
			authorization->set_auth_param("username", sip_utils::Quote(auth_id));
			authorization->set_auth_param("realm", sip_utils::Quote(realm));
			authorization->set_auth_param("nonce", sip_utils::Quote(nonce));
			authorization->set_auth_param("uri", sip_utils::Quote(uriText));
			authorization->set_auth_param("response", sip_utils::Quote(hResponse));
			authorization->set_auth_param("cnonce", sip_utils::Quote(cnonce));
			authorization->set_auth_param("nc", nc);
			authorization->set_auth_param("qop", qop);//sip_utils::Quote(qop));
			if (!alg.isEmpty())
				authorization->set_auth_param("algorithm", alg);
			if (!opaque.isEmpty())
				authorization->set_auth_param("opaque", sip_utils::Quote(opaque));
		}

		m_has_auth = true;

		sip_value_via_t* via = m_current_uac_invite.get_Via_value();
		via->set_branch(sip_utils::GenBranchParameter());

		m_has_sent_proxy_auth = true;

		/// check if we are configured to use an outbound proxy
		//m_current_uac_invite.set_remote_address(m_profile.get_proxy_address(), m_profile.get_proxy_port(), m_profile.get_transport());

		result = push_message_to_transaction(m_current_uac_invite);
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR(m_log_tag, "Proxy Authentication failed with exception %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

the_end:

	return result;
}
//--------------------------------------
//
//--------------------------------------
bool sip_call_session_t::OnWWWAuthentication(const sip_message_t& challenge)
{
	SLOG_SESS(m_log_tag, "%s : WWW Authentication %s", m_session_ref, (const char*)challenge.GetStartLine());

	bool result = false;

	try
	{
		rtl::MutexWatchLock lock(m_proxy_auth_lock, __FUNCTION__);

		if (m_has_sent_proxy_auth)
		{
			SLOG_WARN(m_log_tag, "%s : WWW Authentication : session already sent auth!", m_session_ref);
			goto the_end;
		}

		const rtl::String& auth_id = m_profile.get_auth_id();
		const rtl::String& auth_pwd = m_profile.get_auth_pwd();

		if (auth_id.isEmpty())
		{
			SLOG_WARN(m_log_tag, "%s : WWW Authentication failed : empty user name!", m_session_ref);
			goto the_end;
		}

		if (!challenge.has_WWW_Authenticate())
		{
			SLOG_WARN(m_log_tag, "%s : WWW Authentication failed : hallenge has no WWW Authenticate header!", m_session_ref);
			goto the_end;
		}

		m_www_auth = *challenge.get_WWW_Authenticate_value();
		m_has_www_auth = true;

		/// increment cseq
		m_current_uac_invite.CSeq_Increment();

		rtl::String uriText = m_current_uac_invite.get_request_uri().to_string(false);

		rtl::String realm, alg;
		rtl::String nonce, n;
		rtl::String qop, opaque;
		rtl::String entity = "";

		m_www_auth.get_auth_param("realm", realm);
		m_www_auth.get_auth_param("nonce", n);
		m_www_auth.get_auth_param("qop", qop);
		m_www_auth.get_auth_param("opaque", opaque);
		m_www_auth.get_auth_param("algorithm", alg);

		sip_utils::UnQuote(realm);
		sip_utils::UnQuote(nonce = n);
		sip_utils::UnQuote(qop);
		sip_utils::UnQuote(opaque);

		bool hasQop = !qop.isEmpty();

		sip_value_auth_t* authorization = m_current_uac_invite.make_Authorization_value();

		if (!hasQop)
		{
			char ha1[MD5_HASH_HEX_SIZE+1];
			char ha2[MD5_HASH_HEX_SIZE+1];
			char response[MD5_HASH_HEX_SIZE+1];

			DigestCalcHA1_Sess(alg, auth_id, realm, auth_pwd, nonce, "", ha1);
			DigestCalcHA2(m_current_uac_invite.get_request_method_name(), uriText, ha2);
			DigestCalcResponse(ha1, nonce, ha2, response);
			rtl::String hResponse = response;

			authorization->set_scheme("Digest");
			authorization->set_auth_param("username", sip_utils::Quote(auth_id));
			authorization->set_auth_param("realm", sip_utils::Quote(realm));
			authorization->set_auth_param("nonce", sip_utils::Quote(nonce));
			authorization->set_auth_param("uri", sip_utils::Quote(uriText));
			authorization->set_auth_param("response", sip_utils::Quote(hResponse));
			
			if (!alg.isEmpty())
				authorization->set_auth_param("algorithm", alg);
		}
		else
		{
			char cnonce[MD5_HASH_HEX_SIZE+1];
			DigestCalcHA2(qop, uriText, cnonce);

			char ha1[MD5_HASH_HEX_SIZE+1];
			char ha2[MD5_HASH_HEX_SIZE+1];
			char response[MD5_HASH_HEX_SIZE+1];
			char nc[16];

			int len = std_snprintf(nc, 16, "%08x", ++m_invite_qop_count);
			nc[len] = 0;

			DigestCalcHA1_Sess(alg, auth_id, realm, auth_pwd, nonce, cnonce, ha1);

			if (qop.indexOf(',') > 0)
			{
				qop = "auth";
			}

			DigestCalcHA2_Int(m_current_uac_invite.get_request_method_name(), uriText, qop, entity, ha2);
			DigestCalcResponse_Qop(ha1, nonce, nc, cnonce, qop, ha2, response);
			rtl::String hResponse = response;

			rtl::String st;

			authorization->set_scheme("Digest");
			authorization->set_auth_param("username", sip_utils::Quote(auth_id));
			authorization->set_auth_param("realm", sip_utils::Quote(realm));
			authorization->set_auth_param("nonce", sip_utils::Quote(nonce));
			authorization->set_auth_param("uri", sip_utils::Quote(uriText));
			authorization->set_auth_param("response", sip_utils::Quote(hResponse));
			authorization->set_auth_param("cnonce", sip_utils::Quote(cnonce));
			authorization->set_auth_param("nc", nc);
			authorization->set_auth_param("qop", qop);//sip_utils::Quote(qop));
			if (!alg.isEmpty())
				authorization->set_auth_param("algorithm", alg);
			if (!opaque.isEmpty())
				authorization->set_auth_param("opaque", sip_utils::Quote(opaque));
		}

		m_has_www_auth = true;

		sip_value_via_t* via = m_current_uac_invite.get_Via_value();
		via->set_branch(sip_utils::GenBranchParameter());

		m_has_sent_proxy_auth = true;

		/// check if we are configured to use an outbound proxy
		//m_current_uac_invite.set_remote_address(m_profile.get_proxy_address(), m_profile.get_proxy_port(), m_profile.get_transport());

		result = push_message_to_transaction(m_current_uac_invite);
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR(m_log_tag, "WWW Authentication failed with exception %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

the_end:

	return result;
}
//--------------------------------------
// ProxyAuthenticate
//--------------------------------------
void sip_call_session_t::MakeProxyAuthorization(sip_message_t& message, const sip_value_auth_t& proxyAuth)
{
	SLOG_SESS(m_log_tag, "%s : Make Proxy Authentication", m_session_ref);

	try
	{
		rtl::MutexWatchLock lock(m_proxy_auth_lock, __FUNCTION__);

		const rtl::String& auth_id = m_profile.get_auth_id();
		const rtl::String& auth_pwd = m_profile.get_auth_pwd();

		if (auth_id.isEmpty())
		{
			SLOG_WARN(m_log_tag, "%s : Proxy Authentication User is EMPTY!!! Unable to authenticate Call!!!", m_session_ref);
			return;
		}


		rtl::String uriText = message.get_request_uri().to_string(false);

		rtl::String realm, alg;
		rtl::String nonce, n;
		rtl::String qop, opaque;
		rtl::String entity = "";

		proxyAuth.get_auth_param("realm", realm);
		proxyAuth.get_auth_param("nonce", n);
		proxyAuth.get_auth_param("qop", qop);
		proxyAuth.get_auth_param("opaque", opaque);
		proxyAuth.get_auth_param("algorithm", alg);

		sip_utils::UnQuote(realm);
		sip_utils::UnQuote(nonce = n);
		sip_utils::UnQuote(qop);
		sip_utils::UnQuote(opaque);

		bool hasQop = !qop.isEmpty();

		sip_value_auth_t* authorization = message.make_Proxy_Authorization_value();

		if (!hasQop)
		{
			char ha1[MD5_HASH_HEX_SIZE+1];
			char ha2[MD5_HASH_HEX_SIZE+1];
			char response[MD5_HASH_HEX_SIZE+1];

			DigestCalcHA1_Sess(alg, auth_id, realm, auth_pwd, nonce, "", ha1);
			DigestCalcHA2(message.get_request_method_name(), uriText, ha2);
			DigestCalcResponse(ha1, nonce, ha2, response);
			rtl::String hResponse = response;

			authorization->set_scheme("Digest");
			authorization->set_auth_param("username", sip_utils::Quote(auth_id));
			authorization->set_auth_param("realm", sip_utils::Quote(realm));
			authorization->set_auth_param("nonce", sip_utils::Quote(nonce));
			authorization->set_auth_param("uri", sip_utils::Quote(uriText));
			authorization->set_auth_param("response", sip_utils::Quote(hResponse));
			if (!alg.isEmpty())
				authorization->set_auth_param("algorithm", alg);
		}
		else
		{
			char cnonce[MD5_HASH_HEX_SIZE+1];
			DigestCalcHA2(qop, uriText, cnonce);

			char ha1[MD5_HASH_HEX_SIZE+1];
			char ha2[MD5_HASH_HEX_SIZE+1];
			char response[MD5_HASH_HEX_SIZE+1];
			char nc[16];

			int len = std_snprintf(nc, 16, "%08x", ++m_invite_qop_count);
			nc[len] = 0;

			DigestCalcHA1_Sess(alg, auth_id, realm, auth_pwd, nonce, cnonce, ha1);
			DigestCalcHA2_Int(message.get_request_method_name(), uriText, qop, entity, ha2);
			DigestCalcResponse_Qop(ha1, nonce, nc, cnonce, qop, ha2, response);
			rtl::String hResponse = response;


			rtl::String st;

			authorization->set_scheme("Digest");
			authorization->set_auth_param("username", sip_utils::Quote(auth_id));
			authorization->set_auth_param("realm", sip_utils::Quote(realm));
			authorization->set_auth_param("nonce", sip_utils::Quote(nonce));
			authorization->set_auth_param("uri", sip_utils::Quote(uriText));
			authorization->set_auth_param("response", sip_utils::Quote(hResponse));
			authorization->set_auth_param("cnonce", sip_utils::Quote(cnonce));
			authorization->set_auth_param("nc", nc);
			authorization->set_auth_param("qop", qop);//sip_utils::Quote(qop));
			if (!alg.isEmpty())
				authorization->set_auth_param("algorithm", alg);
			if (!opaque.isEmpty())
				authorization->set_auth_param("opaque", sip_utils::Quote(opaque));
		}
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR(m_log_tag, "Make Proxy Authorization failed with exception %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}
}
//--------------------------------------
// WWWAuthenticate
//--------------------------------------
void sip_call_session_t::MakeWWWAuthorization(sip_message_t& message, const sip_value_auth_t& proxyAuth)
{
	SLOG_SESS(m_log_tag, "%s : Make WWW Authentication", m_session_ref);

	try
	{
		rtl::MutexWatchLock lock(m_proxy_auth_lock, __FUNCTION__);

		const rtl::String& auth_id = m_profile.get_auth_id();
		const rtl::String& auth_pwd = m_profile.get_auth_pwd();

		if (auth_id.isEmpty())
		{
			SLOG_WARN(m_log_tag, "%s : WWW Authentication failed : empty user name!", m_session_ref);
			return;
		}

		rtl::String uriText = message.get_request_uri().to_string(false);

		rtl::String realm, alg;
		rtl::String nonce, n;
		rtl::String qop, opaque;
		rtl::String entity = "";

		proxyAuth.get_auth_param("realm", realm);
		proxyAuth.get_auth_param("nonce", n);
		proxyAuth.get_auth_param("qop", qop);
		proxyAuth.get_auth_param("opaque", opaque);
		proxyAuth.get_auth_param("algorithm", alg);

		sip_utils::UnQuote(realm);
		sip_utils::UnQuote(nonce = n);
		sip_utils::UnQuote(qop);
		sip_utils::UnQuote(opaque);

		bool hasQop = !qop.isEmpty();

		sip_value_auth_t* authorization = message.make_Authorization_value();

		if (!hasQop)
		{
			char ha1[MD5_HASH_HEX_SIZE+1];
			char ha2[MD5_HASH_HEX_SIZE+1];
			char response[MD5_HASH_HEX_SIZE+1];

			DigestCalcHA1_Sess(alg, auth_id, realm, auth_pwd, nonce, "", ha1);
			DigestCalcHA2(message.get_request_method_name(), uriText, ha2);
			DigestCalcResponse(ha1, nonce, ha2, response);
			rtl::String hResponse = response;

			authorization->set_scheme("Digest");
			authorization->set_auth_param("username", sip_utils::Quote(auth_id));
			authorization->set_auth_param("realm", sip_utils::Quote(realm));
			authorization->set_auth_param("nonce", sip_utils::Quote(nonce));
			authorization->set_auth_param("uri", sip_utils::Quote(uriText));
			authorization->set_auth_param("response", sip_utils::Quote(hResponse));
			if (!alg.isEmpty())
				authorization->set_auth_param("algorithm", alg);
		}
		else
		{
			char cnonce[MD5_HASH_HEX_SIZE+1];
			DigestCalcHA2(qop, uriText, cnonce);

			char ha1[MD5_HASH_HEX_SIZE+1];
			char ha2[MD5_HASH_HEX_SIZE+1];
			char response[MD5_HASH_HEX_SIZE+1];
			char nc[16];

			int len = std_snprintf(nc, 16, "%08x", ++m_invite_qop_count);
			nc[len] = 0;


			DigestCalcHA1_Sess(alg, auth_id, realm, auth_pwd, nonce, cnonce, ha1);
			DigestCalcHA2_Int(message.get_request_method_name(), uriText, qop, entity, ha2);
			DigestCalcResponse_Qop(ha1, nonce, nc, cnonce, qop, ha2, response);
			rtl::String hResponse = response;


			rtl::String st;

			authorization->set_scheme("Digest");
			authorization->set_auth_param("username", sip_utils::Quote(auth_id));
			authorization->set_auth_param("realm", sip_utils::Quote(realm));
			authorization->set_auth_param("nonce", sip_utils::Quote(nonce));
			authorization->set_auth_param("uri", sip_utils::Quote(uriText));
			authorization->set_auth_param("response", sip_utils::Quote(hResponse));
			authorization->set_auth_param("cnonce", sip_utils::Quote(cnonce));
			authorization->set_auth_param("nc", nc);
			authorization->set_auth_param("qop", qop);//sip_utils::Quote(qop));
			if (!alg.isEmpty())
				authorization->set_auth_param("algorithm", alg);
			if (!opaque.isEmpty())
				authorization->set_auth_param("opaque", sip_utils::Quote(opaque));
		}
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR(m_log_tag, "Make WWW Authorization failed with exception %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}
}
//--------------------------------------
//
//--------------------------------------
const char* sip_call_session_t::GetAnswerCallResponseName(AnswerCallResponse mode)
{
	static const char* names[] =
	{
		"AnswerCallNow",                // Sends 200 OK back with SDP after receipt of INVITE
		"AnswerCallDeferred",           // Answers with 180 ringing
		"AnswerCallDeferredWithMedia",  // Like AnswerCallDefered Only media is sent in 183 progress
		"AnswerCallRedirect",           // Redirect the call to another address
		"AnswerCallRedirectToProxy",    // Redirect the call to a proxy
		"AnswerCallQueued",             // Tell remote that the call is queued
		"AnswerCallDenied",             // Reject the call
		"NumAnswerCall"
	};

	return names[mode < NumAnswerCall ? mode : NumAnswerCall];
}
//--------------------------------------
// answers the call.  based on the mode.  May be called several times
//--------------------------------------
bool sip_call_session_t::AnswerCall(AnswerCallResponse mode)
{
	SLOG_SESS(m_log_tag, "%s : Answer Call : mode %s", m_session_ref, GetAnswerCallResponseName(mode));

	if (m_state == StateCancelling)
	{
		if (m_trunk != NULL_TRUNK)
			m_trunk->call_session__call_disconnected(*this, m_uas_request);

		StartAutoDestructTimer(30);
		return true;
	}

	if (m_state >= StateMaxAnswerCall)
	{
		SLOG_WARN(m_log_tag, "%s : Answer Call failed : state in Max Answer call state!", m_session_ref);
		return true;
	}

	switch(mode)
	{
	case AnswerCallNow:					// Sends 200 OK back with SDP after receipt of INVITE
		EnqueueMessageEvent(IST_AnswerCallNow, m_current_uas_invite);
		break;
	case AnswerCallDeferred:			// Answers with 180 ringing
		EnqueueMessageEvent(IST_AnswerCallDeferred, m_current_uas_invite);
		break;
	case AnswerCallDeferredWithMedia:	// Like AnswerCallDefered Only media is sent in 183 progress
		EnqueueMessageEvent(IST_AnswerCallDeferredWithMedia, m_current_uas_invite);
		break;
	case AnswerCallQueued:				// Tell remote that the call is queued
		EnqueueMessageEvent(IST_AnswerCallQueued, m_current_uas_invite );
		break;
	case AnswerCallDenied:				// Reject the call
		EnqueueMessageEvent(IST_AnswerCallDenied, m_current_uas_invite );
		break;
	//default:
	//	PAssertAlways(PLogicError);
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool sip_call_session_t::DisconnectCall(bool cancel_reason, int reject_code, const char* reject_reason)
{
	SLOG_SESS(m_log_tag, "%s : Disconnect Call...", m_session_ref);

	m_disconnect_cancel_reason = cancel_reason;

	m_disconnect_status_code = (sip_status_t)reject_code;
	m_disconnect_status_reason = reject_reason;

	EnqueueMessageEvent(ICT_DisconnectCall, m_current_uas_invite);

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool sip_call_session_t::RedirectCall(uint16_t code_3xx, const char* contact_str, int expires_num)
{
	rtl::MutexWatchLock lock(m_uas_request_lock, __FUNCTION__);

	sip_message_t response;
	m_uas_request.make_response(response, code_3xx);

	response.remove_std_header(sip_Contact_e);

	sip_value_contact_t* contact = response.make_Contact_value();
	sip_uri_t curi;

	if (contact_str == nullptr || contact_str[0] == 0)
	{
		/// set the contact address
		

		// GEORGE : есди нет юзера то берем из 'To'
		const char* userName = m_profile.get_username();

		if (userName == nullptr || userName[0] == 0)
		{
			curi.set_user(m_uas_request.To_URI().get_user());
		}
		else
		{
			curi.set_user(userName);
		}

		curi.set_host(inet_ntoa(m_profile.get_listener()));
		curi.set_port(rtl::int_to_string(m_profile.get_listener_port()));
		curi.get_parameters().set("transport", TransportType_toString(m_profile.get_transport()));

		
		contact->set_uri(curi);

		const char* display_name = m_profile.get_display_name();

		if (display_name != nullptr && display_name[0] != 0)
		{
			contact->set_display_name(display_name);
		}
	}
	else
	{
		curi = contact_str;
	}

	if (expires_num > 0)
	{
		response.make_Expires_value()->set_number(expires_num);
	}

	SLOG_SESS(m_log_tag, "%s : Redirect Incoming request %s with %s", m_session_ref,
		(const char*)m_uas_request.GetStartLine(),	(const char*)response.GetStartLine());
	
	return push_message_to_transaction(response);
}
//--------------------------------------
//
//--------------------------------------
bool sip_call_session_t::AnswerReinvite(bool success)
{
	// sip_call_session_manager_t Allowed this call to process
	// check for presence of SDP

	SLOG_SESS(m_log_tag, "%s : Answer Reinvite success %s", m_session_ref, success ? "true" : "false");

	try
	{
		rtl::MutexWatchLock lock(m_answer_call_mutex, __FUNCTION__);

		//bool hasSDP = m_current_uas_invite.has_sdp();
		rtl::String sdpOffer;

		if (!success)
		{
			sip_message_t response;

			m_current_uas_invite.make_response(response, sip_488_NotAcceptableHere);

			push_message_to_transaction(response);
		}
		// now lets answer the call
		else
			SendConnect(m_current_uas_invite);

		m_reinvite_state = ReinviteNone;
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR(m_log_tag, "Answer Reinvite failed with exception %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool sip_call_session_t::InternalAnswerCall(AnswerCallResponse mode, const sip_message_t& invite)
{
	SLOG_SESS(m_log_tag, "%s : internal Answer Call : mode %s", m_session_ref, GetAnswerCallResponseName(mode));

	if (m_state == sip_call_session_t::StateCancelling || m_state > sip_call_session_t::StateConnected)
		return true;


	try
	{
		rtl::MutexWatchLock lock(m_answer_call_mutex, __FUNCTION__);

		switch (mode)
		{
		case AnswerCallNow :               // Sends 200 OK back with SDP after receipt of INVITE
			SendConnect(invite);
			break;
		case AnswerCallDeferredWithMedia:  // Like AnswerCallDefered Only media is sent in 183 progress
			SendProgress(invite);
			break;
		case AnswerCallDeferred:           // Answers with 180 ringing
			SendAlerting(invite);
			break;
		case AnswerCallQueued:             // Tell remote that the call is queued
			SendCallQueued(invite);
			break;
		case AnswerCallDenied:             // Reject the call
			SendReject(invite, sip_403_Forbidden);
			break;
		case NumAnswerCall:
			break;
		//default:
		//	PAssertAlways(PLogicError);
		}
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR(m_log_tag, "Internal Answer Call failed with exception %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::CheckAndReplaceTransport(const sip_message_t& request, sip_message_t& response)
{
	// отправлять на via в инвайте!

	const sip_value_via_t* requestVia = request.get_Via_value();

	if (requestVia->get_protocol() == sip_transport_udp_e)
	{
		in_addr via_addr = net_parse(requestVia->get_address());
		in_addr r_addr = m_route.remoteAddress;

		if (via_addr.s_addr == r_addr.s_addr && requestVia->get_port() != m_route.remotePort)
		{
			// заменяем транспорт!
			const sip_transport_route_t& request_transport = request.getRoute();
			sip_transport_route_t back_route = {
				request_transport.type,
				m_profile.get_interface(), m_profile.get_listener_port(),
				request_transport.remoteAddress, requestVia->get_port()
			};

			response.set_route(back_route);
			return;
		}
	}

	response.set_route(m_route);
}
//--------------------------------------
// Sends 182 Call Queue
//--------------------------------------
bool sip_call_session_t::SendCallQueued(const sip_message_t& request)
{
	sip_message_t alerting;

	request.make_response(alerting, sip_182_Queued);

	CheckAndReplaceTransport(request, alerting);

	InitializeDialogAsUAS(m_current_uas_invite, alerting);

	m_current_uas_invite.make_To_value()->set_tag(alerting.To_Tag());

	return push_message_to_transaction(alerting);
}
//--------------------------------------
//
//--------------------------------------
bool sip_call_session_t::SendAlerting(const sip_message_t& request)
{
	sip_message_t alerting;

	request.make_response(alerting, sip_180_Ringing);

	CheckAndReplaceTransport(request, alerting);

	InitializeDialogAsUAS(m_current_uas_invite, alerting);

	m_current_uas_invite.make_To_value()->set_tag(alerting.To_Tag());

	SLOG_SESS(m_log_tag, "%s : Set to tag '%s'", m_session_ref, (const char*)m_current_uas_invite.To_Tag());

	return push_message_to_transaction(alerting);
}
/// sends and retransmits 200 Ok for an INVITE with SDP
/// 200 ok response will be generated
bool sip_call_session_t::SendConnect(const sip_message_t& request)
{
	sip_message_t ok;

	SLOG_SESS(m_log_tag, "%s : Send Connect to tag '%s'", m_session_ref, (const char*)request.To_Tag());

	if (!request.make_response(ok, sip_200_OK))
	{
		SLOG_WARN(m_log_tag, "%s : Send Connect failed : create response failed", m_session_ref);
		return false;
	}

	// отправлять на via в инвайте!

	CheckAndReplaceTransport(request, ok);

	if (request.IsInvite())
	{
		rtl::String sdp;
		if (request.has_sdp())
		{
			rtl::String body((const char*)request.get_body(), request.get_body_length());
			rtl::String offer(body);
			rtl::String answer;

			if (m_state == StateConnected)
			{
				if (!m_trunk->call_session__prepare_sdp_reanswer(*this, ok))
				{
					SLOG_WARN(m_log_tag, "%s : Send Connect failed(488): require SDP answer failed", m_session_ref);
					request.make_response(ok, sip_488_NotAcceptableHere);

					push_message_to_transaction(ok);
					return true;
				}
				//m_trunk->call_session__call_reestablished(*this, ok);
			}
			else
			{
				if (!m_trunk->call_session__prepare_sdp_answer(*this, ok))
				{
					sip_status_t answer = sip_500_ServerInternalError;

					SLOG_WARN(m_log_tag, "%s : Send Connect failed(%d): require SDP answer failed", m_session_ref, answer);

					SendReject(request, answer);
					m_trunk->call_session__call_disconnected(*this, request);
					return false;
				}
				m_trunk->call_session__call_established(*this, ok);
			}
		}
		else
		{
			// -- GEORGE
			// подготовка предложения в случае отсутствии предложения во входящем инвайте
			
			if (m_state != StateConnected)
			{
				if (!m_trunk->call_session__prepare_sdp_offer(*this, ok))
				{
					SLOG_WARN(m_log_tag, "%s : Send Connect failed(415): require SDP answer failed", m_session_ref);
					SendReject(request, sip_415_UnsupportedMediaType);
					m_trunk->call_session__call_disconnected(*this, request);
					return false;
				}
			}
			else //if (m_state == StateConnected)
			{
				if (!m_trunk->call_session__prepare_sdp_reoffer(*this, ok, false))
				{
					SLOG_WARN(m_log_tag, "%s : Send Connect failed(488): require SDP answer failed", m_session_ref);
					request.make_response(ok, sip_488_NotAcceptableHere);

					push_message_to_transaction(ok);
					return true;
				}
			}
		}

		/// set the contact address
		sip_uri_t curi;

		// GEORGE : есди нет юзера то берем из 'To'
		const char* userName = m_profile.get_username();

		if (userName == nullptr || userName[0] == 0)
		{
			curi.set_user(request.To_URI().get_user());
		}
		else
		{
			curi.set_user(userName);
		}

		in_addr p_addr;
		uint16_t p_port;

		p_addr = m_profile.get_listener();
		p_port = m_profile.get_listener_port();

		curi.set_host(inet_ntoa(p_addr));
		if (p_port != 0 && p_port != 5060)
		{
			curi.set_port(rtl::int_to_string(p_port)); // m_profile.get_nat_port()
		}

		curi.get_parameters().set("transport", TransportType_toString(m_profile.get_transport()));

		sip_value_contact_t* contact = ok.make_Contact_value();
		contact->set_uri(curi);

		const char* display_name = m_profile.get_display_name();

		if (display_name != nullptr && display_name[0] != 0)
		{
			contact->set_display_name(display_name);
		}
	}

	if (request.IsInvite())
	{
		InitializeDialogAsUAS(request, ok);
	}

	{
		rtl::MutexWatchLock l_custom_headers(m_custom_headers_lock, __FUNCTION__);
		for (int i = 0; i < m_custom_headers.getCount(); i++)
		{
			sip_header_t* header = m_custom_headers[i];
			if (header != nullptr)
				ok.set_custom_header(*header);
		}
	}

	/// Send it to the FSM once to terminate the IST transaction
	if (!push_message_to_transaction(ok))
	{
		SLOG_WARN(m_log_tag, "%s : Send Connect failed: send request failed!", m_session_ref);
		return false;
	}

	if (request.IsInvite() && request.getRoute().type == sip_transport_udp_e)
	{
		/// Here we should send the 200 ok to the retransmitter
		retransmit_thread_t* retran = m_call_manager.get_retransmit_thread();
		retran->start_retransmission(m_call_id, ok);		

		//if (m_enable_session_timer && request.IsSupportedExtension("timer") && !OnProcessSessionTimers(request, ok))
		//{
		//	SLOG_WARN(m_log_tag, "%s : Send Connect failed: timer problems!", m_session_ref);
		//	return false;
		//}
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool sip_call_session_t::SendTrying(const sip_message_t& invite)
{
	if (invite.getRoute().type == sip_transport_udp_e)
	{
		sip_message_t trying;
		invite.make_response(trying, sip_100_Trying);

		CheckAndReplaceTransport(invite, trying);

		push_message_to_transaction(trying);
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool sip_call_session_t::SendTrying()
{
	return SendTrying(m_current_uas_invite);
}
//--------------------------------------
//
//--------------------------------------
bool sip_call_session_t::SendProgress(const sip_message_t& invite)
{
	//PAssert(invite.IsInvite(), PLogicError);
	sip_message_t progress;
	
	if (!invite.make_response(progress, sip_183_SessionProgress))
	//if (!invite.make_response(progress, sip_180_Ringing))
	{
		SLOG_WARN(m_log_tag, "%s : Send progress failed: create response failed!", m_session_ref);
		return false;
	}

	CheckAndReplaceTransport(invite, progress);

	rtl::String sdp;
	if (invite.has_sdp())
	{
		if (!m_trunk->call_session__prepare_sdp_answer(*this, progress))
		{
			sip_status_t answer = sip_415_UnsupportedMediaType;

			SLOG_WARN(m_log_tag, "%s : Send progress failed(%d): create response failed!", m_session_ref, answer);

			SendReject(invite, answer);
			m_trunk->call_session__call_disconnected(*this, progress);
			return false;
		}
	}

	InitializeDialogAsUAS(m_current_uas_invite, progress);

	m_current_uas_invite.make_To_value()->set_tag(progress.To_Tag());

	{
		rtl::MutexWatchLock l_custom_headers(m_custom_headers_lock, __FUNCTION__);
		for (int i = 0; i < m_custom_headers.getCount(); i++)
		{
			sip_header_t* header = m_custom_headers[i];
			if (header != nullptr)
				progress.set_custom_header(*header);
		}
	}

	if (!push_message_to_transaction(progress))
		return false;

	/*	12.07.11 Renat
		Нужно было сделать так, чтобы транк уведомлялся о том, что 183 progress отправлен.
		Механизм уведомления есть, но он работает только для запросов. Для ответов механизм 
		отключен на уровне транспорта. Поэтому таким костыльчиком мы сами закинем себе в очередь уведомление,
		как будто оно от транспорта.

		22.01.13 George
		транспорт починил :(
		*/
	//EnqueueMessageEvent(IST_1xxSent, progress);

	return true;
}
//--------------------------------------
// Sends a forbidden by default.
// or GetAnswerCallresponse() if explicitly set
//--------------------------------------
bool sip_call_session_t::SendReject(const sip_message_t& request, sip_status_t statusCode, const rtl::String& reasonPhrase, const rtl::String& warning)
{
	sip_message_t forbidden;
	request.make_response(forbidden, (uint16_t)statusCode, reasonPhrase);

	if (!warning.isEmpty())
	{
		forbidden.make_Warning_value()->assign(warning);
	}

	CheckAndReplaceTransport(request, forbidden);

	{
		rtl::MutexWatchLock l_custom_headers(m_custom_headers_lock, __FUNCTION__);
		for (int i = 0; i < m_custom_headers.getCount(); i++)
		{
			sip_header_t* header = m_custom_headers[i];
			if (header != nullptr)
				forbidden.set_custom_header(*header);
		}
	}

	SLOG_SESS(m_log_tag, "%s : Rejected Incoming request %s with %s", m_session_ref,
		(const char*)request.GetStartLine(), (const char*)forbidden.GetStartLine());
	
	return push_message_to_transaction(forbidden);
}
//--------------------------------------
//
//--------------------------------------
bool sip_call_session_t::SendAcceptByRejection(int statusCode, const rtl::String& reasonPhrase, const rtl::String& warning)
{
	rtl::MutexWatchLock lock(m_uas_request_lock, __FUNCTION__);

	sip_message_t forbidden;
	m_uas_request.make_response(forbidden, (uint16_t)statusCode, reasonPhrase);

	CheckAndReplaceTransport(m_uas_request, forbidden);

	if (!warning.isEmpty())
	{
		forbidden.make_Warning_value()->assign(warning);
	}

	{
		rtl::MutexWatchLock l_custom_headers(m_custom_headers_lock, __FUNCTION__);
		for (int i = 0; i < m_custom_headers.getCount(); i++)
		{
			sip_header_t* header = m_custom_headers[i];
			if (header != nullptr)
				forbidden.set_custom_header(*header);
		}
	}

	SLOG_SESS(m_log_tag, "%s : Rejected Incoming request %s with %s", m_session_ref,
		(const char*)m_uas_request.GetStartLine(),	(const char*)forbidden.GetStartLine());
	
	return push_message_to_transaction(forbidden);
}
//--------------------------------------
//
//--------------------------------------
bool sip_call_session_t::SendAck(const sip_message_t& response)
{
	sip_message_t ack;
	

	if (!CreateRequestWithinDialog(sip_ACK_e, ack))
	{
		SLOG_WARN(m_log_tag, "%s : Send ACK failed: create request failed!", m_session_ref);
		return false;
	}

	SLOG_SESS(m_log_tag, "%s : Send ACK...", m_session_ref);

	/*if (ack.GetRouteSize() > 0)
	{
		Route route = ack.GetRouteAt(0);
		RouteURI routeURI;
		route.GetURI(routeURI, 0);
		if (!routeURI.IsLooseRouter())
		{
			SLOG_SESS(m_log_tag, "%s : Send ACK : Detected Strict Router %s", m_session_ref, (const char*)routeURI.AsString());
			/// we need to pop it 
			ack.PopTopRouteURI();
			SIPURI oldRURI;
			ack.GetRequestURI(oldRURI);
			SIPURI newURI = routeURI.GetURI();
			
			sip_transport_route_t route;

			route.iface = m_profile.get_interface();
			route.iface_port = m_profile.get_interface_port();
			route.remoteAddress = net_t::get_host_by_name(newURI.GetHost());
			route.remotePort = (uint16_t)strtoul(newURI.GetPort(), nullptr, 10);

			sip_transport_t* transport = m_manager.get_user_agent().get_sip_stack().create_transport(&route);

			ack.set_transport(transport);
			
			ack.SetRequestURI(newURI);
			RouteURI newRouteURI(oldRURI, false);
			Route newRoute;
			newRoute.AddURI(newRouteURI);
			ack.AppendRoute(newRoute, true);
		}
	}*/

	if (ack.is_request() && !ack.has_Max_Forwards())
		ack.make_Max_Forwards_value()->set_number(SIP_DEFAULT_MAX_FORWARDS);

	m_manager.get_user_agent().get_sip_stack().send_message_to_transport(ack);

	ICT_OnAckSent(ack);

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool sip_call_session_t::SendCancel(bool reason_200)
{
	if (m_type == sip_session_server_e) /// Only a UAC can send cancel
		return false;

	if (m_state == StateConnected || m_state == StateQueued)
	{
		return SendBye();
	}

	if (m_state == StateDisconnected || m_state == StateCancelling)
	{
		SLOG_SESS(m_log_tag, "%s : Send CANCEL failed : State Disconnected! Command Ignored", m_session_ref);
		return false;
	}

	if (!m_can_send_cancel)
	{
		SLOG_SESS(m_log_tag, "%s : CANCEL will be send later : not provisional response received!", m_session_ref);
		//m_call_manager.get_user_agent().GetStack().BreakInviteClientTransaction(m_current_uac_invite);
		m_send_cancel = true;
		m_cancel_param = reason_200;

		return true;
	}

	m_state = StateCancelling;

	if (!m_current_uac_invite.IsValid())
		return false;

	{
		rtl::MutexWatchLock l_custom_headers(m_custom_headers_lock, __FUNCTION__);
		for (int i = 0; i < m_custom_headers.getCount(); i++)
		{
			sip_header_t* header = m_custom_headers[i];
			if (header != nullptr)
				m_current_uac_invite.set_custom_header(*header);
		}
	}

	if (!m_call_manager.get_user_agent().get_sip_stack().cancel_invite_client_transaction(m_current_uac_invite, reason_200))
	{
		SLOG_WARN(m_log_tag, "%s : Send CANCEL failed : Cancel invite client transaction failed", m_session_ref);
		return false;
	}

	m_can_send_cancel = false;
	m_send_cancel = false;
	m_cancel_param = false;

	/// wait for the cancel transaction to complete
	return true;// m_uac_cancel_sync.Wait(5000);
}
//--------------------------------------
//
//--------------------------------------
//void sip_call_session_t::process_DNS_failover_event(sip_event_dns_failover_t& failOverEvent)
//{
//	SLOG_SESS(m_log_tag, "%s : DNS fail over!", m_session_ref);
//
//	sip_message_t msg = failOverEvent.get_message();
//	
//	if (msg.IsInvite())
//	{
//		m_current_uac_invite = msg;
//	}
//}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::ICT_OnDisconnectCall()
{
	if (m_state < sip_call_session_t::StateConnected)
	{
		// Not yet connected
		if (m_type == sip_session_client_e)      // Local initiated the call
		{
			SendCancel(m_disconnect_cancel_reason);
		}
		else if (m_type == sip_session_server_e) // Remote initiated the call
		{
			SendAcceptByRejection((sip_status_t)m_disconnect_status_code, m_disconnect_status_reason);
		}
	}
	else 
	{
		SendBye();
	}
}
//--------------------------------------
//
//--------------------------------------
bool sip_call_session_t::SendBye()
{
	SLOG_SESS(m_log_tag, "%s : Send BYE", m_session_ref);

	if (m_call_end_reason == sip_call_session_t::NIST_RecvBye)
	{
		return true;
	}

	if (m_state >= StateDisconnected)
	{
		SLOG_WARN(m_log_tag, "%s : Send BYE failed : state >= State.Disconnected. state=%s Aborting...", m_session_ref, GetSessionStateName(m_state));
		return false;
	}

	if (m_state < StateConnected && m_type == sip_session_client_e)
	{
		return SendCancel(false);
	}
	else if (m_state < StateConnected && m_type == sip_session_server_e)
	{
		return sip_session_t::SendAcceptByRejection(m_current_uas_invite);
	}

	m_state = StateDisconnected;

	sip_message_t bye;

	if (!CreateRequestWithinDialog(sip_BYE_e, bye))
	{
		SLOG_WARN(m_log_tag, "%s : Send BYE failed : create request with in dialog failed!", m_session_ref);
		return false;
	}

	if (m_profile.get_bye_auth())
	{
		if (m_has_auth)
		{
			MakeProxyAuthorization(bye, m_auth);
		}
		else if (m_has_www_auth)
		{
			MakeWWWAuthorization(bye, m_www_auth);
		}
	}

	if (m_type == sip_session_server_e && !m_ack_received && bye.getRoute().type == sip_transport_udp_e)
	{
		m_call_manager.get_retransmit_thread()->stop_retransmission(m_call_id);
	}

	{
		rtl::MutexWatchLock l_custom_headers(m_custom_headers_lock, __FUNCTION__);
		for (int i = 0; i < m_custom_headers.getCount(); i++)
		{
			sip_header_t* header = m_custom_headers[i];
			if (header != nullptr)
				bye.set_custom_header(*header);
		}
	}

	bool result = push_message_to_transaction(bye);

	SLOG_SESS(m_log_tag, "%s : BYE %Ssent", m_session_ref, result ? "" : "not ");

	return result; 
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::OnAckFor200OkExpire(const sip_message_t& ret_200ok)
{
	EnqueueMessageEvent(IST_200OkRetransmitTimeout, ret_200ok);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::process_timer_expires_event(sip_event_timer_expires_t& timerEvent)
{
	SLOG_SESS(m_log_tag, "%s : Timer expire", m_session_ref, sip_timer_manager_t::GetTimerName(timerEvent.get_timer()));

	sip_transaction_timer_t timer = timerEvent.get_timer(); 
	if (m_type == sip_session_server_e && timer == sip_timer_H_e)
	{
		m_call_end_reason = sip_call_session_t::IST_4xxSent;

		sip_message_t response;
		m_current_uas_invite.make_response(response, sip_408_RequestTimeout);
		if (m_trunk != NULL_TRUNK)
			m_trunk->call_session__call_disconnected(*this, response);
	}
	else if (m_type == sip_session_client_e && timer == sip_timer_B_e)
	{
		m_call_end_reason = sip_call_session_t::ICT_Recv4xx;

		sip_message_t response;
		m_current_uac_invite.make_response(response, sip_408_RequestTimeout);
		if (m_trunk != NULL_TRUNK)
			m_trunk->call_session__call_disconnected(*this, response);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::process_transport_failure_event(int failureType)
{
	if (m_trunk != NULL_TRUNK)
	{
		sip_message_t msg;
		m_trunk->call_session__failure(*this);
	}

	sip_session_t::process_transport_failure_event(failureType);
}
//--------------------------------------
//
//--------------------------------------
bool sip_call_session_t::SendRefer(const sip_value_uri_t& referTo)
{
	if (m_state >= StateDisconnected || m_state < StateConnected)
		return false;

	sip_message_t refer;
	if (!CreateRequestWithinDialog(sip_REFER_e, refer))
		return false;

	refer.make_Refer_To_value()->assign(referTo);
	refer.make_Referred_By_value()->set_uri(refer.get_From_value()->get_uri());
	refer.make_Event_value()->assign("refer");

	return push_message_to_transaction(refer);
}
//--------------------------------------
//
//--------------------------------------
bool sip_call_session_t::SendNotify(const sip_value_t& state, const sip_value_t& sip_event, const sip_value_t& contentType, const rtl::String& body)
{
	if (m_state >= StateDisconnected || m_state < StateConnected)
		return false;

	sip_message_t notify;
	if (!CreateRequestWithinDialog(sip_NOTIFY_e, notify))
		return false;

	notify.make_Subscription_State_value()->assign(state);
	notify.make_Event_value()->assign(sip_event);
	notify.make_Content_Type_value()->assign(contentType);
	notify.set_body((const char*)body, body.getLength());

	return push_message_to_transaction(notify);
}
//--------------------------------------
//
//--------------------------------------
bool sip_call_session_t::SendOptions()
{
	if (m_state >= StateDisconnected || m_state < StateConnected)
		return false;

	sip_message_t options;
	if (!CreateRequestWithinDialog(sip_OPTIONS_e, options))
	{
		return false;
	}

	options.make_Accept()->add_value("application/sdp");
	options.make_Content_Length_value()->set_number(0);

	return push_message_to_transaction(options);
}
//--------------------------------------
//
//--------------------------------------
bool sip_call_session_t::SendHold(bool hold)
{
	if (m_state != StateConnected)
	{
		SLOG_WARN(m_log_tag, "%s : Send HOLD failed : state %s != State.Connected", m_session_ref, GetSessionStateName(m_state));
		return false;
	}

	sip_message_t invite;
	if (!CreateRequestWithinDialog(sip_INVITE_e, invite))
	{
		SLOG_WARN(m_log_tag, "%s : Send HOLD failed : Create request within dialog failed", m_session_ref);
		return false;
	}

	if (m_trunk == NULL_TRUNK || !m_trunk->call_session__prepare_sdp_reoffer(*this, invite, hold))
	{
		SLOG_WARN(m_log_tag, "%s : Send HOLD failed : require SDP offer failed!", m_session_ref);
		return false;
	}

	if (m_has_auth)
	{
		MakeProxyAuthorization(invite, m_auth);
	}
	else if (m_has_www_auth)
	{
		MakeWWWAuthorization(invite, m_www_auth);
	}

	if (m_dialog_type == sip_dialog_UAS_e)
	{
		m_current_uac_invite = invite;
	}

	return push_message_to_transaction(invite); 
}
//--------------------------------------
//
//--------------------------------------
bool sip_call_session_t::SendReinvite()
{
	if (m_state != StateConnected)
	{
		SLOG_WARN(m_log_tag, "%s : Send Reinvite failed : state %s != State.Connected", m_session_ref, GetSessionStateName(m_state));
		return false;
	}

	bool can_reinvite;

	{
		rtl::MutexWatchLock lock(m_reinvite_lock, __FUNCTION__);

		can_reinvite = m_reinvite_state == ReinviteNone;
		if (can_reinvite)
		{
			m_reinvite_state = ReinviteOutgoing;
		}
	}

	if (!can_reinvite)
		return false;

	sip_message_t invite;
	if (!CreateRequestWithinDialog(sip_INVITE_e, invite))
	{
		SLOG_WARN(m_log_tag, "%s : Send Reinvite failed : Create request within dialog failed", m_session_ref);
		return false;
	}

	if (m_trunk == NULL_TRUNK || !m_trunk->call_session__prepare_sdp_reoffer(*this, invite, false))
	{
		SLOG_WARN(m_log_tag, "%s : Send Reinvite failed : require SDP offer failed!", m_session_ref);
		return false;
	}

	if (m_has_auth)
	{
		MakeProxyAuthorization(invite, m_auth);
	}
	else if (m_has_www_auth)
	{
		MakeWWWAuthorization(invite, m_www_auth);
	}

	if (m_dialog_type == sip_dialog_UAS_e)
	{
		m_current_uac_invite = invite;
	}

	return push_message_to_transaction(invite); 
}
//--------------------------------------
//
//--------------------------------------
bool sip_call_session_t::SendDTMF(char digit, int duration)
{
	static const char* RFC2833Table1Events = "0123456789*#ABCD!";
	if (m_state >= StateDisconnected || m_state < StateConnected)
	{
		SLOG_WARN(m_log_tag, "%s : Send DTMF failed : State.%s != State.Connected", m_session_ref, GetSessionStateName(m_state));
		return false;
	}

	sip_message_t info;
	
	if (!CreateRequestWithinDialog(sip_INFO_e, info))
	{
		SLOG_WARN(m_log_tag, "%s : Send DTMF failed: create request within dialog failed!", m_session_ref);
		return false;
	}

	const char* cp = strchr(RFC2833Table1Events, digit);

	if (cp == nullptr)
		return false;

	int toneIndex = PTR_DIFF(cp, RFC2833Table1Events);

	info.make_Content_Type()->assign("application/dtmf-relay");

	rtl::String body;
	body << "Signal=" << toneIndex << "\r\n";
	body << "Duration=" << duration << "\r\n";

	info.set_body(body, body.getLength());

	if (m_has_auth)
	{
		MakeProxyAuthorization(info, m_auth);
	}
	else if (m_has_www_auth)
	{
		MakeWWWAuthorization(info, m_www_auth);
	}

	return push_message_to_transaction(info);
}
//--------------------------------------
//
//--------------------------------------
bool sip_call_session_t::SendTextMessage(const char* message)
{
	if (m_state >= StateDisconnected || m_state < StateConnected)
		return false;

	sip_message_t info;
	if (!CreateRequestWithinDialog(sip_MESSAGE_e, info))
		return false;


	info.get_Content_Type()->assign("text/plain");

	info.set_body(message);

	return push_message_to_transaction(info);
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::set_custom_headers(const rtl::ArrayT<sip_header_t*>& headers)
{
	clear_custom_headers();

	if (headers.getCount() == 0)
	{
		return;
	}

	rtl::MutexWatchLock lock(m_custom_headers_lock, __FUNCTION__);

	for (int i = 0; i < headers.getCount(); i++)
	{
		const sip_header_t* hdr = headers[i];
		sip_header_t* hdr_copy = NEW sip_header_t(*hdr);
		m_custom_headers.add(hdr_copy);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_call_session_t::clear_custom_headers()
{
	rtl::MutexWatchLock lock(m_custom_headers_lock, __FUNCTION__);

	for (int i = 0; i < m_custom_headers.getCount(); i++)
	{
		DELETEO(m_custom_headers[i]);
	}

	m_custom_headers.clear();
}
//--------------------------------------
//
//--------------------------------------
const char* sip_call_session_t::GetEventName(int eventId)
{
	static const char* eventNames[] = 
	{
	"ICT_Recv1xx",			"ICT_Recv2xx",			"ICT_Recv3xx",
	"ICT_Recv4xx",			"ICT_Recv5xx",			"ICT_Recv6xx",
	"ICT_RecvUnknown",		"ICT_InviteSent",		"ICT_AckSent",
	"ICT_UnknownSent",

	"NICT_Recv1xx",			"NICT_Recv2xx",			"NICT_Recv3xx",
	"NICT_Recv4xx",			"NICT_Recv5xx",			"NICT_Recv6xx",
	"NICT_RecvUnknown",		"NICT_ByeSent",			"NICT_OptionsSent",
	"NICT_InfoSent",		"NICT_CancelSent",		"NICT_NotifySent",
	"NICT_SubscribeSent",	"NICT_UnknownSent",
	
	"IST_RecvInvite",		"IST_RecvMoreInvite",	"IST_RecvAck",
	"IST_RecvUnknown",		"IST_1xxSent",			"IST_2xxSent",
	"IST_3xxSent",			"IST_4xxSent",			"IST_5xxSent",
	"IST_6xxSent",			"IST_UnknownSent",
	
	"NIST_RecvBye",			"NIST_RecvOptions",		"NIST_RecvInfo",
	"NIST_RecvCancel",		"NIST_RecvNotify",		"NIST_RecvSubscribe",
	"NIST_RecvRefer",		"NIST_RecvMessage",		"NIST_RecvUpdate",
	"NIST_RecvUnknown",		"NIST_1xxSent",			"NIST_2xxSent",
	"NIST_3xxSent",			"NIST_4xxSent",			"NIST_5xxSent",
	"NIST_6xxSent",			"NIST_UnknownSent",
	
	"IST_AnswerNow",		"IST_AnswerDeferred",			"IST_AnswerDeferredWithMedia",
	"IST_AnswerRedirect",	"IST_AnswerRedirectToProxy",	"IST_AnswerQueued",
	"IST_AnswerDenied",		"IST_200OkRetransmitTimeout",	"IST_SendUnauthorized",

	"ICT_DisconnectCall",
	};

	size_t idx = eventId - 101;

	return (idx < sizeof(eventNames)/sizeof(const char*)) ? eventNames[idx] : "UnknownEvent";
}
//--------------------------------------
//
//--------------------------------------
sip_null_trunk_t::~sip_null_trunk_t()
{
}
//--------------------------------------
//
//--------------------------------------
void sip_null_trunk_t::call_session__recv_1xx(sip_call_session_t& session, const sip_message_t& message) { }
//--------------------------------------
//
//--------------------------------------
void sip_null_trunk_t::call_session__call_established(sip_call_session_t& session, const sip_message_t& message) { }
//--------------------------------------
//
//--------------------------------------
void sip_null_trunk_t::call_session__call_disconnected(sip_call_session_t& session, const sip_message_t& message) { }
//--------------------------------------
//
//--------------------------------------
void sip_null_trunk_t::call_session__recv_ack(sip_call_session_t& session, const sip_message_t& msg) { }
//--------------------------------------
//
//--------------------------------------
void sip_null_trunk_t::call_session__ack_sent(sip_call_session_t& session) { }
//--------------------------------------
//
//--------------------------------------
reinvite_response_t sip_null_trunk_t::call_session__recv_reinvite(sip_call_session_t& session, const sip_message_t& offer) { return reinvite_response_ok_e; }
//--------------------------------------
//
//--------------------------------------
void sip_null_trunk_t::call_session__call_reestablished(sip_call_session_t& session, const sip_message_t& message) { }
//--------------------------------------
//
//--------------------------------------
bool sip_null_trunk_t::call_session__apply_sdp_offer(sip_call_session_t& session, const sip_message_t& offer) { return false; }
//--------------------------------------
//
//--------------------------------------
bool sip_null_trunk_t::call_session__apply_sdp_answer(sip_call_session_t& session, const sip_message_t& andwer) { return false; }
//--------------------------------------
//
//--------------------------------------
bool sip_null_trunk_t::call_session__prepare_sdp_offer(sip_call_session_t& session, sip_message_t& offer) { return false; }
//--------------------------------------
//
//--------------------------------------
bool sip_null_trunk_t::call_session__prepare_sdp_answer(sip_call_session_t& session, sip_message_t& answer) { return false; }
//--------------------------------------
//
//--------------------------------------
bool sip_null_trunk_t::call_session__apply_sdp_reoffer(sip_call_session_t& session, const sip_message_t& offer) { return false; }
//--------------------------------------
//
//--------------------------------------
bool sip_null_trunk_t::call_session__apply_sdp_reanswer(sip_call_session_t& session, const sip_message_t& anser) { return false; }
//--------------------------------------
//
//--------------------------------------
bool sip_null_trunk_t::call_session__prepare_sdp_reoffer(sip_call_session_t& session, sip_message_t& offer, bool hold) { return false; }
//--------------------------------------
//
//--------------------------------------
bool sip_null_trunk_t::call_session__prepare_sdp_reanswer(sip_call_session_t& session, sip_message_t& answer) { return false; }
//--------------------------------------
//
//--------------------------------------
bool sip_null_trunk_t::call_session__recv_ni_request(sip_call_session_t& session, const sip_message_t& request, sip_message_t& response) { return false; }
//--------------------------------------
//
//--------------------------------------
void sip_null_trunk_t::call_session__recv_ni_response(sip_call_session_t& session, const sip_message_t& msg) { }
//--------------------------------------
//
//--------------------------------------
void sip_null_trunk_t::call_session__failure(sip_call_session_t& session) { }
//--------------------------------------
//
//--------------------------------------
const char* get_reinvite_response_name(reinvite_response_t code)
{
	const char* names[] = { "reinvite_response_wait_e", "reinvite_response_ok_e", "reinvite_response_not_accepted_e" };

	return code >= reinvite_response_wait_e && code < reinvite_response_error_e ? names[code] : "reinvite_response_error_e";
}
//--------------------------------------
