/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_transport_defs.h"
//--------------------------------------
//
//--------------------------------------
const char* TransportType_toString(sip_transport_type_t type)
{
	static const char* type_names[] = { "UDP", "TCP", "TLS", "WS", "WSS" };

	return type >= sip_transport_udp_e && type <= sip_transport_wss_e ?
		type_names[type] : "ERR";
}
//--------------------------------------
//
//--------------------------------------
sip_transport_type_t parse_transport_type(const char* name)
{
	if (name == nullptr || name[0] == 0)
		return sip_transport_udp_e;

	if (std_stricmp(name, "udp") == 0)
		return sip_transport_udp_e;

	if (std_stricmp(name, "tcp") == 0)
		return sip_transport_tcp_e;

	if (std_stricmp(name, "tls") == 0)
		return sip_transport_tls_e;

	if (std_stricmp(name, "ws") == 0)
		return sip_transport_ws_e;

	if (std_stricmp(name, "wss") == 0)
		return sip_transport_wss_e;

	return sip_transport_udp_e;
}
//--------------------------------------
//
//--------------------------------------
sip_transport_type_t parse_transport_type(const char* name, int len)
{
	if (name == nullptr || name[0] == 0)
		return sip_transport_udp_e;

	if (len == 3)
	{
		if (std_strnicmp(name, "udp", 3) == 0)
			return sip_transport_udp_e;

		if (std_strnicmp(name, "tcp", 3) == 0)
			return sip_transport_tcp_e;

		if (std_strnicmp(name, "tls", 3) == 0)
			return sip_transport_tls_e;

		if (std_strnicmp(name, "wss", 3) == 0)
			return sip_transport_wss_e;
	}
	else if (len == 2)
	{
		if (std_strnicmp(name, "ws", 2) == 0)
			return sip_transport_ws_e;
	}


	return sip_transport_udp_e;
}
//--------------------------------------
//
//--------------------------------------
const char* route_to_string(char* buffer, int length, const sip_transport_route_t& route)
{
	int l = std_snprintf(buffer, length, "%s %s:%u", TransportType_toString(route.type), inet_ntoa(route.if_address), route.if_port);
	l = std_snprintf(buffer + l, length - l, "-%s:%u", inet_ntoa(route.remoteAddress), route.remotePort);
	return buffer;
}
//--------------------------------------
//
//--------------------------------------
const char* point_to_string(char* buffer, int length, const sip_transport_point_t& point)
{
	int l = std_snprintf(buffer, length, "%s %s:%u", TransportType_toString(point.type), inet_ntoa(point.address), point.port);
	buffer[l] = 0;
	return buffer;
}
//--------------------------------------
