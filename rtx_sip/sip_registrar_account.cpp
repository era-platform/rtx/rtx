﻿/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_transport_manager.h"
#include "sip_registrar_account.h"
#include "sip_registrar.h"
#include "std_crypto.h"
//--------------------------------------
//
//--------------------------------------
void sip_registrar_account_t::add_ref() 
{
	uint32_t ref = std_interlocked_inc(&m_ref_count);

	SLOG_CALL("reg-acc", "%s : add_ref %u", m_username, ref);
}
//--------------------------------------
//
//--------------------------------------
void sip_registrar_account_t::release()
{
	uint32_t ref = std_interlocked_dec(&m_ref_count);

	SLOG_CALL("reg-acc", "%s : release %u", m_username, ref);

	if (ref == 0)
	{
		SLOG_CALL("reg-acc", "%s : destruct account", m_username);
		DELETEO(this);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_registrar_account_t::clear()
{
	rtl::MutexLock lock(m_sync);

	FREE(m_username);
	FREE(m_auth_id);
	FREE(m_password);
	
	for (int i = 0; i < m_register_list.getCount(); i++)
	{
		sip_registrar_session_t* session = m_register_list[i];
		DELETEO(session);
	}

	m_register_list.clear();

	for (int i = 0; i < m_subscribe_list.getCount(); i++)
	{
		sip_subscribe_session_t* session = m_subscribe_list[i];
		DELETEO(session);
	}

	m_subscribe_list.clear();

	for (int i = 0; i < m_auth_list.getCount(); i++)
	{
		sip_registrar_auth_t& auth = m_auth_list[i];

		if (auth.call_id != nullptr)
			FREE(auth.call_id);
		if (auth.nonce != nullptr)
			FREE(auth.nonce);
	}

	m_auth_list.clear();
}
//--------------------------------------
// если отключим то убиваем все сессии
//--------------------------------------
void sip_registrar_account_t::set_enabled(bool enabled)
{
	rtl::MutexLock lock(m_sync);

	m_enabled = enabled;

	if (!m_enabled)
	{
		SLOG_CALL("reg-acc", "%s : disabling account...", m_username);

		for (int i = 0; i < m_register_list.getCount(); i++)
		{
			sip_registrar_session_t* session = m_register_list[i];

			sip_registrar_contact_info_t* contact_info = session->get_sip_contact();

			// рейз анрегистер евент енд дестрой зис сешн
			// если сессия больше не содержит контактов то удаляем ее 
			SLOG_CALL("reg-acc", "%s : remove disabled session '%s'", m_username, session->get_call_id());

			write_to_regdb(session, true);

			remove_reg_session(session);

			raise_unregister_event(contact_info, sip_unregister_disabled_e);
		}
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_registrar_account_t::setup(const char* username, const char* auth_id, const char* password, int max_expires)
{
	SLOG_CALL("reg-acc", "new : setup user '%s' auth '%s' and password '%s'",
		username,
		auth_id,
		password == nullptr || password[0] == 0 ? "(null)" : "***");

	rtl::MutexLock lock(m_sync);

	// проверим изменения
	// username
	bool raise_lost_event = rtl::strcmp(m_username, username) != 0;

	// password
	if (!raise_lost_event)
	{
		// username same
		raise_lost_event = rtl::strcmp(m_password, password) != 0 || rtl::strcmp(m_auth_id, auth_id) != 0;
	}

	FREE(m_username);
	FREE(m_auth_id);
	FREE(m_password);

	m_username = rtl::strdup(username);
	m_auth_id = rtl::strdup(auth_id);
	m_password = rtl::strdup(password);
	m_max_expires = max_expires;

	if (raise_lost_event)
	{
		for (int i = 0; i < m_register_list.getCount(); i++)
		{
			sip_registrar_session_t* session = m_register_list[i];

			sip_registrar_contact_info_t* contact_info = session->get_sip_contact();		

			// если сессия больше не содержит контактов то удаляем ее 
			SLOG_CALL("reg-acc", "%s: remove old session '%s' ?", m_username, session->get_call_id());

			write_to_regdb(session, true);

			m_register_list.removeAt(i);
			
			DELETEO(session);

			raise_unregister_event(contact_info, sip_unregister_reset_e);
		}

		m_register_list.clear();
	}

	SLOG_CALL("reg-acc", "%s: setup done", m_username);
}
//--------------------------------------
//
//--------------------------------------
void sip_registrar_account_t::update(const char* auth_id, const char* password)
{
	SLOG_CALL("reg-acc", "%s: update auth '%s' and password '%s'",
		m_username,
		auth_id,
		password == nullptr || password[0] == 0 ? "(null)" : "***");

	rtl::MutexLock lock(m_sync);

	// проверим изменения
	// username
	if (rtl::strcmp(auth_id, m_auth_id) == 0 && rtl::strcmp(password, password) == 0)
	{
		SLOG_CALL("reg-acc", "%s: values not changed!", m_username);
		return;
	}

	FREE(m_auth_id);
	FREE(m_password);

	m_auth_id = rtl::strdup(auth_id);
	m_password = rtl::strdup(password);

	for (int i = 0; i < m_register_list.getCount(); i++)
	{
		sip_registrar_session_t* session = m_register_list[i];
		sip_registrar_contact_info_t* contact_info = session->get_sip_contact();

		// если сессия больше не содержит контактов то удаляем ее 
		SLOG_CALL("reg-acc", "%s: remove invalid session '%s'", m_username, session->get_call_id());

		write_to_regdb(session, true);

		m_register_list.removeAt(i);
		DELETEO(session);

		raise_unregister_event(contact_info, sip_unregister_request_e);
	}

	m_register_list.clear();

	SLOG_CALL("reg-acc", "%s: update done", m_username);
}
//--------------------------------------
//
//--------------------------------------
void sip_registrar_account_t::process_transport_disconnected(const sip_transport_route_t& route)
{
	rtl::MutexLock lock(m_sync);

	// account has no sessions!
	if (m_register_list.getCount() == 0)
		return;

	//SLOG_CALL("reg-acc", "%s : captured transport disconnected %s:%u", m_username, inet_ntoa(route.remoteAddress), route.remotePort);

	for (int i = m_register_list.getCount() - 1; i >= 0; i--)
	{
		sip_registrar_session_t* session = m_register_list[i];

		if (session->check_transport_disconnected(route))
		{
			sip_registrar_contact_info_t* contact_info = session->get_sip_contact();

			// если сессия больше не содержит контактов то удаляем ее 
			SLOG_CALL("reg-acc", "%s : remove session '%s' with  broken connection", m_username, session->get_call_id());

			write_to_regdb(session, true);

			//удаляем вручную!
			m_register_list.removeAt(i);
			DELETEO(session);

			raise_unregister_event(contact_info, sip_unregister_failed_e);
		}
	}

	for (int i = m_subscribe_list.getCount() - 1; i >= 0; i--)
	{
		sip_subscribe_session_t* session = m_subscribe_list[i];

		if (session->check_transport_disconnected(route))
		{
			//удаляем вручную!
			m_subscribe_list.removeAt(i);
			DELETEO(session);
		}
	}
}
//--------------------------------------
// поиск по call-id иначе по адресам
//
//--------------------------------------
bool sip_registrar_account_t::process_incoming_request(const sip_message_t& request)
{
	if (!m_enabled)
		return false;

	rtl::String call_id = request.CallId();
	bool auth_checked = false;
	rtl::String method = request.get_request_method_name();

	SLOG_CALL("reg-acc", "%s : incoming %s request call-id: %s", m_username, (const char*)method, (const char*)call_id);
	
		// проверим на предмет работы сервера из-за NAT
	const sip_uri_t& ruri = request.get_request_uri();
	const rtl::String& host = ruri.get_host();

	bool over_nat = m_registrar.is_over_nat(host, request.get_interface_address(), request.get_remote_address());

	rtl::MutexLock lock(m_sync);

	// проверим на звездочку '*'!
	if (check_for_asterisk(request))
	{
		return true;
	}

	// проверяем авторизацию
	if (!check_athorization(request, auth_checked))
	{
		return auth_checked;
	}

	if (method *= "REGISTER")
	{
		return process_register_request(request, over_nat);
	}
	else if (method *= "SUBSCRIBE")
	{
		return process_subscribe_request(request, over_nat);
	}

	return false;
}
//--------------------------------------
//
//--------------------------------------
bool sip_registrar_account_t::process_register_request(const sip_message_t& request, bool over_nat)
{
	// ищем сессию
	sip_registrar_session_t* session = find_reg_session(request.CallId());

	if (session != nullptr)
	{
		// обновим существующую сессию
		return update_reg_session(session, request, over_nat);
	}

	in_addr r_addr = request.get_remote_address();
	uint16_t remotePort = request.get_remote_port();

	session = find_reg_session(r_addr, remotePort);

	if (session != nullptr)
	{
		// заменим существующую сессию новой
		return replace_reg_session(session, request, over_nat);
	}

	// проверим expires на разрегистрацию
	/*Expires x;
	if (request.GetExpires(x))
	{
		if (x.AsInteger() == 0)
			return false;
		
	}
	else
	{
		Contact c;
		ContactURI curi;

		if (request.GetContactAt(c, 0))
		{
			c.GetURI(curi, 0);

			rtl::String expires;
			if (curi.GetParameter("expires", expires))
			{
				int expires_val = atoi(expires);
				if (expires_val == 0)
					return false;
			}
		}
	}*/

	// создадим новую сессию
	return create_new_reg_session(request, over_nat);
}
//--------------------------------------
//
//--------------------------------------
bool sip_registrar_account_t::process_subscribe_request(const sip_message_t& request, bool over_nat)
{
	// ищем сессию
	sip_subscribe_session_t* session = find_sub_session(request.CallId());

	if (session != nullptr)
	{
		// обновим существующую сессию
		return update_sub_session(session, request, over_nat);
	}

	// создадим новую сессию
	return create_new_sub_session(request, over_nat);
}
//--------------------------------------
//
//--------------------------------------
void sip_registrar_account_t::check_contacts(time_t current_time)
{
	rtl::MutexLock lock(m_sync);

	// проверим регистровые сессии
	for (int i = m_register_list.getCount() - 1; i >= 0; i--)
	{
		sip_registrar_session_t* session = m_register_list[i];
		// если сессия больше не содержит контактов то удаляем ее 
		
		sip_registrar_contact_info_t* contact_info = session->get_sip_contact();

		if (!session->check_contacts(current_time))
		{
			SLOG_CALL("reg-acc", "%s : remove expired session '%s'", m_username, session->get_call_id());

			
			write_to_regdb(session, true);
			m_register_list.removeAt(i);
			DELETEO(session);

			raise_unregister_event(contact_info, sip_unregister_expires_e);
		}
		else
		{
			DELETEO(contact_info);
		}
	}

	// проверим подписки на события
	for (int i = m_subscribe_list.getCount() - 1; i >= 0; i--)
	{
		sip_subscribe_session_t* session = m_subscribe_list[i];

		// если сессия больше не содержит контактов то удаляем ее 

		if (!session->check_expire_timer(current_time))
		{
			SLOG_CALL("reg-acc", "%s : remove expired session '%s'", m_username, session->get_call_id());

			m_subscribe_list.removeAt(i);
			DELETEO(session);

			// событие не генерируем. в логике собственный таймер убивает просроченные подписки
		}
	}

	// проверим очередь авторизации
	for (int i = m_auth_list.getCount()-1; i >= 0 ; i--)
	{
		sip_registrar_auth_t& auth = m_auth_list[i];
		if (current_time - auth.m_auth_time > 30)
		{
			if (auth.call_id != nullptr)
				FREE(auth.call_id);
			if (auth.nonce != nullptr)
				FREE(auth.nonce);

			m_auth_list.removeAt(i);
		}
	}
}
//--------------------------------------
//
//--------------------------------------
int sip_registrar_account_t::get_contacts(rtl::PonterArrayT<sip_registrar_contact_info_t>& contacts) const
{
	contacts.clear();

	rtl::MutexLock lock(m_sync);

	for (int i = 0; i < m_register_list.getCount(); i++)
	{
		const sip_registrar_session_t* session = m_register_list[i];

		session->get_contacts(contacts);
	}

	return contacts.getCount();
}
//--------------------------------------
// updating over nat flag
//--------------------------------------
bool sip_registrar_account_t::check_contact(sip_registrar_contact_info_t* contact) const
{
	rtl::MutexLock lock(m_sync);

	const sip_registrar_session_t* session = find_reg_session(contact->get_contact().address, contact->get_contact().port);
	
	return session != nullptr ?	session->check_contact(contact) : false;
}
//--------------------------------------
//
//--------------------------------------
void sip_registrar_account_t::send_sub_notify(const sip_registrar_subscribe_info_t* notify_info)
{
	if (!m_enabled)
		return;

	SLOG_CALL("reg-acc", "%s : send notify...", m_username);

	const char* call_id = notify_info->call_id; // std_wcstombcs(notify_info->call_id);

	sip_subscribe_session_t* sub_session = find_sub_session(call_id);

	if (sub_session != nullptr)
	{
		sub_session->send_notify(notify_info);

		SLOG_CALL("reg-acc", "%s : notify send", m_username);
	}
	else
	{
		SLOG_CALL("reg-acc", "%s : subscribe session not found", m_username);
	}
}
//--------------------------------------
//
//--------------------------------------
bool sip_registrar_account_t::update_reg_session(sip_registrar_session_t* session, const sip_message_t& request, bool over_nat)
{
	bool result;

	// передадим на обработку
	SLOG_CALL("reg-acc", "%s : update existing session '%s' over_nat:%s", m_username, session->get_call_id(), STR_BOOL(over_nat));

	sip_registrar_contact_info_t* contact_info = session->get_sip_contact();

	if (result = session->process_request(request, over_nat))
	{
		// проверим на expires
		if (session->check_contacts(time(nullptr)))
		{
			write_to_regdb(session, false);
			raise_register_event(*session, contact_info);
		}
		else
		{
			SLOG_CALL("reg-acc", "%s : update : remove expired session '%s'", m_username, session->get_call_id());

			write_to_regdb(session, true);

			remove_reg_session(session);
			
			raise_unregister_event(contact_info, sip_unregister_expires_e);
		}
	}
	else
	{
		SLOG_CALL("reg-acc", "%s : update : bad request '%s'", m_username, session->get_call_id());
	}

	return result;
}
//--------------------------------------
// с того же устройства но новая регистрация
// добавить новую и убить старую
// уведомления в таком порядке: sync_detect потом sync_lost
//--------------------------------------
bool sip_registrar_account_t::replace_reg_session(sip_registrar_session_t* session, const sip_message_t& request, bool over_nat)
{
	sip_registrar_session_t* new_session = create_reg_session(request);
		
	if (new_session->process_request(request, over_nat))
	{
		SLOG_CALL("reg-acc", "%s : replace : register new '%s' over_nat:%s", m_username, new_session->get_call_id(), STR_BOOL(over_nat));

		sip_registrar_contact_info_t* contact_info = new_session->get_sip_contact();

		write_to_regdb(new_session, false);
		raise_register_event(*new_session, contact_info);

		SLOG_CALL("reg-acc", "%s : replace : remove old session '%s'", m_username, session->get_call_id());

		contact_info = session->get_sip_contact();

		write_to_regdb(session, true);
		
		remove_reg_session(session);

		raise_unregister_event(contact_info, sip_unregister_replace_e);
	}
	else
	{
		SLOG_CALL("reg-acc", "%s : replace : bad request '%s'", m_username, new_session->get_call_id());

		remove_reg_session(new_session);
		new_session = nullptr;
	}

	return new_session != nullptr;
}
//--------------------------------------
// полностью новая сессия!
//--------------------------------------
bool sip_registrar_account_t::create_new_reg_session(const sip_message_t& request, bool over_nat)
{
	sip_registrar_session_t* new_session = create_reg_session(request);

	SLOG_CALL("reg-acc", "%s : create new session : '%s' over_nat:%s", m_username, (const char*)request.CallId(), STR_BOOL(over_nat));

	const sip_value_via_t* via = request.get_Via_value();
	if (via)
	{
		SLOG_CALL("reg-acc", "%s : create new session : via string %s", m_username, (const char*)via->to_string());
	}
	
	if (new_session->process_request(request, over_nat))
	{
		SLOG_CALL("reg-acc", "%s : create new session : request processed", m_username);

		sip_registrar_contact_info_t* contact_info = new_session->get_sip_contact();
		write_to_regdb(new_session, false);
		raise_register_event(*new_session, contact_info);
	}
	else
	{
		SLOG_CALL("reg-acc", "%s : create new : bad request '%s'", m_username, new_session->get_call_id());

		remove_reg_session(new_session);
		new_session = nullptr;
	}

	return new_session != nullptr;
}
//--------------------------------------
//
//--------------------------------------
bool sip_registrar_account_t::update_sub_session(sip_subscribe_session_t* session, const sip_message_t& request, bool over_nat)
{
	bool result;

	// передадим на обработку
	SLOG_CALL("reg-acc", "%s : update existing subscribe session '%s'", m_username, session->get_call_id());

	if (result = session->process_request(request, over_nat))
	{
		// проверим на expires
		sip_message_t response;

		request.make_response(response, sip_200_OK);

		//Expires expires(session->get_expires_value());
		//response.SetExpires(expires);

		raise_subscribe_event(*session, request, response);

		send_answer(response);

		// true - alive, false - expired
		if (!session->check_expire_timer(time(nullptr)))
		{
			SLOG_CALL("reg-acc", "%s : update : remove subscribe session '%s'", m_username, session->get_call_id());

			remove_sub_session(session);
		}
	}
	else
	{
		SLOG_CALL("reg-acc", "%s : update : bad request '%s'", m_username, session->get_call_id());
	}

	return result;
}
//--------------------------------------
//
//--------------------------------------
bool sip_registrar_account_t::create_new_sub_session(const sip_message_t& request, bool over_nat)
{
	sip_subscribe_session_t* new_session = create_sub_session(request);

	SLOG_CALL("reg-acc", "%s : create new subscribe session : '%s'", m_username, (const char*)request.CallId());

	if (new_session->process_request(request, over_nat))
	{
		sip_message_t response;

		request.make_response(response, sip_200_OK);

		raise_subscribe_event(*new_session, request, response);

		send_answer(response);
	}
	else
	{
		SLOG_CALL("reg-acc", "%s : create new : bad request '%s'", m_username, new_session->get_call_id());

		remove_sub_session(new_session);
		new_session = nullptr;
	}

	return new_session != nullptr;
}
//--------------------------------------
//
//--------------------------------------
sip_registrar_session_t* sip_registrar_account_t::find_reg_session(const char* call_id, int* index)
{
	rtl::MutexLock lock(m_sync);

	for (int i = 0; i < m_register_list.getCount(); i++)
	{
		sip_registrar_session_t* session = m_register_list[i];

		if (strcmp(session->get_call_id(), call_id) == 0)
		{
			if (index != nullptr)
			{
				*index = i;
			}
			
			return session;
		}
	}

	if (index != nullptr)
	{
		*index = -1;
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
sip_registrar_session_t* sip_registrar_account_t::find_reg_session(in_addr r_addr, uint16_t remotePort, int* index)
{
	rtl::MutexLock lock(m_sync);

	for (int i = 0; i < m_register_list.getCount(); i++)
	{
		sip_registrar_session_t* session = m_register_list[i];

		if (session->getRoute().remoteAddress.s_addr == r_addr.s_addr && session->getRoute().remotePort == remotePort)
		{
			if (index != nullptr)
			{
				*index = i;
			}

			return session;
		}
	}

	if (index != nullptr)
	{
		*index = -1;
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
const sip_registrar_session_t* sip_registrar_account_t::find_reg_session(in_addr r_addr, uint16_t remotePort, int* index) const
{
	rtl::MutexLock lock(m_sync);

	for (int i = 0; i < m_register_list.getCount(); i++)
	{
		sip_registrar_session_t* session = m_register_list[i];

		if (session->getRoute().remoteAddress.s_addr == r_addr.s_addr && session->getRoute().remotePort == remotePort)
		{
			if (index != nullptr)
			{
				*index = i;
			}

			return session;
		}
	}

	if (index != nullptr)
	{
		*index = -1;
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
sip_subscribe_session_t* sip_registrar_account_t::find_sub_session(const char* call_id, int* index)
{
	rtl::MutexLock lock(m_sync);

	for (int i = 0; i < m_subscribe_list.getCount(); i++)
	{
		sip_subscribe_session_t* session = m_subscribe_list[i];

		if (strcmp(session->get_call_id(), call_id) == 0)
		{
			if (index != nullptr)
			{
				*index = i;
			}

			return session;
		}
	}

	if (index != nullptr)
	{
		*index = -1;
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
void sip_registrar_account_t::send_401_Unauthorized(const sip_message_t& request)
{
	sip_message_t unauthorized;
	request.make_response(unauthorized, sip_401_Unauthorized);

	// "WWW-Authenticate: Digest realm="voip.homeunix.org", nonce="88ec79faf65b92e4263829498821749a", opaque="a2d30d6ab6eb1a435c86c832c99cec21", algorithm=MD5"
	//WWWAuthenticate auth;
	

	const sip_uri_t& m_remote_uri = request.From_URI();

	const sip_registrar_auth_t& rauth = get_auth(request);

	sip_value_auth_t* auth = unauthorized.make_WWW_Authenticate_value();

	auth->set_scheme("Digest");
	auth->set_auth_param("realm", sip_utils::Quote(m_remote_uri.get_host()));
	auth->set_auth_param("nonce", sip_utils::Quote(rauth.nonce));
	auth->set_auth_param("opaque", "\"6f706171756544617461\"");
	auth->set_auth_param("algorithm", "MD5");
	

	SLOG_CALL("reg-acc", "%s : sending responce 401 Unauthorized", m_username);

	send_answer(unauthorized);
}
//--------------------------------------
//
//--------------------------------------
bool sip_registrar_account_t::check_athorization(const sip_message_t& request, bool& checked)
{
	const sip_value_auth_t* auth = request.get_Authorization_value();

	if (!auth)
	{
		SLOG_CALL("reg-acc", "%s : request has no authorization header -> send 401 unathorized", m_username);
		send_401_Unauthorized(request);
		checked = true;
		return false;
	}

	// WWW-Authenticate:
	/*
	Digest	realm=voip.homeunix.org,
			nonce="48818999e02f8258f3c15c88f7c904e7",
			opaque="8d99e89591b57e262601a894391e4fe8", | opaque=""
			qop="auth, auty-int"
			algorithm=MD5*/
		
	// Authorization:
	/*
	Digest	username="2222",
			realm="voip.homeunix.org",
			algorithm=MD5,
			uri="sip:voip.homeunix.org",
			nonce="48818999e02f8258f3c15c88f7c904e7",
			opaque="8d99e89591b57e262601a894391e4fe8", | opaque=""
			cnonce="xxxxx",
			nc="00000001",
			response="b06feaf34eea2bd75533bea986e5fab8"*/

	const sip_registrar_auth_t& req_auth = get_auth(request);

	rtl::String auth_id;
	rtl::String realm;
	rtl::String uri;
	rtl::String nonce;
	rtl::String opaque;
	rtl::String response;
	rtl::String method = request.get_request_method_name().toUpper();

	checked = false;

	if (!auth->get_auth_param("username", auth_id))
	{
		SLOG_WARN("reg-acc", "%s : request has no 'username' parameter", m_username);
		return false;
	}

	sip_utils::UnQuote(auth_id);

	if (!auth->get_auth_param("realm", realm))
	{
		SLOG_WARN("reg-acc", "%s : request has no 'realm' parameter", m_username);
		return false;
	}

	sip_utils::UnQuote(realm);

	if (!auth->get_auth_param("uri", uri))
	{
		SLOG_WARN("reg-acc", "%s : request has no 'uri' parameter", m_username);
		return false;
	}

	sip_utils::UnQuote(uri);

	if (!auth->get_auth_param("nonce", nonce))
	{
		SLOG_WARN("reg-acc", "%s : request has no 'nonce' parameter", m_username);
		return false;
	}


	if (!auth->get_auth_param("response", response))
	{
		SLOG_WARN("reg-acc", "%s : requerst has no 'response' parameter", m_username);
		return false;
	}

	sip_utils::UnQuote(response);

	// сравним нонс
	
	checked = true;

	sip_utils::UnQuote(nonce);

	rtl::String last_nonce = req_auth.nonce;

	if (last_nonce != nonce)
	{
		SLOG_CALL("reg-acc", "%s : old authorization response -> send 401 Unauthorized", m_username);
		send_401_Unauthorized(request);
		return false;
	}

	// проверим параметры авторизации
	rtl::String userURI;

	if (auth_id.indexOf('@') != BAD_INDEX)
	{
		userURI = auth_id;
	}
	else
	{
		userURI << "sip:" << auth_id << "@" << realm;
	}

	if (::strcmp(auth_id, m_auth_id) != 0)
	{
		SLOG_CALL("reg-acc", "%s : a1 hash error -> send 401 Unauthorized", m_username);
		send_401_Unauthorized(request);
		return false;
	}

	const char* pswd = m_password;

	char ha1[MD5_HASH_HEX_SIZE+1];
	DigestCalcHA1("MD5", auth_id, realm, pswd != nullptr ? pswd : "", ha1);
	ha1[32] = 0;

	char ha2[MD5_HASH_HEX_SIZE+1];
	DigestCalcHA2(method, uri, ha2);
	ha2[32] = 0;

	char resp_hash[MD5_HASH_HEX_SIZE+1];
	DigestCalcResponse(ha1, nonce, ha2, resp_hash);
	
	resp_hash[32] = 0;
	
	rtl::String md5Local =  resp_hash;
	rtl::String md5Remote = response; 

	if (md5Local != md5Remote)
	{
		SLOG_CALL("reg-acc", "%s : authorization token did not match -> send 401 Unauthorized:\n\
\t\t	md5:       MD5\n\
\t\t	user name: %s\n\
\t\t	realm:     %s\n\
\t\t	password:  %s\n\
\t\t	nonce:     %s\n\
\t\t-------------\n\
\t\t	a1 hash:   %s\n\
\t\t    uri:       %s\n\
\t\t-------------\n\
\t\t   local       %s\n\
\t\t   remote      %s", m_username,
			(const char*)auth_id, (const char*)realm,
			m_password != nullptr ? "***" : "(empty)",
			(const char*)nonce, (const char*)ha1, (const char*)uri, (const char*)md5Local, (const char*)md5Remote);

		send_401_Unauthorized(request);
		return false;
	}
	else
	{
		SLOG_CALL("reg-acc", "%s : request authorization OK", m_username);
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool sip_registrar_account_t::check_for_asterisk(const sip_message_t& request)
{
	SLOG_CALL("reg-acc", "%s : check expired request...", m_username);

	if (request.get_Contact()->get_value_count() <= 0)
	{
		SLOG_CALL("reg-acc", "%s : check expires : request has no Contact header", m_username);
		return false;
	}

	const sip_value_contact_t* contact;
	const sip_value_number_t* expires;

	bool asterisk = false;
	int expires_value = -1;
	bool has_live_contact = false;

	if ((contact = request.get_Contact_value()) = nullptr)
	{
		asterisk = contact->get_asterisk_flag();
	}

	if ((expires = request.get_Expires_value()) != nullptr)
	{
		expires_value = expires->get_number();
	}

	has_live_contact = expires_value > 0;

	SLOG_CALL("reg-acc", "%s : check expired request : 1 step asterisk:%s has_live_contact:%s expires_value:%d", m_username,
		STR_BOOL(asterisk), STR_BOOL(has_live_contact), expires_value);

	if (!asterisk && !has_live_contact)
	{
		int contac_list_size = request.get_Contact()->get_value_count();
		for (int i = 0; i < contac_list_size; i++)
		{
			if ((contact = request.get_Contact_value(i)) != nullptr)
			{
				const sip_uri_t& contact_uri = contact->get_uri();
				rtl::String e_value;
				if (contact_uri.get_parameters().get("expires", e_value))
				{
					expires_value = strtoul(e_value, nullptr, 10);

					if (expires_value > 0)
					{
						has_live_contact = true;
					}
				}
			}
		}
	}

	SLOG_CALL("reg-acc", "%s : check expired request : 2 step asterisk:%s has_live_contact:%s expires_value:%d", m_username,
		STR_BOOL(asterisk), STR_BOOL(has_live_contact), expires_value);

	if (asterisk || !has_live_contact)
	{
		SLOG_CALL("reg-acc", "%s : check expires : unregister request", m_username);

		// удалим сессию!
		sip_registrar_session_t* session_by_callid = find_reg_session(request.CallId());
		sip_registrar_session_t* session_by_address = find_reg_session(request.get_remote_address(), request.get_remote_port());

		if (session_by_callid != nullptr && session_by_address != nullptr && session_by_callid == session_by_address)
		{
			SLOG_CALL("reg-acc", "%s : check expires : destroy session %s", m_username, session_by_callid->get_call_id());

			sip_registrar_contact_info_t* contact_info = session_by_callid->get_sip_contact();

			session_by_callid->reset_expires();

			send_reg_200_ok(*session_by_callid, request, 0);

			write_to_regdb(session_by_callid, true);

			remove_reg_session(session_by_callid);

			raise_unregister_event(contact_info, sip_unregister_request_e);
		}
		else
		{
			SLOG_CALL("reg-acc", "%s : check expires : request to deleted or bad session '%s'", m_username, (const char*)request.CallId());
			// в любом случае возвращаем Ok
			sip_message_t response;
			
			request.make_response(response, sip_200_OK);
			
			response.make_Expires_value()->set_number(0);

			send_answer(response);
		}

		return true;
	}

	SLOG_CALL("reg-acc", "%s : check expired request : ok", m_username);

	return false;
}
//--------------------------------------
//
//--------------------------------------
void sip_registrar_account_t::send_reg_200_ok(sip_registrar_session_t& sender, const sip_message_t& request, int expires_value)
{
	sip_message_t response;
	request.make_response(response, sip_200_OK);

	response.make_Expires_value()->set_number(expires_value);

	rtl::PonterArrayT<sip_registrar_contact_info_t> contact_list;
	sender.get_contacts(contact_list);

	for (int i = 0; i < contact_list.getCount(); i++)
	{
		sip_registrar_contact_info_t* info = contact_list[i];

		if (info->get_expires() == 0)
			continue;
		
		sip_uri_t sipuri;
		char str[32];
		sipuri.set_user(m_username);
		sipuri.set_host(inet_ntoa(info->get_contact().address));
		std_snprintf(str, 32, "%u", info->get_contact().port);
		sipuri.set_port(str);
		sipuri.get_parameters().set("transport", TransportType_toString(info->get_contact().type));
		
		response.make_Contact_value()->set_uri(sipuri);
	}

	contact_list.clear();

	m_registrar.send_response(response);
}
//--------------------------------------
//
//--------------------------------------
void sip_registrar_account_t::raise_register_event(const sip_registrar_session_t& session, sip_registrar_contact_info_t* contact_info)
{
	if (m_event_handler != nullptr)
	{
		int reg_count = m_register_list.getCount();
		SLOG_EVENT("reg-acc", "%s : raise REGISTER event for contact %p session count:%d", m_username, contact_info, reg_count);
		m_event_handler->sip_registrar_account_registered(*this, contact_info, reg_count);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_registrar_account_t::raise_unregister_event(sip_registrar_contact_info_t* contact_info, sip_unregister_event_reason_t reason)
{
	if (m_event_handler != nullptr)
	{
		int reg_count = m_register_list.getCount();
		SLOG_EVENT("reg-acc", "%s : raise UNREGISTER event for contact %s session count:%d reason:%s",
			m_username,
			contact_info,
			reg_count,
			get_sip_unregister_event_reason(reason));

		m_event_handler->sip_registrar_account_unregistered(*this, contact_info, reg_count, reason);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_registrar_account_t::raise_subscribe_event(sip_subscribe_session_t& session, const sip_message_t& request, sip_message_t& response)
{
	if (m_event_handler != nullptr)
	{
		SLOG_EVENT("reg-acc", "%s : raise SUBSCRIBE event", m_username);
		m_event_handler->sip_registrar_account_subscribed(*this, request, response);
		session.update_response(response);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_registrar_account_t::write_to_regdb(sip_registrar_session_t* session, bool reset_expires)
{
	//...
}
//--------------------------------------
//
//--------------------------------------
char* sip_registrar_account_t::generate_nonce(const sip_message_t& request)
{
	char nonce[33];

	memset(nonce, 0, sizeof nonce);

	const sip_uri_t& remote_uri = request.From_URI();
	rtl::String call_id = request.CallId();

	md5_context_t md5;

	md5.init();
	md5.update((const char*)call_id, call_id.getLength());
	md5.update(m_username, (int)strlen(m_username));
	md5.final(nonce);

	DigestCalcHA2("REGISTER", remote_uri.get_host(), nonce);
	
	return rtl::strdup(nonce);
}
//--------------------------------------
//
//--------------------------------------
const sip_registrar_auth_t& sip_registrar_account_t::get_auth(const sip_message_t& request)
{
	rtl::MutexLock lock(m_sync);

	rtl::String call_id = request.CallId();

	for (int i = 0; i < m_auth_list.getCount(); i++)
	{
		const sip_registrar_auth_t& auth = m_auth_list[i];
		if (call_id == auth.call_id)
		{
			return auth;
		}
	}

	sip_registrar_auth_t new_auth;

	new_auth.call_id = rtl::strdup(call_id);
	new_auth.nonce = generate_nonce(request);
	new_auth.m_auth_time = time(nullptr);

	m_auth_list.add(new_auth);
	const sip_registrar_auth_t& auth = m_auth_list[m_auth_list.getCount()-1];
	return auth;
}
//--------------------------------------
//
//--------------------------------------
const sip_registrar_auth_t* sip_registrar_account_t::get_auth(const char* call_id)
{
	rtl::MutexLock lock(m_sync);

	for (int i = 0; i < m_auth_list.getCount(); i++)
	{
		const sip_registrar_auth_t& auth = m_auth_list[i];
		if (strcmp(call_id, auth.call_id) == 0)
		{
			return &auth;
		}
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
void sip_registrar_account_t::remove_auth(const char* call_id)
{
	// проверим очередь авторизации
	for (int i = m_auth_list.getCount()-1; i >= 0 ; i--)
	{
		sip_registrar_auth_t& auth = m_auth_list[i];
		if (strcmp(auth.call_id, call_id) == 0)
		{
			if (auth.call_id != nullptr)
				FREE(auth.call_id);
			if (auth.nonce != nullptr)
				FREE(auth.nonce);

			m_auth_list.removeAt(i);
			return;
		}
	}
}
//--------------------------------------
//
//--------------------------------------
sip_registrar_session_t* sip_registrar_account_t::create_reg_session(const sip_message_t& request)
{
	SLOG_CALL("reg-acc", "%s : create session '%s'", m_username, (const char*)request.CallId());

	sip_registrar_session_t* session = NEW sip_registrar_session_t(*this);

	rtl::MutexLock lock(m_sync);
	m_register_list.add(session);

	return session;
}
//--------------------------------------
//
//--------------------------------------
void sip_registrar_account_t::remove_reg_session(sip_registrar_session_t* session)
{
	rtl::MutexLock lock(m_sync);

	for (int i = 0; i < m_register_list.getCount(); i++)
	{
		sip_registrar_session_t* found = m_register_list[i];
		
		if (session == found)
		{
			m_register_list.removeAt(i);
			DELETEO(found);
			break;
		}
	}
}
//--------------------------------------
//
//--------------------------------------
//sip_registrar_session_t* sip_registrar_account_t::add_reg_session(xml_reader_t& reader, uint64_t current_time)
//{
//	sip_registrar_session_t* session = NEW sip_registrar_session_t(*this);
//
//	if (session->read(reader, current_time))
//	{
//		rtl::MutexLock lock(m_sync);
//		m_register_list.add(session);
//		SLOG_CALL("reg-acc", "%s : session read and added", m_username);
//
//
//	}
//	else
//	{
//		DELETEO(session);
//		session = nullptr;
//		SLOG_CALL("reg-acc", "%s : session not read", m_username);
//	}
//
//	return session;
//}
//--------------------------------------
//
//--------------------------------------
sip_subscribe_session_t* sip_registrar_account_t::create_sub_session(const sip_message_t& request)
{
	SLOG_CALL("reg-acc", "%s : create subscribe session '%s'", m_username, (const char*)request.CallId());

	sip_subscribe_session_t* session = NEW sip_subscribe_session_t(*this);

	rtl::MutexLock lock(m_sync);
	m_subscribe_list.add(session);

	return session;
}
//--------------------------------------
//
//--------------------------------------
void sip_registrar_account_t::remove_sub_session(sip_subscribe_session_t* session)
{
	rtl::MutexLock lock(m_sync);

	for (int i = 0; i < m_subscribe_list.getCount(); i++)
	{
		sip_subscribe_session_t* found = m_subscribe_list[i];

		if (session == found)
		{
			m_subscribe_list.removeAt(i);
			DELETEO(found);
			break;
		}
	}
}
//--------------------------------------
