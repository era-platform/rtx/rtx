/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_def.h"
//--------------------------------------
//
//--------------------------------------
static bool is_char_rfc3986(char ch)
{
	/*

	����        "$" | "-" | "_" | "." | "!" | "*" | "'" | "(" | ")" ,
	�� rfc3986  "-" | "_" | "." | "~"

	. - 0x2E
	- - 0x2D
	_ - 0x5F
	~ - 0x7E
	*/

	if (ch >= 'A' && ch <= 'Z')
		return true;
	if (ch >= 'a' && ch <= 'z')
		return true;
	if (ch >= '0' && ch <= '9')
		return true;
	if (ch == '-' || ch == '_' || ch == '.' || ch == '~')
		return true;

	return false;
}
//--------------------------------------
// 4 byte buffer!
//--------------------------------------
static char* escape_char_rfc3986(char ch, char buf[4])
{
	uint8_t t1 = ((uint8_t)ch) >> 4;
	uint8_t t2 = ((uint8_t)ch) & 0x0F;

	buf[0] = '%';
	buf[1] = t1 < 0xA ? t1 + '0' : (t1 - 0x0A) + 'A';
	buf[2] = t2 < 0xA ? t2 + '0' : (t2 - 0x0A) + 'A';
	//	buf[3] = 0;

	return buf;
}

static char tetrad_to_char(int digit, bool high)
{
	char result = 0;

	if (digit >= '0' && digit <= '9')
		result = digit - '0';
	else if (digit >= 'A' && digit <= 'F')
		result = digit - 'A' + 10;
	else if (digit >= 'a' && digit <= 'f')
		result = digit - 'a' + 10;

	if (high)
		result <<= 4;

	return result;
}

/*static char hex_to_char(const char* str, int lenght)
{
	if (lenght < 3)
		return 0;

	return tetrad_to_char(str[2], false) | tetrad_to_char(str[1], true);
}*/
//--------------------------------------
//
//--------------------------------------
rtl::String sip_escape_rfc3986(const char* str)
{
	rtl::String result;
	//const char* xlat = str;
	char buf[4] = { 0 };

	for (const char* it = str; *it != 0; it++)
	{
		if (is_char_rfc3986(*it))
			result << *it;
		else
			result << escape_char_rfc3986(*it, buf);
	}

	return result;
}
//--------------------------------------
//
//--------------------------------------
rtl::String sip_unescape_rfc3986(const char* str)
{
	rtl::String xlat = str;

	int pos;

	pos = (int)-1;

	while ((pos = xlat.indexOf('%', pos + 1)) != STR_BADINDEX)
	{
		int digit1 = xlat[pos + 1];
		int digit2 = xlat[pos + 2];

		if (isxdigit(digit1) && isxdigit(digit2))
		{
			xlat.setAt(pos, (char)(
				(isdigit(digit2) ? (digit2 - '0') : (toupper(digit2) - 'A' + 10)) +
				((isdigit(digit1) ? (digit1 - '0') : (toupper(digit1) - 'A' + 10)) << 4)));
			xlat.remove(pos + 1, 2);
		}
	}

	return xlat;
}
//--------------------------------------
//
//--------------------------------------
//int escape_rfc3986(char* buffer, int size, const char* str, int length)
//{
//	if (length == -1)
//	{
//		length = std_strlen(str);
//	}
//
//	int idx = 0;
//
//	for (int i = 0; i < length; i++)
//	{
//		const char* it = str + i;
//
//		if (is_char_rfc3986(*it))
//		{
//			if (idx <= size)
//			{
//				buffer[idx++] = *it;
//			}
//			else
//			{
//				break;
//			}
//		}
//		else
//		{
//			if (idx < size - 3)
//			{
//				escape_char_rfc3986(*it, buffer + idx);
//				idx += 3;
//			}
//			else
//			{
//				break;
//			}
//		}
//	}
//
//	return idx;
//}
//--------------------------------------
//
//--------------------------------------
char* sip_escape_rfc3986(const char* str, int length)
{
	if (length == -1)
	{
		length = (int)strlen(str);
	}

	char* result = (char*)MALLOC((length * 3) + 1);
	int idx = 0;
	char buf[4] = { 0 };

	for (int i = 0; i < length; i++)
	{
		const char* it = str + i;

		if (is_char_rfc3986(*it))
		{
			*(result + idx) = *it;
			idx++;
		}
		else
		{
			strncpy(result + idx, escape_char_rfc3986(*it, buf), 3);
			idx += 3;
		}
	}

	*(result + idx) = 0;

	return result;
}
//--------------------------------------
//
//--------------------------------------
rtl::String sip_escape_rfc3986(rtl::String str)
{
	rtl::String result;

	char* res = sip_escape_rfc3986(str, str.getLength());

	if (res != nullptr)
	{
		result = res;
		FREE(res);
	}

	return result;
}
//--------------------------------------
//
//--------------------------------------
//char* unescape_rfc3986(char* buffer, int size, const char* str, int length)
//{
//	if (length > size)
//	{
//		buffer[0] = 0;
//		return buffer;
//	}
//
//	if (length == -1)
//	{
//		length = std_strlen(str);
//	}
//
//	int r_idx = 0;
//
//	for (int i = 0; i < length; i++)
//	{
//		if (str[i] == '%')
//		{
//			if (length - i < 3)
//			{
//				buffer[r_idx] = 0;
//				return buffer;
//			}
//
//			buffer[r_idx++] = tetrad_to_char(str[i + 2], false) | tetrad_to_char(str[i + 1], true);//hex_to_char(str + i, length - i);
//			i += 2;
//		}
//		else
//		{
//			buffer[r_idx++] = str[i];
//		}
//	}
//
//	buffer[r_idx] = 0;
//
//	return buffer;
//}
//--------------------------------------
//
//--------------------------------------
char* sip_unescape_rfc3986(const char* str, int length)
{
	if (length == -1)
	{
		length = (int)strlen(str);
	}

	char* result = (char*)MALLOC(length + 1);
	int r_idx = 0;

	for (int i = 0; i < length; i++)
	{
		if (str[i] == '%')
		{
			if (length - i < 3)
			{
				result[r_idx] = 0;
				return result;
			}

			result[r_idx++] = tetrad_to_char(str[i + 2], false) | tetrad_to_char(str[i + 1], true);//hex_to_char(str + i, length - i);
			i += 2;
		}
		else
		{
			result[r_idx++] = str[i];
		}
	}

	result[r_idx] = 0;

	return result;

	//pos = (int)-1;

	//while ((pos = xlat.indexOf('%', pos + 1)) != STR_BADINDEX)
	/*while ((pos = (const char*)memchr(pos, '%', length - (pos-str))) != nullptr)
	{
	int digit1 = xlat[pos + 1];
	int digit2 = xlat[pos + 2];

	if (isxdigit(digit1) && isxdigit(digit2))
	{
	xlat.setAt(pos, (char)(
	(isdigit(digit2) ? (digit2 - '0') : (toupper(digit2) - 'A' + 10)) +
	((isdigit(digit1) ? (digit1 - '0') : (toupper(digit1) - 'A' + 10)) << 4)));
	xlat.remove(pos + 1, 2);
	}
	}

	return xlat;*/
}
rtl::String sip_unescape_rfc3986(rtl::String str)
{
	rtl::String result;

	char* res = sip_unescape_rfc3986(str, str.getLength());
	if (res != nullptr)
	{
		result = res;
		FREE(res);
	}

	return result;
}
//--------------------------------------
//
//--------------------------------------
static rtl::String g_sip_method_names[] =
{
	"INVITE", "ACK", "OPTIONS", "BYE", "CANCEL", "REGISTER",
	"INFO", "UPDATE", "SUBSCRIBE", "NOTIFY", "REFER", "PRACK", "MESSAGE", "PUBLISH",
};
//--------------------------------------
//
//--------------------------------------
const rtl::String& sip_get_method_name(sip_method_type_t type)
{
	return type >= sip_INVITE_e && type < sip_custom_method_e ? g_sip_method_names[type] : rtl::String::empty;
}
//--------------------------------------
//
//--------------------------------------
sip_method_type_t sip_parse_method_type(const char* type_name)
{
	for (size_t i = 0; i < sizeof(g_sip_method_names) / sizeof(const char*); i++)
	{
		if (std_stricmp(type_name, g_sip_method_names[i]) == 0)
			return (sip_method_type_t)i;
	}

	return sip_custom_method_e;
}
//--------------------------------------
//
//--------------------------------------
struct sip_response_code_pair
{
	sip_status_t code;
	rtl::String reason;
};
//--------------------------------------
//
//--------------------------------------
int compare_pairs(const void* left_ptr, const void* right_ptr)
{
	const sip_status_t* left = (const sip_status_t*)left_ptr;
	const sip_response_code_pair* right = (const sip_response_code_pair*)right_ptr;

	return *left - right->code;
}
//--------------------------------------
//
//--------------------------------------
const rtl::String& sip_get_reason_text(sip_status_t code)
{
	static const rtl::String unknown_code("unknown code");
	static const sip_response_code_pair pairs[] =
	{
		{ sip_100_Trying, "Trying" },
		{ sip_180_Ringing, "Ringing" },
		{ sip_181_BeingForwarded, "Call Is Being Forwarded" },
		{ sip_182_Queued, "Queued" },
		{ sip_183_SessionProgress, "Session Progress" },

		{ sip_200_OK, "OK" },
		{ sip_202_Accepted, "Accepted"},

		{ sip_300_MultipleChoices, "Multiple Choices" },
		{ sip_301_MovedPermanently, "Moved Permanently" },
		{ sip_302_MovedTemporarily, "Moved Temporarily" },
		{ sip_305_UseProxy, "Use Proxy" },
		{ sip_380_AlternativeService, "Alternative Service" },

		{ sip_400_BadRequest, "Bad Request" },
		{ sip_401_Unauthorized, "Unauthorized" },
		{ sip_402_PaymentRequired, "Payment Required" },
		{ sip_403_Forbidden, "Forbidden" },
		{ sip_404_NotFound, "Not Found" },
		{ sip_405_MethodNotAllowed, "Method Not Allowed" },
		{ sip_406_NotAcceptable, "Not Acceptable" },
		{ sip_407_ProxyAuthenticationRequired, "Proxy Authentication Required" },
		{ sip_408_RequestTimeout, "Request Timeout" },
		{ sip_409_Conflict, "Conflict" },

		{ sip_410_Gone, "Gone" },
		{ sip_411_LengthRequired, "Length Required" },
		{ sip_412_ConditionalRequestFailed, "Conditional Request Failed" },
		{ sip_413_RequestEntityTooLarge, "Request Entity Too Large" },
		{ sip_414_RequestURITooLong, "Request-URI Too Long" },
		{ sip_415_UnsupportedMediaType, "Unsupported Media Type" },
		{ sip_416_UnsupportedURIScheme, "Unsupported URI Scheme" },

		{ sip_420_BadExtension, "Bad Extension" },
		{ sip_421_ExtensionRequired, "Extension Required" },
		{ sip_422_SessionIntervalTooSmall, "Session Interval Too Small" },
		{ sip_423_IntervalTooBrief, "Interval Too Brief" },

		{ sip_480_TemporarilyUnavailable, "Temporarily Unavailable" },
		{ sip_481_TransactionDoesNotExist, "Call/Transaction Does Not Exist" },
		{ sip_482_LoopDetected, "Loop Detected" },
		{ sip_483_TooManyHops, "Too Many Hops" },
		{ sip_484_AddressIncomplete, "Address Incomplete" },
		{ sip_485_Ambiguous, "Ambiguous" },
		{ sip_486_BusyHere, "Busy Here" },
		{ sip_487_RequestTerminated, "Request Terminated" },
		{ sip_488_NotAcceptableHere, "Not Acceptable Here" },
		{ sip_489_BadEvent, "Bad Event" },
		{ sip_491_RequestPending, "Request Pending" },
		{ sip_493_Undecipherable, "Undecipherable" },

		{ sip_500_ServerInternalError, "Server Internal Error" },
		{ sip_501_NotImplemented, "Not Implemented" },
		{ sip_502_BadGateway, "Bad Gateway" },
		{ sip_503_ServiceUnavailable, "Service Unavailable" },
		{ sip_504_ServerTimeout, "Server Time-out" },
		{ sip_505_VersionNotSupported, "Version Not Supported" },

		{ sip_513_MessageTooLarge, "Message Too Large" },

		{ sip_600_BusyEverywhere, "Busy Everywhere" },
		{ sip_603_Decline, "Decline" },
		{ sip_604_DoesNotExistAnywhere, "Does Not Exist Anywhere" },
		{ sip_606_NotAcceptable, "Not Acceptable" },
	};

	const sip_response_code_pair* found = (const sip_response_code_pair*)bsearch(&code, pairs, sizeof(pairs) / sizeof(sip_response_code_pair), sizeof(sip_response_code_pair), compare_pairs);

	return found != nullptr ? found->reason : unknown_code;
}
static const char* sip_header_list[] =
{
	"Accept",				"Accept-Contact",		"Accept-Encoding",		"Accept-Language",		"Alert-Info",
	"Allow",				"Allow-Events",			"Authentication-Info",	"Authorization",		"Call-ID",
	"Call-Info",			"Contact",				"Content-Disposition",	"Content-Encoding",		"Content-Language",
	"Content-Length",		"Content-Type",			"CSeq",					"Date",					"Diversion",
	"Error-Info",			"Event",				"Expires",				"From",					"History-Info",			
	"Identity",				"Identity-Info",		"In-Reply-To",			"Info-Package",			"Join",
	"Max-Forwards",			"MIME-Version",			"Min-Expires",			"Min-SE",				"Organization",
	"P-Access-Network-Info", "P-Asserted-Identity", "P-Asserted-Service",	"P-Associated-URI",		"P-Called-Party-ID",
	"P-Charging-Function-Addresses", "P-Charging-Vector", "P-DCS-BILLING-INFO", "P-DCS-LAES",		"P-DCS-OSPS",
	"P-DCS-REDIRECT",		"P-DCS-Trace-Party-ID",	"P-Early-Media",		"P-Preferred-Service",	"P-Profile-Key",					
	"P-Refused-URI-List",	"P-Served-User",		"P-Visited-Network-ID", "Path",					"Priority",
	"Proxy-Authenticate",	"Proxy-Authorization",	"Proxy-Require",		"RAck",					"Reason",
	"Record-Route",			"Recv-Info",			"Refer-To",				"Referred-By",			"Reject-Contact",
	"Replaces",				"Reply-To",				"Request-Disposition",	"Require",				"Retry-After",
	"Route",				"RSeq",					"Security-Client",		"Security-Server",		"Security-Verify",
	"Server",				"Service-Route",		"Session-Expires",		"SIP-ETag",				"SIP-If-Match",
	"Subject",				"Subscription-State",	"Supported",			"Suppress-If-Match",	"Target-Dialog",
	"Timestamp",			"To",					"Unsupported",			"User-Agent",			"Via",
	"Warning",				"WWW-Authenticate",
};
//--------------------------------------
//
//--------------------------------------
const char* sip_get_header_name(sip_header_type_t type)
{
	return type >= sip_Accept_e && type < sip_custom_header_e ? sip_header_list[type] : nullptr;
}
//--------------------------------------
//
//--------------------------------------
static int sip_header_info_compare(const void* left, const void* right)
{
	const char* l_ptr = (const char*)left;
	const char** r_ptr = (const char**)right;

	return std_stricmp(l_ptr, *r_ptr);
}
//--------------------------------------
//
//--------------------------------------
sip_header_type_t sip_parse_header_type(const char* type_name)
{
	const char** result = (const char**)bsearch(type_name, sip_header_list, sizeof(sip_header_list) / sizeof(const char*), sizeof(const char*), sip_header_info_compare);

	return (result != nullptr) ? (sip_header_type_t)(result - sip_header_list) : sip_custom_header_e;
}
//--------------------------------------
//
//--------------------------------------
bool selftest_sipdef()
{
	size_t size = sizeof(sip_header_list) / sizeof(const char*);
	bool result = true;
	for (size_t i = 0; i < size; i++)
	{
		const char* name = sip_header_list[i];
		sip_header_type_t found = sip_parse_header_type(name);
		const char* name2 = sip_get_header_name(found);
		if (found != i || strcmp(name, name2) != 0)
		{
			LOG_CALL("sipdef", "SIP headers selftest : search for '%s' result found is '%s' and wanted '%s'\n", name, sip_get_header_name(found), sip_get_header_name((sip_header_type_t)i));
			result = false;
		}
	}

	return result;
}
//--------------------------------------
