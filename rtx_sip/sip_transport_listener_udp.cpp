/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_transport_listener_udp.h"
#include "sip_transport_udp.h"
#include "sip_transport_pool.h"
//--------------------------------------
// ����� �� ���������� ������ 64*T1 = 32 �������, �� �������� �� ������
//--------------------------------------
#define SIP_TRANSPORT_LIFESPAN (1000 * 60)
//--------------------------------------
//
//--------------------------------------
sip_transport_listener_udp_t::sip_transport_listener_udp_t() :
	m_udp_socket(), sip_transport_listener_t(m_udp_socket)
{
	update_name("UDP-L");
}
//--------------------------------------
//
//--------------------------------------
sip_transport_listener_udp_t::~sip_transport_listener_udp_t()
{
	destroy();
}
//--------------------------------------
//
//--------------------------------------
bool sip_transport_listener_udp_t::create(const sip_transport_point_t& iface)
{
	SLOG_NET("net-udp", "%s(%d) : create on %s:%u...", m_name, get_ref_count(), inet_ntoa(iface.address), iface.port);

	if (!m_socket.open(AF_INET, SOCK_DGRAM, IPPROTO_UDP))
	{
		int last_error = std_get_error;
		SLOG_ERR_MSG("net-udp", last_error, "%s(%d) : socket() function failed", m_name, get_ref_count());
		return false;
	}

	m_socket.set_profile("SIP");

	m_if = iface;

	sockaddr_in addr;
	memset(&addr, 0, sizeof addr);
	addr.sin_family = AF_INET;
	addr.sin_addr = m_if.address;
	addr.sin_port = htons(m_if.port);

	bool result = m_socket.bind((sockaddr*)&addr, sizeof addr);

	if (!result)
	{
		int last_error = std_get_error;
		SLOG_ERR_MSG("net-udp", last_error, "%s(%d) : bind() function failed -- if:%s:%u", m_name, get_ref_count(), inet_ntoa(m_if.address), m_if.port);
		return false;
	}

	int bsize = 64 * 1024;
	m_socket.set_option(SOL_SOCKET, SO_RCVBUF, &bsize, sizeof(int));
	m_socket.set_option(SOL_SOCKET, SO_SNDBUF, &bsize, sizeof(int));

	m_socket.set_tag(this);

	SLOG_NET("net-udp", "%s(%d) : created", m_name, get_ref_count());
	
	return true;
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_listener_udp_t::destroy()
{
	sip_transport_listener_t::destroy();

	if (m_socket.get_handle() != INVALID_SOCKET)
	{
		SLOG_NET("net-udp", "%s(%d) : closing socket...", m_name, get_ref_count());
		m_socket.close();
		SLOG_NET("net-udp", "%s(%d) : socket closed", m_name, get_ref_count());
	}
}
//--------------------------------------
//
//--------------------------------------
bool sip_transport_listener_udp_t::start_listen()
{
	return m_socket.get_handle() != INVALID_SOCKET;
}
//--------------------------------------
//
//--------------------------------------
sip_transport_t* sip_transport_listener_udp_t::create_transport(const sip_transport_point_t& endpoint)
{
	sip_transport_t* transport = NEW sip_transport_udp_t(m_if, endpoint, m_socket);

	return transport;
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_listener_udp_t::data_ready()
{
	sockaddr_in addr;
	sockaddrlen_t addr_len = sizeof addr;
	memset(&addr, 0, sizeof addr);
	uint8_t buffer[4096];
	
	SLOG_NET("net-udp", "%s(%d) : READING PACKET...", m_name, get_ref_count());

	int res = m_socket.receive_from(buffer, 4096, 0, (sockaddr*)&addr, &addr_len);

	if (res < 0)
	{
		push_error_to_transport(std_get_error, addr.sin_addr, ntohs(addr.sin_port));
		return;
	}

	if (res == 0 || addr.sin_addr.s_addr == INADDR_ANY)
	{
		// ��������!
		return;
	}

	if (res == 2 && *(uint16_t*)buffer == 0x0A0D) // \r\n
	{
		SLOG_NET("net-udp", "%s(%d) : PONG received from %s:%u", m_name, get_ref_count(), inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));
		return;
	}
	
	if (res < 4)
	{
		SLOG_NET_WARN("net-udp", "%s(%d) : Packet(len:%d) less than 4 bytes! received from %s:%u", m_name, get_ref_count(), res, inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));
		return;
	}

	if (res == 4 && *(uint32_t*)buffer == 0x0A0D0A0D) // \r\n\r\n
	{
		// ��� ���� ���������
		SLOG_NET("net-udp", "%s(%d) : PING received from %s:%u -> send PONG", m_name, get_ref_count(), inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));
		buffer[4] = 0;
		m_socket.send_to(buffer, 2, 0, (sockaddr*)&addr, addr_len);
		return;
	}

	if (m_if.address.s_addr == addr.sin_addr.s_addr && m_if.port == ntohs(addr.sin_port))
	{
		SLOG_NET_WARN("net-udp", "%s(%d) : Send loop detected: packet has same send and receive addresses %s:%u", m_name, get_ref_count(), inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));
		return;
	}

	if (!sip_transport_manager_t::sip_listener_is_address_banned(this, addr.sin_addr))
	{
		if (addr.sin_addr.s_addr != INADDR_ANY && addr.sin_port != 0)
		{
			push_data_to_transport(buffer, res, addr.sin_addr, ntohs(addr.sin_port));
		}
		else
		{
			SLOG_NET_WARN("net-udp", "%s(%d) : readed packet (%d) with empty back address!", m_name, get_ref_count(), res);
		}
	}
	else // BANNED ADDRESS
	{
		if (rtl::Logger::check_trace(TRF_PROTO | TRF_BANNED))
		{
			in_addr if_addr = m_if.address;
			uint16_t if_port = m_if.port;
		
			uint8_t* a = (uint8_t*)&addr.sin_addr.s_addr;
			uint8_t* b = (uint8_t*)&if_addr.s_addr;
		
			TransLog.log("RECV", "%s %d Bytes FROM %d.%d.%d.%d:%u IFACE %d.%d.%d.%d:%u ---- BANNED BY IP!",
				TransportType_toString(m_if.type), res, a[0], a[1], a[2], a[3], ntohs(addr.sin_port), b[0], b[1], b[2], b[3], if_port);
		}
		
		SLOG_NET_WARN("net-udp", "%s(%d) : PACKET NOT PROCESSED -- ADDRESS %s BANNED!", m_name, get_ref_count(), inet_ntoa(addr.sin_addr));
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_listener_udp_t::push_data_to_transport(const uint8_t* packet, int packet_length, in_addr remoteAddress, uint16_t remotePort)
{
	// search transport;
	sip_transport_point_t endpoint = { m_if.type, remoteAddress, remotePort };
	sip_transport_udp_t* transport = (sip_transport_udp_t*)sip_transport_manager_t::get_pool().find_transport(endpoint);

	if (transport == nullptr)
	{
		transport = (sip_transport_udp_t*)create_transport(endpoint);

		transport->capture_("sip_transport_listener_udp_t::create_transport");

		sip_transport_manager_t::get_pool().add_transport(transport);
	}
	
	transport->rx_packet(packet, packet_length, remoteAddress, remotePort);
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_listener_udp_t::push_error_to_transport(int net_error, in_addr remoteAddress, uint16_t remotePort)
{
	// search transport;
	sip_transport_point_t endpoint = { m_if.type, remoteAddress, remotePort };
	sip_transport_udp_t* transport = (sip_transport_udp_t*)sip_transport_manager_t::get_pool().find_transport(endpoint);

	if (transport != nullptr)
	{
		transport->rx_error(net_error, remoteAddress, remotePort);
	}
	else
	{
		// @todo ����� ��������
		SLOG_ERROR("net-udp", "%s(%d) : cant find udp transport for net error(%d) hanling %s:%u", m_name, get_ref_count(), net_error, inet_ntoa(remoteAddress), remotePort);
		sip_transport_route_t route = { m_if.type, m_if.address, m_if.port, remoteAddress, remotePort };
		sip_transport_manager_t::sip_listener_transport_failure(this, net_error, route);
	}
}
//--------------------------------------
