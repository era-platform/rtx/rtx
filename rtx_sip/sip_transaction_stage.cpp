/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "sip_transaction_stage.h"
//--------------------------------------
//
//--------------------------------------
void SIPTransactionWorker::process_transaction()
{
	SLOG_TRANS("TRANSM", "Stage : Worker Thread Started");

	sip_transaction_t* transaction;

	try
	{
		while ((transaction = m_owner.get_transaction()) != nullptr)
		{
			transaction->process_events();
			transaction->release_ref();
		}
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR("TRANSM", "Stage : Transaction worker failed with exception : %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	SLOG_TRANS("TRANSM", "Stage : Worker Thread Stopped");
}
//--------------------------------------
//
//--------------------------------------
void SIPTransactionWorker::thread_run(rtl::Thread* param)
{
	process_transaction();
}
//--------------------------------------
//
//--------------------------------------
SIPTransactionWorker::SIPTransactionWorker(sip_transaction_stage_t& owner) : m_owner(owner)
{
}
//--------------------------------------
//
//--------------------------------------
SIPTransactionWorker::~SIPTransactionWorker()
{
	stop();
}
//--------------------------------------
//
//--------------------------------------
bool SIPTransactionWorker::start()
{
	// �������� ������
	return m_thread.start(this);
}
//--------------------------------------
//
//--------------------------------------
void SIPTransactionWorker::stop()
{
	m_thread.stop();
}
//--------------------------------------
//
//--------------------------------------
sip_transaction_stage_t::sip_transaction_stage_t(int threadCount) :
	m_started(FALSE), m_queue_lock("stage-que-lock"), m_cleaner_lock("stage-clean-lock")
{
	m_queue_sem.create(0, INT_MAX);

	if (threadCount < 2)
		threadCount = 2;

	if (threadCount > 16)
		threadCount = 16;

	SIPTransactionWorker* worker;

	// ��������� ��������!
	for (int i = 0; i < threadCount; i++)
	{
		worker = NEW SIPTransactionWorker(*this);
		m_queue_thread_pool.add(worker);
	}
}
//--------------------------------------
//
//--------------------------------------
sip_transaction_stage_t::~sip_transaction_stage_t()
{
	stop();

	rtl::Timer::stopTimer(this, 0);
}
//--------------------------------------
//
//--------------------------------------
bool sip_transaction_stage_t::start()
{
	if (m_started)
		return true;

	SLOG_TRANS("TRANSM", "Stage : start...");

	{
		rtl::MutexWatchLock lock(m_queue_lock, __FUNCTION__);

		int threadStarted = 0;

		// ������ �������-������������ ����������
		for (int i = 0; i < m_queue_thread_pool.getCount(); i++)
		{
			if (m_queue_thread_pool[i]->start())
				threadStarted++;
		}

		if (threadStarted > 0)
		{
			//m_cleaner_timer->Start(500);
			rtl::Timer::setTimer(this, 0, 500, true);
		}

		m_started = true;
	}

	SLOG_TRANS("TRANSM", "Stage : started");

	return m_started;
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_stage_t::stop()
{
	if (m_started)
	{
		m_started = false;

		SLOG_TRANS("TRANSM", "Stage : stopping...");

		for (int i = 0; i < m_queue_thread_pool.getCount(); i++)
		{
			m_queue_sem.signal();
		}

		for (int i = 0; i < m_queue_thread_pool.getCount(); i++)
		{
			m_queue_thread_pool[i]->stop();
		}

		{
			rtl::MutexWatchLock lock(m_queue_lock, __FUNCTION__);

			for (int i = 0; i < m_queue_thread_pool.getCount(); i++)
			{
				DELETEO(m_queue_thread_pool[i]);
			}
			m_queue_thread_pool.clear();
		}

		{
			rtl::MutexWatchLock lock(m_cleaner_lock, __FUNCTION__);

			for (int i = 0; i < m_cleaner_list.getCount(); i++)
			{
				sip_transaction_t* trans = m_cleaner_list[i];
				DELETEO(trans);
			}
			m_cleaner_list.clear();
		}

		SLOG_TRANS("TRANSM", "Stage : stopped tpool:%d cleaner:%d", m_queue_thread_pool.getCount(), m_cleaner_list.getCount());
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_stage_t::push_transaction(sip_transaction_t* transaction)
{
	SLOG_TRANS("TRANSM", "Stage : enqueue transaction %s to worker!", transaction->get_iid());

	rtl::MutexWatchLock lock(m_queue_lock, __FUNCTION__);

	if (m_started)
	{
		transaction->add_ref();
		m_queue.push(transaction);
		m_queue_sem.signal();
	}
	else
	{
		SLOG_WARN("TRANSM", "Stage : enqueue transaction failed : Transaction stage not started!");
	}
}
//--------------------------------------
//
//--------------------------------------
sip_transaction_t* sip_transaction_stage_t::get_transaction()
{
	sip_transaction_t* transaction = nullptr;
	
	m_queue_sem.wait(INFINITE);

	rtl::MutexWatchLock lock(m_queue_lock, __FUNCTION__);

	if (m_started)
	{
		transaction = m_queue.pop();

		SLOG_TRANS("TRANSM", "Stage : dequeue transaction %s", transaction ? transaction->get_iid() : "(null)");
	}

	return transaction;
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_stage_t::terminate_transaction(sip_transaction_t* transaction)
{
	SLOG_TRANS("TRANSM", "Stage : enqueue terminating transaction %s to cleaner!", transaction->get_iid());
	
	rtl::MutexWatchLock lock(m_cleaner_lock, __FUNCTION__);
	if (m_started)
	{
		// �������� �� ��������� ����������!
		m_cleaner_list.add(transaction);
	}
	else
		SLOG_WARN("TRANSM", "Stage : enqueue terminating transaction failed : Transaction stage not started!");
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_stage_t::timer_elapsed_event(rtl::Timer* sender, int tid)
{
	rtl::async_call_manager_t::callAsync(this, 0, 0, nullptr);
}
//--------------------------------------
//
//--------------------------------------
const char* sip_transaction_stage_t::async_get_name()
{
	return "trans-stage";
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_stage_t::async_handle_call(uint16_t call_id, uintptr_t int_param, void* ptr_param)
{
	//int failCounter = 0;
	sip_transaction_t* trans;

	try
	{
		rtl::MutexWatchLock lock(m_cleaner_lock, __FUNCTION__);

		for (int i = m_cleaner_list.getCount() - 1; i >= 0; i--)
		{
			trans = m_cleaner_list[i];
			// ������� ������ ����� ������ ��� ������ �� ����������!
			if (trans->get_ref_count() == 0)
			{
				m_cleaner_list.removeAt(i);
				DELETEO(trans);
			}
		}
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR("TRANSM", "Stage : cleaner timer catches exception : %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}
}
//--------------------------------------
