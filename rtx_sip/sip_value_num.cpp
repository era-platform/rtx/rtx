/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_value_num.h"
//--------------------------------------
//
//--------------------------------------
rtl::String& sip_value_number_t::prepare(rtl::String& stream) const
{
	return stream << m_number;
}
//--------------------------------------
//
//--------------------------------------
bool sip_value_number_t::parse()
{
	m_number = !m_value.isEmpty() ? strtoul(m_value.trim(), nullptr, 10) : 0;

	return true;
}
//--------------------------------------
//
//--------------------------------------
sip_value_number_t::~sip_value_number_t()
{
	cleanup();
}
//--------------------------------------
//
//--------------------------------------
void sip_value_number_t::cleanup()
{
	m_number = 0;

	sip_value_t::cleanup();
}
//--------------------------------------
//
//--------------------------------------
sip_value_number_t& sip_value_number_t::assign(const sip_value_number_t& v)
{
	sip_value_t::assign(v);
	m_number = v.m_number;

	return *this;
}
//--------------------------------------
//
//--------------------------------------
sip_value_t* sip_value_number_t::clone() const
{
	return NEW sip_value_number_t(*this);
}
//--------------------------------------
