/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_profile.h"
//-----------------------------------------------
//
//-----------------------------------------------
sip_profile_t::sip_profile_t() :
	m_auth_type(auth_digest), m_expires(3600), m_bye_auth(true),
	m_nat_keep_alive_interval(0)
{
	m_proxy_port = 0;
	m_proxy_address.s_addr = INADDR_ANY;
	m_interface_port = 0;
	m_interface.s_addr = INADDR_ANY;
	m_listener_port = 0;
	m_listener.s_addr = INADDR_ANY;
	m_transport = sip_transport_udp_e;
}
//-----------------------------------------------
//
//-----------------------------------------------
sip_profile_t& sip_profile_t::operator = (const sip_profile_t& profile)
{
	m_domain = profile.m_domain;
	m_proxy = profile.m_proxy;

	m_proxy_address = profile.m_proxy_address;
	m_proxy_port = profile.m_proxy_port;
	m_interface = profile.m_interface;
	m_interface_port = profile.m_interface_port;
	m_listener = profile.m_listener;
	m_listener_port = profile.m_listener_port;
	m_transport = profile.m_transport;

	m_request_user = profile.m_request_user;
	m_to_user = profile.m_to_user;
	m_username = profile.m_username;
	m_display_name = profile.m_display_name;
	m_auth_type = profile.m_auth_type;
	m_auth_id = profile.m_auth_id;
	m_auth_pwd = profile.m_auth_pwd;
	m_expires = profile.m_expires;
	m_bye_auth = profile.m_bye_auth;
	m_nat_keep_alive_interval = profile.m_nat_keep_alive_interval;

	return *this;
}
//-----------------------------------------------
