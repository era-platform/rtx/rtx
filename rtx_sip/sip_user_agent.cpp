/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_user_agent.h"
#include "sip_session.h"
//--------------------------------------
//
//--------------------------------------
rtl::Logger SLog;
rtl::Logger TransLog;
//--------------------------------------
//
//--------------------------------------
const char* sip_user_agent_t::async_get_name()
{
	return "User-Agent";
}
//--------------------------------------
//
//--------------------------------------
void sip_user_agent_t::async_handle_call(uint16_t callid, uintptr_t intparam, void* event_)
{
	sip_stack_event_t* sip_event = (sip_stack_event_t*)event_;

	if (sip_event != NULL)
	{
		process_sip_stack_event(sip_event); // m_owner.
		DELETEO(sip_event);
	}
	else
	{
		SLOG_WARN("UA", "=====> Dequeue NULL event...");
	}
}
//--------------------------------------
//
//--------------------------------------
sip_user_agent_t::sip_user_agent_t() : m_sip_stack(*this), m_registrar(*this)
{
	m_initialized = false;

	set_UA_name("RTX test phone");
}
//--------------------------------------
//
//--------------------------------------
sip_user_agent_t::~sip_user_agent_t()
{
	destroy();
}
//--------------------------------------
//
//--------------------------------------
bool sip_user_agent_t::initialize(int work_threads, int trn_threads)
{
	if (m_initialized)
		return true;

	SLOG_TRANS("UA", "initializing SIP User Agent %d threads...", work_threads);

	m_initialized = true;

	m_sip_stack.initialize(trn_threads);

	return true;
}
//--------------------------------------
//
//--------------------------------------
void sip_user_agent_t::process_sip_stack_event(sip_stack_event_t* event)
{
	SLOG_ERROR("UA", "Called abstract sip_user_agent_t::process_sip_stack_event()!");
}
//--------------------------------------
//
//--------------------------------------
void sip_user_agent_t::destroy()
{
	SLOG_CALL("UA", "terminating SIP User Agent...");

	m_sip_stack.destroy_stack();

	SLOG_CALL("UA", "SIP User Agent terminated.");
}
//--------------------------------------
//
//--------------------------------------
bool sip_user_agent_t::push_message_to_transaction(const sip_message_t& request, trans_id& transactionId)
{
	sip_message_t msg = request;
	return m_sip_stack.find_transaction_and_add_event(msg, transactionId, false);
}
//--------------------------------------
//
//--------------------------------------
void sip_user_agent_t::push_message_to_transport(const sip_message_t& request)
{
	m_sip_stack.send_message_to_transport(request);
}
//--------------------------------------
//
//--------------------------------------
void sip_user_agent_t::setup_registrar(sip_registrar_serializer_t* serializer)
{
	m_registrar.setup(serializer);
	m_registrar.start();
}
//--------------------------------------
//
//--------------------------------------
void sip_user_agent_t::set_UA_name(const char* name)
{
	STR_COPY(m_ua_name, name);
}
//--------------------------------------
//
//--------------------------------------
bool sip_user_agent_t::add_transport(sip_transport_type_t type, in_addr if_address, uint16_t if_port)
{
	sip_transport_point_t iface = { type, if_address, if_port };
	return m_sip_stack.add_transport(iface);
}
//--------------------------------------
