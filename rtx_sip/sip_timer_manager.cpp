/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_timer_manager.h"
//--------------------------------------
//
//--------------------------------------
const char* sip_timer_manager_t::GetTimerName(sip_transaction_timer_t type)
{
	static const char* s_TypeNames[] =
	{
		"TIMER_A", "TIMER_B", "TIMER_C", "TIMER_D",
		"TIMER_E", "TIMER_F", "TIMER_G", "TIMER_H",
		"TIMER_I", "TIMER_J", "TIMER_K", "LIFE_SPAN",
		"TIMER_REG", "TIMER_PING"
	};

	return (type >= 0 && type < sizeof(s_TypeNames)/sizeof(const char*)) ? s_TypeNames[type] : "TIMER_UNKNOWN";
}
//--------------------------------------
//
//--------------------------------------
static int compare_timer(const sip_timer_info_t& left, const sip_timer_info_t& right)
{
	int r1 = (int)((uintptr_t)left.handler - (uintptr_t)right.handler);
	return r1 != 0 ? r1 : (int)(left.type - right.type);
}
//--------------------------------------
//
//--------------------------------------
static int compare_timer_key(const sip_timer_key_t& left, const sip_timer_info_t& right)
{
	int r1 = (int)((uintptr_t)left.handler - (uintptr_t)right.handler);
	return r1 != 0 ? r1 : (int)(left.type - right.type);
}
//--------------------------------------
//
//--------------------------------------
sip_timer_manager_t::sip_timer_manager_t() : m_sip_timer_sync("sip-timer-lock"), m_sip_timer_list(compare_timer, compare_timer_key),
	m_sip_timer(NULL), m_sip_timer_last_tick(0),
	 m_sip_timer_resolution(SIP_TIMER_RESOLUTION)
{
	m_sip_timer = new rtl::Timer(false, this, Log, "sip-timer");
}
//--------------------------------------
//
//--------------------------------------
sip_timer_manager_t::~sip_timer_manager_t()
{
	stop_timer_manager();

	if (m_sip_timer != NULL)
		DELETEO(m_sip_timer);
}
//--------------------------------------
//
//--------------------------------------
bool sip_timer_manager_t::start_timer_manager(uint32_t resolution)
{
	SLOG_TRANS("SIPTIMER", "Starting SIP Timer Manager resolution %u...", resolution);

	if (m_sip_timer->isStarted())
		stop_timer_manager();

	if (resolution == 0)
		return false;

	m_sip_timer_resolution = resolution;
	m_sip_timer->start(m_sip_timer_resolution);

	SLOG_TRANS("SIPTIMER", "SIP Timer Manager started");

	return true;
}
//--------------------------------------
//
//--------------------------------------
void sip_timer_manager_t::stop_timer_manager()
{
	if (!m_sip_timer->isStarted())
		return;

	uint32_t currentTime = rtl::DateTime::getTicks();

	SLOG_TRANS("SIPTIMER", "Stop : Stopping SIP Timer manager time %u...", currentTime);

	m_sip_timer->stop();
	m_sip_timer_resolution = 0;

	SLOG_TRANS("SIPTIMER", "Stop : SIP Timer manager stopped");
}
//--------------------------------------
//
//--------------------------------------
sip_timer_info_t* sip_timer_manager_t::find_timer(sip_timer_user_t* handler, sip_transaction_timer_t type)
{
	sip_timer_key_t key = { handler, type };
	return m_sip_timer_list.find(key);
}
//--------------------------------------
//
//--------------------------------------
void sip_timer_manager_t::start_timer(sip_timer_user_t* handler, sip_transaction_timer_t type, uint32_t interval)
{
	rtl::MutexWatchLock lock(m_sip_timer_sync, __FUNCTION__);

	sip_timer_info_t* timer = find_timer(handler, type);

	if (timer == NULL )
	{
		sip_timer_info_t tm = { handler, type, interval, rtl::DateTime::getTicks(), true };
		STR_COPY(tm.name, handler->get_timer_id());
		//int idx = m_sip_timer_list.add(tm);
		SLOG_TRANS("SIPTIMER", "Start timer : Adding NEW %s, start %u interval %u ms for %s",
			GetTimerName(type), tm.start_time, interval, handler->get_timer_id());
	}
	else
	{
		timer->start_time = rtl::DateTime::getTicks();
		timer->interval = interval;
		timer->enabled = true;

		SLOG_TRANS("SIPTIMER", "Start timer : Enabling %s interval %u ms for %s", GetTimerName(type), interval, handler->get_timer_id());
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_timer_manager_t::stop_timer(sip_timer_user_t* handler, sip_transaction_timer_t type)
{
	rtl::MutexWatchLock lock(m_sip_timer_sync, __FUNCTION__);

	sip_timer_info_t* timer = find_timer(handler, type);

	if (timer != NULL)
	{
		SLOG_TRANS("SIPTIMER", "Stop timer : Disabling %s for %s", GetTimerName(timer->type), handler->get_timer_id());
		timer->enabled = false;
	}
	else
	{
		SLOG_WARN("SIPTIMER", "Stop timer : %s for %s NOT FOUND!", timer != NULL ? GetTimerName(timer->type) : "NULL", handler->get_timer_id());
	}

}
//--------------------------------------
//
//--------------------------------------
void sip_timer_manager_t::remove_timer(sip_timer_user_t* handler, sip_transaction_timer_t type)
{
	rtl::MutexWatchLock lock(m_sip_timer_sync, __FUNCTION__);

	sip_timer_key_t key = { handler, type };

	int index = m_sip_timer_list.indexOfKey(key);
	if (index != BAD_INDEX)
	{
		sip_timer_info_t& timer = m_sip_timer_list.getAt(index);
		SLOG_TRANS("SIPTIMER", "Remove timer : Removing %s interval %u ms for %s", GetTimerName(timer.type), timer.interval, handler->get_timer_id());
		m_sip_timer_list.removeAt(index);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_timer_manager_t::remove_all_timers(sip_timer_user_t* handler)
{
	rtl::MutexWatchLock lock(m_sip_timer_sync, __FUNCTION__);

	int found = 0;

	for (int i = int(m_sip_timer_list.getCount()-1); i >= 0 ; i--)
	{
		sip_timer_info_t& timer = m_sip_timer_list.getAt(i);
		if (timer.handler == handler)
		{
			m_sip_timer_list.removeAt(i);
			found++;
		}
	}

	SLOG_TRANS("SIPTIMER", "Remove all timers : found %d timers for %s", found, handler->get_timer_id());
}
//--------------------------------------
//
//--------------------------------------
void sip_timer_manager_t::timer_elapsed_event(rtl::Timer* sender, int tid)
{
	char _ex_trans_name_[128] = { '(', 'n', 'u', 'l', 'l', ')', 0 };
	int index = -1, total = 0;
	sip_timer_info_t *eptr = nullptr, *ecptr = &m_sip_timer_list.getAt(-1);

	try
	{
		rtl::MutexWatchLock lock(m_sip_timer_sync, __FUNCTION__);

		uint32_t current = rtl::DateTime::getTicks();
		int enabledTimers = 0;

		total = m_sip_timer_list.getCount();
		for (int i = 0; i < m_sip_timer_list.getCount(); i++)
		{
			index = i;
			sip_timer_info_t& timer = m_sip_timer_list.getAt(i);
			eptr = &timer;

			int l = std_snprintf(_ex_trans_name_, sizeof(_ex_trans_name_)-1, "<%u|%u|%u|%u|%s|%d/%d>", timer.enabled, timer.interval, timer.start_time, timer.type, timer.name, index, total);
			if (l > 0)
				_ex_trans_name_[l] = 0;
			else
				_ex_trans_name_[0] = 0;

			if (timer.enabled)
			{
				if (current - timer.start_time >= timer.interval)
				{
					SLOG_TRANS("SIPTIMER", "Firing EVENT : %s start %u duration %u -> interval %u ms for %s", GetTimerName(timer.type), timer.start_time, current - timer.start_time, timer.interval, timer.name);

					if (timer.handler != nullptr && timer.name[0] != 0)
					{
						timer.enabled = false;
						timer.handler->sip_timer_elapsed(timer.type, timer.interval);
					}
					else
					{
						timer.enabled = false;
						SLOG_WARN("SIPTIMER", "Firing EVENT : GHOST timer record found! %s start %u duration %u -> interval %u ms for %s",
							GetTimerName(timer.type), timer.start_time, current - timer.start_time, timer.interval, timer.name);
					}
				}
				else
				{
					enabledTimers++;
				}
			}

		}

		if (current - m_sip_timer_last_tick > 30000)
		{
			m_sip_timer_last_tick = current;

			for (int i = (int)(m_sip_timer_list.getCount()-1); i >= 0; i--)
			{
				if (!m_sip_timer_list[i].enabled)
					m_sip_timer_list.removeAt(i);
			}
		}
	}
	catch (rtl::Exception& ex)
	{
		const char* ptr = _ex_trans_name_[0] != 0 ? _ex_trans_name_ : "(empty)";
		bool be = eptr == ecptr;
		
		SLOG_ERROR("SIPTIMER", "SIP Timer manager catches exception while checking timer '%s', index %d/%d, empty ptr %s :\n %s",
			ptr, index, total, STR_BOOL(be),
			ex.get_message());

		ex.raise_notify(__FILE__, __FUNCTION__);
	}
}
//--------------------------------------
//
//--------------------------------------
sip_timer_user_t::sip_timer_user_t(sip_timer_manager_t& manager) : m_timer_manager(manager)
{
	memset(m_internal_id, 0, sizeof(m_internal_id));
}
//--------------------------------------
//
//--------------------------------------
sip_timer_user_t::~sip_timer_user_t()
{
	stop_all_timers();
}
//--------------------------------------
