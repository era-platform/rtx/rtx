﻿/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_session_manager.h"
//--------------------------------------
//
//--------------------------------------
volatile int32_t sip_session_t::s_internal_id_counter = 1;
const char* sip_session_t::s_dialog_type_name[3] = { "NID", "UAC", "UAS" };
//--------------------------------------
// конструктор клиентской сессии
//--------------------------------------
sip_session_t::sip_session_t(sip_session_manager_t& sessionManager, const sip_profile_t& profile, const char* callId) :
	m_profile(profile),
	m_manager(sessionManager),
	m_type(sip_session_client_e),
	m_will_process_events(true),

	
	m_dialog_lock("sip-sess-dialog"),
	m_remote_route_set(sip_Route_e),
	m_local_contact_list(sip_Contact_e),
	m_remote_contact_list(sip_Contact_e),
	m_local_via(sip_Via_e),

	m_uac_request_lock("sip-sess-uac-req"),
	m_uac_response_lock("sip-sess-uac-res"),
	m_uas_request_lock("sip-sess-uas-req"),
	m_uas_response_lock("sip-sess-uas-res"),

	m_destroy_lock("sip-sess-destroy")
{
	m_destroy_time = 0;
	m_purgatory_time = 0;
	m_ready_to_destroy = false;
	m_new_session = true;

	/// IMPORTANT NOTE!!!
	/// this is a client session...  construction of the request is left to
	/// the descendant class since we do not know the request to construct at this point.
	/// Take note that the sip_session_t manager will fetch
	/// the sessions callID after construction and therfore it is extremely important
	/// the client request for this session is constructed and the m_call_id value set
	m_dialog_type = sip_dialog_none_e;

	m_call_id = callId;

	int32_t session_id = std_interlocked_inc(&s_internal_id_counter);
	sprintf(m_session_ref, "UAC(%d)", session_id);

	m_local_sequaence = 1;
	m_remote_sequaence = 0;

	m_log_tag = "SESSION";
	m_destroyed = false;
	
	m_route.type = profile.get_transport();
	m_route.if_address = m_profile.get_interface();
	m_route.if_port = m_profile.get_interface_port();
	m_route.remoteAddress = m_profile.get_proxy_address();
	m_route.remotePort = m_profile.get_proxy_port();

	m_manager.get_user_agent().get_sip_stack().open_route(m_route, "session");
}
//--------------------------------------
// конструктор серверной сессии
//--------------------------------------
sip_session_t::sip_session_t(sip_session_manager_t& sessionManager, const sip_message_t& request) :
	m_manager(sessionManager),
	m_type(sip_session_server_e),
	m_will_process_events(true),
	
	m_dialog_lock("sip-sess-dialog"),
	
	m_remote_route_set(sip_Route_e),
	m_local_contact_list(sip_Contact_e),
	m_remote_contact_list(sip_Contact_e),
	m_local_via(sip_Via_e),

	m_uac_request_lock("sip-sess-uac-req"),
	m_uac_response_lock("sip-sess-uac-res"), m_uas_request_lock("sip-sess-uas-req"), m_uas_response_lock("sip-sess-uas-res"),

	m_destroy_lock("sip-sess-destroy")
{
	m_destroy_time = 0;
	m_purgatory_time = 0;
	m_ready_to_destroy = false;

	m_uas_request = request;

	m_log_tag = "SESSION";
	m_dialog_type = sip_dialog_none_e;
	m_new_session = true;

	/// set the callId;
	m_call_id = m_uas_request.CallId();

	int32_t session_id = std_interlocked_inc(&s_internal_id_counter);
	sprintf(m_session_ref, "UAS(%d)", session_id);

	m_local_sequaence = 4011;
	m_remote_sequaence = request.CSeq_Number();

	m_log_tag = "SESSION";

	/// prepare the URIs
	
	m_local_uri = request.To_URI();
	m_remote_uri = request.From_URI();

	m_destroyed = false;

	m_route = request.getRoute();
	// маршрут уже открыт! просто захватим
	//@spec: transport: 1, 6 : захват транспорта сессией
	m_manager.get_user_agent().get_sip_stack().open_route(m_route, "session");

	m_profile.set_transport(m_route.type);
	m_profile.set_interface(m_route.if_address);
	m_profile.set_interface_port(m_route.if_port);
	m_profile.set_proxy_address(m_route.remoteAddress);
	m_profile.set_proxy_port(m_route.remotePort);
}
//--------------------------------------
// деструктор
//--------------------------------------
sip_session_t::~sip_session_t()
{
	destroy_session();

	m_manager.get_user_agent().get_sip_stack().release_route(m_route, "session");

	SLOG_SESS(m_log_tag, "%s : Session destructed!", m_session_ref);
}
//--------------------------------------
// отправка сип сообщения
//--------------------------------------
bool sip_session_t::push_message_to_transaction(const sip_message_t& request)
{
	sip_message_t out = request;

	/// inform the transport that this isnt a retransmission
	SLOG_SESS(m_log_tag, "%s : push message to transaction over %s (%s)", m_session_ref, inet_ntoa(out.get_interface_address()), (const char*)request.GetStartLine());

	out.SetUACoreName(m_manager.get_UA_name());

	/// check if we have a Max-Forwards
	if (out.is_request() && !out.has_Max_Forwards())
		out.make_Max_Forwards_value()->set_number(SIP_DEFAULT_MAX_FORWARDS);

	rtl::String uaName = m_manager.get_user_agent().get_UA_name();

	if (out.is_request() && !out.has_User_Agent())
	{
		out.make_User_Agent_value()->assign(uaName);
	}
	else if (!out.is_request() && !out.has_Server())
	{
		out.make_Server_value()->assign(uaName);
	}

	/*
	12.2.1 UAC Behavior
	12.2.1.1 Generating the Request

	If the route set is not empty, and its first URI does not contain the
	lr parameter, the UAC MUST place the first URI from the route set
	into the Request-URI, stripping any parameters that are not allowed
	in a Request-URI.  The UAC MUST add a Route header field containing
	the remainder of the route set values in order, including all
	parameters.  The UAC MUST then place the remote target URI into the
	Route header field as the last value.

	For example, if the remote target is sip:user@remoteua and the route
	set contains:

	<sip:proxy1>,<sip:proxy2>,<sip:proxy3;lr>,<sip:proxy4>

	The request will be formed with the following Request-URI and Route
	header field:
	*/

	if (out.is_request())
	{
		if (!out.IsAck())
		{
			SetCurrentUACRequest(out);
		}
	}

	return m_manager.send_message_to_transport(out, this) != 0;
}
//--------------------------------------
//
//--------------------------------------
bool sip_session_t::SendAcceptByRejection(const sip_message_t& requestToReject, int statusCode, const rtl::String& reasonPhrase, const rtl::String& warning)
{
	sip_message_t forbidden;
	requestToReject.make_response(forbidden, (uint16_t)statusCode, reasonPhrase);

	if (!warning.isEmpty())
	{
		forbidden.make_Warning_value()->assign(warning);
	}

	SLOG_SESS(m_log_tag, "%s : Rejected Incoming request %s with %s", m_session_ref, (const char*)requestToReject.GetStartLine(),	(const char*)forbidden.GetStartLine());
	
	return push_message_to_transaction(forbidden);
}
//--------------------------------------
//
//--------------------------------------
bool sip_session_t::SendAcceptByRejection(int statusCode, const rtl::String& reasonPhrase, const rtl::String& warning)
{
	rtl::MutexWatchLock lock(m_uas_request_lock, __FUNCTION__);

	return SendAcceptByRejection(m_uas_request, statusCode, reasonPhrase, warning);
}
//--------------------------------------
//
//--------------------------------------
bool sip_session_t::process_incoming_sip_message(sip_event_message_t& messageEvent)
{
	SLOG_SESS(m_log_tag, "%s : Incoming SIP message %s", m_session_ref, (const char*)messageEvent.get_message().GetStartLine());

	const sip_message_t& message = messageEvent.get_message();

	if (message.is_request())
	{
		if (!message.IsAck())
		{
			rtl::MutexWatchLock lock(m_uas_request_lock, __FUNCTION__);
			m_uas_request = message;
		}
	}
	else
	{
		{
			rtl::MutexWatchLock lock(m_uac_response_lock, __FUNCTION__);
			m_uac_response = message;
		}
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
void sip_session_t::process_sip_message_sent_event(sip_event_tx_t& messageEvent)
{
	const sip_message_t& msg = messageEvent.get_message();

	SLOG_SESS(m_log_tag, "%s : SIP message %s sent", m_session_ref, (const char*)msg.GetStartLine());

	if (msg.is_request())
	{
		if (!msg.IsAck())
		{
			SetCurrentUACRequest(msg);
		}
	}
	else
	{
		//SetCurrentUASResponse(msg);

		rtl::MutexWatchLock lock(m_uas_response_lock, __FUNCTION__);
		m_uas_response = msg;
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_session_t::process_timer_expires_event(sip_event_timer_expires_t& timerEvent)
{
	SLOG_SESS(m_log_tag, "%s : Timer expires %s", m_session_ref, (const char*)timerEvent.get_method());
	
	destroy_session();
}
//--------------------------------------
//
//--------------------------------------
void sip_session_t::process_transport_failure_event(int failureType)
{
	SLOG_SESS(m_log_tag, "%s : Transport failure(%d)", m_session_ref, failureType);
	
	destroy_session();
}
//--------------------------------------
//
//--------------------------------------
void sip_session_t::check_transport_route_disconnected(const sip_transport_route_t& route)
{
	// если наш маршрут то нужно завершать работу
	if (memcmp(&m_route, &route, sizeof(m_route)) == 0)
	{
		push_session_event(NEW sip_session_event_t(*this, sip_session_t::TransportFault));
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_session_t::check_transport_route_failure(const sip_transport_route_t& route, int net_error)
{
	if (route.type != m_route.type)
		return;

	if (route.if_address.s_addr == m_route.if_address.s_addr)
	{
		// 0 - свалился интерейс!
		if (route.remoteAddress.s_addr == 0 || route.remoteAddress.s_addr == m_route.remoteAddress.s_addr)
		{
			push_session_event(NEW sip_session_event_t(*this, sip_session_t::TransportFault));
		}
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_session_t::push_session_event(sip_session_event_t * event)
{
	SLOG_SESS(m_log_tag, "%s : push session event %s", m_session_ref, (const char*)event->get_message().GetStartLine());
	
	m_manager.push_session_event(*this, event);
}
//--------------------------------------
//
//--------------------------------------
void sip_session_t::process_session_event(int event, const sip_message_t& eventMsg)
{
	SLOG_WARN(m_log_tag, "%s : default session event handler!", m_session_ref);
}
//--------------------------------------
// дожидаемся ответов и уже после откючаемся или самоликвидируемся после 30 сек
//--------------------------------------
void sip_session_t::destroy_session(bool wait_answer)
{
	SLOG_SESS(m_log_tag, "%s : destroy session : wait_answer=%s ...", m_session_ref, STR_BOOL(wait_answer));

	try
	{
		rtl::MutexWatchLock lock(m_destroy_lock, __FUNCTION__);

		if (wait_answer)
		{
			StartAutoDestructTimer(10);
		}
		else
		{
			m_will_process_events = false;

			if (!m_destroyed)
			{
				process_destroy_event();
				m_destroyed = true;
				m_manager.queue_for_destruction(m_call_id);
			}
		}
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR(m_log_tag, "destroy session : failed with exception %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	SLOG_SESS(m_log_tag, "%s : session destroyed...", m_session_ref);
}
//--------------------------------------
//
//--------------------------------------
void sip_session_t::process_destroy_event()
{
	SLOG_SESS(m_log_tag, "%s : destroy session event", m_session_ref);

	m_manager.stop_session_timer(this);
}
//--------------------------------------
//
//--------------------------------------
bool sip_session_t::InitializeDialogAsUAC(const sip_message_t& request, const sip_message_t& response)
{
	SLOG_SESS(m_log_tag, "%s : initialize dialog as UAC %s", m_session_ref, (const char*)request.GetStartLine());

	bool result = FALSE;
	
	try
	{
		rtl::MutexWatchLock lock(m_dialog_lock, __FUNCTION__);

		if (m_dialog_type != sip_dialog_none_e)
		{
			result = true;
			goto the_end;
		}

		m_dialog_type = sip_dialog_UAC_e;
		m_local_tag = request.From_Tag();
		m_remote_tag = response.To_Tag();

		const sip_header_t* rlist = response.get_std_header(sip_Record_Route_e);
		if (rlist != nullptr && rlist->get_value_count() > 0)
		{
			/// create the route set in reverse order
			for (int i = rlist->get_value_count() - 1; i >= 0; i--)
			/// but now it has normal order 
			//for (int i = 0; i < rlist->get_value_count(); i++)
			{
				const sip_value_t* route = response.get_Record_Route_value(i);
				sip_value_t* vclone = route->clone();
				SLOG_SESS(m_log_tag, "%s : Select request target uri -- clone '%s'", m_session_ref, (const char*)vclone->to_string());

				m_remote_route_set.add_value(vclone);
			}
		}

		/// set the via
		m_local_via = *request.get_std_header(sip_Via_e);
		m_local_sequaence = request.CSeq_Number();
		m_remote_sequaence = 0;

		m_remote_contact_list = *response.get_std_header(sip_Contact_e);
 		m_local_contact_list = *request.get_std_header(sip_Contact_e);

		if (response.get_Contact()->get_value_count() > 0)
		{
			const sip_value_contact_t* contact = response.get_Contact_value();
			if (contact == nullptr)
			{
				SLOG_WARN(m_log_tag, "%s : Initializing dialog failed : response has no contacts!", m_session_ref);
				goto the_end;
			}

			m_contact_uri = contact->get_uri();

			if (m_contact_uri.get_user().isEmpty() && !m_remote_uri.get_user().isEmpty())
				m_contact_uri.set_user(m_remote_uri.get_user());
		}
		else
		{
			SLOG_WARN(m_log_tag, "%s : Initializing dialog failed : response has no contacts!", m_session_ref);
			goto the_end;
		}

		/// prepare the URIs
		m_local_uri = request.From_URI();
		m_remote_uri = request.To_URI();

		result = true;
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR(m_log_tag, "Initialize Dialog As UAC failed with exception %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

the_end:
	
	return result;
}
//--------------------------------------
//
//--------------------------------------
bool sip_session_t::InitializeDialogAsUAS(const sip_message_t& request, const sip_message_t& response)
{
	SLOG_SESS(m_log_tag, "%s : initialize dialog as UAS %s", m_session_ref, (const char*)request.GetStartLine());

	bool result = FALSE;

	try
	{
		rtl::MutexWatchLock lock(m_dialog_lock, __FUNCTION__);

		if (m_dialog_type != sip_dialog_none_e)
		{
			result = true;
			goto the_end;
		}

		m_dialog_type = sip_dialog_UAS_e;
		m_local_tag = response.To_Tag();
		m_remote_tag = request.From_Tag();

		m_local_contact_list = *response.get_std_header(sip_Contact_e);
		m_remote_contact_list = *request.get_std_header(sip_Contact_e);

		int rlist_size = request.get_Record_Route()->get_value_count();

		if (rlist_size >= 0)
		{
			/// copy the route set in reverse order
			for (int i = rlist_size - 1; i >= 0; i--)
			{
				const sip_value_t* route = request.get_Record_Route_value(i);
				sip_value_t* vclone = route->clone();
				SLOG_SESS(m_log_tag, "%s : add request target uri -- clone '%s'", m_session_ref, (const char*)vclone->to_string());
				m_remote_route_set.add_value(vclone);
//				m_remote_route_set.add_value(route->clone());
			}
		}

		/// create the via
		sip_value_via_t* via = (sip_value_via_t*)m_local_via.get_top_value();

		via->set_address(inet_ntoa(m_profile.get_listener()));
		via->set_port(m_profile.get_listener_port());
		via->set_branch(sip_utils::GenBranchParameter());
		via->set_protocol(m_profile.get_transport());

		m_remote_sequaence = request.CSeq_Number();

		// Renat+Peter 21.06.2011 ???
		m_local_sequaence = 0;

		/// prepare the URIs
		m_local_uri = request.To_URI();
		m_remote_uri = request.From_URI();

		if (request.get_Contact()->get_value_count() > 0)
		{
			const sip_value_contact_t* contact = request.get_Contact_value();
			if (!contact)
			{
				SLOG_WARN(m_log_tag, "%s : Initializing dialog failed : request has no contacts!", m_session_ref);
				goto the_end;
			}

			m_contact_uri = contact->get_uri();
			if (m_contact_uri.get_user().isEmpty() && !m_remote_uri.get_user().isEmpty())
				m_contact_uri.set_user(m_remote_uri.get_user());

			result = true;

			SLOG_SESS(m_log_tag, "%s : UAS dialog initialized", m_session_ref);
		}
		else
		{
			SLOG_WARN(m_log_tag, "%s : Initializing dialog failed : request has no contacts!", m_session_ref);
		}
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR(m_log_tag, "Initialize Dialog as UAS failed with exception %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

the_end:

	return result;
}
//--------------------------------------
// George 08.09.2011
// только для UAC!
//--------------------------------------
bool sip_session_t::CreateRequestWithinDialog(sip_method_type_t method, sip_message_t& request)
{
	bool result = FALSE;
	rtl::String methodName = sip_get_method_name(method);

	SLOG_SESS(m_log_tag, "%s : Create request within dialog method %s", m_session_ref, (const char*)methodName);

	try
	{
		rtl::MutexWatchLock lock(m_dialog_lock, __FUNCTION__);

		SLOG_SESS(m_log_tag, "%s : prepare request %s...", m_session_ref, (const char*)methodName);

		if (m_dialog_type == sip_dialog_none_e)
		{
			SLOG_ERROR(m_log_tag, "%s : Create request within dialog while type is NID!", m_session_ref);
			goto the_end;
		}

		if (methodName.isEmpty())
		{
			SLOG_ERROR(m_log_tag, "%s : Create request within dialog with empy method!", m_session_ref);
			goto the_end;
		}

		/// Create the start line
		request.set_request_line(methodName, m_contact_uri);

		/// Create the from uri
		request.From_URI() = m_local_uri;
		if (!m_local_tag.isEmpty())
			request.From_Tag(m_local_tag);


		/// Create the to uri
		request.make_To_value()->set_uri(m_remote_uri);
		if (!m_remote_tag.isEmpty())
			request.To_Tag(m_remote_tag);

		/// create the via -- ???? George : почему не из профайла?
		request.set_std_header(&m_local_via);
		request.Via_Branch(sip_utils::GenBranchParameter());

		/// only generate a Cseq for nonce-CANCEL and none-ACK requests

		/*
		Requests within a dialog MUST contain strictly monotonically
		increasing and contiguous CSeq sequence numbers (increasing-by-one)
		in each direction (excepting ACK and CANCEL of course, whose numbers
		equal the requests being acknowledged or cancelled).  Therefore, if
		the local sequence number is not empty, the value of the local
		sequence number MUST be incremented by one, and this value MUST be
		placed into the CSeq header field.  If the local sequence number is
		empty, an initial value MUST be chosen using the guidelines of
		Section 8.1.1.5.  The method field in the CSeq header field value
		MUST match the method of the request.
		*/
		if (method != sip_ACK_e && method != sip_CANCEL_e)
			++m_local_sequaence;

		/// Create the CSeq
		request.CSeq(m_local_sequaence, methodName);

		/// set the call-id
		request.CallId(m_call_id);

		/// Set the contact URI
		if (m_local_contact_list.get_value_count() > 0)
		{
			request.set_std_header(&m_local_contact_list);
		}
		else
		{
			sip_uri_t curi;
			// GEORGE : есди нет юзера то берем из фром
			const rtl::String& userName = m_profile.get_username();
			curi.set_user(userName.isEmpty() ? m_local_uri.get_user() : userName);
			curi.set_host(inet_ntoa(m_profile.get_listener()));
			curi.set_port(rtl::int_to_string(m_profile.get_listener_port()));
		
			request.make_Contact_value()->set_uri(curi);
		}

		request.set_body(nullptr, 0);

		bool target_uri_changed = false;
		bool transport_changed = false;
		sip_uri_t target_uri;

		SLOG_SESS(m_log_tag, "%s : Select request target uri -- params: route-set-size=%d", m_session_ref, m_remote_route_set.get_value_count());

		/// transport from route-set if not empty
		if (m_remote_route_set.get_value_count() > 0)
		{
			const sip_value_uri_t* f = (const sip_value_uri_t*)m_remote_route_set.get_top_value();
			target_uri = f->get_uri();

			if (!target_uri.get_parameters().contains("lr"))
			{
				// если нет параметра 'lr' (strict-router) то заменяем и RURI и транспорт в направлении указанном верхним роутом
				// а также удаляем его. remote-target добавляем в конец Route
				/*RouteList route_list = m_remote_route_set;
				route_list.MakeUnique();
				route_list.PopTopURI();*/

				request.make_Route()->assign(m_remote_route_set);
				request.get_std_header(sip_Route_e)->remove_value_at(0);
				request.get_std_header(sip_Route_e)->add_value(m_remote_uri.to_string());

				SLOG_SESS(m_log_tag, "%s : Select request target uri -- route-without 'lr'", m_session_ref);
				SLOG_SESS(m_log_tag, "%s : Select request target uri -- target-uri %s", m_session_ref, (const char*)target_uri.to_string());

				request.set_request_uri(target_uri);
			}
			else
			{
				// если есть параметр 'lr' заменяем только транспорт в направлении указанном верхним роутом
				request.make_Route()->assign(m_remote_route_set);

				SLOG_SESS(m_log_tag, "%s : Select request target uri -- route-with 'lr'", m_session_ref);
				SLOG_SESS(m_log_tag, "%s : Select request target uri -- target-uri '%s'", m_session_ref, (const char*)m_contact_uri.to_string());
			}

			
			int route_list_size = request.get_Route()->get_value_count();
			SLOG_SESS(m_log_tag, "%s : Select request target uri -- route-list-size %d", m_session_ref, route_list_size);
			for (int i = 0; i < route_list_size; i++)
			{
				const sip_value_uri_t* r = request.get_Route_value(i);
				const sip_uri_t& route_uri = r->get_uri();
				SLOG_SESS(m_log_tag, "%s : ----> route-list[%d] = %s", m_session_ref, i, (const char*)route_uri.to_string());
			}

			target_uri_changed = true;
		}
		else if (m_remote_contact_list.get_value_count() > 0 && m_route.type == sip_transport_udp_e)
		{
			sip_value_contact_t* r_contact = (sip_value_contact_t*)m_remote_contact_list.get_value_at(0);

			target_uri = r_contact->get_uri();
			target_uri_changed = true;
		}
		
		if (target_uri_changed)
		{
			const rtl::String& str_host = target_uri.get_host();
			const rtl::String& str_port = target_uri.get_port();
			bool b;
			const rtl::String& str_type = target_uri.get_parameters().get("transport", b);

			in_addr host_addr = net_parse(str_host);
			uint16_t host_port = 5060;
			
			sip_transport_type_t type = parse_transport_type(str_type);
			
			SLOG_SESS(m_log_tag, "%s : target uri changed to : %s:%u! ", m_session_ref, inet_ntoa(host_addr), host_port);

			if (!str_port.isEmpty())
			{
				host_port = (uint16_t)strtoul(str_port, nullptr, 10);
			}

			char addr1[32], addr2[32];
			SLOG_SESS(m_log_tag, "%s : compare transports -- %s:%u with %s:%u (%s)", m_session_ref, net_to_string(addr1, 32, host_addr), host_port,
				net_to_string(addr2, 32, m_route.remoteAddress), m_route.remotePort, TransportType_toString(type));

			// выбор типа транспорта добавлен
			if (host_addr.s_addr == m_route.remoteAddress.s_addr && host_port != m_route.remotePort && m_route.type != type)
			{
				sip_transport_route_t route;
				route.type = type;
				route.if_address = m_route.if_address;
				route.if_port = m_route.if_port;
				route.remoteAddress = host_addr;
				route.remotePort = host_port;

				request.set_route(route);
				transport_changed = true;
			}
		}

		if (!transport_changed)
		{
			request.set_route(m_route);
		}

		SLOG_SESS(m_log_tag, "%s : request %s created : target_uri %s", m_session_ref, (const char*)methodName, inet_ntoa(request.get_remote_address()), request.get_remote_port());

		result = true;
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR(m_log_tag, "Create Request Within Dialog failed with exception %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

the_end:

	return result;
}
//--------------------------------------
//
//--------------------------------------
void sip_session_t::StartNATKeepAlive(/*const SIPURI& target, */int expires)
{
	if (expires == 0)
	{
		m_manager.stop_session_timer(this, SESSION_TIMER_KEEPALIVE);
	}
	else
	{
//		m_nat_keepalive_target = target;
		m_manager.start_session_timer(this, SESSION_TIMER_KEEPALIVE, expires * 1000, false);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_session_t::StopNATKeepAlive()
{
	m_manager.stop_session_timer(this, SESSION_TIMER_KEEPALIVE);
}
//--------------------------------------
//
//--------------------------------------
void sip_session_t::StartAutoDestructTimer(int expires)
{
	SLOG_SESS(m_log_tag, "%s : start auto destruct timer for %d sec...", m_session_ref, expires);
	m_manager.stop_session_timer(this, SESSION_TIMER_ALL);
	m_manager.start_session_timer(this, SESSION_TIMER_AUTODESTROY, expires * 1000, true);
}
//--------------------------------------
//
//--------------------------------------
void sip_session_t::StopAutoDestructTimer()
{
	SLOG_SESS(m_log_tag, "%s : stop auto destruct timer", m_session_ref);
	m_manager.stop_session_timer(this, SESSION_TIMER_AUTODESTROY);
}
//--------------------------------------
//
//--------------------------------------
void sip_session_t::StartSessionExpireTimer(uint32_t seconds, bool single)
{
	if (seconds == 0)
		m_manager.stop_session_timer(this, SESSION_TIMER_EXPIRES);
	else
		m_manager.start_session_timer(this, SESSION_TIMER_EXPIRES, seconds * 1000, single);
}
//--------------------------------------
//
//--------------------------------------
void sip_session_t::StopSessionExpireTimer()
{
	m_manager.stop_session_timer(this, SESSION_TIMER_EXPIRES);
}
//--------------------------------------
//
//--------------------------------------
void sip_session_t::process_session_expire_event()
{
	// do nothing
}
//--------------------------------------
//
//--------------------------------------
void sip_session_t::timer_event(uint32_t timer_id)
{
	// do nothing

	if (timer_id == SESSION_TIMER_KEEPALIVE)
	{
		if (m_profile.get_keep_alive_interval() > 0)
		{
			sip_message_t keepAlive;

			SLOG_SESS(m_log_tag, "%s : KEEP-ALIVE to %s:%u", m_session_ref, inet_ntoa(m_profile.get_proxy_address()), m_profile.get_proxy_port());//(ZString)reqURI);
		
			sip_uri_t requestUri = inet_ntoa(m_profile.get_proxy_address());
			keepAlive.set_request_line(sip_KEEP_ALIVE_e, requestUri);
			keepAlive.set_route(m_route);
			m_manager.get_user_agent().push_message_to_transport(keepAlive);
		}
		else
		{
			m_manager.stop_session_timer(this, SESSION_TIMER_KEEPALIVE);
		}
	}
	else if (timer_id == SESSION_TIMER_AUTODESTROY)
	{
		SLOG_SESS(m_log_tag, "%s : auto destruct timer elapsed", m_session_ref);

		destroy_session();
	}
	else if (timer_id == SESSION_TIMER_EXPIRES)
	{
		process_session_expire_event();
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_session_t::SetCurrentUACRequest(const sip_message_t& request)
{
	rtl::MutexWatchLock lock(m_uac_request_lock, __FUNCTION__);
	m_uac_request = request;
}
//--------------------------------------
//
//--------------------------------------
const char* sip_session_t::async_get_name()
{
	return m_log_tag;
}
//--------------------------------------
//
//--------------------------------------
void sip_session_t::async_handle_call(uint16_t call_id, uintptr_t int_param, void* ptr_param)
{
	if (m_manager.is_valid_session(this))
	{
		switch (call_id)
		{
		case 1:	// timer_event
			timer_event((uint32_t)int_param);
			break;
		}
	}
}
//--------------------------------------
//
//--------------------------------------
#pragma region SIP session list
static int session_compare(const sip_session_list_t::sess_key_t& left, const sip_session_list_t::sess_key_t& right)
{
	return std_stricmp(left.key, right.key);
}
//--------------------------------------
//
//--------------------------------------
static int session_key_compare(const ZString& left, const sip_session_list_t::sess_key_t& right)
{
	return std_stricmp(left, right.key);
}
//--------------------------------------
//
//--------------------------------------
sip_session_list_t::sip_session_list_t(const char* tag) : m_tag(tag), m_pool(session_compare, session_key_compare, 512)
{
}
//--------------------------------------
//
//--------------------------------------
sip_session_list_t::~sip_session_list_t()
{
	for (int i = 0; i < m_pool.getCount(); i++)
	{
		sess_key_t& key = m_pool[i];
		FREE(key.key);
	}
	m_pool.clear();

}
//--------------------------------------
//
//--------------------------------------
bool sip_session_list_t::add(sip_session_t* session, const char* key)
{
	bool res = false;

	if (session != nullptr && key != nullptr)
	{
		sess_key_t trn_rec = { nullptr, session };

		int len = (int)strlen(key);
		trn_rec.key = (char*)MALLOC(len+1);
		strcpy(trn_rec.key, key);
		res = m_pool.add(trn_rec) != BAD_INDEX;
	}

	return res;
}
//--------------------------------------
//
//--------------------------------------
sip_session_t* sip_session_list_t::remove(const char* key)
{
	sip_session_t* session = nullptr;

	if (key != nullptr)
	{
		int index;
		sess_key_t* sess_ptr = m_pool.find(key, index);
		if (sess_ptr != nullptr)
		{
			int index = PTR_DIFF(sess_ptr, m_pool.getBase());
			FREE(sess_ptr->key);
			sess_ptr->key = nullptr;
			session = sess_ptr->session;
			m_pool.removeAt(index);
			return session;
		}
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
void sip_session_list_t::remove(sip_session_t* session)
{
	for (int i = 0; i < m_pool.getCount(); i++)
	{
		sess_key_t& sess_ptr = m_pool[i];
		if (sess_ptr.session == session)
		{
			FREE(sess_ptr.key);
			sess_ptr.key = nullptr;
			m_pool.removeAt(i);
			return;
		}
	}
}
//--------------------------------------
//
//--------------------------------------
sip_session_t* sip_session_list_t::removeAt(int index)
{
	sip_session_t* session = nullptr;

	if (index >= 0 && index < m_pool.getCount())
	{
		sess_key_t& sess_ptr = m_pool[index];
		FREE(sess_ptr.key);
		sess_ptr.key = nullptr;
		session = sess_ptr.session;
		m_pool.removeAt(index);
		SLOG_SESS(m_tag, "Session removed");
		return session;
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
sip_session_t* sip_session_list_t::find(const char* key)
{
	if (key != nullptr)
	{
		sess_key_t* sess_ptr = m_pool.find(key);
		if (sess_ptr != nullptr)
			return sess_ptr->session;
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
sip_session_t* sip_session_list_t::find(const char* key, int& index)
{
	if (key != nullptr)
	{
		sess_key_t* sess_ptr = m_pool.find(key, index);
		if (sess_ptr != nullptr)
			return sess_ptr->session;
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
void sip_session_list_t::clear()
{
	for (int i = 0; i < m_pool.getCount(); i++)
	{
		sess_key_t& key = m_pool[i];

		if (key.key != nullptr)
			FREE(key.key);
	}

	m_pool.clear();
}
#pragma endregion
//--------------------------------------
