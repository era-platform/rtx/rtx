/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "sip_transaction_ic.h"
#include "sip_transaction_manager.h"
//--------------------------------------
//
//--------------------------------------
static const char* GetStateName(int state)
{
	static const char* names[] = { "Idle", "Calling", "Proceeding", "Completed", "Connected", "Terminated" };

	return state >= 0 && state <= sip_transaction_ic_t::ic_transaction_state_terminated_e ? names[state] : "Unknown";
}
//--------------------------------------
//
//--------------------------------------
sip_transaction_ic_t::sip_transaction_ic_t(const rtl::String& id, sip_transaction_manager_t& manager) :
	sip_transaction_t(id, sip_transaction_ict_e, manager)
{
	m_state = ic_transaction_state_idle_e;
	m_has_sent_initial_request = false;

	SLOG_TRANS(get_type_name(), "%s Created with Id: %s", m_internal_id, (const char*)id);
}
//--------------------------------------
//
//--------------------------------------
sip_transaction_ic_t::~sip_transaction_ic_t()
{
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_ic_t::process_sip_transaction_event(sip_message_t& message)
{
	SLOG_TRANS(get_type_name(), "%s State.%s", m_internal_id, GetStateName(m_state));

	switch (m_state)
	{
	case ic_transaction_state_idle_e:
		process_idle_state(message);
		break;
	case ic_transaction_state_calling_e:
		process_calling_state(message);
		break;
	case ic_transaction_state_proceeding_e:
		process_proceeding_state(message);
		break;
	case ic_transaction_state_completed_e:
		process_completed_state(message);
		break;
	case ic_transaction_state_connected_e:
		process_connected_state(message);
		break;
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_ic_t::process_sip_transaction_cancel_event()
{
	stop_timer(sip_timer_A_e);
	stop_timer(sip_timer_B_e);

	set_state(ic_transaction_state_terminated_e);

	destroy();
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_ic_t::process_idle_state(sip_message_t& message)
{
	if (!m_has_sent_initial_request)
	{
		set_opening_request(message);
		m_opening_request.has_transaction(true);

		if (!message.is_request())
		{
			SLOG_ERROR(get_type_name(), "%s State.Idle : sip_transaction_ic_t got none request event on State Idle", m_internal_id);
			return;
		}

		if (message.get_request_method() == sip_INVITE_e)
		{
			if (!message.is_request())
				SLOG_WARN(get_type_name(), "%s State.Idle : sip_transaction_ic_t got None-Invite event on State Idle", m_internal_id);
		}

		/// send it to the transport

		m_has_sent_initial_request = true;
		STR_COPY(m_ua_core_name, message.GetUACoreName());

		SLOG_TRANS(get_type_name(), "%s State.Idle : send to transport", m_internal_id);

		send_message_to_transport(message);

		SLOG_TRANS(get_type_name(), "%s State.Idle --> State.Calling", m_internal_id);

		set_state(ic_transaction_state_calling_e);

		/*
		The state machine for the INVITE client transaction is shown in
		Figure 5.  The initial state, "calling", MUST be entered when the TU
		initiates a NEW client transaction with an INVITE request.  The
		client transaction MUST pass the request to the transport layer for
		transmission (see Section 18).  If an unreliable transport is being
		used, the client transaction MUST start timer A with a value of T1.
		If a reliable transport is being used, the client transaction SHOULD
		NOT start timer A (Timer A controls request retransmissions).  For
		any transport, the client transaction MUST start timer B with a value
		of 64*T1 seconds (Timer B controls transaction timeouts).
		*/

		if (!is_reliable_transport())
			start_timer(sip_timer_A_e, SIP_INTERVAL_A); /// start Time A for UDP only

		start_timer(sip_timer_B_e, SIP_INTERVAL_B);
	}
	// ��������� ��� ����� ����� ������� ���� �� �������� �������!
	else if (message.is_response() && !message.Is2xx())
	{
		SLOG_TRANS(get_type_name(), "%s (1xx) Handle State.Idle --> State.Calling", m_internal_id);

		set_state(ic_transaction_state_calling_e);
		process_calling_state(message);
	}

}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_ic_t::process_calling_state(sip_message_t& message)
{
	/// this is a retransmission, ignore it
	if (message.is_request())
	{
		SLOG_ERROR(get_type_name(), "%s State.Calling : message is request!", m_internal_id);
		return;
	}

	uint16_t statusCode = message.get_response_code();

	stop_timer(sip_timer_A_e);
	stop_timer(sip_timer_B_e);

	if (statusCode >= 100 && statusCode < 200)
	{
		/*If the client transaction receives a provisional response while in
		the "Calling" state, it transitions to the "Proceeding" state. In the
		"Proceeding" state, the client transaction SHOULD NOT retransmit the
		request any longer. Furthermore, the provisional response MUST be
		passed to the TU.  Any further provisional responses MUST be passed
		up to the TU while in the "Proceeding" state.*/

		SLOG_TRANS(get_type_name(), "%s (1xx) State.Calling --> State.Proceeding", m_internal_id);

		set_state(ic_transaction_state_proceeding_e);

		m_manager.process_received_message_event(message, *this);

	}
	else if (statusCode >= 200 && statusCode < 300)
	{
		/* When in either the "Calling" or "Proceeding" states, reception of a
		2xx response MUST cause the client transaction to enter the
		"Terminated" state, and the response MUST be passed up to the TU.
		The handling of this response depends on whether the TU is a proxy
		core or a UAC core.  A UAC core will handle generation of the ACK for
		this response, while a proxy core will always forward the 200 (OK)
		upstream.  The differing treatment of 200 (OK) between proxy and UAC
		is the reason that handling of it does not take place in the
		transaction layer.*/

		SLOG_TRANS(get_type_name(), "%s (2xx) State.Calling --> State.Connected", m_internal_id);

		set_state(ic_transaction_state_connected_e);

		m_manager.process_received_message_event(message, *this);

		//StartTimer_D();
		start_timer(sip_timer_D_e, SIP_INTERVAL_D);
	}
	else if (statusCode >= 300 && statusCode < 400)
	{
		/*When in either the "Calling" or "Proceeding" states, reception of a
		response with status code from 300-699 MUST cause the client
		transaction to transition to "Completed".  The client transaction
		MUST pass the received response up to the TU, and the client
		transaction MUST generate an ACK request, even if the transport is
		reliable (guidelines for constructing the ACK from the response are
		given in Section 17.1.1.3) and then pass the ACK to the transport
		layer for transmission.  The ACK MUST be sent to the same address,
		port, and transport to which the original request was sent.  The
		client transaction SHOULD start timer D when it enters the
		"Completed" state, with a value of at least 32 seconds for unreliable
		transports, and a value of zero seconds for reliable transports.
		Timer D reflects the amount of time that the server transaction can
		remain in the "Completed" state when unreliable transports are used.
		This is equal to Timer H in the INVITE server transaction, whose*/

		SLOG_TRANS(get_type_name(), "%s (3xx) State.Calling --> State.Completed", m_internal_id);

		set_state(ic_transaction_state_completed_e);

		m_manager.process_received_message_event(message, *this);

		send_ACK(message);

		if (!is_reliable_transport())
			start_timer(sip_timer_D_e, SIP_INTERVAL_D);
		else
			destroy();

	}
	else if (statusCode >= 400 && statusCode < 500)
	{
		/*When in either the "Calling" or "Proceeding" states, reception of a
		response with status code from 300-699 MUST cause the client
		transaction to transition to "Completed".  The client transaction
		MUST pass the received response up to the TU, and the client
		transaction MUST generate an ACK request, even if the transport is
		reliable (guidelines for constructing the ACK from the response are
		given in Section 17.1.1.3) and then pass the ACK to the transport
		layer for transmission.  The ACK MUST be sent to the same address,
		port, and transport to which the original request was sent.  The
		client transaction SHOULD start timer D when it enters the
		"Completed" state, with a value of at least 32 seconds for unreliable
		transports, and a value of zero seconds for reliable transports.
		Timer D reflects the amount of time that the server transaction can
		remain in the "Completed" state when unreliable transports are used.
		This is equal to Timer H in the INVITE server transaction, whose*/

		SLOG_TRANS(get_type_name(), "%s (4xx) State.Calling --> State.Completed", m_internal_id);

		set_state(ic_transaction_state_completed_e);

		///m_TUMessageHandler(message, (INT)this);
		m_manager.process_received_message_event(message, *this);
		send_ACK(message);

		if (!is_reliable_transport())
			start_timer(sip_timer_D_e, SIP_INTERVAL_D);
		else
			destroy();
	}
	else if (statusCode >= 500 && statusCode < 600)
	{
		/*When in either the "Calling" or "Proceeding" states, reception of a
		response with status code from 300-699 MUST cause the client
		transaction to transition to "Completed".  The client transaction
		MUST pass the received response up to the TU, and the client
		transaction MUST generate an ACK request, even if the transport is
		reliable (guidelines for constructing the ACK from the response are
		given in Section 17.1.1.3) and then pass the ACK to the transport
		layer for transmission.  The ACK MUST be sent to the same address,
		port, and transport to which the original request was sent.  The
		client transaction SHOULD start timer D when it enters the
		"Completed" state, with a value of at least 32 seconds for unreliable
		transports, and a value of zero seconds for reliable transports.
		Timer D reflects the amount of time that the server transaction can
		remain in the "Completed" state when unreliable transports are used.
		This is equal to Timer H in the INV329301:59:01.367        DTL: [CID=0x06a1]       RTP: (Audio) addr=63.116.254.207:33968->209.176.20.30:55643 enc=0 rx=104 tx=112 lost=10 outOfOrder=0 late=0 rxTime=0/0 txTime=20/32 jitter=0/1
		329301:59:01.367        DTL: [CID=0x06a1]       RTP: (Audio) addr=63.116.254.207:33970->63.116.254.181:10860 enc=0 rx=112 tx=104 lost=0 outOfOrder=0 late=0 rxTime=20/32 txTime=21/60 jitter=21/21 ITE server transaction, whose*/

		SLOG_TRANS(get_type_name(), "%s (5xx) State.Calling -> State.Completed", m_internal_id);

		set_state(ic_transaction_state_completed_e);

		send_ACK(message);

		/// notify the manager
		m_manager.process_received_message_event(message, *this);

		if (!is_reliable_transport())
			start_timer(sip_timer_D_e, SIP_INTERVAL_D);
		else
			destroy();
	}
	else if (statusCode >= 600 && statusCode < 700)
	{
		/*When in either the "Calling" or "Proceeding" states, reception of a
		response with status code from 300-699 MUST cause the client
		transaction to transition to "Completed".  The client transaction
		MUST pass the received response up to the TU, and the client
		transaction MUST generate an ACK request, even if the transport is
		reliable (guidelines for constructing the ACK from the response are
		given in Section 17.1.1.3) and then pass the ACK to the transport
		layer for transmission.  The ACK MUST be sent to the same address,
		port, and transport to which the original request was sent.  The
		client transaction SHOULD start timer D when it enters the
		"Completed" state, with a value of at least 32 seconds for unreliable
		transports, and a value of zero seconds for reliable transports.
		Timer D reflects the amount of time that the server transaction can
		remain in the "Completed" state when unreliable transports are used.
		This is equal to Timer H in the INVITE server transaction, whose*/

		SLOG_TRANS(get_type_name(), "%s (6xx) State.Calling -> State.Completed", m_internal_id);

		set_state(ic_transaction_state_completed_e);

		m_manager.process_received_message_event(message, *this);
		send_ACK(message);

		if (!is_reliable_transport())
			start_timer(sip_timer_D_e, SIP_INTERVAL_D);
		else
			destroy();
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_ic_t::process_proceeding_state(sip_message_t& message)
{
	/// this is a retransmission, ignore it
	if (message.is_request())
	{
		SLOG_ERROR(get_type_name(), "%s Handle State.Proceeding : message is request!", m_internal_id);
		return;
	}

	uint16_t statusCode = message.get_response_code();

	stop_timer(sip_timer_A_e);
	stop_timer(sip_timer_B_e);

	if (statusCode >= 100 && statusCode < 200)
	{
		/*  Any further provisional responses MUST be passed
		up to the TU while in the "Proceeding" state. */

		SLOG_TRANS(get_type_name(), "%s (6xx) State.Proceeding : received provisional message", m_internal_id);

		m_manager.process_received_message_event(message, *this);
	}
	else if (statusCode >= 200 && statusCode < 300)
	{
		/* When in either the "Calling" or "Proceeding" states, reception of a
		2xx response MUST cause the client transaction to enter the
		"Terminated" state, and the response MUST be passed up to the TU.
		The handling of this response depends on whether the TU is a proxy
		core or a UAC core.  A UAC core will handle generation of the ACK for
		this response, while a proxy core will always forward the 200 (OK)
		upstream.  The differing treatment of 200 (OK) between proxy and UAC
		is the reason that handling of it does not take place in the
		transaction layer.*/

		SLOG_TRANS(get_type_name(), "%s (2xx) State.Proceeding --> State.Connected", m_internal_id);

		set_state(ic_transaction_state_connected_e);

		m_manager.process_received_message_event(message, *this);

		start_timer(sip_timer_D_e, SIP_INTERVAL_D);
	}
	else if (statusCode >= 300 && statusCode < 400)
	{
		/*When in either the "Calling" or "Proceeding" states, reception of a
		response with status code from 300-699 MUST cause the client
		transaction to transition to "Completed".  The client transaction
		MUST pass the received response up to the TU, and the client
		transaction MUST generate an ACK request, even if the transport is
		reliable (guidelines for constructing the ACK from the response are
		given in Section 17.1.1.3) and then pass the ACK to the transport
		layer for transmission.  The ACK MUST be sent to the same address,
		port, and transport to which the original request was sent.  The
		client transaction SHOULD start timer D when it enters the
		"Completed" state, with a value of at least 32 seconds for unreliable
		transports, and a value of zero seconds for reliable transports.
		Timer D reflects the amount of time that the server transaction can
		remain in the "Completed" state when unreliable transports are used.
		This is equal to Timer H in the INVITE server transaction, whose*/

		SLOG_TRANS(get_type_name(), "%s (3xx) State.Proceeding --> State.Completed", m_internal_id);

		set_state(ic_transaction_state_completed_e);

		m_manager.process_received_message_event(message, *this);

		send_ACK(message);

		if (!is_reliable_transport())
			start_timer(sip_timer_D_e, SIP_INTERVAL_D);
		else
			destroy();

	}
	else if (statusCode >= 400 && statusCode < 500)
	{
		/*When in either the "Calling" or "Proceeding" states, reception of a
		response with status code from 300-699 MUST cause the client
		transaction to transition to "Completed".  The client transaction
		MUST pass the received response up to the TU, and the client
		transaction MUST generate an ACK request, even if the transport is
		reliable (guidelines for constructing the ACK from the response are
		given in Section 17.1.1.3) and then pass the ACK to the transport
		layer for transmission.  The ACK MUST be sent to the same address,
		port, and transport to which the original request was sent.  The
		client transaction SHOULD start timer D when it enters the
		"Completed" state, with a value of at least 32 seconds for unreliable
		transports, and a value of zero seconds for reliable transports.
		Timer D reflects the amount of time that the server transaction can
		remain in the "Completed" state when unreliable transports are used.
		This is equal to Timer H in the INVITE server transaction, whose*/

		SLOG_TRANS(get_type_name(), "%s (4xx) State.Proceeding --> State.Completed", m_internal_id);

		set_state(ic_transaction_state_completed_e);

		m_manager.process_received_message_event(message, *this);

		send_ACK(message);

		if (!is_reliable_transport())
			start_timer(sip_timer_D_e, SIP_INTERVAL_D);
		else
			destroy();
	}
	else if (statusCode >= 500 && statusCode < 600)
	{
		/*When in either the "Calling" or "Proceeding" states, reception of a
		response with status code from 300-699 MUST cause the client
		transaction to transition to "Completed".  The client transaction
		MUST pass the received response up to the TU, and the client
		transaction MUST generate an ACK request, even if the transport is
		reliable (guidelines for constructing the ACK from the response are
		given in Section 17.1.1.3) and then pass the ACK to the transport
		layer for transmission.  The ACK MUST be sent to the same address,
		port, and transport to which the original request was sent.  The
		client transaction SHOULD start timer D when it enters the
		"Completed" state, with a value of at least 32 seconds for unreliable
		transports, and a value of zero seconds for reliable transports.
		Timer D reflects the amount of time that the server transaction can
		remain in the "Completed" state when unreliable transports are used.
		This is equal to Timer H in the INVITE server transaction, whose*/

		SLOG_TRANS(get_type_name(), "%s (5xx) State.Proceeding --> State.Completed", m_internal_id);

		set_state(ic_transaction_state_completed_e);

		send_ACK(message);

		/// notify the manager
		m_manager.process_received_message_event(message, *this);

		if (!is_reliable_transport())
			start_timer(sip_timer_D_e, SIP_INTERVAL_D);
		else
			destroy();
	}
	else if (statusCode >= 600 && statusCode < 700)
	{
		/*When in either the "Calling" or "Proceeding" states, reception of a
		response with status code from 300-699 MUST cause the client
		transaction to transition to "Completed".  The client transaction
		MUST pass the received response up to the TU, and the client
		transaction MUST generate an ACK request, even if the transport is
		reliable (guidelines for constructing the ACK from the response are
		given in Section 17.1.1.3) and then pass the ACK to the transport
		layer for transmission.  The ACK MUST be sent to the same address,
		port, and transport to which the original request was sent.  The
		client transaction SHOULD start timer D when it enters the
		"Completed" state, with a value of at least 32 seconds for unreliable
		transports, and a value of zero seconds for reliable transports.
		Timer D reflects the amount of time that the server transaction can
		remain in the "Completed" state when unreliable transports are used.
		This is equal to Timer H in the INVITE server transaction, whose*/

		SLOG_TRANS(get_type_name(), "%s (6xx) State.Proceeding --> State.Completed", m_internal_id);

		set_state(ic_transaction_state_completed_e);

		m_manager.process_received_message_event(message, *this);

		send_ACK(message);

		if (!is_reliable_transport())
			start_timer(sip_timer_D_e, SIP_INTERVAL_D);
		else
			destroy();
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_ic_t::process_connected_state(sip_message_t& message)
{
	int statusCode = message.get_response_code();

	if (statusCode >= 200 && statusCode < 300)
	{
		/* When in either the "Calling" or "Proceeding" states, reception of a
		2xx response MUST cause the client transaction to enter the
		"Terminated" state, and the response MUST be passed up to the TU.
		The handling of this response depends on whether the TU is a proxy
		core or a UAC core.  A UAC core will handle generation of the ACK for
		this response, while a proxy core will always forward the 200 (OK)
		upstream.  The differing treatment of 200 (OK) between proxy and UAC
		is the reason that handling of it does not take place in the
		transaction layer.*/

		m_manager.process_received_message_event(message, *this);

		SLOG_TRANS(get_type_name(), "%s (2xx) State.Connected : Send ACK", m_internal_id);

		send_ACK(message);
	}
	else
	{
		SLOG_WARN(get_type_name(), "%s (?) State.Connected : Illegal state", m_internal_id);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_ic_t::process_completed_state(sip_message_t& message)
{
	/*Any retransmissions of the final response that are received while in
	the "Completed" state MUST cause the ACK to be re-passed to the
	transport layer for retransmission, but the newly received response
	MUST NOT be passed up to the TU.  A retransmission of the response is
	defined as any response which would match the same client transaction
	based on the rules of Section 17.1.3.*/

	if (!message.is_request())
	{
		SLOG_TRANS(get_type_name(), "%s (2xx) State.Completed : Send ACK", m_internal_id);
		send_ACK(message);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_ic_t::process_sip_timer_event(sip_transaction_timer_t timerType, uint32_t interval)
{
	SLOG_TRANS(get_type_name(), "%s Timer event %s state %s",
			m_internal_id, sip_timer_manager_t::GetTimerName(timerType), GetStateName(m_state));

	switch (timerType)
	{
	case sip_timer_life_span_e:
		set_state(ic_transaction_state_terminated_e);
		m_manager.process_timer_expire_event(timerType, *this);
		destroy();
		break;
	case sip_timer_A_e:
		if (m_state == ic_transaction_state_calling_e)
		{
			/*  
			When timer A fires, the client transaction MUST retransmit the
			request by passing it to the transport layer, and MUST reset the
			timer with a value of 2*T1.  The formal definition of retransmit
			within the context of the transaction layer is to take the message
			previously sent to the transport layer and pass it to the transport
			layer once more. */

			/// retransmit
			SLOG_TRANS(get_type_name(), "%s Timer A : retransmit message and restart timer A (%d ms).", m_internal_id, interval * 2);

			send_message_to_transport(m_opening_request);

			/// retstart timer with compounded interval 
			if (interval < 7000)
				start_timer(sip_timer_A_e, interval * 2);
		}
		break;
	case sip_timer_B_e:

		if (m_state == ic_transaction_state_calling_e)
		{
			/*If the client transaction is still in the "Calling" state when timer
			B fires, the client transaction SHOULD inform the TU that a timeout
			has occurred.  The client transaction MUST NOT generate an ACK.  The
			value of 64*T1 is equal to the amount of time required to send seven
			requests in the case of an unreliable transport.*/

			SLOG_TRANS(get_type_name(), "%s Timer B : stop timer A and begin terminating", m_internal_id);


			stop_timer(sip_timer_A_e);

			set_state(ic_transaction_state_terminated_e);

			m_manager.process_timer_expire_event(timerType, *this);

			destroy();
		}
		break;
	case sip_timer_D_e:
		/*
		The client transaction SHOULD start timer D when it enters the
		"Completed" state, with a value of at least 32 seconds for unreliable
		transports, and a value of zero seconds for reliable transports.
		Timer D reflects the amount of time that the server transaction can
		remain in the "Completed" state when unreliable transports are used.
		This is equal to Timer H in the INVITE server transaction, whose
		default is 64*T1.
		*/

		if (m_state == ic_transaction_state_completed_e || m_state == ic_transaction_state_connected_e)
		{
			SLOG_TRANS(get_type_name(), "%s Timer D : Begin terminating", m_internal_id);

			set_state(ic_transaction_state_terminated_e);
			destroy();
		}
		else
		{
			//PAssertAlways(PLogicError);
			SLOG_WARN(get_type_name(), "%s Timer D : Transaction State (%s) IS NOT VALID!", m_internal_id, GetStateName(m_state));
			set_state(ic_transaction_state_terminated_e);
			destroy();
		}
		break;
	default:
		SLOG_WARN(get_type_name(), "%s %s : Invalid timer tick received!", m_internal_id, sip_timer_manager_t::GetTimerName(timerType));
		set_state(ic_transaction_state_terminated_e);
		destroy();
		break;
	};
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_ic_t::send_ACK(const sip_message_t& response)
{
	/* RFC Says:

	17.1.1.3 Construction of the ACK Request

	This section specifies the construction of ACK requests sent within
	the client transaction.  A UAC core that generates an ACK for 2xx
	MUST instead follow the rules described in Section 13.

	The ACK request constructed by the client transaction MUST contain
	values for the Call-ID, From, and Request-URI that are equal to the
	values of those header fields in the request passed to the transport
	by the client transaction (call this the "original request").  The To
	header field in the ACK MUST equal the To header field in the
	response being acknowledged, and therefore will usually differ from
	the To header field in the original request by the addition of the
	tag parameter.  The ACK MUST contain a single Via header field, and
	this MUST be equal to the top Via header field of the original
	request.  The CSeq header field in the ACK MUST contain the same
	value for the sequence number as was present in the original request,
	but the method parameter MUST be equal to "ACK".
	If the INVITE request whose response is being acknowledged had Route
	header fields, those header fields MUST appear in the ACK.  This is
	to ensure that the ACK can be routed properly through any downstream
	stateless proxies.

	Although any request MAY contain a body, a body in an ACK is special
	since the request cannot be rejected if the body is not understood.
	Therefore, placement of bodies in ACK for non-2xx is NOT RECOMMENDED,
	but if done, the body types are restricted to any that appeared in
	the INVITE, assuming that the response to the INVITE was not 415.  If
	it was, the body in the ACK MAY be any type listed in the Accept
	header field in the 415.

	For example, consider the following request:

	INVITE sip:bob@biloxi.com SIP/2.0
	Via: SIP/2.0/UDP pc33.atlanta.com;branch=z9hG4bKkjshdyff
	To: Bob <sip:bob@biloxi.com>
	From: Alice <sip:alice@atlanta.com>;tag=88sja8x
	Max-Forwards: 70
	Call-ID: 987asjd97y7atg
	CSeq: 986759 INVITE

	The ACK request for a non-2xx final response to this request would
	look like this:

	ACK sip:bob@biloxi.com SIP/2.0
	Via: SIP/2.0/UDP pc33.atlanta.com;branch=z9hG4bKkjshdyff
	To: Bob <sip:bob@biloxi.com>;tag=99sa0xk
	From: Alice <sip:alice@atlanta.com>;tag=88sja8x
	Max-Forwards: 70
	Call-ID: 987asjd97y7atg
	CSeq: 986759 ACK

	*/

	sip_message_t ack = m_opening_request;

	/// remove authorization
	ack.remove_std_header(sip_Authorization_e);
	ack.remove_std_header(sip_WWW_Authenticate_e);
	ack.remove_std_header(sip_Authentication_Info_e);

	/// change the RequestLine method
	ack.set_request_method(sip_ACK_e);

	/// Clone the response TO Header
	ack.set_std_header(response.get_std_header(sip_To_e));

	/// Clone the from
	ack.set_std_header(m_opening_request.get_std_header(sip_From_e));

	/// Clone the callId
	ack.set_std_header(m_opening_request.get_std_header(sip_Call_ID_e));

	/// Change CSeq method to ACK
	ack.CSeq(m_opening_request.CSeq_Number(), sip_get_method_name(sip_ACK_e));

	/// set the max forwards to 70
	ack.make_Max_Forwards_value()->set_number(SIP_DEFAULT_MAX_FORWARDS);

	/// blank out body
	ack.set_body(nullptr, 0);

	SLOG_TRANS(get_type_name(), "%s Send ACK", m_internal_id);

	/// Save ack message for retransmission
	//m_TransportSender(ack, (INT)this);
	send_message_to_transport(ack);
}
//--------------------------------------
