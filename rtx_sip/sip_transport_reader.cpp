/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_transport_reader.h"
//--------------------------------------
//
//--------------------------------------
volatile int32_t sip_transport_object_t::m_id_generator = 0;
//--------------------------------------
//
//--------------------------------------
void sip_transport_object_t::generate_name(const char* prefix)
{
	m_oid = std_interlocked_inc(&m_id_generator);
	int len = std_snprintf(m_name, 15, "%s(%u)", prefix, m_oid);
	m_name[len] = 0;
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_object_t::update_name(const char* prefix)
{
	int len = std_snprintf(m_name, 15, "%s(%u)", prefix, m_oid);
	m_name[len] = 0;
}
//--------------------------------------
//
//--------------------------------------
sip_transport_object_t::sip_transport_object_t(net_socket_t& sock) :
	m_next_link(nullptr), m_ref_count(1), m_reader(nullptr), m_readable(true), m_socket(sock)
{
	generate_name("OBJ");
}
//--------------------------------------
//
//--------------------------------------
sip_transport_object_t::~sip_transport_object_t()
{
	SLOG_NET("net-obj", "%s : --------> deleting object %u...", m_name, m_ref_count);
	
	// !!! ������� � �������� ������� ����������� ����������� !!!
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_object_t::stop_reading()
{
	m_readable = false;

	if (m_reader != nullptr)
	{
		m_reader->remove_object(this);
		m_reader = nullptr;
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_object_t::data_ready()
{
	// implemetations
}
//--------------------------------------
//
//--------------------------------------
long sip_transport_object_t::add_ref()
{
	return std_interlocked_inc(&m_ref_count);
}
//--------------------------------------
//
//--------------------------------------
long sip_transport_object_t::release_ref()
{
	long ref_count = std_interlocked_dec(&m_ref_count);

	if (ref_count == 0)
	{
		DELETEO(this);
	}

	return ref_count;
}
//--------------------------------------
//
//--------------------------------------
volatile int32_t sip_transport_reader_t::m_id_generator = 0;
//--------------------------------------
//
//--------------------------------------
sip_transport_reader_t::sip_transport_reader_t()
{
	m_first = m_last = nullptr;
	m_count = 0;
	m_thread_enter = false;

	m_id = std_interlocked_inc(&m_id_generator);
}
//--------------------------------------
//
//--------------------------------------
sip_transport_reader_t::~sip_transport_reader_t()
{
	if (m_read_list.getCount() > 0 || m_thread.isStarted())
		stop();
}
//--------------------------------------
//
//--------------------------------------
bool sip_transport_reader_t::start()
{
	SLOG_NET("net-rd", "%d : starting reader thread...", m_id);
	// ��� ���������? ������� � �������
	if (m_thread.isStarted())
		return true;

	if (!m_thread_wake_up_event.create(true, false))
	{
		SLOG_ERROR("net-rd", "%d : Start failed -- create event return %d", m_id, std_get_error);
		return false;
	}

	if (!m_thread_started_event.create(true, false))
	{
		SLOG_ERROR("net-rd", "%d : Start failed -- create start event return %d", m_id, std_get_error);
		m_thread_wake_up_event.close();
		return false;
	}

	//m_thread.set_name("net-listen");

	if (!m_thread.start(this))
	{
		SLOG_ERROR("net-rd", "%d : Start failed -- create thread return %d", m_id, std_get_error);
		m_thread_wake_up_event.close();
		m_thread_started_event.close();
		return false;
	}

	m_thread_started_event.wait(INFINITE);
	m_thread_started_event.close();

	SLOG_NET("net-rd", "%d : reader thread started", m_id);

	return true;
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_reader_t::stop()
{
	SLOG_NET("net-rd", "%d : stopping reader (%d)...", m_id, m_thread.get_id());

	m_thread.stop();

	rtl::MutexLock lock(m_sync);

	SLOG_NET("net-rd", "%d : stopping reader's objects...", m_id);

	sip_transport_object_t* current = (sip_transport_object_t*)m_first;

	while (current != nullptr)
	{
		current->m_readable = false;
		current->m_reader = nullptr;
		current->release_ref();
		current = current->m_next_link;
	}

	SLOG_NET("net-rd", "%d : cleaning reader...", m_id);

	m_first = m_last = nullptr;
	m_count = 0;

	m_thread_wake_up_event.close();
	m_thread_started_event.close();

	m_read_list.clear();

	SLOG_NET("net-rd", "%d : reader stopped", m_id);
}
//--------------------------------------
//
//--------------------------------------
bool sip_transport_reader_t::add_object(sip_transport_object_t* object)
{
	SLOG_NET("net-rd", "%d : add object %s...", m_id, object->get_id());

	if (m_count >= 64)
	{
		SLOG_NET("net-rd", "%d : adding object %s failed : object count >= 64", m_id, object->get_id(), m_count);
		return false;
	}

	rtl::MutexLock lock(m_sync);

	object->start_reading(this);

	if (m_last == nullptr)
	{
		m_first = m_last = object;
		
		if (!start())
		{
			SLOG_NET("net-rd", "%d : adding object %s failed : start() return false", m_id, object->get_id());
			return false;
		}
	}
	else
	{
		m_last->m_next_link = object; 
		m_last = object;
	}

	object->add_ref();
	m_count++;

	m_thread_wake_up_event.signal();

	SLOG_NET("net-rd", "%d : object %s added", m_id, object->get_id());

	return true;
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_reader_t::remove_object(sip_transport_object_t* object)
{
	SLOG_NET("net-rd", "%d : remove object %s...", m_id, object->get_id());

	rtl::MutexLock lock(m_sync);

	volatile sip_transport_object_t* current = m_first;
	volatile sip_transport_object_t* prev = nullptr;

	object->m_readable = false;

	// ���� ����
	while (current != nullptr && current != object)
	{
		prev = current;
		current = current->m_next_link;
	}

	if (current == nullptr)
	{
		SLOG_NET("net-rd", "%d : object %s not found", m_id, object->get_id());
		return;
	}

	// ���� ����� �� ������� �� ������
	if (prev != nullptr)
	{
		prev->m_next_link = current->m_next_link;

		// ���� ��� ��������� �� ������� ������ �� ����������
		if (current == m_last)
			m_last = prev;
	}
	else
	{
		m_first = current->m_next_link;

		// ���� ������������ �� ������ �� ���������� ���� ����������
		if (m_first == nullptr)
		{
			m_last = nullptr;
		}
	}

	object->m_reader = nullptr;
	object->m_next_link = nullptr;
	object->release_ref();

	m_count--;

	// ���� ��������� ������ �������

	uint32_t start_ticks = rtl::DateTime::getTicks();

	// ���� 1 ������� ����� ���� ��������
	while (m_thread_enter)
	{
		rtl::Thread::sleep(5);

		uint32_t delay = rtl::DateTime::getTicks() - start_ticks;

		if (delay > 1000)
		{
			break;
		}
	}

	SLOG_NET("net-rd", "%d : object %s removed", m_id, object->get_id());
}
//--------------------------------------
//
//--------------------------------------
bool sip_transport_reader_t::prepare_selection()
{
	m_thread_enter = true;

	// ������� ����� ��� ��������!
	sip_transport_object_t* current = (sip_transport_object_t*)m_first;
	
	while (current != nullptr)
	{
		if (current->m_readable)
		{
			current->add_ref();
			net_socket_t& sock = current->m_socket;
			m_read_list.add(&sock);
			m_captured_list.add(current);
		}
		current = current->m_next_link;
	}

	m_thread_enter = false;

	return m_read_list.getCount() > 0;
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_reader_t::process_read_sockets()
{
	try
	{
		sip_transport_object_t* current = nullptr;
		net_socket_t* sock = nullptr;
		//uint32_t current_time = rtl::DateTime::getTicks();

		// ����� �� ������ ������� � ��������
		for (int i = 0; i < m_read_list.getCount(); i++)
		{
			sock = m_read_list[i];
							
			if (sock != nullptr)
			{
				current = (sip_transport_object_t*)sock->get_tag();
				// ��������� ���������� ������
				if (current != nullptr)
				{
					if (&current->m_socket == sock && current->m_readable)
					{
						current->data_ready();
					}
				}
			}
		}

		m_read_list.clear();
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR("net-rd", "%d : processing selected read sockets failed with exception %s", m_id, ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

}
//--------------------------------------
//
//--------------------------------------
void sip_transport_reader_t::thread_run(rtl::Thread* thread)
{
	int result = 0;

	m_thread_started_event.signal();

	SLOG_NET("net-rd", "%d : reader thread run", m_id);

	try
	{
		do
		{
			m_thread_wake_up_event.wait(INFINITE);
			m_thread_wake_up_event.reset();

			while (!m_thread.get_stopping_flag() && prepare_selection())
			{
				// ������� ������ ��� ��������
				result = net_socket_t::select(m_read_list, 250);

				if (result > 0)
				{
					m_thread_enter = true;

					if (result > 0)
					{
						process_read_sockets();
					}

					m_thread_enter = false;
				}
				else if (result < 0)
				{
					int last_error = std_get_error;
					SLOG_ERR_MSG("net-rd", last_error, "%u : reader's select() function failed", m_id);
				}

				for (int i = 0; i < m_captured_list.getCount(); i++)
				{
					sip_transport_object_t* trn_obj = m_captured_list[i];
					trn_obj->release_ref();
				}

				m_captured_list.clear();
			}
		}
		while (!m_thread.get_stopping_flag());
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR("net-rd", "%d : reader thread failed with exception %s", m_id, ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	SLOG_NET("net-rd", "%d : reader thread exit", m_id);
}
//--------------------------------------
