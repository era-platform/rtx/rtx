/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "sip_stack.h"
#include "sip_user_agent.h"
#include "sip_transport.h"
//--------------------------------------
//
//--------------------------------------
const char* sip_event_get_type_name(int type)
{
	static const char* Names[] =
	{
		"message", "timer", "tx", "rx", "unknown_trn",
		"tx_error", "rx_error", "transport_disconnected", "transport_new_interface", "transport_failure", "final"
	};

	return type <= sip_stack_event_t::sip_event_final_e ? Names[type] : "unknown";
}
//--------------------------------------
//
//--------------------------------------
rtl::String sip_event_final_t::to_string()
{
	return rtl::String("'final' event");
}
//--------------------------------------
//
//--------------------------------------
rtl::String sip_event_message_t::to_string()
{
	rtl::String strm;
	strm << "'message' " << (m_message.is_request() ? "request " : "response ") << "event " << m_message.GetStartLine();
	return strm;
}
//--------------------------------------
//
//--------------------------------------
rtl::String sip_event_unknown_transaction_t::to_string()
{
	return rtl::String("'unknow trn' event") + m_message.get_transaction();
}
//--------------------------------------
//
//--------------------------------------
rtl::String sip_event_timer_expires_t::to_string()
{
	rtl::String strm;
	strm << "'timer' expired event " << sip_timer_manager_t::GetTimerName(m_timer_type);
	return strm;
}
//--------------------------------------
//
//--------------------------------------
sip_event_tx_t::~sip_event_tx_t()
{
	m_message.cleanup();
}
rtl::String sip_event_tx_t::to_string()
{
	return rtl::String("'tx' event ") + m_message.GetStartLine();
}
//--------------------------------------
//
//--------------------------------------
rtl::String sip_event_tx_error_t::to_string()
{
	rtl::String strm;
	strm << "'tx error' event (" << m_net_error << ") " << m_message.GetStartLine();
	return strm;
}
//--------------------------------------
//
//--------------------------------------
rtl::String sip_event_rx_t::to_string()
{
	return rtl::String("'rx' event ") + m_message.GetStartLine();
}
//--------------------------------------
//
//--------------------------------------
rtl::String sip_event_rx_error_t::to_string()
{
	static const char* err_names[] = { "address", "user", "domain", "password", "parser" };
	rtl::String strm;
	strm << "rx error' event (" << err_names[m_error_type] << ") from " << inet_ntoa(m_recv_address) << ':' << m_recv_port << ' ' << m_message.GetStartLine();
	return strm;
}
//--------------------------------------
//
//--------------------------------------
rtl::String sip_event_transport_disconnected_t::to_string()
{
	char tmp[128];
	rtl::String strm;
	strm << "'transport disconnected' event route: " << route_to_string(tmp, 128, m_route);
	return strm;
}
//--------------------------------------
//
//--------------------------------------
rtl::String sip_event_transport_new_interface_t::to_string()
{
	rtl::String strm;
	char tmp[128];
	strm << "'transport new interace' event point: " << point_to_string(tmp, 128, m_point);
	return strm;
}
//--------------------------------------
//
//--------------------------------------
rtl::String sip_event_transport_failure_t::to_string()
{
	rtl::String strm;
	char tmp[128];
	strm << "'transport failure' event (" << m_net_error << ") route: " << route_to_string(tmp, 128, m_route);
	return strm;
}
//--------------------------------------
//
//--------------------------------------
sip_stack_t::sip_stack_t(sip_user_agent_t& ua) : m_ua(ua)//, m_read_queue_lock("sip-stack"), m_queue_counter(0)
{
//	m_read_queue_sync.create(0, INT_MAX);
	sip_transport_manager_t::create(this);
}
//--------------------------------------
//
//--------------------------------------
sip_stack_t::~sip_stack_t()
{
}
void sip_stack_t::initialize_stack(int work_threads)
{
	initialize(work_threads);
}
//--------------------------------------
//
//--------------------------------------
void sip_stack_t::destroy_stack()
{

	SLOG_CALL("STACK", "Destroying...");

	force_terminate_transactions();

	SLOG_CALL("STACK", "Stopping transport...");
	sip_transport_manager_t::destroy();
	SLOG_CALL("STACK", "Transport stopped...");

	for (int i = 0; i < m_check_domain_list.getCount(); i++)
	{
		if (m_check_domain_list[i] != nullptr)
			FREE(m_check_domain_list[i]);
	}

	m_check_domain_list.clear();

	for (int i = 0; i < m_check_user_list.getCount(); i++)
	{
		if (m_check_user_list[i] != nullptr)
			FREE(m_check_user_list[i]);
	}
	m_check_user_list.clear();

	for (int i = 0; i < m_banned_user_agent_list.getCount(); i++)
	{
		if (m_banned_user_agent_list[i] != nullptr)
			FREE(m_banned_user_agent_list[i]);
	}
	
	m_banned_user_agent_list.clear();

	SLOG_CALL("STACK", "Destroyed");
}
//--------------------------------------
//
//--------------------------------------
//void sip_stack_t::flush_event_queue(int signalCount)
//{
//	if (!m_terminating)
//	{
//		SLOG_CALL("STACK", "Terminating %d...", signalCount);
//		m_terminating = true;
//
//		if (signalCount > 0)
//		{
//			for (int i = 0; i < signalCount; i++)
//			{
//				push_sip_stack_event(NEW sip_event_final_t());
//			}
//		}
//
//		force_terminate_transactions();
//	}
//}
//--------------------------------------
//
//--------------------------------------
bool sip_stack_t::add_transport(const sip_transport_point_t& iface)
{
	return sip_transport_manager_t::start_listener(iface);
}
//--------------------------------------
//
//--------------------------------------
bool sip_stack_t::open_route(sip_transport_route_t& route, const char* user)
{
	return sip_transport_manager_t::open_route(route, user);
}
//--------------------------------------
//
//--------------------------------------
bool sip_stack_t::set_route_keep_alive_timer(sip_transport_route_t& route, int keep_alive_value)
{
	return sip_transport_manager_t::set_route_keep_alive_timer(route, keep_alive_value);
}
//--------------------------------------
//
//--------------------------------------
void sip_stack_t::release_route(const sip_transport_route_t& route, const char* user)
{
	sip_transport_manager_t::release_route(route, user);
}
//--------------------------------------
//
//--------------------------------------
void sip_stack_t::set_discard_subscribes(bool enable)
{
	sip_transport_manager_t::set_discard_subscribes(enable);
}
//--------------------------------------
// pure virtuals from sip_transaction_manager_t
//--------------------------------------
void sip_stack_t::process_received_message_event(const sip_message_t& msg, sip_transaction_t& transaction)
{
	if (m_terminating)
		return;

	SLOG_CALL("STACK", "call-id: %s --> enqueue income message %s", (const char*)msg.CallId(), (const char*)msg.GetStartLine());

	sip_message_t message = msg;

	message.SetUACoreName(transaction.get_core_name());

	push_sip_stack_event(NEW sip_event_message_t(message, transaction.get_identifier()));
}
//--------------------------------------
//
//--------------------------------------
void sip_stack_t::send_message_to_transport(const sip_message_t& msg, sip_transaction_t& transaction)
{
	if (m_terminating)
		return;

	sip_message_t message = msg;
	//SIPTransportEvent::Type eventType = SIPTransportEvent::UDPPacketSend;

	in_addr if_address = msg.get_interface_address();
	uint16_t if_port = msg.get_interface_port();

	message.set_transaction(transaction.get_identifier_string());
	rtl::String trn_id = message.get_trans_id().AsString();

	SLOG_CALL("STACK", "call-id '%s' --> push message '%s' to transport queue : address'%s:%u' trn-id:%s",
		(const char*)msg.CallId(), (const char*)msg.GetStartLine(), inet_ntoa(if_address), if_port, (const char*)trn_id/*message.get_transaction()*/);

	send_message_to_transport(message);
}
//--------------------------------------
//
//--------------------------------------
void sip_stack_t::send_message_to_transport(const sip_message_t& message)
{
	if (m_terminating)
		return;

	sip_transport_manager_t::send_message_async(message);
}
//--------------------------------------
//
//--------------------------------------
void sip_stack_t::process_timer_expire_event(sip_transaction_timer_t timer, sip_transaction_t& transaction)
{
	if (m_terminating)
		return;

	push_sip_stack_event(NEW sip_event_timer_expires_t(timer, transaction.get_identifier()));
}
//--------------------------------------
//
//--------------------------------------
void sip_stack_t::process_unknown_transaction_event(const sip_message_t& message, bool fromTransport)
{
	if (!fromTransport || m_terminating)
		return;

	trans_id tid(message);

	push_sip_stack_event(NEW sip_event_unknown_transaction_t(message, tid, fromTransport));
}
//--------------------------------------
//
//--------------------------------------
void sip_stack_t::sip_transport_man_packet_arrival(sip_message_t* message)
{
	if (m_terminating)
	{
		DELETEO(message);
		return;
	}

	SLOG_EVENT("STACK", "sip message arrival event...");

	{
//		rtl::MutexWatchLock lock(m_read_queue_lock, __FUNCTION__);

		trans_id trnId;

		if (message->is_request())
			message->FixViaNATParameters();

		SLOG_SESS("STACK", "sip message arrival event -- pass to transaction manager");

		find_transaction_and_add_event(*message, trnId, true);
	}

	DELETEO(message);

	SLOG_EVENT("STACK", "sip message arrival event -- handled");
}
//--------------------------------------
//
//--------------------------------------
void sip_stack_t::sip_transport_man_packet_sent(const sip_message_t& message)
{
	if (m_terminating)
		return;

	if (message.IsKeepAlive())
		return;

	trans_id trnId = message.get_transaction();
	sip_event_tx_t* sip_event = NEW sip_event_tx_t(message, trnId);

	SLOG_CALL("STACK", "SIP Packet sent %s : %s", (const char*)message.GetStartLine(), (const char*)message.get_transaction());

	if (message.IsAck())
	{
		SLOG_CALL("STACK", "PACKET SENT\r\nmessage  : %s\r\nsip event:%s\r\ntrans_id :%s",
			(const char*)message.CallId(),
			(const char*)sip_event->get_call_id(),
			(const char*)trnId.GetCallId());
	}

	//rtl::MutexWatchLock lock(m_read_queue_lock, __FUNCTION__);

	push_sip_stack_event(sip_event);
}
//--------------------------------------
//
//--------------------------------------
void sip_stack_t::sip_transport_man_send_error(const sip_message_t& message, int net_error)
{
	if (m_terminating)
		return;

	trans_id trnId = message.get_transaction();

	push_sip_stack_event(NEW sip_event_tx_error_t(message, trnId, net_error));
}
//--------------------------------------
//
//--------------------------------------
void sip_stack_t::sip_transport_man_recv_error(const sip_transport_route_t& route, int net_error)
{
	push_sip_stack_event(NEW sip_event_transport_failure_t(route, net_error));
}
//--------------------------------------
//
//--------------------------------------
void sip_stack_t::sip_transport_man_disconnected(const sip_transport_route_t& route)
{
	//m_ua->get_registrar().process_transport_disconnected(route);
	push_sip_stack_event(NEW sip_event_transport_disconnected_t(route));
}
//--------------------------------------
//
//--------------------------------------
void sip_stack_t::sip_transport_man_new_interface(const sip_transport_point_t& point)
{
	//m_ua->get_registrar().process_transport_disconnected(route);
	push_sip_stack_event(NEW sip_event_transport_new_interface_t(point));
}
//--------------------------------------
//
//--------------------------------------
bool sip_stack_t::sip_transport_man_check_account(in_addr remoteAddress, uint16_t remotePort, const sip_message_t& message)
{
	if (m_terminating || !m_started)
	{
		SLOG_WARN("STACK", ".....NOT STARTED.....");

		return false;
	}
	
	if (message.is_request())
	{
		if (message.IsRegister())
		{
			sip_event_rx_error_t::rx_error_t errorType = sip_event_rx_error_t::rx_error_unknown; 
			// ����� ������ ��������
			if (filter_account(message, errorType))
			{
				// ���������� � ������������ �����������
				push_sip_stack_event(NEW sip_event_rx_error_t(message, remoteAddress, remotePort, errorType));
				
				SLOG_WARN("STACK", "call-id %s --> FOUND INVALID REGISTER REQUEST", (const char*)message.CallId());
				return false;
			}
		}
		// ����� ��� ����������� �� REGISTER ������
	}
	// ������ ���� ����� �����������

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool sip_stack_t::sip_transport_man_is_address_banned(in_addr remoteAddress)
{
	if (m_terminating/* || !m_started*/)
		return true;
	
	// ������ �������� � ������� ����������� �������
	return filter_ip_address(remoteAddress);
}
//--------------------------------------
//
//--------------------------------------
bool sip_stack_t::sip_transport_man_is_useragent_banned(const char* useragent)
{
	if (m_terminating/* || !m_started*/)
		return true;
	
	// ������ �������� � ������� ����������� �������
	return filter_user_agent(useragent);
}
//--------------------------------------
//
//--------------------------------------
void sip_stack_t::add_to_banned_list(const char* address)
{
	if (address == nullptr || address[0] == 0)
		return;

	if (std_strnicmp(address, "ua:", 3) == 0)
	{
		// ������ ����������� �������
		ban_user_agent(address+3);
	}
	else
	{
		// ������ ����������� ip �������
		in_addr addr = net_parse(address);
		if (addr.s_addr != INADDR_NONE)
		{
			ban_ip_address(addr);
		}
		else
		{
			SLOG_CALL("STACK", "ADDRESS %s IS NOT VALID ADDRESS -- NOT BANNED!", address);
		}
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_stack_t::remove_from_banned_list(const char* address)
{
	if (address == nullptr || address[0] == 0)
		return;

	if (std_strnicmp(address, "ua:", 3) == 0)
	{
		// ������ ����������� �������
		unban_user_agent(address + 3);
	}
	else
	{
		in_addr addr = net_parse(address);
		if (addr.s_addr != INADDR_NONE)
		{
			unban_ip_address(addr);
		}
		else
		{
			SLOG_CALL("STACK", "ADDRESS %s IS NOT VALID ADDRESS -- NOT UNBANNED!", address);
		}
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_stack_t::add_check_domain(const char* domain)
{
	if (domain == nullptr || domain[0] == 0)
		return;

	bool found = false;

	{
		rtl::MutexLock lock(m_check_lock);

		for (int i = 0; i < m_check_domain_list.getCount(); i++)
		{
			if (found = (std_stricmp(domain, m_check_domain_list[i]) == 0))
			{
				// already banned
				break;
			}
		}

		if (!found)
		{
			char* ptr = rtl::strdup(domain);
			m_check_domain_list.add(ptr);
		}
	}

	SLOG_CALL("STACK", "CHECK DOMAIN '%s' %s!", domain, found ? "ALREADY EXIST" : "ADDED");
}
//--------------------------------------
//
//--------------------------------------
void sip_stack_t::add_check_user(const char* username)
{
	if (username == nullptr || username[0] == 0)
		return;

	bool found = false;

	{
		rtl::MutexLock lock(m_check_lock);

		for (int i = 0; i < m_check_user_list.getCount(); i++)
		{
			if (found = (std_stricmp(username, m_check_user_list[i]) == 0))
			{
				break;
			}
		}
			
		if (!found)
		{
			char* ptr = rtl::strdup(username);
			m_check_user_list.add(ptr);
		}
	}
	
	SLOG_CALL("STACK", "CHECK USER '%s' %s", username, found ? "ALREADY EXIST" : "ADDED");
}
//--------------------------------------
//
//--------------------------------------
void sip_stack_t::ban_ip_address(in_addr address)
{
	bool found = false;

	{
		rtl::MutexLock lock(m_banned_lock);

		for (int i = 0; i < m_banned_ip_list.getCount(); i++)
		{
			if (found = (address.s_addr == m_banned_ip_list[i].s_addr))
			{
				break;
			}
		}

		if (!found)
			m_banned_ip_list.add(address);
	}
	
	SLOG_CALL("STACK", "ADDRESS %s %s!", inet_ntoa(address), found ? "ALREADY BANNED" : "BANNED");
}
//--------------------------------------
//
//--------------------------------------
void sip_stack_t::unban_ip_address(in_addr address)
{
	bool found = false;

	{
		rtl::MutexLock lock(m_banned_lock);
		int i = 0;

		for (; i < m_banned_ip_list.getCount(); i++)
		{
			if (found = (m_banned_ip_list[i].s_addr == address.s_addr))
			{
				break;
			}
		}

		if (found)
		{
			m_banned_ip_list.removeAt(i);
		}
	}

	SLOG_CALL("STACK", "ADDRESS %s %s!", inet_ntoa(address), found ? "UNBANNED" : "NOT FOUND -- IT IS NOT BANNED!");
}
//--------------------------------------
//
//--------------------------------------
void sip_stack_t::ban_user_agent(const char* useragent)
{
	if (useragent == nullptr || useragent[0] == 0)
		return;

	bool found = false;
	
	{
		rtl::MutexLock lock(m_banned_lock);

		for (int i = 0; i < m_banned_user_agent_list.getCount(); i++)
		{
			if (found = (std_stricmp(useragent, m_banned_user_agent_list[i]) == 0))
			{
				break;
			}
		}

		if (!found)
		{
			char* ptr = rtl::strdup(useragent);
			m_banned_user_agent_list.add(ptr);
		}
	}

	SLOG_CALL("STACK", "USER-AGENT '%s' %s!", useragent, found ? "ALREADY BANNED" : "BANNED");
}
//--------------------------------------
//
//--------------------------------------
void sip_stack_t::unban_user_agent(const char* useragent)
{
	bool found = false;

	{
		rtl::MutexLock lock(m_banned_lock);
		int i = 0;

		for (; i < m_banned_user_agent_list.getCount(); i++)
		{
			if (found = (std_stricmp(m_banned_user_agent_list[i], useragent) == 0))
			{
				break;
			}
		}

		if (found)
		{
			FREE(m_banned_user_agent_list[i]);
			m_banned_user_agent_list.removeAt(i);
		}
	}
	
	SLOG_CALL("STACK", "USER-AGENT '%s' %s!", useragent, found ? "UNBANNED" : "NOT FOUND -- IT IS NOT BANNED!");
}
//--------------------------------------
//
//--------------------------------------
bool sip_stack_t::filter_ip_address(in_addr remoteAddress)
{
	rtl::MutexLock lock(m_banned_lock);
	
	for (int i = 0; i < m_banned_ip_list.getCount(); i++)
	{
		if (m_banned_ip_list[i].s_addr == remoteAddress.s_addr)
		{
			return true;
		}
	}
	
	return false;
}
//--------------------------------------
//
//--------------------------------------
bool sip_stack_t::filter_user_agent(const char* useragent)
{
	rtl::MutexLock lock(m_banned_lock);
	
	for (int i = 0; i < m_banned_user_agent_list.getCount(); i++)
	{
		const char* to_filter = m_banned_user_agent_list[i];
		int len = (int)strlen(to_filter);
		
		if (std_strnicmp(useragent, to_filter, len) == 0)
		{
			return true;
		}
	}

	return false;
}
//--------------------------------------
//
//--------------------------------------
bool sip_stack_t::filter_account(const sip_message_t& message, sip_event_rx_error_t::rx_error_t& type)
{
	const sip_uri_t& requestURI = message.get_request_uri();
	const sip_uri_t& toURI = message.To_URI();
	const sip_uri_t& fromURI = message.From_URI();
	
	type = sip_event_rx_error_t::rx_error_unknown;

	if (requestURI.get_host().isEmpty() || (toURI.get_user().isEmpty() && fromURI.get_user().isEmpty()))
		return true;

	bool domainFound = false;
	bool userFound = false;

	const char* to_username = toURI.get_user();
//	const char* from_username = fromURI.get_user();
	const char* request_domain = requestURI.get_host();

	rtl::MutexLock lock(m_check_lock);

	try
	{
		// �������� ������
		if (m_check_domain_list.getCount() == 0 || m_check_user_list.getCount() == 0)
		{
			userFound = false;
			goto the_end;
		}

		for (int i = 0; i < m_check_domain_list.getCount(); i++)
		{
			if (std_stricmp(request_domain, m_check_domain_list[i]) == 0)
			{
				domainFound = true;
				break;
			}
		}

		if (!domainFound)
		{
			type = sip_event_rx_error_t::rx_error_domain_e;
			userFound = false;
			goto the_end;
		}

		// ������ ��������� -- �������� �������������

		const char* user = toURI.get_user();
		
		if (user == nullptr || user[0] == 0)
		{
			user = fromURI.get_user();
		}

		if (user == nullptr || user[0] == 0)
		{
			type = sip_event_rx_error_t::rx_error_user_e;
			userFound = false;
			goto the_end;
		}

		for (int i = 0; i < m_check_user_list.getCount(); i++)
		{
			if (std_stricmp(user, m_check_user_list[i]) == 0)
			{
				userFound = true;
				break;
			}
		}

		if (!userFound)
		{
			type = sip_event_rx_error_t::rx_error_user_e;
		}
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR("STACK", "Filter Account failed with exception %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

the_end:

	SLOG_CALL("STACK", "User %s@%s %s", to_username, request_domain, userFound? "Found" : "Not Found");

	return !userFound;
}
//--------------------------------------
//
//--------------------------------------
void sip_stack_t::push_sip_stack_event(sip_stack_event_t* sip_event)
{
//	rtl::MutexWatchLock lock(m_read_queue_lock, __FUNCTION__);

//	long cnt = InterlockedIncrement(&m_queue_counter);

	//SLOG_CALL("STACK", "=====> Enqueue (i:%d c:%d h:%d t:%d) event %s call-id '%s'",
	//	cnt, m_read_queue.getCount(), m_read_queue.getHead(), m_read_queue.getTail(), sip_event->get_type_name(), (const char*)sip_event->get_call_id());

	rtl::async_call_manager_t::callAsync(&m_ua, 1001, 0, sip_event);
	//m_read_queue.push(sip_event);
	

//	m_read_queue_sync.signal();
}
//--------------------------------------
//
//--------------------------------------
//sip_stack_event_t* sip_stack_t::get_sip_stack_event()
//{
//	m_read_queue_sync.wait(INFINITE);
//
//	sip_stack_event_t* sip_event = nullptr;
//
//	rtl::MutexWatchLock lock(m_read_queue_lock, __FUNCTION__);
//
//	long cnt = InterlockedDecrement(&m_queue_counter);
//	sip_event = m_read_queue.pop();
//
//	if (sip_event != nullptr)
//	{
//		SLOG_CALL("STACK", "=====> Dequeue (i:%d c:%d h:%d t:%d) event %s call-id '%s'",
//			cnt, m_read_queue.getCount(), m_read_queue.getHead(), m_read_queue.getTail(), sip_event->get_type_name(), (const char*)sip_event->get_call_id());
//	}
//	else
//	{
//		SLOG_WARN("STACK", "=====> nullptr object dequeued");
//	}
//
//	return sip_event;
//}
//--------------------------------------
