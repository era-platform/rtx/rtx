/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_transport_listener.h"
#include "sip_transport_pool.h"
#include "sip_transport_udp.h"
#include "sip_transport_tcp.h"
//--------------------------------------
// ����� �� ���������� ������ 64*T1 = 32 �������, �� �������� �� ������
//--------------------------------------
#define SIP_TRANSPORT_LIFESPAN (1000 * 60)
//---
#define ASYNC_REMOVE_TRANSPORT		0
#define ASYNC_DESTROY_TRANSPORT		1
//--------------------------------------
//
//--------------------------------------
sip_transport_listener_t::sip_transport_listener_t(net_socket_t& sock) :
	sip_transport_object_t(sock)
{
	m_if.type = sip_transport_udp_e;
	m_if.address.s_addr = INADDR_ANY;
	m_if.port = 0;
}
//--------------------------------------
//
//--------------------------------------
sip_transport_listener_t::~sip_transport_listener_t()
{
	stop_listen();
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_listener_t::destroy()
{
	SLOG_NET("net-l", "%s(%d) : destroying...", m_name, get_ref_count());

	sip_transport_manager_t::destroy_sip_transports(m_if);

	stop_listen();

	SLOG_NET("net-l", "%s(%d) : destroyed", m_name, get_ref_count());
}
//--------------------------------------
//
//--------------------------------------
void sip_transport_listener_t::stop_listen(bool failure)
{
	SLOG_NET("net-l", "%s(%d) : stopping listener...", m_name, get_ref_count());

	if (m_reader != nullptr)
	{
		SLOG_NET("net-l", "%s(%d) : detaching from reader", m_name, get_ref_count());
		m_reader->remove_object(this);
		m_reader = nullptr;
	}

	if (failure)
	{
		SLOG_NET("net-l", "listener raise failure event");
		sip_transport_route_t route = { m_if.type, m_if.address, m_if.port, 0, 0 };
		sip_transport_manager_t::sip_listener_transport_failure(this, 0, route); // WSAENETDOWN
	}

	SLOG_NET("net-l", "listener stopped");
}
//--------------------------------------
