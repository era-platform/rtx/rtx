/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_reg_session_client.h"
//--------------------------------------
//
//--------------------------------------
sip_reg_session_manager_t::sip_reg_session_manager_t(sip_user_agent_t& ua, int sessionThreadCount) :
	sip_session_manager_t(ua, sessionThreadCount, "Registrar")
{
	m_log_tag = "REGM";
}
//--------------------------------------
//
//--------------------------------------
sip_reg_session_manager_t::~sip_reg_session_manager_t()
{
	//critical_section_lock_t lock(m_reg_accounts_sync);
	//m_reg_accounts_list.clear();
}
//--------------------------------------
//
//--------------------------------------
void sip_reg_session_manager_t::stop()
{
	SLOG_SESS(m_log_tag, "sip_reg_session_manager_t::Stop");

	sip_session_manager_t::stop();
	
	//critical_section_lock_t lock(m_reg_accounts_sync);
	//m_reg_accounts_list.clear();
}
//--------------------------------------
//
//--------------------------------------
void sip_reg_session_manager_t::process_incoming_message(sip_event_message_t& messageEvent, sip_session_t* session)
{
	const sip_message_t& msg = messageEvent.get_message();

	// ��������� ����� �������
	if (msg.is_request())
	{
		// respond back with bad request
		SLOG_WARN(m_log_tag, "Registrar received non register request! %s", (const char*)msg.GetStartLine());
		sip_message_t response;
		msg.make_response(response, sip_400_BadRequest);
		send_message_to_transport(response);
		return;
	}
	else
	{
		SLOG_SESS(m_log_tag, "process_incoming_message %s", (ZString)msg.GetStartLine());
		
		try
		{
			rtl::String callId = msg.CallId();
			sip_reg_session_client_t* regSession = (sip_reg_session_client_t*)find_session_by_call_id(callId);
		
			if (regSession != NULL)
				regSession->process_response(msg);
		}
		catch (rtl::Exception& ex)
		{
			SLOG_ERROR(m_log_tag, "Incoming SIP Message failed with exception %s", ex.get_message());
			ex.raise_notify(__FILE__, __FUNCTION__);
		}
	}

	/*if (msg.IsSubscribe() || msg.GetCSeqMethod() == "SUBSCRIBE")
	{
		if (msg.IsRequest())
		{
			sip_session_manager_t::process_incoming_message(messageEvent, session);
		}
		else
		{
			sip_reg_session_client_t* regSession = (sip_reg_session_client_t*)find_session_by_call_id(msg.GetCallId());
			if (regSession != NULL)
				regSession->process_response(msg);
		}
	}
	else if (msg.IsNotify() || msg.GetCSeqMethod() == "NOTIFY")
	{
		if (msg.IsRequest())
		{
			sip_session_manager_t::process_incoming_message(messageEvent, session);
		}
		else if (session != NULL)
		{
			sip_reg_session_client_t* regSession = (sip_reg_session_client_t*)find_session_by_call_id(msg.GetCallId());
			if (regSession != NULL)
				regSession->process_response(msg);
		}
	}
	else if (!msg.IsUnregister())
	{
		process_register_message(messageEvent, session);
	}
	else if (msg.IsUnregister())
	{
		process_unregister_message(messageEvent, session);
	}*/
}
//--------------------------------------
// returns a NEW sip_reg_session_client_t.  Users may override this function 
// to return their own implementation of the sip_reg_session_client_t object
//--------------------------------------
sip_session_t* sip_reg_session_manager_t::create_new_client_session(const sip_profile_t& profile, const char* callId)
{
	SLOG_SESS(m_log_tag, "Create Register Client Session Call-Id: %s", callId);

	sip_reg_session_client_t* regSession = NULL;

	try
	{
		regSession = NEW sip_reg_session_client_t(*this, profile, callId);
		SLOG_SESS(m_log_tag, "Create Client Session for %s@%s --> Call-Id: %s", (const char*)profile.get_username(), (const char*)profile.get_domain(), callId);
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR(m_log_tag, "Create Client Session failed with exception %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	return regSession;
}
//--------------------------------------
//
//--------------------------------------
sip_session_t* sip_reg_session_manager_t::create_new_server_session(sip_message_t& request)
{
	return nullptr;

	/*SLOG_SESS(m_log_tag, "Create Register Server Session %s", (ZString)request.GetStartLine());

	if (!request.IsRegister())
		return NULL;

	sip_reg_session_event_handler_t* reg_event_handler = find_account(request);
	
	if (reg_event_handler == NULL)
		return NULL;

	sip_reg_session_client_t* regSession = NULL;

	To to;
	if (!request.GetTo(to))
	{
		SLOG_ERROR(m_log_tag, "Create register session failed -- request has no To header!");
		return NULL;
	}

	rtl::String username = to.GetURI().GetUser();

	try
	{
		regSession = NEW sip_reg_session_client_t(*this, request);
		regSession->set_event_handler(reg_event_handler);
		
		critical_section_lock_t lock(m_reg_accounts_sync);
		sip_reg_session_client_t* oldSession = (sip_reg_session_client_t*)m_reg_accounts_list.find(username);

		if (oldSession != NULL)
		{
			SLOG_SESS(m_log_tag, "Found old session for %s", (const char*)username);
			oldSession->destroy_session();
		}

		SLOG_SESS(m_log_tag, "RegisterSessionsByURI.Add(%p) URI ID %s -- %s", regSession, regSession->get_call_id(), (const char*)username);
		m_reg_accounts_list.add(regSession, username);
		
	}
	catch (Exception& ex)
	{
		SLOG_ERROR(m_log_tag, "Create Server Session failed with exception %s", ex.Message());
		ex.Notify(__FILE__, __FUNCTION__);
	}

	return regSession;*/
}
//--------------------------------------
//
//--------------------------------------
//void sip_reg_session_manager_t::remove_session_by_username(const char* username)
//{
//	SLOG_SESS(m_log_tag, "RegisterSessionsByURI.Remove by name %s", username);
//
//	critical_section_lock_t lock(m_reg_accounts_sync);
//	m_reg_accounts_list.remove(username);
//}
//--------------------------------------
//
//--------------------------------------
//void sip_reg_session_manager_t::process_register_message(sip_event_message_t& messageEvent, sip_session_t* session)
//{
//	const sip_message_t& msg = messageEvent.get_message();
//
//	/// check if the message is a 401 authentication required
//	if (!msg.IsRequest())
//	{
//		SLOG_SESS(m_log_tag, "process_incoming_message %s", (ZString)msg.GetStartLine());
//
//		try
//		{
//			rtl::String callId = msg.GetCallId();
//			sip_reg_session_client_t* regSession = (sip_reg_session_client_t*)find_session_by_call_id(callId);
//
//			if (regSession != NULL)
//				regSession->process_response(msg);
//		}
//		catch (Exception& ex)
//		{
//			SLOG_ERROR(m_log_tag, "Incoming SIP Message failed with exception %s", ex.Message());
//			ex.Notify(__FILE__, __FUNCTION__);
//		}
//	}
//	else
//	{
//		sip_session_manager_t::process_incoming_message(messageEvent, session);
//	}
//}
//--------------------------------------
//
//--------------------------------------
//void sip_reg_session_manager_t::process_unregister_message(sip_event_message_t& messageEvent, sip_session_t* session)
//{
//	const sip_message_t& msg = messageEvent.get_message();
//
//	/// this is an UnRegister
//	SLOG_SESS(m_log_tag, "process_incoming_message UNREGISTER %s", (ZString)msg.GetStartLine());
//		
//	try
//	{
//		rtl::String callId = msg.GetCallId();
//
//		sip_reg_session_client_t* sessionByURI = (sip_reg_session_client_t*)find_session_by_call_id(callId);
//
//		if (msg.IsRequest())
//		{
//			// UAS
//			if (sessionByURI != NULL)
//			{
//				sip_message_t ok;
//				msg.CreateResponse(ok, sip_message_t::Code200_Ok, "");
//				Expires expires(0);
//				ok.SetExpires(expires);
//				ContactList c = msg.GetContactList();
//				ok.SetContactList(c);
//				sessionByURI->push_message_to_transaction(ok);
//				sessionByURI->process_unregistration(messageEvent.get_message());
//			}
//			else
//			{
//				/// just send an OK
//				SLOG_WARN(m_log_tag, "Unregister without a session present %s", (ZString)msg.GetStartLine());
//				sip_message_t ok;
//				msg.CreateResponse(ok, sip_message_t::Code200_Ok);
//				TransactionId tid;
//				send_message_to_transport(ok);
//			}
//		}
//		else
//		{
//			// UAC
//			if (sessionByURI != NULL)
//				sessionByURI->process_response(msg);
//			else
//			{
//				// Check if we have a session by call-id
//				sessionByURI = (sip_reg_session_client_t*)find_session_by_call_id(msg.GetCallId());
//
//				if (sessionByURI != NULL)
//					sessionByURI->process_response(msg);
//			}
//		}
//	}
//	catch (Exception& ex)
//	{
//		SLOG_ERROR(m_log_tag, "Incoming SIP Message failed with exception %s", ex.Message());
//		ex.Notify(__FILE__, __FUNCTION__);
//	}
//}
//--------------------------------------
