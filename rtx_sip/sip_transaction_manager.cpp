/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "sip_transaction_manager.h"
#include "sip_transaction_stage.h"
#include "sip_transport_manager.h"
//-----------------------------------------------
//
//-----------------------------------------------
sip_transaction_manager_t::sip_transaction_manager_t() : m_pool_lock("trn-pool-lock"), m_ack_pool_lock("trn-ack-pool-lock")
{
	m_started = false;
	m_terminating = false;
}
//-----------------------------------------------
//
//-----------------------------------------------
sip_transaction_manager_t::~sip_transaction_manager_t()
{
	if (m_stage != nullptr)
	{
		SLOG_TRANS("TRANSM", "Stop Transactions Stage...");
		m_stage->stop();
		DELETEO(m_stage);
		m_stage = nullptr;
		SLOG_TRANS("TRANSM", "Transactions Stage stoped");
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
bool sip_transaction_manager_t::initialize(int work_threads)
{
	m_stage = NEW sip_transaction_stage_t(work_threads);

	m_stage->start();

	sip_timer_manager_t::start_timer_manager();

	SLOG_TRANS("TRANSM", "Transaction Manager started!");

	return m_started = true;
}
//-----------------------------------------------
//
//-----------------------------------------------
void sip_transaction_manager_t::destroy()
{
	force_terminate_transactions();
}
//-----------------------------------------------
//
//-----------------------------------------------
void sip_transaction_manager_t::force_terminate_transactions()
{
	SLOG_TRANS("TRANSM", "Force Terminating Transactions...");

	sip_timer_manager_t::stop_timer_manager();
	
	if (m_stage != nullptr)
	{
		SLOG_TRANS("TRANSM", "Stop Transactions Stage...");
		m_stage->stop();
		DELETEO(m_stage);
		m_stage = nullptr;
		SLOG_TRANS("TRANSM", "Transactions Stage stoped");
	}

	{
		rtl::MutexWatchLock lock(m_pool_lock, __FUNCTION__);

		SLOG_TRANS("TRANSM", "Cleanup transactions pool %d elelents...", m_pool.getCount());

		for (int i = m_pool.getCount()-1; i >= 0; i--)
		{
			sip_transaction_t* trn = m_pool.getAt(i);
			/// process pending events if any
			SLOG_TRANS("TRANSM", "Terminate Transaction %s", trn->get_iid());
			trn->process_events();
			trn->process_sip_transaction_final_event();
			/// allow descendants to cleanup before deletion
			DELETEO(trn);
		}
		
		m_pool.clear();

		SLOG_TRANS("TRANSM", "Transactions pool cleaned");
	}

	{
		rtl::MutexWatchLock lock(m_ack_pool_lock, __FUNCTION__);

		SLOG_TRANS("TRANSM", "Cleanup ACK transactions pool %d elements...", m_ack_pool.getCount());

		for (int i = m_ack_pool.getCount()-1; i >= 0; i--)
		{
			sip_transaction_t* trn = m_ack_pool.getAt(i);
			/// process pending events if any
			SLOG_TRANS("TRANSM", "Terminate ACK Transaction %s", trn->get_iid());
			trn->process_events();
			trn->process_sip_transaction_final_event();
			/// allow descendants to cleanup before deletion
			DELETEO(trn);
		}
		
		m_ack_pool.clear();

		SLOG_TRANS("TRANSM", "Transactions ACK pool cleaned");
	}

	SLOG_TRANS("TRANSM", "Transactions Terminated");

	m_started = false;
}
//--------------------------------------
//
//--------------------------------------
bool sip_transaction_manager_t::create_transaction(const sip_message_t& trnEvent, bool fromTransport)
{
	SLOG_TRANS("TRANSM", "Create Transaction : message %s", (const char*)trnEvent.GetStartLine());

	if (m_terminating)
	{
		SLOG_WARN("TRANSM", "Create Transaction : In terminating state.");
		return false;
	}

	rtl::String idPrefix;
	sip_transaction_type_t type;


	if (fromTransport)
	{
		///type = sip_transaction_t::Server;
		if (trnEvent.IsInvite())
		{
			type = sip_transaction_ist_e;
			idPrefix = "IST";
		}
		else
		{
			type = sip_transaction_nist_e;
			idPrefix = "NIST";
		}
	}
	else
	{
		if (trnEvent.IsInvite())
		{
			type = sip_transaction_ict_e;
			idPrefix = "ICT";
		}
		else
		{
			type = sip_transaction_nict_e;
			idPrefix = "NICT";
		}
	}

	trans_id id = trnEvent.get_trans_id();

	if (id.AsString().isEmpty())
	{
		SLOG_WARN("TRANSM", "Create Transaction : Unable to create transaction because transaction-id is empty (%s)", (const char*)trnEvent.GetStartLine());
		return false;
	}

	id.SetStateMachine(idPrefix);

	bool result = false;

	try
	{
		rtl::MutexWatchLock lock(m_pool_lock, __FUNCTION__);

		sip_transaction_t* transaction = create_new_transaction(trnEvent, id, type);
		
		if (transaction != nullptr)
		{
			rtl::String str = (const char*)id.AsString();
			const char* sid = (const char*)str;
			m_pool.add(transaction, sid);
			sip_message_t* msg = NEW sip_message_t(trnEvent);
			transaction->push_event(sip_transaction_event_message_e, (uintptr_t)msg, 0);
			SLOG_TRANS("TRANSM", "Create Transaction : Transaction created : %s", sid);
			
			result = true;
		}
		else if (rtl::Logger::check_trace(TRF_TRANS))
		{
			SLOG_WARN("TRANSM", "Create Transaction : Transaction is nullptr");
		}
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR("TRANSM", "Create Transaction : failed with exception : %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	return result;
}
//--------------------------------------
//
//--------------------------------------
bool sip_transaction_manager_t::check_transaction(const sip_message_t& trnEvent, trans_id& transactionId)
{
	if (m_terminating)
	{
		SLOG_WARN("TRANSM", "Check Transaction : In terminating state.");
		return false;
	}

	if (!trnEvent.IsValid())
	{
		SLOG_WARN("TRANSM", "Check Transaction : Invalid message %s", (const char*)trnEvent.GetStartLine());
		return false;
	}

	SLOG_TRANS("TRANSM", "Check Transaction : message %s callId %s", (const char*)trnEvent.GetStartLine(), (const char*)trnEvent.CallId());

	bool result = false;

	try
	{
		rtl::MutexWatchLock lock(m_pool_lock, __FUNCTION__);

		rtl::String callId = trnEvent.CallId();
		rtl::String idPrefix;
		//sip_transaction_type_t type;

		if (trnEvent.is_request())
		{
			if (trnEvent.IsInvite())
			{
				//type = sip_transaction_ict_e;
				idPrefix = "ICT";
			}
			else
			{
				//type = sip_transaction_nict_e;
				idPrefix = "NICT";
			}
		}

		rtl::String id = trnEvent.get_trans_id().AsString();

		if (id.isEmpty())
		{
			SLOG_WARN("TRANSM", "Check Transaction : Unable to create transaction because transaction-id is empty");
			goto the_end;
		}

		id = idPrefix + "|" + id;

		transactionId = id;

		sip_transaction_t* transaction = m_pool.find(id);

		if (transaction == nullptr && !trnEvent.IsAck())
		{
			SLOG_WARN("TRANSM", "Check Transaction : TRANSACTION DOES NOT EXIST OR DESTROYED!\n\
	\t\t\tMessage: %s\n\
	\t\t\tCall-Id: %s", (const char*)trnEvent.GetStartLine(), (const char*)transactionId.GetCallId());
		}
		else
		{
			result = TRUE;
		}
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR("TRANSM", "Check Transaction : failed with exception %s", ex.get_message());
		result = FALSE;
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

the_end:

	return result;
}
//--------------------------------------
//
//--------------------------------------
bool sip_transaction_manager_t::find_transaction_and_add_event(sip_message_t& trnEvent, trans_id& transactionId, bool fromTransport)
{
	if (m_terminating)
	{
		SLOG_WARN("TRANSM", "Find Transaction : In terminating state.");
		return false;
	}

	if (!trnEvent.IsValid())
	{
		SLOG_WARN("TRANSM", "Find Transaction : Invalid message %s", (const char*)trnEvent.GetStartLine());
		return false;
	}

	SLOG_TRANS("TRANSM", "Find Transaction : message %s from %s callId %s begin search...", (const char*)trnEvent.GetStartLine(),
			fromTransport ? "transport" : "stack", (const char*)trnEvent.CallId());

	rtl::String callId = trnEvent.CallId();
	rtl::String idPrefix;
	//sip_transaction_type_t type;

	if (fromTransport)
	{
		if (trnEvent.is_request())
		{
			if (trnEvent.IsInvite())
			{
	//			type = sip_transaction_ist_e;
				idPrefix = "IST";
			}
			else if (trnEvent.IsAck())
			{
				if (find_ack_transaction_and_add_event(trnEvent, transactionId))
				{
					return true;
				}
			}
			else
			{
	//			type = sip_transaction_nist_e;
				idPrefix = "NIST";
			}
		}
		else
		{
			const sip_value_cseq_t* cseq = trnEvent.get_CSeq_value();

			if (!cseq)
			{
				SLOG_WARN("TRANSM", "Find Transaction : Unable to match transaction because CSeq is empty or malformed");
				return false;
			}
			
			if (cseq->get_method() *= "INVITE")
			{
	//			type = sip_transaction_ict_e;
				idPrefix = "ICT";
			}
			else
			{
	//			type = sip_transaction_nict_e;
				idPrefix = "NICT";
			}
		}
	}
	else
	{
		if (trnEvent.is_request())
		{
			if (trnEvent.IsInvite())
			{
	//			type = sip_transaction_ict_e;
				idPrefix = "ICT";
			}
			else
			{
	//			type = sip_transaction_nict_e;
				idPrefix = "NICT";
			}
		}
		else
		{
			const sip_value_cseq_t* cseq = trnEvent.get_CSeq_value();
			if (!cseq)
			{
				SLOG_WARN("TRANSM", "Find Transaction : Unable to match transaction because CSeq is empty or malformed");
				return false;
			}

			if (cseq->get_method() *= "INVITE")
			{
	//			type = sip_transaction_ist_e;
				idPrefix = "IST";
			}
			else
			{
	//			type = sip_transaction_nist_e;
				idPrefix = "NIST";
			}
		}
	}

	rtl::String id = trnEvent.get_trans_id().AsString();

	if (id.isEmpty())
	{
		SLOG_WARN("TRANSM", "Find Transaction : Unable to create transaction because transaction-id is empty");
		return false;
	}

	id = idPrefix + "|" + id;

	transactionId = id;

	trnEvent.set_transaction(transactionId.AsString());

	SLOG_TRANS("TRANSM", "Find Transaction : find transaction ID %s", (const char*)id);

	const char* key = (const char*)id;
	bool res = true;

	try
	{
		rtl::MutexWatchLock lock(m_pool_lock, __FUNCTION__);

		sip_transaction_t* transaction = m_pool.find(key);

		if (transaction == nullptr)
		{
			if (trnEvent.is_request() && !trnEvent.IsAck() && !trnEvent.has_transaction())
			{
				// lets make sure that if this is a send status message
				// that it does not create a NEW transaction
				res = create_transaction(trnEvent, fromTransport);
			}
			else
			{
				if (!trnEvent.IsAck())
					SLOG_WARN("TRANSM", "Find Transaction : unknown transaction.");

				process_unknown_transaction_event(trnEvent, fromTransport);
				res = false;
			}
		}
		else
		{
			SLOG_TRANS("TRANSM", "Find Transaction : found");
			sip_message_t* msg = NEW sip_message_t(trnEvent);
			transaction->push_event(sip_transaction_event_message_e, (uintptr_t)msg, 0);
		}
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR("TRANSM", "Find Transaction : failed with exception %s", ex.get_message());
		res = FALSE;
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	return res;
}
//--------------------------------------
//
//--------------------------------------
bool sip_transaction_manager_t::remove_transaction(sip_transaction_t* transaction)
{
	bool result = false;

	const trans_id& transactionId = transaction->get_identifier();

	rtl::String stid = transactionId.AsString();
	const char* key = stid;
	SLOG_TRANS("TRANSM", "Remove Transaction %s", key);

	try
	{
		rtl::MutexWatchLock lock(m_pool_lock, __FUNCTION__);
		if (m_pool.remove(key) == nullptr)
		{
			m_pool.remove(transaction);
		}
		result = true;
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR("TRANSM", "Remove Transaction : failed with exception %s", ex.get_message());
		result = false;
		ex.raise_notify(__FILE__, __FUNCTION__);

		return false;
	}

	try
	{
		rtl::MutexWatchLock lock(m_ack_pool_lock, __FUNCTION__);
		if (m_ack_pool.remove(key) == nullptr)
		{
			m_ack_pool.remove(transaction);
		}
		result = true;
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR("TRANSM", "Remove ACK Transaction : failed with exception %s", ex.get_message());
		result = false;
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	SLOG_TRANS("TRANSM", "Transaction %s", result ? "removed" : "not found");

	return result;
}
//--------------------------------------
//
//--------------------------------------
bool sip_transaction_manager_t::find_ack_transaction_and_add_event(const sip_message_t& ack, trans_id& transactionId)
{
	if (m_terminating)
	{
		SLOG_WARN("TRANSM", "Find ACK Transaction : In terminating state.");
		return false;
	}

	if (!ack.IsAck())
	{
		SLOG_WARN("TRANSM", "Find ACK Transaction : Not ACK message.");
		return false;
	}

	rtl::String ackId;
	rtl::String cid;
	const sip_value_uri_t* from;
	rtl::String fromTag;
	const sip_value_via_t* via;
	rtl::String viaBranch;

	SLOG_TRANS("TRANSM", "Find ACK Transaction : callId %s", (const char*)ack.CallId());

	from = ack.get_From_value();

	if (!from)
	{
		SLOG_WARN("TRANSM", "Find ACK Transaction : message has no To header!");
		return false;
	}

	via = ack.get_Via_value();

	if (!via)
	{
		SLOG_WARN("TRANSM", "Find ACK Transaction : message has no Via header!");
		return false;
	}

	fromTag = ack.From_Tag();

	viaBranch = via->get_branch();

	cid = ack.CallId();

	if (cid.isEmpty())
	{
		SLOG_WARN("TRANSM", "Find ACK Transaction : message has no CallId header!");
		return false;
	}

	ackId = cid + fromTag + "|" + viaBranch + "|ACK";

	transactionId = ackId;

	//SLOG_TRANS("TRANSM", "Find ACK Transaction : Searching ACK Transaction %s", (const char*)ackId);

	bool result = false;

	try
	{
		rtl::MutexWatchLock lock(m_ack_pool_lock, __FUNCTION__);

		const char* key = ackId;

		sip_transaction_t* transaction = m_ack_pool.remove(key);

		if (transaction != nullptr)
		{
			SLOG_TRANS("TRANSM", "Find ACK Transaction : found ACK transaction id %d %s!", m_ack_pool.getCount(), (const char*)ackId);

			sip_message_t* msg = NEW sip_message_t(ack);
			transaction->push_event(sip_transaction_event_message_e, (uintptr_t)msg, 0);

			result = true;
		}
		else
		{
			SLOG_TRANS("TRANSM", "Find ACK Transaction : not found!");
		}
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR("TRANSM", "Find ACK Transaction failed with exception %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	return result;
}
//--------------------------------------
//
//--------------------------------------
bool sip_transaction_manager_t::add_ack_transaction(const sip_message_t& response, sip_transaction_t* transaction)
{
	if (m_terminating)
	{
		return false;
	}

	SLOG_TRANS("TRANSM", "Add ACK Transaction : callId %s", (const char*)response.CallId());

	rtl::String ackId;
	rtl::String cid;
	rtl::String fromTag;
	rtl::String viaBranch;

	const sip_value_uri_t* from = response.get_From_value();
	if (!from)
	{
		SLOG_WARN("TRANSM", "Add ACK Transaction : message has no To header!");
		return false;
	}

	const sip_value_via_t* via = response.get_Via_value();
	if (!via)
	{
		SLOG_WARN("TRANSM", "Add ACK Transaction : message has no Via header!");
		return false;
	}

	fromTag = response.From_Tag();

	viaBranch = via->get_branch();

	cid = response.CallId();

	if (cid.isEmpty())
	{
		SLOG_WARN("TRANSM", "Add ACK Transaction : message has no CallId header!");
		return false;
	}

	ackId = cid + fromTag + "|" + viaBranch + "|ACK";

	bool result = false;

	try
	{
		rtl::MutexWatchLock lock(m_ack_pool_lock, __FUNCTION__);

		//SLOG_TRANS("TRANSM", "Add ACK Transaction : trans id %d %s", m_ack_pool.getCount(), (const char*)ackId);

		transaction->set_ack_id(ackId);
		
		m_ack_pool.add(transaction, ackId);
		

		result = true;
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR("TRANSM", "Add ACK Transaction failed with exception %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	return result;
}
//--------------------------------------
//
//--------------------------------------
bool sip_transaction_manager_t::remove_ack_transaction(const sip_message_t& response, sip_transaction_t* transaction)
{
	if (m_terminating)
	{
		return false;
	}

	bool result = false;
	const rtl::String& ackId = transaction->get_ack_id();
	const char* key = ackId;
	SLOG_TRANS("TRANSM", "Remove ACK Transaction : trans cnt %d %s", m_ack_pool.getCount(), (const char*)ackId);

	try
	{
		rtl::MutexWatchLock lock(m_ack_pool_lock, __FUNCTION__);
		if (m_ack_pool.remove(key) == nullptr)
		{
			m_ack_pool.remove(transaction);
		}
		result = true;
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR("TRANSM", "Remove ACK Transaction failed with exception %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	return result;
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_manager_t::process_received_message_event(const sip_message_t& message, sip_transaction_t& transaction)
{
	SLOG_ERROR("TRANSM", "pure virtual call -- %s", __FUNCTION__);
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_manager_t::send_message_to_transport(const sip_message_t& message, sip_transaction_t& transaction)
{
	SLOG_ERROR("TRANSM", "pure virtual call -- %s", __FUNCTION__);
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_manager_t::process_timer_expire_event(sip_transaction_timer_t timer, sip_transaction_t& transaction)
{
	SLOG_ERROR("TRANSM", "pure virtual call -- %s", __FUNCTION__);
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_manager_t::process_unknown_transaction_event(const sip_message_t &, bool)
{
	SLOG_ERROR("TRANSM", "pure virtual call -- %s", __FUNCTION__);
}
//--------------------------------------
//
//--------------------------------------
sip_transaction_t* sip_transaction_manager_t::create_new_transaction(const sip_message_t& trnEvent, const trans_id& TID, sip_transaction_type_t type)
{
	SLOG_ERROR("TRANSM", "pure virtual call -- %s", __FUNCTION__);
	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
//bool sip_transaction_manager_t::break_invite_client_transaction(const sip_message_t& invite)
//{
//	if (m_terminating)
//	{
//		return false;
//	}
//
//	/*
//	Breaking client invite transaction from disconnecting session
//	*/
//
////	PAssert(invite.IsInvite(), PLogicError);
//
//	bool result = false;
//
//	try
//	{
//		rtl::MutexWatchLock lock(m_pool_lock, __FUNCTION__);
//
//		trans_id id = invite.get_trans_id();
//
//		if (id.AsString().isEmpty())
//		{
//			SLOG_WARN("TRANSM", "Break ICT : CallId %s : Unable to match transaction because transaction-id is empty", (const char*)invite.CallId());
//			goto the_end;
//		}
//
//		id.SetStateMachine("ICT");
//
//		const char* key = id.AsString();
//
//		sip_transaction_t* transaction = m_pool.find(key);
//
//		if (transaction == nullptr)
//		{
//			SLOG_WARN("TRANSM", "Break ICT : Transaction not found!");
//			goto the_end;
//		}
//
//		SLOG_TRANS("TRANSM", "Break %s : CallId %s", transaction->get_iid(), (const char*)invite.CallId());
//
//		transaction->process_sip_transaction_cancel_event();
//		result = true;
//	}
//	catch (rtl::Exception& ex)
//	{
//		SLOG_ERROR("TRANSM", "Break ICT Transaction : failed with exception %s", ex.get_message());
//		result = FALSE;
//		ex.raise_notify(__FILE__, __FUNCTION__);
//	}
//the_end:
//
//	return result;
//}
//--------------------------------------
//
//--------------------------------------
bool sip_transaction_manager_t::cancel_invite_client_transaction(const sip_message_t& invite, bool reason_200)
{
	if (m_terminating)
	{
		return false;
	}

	/*
	9 Canceling a Request

	The previous section has discussed general UA behavior for generating
	requests and processing responses for requests of all methods.  In
	this section, we discuss a general purpose method, called CANCEL.

	The CANCEL request, as the name implies, is used to cancel a previous
	request sent by a client.  Specifically, it asks the UAS to cease
	processing the request and to generate an error response to that
	request.  CANCEL has no effect on a request to which a UAS has
	already given a final response.  Because of this, it is most useful
	to CANCEL requests to which it can take a server long time to
	respond.  For this reason, CANCEL is best for INVITE requests, which
	can take a long time to generate a response.  In that usage, a UAS
	that receives a CANCEL request for an INVITE, but has not yet sent a
	final response, would "stop ringing", and then respond to the INVITE
	with a specific error response (a 487).

	CANCEL requests can be constructed and sent by both proxies and user
	agent clients.  Section 15 discusses under what conditions a UAC
	would CANCEL an INVITE request, and Section 16.10 discusses proxy
	usage of CANCEL.

	A stateful proxy responds to a CANCEL, rather than simply forwarding
	a response it would receive from a downstream element.  For that
	reason, CANCEL is referred to as a "hop-by-hop" request, since it is
	responded to at each stateful proxy hop.

	9.1 Client Behavior

	A CANCEL request SHOULD NOT be sent to cancel a request other than
	INVITE.

	Since requests other than INVITE are responded to immediately,
	sending a CANCEL for a non-INVITE request would always create a
	race condition.





	Rosenberg, et. al.          Standards Track                    [Page 53]

	RFC 3261            SIP: Session Initiation Protocol           June 2002


	The following procedures are used to construct a CANCEL request.  The
	Request-URI, Call-ID, To, the numeric part of CSeq, and From header
	fields in the CANCEL request MUST be identical to those in the
	request being cancelled, including tags.  A CANCEL constructed by a
	client MUST have only a single Via header field value matching the
	top Via value in the request being cancelled.  Using the same values
	for these header fields allows the CANCEL to be matched with the
	request it cancels (Section 9.2 indicates how such matching occurs).
	However, the method part of the CSeq header field MUST have a value
	of CANCEL.  This allows it to be identified and processed as a
	transaction in its own right (See Section 17).

	If the request being cancelled contains a Route header field, the
	CANCEL request MUST include that Route header field's values.

	This is needed so that stateless proxies are able to route CANCEL
	requests properly.

	The CANCEL request MUST NOT contain any Require or Proxy-Require
	header fields.

	Once the CANCEL is constructed, the client SHOULD check whether it
	has received any response (provisional or final) for the request
	being cancelled (herein referred to as the "original request").

	If no provisional response has been received, the CANCEL request MUST
	NOT be sent; rather, the client MUST wait for the arrival of a
	provisional response before sending the request.  If the original
	request has generated a final response, the CANCEL SHOULD NOT be
	sent, as it is an effective no-op, since CANCEL has no effect on
	requests that have already generated a final response.  When the
	client decides to send the CANCEL, it creates a client transaction
	for the CANCEL and passes it the CANCEL request along with the
	destination address, port, and transport.  The destination address,
	port, and transport for the CANCEL MUST be identical to those used to
	send the original request.

	If it was allowed to send the CANCEL before receiving a response
	for the previous request, the server could receive the CANCEL
	before the original request.

	Note that both the transaction corresponding to the original request
	and the CANCEL transaction will complete independently.  However, a
	UAC canceling a request cannot rely on receiving a 487 (Request
	Terminated) response for the original request, as an RFC 2543-
	compliant UAS will not generate such a response.  If there is no
	final response for the original request in 64*T1 seconds (T1 is




	Rosenberg, et. al.          Standards Track                    [Page 54]

	RFC 3261            SIP: Session Initiation Protocol           June 2002


	defined in Section 17.1.1.1), the client SHOULD then consider the
	original transaction cancelled and SHOULD destroy the client
	transaction handling the original request.

	9.2 Server Behavior

	The CANCEL method requests that the TU at the server side cancel a
	pending transaction.  The TU determines the transaction to be
	cancelled by taking the CANCEL request, and then assuming that the
	request method is anything but CANCEL or ACK and applying the
	transaction matching procedures of Section 17.2.3.  The matching
	transaction is the one to be cancelled.

	The processing of a CANCEL request at a server depends on the type of
	server.  A stateless proxy will forward it, a stateful proxy might
	respond to it and generate some CANCEL requests of its own, and a UAS
	will respond to it.  See Section 16.10 for proxy treatment of CANCEL.

	A UAS first processes the CANCEL request according to the general UAS
	processing described in Section 8.2.  However, since CANCEL requests
	are hop-by-hop and cannot be resubmitted, they cannot be challenged
	by the server in order to get proper credentials in an Authorization
	header field.  Note also that CANCEL requests do not contain a
	Require header field.

	If the UAS did not find a matching transaction for the CANCEL
	according to the procedure above, it SHOULD respond to the CANCEL
	with a 481 (Call Leg/Transaction Does Not Exist).  If the transaction
	for the original request still exists, the behavior of the UAS on
	receiving a CANCEL request depends on whether it has already sent a
	final response for the original request.  If it has, the CANCEL
	request has no effect on the processing of the original request, no
	effect on any session state, and no effect on the responses generated
	for the original request.  If the UAS has not issued a final response
	for the original request, its behavior depends on the method of the
	original request.  If the original request was an INVITE, the UAS
	SHOULD immediately respond to the INVITE with a 487 (Request
	Terminated).  A CANCEL request has no impact on the processing of
	transactions with any other method defined in this specification.

	Regardless of the method of the original request, as long as the
	CANCEL matched an existing transaction, the UAS answers the CANCEL
	request itself with a 200 (OK) response.  This response is
	constructed following the procedures described in Section 8.2.6
	noting that the To tag of the response to the CANCEL and the To tag
	in the response to the original request SHOULD be the same.  The
	response to CANCEL is passed to the server transaction for
	transmission.
	*/

	//PAssert(invite.IsInvite(), PLogicError);

	bool result = false;

	try
	{
		rtl::MutexWatchLock lock(m_pool_lock, __FUNCTION__);

		trans_id id = invite.get_trans_id();

		if (id.AsString().isEmpty())
		{
			SLOG_WARN("TRANSM", "Cancel ICT : CallId %s : Unable to match transaction because transaction-id is empty", (const char*)invite.CallId());
			goto the_end;
		}

		id.SetStateMachine("ICT");

		rtl::String tid = id.AsString();
		const char* key = tid;


		sip_transaction_t* transaction = m_pool.find(key);

		if (transaction == nullptr)
		{
			SLOG_WARN("TRANSM", "Cancel ICT : Transaction not found!");
			goto the_end;
		}

		SLOG_TRANS("TRANSM", "Cancel %s : CallId %s", transaction->get_iid(), (const char*)invite.CallId());
		
		
		sip_message_t cancel;

		cancel.set_request_method(sip_CANCEL_e);
		cancel.set_std_header(invite.get_std_header(sip_From_e));
		cancel.set_std_header(invite.get_std_header(sip_To_e));
		cancel.set_std_header(invite.get_std_header(sip_Via_e));
		cancel.CSeq(invite.CSeq_Number(), "CANCEL");
		cancel.CallId(invite.CallId());

		cancel.make_Max_Forwards_value()->set_number(SIP_DEFAULT_MAX_FORWARDS);

		if (reason_200)
		{
			cancel.make_Reason()->assign("SIP ;cause=200 ;text=\"Call completed elsewhere\"");
		}

		cancel.set_body(nullptr, 0);

		for (int i = 0; i < invite.get_custom_header_count(); i++)
		{
			cancel.set_custom_header(*invite.get_custom_header_at(i));
		}

		trans_id transactionId = rtl::String(rtl::String("NICT|") + cancel.get_trans_id().AsString());

		sip_transaction_t* cancelTransaction = create_new_transaction(cancel, transactionId, sip_transaction_nict_e);

		if (cancelTransaction == nullptr)
		{
			SLOG_WARN("TRANSM", "Cancel ICT : System can't create CANCEL transaction!");
			goto the_end;
		}

		m_pool.add(cancelTransaction, transactionId.AsString());

		//cancel.set_remote_address(invite.get_remote_address(), invite.get_remote_port(), invite.get_transport_type());
		cancel.set_route(invite.getRoute());

		sip_message_t* pCancel = NEW sip_message_t(cancel);
		
		//pCancel->set_interface(invite.get_interface_address(), invite.get_interface_port());

		in_addr iface = pCancel->get_interface_address();

		SLOG_TRANS("TRANSM", "Cancel ICT : Send CANCEL request iface '%s'", inet_ntoa(iface));

		cancelTransaction->push_event(sip_transaction_event_message_e, (uintptr_t)pCancel, 0);
		
		result = true;
	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR("TRANSM", "Cancel ICT : failed with exception %s", ex.get_message());
		result = FALSE;
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

the_end:

	return result;
}
//--------------------------------------
//
//--------------------------------------
void sip_transaction_manager_t::stop_cancel_invite_client_transaction(const sip_message_t& invite)
{
	// ����� ����� ����� � ����� ������ ����������

	try
	{
		rtl::MutexWatchLock lock(m_pool_lock, __FUNCTION__);

		trans_id id = invite.get_trans_id();
		id.SetMethod("CANCEL");
		id.SetStateMachine("NICT");

		if (id.AsString().isEmpty())
		{
			SLOG_WARN("TRANSM", "Stop Cancel ICT : CallId %s : Unable to match transaction because transaction-id is empty", (const char*)invite.CallId());
			goto the_end;
		}

		rtl::String tid = id.AsString();
		const char* key = tid;


		sip_transaction_t* transaction = m_pool.find(key);

		if (transaction == nullptr)
		{
			SLOG_WARN("TRANSM", "Stop Cancel ICT : Transaction not found!");
			goto the_end;
		}

		SLOG_TRANS("TRANSM", "Stop Cancel %s : transaction found -- remove it. CallId %s ", transaction->get_iid(), (const char*)invite.CallId());

		transaction->destroy();

		m_pool.remove(transaction);

	}
	catch (rtl::Exception& ex)
	{
		SLOG_ERROR("TRANSM", "Cancel ICT : failed with exception %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

the_end:
	;
}
//--------------------------------------
//
//--------------------------------------
//BOOL sip_transaction_manager_t::CancelInviteServerTransaction(const sip_message_t& cancel)
//{
//	if (m_terminating)
//		return FALSE;
//
//	sip_message_t invite = cancel;
//	///change the method to INVITE
//	StartLine startLine;
//	invite.GetStartLine(startLine);
//	startLine.GetRequestLine()->SetMethod("INVITE");
//	invite.SetStartLine(startLine);
//
//	CSeq cseq;
//	invite.GetCSeq(cseq);
//	cseq.SetMethod("INVITE");
//	invite.SetCSeq(cseq);
//
//	sip_message_t* message = NEW sip_message_t(cancel);
//
//	trans_id transactionId = rtl::String(rtl::String("IST|") + invite.GetTransaction().AsString());
//	sip_transaction_event_info_t trnEvent = { sip_transaction_event_cancel_e, 0, 0 };
//
//	SLOG_TRANS("TRANSM", "Cancel IST : Prepare CANCEL request callId '%s'", (const char*)cancel.CallId());
//
//	return FindTransactionAndAddEvent(transactionId, trnEvent);
//}
//--------------------------------------
