/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_value_via.h"
//--------------------------------------
//
//--------------------------------------
inline const char* SKIP_WS(const char* text)
{
	char ch = *text;

	while (ch != 0 && ch == ' ' && ch == '\t') ch = *(++text);

	return text;
}
//--------------------------------------
//
//--------------------------------------
sip_value_via_t::sip_value_via_t() : m_transport_type(sip_transport_udp_e), m_port(0), m_ttl(0), m_rport(0)
{
	m_maddr.s_addr = 0;
	m_received.s_addr = 0;
}
//--------------------------------------
//
//--------------------------------------
sip_value_via_t::~sip_value_via_t()
{
	cleanup();
}
void sip_value_via_t::cleanup()
{
	m_transport_type = sip_transport_udp_e;
	m_address.setEmpty();
	m_port = 0;
	m_branch.setEmpty();
	m_maddr.s_addr = INADDR_ANY;
	m_ttl = 0;
	m_rport = 0;
	m_received.s_addr = INADDR_ANY;
}
//--------------------------------------
//
//--------------------------------------
rtl::String& sip_value_via_t::prepare(rtl::String& stream) const
{
	stream << "SIP/2.0/" << TransportType_toString(m_transport_type) << ' ' << m_address;

	if (m_port != 0 && m_port != 5060)
		stream << ':' << m_port;

	stream << ";branch=" << m_branch;

	if (m_maddr.s_addr != INADDR_ANY && m_maddr.s_addr != INADDR_NONE)
		stream << ";maddr=" << inet_ntoa(m_maddr);

	if (m_ttl > 0)
		stream << ";ttl=" << m_ttl;

	if (m_rport < 0x10000)
	{
		stream << ";rport";

		if (m_rport > 0)
			stream << '=' << m_rport;
	}

	if (m_received.s_addr != INADDR_ANY && m_received.s_addr != INADDR_NONE)
		stream << ";received=" << inet_ntoa(m_received);

	return stream;
}
//--------------------------------------
// SIP/2.0/UDP 192.168.0.110:5062;branch=z9hG4bK620795769
//--------------------------------------
bool sip_value_via_t::parse()
{
	const char* zvalue = m_value;
	
	zvalue = SKIP_WS(zvalue);

	if (std_strnicmp(zvalue, "SIP", 3) != 0)
		return false;
	
	zvalue += 3;

	if ((zvalue = strchr(zvalue, '/')) == nullptr)
	{
		return false;
	}
	
	zvalue = SKIP_WS(++zvalue);

	if (std_strnicmp(zvalue, "2.0", 3) != 0)
		return false;

	zvalue += 3;

	if ((zvalue = strchr(zvalue, '/')) == nullptr)
	{
		return false;
	}

	const char* space = strchr(++zvalue, ' ');

	if (space == nullptr)
		return false;

	m_transport_type = parse_transport_type(zvalue, PTR_DIFF(space, zvalue));

	zvalue = space + 1;

	m_address = SKIP_WS(zvalue);

	int index = m_address.indexOf(':');

	if (index != -1)
	{
		m_port = (uint16_t)strtoul((const char*)m_address + index + 1, nullptr, 10);
		m_address.remove(index, -1);
	}
	else
	{
		m_port = 0;
	}

	m_branch.setEmpty();
	m_ttl = 0;
	m_maddr.s_addr = INADDR_ANY;
	m_rport = 0xFFFFFFFF;
	m_received.s_addr = INADDR_ANY;

	for (int i = m_params.getCount() - 1; i >= 0; i--)
	{
		const mime_param_t* param = m_params.getAt(i);

		if (std_stricmp(param->getName(), "branch") == 0)
		{
			m_branch = param->getValue();
			m_params.removeAt(i);
		}
		else if (std_stricmp(param->getName(), "maddr") == 0)
		{
			m_maddr.s_addr = inet_addr(param->getValue());
			m_params.removeAt(i);
		}
		else if (std_stricmp(param->getName(), "ttl") == 0)
		{
			m_ttl = (uint16_t)strtoul(param->getValue(), nullptr, 10);
			m_params.removeAt(i);
		}
		else if (std_stricmp(param->getName(), "rport") == 0)
		{
			const rtl::String& rport = param->getValue();

			m_rport = rport.isEmpty() ? 0 : strtoul(rport, nullptr, 10);

			m_params.removeAt(i);
		}
		else if (std_stricmp(param->getName(), "received") == 0)
		{
			m_received.s_addr = inet_addr(param->getValue());
			m_params.removeAt(i);
		}
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
sip_value_via_t& sip_value_via_t::assign(const sip_value_via_t& v)
{
	sip_value_t::assign(v);

	m_transport_type = v.m_transport_type;
	m_address = v.m_address;
	m_port = v.m_port;

	m_branch = v.m_branch;
	m_maddr = v.m_maddr;
	m_ttl = v.m_ttl;
	m_rport = v.m_rport;
	m_received = v.m_received;

	return *this;
}
//--------------------------------------
//
//--------------------------------------
sip_value_t* sip_value_via_t::clone() const
{
	return NEW sip_value_via_t(*this);
}
//--------------------------------------
