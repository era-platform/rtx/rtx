﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//--------------------------------------
// Interface to hmac auth_type_t
//--------------------------------------
// based on libsrtp written by
// David A. McGrew
// Cisco Systems, Inc.
//--------------------------------------
#pragma once

#include "srtp_auth.h"
#include "std_crypto.h"
//--------------------------------------
//
//--------------------------------------
class hmac_ctx_t : public auth_t
{
	uint8_t m_opad[64];
	sha1_context_t m_ctx;
	sha1_context_t m_init_ctx;

public:
	hmac_ctx_t(int key_len, int out_len);
	virtual ~hmac_ctx_t();

	virtual bool create(const uint8_t *key, int key_len);
	virtual bool compute(const uint8_t *message, int msg_octets, uint8_t *result, int tag_len = -1);
	virtual bool update(const uint8_t *message, int msg_octets);
	virtual bool start();
};
//--------------------------------------
