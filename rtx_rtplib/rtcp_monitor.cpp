/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtcp_monitor.h"
#include "rtcp_packet_XR.h"
#include "rtcp_packet_AVB.h"
//#include "srtp.h"
//--------------------------------------
//
//--------------------------------------
#define RTCP_BW					(160 * 50)	// FIXME: default bandwidth (octet/second)
#define CODEC_RATE				8000		// FIXME
#define MAX_DROPOUT				3000
#define MAX_MISORDER			100
#define MIN_SEQUENTIAL			2
#define RTCP_TIMER_ID_NONE		0
#define RTCP_TIMER_ID_BYE		1
#define RTCP_TIMER_ID_REPORT	2
//--------------------------------------
//
//--------------------------------------
static rtcp_source_t* rtcp_create_source(uint32_t ssrc, uint16_t seq, uint32_t ts);
static int rtcp_source_init_seq(rtcp_source_t* self, uint16_t seq, uint32_t ts);
static bool rtcp_source_update_seq(rtcp_source_t* self, uint16_t seq, uint32_t ts);
static bool rtcp_source_is_probed(const rtcp_source_t* self);
static const char* rtcp_get_event_name(rtcp_event_t e);
//--------------------------------------
//
//--------------------------------------
static rtcp_source_t* rtcp_create_source(uint32_t ssrc, uint16_t seq, uint32_t ts)
{
	rtcp_source_t* source = NEW rtcp_source_t;
	memset(source, 0, sizeof(rtcp_source_t));
	
	rtcp_source_init_seq(source, seq, ts);

	source->ssrc = ssrc;
	source->max_seq = seq - 1;
	source->probation = MIN_SEQUENTIAL;
	source->rate = CODEC_RATE; //FIXME

	return source;
}
//--------------------------------------
//
//--------------------------------------
static int rtcp_source_init_seq(rtcp_source_t* self, uint16_t seq, uint32_t ts)
{
	self->base_seq = seq;
	self->max_seq = seq;
	self->bad_seq = RTP_SEQ_MOD + 1;   /* so seq == bad_seq is false */
	self->cycles = 0;
	self->received = 0;
	self->received_prior = 0;
	self->expected_prior = 0;
	self->base_ts = ts;
	self->max_ts = ts;

	return 0;
}
//--------------------------------------
//
//--------------------------------------
static bool rtcp_source_update_seq(rtcp_source_t* self, uint16_t seq, uint32_t ts)
{
	uint16_t udelta = seq - self->max_seq;

	/*
	* Source is not valid until MIN_SEQUENTIAL packets with
	* sequential sequence numbers have been received.
	*/
	if (self->probation)
	{
		/* packet is in sequence */
		if (seq == self->max_seq + 1)
		{
			self->probation--;
			self->max_seq = seq;
			self->max_ts = ts;
			if (self->probation == 0)
			{
				rtcp_source_init_seq(self, seq, ts);
				self->received++;
				return true;
			}
		}
		else
		{
			self->probation = MIN_SEQUENTIAL - 1;
			self->max_seq = seq;
			self->max_ts = ts;
		}
		
		return false;
	}
	else if (udelta < MAX_DROPOUT)
	{
		/* in order, with permissible gap */
		if (seq < self->max_seq)
		{
			/*
			* Sequence number wrapped - count another 64K cycle.
			*/
			self->cycles += RTP_SEQ_MOD;
		}
		self->max_seq = seq;
		self->max_ts = ts;
	}
	else if (udelta <= RTP_SEQ_MOD - MAX_MISORDER)
	{
		/* the sequence number made a very large jump */
		if (seq == self->bad_seq)
		{
			/*
			* Two sequential packets -- assume that the other side
			* restarted without telling us so just re-sync
			* (i.e., pretend this was the first packet).
			*/
			rtcp_source_init_seq(self, seq, ts);
		}
		else
		{
			self->bad_seq = (seq + 1) & (RTP_SEQ_MOD-1);
			return false;
		}
	}
	else
	{
		/* duplicate or reordered packet */
	}
	
	self->received++;
	
	return true;
}
//--------------------------------------
//
//--------------------------------------
static bool rtcp_source_is_probed(const rtcp_source_t* self)
{
	return (self && self->probation == 0);
}
//--------------------------------------
//
//--------------------------------------
rtcp_monitor_t::rtcp_monitor_t(rtl::Logger* log, const char* name, const char* addr, bool webrtc) : m_log(log), m_event_handler_rx(nullptr), m_event_handler_tx(nullptr),
	m_source_local(nullptr), m_sdes(nullptr)
{
	m_timer_started = false;
	
	m_time_start = 0;
	m_fir_seqnr = 0;

	memset(m_cname, 0, sizeof m_cname);
	m_is_cname_defined = false;
	m_packets_count = 0;
	m_octets_count = 0;

	m_tp = 0;
	m_tn = 0;
	
	m_pmembers = 0;
	m_avg_rtcp_size = 0.0;

	// RFC 3550 - 6.3.2 Initialization
	m_initial = true;
	m_we_sent = false;
	m_senders = 1;
	m_members = 1;
	m_rtcp_bw = RTCP_BW;	//FIXME: as parameter from the code, Also added possiblities to update this value
	m_ssrc_opposite = 0;

	memset(m_context, 0, m_name_size);
	memset(m_termination, 0, m_name_size);
	memset(m_addr, 0, m_name_size);
	strncpy(m_addr, addr, m_name_size);

	if (name != nullptr)
	{
		const char* tmp1 = strstr(name, "-");
		tmp1++;
		tmp1++;
		const char* tmp2 = strstr(tmp1, "-");
		uint32_t size1 = uint32_t(tmp2 - tmp1);
		tmp2++;
		const char* tmp3 = strstr(tmp2, "-");
		uint32_t size2 = uint32_t(tmp3 - tmp2);

		uint32_t size = (size1 > m_name_size) ? m_name_size : size1;
		memcpy(m_context, tmp1, size);
		size = (size2 > m_name_size) ? m_name_size : size2;
		memcpy(m_termination, tmp2, size);
	}
	m_webrtc = webrtc;
}
//--------------------------------------
//
//--------------------------------------
rtcp_monitor_t::~rtcp_monitor_t()
{
	m_event_handler_rx = nullptr;
	m_event_handler_tx = nullptr;

	clear_sourses();

	if (m_sdes != nullptr)
	{
		DELETEO(m_sdes);
		m_sdes = nullptr;
	}

	m_log = nullptr;
}
//--------------------------------------
//
//--------------------------------------
void rtcp_monitor_t::set_session_event_handler_rx(rtcp_monitor_event_handler_rx_t* handler)
{
	rtl::MutexLock lock(m_sync);
	m_event_handler_rx = handler;
}
void rtcp_monitor_t::set_session_event_handler_tx(rtcp_monitor_event_handler_tx_t* handler)
{
	rtl::MutexLock lock(m_sync);
	m_event_handler_tx = handler;
}
//--------------------------------------
//
//--------------------------------------
void rtcp_monitor_t::setup(uint32_t ssrc, uint32_t ssrc_opposite, const char* cname)
{
	rtl::MutexLock lock(m_sync);

	PLOG_RTP_WRITE("rtcp-mon", "setting up RTCP monitor to SSRC %X", ssrc);

	// RFC 3550 - 6.3.2 Initialization
	m_source_local = rtcp_create_source(ssrc, 0, 0);
	add_source(m_source_local);

	m_initial = true;
	m_we_sent = false;
	m_senders = 1;
	m_members = 1;
	m_rtcp_bw = RTCP_BW;	//FIXME: as parameter from the code, Also added possiblities to update this value

	memset(m_cname, 0, sizeof m_cname);
	if (cname != nullptr)
	{
		strncpy(m_cname, cname, MD5_HASH_HEX_SIZE);
		m_cname[MD5_HASH_HEX_SIZE] = 0;
		m_is_cname_defined = true;
	}

	m_ssrc_opposite = ssrc_opposite;
}
//--------------------------------------
//
//--------------------------------------
void rtcp_monitor_t::reset()
{
	stop();

	rtl::MutexLock lock(m_sync);

	m_event_handler_rx = nullptr;
	m_event_handler_tx = nullptr;

	if (m_sdes != nullptr)
	{
		DELETEO(m_sdes);
		m_sdes = nullptr;
	}

	m_time_start = 0;
	
	m_fir_seqnr = 0;

	memset(m_cname, 0, sizeof m_cname);
	m_is_cname_defined = false;
	m_packets_count = 0;
	m_octets_count = 0;

	m_tp = 0;
	m_tn = 0;
	m_pmembers = 0;
	m_members = 0;
	m_senders = 0;
	m_rtcp_bw = 0.0;
	m_we_sent = false;
	m_avg_rtcp_size = 0.0;
	m_initial = 0;

	clear_sourses();
}
//--------------------------------------
//
//--------------------------------------
bool rtcp_monitor_t::start()
{
	if (m_webrtc)
	{
		PLOG_RTP_WRITE("rtcp-mon", "RTCP monitor for webrtc");
		return true;
	}

	PLOG_RTP_WRITE("rtcp-mon", "starting RTCP monitor...");
	
	if (m_timer_started)
	{
		PLOG_RTP_WRITE("rtcp-mon", "RTCP monitor already started");
		return true;
	}
	// Send Initial RR (mandatory)

	schedule(0, rtcp_event_report_e);

	// set start time
	m_time_start = rtl::DateTime::getTicks64();

	PLOG_RTP_WRITE("rtcp-mon", "RTCP monitor started");

	return true;
}
//--------------------------------------
//
//--------------------------------------
void rtcp_monitor_t::stop()
{
	PLOG_RTP_WRITE("rtcp-mon", "stopping RTCP monitor timer:%s...", m_timer_started ? L"started" : L"stopped");

	if (m_timer_started)
	{
		send_bye_event(rtcp_event_report_e);

		rtcp_timer_t::remove_timer(this, RTCP_TIMER_ID_BYE);
		rtcp_timer_t::remove_timer(this, RTCP_TIMER_ID_REPORT);

		PLOG_RTP_WRITE("rtcp-mon", "RTCP monitor stopped");
	}
	else
	{
		PLOG_RTP_WRITE("rtcp-mon", "RTCP monitor already stopped");
	}
}
//--------------------------------------
//
//--------------------------------------
bool rtcp_monitor_t::coincidence(uint32_t ssrc, uint32_t ssrc_opposite, const char* cname)
{
	if (rtl::strcmp(cname, m_cname) == 0)
	{
		return true;
	}

	if (m_ssrc_opposite == ssrc_opposite)
	{
		return true;
	}

	if (m_source_local->ssrc == ssrc)
	{
		return true;
	}

	return false;
}
//--------------------------------------
bool rtcp_monitor_t::match(uint32_t ssrc, uint32_t ssrc_opposite, const char* cname)
{
	if (rtl::strcmp(cname, m_cname) == 0)
	{
		if (m_ssrc_opposite == ssrc_opposite)
		{
			if (m_source_local->ssrc == ssrc)
			{
				return true;
			}
		}
	}

	return false;
}
//--------------------------------------
//
//--------------------------------------
uint32_t rtcp_monitor_t::get_ssrc()
{
	return m_source_local->ssrc;
}
//--------------------------------------
//
//--------------------------------------
uint32_t rtcp_monitor_t::get_ssrc_opposite()
{
	return m_ssrc_opposite;
}
//--------------------------------------
//
//--------------------------------------
const char* rtcp_monitor_t::get_cname()
{
	return m_cname;
}
//--------------------------------------
//
//--------------------------------------
void rtcp_monitor_t::rtcp_event_tx_data(const rtp_packet* packet)
{
	if (m_webrtc)
	{
		return;
	}

	if (!m_timer_started)
	{
		return;
	}

	rtl::MutexLock lock(m_sync);

	// create local source if not already done
	// first destroy it if the ssrc don't match
	//if (m_source_local && m_source_local->ssrc != ntohl(packet->ssrc))
	if (m_source_local && m_source_local->ssrc != packet->get_ssrc())
	{
		// local ssrc has changed: sould never happen ...but who know?
		// remove the source
		//TSK_DEBUG_WARN("Not expected to be called");
		
		bool removed = remove_source(m_source_local->ssrc);

		m_source_local = nullptr;

		if (m_sdes != nullptr)
		{
			DELETEO(m_sdes);
			m_sdes = nullptr;
		}

		m_packets_count = 0;
		m_octets_count = 0;
		
		if(removed)
		{
			--m_senders;
			--m_members;
		}
	}
	
	if (!m_source_local)
	{
		//m_source_local = rtcp_create_source(ntohl(packet->ssrc), ntohs(packet->seq), ntohl(packet->ts));
		m_source_local = rtcp_create_source(packet->get_ssrc(), packet->get_sequence_number(), packet->getTimestamp());
		// add the source (refresh the number of senders, ...)
		add_source(m_source_local);
		// 'members' and 'senders' were already initialized in the constructor
	}
	
	if (!m_we_sent)
	{
		m_we_sent = true;
	}

	m_packets_count++;
	//m_octets_count += size;
	m_octets_count += packet->get_payload_length();
}
//--------------------------------------
//
//--------------------------------------
void rtcp_monitor_t::rtcp_event_rx_data(const rtp_packet* packet)
{
	if (m_webrtc)
	{
		return;
	}

	rtcp_source_t* source;

	if (!m_timer_started)
	{
		return;
	}

	rtl::MutexLock lock(m_sync);

	process_rtp_in(packet);

	if ((source = find_source(packet->get_ssrc())) != nullptr)
	{
		if (rtcp_source_update_seq(source, packet->get_sequence_number(), packet->getTimestamp()))
		{
			// RFC 3550 A.8 Estimating the Interarrival Jitter
			/* uint32_t expected = (source->cycles + source->max_seq) - source->base_seq + 1; */
			double arrival = (((double)(source->max_ts - source->base_ts) / (double)source->rate) * 1000);
			int32_t transit = (int32_t)arrival - packet->getTimestamp();
			int32_t d = (transit - source->transit);
			
			if (d < 0)
			{
				d = -d;
			}

			source->transit = transit;
			source->jitter += (1./16.) * ((double)d - source->jitter);
		}
	}

	return;
}
//--------------------------------------
//
//--------------------------------------
void rtcp_monitor_t::rtcp_event_rx_ctrl(const rtcp_packet_t* packet)
{
	if (m_log && rtl::Logger::check_trace(TRF_RTP))
	{
		char buffer[2048];
		buffer[0] = 0;
		int written = packet->dump(buffer, 2047);
		buffer[written] = 0;
		if (buffer[0] != 0 && written > 0)
		{
			m_log->log("rtcp-mon", "RTCP PACKET RX\r\n%s", buffer);
		}
	}

	if (m_webrtc)
	{
		return;
	}

	if (!m_timer_started)
	{
		return;
	}

	// deserialize the RTCP packet for processing
	{
		rtl::MutexLock lock(m_sync); //tsk_safeobj_lock(self);

		process_rtcp_in(packet, packet->getType() == rtcp_type_bye_e ? rtcp_event_bye_e : rtcp_event_report_e);

		if (packet->getType() == rtcp_type_sr_e)
		{
			rtcp_source_t* source;
			if ((source = find_source(packet->get_ssrc())) != nullptr)
			{
				const rtcp_sr_packet_t* sr = (const rtcp_sr_packet_t*)packet->get_packet(rtcp_type_sr_e);

				source->ntp_lsw = sr->get_ntp_fraction();
				source->ntp_msw = sr->get_ntp_timestamp();
				source->dlsr = rtl::DateTime::getTicks64();

				source->packets_sent = sr->get_packets_sent();
				source->octets_sent = sr->get_octets_sent();
			}
		}
	} 

	if (m_event_handler_rx != nullptr)
	{
		m_event_handler_rx->rtcp_session_rx(packet);
	}
}
//--------------------------------------
//
//--------------------------------------
bool rtcp_monitor_t::signal_packet_loss(uint32_t ssrc_media, const uint16_t* seq_nums, int count)
{
	PLOG_RTP_WRITE("rtcp-mon", "signal \"packet-lost\" : ssrc:%X seq-nums:%u count:%d", ssrc_media, seq_nums, count);

	if (!m_source_local || !seq_nums || !count)
	{
		return false;
	}
	
	rtcp_packet_t rtcp_packet;
	
	rtcp_rr_packet_t* rr = NEW rtcp_rr_packet_t;
	rr->set_ssrc(m_source_local->ssrc);
	rtcp_packet.add_packet(rr);

	rtcp_rtpfb_packet_t* rtpfb = NEW rtcp_rtpfb_packet_t;
	rtpfb->set_ssrc(m_source_local->ssrc);
	rtpfb->set_fb_type(rtcp_rtpfb_nack_e);
	rtpfb->set_media_source(ssrc_media);
	rtpfb->add_nack(seq_nums, count);

	rtcp_packet.add_packet(rtpfb);
	
	return send_packet(&rtcp_packet) > 0;
}
//--------------------------------------
//
//--------------------------------------
bool rtcp_monitor_t::signal_frame_corrupted(uint32_t ssrc_media)
{
	rtcp_packet_t packet;
	
	if (!m_source_local)
	{
		return false;
	}

	PLOG_RTP_WRITE("rtcp-mon", "signal \"frame-corrapted\" : ssrc:%X", ssrc_media);

	rtcp_rr_packet_t* rr = NEW rtcp_rr_packet_t;
	rr->set_ssrc(m_source_local->ssrc);
	packet.add_packet(rr);

	rtcp_psfb_packet_t* psfb_fir = NEW rtcp_psfb_packet_t;
	psfb_fir->set_fb_type(rtcp_psfb_fir_e);
	psfb_fir->set_media_source(ssrc_media);
	psfb_fir->add_fir(m_source_local->ssrc, m_fir_seqnr++);

	packet.add_packet(psfb_fir);
	
	return send_packet(&packet) > 0;
}
//--------------------------------------
//
//--------------------------------------
bool rtcp_monitor_t::signal_jb_error(uint32_t ssrc_media)
{
	PLOG_RTP_WRITE("rtcp-mon", "signal \"jitter-error\" : ssrc:%X", ssrc_media);

	return signal_frame_corrupted(ssrc_media);
}
//--------------------------------------
//
//--------------------------------------
void rtcp_monitor_t::clear_sourses()
{
	for (int i = 0; i < m_sources.getCount(); i++)
	{
		rtcp_source_t* source = m_sources.getAt(i);
		if (source == nullptr)
		{
			continue;
		}
		DELETEO(source);
		source = nullptr;
	}
	m_source_local = nullptr;
	m_sources.clear();
}
//--------------------------------------
//
//--------------------------------------
bool rtcp_monitor_t::have_source(uint32_t ssrc)
{
	for (int i = 0; i < m_sources.getCount(); i++)
	{
		rtcp_source_t* source = m_sources[i];

		if (source->ssrc == ssrc)
		{
			return true;
		}
	}

	return false;
}
//--------------------------------------
//
//--------------------------------------
rtcp_source_t* rtcp_monitor_t::find_source(uint32_t ssrc)
{
	for (int i = 0; i < m_sources.getCount(); i++)
	{
		rtcp_source_t* source = m_sources[i];

		if (source->ssrc == ssrc)
		{
			return source;
		}
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
bool rtcp_monitor_t::add_source(rtcp_source_t* source)
{
	PLOG_RTP_WRITE("rtcp-mon", "add source ptr:%p ssrc:%X...", source, source ? source->ssrc : 0);
	if (source == nullptr)
	{
		return false;
	}

	rtl::MutexLock lock(m_sync);
	m_sources.add(source);

	PLOG_RTP_WRITE("rtcp-mon", "source added");

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool rtcp_monitor_t::add_source(uint32_t ssrc, uint16_t seq, uint32_t ts)
{
	rtcp_source_t* source;

	PLOG_RTP_WRITE("rtcp-mon", "add source ssrc:%X seq:%u ts:%X", ssrc, seq, ts);

	rtl::MutexLock lock(m_sync);

	for (int i = 0; i < m_sources.getCount(); i++)
	{
		source = m_sources[i];
		if (source->ssrc == ssrc)
		{
			PLOG_RTP_WRITE("rtcp-mon", "source already exist");
			return true;
		}
	}

	PLOG_RTP_WRITE("rtcp-mon", "create new source");

	return add_source(rtcp_create_source(ssrc, seq, ts));
}
//--------------------------------------
//
//--------------------------------------
bool rtcp_monitor_t::remove_source(uint32_t ssrc)
{
	PLOG_RTP_WRITE("rtcp-mon", "remove source ssrc:%08X...", ssrc);

	rtl::MutexLock lock(m_sync);
	
	for (int i = 0; i <= m_sources.getCount(); i++)
	{
		rtcp_source_t* source = m_sources[i];
		if (source != nullptr && source->ssrc == ssrc)
		{
			PLOG_RTP_WRITE("rtcp-mon", "source %p removed", source);
			m_sources.removeAt(i);
			if ((m_source_local != nullptr) && (m_source_local->ssrc == ssrc))
			{
				m_source_local = nullptr;
			}
			DELETEO(source);
			source = nullptr;
			return true;
		}
	}

	PLOG_RTP_WRITE("rtcp-mon", "source already removed or never added!");

	return false;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_monitor_t::send_packet(rtcp_packet_t* packet)
{
	if ((m_event_handler_tx == nullptr) || (m_source_local == nullptr))
	{
		return 0;
	}

	rtcp_sdes_packet_t* sdes = nullptr;

	// SDES
	if (!m_sdes)
	{
		sdes = NEW rtcp_sdes_packet_t;
		int chunk_index = sdes->add_chunk(m_source_local->ssrc);
		
		if (chunk_index != BAD_INDEX)
		{
			static const char* _name = "rtxmg3";

			sdes->add_chunk_item(chunk_index, rtcp_sdes_cname_e, m_cname, MD5_HASH_HEX_SIZE);
			sdes->add_chunk_item(chunk_index, rtcp_sdes_name_e, _name, int(strlen(_name)));
		}
		else
		{
			DELETEO(sdes);
			sdes = nullptr;
		}
	}
	
	if (sdes != nullptr)
	{
		packet->add_packet(sdes);
	}

	if (m_event_handler_tx == nullptr)
	{
		return 0;
	}

	return m_event_handler_tx->rtcp_session_tx(packet);
}
//--------------------------------------
//
//--------------------------------------
void rtcp_monitor_t::rtcp_timer_elapsed(uint32_t tid)
{
	try
	{
		rtl::MutexLock lock(m_sync);

		if (!m_timer_started)
			return;

		PLOG_RTP_WRITE("rtcp-mon", "RTCP timer elapsed tid:%s...", tid == RTCP_TIMER_ID_BYE ? "bye" : tid == RTCP_TIMER_ID_REPORT ? "report" : "unknown");

		if (tid == RTCP_TIMER_ID_BYE)
		{
			expire_event(rtcp_event_bye_e);
		}
		else if (tid == RTCP_TIMER_ID_REPORT)
		{
			expire_event(rtcp_event_report_e);
		}
	}
	catch (rtl::Exception& ex)
	{
		PLOG_RTP_ERROR("rtcp-mon", "RTCP timer elapsed tid:%s event failed with exception %s", tid == RTCP_TIMER_ID_BYE ? "bye" : tid == RTCP_TIMER_ID_REPORT ? "report" : "unknown", ex.get_message());
	}
}
//--------------------------------------
//
//--------------------------------------
void rtcp_monitor_t::schedule(double tn, rtcp_event_t e)
{
	if (m_webrtc)
	{
		PLOG_RTP_WARNING("rtcp-mon", "schedule RTCP monitor for webrtc");
		return;
	}

	PLOG_RTP_WRITE("rtcp-mon", "schedule tasks tn:%f event:%s", tn, rtcp_get_event_name(e));

	rtl::MutexLock lock(m_sync);

	switch (e)
	{
		case rtcp_event_bye_e: 
			rtcp_timer_t::add_timer(this, RTCP_TIMER_ID_BYE, (uint32_t)tn);
			m_timer_started = true;
			break;
		case rtcp_event_report_e:
			rtcp_timer_t::add_timer(this, RTCP_TIMER_ID_REPORT, (uint32_t)tn);
			m_timer_started = true;
			break;
		default:
			break;
	}
}
//--------------------------------------
//
//--------------------------------------
void rtcp_monitor_t::process_rtp_in(const rtp_packet* packet)
{
	if (is_new_member_rtp(packet))
	{
		int count = add_member_rtp(packet, false);
		
		m_senders += count;
		m_members += count;
	}
}
//--------------------------------------
// OnReceive
//--------------------------------------
void rtcp_monitor_t::process_rtcp_in(const rtcp_packet_t* packet, rtcp_event_t e)
{
	/* What we do depends on whether we have left the group, and are
	* waiting to send a BYE (TypeOfEvent(e) == EVENT_BYE) or an RTCP
	* report.  p represents the packet that was just received.  */

	bool is_bye_packet = packet->getType() == rtcp_type_bye_e;

	if (!is_bye_packet)
	{
		if (is_new_member_rtcp(packet->get_lead_packet()))
		{
			m_members += add_member_rtcp(packet, false);
		}
		
		//m_avg_rtcp_size = (1./16.) * size + (15./16.) * (m_avg_rtcp_size);
		m_avg_rtcp_size = (1. / 16.) * packet->get_full_length() + (15. / 16.) * (m_avg_rtcp_size);
	}
	else
	{
		//m_avg_rtcp_size = (1./16.) * size + (15./16.) * (m_avg_rtcp_size);
		m_avg_rtcp_size = (1. / 16.) * packet->get_full_length() + (15. / 16.) * (m_avg_rtcp_size);

		if (e == rtcp_event_report_e)
		{
			double tc = (double)rtl::DateTime::getTicks64();
			int count = remove_member_rtcp(packet);
			
			m_senders -= count;
			m_members -= count;

			if (m_members < m_pmembers && m_pmembers)
			{
				m_tn = (uint64_t)(tc + (((double)m_members) / (m_pmembers)) * (m_tn - tc));
				m_tp = (uint64_t)(tc - (((double)m_members) / (m_pmembers)) * (tc - m_tp));

				/* Reschedule the next report for time tn */
				schedule((double)m_tn, e);
				m_pmembers = m_members;
			}
		}
		else if (e == rtcp_event_bye_e)
		{
			m_members += 1;
		}
	}
}
//--------------------------------------
//
//--------------------------------------
double rtcp_monitor_t::calc_rtcp_interval()
{
	/*
	* Minimum average time between RTCP packets from this site (in
	* seconds).  This time prevents the reports from `clumping' when
	* sessions are small and the law of large numbers isn't helping
	* to smooth out the traffic.  It also keeps the report interval
	* from becoming ridiculously small during transient outages like
	* a network partition.
	*/
	#define RTCP_MIN_TIME 5.
	/*
	* Fraction of the RTCP bandwidth to be shared among active
	* senders.  (This fraction was chosen so that in a typical
	* session with one or two active senders, the computed report
	* time would be roughly equal to the minimum report time so that
	* we don't unnecessarily slow down receiver reports.)  The
	* receiver fraction must be 1 - the sender fraction.
	*/
	#define RTCP_SENDER_BW_FRACTION 0.25
	#define RTCP_RCVR_BW_FRACTION (1 - RTCP_SENDER_BW_FRACTION)
	/*
	 * To compensate for "timer reconsideration" converging to a
	 * value below the intended average.
	 */
	#define COMPENSATION  (2.71828 - 1.5)

	double t;                   /* interval */
	double rtcp_min_time = RTCP_MIN_TIME;
	int n;                      /* no. of members for computation */

	/*
	* Very first call at application start-up uses half the min
	* delay for quicker notification while still allowing some time
	* before reporting for randomization and to learn about other
	* sources so the report interval will converge to the correct
	* interval more quickly.
	*/
	if (m_initial)
	{
		rtcp_min_time /= 2;
	}
	/*
	* Dedicate a fraction of the RTCP bandwidth to senders unless
	* the number of senders is large enough that their share is
	* more than that fraction.
	*/
	n = m_members;
	if (m_senders <= m_members * RTCP_SENDER_BW_FRACTION)
	{
		if (m_we_sent)
		{
			m_rtcp_bw *= RTCP_SENDER_BW_FRACTION;
			n = m_senders;
		}
		else
		{
			m_rtcp_bw *= RTCP_RCVR_BW_FRACTION;
			n -= m_senders;
		}
	}

	/*
	* The effective number of sites times the average packet size is
	* the total number of octets sent when each site sends a report.
	* Dividing this by the effective bandwidth gives the time
	* interval over which those packets must be sent in order to
	* meet the bandwidth target, with a minimum enforced.  In that
	* time interval we send one report so this time is also our
	* average time between reports.
	*/
	t = m_avg_rtcp_size * n / m_rtcp_bw;
	if (t < rtcp_min_time)
		t = rtcp_min_time;

	/*
	* To avoid traffic bursts from unintended synchronization with
	* other sites, we then pick our actual next report interval as a
	* random number uniformly distributed between 0.5*t and 1.5*t.
	*/
	t = t * ((((double)rand()) / RAND_MAX) + 0.5);
	t = t / COMPENSATION;

	return (t * 1000);
}
//--------------------------------------
//
//--------------------------------------
void rtcp_monitor_t::expire_event(rtcp_event_t e)
{
	/* This function is responsible for deciding whether to send an
	* RTCP report or BYE packet now, or to reschedule transmission.
	* It is also responsible for updating the pmembers, initial, tp,
	* and avg_rtcp_size state variables.  This function should be
	* called upon expiration of the event timer used by Schedule().
	*/

	PLOG_RTP_WRITE("rtcp-mon", "event %s expired...", rtcp_get_event_name(e));

	double t;     /* Interval */
	double tn;    /* Next transmit time */
	double tc;

	/* In the case of a BYE, we use "timer reconsideration" to
	* reschedule the transmission of the BYE if necessary */
	
	if (e == rtcp_event_bye_e)
	{
		t = calc_rtcp_interval();
		tn = m_tp + t;
		
		if (tn <= rtl::DateTime::getTicks64())
		{
			send_bye_event(e);
		}
		/*else
		{
			schedule(0, e);
		}*/

	}
	else if (e == rtcp_event_report_e)
	{
		t = calc_rtcp_interval();
		tn = m_tp + t;
		if (tn <= (tc = (double)rtl::DateTime::getTicks64()))
		{
			int SentPacketSize = send_rtcp_report(e);

			m_avg_rtcp_size = (1./16.)*SentPacketSize + (15./16.)*(m_avg_rtcp_size);
			m_tp = (uint64_t)tc;

			/* We must redraw the interval.  Don't reuse the
			one computed above, since its not actually
			distributed the same, as we are conditioned
			on it being small enough to cause a packet to
			be sent */

			t = calc_rtcp_interval();

			schedule(t, e);

			m_initial = false;

		}
		else
		{
			schedule(2500.0, e);
		}
		
		m_pmembers = m_members;
	}
}
//--------------------------------------
//
//--------------------------------------
bool rtcp_monitor_t::send_bye_event(rtcp_event_t e)
{
	rtcp_bye_packet_t* bye = NEW rtcp_bye_packet_t();
	int written = 0;
	uint8_t buffer[2048];

	rtl::MutexLock lock(m_sync);

	if (m_event_handler_tx == nullptr)
	{
		return false;
	}

	PLOG_RTP_WRITE("rtcp-mon", "sending BYE packet...");

	if (m_source_local)
	{
		bye->set_ssrc(m_source_local->ssrc);

		// serialize and send the packet
		written = bye->write(buffer, 2047);
	}

	char out[2048];
	out[0] = 0;
	written = bye->dump(out, 2047);
	out[written] = 0;

	if (out[0] != 0 && written > 0)
	{
		PLOG_RTP_WRITE("rtcp-mon", "RTCP PACKET SENDING\r\n%s", out);
	}

	rtcp_packet_t rtcp_packet;
	if (!rtcp_packet.add_packet(bye))
	{
		return false;
	}

	//!!! �������� bye ���������� ������ rtcp_packet_t
	// �� ����� ����� NEW

	//return written > 0 ? m_tx_handler->rtcp_tx((const rtcp_header_t*)buffer, written) > 0 : false;
	// ������� � �����
	
	return m_event_handler_tx->rtcp_session_tx(&rtcp_packet) > 0;
}
//--------------------------------------
//
//--------------------------------------
bool rtcp_monitor_t::send_rtcp_report(rtcp_event_t e)
{
	PLOG_RTP_WRITE("rtcp-mon", "sending report packet...");

	if (m_source_local == nullptr)
	{
		return false;
	}

	rtl::MutexLock lock(m_sync);

	if (m_initial)
	{
		// Send Receiver report (manadatory to be the first on)
		rtcp_packet_t packet;
		rtcp_rr_packet_t* rr = NEW rtcp_rr_packet_t;
		rr->set_ssrc(m_source_local->ssrc);
		packet.add_packet(rr);
		// serialize and send the packet
		return send_packet(&packet) > 0;
	}

	rtcp_sr_packet_t* sr = NEW rtcp_sr_packet_t;

	uint64_t ntp_now = rtl::DateTime::getNTP();
	uint64_t time_now = rtl::DateTime::getTicks64();
	rtcp_report_t rblock;
	rtcp_source_t* source;
	//tsk_list_item_t *item;
	bool packet_lost = false;
			
	// sender info
	sr->set_ssrc(m_source_local->ssrc);
	sr->set_ntp_timestamp(ntp_now >> 32);
	sr->set_ntp_fraction(ntp_now & 0xFFFFFFFF);
	sr->set_packets_sent(m_packets_count);
	sr->set_octets_sent(m_octets_count);
	sr->set_rtp_timestamp(rtl::DateTime::getTicks());

	// report blocks
	for (int i = 0; i < m_sources.getCount(); i++)
	{
		source = m_sources[i];

		if (!rtcp_source_is_probed(source))
		{
			continue;
		}
				
		memset(&rblock, 0, sizeof rblock);
		if (true)
		{
			uint32_t expected, expected_interval, received_interval, lost_interval;
					
			rblock.ssrc = source->ssrc;
			// RFC 3550 - A.3 Determining Number of Packets Expected and Lost
			expected = (source->cycles + source->max_seq) - source->base_seq + 1;
			expected_interval = expected - source->expected_prior;
			source->expected_prior = expected;
			received_interval = source->received - source->received_prior;
			source->received_prior = source->received;
			lost_interval = expected_interval - received_interval;
					
			if (expected_interval == 0 || lost_interval <= 0)
			{
				rblock.fraction_lost = 0;
			}
			else
			{
				rblock.fraction_lost = (lost_interval << 8) / expected_interval;
			}

			uint32_t packets_lost = (expected - source->received);
			uint8_t* packets_lost_ptr = (uint8_t*)&packets_lost;
			rblock.packets_lost[0] = packets_lost_ptr[2];
			rblock.packets_lost[1] = packets_lost_ptr[1];
			rblock.packets_lost[2] = packets_lost_ptr[0];
					
			if (!packet_lost && rblock.fraction_lost)
			{
				packet_lost = true;
			}
					
			rblock.last_seq = ((source->cycles & 0xFFFF) << 16) | source->max_seq;
			rblock.jitter = (uint32_t)source->jitter;
			rblock.lsr = ((source->ntp_msw & 0xFFFF) << 16) | ((source->ntp_lsw & 0xFFFF0000) >> 16);
					
			if(source->dlsr)
			{
				rblock.dlsr = (uint32_t)(((time_now - source->dlsr) * 65536) / 1000); // in units of 1/65536 seconds
			}
					
			sr->add_report(rblock);					
		}
	}

	// RFC 4885 - 3.1. Compound RTCP Feedback Packets
	// The FB message(s) MUST be placed in the compound packet after RR and
	// SDES RTCP packets defined in [1].  The ordering with respect to other
	// RTCP extensions is not defined.
	// RFC 5104 Full Intra Request (FIR)
	if (packet_lost)
	{
	}

	rtcp_packet_t packet;
	packet.add_packet(sr);
	
	// serialize and send the packet
	
	return send_packet(&packet) > 0;
}
//--------------------------------------
// Returns true if the packet is from a member or not
// also checks all csrc
//--------------------------------------
bool rtcp_monitor_t::is_new_member_rtp(const rtp_packet* packet)
{
	//for (int i = 0; i < packet->cc && i < sizeof(packet->csrc) / sizeof(packet->csrc[0]); i++)
	for (int i = 0; i < packet->get_csrc_count(); i++)
	{
		if (!have_source(packet->get_csrc_at(i)))
		{
			return false;
		}
	}

	return !have_source(packet->get_ssrc());
}
//--------------------------------------
//
//--------------------------------------
bool rtcp_monitor_t::is_new_member_rtcp(const rtcp_base_packet_t* packet)
{
	uint32_t ssrc = 0;
	
	switch(packet->getType())
	{
	case rtcp_type_rr_e:
	case rtcp_type_sr_e:
		ssrc = packet->get_ssrc();
		break;
	case rtcp_type_bye_e:
		{
			const rtcp_bye_packet_t* bye = (const rtcp_bye_packet_t*)packet;

			for(int i = 0; i < bye->get_source_count(); ++i)
			{
				if (!have_source(bye->get_source(i)))
				{
					return false;
				}
			}

			return true;
		}
	default:
		return false;
	}

	return !have_source(ssrc);
}
//--------------------------------------
//
//--------------------------------------
int rtcp_monitor_t::add_member_rtp(const rtp_packet* packet, bool sender)
{
	int count = 0;
		
	if (add_source(packet->get_ssrc(), packet->get_sequence_number(), packet->getTimestamp()))
		count++;
		
	//for (int i = 0; i < packet->cc && i < sizeof(packet->csrc) / sizeof(packet->csrc[0]); i++)
	for (int i = 0; i < packet->get_csrc_count(); i++)
	{
		if (add_source(packet->get_csrc_at(i), 0, 0))
			count++;
	}

	return count;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_monitor_t::add_member_rtcp(const rtcp_packet_t* packet, bool sender)
{
	int count = add_member_by_packet(packet->get_lead_packet(), sender);

	if (count > 0)
	{
		for (int i = 1; i < packet->get_packet_count(); i++)
		{
			add_member_by_packet(packet->get_packet_at(i), sender);
		}
	}

	return count;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_monitor_t::add_member_by_packet(const rtcp_base_packet_t* packet, bool sender)
{
	int count = 0;

	switch (packet->getType())
	{
	case rtcp_type_rr_e:
		{
			const rtcp_rr_packet_t* rr = (rtcp_rr_packet_t*)packet;
			if (add_source(rr->get_ssrc(), 0, 0))
				++count;

			for (int i = 0; i < rr->get_report_count(); i++)
			{
				const rtcp_report_t& rreport = rr->getAt(i);
				if (add_source(rreport.ssrc, 0, 0))
					++count;
			}

			break;
		}
	case rtcp_type_sr_e:
		{
			const rtcp_sr_packet_t* sr = (rtcp_sr_packet_t*)packet;
			if (add_source(sr->get_ssrc(), 0, 0))
				++count;

			for (int i = 0; i < sr->get_report_count(); i++)
			{
				const rtcp_report_t& sreport = sr->getAt(i);
				if (add_source(sreport.ssrc, 0, 0))
					++count;
			}

			break;
		}
	}

	return count;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_monitor_t::remove_member_rtp(const rtp_packet* packet)
{
	int count = 0;
	
	if (remove_source(packet->get_ssrc()))
		++count;

	//for (int i = 0; i < packet->cc && i < sizeof(packet->csrc) / sizeof(packet->csrc[0]); i++)
	for (int i = 0; i < packet->get_csrc_count(); i++)
	{
		if (remove_source(packet->get_csrc_at(i)))
			++count;
	}

	return count;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_monitor_t::remove_member_rtcp(const rtcp_packet_t* packet)
{
	int count = remove_member_by_packet(packet->get_lead_packet());

	if (count > 0)
	{
		for (int i = 1; i < packet->get_packet_count(); i++)
		{
			remove_member_by_packet(packet->get_packet_at(i));
		}
	}

	return count;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_monitor_t::remove_member_by_packet(const rtcp_base_packet_t* packet)
{
	int count = 0;

	switch (packet->getType())
	{
	case rtcp_type_rr_e:
		{
			const rtcp_rr_packet_t* rr = (const rtcp_rr_packet_t*)packet;
			if (remove_source(rr->get_ssrc()))
				++count;
				
			for (int i = 0; i < rr->get_report_count(); i++)
			{
				const rtcp_report_t& rreport = rr->getAt(i);
				if (remove_source(rreport.ssrc))
					++count;
			}

			break;
		}
	case rtcp_type_sr_e:
		{
			const rtcp_sr_packet_t* sr = (const rtcp_sr_packet_t*)packet;
			if (remove_source(sr->get_ssrc()))
				++count;
				
			for (int i = 0; i < sr->get_report_count(); i++)
			{
				const rtcp_report_t& sreport = sr->getAt(i);
				if (remove_source(sreport.ssrc))
					++count;
			}

			break;
		}
	}
	
	return count;
}
//--------------------------------------
//
//--------------------------------------
static const char* rtcp_get_event_name(rtcp_event_t e)
{
	static const char* names[] = { "rtcp_event_bye_e", "rtcp_event_report_e", "rtcp_event_rtp_e" };
	return e >= rtcp_event_bye_e && e <= rtcp_event_rtp_e ? names[e] : "rtcp_event_unknown_e";
}
//--------------------------------------
//
//--------------------------------------
bool rtcp_monitor_t::recalc_packet(rtcp_packet_t* packet)
{
	if (packet == nullptr || m_source_local == nullptr)
	{
		return false;
	}

	uint32_t ssrc = m_source_local->ssrc;

	for (int i = 0; i < packet->get_packet_count(); i++)
	{
		rtcp_base_packet_t* pckt = packet->get_packet_at(i);
		uint32_t old_ssrc = pckt->get_ssrc();
		pckt->set_ssrc(ssrc);
		PLOG_CALL("RTCP", "rtcp_base_packet_t --- old ssrc (%10u), new ssrc (%10u)", old_ssrc, ssrc);

		if (pckt->getType() == rtcp_type_t::rtcp_type_rr_e)
		{
			PLOG_CALL("RTCP", "rtcp_type_rr_e");
			rtcp_rr_packet_t* rr_pckt = (rtcp_rr_packet_t*)pckt;
			for (int j = 0; j < rr_pckt->get_report_count(); j++)
			{
				rtcp_report_t* report = (rtcp_report_t*)&rr_pckt->getAt(j);
				uint32_t ssrc_op = find_oposite_ssrc(report->ssrc);
				PLOG_CALL("RTCP", "rtcp_type_rr_e --- rtcp_report_t: old ssrc (%10u), new ssrc (%10u)", report->ssrc, ssrc_op);
				report->ssrc = ssrc_op;
			}
		}
		else if (pckt->getType() == rtcp_type_t::rtcp_type_sr_e)
		{
			PLOG_CALL("RTCP", "rtcp_type_sr_e");
			rtcp_sr_packet_t* sr_pckt = (rtcp_sr_packet_t*)pckt;
			for (int j = 0; j < sr_pckt->get_report_count(); j++)
			{
				rtcp_report_t* report = (rtcp_report_t*)&sr_pckt->getAt(j);
				uint32_t ssrc_op = find_oposite_ssrc(report->ssrc);
				PLOG_CALL("RTCP", "rtcp_type_sr_e --- rtcp_report_t: old ssrc (%10u), new ssrc (%10u)", report->ssrc, ssrc_op);
				report->ssrc = ssrc_op;
			}
		}
		else if (pckt->getType() == rtcp_type_t::rtcp_type_bye_e)
		{
			PLOG_CALL("RTCP", "rtcp_type_bye_e");
			//rtcp_bye_packet_t* bye_pckt = (rtcp_bye_packet_t*)pckt;
			/*for (int j = 0; j < bye_pckt->get_source_count(); j++)
			{
			uint32_t source = bye_pckt->get_source(j);
			source = m_ssrc_rx;
			}*/
		}
		else if (pckt->getType() == rtcp_type_t::rtcp_type_app_e)
		{
			PLOG_CALL("RTCP", "rtcp_type_app_e");
			//rtcp_app_packet_t* app_pckt = (rtcp_app_packet_t*)pckt;
			//app_pckt->set_signature(m_ssrc_rx);
		}
		else if (pckt->getType() == rtcp_type_t::rtcp_type_sdes_e)
		{
			PLOG_CALL("RTCP", "rtcp_type_sdes_e");
			rtcp_sdes_packet_t* sdes_pckt = (rtcp_sdes_packet_t*)pckt;

			PLOG_CALL("RTCP", "rtcp_sdes_packet_t --- old ssrc (%10u), new ssrc (%10u)", old_ssrc, ssrc);
			int index_old = sdes_pckt->get_chunk_index(old_ssrc);
			int index_new = sdes_pckt->add_chunk(ssrc);

			for (int k = 0; k < sdes_pckt->get_chunk_item_count(index_old); k++)
			{
				const rtcp_sdes_item_t* sdes_item = sdes_pckt->get_chunk_item_at(index_old, k);
				if (sdes_item == nullptr)
				{
					continue;
				}

				PLOG_CALL("RTCP", "rtcp_sdes_packet_t --- rtcp_sdes_item_t: type (%s), value (%s)", get_rtcp_sdes_type_name((rtcp_sdes_type_t)sdes_item->type), sdes_item->data);
				if (sdes_item->type == rtcp_sdes_type_t::rtcp_sdes_cname_e)
				{
					sdes_pckt->add_chunk_item(index_new, rtcp_sdes_type_t::rtcp_sdes_cname_e, m_cname, MD5_HASH_SIZE);
					PLOG_CALL("RTCP", "rtcp_sdes_packet_t --- rtcp_sdes_item_t: type (%s), new value (%s)", get_rtcp_sdes_type_name((rtcp_sdes_type_t)sdes_item->type), m_cname);
				}
				else
				{
					sdes_pckt->add_chunk_item(index_new, (rtcp_sdes_type_t)sdes_item->type, sdes_item->data, sdes_item->length);
				}
			}
			sdes_pckt->remove_chunk(index_old);
		}
		else if (pckt->getType() == rtcp_type_t::rtcp_type_rtpfb_e)
		{
			PLOG_CALL("RTCP", "rtcp_type_rtpfb_e");
			rtcp_rtpfb_packet_t* rtpfb_pckt = (rtcp_rtpfb_packet_t*)pckt;
			uint32_t ssrc_op = find_oposite_ssrc(rtpfb_pckt->get_media_source());
			PLOG_CALL("RTCP", "rtcp_rtpfb_packet_t --- old ssrc (%10u), new ssrc (%10u)", rtpfb_pckt->get_media_source(), ssrc_op);
			rtpfb_pckt->set_media_source(ssrc_op);

			for (int k = 0; k < rtpfb_pckt->get_entry_count(); k++)
			{
				PLOG_CALL("RTCP", "rtpfb entry type: %d", rtpfb_pckt->get_fb_type());
				if (rtcp_rtpfb_format_t::rtcp_rtpfb_tmmbn_e == rtpfb_pckt->get_fb_type() || rtcp_rtpfb_format_t::rtcp_rtpfb_tmmbr_e == rtpfb_pckt->get_fb_type())
				{
					rtcp_rtpfb_tmm_t* tmm = (rtcp_rtpfb_tmm_t*)rtpfb_pckt->get_tmm_at(k);
					if (tmm == nullptr)
					{
						continue;
					}
					PLOG_CALL("RTCP", "rtcp_rtpfb_packet_t --- rtcp_rtpfb_tmm_t: old ssrc (%10u), new ssrc (%10u)", tmm->ssrc, ssrc);
					tmm->ssrc = ssrc;
				}/*
				else if (rtcp_rtpfb_format_t::rtcp_rtpfb_transport_cc_e == rtpfb_pckt->get_fb_type())
				{
					rtcp_rtpfb_transport_cc_t* cc = (rtcp_rtpfb_transport_cc_t*)rtpfb_pckt->get_cc_at(k);
					if (cc == nullptr)
					{
						continue;
					}
					PLOG_CALL("RTCP", "rtcp_rtpfb_packet_t --- rtcp_rtpfb_transport_cc_t: old ssrc (%10u), new ssrc (%10u)", cc->ssrc, ssrc);
					cc->ssrc = ssrc;
				}*/
			}
		}
		else if (pckt->getType() == rtcp_type_t::rtcp_type_psfb_e)
		{
			rtcp_psfb_packet_t* psfb_pckt = (rtcp_psfb_packet_t*)pckt;
			PLOG_CALL("RTCP", "rtcp_type_psfb_e --- len:%d", psfb_pckt->getLength());

			uint32_t ssrc_op = find_oposite_ssrc(psfb_pckt->get_media_source());
			PLOG_CALL("RTCP", "rtcp_psfb_packet_t --- old ssrc (%10u), new ssrc (%10u)", psfb_pckt->get_media_source(), ssrc_op);
			psfb_pckt->set_media_source(ssrc_op);

			for (int k = 0; k < psfb_pckt->get_entry_count(); k++)
			{
				PLOG_CALL("RTCP", "psfb entry type: %d", psfb_pckt->get_fb_type());
				rtcp_psfb_fir_t* fir = (rtcp_psfb_fir_t*)psfb_pckt->get_fir_at(k);
				if (fir != nullptr)
				{
					uint32_t ssrc_op2 = find_oposite_ssrc(fir->ssrc);
					PLOG_CALL("RTCP", "rtcp_psfb_packet_t --- rtcp_psfb_fir_t: old ssrc (%10u), new ssrc (%10u)", fir->ssrc, ssrc_op2);
					fir->ssrc = ssrc_op2;
					continue;
				}

				rtcp_psfb_tst_t* tst = (rtcp_psfb_tst_t*)psfb_pckt->get_tst_at(k);
				if (tst != nullptr)
				{
					uint32_t ssrc_op3 = find_oposite_ssrc(tst->ssrc);
					PLOG_CALL("RTCP", "rtcp_psfb_packet_t --- rtcp_psfb_tst_t: old ssrc (%10u), new ssrc (%10u)", tst->ssrc, ssrc_op3);
					tst->ssrc = ssrc_op3;
					continue;
				}

				rtcp_psfb_vbcm_t* vbcm = (rtcp_psfb_vbcm_t*)psfb_pckt->get_vbcm_at(k);
				if (vbcm != nullptr)
				{
					uint32_t ssrc_op4 = find_oposite_ssrc(vbcm->ssrc);
					PLOG_CALL("RTCP", "rtcp_psfb_packet_t --- rtcp_psfb_vbcm_t: old ssrc (%10u), new ssrc (%10u)", vbcm->ssrc, ssrc_op4);
					vbcm->ssrc = ssrc_op4;
					continue;
				}

				rtcp_psfb_rpsi_t* rpsi = (rtcp_psfb_rpsi_t*)psfb_pckt->get_rpsi();
				if (rpsi != nullptr)
				{
					PLOG_CALL("RTCP", "rtcp_psfb_packet_t --- rtcp_psfb_rpsi_t");
					continue;
				}
				rtcp_psfb_app_t* app = (rtcp_psfb_app_t*)psfb_pckt->get_app_at(k);
				if (app != nullptr)
				{
					uint32_t ssrc_op5 = find_oposite_ssrc(ntohl(app->app.remb.ssrc_fb[0]));
					PLOG_CALL("RTCP", "rtcp_psfb_packet_t --- rtcp_psfb_app_t: old ssrc(% 10u), new ssrc(% 10u)", ntohl(app->app.remb.ssrc_fb[0]), ssrc_op5);

					app->app.remb.ssrc_fb[0] = htonl(ssrc_op5);

					continue;
				}
			}
		}
		else if (pckt->getType() == rtcp_type_t::rtcp_type_avb_e)
		{
			PLOG_CALL("RTCP", "rtcp_type_avb_e");
		}
		else if (pckt->getType() == rtcp_type_t::rtcp_type_xr_e)
		{
			PLOG_CALL("RTCP", "rtcp_type_xr_e");
			rtcp_xr_packet_t* xr_pckt = (rtcp_xr_packet_t*)pckt;
			if (xr_pckt->getType() == rtcp_xr_block_type_t::rtcp_xr_loss_rle_e)
			{
				rtcp_xr_loss_rle_t* loss_rle = xr_pckt->get_loss_rle();
				if (loss_rle != nullptr)
				{
					PLOG_CALL("RTCP", "rtcp_xr_packet_t --- rtcp_xr_loss_rle_t: old ssrc (%10u), new ssrc (%10u)", loss_rle->ssrc, ssrc);
					loss_rle->ssrc = ssrc;
				}
			}
			else if (xr_pckt->getType() == rtcp_xr_block_type_t::rtcp_xr_dublicate_rle_e)
			{
				rtcp_xr_duplicate_rle_t* dublicate_rle = xr_pckt->get_duplicate_rle();
				if (dublicate_rle != nullptr)
				{
					PLOG_CALL("RTCP", "rtcp_xr_packet_t --- rtcp_xr_duplicate_rle_t: old ssrc (%10u), new ssrc (%10u)", dublicate_rle->ssrc, ssrc);
					dublicate_rle->ssrc = ssrc;
				}
			}
			else if (xr_pckt->getType() == rtcp_xr_block_type_t::rtcp_xr_receipt_times_e)
			{
				rtcp_xr_receipt_times_t* receipt_times = xr_pckt->get_receipt_times();
				if (receipt_times != nullptr)
				{
					PLOG_CALL("RTCP", "rtcp_xr_packet_t --- rtcp_xr_receipt_times_t: old ssrc (%10u), new ssrc (%10u)", receipt_times->ssrc, ssrc);
					receipt_times->ssrc = ssrc;
				}
			}
			else if (xr_pckt->getType() == rtcp_xr_block_type_t::rtcp_xr_dlrr_e)
			{
				rtcp_xr_dlrr_t* dlrr = xr_pckt->get_dlrr();
				if (dlrr != nullptr)
				{
					int real_size = (dlrr->block_length + 1) * sizeof(uint32_t);
					int sub_blocks_size = real_size - sizeof(rtcp_xr_header_t);
					int sub_blocks_count = sub_blocks_size / sizeof(rtcp_xr_dlrr_sub_block_t);
					for (int d = 0; d < sub_blocks_count; d++)
					{
						PLOG_CALL("RTCP", "rtcp_xr_packet_t --- rtcp_xr_dlrr_t: %d old ssrc (%10u), new ssrc (%10u)", d, dlrr->sub_blocks[d].ssrc, ssrc);
						dlrr->sub_blocks[d].ssrc = ssrc;
					}
				}
			}
			else if (xr_pckt->getType() == rtcp_xr_block_type_t::rtcp_xr_stat_sum_e)
			{
				rtcp_xr_stat_sum_t* stat_sum = xr_pckt->get_stat_sum();
				if (stat_sum != nullptr)
				{
					PLOG_CALL("RTCP", "rtcp_xr_packet_t --- rtcp_xr_stat_sum_t: old ssrc (%10u), new ssrc (%10u)", stat_sum->ssrc, ssrc);
					stat_sum->ssrc = ssrc;
				}
			}
			else if (xr_pckt->getType() == rtcp_xr_block_type_t::rtcp_xr_voip_metrics_e)
			{
				rtcp_xr_voip_metrics_t* voip_metrics = xr_pckt->get_voip_metrics();
				if (voip_metrics != nullptr)
				{
					PLOG_CALL("RTCP", "rtcp_xr_packet_t --- rtcp_xr_voip_metrics_t: old ssrc (%10u), new ssrc (%10u)", voip_metrics->ssrc, ssrc);
					voip_metrics->ssrc = ssrc;
				}
				break;
			}
		}
		else if (pckt->getType() == rtcp_type_t::rtcp_type_invalid_e)
		{
			PLOG_CALL("RTCP", "rtcp_type_invalid_e");
		}
		else
		{
			PLOG_CALL("RTCP", "unknown");
		}
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
void rtcp_monitor_t::print(rtl::Logger* log)
{
	if (log == nullptr)
	{
		return;
	}

	uint64_t time_now = rtl::DateTime::getTicks64();
	const int size = 1024;
	int wsize = 0;
	char buff[size] = { 0 };

	wsize += std_snprintf(buff + wsize, size - wsize, "Context = %s, Termination = %s (%s)\n", m_context, m_termination, m_addr);
	//wsize += std_snprintf(buff + wsize, size - wsize, "\t----------OUT------------\n");
	if (m_source_local == nullptr)
	{
		wsize += std_snprintf(buff + wsize, size - wsize, "\tSSRC:  -  \n");
	}
	else
	{
		wsize += std_snprintf(buff + wsize, size - wsize, "\tSSRC:%08X(%10u)\n", m_source_local->ssrc, m_source_local->ssrc);
	}
	wsize += std_snprintf(buff + wsize, size - wsize, "\tpackets sent     : %d\n", m_packets_count);
	wsize += std_snprintf(buff + wsize, size - wsize, "\toctets sent      : %d\n", m_octets_count);


	// report blocks
	for (int i = 0; i < m_sources.getCount(); i++)
	{
		rtcp_source_t* source = m_sources[i];

		if (!rtcp_source_is_probed(source))
		{
			continue;
		}

		wsize += std_snprintf(buff + wsize, size - wsize, "\t-------------------------\n");
		wsize += std_snprintf(buff + wsize, size - wsize, "\tSSRC:%08X(%10u)\n", source->ssrc, source->ssrc);
		wsize += std_snprintf(buff + wsize, size - wsize, "\tpackets sent     : %d\n", source->packets_sent);
		wsize += std_snprintf(buff + wsize, size - wsize, "\toctets sent      : %d\n", source->octets_sent);
		wsize += std_snprintf(buff + wsize, size - wsize, "\tpackets received : %d\n", source->received);

		uint32_t expected = (source->cycles + source->max_seq) - source->base_seq + 1;
		//uint32_t expected_interval = expected - source->expected_prior;
		//uint32_t received_interval = source->received - source->received_prior;
		//uint32_t lost_interval = expected_interval - received_interval;

		uint32_t packets_lost = (expected - source->received);
		wsize += std_snprintf(buff + wsize, size - wsize, "\tlost             : %d\n", packets_lost);
		wsize += std_snprintf(buff + wsize, size - wsize, "\tjitter           : %d\n", (uint32_t)source->jitter);
		
		if (source->dlsr)
		{
			wsize += std_snprintf(buff + wsize, size - wsize, "\tdelays (ms)      : %lld\n", time_now - source->dlsr);
		}
		else
		{
			wsize += std_snprintf(buff + wsize, size - wsize, "\tdelays           : 0\n");
		}
	}


	buff[wsize + 1] = 0;

	log->log("RTCPSTAT", buff);
}
//--------------------------------------
//
//--------------------------------------
void rtcp_monitor_t::set_triple(const mg_rtcp_triple_list* triple_list)
{
	for (int i = 0; i < triple_list->getCount(); i++)
	{
		rtcp_triple_t tr1 = triple_list->getAt(i);
		rtcp_triple_t tr;
		tr.ssrc = tr1.ssrc;
		tr.ssrc_opposite = tr1.ssrc_opposite;
		memcpy(tr.cname, tr1.cname, MD5_HASH_HEX_SIZE);
		tr.cname[MD5_HASH_HEX_SIZE] = 0;
		m_triple_list.add(tr);
	}
}
uint32_t rtcp_monitor_t::find_oposite_ssrc(uint32_t ssrc)
{
	for (int i = 0; i < m_triple_list.getCount(); i++)
	{
		rtcp_triple_t tr = m_triple_list.getAt(i);
		if (tr.ssrc == ssrc)
		{
			return tr.ssrc_opposite;
		}
	}

	return m_ssrc_opposite;
}
