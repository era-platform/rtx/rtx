﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 //--------------------------------------
// Implementation of hmac auth_type_t
//--------------------------------------
// based on libsrtp written by
// David A. McGrew
// Cisco Systems, Inc.
//--------------------------------------
#include "stdafx.h"

#include "srtp_hmac.h" 
//--------------------------------------
//
//--------------------------------------
hmac_ctx_t::hmac_ctx_t(int key_len, int out_len) : auth_t(key_len, out_len)
{
	memset(m_opad, 0, sizeof m_opad);
}
//--------------------------------------
//
//--------------------------------------
hmac_ctx_t::~hmac_ctx_t()
{
}
//--------------------------------------
//
//--------------------------------------
bool hmac_ctx_t::create(const uint8_t *key, int key_len)
{
	int i;
	uint8_t ipad[64]; 

	/*
	* check key length - note that we don't support keys larger
	* than 20 bytes yet
	*/
	if (key_len > 20)
		return false;

	/*
	* set values of ipad and opad by exoring the key into the
	* appropriate constant values
	*/
	for (i = 0; i < key_len; i++)
	{
		ipad[i] = key[i] ^ 0x36;
		m_opad[i] = key[i] ^ 0x5c;
	}

	/* set the rest of ipad, opad to constant values */
	for (; i < 64; i++)
	{
		ipad[i] = 0x36;
		((uint8_t *)m_opad)[i] = 0x5c;
	}  

	/* initialize sha1 context */
	m_init_ctx.reset();

	/* hash ipad ^ key */
	m_init_ctx.input(ipad, 64);
	memcpy(&m_ctx, &m_init_ctx, sizeof m_ctx);

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool hmac_ctx_t::start()
{
	memcpy(&m_ctx, &m_init_ctx, sizeof m_ctx);
	return true;
}
//--------------------------------------
//
//--------------------------------------
bool hmac_ctx_t::update(const uint8_t *message, int msg_octets)
{
  /* hash message into sha1 context */
	m_ctx.input(message, msg_octets);
	return true;
}
//--------------------------------------
//
//--------------------------------------
bool hmac_ctx_t::compute(const uint8_t* message, int msg_octets, uint8_t* result, int tag_len)
{
	uint32_t hash_value[5];
	uint32_t H[5];
	int i;

	if (tag_len == -1)
		tag_len = m_out_len;

	/* check tag length, return error if we can't provide the value expected */
	if (tag_len > 20)
		return false;

	/* hash message, copy output into H */
	update(message, msg_octets);
	m_ctx.result((uint8_t*)H);

	/*
	* note that we don't need to debug_print() the input, since the
	* function hmac_update() already did that for us
	*/
	//debug_print(mod_hmac, "intermediate state: %s", octet_string_hex_string((uint8_t *)H, 20));

	/* re-initialize hash context */
	m_ctx.reset();

	/* hash opad ^ key  */
	m_ctx.input((uint8_t *)m_opad, 64);

	/* hash the result of the inner hash */
	m_ctx.input((uint8_t *)H, 20);

	/* the result is returned in the array hash_value[] */
	m_ctx.result((uint8_t*)hash_value);

	/* copy hash_value to *result */
	for (i = 0; i < tag_len; i++)
	{
		result[i] = ((uint8_t *)hash_value)[i];
	}

	//debug_print(mod_hmac, "output: %s", octet_string_hex_string((uint8_t *)hash_value, tag_len));

	return true;
}
//--------------------------------------
