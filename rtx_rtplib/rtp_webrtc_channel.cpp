﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtp_webrtc_channel.h"
#include "rtcp_packet.h"
#include "srtp.h"
#include "sdp_fingerprint.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define LOG_PREFIX "webrtc"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtp_webrtc_channel_t::rtp_webrtc_channel_t(rtl::Logger* log, rtp_channel_type_t type) :
rtp_channel_t(log), m_log(log), m_type(type),
m_data_socket(log), m_control_socket(log), m_data_video_socket(log), m_control_video_socket(log)

{
	m_socket_io_service = nullptr;

	m_data_port = 0;
	m_ctrl_port = 0;
	m_data_video_port = 0;
	m_ctrl_video_port = 0;

	m_lock.clear();

	m_started = false;

	m_tx_subscriber_count = 0;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtp_webrtc_channel_t::~rtp_webrtc_channel_t()
{
	destroy();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtp_channel_type_t rtp_webrtc_channel_t::getType() const
{
	return m_type;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtp_webrtc_channel_t::bind_audio_socket(const ip_address_t* iface, uint16_t data_port)
{
	if (iface == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX,"bind_data_socket -- invalid address");
		return false;
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "bind_data_socket -- binding socket on %s, %u ...", iface->to_string(), data_port);

	m_data_socket.close();

	if (m_data_socket.create(iface->get_family(), e_ip_protocol_type::E_IP_PROTOCOL_UDP))
	{
		socket_address saddr(iface, data_port);

		if (m_data_socket.bind(&saddr))
		{
			PLOG_RTP_WRITE(LOG_PREFIX,"bind_data_socket -- ok");
			
			m_interface.copy_from(iface);
			m_data_port = m_data_socket.get_local_address()->get_port();

			char name[64] = { 0 };
			int len = std_snprintf(name, 64, "webrtc_channel:%s(%u)", m_interface.to_string(), m_data_port);
			name[len] = 0;
			rtp_channel_t::set_name(name);
			m_subscribers.update_channel_name(name);

			int bsize = 64 * 1024;
			m_data_socket.set_option(SOL_SOCKET, SO_RCVBUF, &bsize, sizeof(int));
			m_data_socket.set_option(SOL_SOCKET, SO_SNDBUF, &bsize, sizeof(int));

			return true;
		}
		else
		{
			PLOG_RTP_ERROR(LOG_PREFIX,"bind_data_socket -- net_socket.bind(%u) failed", data_port);
		}
	}
	else
	{
		PLOG_RTP_ERROR(LOG_PREFIX,"bind_data_socket -- net_socket.create() failed");
	}

	m_data_socket.close();

	return false;
}
//------------------------------------------------------------------------------
bool rtp_webrtc_channel_t::bind_ctrl_audio_socket(const ip_address_t* iface, uint16_t ctrl_port)
{
	if (iface == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "bind_ctrl_socket -- invalid address");
		return false;
	}

	if (ctrl_port == 0)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "bind_ctrl_socket -- invalid ctrl port");
		return false;
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "bind_ctrl_socket -- binding socket on %s, %u ...", iface->to_string(), ctrl_port);

	m_control_socket.close();

	if (m_control_socket.create(iface->get_family(), e_ip_protocol_type::E_IP_PROTOCOL_UDP))
	{
		socket_address saddr(iface, ctrl_port);

		if (m_control_socket.bind(&saddr))
		{
			PLOG_RTP_WRITE(LOG_PREFIX, "bind_ctrl_socket -- ok");

			m_interface.copy_from(iface);
			m_ctrl_port = saddr.get_port();

			char name[64] = { 0 };
			int len = std_snprintf(name, 64, "%s:%u", (char*)rtp_channel_t::getName(), m_ctrl_port);
			//int len_write = std_snprintf((char*)name + len - 1, 64 - len, ":%u)", m_ctrl_port);
			//len += len_write;
			name[len] = 0;
			rtp_channel_t::set_name(name);

			int bsize = 64 * 1024;
			m_control_socket.set_option(SOL_SOCKET, SO_RCVBUF, &bsize, sizeof(int));
			m_control_socket.set_option(SOL_SOCKET, SO_SNDBUF, &bsize, sizeof(int));

			return true;
		}
		else
		{
			PLOG_RTP_ERROR(LOG_PREFIX, "bind_ctrl_socket -- net_socket.bind(%u) failed", ctrl_port);
		}
	}
	else
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "bind_ctrl_socket -- net_socket.create() failed");
	}

	m_control_socket.close();

	return false;
}
//------------------------------------------------------------------------------
bool rtp_webrtc_channel_t::bind_video_socket(const ip_address_t* iface, uint16_t data_port)
{
	if (iface == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "bind_data_video_socket -- invalid address");
		return false;
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "bind_data_video_socket -- binding socket on %s, %u ...", iface->to_string(), data_port);

	m_data_video_socket.close();

	if (m_data_video_socket.create(iface->get_family(), e_ip_protocol_type::E_IP_PROTOCOL_UDP))
	{
		socket_address saddr(iface, data_port);

		if (m_data_video_socket.bind(&saddr))
		{
			PLOG_RTP_WRITE(LOG_PREFIX, "bind_data_video_socket -- ok");
			m_data_video_port = m_data_video_socket.get_local_address()->get_port();

			int bsize = 64 * 1024;
			m_data_video_socket.set_option(SOL_SOCKET, SO_RCVBUF, &bsize, sizeof(int));
			m_data_video_socket.set_option(SOL_SOCKET, SO_SNDBUF, &bsize, sizeof(int));

			return true;
		}
		else
		{
			PLOG_RTP_ERROR(LOG_PREFIX, "bind_data_video_socket -- net_socket.bind(%u) failed", data_port);
		}
	}
	else
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "bind_data_video_socket -- net_socket.create() failed");
	}

	m_data_video_socket.close();

	return false;
}
//------------------------------------------------------------------------------
bool rtp_webrtc_channel_t::bind_ctrl_video_socket(const ip_address_t* iface, uint16_t ctrl_port)
{
	if (iface == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "bind_ctrl_video_socket -- invalid address");
		return false;
	}

	if (ctrl_port == 0)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "bind_ctrl_video_socket -- invalid ctrl port");
		return false;
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "bind_ctrl_video_socket -- binding socket on %s, %u ...", iface->to_string(), ctrl_port);

	m_control_video_socket.close();

	if (m_control_video_socket.create(iface->get_family(), e_ip_protocol_type::E_IP_PROTOCOL_UDP))
	{
		socket_address saddr(iface, ctrl_port);

		if (m_control_video_socket.bind(&saddr))
		{
			PLOG_RTP_WRITE(LOG_PREFIX, "bind_ctrl_video_socket -- ok");
			m_ctrl_video_port = saddr.get_port();

			int bsize = 64 * 1024;
			m_control_video_socket.set_option(SOL_SOCKET, SO_RCVBUF, &bsize, sizeof(int));
			m_control_video_socket.set_option(SOL_SOCKET, SO_SNDBUF, &bsize, sizeof(int));

			return true;
		}
		else
		{
			PLOG_RTP_ERROR(LOG_PREFIX, "bind_ctrl_video_socket -- net_socket.bind(%u) failed", ctrl_port);
		}
	}
	else
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "bind_ctrl_video_socket -- net_socket.create() failed");
	}

	m_control_video_socket.close();

	return false;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtp_webrtc_channel_t::init()
{
	return m_subscribers.init(m_log, this);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtp_webrtc_channel_t::destroy()
{
	stop_io();

	m_data_socket.close();
	m_control_socket.close();
	m_data_video_socket.close();
	m_control_video_socket.close();

	return m_tx_subscriber_count == 0;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtp_webrtc_channel_t::started()
{
	return m_started;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int rtp_webrtc_channel_t::start_io(socket_io_service_t* iosvc)
{
	if (m_started)
	{
		//PLOG_CALL(LOG_PREFIX, "start_io -- already started");
		return 0;
	}

	if (iosvc == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX,"start_io -- invalid io service");
		return -1;
	}

	m_socket_io_service = iosvc;

	insert_stat(this);

	if (!m_socket_io_service->add(&m_data_socket, this))
	{
		return -2;
	}
	if (!m_socket_io_service->add(&m_control_socket, this))
	{
		return -3;
	}
	if (!m_socket_io_service->add(&m_data_video_socket, this))
	{
		return -4;
	}
	if (!m_socket_io_service->add(&m_control_video_socket, this))
	{
		return -5;
	}

	m_started = true;

	return 1;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_webrtc_channel_t::stop_io()
{
	if (m_socket_io_service != nullptr)
	{
		remove_stat(this);

		m_socket_io_service->remove(&m_data_socket);
		m_socket_io_service->remove(&m_control_socket);
		m_socket_io_service->remove(&m_data_video_socket);
		m_socket_io_service->remove(&m_control_video_socket);

		m_started = false;

		m_socket_io_service = nullptr;
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const ip_address_t* rtp_webrtc_channel_t::get_net_interface() const
{
	return &m_interface;
}
const uint16_t rtp_webrtc_channel_t::get_data_port() const
{
	return m_data_port;
}
uint16_t rtp_webrtc_channel_t::get_ctrl_port() const
{
	return m_ctrl_port;
}
uint16_t rtp_webrtc_channel_t::get_video_port() const
{
	return m_data_video_port;
}
uint16_t rtp_webrtc_channel_t::get_ctrl_video_port() const
{
	return m_ctrl_video_port;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const socket_io_service_t* rtp_webrtc_channel_t::get_io_service() const
{
	return m_socket_io_service;
}
void rtp_webrtc_channel_t::set_io_service(socket_io_service_t* svc)
{
	m_socket_io_service = svc;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const rtp_webrtc_subscriber_t* rtp_webrtc_channel_t::get_event_subscriber(uint32_t ssrc)
{
	const rtp_webrtc_subscriber_t* sub = nullptr;

	int count = 3;
	while (count > 0)
	{
		if (m_lock.test_and_set(std::memory_order_acquire))
		{
			count--;
			if (count == 0)
			{
				PLOG_RTP_WRITE(LOG_PREFIX, "get_event_subscriber -- ID(%s) timeout.", (const char*)getName());
			}
			continue;
		}

		sub = m_subscribers.get_subscriber(ssrc);

		m_lock.clear(std::memory_order_release);
		break;
	}

	return sub;
}
//------------------------------------------------------------------------------
void rtp_webrtc_channel_t::set_event_handler_rx(uint32_t ssrc, i_rx_channel_event_handler* handler)
{
	bool go = true;
	while (go)
	{
		if (m_lock.test_and_set(std::memory_order_acquire))
		{
			continue;
		}

		m_subscribers.create_subscriber(ssrc, handler);
		PLOG_RTP_WRITE(LOG_PREFIX, "set_event_handler_rx -- ID(%s) add subscriber ssrc:%u.", (const char*)getName(), ssrc);

		m_lock.clear(std::memory_order_release);
		go = false;
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtp_webrtc_channel_t::send_packet(const rtp_packet* packet, const socket_address* saddr)
{
	mg_stun_t* stun = m_subscribers.get_stun();
	if (stun != nullptr)
	{
		stun->send_stun_ping();
	}

	if (packet == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX,"send_packet -- invalid packet.");
		return false;
	}

	if (saddr == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX,"send_packet -- invalid address.");
		return false;
	}

	uint32_t ssrc = packet->get_ssrc();
	//PLOG_RTP_WRITE(LOG_PREFIX, "send_packet -- ID(%s) try send ssrc:%u(%10u) to:%s .", (const char*)getName(), ssrc, packet->get_payload_length(), saddr->to_string());

	const rtp_webrtc_subscriber_t* subscriber = get_event_subscriber(ssrc);
	if (subscriber == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "send_packet -- subscriber not found.");
		return false;
	}

	uint8_t buffer[rtp_packet::PAYLOAD_BUFFER_SIZE];
	uint32_t length = packet->write_packet(buffer, sizeof(buffer));

	if (length == 0)
	{
		PLOG_RTP_WARNING(LOG_PREFIX, "send_packet -- write packet failed.");
		return false;
	}

	srtp_stream_ctx_t* out = subscriber->get_srtp_filter_out();
	int length_int = int(length);
	if (out == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "send_packet -- ID(%s) srtp_filter_out is null! ssrc:%u(%10u) to:%s .", (const char*)getName(), ssrc, packet->get_payload_length(), saddr->to_string());
        return false;
	}
	if (!out->srtp_pack((void*)buffer, &length_int))
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "send_packet -- ID(%s) srtp pack failed! seq:%5u ssrc:%u(%10u) to:%s .",
			(const char*)getName(), packet->get_sequence_number(), ssrc, packet->get_payload_length(), saddr->to_string());
		return false;
	}

	//PLOG_RTP_WRITE(LOG_PREFIX, " sending -- ID(%s) ssrc:%10u pt:%3u seq:%5u (%6u)   to:%s .", (const char*)getName(), ssrc,
	//	packet->get_payload_type(), packet->get_sequence_number(), length_int, saddr->to_string());

	const rtp_header_t* hdr = (const rtp_header_t*)buffer;
	PLOG_RTP_FLOW("WebRTC", "RTP  SEND(%5u) v:%1u p:%1u e:%1u cc:%1u m:%1u pt:%3u seq:%5u ts:%10u ssrc:%10u   to %s", length_int,
		hdr->ver, hdr->pad, hdr->ext, hdr->cc, hdr->m, hdr->pt, ntohs(hdr->seq), ntohl(hdr->ts), ntohl(hdr->ssrc),
		saddr->to_string());


	return send_data(buffer, length_int, saddr);
}
//------------------------------------------------------------------------------
bool rtp_webrtc_channel_t::send_data(const uint8_t* data, uint32_t len, const socket_address* saddr)
{
	if (m_socket_io_service == nullptr)
	{
		//PLOG_RTP_ERROR(LOG_PREFIX, "send_data -- socket_io_service is undefined.");
		return false;
	}

	if (!m_socket_io_service->send_data_to(&m_data_socket, data, len, saddr))
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "send_data -- socket_io_service.send_data() failed.");
		return false;
	}

	update_tx_stat(len);

	return true;
}
//------------------------------------------------------------------------------
bool rtp_webrtc_channel_t::send_ctrl(const rtcp_packet_t* packet, const socket_address* saddr)
{
	mg_stun_t* stun = m_subscribers.get_stun();
	if (stun != nullptr)
	{
		stun->send_stun_ping();
	}

	if (packet == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "send_ctrl -- invalid packet.");
		return false;
	}

	if (saddr == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "send_ctrl -- invalid address.");
		return false;
	}

	uint32_t ssrc = packet->get_ssrc();
	//PLOG_RTP_WRITE(LOG_PREFIX, "send_ctrl -- ID(%s) try send ssrc:%u to:%s .", (const char*)getName(), ssrc, saddr->to_string());

	const rtp_webrtc_subscriber_t* subscriber = get_event_subscriber(ssrc);
	if (subscriber == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "send_ctrl -- subscriber not found.");
		return false;
	}

	uint8_t buffer[rtp_packet::PAYLOAD_BUFFER_SIZE];
	uint32_t length = packet->write(buffer, sizeof(buffer));

	if (length == 0)
	{
		PLOG_RTP_WARNING(LOG_PREFIX, "send_ctrl -- write packet failed.");
		return false;
	}

	srtp_stream_ctx_t* out = subscriber->get_srtp_filter_out();
	int length_int = int(length);
	if ((out != nullptr) && (!out->srtcp_pack((void*)buffer, &length_int)))
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "send_ctrl -- srtcp pack failed.");
		return false;
	}

	PLOG_RTP_WRITE(LOG_PREFIX, " sending -- ID(%s) ssrc:%10u pt:%3u seq:----- (%6u)   to:%s .", (const char*)getName(), ssrc,
		packet->getType(), length_int, saddr->to_string());

	if (m_socket_io_service == nullptr)
	{
		//PLOG_RTP_ERROR(LOG_PREFIX, "send_ctrl -- socket_io_service is undefined.");
		return false;
	}

	const rtcp_header_t* rtcp_pack = (const rtcp_header_t*)buffer;
	PLOG_RTP_FLOW("WebRTC", "RTCP SEND(%5u) v:%1u p:%1u rc:%2u        pt:%3u len:%5u               ssrc:%10u   to: %s.", length,
		rtcp_pack->ver, rtcp_pack->pad, rtcp_pack->rc, rtcp_pack->pt, ntohs(rtcp_pack->length), ntohl(rtcp_pack->ssrc),
		saddr->to_string());

	if (!m_socket_io_service->send_data_to(&m_data_socket, buffer, length_int, saddr))
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "send_ctrl -- socket_io_service.send_data() failed.");
		return false;
	}

	update_tx_stat(length_int);

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_webrtc_channel_t::socket_data_available(net_socket* /*sock*/)
{
	PLOG_RTP_WARNING(LOG_PREFIX,"socket_data_available -- empty method");
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_webrtc_channel_t::socket_data_received(net_socket* sock, const uint8_t* data, uint32_t len, const socket_address* from)
{
	if (sock == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX,"socket_data_received -- invalid socket");
		return;
	}

	if (data == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX,"socket_data_received -- invalid data");
		return;
	}

	if (len == 0)
	{
		PLOG_RTP_ERROR(LOG_PREFIX,"socket_data_received -- invalid length");
		return;
	}

	if (from == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX,"socket_data_received -- invalid address");
		return;
	}

	update_rx_stat(len);

	uint8_t b1 = data[0];
	if (b1 == 0 || b1 == 1) // STUN
	{
		m_subscribers.process_stun_message(data, len, from);
	}
	else if (b1 > 19 && b1 < 64)
	{
		mg_stun_t* stun = m_subscribers.get_stun();
		if (stun != nullptr)
		{
			// хром требует пинга для начала дтлс сессии.
			stun->send_stun_ping();
		}
		
		m_subscribers.process_dtls_message(data, len, from);
	}
	else if ((sock == &m_data_socket) || (sock == &m_data_video_socket))
	{
		rtp_data_received(data, len, from);
	}
	else if ((sock == &m_control_socket) || (sock == &m_control_video_socket))
	{
		rtcp_data_received(data, len, from);
	}
	else
	{
		PLOG_RTP_ERROR(LOG_PREFIX,"socket_data_received -- unknown socket");
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_webrtc_channel_t::rtp_data_received(const uint8_t* data, uint32_t len, const socket_address* from)
{
	if (rtcp_packet_t::is_rtcp_packet(data, len))
	{
		rtcp_data_received(data, len, from);
		return;
	}

	//rtp_header_t* rtp_pack = (rtp_header_t*)data;
	rtp_header_t* hdr = (rtp_header_t*)data;
	uint32_t ssrc = ntohl(hdr->ssrc);
	//PLOG_RTP_WRITE(LOG_PREFIX, "rtp_data_received -- ID(%s) try received ssrc:%u(%10u) from %s.", (const char*)getName(), ssrc, len, from->to_string());
	/*PLOG_RTP_WRITE(LOG_PREFIX, "received -- ID(%s) ssrc:%10u pt:%3u seq:%5u (%6u) from:%s .", (const char*)getName(), ssrc,
		rtp_pack->pt, ntohs(rtp_pack->seq), len, from->to_string());*/

	PLOG_RTP_FLOW("WebRTC", "RTP  RECV(%5u) v:%1u p:%1u e:%1u cc:%1u m:%1u pt:%3u seq:%5u ts:%10u ssrc:%10u from:%s", len,
		hdr->ver, hdr->pad, hdr->ext, hdr->cc, hdr->m, hdr->pt, ntohs(hdr->seq), ntohl(hdr->ts), ntohl(hdr->ssrc), from->to_string());

	const rtp_webrtc_subscriber_t* subscriber = get_event_subscriber(ssrc);
	if (subscriber == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "rtp_data_received -- ID(%s) subscriber not found! ssrc:%u(%10u) from %s.", (const char*)getName(), ssrc, len, from->to_string());
		return;
	}

	srtp_stream_ctx_t* in = subscriber->get_srtp_filter_in();
	
	/*if (rtl::Logger::check_trace(TRF_NET))
	{
		PLOG_RTP_FLOW(in != nullptr ? "SRTP" : "RTP",
			"WEBRTC : media rx %5d v:%u p:%u x:%u cc:%2u m:%u pt:%3u seq:%5u ts:%08X ssrc:%08X(%10u) from %s over %s:%u",
			len, rtp_pack->ver, rtp_pack->pad, rtp_pack->ext, rtp_pack->cc, rtp_pack->m, rtp_pack->pt,
			ntohs(rtp_pack->seq), ntohl(rtp_pack->ts), ntohl(rtp_pack->ssrc), ssrc,
			from->to_string(),
			m_interface.to_string(), m_data_port);
	}*/
	
	int length = int(len);

	if (in == nullptr)
    {
        PLOG_RTP_ERROR(LOG_PREFIX, "rtp_data_received -- srtp_filter_in is null!.");
        return;
    }
    if (!in->srtp_unpack((void*)data, &length))
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "rtp_data_received -- srtp_unpack() failed.");
		return;
	}

	rtp_packet packet;
	if (!packet.read_packet(data, length))
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "rtp_data_received -- rtp_packet.read_packet() failed.");
		return;
	}

	/*if (!packet.read_packet(data, length))
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "rtp_data_received -- rtp_packet.read_packet() failed.");
		return;
	}*/

	i_rx_channel_event_handler* stream = subscriber->get_handler();
	if (stream == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "rtp_data_received -- stream is nullptr.");
		return;
	}

	stream->rx_packet_stream(this, &packet, from);

	//PLOG_RTP_WRITE(LOG_PREFIX, "rtp_data_received -- ID(%s) received ssrc:%u(%10u) from %s.", (const char*)getName(), ssrc, length, from->to_string());
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_webrtc_channel_t::rtcp_data_received(const uint8_t* data, uint32_t len, const socket_address* from)
{
	rtcp_header_t* rtcp_pack = (rtcp_header_t*)data;

	uint32_t ssrc = ntohl(rtcp_pack->ssrc);
	//PLOG_RTP_WRITE(LOG_PREFIX, "rtp_data_received -- ID(%s) try received ssrc:%u(%10u) from %s.", (const char*)getName(), ssrc, len, from->to_string());
	/*PLOG_RTP_WRITE(LOG_PREFIX, "received -- ID(%s) ssrc:%10u pt:%3u seq:----- (%6u) from:%s .", (const char*)getName(), ssrc,
		rtcp_pack->pt, len, from->to_string());*/

	PLOG_RTP_FLOW("WebRTC", "RTCP RECV(%5u) v:%1u p:%1u rc:%2u        pt:%3u len:%5u               ssrc:%10u from:%s", len,
		rtcp_pack->ver, rtcp_pack->pad, rtcp_pack->rc, rtcp_pack->pt, ntohs(rtcp_pack->length), ntohl(rtcp_pack->ssrc), from->to_string());

	const rtp_webrtc_subscriber_t* subscriber = get_event_subscriber(ssrc);
	if (subscriber == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "rtcp_data_received -- subscriber not found.");
		return;
	}

	srtp_stream_ctx_t* in = subscriber->get_srtp_filter_in();

	int length = int(len);

	if ((in != nullptr) && (!in->srtcp_unpack((void*)data, &length)))
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "rtcp_data_received -- srtcp unpack failed.");
		return;
	}

	//PLOG_NET_BIN("RTCP", data, length, "data:");

	i_rx_channel_event_handler* stream = subscriber->get_handler();
	if (stream == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "rtcp_data_received -- stream is nullptr.");
		return;
	}

	rtcp_packet_t packet;
	if (packet.read(data, length) != length)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "rtcp_data_received -- packet read fail.");
		return;
	}

	stream->rx_ctrl_stream(this, &packet, from);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_webrtc_channel_t::tx_packet_channel(/*const mg_tx_stream_t* stream, */const rtp_packet* packet, const socket_address* to)
{
	send_packet(packet, to);
}
//------------------------------------------------------------------------------
void rtp_webrtc_channel_t::tx_data_channel(/*const mg_tx_stream_t* stream, */const uint8_t* data, uint32_t len, const socket_address* to)
{
	PLOG_RTP_FLOW("WebRTC", "Data SEND(%5u) to %s", len, to->to_string());

	send_data(data, len, to);
}
//------------------------------------------------------------------------------
void rtp_webrtc_channel_t::tx_ctrl_channel(/*const mg_tx_stream_t* stream, */const rtcp_packet_t* packet, const socket_address* to)
{
	send_ctrl(packet, to);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_webrtc_channel_t::clear_rx_handler(uint32_t ssrc)
{
	set_event_handler_rx(ssrc, nullptr);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_webrtc_channel_t::inc_tx_subscriber(uint32_t ssrc)
{
	bool go = true;
	while (go)
	{
		if (m_lock.test_and_set(std::memory_order_acquire))
		{
			continue;
		}

		m_subscribers.create_subscriber(ssrc, nullptr);
		PLOG_RTP_WRITE(LOG_PREFIX, "inc_tx_subscriber -- ID(%s) add subscriber ssrc:%u.", (const char*)getName(), ssrc);

		m_tx_subscriber_count = std_interlocked_inc(&m_tx_subscriber_count);

		m_lock.clear(std::memory_order_release);
		go = false;
	}
}
//------------------------------------------------------------------------------
void rtp_webrtc_channel_t::dec_tx_subscriber(uint32_t ssrc)
{
	bool go = true;
	while (go)
	{
		if (m_lock.test_and_set(std::memory_order_acquire))
		{
			continue;
		}

		m_subscribers.release_subscriber(ssrc);
		PLOG_RTP_WRITE(LOG_PREFIX, "dec_tx_subscriber -- ID(%s) release subscriber ssrc:%u.", (const char*)getName(), ssrc);

		if (m_tx_subscriber_count > 0)
		{
			m_tx_subscriber_count = std_interlocked_dec(&m_tx_subscriber_count);
		}

		m_lock.clear(std::memory_order_release);
		go = false;
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtp_webrtc_channel_t::setup_ice_params_local(const char* l_ufrag, const char* l_pswd)
{
	m_subscribers.get_stun()->setup_ice_params_local(l_ufrag, l_pswd);

	return true;
}
//------------------------------------------------------------------------------
bool rtp_webrtc_channel_t::setup_ice_params_remote(const char* r_ufrag, const char* r_pswd)
{
	m_subscribers.get_stun()->setup_ice_params_remote(r_ufrag, r_pswd);

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtp_webrtc_channel_t::fill_fingerprint_param(sdp_fingerprint_t* fingerprint_param)
{
	if (fingerprint_param == nullptr)
	{
		return false;
	}

	if (!m_subscribers.get_dtls()->prepare_fingerprint("sha-256"))
	{
		return false;
	}

	fingerprint_param->set_hash(
		m_subscribers.get_dtls()->get_fingerprint_algorithm(),
		m_subscribers.get_dtls()->get_fingerprint(),
		m_subscribers.get_dtls()->get_fingerprint_length());

	return true;
}
//------------------------------------------------------------------------------
bool rtp_webrtc_channel_t::apply_fingerprint_param(sdp_fingerprint_t* fingerprint_param)
{
	if (fingerprint_param == nullptr)
	{
		return false;
	}

	m_subscribers.get_dtls()->set_remote_fingerprint(
		fingerprint_param->get_hash_function(),
		fingerprint_param->get_hash(),
		fingerprint_param->get_hash_length());

	if (!m_subscribers.get_dtls()->prepare_fingerprint())
	{
		return false;
	}
	if (fingerprint_param->get_role() == fingerprint_role_t::passive)
	{
		m_subscribers.get_dtls()->set_dtls_connection_role(dtls_connection_role_t::dtls_connection_role_active_e);
	}
	else
	{
		m_subscribers.get_dtls()->set_dtls_connection_role(dtls_connection_role_t::dtls_connection_role_passive_e);
	}

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_webrtc_channel_t::print_ports(const char* pef, rtl::Logger* log)
{
	if (log != nullptr)
	{
		log->log(LOG_PREFIX, "%s", pef);
		log->log(LOG_PREFIX, "--------------");
		if (m_data_port > 0)
		{
			rtl::String p1 = "";
			p1 << m_data_port;
			log->log(LOG_PREFIX, "      |%s", (const char*)p1);
		}
		
		if (m_ctrl_port > 0)
		{
			rtl::String p2 = "";
			p2 << m_ctrl_port;
			log->log(LOG_PREFIX, "      |%s", (const char*)p2);
		}

		if (m_data_video_port > 0)
		{
			rtl::String p3 = "";
			p3 << m_data_video_port;
			log->log(LOG_PREFIX, "      |%s", (const char*)p3);
		}

		if (m_ctrl_video_port > 0)
		{
			rtl::String p4 = "";
			p4 << m_ctrl_video_port;
			log->log(LOG_PREFIX, "      |%s", (const char*)p4);
		}
		
		log->log(LOG_PREFIX, "--------------");
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
