﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 //--------------------------------------
//  AES Integer Counter Mode
//--------------------------------------
// based on libsrtp written by
// David A. McGrew
// Cisco Systems, Inc.
//--------------------------------------
#include "stdafx.h"

#include "srtp_aes_icm.h"
//--------------------------------------
//
//--------------------------------------
aes_icm_ctx_t::aes_icm_ctx_t(int key_len) : cipher_t(key_len), m_bytes_in_buffer(0)
{
	memset(&m_counter, 0, sizeof m_counter);
	memset(&m_offset, 0, sizeof m_offset);
	memset(&m_keystream_buffer, 0, sizeof m_keystream_buffer);
	memset(m_expanded_key, 0, sizeof m_expanded_key);

	m_cipher_id = 1; // AES_128_ICM
}
//--------------------------------------
//
//--------------------------------------
aes_icm_ctx_t::~aes_icm_ctx_t()
{
	memset(&m_counter, 0, sizeof m_counter);
	memset(&m_offset, 0, sizeof m_offset);
	memset(&m_keystream_buffer, 0, sizeof m_keystream_buffer);
	memset(m_expanded_key, 0, sizeof m_expanded_key);

	m_bytes_in_buffer = 0;
}
//--------------------------------------
//
//--------------------------------------
bool aes_icm_ctx_t::create(const uint8_t *key, cipher_direction_t dir)
{
	v128_t tmp_key;

	/* set counter and initial values to 'offset' value */
	/* FIX!!! this assumes the salt is at key + 16, and thus that the */
	/* FIX!!! cipher key length is 16!  Also note this copies past the
	end of the 'key' array by 2 bytes! */
	v128_copy_octet_string(&m_counter, key + 16);
	v128_copy_octet_string(&m_offset, key + 16);

	/* force last two octets of the offset to zero (for srtp compatibility) */
	m_offset.v8[14] = m_offset.v8[15] = 0;
	m_counter.v8[14] = m_counter.v8[15] = 0;

	/* set tmp_key (for alignment) */
	v128_copy_octet_string(&tmp_key, key);

	/* expand key */
	aes_expand_encryption_key(&tmp_key, m_expanded_key);

	/* indicate that the keystream_buffer is empty */
	m_bytes_in_buffer = 0;

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool aes_icm_ctx_t::set_iv(void *iv)
{
	v128_t *nonce = (v128_t*)iv;

	v128_xor(&m_counter, &m_offset, nonce);
  
	// indicate that the keystream_buffer is empty
	m_bytes_in_buffer = 0;

	return true;
}
//--------------------------------------
//
//--------------------------------------
 void aes_icm_ctx_t::advance()
{
	/* fill buffer with new keystream */
	v128_copy(&m_keystream_buffer, &m_counter);
	aes_encrypt(&m_keystream_buffer, m_expanded_key);
	m_bytes_in_buffer = sizeof(v128_t);
  
	/* clock counter forward */
	if (!++(m_counter.v8[15])) 
		++(m_counter.v8[14]);
}
//--------------------------------------
//
//--------------------------------------
bool aes_icm_ctx_t::encrypt(uint8_t *buffer, uint32_t *enc_len)
{
	uint32_t bytes_to_encr = *enc_len;
	uint32_t i;
	uint32_t *b;

	/* check that there's enough segment left but not for ismacryp*/
	if ((bytes_to_encr + htons(m_counter.v16[7])) > 0xffff)
		return false;

	if (bytes_to_encr <= (uint32_t)m_bytes_in_buffer)
	{
		/* deal with odd case of small bytes_to_encr */
		for (i = (sizeof(v128_t) - m_bytes_in_buffer);	i < (sizeof(v128_t) - m_bytes_in_buffer + bytes_to_encr); i++)
		{
			*buffer++ ^= m_keystream_buffer.v8[i];
		}

		m_bytes_in_buffer -= bytes_to_encr;

		// return now to avoid the main loop
		return true;
	}

	// encrypt bytes until the remaining data is 16-byte aligned
	for (i=(sizeof(v128_t) - m_bytes_in_buffer); i < sizeof(v128_t); i++) 
	{
		*buffer++ ^= m_keystream_buffer.v8[i];
	}

	bytes_to_encr -= m_bytes_in_buffer;
	m_bytes_in_buffer = 0;
	
	// now loop over entire 16-byte blocks of keystream
	for (i = 0; i < (bytes_to_encr/sizeof(v128_t)); i++)
	{
		/* fill buffer with new keystream */
		advance();

		/*
		* add keystream into the data buffer (this would be a lot faster
		* if we could assume 32-bit alignment!)
		*/

		if ((((uintptr_t) buffer) & 0x03) != 0)
		{
			*buffer++ ^= m_keystream_buffer.v8[0];
			*buffer++ ^= m_keystream_buffer.v8[1];
			*buffer++ ^= m_keystream_buffer.v8[2];
			*buffer++ ^= m_keystream_buffer.v8[3];
			*buffer++ ^= m_keystream_buffer.v8[4];
			*buffer++ ^= m_keystream_buffer.v8[5];
			*buffer++ ^= m_keystream_buffer.v8[6];
			*buffer++ ^= m_keystream_buffer.v8[7];
			*buffer++ ^= m_keystream_buffer.v8[8];
			*buffer++ ^= m_keystream_buffer.v8[9];
			*buffer++ ^= m_keystream_buffer.v8[10];
			*buffer++ ^= m_keystream_buffer.v8[11];
			*buffer++ ^= m_keystream_buffer.v8[12];
			*buffer++ ^= m_keystream_buffer.v8[13];
			*buffer++ ^= m_keystream_buffer.v8[14];
			*buffer++ ^= m_keystream_buffer.v8[15];
		}
		else
		{
			b = (uint32_t *)buffer;
			*b++ ^= m_keystream_buffer.v32[0];
			*b++ ^= m_keystream_buffer.v32[1];
			*b++ ^= m_keystream_buffer.v32[2];
			*b++ ^= m_keystream_buffer.v32[3];
			buffer = (uint8_t *)b;
		}
	}
  
	/* if there is a tail end of the data, process it */
	if ((bytes_to_encr & 0xf) != 0)
	{
		/* fill buffer with new keystream */
		advance();

		for (i=0; i < (bytes_to_encr & 0xf); i++)
			*buffer++ ^= m_keystream_buffer.v8[i];

		/* reset the keystream buffer size to right value */
		m_bytes_in_buffer = sizeof(v128_t) - i;  
	}
	else
	{
		/* no tail, so just reset the keystream buffer size to zero */
		m_bytes_in_buffer = 0;
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool aes_icm_ctx_t::decrypt(uint8_t *buffer, uint32_t *dec_len)
{
	return encrypt(buffer, dec_len);
}
//--------------------------------------
