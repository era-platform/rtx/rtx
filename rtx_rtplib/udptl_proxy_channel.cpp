﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "udptl_proxy_channel.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
udptl_proxy_channel_t::udptl_proxy_channel_t(rtl::Logger* log) :
	rtp_channel_t(log), m_log(log), m_data_socket(log)
{
	m_event_handler = nullptr;
	m_socket_io_service = nullptr;

	m_data_port = 0;

	m_lock.clear();

	m_subscriber_count = 0;

	m_started = false;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
udptl_proxy_channel_t::~udptl_proxy_channel_t()
{
	destroy();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtp_channel_type_t udptl_proxy_channel_t::getType() const
{
	return rtp_channel_type_t::rtp_channel_type_udptl_proxy_e;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool udptl_proxy_channel_t::bind_data_socket(const ip_address_t* iface, uint16_t data_port)
{
	if (iface == nullptr)
	{
		PLOG_RTP_ERROR("udp","bind_data_socket -- invalid address");
		return false;
	}

	PLOG_RTP_WRITE("bind_data_socket -- binding socket on %s, %u ...", iface->to_string(), data_port);

	m_data_socket.close();

	if (m_data_socket.create(iface->get_family(), e_ip_protocol_type::E_IP_PROTOCOL_UDP))
	{
		socket_address saddr(iface, data_port);

		if (m_data_socket.bind(&saddr))
		{
			PLOG_RTP_WRITE("udp","bind_data_socket -- ok");
			
			m_interface.copy_from(iface);
			m_data_port = m_data_socket.get_local_address()->get_port();

			char name[64] = {0};
			int len = std_snprintf(name, 64, "udptl_proxy_channel:%s(%u)", m_interface.to_string(), m_data_port);
			name[len] = 0;
			rtp_channel_t::set_name(name);

			int bsize = 64 * 1024;
			m_data_socket.set_option(SOL_SOCKET, SO_RCVBUF, &bsize, sizeof(int));
			m_data_socket.set_option(SOL_SOCKET, SO_SNDBUF, &bsize, sizeof(int));

			return true;
		}
		else
		{
			PLOG_RTP_ERROR("udp","bind_data_socket -- net_socket.bind(%u) failed", data_port);
		}
	}
	else
	{
		PLOG_RTP_ERROR("udp","bind_data_socket -- net_socket.create() failed");
	}

	m_data_socket.close();

	return false;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool udptl_proxy_channel_t::destroy()
{
	stop_io();

	m_data_socket.close();

	return m_subscriber_count == 0;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool udptl_proxy_channel_t::started()
{
	return m_started;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int udptl_proxy_channel_t::start_io(socket_io_service_t* iosvc)
{
	if (m_started)
	{
		//PLOG_CALL("udp", "start_io -- already started");
		return 0;
	}

	if (iosvc == nullptr)
	{
		PLOG_RTP_ERROR("udp","start_io -- invalid io service");
		return -1;
	}

	m_socket_io_service = iosvc;

	if (!m_socket_io_service->add(&m_data_socket, this))
	{
		//PLOG_RTP_ERROR("udp","start_io -- socket_io_service.add() failed");
		return -2;
	}
	else
	{
		insert_stat(this);

		m_started = true;
	}

	return 1;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void udptl_proxy_channel_t::stop_io()
{
	if (m_socket_io_service != nullptr)
	{
		remove_stat(this);

		m_socket_io_service->remove(&m_data_socket);

		m_started = false;

		m_socket_io_service = nullptr;
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const ip_address_t* udptl_proxy_channel_t::get_net_interface() const
{
	return &m_interface;
}
const uint16_t udptl_proxy_channel_t::get_data_port() const
{
	return m_data_port;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const socket_io_service_t* udptl_proxy_channel_t::get_io_service() const
{
	return m_socket_io_service;
}
void udptl_proxy_channel_t::set_io_service(socket_io_service_t* svc)
{
	m_socket_io_service = svc;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
i_rx_channel_event_handler*	udptl_proxy_channel_t::get_event_handler()
{
	if (m_lock.test_and_set(std::memory_order_acquire))
	{
		return nullptr;
	}

	i_rx_channel_event_handler* h = m_event_handler;
	m_lock.clear(std::memory_order_release);
	return h;
}
void udptl_proxy_channel_t::set_event_handler(i_rx_channel_event_handler* handler)
{
	bool go = true;
	while (go)
	{
		if (m_lock.test_and_set(std::memory_order_acquire))
		{
			continue;
		}

		m_event_handler = handler;
		m_lock.clear(std::memory_order_release);
		go = false;
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void udptl_proxy_channel_t::print_ports(const char* pef, rtl::Logger* log)
{
	if (log != nullptr)
	{
		log->log("udp", "%s", pef);
		log->log("udp", "--------------");
		if (m_data_port > 0)
		{
			rtl::String p1 = "";
			p1 << m_data_port;
			log->log("udp", "      |%s", (const char*)p1);
		}

		log->log("udp", "--------------");
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool udptl_proxy_channel_t::send_packet(const rtp_packet* packet, const socket_address* saddr)
{
	if (packet == nullptr)
	{
		PLOG_RTP_ERROR("udp","send_packet -- invalid packet");
		return false;
	}

	if (saddr == nullptr)
	{
		PLOG_RTP_ERROR("udp","send_packet -- invalid address");
		return false;
	}

	uint8_t buffer[rtp_packet::PAYLOAD_BUFFER_SIZE];
	uint32_t length = packet->write_packet(buffer, sizeof(buffer));
	
	if (length == 0)
	{
		PLOG_RTP_WARNING("udp","send_packet -- rtp_packet.write_packet() failed");
		return false;
	}

	PLOG_RTP_WRITE("udp", " sending -- ID(%s) ssrc:%10u pt:%3u seq:%5u (%6u)   to:%s .", (const char*)getName(), packet->get_ssrc(),
		packet->get_payload_type(), packet->get_sequence_number(), length, saddr->to_string());

	return send_data(buffer, length, saddr);
}
//------------------------------------------------------------------------------
bool udptl_proxy_channel_t::send_data(const uint8_t* data, uint32_t len, const socket_address* saddr)
{
	PLOG_RTP_FLOW("udp", "send_data -- sending %u bytes to %s", len, saddr->to_string());

	if (m_socket_io_service == nullptr)
	{
		//PLOG_RTP_ERROR("udp", "send_data -- socket_io_service is undefined");
		return false;
	}

	if (!m_socket_io_service->send_data_to(&m_data_socket, data, len, saddr))
	{
		PLOG_RTP_ERROR("udp", "send_data -- socket_io_service.send_data() failed");
		return false;
	}

	update_tx_stat(len);

	return true;
}
//------------------------------------------------------------------------------
bool udptl_proxy_channel_t::send_ctrl(const rtcp_packet_t* packet, const socket_address* saddr)
{
	if (packet == nullptr)
	{
		PLOG_RTP_ERROR("udp", "send_ctrl -- invalid packet");
		return false;
	}

	if (saddr == nullptr)
	{
		PLOG_RTP_ERROR("udp", "send_ctrl -- invalid address");
		return false;
	}

	uint8_t buffer[rtp_packet::PAYLOAD_BUFFER_SIZE];
	uint32_t length = packet->write(buffer, sizeof(buffer));

	if (length == 0)
	{
		PLOG_RTP_WARNING("udp", "send_ctrl -- write packet failed");
		return false;
	}

	return send_data(buffer, length, saddr);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void udptl_proxy_channel_t::inc_tx_subscriber(uint32_t ssrc)
{
	m_subscriber_count = std_interlocked_inc(&m_subscriber_count);
}
//------------------------------------------------------------------------------
void udptl_proxy_channel_t::dec_tx_subscriber(uint32_t ssrc)
{
	if (m_subscriber_count > 0)
	{
		m_subscriber_count = std_interlocked_dec(&m_subscriber_count);
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void udptl_proxy_channel_t::socket_data_available(net_socket* /*sock*/)
{
	PLOG_RTP_WARNING("udp","socket_data_available -- empty method");
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void udptl_proxy_channel_t::socket_data_received(net_socket* sock, const uint8_t* data, uint32_t len, const socket_address* from)
{
	if (sock == nullptr)
	{
		PLOG_RTP_ERROR("udp","socket_data_received -- invalid socket");
		return;
	}

	if (data == nullptr)
	{
		PLOG_RTP_ERROR("udp","socket_data_received -- invalid data");
		return;
	}

	if (len == 0)
	{
		PLOG_RTP_ERROR("udp","socket_data_received -- invalid length");
		return;
	}

	if (from == nullptr)
	{
		PLOG_RTP_ERROR("udp","socket_data_received -- invalid address");
		return;
	}

	update_rx_stat(len);

	if (sock == &m_data_socket)
	{
		data_received(data, len, from);
	}
	else
	{
		PLOG_RTP_ERROR("udp","socket_data_received -- unknown socket");
		// возможно пришло из другого канала
		// по этому:
		data_received(data, len, from);
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void udptl_proxy_channel_t::data_received(const uint8_t* data, uint32_t len, const socket_address* from)
{
	PLOG_RTP_WRITE("udp", "received -- ID(%s)                                  (%6u) from:%s .", (const char*)getName(), len, from->to_string());

	i_rx_channel_event_handler* h = get_event_handler();
	if (h != nullptr)
	{
		h->rx_data_stream(this, data, len, from);
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void udptl_proxy_channel_t::tx_packet_channel(/*const mg_tx_stream_t* stream, */const rtp_packet* packet, const socket_address* to)
{
	send_packet(packet, to);
}
void udptl_proxy_channel_t::tx_data_channel(/*const mg_tx_stream_t* stream, */const uint8_t* data, uint32_t len, const socket_address* to)
{
	send_data(data, len, to);
}
void udptl_proxy_channel_t::tx_ctrl_channel(/*const mg_tx_stream_t* stream, */const rtcp_packet_t* packet, const socket_address* to)
{
	send_ctrl(packet, to);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void udptl_proxy_channel_t::clear_handler(uint32_t ssrc)
{
	set_event_handler(nullptr);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
