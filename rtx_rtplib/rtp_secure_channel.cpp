﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_crypto.h"
#include "rtp_secure_channel.h"
#include "dtls_define.h"
#include "srtp.h"
#include "sdp_crypto.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define LOG_PREFIX "srtp"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtp_secure_channel_t::rtp_secure_channel_t(rtl::Logger* log) :
	rtp_channel_t(log), m_log(log), m_data_socket(log), m_control_socket(log)
{
	m_handler = nullptr;
	m_socket_io_service = nullptr;

	m_data_port = m_ctrl_port = 0;

	m_lock.clear();

	m_started = false;

	m_srtp_filter_in = nullptr;
	m_srtp_filter_out = nullptr;

	memset(&m_sdp_crypto_param_out, 0, sizeof(m_sdp_crypto_param_out));
	memset(&m_sdp_crypto_param_out_2, 0, sizeof(m_sdp_crypto_param_out_2));

	m_subscriber_count = 0;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtp_secure_channel_t::~rtp_secure_channel_t()
{
	destroy();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtp_channel_type_t rtp_secure_channel_t::getType() const
{
	return rtp_channel_type_t::rtp_channel_type_secure_e;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtp_secure_channel_t::bind_data_socket(const ip_address_t* iface, uint16_t data_port)
{
	if (iface == NULL)
	{
		PLOG_RTPLIB_ERROR(LOG_PREFIX,"bind_data_socket -- invalid address");
		return false;
	}

	PLOG_RTPLIB_WRITE(LOG_PREFIX, "bind_data_socket -- binding socket on %s, %u ...", iface->to_string(), data_port);

	m_data_socket.close();

	if (m_data_socket.create(iface->get_family(), e_ip_protocol_type::E_IP_PROTOCOL_UDP))
	{
		socket_address saddr(iface, data_port);

		if (m_data_socket.bind(&saddr))
		{
			PLOG_RTPLIB_WRITE(LOG_PREFIX,"bind_data_socket -- ok");
			
			m_interface.copy_from(iface);
			m_data_port = m_data_socket.get_local_address()->get_port();

			char name[64] = { 0 };
			int len = std_snprintf(name, 64, "secure_channel:%s(%u)", m_interface.to_string(), m_data_port);
			name[len] = 0;
			rtp_channel_t::set_name(name);

			int bsize = 64 * 1024;
			m_data_socket.set_option(SOL_SOCKET, SO_RCVBUF, &bsize, sizeof(int));
			m_data_socket.set_option(SOL_SOCKET, SO_SNDBUF, &bsize, sizeof(int));

			return true;
		}
		else
		{
			PLOG_RTPLIB_ERROR(LOG_PREFIX,"bind_data_socket -- net_socket.bind(%u) failed", data_port);
		}
	}
	else
	{
		PLOG_RTPLIB_ERROR(LOG_PREFIX,"bind_data_socket -- net_socket.create() failed");
	}

	m_data_socket.close();

	return false;
}
//------------------------------------------------------------------------------
bool rtp_secure_channel_t::bind_ctrl_socket(const ip_address_t* iface, uint16_t ctrl_port)
{
	if (iface == NULL)
	{
		PLOG_RTPLIB_ERROR(LOG_PREFIX, "bind_ctrl_socket -- invalid address");
		return false;
	}

	if (ctrl_port == 0)
	{
		PLOG_RTPLIB_ERROR(LOG_PREFIX, "bind_ctrl_socket -- invalid ctrl port");
		return false;
	}

	PLOG_RTPLIB_WRITE("bind_ctrl_socket -- binding socket on %s, %u ...", iface->to_string(), ctrl_port);

	m_control_socket.close();

	if (m_control_socket.create(iface->get_family(), e_ip_protocol_type::E_IP_PROTOCOL_UDP))
	{
		socket_address saddr(iface, ctrl_port);

		if (m_control_socket.bind(&saddr))
		{
			PLOG_RTPLIB_WRITE(LOG_PREFIX, "bind_ctrl_socket -- ok");

			m_interface.copy_from(iface);
			m_ctrl_port = saddr.get_port();

			char name[64] = { 0 };
			int len = std_snprintf(name, 64, "%s:%u", (char*)rtp_channel_t::getName(), m_ctrl_port);
			//int len_write = std_snprintf((char*)name + len - 1, 64 - len, ":%u)", m_ctrl_port);
			//len += len_write;
			name[len] = 0;
			rtp_channel_t::set_name(name);

			int bsize = 64 * 1024;
			m_control_socket.set_option(SOL_SOCKET, SO_RCVBUF, &bsize, sizeof(int));
			m_control_socket.set_option(SOL_SOCKET, SO_SNDBUF, &bsize, sizeof(int));

			return true;
		}
		else
		{
			PLOG_RTPLIB_ERROR(LOG_PREFIX, "bind_ctrl_socket -- net_socket.bind(%u) failed", ctrl_port);
		}
	}
	else
	{
		PLOG_RTPLIB_ERROR(LOG_PREFIX, "bind_ctrl_socket -- net_socket.create() failed");
	}

	m_control_socket.close();

	return false;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtp_secure_channel_t::destroy()
{
	stop_io();

	m_data_socket.close();
	m_control_socket.close();

	set_event_handler(nullptr);

	if (m_srtp_filter_in != nullptr)
	{
		DELETEO(m_srtp_filter_in);
		m_srtp_filter_in = nullptr;
	}
	if (m_srtp_filter_out != nullptr)
	{
		DELETEO(m_srtp_filter_out);
		m_srtp_filter_out = nullptr;
	}

	return m_subscriber_count == 0;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtp_secure_channel_t::started()
{
	return m_started;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int rtp_secure_channel_t::start_io(socket_io_service_t* iosvc)
{
	if (m_started)
	{
		return 0;
	}

	if (iosvc == NULL)
	{
		PLOG_RTPLIB_ERROR(LOG_PREFIX,"start_io -- invalid io service");
		return -1;
	}

	m_socket_io_service = iosvc;

	if (!m_socket_io_service->add(&m_data_socket, this) || !m_socket_io_service->add(&m_control_socket, this))
	{
		return -2;
	}
	else
	{
		insert_stat(this);

		m_started = true;
	}

	return 1;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_secure_channel_t::stop_io()
{
	if (m_socket_io_service != NULL)
	{
		remove_stat(this);

		m_socket_io_service->remove(&m_data_socket);
		m_socket_io_service->remove(&m_control_socket);

		m_started = false;

		m_socket_io_service = NULL;
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const ip_address_t* rtp_secure_channel_t::get_net_interface() const
{
	return &m_interface;
}
const uint16_t rtp_secure_channel_t::get_data_port() const
{
	return m_data_port;
}
uint16_t rtp_secure_channel_t::get_ctrl_port() const
{
	return m_ctrl_port;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const socket_io_service_t* rtp_secure_channel_t::get_io_service() const
{
	return m_socket_io_service;
}
void rtp_secure_channel_t::set_io_service(socket_io_service_t* svc)
{
	m_socket_io_service = svc;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
i_rx_channel_event_handler* rtp_secure_channel_t::get_event_handler()
{
	if (m_lock.test_and_set(std::memory_order_acquire))
	{
		return nullptr;
	}

	i_rx_channel_event_handler* h = m_handler;

	m_lock.clear(std::memory_order_release);

	return h;
}
void rtp_secure_channel_t::set_event_handler(i_rx_channel_event_handler* handler)
{
	bool go = true;
	while (go)
	{
		if (m_lock.test_and_set(std::memory_order_acquire))
		{
			continue;
		}

		m_handler = handler;

		m_lock.clear(std::memory_order_release);
		go = false;
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtp_secure_channel_t::send_packet(const rtp_packet* packet, const socket_address* saddr)
{
	if (packet == NULL)
	{
		PLOG_RTPLIB_ERROR(LOG_PREFIX,"send_packet -- invalid packet");
		return false;
	}

	if (saddr == NULL)
	{
		PLOG_RTPLIB_ERROR(LOG_PREFIX,"send_packet -- invalid address");
		return false;
	}

	uint8_t buffer[rtp_packet::PAYLOAD_BUFFER_SIZE];
	uint32_t length = packet->write_packet(buffer, sizeof(buffer));
	if (length == 0)
	{
		PLOG_RTPLIB_WARNING(LOG_PREFIX,"send_packet -- rtp_packet.write_packet() failed");
		return false;
	}

	/*PLOG_RTP_WRITE(LOG_PREFIX, " sending -- ID(%s) ssrc:%10u pt:%3u seq:%5u (%6u)   to:%s .", (const char*)getName(), packet->get_ssrc(),
		packet->get_payload_type(), packet->get_sequence_number(), length, saddr->to_string());*/

	return send_data(buffer, length, saddr);
}
//------------------------------------------------------------------------------
bool rtp_secure_channel_t::send_data(const uint8_t* data, uint32_t len, const socket_address* saddr)
{
	PLOG_RTP_FLOW(LOG_PREFIX, "send_data -- sending %u bytes to %s", len, saddr->to_string());

	if (m_socket_io_service == NULL)
	{
		//PLOG_RTPLIB_ERROR(LOG_PREFIX, "send_data -- socket_io_service is undefined");
		return false;
	}

	if ((m_srtp_filter_out != nullptr) && (!m_srtp_filter_out->srtp_pack((void*)data, (int*)&len)))
	{
		PLOG_RTPLIB_ERROR(LOG_PREFIX, "send_data -- srtp_pack() failed.");
		return false;
	}

	if (!m_socket_io_service->send_data_to(&m_data_socket, data, len, saddr))
	{
		PLOG_RTPLIB_ERROR(LOG_PREFIX, "send_data -- socket_io_service.send_data() failed");
		return false;
	}
	else
	{
		PLOG_RTP_WRITE(LOG_PREFIX, "send_packet -- socket_io_service.send_data(l=%d) to: %s:%d",
			len, saddr->get_ip_address()->to_string(), saddr->get_port());
	}

	update_tx_stat(len);

	return true;
}
//------------------------------------------------------------------------------
bool rtp_secure_channel_t::send_ctrl(const rtcp_packet_t* packet, const socket_address* saddr)
{
	if (packet == NULL)
	{
		PLOG_RTPLIB_ERROR(LOG_PREFIX, "send_ctrl -- invalid packet.");
		return false;
	}

	if (saddr == NULL)
	{
		PLOG_RTPLIB_ERROR(LOG_PREFIX, "send_ctrl -- invalid address.");
		return false;
	}

	uint8_t buffer[rtp_packet::PAYLOAD_BUFFER_SIZE];
	uint32_t length = packet->write(buffer, sizeof(buffer));
	if (length == 0)
	{
		PLOG_RTPLIB_WARNING(LOG_PREFIX, "send_ctrl -- write packet failed.");
		return false;
	}

	if (m_socket_io_service == NULL)
	{
		//PLOG_RTPLIB_ERROR(LOG_PREFIX, "send_ctrl -- socket_io_service is undefined");
		return false;
	}

	if ((m_srtp_filter_out != nullptr) && (!m_srtp_filter_out->srtcp_pack((void*)buffer, (int*)&length)))
	{
		PLOG_RTPLIB_ERROR(LOG_PREFIX, "send_ctrl -- srtp_pack() failed.");
		return false;
	}

	PLOG_RTP_WRITE(LOG_PREFIX, " sending -- ID(%s) ssrc:%10u                  (%6u)   to:%s .", (const char*)getName(), packet->get_ssrc(),
		length, saddr->to_string());

	if (!m_socket_io_service->send_data_to(&m_control_socket, buffer, length, saddr))
	{
		PLOG_RTPLIB_ERROR(LOG_PREFIX, "send_ctrl -- socket_io_service.send_data() failed");
		return false;
	}

	update_tx_stat(length);

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_secure_channel_t::inc_tx_subscriber(uint32_t ssrc)
{
	m_subscriber_count = std_interlocked_inc(&m_subscriber_count);
}
//------------------------------------------------------------------------------
void rtp_secure_channel_t::dec_tx_subscriber(uint32_t ssrc)
{
	if (m_subscriber_count > 0)
	{
		m_subscriber_count = std_interlocked_dec(&m_subscriber_count);
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_secure_channel_t::socket_data_available(net_socket* /*sock*/)
{
	PLOG_RTPLIB_WARNING(LOG_PREFIX,"socket_data_available -- empty method");
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_secure_channel_t::socket_data_received(net_socket* sock, const uint8_t* data, uint32_t len, const socket_address* from)
{
	if (sock == NULL)
	{
		PLOG_RTPLIB_ERROR(LOG_PREFIX,"socket_data_received -- invalid socket");
		return;
	}

	if (data == NULL)
	{
		PLOG_RTPLIB_ERROR(LOG_PREFIX,"socket_data_received -- invalid data");
		return;
	}

	if (len == 0)
	{
		PLOG_RTPLIB_ERROR(LOG_PREFIX,"socket_data_received -- invalid length");
		return;
	}

	if (from == NULL)
	{
		PLOG_RTPLIB_ERROR(LOG_PREFIX,"socket_data_received -- invalid address");
		return;
	}

	update_rx_stat(len);

	if (sock == &m_data_socket)
	{
		rtp_data_received(data, len, from);
	}
	else if (sock == &m_control_socket)
	{
		rtcp_data_received(data, len, from);
	}
	else
	{
		PLOG_RTPLIB_ERROR(LOG_PREFIX,"socket_data_received -- unknown socket");
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_secure_channel_t::rtp_data_received(const uint8_t* data, uint32_t len, const socket_address* from)
{
	rtp_header_t* rtp_pack = (rtp_header_t*)data;

	PLOG_RTP_WRITE(LOG_PREFIX, "received -- ID(%s) ssrc:%10u pt:%3u seq:%5u (%6u) from:%s .", (const char*)getName(), ntohl(rtp_pack->ssrc),
		rtp_pack->pt, ntohs(rtp_pack->seq), len, from->to_string());

	if ((m_srtp_filter_in != nullptr) && (!m_srtp_filter_in->srtp_unpack((void*)data, (int*)&len)))
	{
		PLOG_RTPLIB_ERROR(LOG_PREFIX, "rtp_data_received -- srtp_unpack() failed.");
		return;
	}

	rtp_packet packet;
	if (!packet.read_packet(data, len))
	{
		PLOG_RTPLIB_ERROR(LOG_PREFIX, "rtp_data_received -- read_packet() failed.");
		return;
	}

	PLOG_RTP_FLOW(LOG_PREFIX, "rtp_data_received -- from: %s:%d", from->get_ip_address()->to_string(), from->get_port());

	i_rx_channel_event_handler* stream = get_event_handler();
	if (stream == nullptr)
	{
		PLOG_RTPLIB_ERROR(LOG_PREFIX, "rtp_data_received -- stream is null.");
		return;
	}

	stream->rx_packet_stream(this, &packet, from);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_secure_channel_t::rtcp_data_received(const uint8_t* data, uint32_t len, const socket_address* from)
{
	rtcp_header_t* hdr = (rtcp_header_t*)data;

	PLOG_RTP_FLOW(LOG_PREFIX, "rtcp_data_received -- v:% 1d rc: 5d pt:% 3d l:% 5d ssrc:% 10d from: %s:%d.",
		hdr->ver, hdr->rc, hdr->pt, hdr->length, hdr->ssrc,
		from->get_ip_address()->to_string(), from->get_port());

	i_rx_channel_event_handler* h = get_event_handler();
	if (h == nullptr)
	{
		PLOG_RTPLIB_ERROR(LOG_PREFIX, "rtcp_data_received -- handler not found.");
		return;
	}

	if (rtcp_packet_t::is_rtcp_packet(data, len))
	{
		rtcp_packet_t packet;
		if (len != (uint32_t)packet.read(data, len))
		{
			PLOG_RTP_WARNING(LOG_PREFIX, "rtcp_data_received -- read packet failed.");
			return;
		}

		h->rx_ctrl_stream(this, &packet, from);
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_secure_channel_t::tx_packet_channel(/*const mg_tx_stream_t* stream, */const rtp_packet* packet, const socket_address* to)
{
	send_packet(packet, to);
}
//------------------------------------------------------------------------------
void rtp_secure_channel_t::tx_data_channel(/*const mg_tx_stream_t* stream, */const uint8_t* data, uint32_t len, const socket_address* to)
{
	send_data(data, len, to);
}
//------------------------------------------------------------------------------
void rtp_secure_channel_t::tx_ctrl_channel(/*const mg_tx_stream_t* stream, */const rtcp_packet_t* packet, const socket_address* to)
{
	send_ctrl(packet, to);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_secure_channel_t::clear_handler(uint32_t ssrc)
{
	set_event_handler(nullptr);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtl::String rtp_secure_channel_t::rtp_crypto_to_string(const rtp_crypto_param_t* key, int tag)
{
	rtl::String str;

	if (key == nullptr)
	{
		return str;
	}

	sdp_crypto_suite_t crypto_suite = (sdp_crypto_suite_t)key->crypto_suite;

	sdp_crypto_parameter_t crypto_param(tag);
	crypto_param.set_crypto_suite(crypto_suite);
	crypto_param.add_key(key->key, 30, 0, 0, 0);

	char buff[512] = { 0 };
	if (crypto_param.encode(buff, 512) <= 0)
	{
		return str;
	}

	str.append(buff);

	return str;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
static bool set_crypto_params_to_profile(const rtp_crypto_param_t* key, srtp_policy_t* policy)
{
	policy->ssrc = 0;
	policy->key = (uint8_t*)key->key;

	if (key->crypto_suite == srtp_profile_aes128_cm_sha1_80)
	{
		crypto_policy_set_aes_cm_128_hmac_sha1_80(&policy->rtp);
		crypto_policy_set_aes_cm_128_hmac_sha1_80(&policy->rtcp);
	}
	else if (key->crypto_suite == srtp_profile_aes128_cm_sha1_32)
	{
		crypto_policy_set_aes_cm_128_hmac_sha1_32(&policy->rtp);
		crypto_policy_set_aes_cm_128_hmac_sha1_32(&policy->rtcp);
	}

	if (key->null_rtp_auth && key->null_rtp_cipher)
	{
		policy->rtp.auth_type = NULL_AUTH;
		policy->rtp.auth_key_len = 0;
		policy->rtp.auth_tag_len = 0;

		policy->rtp.cipher_type = NULL_CIPHER;
		policy->rtp.cipher_key_len = 0;
		policy->rtp.sec_serv = sec_serv_none;
	}
	else if (key->null_rtp_cipher)
	{
		policy->rtp.cipher_type = NULL_CIPHER;
		policy->rtp.cipher_key_len = 0;
		policy->rtp.sec_serv = sec_serv_auth;
	}
	else if (key->null_rtp_auth)
	{
		policy->rtp.auth_type = NULL_AUTH;
		policy->rtp.auth_key_len = 0;
		policy->rtp.auth_tag_len = 0;
		policy->rtp.sec_serv = sec_serv_conf;
	}

	if (key->null_rtcp_cipher)
	{
		policy->rtcp.cipher_type = NULL_CIPHER;
		policy->rtcp.cipher_key_len = 0;
		policy->rtcp.sec_serv = sec_serv_auth;
	}

	return true;
}
//------------------------------------------------------------------------------
bool rtp_secure_channel_t::setup_srtp_policy_in(rtl::String* sdp_crypto)
{
	if (sdp_crypto == nullptr)
	{
		return false;
	}

	//"a=crypto:" tag 1*WSP crypto-suite 1*WSP key-params *(1*WSP session-param)
	sdp_crypto_parameter_t crypto(1);
	if (!crypto.decode(*sdp_crypto))
	{
		return false;
	}

	sdp_crypto_suite_t crypto_suite = crypto.get_crypto_suite();

	const sdp_crypto_key_param_t& key_param = crypto.get_key_at(0);

	rtp_crypto_param_t crypto_in;
	crypto_in.crypto_suite = crypto_suite;
	memcpy(crypto_in.key, key_param.key, 30);
	crypto_in.null_rtcp_cipher = crypto.get_unencrypted_srtcp_flag();
	crypto_in.null_rtp_cipher = crypto.get_unencrypted_srtp_flag();
	crypto_in.null_rtp_auth = crypto.get_unauthenticated_srtp_flag();

	return setup_srtp_policy(&crypto_in); // , nullptr

	//if (crypto_suite == sdp_crypto_aes_cm_128_hmac_sha1_80_e)
	//{
	//	const sdp_crypto_key_param_t& key_param = crypto.get_key_at(0);

	//	rtp_crypto_param_t crypto_in;
	//	crypto_in.crypto_suite = crypto_suite;
	//	memcpy(crypto_in.key, key_param.key, 30);
	//	crypto_in.null_rtcp_cipher = crypto.get_unencrypted_srtcp_flag();
	//	crypto_in.null_rtp_cipher = crypto.get_unencrypted_srtp_flag();
	//	crypto_in.null_rtp_auth = crypto.get_unauthenticated_srtp_flag();

	//	return setup_srtp_policy(&crypto_in); // , nullptr
	//}
	//else if (crypto_suite == sdp_crypto_aes_cm_128_hmac_sha1_32_e)
	//{
	//	const sdp_crypto_key_param_t& key_param = crypto.get_key_at(0);

	//	rtp_crypto_param_t crypto_in;
	//	crypto_in.crypto_suite = crypto_suite;
	//	memcpy(crypto_in.key, key_param.key, 30);
	//	crypto_in.null_rtcp_cipher = crypto.get_unencrypted_srtcp_flag();
	//	crypto_in.null_rtp_cipher = crypto.get_unencrypted_srtp_flag();
	//	crypto_in.null_rtp_auth = crypto.get_unauthenticated_srtp_flag();

	//	return setup_srtp_policy(&crypto_in); // , nullptr
	//}

	//return false;
}
//------------------------------------------------------------------------------
//bool rtp_secure_channel_t::setup_srtp_policy_out(rtl::String* sdp_crypto)
//{
//	if (sdp_crypto == nullptr)
//	{
//		return false;
//	}
//
//	/*m_sdp_crypto_param_out = *sdp_crypto;
//	m_sdp_crypto_param_out.trim();*/
//
//	//"a=crypto:" tag 1*WSP crypto-suite 1*WSP key-params *(1*WSP session-param)
//	sdp_crypto_parameter_t crypto;
//	if (!crypto.decode(*sdp_crypto))
//	{
//		return false;
//	}
//
//	sdp_crypto_suite_t crypto_suite = crypto.get_crypto_suite();
//
//	if (crypto_suite == sdp_crypto_aes_cm_128_hmac_sha1_80_e)
//	{
//		const sdp_crypto_key_param_t& key_param = crypto.get_key_at(0);
//
//		rtp_crypto_param_t crypto_out;
//		crypto_out.crypto_suite = crypto_suite;
//		memcpy(crypto_out.key, key_param.key, 30);
//		crypto_out.null_rtcp_cipher = crypto.get_unencrypted_srtcp_flag();
//		crypto_out.null_rtp_cipher = crypto.get_unencrypted_srtp_flag();
//		crypto_out.null_rtp_auth = crypto.get_unauthenticated_srtp_flag();
//
//		return setup_srtp_policy_out(&crypto_out);
//	}
//
//	return false;
//}
//------------------------------------------------------------------------------
bool rtp_secure_channel_t::setup_srtp_policy_in(const struct rtp_crypto_param_t* key_in)
{
	srtp_policy_t policy;
	srtp_stream_ctx_t* stream;

	if (m_srtp_filter_in != nullptr)
	{
		DELETEO(m_srtp_filter_in);
		m_srtp_filter_in = nullptr;
	}

	if (m_srtp_filter_in == nullptr)
	{
		stream = NEW srtp_stream_ctx_t(m_log);
		set_crypto_params_to_profile(key_in, &policy);
		if (!stream->initialize(&policy))
		{
			PLOG_RTPLIB_ERROR(LOG_PREFIX, "setup_srtp_policy -- initialize srtp filter in failed.");
			DELETEO(stream);
			return false;
		}

		m_srtp_filter_in = stream;
		PLOG_RTPLIB_WRITE(LOG_PREFIX, "setup_srtp_policy -- enabling srtp IN");
	}

	return true;
}
//------------------------------------------------------------------------------
bool rtp_secure_channel_t::setup_srtp_policy_out(const struct rtp_crypto_param_t* key_out)
{
	srtp_policy_t policy;
	srtp_stream_ctx_t* stream;

	if (m_srtp_filter_out != nullptr)
	{
		DELETEO(m_srtp_filter_out);
		m_srtp_filter_out = nullptr;
	}

	if (m_srtp_filter_out == nullptr)
	{
		stream = NEW srtp_stream_ctx_t(m_log);
		set_crypto_params_to_profile(key_out, &policy);
		if (!stream->initialize(&policy))
		{
			PLOG_RTPLIB_ERROR(LOG_PREFIX, "setup_srtp_policy -- initialize srtp filter out failed.");
			DELETEO(stream);
			return false;
		}

		m_srtp_filter_out = stream;
		PLOG_RTPLIB_WRITE(LOG_PREFIX, "setup_srtp_policy -- enabling srtp OUT");
	}

	return true;
}
//------------------------------------------------------------------------------
bool rtp_secure_channel_t::setup_srtp_policy(const rtp_crypto_param_t* key_in) // , const rtp_crypto_param_t* key_out
{
	if (!setup_srtp_policy_in(key_in))
	{
		return false;
	}

	if (m_sdp_crypto_param_out.crypto_suite == 0)
	{
		m_sdp_crypto_param_out.crypto_suite = key_in->crypto_suite;
		crypto_get_random(m_sdp_crypto_param_out.key, 30);
		m_sdp_crypto_param_out.null_rtp_auth = false;
		m_sdp_crypto_param_out.null_rtp_cipher = false;
		m_sdp_crypto_param_out.null_rtcp_cipher = false;
	}

	if (!setup_srtp_policy_out(key_in->crypto_suite == m_sdp_crypto_param_out.crypto_suite ? &m_sdp_crypto_param_out : &m_sdp_crypto_param_out_2))
	{
		DELETEO(m_srtp_filter_in);
		m_srtp_filter_in = nullptr;
		return false;
	}

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtl::String rtp_secure_channel_t::get_sdp_crypto_param_out()
{
	return rtp_crypto_to_string(&m_sdp_crypto_param_out, 1);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_secure_channel_t::prepare_crypto_out_params(rtl::StringList& params)
{
	m_sdp_crypto_param_out.crypto_suite = sdp_crypto_aes_cm_128_hmac_sha1_80_e;
	crypto_get_random(m_sdp_crypto_param_out.key, 30);
	m_sdp_crypto_param_out.null_rtp_auth = false;
	m_sdp_crypto_param_out.null_rtp_cipher = false;
	m_sdp_crypto_param_out.null_rtcp_cipher = false;

	params.add(rtp_crypto_to_string(&m_sdp_crypto_param_out, 1));

	m_sdp_crypto_param_out_2.crypto_suite = sdp_crypto_aes_cm_128_hmac_sha1_32_e;
	crypto_get_random(m_sdp_crypto_param_out_2.key, 30);
	m_sdp_crypto_param_out_2.null_rtp_auth = false;
	m_sdp_crypto_param_out_2.null_rtp_cipher = false;
	m_sdp_crypto_param_out_2.null_rtcp_cipher = false;

	params.add(rtp_crypto_to_string(&m_sdp_crypto_param_out_2, 2));
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_secure_channel_t::print_ports(const char* pef, rtl::Logger* log)
{
	if (log != nullptr)
	{
		log->log(LOG_PREFIX, "%s", pef);
		log->log(LOG_PREFIX, "--------------");
		if (m_data_port > 0)
		{
			rtl::String p1 = "";
			p1 << m_data_port;
			log->log(LOG_PREFIX, "      |%s", (const char*)p1);
		}

		if (m_ctrl_port > 0)
		{
			rtl::String p2 = "";
			p2 << m_ctrl_port;
			log->log(LOG_PREFIX, "      |%s", (const char*)p2);
		}

		log->log(LOG_PREFIX, "--------------");
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
