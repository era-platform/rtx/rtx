/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

//-----------------------------------------------
//
//-----------------------------------------------
struct rtcp_timer_callback_t
{
	virtual void rtcp_timer_elapsed(uint32_t timerId) = 0;
};
//-----------------------------------------------
//
//-----------------------------------------------
class rtcp_timer_t
{
public:
	static bool add_timer(rtcp_timer_callback_t* callback, uint32_t timerId, uint32_t timeout);
	static void remove_timer(rtcp_timer_callback_t* callback, uint32_t timerId);
	static void remove_all_timers(rtcp_timer_callback_t* callback);
};
//-----------------------------------------------
