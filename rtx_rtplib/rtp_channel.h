﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "rtx_rtplib.h"
#include "net/socket_io_service.h"
#include "rtp_packet.h"
#include "rtcp_packet.h"
#include "std_array_templates.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
enum rtp_channel_type_t
{
	rtp_channel_type_simple_e,			// канал с двумя портами (дата и контрольный)
	rtp_channel_type_secure_e,			// защищенный канал с двумя портами (дата и контрольный)
	rtp_channel_type_webrtc_e,			// канал webrtc
	rtp_channel_type_webrtc_bundle_e,	// канал webrtc с одним портом
	rtp_channel_type_udptl_proxy_e
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
struct rtp_channel_stat_t
{
	volatile uint32_t tx_packets;
	volatile uint32_t tx_bytes;
	volatile uint32_t rx_packets;
	volatile uint32_t rx_bytes;
	uint32_t timeStart;
	char name[64]; // !rtp_channel_t have const 64 length name. see rtp_channel_t class
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class RTX_RTPLIB_API rtp_channel_t : public i_socket_event_handler, public net_sender_handler
{
	rtp_channel_t(const rtp_channel_t&);
	rtp_channel_t& operator=(const rtp_channel_t&);

public:
	rtp_channel_t(rtl::Logger* log);
	virtual ~rtp_channel_t();

	virtual const uint8_t* getName() const;
	virtual rtp_channel_type_t getType() const = 0;

	virtual bool destroy() = 0;
	virtual bool started() = 0;

	virtual int start_io(socket_io_service_t* svc) = 0;
	virtual void stop_io() = 0;

	virtual const ip_address_t* get_net_interface() const = 0;
	virtual const uint16_t get_data_port() const = 0;
	
	virtual const socket_io_service_t* get_io_service() const = 0;
	virtual void set_io_service(socket_io_service_t* iosvc) = 0;

	virtual bool send_packet(const rtp_packet* packet, const socket_address* saddr) = 0;
	virtual bool send_data(const uint8_t* data, uint32_t len, const socket_address* saddr) = 0;
	virtual bool send_ctrl(const rtcp_packet_t* packet, const socket_address* saddr) = 0;

	// при уничтожении канал какое то время находится на кладбище.
	// и уже потом удаляется. Для этого в метод передается время удаления канала.
	void set_time_destroy(uint64_t time);
	uint64_t get_time_destroy();

	// Иногда на порт приходят данные которые адресованны другому каналу.
	// Для этого создаем зависимость цепочки каналов принадлежащие одному теринатору.
	// Передаем сюда канал предшественник.
	void set_next_channel(rtp_channel_t* channel);
	rtp_channel_t* get_next_channel();

	virtual void socket_data_available(net_socket* sock) = 0;
	virtual void socket_data_received(net_socket* sock, const uint8_t* data, uint32_t len, const socket_address* from) = 0;

	static void print_statistics(rtl::FileStream& stream);

	virtual void print_ports(const char* pef, rtl::Logger* log) = 0;

protected:
	void set_name(const char* name);

	static void insert_stat(rtp_channel_t* channel);
	static void remove_stat(rtp_channel_t* channel);

	void update_rx_stat(uint32_t bytes) { std_interlocked_inc(&m_stat.rx_packets), std_interlocked_add(&m_stat.rx_bytes, bytes); }
	void update_tx_stat(uint32_t bytes) { std_interlocked_inc(&m_stat.tx_packets), std_interlocked_add(&m_stat.tx_bytes, bytes); }

private:
	rtl::Logger* m_log;
	const static uint32_t m_size_name = 64;
	uint8_t m_name[m_size_name];
	uint64_t m_death_time;
	rtp_channel_t* m_next_channel;
	rtp_channel_stat_t m_stat;
	volatile rtp_channel_t* m_next_stat;
	volatile rtp_channel_t* m_prev_stat;
};
//------------------------------------------------------------------------------
// Хендлер стрима. Хранится в канале.
//------------------------------------------------------------------------------
class RTX_RTPLIB_API i_rx_channel_event_handler
{
	i_rx_channel_event_handler(const i_rx_channel_event_handler&);
	i_rx_channel_event_handler&	operator=(const i_rx_channel_event_handler&);

public:
	i_rx_channel_event_handler();
	virtual	~i_rx_channel_event_handler();

	// отправить пакет из канала в стрим.
	virtual bool rx_packet_stream(const rtp_channel_t* channel, const rtp_packet* packet, const socket_address* from) = 0;
	// отправить данные из канала в стрим.
	virtual bool rx_data_stream(const rtp_channel_t* channel, const uint8_t* data, uint32_t len, const socket_address* from) = 0;
	// отправить контрольный пакет в стрим
	virtual bool rx_ctrl_stream(const rtp_channel_t* channel, const rtcp_packet_t* packet, const socket_address* from) = 0;

	// сигнал на внешку.
	//virtual void mg_termination__net_error() = 0;
};
//------------------------------------------------------------------------------
// Хендлер канала. Хранится в стриме.
//------------------------------------------------------------------------------
class RTX_RTPLIB_API i_tx_channel_event_handler
{
	i_tx_channel_event_handler(const i_tx_channel_event_handler&);
	i_tx_channel_event_handler&	operator=(const i_tx_channel_event_handler&);

public:
	i_tx_channel_event_handler();
	virtual ~i_tx_channel_event_handler();

	// отправить пакет из стрима в канал.
	virtual void tx_packet_channel(const rtp_packet* packet, const socket_address* to) = 0;
	// отправить данные из стрима в канал.
	virtual void tx_data_channel(const uint8_t* data, uint32_t len, const socket_address* to) = 0;
	// отправить контрльный пакет из стрима в канал.
	virtual void tx_ctrl_channel(const rtcp_packet_t* packet, const socket_address* to) = 0;

	// для синхронизации при удалении канала
	virtual void inc_tx_subscriber(uint32_t ssrc) = 0;
	virtual void dec_tx_subscriber(uint32_t ssrc) = 0;
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
struct mg_channel_at_stream_t
{
	uint32_t stream_id;
	rtp_channel_t* channel;
};
typedef rtl::ArrayT<mg_channel_at_stream_t*> rtp_channel_list;
//--------------------------------------
