﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 //--------------------------------------
// a replay database with extended range, using a rollover counter
//--------------------------------------
// based on libsrtp written by
// David A. McGrew
// Cisco Systems, Inc.
//--------------------------------------
#include "stdafx.h"
#include "srtp_rdbx.h"
//--------------------------------------
//
//--------------------------------------
#define rdbx_high_bit_in_bitmask 127
//--------------------------------------
// index_guess(local, guess, s)
//
// given a xtd_seq_num_t local (which represents the highest
// known-to-be-good index) and a sequence number s (from a newly
// arrived packet), sets the contents of *guess to contain the best
// guess of the packet index to which s corresponds, and returns the
// difference between *guess and *local
//--------------------------------------
static int index_guess(const xtd_seq_num_t *local, xtd_seq_num_t *guess, sequence_number_t s)
{
	uint32_t local_roc = (uint32_t)(*local >> 16);
	uint16_t local_seq = (uint16_t) *local;

	uint32_t guess_roc = (uint32_t)(*guess >> 16);
	uint16_t guess_seq = (uint16_t) *guess;  

	int difference;

	if (local_seq < seq_num_median)
	{
		if (s - local_seq > seq_num_median)
		{
			guess_roc = local_roc - 1;
			difference = seq_num_max - s + local_seq;
		}
		else
		{
			guess_roc = local_roc;
			difference = s - local_seq;
		}
	}
	else
	{
		if (local_seq - seq_num_median > s)
		{
			guess_roc = local_roc+1;
			difference = seq_num_max - local_seq + s;
		}
		else
		{
			difference = s - local_seq;
			guess_roc = local_roc;
		}
	}
	guess_seq = s;

	/* Note: guess_roc is 32 bits, so this generates a 48-bit result! */
	*guess = (((uint64_t) guess_roc) << 16) | guess_seq;

	return difference;
}
//--------------------------------------
//
//--------------------------------------
rdbx_t::rdbx_t()
{
	v128_set_to_zero(&m_bitmask);
	m_index = 0;
}
//--------------------------------------
// rdbx_check(&r, delta) checks to see if the xtd_seq_num_t which is at rdbx->index + delta is in the rdb
//--------------------------------------
bool rdbx_t::check(int delta) const
{
	if (delta > 0)
	{
		/* if delta is positive, it's good */
		return true;
	}
	else if (rdbx_high_bit_in_bitmask + delta < 0)
	{
		/* if delta is lower than the bitmask, it's bad */
		return false; 
	}
	else if (v128_get_bit(&m_bitmask, rdbx_high_bit_in_bitmask + delta) == 1)
	{
		/* delta is within the window, so check the bitmask */
		return false;
	}

	/* otherwise, the index is okay */
	return true; 
}
//--------------------------------------
// rdbx_add_index adds the xtd_seq_num_t at rdbx->window_start + d to
// replay_db (and does *not* check if that xtd_seq_num_t appears in db)
//
// this function should be called only after replay_check has
// indicated that the index does not appear in the rdbx, e.g., a mutex
// should protect the rdbx between these calls if need be
//--------------------------------------
bool rdbx_t::add_index(int delta)
{
	if (delta > 0)
	{
		/* shift forward by delta */
		m_index += delta;
		v128_left_shift(&m_bitmask, delta);
		v128_set_bit(&m_bitmask, 127);
	}
	else
	{
		/* delta is in window, so flip bit in bitmask */
		v128_set_bit(&m_bitmask, -delta);
	}

	/* note that we need not consider the case that delta == 0 */
	return true;
}
//--------------------------------------
// given an rdbx and a sequence number s (from a newly arrived packet),
// sets the contents of *guess to contain the best guess of the packet
// index to which s corresponds, and returns the difference between
// *guess and the locally stored synch info
//--------------------------------------
int rdbx_t::estimate_index(xtd_seq_num_t *guess, sequence_number_t s) const
{
	/*
	* if the sequence number and rollover counter in the rdbx are
	* non-zero, then use the index_guess(...) function, otherwise, just
	* set the rollover counter to zero (since the index_guess(...)
	* function might incorrectly guess that the rollover counter is
	* 0xffffffff)
	*/

	if (m_index > seq_num_median)
		return index_guess(&m_index, guess, s);

	*guess = s;

	return s - (uint16_t) m_index;
}
//--------------------------------------
// rdb_init initalizes rdb
//--------------------------------------
rdb_t::rdb_t()
{
	v128_set_to_zero(&m_bitmask);
	m_window_start = 0;
}
//--------------------------------------
// rdb_check checks to see if index appears in rdb
//--------------------------------------
bool rdb_t::check(uint32_t index) const
{
	/* if the index appears after (or at very end of) the window, its good */
	if (index >= m_window_start + rdb_bits_in_bitmask)
		return true;

	/* if the index appears before the window, its bad */
	if (index < m_window_start)
		return false;

	/* otherwise, the index appears within the window, so check the bitmask */
	if (v128_get_bit(&m_bitmask, (index - m_window_start)) == 1)
		return false;

	/* otherwise, the index is okay */
	return true;
}
//--------------------------------------
//
// rdb_add_index adds index to rdb_t (and does *not* check if
// index appears in db)
//
// this function should be called only after rdb_check has
// indicated that the index does not appear in the rdb, e.g., a mutex
// should protect the rdb between these calls
//--------------------------------------
bool rdb_t::add_index(uint32_t index)
{
	int delta;  

	/* here we *assume* that index > rdb->window_start */

	delta = (index - m_window_start);    
	if (delta < rdb_bits_in_bitmask)
	{
		/* if the index is within the window, set the appropriate bit */
		v128_set_bit(&m_bitmask, delta);

	}
	else
	{ 
		delta -= rdb_bits_in_bitmask - 1;

		/* shift the window forward by delta bits*/
		v128_left_shift(&m_bitmask, delta);
		v128_set_bit(&m_bitmask, rdb_bits_in_bitmask-delta);
		m_window_start += delta;
	}    

	return true;
}
//--------------------------------------
