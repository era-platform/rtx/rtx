﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 //--------------------------------------
// header for the cryptographic kernel
//--------------------------------------
// based on libsrtp written by
// David A. McGrew
// Cisco Systems, Inc.
//--------------------------------------
#include "stdafx.h"
#include "srtp_crypto.h"
#include "srtp_hmac.h"
#include "srtp_aes_icm.h"
//--------------------------------------
//
//--------------------------------------
cipher_t* crypto_alloc_cipher(int cipher_type, int key_len)
{
	cipher_t* cipher = NULL;

	if (cipher_type == NULL_CIPHER)
		cipher = NEW null_cipher_ctx_t(key_len);
	else if (cipher_type == AES_128_ICM)
		cipher = NEW aes_icm_ctx_t(key_len);

	return cipher;
}
//--------------------------------------
//
//--------------------------------------
auth_t* crypto_alloc_auth(int auth_type, int key_len, int tag_len)
{
	auth_t* auth = NULL;

	if (auth_type == NULL_AUTH)
		auth = NEW null_auth_ctx_t(key_len, tag_len);
	else
		auth = NEW hmac_ctx_t(key_len, tag_len);

	return auth;
}
//--------------------------------------
//
//--------------------------------------
//bool crypto_get_random(uint8_t* buffer, uint32_t length)
//{
//	uint32_t *dst = (uint32_t*)buffer;
//	int int_len = length / 4;
//	int byte_len = length % 4;
//	uint32_t val = 0;
//
//	srand(rtl::DateTime::getTicks());
//
//	while (int_len)
//	{
//		val = rand();
//
//		*dst++ = val;
//		int_len--;
//	}
//
//	if (byte_len > 0)
//	{
//		uint8_t* bytes = (uint8_t*)dst;
//		val = rand();
//
//		while (byte_len)
//		{
//			*bytes++ = val >> (--byte_len * 8);
//		}
//	}
//
//	return true;
//	///* Generic C-library (rand()) version */
//	///* This is a random source of last resort */
//	//uint8_t *dst = (uint8_t *)buffer;
//	//while (length)
//	//{
//	//	int val = rand();
//	//	/* rand() returns 0-32767 (ugh) */
//	//	/* Is this a good enough way to get random bytes?
//	//	It is if it passes FIPS-140... */
//	//	*dst++ = val & 0xff;
//	//	length--;
//	//}
//}
//--------------------------------------
