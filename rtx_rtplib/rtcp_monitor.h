/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtcp_packet.h"
#include "rtcp_timer.h"
#include "std_crypto.h"
#include "rtp_channel.h"
#include "rtp_packet.h"
//--------------------------------------
// RTCP receive event callback
// decoded compaund rtcp packet object
//--------------------------------------
struct RTX_RTPLIB_API rtcp_monitor_event_handler_rx_t
{
	// ����� � ����� � ����
	virtual int rtcp_session_rx(const rtcp_packet_t* packet) = 0;
};
struct RTX_RTPLIB_API rtcp_monitor_event_handler_tx_t
{
	// ������� � �����
	virtual int rtcp_session_tx(const rtcp_packet_t* packet) = 0;
};
//--------------------------------------
//
//--------------------------------------
struct rtcp_source_t
{
	uint32_t ssrc;				/* source's ssrc */
	uint16_t max_seq;			/* highest seq. number seen */
	uint32_t cycles;			/* shifted count of seq. number cycles */
	uint32_t base_seq;			/* base seq number */
	uint32_t bad_seq;			/* last 'bad' seq number + 1 */
	uint32_t probation;			/* sequ. packets till source is valid */
	uint32_t received;			/* packets received */
	uint32_t expected_prior;	/* packet expected at last interval */
	uint32_t received_prior;	/* packet received at last interval */
	uint32_t transit;			/* relative trans time for prev pkt */
	double jitter;				/* estimated jitter */
	
	uint32_t base_ts;			/* base timestamp */
	uint32_t max_ts;			/* highest timestamp number seen */
	uint32_t rate;				/* codec sampling rate */

	uint32_t ntp_msw;			/* last received NTP timestamp from RTCP sender */
	uint32_t ntp_lsw;			/* last received NTP timestamp from RTCP sender */
	uint64_t dlsr;				/* delay since last SR */

	uint32_t packets_sent;
	uint32_t octets_sent;
};
//--------------------------------------
//
//--------------------------------------
enum rtcp_event_t
{
	rtcp_event_bye_e,
	rtcp_event_report_e,
	rtcp_event_rtp_e
};
//--------------------------------------
//
//--------------------------------------
enum rtcp_packet_type_t
{
	rtcp_packet_report_e,
	rtcp_packet_bye_e,
	rtcp_packet_rtp_e,
};
//--------------------------------------
//
//--------------------------------------
struct rtcp_triple_t
{
	uint32_t ssrc;
	uint32_t ssrc_opposite;
	char cname[MD5_HASH_HEX_SIZE + 1];
};
typedef rtl::ArrayT<rtcp_triple_t> mg_rtcp_triple_list;
//--------------------------------------
//
//--------------------------------------
class rtcp_monitor_t : public rtcp_timer_callback_t
{
	rtl::Mutex m_sync;

	rtl::Logger* m_log;

	bool m_webrtc;

	rtcp_monitor_event_handler_rx_t* m_event_handler_rx;
	rtcp_monitor_event_handler_tx_t* m_event_handler_tx;

	/// timer values
	bool m_timer_started;

	rtcp_source_t* m_source_local;		// local source
	rtcp_sdes_chunk_t* m_sdes;
	uint64_t m_time_start;				// Start time in millis (NOT in NTP unit yet)
	
	// <RTCP-FB>
	uint8_t m_fir_seqnr;
	// </RTCP-FB>

	// <sender>
	char m_cname[MD5_HASH_HEX_SIZE+1];
	bool m_is_cname_defined;
	uint32_t m_packets_count;
	uint32_t m_octets_count;
	uint32_t m_ssrc_opposite;
	// </sender>

	// <others>
	uint64_t m_tp;			// the last time an RTCP packet was transmitted;
	uint64_t m_tn;			// the next scheduled transmission time of an RTCP packet
	int32_t m_pmembers;		// the estimated number of session members at the time tn was last recomputed
	int32_t m_members;		// the most current estimate for the number of session members
	int32_t m_senders;		// the most current estimate for the number of senders in the session
	double m_rtcp_bw;		// The target RTCP bandwidth, i.e., the total bandwidth that will be used for RTCP packets by all members of this session, in octets per second.
							// This will be a specified fraction of the "session bandwidth" parameter supplied to the application at startup
	bool m_we_sent;			// Flag that is true if the application has sent data since the 2nd previous RTCP report was transmitted
	double m_avg_rtcp_size; // The average compound RTCP packet size, in octets, over all RTCP packets sent and received by this participant.
							// The size includes lower-layer transport and network protocol headers (e.g., UDP and IP) as explained in Section 6.2
	bool m_initial;			// Flag that is true if the application has not yet sent an RTCP packet
	// </others>

	rtl::ArrayT<rtcp_source_t*> m_sources;
	const static uint32_t m_name_size = 64;
	char m_context[m_name_size];
	char m_termination[m_name_size];
	char m_addr[m_name_size];

	mg_rtcp_triple_list m_triple_list;

public:
	RTX_RTPLIB_API rtcp_monitor_t(rtl::Logger* log, const char* name, const char* addr, bool webrtc);
	RTX_RTPLIB_API ~rtcp_monitor_t();

	RTX_RTPLIB_API void set_session_event_handler_tx(rtcp_monitor_event_handler_tx_t* event_handler);
	RTX_RTPLIB_API void set_session_event_handler_rx(rtcp_monitor_event_handler_rx_t* event_handler);

	RTX_RTPLIB_API void setup(uint32_t ssrc, uint32_t ssrc_opposite, const char* cname);
	RTX_RTPLIB_API void reset();

	RTX_RTPLIB_API bool start();
	RTX_RTPLIB_API void stop();
	
	RTX_RTPLIB_API bool coincidence(uint32_t ssrc, uint32_t ssrc_opposite, const char* cname);
	RTX_RTPLIB_API bool match(uint32_t ssrc, uint32_t ssrc_opposite, const char* cname);
	RTX_RTPLIB_API uint32_t get_ssrc();
	RTX_RTPLIB_API uint32_t get_ssrc_opposite();
	RTX_RTPLIB_API const char* get_cname();

	bool signal_packet_loss(uint32_t ssrc_media, const uint16_t* seq_nums, int count);
	bool signal_frame_corrupted(uint32_t ssrc_media);
	bool signal_jb_error(uint32_t ssrc_media);

	RTX_RTPLIB_API void rtcp_event_tx_data(const rtp_packet* packet);

	RTX_RTPLIB_API void rtcp_event_rx_data(const rtp_packet* packet);
	RTX_RTPLIB_API void rtcp_event_rx_ctrl(const rtcp_packet_t* packet);

	RTX_RTPLIB_API bool recalc_packet(rtcp_packet_t* packet);

	RTX_RTPLIB_API void print(rtl::Logger* log);

	RTX_RTPLIB_API void set_triple(const mg_rtcp_triple_list* triple_list);
private:
	void clear_sourses();
	bool have_source(uint32_t ssrc);
	rtcp_source_t* find_source(uint32_t ssrc);
	
	bool add_source(rtcp_source_t* source);
	bool add_source(uint32_t ssrc, uint16_t seq, uint32_t ts);
	bool remove_source(uint32_t ssrc);
	
	void schedule(double tn, rtcp_event_t e);
	void process_rtp_in(const rtp_packet* packet);
	void process_rtcp_in(const rtcp_packet_t* packet, rtcp_event_t e);

	double calc_rtcp_interval();
	void expire_event(rtcp_event_t e);

	int send_packet(rtcp_packet_t* pkt);
	bool send_bye_event(rtcp_event_t e);
	bool send_rtcp_report(rtcp_event_t e);
	
	bool is_new_member_rtp(const rtp_packet* packet);
	bool is_new_member_rtcp(const rtcp_base_packet_t* packet);

	int add_member_rtp(const rtp_packet* packet, bool sender);
	int add_member_rtcp(const rtcp_packet_t* packet, bool sender);
	int add_member_by_packet(const rtcp_base_packet_t* packet, bool sender);

	int remove_member_rtp(const rtp_packet* packet);
	int remove_member_rtcp(const rtcp_packet_t* packet);
	int remove_member_by_packet(const rtcp_base_packet_t* packet);

	/// timer_event_handler_t interface
	virtual void rtcp_timer_elapsed(uint32_t timerId) override;

	uint32_t find_oposite_ssrc(uint32_t ssrc);
};
//--------------------------------------
