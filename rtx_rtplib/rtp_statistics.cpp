/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtp_statistics.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtp_statistics_t::rtp_statistics_t(const char* name, const char* addr)
{
	clear();

	if (name == nullptr)
	{
		return;
	}
	
	const char* tmp1 = strstr(name, "-");
	tmp1++;
	tmp1++;
	const char* tmp2 = strstr(tmp1, "-");
	uint32_t size1 = uint32_t(tmp2 - tmp1);
	tmp2++;
	const char* tmp3 = strstr(tmp2, "-");
	uint32_t size2 = uint32_t(tmp3 - tmp2);

	uint32_t size = (size1 > m_name_size) ? m_name_size : size1;
	memcpy(m_context, tmp1, size);
	size = (size2 > m_name_size) ? m_name_size : size2;
	memcpy(m_termination, tmp2, size);

	strncpy(m_addr, addr, m_name_size);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtp_statistics_t::~rtp_statistics_t()
{
	clear();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_statistics_t::clear()
{
	memset(&m_rx_statistics, 0, sizeof(stream_statistics_t));
	memset(&m_tx_statistics, 0, sizeof(stream_statistics_t));

	m_time_step = 20;

	memset(m_context, 0, m_name_size);
	memset(m_termination, 0, m_name_size);
	memset(m_id, 0, m_name_size);
	memset(m_addr, 0, m_name_size);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
stream_statistics_t* rtp_statistics_t::get_stream_statistics_rx()
{
	return &m_rx_statistics;
}
//------------------------------------------------------------------------------
stream_statistics_t* rtp_statistics_t::get_stream_statistics_tx()
{
	return &m_tx_statistics;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_statistics_t::set_id(const char* id, uint32_t id_size)
{
	memset(m_id, 0, m_name_size);
	uint32_t size = (id_size > m_name_size) ? m_name_size : id_size;
	memcpy(m_id, id, size);
}
//------------------------------------------------------------------------------
bool rtp_statistics_t::check_id(const char* id, uint32_t id_size)
{
	uint32_t size = (id_size > m_name_size) ? m_name_size : id_size;
	return memcmp(m_id, id, size) == 0;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_statistics_t::start()
{
	uint32_t tick = rtl::DateTime::getTicks();
	start_rx(tick);
	start_tx(tick);
}
//------------------------------------------------------------------------------
void rtp_statistics_t::start_rx()
{
	start_rx(rtl::DateTime::getTicks());
}
//------------------------------------------------------------------------------
void rtp_statistics_t::start_tx()
{
	start_tx(rtl::DateTime::getTicks());
}
//------------------------------------------------------------------------------
void rtp_statistics_t::start_rx(uint32_t tick)
{
	m_rx_statistics.last_recv_time = tick;
	m_rx_statistics.start_time = tick;
}
//------------------------------------------------------------------------------
void rtp_statistics_t::start_tx(uint32_t tick)
{
	m_tx_statistics.last_recv_time = tick;
	m_tx_statistics.start_time = tick;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_statistics_t::stop()
{
	uint32_t tick = rtl::DateTime::getTicks();
	stop_rx(tick);
	stop_tx(tick);
}
//------------------------------------------------------------------------------
void rtp_statistics_t::stop_rx()
{
	stop_rx(rtl::DateTime::getTicks());
}
//------------------------------------------------------------------------------
void rtp_statistics_t::stop_tx()
{
	stop_tx(rtl::DateTime::getTicks());
}
//------------------------------------------------------------------------------
void rtp_statistics_t::stop_rx(uint32_t tick)
{
	m_rx_statistics.stop_time = tick;
}
//------------------------------------------------------------------------------
void rtp_statistics_t::stop_tx(uint32_t tick)
{
	m_tx_statistics.stop_time = tick;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_statistics_t::update()
{
	uint32_t tick = rtl::DateTime::getTicks();
	update_rx(tick);
	update_tx(tick);
}
//------------------------------------------------------------------------------
void rtp_statistics_t::update_rx()
{
	update_rx(rtl::DateTime::getTicks());
}
//------------------------------------------------------------------------------
void rtp_statistics_t::update_tx()
{
	update_tx(rtl::DateTime::getTicks());
}
//------------------------------------------------------------------------------
void rtp_statistics_t::update_rx(uint32_t tick)
{
	int delay = int(tick - m_rx_statistics.last_recv_time);

	if (delay > int(m_time_step * 2))
	{
		m_rx_statistics.delayed_count++;

		if (delay > m_rx_statistics.max_delay)
		{
			m_rx_statistics.max_delay = delay;
		}
	}

	m_rx_statistics.last_recv_time = tick;
}
//------------------------------------------------------------------------------
void rtp_statistics_t::update_tx(uint32_t tick)
{
	int delay = int(tick - m_tx_statistics.last_recv_time);

	if (delay > int(m_time_step * 2))
	{
		m_tx_statistics.delayed_count++;

		if (delay > m_tx_statistics.max_delay)
		{
			m_tx_statistics.max_delay = delay;
		}
	}

	m_tx_statistics.last_recv_time = tick;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_statistics_t::print_statistics(rtl::Logger* log)
{
	if (log == nullptr)
	{
		return;
	}

	log->log("RTPSTAT", "Context = %s, Termination = %s (%s)\n\
\t----------OUT------------\n\
\tpackets sent     : %d\n\
\tbytes sent       : %d\n\
\tdelays           : %d\n\
\tmax delay        : %u\n\
\taverage delays   : %u\n\
\t-----------IN------------\n\
\tpackets received : %d\n\
\tbytes received   : %d\n\
\tout of order     : %d\n\
\tinvalid          : %d\n\
\tlost             : %d\n\
\tdelays           : %d\n\
\tmax delay        : %u\n\
\taverage delays   : %u\n\
			",
			m_context, m_termination, m_addr,
			m_tx_statistics.packets, m_tx_statistics.bytes,
			m_tx_statistics.delayed_count, m_tx_statistics.max_delay,
			(m_tx_statistics.stop_time - m_tx_statistics.start_time) / (m_tx_statistics.packets ? m_tx_statistics.packets : 1),
			m_rx_statistics.packets, m_rx_statistics.bytes, m_rx_statistics.out_of_order, m_rx_statistics.invalid, m_rx_statistics.lost,
			m_rx_statistics.delayed_count, m_rx_statistics.max_delay,
			(m_rx_statistics.stop_time - m_rx_statistics.start_time) / (m_rx_statistics.packets ? m_rx_statistics.packets : 1));
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_statistics_t::change_time_step(uint32_t time_step)
{
	m_time_step = time_step;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
