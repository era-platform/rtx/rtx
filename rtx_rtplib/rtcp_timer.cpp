/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtcp_timer.h"
//-----------------------------------------------
//
//-----------------------------------------------
static bool s_initialized = false;
static rtl::Mutex s_sync;
struct rtcp_timer_user_t
{
	rtcp_timer_callback_t* callback;
	uint32_t timerId;
	uint32_t fireTime;
	//.....
	rtcp_timer_user_t* next;
	rtcp_timer_user_t* prev;
};
static rtcp_timer_user_t* s_first;
static rtcp_timer_user_t* s_last;
static volatile bool s_timerEnter;
static volatile uint32_t s_timerTickCounter;
static rtl::Timer* s_timer = nullptr;

static void timer_elapsed(rtl::Timer* sender, uint32_t elapsed, void* userData, int tid);
//-----------------------------------------------
//
//-----------------------------------------------
static void rtcp_timer_start()
{
	rtl::MutexLock lock(s_sync);

	if (s_initialized)
		return;

	if (s_timer == nullptr)
	{
		s_timerEnter = false;
		s_timer = NEW rtl::Timer(false, timer_elapsed, nullptr, Log, "rtcp-timer");
		bool res = s_timer->start(1000);
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
static void rtcp_timer_stop()
{
	rtl::MutexLock lock(s_sync);

	if (!s_initialized)
		return;

	s_initialized = false;

	if (s_timer != nullptr)
	{
		s_first = s_last = nullptr;
		s_timer->stop();
		DELETEO(s_timer);
		s_timer = nullptr;
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
bool rtcp_timer_t::add_timer(rtcp_timer_callback_t* callback, uint32_t timerId, uint32_t timeout)
{
	if (!s_initialized)
		return false;

	rtl::MutexLock lock(s_sync);

	rtcp_timer_user_t* user = NEW rtcp_timer_user_t { callback, timerId, rtl::DateTime::getTicks() + timeout };

	if (s_last == nullptr)
	{
		s_first = s_last = user;

		rtcp_timer_start();
	}
	else
	{
		s_last->next = user;
		s_last = user;
	}

	user->next = nullptr;

	return true;
}
//-----------------------------------------------
//
//-----------------------------------------------
void rtcp_timer_t::remove_timer(rtcp_timer_callback_t* callback, uint32_t timerId)
{
	if (!s_initialized)
		return;

	rtl::MutexLock lock(s_sync);

	//if (user != nullptr && *user != nullptr)
	//	DELETEO(user);

	rtcp_timer_user_t* current = s_first;
	rtcp_timer_user_t* prev = nullptr;

	while (current != nullptr && current->callback != callback && current->timerId != timerId)
	{
		prev = current;
		current = current->next;
	}

	if (current != nullptr)
	{
		if (prev != nullptr)
		{
			prev->next = current->next;

			// ���� ��� ��������� �� ������� ������ �� ����������
			if (current == s_last)
				s_last = prev;
		}
		else
		{
			s_first = current->next;

			// ���� ������������ �� ������ �� ���������� ���� ����������
			if (s_first == nullptr)
			{
				s_last = nullptr;
			}
		}

		// ���� ��������� ������ �������
		while (s_timerEnter)
		{
			//SwitchToThread();
			rtl::Thread::sleep(5);
		}

		if (s_first == nullptr)
		{
			rtcp_timer_stop();
		}

		DELETEO(current);
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
void rtcp_timer_t::remove_all_timers(rtcp_timer_callback_t* callback)
{
	if (!s_initialized)
		return;

	rtl::MutexLock lock(s_sync);

	rtcp_timer_user_t* current = s_first;
	rtcp_timer_user_t* prev = nullptr;

	while (current != nullptr && current->callback != callback)
	{
		prev = current;
		current = current->next;
	}

	if (current != nullptr)
	{
		if (prev != nullptr)
		{
			prev->next = current->next;

			// ���� ��� ��������� �� ������� ������ �� ����������
			if (current == s_last)
				s_last = prev;
		}
		else
		{
			s_first = current->next;

			// ���� ������������ �� ������ �� ���������� ���� ����������
			if (s_first == nullptr)
			{
				s_last = nullptr;
			}
		}

		// ���� ��������� ������ �������
		while (s_timerEnter)
		{
			//SwitchToThread();
			rtl::Thread::sleep(5);
		}

		if (s_first == nullptr)
		{
			rtcp_timer_stop();
		}

		DELETEO(current);
	}
}
static void async_timer_proc(void* userData, uint16_t callId, uintptr_t intParam, void* ptrParam)
{
	s_timerEnter = true;

	uint32_t currentTime = rtl::DateTime::getTicks();

	rtcp_timer_user_t* current = s_first;

	while (current != nullptr)
	{
		//	currentTime - current->m_timer_last_tick, current->m_timer_period - 10);
		// �������� ���� � ����������� �� 10 ����
		if (currentTime - current->fireTime > 0)
		{
			current->callback->rtcp_timer_elapsed(current->timerId);
		}

		current = current->next;
	}

	s_timerEnter = false;
}
//-----------------------------------------------
//
//-----------------------------------------------
void timer_elapsed(rtl::Timer* sender, uint32_t elapsed, void* userData, int tid)
{
	async_timer_proc(nullptr, 0, 0, nullptr);
}
//-----------------------------------------------
