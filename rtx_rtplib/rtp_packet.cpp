﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtp_packet.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtp_packet::rtp_packet()
{
	reset();
}
rtp_packet::~rtp_packet()
{
}
//------------------------------------------------------------------------------
// распаковка rtp-пакета из буфера.
//------------------------------------------------------------------------------
//	0                   1                   2                   3
//	0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
//	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//	|V=2|P|X|  CC   |M|     PT      |       sequence number         |
//	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//	|                           timestamp                           |
//	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//	|           synchronization source (SSRC) identifier            |
//	+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
//	|            contributing source (CSRC) identifiers             |
//	|                             ....                              |
//	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//------------------------------------------------------------------------------
bool rtp_packet::read_packet(const void* data, uint32_t length)
{
	if (length < FIXED_RTP_HEADER_LENGTH)
		return false;

	if (length > FIXED_RTP_HEADER_LENGTH + PAYLOAD_BUFFER_SIZE)
		return false;

	reset();

	const uint8_t* pdata = (const uint8_t*) data;

	//-------------------------

	m_version = *pdata;
	m_version &= 0xC0; // 1100 0000
	m_version >>= 6;

	uint8_t byte = *pdata;
	byte &= 0x20; // 0010 0000
	bool padding_flag = (byte != 0);

	byte = *pdata;
	byte &= 0x10; // 0001 0000
	m_externsion_flag = (byte != 0);

	m_csrc_count = *pdata;
	m_csrc_count &= 0x0F; // 0000 1111

	++pdata;
	--length;

	//-------------------------

	byte = *pdata;
	byte &= 0x80; // 1000 0000
	m_marker = (byte != 0);

	m_payload_type = *pdata;
	m_payload_type &= 0x7F; // 0111 1111

	++pdata;
	--length;

	//-------------------------

	m_sequence_number = *((const uint16_t*)pdata);
	m_sequence_number = ntohs(m_sequence_number);

	pdata += sizeof(uint16_t);
	length -= sizeof(uint16_t);

	//-------------------------

	m_timestamp = *((const uint32_t*)pdata);
	m_timestamp = ntohl(m_timestamp);

	pdata += sizeof(uint32_t);
	length -= sizeof(uint32_t);

	//-------------------------

	m_ssrc = *((const uint32_t*)pdata);
	m_ssrc = ntohl(m_ssrc);

	pdata += sizeof(uint32_t);
	length -= sizeof(uint32_t);

	//-------------------------

	if (m_csrc_count != 0)
	{
		for (uint8_t i = 0; i < m_csrc_count; ++i)
		{
			if (length < sizeof(uint32_t))
				return false;

			uint32_t dword = *((const uint32_t*)pdata);
			dword = ntohl(dword);
			m_csrc_list[i] = dword;

			pdata += sizeof(uint32_t);
			length -= sizeof(uint32_t);
		}
	}

	//-------------------------

	if (m_externsion_flag)
	{
		if (length < sizeof(uint32_t))
			return false;
		/* One-Byte Header
		 0                   1                   2                   3
		 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		|       0xBE    |    0xDE       |           length=3            |
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		|  ID   | L=0   |     data      |  ID   |  L=1  |   data...
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
			...data     |    0 (pad)    |    0 (pad)    |  ID   | L=3   |
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		|                          data                                 |
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		*/
		
		uint16_t len = *((const uint16_t*)(pdata + sizeof(uint16_t)));
		len = ntohs(len);

		m_extension_length = sizeof(uint16_t) + sizeof(uint16_t);
		/*for (int i = 0; i < len; i++)
		{
			const uint8_t* next_ext = pdata + m_extension_length;
			if ((*next_ext) == 0)
			{
				m_extension_length += sizeof(uint8_t);
				i--;
				continue;
			}

			uint8_t len_ext = ((*next_ext & 0xf0) >> 4) + sizeof(uint8_t);

			m_extension_length += sizeof(uint8_t) + len_ext;
		}*/

		m_extension_length += len * sizeof(uint32_t);

		if (m_extension_length > FIXED_RTP_EXTENSION_LENGTH)
		{
			return false;
		}
		memcpy(m_extension_buff, pdata, m_extension_length);

		if (length < m_extension_length)
			return false;

		pdata += m_extension_length;
		length -= m_extension_length;
	}

	//-------------------------

	if (padding_flag)
	{
		if (length == 0)
			return false;

		// в последнем байте всех данных лежит количество байт паддинга (включая сам этот байт).
		m_padding_count = pdata[length-1];
		if (length < m_padding_count)
			return false;
		length -= m_padding_count;
	}

	//-------------------------

	// если остались еще данные, то это payload
	if (length != 0)
	{
		memcpy(m_payload, pdata, length);
		m_payload_length = length;
	}

	return true;
}
//------------------------------------------------------------------------------
// упаковка rtp-пакета в буфер.
//------------------------------------------------------------------------------
uint32_t rtp_packet::write_packet(void* data, uint32_t size) const
{
	if ((data == nullptr) || (size == 0))
		return 0;

	if (size < FIXED_RTP_HEADER_LENGTH)
		return 0;

	uint8_t* pdata = (uint8_t*)data;

	//-------------------------

	uint8_t byte = (m_version << 6);

	if (m_padding_count != 0)
		byte |= 0x20; // 0010 0000

	if (m_externsion_flag)
	{
		byte |= 0x10; // 0001 0000
	}

	uint8_t tmp = m_csrc_count & 0x0F;
	byte |= tmp;

	*pdata = byte;

	++pdata;
	--size;

	//-------------------------

	byte = 0;

	if (m_marker)
		byte |= 0x80; // 1000 0000

	tmp = m_payload_type & 0x7F; // 0111 1111
	byte |= tmp;

	*pdata = byte;

	++pdata;
	--size;

	//-------------------------

	uint16_t word = htons(m_sequence_number);

	*((uint16_t*)pdata) = word;

	pdata += sizeof(uint16_t);
	size -= sizeof(uint16_t);

	//-------------------------

	uint32_t dword = htonl(m_timestamp);

	*((uint32_t*)pdata) = dword;

	pdata += sizeof(uint32_t);
	size -= sizeof(uint32_t);

	//-------------------------

	dword = htonl(m_ssrc);

	*((uint32_t*)pdata) = dword;

	pdata += sizeof(uint32_t);
	size -= sizeof(uint32_t);

	//-------------------------

	if (m_csrc_count != 0)
	{
		for (uint8_t i = 0; i < m_csrc_count; ++i)
		{
			if (size < sizeof(uint32_t))
				return 0;

			dword = m_csrc_list[i];
			dword = htonl(dword);

			*((uint32_t*)pdata) = dword;

			pdata += sizeof(uint32_t);
			size -= sizeof(uint32_t);
		}
	}

	//-------------------------

	if (m_extension_length != 0)
	{
		if (m_extension_length > size)
			return 0;

		memcpy(pdata, m_extension_buff, m_extension_length);

		pdata += m_extension_length;
		size -= m_extension_length;
	}

	//-------------------------

	if (m_payload_length != 0)
	{
		if (m_payload_length > size)
			return 0;

		memcpy(pdata, m_payload, m_payload_length);

		pdata += m_payload_length;
		size -= m_payload_length;
	}

	//-------------------------

	if (m_padding_count != 0)
	{
		if (m_padding_count > size)
			return 0;

		for (uint8_t i = 0; i < m_padding_count - 1; ++i)
		{
			*pdata = 0;
			++pdata;
		}

		*pdata = m_padding_count;
		++pdata;
		size -= m_padding_count;
	}

	//-------------------------

	uint32_t written = uint32_t(pdata - (uint8_t*)data);
	return written;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_packet::reset()
{
	m_version = 2;
    //m_padding_flag = false;
    m_externsion_flag = false;
    m_csrc_count = 0;
    m_marker = false;
    m_payload_type = 0;
    m_sequence_number = 0;
    m_timestamp = 0;
    m_ssrc = 0;
	m_payload_length = 0;
	m_padding_count = 0;

	m_extension_length = 0;

	m_samples = 160;

	m_priv_type = rtp_packet_priv_type::simple;
	m_priv_partial_count = 0;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtp_packet::copy_from(const rtp_packet* packet)
{
	if (copy_head_from(packet))
	{
		m_payload_length = packet->get_payload_length();
		if (m_payload_length != 0)
		{
			memcpy(m_payload, packet->get_payload(), m_payload_length);
		}

		m_samples = packet->get_samples();
		return true;
	}
	
	return false;
}
//------------------------------------------------------------------------------
bool rtp_packet::copy_head_from(const rtp_packet* packet)
{
	if (packet == nullptr)
	{
		return false;
	}

	m_version = packet->get_version();
	//m_padding_flag = false;
	m_externsion_flag = packet->get_extension_flag();
	m_csrc_count = packet->get_csrc_count();
	m_marker = packet->get_marker();
	m_payload_type = packet->get_payload_type();
	m_sequence_number = packet->get_sequence_number();
	m_timestamp = packet->getTimestamp();
	m_ssrc = packet->get_ssrc();
	m_padding_count = packet->get_padding_count();

	for (int i = 0; i < m_csrc_count; i++)
	{
		m_csrc_list[i] = packet->get_csrc_at(i);
	}

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_packet::set_csrc_at(uint8_t index, uint32_t value)
{
	if (index < 15)
		m_csrc_list[index] = value;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t rtp_packet::set_payload(const void* data, uint32_t length)
{
	if ((data == nullptr) || (length == 0))
		return 0;

	if (length > PAYLOAD_BUFFER_SIZE)
		length = PAYLOAD_BUFFER_SIZE;

	memcpy(m_payload, data, length);
	m_payload_length = length;

	return m_payload_length;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t rtp_packet::append_payload(const void* data, uint32_t length)
{
	if ((data == nullptr) || (length == 0))
		return 0;

	if (m_payload_length + length > PAYLOAD_BUFFER_SIZE)
		length = PAYLOAD_BUFFER_SIZE - m_payload_length;

	if (length <= 0)
		return 0;

	memcpy(m_payload + m_payload_length, data, length);
	m_payload_length += length;

	return m_payload_length;
}
uint32_t rtp_packet::update_payload(int offset, const void* data, uint32_t length)
{
	if ((data == nullptr) || (length == 0))
		return 0;

	if (offset + length > PAYLOAD_BUFFER_SIZE)
		length = PAYLOAD_BUFFER_SIZE - offset;

	if (offset <= 0)
		return 0;

	memcpy(m_payload + offset, data, length);
	
	if (offset + length > m_payload_length)
		m_payload_length = offset + length;

	return m_payload_length;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t rtp_packet::get_full_length() const
{
	uint32_t full_length = 0;
	//-------------------------

	full_length++;

	//-------------------------

	full_length++;

	//-------------------------

	full_length += sizeof(uint16_t);

	//-------------------------

	full_length += sizeof(uint32_t);

	//-------------------------

	full_length += sizeof(uint32_t);

	//-------------------------

	if (m_csrc_count != 0)
	{
		for (uint8_t i = 0; i < m_csrc_count; ++i)
		{
			full_length += sizeof(uint32_t);
		}
	}

	//-------------------------

	if (m_extension_length != 0)
	{
		full_length += m_extension_length;
	}

	//-------------------------

	if (m_payload_length != 0)
	{
		full_length += m_payload_length;
	}

	//-------------------------

	if (m_padding_count != 0)
	{
		for (uint8_t i = 0; i < m_padding_count - 1; ++i)
		{
			full_length++;
		}

		full_length++;
	}

	//-------------------------

	return full_length;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtp_packet_priv_type rtp_packet::get_priv_type() const
{
	return m_priv_type;
}
void rtp_packet::set_priv_type(rtp_packet_priv_type type)
{
	m_priv_type = type;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint8_t rtp_packet::get_priv_partial_count() const
{
	return m_priv_partial_count;
}
void rtp_packet::set_priv_partial_count(uint8_t count)
{
	m_priv_partial_count = count;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
