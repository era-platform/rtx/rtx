﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//--------------------------------------
// based on libsrtp written by
// David A. McGrew
// Cisco Systems, Inc.
//--------------------------------------
#pragma once

#include "srtp_aes.h"
#include "srtp_cipher.h"
//--------------------------------------
//
//--------------------------------------
class aes_icm_ctx_t : public cipher_t
{
	int m_bytes_in_buffer;				// number of unused bytes in buffer
	v128_t m_counter;					// holds the counter value
	v128_t m_offset;					// initial offset value
	v128_t m_keystream_buffer;			// buffers bytes of keystream
	aes_expanded_key_t m_expanded_key;	// the cipher key

public:
	aes_icm_ctx_t(int key_len);
	virtual ~aes_icm_ctx_t();

	virtual bool create(const uint8_t *key, cipher_direction_t dir = direction_any);
	virtual bool set_iv(void *iv);
	virtual bool encrypt(uint8_t *buffer, uint32_t *octets_to_encrypt);
	virtual bool decrypt(uint8_t *buffer, uint32_t *octets_to_decrypt);

private:
	void advance();
};
//--------------------------------------
