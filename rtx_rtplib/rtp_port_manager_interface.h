﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "std_mutex.h"
#include "std_timer.h"
#include "rtp_port_reader_manager.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class rtp_port_manager_interface_t
{
	rtp_port_manager_interface_t(const rtp_port_manager_interface_t&);
	rtp_port_manager_interface_t&	operator=(const rtp_port_manager_interface_t&);

public:
	rtp_port_manager_interface_t(rtl::Logger* log);
	~rtp_port_manager_interface_t();

	bool create(const char* iface, const char* address_key);
	void destroy();

	void set_port_range(uint16_t begin, uint32_t count);

	rtp_channel_t* get_channel(rtp_channel_type_t type);
	void release_channel(rtp_channel_t* channel);
	bool start_channel_listen(rtp_channel_t* channel);
	void stop_channel_listen(rtp_channel_t* channel);

	const ip_address_t* get_interface() const;
	const char* get_address_key() const;

	bool active();
	void activate(bool activate);

private:
	uint16_t port_alloc();
	uint16_t even_port_alloc();
	void port_free(uint16_t port);
	void port_ban(uint16_t port);

	rtp_channel_t* create_channel(rtp_channel_type_t type);
	bool bind_channel(rtp_channel_t* channel);
	bool delete_channel(rtp_channel_t* channel);

	// проверка портов при дестрое объекта... 
	// если какие то порты остались занятые - ругаемся.
	void ports_check();

	// <timer_event_handler_t>
	static void timer_cleanup_purgatory(rtl::Timer* sender, uint32_t elapsed, void* userData, int tid);
	static void	 async_cleanup_purgatory(void* userData, uint16_t callId, uintptr_t intParam, void* ptrParam);
	void cleanup_purgatory();

private:
	uint16_t bind_channel_data_port(rtp_channel_t* channel);
	uint16_t bind_channel_ctrl_port(rtp_channel_t* channel);
	uint16_t bind_channel_data_port(rtp_channel_t* channel, uint16_t port);
	uint16_t bind_channel_ctrl_port(rtp_channel_t* channel, uint16_t port);

	uint16_t bind_channel_video_port(rtp_channel_t* channel);
	uint16_t bind_channel_ctrl_video_port(rtp_channel_t* channel);
	uint16_t bind_channel_video_port(rtp_channel_t* channel, uint16_t port);
	uint16_t bind_channel_ctrl_video_port(rtp_channel_t* channel, uint16_t port);

private:
	rtl::Logger* m_log;
	rtl::MutexWatch m_mutex;
	rtl::String m_address_key;

	volatile bool m_active;

	mg_reader_manager_t m_reader_manager;

	ip_address_t m_interface;					// интерфейс на который будут садится каналы
	rtl::ArrayT<rtp_channel_t*> m_purgatory;		// кладбище RTP каналов! каналы удаляются!
	 
	uint16_t m_port_begin;
	uint32_t m_port_last;						// номер следующего свободного порта в пуле
	uint16_t m_port_end;
	rtl::ArrayT<uint32_t> m_port_range;				// LOWORD port HIWORD state
												//   0x80000000 занят,
												//   0x40000000 недоступен,
												//   0x0xxx0000 - количество неудачных попыток подряд.
												//   После достижения значения 8 - порт считается недоступным
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
typedef rtl::ArrayT<rtp_port_manager_interface_t*> mg_interface_list;
//------------------------------------------------------------------------------
