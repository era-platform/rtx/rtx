/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#pragma pack(push, 1)
//--------------------------------------
//
//--------------------------------------
#include "rtx_rtplib.h"
//--------------------------------------
// rtcp packet types defined in 3550, 4585, 4104
//--------------------------------------
enum rtcp_type_t
{
	rtcp_type_sr_e = 200,
	rtcp_type_rr_e = 201,
	rtcp_type_sdes_e = 202,
	rtcp_type_bye_e = 203,
	rtcp_type_app_e = 204,
	rtcp_type_rtpfb_e = 205,
	rtcp_type_psfb_e = 206,
	rtcp_type_xr_e = 207,
	rtcp_type_avb_e = 208,
	rtcp_type_last_e = 211,
	rtcp_type_invalid_e = 0
};
const char* get_rtcp_type_name(rtcp_type_t type);
//--------------------------------------
//
//--------------------------------------
enum rtcp_sdes_type_t
{
	rtcp_sdes_end_e = 0,
	rtcp_sdes_cname_e = 1,
	rtcp_sdes_name_e = 2,
	rtcp_sdes_email_e = 3,
	rtcp_sdes_phone_e = 4,
	rtcp_sdes_loc_e = 5,
	rtcp_sdes_tool_e = 6,
	rtcp_sdes_note_e = 7,
	rtcp_sdes_priv_e = 8
};
const char* get_rtcp_sdes_type_name(rtcp_sdes_type_t type);
//--------------------------------------
// RTCP common header word
//--------------------------------------
//
//         0                   1                   2                   3
//         0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
//        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// header |V=2|P|    RC   |   PT=SR=200   |             Length            |
//        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//        |                         SSRC of sender                        |
//        +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
// sender |              NTP timestamp, most significant word             |
// info   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//        |             NTP timestamp, least significant word             |
//        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//        |                         RTP timestamp                         |
//        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//        |                     sender's packet count                     |
//        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//        |                      sender's octet count                     |
//        +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
// report |                 SSRC_1 (SSRC of first source)                 |
// block  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   1    | fraction lost |       cumulative number of packets lost       |
//        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//        |           extended highest sequence number received           |
//        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//        |                      interarrival jitter                      |
//        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//        |                         last SR (LSR)                         |
//        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//        |                   delay since last SR (DLSR)                  |
//        +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
// report |                 SSRC_2 (SSRC of second source)                |
// block  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   2    :                               ...                             :
//        +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
//        |                  profile-specific extensions                  |
//        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//
//--------------------------------------

//--------------------------------------
// common report block
//--------------------------------------
/*
report |                 SSRC_1 (SSRC of first source)                 |
block  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  1    | fraction lost |       cumulative number of packets lost       |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |           extended highest sequence number received           |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                      interarrival jitter                      |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                         last SR (LSR)                         |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                   delay since last SR (DLSR)                  |


report |                 SSRC_1 (SSRC of first source)                 |
block  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  1    | fraction lost |       cumulative number of packets lost       |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |           extended highest sequence number received           |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                      interarrival jitter                      |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                         last SR (LSR)                         |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                   delay since last SR (DLSR)                  |
*/
struct rtcp_report_t
{
	uint32_t		ssrc;
	uint8_t			fraction_lost;
	uint8_t			packets_lost[3];
	uint16_t		ext_seq, last_seq;
	uint32_t		jitter;
	uint32_t		lsr;
	uint32_t		dlsr;
};
#define RTCP_REPORT_SIZE	24	// sizeof(rtcp_report_t)
#define RTCP_REPORT_SIZE_4	6   // (RTCP_REPORT_SIZE/sizeof(uint32_t))
//--------------------------------------
// Reception report block (RR)
//--------------------------------------
/*
6.4.2 RR: Receiver Report RTCP Packet

       0                   1                   2                   3
	   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
header |V=2|P|    RC   |   PT=RR=201   |             length            |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                     SSRC of packet sender                     |
	   +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
report |                 SSRC_1 (SSRC of first source)                 |
block  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  1    | fraction lost |       cumulative number of packets lost       |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |           extended highest sequence number received           |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                      interarrival jitter                      |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                         last SR (LSR)                         |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                   delay since last SR (DLSR)                  |
	   +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
report |                 SSRC_2 (SSRC of second source)                |
block  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  2    :                               ...                             :
       +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
	   |                  profile-specific extensions                  |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

*/
struct rtcp_receiver_t
{
	rtcp_report_t	report[1];		/* variable-length list */
};
#define RTCP_RECEIVER_SIZE		0	// (sizeof(rtcp_receiver_report_t)-sizeof(rtcp_report_t))
#define RTCP_RECEIVER_SIZE_4	0	// (RTCP_RECEIVER_SIZE/sizeof(uint32_t))
//--------------------------------------
// sender report (SR)
//--------------------------------------
/*
6.4.1 SR: Sender Report RTCP Packet

       0                   1                   2                   3
	   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
header |V=2|P|    RC   |   PT=SR=200   |             length            |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                         SSRC of sender                        |
	   +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
sender |              NTP timestamp, most significant word             |
info   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |             NTP timestamp, least significant word             |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                         RTP timestamp                         |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                     sender's packet count                     |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                      sender's octet count                     |
	   +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
report |                 SSRC_1 (SSRC of first source)                 |
block  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  1    | fraction lost |       cumulative number of packets lost       |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |           extended highest sequence number received           |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                      interarrival jitter                      |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                         last SR (LSR)                         |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                   delay since last SR (DLSR)                  |
	   +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
report |                 SSRC_2 (SSRC of second source)                |
block  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  2    :                               ...                             :
       +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
	   |                  profile-specific extensions                  |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/
struct rtcp_sender_t
{
	uint32_t		ntp_timestamp;		/* ntp timestamp */
	uint32_t		ntp_fraction;
	uint32_t		rtp_timestamp;		/* rtp timestamp */
	uint32_t		packets_sent;		/* packets sent */
	uint32_t		octets_sent;		/* octets sent */
	rtcp_report_t	report[1];			/* variable-length list */
};
#define RTCP_SENDER_SIZE	20	//(sizeof(rtcp_sender_report_t)-sizeof(rtcp_report_t))
#define RTCP_SENDER_SIZE_4	5	//(RTCP_SENDER_SIZE/sizeof(uint32_t))
//--------------------------------------
// SDES item
//--------------------------------------
/*
6.5 SDES: Source Description RTCP Packet

	   0                   1                   2                   3
	   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
header |V=2|P|    SC   |  PT=SDES=202  |             length            |
	   +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
chunk  |                          SSRC/CSRC_1                          |
  1    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                           SDES items                          |
	   |                              ...                              |
	   +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
chunk  |                          SSRC/CSRC_2                          |
  2    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                           SDES items                          |
	   |                              ...                              |
	   +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
*/
struct rtcp_sdes_item_t
{
	uint8_t			type;			/* type of item (rtcp_sdes_type_t) */
	uint8_t			length;		/* length of item (in octets) */
	char			data[1];		/* text, not null-terminated */
};
#define RTCP_SDES_ITEM_SIZE		2 // (sizeof(rtcp_sdes_item_t)-sizeof(char))
//#define RTCP_SDES_ITEM_SIZE_4	(RTCP_SDES_ITEM_SIZE/sizeof(uint32_t))
//--------------------------------------
// source description (SDES)
//--------------------------------------
struct rtcp_sdes_chunk_t
{
	uint32_t			ssrc;		/* first ssrc/csrc */
	rtcp_sdes_item_t	items[1];	/* list of sdes items */
};
#define RTCP_SDES_SIZE		4 // (sizeof(rtcp_sdes_chunk_t)-sizeof(rtcp_sdes_item_t))
#define RTCP_SDES_SIZE_4	1 // (RTCP_SDES_SIZE/sizeof(uint32_t))

//--------------------------------------
//
//--------------------------------------
/*
6.6 BYE: Goodbye RTCP Packet

       0                   1                   2                   3
	   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |V=2|P|    SC   |   PT=BYE=203  |             length            |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                           SSRC/CSRC                           |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   :                              ...                              :
	   +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
 (opt) |     length    |               reason for leaving            ...
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/
struct rtcp_bye_t
{
	uint32_t sources[1];   /* list of sources */ /* can't express trailing text for reason */
};
#define RTCP_BYE_SIZE		4 // sizeof(rtcp_bye_pack_t)
#define RTCP_BYE_SIZE_4		1 // (RTCP_BYE_SIZE/sizeof(uint32_t))
//--------------------------------------
// Feedback
//--------------------------------------
/*
6.1.   Common Packet Format for Feedback Messages

All FB messages MUST use a common packet format that is depicted in
Figure 3:

0                   1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|V=2|P|   FMT   |       PT      |          length               |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                  SSRC of packet sender                        |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                  SSRC of media source                         |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
:            Feedback Control Information (FCI)                 :
:                                                               :

Figure 3: Common Packet Format for Feedback Messages
*/
/*
0:    unassigned
1:    Generic NACK
2-30: unassigned
31:   reserved for future expansion of the identifier number space
*/
enum rtcp_rtpfb_format_t
{
	rtcp_rtpfb_unassigned_e = 0,	// 0:    unassigned
	rtcp_rtpfb_nack_e = 1,			// 1:    Generic NACK
	// 2:    reserved
	rtcp_rtpfb_tmmbr_e = 3,			// 3:    Temporary Maximum Media Stream Bit Rate Request (TMMBR)
	rtcp_rtpfb_tmmbn_e = 4,			// 4:    Temporary Maximum Media Stream Bit Rate Notification (TMMBN)
	rtcp_rtpfb_transport_cc_e = 15,	// 15:	 Transport-wide RTCP Feedback Message
	// 5-30: unassigned
	rtcp_rtpfb_reserved_e = 31		// 31:   reserved for future expansion of the identifier number space
};
const char* get_rtcp_rtpfb_type_name(rtcp_rtpfb_format_t type);
//--------------------------------------
//
//--------------------------------------
/*
0                   1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|            PID                |             BLP               |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

Figure 4: Syntax for the Generic NACK message
*/
struct rtcp_rtpfb_nack_t
{
	uint16_t pid;
	uint16_t blp;
};
/*
0                   1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                              SSRC                             |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
| MxTBR Exp |  MxTBR Mantissa                 |Measured Overhead|
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

Figure 2 - Syntax of an FCI Entry in the TMMBR Message

0                   1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                              SSRC                             |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
| MxTBR Exp |  MxTBR Mantissa                 |Measured Overhead|
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

Figure 3 - Syntax of an FCI Entry in the TMMBN Message
*/
struct rtcp_rtpfb_tmm_t
{
	uint32_t ssrc;
	uint32_t overhead : 9, mantissa : 17, exp : 6;
};

/*
0                   1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|V=2|P|  FMT=15 |    PT=205     |           length              |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                     SSRC of packet sender                     |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                      SSRC of media source                     |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|      base sequence number     |      packet status count      |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                 reference time                | fb pkt. count |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|          packet chunk         |         packet chunk          |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
.                                                               .
.                                                               .
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|         packet chunk          |  recv delta   |  recv delta   |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
.                                                               .
.                                                               .
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|           recv delta          |  recv delta   | zero padding  |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/
struct rtcp_rtpfb_transport_cc_t
{
	uint16_t bsn;
	uint16_t psc;
	uint32_t fbpktc : 8, rt : 24;
};
struct rtcp_rtpfb_transport_cc_rl_t
{
	uint16_t l : 13, s : 2, t : 1;
};
struct rtcp_rtpfb_transport_cc_stat_t
{
	uint16_t b : 14, s : 1, t : 1;
};


/*
0                   1
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|T| S |       Run Length        |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/
struct rtcp_rtpfb_rlch_t
{
	uint16_t t : 1, s : 2, l: 13;
};
/*
0                   1
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|T|S|       symbol list         |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/
struct rtcp_rtpfb_vch_t
{
	uint16_t t : 1, s : 1, l : 14;
};

//--------------------------------------
//
//--------------------------------------
/*
0:     unassigned
1:     Picture Loss Indication (PLI)
2:     Slice Loss Indication (SLI)
3:     Reference Picture Selection Indication (RPSI)
4-14:  unassigned
15:    Application layer FB (AFB) message
16-30: unassigned
31:    reserved for future expansion of the sequence number space
*/
enum rtcp_psfb_format_t
{
	rtcp_psfb_unassigned_e = 0,		// 0:   [RFC4585]  unassigned
	rtcp_psfb_pli_e = 1,			// 1:   [RFC4585]  Picture Loss Indication (PLI)
	rtcp_psfb_sli_e = 2,			// 2:   [RFC4585]  Slice Loss Indication (SLI)
	rtcp_psfb_rpsi_e = 3,			// 3:   [RFC4585]  Reference Picture Selection Indication (RPSI)
	rtcp_psfb_fir_e = 4,			// 4:   [RFC5104]  Full Intra Request (FIR) Command
	rtcp_psfb_tstr_e = 5,			// 5:   [RFC5104]  Temporal-Spatial Trade-off Request (TSTR)
	rtcp_psfb_tstn_e = 6,				// 6:   [RFC5104]  Temporal-Spatial Trade-off Notification (TSTN)
	rtcp_psfb_vbcm_e = 7,				// 7:   [RFC5104]  Video Back Channel Message (VBCM)
	// 8-14:[RFC4585]  unassigned
	rtcp_psfb_app_e = 15,			// 15:  [RFC4585]  Application layer FB (AFB) message
	// 16-30: [RFC4585]unassigned
	rtcp_psfb_reserved_e = 31			//  31: [RFC4585]  reserved for future expansion of the sequence number space
};
const char* get_rtcp_psfb_type_name(rtcp_psfb_format_t type);
//--------------------------------------
//
//--------------------------------------
/*
0                   1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|            First        |        Number           | PictureID |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

Figure 6: Syntax of the Slice Loss Indication (SLI)
*/
struct rtcp_psfb_sli_t
{
	uint32_t pid : 6, number : 13, first : 13;
};
#define RTCP_PSFB_SLI_SIZE		4
#define RTCP_PSFB_SLI_SIZE_4	1
//--------------------------------------
//
//--------------------------------------
/*
0                   1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|      PB       |0| Payload Type|    Native RPSI bit string     |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|   defined per codec          ...                | Padding (0) |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

Figure 7: Syntax of the Reference Picture Selection Indication (RPSI)
*/
struct rtcp_psfb_rpsi_t
{
	uint8_t pb; //?
	uint8_t pt : 7, zero : 1;
	uint8_t native_bits[1];
};
/*
0                   1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                              SSRC                             |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
| Seq nr.       |    Reserved                                   |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

Figure 4 - Syntax of an FCI Entry in the FIR Message
*/
struct rtcp_psfb_fir_t
{
	uint32_t ssrc;
	uint8_t seqno;
	uint8_t reserverd[3];
};
/*
0                   1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                              SSRC                             |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|  Seq nr.      |  Reserved                           | Index   |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

Figure 5 - Syntax of an FCI Entry in the TSTR Message

0                   1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                              SSRC                             |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|  Seq nr.      |  Reserved                           | Index   |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

Figure 6 - Syntax of the TSTN
*/
struct rtcp_psfb_tst_t
{
	uint32_t ssrc;
	uint32_t index : 5, reserved : 19, seqno : 8;
};
/*
0                   1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                              SSRC                             |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
| Seq nr.       |0| Payload Type| Length                        |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                    VBCM Octet String....      |    Padding    |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

Figure 7 - Syntax of an FCI Entry in the VBCM
*/
struct rtcp_psfb_vbcm_t
{
	uint32_t ssrc;
	uint8_t seqno;
	uint8_t payload : 7, zero : 1;
	uint16_t length;
	char data[1];
};

// 0                   1                   2                   3               
// 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
//+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//|  Unique identifier 'R' 'E' 'M' 'B'                            |
//+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//|  Num SSRC     | BR Exp    |  BR Mantissa                      |
//+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//|   SSRC feedback                                               |
//+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//|  ...                                                          |
//#define REMB 0x52454D42// REMB
#define REMB 0x424D4552// REMB
struct rtcp_psfb_app_remb_t
{
	uint32_t br_mantissa : 18, br_exp : 6, num_ssrc : 8;
	uint32_t ssrc_fb[4];
};

struct rtcp_psfb_app_t
{
	uint32_t ID;

	union app_entry_t
	{
		rtcp_psfb_app_remb_t remb;
	};

	app_entry_t app;
};
//--------------------------------------
//
//--------------------------------------
struct rtcp_feedback_t
{
	uint32_t media_source;
	union
	{
		rtcp_rtpfb_nack_t nack[1];
		rtcp_rtpfb_tmm_t tmm[1];
		rtcp_psfb_sli_t sli[1];
		rtcp_psfb_rpsi_t rpsi[1];
		rtcp_psfb_fir_t fir[1];
		rtcp_psfb_tst_t tst[1];
		rtcp_psfb_vbcm_t vbcm[1];
	};
};
// no length defined
//--------------------------------------
//
//--------------------------------------
union rtcp_body_t
{
	rtcp_sender_t		sender;			/* sender report (sr) */
	rtcp_receiver_t		receiver;		/* reception report (rr) */
	rtcp_sdes_chunk_t	sdes;			/* source description (sdes) */
	rtcp_bye_t			bye;			/* bye */
	rtcp_feedback_t		feedback;		/* RTP feed back (rtpfb, psfb, etc) */
	uint8_t				data[1];		/* app data */
};
//--------------------------------------
//
//--------------------------------------
struct rtcp_t
{
	rtcp_header_t		header;
	rtcp_body_t			body;
};
//--------------------------------------
// Per-source state information
//--------------------------------------
struct rtp_source_t
{
	uint16_t	max_seq;        /* highest seq. number seen */
	uint32_t	cycles;         /* shifted count of seq. number cycles */
	uint32_t	base_seq;       /* base seq number */
	uint32_t	bad_seq;        /* last 'bad' seq number + 1 */
	uint32_t	probation;      /* sequ. packets till source is valid */
	uint32_t	received;       /* packets received */
	uint32_t	expected_prior; /* packet expected at last interval */
	uint32_t	received_prior; /* packet received at last interval */
	uint32_t	transit;        /* relative trans time for prev pkt */
	uint32_t	jitter;         /* estimated jitter */
	/* ... */
};
#pragma pack(pop)
//--------------------------------------
//
//--------------------------------------
#define RTCP_MAX_PACKET_LEN	2048
//--------------------------------------
// ������� ����������
//--------------------------------------
class rtcp_base_packet_t
{
protected:
	int	m_version;
	int m_padding;
	int m_counter;
	rtcp_type_t m_type;
	uint16_t m_length;
	uint32_t m_ssrc;

public:
	rtcp_base_packet_t(rtcp_type_t type);
	virtual ~rtcp_base_packet_t();

	/// serialization
	virtual int read(const uint8_t* in, int length);
	virtual int write(uint8_t* out, int size);
	virtual int dump(char* out, int size) const;
	/// getters
	int get_version() const { return m_version; }
	int get_padding() const { return m_padding; }
	int get_counter() const { return m_type; }
	rtcp_type_t getType() const { return m_type; }
	int getLength() const { return m_length; }
	uint32_t get_ssrc() const { return m_ssrc; }

	/// setters
	void set_ssrc(uint32_t ssrc) { m_ssrc = ssrc; }
};
//--------------------------------------
// receiver report
//--------------------------------------
class rtcp_rr_packet_t : public rtcp_base_packet_t
{
protected:
	rtl::ArrayT<rtcp_report_t> m_report_list;

	/// ����������� ��� ����������� �������
	rtcp_rr_packet_t(rtcp_type_t type): rtcp_base_packet_t(type), m_report_list(32) { }

public:
	rtcp_rr_packet_t() : rtcp_base_packet_t(rtcp_type_rr_e), m_report_list(32) { }
	virtual ~rtcp_rr_packet_t();

	/// serialization
	virtual int read(const uint8_t* in, int length);
	virtual int write(uint8_t* out, int size);
	virtual int dump(char* out, int size) const;

	/// getters
	int get_report_count() const { return m_report_list.getCount(); }
	const rtcp_report_t& getAt(int index) const { return m_report_list[index]; }

	/// setter
	bool add_report(const rtcp_report_t& report);
};
//--------------------------------------
// sender report
//--------------------------------------
class rtcp_sr_packet_t : public rtcp_rr_packet_t
{
	uint32_t m_ntp_timestamp;	/* ntp timestamp */
	uint32_t m_ntp_fraction;
	uint32_t m_rtp_timestamp;	/* rtp timestamp */
	uint32_t m_packets_sent;	/* packets sent */
	uint32_t m_octets_sent;		/* octets sent */

public:
	rtcp_sr_packet_t();
	virtual ~rtcp_sr_packet_t();

	virtual int read(const uint8_t* in, int length);
	virtual int write(uint8_t* out, int size);
	virtual int dump(char* out, int size) const;

	// getters
	uint32_t get_ntp_timestamp() const { return m_ntp_timestamp; }
	uint32_t get_ntp_fraction() const { return m_ntp_fraction; }
	uint32_t get_rtp_timestamp() const { return m_rtp_timestamp; }
	uint32_t get_packets_sent() const { return m_packets_sent; }
	uint32_t get_octets_sent() const { return m_octets_sent; }

	// setters
	void set_ntp_timestamp(uint32_t value) { m_ntp_timestamp = value; }
	void set_ntp_fraction(uint32_t value) { m_ntp_fraction = value; }
	void set_rtp_timestamp(uint32_t value) { m_rtp_timestamp = value; }
	void set_packets_sent(uint32_t value) { m_packets_sent = value; }
	void set_octets_sent(uint32_t value) { m_octets_sent = value; }
};
//--------------------------------------
//
//--------------------------------------
class rtcp_chunk_t
{
	uint32_t m_ssrc;
	rtl::ArrayT<rtcp_sdes_item_t*> m_items;

public:
	rtcp_chunk_t() : m_ssrc(0) { }
	~rtcp_chunk_t() { cleanup(); }

	uint32_t get_ssrc() { return m_ssrc; }
	void set_ssrc(uint32_t ssrc) { m_ssrc = ssrc; }

	int getCount() { return m_items.getCount(); }
	rtcp_sdes_item_t* get(int index) { return m_items.getAt(index); }

	int read(const uint8_t* in, int length);
	int write(uint8_t* out, int size);

	bool add(rtcp_sdes_type_t type, const void* data, int length);
	bool add(const rtcp_sdes_item_t* item) { return add((rtcp_sdes_type_t)item->type, item->data, item->length); }
	bool set(rtcp_sdes_type_t type, int index, const void* data, int length);
	bool set(const rtcp_sdes_item_t* item, int index) { return set((rtcp_sdes_type_t)item->type, index, item->data, item->length); }
	bool remove(rtcp_sdes_type_t type, int index);
	void cleanup();
	int calculate_length();
};
//--------------------------------------
//
//--------------------------------------
class rtcp_sdes_packet_t : public rtcp_base_packet_t
{
	rtl::ArrayT<rtcp_chunk_t*> m_chunk_list;

	void cleanup();

public:
	rtcp_sdes_packet_t() : rtcp_base_packet_t(rtcp_type_sdes_e) { }
	virtual ~rtcp_sdes_packet_t();

	virtual int read(const uint8_t* in, int length);
	virtual int write(uint8_t* out, int size);
	virtual int dump(char* out, int size) const;

	// getters
	int get_chank_count() const { return m_chunk_list.getCount(); }
	uint32_t get_chank_ssrc(int index) const { return index >= 0 && index < m_chunk_list.getCount() ? m_chunk_list[index]->get_ssrc() : 0; }

	int get_chunk_index(uint32_t chunk_ssrc) const;
	int get_chunk_item_count(int chunk_idx) const;
	const rtcp_sdes_item_t* get_chunk_item(int chunk_idx, rtcp_sdes_type_t type, int item_idx = 0) const;
	const rtcp_sdes_item_t* get_chunk_item_at(int chunk_idx, int item_idx = 0) const;
	
	// setters
	int add_chunk(uint32_t ssrc);
	bool remove_chunk(int index);

	int add_chunk_item(int chunk_idx, rtcp_sdes_type_t type, const char* data, int length);
	bool set_chunk_item(int chunk_idx, rtcp_sdes_type_t type, int index, const char* data, int length);
	bool remove_chunk_item(int chunk_idx, rtcp_sdes_type_t type, int index);
};
//--------------------------------------
//
//--------------------------------------
class rtcp_bye_packet_t : public rtcp_base_packet_t
{
	rtl::ArrayT<uint32_t> m_bye_list;
	rtl::String m_reason;

	void cleanup() { m_bye_list.clear(); m_reason = rtl::String::empty; }

public:
	rtcp_bye_packet_t() : rtcp_base_packet_t(rtcp_type_bye_e) { }
	virtual ~rtcp_bye_packet_t();

	virtual int read(const uint8_t* in, int length);
	virtual int write(uint8_t* out, int size);
	virtual int dump(char* out, int size) const;

	bool add_source(uint32_t ssrc);
	int get_source_count() const { return m_bye_list.getCount() + 1; }
	uint32_t get_source(int index) const { return index == 0 ? m_ssrc : m_bye_list.getAt(index-1); }

	const rtl::String& get_reason() const { return m_reason; }
	void set_reason(const rtl::String& reason) { m_reason = reason; }
};
//--------------------------------------
//
//--------------------------------------
class rtcp_app_packet_t : public rtcp_base_packet_t
{
	uint32_t m_sign;
	int m_data_length;
	void* m_data;

public:
	rtcp_app_packet_t();
	virtual ~rtcp_app_packet_t();

	virtual int read(const uint8_t* in, int length);
	virtual int write(uint8_t* out, int size);
	virtual int dump(char* out, int size) const;

	void set_signature(uint32_t sign) { m_sign = sign; }
	uint32_t get_signature() const { return m_sign; }

	void set_data(const void* data, int length);
	
	int get_data_length() const { return m_data_length; };
	const void* get_data() const { return m_data; };
};
//--------------------------------------
//
//--------------------------------------
class rtcp_rtpfb_packet_t : public rtcp_base_packet_t
{
	union rtpfb_entry_t
	{
		rtcp_rtpfb_nack_t nack;
		rtcp_rtpfb_tmm_t tmm;
		rtcp_rtpfb_transport_cc_t cc;
	};

	uint32_t m_media_source;
	rtl::ArrayT<rtpfb_entry_t> m_entry_list;

	uint8_t m_buff_transport_cc[64];
	uint32_t m_buff_transport_cc_size = 0;

	int read_nack(const uint8_t* in, int length);
	int read_tmm(const uint8_t* in, int length);
	int read_transport_cc(const uint8_t* in, int length);

	int write_nack(uint8_t* out, int size);
	int write_tmm(uint8_t* out, int size);
	int write_transport_cc(uint8_t* out, int size);

public:
	rtcp_rtpfb_packet_t();
	virtual ~rtcp_rtpfb_packet_t();

	virtual int read(const uint8_t* in, int length);
	virtual int write(uint8_t* out, int size);
	virtual int dump(char* out, int size) const;

	rtcp_rtpfb_format_t get_fb_type() const { return (rtcp_rtpfb_format_t)m_counter; }
	void set_fb_type(rtcp_rtpfb_format_t type);

	uint32_t get_media_source() { return m_media_source; }
	void set_media_source(uint32_t ssrc) { m_media_source = ssrc; }

	int get_entry_count() const { return m_entry_list.getCount(); }
	const rtcp_rtpfb_nack_t* get_nack_at(int index) const;
	const rtcp_rtpfb_tmm_t* get_tmm_at(int index) const;
	const rtcp_rtpfb_transport_cc_t* get_cc_at(int index) const;

	void add_nack(const uint16_t* seq_nums, int count);
	void add_tmm(rtcp_rtpfb_tmm_t* tmm);
};
//--------------------------------------
//
//--------------------------------------
class rtcp_psfb_packet_t : public rtcp_base_packet_t
{
	union psfb_entry_t
	{
		rtcp_psfb_sli_t sli;
		rtcp_psfb_fir_t fir;
		rtcp_psfb_tst_t tst;
		rtcp_psfb_vbcm_t* vbcm;
		rtcp_psfb_app_t app;
	};

	uint8_t m_app_buff[64];
	int m_app_length;

	uint32_t m_media_source;
	rtl::ArrayT<psfb_entry_t> m_entry_list;
	
	rtcp_psfb_rpsi_t* m_rpsi; // ����� ����� ���� ��������� � ���������. ���� ����� ���� ������ ����!
	int m_rpsi_length;

	int read_sli(const uint8_t* in, int length);
	int read_rpsi(const uint8_t* in, int length);
	int read_fir(const uint8_t* in, int length);
	int read_tst(const uint8_t* in, int length);
	int read_vbcm(const uint8_t* in, int length);
	int read_app(const uint8_t* in, int length);
	int read_remb(const uint8_t* in, int length);

	int write_sli(uint8_t* out, int size);
	int write_rpsi(uint8_t* out, int size);
	int write_fir(uint8_t* out, int size);
	int write_tst(uint8_t* out, int size);
	int write_vbcm(uint8_t* out, int size);
	int write_app(uint8_t* out, int size);
	int write_remb(rtcp_psfb_app_t* app, uint8_t* out, int size);

	int calc_vbcm_length();
	int calc_app_length();
	void cleanup();

public:
	rtcp_psfb_packet_t();
	virtual ~rtcp_psfb_packet_t();

	virtual int read(const uint8_t* in, int length);
	virtual int write(uint8_t* out, int size);
	virtual int dump(char* out, int size) const;

	rtcp_psfb_format_t get_fb_type() const { return (rtcp_psfb_format_t)m_counter; }
	void set_fb_type(rtcp_psfb_format_t type) { cleanup(); m_counter = type; }

	uint32_t get_media_source() const { return m_media_source; }
	void set_media_source(uint32_t ssrc) { m_media_source = ssrc; }

	int get_entry_count() const { return m_entry_list.getCount(); }
	const rtcp_psfb_sli_t* get_sli_at(int index) const { return (rtcp_psfb_format_t)m_counter == rtcp_psfb_sli_e && index >= 0 && index < m_entry_list.getCount() ? &m_entry_list[index].sli : nullptr; }
	const rtcp_psfb_rpsi_t* get_rpsi() const { return (rtcp_psfb_format_t)m_counter == rtcp_psfb_rpsi_e ? m_rpsi : nullptr; }
	int get_rpsi_length() { return (rtcp_psfb_format_t)m_counter == rtcp_psfb_rpsi_e ? m_rpsi_length : 0; }
	const rtcp_psfb_fir_t* get_fir_at(int index) const { return (rtcp_psfb_format_t)m_counter == rtcp_psfb_fir_e && index >= 0 && index < m_entry_list.getCount() ? &m_entry_list[index].fir : nullptr; }
	const rtcp_psfb_tst_t* get_tst_at(int index) const { return ((rtcp_psfb_format_t)m_counter == rtcp_psfb_tstr_e || (rtcp_psfb_format_t)m_counter == rtcp_psfb_tstn_e) &&
		index >= 0 && index < m_entry_list.getCount() ? &m_entry_list[index].tst : nullptr; }
	const rtcp_psfb_vbcm_t* get_vbcm_at(int index) const { return (rtcp_psfb_format_t)m_counter == rtcp_psfb_vbcm_e && index >= 0 && index < m_entry_list.getCount() ? m_entry_list[index].vbcm : nullptr; }
	const rtcp_psfb_app_t* get_app_at(int index) const { return (rtcp_psfb_format_t)m_counter == rtcp_psfb_app_e && index >= 0 && index < m_entry_list.getCount() ? &m_entry_list[index].app : nullptr; }

	void add_sli(rtcp_psfb_sli_t* sli);
	void set_rpsi(rtcp_psfb_rpsi_t* rpsi, int length);
	void add_fir(uint32_t ssrc, uint8_t seqnr);
	void add_tst(rtcp_psfb_tst_t* tst);
	void add_vbcm(rtcp_psfb_vbcm_t* vbcm);
};
//--------------------------------------
//
//--------------------------------------
class rtcp_packet_t
{
	rtl::ArrayT<rtcp_base_packet_t*> m_packet_list;

	void cleanup();
	rtcp_base_packet_t* create_packet(rtcp_type_t type);

	int m_full_length; // ������ ������ ������ (������� ������� ��������� �������)

public:
	RTX_RTPLIB_API rtcp_packet_t() { }
	RTX_RTPLIB_API ~rtcp_packet_t() { cleanup(); }

	rtcp_type_t getType() const { return m_packet_list.getCount() > 0 ? m_packet_list[0]->getType() : rtcp_type_invalid_e; }
	uint32_t get_ssrc() const { return m_packet_list.getCount() > 0 ? m_packet_list[0]->get_ssrc() : 0; }

	RTX_RTPLIB_API static bool is_rtcp_packet(const void* data, uint32_t length);

	rtcp_base_packet_t* get_lead_packet() { return m_packet_list.getAt(0); }
	const rtcp_base_packet_t* get_lead_packet() const { return m_packet_list.getAt(0); }
	int get_packet_count() const { return m_packet_list.getCount(); }
	rtcp_base_packet_t* get_packet_at(int index) { return m_packet_list.getAt(index); }
	const rtcp_base_packet_t* get_packet_at(int index) const { return m_packet_list.getAt(index); }
	rtcp_base_packet_t* get_packet(rtcp_type_t type, int index = 0);
	const rtcp_base_packet_t* get_packet(rtcp_type_t type, int index = 0) const;

	int get_full_length() const;

	bool add_packet(rtcp_base_packet_t* packet) { m_packet_list.add(packet); return true; }

	RTX_RTPLIB_API int read(const uint8_t* in, int length);
	RTX_RTPLIB_API int write(uint8_t* out, int size) const;
	RTX_RTPLIB_API bool copy(const rtcp_packet_t* packet);
	int dump(char* out, int size) const;
};
//--------------------------------------

