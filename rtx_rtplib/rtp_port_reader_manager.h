﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "rtp_channel.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class mg_listener_t;
//------------------------------------------------------------------------------
typedef rtl::ArrayT<mg_listener_t*> mg_listener_list;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
struct mg_reader_implementation
{
	virtual bool start_channel_listen(rtp_channel_t* channel) = 0;
	virtual void stop_channel_listen(rtp_channel_t* channel) = 0;
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class mg_reader_manager_t : public mg_reader_implementation
{
	mg_reader_manager_t(const mg_reader_manager_t&);
	mg_reader_manager_t& operator=(const mg_reader_manager_t&);

public:
	mg_reader_manager_t(rtl::Logger* log);
	~mg_reader_manager_t();

	bool create();
	void destroy();

	virtual bool start_channel_listen(rtp_channel_t* channel);
	virtual void stop_channel_listen(rtp_channel_t* channel);

private:
	mg_listener_t* get_free_listener();

private:
	rtl::Logger* m_log;
	mg_listener_list m_listenerList;
	uint32_t m_thread_count;
};
//------------------------------------------------------------------------------
