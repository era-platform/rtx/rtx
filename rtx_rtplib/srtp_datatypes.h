﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//--------------------------------------
// based on libsrtp written by
// David A. McGrew
// Cisco Systems, Inc.
//--------------------------------------
#pragma once

//--------------------------------------
//
//--------------------------------------
/* if DATATYPES_USE_MACROS is defined, then little functions are macros */
#define DATATYPES_USE_MACROS  

union v16_t
{
	uint8_t  v8[2];
	uint16_t value;
};

union v32_t
{
	uint8_t  v8[4];
	uint16_t v16[2];
	uint32_t value;
};

union v64_t
{
	uint8_t  v8[8];
	uint16_t v16[4];
	uint32_t v32[2];
	uint64_t value;
};

union v128_t
{
	uint8_t  v8[16];
	uint16_t v16[8];
	uint32_t v32[4];
	uint64_t v64[2];
};

/* some useful and simple math functions */

#define pow_2(X) ( (uint32_t)1 << (X) )   /* 2^X     */

#define pow_minus_one(X) ( (X) ? -1 : 1 )      /* (-1)^X  */

/*
 * octet_get_weight(x) returns the hamming weight (number of bits equal to
 * one) in the octet x
 */

int octet_get_weight(uint8_t octet);
char* octet_bit_string(uint8_t x);

#define MAX_PRINT_STRING_LEN 1024

//char* octet_string_hex_string(const void *str, int length);
char* v128_bit_string(v128_t *x);
char* v128_hex_string(v128_t *x);
uint8_t nibble_to_hex_char(uint8_t nibble);
char* char_to_hex_string(char *x, int num_char);
uint8_t hex_string_to_octet(char *s);

/*
 * hex_string_to_octet_string(raw, hex, len) converts the hexadecimal
 * string at *hex (of length len octets) to the equivalent raw data
 * and writes it to *raw.
 *
 * if a character in the hex string that is not a hexadeciaml digit
 * (0123456789abcdefABCDEF) is encountered, the function stops writing
 * data to *raw
 *
 * the number of hex digits copied (which is two times the number of
 * octets in *raw) is returned
 */

int hex_string_to_octet_string(char *raw, char *hex, int len);
v128_t hex_string_to_v128(char *s);
void v128_copy_octet_string(v128_t *x, const uint8_t s[16]);
void v128_left_shift(v128_t *x, int index);
void v128_right_shift(v128_t *x, int index);

/*
 * the following macros define the data manipulation functions
 * 
 * If DATATYPES_USE_MACROS is defined, then these macros are used
 * directly (and function call overhead is avoided).  Otherwise,
 * the macros are used through the functions defined in datatypes.c
 * (and the compiler provides better warnings).
 */

#define _v128_set_to_zero(x)    \
(                               \
  (x)->v32[0] = 0,              \
  (x)->v32[1] = 0,              \
  (x)->v32[2] = 0,              \
  (x)->v32[3] = 0               \
)

#define _v128_copy(x, y)         \
(                                \
  (x)->v32[0] = (y)->v32[0],     \
  (x)->v32[1] = (y)->v32[1],     \
  (x)->v32[2] = (y)->v32[2],     \
  (x)->v32[3] = (y)->v32[3]      \
)

#define _v128_xor(z, x, y)                      \
(                                               \
   (z)->v32[0] = (x)->v32[0] ^ (y)->v32[0],     \
   (z)->v32[1] = (x)->v32[1] ^ (y)->v32[1],     \
   (z)->v32[2] = (x)->v32[2] ^ (y)->v32[2],     \
   (z)->v32[3] = (x)->v32[3] ^ (y)->v32[3]      \
)

#define _v128_and(z, x, y)                      \
(                                               \
   (z)->v32[0] = (x)->v32[0] & (y)->v32[0],     \
   (z)->v32[1] = (x)->v32[1] & (y)->v32[1],     \
   (z)->v32[2] = (x)->v32[2] & (y)->v32[2],     \
   (z)->v32[3] = (x)->v32[3] & (y)->v32[3]      \
)

#define _v128_or(z, x, y)                       \
(                                               \
   (z)->v32[0] = (x)->v32[0] | (y)->v32[0],     \
   (z)->v32[1] = (x)->v32[1] | (y)->v32[1],     \
   (z)->v32[2] = (x)->v32[2] | (y)->v32[2],     \
   (z)->v32[3] = (x)->v32[3] | (y)->v32[3]      \
)

#define _v128_complement(x)        \
(                                  \
   (x)->v32[0] = ~(x)->v32[0],     \
   (x)->v32[1] = ~(x)->v32[1],     \
   (x)->v32[2] = ~(x)->v32[2],     \
   (x)->v32[3] = ~(x)->v32[3]      \
)

/* ok for NO_64BIT_MATH if it can compare uint64_t's (even as structures) */
#define _v128_is_eq(x, y)                                        \
  (((x)->v64[0] == (y)->v64[0]) && ((x)->v64[1] == (y)->v64[1]))


#define _v128_xor_eq(z, x)         \
(                                  \
   (z)->v64[0] ^= (x)->v64[0],     \
   (z)->v64[1] ^= (x)->v64[1]      \
)

/* NOTE!  This assumes an odd ordering! */
/* This will not be compatible directly with math on some processors */
/* bit 0 is first 32-bit word, low order bit. in little-endian, that's
   the first byte of the first 32-bit word.  In big-endian, that's
   the 3rd byte of the first 32-bit word */
/* The get/set bit code is used by the replay code ONLY, and it doesn't
   really care which bit is which.  AES does care which bit is which, but
   doesn't use the 128-bit get/set or 128-bit shifts  */

#define _v128_get_bit(x, bit) (((((x)->v32[(bit) >> 5]) >> ((bit) & 31)) & 1))
#define _v128_set_bit(x, bit) ((((x)->v32[(bit) >> 5]) |= ((uint32_t)1 << ((bit) & 31))))
#define _v128_clear_bit(x, bit) ((((x)->v32[(bit) >> 5]) &= ~((uint32_t)1 << ((bit) & 31))))
#define _v128_set_bit_to(x, bit, value)   ((value) ? _v128_set_bit(x, bit) : _v128_clear_bit(x, bit))

#define v128_set_to_zero(z)       _v128_set_to_zero(z)
#define v128_copy(z, x)           _v128_copy(z, x)
#define v128_xor(z, x, y)         _v128_xor(z, x, y)
#define v128_and(z, x, y)         _v128_and(z, x, y)
#define v128_or(z, x, y)          _v128_or(z, x, y)
#define v128_complement(x)        _v128_complement(x) 
#define v128_is_eq(x, y)          _v128_is_eq(x, y)
#define v128_xor_eq(x, y)         _v128_xor_eq(x, y)
#define v128_get_bit(x, i)        _v128_get_bit(x, i)
#define v128_set_bit(x, i)        _v128_set_bit(x, i)
#define v128_clear_bit(x, i)      _v128_clear_bit(x, i)
#define v128_set_bit_to(x, i, y)  _v128_set_bit_to(x, i, y)

/*
 * octet_string_is_eq(a,b, len) returns 1 if the length len strings a
 * and b are not equal, returns 0 otherwise
 */

inline bool octet_string_is_eq(uint8_t *a, uint8_t *b, int len) { return memcmp(a, b, len) == 0; }
inline void octet_string_set_to_zero(uint8_t *s, int len) { memset(s, 0, len); }

/* 
 * Convert big endian integers to CPU byte order.
 */
//#include <winsock2.h>
#define be32_to_cpu(x)	ntohl((x))
/* use the native 64-bit math */
inline uint64_t be64_to_cpu(uint64_t v) { return (uint64_t)((be32_to_cpu((uint32_t)(v >> 32))) | (((uint64_t)be32_to_cpu((uint32_t)v)) << 32)); }
/* _DATATYPES_H */
//--------------------------------------
