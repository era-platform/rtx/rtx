﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtp_port_manager_interface.h"
#include "std_sys_env.h"
#include "rtp_simple_channel.h"
#include "rtp_webrtc_channel.h"
#include "rtp_secure_channel.h"
#include "udptl_proxy_channel.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define LOG_PREFIX "RTP-IPM" // Media Gateway Interface Port Manager
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define RTP_CHANNEL_ALLOCATED_FLAG			0x80000000
#define RTP_CHANNEL_BANNED_FLAG				0x40000000
#define RTP_CHANNEL_FLAG_MASK				0xF0000000
#define RTP_CHANNEL_ATTEMP_MASK				0x0FFF0000
#define RTP_CHANNEL_PORT_MASK				0x0000FFFF
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtp_port_manager_interface_t::rtp_port_manager_interface_t(rtl::Logger* log) :
	m_log(log), m_mutex("mg-port-man-iface"), m_reader_manager(log)
{
	m_port_last = 0;
	m_port_begin = 0;
	m_port_end = 0;

	m_active = true;
	
	//m_address_key.clear();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtp_port_manager_interface_t::~rtp_port_manager_interface_t()
{
	destroy();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtp_port_manager_interface_t::create(const char* iface, const char* address_key)
{
	PLOG_RTP_WRITE(LOG_PREFIX, "create -- begin.");

	if (iface == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "create -- ip_interface is null.");
		return false;
	}

	if (address_key == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "create -- address_key is null.");
		return false;
	}

	m_interface.parse(iface);

	m_address_key = address_key;

	if (!m_reader_manager.create())
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "create -- create reader manager fail.");
		return false;
	}

	rtl::Timer::setTimer(timer_cleanup_purgatory, this, 0, 500, true);

	PLOG_RTP_WRITE(LOG_PREFIX, "create -- end.(interfase: %s)", iface);
	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_port_manager_interface_t::destroy()
{
	PLOG_RTP_WRITE(LOG_PREFIX, "destroy -- begin.");

	m_active = false;

	rtl::Timer::stopTimer(timer_cleanup_purgatory, this, 0);

	PLOG_RTP_WRITE(LOG_PREFIX, "destroy -- destroyng inteface %s...", m_interface.to_string());

	m_reader_manager.destroy();

	{
		//rtl::MutexLock lock(m_mutex);
		rtl::MutexWatchLock lock(m_mutex, __FUNCTION__);

		// пробежимся по мертвым каналам
		for (int i = (int)m_purgatory.getCount() - 1; i >= 0; i--)
		{
			rtp_channel_t* channel = m_purgatory.getAt(i);
			delete_channel(channel);
		}
		m_purgatory.clear();
		PLOG_RTP_WRITE(LOG_PREFIX, "destroy -- purgatory cleaned");

		// проверим порты. если остались занятые то отругаемся.
		ports_check();

		m_port_range.clear();
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "destroy -- end.");
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_port_manager_interface_t::set_port_range(uint16_t begin, uint32_t count)
{
	PLOG_RTP_WRITE(LOG_PREFIX, "set_port_range -- begin.");

	if (!m_active)
	{
		PLOG_RTP_WARNING(LOG_PREFIX, "set_port_range -- inactive");
		return;
	}

	if (begin + count >= 65536)
	{
		PLOG_RTP_WARNING(LOG_PREFIX, "set_port_range -- invalid range : start %u count %d", begin, count);
		return;
	}

	if (begin == 0)
	{
		PLOG_RTP_WRITE(LOG_PREFIX, "set_port_range -- begin == 0. Ports free.");

		//rtl::MutexLock lock(m_mutex);
		rtl::MutexWatchLock lock(m_mutex, __FUNCTION__);

		m_port_range.clear();
		m_port_begin = begin;
		m_port_end = 0;

		return;
	}

	{
		//rtl::MutexLock lock(m_mutex);
		rtl::MutexWatchLock lock(m_mutex, __FUNCTION__);

		uint16_t port_end = begin + (count - 1);

		if ((begin > m_port_end) || (m_port_begin > port_end))
		{
			m_port_range.clear();
		}

		if ((m_port_range.getCount() == 0))
		{
			for (int i = 0; i < (int)count; i++)
			{
				uint32_t port = begin + i;
				m_port_range.add(port);
			}
		}
		else
		{
			// выравниваем спереди.
			if ((begin > m_port_begin) && (begin < m_port_end))
			{
				//	|x|x|x|x|x|x|x|x|x|x|x|...
				//	      |x|x|x|x|x|x|x|x|...
				m_port_range.removeRange(0, begin - m_port_begin);
			}
			else if ((begin < m_port_begin) && (port_end > m_port_begin))
			{
				//		  |x|x|x|x|x|x|x|x|...
				//	|x|x|x|x|x|x|x|x|x|x|x|...
				for (int i = 0; i < (m_port_begin - begin); i++)
				{
					uint32_t port = begin + i;
					m_port_range.insert(i, port);
				}
			}

			// выравниваем сзади.
			if (m_port_end < port_end)
			{
				//	|x|x|x|x|x|x|x|x|x|x|x|
				//	|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|
				for (int i = (int)m_port_end + 1; i <= (int)(port_end); i++)
				{
					uint32_t port = i;
					m_port_range.add(port);
				}
			}
			else if (m_port_end > port_end)
			{
				//	|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|
				//	|x|x|x|x|x|x|x|x|x|x|x|
				int index_begin = port_end - begin + 1;
				m_port_range.removeRange(index_begin, m_port_range.getCount());
			}
		}

		m_port_begin = begin;
		m_port_end = port_end;
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "set_port_range -- end. (start: %u end: %u)", m_port_begin, m_port_end);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtp_channel_t* rtp_port_manager_interface_t::get_channel(rtp_channel_type_t type)
{
	PLOG_RTP_WRITE(LOG_PREFIX, "get_channel -- begin.");

	if (!m_active)
	{
		PLOG_RTP_WARNING(LOG_PREFIX, "get_channel -- inactive");
		return nullptr;
	}

	rtp_channel_t* channel = create_channel(type);
	if (channel == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "get_channel -- create channel fail.");
		return nullptr;
	}

	if (!bind_channel(channel))
	{
		PLOG_RTP_WARNING(LOG_PREFIX, "get_channel -- failed : no port allocated.");
		channel->destroy();
		DELETEO(channel);
		return nullptr;
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "get_channel -- end. channel ready : %s.", (const char*)channel->getName());
	if (rtl::Logger::check_trace(TRF_RTP))
	{
		channel->print_ports("| busy ports |", m_log);
	}

	return channel;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_port_manager_interface_t::release_channel(rtp_channel_t* channel)
{
	PLOG_RTP_WRITE(LOG_PREFIX, "release_channel -- begin.");

	if (channel == nullptr)
	{
		PLOG_RTP_WARNING(LOG_PREFIX, "release_channel -- channel is null.");
		return;
	}

	{
		//rtl::MutexLock lock(m_mutex);
		rtl::MutexWatchLock lock(m_mutex, __FUNCTION__);

		channel->stop_io();
		channel->set_time_destroy(rtl::DateTime::getTicks());
		m_purgatory.add(channel);
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "release_channel -- move to purgatory channel %s", channel->getName());
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtp_channel_t* rtp_port_manager_interface_t::create_channel(rtp_channel_type_t type)
{
	if (type == rtp_channel_type_t::rtp_channel_type_simple_e)
	{
		return NEW rtp_simple_channel_t(m_log);
	}
	else if ((type == rtp_channel_type_t::rtp_channel_type_webrtc_e) || (type == rtp_channel_type_t::rtp_channel_type_webrtc_bundle_e))
	{
		rtp_webrtc_channel_t* channel = NEW rtp_webrtc_channel_t(m_log, type);
		if (!channel->init())
		{
			DELETEO(channel);
			channel = nullptr;
			return nullptr;
		}
		return channel;
	}
	else if (type == rtp_channel_type_t::rtp_channel_type_secure_e)
	{
		return NEW rtp_secure_channel_t(m_log);
	}
	else if (type == rtp_channel_type_t::rtp_channel_type_udptl_proxy_e)
	{
		return NEW udptl_proxy_channel_t(m_log);
	}

	return nullptr;
}
//------------------------------------------------------------------------------
bool rtp_port_manager_interface_t::bind_channel(rtp_channel_t* channel)
{
	if (channel == nullptr)
	{
		return false;
	}

	rtl::MutexWatchLock lock(m_mutex, __FUNCTION__);

	uint16_t port_data = bind_channel_data_port(channel);
	if (port_data == 0)
	{
		return false;
	}

	uint16_t port_ctrl = bind_channel_ctrl_port(channel);
	if (port_ctrl == 0)
	{
		port_free(port_data);
		return false;
	}

	if (channel->getType() == rtp_channel_type_t::rtp_channel_type_webrtc_bundle_e)
	{
		uint16_t port_video = bind_channel_video_port(channel);
		if (port_video == 0)
		{
			port_free(port_data);
			port_free(port_ctrl);
			return false;
		}

		uint16_t port_ctrl_video = bind_channel_ctrl_video_port(channel);
		if (port_ctrl_video == 0)
		{
			port_free(port_video);
			port_free(port_data);
			port_free(port_ctrl);
			return false;
		}
	}

	if (port_ctrl == port_data)
	{
		// в случае когда канал без контрольного порта.
		return true;
	}

	if (port_ctrl != (port_data + 1))
	{
		port_free(port_data);
		port_free(port_ctrl);
		return false;
	}

	return true;
}
//------------------------------------------------------------------------------
uint16_t rtp_port_manager_interface_t::bind_channel_data_port(rtp_channel_t* channel)
{
	if (channel == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "bind_channel_data_port -- channel is null.");
		return 0;
	}

	uint16_t port_data = even_port_alloc();
	// возвращается 0 если не задан диапазон портов и порт нужно брать по умолчанию.
	if ((port_data == 0) && (m_port_begin != 0))
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "bind_channel_data_port -- port not found.");
		return 0;
	}

	// проверяем в цикле порты
	bool go = true;
	while (go)
	{
		uint16_t port_data_bind = bind_channel_data_port(channel, port_data);
		if ((port_data_bind == 0) && (port_data != 0))
		{
			// порт кем то занят отметим его
			port_ban(port_data);
		}
		else
		{
			return port_data_bind;
		}

		// берем следующий порт
		port_data = even_port_alloc();
		if (port_data == 0)
		{
			go = false;
			// свободных портов нет
			break;
		}
	}

	return 0;
}
//------------------------------------------------------------------------------
uint16_t rtp_port_manager_interface_t::bind_channel_ctrl_port(rtp_channel_t* channel)
{
	if (channel == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "bind_channel_ctrl_port -- channel is null.");
		return 0;
	}

	uint16_t port_data = channel->get_data_port();

	uint16_t port_ctrl = port_alloc();
	if (port_ctrl != (port_data + 1))
	{
		return 0;
	}

	uint16_t port_ctrl_bind = bind_channel_ctrl_port(channel, port_ctrl);
	if (port_ctrl_bind == 0)
	{
		// порт кем то занят отметим его
		port_ban(port_ctrl);
	}
	
	return port_ctrl_bind;
}
//------------------------------------------------------------------------------
uint16_t rtp_port_manager_interface_t::bind_channel_data_port(rtp_channel_t* channel, uint16_t port)
{
	if (channel == nullptr)
	{
		return 0;
	}

	if (channel->getType() == rtp_channel_type_t::rtp_channel_type_simple_e)
	{
		rtp_simple_channel_t* simpl_channel = (rtp_simple_channel_t*)channel;
		if (simpl_channel->bind_data_socket(&m_interface, port))
		{
			return simpl_channel->get_data_port();
		}
	}
	else if ((channel->getType() == rtp_channel_type_t::rtp_channel_type_webrtc_bundle_e) ||
		(channel->getType() == rtp_channel_type_t::rtp_channel_type_webrtc_e))
	{
		rtp_webrtc_channel_t* web_channel = (rtp_webrtc_channel_t*)channel;
		if (web_channel->bind_audio_socket(&m_interface, port))
		{
			return web_channel->get_data_port();
		}
	}
	else if (channel->getType() == rtp_channel_type_t::rtp_channel_type_secure_e)
	{
		rtp_secure_channel_t* secure_channel = (rtp_secure_channel_t*)channel;
		if (secure_channel->bind_data_socket(&m_interface, port))
		{
			return secure_channel->get_data_port();
		}
	}
	else if (channel->getType() == rtp_channel_type_t::rtp_channel_type_udptl_proxy_e)
	{
		udptl_proxy_channel_t* udptl_proxy_channel = (udptl_proxy_channel_t*)channel;
		if (udptl_proxy_channel->bind_data_socket(&m_interface, port))
		{
			return udptl_proxy_channel->get_data_port();
		}
	}

	return 0;
}
//------------------------------------------------------------------------------
uint16_t rtp_port_manager_interface_t::bind_channel_ctrl_port(rtp_channel_t* channel, uint16_t port)
{
	if (channel == nullptr)
	{
		return 0;
	}

	if (channel->getType() == rtp_channel_type_t::rtp_channel_type_simple_e)
	{
		rtp_simple_channel_t* simpl_channel = (rtp_simple_channel_t*)channel;
		if (simpl_channel->bind_ctrl_socket(&m_interface, port))
		{
			return simpl_channel->get_ctrl_port();
		}
	}
	else if ((channel->getType() == rtp_channel_type_t::rtp_channel_type_webrtc_e) ||
		(channel->getType() == rtp_channel_type_t::rtp_channel_type_webrtc_bundle_e))
	{
		rtp_webrtc_channel_t* web_channel = (rtp_webrtc_channel_t*)channel;
		if (web_channel->bind_ctrl_audio_socket(&m_interface, port))
		{
			return web_channel->get_ctrl_port();
		}
	}
	else if (channel->getType() == rtp_channel_type_t::rtp_channel_type_secure_e)
	{
		rtp_secure_channel_t* secure_channel = (rtp_secure_channel_t*)channel;
		if (secure_channel->bind_ctrl_socket(&m_interface, port))
		{
			return secure_channel->get_ctrl_port();
		}
	}
	else if (channel->getType() == rtp_channel_type_t::rtp_channel_type_udptl_proxy_e)
	{
		udptl_proxy_channel_t* udptl_proxy_channel = (udptl_proxy_channel_t*)channel;
		return udptl_proxy_channel->get_data_port();
	}

	return 0;
}
//------------------------------------------------------------------------------
uint16_t rtp_port_manager_interface_t::bind_channel_video_port(rtp_channel_t* channel)
{
	uint16_t port_data = port_alloc();
	// возвращается 0 если не задан диапазон портов и порт нужно брать по умолчанию.
	if ((port_data == 0) && (m_port_begin != 0))
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "bind_channel_video_port -- port not found.");
		return 0;
	}

	// проверяем в цикле порты
	bool go = true;
	while (go)
	{
		uint16_t port_data_bind = bind_channel_video_port(channel, port_data);
		if ((port_data_bind == 0) && (port_data != 0))
		{
			// порт кем то занят отметим его
			port_ban(port_data);
		}
		else
		{
			return port_data_bind;
		}

		// берем следующий порт
		port_data = port_alloc();
		if (port_data == 0)
		{
			go = false;
			// свободных портов нет
			break;
		}
	}

	return 0;
}
//------------------------------------------------------------------------------
uint16_t rtp_port_manager_interface_t::bind_channel_ctrl_video_port(rtp_channel_t* channel)
{
	uint16_t port_data = channel->get_data_port();

	uint16_t port_ctrl = port_alloc();
	if ((port_ctrl == 0) && (m_port_begin == 0))
	{
		port_ctrl = port_data + 1;
	}

	uint16_t port_ctrl_bind = bind_channel_ctrl_video_port(channel, port_ctrl);
	if (port_ctrl_bind == 0)
	{
		// порт кем то занят отметим его
		port_ban(port_ctrl);
	}

	return port_ctrl_bind;
}
//------------------------------------------------------------------------------
uint16_t rtp_port_manager_interface_t::bind_channel_video_port(rtp_channel_t* channel, uint16_t port)
{
	if (channel->getType() == rtp_channel_type_t::rtp_channel_type_webrtc_bundle_e)
	{
		rtp_webrtc_channel_t* web_channel = (rtp_webrtc_channel_t*)channel;
		if (web_channel->bind_video_socket(&m_interface, port))
		{
			return web_channel->get_video_port();
		}
	}

	return 0;
}
//------------------------------------------------------------------------------
uint16_t rtp_port_manager_interface_t::bind_channel_ctrl_video_port(rtp_channel_t* channel, uint16_t port)
{
	if (channel->getType() == rtp_channel_type_t::rtp_channel_type_webrtc_bundle_e)
	{
		rtp_webrtc_channel_t* web_channel = (rtp_webrtc_channel_t*)channel;
		if (web_channel->bind_ctrl_video_socket(&m_interface, port))
		{
			return web_channel->get_ctrl_video_port();
		}
	}

	return 0;
}
//------------------------------------------------------------------------------
bool rtp_port_manager_interface_t::delete_channel(rtp_channel_t* channel)
{
	if (channel == nullptr)
	{
		return false;
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "delete_channel -- %s", channel->getName());

	uint32_t data_port = 0;
	uint32_t ctrl_port = 0;
	uint32_t video_data_port = 0;
	uint32_t video_ctrl_port = 0;

	if (channel->getType() == rtp_channel_type_t::rtp_channel_type_simple_e)
	{
		rtp_simple_channel_t* simpl_channel = (rtp_simple_channel_t*)channel;
		data_port = simpl_channel->get_data_port();
		ctrl_port = simpl_channel->get_ctrl_port();
	}
	else if (channel->getType() == rtp_channel_type_t::rtp_channel_type_webrtc_e)
	{
		rtp_webrtc_channel_t* web_channel = (rtp_webrtc_channel_t*)channel;
		data_port = web_channel->get_data_port();
		ctrl_port = web_channel->get_ctrl_port();
		video_data_port = web_channel->get_video_port();
		video_ctrl_port = web_channel->get_ctrl_video_port();
	}
	else if (channel->getType() == rtp_channel_type_t::rtp_channel_type_webrtc_bundle_e)
	{
		rtp_webrtc_channel_t* web_channel = (rtp_webrtc_channel_t*)channel;
		data_port = web_channel->get_data_port();
		ctrl_port = web_channel->get_ctrl_port();
		video_data_port = web_channel->get_video_port();
		video_ctrl_port = web_channel->get_ctrl_video_port();
	}
	else if (channel->getType() == rtp_channel_type_t::rtp_channel_type_secure_e)
	{
		rtp_secure_channel_t* secure_channel = (rtp_secure_channel_t*)channel;
		data_port = secure_channel->get_data_port();
		ctrl_port = secure_channel->get_ctrl_port();
	}
	else if (channel->getType() == rtp_channel_type_t::rtp_channel_type_udptl_proxy_e)
	{
		udptl_proxy_channel_t* udptl_proxy_channel = (udptl_proxy_channel_t*)channel;
		data_port = udptl_proxy_channel->get_data_port();
	}
	else
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "delete_channel -- %s wrong type.", channel->getName());
		return false;
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "delete_channel -- need free ports:");
	
	if (rtl::Logger::check_trace(TRF_RTP))
	{
		rtl::String msg = "| free ports | ";
		msg << (const char*)channel->getName();
		channel->print_ports((const char*)msg, m_log);
	}

	if (channel->destroy())
	{
		if (data_port != 0)
		{
			port_free(data_port);
		}
		if (ctrl_port != 0)
		{
			port_free(ctrl_port);
		}
		if (video_data_port != 0)
		{
			port_free(video_data_port);
		}
		if (video_ctrl_port != 0)
		{
			port_free(video_ctrl_port);
		}

		return true;
	}

	return false;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtp_port_manager_interface_t::start_channel_listen(rtp_channel_t* channel)
{
	PLOG_RTP_WRITE(LOG_PREFIX, "start_channel_listen -- begin.");

	if (!m_active)
	{
		PLOG_RTP_WARNING(LOG_PREFIX, "start_channel_listen -- inactive");
		return false;
	}

	if (channel == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "start_channel_listen -- channel is null.");
		return false;
	}

	if (!channel->get_net_interface()->equals(&m_interface))
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "start_channel_listen -- wrong interface.");
		return false;
	}

	if (!m_reader_manager.start_channel_listen(channel))
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "start_channel_listen -- start in reader manager failed.");
		return false;
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "start_channel_listen --end.");
	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_port_manager_interface_t::stop_channel_listen(rtp_channel_t* channel)
{
	PLOG_RTP_WRITE(LOG_PREFIX, "stop_channel_listen -- begin.");

	if (channel == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "stop_channel_listen -- channel is null.");
		return;
	}

	if (!channel->get_net_interface()->equals(&m_interface))
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "stop_channel_listen -- wrong interface.");
		return;
	}

	m_reader_manager.stop_channel_listen(channel);
	PLOG_RTP_WRITE(LOG_PREFIX, "stop_channel_listen -- end.");
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint16_t rtp_port_manager_interface_t::even_port_alloc()
{
	PLOG_RTP_WRITE(LOG_PREFIX, "even_port_alloc -- begin.");

	int len = m_port_range.getCount();
	uint16_t port = port_alloc();
	int count = 0;
	uint16_t first_port = port;
	while ((port % 2) > 0)
	{
		port_free(port);
		port = port_alloc();

		count++;
		if ((first_port == port) || (count > len))
		{
			port = 0;
			break;
		}
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "even_port_alloc -- end(%u).", port);
	return port;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint16_t rtp_port_manager_interface_t::port_alloc()
{
	PLOG_RTP_WRITE(LOG_PREFIX, "port_alloc -- begin.");

	// выбор порта по кольцу!
	if (m_port_last > uint32_t(m_port_end - m_port_begin))
	{
		m_port_last = 0;
	}

	// пройдемся от последнего до конца
	for (int i = m_port_last; i < m_port_range.getCount(); i++)
	{
		uint32_t port_state = m_port_range.getAt(i);

		uint32_t state = port_state & RTP_CHANNEL_FLAG_MASK;
		if (state == 0)
		{
			port_state |= RTP_CHANNEL_ALLOCATED_FLAG;
			m_port_range.setAt(i, port_state);
			m_port_last = i + 1;
			uint16_t port = (uint16_t)(port_state & RTP_CHANNEL_PORT_MASK);

			PLOG_RTP_WRITE(LOG_PREFIX, "port_alloc -- end. port %u allocated index:%d last:%d", port, i, m_port_last);
			return port;
		}
		else if (state == RTP_CHANNEL_ALLOCATED_FLAG)
		{
			uint16_t port = (uint16_t)(port_state & RTP_CHANNEL_PORT_MASK);
			PLOG_RTP_WRITE(LOG_PREFIX, "port_alloc -- port %u ALLOCATED.", port);
		}
		else if (state == RTP_CHANNEL_BANNED_FLAG)
		{
			uint16_t port = (uint16_t)(port_state & RTP_CHANNEL_PORT_MASK);
			PLOG_RTP_WRITE(LOG_PREFIX, "port_alloc -- port %u BANNED.", port);
		}
	}

	// пройдемся от начала до вверх
	for (uint32_t i = 0; i < m_port_last; i ++)
	{
		uint32_t port_state = m_port_range[i];
		if ((port_state & RTP_CHANNEL_FLAG_MASK) == 0)
		{
			port_state |= RTP_CHANNEL_ALLOCATED_FLAG;
			m_port_range.setAt(i, port_state);
			m_port_last = i + 1;
			uint16_t port = (uint16_t)(port_state & RTP_CHANNEL_PORT_MASK);

			PLOG_RTP_WRITE(LOG_PREFIX, "port_alloc -- end. port %u allocated index:%d last:%d", port, i, m_port_last);
			return port;
		}
	}

	PLOG_RTP_WARNING(LOG_PREFIX, "port_alloc -- no port can be allocated! last %d begin %u end %u", m_port_last, m_port_begin, m_port_end);
	return 0;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_port_manager_interface_t::port_free(uint16_t port)
{
	PLOG_RTP_WRITE(LOG_PREFIX, "port_free -- begin.");

	// сбрасываем флаг занятости
	int index = int(port - m_port_begin);
	uint32_t port_state = port;

	if (index >= 0 && index < m_port_range.getCount())
	{
		PLOG_RTP_WRITE(LOG_PREFIX, "port_free -- port %u freed.", port);
		m_port_range.setAt(index, port_state);
	}
	else
	{
		PLOG_RTP_WARNING(LOG_PREFIX, "port_free -- unknown port %u -- ignored.", port);
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "port_free -- end.");
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_port_manager_interface_t::port_ban(uint16_t port)
{
	PLOG_RTP_WRITE(LOG_PREFIX, "port_ban -- begin.");

	// сбрасываем флаг занятости
	int index = int(port - m_port_begin);

	if (index >= 0 && index < m_port_range.getCount())
	{
		uint32_t port_state = m_port_range.getAt(index);
		uint32_t attempt_count = (port_state & RTP_CHANNEL_ATTEMP_MASK) >> 16;

		if (++attempt_count > 8)
		{
			port_state = RTP_CHANNEL_BANNED_FLAG | port;
			PLOG_RTP_WRITE(LOG_PREFIX, "port_ban -- ban port %u -- attempt:%d", port, attempt_count);
		}
		else
		{
			port_state = (attempt_count << 16) | port;
			PLOG_RTP_WRITE(LOG_PREFIX, "port_ban -- add attempts to port %u attempt:%d", port, attempt_count);
		}

		m_port_range.setAt(index, port_state);
	}
	else
	{
		PLOG_RTP_WARNING(LOG_PREFIX, "port_ban -- unknown port %u -- ignored", port);
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "port_ban -- end.");
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_port_manager_interface_t::ports_check()
{
	for (int i = 0; i < m_port_range.getCount(); i ++)
	{
		uint32_t port_state = m_port_range.getAt(i);

		if ((port_state & RTP_CHANNEL_FLAG_MASK) == RTP_CHANNEL_ALLOCATED_FLAG)
		{
			uint16_t port = (uint16_t)(port_state & RTP_CHANNEL_PORT_MASK);

			PLOG_RTP_WARNING(LOG_PREFIX, "ports_check -- port %u not free!", port);
		}
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_port_manager_interface_t::timer_cleanup_purgatory(rtl::Timer* sender, uint32_t elapsed, void* userData, int tid)
{
	rtl::async_call_manager_t::callAsync(async_cleanup_purgatory, (void*)userData, 0, elapsed, nullptr);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_port_manager_interface_t::async_cleanup_purgatory(void* userData, uint16_t callId, uintptr_t intParam, void* ptrParam)
{
	rtp_port_manager_interface_t* manager = (rtp_port_manager_interface_t*)userData;
	manager->cleanup_purgatory();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_port_manager_interface_t::cleanup_purgatory()
{
	uint32_t currentTime = rtl::DateTime::getTicks();

	{
		//rtl::MutexLock lock(m_mutex);
		rtl::MutexWatchLock lock(m_mutex, __FUNCTION__);

		// пробежимся по мертвым каналам
		for (int i = (int)m_purgatory.getCount() - 1; i >= 0; i--)
		{
			rtp_channel_t* channel = m_purgatory.getAt(i);
			if (channel == nullptr)
			{
				continue;
			}
			if (currentTime - channel->get_time_destroy() > 1000)
			{
				if (delete_channel(channel))
				{
					m_purgatory.removeAt(i);
					DELETEO(channel);
					channel = nullptr;
				}
			}
		}
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const ip_address_t*	 rtp_port_manager_interface_t::get_interface() const
{
	return &m_interface;
}
//------------------------------------------------------------------------------
const char*	rtp_port_manager_interface_t::get_address_key() const
{
	return m_address_key;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtp_port_manager_interface_t::active()
{
	return m_active;
}
//------------------------------------------------------------------------------
void rtp_port_manager_interface_t::activate(bool activate)
{
	m_active = activate;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
