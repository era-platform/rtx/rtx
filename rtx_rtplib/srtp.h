﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//--------------------------------------
// based on libsrtp written by
// David A. McGrew
// Cisco Systems, Inc.
//--------------------------------------

#pragma once

#include "rtx_rtplib.h"
#include "srtp_crypto.h"
#include "srtp_key.h"
#include "srtp_define.h"
//--------------------------------------
// SRTP_MASTER_KEY_LEN is the nominal master key length supported by libSRTP
//--------------------------------------
#define SRTP_MASTER_KEY_LEN 30
//--------------------------------------
// SRTP_MAX_KEY_LEN is the maximum key length supported by libSRTP
//--------------------------------------
#define SRTP_MAX_KEY_LEN 64
//--------------------------------------
// SRTP_MAX_TAG_LEN is the maximum tag length supported by libSRTP
//--------------------------------------
#define SRTP_MAX_TAG_LEN 12 
//--------------------------------------
// SRTP_MAX_TRAILER_LEN is the maximum length of the SRTP trailer
// (authentication tag and MKI) supported by libSRTP.  This value is
// the maximum number of octets that will be added to an RTP packet by
// srtp_protect().
//
// the maximum number of octets added by srtp_protect().
//--------------------------------------
//--------------------------------------
// nota bene: since libSRTP doesn't support the use of the MKI, the
// SRTP_MAX_TRAILER_LEN value is just the maximum tag length
//--------------------------------------
#define SRTP_MAX_TRAILER_LEN SRTP_MAX_TAG_LEN 
//--------------------------------------
// rtp headers
//--------------------------------------
#pragma pack(push, 1)

//struct srtp_hdr_t
//{
//	uint8_t cc:4;		/* CSRC count             */
//	uint8_t x:1;		/* header extension flag  */
//	uint8_t p:1;		/* padding flag           */
//	uint8_t version:2;	/* protocol version    */
//	uint8_t pt:7;		/* payload type           */
//	uint8_t m:1;		/* marker bit             */
//	uint16_t seq;		/* sequence number        */
//	uint32_t ts;		/* timestamp              */
//	uint32_t ssrc;		/* synchronization source */
//};

//struct srtcp_hdr_t
//{
//	uint8_t rc:5;		/* reception report count */
//	uint8_t p:1;		/* padding flag           */
//	uint8_t version:2;	/* protocol version       */
//	uint8_t pt:8;		/* payload type           */
//	uint16_t len;		/* length                 */
//	uint32_t ssrc;	    /* synchronization source */
//};

struct srtcp_trailer_t
{
	uint32_t index:31;    /* srtcp packet index in network order! */
	uint32_t e:1;         /* encrypted? 1=yes */
	/* optional mikey/etc go here */
	/* and then the variable-length auth tag */
};

#pragma pack(pop)

struct srtp_hdr_xtnd_t
{
	uint16_t profile_specific;    /* profile-specific info               */
	uint16_t length;              /* number of 32-bit words in extension */
};
//--------------------------------------
// A sec_serv_t enumeration is used to describe the particular
// security services that will be applied by a particular crypto
// policy (or other mechanism).
//--------------------------------------
enum sec_serv_t
{
	sec_serv_none          = 0,		// no services
	sec_serv_conf          = 1,		// confidentiality
	sec_serv_auth          = 2,		// authentication
	sec_serv_conf_and_auth = 3		// confidentiality and authentication
};
//--------------------------------------
// A crypto_policy_t describes a particular cryptographic policy that
// can be applied to an SRTP or SRTCP stream.  An SRTP session policy
// consists of a list of these policies, one for each SRTP stream 
// in the session.
//--------------------------------------
struct crypto_policy_t
{
	uint32_t cipher_type;		// An integer representing the type of cipher. */
	int cipher_key_len;			// The length of the cipher key in octets. */
	uint32_t auth_type;			// An integer representing the authentication function. */
	int auth_key_len;			// The length of the authentication function key in octets. */
	int auth_tag_len;			// The length of the authentication tag in octets. */
	sec_serv_t sec_serv;		// The flag indicating the security services to be applied. */
};
//--------------------------------------
// represents the policy for an SRTP session.  
//--------------------------------------
struct srtp_policy_t
{
	uint32_t ssrc;				// The SSRC value of stream.
	crypto_policy_t rtp;        // SRTP crypto policy.
	crypto_policy_t rtcp;       // SRTCP crypto policy.
	uint8_t *key;				// Pointer to the SRTP master key for.
};
//--------------------------------------
// crypto_policy_set_rtp_default() sets a crypto policy
// structure to the SRTP default policy for RTP protection.
//--------------------------------------
// The function call crypto_policy_set_rtp_default(&p) sets the
// crypto_policy_t at location p to the SRTP default policy for RTP
// protection, as defined in the specification.  This function is a
// convenience that helps to avoid dealing directly with the policy
// data structure.  You are encouraged to initialize policy elements
// with this function call.  Doing so may allow your code to be
// forward compatible with later versions of libSRTP that include more
// elements in the crypto_policy_t datatype.
//--------------------------------------
void crypto_policy_set_rtp_default(crypto_policy_t *p);
//--------------------------------------
// The function call crypto_policy_set_rtcp_default(&p) sets the
// crypto_policy_t at location p to the SRTP default policy for RTCP
// protection, as defined in the specification.  This function is a
// convenience that helps to avoid dealing directly with the policy
// data structure.  You are encouraged to initialize policy elements
// with this function call.  Doing so may allow your code to be
// forward compatible with later versions of libSRTP that include more
// elements in the crypto_policy_t datatype.
//--------------------------------------
void crypto_policy_set_rtcp_default(crypto_policy_t *p);
//--------------------------------------
// The function crypto_policy_set_aes_cm_128_hmac_sha1_80() is a
// synonym for crypto_policy_set_rtp_default().  It conforms to the
// naming convention used in
// http://www.ietf.org/internet-drafts/draft-ietf-mmusic-sdescriptions-12.txt
//--------------------------------------
#define crypto_policy_set_aes_cm_128_hmac_sha1_80(p) crypto_policy_set_rtp_default(p)
//--------------------------------------
// The function call crypto_policy_set_aes_cm_128_hmac_sha1_32(&p)
// sets the crypto_policy_t at location p to use policy
// AES_CM_128_HMAC_SHA1_32 as defined in
// draft-ietf-mmusic-sdescriptions-12.txt.  This policy uses AES-128
// Counter Mode encryption and HMAC-SHA1 authentication, with an
// authentication tag that is only 32 bits long.  This length is
// considered adequate only for protecting audio and video media that
// use a stateless playback function.  See Section 7.5 of RFC 3711
// (http://www.ietf.org/rfc/rfc3711.txt).
//
// This function is a convenience that helps to avoid dealing directly
// with the policy data structure.  You are encouraged to initialize
// policy elements with this function call.  Doing so may allow your
// code to be forward compatible with later versions of libSRTP that
// include more elements in the crypto_policy_t datatype.
//
// @warning This crypto policy is intended for use in SRTP, but not in
// SRTCP.  It is recommended that a policy that uses longer
// authentication tags be used for SRTCP.  See Section 7.5 of RFC 3711
// (http://www.ietf.org/rfc/rfc3711.txt).
//--------------------------------------
void crypto_policy_set_aes_cm_128_hmac_sha1_32(crypto_policy_t *p);
//--------------------------------------
// The function call crypto_policy_set_aes_cm_128_null_auth(&p) sets
// the crypto_policy_t at location p to use the SRTP default cipher
// (AES-128 Counter Mode), but to use no authentication method.  This
// policy is NOT RECOMMENDED unless it is unavoidable; see Section 7.5
// of RFC 3711 (http://www.ietf.org/rfc/rfc3711.txt).
//
// This function is a convenience that helps to avoid dealing directly
// with the policy data structure.  You are encouraged to initialize
// policy elements with this function call.  Doing so may allow your
// code to be forward compatible with later versions of libSRTP that
// include more elements in the crypto_policy_t datatype.
//
// @warning This policy is NOT RECOMMENDED for SRTP unless it is
// unavoidable, and it is NOT RECOMMENDED at all for SRTCP; see
// Section 7.5 of RFC 3711 (http://www.ietf.org/rfc/rfc3711.txt).
//--------------------------------------
void crypto_policy_set_aes_cm_128_null_auth(crypto_policy_t *p);
// The function call crypto_policy_set_null_cipher_hmac_sha1_80(&p)
// sets the crypto_policy_t at location p to use HMAC-SHA1 with an 80
// bit authentication tag to provide message authentication, but to
// use no encryption.  This policy is NOT RECOMMENDED for SRTP unless
// there is a requirement to forego encryption.  
// 
// This function is a convenience that helps to avoid dealing directly
// with the policy data structure.  You are encouraged to initialize
// policy elements with this function call.  Doing so may allow your
// code to be forward compatible with later versions of libSRTP that
// include more elements in the crypto_policy_t datatype.
//
// @warning This policy is NOT RECOMMENDED for SRTP unless there is a
// requirement to forego encryption.  
//--------------------------------------
void crypto_policy_set_null_cipher_hmac_sha1_80(crypto_policy_t *p);
//--------------------------------------
// The function call crypto_policy_set_rtp_default(&policy, profile)
// sets the crypto_policy_t at location policy to the policy for RTP
// protection, as defined by the srtp_profile_t profile.
// 
// This function is a convenience that helps to avoid dealing directly
// with the policy data structure.  You are encouraged to initialize
// policy elements with this function call.  Doing so may allow your
// code to be forward compatible with later versions of libSRTP that
// include more elements in the crypto_policy_t datatype.
//--------------------------------------
bool crypto_policy_set_from_profile_for_rtp(crypto_policy_t *policy, srtp_profile_t profile);
//--------------------------------------
// The function call crypto_policy_set_rtcp_default(&policy, profile)
// sets the crypto_policy_t at location policy to the policy for RTCP
// protection, as defined by the srtp_profile_t profile.
// 
// This function is a convenience that helps to avoid dealing directly
// with the policy data structure.  You are encouraged to initialize
// policy elements with this function call.  Doing so may allow your
// code to be forward compatible with later versions of libSRTP that
// include more elements in the crypto_policy_t datatype.
//--------------------------------------
bool crypto_policy_set_from_profile_for_rtcp(crypto_policy_t *policy, srtp_profile_t profile);
//--------------------------------------
// @brief returns the master key length for a given SRTP profile
//--------------------------------------
uint32_t srtp_profile_get_master_key_length(srtp_profile_t profile);
//--------------------------------------
// returns the master salt length for a given SRTP profile
//--------------------------------------
uint32_t srtp_profile_get_master_salt_length(srtp_profile_t profile);
//--------------------------------------
// The function call append_salt_to_key(k, klen, s, slen) 
// copies the string s to the location at klen bytes following
// the location k.  
//
// @warning There must be at least bytes_in_salt + bytes_in_key bytes
//          available at the location pointed to by key.
//--------------------------------------
void append_salt_to_key(uint8_t *key, uint32_t bytes_in_key, uint8_t *salt, uint32_t bytes_in_salt);
//--------------------------------------
/* in host order, so outside the #if */
//--------------------------------------
#define SRTCP_E_BIT      0x80000000
//--------------------------------------
/* for byte-access */
//--------------------------------------
#define SRTCP_E_BYTE_BIT 0x80
#define SRTCP_INDEX_MASK 0x7fffffff
/*--------------------------------------
 * The typedef srtp_stream_t is a pointer to a structure that
 * represents an SRTP stream.  This datatype is intentionally
 * opaque in order to separate the interface from the implementation. 
 * 
 * An SRTP stream consists of all of the traffic sent to an SRTP
 * session by a single participant.  A session can be viewed as
 * a set of streams.
 --------------------------------------*/
enum direction_t
{ 
	dir_unknown       = 0,
	dir_srtp_sender   = 1, 
	dir_srtp_receiver = 2
};
/*--------------------------------------
 * The enum srtp_event_t defines events that need to be handled
 * outside the `data plane', such as SSRC collisions and 
 * key expirations.  
 *
 * When a key expires or the maximum number of packets has been
 * reached, an SRTP stream will enter an `expired' state in which no
 * more packets can be protected or unprotected.  When this happens,
 * it is likely that you will want to either deallocate the stream
 * (using srtp_stream_dealloc()), and possibly allocate a new one.
 *
 * When an SRTP stream expires, the other streams in the same session
 * are unaffected, unless key sharing is used by that stream.  In the
 * latter case, all of the streams in the session will expire.
 --------------------------------------*/
enum srtp_event_t
{ 
	event_ssrc_collision,			// An SSRC collision occured.
	event_key_soft_limit,			// An SRTP stream reached the soft key usage limit and will expire soon.
	event_key_hard_limit,			// An SRTP stream reached the hard key usage limit and has expired.
	event_packet_index_limit		// An SRTP stream reached the hard packet limit (2^48 packets).
};

//--------------------------------------
// The struct srtp_event_data_t holds the data passed to the event handler function.  
//--------------------------------------
class srtp_stream_ctx_t;	// The stream in which the event happend.
//--------------------------------------
//
//--------------------------------------
struct srtp_event_data_t
{
	srtp_stream_ctx_t* stream;		// The stream in which the event happend.
	srtp_event_t  event_type;		// An enum indicating the type of event.
};
//--------------------------------------
// The typedef srtp_event_handler_func_t is the prototype for the
// event handler function.  It has as its only argument an
// srtp_event_data_t which describes the event that needs to be handled.
// There can only be a single, global handler for all events in libSRTP.
//--------------------------------------
typedef void (*srtp_event_handler_func_t)(srtp_event_data_t *data);
/*-------------------------------------- 
 * an srtp_stream_t has its own SSRC, encryption key, authentication
 * key, sequence number, and replay database
 * 
 * note that the keys might not actually be unique, in which case the
 * cipher_t and auth_t pointers will point to the same structures
 --------------------------------------*/
class ME_DEBUG_API srtp_stream_ctx_t
{
	rtl::Logger* m_log;
	cipher_t *m_rtp_cipher;
	auth_t *m_rtp_auth;
	rdbx_t m_rtp_rdbx;
	sec_serv_t m_rtp_services;
	cipher_t *m_rtcp_cipher;
	auth_t *m_rtcp_auth;
	rdb_t m_rtcp_rdb;
	sec_serv_t m_rtcp_services;
	key_limit_ctx_t m_limit;
	direction_t m_direction;
	srtp_event_handler_func_t m_event_handler;
	srtp_policy_t m_policy;

	void raise_event(srtp_event_t srtp_event);
	bool initialize_keys(const void *key);

public:
	srtp_stream_ctx_t(rtl::Logger* log);
	~srtp_stream_ctx_t();

	bool initialize(const srtp_policy_t *p);

	const srtp_policy_t* get_policy() { return &m_policy; }

	void set_direction(direction_t dir) { m_direction = dir; }
	direction_t get_direction() { return m_direction; }
	
	sec_serv_t get_rtp_service() const { return m_rtp_services; }
	void set_rtp_service(sec_serv_t serv)
	{
		m_rtp_services = serv;
	}

	sec_serv_t get_rtcp_service() const { return m_rtcp_services; }
	void set_rtcp_service(sec_serv_t serv) { m_rtcp_services = serv; }

	key_event_t update_limit() { return m_limit.update(); }

	auth_t* get_rtp_auth() { return m_rtp_auth; }
	rdbx_t* get_rtp_rdbx() { return &m_rtp_rdbx; }
	cipher_t* get_rtp_cipher() { return m_rtp_cipher; }

	auth_t* get_rtcp_auth() { return m_rtcp_auth; }
	rdb_t* get_rtcp_rdb() { return &m_rtcp_rdb; }
	cipher_t* get_rtcp_cipher() { return m_rtcp_cipher; }

	bool srtp_pack(void *rtp_hdr, int *len_ptr);
	bool srtp_unpack(void *srtp_hdr, int *len_ptr);
	
	bool srtcp_pack(void *rtcp_hdr, int *pkt_octet_len);
	bool srtcp_unpack(void *srtcp_hdr, int *pkt_octet_len);

	void set_event_handler(srtp_event_handler_func_t handler) { m_event_handler = handler; }
};
//--------------------------------------
