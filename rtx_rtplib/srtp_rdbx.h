﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//--------------------------------------
// based on libsrtp written by
// David A. McGrew
// Cisco Systems, Inc.
//--------------------------------------
#pragma once

#include "srtp_datatypes.h"
//--------------------------------------
//
//--------------------------------------
typedef uint16_t sequence_number_t;		// 16 bit sequence number
typedef uint32_t rollover_counter_t;	// 32 bit rollover counter
//--------------------------------------
//
//--------------------------------------
#define seq_num_median (1 << (8*sizeof(sequence_number_t) - 1))
#define seq_num_max (1 << (8*sizeof(sequence_number_t)))
//--------------------------------------
// An xtd_seq_num_t is a 64-bit unsigned integer used as an 'extended' sequence number.
//--------------------------------------
typedef uint64_t xtd_seq_num_t;
//--------------------------------------
// An rdbx_t is a replay database with extended range; it uses an
// xtd_seq_num_t and a bitmask of recently received indices.
//--------------------------------------
class rdbx_t
{
	xtd_seq_num_t m_index;
	v128_t m_bitmask;

public:
	rdbx_t();

	void initialize();
	int estimate_index(xtd_seq_num_t *guess, sequence_number_t s) const;
	bool check(int difference) const;
	bool add_index(int delta);

	xtd_seq_num_t get_index() const { return m_index; }
	v128_t* get_bitmask() { return &m_bitmask; }
};
//--------------------------------------
// this implementation of a replay database works as follows:
// 
// window_start is the index of the first packet in the window
// bitmask      a bit-buffer, containing the most recently entered
//              index as the leftmost bit 
//--------------------------------------
class rdb_t
{
	uint32_t m_window_start;   // packet index of the first bit in bitmask
	v128_t m_bitmask;  

public:
	rdb_t();
	
	void initialize();
	bool check(uint32_t index) const;
	bool add_index(uint32_t index);
	bool increment() { return m_window_start++ <= 0x7fffffff; }

	uint32_t getValue() const { return m_window_start; }
	v128_t* get_bitmask() { return &m_bitmask; }
};

#define rdb_bits_in_bitmask int(8 * sizeof(v128_t))   
//--------------------------------------
