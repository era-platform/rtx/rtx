﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//--------------------------------------
// key usage limits enforcement
//--------------------------------------
// based on libsrtp written by
// David A. McGrew
// Cisco Systems, Inc.
//--------------------------------------
#pragma once

#include "srtp_rdbx.h"   // for xtd_seq_num_t
//--------------------------------------
//
//--------------------------------------
enum key_event_t
{
	key_event_normal,
	key_event_soft_limit,
	key_event_hard_limit
};
//--------------------------------------
//
//--------------------------------------
enum key_state_t
{ 
	key_state_normal,
	key_state_past_soft_limit,
	key_state_expired
};
//--------------------------------------
//
//--------------------------------------
class key_limit_ctx_t
{
	xtd_seq_num_t m_num_left;
	key_state_t   m_state;

public:
	key_limit_ctx_t() : m_num_left(0), m_state(key_state_normal) { }
	key_limit_ctx_t(const key_limit_ctx_t& limit) { *this = limit; }

	key_limit_ctx_t& operator = (const key_limit_ctx_t& limit) { m_num_left = limit.m_num_left; m_state = limit.m_state; return *this; }

	bool check() const { return m_state == key_state_expired; }
	bool set(const xtd_seq_num_t s);
	key_event_t update();

	xtd_seq_num_t get_num_left() const { return m_num_left; }
	key_state_t get_state() const { return m_state; }
};
//--------------------------------------
