﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//--------------------------------------
// Common interface to authentication functions
//--------------------------------------
// based on libsrtp written by
// David A. McGrew
// Cisco Systems, Inc.
//--------------------------------------
#pragma once

#include "srtp_datatypes.h"          
//--------------------------------------
// common interface to authentication classes
//--------------------------------------
class auth_t
{
protected:
	int m_auth_id;
	int m_out_len;			// length of output tag in octets
	int m_key_len;			// length of key in octets
	int m_prefix_len;		// length of keystream prefix

public:
	auth_t(int key_len, int out_len);
	virtual ~auth_t();

	virtual bool create(const uint8_t *key, int key_len) = 0;
	virtual bool compute(const uint8_t *message, int msg_octets, uint8_t *tag, int tag_len = -1) = 0;
	virtual bool update(const uint8_t *message, int msg_octets) = 0;
	virtual bool start() = 0;

	int get_key_length() const { return m_key_len; }
	int get_tag_length() const { return m_out_len; }
	int get_prefix_length() const { return m_prefix_len; }

	int get_auth_id() const { return m_auth_id; }
};
//--------------------------------------
//
//--------------------------------------
class null_auth_ctx_t : public auth_t
{
public:
	null_auth_ctx_t(int key_len, int out_len);
	virtual ~null_auth_ctx_t();

	virtual bool create(const uint8_t *key, int key_len);
	virtual bool compute(const uint8_t *message, int msg_octets, uint8_t *tag, int tag_len = -1);
	virtual bool update(const uint8_t *message, int msg_octets);
	virtual bool start();
};
//--------------------------------------
