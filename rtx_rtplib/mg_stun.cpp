﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_stun.h"
#include "rtp_webrtc_channel.h"
#include "rtp_webrtc_subscriber.h"
#include "net_stun_message.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define LOG_PREFIX "STUN"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
mg_stun_t::mg_stun_t(rtl::Logger* log, rtp_channel_t* rtp, rtp_webrtc_subscriber_callback* callback) :
	m_log(log), m_rtp(rtp), m_callback(callback)
{
	m_id.append((rtp == nullptr) ? "-" : (const char*)rtp->getName());

	m_ice_send_ping = false;
	m_ice_last_ping_sent = 0;
	m_ice_proto_google = false;
	m_ice_username_google = false;
	m_ice_srtp_enabled = false;
	m_ice_rtp_send_enabled = false;
	m_ice_controlling = 0;

	m_ice_local_ufrag = rtl::String::empty;
	m_ice_local_pwd = rtl::String::empty;
	m_ice_remote_ufrag = rtl::String::empty;
	m_ice_remote_pwd = rtl::String::empty;

	m_ice_tie_breaker = rand();
	m_ice_tie_breaker <<= 32;
	m_ice_tie_breaker |= rand();
	m_ice_tie_breaker &= 0x7FFFFFFFFFFFFFFF;

	m_ice_stat_printed = false;

	m_change_address = false;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
mg_stun_t::~mg_stun_t()
{
	m_ice_controlling = 0;

	m_callback = nullptr;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mg_stun_t::setup_ice_params_local(const char* l_ufrag, const char* l_pswd)
{
	PLOG_STUN_WRITE(LOG_PREFIX, "%s :setup ice pinger: l_ufrag:%s l_pwd:%s", (const char*)m_id, l_ufrag, l_pswd);

	m_ice_local_ufrag = rtl::String::empty;
	m_ice_local_pwd = rtl::String::empty;

	if (l_ufrag == nullptr || l_ufrag[0] == 0)
	{
		m_ice_send_ping = false;
		m_ice_rtp_send_enabled = true;
		return;
	}

	m_ice_local_ufrag = rtl::strdup(l_ufrag);

	if (l_pswd != nullptr && l_pswd[0] != 0)
	{
		m_ice_local_pwd = rtl::strdup(l_pswd);
	}

	m_ice_send_ping = false;
	m_ice_rtp_send_enabled = false;
	m_ice_srtp_enabled = false;
}
//------------------------------------------------------------------------------
void mg_stun_t::setup_ice_params_remote(const char* r_ufrag, const char* r_pswd)
{
	PLOG_STUN_WRITE(LOG_PREFIX, "%s :setup ice pinger: r_ufrag:%s r_pwd:%s", (const char*)m_id, r_ufrag, r_pswd);

	m_ice_remote_ufrag = rtl::String::empty;
	m_ice_remote_pwd = rtl::String::empty;

	if (r_ufrag == nullptr || r_ufrag[0] == 0)
	{
		m_ice_send_ping = false;
		m_ice_rtp_send_enabled = true;
		return;
	}

	m_ice_remote_ufrag = rtl::strdup(r_ufrag);

	if (r_pswd != nullptr && r_pswd[0] != 0)
	{
		m_ice_remote_pwd = rtl::strdup(r_pswd);
	}

	m_ice_send_ping = false;
	m_ice_rtp_send_enabled = false;
	m_ice_srtp_enabled = false;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mg_stun_t::process_stun_message(const uint8_t* packet, int length, const socket_address* addr)
{
	// ответим
	m_ice_pinger_address.copy_from(addr);

	net_stun_message_t msg;
	if (msg.decode(packet, length))
	{
		PLOG_STUN_WRITE(LOG_PREFIX, "%s : %s received %s from %s", (const char*)m_id, 
			msg.is_request() ? "request" : "response", get_net_stun_msg_name(msg.getType()), m_ice_pinger_address.to_string());
		msg.log(m_log);

		if (msg.getType() == net_stun_binding_request)
		{
			send_stun_pong(&msg, &m_ice_pinger_address);
			stun_ping_received(&m_ice_pinger_address);
			send_stun_ping();
		}
		else if (msg.getType() == net_stun_binding_success_response)
		{
			m_ice_rtp_send_enabled = true;
			stun_pong_received(&m_ice_pinger_address);
		}
		else if (msg.getType() == net_stun_binding_error_response)
		{
			PLOG_STUN_WARNING(LOG_PREFIX, "%s : STUN Error message received! %s %s (%s:%s)", (const char*)m_id,
				(const char*)m_ice_local_ufrag, (const char*)m_ice_local_pwd, (const char*)m_ice_remote_ufrag, (const char*)m_ice_remote_pwd);
		}
		else
		{
			PLOG_STUN_WARNING(LOG_PREFIX, "%s : Unusable STUN message received!", (const char*)m_id);
		}
	}
	else
	{
		PLOG_STUN_WARNING(LOG_PREFIX, "%s : Invalid STUN message received", (const char*)m_id);
		return;
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mg_stun_t::send_stun_pong(const net_stun_message_t* msg, const socket_address* addr)
{
	if (msg == nullptr)
	{
		return;
	}

	net_stun_message_t response(net_stun_binding_success_response, nullptr, m_ice_local_pwd);

	response.set_transaction_id(msg->get_transaction_id());

	const net_stun_attribute_t& fingerprint = msg->get_attribute(net_stun_fingerprint);
	if (fingerprint.type == net_stun_fingerprint)
	{
		response.set_fingerprint(true);
		m_ice_srtp_enabled = true;
	}
	else // признак chrome 22
	{
		m_ice_srtp_enabled = true;
	}

	const net_stun_attribute_t& msg_int = msg->get_attribute(net_stun_message_integrity);
	if (msg_int.type == net_stun_message_integrity)
	{
		m_ice_proto_google = false;
		response.set_nointegrity(false);
		response.set_fingerprint(true);
	}

	// формируем имя пользователя по входящему пакету
	const net_stun_attribute_t& username = msg->get_attribute(net_stun_username);

	m_ice_username_google = memchr(username.data_ptr, ':', username.length) == nullptr;

	const ip_address_t* ipaddr = addr->get_ip_address();
	if (ipaddr != nullptr)
	{
		if (ipaddr->get_family() == e_ip_address_family::E_IP_ADDRESS_FAMILY_IPV4)
		{
			in_addr* addr_v4 = (in_addr*)ipaddr->get_native();
			response.add_attribute_address(net_stun_xor_mapped_address, addr->get_port(), *addr_v4);
		}
		else
		{
			in6_addr* addr_v6 = (in6_addr*)ipaddr->get_native();
			response.add_attribute_address(net_stun_xor_mapped_address, addr->get_port(), *addr_v6);
		}
	}

	// если во входящем сообщении есть атрибут controlled,
	// то мы выставляем противоположный тип.
	if (msg->has_attribute(net_stun_ice_controlled))
	{
		m_ice_controlling = 1;
	}
	// и наоборот
	else if (msg->has_attribute(net_stun_ice_controlling))
	{
		m_ice_controlling = -1;
	}
	//add_ice_controll_param_pong(&response);

	uint8_t buffer[2048];

	int len = response.encode(buffer, 2048);

	if (len <= 0)
	{
		PLOG_STUN_WARNING(LOG_PREFIX, "%s : response send failed!", (const char*)m_id);
		return;
	}

	response.log(m_log);

	if (m_rtp != nullptr)
	{
		PLOG_STUN_WRITE(LOG_PREFIX, "%s : response %s sent to %s", (const char*)m_id, get_net_stun_msg_name(response.getType()), addr->to_string());
		m_rtp->send_data(buffer, len, addr);
	}

	m_ice_send_ping = true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mg_stun_t::send_stun_ping()
{
	uint32_t current_timestamp = rtl::DateTime::getTicks();

	if (!m_ice_send_ping || ((m_ice_last_ping_sent != 0) && (current_timestamp - m_ice_last_ping_sent < 500)))
	{
		//PLOG_STUN_WARNING(LOG_PREFIX, "%s : ice ping not send!", (const char*)m_id);
		return;
	}

	m_ice_last_ping_sent = current_timestamp;

	// мне нужно формировать имя пользователя
	char username[256];
	int len = std_snprintf(username, 256, m_ice_username_google ? "%s%s" : "%s:%s", (const char*)m_ice_remote_ufrag, (const char*)m_ice_local_ufrag);
	username[len] = 0;
	net_stun_message_t request(net_stun_binding_request, username, m_ice_remote_pwd);

	request.generate_transaction_id();
	request.set_fingerprint(true);//!m_ice_proto_google);
	request.set_nointegrity(false);//m_ice_proto_google);

	add_ice_controll_param_ping(&request);

	request.add_attribute_data(net_stun_ice_use_candidate, nullptr, 0);
	request.add_attribute_int32(net_stun_ice_priority, 123434323);

	uint8_t buffer[1024];

	len = request.encode(buffer, 1024);

	if (len <= 0)
	{
		PLOG_STUN_WARNING(LOG_PREFIX, "%s : request send failed!", (const char*)m_id);
		return;
	}

	request.log(m_log);

	if (m_rtp != nullptr)
	{
		m_rtp->send_data(buffer, len, &m_ice_pinger_address);
		PLOG_STUN_WRITE(LOG_PREFIX, "%s : request %s sent to %s", (const char*)m_id, get_net_stun_msg_name(request.getType()), m_ice_pinger_address.to_string());
	}

	if (!m_ice_stat_printed)
	{
		PLOG_STUN_WRITE(LOG_PREFIX, "%s :ice pinger stat:\n\
						  send_ping:%s\r\n\
						  proto_google:%s\r\n\
						  user_google:%s\r\n\
						  srtp_enabled:%s\r\n\
						  rtp_send_enabled:%s\r\n\
						  local_ufrag:%s\r\n\
						  local_pwd:%s\r\n\
						  remote_ufrag:%s\r\n\
						  remote_pwd:%s\\r\n\
						  tie_breaker:%I64x",
						  (const char*)m_id,
						  STR_BOOL(m_ice_send_ping),
						  STR_BOOL(m_ice_proto_google),
						  STR_BOOL(m_ice_username_google),
						  STR_BOOL(m_ice_srtp_enabled),
						  STR_BOOL(m_ice_rtp_send_enabled),
						  (const char*)m_ice_local_ufrag, (const char*)m_ice_local_pwd,
						  (const char*)m_ice_remote_ufrag, (const char*)m_ice_remote_pwd,
						  m_ice_tie_breaker);

		m_ice_stat_printed = true;
	}
}
//------------------------------------------------------------------------------
void mg_stun_t::send_stun_ping(const socket_address* saddr)
{
	uint32_t current_timestamp = rtl::DateTime::getTicks();

	m_ice_last_ping_sent = current_timestamp;

	// мне нужно формировать имя пользователя
	char username[256];
	int len = std_snprintf(username, 256, m_ice_username_google ? "%s%s" : "%s:%s", (const char*)m_ice_remote_ufrag, (const char*)m_ice_local_ufrag);
	username[len] = 0;
	net_stun_message_t request(net_stun_binding_request, username, m_ice_remote_pwd);

	request.generate_transaction_id();
	request.set_fingerprint(true);//!m_ice_proto_google);
	request.set_nointegrity(false);//m_ice_proto_google);

	add_ice_controll_param_ping(&request);

	request.add_attribute_data(net_stun_ice_use_candidate, nullptr, 0);
	request.add_attribute_int32(net_stun_ice_priority, 123434323);

	uint8_t buffer[1024];

	len = request.encode(buffer, 1024);

	if (len <= 0)
	{
		PLOG_STUN_WARNING(LOG_PREFIX, "%s : request send failed!", (const char*)m_id);
		return;
	}

	request.log(m_log);

	if (m_rtp != nullptr)
	{
		m_rtp->send_data(buffer, len, saddr);
		PLOG_STUN_WRITE(LOG_PREFIX, "%s : request %s sent to %s", (const char*)m_id, get_net_stun_msg_name(request.getType()), saddr->to_string());
	}

	if (!m_ice_stat_printed)
	{
		PLOG_STUN_WRITE(LOG_PREFIX, "%s :ice pinger stat:\n\
						  send_ping:%s\r\n\
						  proto_google:%s\r\n\
						  user_google:%s\r\n\
						  srtp_enabled:%s\r\n\
						  rtp_send_enabled:%s\r\n\
						  local_ufrag:%s\r\n\
						  local_pwd:%s\r\n\
						  remote_ufrag:%s\r\n\
						  remote_pwd:%s\\r\n\
						  tie_breaker:%I64x",
						  (const char*)m_id,
						  STR_BOOL(m_ice_send_ping),
						  STR_BOOL(m_ice_proto_google),
						  STR_BOOL(m_ice_username_google),
						  STR_BOOL(m_ice_srtp_enabled),
						  STR_BOOL(m_ice_rtp_send_enabled),
						  (const char*)m_ice_local_ufrag, (const char*)m_ice_local_pwd,
						  (const char*)m_ice_remote_ufrag, (const char*)m_ice_remote_pwd,
						  m_ice_tie_breaker);

		m_ice_stat_printed = true;
	}

	m_ice_pinger_address.copy_from(saddr);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mg_stun_t::add_ice_controll_param(net_stun_message_t* msg)
{
	if (msg == nullptr)
	{
		return;
	}

	if (m_ice_controlling > 0)
	{
		msg->add_attribute_int64(net_stun_ice_controlling, m_ice_tie_breaker);
	}
	else if (m_ice_controlling < 0)
	{
		msg->add_attribute_int64(net_stun_ice_controlled, m_ice_tie_breaker);
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mg_stun_t::add_ice_controll_param_ping(net_stun_message_t* request)
{
	// выставляем атрибут контроля взависимости от входящего пинга.
	if (m_ice_controlling == 0)
	{
		if (request == nullptr)
		{
			return;
		}

		// по умолчанию.
		request->add_attribute_int64(net_stun_ice_controlled, m_ice_tie_breaker);
	}
	else
	{
		add_ice_controll_param(request);
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mg_stun_t::add_ice_controll_param_pong(net_stun_message_t* response)
{
	if (response == nullptr)
	{
		return;
	}

	// если же ни одного атрибута контроля нет, то тоже ничего не добавляем.
	if (m_ice_controlling == 0)
	{
		return;
	}

	add_ice_controll_param(response);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mg_stun_t::update_channel_name(const char* name)
{
	m_id = rtl::String::empty;
	m_id.append(name);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mg_stun_t::stun_ping_received(const socket_address* addr)
{
	if (!m_change_address)
	{
		// запомним адрес принятого ответа
		m_remote_address.copy_from(addr);

		//Изменили один раз и хватит.
		m_change_address = true;
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mg_stun_t::stun_pong_received(const socket_address* addr)
{
	if (addr == nullptr)
	{
		PLOG_STUN_WARNING(LOG_PREFIX, "%s : address is null.", (const char*)m_id);
		return;
	}

	if (m_callback == nullptr)
	{
		PLOG_STUN_WARNING(LOG_PREFIX, "%s : dtls callback is null.", (const char*)m_id);
		return;
	}

	m_callback->start_dtls(addr);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool mg_stun_t::check_change_address()
{
	return false;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
