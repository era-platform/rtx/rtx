﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtp_port_reader_manager.h"
#include "std_sys_env.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define LOG_PREFIX_LST "RTP-LIST" // Media Gateway Reader Manager
#define LOG_PREFIX "RTP-RDR" // Media Gateway Reader Manager
//------------------------------------------------------------------------------
//#define PLOG_RTP_ERROR PLOG_RTP_ERROR
//#define PLOG_RTP_WRITE PLOG_RTP_WRITE
//#define PLOG_RTP_WARNING PLOG_RTP_WARNING
//#define PLOG_RTP_ERROR PLOG_RTP_ERROR
//#define PLOG_RTP_WRITE PLOG_CALL
//#define PLOG_RTP_WARNINGPLOG_WARNING
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class mg_listener_t
{
	mg_listener_t(const mg_listener_t&);
	mg_listener_t&	operator=(const mg_listener_t&);

public:
	mg_listener_t(rtl::Logger* log);
	~mg_listener_t();

	bool				create();
	void				destroy();

	bool				start_channel_listen(rtp_channel_t* channel);
	void				stop_channel_listen(rtp_channel_t* channel);

	const uint32_t		get_count_channel_listen() const;

	const socket_io_service_t* get_io_service() const;

private:
	rtl::Logger*				m_log;

	socket_io_service_t	m_socket_io_service;

	volatile uint32_t	m_count_channel; // количество читаемых каналов
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
mg_listener_t::mg_listener_t(rtl::Logger* log) :
	m_log(log), m_socket_io_service(log)
{
	m_count_channel = 0;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
mg_listener_t::~mg_listener_t()
{
	destroy();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool mg_listener_t::create()
{
	if (!m_socket_io_service.create(1))
	{
		PLOG_RTP_ERROR(LOG_PREFIX_LST, "create -- socket_io_service create fail.");
		return false;
	}

	PLOG_RTP_WRITE(LOG_PREFIX_LST, "create -- socket_io_service created.");
	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mg_listener_t::destroy()
{
	m_socket_io_service.destroy();
	PLOG_RTP_WRITE(LOG_PREFIX_LST, "destroy -- socket_io_service destroed.");
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool mg_listener_t::start_channel_listen(rtp_channel_t* channel)
{
	PLOG_RTP_WRITE(LOG_PREFIX_LST, "start_channel_listen -- begin.");

	if (channel == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX_LST, "start_channel_listen -- channel is null.");
		return false;
	}

	int res = channel->start_io(&m_socket_io_service);
	if (res < 0)
	{
		PLOG_RTP_ERROR(LOG_PREFIX_LST, "start_channel_listen -- %s start socket io service failed.", channel->getName());
		return false;
	}

	if (res > 0)
	{
		std_interlocked_inc(&m_count_channel);
		PLOG_RTP_WRITE(LOG_PREFIX_LST, "start_channel_listen -- %s started.", channel->getName());
	}
	
	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mg_listener_t::stop_channel_listen(rtp_channel_t* channel)
{
	PLOG_RTP_WRITE(LOG_PREFIX_LST, "stop_channel_listen -- begin.");

	if (channel == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX_LST, "stop_channel_listen -- channel is null.");
		return;
	}

	PLOG_RTP_WRITE(LOG_PREFIX_LST, "stop_channel_listen -- %s stopping....", channel->getName());

	channel->stop_io();

	if (m_count_channel > 0)
	{
		std_interlocked_dec(&m_count_channel);
	}

	PLOG_RTP_WRITE(LOG_PREFIX_LST, "stop_channel_listen -- %s stopped.", channel->getName());
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const uint32_t mg_listener_t::get_count_channel_listen() const
{
	return m_count_channel;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const socket_io_service_t* mg_listener_t::get_io_service() const
{
	return &m_socket_io_service;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
mg_reader_manager_t::mg_reader_manager_t(rtl::Logger* log) :
	m_log(log)
{
	m_thread_count = 0;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
mg_reader_manager_t::~mg_reader_manager_t()
{
	destroy();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool mg_reader_manager_t::create()
{
	PLOG_RTP_WRITE(LOG_PREFIX, "create -- begin.");

	if (m_thread_count != 0)
	{
		destroy();
	}

	m_thread_count = std_get_processors_count();

	for (uint32_t i = 0; i < m_thread_count; i++)
	{
		mg_listener_t* mg_listener = NEW mg_listener_t(m_log);
		if (!mg_listener->create())
		{
			DELETEO(mg_listener);
			mg_listener = nullptr;
			PLOG_RTP_ERROR(LOG_PREFIX, "create -- create media gateway listener fail.");
			return false;
		}

		m_listenerList.add(mg_listener);
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "create -- end.(listener count: %d)", m_thread_count);
	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mg_reader_manager_t::destroy()
{
	PLOG_RTP_WRITE(LOG_PREFIX, "destroy -- begin.");

	for (int i = 0; i < m_listenerList.getCount(); i ++)
	{
		mg_listener_t* mg_listener = m_listenerList.getAt(i);
		if (mg_listener == nullptr)
		{
			continue;
		}

		mg_listener->destroy();
		DELETEO(mg_listener);
		mg_listener = nullptr;
	}

	m_listenerList.clear();

	m_thread_count = 0;

	PLOG_RTP_WRITE(LOG_PREFIX, "destroy -- end.");
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
mg_listener_t* mg_reader_manager_t::get_free_listener()
{
	PLOG_RTP_WRITE(LOG_PREFIX, "get_free_listener -- begin.");

	uint32_t count = m_listenerList.getCount();
	if ((m_thread_count == 0) || (count == 0))
	{
		PLOG_RTP_WARNING(LOG_PREFIX, "get_free_listener -- thread count 0.");
		return nullptr;
	}
	
	if (count == 1)
	{
		return m_listenerList.getAt(0);
	}

	mg_listener_t* mg_last_listener = nullptr;

	for (int i = 0; i < m_listenerList.getCount(); i++)
	{
		mg_listener_t* mg_listener = m_listenerList.getAt(i);
		if (mg_listener == nullptr)
		{
			continue;
		}

		const uint32_t last_count_listen = (mg_last_listener == nullptr) ? UINT32_MAX : mg_last_listener->get_count_channel_listen();
		const uint32_t count_listen = mg_listener->get_count_channel_listen();
		PLOG_RTP_WRITE(LOG_PREFIX, "get_free_listener --index:%d - listen count %u.", i, count_listen);

		if (count_listen < last_count_listen)
		{
			mg_last_listener = mg_listener;
			PLOG_RTP_WRITE(LOG_PREFIX, "get_free_listener --index:%d - listen selected.", i);
		}
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "get_free_listener -- end.");
	return mg_last_listener;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool mg_reader_manager_t::start_channel_listen(rtp_channel_t* channel)
{
	PLOG_RTP_WRITE(LOG_PREFIX, "start_channel_listen -- begin.");

	if (channel == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "start_channel_listen -- channel is null.");
		return false;
	}

	mg_listener_t* listener = get_free_listener();
	if (listener == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "start_channel_listen -- not free listener: (%s).", channel->getName());
		return false;
	}

	if (!listener->start_channel_listen(channel))
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "start_channel_listen -- listener not started: (%s).", channel->getName());
		return false;
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "start_channel_listen -- end. (%s started.)", channel->getName());
	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mg_reader_manager_t::stop_channel_listen(rtp_channel_t* channel)
{
	PLOG_RTP_WRITE(LOG_PREFIX, "stop_channel_listen -- begin.");

	if (channel == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "stop_channel_listen -- channel is null.");
		return;
	}

	const socket_io_service_t* service = channel->get_io_service();
	mg_listener_t* mg_listener_at_list = nullptr;
	for (int i = 0; i < m_listenerList.getCount(); i++)
	{
		mg_listener_at_list = m_listenerList.getAt(i);
		if (mg_listener_at_list == nullptr)
		{
			continue;
		}

		const socket_io_service_t* service_at_list = mg_listener_at_list->get_io_service();
		if (service == service_at_list)
		{
			break;
		}
	}

	if (mg_listener_at_list == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "stop_channel_listen -- listener not found: (%s).", channel->getName());
		return;
	}

	mg_listener_at_list->stop_channel_listen(channel);

	PLOG_RTP_WRITE(LOG_PREFIX, "stop_channel_listen -- end. (%s stoped.)", channel->getName());
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
