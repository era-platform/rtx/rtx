﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtcp_packet_AVB.h"

#if defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
#if defined(TARGET_OS_LINUX)
#include <endian.h>
#elif defined(TARGET_OS_FREEBSD)
#include <sys/endian.h>
#endif
#ifndef htonll
#define htonll(v64) htobe64(v64)
#define ntohll(v64) be64toh(v64)
#endif // htonll
#endif // Linux

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtcp_avb_packet_t::rtcp_avb_packet_t() : rtcp_base_packet_t(rtcp_type_avb_e)
{
	m_counter = 0;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtcp_avb_packet_t::~rtcp_avb_packet_t()
{
	cleanup();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtcp_avb_packet_t::cleanup()
{
	m_avb.as_timestamp = 0;
	m_avb.gmClockIdentity = 0;
	m_avb.gmPortNumber = 0;
	m_avb.gmTimeBaseIndicator = 0;
	m_avb.name = 0;
	m_avb.rtp_timestamp = 0;
	m_avb.stream_id = 0;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int rtcp_avb_packet_t::read(const uint8_t* in, int length)
{
	cleanup();

	uint32_t hs = sizeof(rtcp_header_t);
	if ((uint32_t)length < hs)
	{
		return 0;
	}

	int read = rtcp_base_packet_t::read(in, length);
	uint32_t real_length = (m_length + 1) * sizeof(uint32_t);

	if ((real_length >(uint32_t)length) || (real_length != sizeof(rtcp_avb_t)))
	{
		return 0;
	}

	rtcp_avb_t* avb = (rtcp_avb_t*)(in + read);

	m_avb.name = ntohl(avb->name);
	m_avb.gmTimeBaseIndicator = ntohs(avb->gmTimeBaseIndicator);
	m_avb.gmPortNumber = ntohs(avb->gmPortNumber);
	m_avb.gmClockIdentity = ntohll(avb->gmClockIdentity);
	m_avb.stream_id = ntohll(avb->stream_id);
	m_avb.as_timestamp = ntohl(avb->as_timestamp);
	m_avb.rtp_timestamp = ntohl(avb->rtp_timestamp);

	read += sizeof(rtcp_avb_t);

	return read;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int rtcp_avb_packet_t::write(uint8_t* out, int size)
{
	uint32_t hs = sizeof(rtcp_header_t);
	int real_length = (m_length + 1) * sizeof(uint32_t);
	int to_write = hs + real_length;
	if (to_write > size)
	{
		return 0;
	}

	int written = rtcp_base_packet_t::write(out, size);

	rtcp_avb_t* avb = (rtcp_avb_t*)(out + written);

	avb->name = htonl(m_avb.name);
	avb->gmTimeBaseIndicator = htons(m_avb.gmTimeBaseIndicator);
	avb->gmPortNumber = htons(m_avb.gmPortNumber);
	avb->gmClockIdentity = htonll(m_avb.gmClockIdentity);
	avb->stream_id = htonll(m_avb.stream_id);
	avb->as_timestamp = htonl(m_avb.as_timestamp);
	avb->rtp_timestamp = htonl(m_avb.rtp_timestamp);

	written += sizeof(rtcp_avb_t);

	return written;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int rtcp_avb_packet_t::dump(char* out, int size) const
{
	int written = rtcp_base_packet_t::dump(out, size);

	written += std_snprintf(out + written, size - written, "\tAVB : subtype: %X name:%X, stream_id:%llX\r\n", m_counter, m_avb.name, m_avb.stream_id);

	return written;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
