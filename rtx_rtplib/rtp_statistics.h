/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtp_packet.h"
#include "std_logger.h"

 //------------------------------------------------------------------------------
// статистика по направлениям
//------------------------------------------------------------------------------
struct RTX_RTPLIB_API stream_statistics_t
{
	int packets;
	int bytes;
	int lost;
	int out_of_order;
	int invalid;
	uint32_t start_time;
	uint32_t stop_time;
	uint32_t last_recv_time;
	int max_delay;
	int delayed_count;
	int filtered;
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class RTX_RTPLIB_API rtp_statistics_t
{
	rtp_statistics_t(const rtp_statistics_t&);
	rtp_statistics_t& operator=(const rtp_statistics_t&);

public:
	rtp_statistics_t(const char* name, const char* addr);
	virtual	~rtp_statistics_t();

	stream_statistics_t* get_stream_statistics_rx();
	stream_statistics_t* get_stream_statistics_tx();

	void set_id(const char* id, uint32_t id_size);
	bool check_id(const char* id, uint32_t id_size);

	void start();
	void start_rx();
	void start_tx();

	void stop();
	void stop_rx();
	void stop_tx();

	void update();
	void update_rx();
	void update_tx();

	void change_time_step(uint32_t time_step);

	void print_statistics(rtl::Logger* log);

private:
	void clear();

	void start_rx(uint32_t tick);
	void start_tx(uint32_t tick);

	void stop_rx(uint32_t tick);
	void stop_tx(uint32_t tick);

	void update_rx(uint32_t tick);
	void update_tx(uint32_t tick);

private:
	stream_statistics_t m_rx_statistics;
	stream_statistics_t m_tx_statistics;

	const static uint32_t m_name_size = 64;
	char m_context[m_name_size];
	char m_termination[m_name_size];
	char m_id[m_name_size];
	char m_addr[m_name_size];

	uint32_t m_time_step;
};
//------------------------------------------------------------------------------
