﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "std_basedef.h"
#include "std_typedef.h"
#include "std_funcdef.h"
#include "std_logger.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define RTPLIB_LOGGING	m_log && rtl::Logger::check_trace(TRF_NET)
#define PLOG_RTPLIB_WRITE		if (RTPLIB_LOGGING)	m_log->log
#define PLOG_RTPLIB_WARNING		if (RTPLIB_LOGGING)	m_log->log_warning
#define PLOG_RTPLIB_ERROR		if (RTPLIB_LOGGING)	m_log->log_error
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#if defined (TARGET_OS_WINDOWS)
#ifdef RTX_RTPLIB_EXPORTS
	#ifdef _DEBUG
#define ME_DEBUG_API __declspec(dllexport)
	#else
#define ME_DEBUG_API
	#endif
	#define RTX_RTPLIB_API __declspec(dllexport)
#else
#define RTX_RTPLIB_API __declspec(dllimport)
#endif
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
#define RTX_RTPLIB_API
#define ME_DEBUG_API
#endif
//------------------------------------------------------------------------------
// Current protocol version.
//------------------------------------------------------------------------------
#define RTP_VERSION			2
#define RTP_FIXED_HEADER	12

#define RTP_SEQ_MOD			(1<<16)
#define RTP_MAX_SDES		255      /* maximum text length for SDES */

#define RTCP_HEADER_SIZE	8		// sizeof(rtcp_header_t)
#define RTCP_HEADER_SIZE_4	2		// (RTCP_HEADER_SIZE/sizeof(uint32_t))
//------------------------------------------------------------------------------
// Big-endian mask for version, padding bit and packet type pair
//------------------------------------------------------------------------------
#define RTCP_VALID_MASK (0xc000 | 0x2000 | 0xfe)
#define RTCP_VALID_VALUE ((RTP_VERSION << 14) | RTCP_SR)
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

#pragma pack(push, 1)
struct rtp_header_t
{
	uint8_t		cc : 4, ext : 1, pad : 1, ver : 2;
	uint8_t		pt : 7, m : 1;
	uint16_t	seq;
	uint32_t	ts;
	uint32_t	ssrc;
	uint32_t	csrc[1];
};
struct rtcp_header_t
{
	uint8_t		rc : 5, pad : 1, ver : 2;
	uint8_t		pt;
	uint16_t	length;
	uint32_t	ssrc;
};
#pragma pack(pop)
//------------------------------------------------------------------------------

