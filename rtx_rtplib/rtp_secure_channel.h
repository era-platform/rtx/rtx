﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtp_channel.h"
#include <atomic>
#include "dtls_define.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class RTX_RTPLIB_API rtp_secure_channel_t : public rtp_channel_t, public i_tx_channel_event_handler
{
	rtp_secure_channel_t(const rtp_secure_channel_t&);
	rtp_secure_channel_t& operator=(const rtp_secure_channel_t&);

public:
	rtp_secure_channel_t(rtl::Logger* log);
	virtual							~rtp_secure_channel_t();

	virtual rtp_channel_type_t		getType() const;

	virtual bool					destroy();
	virtual bool					started();

	virtual int						start_io(socket_io_service_t* svc);
	virtual void					stop_io();

	virtual const ip_address_t*		get_net_interface() const;
	virtual const uint16_t			get_data_port() const;
	uint16_t						get_ctrl_port() const;

	virtual const socket_io_service_t* get_io_service() const;
	virtual void					set_io_service(socket_io_service_t* iosvc);

	virtual bool					send_packet(const rtp_packet* packet, const socket_address* saddr);
	virtual bool					send_data(const uint8_t* data, uint32_t len, const socket_address* saddr);
	virtual bool					send_ctrl(const rtcp_packet_t* packet, const socket_address* saddr);

	// для синхронизации при удалении канала
	virtual void					inc_tx_subscriber(uint32_t ssrc);
	virtual void					dec_tx_subscriber(uint32_t ssrc);

	bool							bind_data_socket(const ip_address_t* iface, uint16_t data_port);
	bool							bind_ctrl_socket(const ip_address_t* iface, uint16_t ctrl_port);
	
	i_rx_channel_event_handler*		get_event_handler();
	void							set_event_handler(i_rx_channel_event_handler* handler);

	bool							setup_srtp_policy_in(rtl::String* sdp_crypto);
//	bool							setup_srtp_policy_out(rtl::String* sdp_crypto);
	bool							setup_srtp_policy_in(const struct rtp_crypto_param_t* key_in);
	bool							setup_srtp_policy_out(const struct rtp_crypto_param_t* key_out);
	bool							setup_srtp_policy(const struct rtp_crypto_param_t* key_in); // , const struct rtp_crypto_param_t* key_out
	rtl::String						get_sdp_crypto_param_out();
	void							prepare_crypto_out_params(rtl::StringList& params);

	virtual void					print_ports(const char* pef, rtl::Logger* log);

private:
	virtual void					socket_data_available(net_socket* sock);
	virtual void					socket_data_received(net_socket* sock, const uint8_t* data, uint32_t len, const socket_address* from);

	void							rtp_data_received(const uint8_t* data, uint32_t len, const socket_address* from);
	void							rtcp_data_received(const uint8_t* data, uint32_t len, const socket_address* from);

	virtual void tx_packet_channel(/*const mg_tx_stream_t* stream, */const rtp_packet* packet, const socket_address* to);
	// отправить данные из стрима в канал.
	virtual void tx_data_channel(/*const mg_tx_stream_t* stream, */const uint8_t* data, uint32_t len, const socket_address* to);
	// отправить контрльный пакет из стрима в канал.
	virtual void tx_ctrl_channel(/*const mg_tx_stream_t* stream, */const rtcp_packet_t* packet, const socket_address* to);

	virtual void					clear_handler(uint32_t ssrc);
	
	static rtl::String					rtp_crypto_to_string(const rtp_crypto_param_t* key, int tag);

private:
	rtl::Logger*							m_log;

	volatile bool					m_started;

	ip_address_t					m_interface;
	uint16_t						m_data_port;
	uint16_t						m_ctrl_port;

	net_socket						m_data_socket;
	net_socket						m_control_socket;

	std::atomic_flag				m_lock;
	
	i_rx_channel_event_handler*		m_handler;

	socket_io_service_t*			m_socket_io_service;

	class srtp_stream_ctx_t*		m_srtp_filter_in;
	class srtp_stream_ctx_t*		m_srtp_filter_out;

	rtp_crypto_param_t				m_sdp_crypto_param_out;
	rtp_crypto_param_t				m_sdp_crypto_param_out_2;

	volatile uint32_t				m_subscriber_count;
};
//------------------------------------------------------------------------------
