﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 //--------------------------------------
// interface to libsrtp
//--------------------------------------
// based on libsrtp written by
// David A. McGrew
// Cisco Systems, Inc.
//--------------------------------------
#include "stdafx.h"
#include "rtp_packet.h"
#include "srtp.h"
#include "srtp_aes_icm.h"
#include "srtp_hmac.h"
#include "srtp_crypto.h"
//--------------------------------------
//
//--------------------------------------
#define LOG_PREFIX "srtp-ctx"
//--------------------------------------
//
//--------------------------------------
#define octets_in_rtp_header   12
#define uint32s_in_rtp_header  3
#define octets_in_rtcp_header  8
#define uint32s_in_rtcp_header 2
//--------------------------------------
//
//--------------------------------------
srtp_stream_ctx_t::srtp_stream_ctx_t(rtl::Logger* log) : m_log(log),
	m_rtp_cipher(nullptr),	m_rtp_auth(nullptr), m_rtp_services(sec_serv_none),
	m_rtcp_cipher(nullptr), m_rtcp_auth(nullptr), m_rtcp_services(sec_serv_none),
	m_direction(dir_unknown)
{
}
//--------------------------------------
//
//--------------------------------------
srtp_stream_ctx_t::~srtp_stream_ctx_t()
{
	/*
	* we use a conservative deallocation strategy - if any deallocation
	* fails, then we report that fact without trying to deallocate
	* anything else
	*/

	// deallocate cipher, if it is not the same as that in template
	if (m_rtp_cipher != nullptr)
	{
		DELETEO(m_rtp_cipher);
		m_rtp_cipher = nullptr;
	}

	// deallocate auth function, if it is not the same as that in template
	if (m_rtp_auth != nullptr)
	{
		PLOG_RTPLIB_WRITE(LOG_PREFIX, "delete crypto rtp auth %p", m_rtp_auth);

		DELETEO(m_rtp_auth);
		m_rtp_auth = nullptr;

		
	}

	// deallocate rtcp cipher, if it is not the same as that in template
	if (m_rtcp_cipher != nullptr)
	{
		DELETEO(m_rtcp_cipher);
		m_rtcp_cipher = nullptr;
	}

	// deallocate rtcp auth function, if it is not the same as that in template
	if (m_rtcp_auth != nullptr)
	{
		PLOG_RTPLIB_WRITE(LOG_PREFIX, "delete crypto rtcp auth %p", m_rtcp_auth);

		DELETEO(m_rtcp_auth);
		m_rtcp_auth = nullptr;
	}

	if (m_policy.key != nullptr)
	{
		FREE(m_policy.key);
		m_policy.key = nullptr;
	}
}
/*--------------------------------------
 * srtp_kdf_t is a key derivation context
 *
 * srtp_kdf_init(&kdf, k) initializes kdf with the key k
 * 
 * srtp_kdf_generate(&kdf, l, kl, keylen) derives the key
 * corresponding to label l and puts it into kl; the length
 * of the key in octets is provided as keylen.  this function
 * should be called once for each subkey that is derived.
 *
 * srtp_kdf_clear(&kdf) zeroizes the kdf state
 --------------------------------------*/
enum srtp_prf_label
{
	label_rtp_encryption  = 0x00,
	label_rtp_msg_auth    = 0x01,
	label_rtp_salt        = 0x02,
	label_rtcp_encryption = 0x03,
	label_rtcp_msg_auth   = 0x04,
	label_rtcp_salt       = 0x05
};
/*--------------------------------------
 * srtp_kdf_t represents a key derivation function.  The SRTP
 * default KDF is the only one implemented at present.
 --------------------------------------*/
static bool srtp_kdf_generate(aes_icm_ctx_t* kdf, srtp_prf_label label, uint8_t *key, int length)
{
	v128_t nonce;

	/* set eigth octet of nonce to <label>, set the rest of it to zero */
	v128_set_to_zero(&nonce);
	nonce.v8[7] = label;

	kdf->set_iv(&nonce);  

	/* generate keystream output */
	kdf->output(key, length);

	return true;
}
/*--------------------------------------
 *  end of key derivation functions 
 --------------------------------------*/
#define MAX_SRTP_KEY_LEN 256
//--------------------------------------
//
//--------------------------------------
bool srtp_stream_ctx_t::initialize_keys(const void *key)
{
	aes_icm_ctx_t kdf(30);
	uint8_t tmp_key[MAX_SRTP_KEY_LEN];

	/* initialize KDF state     */
	kdf.create((const uint8_t*)key);

	/* generate encryption key  */
	srtp_kdf_generate(&kdf, label_rtp_encryption, tmp_key, m_rtp_cipher->get_key_length());
	/* 
	* if the cipher in the srtp context is aes_icm, then we need
	* to generate the salt value
	*/
	if (m_rtp_cipher->get_cipher_id() == AES_128_ICM)
	{
		/* FIX!!! this is really the cipher key length; rest is salt */
		int base_key_len = 16;
		int salt_len = m_rtp_cipher->get_key_length() - base_key_len;
		/* generate encryption salt, put after encryption key */
		srtp_kdf_generate(&kdf, label_rtp_salt, tmp_key + base_key_len, salt_len);
	}

	/* initialize cipher */
	if (!m_rtp_cipher->create(tmp_key, direction_any))
	{
		/* zeroize temp buffer */
		octet_string_set_to_zero(tmp_key, MAX_SRTP_KEY_LEN);
		return false;
	}

	/* generate authentication key */
	srtp_kdf_generate(&kdf, label_rtp_msg_auth, tmp_key, m_rtp_auth->get_key_length());
	//debug_print(mod_srtp, "auth key:   %s",	octet_string_hex_string(tmp_key, get_key_length(m_rtp_auth))); 

	/* initialize auth function */
	if (!m_rtp_auth->create(tmp_key, m_rtp_auth->get_key_length()))
	{
		/* zeroize temp buffer */
		octet_string_set_to_zero(tmp_key, MAX_SRTP_KEY_LEN);
		return false;
	}

	// ...now initialize SRTCP keys

	/* generate encryption key  */
	srtp_kdf_generate(&kdf, label_rtcp_encryption, tmp_key, m_rtcp_cipher->get_key_length());
	/* 
	* if the cipher in the srtp context is aes_icm, then we need
	* to generate the salt value
	*/
	if (m_rtcp_cipher->get_cipher_id() == AES_128_ICM)
	{
		/* FIX!!! this is really the cipher key length; rest is salt */
		int base_key_len = 16;
		int salt_len = m_rtcp_cipher->get_key_length() - base_key_len;

		//debug_print(mod_srtp, "found aes_icm, generating rtcp salt", nullptr);

		/* generate encryption salt, put after encryption key */
		srtp_kdf_generate(&kdf, label_rtcp_salt, tmp_key + base_key_len, salt_len);
	}
	//debug_print(mod_srtp, "rtcp cipher key: %s", octet_string_hex_string(tmp_key, m_rtcp_cipher->get_key_length()));

	/* initialize cipher */
	if (!m_rtcp_cipher->create(tmp_key, direction_any))
	{
		/* zeroize temp buffer */
		octet_string_set_to_zero(tmp_key, MAX_SRTP_KEY_LEN);
		return false;
	}

	/* generate authentication key */
	srtp_kdf_generate(&kdf, label_rtcp_msg_auth, tmp_key, m_rtcp_auth->get_key_length());
	//debug_print(mod_srtp, "rtcp auth key:   %s", octet_string_hex_string(tmp_key, get_key_length(m_rtcp_auth))); 

	/* initialize auth function */
	if (!m_rtcp_auth->create(tmp_key, m_rtcp_auth->get_key_length()))
	{
		/* zeroize temp buffer */
		octet_string_set_to_zero(tmp_key, MAX_SRTP_KEY_LEN);
		return false;
	}

	/* clear memory then return */
	octet_string_set_to_zero(tmp_key, MAX_SRTP_KEY_LEN);  

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool srtp_stream_ctx_t::initialize(const srtp_policy_t *policy)
{
	// allocate cipher
	if ((m_rtp_cipher = crypto_alloc_cipher(policy->rtp.cipher_type, policy->rtp.cipher_key_len)) == nullptr)
		return false;
	
	// allocate auth function
	if ((m_rtp_auth = crypto_alloc_auth(policy->rtp.auth_type, policy->rtp.auth_key_len, policy->rtp.auth_tag_len)) == nullptr)
		return false;

	PLOG_RTPLIB_WRITE(LOG_PREFIX, "new crypto rtp auth %p", m_rtp_auth);
	
	//...and now the RTCP-specific initialization - first, allocate the cipher
	if ((m_rtcp_cipher = crypto_alloc_cipher(policy->rtcp.cipher_type, policy->rtcp.cipher_key_len)) == nullptr)
		return false;

	// allocate auth function
	if ((m_rtcp_auth = crypto_alloc_auth(policy->rtcp.auth_type, policy->rtcp.auth_key_len, policy->rtcp.auth_tag_len)) == nullptr)
		return false;

	PLOG_RTPLIB_WRITE(LOG_PREFIX, "new crypto rtcp auth %p", m_rtcp_auth);

	m_limit.set(0xffffffffffffLL);

	// set the security service flags
	m_rtp_services  = policy->rtp.sec_serv;
	m_rtcp_services = policy->rtcp.sec_serv;

	/*
	* set direction to unknown - this flag gets checked in srtp_protect(),
	* srtp_unprotect(), srtp_protect_rtcp(), and srtp_unprotect_rtcp(), and 
	* gets set appropriately if it is set to unknown.
	*/
	m_direction = dir_unknown;

	m_policy = *policy;

	m_policy.key = (uint8_t*)MALLOC(policy->rtp.cipher_key_len);
	memcpy(m_policy.key, policy->key, policy->rtp.cipher_key_len);

	/* initialize SRTCP replay database */
	/* initialize keys */
	return initialize_keys(policy->key);
}
//--------------------------------------
//
//--------------------------------------
void srtp_stream_ctx_t::raise_event(srtp_event_t srtp_event)
{
	if (m_event_handler)
	{
		srtp_event_data_t data;
		data.stream = this;
		data.event_type = srtp_event;
		
		m_event_handler(&data);
	}
}
//--------------------------------------
//
//--------------------------------------
bool srtp_stream_ctx_t::srtp_pack(void *rtp_hdr, int *pkt_octet_len)
{
	rtp_header_t *hdr = (rtp_header_t*)rtp_hdr;
	uint32_t *enc_start;        /* pointer to start of encrypted portion  */
	uint32_t *auth_start;       /* pointer to start of auth. portion      */
	uint32_t enc_octet_len = 0; /* number of octets in encrypted portion  */
	xtd_seq_num_t est;          /* estimated xtd_seq_num_t of *hdr        */
	int delta;                  /* delta of local pkt idx and that in hdr */
	uint8_t *auth_tag = nullptr;   /* location of auth_tag within packet     */
	int tag_len;
	int prefix_len;

	/* we assume the hdr is 32-bit aligned to start */

	/* check the packet length - it must at least contain a full header */
	if (*pkt_octet_len < octets_in_rtp_header)
		return false;

	if (m_direction != dir_srtp_sender)
	{
		if (m_direction == dir_unknown)
		{
			m_direction = dir_srtp_sender;
		}
		else
		{
			raise_event(event_ssrc_collision);
		}
	}

	/* 
	* update the key usage limit, and check it to make sure that we
	* didn't just hit either the soft limit or the hard limit, and call
	* the event handler if we hit either.
	*/
	switch (m_limit.update())
	{
	case key_event_normal:
		break;
	case key_event_soft_limit: 
		raise_event(event_key_soft_limit);
		break; 
	case key_event_hard_limit:
		raise_event(event_key_hard_limit);
		return false;
	default:
		break;
	}

	/* get tag length from stream */
	tag_len = m_rtp_auth->get_tag_length(); 

	/*
	* find starting point for encryption and length of data to be
	* encrypted - the encrypted portion starts after the rtp header
	* extension, if present; otherwise, it starts after the last csrc,
	* if any are present
	*
	* if we're not providing confidentiality, set enc_start to nullptr
	*/
	if (m_rtp_services & sec_serv_conf)
	{
		enc_start = (uint32_t *)rtp_hdr + uint32s_in_rtp_header + hdr->cc;
		if (hdr->ext)
		{
			srtp_hdr_xtnd_t *xtn_hdr = (srtp_hdr_xtnd_t *)enc_start;
			enc_start += (ntohs(xtn_hdr->length) + 1);
		}
		enc_octet_len = (uint32_t)(*pkt_octet_len - ((enc_start - (uint32_t *)rtp_hdr) << 2));
	}
	else
	{
		enc_start = nullptr;
	}

	/* 
	* if we're providing authentication, set the auth_start and auth_tag
	* pointers to the proper locations; otherwise, set auth_start to nullptr
	* to indicate that no authentication is needed
	*/
	if (m_rtp_services & sec_serv_auth)
	{
		auth_start = (uint32_t *)rtp_hdr;
		auth_tag = (uint8_t *)rtp_hdr + *pkt_octet_len;
	}
	else
	{
		auth_start = nullptr;
		auth_tag = nullptr;
	}

	/*
	* estimate the packet index using the start of the replay window   
	* and the sequence number from the header
	*/
	delta = m_rtp_rdbx.estimate_index(&est, ntohs(hdr->seq));
	if (!m_rtp_rdbx.check(delta))
		return false;  /* we've been asked to reuse an index */

	m_rtp_rdbx.add_index(delta);

	/* 
	* if we're using rindael counter mode, set nonce and seq 
	*/

	bool result = false;

	if (m_rtp_cipher->get_cipher_id() == AES_128_ICM)
	{
		v128_t iv;

		iv.v32[0] = 0;
		iv.v32[1] = hdr->ssrc;
		iv.v64[1] = be64_to_cpu(est << 16);
		result = m_rtp_cipher->set_iv(&iv);
	}
	else
	{  
		v128_t iv;

		/* otherwise, set the index to est */  
		iv.v64[0] = 0;
		iv.v64[1] = be64_to_cpu(est);
		result = m_rtp_cipher->set_iv(&iv);
	}

	if (!result)
		return false;

	/* shift est, put into network byte order */
	est = be64_to_cpu(est << 16);

	/* 
	* if we're authenticating using a universal hash, put the keystream
	* prefix into the authentication tag
	*/
	if (auth_start)
	{
		prefix_len = m_rtp_auth->get_prefix_length();
		if (prefix_len)
		{
			if (!m_rtp_cipher->output( auth_tag, prefix_len))
				return false;
			//debug_print(mod_srtp, "keystream prefix: %s", octet_string_hex_string(auth_tag, prefix_len));
		}
	}

	/* if we're encrypting, exor keystream into the message */
	if (enc_start)
	{
		if (!m_rtp_cipher->encrypt((uint8_t *)enc_start, &enc_octet_len))
			return false;
	}

	/*
	*  if we're authenticating, run authentication function and put result
	*  into the auth_tag 
	*/
	if (auth_start)
	{
		try
		{
			/* initialize auth func context */
			if (!m_rtp_auth->start())
				return false;

			/* run auth func over packet */
			if (!m_rtp_auth->update((uint8_t *)auth_start, *pkt_octet_len))
				return false;

			/* run auth func over ROC, put result into auth_tag */
			if (!m_rtp_auth->compute((uint8_t *)&est, 4, auth_tag))
				return false;
		}
		catch (rtl::Exception& eh)
		{
			LOG_ERROR(LOG_PREFIX, "srtp_pack catches exception\n%s", eh.get_message());
		}
	}

	if (auth_tag)
	{
		/* increase the packet length by the length of the auth tag */
		*pkt_octet_len += tag_len;
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool srtp_stream_ctx_t::srtp_unpack(void *srtp_hdr, int *pkt_octet_len)
{
	rtp_header_t *hdr = (rtp_header_t *)srtp_hdr;
	uint32_t *enc_start;      /* pointer to start of encrypted portion  */
	uint32_t *auth_start;     /* pointer to start of auth. portion      */
	uint32_t enc_octet_len = 0;/* number of octets in encrypted portion */
	uint8_t *auth_tag = nullptr; /* location of auth_tag within packet     */
	xtd_seq_num_t est;        /* estimated xtd_seq_num_t of *hdr        */
	int delta;                /* delta of local pkt idx and that in hdr */
	v128_t iv;
	bool result;

	uint8_t tmp_tag[SRTP_MAX_TAG_LEN];
	int tag_len, prefix_len;

	/* we assume the hdr is 32-bit aligned to start */

	/* check the packet length - it must at least contain a full header */
	if (*pkt_octet_len < octets_in_rtp_header)
	{
		return false;
	}

	/* estimate packet index from seq. num. in header */
	delta = m_rtp_rdbx.estimate_index(&est, ntohs(hdr->seq));

	/* check replay database */
	if (!m_rtp_rdbx.check(delta))
	{
		return false;
	}

	/* get tag length from stream */
	tag_len = m_rtp_auth->get_tag_length(); 

	/* 
	* set the cipher's IV properly, depending on whatever cipher we
	* happen to be using
	*/

	if (m_rtp_cipher->get_cipher_id() == AES_128_ICM)
	{
		/* aes counter mode */
		iv.v32[0] = 0;
		iv.v32[1] = hdr->ssrc;  /* still in network order */
		iv.v64[1] = be64_to_cpu(est << 16);
		result = m_rtp_cipher->set_iv(&iv);
	}
	else
	{  
		/* no particular format - set the iv to the pakcet index */  
		iv.v64[0] = 0;
		iv.v64[1] = be64_to_cpu(est);
		result = m_rtp_cipher->set_iv(&iv);
	}

	if (!result)
	{
		return false;
	}

	/* shift est, put into network byte order */
	est = be64_to_cpu(est << 16);

	/*
	* find starting point for decryption and length of data to be
	* decrypted - the encrypted portion starts after the rtp header
	* extension, if present; otherwise, it starts after the last csrc,
	* if any are present
	*
	* if we're not providing confidentiality, set enc_start to nullptr
	*/
	if (m_rtp_services & sec_serv_conf)
	{
		enc_start = (uint32_t *)srtp_hdr + uint32s_in_rtp_header + hdr->cc;
		if (hdr->ext)
		{
			srtp_hdr_xtnd_t *xtn_hdr = (srtp_hdr_xtnd_t *)enc_start;
			enc_start += (ntohs(xtn_hdr->length) + 1);
		}  
		
		enc_octet_len = (uint32_t)(*pkt_octet_len - tag_len - ((enc_start - (uint32_t *)srtp_hdr) << 2));
	}
	else
	{
		enc_start = nullptr;
	}

	/* 
	* if we're providing authentication, set the auth_start and auth_tag
	* pointers to the proper locations; otherwise, set auth_start to nullptr
	* to indicate that no authentication is needed
	*/
	if (m_rtp_services & sec_serv_auth)
	{
		auth_start = (uint32_t *)srtp_hdr;
		auth_tag = (uint8_t *)srtp_hdr + *pkt_octet_len - tag_len;
	}
	else
	{
		auth_start = nullptr;
		auth_tag = nullptr;
	} 

	/*
	* if we expect message authentication, run the authentication
	* function and compare the result with the value of the auth_tag
	*/
	if (auth_start)
	{
		/* 
		* if we're using a universal hash, then we need to compute the
		* keystream prefix for encrypting the universal hash output
		*
		* if the keystream prefix length is zero, then we know that
		* the authenticator isn't using a universal hash function
		*/  
		if (m_rtp_auth->get_prefix_length() != 0)
		{
			prefix_len = m_rtp_auth->get_prefix_length();
			if (!m_rtp_cipher->output( tmp_tag, prefix_len))
			{
				return false;
			}
		} 

		/* initialize auth func context */
		if (!m_rtp_auth->start())
		{
			return false;
		}

		/* now compute auth function over packet */
		m_rtp_auth->update((uint8_t *)auth_start, *pkt_octet_len - tag_len);

		/* run auth func over ROC, then write tmp tag */
		if (!m_rtp_auth->compute((uint8_t *)&est, 4, tmp_tag))
		{
			return false;   
		}

		if (memcmp(tmp_tag, auth_tag, tag_len) != 0)
		{
			return false;
		}

		/*if (!octet_string_is_eq(tmp_tag, auth_tag, tag_len))
			return false;*/
	}

	/* 
	* update the key usage limit, and check it to make sure that we
	* didn't just hit either the soft limit or the hard limit, and call
	* the event handler if we hit either.
	*/
	switch (m_limit.update())
	{
	case key_event_normal:
		break;
	case key_event_soft_limit: 
		raise_event(event_key_soft_limit);
		break; 
	case key_event_hard_limit:
		raise_event(event_key_hard_limit);
		return false;
	default:
		break;
	}

	/* if we're encrypting, add keystream into ciphertext */
	if (enc_start)
	{
		if (!m_rtp_cipher->encrypt((uint8_t *)enc_start, &enc_octet_len))
			return false;
	}

	/* 
	* verify that stream is for received traffic - this check will
	* detect SSRC collisions, since a stream that appears in both
	* srtp_protect() and srtp_unprotect() will fail this test in one of
	* those functions.
	*
	* we do this check *after* the authentication check, so that the
	* latter check will catch any attempts to fool us into thinking
	* that we've got a collision
	*/
	if (m_direction != dir_srtp_receiver)
	{
		if (m_direction == dir_unknown)
		{
			m_direction = dir_srtp_receiver;
		}
		else
		{
			raise_event(event_ssrc_collision);
		}
	}

	/* 
	* the message authentication function passed, so add the packet
	* index into the replay database 
	*/
	m_rtp_rdbx.add_index(delta);

	/* decrease the packet length by the length of the auth tag */
	*pkt_octet_len -= tag_len;

	return true;
}
//--------------------------------------
// secure rtcp functions
//--------------------------------------
bool srtp_stream_ctx_t::srtcp_pack(void *rtcp_hdr, int *pkt_octet_len)
{
	rtcp_header_t *hdr = (rtcp_header_t*)rtcp_hdr;
	uint32_t *enc_start;      /* pointer to start of encrypted portion  */
	uint32_t *auth_start;     /* pointer to start of auth. portion      */
	uint32_t *trailer;        /* pointer to start of trailer            */
	uint32_t enc_octet_len = 0;/* number of octets in encrypted portion */
	uint8_t *auth_tag = nullptr; /* location of auth_tag within packet     */

	int tag_len;
	int prefix_len;
	uint32_t seq_num;

	/* we assume the hdr is 32-bit aligned to start */
	/*
	* look up ssrc in srtp_stream list, and process the packet with 
	* the appropriate stream.  if we haven't seen this stream before,
	* there's only one key for this srtp_session, and the cipher
	* supports key-sharing, then we assume that a new stream using
	* that key has just started up
	*/

	/* 
	* verify that stream is for sending traffic - this check will
	* detect SSRC collisions, since a stream that appears in both
	* srtp_protect() and srtp_unprotect() will fail this test in one of
	* those functions.
	*/
	if (m_direction != dir_srtp_sender)
	{
		if (m_direction == dir_unknown)
		{
			m_direction = dir_srtp_sender;
		}
		else
		{
			raise_event(event_ssrc_collision);
		}
	}  

	/* get tag length from stream context */
	tag_len = m_rtcp_auth->get_tag_length(); 

	/*
	* set encryption start and encryption length - if we're not
	* providing confidentiality, set enc_start to nullptr
	*/
	enc_start = (uint32_t *)rtcp_hdr + uint32s_in_rtcp_header;
	enc_octet_len = *pkt_octet_len - octets_in_rtcp_header;

	/* all of the packet, except the header, gets encrypted */
	/* NOTE: hdr->length is not usable - it refers to only the first
	RTCP report in the compound packet! */
	/* NOTE: trailer is 32-bit aligned because RTCP 'packets' are always
	multiples of 32-bits (RFC 3550 6.1) */
	trailer = (uint32_t *) ((char *)enc_start + enc_octet_len);

	if (m_rtcp_services & sec_serv_conf)
	{
		*trailer = htonl(SRTCP_E_BIT);     /* set encrypt bit */    
	}
	else
	{
		enc_start = nullptr;
		enc_octet_len = 0;
		/* 0 is network-order independant */
		*trailer = 0x00000000;     /* set encrypt bit */    
	}

	/* 
	* set the auth_start and auth_tag pointers to the proper locations
	* (note that srtpc *always* provides authentication, unlike srtp)
	*/
	/* Note: This would need to change for optional mikey data */
	auth_start = (uint32_t *)rtcp_hdr;
	auth_tag = (uint8_t *)rtcp_hdr + *pkt_octet_len + sizeof(srtcp_trailer_t);

	/* 
	* check sequence number for overruns, and copy it into the packet
	* if its value isn't too big
	*/
	if (!m_rtcp_rdb.increment())
		return false;

	seq_num = m_rtcp_rdb.getValue();
	*trailer |= htonl(seq_num);

	/* 
	* if we're using rindael counter mode, set nonce and seq 
	*/
	bool result;

	if (m_rtcp_cipher->get_cipher_id() == AES_128_ICM)
	{
		v128_t iv;

		iv.v32[0] = 0;
		iv.v32[1] = hdr->ssrc;  /* still in network order! */
		iv.v32[2] = htonl(seq_num >> 16);
		iv.v32[3] = htonl(seq_num << 16);
		result = m_rtcp_cipher->set_iv(&iv);
	}
	else
	{  
		v128_t iv;

		/* otherwise, just set the index to seq_num */  
		iv.v32[0] = 0;
		iv.v32[1] = 0;
		iv.v32[2] = 0;
		iv.v32[3] = htonl(seq_num);
		result = m_rtcp_cipher->set_iv(&iv);
	}
	
	if (!result)
		return false;

	/* 
	* if we're authenticating using a universal hash, put the keystream
	* prefix into the authentication tag
	*/

	/* if auth_start is non-nullptr, then put keystream into tag  */
	if (auth_start)
	{
		/* put keystream prefix into auth_tag */
		prefix_len = m_rtcp_auth->get_prefix_length();    

		if (!m_rtcp_cipher->output(auth_tag, prefix_len))
			return false;
	}

	/* if we're encrypting, exor keystream into the message */
	if (enc_start)
	{
		if (!m_rtcp_cipher->encrypt((uint8_t *)enc_start, &enc_octet_len))
			return false;
	}

	/* initialize auth func context */
	m_rtcp_auth->start();

	/* 
	* run auth func over packet (including trailer), and write the
	* result at auth_tag 
	*/
	if (!m_rtcp_auth->compute((uint8_t *)auth_start, (*pkt_octet_len) + sizeof(srtcp_trailer_t), auth_tag))
		return false;

	/* increase the packet length by the length of the auth tag and seq_num*/
	*pkt_octet_len += (tag_len + sizeof(srtcp_trailer_t));

	return true;  
}
//--------------------------------------
//
//--------------------------------------
bool srtp_stream_ctx_t::srtcp_unpack(void *srtcp_hdr, int *pkt_octet_len)
{
	rtcp_header_t* hdr = (rtcp_header_t*)srtcp_hdr;
	uint32_t *enc_start;      /* pointer to start of encrypted portion  */
	uint32_t *auth_start;     /* pointer to start of auth. portion      */
	uint32_t *trailer;        /* pointer to start of trailer            */
	uint32_t enc_octet_len = 0;/* number of octets in encrypted portion */
	uint8_t *auth_tag = nullptr; /* location of auth_tag within packet     */
	uint8_t tmp_tag[SRTP_MAX_TAG_LEN];

	int tag_len;
	int prefix_len;
	uint32_t seq_num;

	/* get tag length from stream context */
	tag_len = m_rtcp_auth->get_tag_length(); 

	/*
	* set encryption start, encryption length, and trailer
	*/
	enc_octet_len = *pkt_octet_len - (octets_in_rtcp_header + tag_len + sizeof(srtcp_trailer_t));
	/* index & E (encryption) bit follow normal data.  hdr->len
	is the number of words (32-bit) in the normal packet minus 1 */
	/* This should point trailer to the word past the end of the
	normal data. */
	/* This would need to be modified for optional mikey data */
	/*
	* NOTE: trailer is 32-bit aligned because RTCP 'packets' are always
	*	 multiples of 32-bits (RFC 3550 6.1)
	*/
	trailer = (uint32_t *)((char *)srtcp_hdr + *pkt_octet_len - (tag_len + sizeof(srtcp_trailer_t)));
	if (*((uint8_t *) trailer) & SRTCP_E_BYTE_BIT)
	{
		enc_start = (uint32_t *)srtcp_hdr + uint32s_in_rtcp_header;
	}
	else
	{
		enc_octet_len = 0;
		enc_start = nullptr; /* this indicates that there's no encryption */
	}

	/* 
	* set the auth_start and auth_tag pointers to the proper locations
	* (note that srtcp *always* uses authentication, unlike srtp)
	*/
	auth_start = (uint32_t *)srtcp_hdr;
	auth_tag = (uint8_t *)srtcp_hdr + *pkt_octet_len - tag_len;

	/* 
	* check the sequence number for replays
	*/
	/* this is easier than dealing with bitfield access */
	seq_num = ntohl(*trailer) & SRTCP_INDEX_MASK;
//	debug_print(mod_srtp, "srtcp index: %x", seq_num);
	if (!m_rtcp_rdb.check(seq_num))
		return false;

	/* 
	* if we're using aes counter mode, set nonce and seq 
	*/
	bool result;
	if (m_rtcp_cipher->get_cipher_id() == AES_128_ICM)
	{
		v128_t iv;

		iv.v32[0] = 0;
		iv.v32[1] = hdr->ssrc; /* still in network order! */
		iv.v32[2] = htonl(seq_num >> 16);
		iv.v32[3] = htonl(seq_num << 16);
		result = m_rtcp_cipher->set_iv(&iv);
	}
	else
	{  
		v128_t iv;

		/* otherwise, just set the index to seq_num */  
		iv.v32[0] = 0;
		iv.v32[1] = 0;
		iv.v32[2] = 0;
		iv.v32[3] = htonl(seq_num);
		result = m_rtcp_cipher->set_iv(&iv);

	}
	
	if (!result)
		return false;

	/* initialize auth func context */
	m_rtcp_auth->start();

	/* run auth func over packet, put result into tmp_tag */
	if (!m_rtcp_auth->compute((uint8_t *)auth_start, *pkt_octet_len - tag_len, tmp_tag))
		return false;

	/* compare the tag just computed with the one in the packet */
//	debug_print(mod_srtp, "srtcp tag from packet:    %s", octet_string_hex_string(auth_tag, tag_len));
	if (!octet_string_is_eq(tmp_tag, auth_tag, tag_len))
		return false;

	/* 
	* if we're authenticating using a universal hash, put the keystream
	* prefix into the authentication tag
	*/
	prefix_len = m_rtcp_auth->get_prefix_length();
	if (prefix_len)
	{
		if (!m_rtcp_cipher->output(auth_tag, prefix_len))
			return false;
	}

	/* if we're decrypting, exor keystream into the message */
	if (enc_start)
	{
		if (!m_rtcp_cipher->encrypt((uint8_t *)enc_start, &enc_octet_len))
			return false;
	}

	/* decrease the packet length by the length of the auth tag and seq_num*/
	*pkt_octet_len -= (tag_len + sizeof(srtcp_trailer_t));

	/* 
	* verify that stream is for received traffic - this check will
	* detect SSRC collisions, since a stream that appears in both
	* srtp_protect() and srtp_unprotect() will fail this test in one of
	* those functions.
	*
	* we do this check *after* the authentication check, so that the
	* latter check will catch any attempts to fool us into thinking
	* that we've got a collision
	*/
	if (m_direction != dir_srtp_receiver)
	{
		if (m_direction == dir_unknown)
		{
			m_direction = dir_srtp_receiver;
		}
		else
		{
			raise_event(event_ssrc_collision);
		}
	}

	/* we've passed the authentication check, so add seq_num to the rdb */
	m_rtcp_rdb.add_index(seq_num);

	return true;
}
/*--------------------------------------
 * the default policy - provides a convenient way for callers to use
 * the default security policy
 * 
 * this policy is that defined in the current SRTP internet draft.
 *-------------------------------------- 
 * NOTE: cipher_key_len is really key len (128 bits) plus salt len
 *  (112 bits)
 --------------------------------------*/

//--------------------------------------
// There are hard-coded 16's for base_key_len in the key generation code 
//--------------------------------------
void crypto_policy_set_rtp_default(crypto_policy_t *p)
{
	p->cipher_type     = AES_128_ICM;           
	p->cipher_key_len  = 30;                /* default 128 bits per RFC 3711 */
	p->auth_type       = HMAC_SHA1;             
	p->auth_key_len    = 20;                /* default 160 bits per RFC 3711 */
	p->auth_tag_len    = 10;                /* default 80 bits per RFC 3711 */
	p->sec_serv        = sec_serv_conf_and_auth;
}
//--------------------------------------
//
//--------------------------------------
void crypto_policy_set_rtcp_default(crypto_policy_t *p)
{
	p->cipher_type     = AES_128_ICM;           
	p->cipher_key_len  = 30;                 /* default 128 bits per RFC 3711 */
	p->auth_type       = HMAC_SHA1;             
	p->auth_key_len    = 20;                 /* default 160 bits per RFC 3711 */
	p->auth_tag_len    = 10;                 /* default 80 bits per RFC 3711 */
	p->sec_serv        = sec_serv_conf_and_auth;
}
//--------------------------------------
//
//--------------------------------------
void crypto_policy_set_aes_cm_128_hmac_sha1_32(crypto_policy_t *p)
{
	/*
	* corresponds to draft-ietf-mmusic-sdescriptions-12.txt
	*
	* note that this crypto policy is intended for SRTP, but not SRTCP
	*/

	p->cipher_type     = AES_128_ICM;           
	p->cipher_key_len  = 30;                /* 128 bit key, 112 bit salt */
	p->auth_type       = HMAC_SHA1;             
	p->auth_key_len    = 20;                /* 160 bit key               */
	p->auth_tag_len    = 4;                 /* 32 bit tag                */
	p->sec_serv        = sec_serv_conf_and_auth;
}
//--------------------------------------
//
//--------------------------------------
void crypto_policy_set_aes_cm_128_null_auth(crypto_policy_t *p)
{
	/*
	* corresponds to draft-ietf-mmusic-sdescriptions-12.txt
	*
	* note that this crypto policy is intended for SRTP, but not SRTCP
	*/

	p->cipher_type     = AES_128_ICM;           
	p->cipher_key_len  = 30;                /* 128 bit key, 112 bit salt */
	p->auth_type       = NULL_AUTH;             
	p->auth_key_len    = 0; 
	p->auth_tag_len    = 0; 
	p->sec_serv        = sec_serv_conf;
}
//--------------------------------------
//
//--------------------------------------
void crypto_policy_set_null_cipher_hmac_sha1_80(crypto_policy_t *p)
{
	/*
	* corresponds to draft-ietf-mmusic-sdescriptions-12.txt
	*/

	p->cipher_type     = NULL_CIPHER;           
	p->cipher_key_len  = 0;
	p->auth_type       = HMAC_SHA1;             
	p->auth_key_len    = 20; 
	p->auth_tag_len    = 10; 
	p->sec_serv        = sec_serv_auth;
}
//--------------------------------------
// dtls keying for srtp 
//--------------------------------------
bool crypto_policy_set_from_profile_for_rtp(crypto_policy_t *policy, srtp_profile_t profile)
{
	/* set SRTP policy from the SRTP profile in the key set */
	switch(profile)
	{
	case srtp_profile_aes128_cm_sha1_80:
		crypto_policy_set_aes_cm_128_hmac_sha1_80(policy);
		crypto_policy_set_aes_cm_128_hmac_sha1_80(policy);
		break;
	case srtp_profile_aes128_cm_sha1_32:
		crypto_policy_set_aes_cm_128_hmac_sha1_32(policy);
		crypto_policy_set_aes_cm_128_hmac_sha1_80(policy);
		break;
	case srtp_profile_null_sha1_80:
		crypto_policy_set_null_cipher_hmac_sha1_80(policy);
		crypto_policy_set_null_cipher_hmac_sha1_80(policy);
		break;
		/* the following profiles are not (yet) supported */
	case srtp_profile_null_sha1_32:
	default:
		return false;
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool crypto_policy_set_from_profile_for_rtcp(crypto_policy_t *policy, srtp_profile_t profile)
{
	/* set SRTP policy from the SRTP profile in the key set */
	switch(profile)
	{
	case srtp_profile_aes128_cm_sha1_80:
		crypto_policy_set_aes_cm_128_hmac_sha1_80(policy);
		break;
	case srtp_profile_aes128_cm_sha1_32:
		crypto_policy_set_aes_cm_128_hmac_sha1_80(policy);
		break;
	case srtp_profile_null_sha1_80:
		crypto_policy_set_null_cipher_hmac_sha1_80(policy);
		break;
		/* the following profiles are not (yet) supported */
	case srtp_profile_null_sha1_32:
	default:
		return false;
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
void append_salt_to_key(uint8_t *key, uint32_t bytes_in_key, uint8_t *salt, uint32_t bytes_in_salt)
{
	memcpy(key + bytes_in_key, salt, bytes_in_salt);
}
//--------------------------------------
//
//--------------------------------------
uint32_t srtp_profile_get_master_key_length(srtp_profile_t profile)
{
	switch(profile)
	{
	case srtp_profile_aes128_cm_sha1_80:
		return 16;
		break;
	case srtp_profile_aes128_cm_sha1_32:
		return 16;
		break;
	case srtp_profile_null_sha1_80:
		return 16;
		break;
		/* the following profiles are not (yet) supported */
	case srtp_profile_null_sha1_32:
	default:
		return 0;  /* indicate error by returning a zero */
	}
}
//--------------------------------------
//
//--------------------------------------
uint32_t srtp_profile_get_master_salt_length(srtp_profile_t profile)
{
	switch(profile)
	{
	case srtp_profile_aes128_cm_sha1_80:
		return 14;
		break;
	case srtp_profile_aes128_cm_sha1_32:
		return 14;
		break;
	case srtp_profile_null_sha1_80:
		return 14;
		break;
		/* the following profiles are not (yet) supported */
	case srtp_profile_null_sha1_32:
	default:
		return 0;  /* indicate error by returning a zero */
	}
}
//--------------------------------------
