﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_rtplib.h"
#include "std_array_templates.h"
#include "mg_stun.h"
#include "dtls.h"

 //------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
class i_rx_channel_event_handler;
class srtp_stream_ctx_t;
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
class rtp_webrtc_subscriber_t
{
	rtp_webrtc_subscriber_t(const rtp_webrtc_subscriber_t&);
	rtp_webrtc_subscriber_t& operator=(const rtp_webrtc_subscriber_t&);

public:
	rtp_webrtc_subscriber_t(rtl::Logger* log, i_rx_channel_event_handler* handler, uint32_t ssrc);
	virtual	~rtp_webrtc_subscriber_t();

	uint32_t get_ssrc() const;

	i_rx_channel_event_handler* get_handler() const;

	bool setup_srtp_policy(const rtp_crypto_param_t* key_in, const rtp_crypto_param_t* key_out);
	srtp_stream_ctx_t* get_srtp_filter_in() const;
	srtp_stream_ctx_t* get_srtp_filter_out() const;

private:
	rtl::Logger* m_log;
	uint32_t m_ssrc;

	i_rx_channel_event_handler* m_handler;
	
	srtp_stream_ctx_t* m_srtp_filter_in;
	srtp_stream_ctx_t* m_srtp_filter_out;
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
typedef rtl::PonterArrayT<rtp_webrtc_subscriber_t> rtp_subscriber_list_t;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
struct rtp_webrtc_subscriber_callback
{
	virtual void start_dtls(const socket_address* addr) = 0;
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class rtp_webrtc_subscriber_manager_t : public rtp_webrtc_subscriber_callback
{
	rtp_webrtc_subscriber_manager_t(const rtp_webrtc_subscriber_manager_t&);
	rtp_webrtc_subscriber_manager_t& operator = (const rtp_webrtc_subscriber_manager_t&);
	rtp_webrtc_subscriber_manager_t(rtp_webrtc_subscriber_manager_t&&);
	rtp_webrtc_subscriber_manager_t& operator = (rtp_webrtc_subscriber_manager_t&&);

public:
	rtp_webrtc_subscriber_manager_t();
	~rtp_webrtc_subscriber_manager_t();

	bool init(rtl::Logger* log, rtp_channel_t* rtp);
	const rtp_webrtc_subscriber_t* get_subscriber(uint32_t ssrc) const;
	bool create_subscriber(uint32_t ssrc, i_rx_channel_event_handler* handler);
	void release_subscriber(const rtp_webrtc_subscriber_t* subscriber);
	void release_subscriber(uint32_t ssrc);
	void process_stun_message(const uint8_t* packet, int length, const socket_address* addr);
	void process_dtls_message(const uint8_t* packet, int length, const socket_address* addr);
	dtls* get_dtls();
	virtual void start_dtls(const socket_address* addr);
	mg_stun_t* get_stun();
	void update_channel_name(const char* name);

private:
	bool setup_srtp_policy(const rtp_crypto_param_t* key_in, const rtp_crypto_param_t* key_out);

private:
	rtl::Logger* m_log;
	rtp_subscriber_list_t m_subscribers;
	mg_stun_t* m_stun;
	dtls* m_dtls;
	volatile bool m_already_setup_srtp_policy;
};
//-----------------------------------------------
