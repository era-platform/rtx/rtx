﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtp_webrtc_subscriber.h"
#include "rtp_channel.h"
#include "dtls.h"
#include "srtp.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define LOG_PREFIX "webrtc"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtp_webrtc_subscriber_t::rtp_webrtc_subscriber_t(rtl::Logger* log, i_rx_channel_event_handler* handler, uint32_t ssrc)
{
	m_ssrc = ssrc;
	m_handler = handler;
	m_log = log;

	m_srtp_filter_in = nullptr;
	m_srtp_filter_out = nullptr;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtp_webrtc_subscriber_t::~rtp_webrtc_subscriber_t()
{
	m_ssrc = 0;
	m_handler = nullptr;
	m_log = nullptr;

	if (m_srtp_filter_in != nullptr)
	{
		DELETEO(m_srtp_filter_in);
		m_srtp_filter_in = nullptr;
	}
	if (m_srtp_filter_out != nullptr)
	{
		DELETEO(m_srtp_filter_out);
		m_srtp_filter_out = nullptr;
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t rtp_webrtc_subscriber_t::get_ssrc() const
{
	return m_ssrc;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
i_rx_channel_event_handler* rtp_webrtc_subscriber_t::get_handler() const
{
	return m_handler;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
static bool set_crypto_params_to_profile(const rtp_crypto_param_t* key, srtp_policy_t* policy);
static bool is_crypto_policy_equal(const srtp_policy_t* p1, const srtp_policy_t* p2);
//------------------------------------------------------------------------------
bool rtp_webrtc_subscriber_t::setup_srtp_policy(const rtp_crypto_param_t* key_in, const rtp_crypto_param_t* key_out)
{
	srtp_policy_t policy_in;
	srtp_policy_t policy_out;

	PLOG_RTP_WRITE(LOG_PREFIX, "setup_srtp_policy -- ssrc:%u.", m_ssrc);

	set_crypto_params_to_profile(key_in, &policy_in);
	set_crypto_params_to_profile(key_out, &policy_out);

	srtp_stream_ctx_t* stream;

	if (m_srtp_filter_in != nullptr && !is_crypto_policy_equal(&policy_in, m_srtp_filter_in->get_policy()))
	{
		DELETEO(m_srtp_filter_in);
		m_srtp_filter_in = nullptr;
	}
	if (m_srtp_filter_out != nullptr && !is_crypto_policy_equal(&policy_out, m_srtp_filter_out->get_policy()))
	{
		DELETEO(m_srtp_filter_out);
		m_srtp_filter_out = nullptr;
	}

	if (m_srtp_filter_in == nullptr)
	{
		stream = NEW srtp_stream_ctx_t(m_log);
		set_crypto_params_to_profile(key_in, &policy_in);
		if (!stream->initialize(&policy_in))
		{
			DELETEO(stream);
			//PLOG_RTP_WRITE(L"rtp", L"%S : -----> setup SRTP failed", m_id);
			return false;
		}

		m_srtp_filter_in = stream;
		//PLOG_RTP_WRITE(L"rtp", L"%S : -----> enabling srtp IN", m_id);
	}

	//policy.key = (uint8_t*)key_out;

	if (m_srtp_filter_out == nullptr)
	{
		stream = NEW srtp_stream_ctx_t(m_log);
		//set_crypto_params_to_profile(key_out, &policy);
		stream->initialize(&policy_out);
		m_srtp_filter_out = stream;

		//PLOG_RTP_WRITE(L"rtp", L"%S : -----> enabling srtp OUT", m_id);
	}

	return true;
}
//------------------------------------------------------------------------------
srtp_stream_ctx_t* rtp_webrtc_subscriber_t::get_srtp_filter_in() const
{
	return m_srtp_filter_in;
}
//------------------------------------------------------------------------------
srtp_stream_ctx_t* rtp_webrtc_subscriber_t::get_srtp_filter_out() const
{
	return m_srtp_filter_out;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtp_webrtc_subscriber_manager_t::rtp_webrtc_subscriber_manager_t()
{
	m_stun = nullptr;
	m_dtls = nullptr;

	m_already_setup_srtp_policy = false;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
rtp_webrtc_subscriber_manager_t::~rtp_webrtc_subscriber_manager_t()
{
	m_subscribers.clear();

	if (m_stun != nullptr)
	{
		DELETEO(m_stun);
		m_stun = nullptr;
	}

	if (m_dtls != nullptr)
	{
		m_dtls->dispose();
		DELETEO(m_dtls);
		m_dtls = nullptr;
	}
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
const rtp_webrtc_subscriber_t* rtp_webrtc_subscriber_manager_t::get_subscriber(uint32_t ssrc) const
{
	for (int i = 0; i < m_subscribers.getCount(); i++)
	{
		const rtp_webrtc_subscriber_t* subscriber = m_subscribers.getAt(i);
		if (subscriber == nullptr)
		{
			continue;
		}

		if (subscriber->get_ssrc() == ssrc)
		{
			return subscriber;
		}
	}

	return nullptr;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtp_webrtc_subscriber_manager_t::init(rtl::Logger* log, rtp_channel_t* rtp)
{
	m_log = log;

	if (m_stun != nullptr)
	{
		DELETEO(m_stun);
		m_stun = nullptr;
	}

	if (m_dtls != nullptr)
	{
		m_dtls->dispose();
		DELETEO(m_dtls);
		m_dtls = nullptr;
	}

	m_dtls = NEW dtls(log, rtp);

	m_stun = NEW mg_stun_t(log, rtp, this);

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_webrtc_subscriber_manager_t::update_channel_name(const char* name)
{
    if (m_stun != nullptr)
    {
        m_stun->update_channel_name(name);
    }
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
mg_stun_t* rtp_webrtc_subscriber_manager_t::get_stun()
{
	return m_stun;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
dtls* rtp_webrtc_subscriber_manager_t::get_dtls()
{
	return m_dtls;
}
//------------------------------------------------------------------------------
void rtp_webrtc_subscriber_manager_t::start_dtls(const socket_address* addr)
{
	if (addr == nullptr)
	{
		//PLOG_RTPLIB_ERROR(LOG_PREFIX, "%s : address is null.", (const char*)m_id);
		return;
	}

	if (m_dtls == nullptr)
	{
		return;
	}

	if (!m_dtls->setup_dtls())
	{
		return;
	}

	m_dtls->do_dtls(nullptr, 0, addr->get_ip_address(), addr->get_port());

	if (m_dtls->chek_crypto())
	{
		setup_srtp_policy(m_dtls->get_crypto_in(), m_dtls->get_crypto_out());
	}
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool rtp_webrtc_subscriber_manager_t::setup_srtp_policy(const rtp_crypto_param_t* key_in, const rtp_crypto_param_t* key_out)
{
	if (m_already_setup_srtp_policy)
	{
		return true;
	}

	for (int i = 0; i < m_subscribers.getCount(); i++)
	{
		rtp_webrtc_subscriber_t* subscriber_at_list = m_subscribers.getAt(i);
		if (subscriber_at_list == nullptr)
		{
			continue;
		}

		if (!subscriber_at_list->setup_srtp_policy(key_in, key_out))
		{
			continue;
		}
	}

	m_already_setup_srtp_policy = true;

	return true;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool rtp_webrtc_subscriber_manager_t::create_subscriber(uint32_t ssrc, i_rx_channel_event_handler* handler)
{
	const rtp_webrtc_subscriber_t* subscriber = get_subscriber(ssrc);
	if (subscriber != nullptr)
	{
		release_subscriber(subscriber);
	}

	rtp_webrtc_subscriber_t* new_subscriber = NEW rtp_webrtc_subscriber_t(m_log, handler, ssrc);
	
	if ((m_dtls != nullptr) && (m_dtls->chek_crypto()))
	{
		new_subscriber->setup_srtp_policy(m_dtls->get_crypto_in(), m_dtls->get_crypto_out());
	}

	m_subscribers.add(new_subscriber);

	return true;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void rtp_webrtc_subscriber_manager_t::release_subscriber(uint32_t ssrc)
{
	for (int i = 0; i < m_subscribers.getCount(); i++)
	{
		rtp_webrtc_subscriber_t* subscriber_at_list = m_subscribers.getAt(i);
		if (subscriber_at_list == nullptr)
		{
			continue;
		}

		if (subscriber_at_list->get_ssrc() == ssrc)
		{
			m_subscribers.removeAt(i);
			break;
		}
	}
}
//------------------------------------------------------------------------------
void rtp_webrtc_subscriber_manager_t::release_subscriber(const rtp_webrtc_subscriber_t* subscriber)
{
	if (subscriber == nullptr)
	{
		return;
	}

	release_subscriber(subscriber->get_ssrc());
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void rtp_webrtc_subscriber_manager_t::process_stun_message(const uint8_t* packet, int length, const socket_address* addr)
{
	if (m_stun == nullptr)
	{
		return;
	}

	m_stun->process_stun_message(packet, length, addr);
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void rtp_webrtc_subscriber_manager_t::process_dtls_message(const uint8_t* packet, int length, const socket_address* addr)
{
	if (m_dtls == nullptr)
	{
		return;
	}

	if (!m_dtls->setup_dtls())
	{
		return;
	}

	m_dtls->do_dtls((void*)packet, length, addr->get_ip_address(), addr->get_port());

	if (m_dtls->chek_crypto())
	{
		setup_srtp_policy(m_dtls->get_crypto_in(), m_dtls->get_crypto_out());
	}
}
//--------------------------------------
//
//--------------------------------------
static bool is_crypto_policy_equal(const crypto_policy_t* p1, const crypto_policy_t* p2)
{
	return memcmp(p1, p2, sizeof(crypto_policy_t)) == 0;
}
static bool is_crypto_policy_equal(const srtp_policy_t* p1, const srtp_policy_t* p2)
{
	/*
	struct srtp_stream_policy_t
	{
	uint32_t ssrc;				// The SSRC value of stream.
	crypto_policy_t rtp;        // SRTP crypto policy.
	crypto_policy_t rtcp;       // SRTCP crypto policy.
	uint8_t *key;				// Pointer to the SRTP master key for.
	};
	*/

	if (p1->ssrc != p2->ssrc)
		return false;

	if (!is_crypto_policy_equal(&p1->rtp, &p2->rtp))
		return false;

	if (!is_crypto_policy_equal(&p1->rtcp, &p2->rtcp))
		return false;

	if (memcmp(p1->key, p2->key, p1->rtp.cipher_key_len) != 0)
		return false;

	return true;
}
static bool set_crypto_params_to_profile(const rtp_crypto_param_t* key, srtp_policy_t* policy)
{
	policy->ssrc = 0;
	policy->key = (uint8_t*)key->key;

	if (key->crypto_suite == srtp_profile_aes128_cm_sha1_80)
	{
		crypto_policy_set_aes_cm_128_hmac_sha1_80(&policy->rtp);
		crypto_policy_set_aes_cm_128_hmac_sha1_80(&policy->rtcp);
	}
	else if (key->crypto_suite == srtp_profile_aes128_cm_sha1_32)
	{
		crypto_policy_set_aes_cm_128_hmac_sha1_32(&policy->rtp);
		crypto_policy_set_aes_cm_128_hmac_sha1_32(&policy->rtcp);
	}

	if (key->null_rtp_auth && key->null_rtp_cipher)
	{
		policy->rtp.auth_type = NULL_AUTH;
		policy->rtp.auth_key_len = 0;
		policy->rtp.auth_tag_len = 0;

		policy->rtp.cipher_type = NULL_CIPHER;
		policy->rtp.cipher_key_len = 0;
		policy->rtp.sec_serv = sec_serv_none;
	}
	else if (key->null_rtp_cipher)
	{
		policy->rtp.cipher_type = NULL_CIPHER;
		policy->rtp.cipher_key_len = 0;
		policy->rtp.sec_serv = sec_serv_auth;
	}
	else if (key->null_rtp_auth)
	{
		policy->rtp.auth_type = NULL_AUTH;
		policy->rtp.auth_key_len = 0;
		policy->rtp.auth_tag_len = 0;
		policy->rtp.sec_serv = sec_serv_conf;
	}

	if (key->null_rtcp_cipher)
	{
		policy->rtcp.cipher_type = NULL_CIPHER;
		policy->rtcp.cipher_key_len = 0;
		policy->rtcp.sec_serv = sec_serv_auth;
	}

	return true;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
