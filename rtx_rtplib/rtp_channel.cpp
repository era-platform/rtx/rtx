﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtp_channel.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
i_tx_channel_event_handler::i_tx_channel_event_handler()
{
}
i_tx_channel_event_handler::~i_tx_channel_event_handler()
{
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
i_rx_channel_event_handler::i_rx_channel_event_handler()
{
}
i_rx_channel_event_handler::~i_rx_channel_event_handler()
{
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtp_channel_t::rtp_channel_t(rtl::Logger* log) : m_log(log)
{
	m_death_time = 0;

	memset(m_name, 0, m_size_name);
	int len = std_snprintf((char*)m_name, m_size_name, "channel: - ");
	m_name[len] = 0;

	m_next_channel = nullptr;

	memset(&m_stat, 0, sizeof(m_stat));
	strcpy(m_stat.name, "dummy");

	m_prev_stat = m_next_stat = nullptr;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtp_channel_t::~rtp_channel_t()
{
	m_next_channel = nullptr;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_channel_t::set_time_destroy(uint64_t time)
{
	m_death_time = time;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint64_t rtp_channel_t::get_time_destroy()
{
	return m_death_time;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const uint8_t* rtp_channel_t::getName() const
{
	return m_name;
}
//------------------------------------------------------------------------------
void rtp_channel_t::set_name(const char* name)
{
	//STR_COPY((char*)m_name, name);
	strncpy((char*)m_name, name, sizeof(m_name)-1);
	m_name[sizeof(m_name)-1] = 0;
	strncpy(m_stat.name, (char*)m_name, 63);
	m_stat.name[63] = 0;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_channel_t::set_next_channel(rtp_channel_t* channel)
{
	m_next_channel = channel;
}
//------------------------------------------------------------------------------
rtp_channel_t* rtp_channel_t::get_next_channel()
{
	return m_next_channel;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
static bool m_stat_enabled = false;
static rtl::Mutex m_stat_lock;
static volatile uint32_t m_stat_count = 0;
static volatile rtp_channel_t* m_stat_list = nullptr;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_channel_t::insert_stat(rtp_channel_t* channel)
{
	if (!m_stat_enabled)
		return;

	channel->m_next_stat = channel->m_prev_stat = nullptr;

	channel->m_stat.timeStart = rtl::DateTime::getTicks();

	rtl::MutexLock lock(m_stat_lock);

	if (m_stat_list)
	{
		channel->m_next_stat = m_stat_list;
		m_stat_list->m_prev_stat = channel;
	}
	
	m_stat_list = channel;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_channel_t::remove_stat(rtp_channel_t* channel)
{
	if (!m_stat_enabled)
		return;

	if (channel == m_stat_list)
	{
		m_stat_list = channel->m_next_stat;
		m_stat_list->m_prev_stat = nullptr;
	}
	else
	{
		channel->m_prev_stat = channel->m_next_stat;
		if (channel->m_next_stat != nullptr)
		{
			channel->m_next_stat = channel->m_prev_stat;
		}
	}

	channel->m_next_stat = channel->m_prev_stat = nullptr;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_channel_t::print_statistics(rtl::FileStream& stream)
{
	if (!m_stat_enabled)
		return;

	int stat_count = m_stat_count + 1024;
	int i = 0;
	rtl::ArrayT<rtp_channel_stat_t> stat(stat_count);

	{
		rtl::MutexLock lock(m_stat_lock);

		rtp_channel_t* next = (rtp_channel_t*)m_stat_list;

		while (next != nullptr && i++ < stat_count)
		{
			stat.add(next->m_stat);
			next = (rtp_channel_t*)next->m_next_stat;
		}
	}

	if (i > 0)
	{
		uint32_t curTime = rtl::DateTime::getTicks();
		stream.write("\r\n", 2);
		for (int j = 0; j < stat.getCount(); j++)
		{
			rtp_channel_stat_t& s = stat[j];
			int left = curTime - s.timeStart;
			float rbt = left > 0 ? ((float)s.rx_bytes / (float)left) * 8.0f : 0.0f;
			float tbt = left > 0 ? ((float)s.tx_bytes / (float)left) * 8.0f : 0.0f;
			stream.writeFormat("%s rx: % 9u packets % 9u bytes % 8.3f KBit/sec tx: % 9u packets % 9u bytes % 8.3f KBit/sec\r\n",
				s.name, s.rx_packets, s.rx_bytes, rbt,
				        s.tx_packets, s.tx_bytes, tbt);
		}
		stream.write("---\r\n", 5);
	}
}
//------------------------------------------------------------------------------
