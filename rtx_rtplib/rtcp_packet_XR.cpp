/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtcp_packet_XR.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const char* get_rtcp_xr_type_name(rtcp_xr_block_type_t type)
{
	
	switch (type)
	{
	case rtcp_xr_block_type_t::rtcp_xr_loss_rle_e:
		return "Loss RLE Report Block";
	}

	return "XR undefined";
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtcp_xr_packet_t::rtcp_xr_packet_t() : rtcp_base_packet_t(rtcp_type_avb_e)
{
	m_counter = 0;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtcp_xr_packet_t::~rtcp_xr_packet_t()
{
	cleanup();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtcp_xr_packet_t::cleanup()
{
	
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int rtcp_xr_packet_t::read(const uint8_t* in, int length)
{
	cleanup();

	int read = rtcp_base_packet_t::read(in, length);
	if (((m_length + 1) * sizeof(uint32_t)) > (uint32_t)length)
	{
		return 0;
	}

	rtcp_xr_header_t* h = (rtcp_xr_header_t*)(in + read);
	m_type = (rtcp_xr_block_type_t)h->bt; 
	uint16_t bl = ntohs(h->block_length);
	switch (m_type)
	{
	case rtcp_xr_block_type_t::rtcp_xr_loss_rle_e:
		read += read_loss_rle(in + read, bl);
		break;
	case rtcp_xr_block_type_t::rtcp_xr_dublicate_rle_e:
		read += read_duplicate_rle(in + read, bl);
		break;
	case rtcp_xr_block_type_t::rtcp_xr_receipt_times_e:
		read += read_receipt_times(in + read, bl);
		break;
	case rtcp_xr_block_type_t::rtcp_xr_reference_time_e:
		read += read_reference_time(in + read, bl);
		break;
	case rtcp_xr_block_type_t::rtcp_xr_dlrr_e:
		read += read_dlrr(in + read, bl);
		break;
	case rtcp_xr_block_type_t::rtcp_xr_stat_sum_e:
		read += read_stat_sum(in + read, bl);
		break;
	case rtcp_xr_block_type_t::rtcp_xr_voip_metrics_e:
		read += read_voip_metrics(in + read, bl);
		break;
	}

	return read;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int rtcp_xr_packet_t::write(uint8_t* out, int size)
{
	int to_write = (m_length + 1) * sizeof(uint32_t);
	if (to_write > size)
	{
		return 0;
	}

	int written = rtcp_base_packet_t::write(out, size);

	switch (m_type)
	{
	case rtcp_xr_block_type_t::rtcp_xr_loss_rle_e:
		written += write_loss_rle(out + written, size - written);
		break;
	case rtcp_xr_block_type_t::rtcp_xr_dublicate_rle_e:
		written += write_duplicate_rle(out + written, size - written);
		break;
	case rtcp_xr_block_type_t::rtcp_xr_receipt_times_e:
		written += write_receipt_times(out + written, size - written);
		break;
	case rtcp_xr_block_type_t::rtcp_xr_reference_time_e:
		written += write_reference_time(out + written, size - written);
		break;
	case rtcp_xr_block_type_t::rtcp_xr_dlrr_e:
		written += write_dlrr(out + written, size - written);
		break;
	case rtcp_xr_block_type_t::rtcp_xr_stat_sum_e:
		written += write_stat_sum(out + written, size - written);
		break;
	case rtcp_xr_block_type_t::rtcp_xr_voip_metrics_e:
		written += write_voip_metrics(out + written, size - written);
		break;
	}

	return written;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int rtcp_xr_packet_t::dump(char* out, int size) const
{
	int written = rtcp_base_packet_t::dump(out, size);
	int sub_blocks_size = 0;
	int sub_blocks_count = 0;

	written += std_snprintf(out + written, size - written, "\tXR HEADER : type: %s\r\n", get_rtcp_xr_type_name(m_type));

	switch (m_type)
	{
	case rtcp_xr_block_type_t::rtcp_xr_loss_rle_e:
		written += std_snprintf(out + written, size - written, "\t    ssrc:%X,\r\n", m_entry.loss_rle.ssrc);
		break;
	case rtcp_xr_block_type_t::rtcp_xr_dublicate_rle_e:
		written += std_snprintf(out + written, size - written, "\t    ssrc:%X,\r\n", m_entry.duplicate_rle.ssrc);
		break;
	case rtcp_xr_block_type_t::rtcp_xr_receipt_times_e:
		written += std_snprintf(out + written, size - written, "\t    ssrc:%X,\r\n", m_entry.receipt_times.ssrc);
		break;
	case rtcp_xr_block_type_t::rtcp_xr_dlrr_e:
		sub_blocks_size = m_entry.dlrr.block_length - sizeof(rtcp_xr_header_t);
		sub_blocks_count = sub_blocks_size / sizeof(rtcp_xr_dlrr_sub_block_t);
		for (int i = 0; i < sub_blocks_count; i++)
		{
			written += std_snprintf(out + written, size - written, "\t   block: %d - ssrc:%X,\r\n", i, m_entry.dlrr.sub_blocks[i].ssrc);
		}
		break;
	case rtcp_xr_block_type_t::rtcp_xr_stat_sum_e:
		written += std_snprintf(out + written, size - written, "\t    ssrc:%X,\r\n", m_entry.stat_sum.ssrc);
		break;
	case rtcp_xr_block_type_t::rtcp_xr_voip_metrics_e:
		written += std_snprintf(out + written, size - written, "\t    ssrc:%X,\r\n", m_entry.voip_metrics.ssrc);
		break;
	}

	return written;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtcp_xr_block_type_t rtcp_xr_packet_t::getType()
{
	return m_type;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtcp_xr_loss_rle_t* rtcp_xr_packet_t::get_loss_rle()
{
	if (m_type == rtcp_xr_loss_rle_e)
	{
		return (rtcp_xr_loss_rle_t*)&m_entry;
	}

	return nullptr;
}
//------------------------------------------------------------------------------
rtcp_xr_duplicate_rle_t* rtcp_xr_packet_t::get_duplicate_rle()
{
	if (m_type == rtcp_xr_dublicate_rle_e)
	{
		return (rtcp_xr_duplicate_rle_t*)&m_entry;
	}

	return nullptr;
}
//------------------------------------------------------------------------------
rtcp_xr_receipt_times_t* rtcp_xr_packet_t::get_receipt_times()
{
	if (m_type == rtcp_xr_receipt_times_e)
	{
		return (rtcp_xr_receipt_times_t*)&m_entry;
	}

	return nullptr;
}
//------------------------------------------------------------------------------
rtcp_xr_dlrr_t* rtcp_xr_packet_t::get_dlrr()
{
	if (m_type == rtcp_xr_dlrr_e)
	{
		return (rtcp_xr_dlrr_t*)&m_entry;
	}

	return nullptr;
}
//------------------------------------------------------------------------------
rtcp_xr_stat_sum_t* rtcp_xr_packet_t::get_stat_sum()
{
	if (m_type == rtcp_xr_stat_sum_e)
	{
		return (rtcp_xr_stat_sum_t*)&m_entry;
	}

	return nullptr;
}
//------------------------------------------------------------------------------
rtcp_xr_voip_metrics_t* rtcp_xr_packet_t::get_voip_metrics()
{
	if (m_type == rtcp_xr_voip_metrics_e)
	{
		return (rtcp_xr_voip_metrics_t*)&m_entry;
	}

	return nullptr;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int rtcp_xr_packet_t::read_loss_rle(const uint8_t* in, int length)
{
	const rtcp_xr_loss_rle_t* h = (const rtcp_xr_loss_rle_t*)in;

	int read = 0;

	m_entry.loss_rle.block_type = h->block_type;
	m_entry.loss_rle.t = h->t;
	m_entry.loss_rle.block_length = ntohs(h->block_length);

	m_entry.loss_rle.ssrc = ntohl(h->ssrc);
	m_entry.loss_rle.begin_seq = h->begin_seq;
	m_entry.loss_rle.end_seq = h->end_seq;

	int head_size = sizeof(rtcp_xr_header_t) + sizeof(m_entry.loss_rle.ssrc) + sizeof(m_entry.loss_rle.begin_seq) + sizeof(m_entry.loss_rle.end_seq);
	int real_size = (m_entry.loss_rle.block_length + 1) * sizeof(uint32_t);
	int chuncks_size = real_size - head_size;
	if ((chuncks_size + head_size) > length)
	{
		return 0;
	}
	int chuncks_count = chuncks_size / sizeof(uint16_t);
	if (chuncks_count > RTCP_XR_MAX_RLE_CHUNKS)
	{
		return 0;
	}

	read += head_size;
	memcpy(m_entry.loss_rle.chunks, h->chunks, chuncks_size);
	read += chuncks_size;

	return read;
}
//------------------------------------------------------------------------------
int rtcp_xr_packet_t::read_duplicate_rle(const uint8_t* in, int length)
{
	const rtcp_xr_duplicate_rle_t* h = (const rtcp_xr_duplicate_rle_t*)in;

	int read = 0;

	m_entry.duplicate_rle.block_type = h->block_type;
	m_entry.duplicate_rle.t = h->t;
	m_entry.duplicate_rle.block_length = ntohs(h->block_length);

	m_entry.duplicate_rle.ssrc = ntohl(h->ssrc);
	m_entry.duplicate_rle.begin_seq = h->begin_seq;
	m_entry.duplicate_rle.end_seq = h->end_seq;

	int head_size = sizeof(rtcp_xr_header_t) + sizeof(m_entry.duplicate_rle.ssrc) + sizeof(m_entry.duplicate_rle.begin_seq) + sizeof(m_entry.duplicate_rle.end_seq);
	int real_size = (m_entry.duplicate_rle.block_length + 1) * sizeof(uint32_t);
	int chuncks_size = real_size - head_size;
	if ((chuncks_size + head_size) > length)
	{
		return 0;
	}
	int chuncks_count = chuncks_size / sizeof(uint16_t);
	if (chuncks_count > RTCP_XR_MAX_RLE_CHUNKS)
	{
		return 0;
	}

	read += head_size;
	memcpy(m_entry.duplicate_rle.chunks, h->chunks, chuncks_size);
	read += chuncks_size;

	return read;
}
//------------------------------------------------------------------------------
int rtcp_xr_packet_t::read_receipt_times(const uint8_t* in, int length)
{
	const rtcp_xr_receipt_times_t* h = (const rtcp_xr_receipt_times_t*)in;

	int read = 0;

	m_entry.receipt_times.block_type = h->block_type;
	m_entry.receipt_times.t = h->t;
	m_entry.receipt_times.block_length = ntohs(h->block_length);

	m_entry.receipt_times.ssrc = ntohl(h->ssrc);
	m_entry.receipt_times.begin_seq = h->begin_seq;
	m_entry.receipt_times.end_seq = h->end_seq;

	int head_size = sizeof(rtcp_xr_header_t) + sizeof(m_entry.receipt_times.ssrc) + sizeof(m_entry.receipt_times.begin_seq) + sizeof(m_entry.receipt_times.end_seq);
	int real_size = (m_entry.receipt_times.block_length + 1) * sizeof(uint32_t);
	int times_size = real_size - head_size;
	if ((times_size + head_size) > length)
	{
		return 0;
	}
	int times_count = times_size / sizeof(uint32_t);
	if (times_count > RTCP_XR_MAX_RECEIPT_TIMES)
	{
		return 0;
	}

	read += head_size;
	memcpy(m_entry.receipt_times.receipt_times, h->receipt_times, times_size);
	read += times_size;

	return read;
}
//------------------------------------------------------------------------------
int rtcp_xr_packet_t::read_reference_time(const uint8_t* in, int length)
{
	if (sizeof(rtcp_xr_reference_time_t) > (uint32_t)length)
	{
		return 0;
	}

	const rtcp_xr_reference_time_t* h = (const rtcp_xr_reference_time_t*)in;

	m_entry.reference_time.block_type = h->block_type;
	m_entry.reference_time.block_length = ntohs(h->block_length);

	m_entry.reference_time.most = h->most;
	m_entry.reference_time.least = h->least;

	return sizeof(rtcp_xr_reference_time_t);
}
//------------------------------------------------------------------------------
int rtcp_xr_packet_t::read_dlrr(const uint8_t* in, int length)
{
	const rtcp_xr_dlrr_t* h = (const rtcp_xr_dlrr_t*)in;

	int read = 0;

	m_entry.dlrr.block_type = h->block_type;
	m_entry.dlrr.block_length = ntohs(h->block_length);
	read += sizeof(rtcp_xr_header_t);

	int real_size = (m_entry.dlrr.block_length + 1) * sizeof(uint32_t);
	int sub_blocks_size = real_size - sizeof(rtcp_xr_header_t);
	if ((sub_blocks_size + sizeof(rtcp_xr_header_t)) > (uint32_t)length)
	{
		return 0;
	}
	int sub_blocks_count = sub_blocks_size / sizeof(rtcp_xr_dlrr_sub_block_t);
	if (sub_blocks_count > RTCP_XR_MAX_DLRR_BLOCKS)
	{
		return 0;
	}

	for (int i = 0; i < sub_blocks_count; i++)
	{
		const rtcp_xr_dlrr_sub_block_t* block = (const rtcp_xr_dlrr_sub_block_t*)(in + read);
		m_entry.dlrr.sub_blocks[i].ssrc = ntohl(block->ssrc);
		m_entry.dlrr.sub_blocks[i].last_rr = block->last_rr;
		m_entry.dlrr.sub_blocks[i].dlrr = block->dlrr;
		read += sizeof(rtcp_xr_dlrr_sub_block_t);
	}

	return read;
}
//------------------------------------------------------------------------------
int rtcp_xr_packet_t::read_stat_sum(const uint8_t* in, int length)
{
	if (sizeof(rtcp_xr_stat_sum_t) > (uint32_t)length)
	{
		return 0;
	}

	const rtcp_xr_stat_sum_t* h = (const rtcp_xr_stat_sum_t*)in;

	m_entry.stat_sum.block_type = h->block_type;
	m_entry.stat_sum.l = h->l;
	m_entry.stat_sum.d = h->d;
	m_entry.stat_sum.j = h->j;
	m_entry.stat_sum.t = h->t;
	m_entry.stat_sum.block_length = ntohs(h->block_length);

	m_entry.stat_sum.ssrc = ntohl(h->ssrc);
	m_entry.stat_sum.begin_seq = h->begin_seq;
	m_entry.stat_sum.end_seq = h->end_seq;
	m_entry.stat_sum.lost_packets = h->lost_packets;
	m_entry.stat_sum.dup_packets = h->dup_packets;
	m_entry.stat_sum.min_jitter = h->min_jitter;
	m_entry.stat_sum.max_jitter = h->max_jitter;
	m_entry.stat_sum.mean_jitter = h->mean_jitter;
	m_entry.stat_sum.dev_jitter = h->dev_jitter;
	m_entry.stat_sum.min_ttl_or_hl = h->min_ttl_or_hl;
	m_entry.stat_sum.max_ttl_or_hl = h->max_ttl_or_hl;
	m_entry.stat_sum.mean_ttl_or_hl = h->mean_ttl_or_hl;
	m_entry.stat_sum.dev_ttl_or_hl = h->dev_ttl_or_hl;
	
	return sizeof(rtcp_xr_stat_sum_t);
}
//------------------------------------------------------------------------------
int rtcp_xr_packet_t::read_voip_metrics(const uint8_t* in, int length)
{
	if (sizeof(rtcp_xr_voip_metrics_t) > (uint32_t)length)
	{
		return 0;
	}

	const rtcp_xr_voip_metrics_t* h = (const rtcp_xr_voip_metrics_t*)in;

	m_entry.voip_metrics.block_type = h->block_type;
	m_entry.voip_metrics.block_length = ntohs(h->block_length);
	m_entry.voip_metrics.ssrc = ntohl(h->ssrc);
	m_entry.voip_metrics.loss_rate = h->loss_rate;
	m_entry.voip_metrics.discard_rate = h->discard_rate;
	m_entry.voip_metrics.burst_density = h->burst_density;
	m_entry.voip_metrics.gap_density = h->gap_density;
	m_entry.voip_metrics.burst_duration = h->burst_duration;
	m_entry.voip_metrics.gap_duration = h->gap_duration;
	m_entry.voip_metrics.round_trip_delay = h->round_trip_delay;
	m_entry.voip_metrics.end_system_delay = h->end_system_delay;
	m_entry.voip_metrics.signal_level = h->signal_level;
	m_entry.voip_metrics.noise_level = h->noise_level;
	m_entry.voip_metrics.RERL = h->RERL;
	m_entry.voip_metrics.Gmin = h->Gmin;
	m_entry.voip_metrics.r_factor = h->r_factor;
	m_entry.voip_metrics.ext_r_factor = h->ext_r_factor;
	m_entry.voip_metrics.mos_lq = h->mos_lq;
	m_entry.voip_metrics.mos_cq = h->mos_cq;
	m_entry.voip_metrics.rx_config = h->rx_config;
	m_entry.voip_metrics.jb_nominal = h->jb_nominal;
	m_entry.voip_metrics.jb_maximum = h->jb_maximum;
	m_entry.voip_metrics.jb_abs_max = h->jb_abs_max;
	
	return sizeof(rtcp_xr_voip_metrics_t);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int rtcp_xr_packet_t::write_loss_rle(uint8_t* out, int size)
{
	rtcp_xr_loss_rle_t* loss_rle = (rtcp_xr_loss_rle_t*)(out);
	int real_size = (m_entry.loss_rle.block_length + 1) * sizeof(uint32_t);
	if (real_size > size)
	{
		return 0;
	}

	loss_rle->block_type = m_entry.loss_rle.block_type;
	loss_rle->t = m_entry.loss_rle.t;
	loss_rle->block_length = htons(m_entry.loss_rle.block_length);

	loss_rle->ssrc = htonl(m_entry.loss_rle.ssrc);
	loss_rle->begin_seq = m_entry.loss_rle.begin_seq;
	loss_rle->end_seq = m_entry.loss_rle.end_seq;

	int head_size = sizeof(rtcp_xr_header_t) + sizeof(m_entry.loss_rle.ssrc) + sizeof(m_entry.loss_rle.begin_seq) + sizeof(m_entry.loss_rle.end_seq);

	int chuncks_size = real_size - head_size;
	memcpy(loss_rle->chunks, m_entry.loss_rle.chunks, chuncks_size);

	return head_size + chuncks_size;
}
//------------------------------------------------------------------------------
int rtcp_xr_packet_t::write_duplicate_rle(uint8_t* out, int size)
{
	rtcp_xr_duplicate_rle_t* duplicate_rle = (rtcp_xr_duplicate_rle_t*)(out);
	int real_size = (m_entry.duplicate_rle.block_length + 1) * sizeof(uint32_t);
	if (real_size > size)
	{
		return 0;
	}

	duplicate_rle->block_type = m_entry.duplicate_rle.block_type;
	duplicate_rle->t = m_entry.duplicate_rle.t;
	duplicate_rle->block_length = htons(m_entry.duplicate_rle.block_length);

	duplicate_rle->ssrc = htonl(m_entry.duplicate_rle.ssrc);
	duplicate_rle->begin_seq = m_entry.duplicate_rle.begin_seq;
	duplicate_rle->end_seq = m_entry.duplicate_rle.end_seq;

	int head_size = sizeof(rtcp_xr_header_t) + sizeof(m_entry.duplicate_rle.ssrc) + sizeof(m_entry.duplicate_rle.begin_seq) + sizeof(m_entry.duplicate_rle.end_seq);

	int chuncks_size = real_size - head_size;
	memcpy(duplicate_rle->chunks, m_entry.duplicate_rle.chunks, chuncks_size);

	return head_size + chuncks_size;
}
//------------------------------------------------------------------------------
int rtcp_xr_packet_t::write_receipt_times(uint8_t* out, int size)
{
	rtcp_xr_receipt_times_t* receipt_times = (rtcp_xr_receipt_times_t*)(out);
	int real_size = (m_entry.receipt_times.block_length + 1) * sizeof(uint32_t);
	if (real_size > size)
	{
		return 0;
	}

	receipt_times->block_type = m_entry.receipt_times.block_type;
	receipt_times->t = m_entry.receipt_times.t;
	receipt_times->block_length = htons(m_entry.receipt_times.block_length);

	receipt_times->ssrc = htonl(m_entry.receipt_times.ssrc);
	receipt_times->begin_seq = m_entry.receipt_times.begin_seq;
	receipt_times->end_seq = m_entry.receipt_times.end_seq;

	int head_size = sizeof(rtcp_xr_header_t) + sizeof(m_entry.receipt_times.ssrc) + sizeof(m_entry.receipt_times.begin_seq) + sizeof(m_entry.receipt_times.end_seq);

	int times_size = real_size - head_size;
	memcpy(receipt_times->receipt_times, m_entry.receipt_times.receipt_times, times_size);

	return head_size + times_size;
}
//------------------------------------------------------------------------------
int rtcp_xr_packet_t::write_reference_time(uint8_t* out, int size)
{
	if (sizeof(rtcp_xr_reference_time_t) > (size_t)size)
	{
		return 0;
	}

	rtcp_xr_reference_time_t* reference_time = (rtcp_xr_reference_time_t*)(out);

	reference_time->block_type = m_entry.reference_time.block_type;
	reference_time->block_length = htons(m_entry.reference_time.block_length);

	reference_time->most = m_entry.reference_time.most;
	reference_time->least = m_entry.reference_time.least;

	return sizeof(rtcp_xr_reference_time_t);
}
//------------------------------------------------------------------------------
int rtcp_xr_packet_t::write_dlrr(uint8_t* out, int size)
{
	rtcp_xr_dlrr_t* dlrr = (rtcp_xr_dlrr_t*)(out);
	int real_size = (m_entry.dlrr.block_length + 1) * sizeof(uint32_t);
	if (real_size > size)
	{
		return 0;
	}

	dlrr->block_type = m_entry.dlrr.block_type;
	dlrr->block_length = htons(m_entry.dlrr.block_length);
	int write = sizeof(rtcp_xr_header_t);

	int sub_blocks_size = real_size - sizeof(rtcp_xr_header_t);
	int sub_blocks_count = sub_blocks_size / sizeof(rtcp_xr_dlrr_sub_block_t);
	for (int i = 0; i < sub_blocks_count; i++)
	{
		rtcp_xr_dlrr_sub_block_t* block = (rtcp_xr_dlrr_sub_block_t*)(out + write);
		block->ssrc = htonl(m_entry.dlrr.sub_blocks[i].ssrc);
		block->last_rr = m_entry.dlrr.sub_blocks[i].last_rr;
		block->dlrr = m_entry.dlrr.sub_blocks[i].dlrr;

		write += sizeof(rtcp_xr_dlrr_sub_block_t);
	}

	return write;
}
//------------------------------------------------------------------------------
int rtcp_xr_packet_t::write_stat_sum(uint8_t* out, int size)
{
	if (sizeof(rtcp_xr_stat_sum_t) > (size_t)size)
	{
		return 0;
	}

	rtcp_xr_stat_sum_t* stat_sum = (rtcp_xr_stat_sum_t*)(out);
	
	stat_sum->block_type = m_entry.stat_sum.block_type;
	stat_sum->l = m_entry.stat_sum.l;
	stat_sum->d = m_entry.stat_sum.d;
	stat_sum->j = m_entry.stat_sum.j;
	stat_sum->t = m_entry.stat_sum.t;
	stat_sum->block_length = htons(m_entry.stat_sum.block_length);

	stat_sum->ssrc = htonl(m_entry.stat_sum.ssrc);
	stat_sum->begin_seq = m_entry.stat_sum.begin_seq;
	stat_sum->end_seq = m_entry.stat_sum.end_seq;
	stat_sum->lost_packets = m_entry.stat_sum.lost_packets;
	stat_sum->dup_packets = m_entry.stat_sum.dup_packets;
	stat_sum->min_jitter = m_entry.stat_sum.min_jitter;
	stat_sum->max_jitter = m_entry.stat_sum.max_jitter;
	stat_sum->mean_jitter = m_entry.stat_sum.mean_jitter;
	stat_sum->dev_jitter = m_entry.stat_sum.dev_jitter;
	stat_sum->min_ttl_or_hl = m_entry.stat_sum.min_ttl_or_hl;
	stat_sum->max_ttl_or_hl = m_entry.stat_sum.max_ttl_or_hl;
	stat_sum->mean_ttl_or_hl = m_entry.stat_sum.mean_ttl_or_hl;
	stat_sum->dev_ttl_or_hl = m_entry.stat_sum.dev_ttl_or_hl;

	return sizeof(rtcp_xr_stat_sum_t);
}
//------------------------------------------------------------------------------
int rtcp_xr_packet_t::write_voip_metrics(uint8_t* out, int size)
{
	if (sizeof(rtcp_xr_voip_metrics_t) > (size_t)size)
	{
		return 0;
	}

	rtcp_xr_voip_metrics_t* voip_metrics = (rtcp_xr_voip_metrics_t*)(out);

	voip_metrics->block_type = m_entry.voip_metrics.block_type;
	voip_metrics->block_length = htons(m_entry.voip_metrics.block_length);
	voip_metrics->ssrc = htonl(m_entry.voip_metrics.ssrc);
	voip_metrics->loss_rate = m_entry.voip_metrics.loss_rate;
	voip_metrics->discard_rate = m_entry.voip_metrics.discard_rate;
	voip_metrics->burst_density = m_entry.voip_metrics.burst_density;
	voip_metrics->gap_density = m_entry.voip_metrics.gap_density;
	voip_metrics->burst_duration = m_entry.voip_metrics.burst_duration;
	voip_metrics->gap_duration = m_entry.voip_metrics.gap_duration;
	voip_metrics->round_trip_delay = m_entry.voip_metrics.round_trip_delay;
	voip_metrics->end_system_delay = m_entry.voip_metrics.end_system_delay;
	voip_metrics->signal_level = m_entry.voip_metrics.signal_level;
	voip_metrics->noise_level = m_entry.voip_metrics.noise_level;
	voip_metrics->RERL = m_entry.voip_metrics.RERL;
	voip_metrics->Gmin = m_entry.voip_metrics.Gmin;
	voip_metrics->r_factor = m_entry.voip_metrics.r_factor;
	voip_metrics->ext_r_factor = m_entry.voip_metrics.ext_r_factor;
	voip_metrics->mos_lq = m_entry.voip_metrics.mos_lq;
	voip_metrics->mos_cq = m_entry.voip_metrics.mos_cq;
	voip_metrics->rx_config = m_entry.voip_metrics.rx_config;
	voip_metrics->jb_nominal = m_entry.voip_metrics.jb_nominal;
	voip_metrics->jb_maximum = m_entry.voip_metrics.jb_maximum;
	voip_metrics->jb_abs_max = m_entry.voip_metrics.jb_abs_max;

	return sizeof(rtcp_xr_voip_metrics_t);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
