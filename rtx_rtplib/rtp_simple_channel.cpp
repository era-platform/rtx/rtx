﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtp_simple_channel.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtp_simple_channel_t::rtp_simple_channel_t(rtl::Logger* log) :
	rtp_channel_t(log), m_log(log), m_data_socket(log), m_control_socket(log)
{
	m_event_handler = nullptr;
	m_socket_io_service = nullptr;

	m_data_port = m_ctrl_port = 0;

	m_lock.clear();

	m_subscriber_count = 0;

	m_started = false;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtp_simple_channel_t::~rtp_simple_channel_t()
{
	destroy();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtp_channel_type_t rtp_simple_channel_t::getType() const
{
	return rtp_channel_type_t::rtp_channel_type_simple_e;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtp_simple_channel_t::bind_data_socket(const ip_address_t* iface, uint16_t data_port)
{
	if (iface == nullptr)
	{
		PLOG_RTP_ERROR("rtp","bind_data_socket -- invalid address");
		return false;
	}

	PLOG_RTP_WRITE("rtp", "bind_data_socket -- binding socket on %s, %u ...", iface->to_string(), data_port);

	m_data_socket.close();

	if (m_data_socket.create(iface->get_family(), e_ip_protocol_type::E_IP_PROTOCOL_UDP))
	{
		socket_address saddr(iface, data_port);

		if (m_data_socket.bind(&saddr))
		{
			PLOG_RTP_WRITE("rtp","bind_data_socket -- ok");
			
			m_interface.copy_from(iface);
			m_data_port = m_data_socket.get_local_address()->get_port();

			char name[64] = { 0 };
			int len = std_snprintf(name, 64, "simple_channel:%s(%u)", m_interface.to_string(), m_data_port);
			name[len] = 0;
			rtp_channel_t::set_name(name);

			int bsize = 64 * 1024;
			m_data_socket.set_option(SOL_SOCKET, SO_RCVBUF, &bsize, sizeof(int));
			m_data_socket.set_option(SOL_SOCKET, SO_SNDBUF, &bsize, sizeof(int));

			return true;
		}
		else
		{
			PLOG_RTP_ERROR("rtp","bind_data_socket -- net_socket.bind(%u) failed", data_port);
		}
	}
	else
	{
		PLOG_RTP_ERROR("rtp","bind_data_socket -- net_socket.create() failed");
	}

	m_data_socket.close();

	return false;
}
//------------------------------------------------------------------------------
bool rtp_simple_channel_t::bind_ctrl_socket(const ip_address_t* iface, uint16_t ctrl_port)
{
	if (iface == nullptr)
	{
		PLOG_RTP_ERROR("rtp", "bind_ctrl_socket -- invalid address");
		return false;
	}

	if (ctrl_port == 0)
	{
		PLOG_RTP_ERROR("rtp", "bind_ctrl_socket -- invalid ctrl port");
		return false;
	}

	PLOG_RTP_WRITE("rtp", "bind_ctrl_socket -- binding socket on %s, %u ...", iface->to_string(), ctrl_port);

	m_control_socket.close();

	if (m_control_socket.create(iface->get_family(), e_ip_protocol_type::E_IP_PROTOCOL_UDP))
	{
		socket_address saddr(iface, ctrl_port);

		if (m_control_socket.bind(&saddr))
		{
			PLOG_RTP_WRITE("rtp", "bind_ctrl_socket -- ok");

			m_interface.copy_from(iface);
			m_ctrl_port = saddr.get_port();

			char name[64] = { 0 };
			int len = std_snprintf(name, 64, "%s:%u", (char*)rtp_channel_t::getName(), m_ctrl_port);
			//int len_write = std_snprintf((char*)name + len - 1, 64 - len, ":%u)", m_ctrl_port);
			//len += len_write;
			name[len] = 0;
			rtp_channel_t::set_name(name);

			int bsize = 64 * 1024;
			m_control_socket.set_option(SOL_SOCKET, SO_RCVBUF, &bsize, sizeof(int));
			m_control_socket.set_option(SOL_SOCKET, SO_SNDBUF, &bsize, sizeof(int));

			return true;
		}
		else
		{
			PLOG_RTP_ERROR("rtp", "bind_ctrl_socket -- net_socket.bind(%u) failed", ctrl_port);
		}
	}
	else
	{
		PLOG_RTP_ERROR("rtp", "bind_ctrl_socket -- net_socket.create() failed");
	}

	m_control_socket.close();

	return false;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtp_simple_channel_t::destroy()
{
	PLOG_RTP_WRITE("rtp", "destroy...");

	stop_io();

	m_data_socket.close();
	m_control_socket.close();

	PLOG_RTP_WRITE("rtp", "destroy : subscr(%d): done", m_subscriber_count);

	return m_subscriber_count == 0;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtp_simple_channel_t::started()
{
	return m_started;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int rtp_simple_channel_t::start_io(socket_io_service_t* iosvc)
{
	if (m_started)
	{
		//PLOG_CALL("rtp", "start_io -- already started");
		return 0;
	}

	if (iosvc == nullptr)
	{
		PLOG_RTP_ERROR("rtp","start_io -- invalid io service");
		return -1;
	}

	m_socket_io_service = iosvc;

	if (!m_socket_io_service->add(&m_data_socket, this) || !m_socket_io_service->add(&m_control_socket, this))
	{
		//PLOG_RTP_ERROR("rtp","start_io -- socket_io_service.add() failed");
		return -2;
	}
	else
	{
		insert_stat(this);

		m_started = true;
	}

	return 1;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_simple_channel_t::stop_io()
{
	if (m_socket_io_service != nullptr)
	{
		remove_stat(this);

		m_socket_io_service->remove(&m_data_socket);
		m_socket_io_service->remove(&m_control_socket);

		m_started = false;

		m_socket_io_service = nullptr;
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const ip_address_t* rtp_simple_channel_t::get_net_interface() const
{
	return &m_interface;
}
const uint16_t rtp_simple_channel_t::get_data_port() const
{
	return m_data_port;
}
uint16_t rtp_simple_channel_t::get_ctrl_port() const
{
	return m_ctrl_port;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const socket_io_service_t* rtp_simple_channel_t::get_io_service() const
{
	return m_socket_io_service;
}
void rtp_simple_channel_t::set_io_service(socket_io_service_t* svc)
{
	m_socket_io_service = svc;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
i_rx_channel_event_handler*	rtp_simple_channel_t::get_event_handler()
{
	if (m_lock.test_and_set(std::memory_order_acquire))
	{
		return nullptr;
	}

	i_rx_channel_event_handler* h = m_event_handler;
	m_lock.clear(std::memory_order_release);
	return h;
}
void rtp_simple_channel_t::set_event_handler(i_rx_channel_event_handler* handler)
{
	bool go = true;
	while (go)
	{
		if (m_lock.test_and_set(std::memory_order_acquire))
		{
			continue;
		}

		m_event_handler = handler;
		m_lock.clear(std::memory_order_release);
		go = false;
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_simple_channel_t::print_ports(const char* pef, rtl::Logger* log)
{
	if (log != nullptr)
	{
		log->log("rtp", "%s", pef);
		log->log("rtp", "--------------");

		if (m_data_port > 0)
		{
			rtl::String p1 = "";
			p1 << m_data_port;
			log->log("rtp", "      |%s", (const char*)p1);
		}

		if (m_ctrl_port > 0)
		{
			rtl::String p2 = "";
			p2 << m_ctrl_port;
			log->log("rtp", "      |%s", (const char*)p2);
		}

		log->log("rtp", "--------------");
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtp_simple_channel_t::send_packet(const rtp_packet* packet, const socket_address* saddr)
{
	if (packet == nullptr)
	{
		PLOG_RTP_ERROR("rtp","send_packet -- invalid packet");
		return false;
	}

	if (saddr == nullptr)
	{
		PLOG_RTP_ERROR("rtp","send_packet -- invalid address");
		return false;
	}

	uint8_t buffer[rtp_packet::PAYLOAD_BUFFER_SIZE];
	uint32_t length = packet->write_packet(buffer, sizeof(buffer));
	
	if (length == 0)
	{
		PLOG_RTP_WARNING("rtp","send_packet -- rtp_packet.write_packet() failed");
		return false;
	}

	/*PLOG_RTP_FLOW("rtp", " sending -- ID(%s) ssrc:%10u pt:%3u seq:%5u (%6u)   to:%s .", (const char*)getName(), packet->get_ssrc(),
		packet->get_payload_type(), packet->get_sequence_number(), length, saddr->to_string());*/

	return send_data(buffer, length, saddr);
}
//------------------------------------------------------------------------------
bool rtp_simple_channel_t::send_data(const uint8_t* data, uint32_t len, const socket_address* saddr)
{
	const rtp_header_t* hdr = (const rtp_header_t*)data;
	PLOG_RTP_FLOW("RTP", "RTP  SEND(%5u) v:%1u p:%1u e:%1u cc:%1u m:%1u pt:%3u seq:%5u ts:%10u ssrc:%10u   to:%s", len,
		hdr->ver, hdr->pad, hdr->ext, hdr->cc, hdr->m, hdr->pt, ntohs(hdr->seq), ntohl(hdr->ts), ntohl(hdr->ssrc),
		saddr->to_string());

	if (m_socket_io_service == nullptr)
	{
		//PLOG_RTP_ERROR("rtp", "send_data -- socket_io_service is undefined");
		return false;
	}

	if (!m_socket_io_service->send_data_to(&m_data_socket, data, len, saddr))
	{
		PLOG_RTP_ERROR("rtp", "send_data -- socket_io_service.send_data() failed");
		return false;
	}

	update_tx_stat(len);

	return true;
}
//------------------------------------------------------------------------------
bool rtp_simple_channel_t::send_ctrl(const rtcp_packet_t* packet, const socket_address* saddr)
{
	if (packet == nullptr)
	{
		PLOG_RTP_ERROR("rtp", "send_ctrl -- invalid packet");
		return false;
	}

	if (saddr == nullptr)
	{
		PLOG_RTP_ERROR("rtp", "send_ctrl -- invalid address");
		return false;
	}

	uint8_t buffer[rtp_packet::PAYLOAD_BUFFER_SIZE];
	uint32_t length = packet->write(buffer, sizeof(buffer));

	if (length == 0)
	{
		PLOG_RTP_WARNING("rtp", "send_ctrl -- write packet failed");
		return false;
	}

	if (m_socket_io_service == nullptr)
	{
		//PLOG_RTP_ERROR("rtp", "send_ctrl -- socket_io_service is undefined");
		return false;
	}

	/*PLOG_RTP_WRITE("rtp", " sending -- ID(%s) ssrc:%10u                  (%6u)   to:%s .", (const char*)getName(), packet->get_ssrc(),
		length, saddr->to_string());*/

	rtcp_header_t* hdr = (rtcp_header_t*)buffer;
	PLOG_RTP_FLOW("RTCP", "RTCP RECV(%5u) v:%1u p:%1u rc:%2u        pt:%3u len:%5u               ssrc:%10u   to:%s", length,
		hdr->ver, hdr->pad, hdr->rc, hdr->pt, ntohs(hdr->length), ntohl(hdr->ssrc),
		saddr->to_string());

	if (!m_socket_io_service->send_data_to(&m_control_socket, buffer, length, saddr))
	{
		PLOG_RTP_ERROR("rtp", "send_ctrl -- socket_io_service.send_data() failed");
		return false;
	}

	update_tx_stat(length);

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_simple_channel_t::inc_tx_subscriber(uint32_t ssrc)
{
	m_subscriber_count = std_interlocked_inc(&m_subscriber_count);
}
//------------------------------------------------------------------------------
void rtp_simple_channel_t::dec_tx_subscriber(uint32_t ssrc)
{
	if (m_subscriber_count > 0)
	{
		m_subscriber_count = std_interlocked_dec(&m_subscriber_count);
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_simple_channel_t::socket_data_available(net_socket* /*sock*/)
{
	PLOG_RTP_WARNING("rtp","socket_data_available -- empty method");
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_simple_channel_t::socket_data_received(net_socket* sock, const uint8_t* data, uint32_t len, const socket_address* from)
{
	if (sock == nullptr)
	{
		PLOG_RTP_ERROR("rtp","socket_data_received -- invalid socket");
		return;
	}

	if (data == nullptr)
	{
		PLOG_RTP_ERROR("rtp","socket_data_received -- invalid data");
		return;
	}

	if (len == 0)
	{
		PLOG_RTP_ERROR("rtp","socket_data_received -- invalid length");
		return;
	}

	if (from == nullptr)
	{
		PLOG_RTP_ERROR("rtp","socket_data_received -- invalid address");
		return;
	}

	update_rx_stat(len);

	bool send = false;
	if (sock == &m_data_socket)
	{
		send = rtp_data_received(data, len, from);
	}
	else if (sock == &m_control_socket)
	{
		send = rtcp_data_received(data, len, from);
	}
	else
	{
		PLOG_RTP_ERROR("rtp","socket_data_received -- unknown socket");
		// возможно пришло из другого канала
		// по этому:
		i_rx_channel_event_handler* h = get_event_handler();
		if (h != nullptr)
		{
			h->rx_data_stream(this, data, len, from);
		}
	}

	if (!send)
	{
		rtp_channel_t* next = rtp_channel_t::get_next_channel();
		if (next != nullptr)
		{
			next->socket_data_received(sock, data, len, from);
		}
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtp_simple_channel_t::rtp_data_received(const uint8_t* data, uint32_t len, const socket_address* from)
{
	rtp_header_t* hdr = (rtp_header_t*)data;

	/*PLOG_RTP_FLOW("rtp", "received -- ID(%s) ssrc:%10u pt:%3u seq:%5u (%6u) from:%s .", (const char*)getName(), ntohl(hdr->ssrc),
		hdr->pt, ntohs(hdr->seq), len, from->to_string());*/

	rtp_packet packet;

	if (!packet.read_packet(data, len))
	{
		PLOG_RTP_ERROR("rtp","rtp_data_received -- rtp_packet.read_packet() failed");
		return false;
	}

	PLOG_RTP_FLOW("RTP", "RTP  RECV(%5u) v:%1u p:%1u e:%1u cc:%1u m:%1u pt:%3u seq:%5u ts:%10u ssrc:%10u from:%s", len,
		hdr->ver, hdr->pad, hdr->ext, hdr->cc, hdr->m, hdr->pt, ntohs(hdr->seq), ntohl(hdr->ts), ntohl(hdr->ssrc),
		from->to_string());

	i_rx_channel_event_handler* h = get_event_handler();
	if (h != nullptr)
	{
		return h->rx_packet_stream(this, &packet, from);
	}

	return false;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtp_simple_channel_t::rtcp_data_received(const uint8_t* data, uint32_t len, const socket_address* from)
{
	rtcp_header_t* hdr = (rtcp_header_t*)data;

	PLOG_RTP_FLOW("RTP", "RTCP RECV(%5u) v:%1u p:%1u rc:%2u        pt:%3u len:%5u               ssrc:%10u from:%s", len,
		hdr->ver, hdr->pad, hdr->rc, hdr->pt, ntohs(hdr->length), ntohl(hdr->ssrc), from->to_string());

	if (hdr->ver != 2 || len == 20)
	{
		m_log->log_binary("rtp", data, len, "rtcp_data_received -- INVALID RTP VERSION.");
	}

	i_rx_channel_event_handler* h = get_event_handler();
	if (h == nullptr)
	{
		PLOG_RTP_ERROR("rtp", "rtcp_data_received -- handler not found.");
		return false;
	}

	if (rtcp_packet_t::is_rtcp_packet(data, len))
	{
		rtcp_packet_t packet;
		if (len != (uint32_t)packet.read(data, len))
		{
			PLOG_RTP_WARNING("rtp", "rtcp_data_received -- read packet failed.");
			return false;
		}

		return h->rx_ctrl_stream(this, &packet, from);
	}
	else
	{
		m_log->log_binary("rtp", data, len, "invalid RTCP packet received from %s!", from->to_string());
	}

	return false;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_simple_channel_t::tx_packet_channel(/*const mg_tx_stream_t* stream, */const rtp_packet* packet, const socket_address* to)
{
	send_packet(packet, to);
}
void rtp_simple_channel_t::tx_data_channel(/*const mg_tx_stream_t* stream, */const uint8_t* data, uint32_t len, const socket_address* to)
{
	send_data(data, len, to);
}
void rtp_simple_channel_t::tx_ctrl_channel(/*const mg_tx_stream_t* stream, */const rtcp_packet_t* packet, const socket_address* to)
{
	send_ctrl(packet, to);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_simple_channel_t::clear_handler(uint32_t ssrc)
{
	set_event_handler(nullptr);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
