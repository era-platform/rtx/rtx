﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 //--------------------------------------
// based on libsrtp written by
// David A. McGrew
// Cisco Systems, Inc.
//--------------------------------------
#include "stdafx.h"

#include "srtp_cipher.h"
//--------------------------------------
//
//--------------------------------------
cipher_t::cipher_t(int key_len) : m_key_len(key_len)
{
	m_cipher_id = 0;
}
//--------------------------------------
//
//--------------------------------------
cipher_t::~cipher_t()
{
	m_cipher_id = 0;
	m_key_len = 0;
}
//--------------------------------------
//
//--------------------------------------
bool cipher_t::output(uint8_t *buffer, int num_octets_to_output)
{
	// zeroize the buffer
	octet_string_set_to_zero(buffer, num_octets_to_output);
  
	// exor keystream into buffer
	return encrypt(buffer, (uint32_t *)&num_octets_to_output);
}
//--------------------------------------
//
//--------------------------------------
null_cipher_ctx_t::null_cipher_ctx_t(int key_len) : cipher_t(key_len)
{
}
//--------------------------------------
//
//--------------------------------------
null_cipher_ctx_t::~null_cipher_ctx_t()
{
}
//--------------------------------------
//
//--------------------------------------
bool null_cipher_ctx_t::create(const uint8_t *key, cipher_direction_t dir)
{
	return true;
}
//--------------------------------------
//
//--------------------------------------
bool null_cipher_ctx_t::set_iv(void *iv)
{
	return true;
}
//--------------------------------------
//
//--------------------------------------
bool null_cipher_ctx_t::encrypt(uint8_t *buffer, uint32_t *octets_to_encrypt)
{
	return true;
}
//--------------------------------------
//
//--------------------------------------
bool null_cipher_ctx_t::decrypt(uint8_t *buffer, uint32_t *octets_to_decrypt)
{
	return true;
}
//--------------------------------------
