/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtcp_packet.h"

 //------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
/*
0                   1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|      BT       | type-specific |         block length          |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
:             type-specific block contents                      :
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/
struct rtcp_xr_header_t
{
	uint8_t type_specific;
	uint8_t bt;
	uint16_t block_length;
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
enum rtcp_xr_block_type_t
{
	rtcp_xr_loss_rle_e = 1,						// 1	Loss RLE Report Block	[RFC3611]
	rtcp_xr_dublicate_rle_e = 2,				// 2	Duplicate RLE Report Block	[RFC3611]
	rtcp_xr_receipt_times_e = 3,				// 3	Packet Receipt Times Report Block	[RFC3611]
	rtcp_xr_reference_time_e = 4,				// 4	Receiver Reference Time Report Block	[RFC3611]
	rtcp_xr_dlrr_e = 5,							// 5	DLRR Report Block	[RFC3611]
	rtcp_xr_stat_sum_e = 6,						// 6	Statistics Summary Report Block	[RFC3611]
	rtcp_xr_voip_metrics_e = 7,					// 7	VoIP Metrics Report Block	[RFC3611]
	rtcp_xr_e = 8,								// 8	RTCP XR	[RFC5093]
	rtcp_xr_texas_extended_viop_e = 9,			// 9	Texas Instruments Extended VoIP Quality Block	[http://focus.ti.com/general/docs/bcg/bcgdoccenter.tsp?templateId=6116&navigationId=12078#42][David_Lide]
	rtcp_xr_post_repair_loss_rle_e = 10,		// 10	Post-repair Loss RLE Report Block	[RFC5725]
	rtcp_xr_multicact_acquisition_e = 11,		// 11	Multicast Acquisition Report Block	[RFC6332]
	rtcp_xr_idms_e = 12,						// 12	IDMS Report Block	[RFC7272]
	rtcp_xr_ecn_e = 13,							// 13	ECN Summary Report	[RFC6679]
	rtcp_xr_measurement_info_e = 14,			// 14	Measurement Information Block	[RFC6776]
	rtcp_xr_delay_variation_e = 15,				// 15	Packet Delay Variation Metrics Block	[RFC6798]
	rtcp_xr_delay_metrics_e = 16,				// 16	Delay Metrics Block	[RFC6843]
	rtcp_xr_burst_loss_sum_e = 17,				// 17	Burst/Gap Loss Summary Statistics Block	[RFC7004]
	rtcp_xr_burst_discard_sum_e = 18,			// 18	Burst/Gap Discard Summary Statistics Block	[RFC7004]
	rtcp_xr_impairment_statistics_e = 19,		// 19	Frame Impairment Statistics Summary	[RFC7004]
	rtcp_xr_burst_loss_metrics_e = 20,			// 20	Burst/Gap Loss Metrics Block	[RFC6958]
	rtcp_xr_burst_discard_metrics_e = 21,		// 21	Burst/Gap Discard Metrics Block	[RFC7003][RFC Errata 3735]
	rtcp_xr_mpeg2_psi_independent_e = 22,		// 22	MPEG2 Transport Stream PSI-Independent Decodability Statistics Metrics Block	[RFC6990]
	rtcp_xr_de_jitter_e = 23,					// 23	De-Jitter Buffer Metrics Block	[RFC7005]
	rtcp_xr_discard_count_e = 24,				// 24	Discard Count Metrics Block	[RFC7002]
	rtcp_xr_drle_e = 25,						// 25	DRLE (Discard RLE Report)	[RFC7097]
	rtcp_xr_bdr_e = 26,							// 26	BDR (Bytes Discarded Report)	[RFC7243]
	rtcp_xr_rfisd_e = 27,						// 27	RFISD (RTP Flows Initial Synchronization Delay)	[RFC7244]
	rtcp_xr_rfso_e = 28,						// 28	RFSO (RTP Flows Synchronization Offset Metrics Block)	[RFC7244]
	rtcp_xr_mos_e = 29,							// 29	MOS Metrics Block	[RFC7266]
	rtcp_xr_lcb_e = 30,							// 30	LCB (Loss Concealment Metrics Block)	[RFC7294, Section 4.1]
	rtcp_xr_csb_e = 31,							// 31	CSB (Concealed Seconds Metrics Block)	[RFC7294, Section 4.1]
	rtcp_xr_mpeg2_psi_e = 32,					// 32	MPEG2 Transport Stream PSI Decodability Statistics Metrics Block	[RFC7380]
	rtcp_xr_post_repair_loss_count_e = 33,		// 33	Post-Repair Loss Count Metrics Report Block	[RFC7509]
	rtcp_xr_video_loss_concealment_e = 34,		// 34	Video Loss Concealment Metric Report Block	[RFC7867]
	rtcp_xr_independend_burst_discard_e = 35,	// 35	Independent Burst/Gap Discard Metrics Block	[RFC8015]
	// 36-254	Unassigned
	rtcp_xr_reserved_e = 255					// 255	Reserved for future extensions	[RFC3611]
};
const char* get_rtcp_xr_type_name(rtcp_xr_block_type_t type);
/*
The Loss RLE Report Block has the following format:

0                   1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|     BT=1      | rsvd. |   T   |         block length          |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                        SSRC of source                         |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|          begin_seq            |             end_seq           |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|          chunk 1              |             chunk 2           |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
:                              ...                              :
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|          chunk n-1            |             chunk n           |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/
#define RTCP_XR_MAX_RLE_CHUNKS	184
struct rtcp_xr_loss_rle_t
{
	uint8_t rsvd : 4, t : 4;
	uint8_t block_type;
	uint16_t block_length;
	uint32_t ssrc;
	uint16_t begin_seq;
	uint16_t end_seq;
	uint16_t chunks[RTCP_XR_MAX_RLE_CHUNKS];
};
/*
The Duplicate RLE Report Block has the following format:

0                   1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|     BT=2      | rsvd. |   T   |         block length          |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                        SSRC of source                         |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|          begin_seq            |             end_seq           |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|          chunk 1              |             chunk 2           |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
:                              ...                              :
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|          chunk n-1            |             chunk n           |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/
struct rtcp_xr_duplicate_rle_t
{
	uint8_t rsvd : 4, t : 4;
	uint8_t block_type;
	uint16_t block_length;
	uint32_t ssrc;
	uint16_t begin_seq;
	uint16_t end_seq;
	uint16_t chunks[RTCP_XR_MAX_RLE_CHUNKS];
};
/*
The Packet Receipt Times Report Block has the following format:

0                   1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|     BT=3      | rsvd. |   T   |         block length          |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                        SSRC of source                         |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|          begin_seq            |             end_seq           |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|       Receipt time of packet begin_seq                        |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|       Receipt time of packet (begin_seq + 1) mod 65536        |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
:                              ...                              :
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|       Receipt time of packet (end_seq - 1) mod 65536          |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/
#define RTCP_XR_MAX_RECEIPT_TIMES	64
struct rtcp_xr_receipt_times_t
{
	uint8_t rsvd : 4, t : 4;
	uint8_t block_type;
	uint16_t block_length;
	uint32_t ssrc;
	uint16_t begin_seq;
	uint16_t end_seq;
	uint32_t receipt_times[RTCP_XR_MAX_RECEIPT_TIMES];
};
/*
Receiver Reference Time Report Block

0                   1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|     BT=4      |   reserved    |       block length = 2        |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|              NTP timestamp, most significant word             |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|             NTP timestamp, least significant word             |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/
struct rtcp_xr_reference_time_t
{
	uint8_t rsvd;
	uint8_t block_type;
	uint16_t block_length;
	uint32_t most;
	uint32_t least;
};
/*
DLRR Report Block

0                   1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|     BT=5      |   reserved    |         block length          |
+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
|                 SSRC_1 (SSRC of first receiver)               | sub-
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ block
|                         last RR (LRR)                         |   1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                   delay since last RR (DLRR)                  |
+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
|                 SSRC_2 (SSRC of second receiver)              | sub-
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ block
:                               ...                             :   2
+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
*/
#define RTCP_XR_MAX_DLRR_BLOCKS	16
struct rtcp_xr_dlrr_sub_block_t
{
	uint32_t ssrc;
	uint32_t last_rr;
	uint32_t dlrr;
};
struct rtcp_xr_dlrr_t
{
	uint8_t rsvd;
	uint8_t block_type;
	uint16_t block_length;
	rtcp_xr_dlrr_sub_block_t sub_blocks[RTCP_XR_MAX_DLRR_BLOCKS];
};
/*
The Statistics Summary Report Block has the following format:

0                   1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|     BT=6      |L|D|J|ToH|rsvd.|       block length = 9        |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                        SSRC of source                         |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|          begin_seq            |             end_seq           |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                        lost_packets                           |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                        dup_packets                            |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                         min_jitter                            |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                         max_jitter                            |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                         mean_jitter                           |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                         dev_jitter                            |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
| min_ttl_or_hl | max_ttl_or_hl |mean_ttl_or_hl | dev_ttl_or_hl |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/
struct rtcp_xr_stat_sum_t
{
	uint8_t l:1, d:1, j:1, t:2, rsvd:3;
	uint8_t block_type;
	uint16_t block_length;
	uint32_t ssrc;
	uint16_t begin_seq;
	uint16_t end_seq;
	uint32_t lost_packets;
	uint32_t dup_packets;
	uint32_t min_jitter;
	uint32_t max_jitter;
	uint32_t mean_jitter;
	uint32_t dev_jitter;
	uint8_t min_ttl_or_hl;
	uint8_t max_ttl_or_hl;
	uint8_t mean_ttl_or_hl;
	uint8_t dev_ttl_or_hl;
};
/*
VoIP Metrics Report Block

0                   1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|     BT=7      |   reserved    |       block length = 8        |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                        SSRC of source                         |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|   loss rate   | discard rate  | burst density |  gap density  |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|       burst duration          |         gap duration          |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|     round trip delay          |       end system delay        |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
| signal level  |  noise level  |     RERL      |     Gmin      |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|   R factor    | ext. R factor |    MOS-LQ     |    MOS-CQ     |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|   RX config   |   reserved    |          JB nominal           |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|          JB maximum           |          JB abs max           |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/
struct rtcp_xr_voip_metrics_t
{
	uint8_t rsvd;
	uint8_t block_type;
	uint16_t block_length;
	uint32_t ssrc;

	uint8_t discard_rate;
	uint8_t loss_rate;
	uint8_t gap_density;
	uint8_t burst_density;
	uint16_t burst_duration;
	uint16_t gap_duration;
	uint16_t round_trip_delay;
	uint16_t end_system_delay;
	uint8_t noise_level;
	uint8_t signal_level;
	uint8_t Gmin;
	uint8_t RERL;
	uint8_t ext_r_factor;
	uint8_t r_factor;
	uint8_t mos_cq;
	uint8_t mos_lq;
	uint8_t reserved;
	uint8_t rx_config;
	uint16_t jb_nominal;
	uint16_t jb_maximum;
	uint16_t jb_abs_max;
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class rtcp_xr_packet_t : public rtcp_base_packet_t
{
	union xr_entry_t
	{
		rtcp_xr_loss_rle_t loss_rle;
		rtcp_xr_duplicate_rle_t duplicate_rle;
		rtcp_xr_receipt_times_t receipt_times;
		rtcp_xr_reference_time_t reference_time;
		rtcp_xr_dlrr_t dlrr;
		rtcp_xr_stat_sum_t stat_sum;
		rtcp_xr_voip_metrics_t voip_metrics;
	};

	xr_entry_t m_entry;

	rtcp_xr_block_type_t m_type;

	void cleanup();

public:
	rtcp_xr_packet_t();
	virtual ~rtcp_xr_packet_t();

	virtual int read(const uint8_t* in, int length);
	virtual int write(uint8_t* out, int size);
	virtual int dump(char* out, int size) const;

	rtcp_xr_block_type_t getType();

	rtcp_xr_loss_rle_t* get_loss_rle();
	rtcp_xr_duplicate_rle_t* get_duplicate_rle();
	rtcp_xr_receipt_times_t* get_receipt_times();
	rtcp_xr_dlrr_t* get_dlrr();
	rtcp_xr_stat_sum_t* get_stat_sum();
	rtcp_xr_voip_metrics_t* get_voip_metrics();

private:
	int read_loss_rle(const uint8_t* in, int length);
	int read_duplicate_rle(const uint8_t* in, int length);
	int read_receipt_times(const uint8_t* in, int length);
	int read_reference_time(const uint8_t* in, int length);
	int read_dlrr(const uint8_t* in, int length);
	int read_stat_sum(const uint8_t* in, int length);
	int read_voip_metrics(const uint8_t* in, int length);

	int write_loss_rle(uint8_t* out, int size);
	int write_duplicate_rle(uint8_t* out, int size);
	int write_receipt_times(uint8_t* out, int size);
	int write_reference_time(uint8_t* out, int size);
	int write_dlrr(uint8_t* out, int size);
	int write_stat_sum(uint8_t* out, int size);
	int write_voip_metrics(uint8_t* out, int size);
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

