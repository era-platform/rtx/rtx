﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "std_mutex.h"
#include "std_logger.h"
#include "rtp_channel.h"
#include "rtp_port_manager_interface.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class rtp_port_manager_t
{
public:
	RTX_RTPLIB_API static void initialize(rtl::Logger* log);
	RTX_RTPLIB_API static void release2();

	RTX_RTPLIB_API static void destroy();

	RTX_RTPLIB_API static void set_interface(const char* ip_interface, const char* address_key);
	RTX_RTPLIB_API static void free_interface(const char* address_key);

	RTX_RTPLIB_API static bool set_port_range(const char* address_key, uint16_t begin, uint32_t count);
	RTX_RTPLIB_API static void free_port_range(const char* address_key);

	RTX_RTPLIB_API static rtp_channel_t* get_channel(const char* address_key, rtp_channel_type_t type);
	RTX_RTPLIB_API static void release_channel(rtp_channel_t* channel);

	RTX_RTPLIB_API static bool start_channel_listen(rtp_channel_t* channel);
	RTX_RTPLIB_API static void stop_channel_listen(rtp_channel_t* channel);

	RTX_RTPLIB_API static bool compare_channels(rtp_channel_t* channel_1, rtp_channel_t* channel_2);

private:
	static rtp_port_manager_interface_t* find_interface_at_key(const char* address_key);
	static rtp_port_manager_interface_t* find_interface_at_iface(const char* ip_interface);
	static rtp_port_manager_interface_t* add_interface(const char* ip_interface, const char* address_key);

private:
	static rtl::Logger* m_log;
	static rtl::MutexWatch m_mutex;
 	static mg_interface_list m_interfaces;	// массив интерфейсов // удаляется корректно!
};
//------------------------------------------------------------------------------
