﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//--------------------------------------
// header for the cryptographic kernel
//--------------------------------------
// based on libsrtp written by
// David A. McGrew
// Cisco Systems, Inc.
//--------------------------------------
#pragma once

#include "srtp_cipher.h"
#include "srtp_auth.h"
#include "srtp_datatypes.h"
//--------------------------------------
// The null cipher performs no encryption.
//--------------------------------------
#define NULL_CIPHER 0
//--------------------------------------
// AES-128 Integer Counter Mode (AES ICM)             
//--------------------------------------
#define AES_128_ICM 1
//--------------------------------------
// The null authentication function performs no authentication.
//--------------------------------------
#define NULL_AUTH 0
//--------------------------------------
// HMAC-SHA1
//--------------------------------------
#define HMAC_SHA1 1
//--------------------------------------
// allocates a cipher of type id at location *cp, with key length
// key_len octets.
//--------------------------------------
cipher_t* crypto_alloc_cipher(int cipher_id, int key_len);
//--------------------------------------
// allocates an auth function of type id at location *ap, with key
// length key_len octets and output tag length of tag_len.
//--------------------------------------
auth_t* crypto_alloc_auth(int auth_id, int key_len, int tag_len);
//--------------------------------------
//
//--------------------------------------
const char* get_cipher_description(int cipher_id);
//--------------------------------------
//
//--------------------------------------
const char* get_auth_description(int auth_id);
//--------------------------------------
// writes a random octet string.
//--------------------------------------
// The function call crypto_get_random(dest, len) writes len octets of
// random data to the location to which dest points, and returns an
// error code.  This error code must be checked, and if a failure is
// reported, the data in the buffer must not be used.
//--------------------------------------
//bool crypto_get_random(uint8_t *buffer, uint32_t length);
//--------------------------------------
