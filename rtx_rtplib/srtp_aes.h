﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//--------------------------------------
// based on libsrtp written by
// David A. McGrew
// Cisco Systems, Inc.
//--------------------------------------

#pragma once

#include "srtp_datatypes.h"
//--------------------------------------
// aes internals
//--------------------------------------
typedef v128_t aes_expanded_key_t[11];
//--------------------------------------
//
//--------------------------------------
void aes_expand_encryption_key(const v128_t *key, aes_expanded_key_t expanded_key);
void aes_expand_decryption_key(const v128_t *key, aes_expanded_key_t expanded_key);
void aes_encrypt(v128_t *plaintext, const aes_expanded_key_t exp_key);
void aes_decrypt(v128_t *plaintext, const aes_expanded_key_t exp_key);
//--------------------------------------
