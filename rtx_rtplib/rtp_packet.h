﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//------------------------------------------------------------------------------
//	
//------------------------------------------------------------------------------
#include "rtx_rtplib.h"
//------------------------------------------------------------------------------
//	
//------------------------------------------------------------------------------
enum class rtp_packet_priv_type
{
	// RTX-84
	simple,		// обычный пакет
	generate,	// генерируемый (например комфортный шум)
	partial		// частичный (пакет как часть фрейма, например при разбивке из 30мс пакета в 10мс)
};
//------------------------------------------------------------------------------
//	
//------------------------------------------------------------------------------
class RTX_RTPLIB_API rtp_packet
{
	rtp_packet(const rtp_packet&);
	rtp_packet& operator=(const rtp_packet&);

public:
	static const uint32_t	FIXED_RTP_HEADER_LENGTH = 12;
	static const uint32_t	PAYLOAD_BUFFER_SIZE = 4096;
	static const uint32_t	FIXED_RTP_EXTENSION_LENGTH = 32;

	rtp_packet();
	virtual ~rtp_packet();

	// распаковка rtp-пакета из буфера.
	bool read_packet(const void* data, uint32_t length);
	// упаковка rtp-пакета в буфер.
	uint32_t write_packet(void* data, uint32_t size) const;

	// сброс всех полей.
	void reset();

	bool copy_from(const rtp_packet* packet);
	bool copy_head_from(const rtp_packet* packet);

	uint8_t get_version() const { return m_version; }
	void set_version(uint8_t version) { m_version = version; }
	// возвращает количество байт паддинга, или 0, если паддинга нет.
	uint8_t get_padding_count() const { return m_padding_count; }
	// задает количество байт паддинга, или 0, если паддинг не нужен.
	void set_padding_count(uint8_t count) { m_padding_count = count; }
	bool get_extension_flag() const { return m_externsion_flag; }
	uint8_t get_csrc_count() const { return m_csrc_count; }
	void set_csrc_count(uint8_t value) { m_csrc_count = value > 15 ? 15 : value; }
	bool get_marker() const { return m_marker; }
	void set_marker(bool value) { m_marker = value; }
	uint8_t get_payload_type() const { return m_payload_type; }
	void set_payload_type(uint8_t value) { m_payload_type = value; }
	uint16_t get_sequence_number() const { return m_sequence_number; }
	void set_sequence_number(uint16_t value) { m_sequence_number = value; }
	uint32_t getTimestamp() const { return m_timestamp; }
	void set_timestamp(uint32_t value) { m_timestamp = value; }
	uint32_t get_ssrc() const { return m_ssrc; }
	void set_ssrc(uint32_t value) { m_ssrc = value; }
	uint32_t get_csrc_at(uint8_t index) const { return index >= 15 ? 0 : m_csrc_list[index]; }
	void set_csrc_at(uint8_t index, uint32_t value);
	uint32_t get_samples() const { return m_samples; }
	void set_samples(uint32_t value) { m_samples = value; }
	const uint8_t* get_payload() const { return m_payload; }
	uint32_t get_payload_length() const { return m_payload_length; }
	uint32_t set_payload(const void* data, uint32_t length);
	uint32_t append_payload(const void* data, uint32_t length);
	uint32_t update_payload(int offset, const void* data, uint32_t length);

	// получить полный размер пакета вместе с заголовками.
	uint32_t get_full_length() const;

	// RTX-84. 0-simple, 1-generate, 2-partial
	rtp_packet_priv_type get_priv_type() const;
	void set_priv_type(rtp_packet_priv_type type);
	// RTX-84. if priv_type 2
	uint8_t get_priv_partial_count() const;
	void set_priv_partial_count(uint8_t count);

private:
	uint8_t					m_version;			/* protocol version */
    bool					m_externsion_flag;	/* header extension flag */
    uint8_t					m_csrc_count;		/* CSRC count */
    bool					m_marker;			/* marker bit */
    uint8_t					m_payload_type;		/* payload type */
    uint16_t				m_sequence_number;	/* sequence number */
    uint32_t				m_timestamp;		/* timestamp */
    uint32_t				m_ssrc;				/* synchronization source */
    uint32_t				m_csrc_list[15];	/* optional CSRC list */

	uint32_t				m_samples;

	uint8_t					m_payload[PAYLOAD_BUFFER_SIZE];
	uint32_t				m_payload_length;

	uint8_t					m_padding_count;

	uint8_t					m_extension_buff[FIXED_RTP_EXTENSION_LENGTH];
	uint32_t				m_extension_length;

	// RTX-84
	rtp_packet_priv_type	m_priv_type;
	uint8_t					m_priv_partial_count;
};
//--------------------------------------
