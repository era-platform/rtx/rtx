﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtp_channel.h"
#include "rtp_webrtc_subscriber.h"
#include <atomic>
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
class RTX_RTPLIB_API rtp_webrtc_channel_t : public rtp_channel_t, public i_tx_channel_event_handler
{
	rtp_webrtc_channel_t(const rtp_webrtc_channel_t&);
	rtp_webrtc_channel_t& operator=(const rtp_webrtc_channel_t&);

public:
	rtp_webrtc_channel_t(rtl::Logger* log, rtp_channel_type_t type);
	virtual ~rtp_webrtc_channel_t();

	virtual rtp_channel_type_t getType() const;
	virtual bool started();
	bool init();
	virtual bool destroy();

	virtual int start_io(socket_io_service_t* svc);
	virtual void stop_io();

	virtual const ip_address_t* get_net_interface() const;
	virtual const uint16_t get_data_port() const;
	uint16_t get_ctrl_port() const;
	uint16_t get_video_port() const;
	uint16_t get_ctrl_video_port() const;

	virtual const socket_io_service_t* get_io_service() const;
	virtual void set_io_service(socket_io_service_t* iosvc);

	virtual bool send_packet(const rtp_packet* packet, const socket_address* saddr);
	virtual bool send_data(const uint8_t* data, uint32_t len, const socket_address* saddr);
	virtual bool send_ctrl(const rtcp_packet_t* packet, const socket_address* saddr);

	bool bind_audio_socket(const ip_address_t* iface, uint16_t data_port);
	bool bind_ctrl_audio_socket(const ip_address_t* iface, uint16_t ctrl_port);
	bool bind_video_socket(const ip_address_t* iface, uint16_t data_port);
	bool bind_ctrl_video_socket(const ip_address_t* iface, uint16_t ctrl_port);

	const rtp_webrtc_subscriber_t* get_event_subscriber(uint32_t ssrc);
	void set_event_handler_rx(uint32_t ssrc, i_rx_channel_event_handler* handler);

	// для синхронизации при удалении канала
	virtual void inc_tx_subscriber(uint32_t ssrc) override;
	virtual void dec_tx_subscriber(uint32_t ssrc) override;

	bool setup_ice_params_local(const char* l_ufrag, const char* l_pswd);
	bool setup_ice_params_remote(const char* r_ufrag, const char* r_pswd);

	bool fill_fingerprint_param(class sdp_fingerprint_t* fingerprint_param);
	bool apply_fingerprint_param(class sdp_fingerprint_t* fingerprint_param);

	virtual void print_ports(const char* pef, rtl::Logger* log) override;
private:
	virtual void socket_data_available(net_socket* sock) override;
	virtual void socket_data_received(net_socket* sock, const uint8_t* data, uint32_t len, const socket_address* from) override;

	void rtp_data_received(const uint8_t* data, uint32_t len, const socket_address* from);
	void rtcp_data_received(const uint8_t* data, uint32_t len, const socket_address* from);

	// отправить пакет из стрима в канал.
	virtual void tx_packet_channel(const rtp_packet* packet, const socket_address* to) override;
	// отправить данные из стрима в канал.
	virtual void tx_data_channel(const uint8_t* data, uint32_t len, const socket_address* to) override;
	// отправить контрльный пакет из стрима в канал.
	virtual void tx_ctrl_channel(const rtcp_packet_t* packet, const socket_address* to) override;

	void clear_rx_handler(uint32_t ssrc);

private:
	rtl::Logger* m_log;
	rtp_channel_type_t m_type;
	volatile bool m_started;
	ip_address_t m_interface;
	uint16_t m_data_port;
	uint16_t m_ctrl_port;
	uint16_t m_data_video_port;
	uint16_t m_ctrl_video_port;
	net_socket m_data_socket;
	net_socket m_control_socket;
	net_socket m_data_video_socket;
	net_socket m_control_video_socket;
	std::atomic_flag m_lock;
	rtp_webrtc_subscriber_manager_t	m_subscribers;
	socket_io_service_t* m_socket_io_service;
	volatile uint32_t m_tx_subscriber_count;
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
