﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 //--------------------------------------
// key usage limits enforcement
//--------------------------------------
// based on libsrtp written by
// David A. McGrew
// Cisco Systems, Inc.
//--------------------------------------
#include "stdafx.h"
#include "srtp_key.h"
//--------------------------------------
//
//--------------------------------------
#define soft_limit 0x10000
//--------------------------------------
//
//--------------------------------------
bool key_limit_ctx_t::set(const xtd_seq_num_t seq_num)
{
	if (seq_num < soft_limit)
		return false;

	m_num_left = seq_num;
	m_state = key_state_normal;
	return true;
}
//--------------------------------------
//
//--------------------------------------
key_event_t key_limit_ctx_t::update()
{
	m_num_left--;
	
	if (m_num_left >= soft_limit)
	{
		return key_event_normal;   /* we're above the soft limit */
	}

	if (m_state == key_state_normal)
	{
		/* we just passed the soft limit, so change the state */
		m_state = key_state_past_soft_limit;
	}

	if (m_num_left < 1)
	{
		/* we just hit the hard limit */
		m_state = key_state_expired;
		return key_event_hard_limit;
	}

	return key_event_soft_limit;
}
//--------------------------------------
