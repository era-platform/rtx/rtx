﻿/* RTX H.248 Media Gate RTP Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

 //------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "rtx_rtplib.h"
#include "net/socket_address.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//#define STUN_LOGGING  m_log && rtl::Logger::check_trace(TRF_NET)
#define STUN_LOGGING  m_log && rtl::Logger::check_trace(TRF_NET)
#define PLOG_STUN_WRITE		if (STUN_LOGGING)	m_log->log
#define PLOG_STUN_WARNING	if (STUN_LOGGING)	m_log->log_warning
#define PLOG_STUN_ERROR		if (STUN_LOGGING)	m_log->log_error
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class net_stun_message_t;
class rtp_channel_t;
struct rtp_webrtc_subscriber_callback;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class mg_stun_t
{
	mg_stun_t(const mg_stun_t&);
	mg_stun_t& operator=(const mg_stun_t&);

public:
	mg_stun_t(rtl::Logger* log, rtp_channel_t* rtp, rtp_webrtc_subscriber_callback* callback);
	virtual	~mg_stun_t();

	void setup_ice_params_local(const char* l_ufrag, const char* l_pswd);
	void setup_ice_params_remote(const char* r_ufrag, const char* r_pswd);

	// входящий пинг от пира
	void process_stun_message(const uint8_t* packet, int length, const socket_address* addr);
	// отправка понга пиру
	void send_stun_pong(const net_stun_message_t* msg, const socket_address* addr);
	// отправка пинга пиру. понг от пира игнорируем
	void send_stun_ping();
	void send_stun_ping(const socket_address* saddr);

	bool check_change_address();

	// выставляем параметр контроля.
	void add_ice_controll_param(net_stun_message_t* msg);
	// выставляем параметр контроля при пинге.
	void add_ice_controll_param_ping(net_stun_message_t* request);
	// выставляем параметр контроля при понге.
	void add_ice_controll_param_pong(net_stun_message_t* response);

	void update_channel_name(const char* name);

private:
	void stun_ping_received(const socket_address* addr);
	void stun_pong_received(const socket_address* addr);

private:
	rtl::Logger*			m_log;

	rtp_channel_t*	m_rtp;
	rtl::String		m_id;

	rtp_webrtc_subscriber_callback* m_callback;

	/// webRTC
	bool			m_ice_send_ping;		// необходимость отправлять пинги -- выставляется после получения первого пинга от пира
	socket_address	m_ice_pinger_address;	// куда отправлять пинги
	uint32_t		m_ice_last_ping_sent;	// время отправки последнего пинга
	bool			m_ice_proto_google;		// выставляется по первому приходящему пакету. также влияет на включение отключние шифрования
	bool			m_ice_username_google;	// имя пользователя формируется с ':' или без (для googla)
	bool			m_ice_srtp_enabled;		// разрешение шифрования -- chrome 22 работает без шифрования
	bool			m_ice_rtp_send_enabled;	// разрешение на отправку ртп потока
	rtl::String		m_ice_local_ufrag;		// локальная часть username
	rtl::String		m_ice_local_pwd;		// локальный пароль
	rtl::String		m_ice_remote_ufrag;		// часть username от пира
	rtl::String		m_ice_remote_pwd;		// пароль пира
	uint64_t		m_ice_tie_breaker;		// часть айс протокола
	bool			m_ice_stat_printed;		// печать начальных параметров
	int				m_ice_controlling;		// определяет кто контролирует пинг

	bool			m_change_address;
	socket_address	m_remote_address;
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
