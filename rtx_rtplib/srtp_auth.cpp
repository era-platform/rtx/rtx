﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 //--------------------------------------
// Some bookkeeping functions for authentication functions
//--------------------------------------
// based on libsrtp written by
// David A. McGrew
// Cisco Systems, Inc.
//--------------------------------------
#include "stdafx.h"
#include "srtp_auth.h"
//--------------------------------------
//
//--------------------------------------
auth_t::auth_t(int key_len, int out_len) : m_auth_id(0), m_out_len(out_len), m_key_len(key_len), m_prefix_len(0)
{
}
//--------------------------------------
//
//--------------------------------------
auth_t::~auth_t()
{
	m_auth_id = 0;
	m_key_len = m_out_len = m_prefix_len = 0;
}
//--------------------------------------
//
//--------------------------------------
null_auth_ctx_t::null_auth_ctx_t(int key_len, int out_len) : auth_t(key_len, out_len)
{
}
//--------------------------------------
//
//--------------------------------------
null_auth_ctx_t::~null_auth_ctx_t()
{
}
//--------------------------------------
//
//--------------------------------------
bool null_auth_ctx_t::create(const uint8_t *key, int key_len)
{
	// accept any length of key, and do nothing
	return true;
}
//--------------------------------------
//
//--------------------------------------
bool null_auth_ctx_t::compute(const uint8_t *message, int msg_octets, uint8_t *result, int tag_len)
{
	return true;
}
//--------------------------------------
//
//--------------------------------------
bool null_auth_ctx_t::update(const uint8_t *message, int msg_octets)
{
	return true;
}
//--------------------------------------
//
//--------------------------------------
bool null_auth_ctx_t::start()
{
	return true;
}
//--------------------------------------
