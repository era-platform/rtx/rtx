/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtcp_packet.h"
#include "rtcp_packet_XR.h"
#include "rtcp_packet_AVB.h"
//--------------------------------------
//
//--------------------------------------
const char* get_rtcp_type_name(rtcp_type_t type)
{
	const char* names[] = { "rtcp_sr_200", "rtcp_rr_201", "rtcp_sdes_202", "rtcp_bye_203", "rtcp_app_204", "rtcp_rtpfb_205", "rtcp_psfb_206", "rtcp_xr_207", "rtcp_avb_208" };

	return (type >= rtcp_type_sr_e && type <= rtcp_type_avb_e) ? names[type - 200] : "rtcp_unknown";
}
//--------------------------------------
//
//--------------------------------------
const char* get_rtcp_sdes_type_name(rtcp_sdes_type_t type)
{
	const char* names[] = { "rtcp_sdes_end", "rtcp_sdes_cname", "rtcp_sdes_name", "rtcp_sdes_email",
		"rtcp_sdes_phone", "rtcp_sdes_loc", "rtcp_sdes_tool", "rtcp_sdes_note", "rtcp_sdes_priv" };

	return (type >= rtcp_sdes_cname_e && type <= rtcp_sdes_priv_e) ? names[type] : "rtcp_sdes_unknown";
}
//--------------------------------------
//rtcp_rtpfb_unassigned_e = 0,
//rtcp_rtpfb_nack_e = 1,		
//rtcp_rtpfb_tmmbr_e = 3,		
//rtcp_rtpfb_tmmbn_e = 4,		
//rtcp_rtpfb_reserved_e  = 31	
//--------------------------------------
const char* get_rtcp_rtpfb_type_name(rtcp_rtpfb_format_t type)
{
//	const char* names[] = { "rtpfb_unassigned", "rtpfb_nack", "rtpfb_tmmbr", "rtpfb_tmmbn", "rtpfb_reserved" };
	switch (type)
	{
	case rtcp_rtpfb_unassigned_e: return "rtpfb_unassigned";
	case rtcp_rtpfb_nack_e: return "rtpfb_nack";
	case rtcp_rtpfb_tmmbr_e: return "rtpfb_tmmbr";
	case rtcp_rtpfb_tmmbn_e: return "rtpfb_tmmbn";
	case rtcp_rtpfb_transport_cc_e: return "rtpf_transport_cc";
	}

	return type <= rtcp_rtpfb_reserved_e ? "rtpfb_reserved" : "rtpfb_invalid";
}
/*
rtcp_psfb_unassigned_e = 0,
rtcp_psfb_pli_e = 1,
rtcp_psfb_sli_e = 2,
rtcp_psfb_rpsi_e = 3,
rtcp_psfb_fir_e = 4,
rtcp_psfb_tstr_e = 5,
rtcp_psfb_tstn_e = 6,
rtcp_psfb_vbcm_e = 7,
rtcp_psfb_app_e = 15,
rtcp_psfb_reserved_e = 31
*/
const char* get_rtcp_psfb_type_name(rtcp_psfb_format_t type)
{
	const char* names[] = { "psfb_unassigned", "psfb_pli", "psfb_sli", "psfb_rpsi", "psfb_fir", "psfb_tstr", "psfb_tstn", "psfb_vbcm" };

	if (type >= rtcp_psfb_unassigned_e && type <= rtcp_psfb_vbcm_e)
		return names[type];

	if (type == rtcp_psfb_app_e)
		return "psfb_app";

	if (type <= rtcp_psfb_reserved_e)
		return "psfb_reserved";

	return "psfb_invalid";
}
//--------------------------------------
//
//--------------------------------------
static int dump_report(char* out, int size, const rtcp_report_t* rep);
static int dump_binary(char* out, int size, uint8_t* data, int length);
//--------------------------------------
//
//--------------------------------------
rtcp_base_packet_t::rtcp_base_packet_t(rtcp_type_t type)
{
	m_version = RTP_VERSION;
	m_padding = 0;
	m_counter = 0;
	m_type = type;
    m_length = 0;
	m_ssrc = 0;
}
//--------------------------------------
//
//--------------------------------------
rtcp_base_packet_t::~rtcp_base_packet_t()
{
}
//--------------------------------------
//
//--------------------------------------
int rtcp_base_packet_t::read(const uint8_t* in, int length)
{
	if ((uint32_t)length < sizeof(rtcp_header_t))
		return 0;

	const rtcp_header_t* header = (const rtcp_header_t*)in;
	m_version = header->ver;
	m_padding = header->pad;
	m_counter = header->rc;
	m_type = (rtcp_type_t)header->pt;
    m_length = ntohs(header->length);
	m_ssrc = ntohl(header->ssrc);

	return sizeof(rtcp_header_t);
}
//--------------------------------------
//
//--------------------------------------
int rtcp_base_packet_t::write(uint8_t* out, int size)
{
	if ((uint32_t)size < sizeof(rtcp_header_t))
		return 0;
	
	rtcp_header_t* header = (rtcp_header_t*)out;
	header->ver = m_version;
	header->pad = m_padding;
	header->rc = m_counter;
	header->pt = m_type;
	header->length = htons(m_length);
	header->ssrc = htonl(m_ssrc);

	return sizeof(rtcp_header_t);
}
//--------------------------------------
//
//--------------------------------------
int rtcp_base_packet_t::dump(char* out, int size) const
{
	return std_snprintf(out, size, "\t-----------\r\n\tRTCP HEADER : ver:%d, pad:%d, rc:%d, type:%s, len:%d/%d, ssrc:%X(%10u)\r\n",
		m_version, m_padding, m_counter,
		get_rtcp_type_name(m_type), m_length, (m_length + 1) * 4, m_ssrc, m_ssrc);
}
//--------------------------------------
//
//--------------------------------------
rtcp_rr_packet_t::~rtcp_rr_packet_t()
{
}
//--------------------------------------
//
//--------------------------------------
int rtcp_rr_packet_t::read(const uint8_t* in, int length)
{
	int read = 0;

	if (!(read = rtcp_base_packet_t::read(in, length)))
		return 0;

	// ���� ����� ������ ��� �� ����� ���������
	if (length < int(read + m_counter * sizeof(rtcp_report_t)))
		return 0;

	// ���� � ��������� ������ � ������
	if ((m_length + 1) != (2 + m_counter * 6))
		return 0;

	m_report_list.clear();

	for (int i = 0; i < m_counter; i++)
	{
		const uint8_t* report = in + read;
		rtcp_report_t to_add;

		to_add.ssrc = ntohl(*(const uint32_t*)report);

		to_add.fraction_lost = report[4];
		to_add.packets_lost[0] = report[5];
		to_add.packets_lost[1] = report[6];
		to_add.packets_lost[2] = report[7];

		to_add.ext_seq = ntohs(*(const uint16_t*)(report+8));
		to_add.last_seq = ntohs(*(const uint16_t*)(report+10));

		to_add.jitter = ntohl(*(const uint32_t*)(report+12));
		to_add.lsr = ntohl(*(const uint32_t*)(report+16));
		to_add.dlsr = ntohl(*(const uint32_t*)(report+20));

		m_report_list.add(to_add);

		read += sizeof(rtcp_report_t); // ����������
	}

	return read;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_rr_packet_t::write(uint8_t* out, int size)
{
	int written = 0;

	// set rtcp header values
	m_counter = m_report_list.getCount();
	// calculate length
	// rtcp header + ssrc + report count * sizeof report
	int to_write = 4 + 4 + m_counter * sizeof (rtcp_report_t);
	m_length = (to_write >> 2) - 1; // in 4-byte words minus one

	if (size < to_write)
		return 0;
	
	written = rtcp_base_packet_t::write(out, size);

	for (int i = 0; i < m_counter; i++)
	{
		const rtcp_report_t& l_report = m_report_list[i];
		uint8_t* report = out + written;

		*(uint32_t*)report = htonl(l_report.ssrc);

		report[4] = l_report.fraction_lost;
		report[5] = l_report.packets_lost[0];
		report[6] = l_report.packets_lost[1];
		report[7] = l_report.packets_lost[2];

		*(uint16_t*)(report+8) = htons(l_report.ext_seq);
		*(uint16_t*)(report+10) = htons(l_report.last_seq);

		*(uint32_t*)(report+12) = htonl(l_report.jitter);
		*(uint32_t*)(report+16) = htonl(l_report.lsr);
		*(uint32_t*)(report+20) = htonl(l_report.dlsr);

		written += sizeof(rtcp_report_t); // ����������
	}

	return written;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_rr_packet_t::dump(char* out, int size) const
{
	int written = rtcp_base_packet_t::dump(out, size);

	written += std_snprintf(out + written , size - written, "\tRR HEADER   :\r\n");

	for (int i = 0; i < m_report_list.getCount(); i++)
	{
		const rtcp_report_t& rep = m_report_list[i];
		written += dump_report(out + written, size - written, &rep);
	}

	return written;
}
//--------------------------------------
//
//--------------------------------------
bool rtcp_rr_packet_t::add_report(const rtcp_report_t& report)
{
	if (m_counter < 31)
	{
		m_report_list.add(report);
		m_counter++;
		
		return true;
	}

	return false;
}
//--------------------------------------
//
//--------------------------------------
rtcp_sr_packet_t::rtcp_sr_packet_t() : rtcp_rr_packet_t(rtcp_type_sr_e)
{
	m_ntp_timestamp = 0;
	m_ntp_fraction = 0;
	m_rtp_timestamp = 0;
	m_packets_sent = 0;
	m_octets_sent = 0;
}
//--------------------------------------
//
//--------------------------------------
rtcp_sr_packet_t::~rtcp_sr_packet_t()
{
}
//--------------------------------------
//
//--------------------------------------
int rtcp_sr_packet_t::read(const uint8_t* in, int length)
{
	int read = 0;

	if (!(read = rtcp_base_packet_t::read(in, length)))
		return 0;

	// sender info + report-count * sizeof report
	// ���� ����� ������ ��� �� ����� ���������
	if (length < int(read + 20 + m_counter * sizeof(rtcp_report_t)))
		return 0;

	// ���� � ��������� ������ � ������
	if ((m_length + 1) != (7 + m_counter * 6))
		return 0;

	const rtcp_sender_t* sender = (const rtcp_sender_t*)(in + read);

	m_ntp_timestamp = ntohl(sender->ntp_timestamp);
	m_ntp_fraction = ntohl(sender->ntp_fraction);
	m_rtp_timestamp = ntohl(sender->rtp_timestamp);
	m_packets_sent = ntohl(sender->packets_sent);
	m_octets_sent = ntohl(sender->octets_sent);
	
	read += 20;
	
	m_report_list.clear();

	for (int i = 0; i < m_counter; i++)
	{
		const uint8_t* report = in + read;
		rtcp_report_t to_add;

		to_add.ssrc = ntohl(*(const uint32_t*)report);

		to_add.fraction_lost = report[4];
		to_add.packets_lost[0] = report[5];
		to_add.packets_lost[1] = report[6];
		to_add.packets_lost[2] = report[7];

		to_add.ext_seq = ntohs(*(const uint16_t*)(report+8));
		to_add.last_seq = ntohs(*(const uint16_t*)(report+10));

		to_add.jitter = ntohl(*(const uint32_t*)(report+12));
		to_add.lsr = ntohl(*(const uint32_t*)(report+16));
		to_add.dlsr = ntohl(*(const uint32_t*)(report+20));
		
		m_report_list.add(to_add);

		read += 24; // ����������
	}

	return read;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_sr_packet_t::write(uint8_t* out, int size)
{
	int written = 0;

	// set rtcp header values
	m_counter = m_report_list.getCount();
	// calculate length
	// rtcp header + ssrc + report count * sizeof report
	int to_write = 4 + 4 + 20 + m_counter * sizeof (rtcp_report_t);
	m_length = (to_write >> 2) - 1; // in 4-byte words

	if (size < to_write)
		return 0;
	
	written = rtcp_base_packet_t::write(out, size);

	rtcp_sender_t* sender = (rtcp_sender_t*)(out + written);

	sender->ntp_timestamp = htonl(m_ntp_timestamp);
	sender->ntp_fraction = htonl(m_ntp_fraction);
	sender->rtp_timestamp = htonl(m_rtp_timestamp);
	sender->packets_sent = htonl(m_packets_sent);
	sender->octets_sent = htonl(m_octets_sent);
	
	written += 20;

	
	
	for (int i = 0; i < m_counter; i++)
	{
		uint8_t* report = out + written;
		const rtcp_report_t& l_report = m_report_list[i];
		
		*(uint32_t*)report = htonl(l_report.ssrc);

		report[4] = l_report.fraction_lost;
		report[5] = l_report.packets_lost[0];
		report[6] = l_report.packets_lost[1];
		report[7] = l_report.packets_lost[2];

		*(uint16_t*)(report+8) = htons(l_report.ext_seq);
		*(uint16_t*)(report+10) = htons(l_report.last_seq);

		*(uint32_t*)(report+12) = htonl(l_report.jitter);
		*(uint32_t*)(report+16) = htonl(l_report.lsr);
		*(uint32_t*)(report+20) = htonl(l_report.dlsr);

		written += sizeof(rtcp_report_t); // ����������
	}

	return written;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_sr_packet_t::dump(char* out, int size) const
{
	int written = rtcp_base_packet_t::dump(out, size);
		
	written += std_snprintf(out + written , size - written, "\tSR HEADER   : ntp ts:%08X, ntp frac:%08X, rtp ts:%08X, packets sent:%u, octets sent:%u\r\n",
		m_ntp_timestamp, m_ntp_fraction, m_rtp_timestamp, m_packets_sent, m_octets_sent);

	for (int i = 0; i < m_report_list.getCount(); i++)
	{
		const rtcp_report_t& rep = m_report_list[i];
		written += dump_report(out + written, size - written, &rep);
	}

	return written;
};
//--------------------------------------
//
//--------------------------------------
int rtcp_chunk_t::read(const uint8_t* in, int length)
{
	int read = 0;

	const rtcp_sdes_chunk_t* chunk = (const rtcp_sdes_chunk_t*)in;
	const rtcp_sdes_item_t* read_item = chunk->items;

	m_ssrc = ntohl(chunk->ssrc);

	read += 4;
		
	while (read_item->type != rtcp_sdes_end_e)
	{
		add(read_item);
			
		int read_len = read_item->length + 2;
		read += read_len;
			
		if (length - read <= 0)
			break;

		read_item = (const rtcp_sdes_item_t*)(((const uint8_t*)read_item) + read_len);
	}

    read++;

	int pad = read & 3;
	if (pad > 0)
		read += 4 - pad;

	return read;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_chunk_t::write(uint8_t* out, int size)
{
	if (size < 8)
		return 0;

	int written = 0;

	*(uint32_t*)out = htonl(m_ssrc);
	written += sizeof(uint32_t);

	for (int i = 0; i < m_items.getCount(); i++)
	{
		rtcp_sdes_item_t* item = m_items[i];
		if (size - written >= item->length + 2)
		{
			*(out + written++) = item->type;
			*(out + written++) = item->length;
			memcpy(out + written, item->data, item->length);
			written += item->length;
		}
	}

	out[written++] = 0; // writing zero terminal sdes item type without length

	int pad = written & 3;
	if (pad > 0)
	{
		for (int i = 0; i < 4 - pad; i++)
		{
			out[written++] = 0;
		}
	}

	return written;
}
//--------------------------------------
//
//--------------------------------------
bool rtcp_chunk_t::add(rtcp_sdes_type_t type, const void* data, int length)
{
	if (type == rtcp_sdes_end_e || data == nullptr || length < 0)
		return false;

	rtcp_sdes_item_t* item = (rtcp_sdes_item_t*)MALLOC(length+3);

	item->type = type;
	item->length = length;

	memcpy(item->data, data, length);

	item->data[length] = 0;
	m_items.add(item);
	return true;
}
//--------------------------------------
//
//--------------------------------------
bool rtcp_chunk_t::set(rtcp_sdes_type_t type, int index, const void* data, int length)
{
	if (type == rtcp_sdes_end_e)
		return false;

	int item_ref = 0;
	for (int i = 0; i < m_items.getCount(); i++)
	{
		rtcp_sdes_item_t* item = m_items[i];
		
		if (item->type == type)
		{
			if (item_ref == index)
			{
				if (item->length < length)
				{
					FREE(item);
					item = (rtcp_sdes_item_t*)MALLOC(length + 2);
					item->type = type;
					item->length = length;
					memcpy(item->data, data, length);
					m_items.setAt(i, item);
				}
				else
				{
					item->length = length;
					memcpy(item->data, data, length);
				}
				return true;
			}

			item_ref++;
		}
	}

	return false;
}
//--------------------------------------
//
//--------------------------------------
bool rtcp_chunk_t::remove(rtcp_sdes_type_t type, int index)
{
	int item_ref = 0;
	for (int i = 0; i < m_items.getCount(); i++)
	{
		rtcp_sdes_item_t* item = m_items[i];
		
		if (item->type == type)
		{
			if (item_ref == index)
			{
				FREE(item);
				m_items.removeAt(i);
				return true;
			}
			
			item_ref++;
		}
	}

	return false;
}
//--------------------------------------
//
//--------------------------------------
void rtcp_chunk_t::cleanup()
{
	for (int i = 0; i < m_items.getCount(); i++)
	{
		rtcp_sdes_item_t* item = m_items.getAt(i);
		FREE(item);
	}

	m_items.clear();
	m_ssrc = 0;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_chunk_t::calculate_length()
{
	int length = 4; // ssrc
	
	for (int i = 0; i < m_items.getCount(); i++)
	{
		length += m_items[i]->length + 2;
	}

	length++;

	int pad = length & 3;
	if (pad > 0)
		length += 4 - pad;

	return length;
}
//--------------------------------------
//
//--------------------------------------
rtcp_sdes_packet_t::~rtcp_sdes_packet_t()
{
	cleanup();
}
//--------------------------------------
//
//--------------------------------------
void rtcp_sdes_packet_t::cleanup()
{
	for (int i = 0; i < m_chunk_list.getCount(); i++)
	{
		rtcp_chunk_t* sdes = m_chunk_list[i];
		DELETEO(sdes);
	}

	m_chunk_list.clear();
}
//--------------------------------------
//
//--------------------------------------
int rtcp_sdes_packet_t::read(const uint8_t* in, int length)
{
	int read = 0;

	cleanup();

	if (!(read = rtcp_base_packet_t::read(in, length)))
		return 0;

	if (length < 9)
		return 0;

	read -= 4;

	int read_chank_count = 0;

	while (length - read >= 8 && read_chank_count < m_counter)
	{
		rtcp_chunk_t* chunk = NEW rtcp_chunk_t;
		
		read += chunk->read(in + read, length - read);

		m_chunk_list.add(chunk);
		
		read_chank_count++;
	}

	return read;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_sdes_packet_t::write(uint8_t* out, int size)
{
	int written = 0;
	int to_write = 4;

	m_counter = m_chunk_list.getCount();

	for (int i = 0; i < m_chunk_list.getCount(); i++)
	{
		to_write += m_chunk_list[i]->calculate_length();
	}

	if (to_write > size)
		return 0;

	m_length = (to_write >> 2) - 1;

	written = rtcp_base_packet_t::write(out, size);
	written -= 4;

	for (int i = 0; i < m_chunk_list.getCount(); i++)
	{
		rtcp_chunk_t* chunk = m_chunk_list[i];
		written += chunk->write(out + written, size - written);
	}

	return written;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_sdes_packet_t::dump(char* out, int size) const
{
	int written = rtcp_base_packet_t::dump(out, size);

	written += std_snprintf(out + written , size - written, "\tSDES HEADER :\r\n");

	for (int i = 0; i < m_chunk_list.getCount(); i++)
	{
		rtcp_chunk_t* sdes_chank = m_chunk_list[i];
		written += std_snprintf(out + written, size - written, "\tSDES CHANK  : ssrc:%X\r\n", sdes_chank->get_ssrc());

		for (int j = 0; j < sdes_chank->getCount(); j++)
		{
			const rtcp_sdes_item_t* item = sdes_chank->get(j);
			if (item != nullptr)
				written += std_snprintf(out + written, size - written, "\tSDES ITEM   : type:%s, len:%d, value:%s\r\n", get_rtcp_sdes_type_name((rtcp_sdes_type_t)item->type), item->length, item->data);
			else
				written += std_snprintf(out + written, size - written, "\tSDES ITEM   : type:(null)\r\n");
		}
	}

	return written;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_sdes_packet_t::get_chunk_index(uint32_t chank_ssrc) const
{
	for (int i = 0; i < m_chunk_list.getCount(); i++)
	{
		if (m_chunk_list[i]->get_ssrc() == chank_ssrc)
			return i;
	}

	return -1;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_sdes_packet_t::get_chunk_item_count(int chank_idx) const
{
	rtcp_chunk_t* chank = m_chunk_list[chank_idx];

	return chank != nullptr ? chank->getCount() : 0;
}
//--------------------------------------
//
//--------------------------------------
const rtcp_sdes_item_t* rtcp_sdes_packet_t::get_chunk_item(int chank_idx, rtcp_sdes_type_t type, int item_idx) const
{
	rtcp_chunk_t* chank = m_chunk_list[chank_idx];

	if (chank != nullptr)
	{
		int item_ref = 0;
		
		for (int i = 0; i < chank->getCount(); i++)
		{
			rtcp_sdes_item_t* item = chank->get(i);
			if (item->type == type)
			{
				if (item_ref == item_idx)
					return item;
				
				item_ref++;
			}
		}
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
const rtcp_sdes_item_t* rtcp_sdes_packet_t::get_chunk_item_at(int chank_idx, int item_idx) const
{
	rtcp_chunk_t* chank = m_chunk_list[chank_idx];
	return chank != nullptr ? chank->get(item_idx) : nullptr;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_sdes_packet_t::add_chunk(uint32_t ssrc)
{
	if (m_counter >= 31)
		return -1;

	rtcp_chunk_t* chank = NEW rtcp_chunk_t;
	chank->set_ssrc(ssrc);
	
	return m_chunk_list.add(chank);
}
//--------------------------------------
//
//--------------------------------------
bool rtcp_sdes_packet_t::remove_chunk(int index)
{
	rtcp_chunk_t* chank = m_chunk_list[index];

	if (chank == nullptr)
		return false;

	m_chunk_list.removeAt(index);
	DELETEO(chank);

	return true;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_sdes_packet_t::add_chunk_item(int chank_idx, rtcp_sdes_type_t type, const char* data, int length)
{
	rtcp_chunk_t* chank = m_chunk_list[chank_idx];

	if (chank == nullptr)
		return -1;

	return chank->add(type, data, length);
}
//--------------------------------------
//
//--------------------------------------
bool rtcp_sdes_packet_t::set_chunk_item(int chank_idx, rtcp_sdes_type_t type, int index, const char* data, int length)
{
	rtcp_chunk_t* chank = m_chunk_list[chank_idx];

	if (chank == nullptr)
		return false;

	return chank->set(type, index, data, length);
}
//--------------------------------------
//
//--------------------------------------
bool rtcp_sdes_packet_t::remove_chunk_item(int chank_idx, rtcp_sdes_type_t type, int index)
{
	rtcp_chunk_t* chank = m_chunk_list[chank_idx];

	if (chank == nullptr)
		return false;

	return chank->remove(type, index);
}
//--------------------------------------
//
//--------------------------------------
rtcp_bye_packet_t::~rtcp_bye_packet_t()
{
}
//--------------------------------------
//
//--------------------------------------
int rtcp_bye_packet_t::read(const uint8_t* in, int length)
{
	cleanup();

	int read = 0;

	if (!(read = rtcp_base_packet_t::read(in, length)))
		return 0;

	if (length < int(4 + m_counter * sizeof(uint32_t)))
		return 0;

	int to_read = (m_length + 1) << 2; // bytes

	if (to_read > length)
		return 0;

	const uint32_t* ssrc_ptr = (const uint32_t*)(in + read);

	// ������� �������������� ����������
	for (int i = 1; i < m_counter; i++)
	{
		m_bye_list.add(*ssrc_ptr++);
		read += sizeof(uint32_t);
	}

	// ���� ���� ����� ������� ����� �� ������� ���
	if (to_read > read)
	{
		uint8_t reason_len = *(in + read);
		if (reason_len > 0)
			m_reason.assign((char*)(in + read + 1), reason_len);

		read += reason_len + 1;
	}

	return read;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_bye_packet_t::write(uint8_t* out, int size)
{
	int written = 0;
	int reason_len = m_reason.getLength();

	if (reason_len > 0)
		reason_len++;

	m_counter = m_bye_list.getCount() + 1;

	int to_write = 4 + m_counter * sizeof(uint32_t) + reason_len;
	
	int pad = to_write & 3;
	if (pad > 0)
	{
		to_write += 4 - pad;
	}
	
	m_length = (to_write >> 2) - 1;

	if (size < to_write)
		return 0;

	written = rtcp_base_packet_t::write(out, size);

	uint32_t* ssrc_ptr = (uint32_t*)(out + written);

	for (int i = 0; i < m_bye_list.getCount(); i++)
		*ssrc_ptr++ = m_bye_list[i];

	written += m_bye_list.getCount() * sizeof(uint32_t);

	if (!m_reason.isEmpty())
	{
		reason_len = m_reason.getLength() + 1;
		pad = reason_len & 3;
		if (pad > 0)
			reason_len += 4 - pad;

		*(out + written) = m_reason.getLength();
		memcpy(out + written + 1, m_reason, m_reason.getLength()+1);

		written += reason_len;
	}

	return written;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_bye_packet_t::dump(char* out, int size) const
{
	int written = rtcp_base_packet_t::dump(out, size);

	for (int i = 1; i < m_bye_list.getCount(); i++)
	{
		written += std_snprintf(out + written, size - written, "\tBYE HEADER  : ssrc:%X\r\n", m_bye_list[i]);
	}

	if (!m_reason.isEmpty())
	{
		written += std_snprintf(out + written, size - written, "\tBYE REASON  : %s\r\n", (const char*)m_reason);
	}

	return written;
}
//--------------------------------------
//
//--------------------------------------
bool rtcp_bye_packet_t::add_source(uint32_t ssrc)
{
	if (m_bye_list.getCount() >= 31)
		return false;

	bool found = false;

	for (int i = 0; i < m_bye_list.getCount(); i++)
	{
		found = m_bye_list[i] == ssrc;
		if (found)
		{
			break;
		}
	}
	
	if (!found)
		m_bye_list.add(ssrc);

	return !found; // i.e. added
}
//--------------------------------------
//
//--------------------------------------
rtcp_app_packet_t::rtcp_app_packet_t() : rtcp_base_packet_t(rtcp_type_app_e),
	m_sign(0), m_data_length(0), m_data(nullptr)
{
}
//--------------------------------------
//
//--------------------------------------
rtcp_app_packet_t::~rtcp_app_packet_t()
{
	if (m_data != nullptr)
		FREE(m_data);
}
//--------------------------------------
//
//--------------------------------------
int rtcp_app_packet_t::read(const uint8_t* in, int length)
{
	m_sign = 0;
	set_data(nullptr, 0);

	// rtcp header + ssrc + add sign
	if (length < 12)
		return 0;

	int read = rtcp_base_packet_t::read(in, length);

	m_sign = *(const uint32_t*)(in + read);
	read += 4;

	int to_read = (m_length + 1) << 2;

	if (to_read > length)
		return 0;
	
	int data_length = to_read - read;

	if (data_length + read > length)
		data_length = length - read;

	set_data(in + read, data_length);
	
	read += data_length;
	
	return read;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_app_packet_t::write(uint8_t* out, int size)
{
	int to_write = 12 + m_data_length;

	int pad = to_write & 3;
	if (pad > 0)
		to_write += 4 - pad;

	if (to_write > size)
		return 0;

	m_length = (to_write >> 2) - 1;

	int written = rtcp_base_packet_t::write(out, size);

	*(uint32_t*)(out + written) = m_sign;
	written += 4;
	
	memcpy(out + written, m_data, m_data_length);
	written += m_data_length;

	if (pad > 0)
	{
		for (int i = 0; i < 4 - pad; i++)
		{
			out[written++] = 0;
		}
	}

	return to_write;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_app_packet_t::dump(char* out, int size) const
{
	int written = rtcp_base_packet_t::dump(out, size);

	written += std_snprintf(out + written, size - written, "\tAPP HEADER  : sign:%X\r\n", m_sign);
	written += std_snprintf(out + written, size - written, "\tAPP DATA    : len:%d, data:%s\r\n", m_data_length, (const char*)m_data);

	return written;
}
//--------------------------------------
//
//--------------------------------------
void rtcp_app_packet_t::set_data(const void* data, int length)
{
	if (m_data != nullptr)
		FREE(m_data);

	m_data_length = 0;

	if (length > 0 && data != nullptr)
	{
		m_data = MALLOC(length);
		memcpy(m_data, data, m_data_length = length);
	}
}
//--------------------------------------
//
//--------------------------------------
rtcp_rtpfb_packet_t::rtcp_rtpfb_packet_t() : rtcp_base_packet_t(rtcp_type_rtpfb_e), m_media_source(0)
{
	m_counter = rtcp_rtpfb_unassigned_e;
}
//--------------------------------------
//
//--------------------------------------
rtcp_rtpfb_packet_t::~rtcp_rtpfb_packet_t()
{
}
//--------------------------------------
//
//--------------------------------------
int rtcp_rtpfb_packet_t::read_nack(const uint8_t* in, int length)
{
	rtpfb_entry_t entry;
	const rtcp_rtpfb_nack_t* nack = (const rtcp_rtpfb_nack_t*)in;
	int read = 0;

	do
	{
		entry.nack.pid = ntohs(nack->pid);
		entry.nack.blp = ntohs(nack->blp);
		m_entry_list.add(entry);

		nack++;
		read += sizeof(rtcp_rtpfb_nack_t);
	}
	while (read < length);

	return read;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_rtpfb_packet_t::read_tmm(const uint8_t* in, int length)
{
	rtpfb_entry_t entry;
	const rtcp_rtpfb_tmm_t* tmm = (const rtcp_rtpfb_tmm_t*)in;
	int read = 0;

	do
	{
		entry.tmm.ssrc = ntohl(tmm->ssrc);
		entry.tmm.exp = tmm->exp;
		entry.tmm.mantissa = tmm->mantissa;
		entry.tmm.overhead = tmm->overhead;

		m_entry_list.add(entry);

		tmm++;
		read += sizeof(rtcp_rtpfb_tmm_t);
	}
	while (read < length);

	return read;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_rtpfb_packet_t::read_transport_cc(const uint8_t* in, int length)
{
	rtpfb_entry_t entry;
	const rtcp_rtpfb_transport_cc_t* cc = (const rtcp_rtpfb_transport_cc_t*)in;
	
	int read = 0;

	entry.cc.bsn = ntohs(cc->bsn);
	entry.cc.psc = ntohs(cc->psc);
	entry.cc.rt = ntohl(cc->rt);
	entry.cc.fbpktc = cc->fbpktc;
	
	m_entry_list.add(entry);
	uint32_t real_length = (m_length + 1) * 4;	
	read += sizeof(rtcp_rtpfb_transport_cc_t);
	
	m_buff_transport_cc_size = (real_length - sizeof(rtcp_rtpfb_transport_cc_t) - sizeof(rtcp_header_t) - sizeof(uint32_t));
	if (sizeof(m_buff_transport_cc) <= m_buff_transport_cc_size)
	{
		return 0;
	}

	memset(m_buff_transport_cc, 0, sizeof(m_buff_transport_cc));
	memcpy(m_buff_transport_cc, in + read, m_buff_transport_cc_size);
	read += m_buff_transport_cc_size;

	return read;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_rtpfb_packet_t::write_nack(uint8_t* out, int size)
{
	int written = 0;
	
	rtcp_rtpfb_nack_t* nack = (rtcp_rtpfb_nack_t*)out;

	for (int i = 0; i < m_entry_list.getCount(); i++)
	{
		rtpfb_entry_t& entry = m_entry_list[i];

		nack->pid = htons(entry.nack.pid);
		nack->blp = htons(entry.nack.blp);

		nack++;
		written += sizeof(rtcp_rtpfb_nack_t);
	}

	return written;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_rtpfb_packet_t::write_tmm(uint8_t* out, int size)
{
	int written = 0;
	
	rtcp_rtpfb_tmm_t* tmm = (rtcp_rtpfb_tmm_t*)out;

	for (int i = 0; i < m_entry_list.getCount(); i++)
	{
		rtpfb_entry_t& entry = m_entry_list[i];
		tmm->ssrc = htonl(entry.tmm.ssrc);
		tmm->exp = entry.tmm.exp;
		tmm->mantissa = entry.tmm.mantissa;
		tmm->overhead = entry.tmm.overhead;

		tmm++;
		written += sizeof(rtcp_rtpfb_tmm_t);
	}

	return written;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_rtpfb_packet_t::write_transport_cc(uint8_t* out, int size)
{
	int written = 0;

	rtcp_rtpfb_transport_cc_t* cc = (rtcp_rtpfb_transport_cc_t*)out;

	rtpfb_entry_t& entry = m_entry_list[0];
	cc->bsn = htons(entry.cc.bsn);
	cc->psc = htons(entry.cc.psc);
	cc->rt = htonl(entry.cc.rt);
	cc->fbpktc = entry.cc.fbpktc;
	
	written += sizeof(rtcp_rtpfb_transport_cc_t);

	memcpy(out + written, m_buff_transport_cc, m_buff_transport_cc_size);
	written += m_buff_transport_cc_size;
		
	return written;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_rtpfb_packet_t::read(const uint8_t* in, int length)
{
	m_media_source = 0;
	m_entry_list.clear();

	// rtcp header + ssrc + add sign
	if (length < 16)
		return 0;

	int read = rtcp_base_packet_t::read(in, length);

	m_media_source = ntohl(*(const uint32_t*)(in + read));
	read += 4;

	int to_read = (m_length + 1) << 2;

	if (to_read > length)
		return 0;
	
	if ((rtcp_rtpfb_format_t)m_counter == rtcp_rtpfb_nack_e)
	{
		read += read_nack(in + read, to_read - read);
	}
	else if ((rtcp_rtpfb_format_t)m_counter == rtcp_rtpfb_tmmbr_e ||
		(rtcp_rtpfb_format_t)m_counter == rtcp_rtpfb_tmmbn_e)
	{
		read += read_tmm(in + read, to_read - read);
	}
	else if ((rtcp_rtpfb_format_t)m_counter == rtcp_rtpfb_transport_cc_e)
	{
		read += read_transport_cc(in + read, to_read - read);
	}

	return read;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_rtpfb_packet_t::write(uint8_t* out, int size)
{
	int to_write = 12;

	if ((rtcp_rtpfb_format_t)m_counter == rtcp_rtpfb_nack_e)
	{
		to_write += m_entry_list.getCount() * sizeof(rtcp_rtpfb_nack_t);
	}
	else if ((rtcp_rtpfb_format_t)m_counter == rtcp_rtpfb_transport_cc_e)
	{
		to_write = sizeof(rtcp_rtpfb_transport_cc_t) + m_buff_transport_cc_size + sizeof(rtcp_header_t) + sizeof(uint32_t);
	}
	else if ((rtcp_rtpfb_format_t)m_counter == rtcp_rtpfb_tmmbr_e ||
		(rtcp_rtpfb_format_t)m_counter == rtcp_rtpfb_tmmbn_e)
	{
		to_write += m_entry_list.getCount() * sizeof(rtcp_rtpfb_tmm_t);
	}
	else
	{
		return 0;
	}

	if (to_write > size)
		return 0;

	m_length = (to_write >> 2) - 1;

	int written = rtcp_base_packet_t::write(out, size);

	*(uint32_t*)(out + written) = htonl(m_media_source);
	written += 4;

	if ((rtcp_rtpfb_format_t)m_counter == rtcp_rtpfb_nack_e)
	{
		written += write_nack(out + written, size - written);
	}
	else if ((rtcp_rtpfb_format_t)m_counter == rtcp_rtpfb_transport_cc_e)
	{
		written += write_transport_cc(out + written, size - written);
	}
	else if ((rtcp_rtpfb_format_t)m_counter == rtcp_rtpfb_tmmbr_e ||
		(rtcp_rtpfb_format_t)m_counter == rtcp_rtpfb_tmmbn_e)
	{
		written += write_tmm(out + written, size - written);
	}

	return written;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_rtpfb_packet_t::dump(char* out, int size) const
{
	int written = rtcp_base_packet_t::dump(out, size);

	written += std_snprintf(out + written, size - written, "\tFB HEADER  : len: %d(%d), type: %s, mssrc:%X(%10u)\r\n", m_length, m_buff_transport_cc_size, get_rtcp_rtpfb_type_name((rtcp_rtpfb_format_t)m_counter), m_media_source, m_media_source);

	for (int i = 0; i < m_entry_list.getCount(); i++)
	{
		switch (get_fb_type())
		{
		case rtcp_rtpfb_nack_e:
			{
				const rtcp_rtpfb_nack_t* nack = get_nack_at(i);
				written += std_snprintf(out + written, size - written, "\tNACK HEADER : idx:%d, pid:%d, blp:%d\r\n", i, nack->pid, nack->blp);
			}
			break;
		case rtcp_rtpfb_tmmbr_e:
		case rtcp_rtpfb_tmmbn_e:
			{
				const rtcp_rtpfb_tmm_t* tmm = get_tmm_at(i);
				if (tmm == nullptr)
				{
					break;
				}
				written += std_snprintf(out + written, size - written, "\tTMM HEADER  : idx:%d, ssrc:%X, exp:%d, mantissa:%d, overhead:%d\r\n", i, tmm->ssrc, tmm->exp, tmm->mantissa, tmm->overhead);
			}
			break;
		case rtcp_rtpfb_transport_cc_e:
			{
				const rtcp_rtpfb_transport_cc_t* cc = get_cc_at(i);
				if (cc == nullptr)
				{
					break;
				}
				written += std_snprintf(out + written, size - written, "\tCC HEADER  : idx:%d, base seq num:%d, pkt count:%d, reference time:%d, fb pkt. count:%d\r\n", i, cc->bsn, cc->psc, cc->rt, cc->fbpktc);
				const uint16_t* chank = (const uint16_t*)m_buff_transport_cc;
				const uint8_t* end = (m_buff_transport_cc + m_buff_transport_cc_size);
				while (*chank != 0 && (const uint8_t*)chank < end)
				{
					uint16_t bin = ntohs(*chank++);
					rtcp_rtpfb_transport_cc_rl_t* rle = (rtcp_rtpfb_transport_cc_rl_t*)&bin;// chank++;

					if (!rle->t)
					{
						
						written += std_snprintf(out + written, size - written, "\t\t\trun len : (%04X) T:%d, S:%d, RL:%d\r\n", bin, rle->t, rle->s, rle->l);
					}
					else 
					{
						rtcp_rtpfb_transport_cc_stat_t* st = (rtcp_rtpfb_transport_cc_stat_t*)rle;
						written += std_snprintf(out + written, size - written, "\t\t\tstatus : (%04X) T:%d, S:%d, bits:%04x\r\n", bin, st->t, st->s, st->b & 0x3fff);
					}
				}

				uint8_t* delta = (uint8_t*)(chank+1);
				int mean = m_buff_transport_cc_size - int(delta - m_buff_transport_cc);
				written += std_snprintf(out + written, size - written, "\t\t\t----- mean: %d\r\n", mean);
				if (mean > 0)
					for (int j = 0; j < mean; j++)
					{
						written += std_snprintf(out + written, size - written, "\t\t\tdeltas[%d] : 0x%02x\r\n", j, *delta++);
					}
			}
			break;
		}
	}

	return written;
}
//--------------------------------------
//
//--------------------------------------
void rtcp_rtpfb_packet_t::set_fb_type(rtcp_rtpfb_format_t type)
{
	if (type == (rtcp_rtpfb_format_t)m_counter)
		return;

	m_entry_list.clear();
	m_counter = type;
}
//--------------------------------------
//
//--------------------------------------
const rtcp_rtpfb_nack_t* rtcp_rtpfb_packet_t::get_nack_at(int index) const
{
	if ((rtcp_rtpfb_format_t)m_counter != rtcp_rtpfb_nack_e)
	{
		return nullptr;
	}

	if ((index < 0) || (index >= m_entry_list.getCount()))
	{
		return nullptr;
	}
	
	return &m_entry_list[index].nack;
}
//--------------------------------------
//
//--------------------------------------
const rtcp_rtpfb_tmm_t* rtcp_rtpfb_packet_t::get_tmm_at(int index) const
{
	if ((rtcp_rtpfb_format_t)m_counter != rtcp_rtpfb_tmmbn_e)
	{
		return nullptr;
	}

	if ((rtcp_rtpfb_format_t)m_counter != rtcp_rtpfb_tmmbr_e)
	{
		return nullptr;
	}

	if ((index < 0) || (index >= m_entry_list.getCount()))
	{
		return nullptr;
	}
	
	return &m_entry_list[index].tmm;
}
//--------------------------------------
//
//--------------------------------------
const rtcp_rtpfb_transport_cc_t* rtcp_rtpfb_packet_t::get_cc_at(int index) const
{
	if ((rtcp_rtpfb_format_t)m_counter != rtcp_rtpfb_transport_cc_e)
	{
		return nullptr;
	}

	if ((index < 0) || (index >= m_entry_list.getCount()))
	{
		return nullptr;
	}

	return &m_entry_list[index].cc;
}
//--------------------------------------
//
//--------------------------------------
void rtcp_rtpfb_packet_t::add_nack(const uint16_t* seq_nums, int count)
{
	if ((rtcp_rtpfb_format_t)m_counter != rtcp_rtpfb_nack_e)
	{
		return;
	}

	rtpfb_entry_t entry;

	entry.nack.pid = htons(seq_nums[0]);
	entry.nack.blp = 0;
	for (int i = 1; i < count; i++)
	{
		int offset = seq_nums[i] - entry.nack.pid;
		entry.nack.blp |= 1 << (16 - offset - 1);
	}

	m_entry_list.add(entry);
}
//--------------------------------------
//
//--------------------------------------
void rtcp_rtpfb_packet_t::add_tmm(rtcp_rtpfb_tmm_t* tmm)
{
	if ((rtcp_rtpfb_format_t)m_counter != rtcp_rtpfb_tmmbr_e &&
		(rtcp_rtpfb_format_t)m_counter != rtcp_rtpfb_tmmbn_e)
	{
		return;
	}

	rtpfb_entry_t entry;
	entry.tmm = *tmm;

	m_entry_list.add(entry);
}
//--------------------------------------
//
//--------------------------------------
rtcp_psfb_packet_t::rtcp_psfb_packet_t() : rtcp_base_packet_t(rtcp_type_psfb_e), m_media_source(0), m_rpsi(nullptr), m_rpsi_length(0)
{
	m_counter = rtcp_psfb_unassigned_e;
}
//--------------------------------------
//
//--------------------------------------
rtcp_psfb_packet_t::~rtcp_psfb_packet_t()
{
	cleanup();
}
//--------------------------------------
//
//--------------------------------------
int rtcp_psfb_packet_t::read_sli(const uint8_t* in, int length)
{
	psfb_entry_t entry;
	const rtcp_psfb_sli_t* sli = (const rtcp_psfb_sli_t*)in;
	int read = 0;

	do
	{
		entry.sli = *sli;
		m_entry_list.add(entry);

		sli++;
		read += sizeof(rtcp_psfb_sli_t);
	}
	while (read < length);

	return read;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_psfb_packet_t::read_rpsi(const uint8_t* in, int length)
{
	if (m_rpsi != nullptr)
	{
		FREE(m_rpsi);
		m_rpsi = nullptr;
		m_rpsi_length = 0;
	}

	if (length > 0)
	{
		const rtcp_psfb_rpsi_t* rpsi = (const rtcp_psfb_rpsi_t*)in;
		//int read = 0;

		m_rpsi = (rtcp_psfb_rpsi_t*)MALLOC(length);
		memcpy(m_rpsi, rpsi, m_rpsi_length = length);
	}

	return length;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_psfb_packet_t::read_fir(const uint8_t* in, int length)
{
	psfb_entry_t entry;
	memset(&entry, 0, sizeof entry);

	const rtcp_psfb_fir_t* fir = (const rtcp_psfb_fir_t*)in;
	int read = 0;

	do
	{
		entry.fir.ssrc = ntohl(fir->ssrc);
		entry.fir.seqno = fir->seqno;

		m_entry_list.add(entry);

		fir++;
		read += sizeof(rtcp_psfb_fir_t);
	}
	while (read < length);

	return read;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_psfb_packet_t::read_tst(const uint8_t* in, int length)
{
	psfb_entry_t entry;
	const rtcp_psfb_tst_t* tst = (const rtcp_psfb_tst_t*)in;
	int read = 0;

	do
	{
		entry.tst.ssrc = ntohl(tst->ssrc);
		entry.tst.seqno = tst->seqno;
		entry.tst.index = tst->index;
		entry.tst.reserved = 0;

		m_entry_list.add(entry);

		tst++;
		read += sizeof(rtcp_psfb_tst_t);
	}
	while (read < length);

	return read;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_psfb_packet_t::read_vbcm(const uint8_t* in, int length)
{
	psfb_entry_t entry;
	const rtcp_psfb_vbcm_t* vbcm = (const rtcp_psfb_vbcm_t*)in;
	int read = 0;

	do
	{
		int vbcm_len = ntohs(vbcm->length) + 8;
		entry.vbcm = (rtcp_psfb_vbcm_t*)MALLOC(vbcm_len);
		entry.vbcm->ssrc = ntohl(vbcm->ssrc);
		entry.vbcm->seqno = vbcm->seqno;
		entry.vbcm->zero = vbcm->zero;
		entry.vbcm->payload = vbcm->payload;
		entry.vbcm->length = ntohs(vbcm->length);
		memcpy(entry.vbcm->data, vbcm->data, entry.vbcm->length);
		m_entry_list.add(entry);
		read += vbcm_len;

		int pad = read & 3;
		if (pad > 0)
			read += 4 - pad;

		vbcm = (const rtcp_psfb_vbcm_t*)(in + read);
	}
	while (read < length);

	return read;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_psfb_packet_t::read_app(const uint8_t* in, int length)
{
	uint32_t* id = (uint32_t*)in;
	if (*id == REMB)// REMB
	{
		return read_remb(in, length);
	}

	if (length > 64)
	{
		return 0;
	}

	memcpy(m_app_buff, in, length);
	m_app_length = length;

	return length;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_psfb_packet_t::read_remb(const uint8_t* in, int length)
{
	psfb_entry_t entry;
	const rtcp_psfb_app_t* app = (const rtcp_psfb_app_t*)in;
	int read = 0;

	entry.app.ID = app->ID;
	read += sizeof(entry.app.ID);

	memset(&entry.app.app.remb, 0, sizeof(rtcp_psfb_app_remb_t));
	const rtcp_psfb_app_remb_t* remb = (const rtcp_psfb_app_remb_t*)(in + read);
	entry.app.app.remb.br_exp = remb->br_exp;
	entry.app.app.remb.br_mantissa = remb->br_mantissa;
	entry.app.app.remb.num_ssrc = remb->num_ssrc;
	read += sizeof(uint32_t);
	
	int count = (length - read) / sizeof(uint32_t);
	if (count > 4)
	{
		return 0;
	}

	for (int i = 0; i < count; i++)
	{
		entry.app.app.remb.ssrc_fb[i] = remb->ssrc_fb[i];
		read += sizeof(uint32_t);
	}

	m_entry_list.add(entry);

	return read;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_psfb_packet_t::write_sli(uint8_t* out, int size)
{
	int written = 0;
	
	rtcp_psfb_sli_t* sli = (rtcp_psfb_sli_t*)out;

	for (int i = 0; i < m_entry_list.getCount(); i++)
	{
		psfb_entry_t& entry = m_entry_list[i];
		*sli = entry.sli;

		sli++;
		written += sizeof(rtcp_psfb_sli_t);
	}

	return written;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_psfb_packet_t::write_rpsi(uint8_t* out, int size)
{
	int written = 0;
	
	rtcp_psfb_rpsi_t* rpsi = (rtcp_psfb_rpsi_t*)out;

	if (m_rpsi != nullptr)
	{
		memcpy(rpsi, m_rpsi, m_rpsi_length);
		written += m_rpsi_length;

		int pad = written & 3;
		if (pad > 0)
		{
			for (int i = 0; i < 4 - pad; i++)
			{
				out[written++] = 0;
			}
		}
	}

	return written;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_psfb_packet_t::write_fir(uint8_t* out, int size)
{
	int written = 0;
	
	rtcp_psfb_fir_t* fir = (rtcp_psfb_fir_t*)out;

	for (int i = 0; i < m_entry_list.getCount(); i++)
	{
		psfb_entry_t& entry = m_entry_list[i];
		fir->ssrc = htonl(entry.fir.ssrc);
		fir->seqno = entry.fir.seqno;
		memset(fir->reserverd, 0, 3);
		fir++;
		written += sizeof(rtcp_psfb_fir_t);
	}

	return written;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_psfb_packet_t::write_tst(uint8_t* out, int size)
{
	int written = 0;
	
	rtcp_psfb_tst_t* tst = (rtcp_psfb_tst_t*)out;

	for (int i = 0; i < m_entry_list.getCount(); i++)
	{
		psfb_entry_t& entry = m_entry_list[i];
		tst->ssrc = htonl(entry.tst.ssrc);
		tst->seqno = entry.tst.seqno;
		tst->index = entry.tst.index;

		tst++;
		written += sizeof(rtcp_psfb_tst_t);
	}

	return written;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_psfb_packet_t::write_vbcm(uint8_t* out, int size)
{
	int written = 0;
	
	for (int i = 0; i < m_entry_list.getCount(); i++)
	{
		rtcp_psfb_vbcm_t* vbcm = (rtcp_psfb_vbcm_t*)(out + written);
		psfb_entry_t& entry = m_entry_list[i];
		
		vbcm->ssrc = htonl(entry.vbcm->ssrc);
		vbcm->seqno = entry.vbcm->seqno;
		vbcm->payload = entry.vbcm->payload;
		vbcm->length = htons(entry.vbcm->length);
		memcpy(vbcm->data, entry.vbcm->data, entry.vbcm->length);

		written += entry.vbcm->length + 8;
		int pad = written & 3;
		if (pad > 0)
			written += 4 - pad;
	}

	return written;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_psfb_packet_t::write_app(uint8_t* out, int size)
{
	int count = m_entry_list.getCount();
	if (count == 0)
	{
		if (size < m_app_length)
		{
			return 0;
		}
		memcpy(out, m_app_buff, m_app_length);
		return m_app_length;
	}

	int written = 0;

	for (int i = 0; i < count; i++)
	{
		rtcp_psfb_app_t* app = (rtcp_psfb_app_t*)(out + written);
		psfb_entry_t& entry = m_entry_list[i];
		
		app->ID = entry.app.ID;
		written += sizeof(entry.app.ID);
		if (app->ID == REMB) // REMB
		{
			written += write_remb(&entry.app, out + written, size - written);
		}
	}

	return written;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_psfb_packet_t::write_remb(rtcp_psfb_app_t* app, uint8_t* out, int size)
{
	int written = 0;

	rtcp_psfb_app_remb_t* remb = (rtcp_psfb_app_remb_t*)(out + written);
	rtcp_psfb_app_t::app_entry_t entry = app->app;

	remb->br_mantissa = entry.remb.br_mantissa;
	remb->br_exp = entry.remb.br_exp;
	remb->num_ssrc = entry.remb.num_ssrc;
	written += sizeof(uint32_t);

	for (int i = 0; i < 4; i++)
	{
		if (entry.remb.ssrc_fb[i] == 0)
		{
			break;
		}
		remb->ssrc_fb[i] = entry.remb.ssrc_fb[i];
		written += sizeof(uint32_t);
	}

	return written;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_psfb_packet_t::calc_vbcm_length()
{
	int length = 0;

	for (int i = 0; i < m_entry_list.getCount(); i++)
	{
		length += m_entry_list[i].vbcm->length + 8;
		
		int pad = length & 3;
		if (pad > 0)
			length += 4 - pad;
	}

	return length;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_psfb_packet_t::calc_app_length()
{
	int length = 0;

	for (int i = 0; i < m_entry_list.getCount(); i++)
	{
		length += sizeof(m_entry_list[i].app.ID);
		if (m_entry_list[i].app.ID == REMB) // REMB
		{
			length += sizeof(uint32_t);
			for (int i = 0; i < 4; i++)
			{
				if (m_entry_list[i].app.app.remb.ssrc_fb[i] == 0)
				{
					break;
				}
				length += sizeof(uint32_t);
			}
		}
	}

	return length;
}
//--------------------------------------
//
//--------------------------------------
void rtcp_psfb_packet_t::cleanup()
{
	rtcp_psfb_format_t fmt = (rtcp_psfb_format_t)m_counter;

	if (m_rpsi != nullptr)
	{
		FREE(m_rpsi);
		m_rpsi = nullptr;
		m_rpsi_length = 0;
	}
	
	if (fmt == rtcp_psfb_vbcm_e)
	{
		for (int i = 0; i < m_entry_list.getCount(); i++)
		{
			psfb_entry_t& entry = m_entry_list[i];
			FREE(entry.vbcm);
		}
	}

	m_entry_list.clear();

	m_media_source = 0;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_psfb_packet_t::read(const uint8_t* in, int length)
{
	cleanup();

	if (length < 12)
		return 0;

	int read = rtcp_base_packet_t::read(in, length);

	m_media_source = ntohl(*(const uint32_t*)(in + read));
	read += 4;

	int to_read = (m_length + 1) << 2;

	if (to_read > length)
		return 0;
	
	switch ((rtcp_psfb_format_t)m_counter)
	{
	case rtcp_psfb_sli_e:
		read += read_sli(in + read, to_read - read);
		break;
	case rtcp_psfb_rpsi_e:
		read += read_rpsi(in + read, to_read - read);
		break;
	case rtcp_psfb_fir_e:
		read += read_fir(in + read, to_read - read);
		break;
	case rtcp_psfb_tstr_e:
	case rtcp_psfb_tstn_e:
		read += read_tst(in + read, to_read - read);
		break;
	case rtcp_psfb_vbcm_e:
		read += read_vbcm(in + read, to_read - read);
		break;
	case rtcp_psfb_app_e:
		read += read_app(in + read, to_read - read);
		break;
	}

	return read;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_psfb_packet_t::write(uint8_t* out, int size)
{
	int to_write = 12; // rtcp header + ssrc + media_source

	switch ((rtcp_psfb_format_t)m_counter)
	{
	case rtcp_psfb_sli_e:
		to_write += m_entry_list.getCount() * sizeof(rtcp_psfb_sli_t);
		break;
	case rtcp_psfb_rpsi_e:
		to_write += m_rpsi_length;
		break;
	case rtcp_psfb_fir_e:
		to_write += m_entry_list.getCount() * sizeof(rtcp_psfb_fir_t);
		break;
	case rtcp_psfb_tstr_e:
	case rtcp_psfb_tstn_e:
		to_write += m_entry_list.getCount() * sizeof(rtcp_psfb_tst_t);
		break;
	case rtcp_psfb_vbcm_e:
		to_write += calc_vbcm_length();
		break;
	case rtcp_psfb_app_e:
		to_write += calc_app_length();
		break;
	}

	if (to_write > size)
		return 0;

	int pad = to_write & 3;
	if (pad > 0)
		to_write += 4 - pad;

	m_length = (to_write >> 2) - 1;

	int written = rtcp_base_packet_t::write(out, size);

	*(uint32_t*)(out + written) = htonl(m_media_source);
	written += 4;

	switch ((rtcp_psfb_format_t)m_counter)
	{
	case rtcp_psfb_sli_e:
		written += write_sli(out + written, size - written);
		break;
	case rtcp_psfb_rpsi_e:
		written += write_rpsi(out + written, size - written);
		break;
	case rtcp_psfb_fir_e:
		written += write_fir(out + written, size - written);
		break;
	case rtcp_psfb_tstr_e:
	case rtcp_psfb_tstn_e:
		written += write_tst(out + written, size - written);
		break;
	case rtcp_psfb_vbcm_e:
		written += write_vbcm(out + written, size - written);
		break;
	case rtcp_psfb_app_e:
		written += write_app(out + written, size - written);
		break;
	}

	return written;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_psfb_packet_t::dump(char* out, int size) const
{
	int written = rtcp_base_packet_t::dump(out, size);

	written += std_snprintf(out + written, size - written, "\tFBI HEADER  : type:%s, mssrc: %X(%10u)\r\n", get_rtcp_psfb_type_name((rtcp_psfb_format_t)m_counter), m_media_source, m_media_source);

	switch (get_fb_type())
	{
	case rtcp_psfb_sli_e:
		for (int i = 0; i < m_entry_list.getCount(); i++)
		{
			const rtcp_psfb_sli_t& sli = m_entry_list[i].sli;
			written += std_snprintf(out + written, size - written, "\tSLI HEADER  : first:%d, num:%d, pid:%d\r\n", sli.first, sli.number, sli.pid);
		}
		break;
	case rtcp_psfb_rpsi_e:
		{
			written += std_snprintf(out + written, size - written, "\t--------- RPSI HEADER : pb:%d, pt:%d nb len:%d\r\n", m_rpsi->pb, m_rpsi->pt, m_rpsi_length - (int)sizeof(*m_rpsi) + 1);
			written += dump_binary(out + written, size - written, m_rpsi->native_bits, m_rpsi_length - sizeof(*m_rpsi) + 1);
		}
		break;
	case rtcp_psfb_fir_e:
		for (int i = 0; i < m_entry_list.getCount(); i++)
		{
			const rtcp_psfb_fir_t& fir = m_entry_list[i].fir;
			written += std_snprintf(out + written, size - written, "\tFIR HEADER  : ssrc:%X, seq:%d, zero:%d%d%d\r\n", fir.ssrc, fir.seqno, fir.reserverd[0], fir.reserverd[1], fir.reserverd[2]);
		}
		break;
	case rtcp_psfb_tstr_e:
	case rtcp_psfb_tstn_e:
		for (int i = 0; i < m_entry_list.getCount(); i++)
		{
			const rtcp_psfb_tst_t& tst = m_entry_list[i].tst;
			written += std_snprintf(out + written, size - written, "\tTST HEADER  : ssrc:%X, seq:%d, idx:%d, zero:%d\r\n", tst.ssrc, tst.seqno, tst.index, tst.reserved);
		}
		break;
	case rtcp_psfb_vbcm_e:
		for (int i = 0; i < m_entry_list.getCount(); i++)
		{
			rtcp_psfb_vbcm_t* vbcm = m_entry_list[i].vbcm;
			written += std_snprintf(out + written, size - written, "\tVBCM HEADER : ssrc:%X, seq:%d, pl:%d, zero:%d, len:%d\r\n", vbcm->ssrc, vbcm->seqno, vbcm->payload, vbcm->zero, vbcm->length);
			written += dump_binary(out + written, size - written, (uint8_t*)vbcm->data, vbcm->length);
		}
		break;
	case rtcp_psfb_app_e:
		break;
	}

	return written;
}

//--------------------------------------
//
//--------------------------------------
void rtcp_psfb_packet_t::add_sli(rtcp_psfb_sli_t* sli)
{
	if ((rtcp_psfb_format_t)m_counter == rtcp_psfb_sli_e)
	{
		psfb_entry_t entry;
		entry.sli = *sli;
		m_entry_list.add(entry);
	}
}
//--------------------------------------
//
//--------------------------------------
void rtcp_psfb_packet_t::set_rpsi(rtcp_psfb_rpsi_t* rpsi, int length)
{
	if ((rtcp_psfb_format_t)m_counter == rtcp_psfb_rpsi_e)
	{
		if (m_rpsi != nullptr)
		{
			FREE(m_rpsi);
			m_rpsi = nullptr;
			m_rpsi_length = 0;
		}

		if (rpsi != nullptr && length > 0)
		{
			m_rpsi = (rtcp_psfb_rpsi_t*)MALLOC(m_rpsi_length = length);
			memcpy(m_rpsi, rpsi, length);
		}
	}
}
//--------------------------------------
//
//--------------------------------------
void rtcp_psfb_packet_t::add_fir(uint32_t ssrc, uint8_t seqnr)
{
	if ((rtcp_psfb_format_t)m_counter == rtcp_psfb_fir_e)
	{
		psfb_entry_t entry;
		entry.fir.ssrc = ssrc;
		entry.fir.seqno = seqnr;
		m_entry_list.add(entry);
	}
}
//--------------------------------------
//
//--------------------------------------
void rtcp_psfb_packet_t::add_tst(rtcp_psfb_tst_t* tst)
{
	if ((rtcp_psfb_format_t)m_counter == rtcp_psfb_tstr_e || (rtcp_psfb_format_t)m_counter == rtcp_psfb_tstn_e)
	{
		psfb_entry_t entry;
		entry.tst = *tst;
		m_entry_list.add(entry);
	}
}
//--------------------------------------
//
//--------------------------------------
void rtcp_psfb_packet_t::add_vbcm(rtcp_psfb_vbcm_t* vbcm)
{
	if ((rtcp_psfb_format_t)m_counter == rtcp_psfb_vbcm_e)
	{
		psfb_entry_t entry;
		entry.vbcm = (rtcp_psfb_vbcm_t*)MALLOC(vbcm->length + 8);
		memcpy(entry.vbcm, vbcm, vbcm->length + 8);
		m_entry_list.add(entry);
	}
}

//--------------------------------------
//
//--------------------------------------
rtcp_base_packet_t* rtcp_packet_t::create_packet(rtcp_type_t type)
{
	switch (type)
	{
	case rtcp_type_sr_e:	return NEW rtcp_sr_packet_t();
	case rtcp_type_rr_e:	return NEW rtcp_rr_packet_t();
	case rtcp_type_sdes_e:	return NEW rtcp_sdes_packet_t();
	case rtcp_type_bye_e:	return NEW rtcp_bye_packet_t();
	case rtcp_type_app_e:	return NEW rtcp_app_packet_t();
	case rtcp_type_rtpfb_e:	return NEW rtcp_rtpfb_packet_t();
	case rtcp_type_psfb_e:	return NEW rtcp_psfb_packet_t();
	case rtcp_type_xr_e:	return NEW rtcp_xr_packet_t();
	case rtcp_type_avb_e:	return NEW rtcp_avb_packet_t();
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
rtcp_base_packet_t* rtcp_packet_t::get_packet(rtcp_type_t type, int index)
{
	int packet_ref = 0;

	for (int i = 0; i < m_packet_list.getCount(); i++)
	{
		rtcp_base_packet_t* packet = m_packet_list[i];
		
		if (packet->getType() == type && packet_ref == index)
		{
			return packet;
		}

		packet_ref++;
	}

	return nullptr;
}
//--------------------------------------
const rtcp_base_packet_t* rtcp_packet_t::get_packet(rtcp_type_t type, int index) const
{
	int packet_ref = 0;

	for (int i = 0; i < m_packet_list.getCount(); i++)
	{
		rtcp_base_packet_t* packet = m_packet_list[i];

		if (packet->getType() == type && packet_ref == index)
		{
			return packet;
		}

		packet_ref++;
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_packet_t::read(const uint8_t* in, int length)
{
	int read = 0;
	int packet_read = 0;
	rtcp_base_packet_t* packet;

	cleanup();

	while (read < length)
	{
		int type = *(in + read + 1);
		rtcp_type_t type_t = (rtcp_type_t)type;
		packet = create_packet(type_t);
		if (packet == nullptr)
		{
			break;
		}
			
		if ((packet_read = packet->read(in+read, length-read)) == 0)
		{
			DELETEO(packet);
			return 0;
		}

		m_packet_list.add(packet);
		read += packet_read;
	}
	m_full_length = read;
	return read;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_packet_t::write(uint8_t* out, int size) const
{
	int written = 0;

	for (int i = 0; i < m_packet_list.getCount(); i++)
	{
		written += m_packet_list[i]->write(out + written, size - written);
	}

	return written;
}
//--------------------------------------
//
//--------------------------------------
bool rtcp_packet_t::copy(const rtcp_packet_t* packet)
{
	cleanup();

	if (packet == nullptr)
	{
		return false;
	}

	uint8_t buff[4096] = { 0 };
	int write = packet->write(buff, 4096);

	return write == read(buff, write);
}
//--------------------------------------
//
//--------------------------------------
int rtcp_packet_t::dump(char* out, int size) const
{
	int written = std_snprintf(out, size, "\tRTCP PACKET\r\n");

	for (int i = 0; i < m_packet_list.getCount(); i++)
	{
		const rtcp_base_packet_t* bpacket = m_packet_list[i];

		written += bpacket->dump(out + written, size - written);
	}

	return written;
}
//--------------------------------------
//
//--------------------------------------
int rtcp_packet_t::get_full_length() const
{
	return m_full_length;
}
//--------------------------------------
//
//--------------------------------------
void rtcp_packet_t::cleanup()
{
	for (int i = 0; i < m_packet_list.getCount(); i++)
	{
		DELETEO(m_packet_list[i]);
	}

	m_packet_list.clear();
}
//--------------------------------------
//
//--------------------------------------
static int dump_report(char* out, int size, const rtcp_report_t* rep)
{
	uint32_t packet_lost = rep->packets_lost[2];
	packet_lost |= rep->packets_lost[1] << 8;
	packet_lost |= rep->packets_lost[2] << 16;

	return std_snprintf(out, size, "\tREPORT      : ssrc:%X, frac lost:%u, pack lost:%u, ext-seq:%u last seq:%u, jitter:%u, lsr:%u, dlsr:%u\r\n",
		rep->ssrc, rep->fraction_lost, packet_lost, rep->ext_seq, rep->last_seq, rep->jitter, rep->lsr, rep->dlsr);
}
//--------------------------------------
//
//--------------------------------------
static int dump_binary(char* out, int size, uint8_t* data, int length)
{
	// write binary dump to output
	int rows = (length / 16) ;
	if (length % 16)
	rows ++;

	int written = std_snprintf(out, size, "\tBINARY DUMP\r\n");

	int j = 0;

	for (int i = 0; i < rows; i++)
	{
		written += std_snprintf(out + written, size - written, "\t%08x: ", i * 16);
		for (j = 0; j < 16; j++)
		{
			int index = (i * 16) + j;

			uint8_t ch = data[index];

			*(out + written++) = ' ';

			if (index < length)
			{
				uint8_t lt = ch & 0x0F;
				uint8_t ht = (ch & 0xF0) >> 4;
					
				if (!(j % 4) && j != 0)
				{
					//putchar(' ');
					*(out + written++) = ' ';
				}
			
				/*putchar((char)(ht > 9 ? ht - 10 + 'a' : ht + '0'));
				putchar((char)(lt > 9 ? lt - 10 + 'a' : lt + '0'));*/
				*(out + written++) = (char)(ht > 9 ? ht - 10 + 'a' : ht + '0');
				*(out + written++) = (char)(lt > 9 ? lt - 10 + 'a' : lt + '0');
			}
		}

		//putchar('\n');
		*(out + written++) = '\r';
		*(out + written++) = '\n';
	}

	return written;
}
//--------------------------------------
//
//--------------------------------------
bool rtcp_packet_t::is_rtcp_packet(const void* data, uint32_t length)
{
	if (data == nullptr)
	{
		return false;
	}
	if (length < 2)
	{
		return false;
	}

	//         0                   1                   2                   3
	//         0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
	//        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	// header |V=2|P|    RC   |   PT=SR=200   |             Length            |
	//        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	const uint8_t* pdata = (const uint8_t*)data;
	uint8_t pt = *(pdata + 1);

	if ((pt >= rtcp_type_t::rtcp_type_sr_e) && (pt <= rtcp_type_t::rtcp_type_last_e))
	{
		return true;
	}

	return false;
}
//--------------------------------------
