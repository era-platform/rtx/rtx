﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//--------------------------------------
// based on libsrtp written by
// David A. McGrew
// Cisco Systems, Inc.
//--------------------------------------
#pragma once

#include "srtp_datatypes.h"          
//--------------------------------------
// A cipher_direction_t is an enum that describes a particular cipher
// operation, i.e. encryption or decryption.  For some ciphers, this
// distinction does not matter, but for others, it is essential.
//--------------------------------------
enum cipher_direction_t
{ 
	direction_encrypt, /**< encryption (convert plaintext to ciphertext) */
	direction_decrypt, /**< decryption (convert ciphertext to plaintext) */
	direction_any      /**< encryption or decryption                     */
} ;
//--------------------------------------
// make truth interface -> base class for all ciphers
//--------------------------------------
class cipher_t
{
protected:
	int m_cipher_id;
	int m_key_len;

public:
	cipher_t(int key_len);
	virtual ~cipher_t();

	virtual bool create(const uint8_t *key, cipher_direction_t dir) = 0;
	virtual bool set_iv(void *iv) = 0;
	virtual bool encrypt(uint8_t *buffer, uint32_t *octets_to_encrypt) = 0;
	virtual bool decrypt(uint8_t *buffer, uint32_t *octets_to_decrypt) = 0;

	bool output(uint8_t *buffer, int num_octets_to_output);

	int get_key_length() const { return m_key_len; }
	int get_cipher_id() const { return m_cipher_id; }
};
//--------------------------------------
//
//--------------------------------------
class null_cipher_ctx_t : public cipher_t
{
public:
	null_cipher_ctx_t(int key_len);
	virtual ~null_cipher_ctx_t();

	virtual bool create(const uint8_t *key, cipher_direction_t dir);
	virtual bool set_iv(void *iv);
	virtual bool encrypt(uint8_t *buffer, uint32_t *octets_to_encrypt);
	virtual bool decrypt(uint8_t *buffer, uint32_t *octets_to_decrypt);
};
//--------------------------------------
