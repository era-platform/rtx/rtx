﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtp_port_manager.h"
#include "rtp_simple_channel.h"
#include "rtp_webrtc_channel.h"
#include "rtp_secure_channel.h"
#include "udptl_proxy_channel.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define LOG_PREFIX "RTP-PM" // Media Gateway Port Manager
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtl::Logger* rtp_port_manager_t::m_log = nullptr;
rtl::MutexWatch rtp_port_manager_t::m_mutex("mg-port-man");
mg_interface_list rtp_port_manager_t::m_interfaces;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_port_manager_t::initialize(rtl::Logger* log)
{
	m_log = log; 
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_port_manager_t::release2()
{
	destroy();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_port_manager_t::destroy()
{
	PLOG_RTP_WRITE(LOG_PREFIX, "destroy -- begin. destroying RTP Manager...ifaces count %d", m_interfaces.getCount());

	{
		//rtl::MutexLock lock(m_mutex);
		rtl::MutexWatchLock lock(m_mutex, __FUNCTION__);

		for (int i = 0; i < m_interfaces.getCount(); i++)
		{
			PLOG_RTP_WRITE(LOG_PREFIX, "destroy -- destroying iface with index: %d", i);
			rtp_port_manager_interface_t* iface_man = m_interfaces.getAt(i);
			if (iface_man == nullptr)
			{
				continue;
			}
			PLOG_RTP_WRITE(LOG_PREFIX, "destroy -- destroying iface: %s", iface_man->get_interface()->to_string());
			iface_man->destroy();
			DELETEO(iface_man);
			iface_man = nullptr;
			PLOG_RTP_WRITE(LOG_PREFIX, "destroy -- iface with index: %d destroyed", i);
		}
		m_interfaces.clear();
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "destroy -- end.");
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_port_manager_t::set_interface(const char* ip_interface, const char* address_key)
{
	PLOG_RTP_WRITE(LOG_PREFIX, "set_interface -- begin.");

	if (ip_interface == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "set_interface -- ip_interface is null.");
		return;
	}

	{
		//rtl::MutexLock lock(m_mutex);
		rtl::MutexWatchLock lock(m_mutex, __FUNCTION__);

		rtp_port_manager_interface_t* iface_man = find_interface_at_key(address_key);
		if (iface_man == nullptr)
		{
			iface_man = find_interface_at_iface(ip_interface);
		}

		if (iface_man == nullptr)
		{
			iface_man = add_interface(ip_interface, address_key);
			if (iface_man == nullptr)
			{
				PLOG_RTP_ERROR(LOG_PREFIX, "set_interface -- add interface fail.");
				return;
			}
		}
		else if (iface_man->active())
		{
			PLOG_RTP_ERROR(LOG_PREFIX, "set_interface -- interface already exist.");
			return;
		}
		else
		{
			iface_man->set_port_range(0, 0);
			iface_man->activate(true);
		}
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "set_interface -- end.");
	return;
}
//------------------------------------------------------------------------------
// Не удаляем объекты интерфейсов. Просто снимаем ограничение на выделение портов.
//------------------------------------------------------------------------------
void rtp_port_manager_t::free_interface(const char* address_key)
{
	PLOG_RTP_WRITE(LOG_PREFIX, "free_interface -- begin.");

	if (address_key == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "free_interface -- address_key is null.");
		return;
	}

	{
		//rtl::MutexLock lock(m_mutex);
		rtl::MutexWatchLock lock(m_mutex, __FUNCTION__);

		bool all = (strcmp(address_key, "ALL") == 0);
		
		for (int i = 0; i < m_interfaces.getCount(); i++)
		{
			rtp_port_manager_interface_t* iface_man = m_interfaces.getAt(i);
			if (iface_man == nullptr)
			{
				PLOG_RTP_WARNING(LOG_PREFIX, "free_interface -- interface with index: %d is null.", i);
				continue;
			}

			const char* addrk = iface_man->get_address_key();
			if ((all) || (strcmp(addrk, address_key) == 0))
			{
				iface_man->activate(false);
				if (!all)
				{
					break;
				}
			}
		}
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "free_interface -- end.");
	return;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtp_port_manager_t::set_port_range(const char* address_key, uint16_t begin, uint32_t count)
{
	PLOG_RTP_WRITE(LOG_PREFIX, "set_port_range -- begin.");

	if (address_key == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "set_port_range -- address_key is null.");
		return false;
	}

	if (begin + count >= 65536)
	{
		PLOG_RTP_WARNING(LOG_PREFIX, "set_port_range -- failed : invalid range initial : port %u count %d", begin, count);
		count = 65536 - begin;
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "set_port_range -- new RTP port range (start:%u count:%d) for %s", begin, count, address_key);

	{
		//rtl::MutexLock lock(m_mutex);
		rtl::MutexWatchLock lock(m_mutex, __FUNCTION__);

		// если для всех то и присвоим всем
		bool all = (strcmp(address_key, "ALL") == 0);

		bool least_one = false;
		for (int i = 0; i < m_interfaces.getCount(); i++)
		{
			rtp_port_manager_interface_t* iface_man = m_interfaces.getAt(i);
			if (iface_man == nullptr)
			{
				PLOG_RTP_WARNING(LOG_PREFIX, "set_port_range -- interface by index: %d is null.", i);
				continue;
			}

			const char* addrk = iface_man->get_address_key();
			if ((all) || (strcmp(addrk, address_key) == 0))
			{
				if (iface_man->active())
				{
					iface_man->set_port_range(begin, count);
					least_one = true;
				}
				if (!all)
				{
					break;
				}
			}
		}

		if (!least_one)
		{
			PLOG_RTP_ERROR(LOG_PREFIX, "set_port_range -- interfaces not found.");
			return false;
		}
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "set_port_range -- end.");
	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_port_manager_t::free_port_range(const char* address_key)
{
	PLOG_RTP_WRITE(LOG_PREFIX, "free_port_range -- begin.");

	if (address_key == nullptr)
	{
		PLOG_RTP_ERROR(LOG_PREFIX, "free_port_range -- ip_interface is null.");
		return;
	}

	{
		//rtl::MutexLock lock(m_mutex);
		rtl::MutexWatchLock lock(m_mutex, __FUNCTION__);

		bool all = (strcmp(address_key, "ALL") == 0);

		for (int i = 0; i < m_interfaces.getCount(); i++)
		{
			rtp_port_manager_interface_t* iface_man = m_interfaces.getAt(i);
			if (iface_man == nullptr)
			{
				PLOG_RTP_WARNING(LOG_PREFIX, "free_port_range -- interface with index: %d is null.", i);
				continue;
			}

			const char* addrk = iface_man->get_address_key();
			if ((all) || (strcmp(addrk, address_key) == 0))
			{
				iface_man->set_port_range(0,0);
				if (!all)
				{
					break;
				}
			}
		}
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "free_port_range -- end.");
	return;
}
//------------------------------------------------------------------------------
// Получение порта верно только в случае существующего интерфейса!
//------------------------------------------------------------------------------
rtp_channel_t* rtp_port_manager_t::get_channel(const char* address_key, rtp_channel_type_t type)
{
	PLOG_RTP_WRITE(LOG_PREFIX, "get_channel -- begin. allocate RTP channel on iface %s.", address_key);

	rtp_port_manager_interface_t* iface_man = nullptr;

	{
		//rtl::MutexLock lock(m_mutex);
		rtl::MutexWatchLock lock(m_mutex, __FUNCTION__);

		for (int i = 0; i < m_interfaces.getCount(); i++)
		{
			rtp_port_manager_interface_t* iface_man_at_list = m_interfaces.getAt(i);
			if (iface_man_at_list == nullptr)
			{
				PLOG_RTP_WARNING(LOG_PREFIX, "get_channel -- interface by index: %d is null.", i);
				continue;
			}

			const char* addrk = iface_man_at_list->get_address_key();
			if (strcmp(addrk, address_key) == 0)
			{
				if (iface_man_at_list->active())
				{
					iface_man = iface_man_at_list;
					break;
				}
			}
		}

		if (iface_man == nullptr)
		{
			PLOG_RTP_WARNING(LOG_PREFIX, "get_channel -- allocating RTP channel failed : can't find interface.");
			return nullptr;
		}
	}

	rtp_channel_t* channel = iface_man->get_channel(type);
	if (channel == nullptr)
	{
		PLOG_RTP_WARNING(LOG_PREFIX, "get_channel -- allocating RTP channel failed : can't allocate port.");
		return nullptr;
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "get_channel -- end. (%s)", channel->getName());
	return channel;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_port_manager_t::release_channel(rtp_channel_t* channel)
{
	PLOG_RTP_WRITE(LOG_PREFIX, "release_channel -- begin.");

	if (channel == nullptr)
	{
		PLOG_RTP_WARNING(LOG_PREFIX, "release_channel -- releasing null RTP channel!");
		return;
	}

	const ip_address_t* ipaddr = channel->get_net_interface();
	
	PLOG_RTP_WRITE(LOG_PREFIX, "release_channel -- release RTP channel on iface %s", ipaddr->to_string());

	rtp_port_manager_interface_t* iface_man = nullptr;

	{
		//rtl::MutexLock lock(m_mutex);
		rtl::MutexWatchLock lock(m_mutex, __FUNCTION__);

		iface_man = find_interface_at_iface(ipaddr->to_string());
	}

	if (iface_man == nullptr)
	{
		PLOG_RTP_WARNING(LOG_PREFIX, "release_channel -- unknown RTP channel -- ignored.");
	}
	else
	{
		iface_man->release_channel(channel);
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "release_channel -- end.");
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtp_port_manager_interface_t* rtp_port_manager_t::find_interface_at_key(const char* address_key)
{
	PLOG_RTP_WRITE(LOG_PREFIX, "find_interface -- begin. check interface %s.", address_key);

	for (int i = 0; i < m_interfaces.getCount(); i++)
	{
		rtp_port_manager_interface_t* iface_man = m_interfaces.getAt(i);
		if (iface_man == nullptr)
		{
			PLOG_RTP_WARNING(LOG_PREFIX, "find_interface -- interface with index: %d is null.", i);
			continue;
		}

		const char* ipaddr = iface_man->get_address_key();
		if (strcmp(ipaddr, address_key) == 0)
		{
			PLOG_RTP_WRITE(LOG_PREFIX, "find_interface -- end.");
			return iface_man;
		}
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "find_interface -- not found.");
	return nullptr;
}
//------------------------------------------------------------------------------
rtp_port_manager_interface_t* rtp_port_manager_t::find_interface_at_iface(const char* ip_interface)
{
	PLOG_RTP_WRITE(LOG_PREFIX, "find_interface -- begin. check interface %s.", ip_interface);

	for (int i = 0; i < m_interfaces.getCount(); i++)
	{
		rtp_port_manager_interface_t* iface_man = m_interfaces.getAt(i);
		if (iface_man == nullptr)
		{
			PLOG_RTP_WARNING(LOG_PREFIX, "find_interface -- interface with index: %d is null.", i);
			continue;
		}

		const ip_address_t* ipaddr = iface_man->get_interface();
		if ((ipaddr != nullptr) && (strcmp(ipaddr->to_string(), ip_interface) == 0))
		{
			PLOG_RTP_WRITE(LOG_PREFIX, "find_interface -- end.");
			return iface_man;
		}
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "find_interface -- not found.");
	return nullptr;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtp_port_manager_interface_t* rtp_port_manager_t::add_interface(const char* ip_interface, const char* address_key)
{
	PLOG_RTP_WRITE(LOG_PREFIX, "add_interface -- begin.");

	rtp_port_manager_interface_t* iface_man = NEW rtp_port_manager_interface_t(m_log);
	if (iface_man->create(ip_interface, address_key))
	{
		PLOG_RTP_WRITE(LOG_PREFIX, "add_interface -- allocate interface %s.", ip_interface);
		m_interfaces.add(iface_man);
	}
	else
	{
		PLOG_RTP_WARNING(LOG_PREFIX, "add_interface -- interface allocation failed %s.", ip_interface);
		DELETEO(iface_man);
		iface_man = nullptr;
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "add_interface -- end.");
	return iface_man;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtp_port_manager_t::start_channel_listen(rtp_channel_t* channel)
{
	PLOG_RTP_WRITE(LOG_PREFIX, "start_channel_listen -- begin.");

	if (channel == nullptr)
	{
		PLOG_RTP_WARNING(LOG_PREFIX, "start_channel_listen -- null RTP channel!");
		return false;
	}

	const ip_address_t* ipaddr = channel->get_net_interface();
	
	PLOG_RTP_WRITE(LOG_PREFIX, "start_channel_listen -- start RTP channel on iface %s", ipaddr->to_string());

	rtp_port_manager_interface_t* iface_man = nullptr;

	{
		//rtl::MutexLock lock(m_mutex);
		rtl::MutexWatchLock lock(m_mutex, __FUNCTION__);

		iface_man = find_interface_at_iface(ipaddr->to_string());
	}

	if (iface_man == nullptr)
	{
		PLOG_RTP_WARNING(LOG_PREFIX, "start_channel_listen -- unknown RTP channel -- ignored.");
		return false;
	}
	else
	{
		iface_man->start_channel_listen(channel);
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "start_channel_listen -- end.");
	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtp_port_manager_t::stop_channel_listen(rtp_channel_t* channel)
{
	PLOG_RTP_WRITE(LOG_PREFIX, "stop_channel_listen -- begin.");

	if (channel == nullptr)
	{
		PLOG_RTP_WARNING(LOG_PREFIX, "stop_channel_listen -- null RTP channel!");
		return;
	}

	const ip_address_t* ipaddr = channel->get_net_interface();

	PLOG_RTP_WRITE(LOG_PREFIX, "stop_channel_listen -- stop RTP channel on iface %s", ipaddr->to_string());

	rtp_port_manager_interface_t* iface_man = nullptr;

	{
		rtl::MutexWatchLock lock(m_mutex, __FUNCTION__);

		iface_man = find_interface_at_iface(ipaddr->to_string());
	}

	if (iface_man == nullptr)
	{
		PLOG_RTP_WARNING(LOG_PREFIX, "stop_channel_listen -- unknown RTP channel -- ignored.");
		return;
	}
	else
	{
		iface_man->stop_channel_listen(channel);
	}

	PLOG_RTP_WRITE(LOG_PREFIX, "stop_channel_listen -- end.");
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtp_port_manager_t::compare_channels(rtp_channel_t* channel_1, rtp_channel_t* channel_2)
{
	if ((channel_1 == nullptr) || (channel_2 == nullptr))
	{
		return false;
	}

	rtp_channel_type_t type_1 = channel_1->getType();
	rtp_channel_type_t type_2 = channel_2->getType();

	if (type_1 != type_2)
	{
		return false;
	}

	const ip_address_t* iface_1 = channel_1->get_net_interface();
	const ip_address_t* iface_2 = channel_2->get_net_interface();
	if (!iface_1->equals(iface_2))
	{
		return false;
	}

	if ((type_1 == rtp_channel_type_t::rtp_channel_type_simple_e) &&
		(type_2 == rtp_channel_type_t::rtp_channel_type_simple_e))
	{
		rtp_simple_channel_t* simpl_channel_1 = (rtp_simple_channel_t*)channel_1;
		rtp_simple_channel_t* simpl_channel_2 = (rtp_simple_channel_t*)channel_2;

		uint16_t port_data_1 = simpl_channel_1->get_data_port();
		uint16_t port_data_2 = simpl_channel_2->get_data_port();
		if (port_data_1 != port_data_2)
		{
			return false;
		}

		uint16_t port_ctrl_1 = simpl_channel_1->get_ctrl_port();
		uint16_t port_ctrl_2 = simpl_channel_2->get_ctrl_port();
		if (port_ctrl_1 != port_ctrl_2)
		{
			return false;
		}
	}
	else if ((type_1 == rtp_channel_type_t::rtp_channel_type_webrtc_e) && 
		(type_2 == rtp_channel_type_t::rtp_channel_type_webrtc_e))
	{
		rtp_webrtc_channel_t* web_channel_1 = (rtp_webrtc_channel_t*)channel_1;
		rtp_webrtc_channel_t* web_channel_2 = (rtp_webrtc_channel_t*)channel_2;

		uint16_t port_data_1 = web_channel_1->get_data_port();
		uint16_t port_data_2 = web_channel_2->get_data_port();
		if (port_data_1 != port_data_2)
		{
			return false;
		}

		uint16_t port_ctrl_1 = web_channel_1->get_ctrl_port();
		uint16_t port_ctrl_2 = web_channel_2->get_ctrl_port();
		if (port_ctrl_1 != port_ctrl_2)
		{
			return false;
		}
	}
	else if ((type_1 == rtp_channel_type_t::rtp_channel_type_webrtc_bundle_e) &&
		(type_2 == rtp_channel_type_t::rtp_channel_type_webrtc_bundle_e))
	{
		rtp_webrtc_channel_t* web_channel_1 = (rtp_webrtc_channel_t*)channel_1;
		rtp_webrtc_channel_t* web_channel_2 = (rtp_webrtc_channel_t*)channel_2;

		uint16_t port_data_1 = web_channel_1->get_data_port();
		uint16_t port_data_2 = web_channel_2->get_data_port();
		if (port_data_1 != port_data_2)
		{
			return false;
		}
	}
	else if ((type_1 == rtp_channel_type_t::rtp_channel_type_secure_e) &&
		(type_2 == rtp_channel_type_t::rtp_channel_type_secure_e))
	{
		rtp_secure_channel_t* secure_channel_1 = (rtp_secure_channel_t*)channel_1;
		rtp_secure_channel_t* secure_channel_2 = (rtp_secure_channel_t*)channel_2;

		uint16_t port_data_1 = secure_channel_1->get_data_port();
		uint16_t port_data_2 = secure_channel_2->get_data_port();
		if (port_data_1 != port_data_2)
		{
			return false;
		}
	}
	else if ((type_1 == rtp_channel_type_t::rtp_channel_type_udptl_proxy_e) &&
		(type_2 == rtp_channel_type_t::rtp_channel_type_udptl_proxy_e))
	{
		udptl_proxy_channel_t* udptl_channel_1 = (udptl_proxy_channel_t*)channel_1;
		udptl_proxy_channel_t* udptl_channel_2 = (udptl_proxy_channel_t*)channel_2;

		uint16_t port_data_1 = udptl_channel_1->get_data_port();
		uint16_t port_data_2 = udptl_channel_2->get_data_port();
		if (port_data_1 != port_data_2)
		{
			return false;
		}
	}

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
