﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//--------------------------------------
//
//--------------------------------------
#define MEGACO_AUTH_DATA_MAX 32
#define MEGACO_AUTH_DATA_MIN 12

namespace h248
{
	//--------------------------------------
	// authenticationHeader = AuthToken EQUAL SecurityParmIndex COLON SequenceNum COLON AuthData
	//--------------------------------------
	class Authentication
	{
		uint32_t m_securityParmIndex;
		uint32_t m_sequenceNum;
		uint8_t m_authData[MEGACO_AUTH_DATA_MAX];
		int m_authDataLength; // 12 -> 32

	public:
		Authentication();
		Authentication(uint32_t paramIndex, uint32_t seq, const uint8_t* data, int length);
		~Authentication();

		// SecurityParmIndex COLON SequenceNum COLON AuthData
		int readFrom(const char* stream);
		// AuthToken EQUAL SecurityParmIndex COLON SequenceNum COLON AuthData
		int writeTo(char* buffer, int size);

		uint32_t getSecurityParamIndex() const { return m_securityParmIndex; }
		uint32_t getSequenceNumber() const { return m_sequenceNum; }
		const uint8_t* getAuthData() const { return m_authData; }
		int getAuthDataLength() const { return m_authDataLength; }

		bool setValues(uint32_t paramIndex, uint32_t seq, const uint8_t* data, int length);
		void setSecurityParamIndex(uint32_t value) { m_securityParmIndex = value; }
		void setSequenceNumber(uint32_t value) { m_sequenceNum = value; }
		bool setAuthData(const uint8_t* data, int length);
	};
}
//--------------------------------------
