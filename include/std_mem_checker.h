﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <stddef.h>
//--------------------------------------
//
//--------------------------------------
#if defined (TARGET_OS_WINDOWS)
#ifdef RTX_STDLIB_EXPORTS
#define RTX_STD_API __declspec(dllexport)
#else
#define RTX_STD_API __declspec(dllimport)
#endif
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
#define RTX_STD_API
#endif
//--------------------------------------
//
//--------------------------------------
//#define DEBUG_MEMORY
//--------------------------------------
//
//--------------------------------------
#ifdef DEBUG_MEMORY
enum rc_malloc_type_t
{
	rc_malloc_new_e,
	rc_malloc_newar_e,
	rc_malloc_malloc_e,
	rc_malloc_delete_e,
	rc_malloc_deletear_e,
	rc_malloc_free_e,
};

RTX_STD_API void*	std_mem_checker_malloc(size_t size, char* file, int line, rc_malloc_type_t type);
RTX_STD_API void	std_mem_checker_free(void* ptr, rc_malloc_type_t type);
RTX_STD_API void*	std_mem_checker_realloc(void* pvMem, size_t size, char* file, int line); // always buffer
#endif
//--------------------------------------
//
//--------------------------------------
RTX_STD_API void std_mem_checker_initialize();
RTX_STD_API uint64_t std_mem_checker_get_usage();

RTX_STD_API void* std_mallocz(size_t size);
RTX_STD_API void std_freep(void* ptr);
//--------------------------------------
