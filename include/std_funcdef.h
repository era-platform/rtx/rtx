﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "std_basedef.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#if !defined(ARRAY_LEN)
    #define ARRAY_LEN(X)    (sizeof(X)/sizeof(X[0]))
#endif

#if !defined(MAX)
    #define MAX(a,b)            (((a) > (b)) ? (a) : (b))
#endif

#if !defined(MIN)
    #define MIN(a,b)            (((a) < (b)) ? (a) : (b))
#endif

#define NOMINMAX

#ifndef swap
template<class T>
static inline void swap(T& l, T& r)
{
	T t = l;
	l = r;
	r = t;
}
#endif //auto

//#ifndef min
//#define min(a, b) (((a) < (b)) ? (a) : (b))
//#endif
//
//#ifndef max
//#define max(a, b) (((a) > (b)) ? (a) : (b))
//#endif

//------------------------------------------------------------------------------
