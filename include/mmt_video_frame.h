/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "std_json.h"
#include "mmt_drawer.h"
#include "mmt_video_element.h"
#include "mmt_video_stream.h"
#include "mmt_video_font_man.h"

#define TRACE_VIDEO 0

namespace media
{
	//-----------------------------------------------
	// ��������� ����� ������ 
	//-----------------------------------------------
	class VideoFrameGenerator
	{
		//---
		// capture flags
		rtl::Mutex m_sync;
		// common fields
		rtl::ArrayT<VideoFrameElement*> m_elements;
		rtl::ArrayT<VideoFrameStream*> m_bindedStreams;
		//---
		rtl::ArrayT<VideoFrameStream*> m_vadStreams;
		rtl::QueueT<VideoFrameStream*> m_freeStreams;
		rtl::QueueT<uint32_t> m_unassignedTerms;
		struct VAD { uint64_t bindId; int vadPower; };
		VAD m_currentVad;
		//---
		rtl::ArrayT<VideoFrameElement*> m_zOrder;
		//---
		// draw data
		Bitmap m_canvas;
		ImageDrawer m_drawer;
		//---
		Size m_size;
		Color m_backColor;
		Color m_foreColor;
		bool m_useVadDetector;

		rtl::String m_faceName;
		int m_fontHeight;
		const Font* m_defaultFont;

		// standard bitmaps storage:
		static const int NoVideo = 0;
		static const int NoSignal = 1;

		Bitmap* m_imageStorage[2];
		Color m_backColors[2];

#if TRACE_VIDEO
		FileStream m_trace;
#endif

	public:
		RTX_MMT_API VideoFrameGenerator();
		RTX_MMT_API ~VideoFrameGenerator();

		RTX_MMT_API bool initialize(const char* json);
		RTX_MMT_API void destroy();

		RTX_MMT_API void updateVideoStream(uint64_t bindId, const VideoImage* image);
		RTX_MMT_API void updateVideoStreamVAD(uint64_t bindId, bool VAD);
		RTX_MMT_API uint64_t bindVideoStream(uint32_t termId, uint32_t videoTermId);
		RTX_MMT_API void unbindVideoStream(uint64_t bindId);
		RTX_MMT_API const Bitmap* getPicture();

	// internal!
	public:
		void addZOrder(VideoFrameElement* element);
		void updateAreaFor(VideoFrameElement* element);
		void updateArea(const Rectangle& rect);

		Color getBackColor() { return m_backColor; }
		Color getForeColor() { return m_foreColor; }
		const Font* getDefaultFont() { return m_defaultFont; }

		void drawNoSignalImage(const Rectangle& rect);
		void drawNoVideoImage(const Rectangle& rect);

		bool useVadDetector() { return m_useVadDetector; }
#if TRACE_VIDEO
		void trace(const char* fotmat, ...);
		void trace(VideoFrameElement* element);
#endif

	private:
		bool readParams(JsonParser& parser);
		bool readFrameParams(JsonParser& parser);
		bool readFrameElement(JsonParser& parser);
		bool readElements(JsonParser& parser);
		bool readElement(JsonParser& parser);
		bool readTemplates(JsonParser& parser);
		bool readTemplate(JsonParser& parser);

		bool addElement(const VideoRawElement& element);
		bool addTemplate(const rtl::StringList& propList);
		void resetBitmap();

		uint64_t bindToFree(uint64_t bindId);
		void recalculateVad();
#if TRACE_VIDEO
		void openTrace();
		void closeTrace();
#endif
		VideoFrameStream* findVideoStream(uint64_t bindId);
		VideoFrameStream* findVideoStream(uint64_t bindId, int* index);
	};
}
//-----------------------------------------------
