﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

//--------------------------------------
//
//--------------------------------------
#define FS_MAX_PATH 260
#if defined(TARGET_OS_WINDOWS)
#define FS_PATH_DELIMITER '\\'
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
#define FS_PATH_DELIMITER '/'
#endif
//--------------------------------------
//
//--------------------------------------
struct fs_entry_info_t
{
	bool fs_directory;
	int fs_attributes;
	uint64_t fs_filesize;
	char fs_filename[FS_MAX_PATH];
	
};
//--------------------------------------
//
//--------------------------------------
RTX_STD_API bool fs_is_file_exist(const char* path);
RTX_STD_API bool fs_delete_file(const char* path);
RTX_STD_API bool fs_directory_exists(const char* path);
RTX_STD_API bool fs_directory_create(const char* path);
RTX_STD_API bool fs_directory_delete(const char* path);
RTX_STD_API int fs_directory_get_entries(const char* path, rtl::ArrayT<fs_entry_info_t>& list);
RTX_STD_API bool fs_check_path_and_create(const char* path);
//---
RTX_STD_API bool fs_ensure_dir(const char* path);
RTX_STD_API rtl::String fs_dirname(const char* path);
RTX_STD_API rtl::String fs_basename(const char* path);
RTX_STD_API rtl::String fs_join(const char* path1, const char* path2);
RTX_STD_API int fs_split(const char* path, rtl::StringList& str_array);

class filelib
{
public:
	static bool ensure_dir(const char* path);
	static size_t file_size(const char* filepath);
	static rtl::PonterArrayT<void*>* fold_files(const char* path, const char* mask, bool recursive, void* (*fold_handler)(const char* path, const char* filename));
	static bool is_directory(const char* path);
	static bool is_file(const char* path);
	static bool is_regular(const char* path);
	static rtl::DateTime last_modified(const char* path);
	static int wildcard(const char* path, rtl::StringList& result);
};

class filename
{
public:
	static rtl::String absname(const char* filename);
	static rtl::String absname_join(const char* path, const char* filename);
	static rtl::String basename(const char* filepath);
	static rtl::String dirname(const char* filepath);
	static rtl::String extension(const char* filepath);
	static rtl::String join(const char* path1, const char* path2);
	static rtl::String join(const rtl::StringList& components);
	static rtl::String nativename(const char* path);
	static int pathtype(const char* path);
	static rtl::String rootname(const char* path);
	static rtl::String rootname(const char* path, const char* ext);
	static int split(const char* path, rtl::StringList& components);
};
//--------------------------------------
