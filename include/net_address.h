﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

//--------------------------------------
// преобрлзование строки в IP адрес
//--------------------------------------
RTX_NET_API in_addr net_parse(const char* addr_str);
RTX_NET_API bool net_parse(const char* addr_str, in_addr& addr, uint16_t& port);
RTX_NET_API const char* net_to_string(char* buffer, int size, in_addr addr);
RTX_NET_API const char* net_to_string(char* buffer, int size, in_addr addr, uint16_t port);
//--------------------------------------
//
//--------------------------------------
class ip4_address_t
{
	in_addr m_ipv4;

public:
	ip4_address_t() { memset(&m_ipv4, 0, sizeof m_ipv4); }
	ip4_address_t(in_addr address) { m_ipv4 = address; }
	ip4_address_t(const ip4_address_t& address) { m_ipv4 = address.m_ipv4; }
	ip4_address_t(const char* address) { assign(address); }

	ip4_address_t& operator = (in_addr address) { m_ipv4 = address; return *this; }
	ip4_address_t& operator = (const char* address) { return assign(address); }
	ip4_address_t& operator = (const ip4_address_t& address) { m_ipv4 = address.m_ipv4; return *this; }

	operator in_addr() const { return m_ipv4; }

	RTX_NET_API ip4_address_t& assign(const char* buffer);

	RTX_NET_API int to_string(char* buffer, int size);
	RTX_NET_API const char* to_string() const { return inet_ntoa(m_ipv4); }
	RTX_NET_API static ip4_address_t parse(const char* buffer);

	bool is_loopback() const { return m_ipv4.s_addr == INADDR_LOOPBACK; }
	bool is_valid() const { return m_ipv4.s_addr != INADDR_ANY && m_ipv4.s_addr != INADDR_NONE; }
	bool is_multicast() const;
	bool is_unicast() const;
	bool is_anycast() const;
	bool is_private() const;
};

//--------------------------------------
