﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
#include "rtx_stdlib.h"

namespace rtl
{

#ifndef INFINITE
#define INFINITE (0xFFFFFFFF)
#endif

	class Mutex
	{
#ifdef TARGET_OS_WINDOWS
		mutable CRITICAL_SECTION m_cs;
		Mutex(const Mutex& lock) { }
		Mutex& operator = (const Mutex& lock) { }
	public:

#pragma warning(suppress: 28125)
		Mutex() { InitializeCriticalSection(&m_cs); }
		~Mutex() { DeleteCriticalSection(&m_cs); }

		void wait(uint32_t timeout = 0) const { EnterCriticalSection(&m_cs); }
		void signal() const { LeaveCriticalSection(&m_cs); }
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		mutable pthread_mutex_t	m_mutex;
	public:
		RTX_STD_API Mutex();
		RTX_STD_API ~Mutex();

		RTX_STD_API void wait(uint32_t timeout = 0) const;
		RTX_STD_API void signal() const;
#else
#error unsupported operating system!
#endif
	};
	class MutexWatch
	{
		MutexWatch(const MutexWatch& lock) { }
		MutexWatch& operator = (const MutexWatch& lock) { return *this; }

#ifdef TARGET_OS_WINDOWS
		mutable CRITICAL_SECTION m_cs;
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		mutable pthread_mutex_t	m_mutex;
#endif
		uintptr_t m_id;

	public:
		RTX_STD_API MutexWatch(const char* name);
		RTX_STD_API ~MutexWatch();

		RTX_STD_API void wait(const char* proc) const;
		RTX_STD_API void signal() const;

		RTX_STD_API void set_name(const char* name);
		RTX_STD_API static void setup_watcher(bool watcherEnable, int timerPeriod);
	};
	//--------------------------------------
	// класс для автоматического лока и анлока критической секции в пределах области видимости
	//--------------------------------------
	template<class T> class Synchro
	{
		const T& m_sync;
	public:
		Synchro(const T& sync, uint32_t timeout = INFINITE) : m_sync(sync) { m_sync.wait(timeout); }
		Synchro(const T& sync, const char* func) : m_sync(sync) { m_sync.wait(func); }
		~Synchro() { m_sync.signal(); }
	};
	//--------------------------------------
	//
	//--------------------------------------
	typedef Synchro<MutexWatch> MutexWatchLock;
	typedef Synchro<Mutex> MutexLock;

}
//--------------------------------------
