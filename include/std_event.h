﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

namespace rtl
{
	//--------------------------------------
	//
	//--------------------------------------
	class Event
	{
#if defined(TARGET_OS_WINDOWS)
		HANDLE m_handle;

		Event(const Event&) : m_handle(nullptr) { }
		Event& operator = (const Event&) { return *this; }

	public:
		Event() : m_handle(nullptr) { }
		~Event() { close(); }

		operator HANDLE () { return m_handle; }

		bool operator == (HANDLE ptr) const { return m_handle == ptr; }
		bool operator != (HANDLE ptr) const { return m_handle != ptr; }
		bool is_valid() const { return m_handle != nullptr; }

		bool create(bool manual, bool signaled) { close(); return (m_handle = CreateEvent(nullptr, manual, signaled, nullptr)) != nullptr; }
		void close() { m_handle != nullptr ? CloseHandle(m_handle) : (BOOL)0; m_handle = nullptr; }

		bool signal() { return m_handle != nullptr ? SetEvent(m_handle) != FALSE : false; }
		bool reset() { return m_handle != nullptr ? ResetEvent(m_handle) != FALSE : false; }
		bool wait(uint32_t timeout = INFINITE) { return m_handle != nullptr ? WaitForSingleObject(m_handle, timeout) == WAIT_OBJECT_0 : false; }
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		bool m_autoreset;
		volatile bool m_signaled;
		pthread_mutex_t m_mutex;
		pthread_cond_t m_cond;
		bool m_created;

		Event(const Event&) { }
		Event& operator = (const Event&) { return *this; }

	public:
		RTX_STD_API Event();
		~Event() { close(); }

		bool is_valid() const { return true; }

		RTX_STD_API bool create(bool manual, bool signaled);
		RTX_STD_API void close();

		RTX_STD_API bool signal();
		RTX_STD_API bool reset();
		RTX_STD_API bool wait(uint32_t timeout = INFINITE);
#else
#error unsupported operating system!
#endif
	};
}
//--------------------------------------
