﻿/* RTX H.248 Media Gate. Crypto Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#if defined (TARGET_OS_WINDOWS)
#ifdef RTX_CRYPTO_EXPORTS
#define RTX_CRYPTO_API __declspec(dllexport)
#else
#define RTX_CRYPTO_API __declspec(dllimport)
#endif
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
#define RTX_CRYPTO_API
#endif
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "std_basedef.h"
#include "std_typedef.h"
#include "std_funcdef.h"
#include "std_logger.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define CRYPTO_LOGGING	m_log && rtl::Logger::check_trace(TRF_FLAG14)
#define PLOG_CRYPTO_WRITE		if (CRYPTO_LOGGING)	m_log->log
#define PLOG_CRYPTO_WARNING		if (CRYPTO_LOGGING)	m_log->log_warning
#define PLOG_CRYPTO_ERROR		if (CRYPTO_LOGGING)	m_log->log_error
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
extern "C"
{
	
}
//------------------------------------------------------------------------------

