﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mmt_lazy_writer_rtp.h"

namespace media
{
	//--------------------------------------
	// читатель rtp файла
	//--------------------------------------
	class FileReaderRTP
	{
		RTPFileHeader m_header;

		uint8_t* m_buffer;
		int m_length;
		int m_readPtr;

	public:
		RTX_MMT_API FileReaderRTP();
		~FileReaderRTP() { close(); }

		const RTPFileHeader& get_header() { return m_header; }

		RTX_MMT_API bool eof();
		RTX_MMT_API bool open(const char* filepath);
		RTX_MMT_API void close();

		RTX_MMT_API const RTPFilePacketHeader* readNext();
	};
}
