/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_media_tools.h"

namespace media
{
	enum class ColorPlane
	{
		Blue = 0,
		Green = 1,
		Red = 2,
		Alpha = 3
	};

	//-----------------------------------------------
	//
	//-----------------------------------------------
	enum class Transform
	{
		Copy,		// ! copying image to center of new image (overlay)
		ScaleHorz,	// ! scale by width
		ScaleVert,	// ! scale by height
		Stretch,	// ! stretch

		Rotate,		// not implemented
		FlipHorz,	// not implemented
		FlipVert,	// not implemented
	};

#pragma pack(push, 1)
	struct Color
	{
		uint8_t blue;
		uint8_t green;
		uint8_t red;
		uint8_t alpha;

		RTX_MMT_API static const Color Black;
		RTX_MMT_API static const Color White;
		RTX_MMT_API static const Color Green;
		RTX_MMT_API static const Color Red;
		RTX_MMT_API static const Color Blue;
		RTX_MMT_API static const Color Magenta;
		RTX_MMT_API static const Color Cyan;
		RTX_MMT_API static const Color Yellow;
		RTX_MMT_API static const Color None;


	};

	struct BitmapFileHeader
	{
		uint16_t type;
		uint32_t size;
		uint16_t reserved1;
		uint16_t reserved2;
		uint32_t off_bits;

		void clear() { memset(this, 0, sizeof(BitmapFileHeader)); }
	};

	struct BitmapInformationHeader
	{
		uint32_t size;
		uint32_t width;
		uint32_t height;
		uint16_t planes;
		uint16_t bit_count;
		uint32_t compression;
		uint32_t size_image;
		uint32_t x_pels_per_meter;
		uint32_t y_pels_per_meter;
		uint32_t clr_used;
		uint32_t clr_important;

		void clear() { memset(this, 0, sizeof(BitmapInformationHeader)); }
	};
#pragma pack(pop)

	struct HSV
	{
		float H;
		float S;
		float V;
	};

	/* Author: Jan Winkler */

	/*! \brief Convert RGB to HSV color space

	  Converts a given set of RGB values `r', `g', `b' into HSV
	  coordinates. The input RGB values are in the range [0, 1], and the
	  output HSV values are in the ranges h = [0, 360], and s, v = [0,
	  1], respectively.

	  \param fR Red component, used as input, range: [0, 1]
	  \param fG Green component, used as input, range: [0, 1]
	  \param fB Blue component, used as input, range: [0, 1]
	  \param fH Hue component, used as output, range: [0, 360]
	  \param fS Hue component, used as output, range: [0, 1]
	  \param fV Hue component, used as output, range: [0, 1]

	*/
	void RGBtoHSV(const Color& rgb, HSV& hsv);

	/*! \brief Convert HSV to RGB color space

	  Converts a given set of HSV values `h', `s', `v' into RGB
	  coordinates. The output RGB values are in the range [0, 1], and
	  the input HSV values are in the ranges h = [0, 360], and s, v =
	  [0, 1], respectively.

	  \param fR Red component, used as output, range: [0, 1]
	  \param fG Green component, used as output, range: [0, 1]
	  \param fB Blue component, used as output, range: [0, 1]
	  \param fH Hue component, used as input, range: [0, 360]
	  \param fS Hue component, used as input, range: [0, 1]
	  \param fV Hue component, used as input, range: [0, 1]

	*/
	void HSVtoRGB(Color& rgb, const HSV& hsv);

	static inline int luma(const Color& rgb) // color can be a hx string or an array of RGB values 0-255
	{
		return (int)((0.2126 * rgb.red) + (0.7152 * rgb.green) + (0.0722 * rgb.blue)); // SMPTE C, Rec. 709 weightings
	}


	static inline Color ContraColor(const Color& color)
	{
		return luma(color) >= 165 ? Color::Black : Color::White;
	}

	static inline Color hexToColor(const char* text)
	{
		uint32_t value = strtoul(text, nullptr, 16);
		return *(Color*)&value;
	}

	class Bitmap
	{
		int m_width;		// px
		int m_height;		// px
		Color* m_bitmap;	// bitmap
		int m_length;		// px

	private:
		inline int toIndex(int x, int y) const { return y * m_width + x; }

		void transformCopy(Bitmap& image, int width, int height) const;
		void transformStretch(Bitmap& image, int width, int height, int params) const;
		void transformScaleVert(Bitmap& image, int width, int height, int params) const;
		void transformScaleHorz(Bitmap& image, int width, int height, int params) const;

	public:
		static const int BytesPerPixel = 4;
		static const int BytesPerPixel24 = 3;

	public:
		Bitmap() : m_width(0), m_height(0), m_bitmap(nullptr), m_length(0) { }
		Bitmap(const char* filename) : Bitmap() { loadBitmapFile(filename); }
		Bitmap(int width, int height) : Bitmap() { createBitmap(width, height); }
		Bitmap(const Bitmap& image) : Bitmap() { *this = image; }
		~Bitmap() { destroy(); }
		
		
		RTX_MMT_API Bitmap& operator = (const Bitmap& image);

		bool operator ! () const { return !isValid(); }
		bool isValid() const { return m_bitmap != nullptr && m_width > 0 && m_height > 0; }
		void clear() { m_bitmap ? memset(m_bitmap, 0, m_length * sizeof(Color)) : nullptr; }
		
		RTX_MMT_API void fill(Color x);
		RTX_MMT_API void createBitmap(int width, int height);
		RTX_MMT_API void resize(int width, int height, bool clear = false);
		RTX_MMT_API void destroy();

		uint8_t getRedChannel(int x, int y) const { return m_bitmap[toIndex(x, y)].red; }
		uint8_t getGreenChannel(int x, int y) const { return m_bitmap[toIndex(x, y)].green; }
		uint8_t getBlueChannel(int x, int y) const { return m_bitmap[toIndex(x, y)].blue; }
		uint8_t getAlphaChannel(int x, int y) const { return m_bitmap[toIndex(x, y)].alpha; }

		void setRedChannel(int x, int y, uint8_t value) { m_bitmap[toIndex(x, y)].red = value; }
		void setGreenChannel(int x, int y, uint8_t value) { m_bitmap[toIndex(x, y)].green = value; }
		void setBlueChannel(int x, int y, uint8_t value) { m_bitmap[toIndex(x, y)].blue = value; }
		void setAlphaChannel(int x, int y, uint8_t value) { m_bitmap[toIndex(x, y)].alpha = value; }

		Color getPixel(int x, int y) const { return m_bitmap[toIndex(x, y)]; }
		void setPixel(int x, int y, Color colour) { m_bitmap[toIndex(x, y)] = colour; }

		inline int getRowDataLength() const { return m_width * BytesPerPixel; }

		const Color* getRow(int rowIndex) const { return &m_bitmap[rowIndex * m_width]; }
		Color* getRow(int rowIndex) { return &m_bitmap[rowIndex * m_width]; }
		const uint8_t* getRowData(int rowIndex) const { return (uint8_t*)&m_bitmap[rowIndex * m_width]; }
		uint8_t* getRowData(int rowIndex) { return (uint8_t*)&m_bitmap[rowIndex * m_width]; }

		RTX_MMT_API bool copyFrom(const Bitmap& image);
		RTX_MMT_API bool copyFrom(const Bitmap& source_image, int x_offset, int y_offset);
		RTX_MMT_API void transformTo(Bitmap& image, int width, int height, Transform mode, int params = 0) const;
		RTX_MMT_API bool getRegion(int x, int y, int width, int height, Bitmap& dest_image) const;

		RTX_MMT_API bool setRegion(int x, int y, int width, int height, uint8_t value);
		RTX_MMT_API bool setRegion(int x, int y, int width, int height, ColorPlane plane, uint8_t value);
		RTX_MMT_API bool setRegion(int x, int y, int width, int height, Color colour);

		RTX_MMT_API void reflectiveImage(Bitmap& image, bool includeDiagnols = false);

		RTX_MMT_API void saveBitmapFile(const char* filename) const;

		RTX_MMT_API bool loadBitmap(rtl::Stream& filename);
		RTX_MMT_API bool loadBitmapFile(const char* filename);
		RTX_MMT_API bool loadBitmapStruct(const uint8_t* argb, int length);

		RTX_MMT_API bool loadBitmapBase64(const char* base64, int length);

		RTX_MMT_API bool setBitmapBits(int width, int height, const uint8_t* argb, int length);
		RTX_MMT_API bool setBitmapBitsRGB24(int width, int height, const uint8_t* rgb24, int length);

		RTX_MMT_API void setChannel(ColorPlane color, uint8_t value);
		RTX_MMT_API void rorChannel(ColorPlane color, int ror);

		RTX_MMT_API void invertColorPlanes();
		RTX_MMT_API void toGrayscale();

		inline int getDataLength() const { return m_length * BytesPerPixel; }
		inline const uint8_t* getData() const { return (const uint8_t*)m_bitmap; }
		inline uint8_t* getData() { return (uint8_t*)m_bitmap; }

		inline int getPixelCount() const { return m_length; }
		inline const Color* getBitmap() const { return m_bitmap; }
		inline Color* getBitmap() { return m_bitmap; }
		inline int getHeight() const { return m_height; }
		inline int getWidth() const { return m_width; }

		RTX_MMT_API void horizontalFlip();
		RTX_MMT_API void verticalFlip();
		RTX_MMT_API void subsample(Bitmap& dest) const;
		RTX_MMT_API	void upsample(Bitmap& dest) const;

		static inline int offset(ColorPlane color) { return (int)color; }
		static void transformImage(const Bitmap& source, Bitmap& dest, int width, int height, Color emptyColor = { 0,0,0,0 });

	private:
		const uint8_t* end() const { return (const uint8_t*)(m_bitmap + m_length); }
		uint8_t* end() { return (uint8_t*)(m_bitmap + m_length); }

		static inline bool isBigEndian() { uint32_t v = 0x01; return 1 != ((char*)&v)[0]; }
		static inline uint16_t flip(uint16_t v) { return ((v >> 8) | (v << 8)); }
		static inline uint32_t flip(uint32_t v) { return (((v & 0xFF000000) >> 0x18) | ((v & 0x000000FF) << 0x18) | ((v & 0x00FF0000) >> 0x08) | ((v & 0x0000FF00) << 0x08)); }

		template <typename T>
		static inline void readFrom(rtl::Stream& stream, T& t) { stream.read(&t); }
		template <typename T>
		static inline void writeTo(rtl::Stream& stream, const T& t) { stream.write(&t, sizeof(T)); }

		void readBitmapFileHeader(rtl::Stream& stream, BitmapFileHeader& bfh);
		void writeBitmapFileHeader(rtl::Stream& stream, const BitmapFileHeader& bfh) const;
		void readBitmapInformationHeader(rtl::Stream& stream, BitmapInformationHeader& bih);
		void writeBitmapInformationHeader(rtl::Stream& stream, const BitmapInformationHeader& bih) const;
	};

	inline bool operator == (const Color& c0, const Color& c1) { return (c0.red == c1.red) && (c0.green == c1.green) && (c0.blue == c1.blue); }
	inline bool operator != (const Color& c0, const Color& c1) { return (c0.red != c1.red) || (c0.green != c1.green) || (c0.blue != c1.blue); }
}
