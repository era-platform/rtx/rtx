﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mmt_resampler.h"
#include "mmt_recorder.h"
#include "mmt_file_wave.h"

namespace media
{
	//--------------------------------------
	//
	//--------------------------------------
	class RecorderMonoWave : public RecorderMono
	{
		FileWriterWAV m_file;
		WAVFormat* m_fileFormat;
		int m_fileFormatSize;

		short* m_pcmBuffer;
		int m_pcmWritePos;

		// Ресамплер: всегда вызывается в цепочке первым (вся работа идет по секунде)
		Resampler* m_resampler;
		short* m_resampleBuffer;

		// декодирование
		mg_audio_encoder_t* m_enc;
		uint8_t* m_encBuffer;
		int m_encBufferSize;

	public:
		RTX_MMT_API RecorderMonoWave(int freq);
		RTX_MMT_API virtual ~RecorderMonoWave();

		RTX_MMT_API virtual bool create(const char* filePath, const char* outEncoding, int outFrequency, const char* outFMTP);
		RTX_MMT_API virtual void close();
		RTX_MMT_API virtual bool flush();
		RTX_MMT_API virtual int write(const short* samples, int count);

	private:
		int resampleBlock(const short* samples, int count);
		int encodeBlock(const short* samples, int count);

		bool setupGSM610();
		bool setupG711(const char* encoding);
	};
	//--------------------------------------
	//
	//--------------------------------------
	class RecorderStereoWave : public RecorderStereo
	{
		FileWriterWAV m_file;
		WAVFormat m_fileFormat;

		// Ресамплер: всегда вызывается в цепочке первым (вся работа идет по секунде)
		Resampler* m_resampler;
		short* m_resampleLeft;
		short* m_resampleRight;
		short* m_writeBuffer;

	public:
		RTX_MMT_API RecorderStereoWave(int freq);
		RTX_MMT_API virtual ~RecorderStereoWave();

		RTX_MMT_API virtual bool create(const char* filePath, const char* outEncoding, int outFrequency, const char* outFMTP);
		RTX_MMT_API virtual void close();
		RTX_MMT_API virtual bool flush();
		RTX_MMT_API virtual int write(const short* leftChannel, const short* rightChannel, int samplesPerChannel);

	private:
		int resample(const short* leftChannel, const short* rightChannel, int samplesPerChannel);
		int transform(const short* leftChannel, const short* rightChannel, int samplesPerChannel);
	};
}
//--------------------------------------
