﻿/* RTX H.248 Media Gate. Codec Interface Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "std_basedef.h"
#include "std_typedef.h"
#include "std_funcdef.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#if defined (TARGET_OS_WINDOWS)
	#ifdef RTX_CODEC_EXPORTS
		#define RTX_CODEC_API __declspec(dllexport)
	#else
		#define RTX_CODEC_API __declspec(dllimport)
	#endif
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	#define RTX_CODEC_API
#endif
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "rtx_stdlib.h"
#include "mmt_payload_format.h"
#include "mmt_payload_set.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_CODEC_API rtl::String rtx_codec__get_audio_codec_list();
RTX_CODEC_API rtl::String rtx_codec__get_video_codec_list();
RTX_CODEC_API rtl::String rtx_codec__get_codec_list();
RTX_CODEC_API bool rtx_codec__isKnownFormat(const char* encoding);
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_CODEC_API bool rtx_codec__get_available_audio_codecs(media::PayloadSet& set);
RTX_CODEC_API bool rtx_codec__get_available_video_codecs(media::PayloadSet& set);
	
//------------------------------------------------------------------------------
// Параматр: путь до модуля, путь для создания лога к модулю; путь до следующего модуля, путь для лога.
// Пример параметра:
// C:\Develop\rtx_mg3\x64\Debug\rtx_711_module.dll, C:\Develop\rtx_mg3\Log;
// C:\Develop\rtx_mg3\x64\Debug\rtx_729_module.dll, C:\Develop\rtx_mg3\Log;
//------------------------------------------------------------------------------
RTX_CODEC_API bool rtx_codec__load_all_codecs(const char* folder_path, const char* codec_list);
RTX_CODEC_API bool rtx_codec__load_audio_codec(const char* codecpath);
RTX_CODEC_API bool rtx_codec__load_video_codec(const char* codecpath);
RTX_CODEC_API void rtx_codec__unload_codecs();
//------------------------------------------------------------------------------
