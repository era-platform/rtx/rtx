﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

namespace rtl
{

	//--------------------------------------
	//
	//--------------------------------------
	class Semaphore
	{
#ifdef TARGET_OS_WINDOWS
		HANDLE m_handle;
		Semaphore(const Semaphore& lock) { }
		Semaphore& operator = (const Semaphore& lock) { }
	public:

#pragma warning(suppress: 28125)
		Semaphore() : m_handle(nullptr) { }
		~Semaphore() { close(); }

		bool create(int minValue, int maxValue) { return (m_handle = CreateSemaphore(nullptr, minValue, maxValue, nullptr)) != nullptr; }
		void close() { m_handle != nullptr ? CloseHandle(m_handle) : BOOL(0); m_handle = nullptr; }

		bool wait(uint32_t timeout) { return m_handle != nullptr ? WaitForSingleObject(m_handle, timeout) == WAIT_OBJECT_0 : false; }
		bool signal() { return m_handle != nullptr ? ReleaseSemaphore(m_handle, 1, nullptr) != FALSE : false; }
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		sem_t m_handle;

	public:
		Semaphore() { memset(&m_handle, 0, sizeof(m_handle)); }
		~Semaphore() { close(); }
		bool create(int minValue, int maxValue) { return sem_init(&m_handle, 0, minValue) == 0; }
		void close() { sem_destroy(&m_handle); memset(&m_handle, 0, sizeof m_handle); }
		bool wait(uint32_t timeout) { return sem_wait(&m_handle) == 0; }
		bool signal();// { return sem_port(&m_handle) == 0; }
#else
#error unsupported operating system!
#endif
	};
}
//--------------------------------------
