﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

namespace h248
{

	//--------------------------------------
   // идентификаторы
   //--------------------------------------
#define MG_TERM_CHOOSE 0xFFFFFFFE		// $
#define MG_TERM_ALL 0xFFFFFFFF			// *
#define MG_TERM_ROOT 0x0				// ROOT
#define MG_CONTEXT_CHOOSE 0xFFFFFFFE	// $
#define MG_CONTEXT_ALL 0xFFFFFFFF		// *
#define MG_CONTEXT_NULL 0x0				// - (NULL)
#define MG_PORT_CHOOSE 0xFFFFFFFF		// $ for media port

	//--------------------------------------
	//
	//--------------------------------------
	enum class Token
	{
		Add,						// "Add"					AddToken
		AndLgc,					// "ANDLgc"					AndAUDITSelectToken
		Audit,					// "Audit"					AuditToken
		AuditCapability,			// "AuditCapability"		AuditCapToken
		AuditValue,				// "AuditValue"				AuditValueToken
		Authentication,			// "Authentication"			AuthToken 

		Both,					// "Both"					BothToken 
		Bothway,					// "Bothway"				BothwayToken 
		Brief,					// "Brief"					BriefToken 
		Buffer,					// "Buffer"					BufferToken 

		Context,					// "Context"				CtxToken 
		ContextAttr,				// "ContextAttr"			ContextAttrToken 
		ContextAudit,			// "ContextAudit"			ContextAuditToken 
		ContextList,				// "ContextList"			ContextListToken 

		Delay,					// "Delay"					DelayToken 
		DigitMap,				// "DigitMap"				DigitMapToken 
		Disconnected,			// "Disconnected"			DisconnectedToken 
		Duration,				// "Duration"				DurationToken 

		Embed,					// "Embed"					EmbedToken 
		Emergency,				// "Emergency"				EmergencyToken 
		EmergencyOff,			// "EmergencyOff"			EmergencyOffToken 
		EmergencyValue,			// "EmergencyValue"			EmergencyValueToken
		END,						// "END"					SegmentationCompleteToken
		Error,					// "Error"					ErrorToken 
		EventBuffer,				// "EventBuffer"			EventBufferToken 
		Events,					// "Events"					EventsToken 
		External,				// "External"				ExternalToken 

		Failover,				// "Failover"				FailoverToken
		Forced,					// "Forced"					ForcedToken 

		Graceful,				// "Graceful"				GracefulToken

		H221,					// "H221"					H221Token 
		H223,					// "H223"					H223Token 
		H226,					// "H226"					H226Token 
		HandOff,					// "HandOff"				HandOffToken

		IEPSCall,				// "IEPSCall"				IEPSToken 
		ImmAckRequired,			// "ImmAckRequired"			ImmAckRequiredToken
		ImmediateNotify,			// "ImmediateNotify"		NotifyImmediateToken 
		Inactive,				// "Inactive"				InactiveToken 
		InService,				// "InService"				InSvcToken 
		IntByEvent,				// "IntByEvent"				InterruptByEventToken 
		IntBySigDescr,			// "IntBySigDescr"			InterruptByNewSignalsDescrToken 
		Internal,				// "Internal"				InternalToken 
		Intersignal,				// "Intersignal"			IntsigDelayToken 
		Isolate,					// "Isolate"				IsolateToken 
		Iteration,				// "Iteration"				IterationToken 

		KeepActive,				// "KeepActive"				KeepActiveToken 

		Local,					// "Local" ,				LocalToken 
		LocalControl,			// "LocalControl" ,			LocalControlToken
		LockStep,				// "LockStep" ,				LockStepToken 
		Loopback,				// "Loopback" ,				LoopbackToken 

		Media,					// "Media" ,				MediaToken 
		MEGACO,								// "MEGACO" ,				MegacopToken 
		Method,					// "Method" ,				MethodToken 
		MgcIdToTry,				// "MgcIdToTry" ,			MgcIdToken 
		Mode,					// "Mode" ,					ModeToken 
		Modem,					// "Modem" ,				ModemToken 
		Modify,					// "Modify" ,				ModifyToken 
		Move,					// "Move" ,					MoveToken 
		MTP,						// "MTP",					MTPToken 
		Mux,						// "Mux" ,					MuxToken 

		NeverNotify,				// "NeverNotify"			NeverNotifyToken 
		Notify,					// "Notify"					NotifyToken 
		NotifyCompletion,		// "NotifyCompletion"		NotifyCompletionToken
		Nx64Kservice,			// "Nx64Kservice"			Nx64kToken 

		ObservedEvents,			// "ObservedEvents"			ObservedEventsToken
		Oneway,					// "Oneway" ,				OnewayToken 
		OnewayBoth,				// "OnewayBoth"				OnewayBothToken 
		OnewayExternal,			// "OnewayExternal"			OnewayExternalToken
		OnOff,					// "OnOff"					OnOffToken 
		ORLgc,					// "ORLgc"					OrAUDITselectToken 
		OtherReason,				// "OtherReason"			OtherReasonToken 
		OutOfService,			// "OutOfService"			OutOfSvcToken 

		Packages,				// "Packages"				PackagesToke
		Pending,					// "Pending"				PendingToken 
		Priority,				// "Priority"				PriorityToke
		Profile,					// "Profile"				ProfileToken 

		Reason,					// "Reason"					ReasonToken 
		ReceiveOnly,				// "ReceiveOnly"			RecvonlyToken
		RegulatedNotify,			// "RegulatedNotify"		NotifyRegulatedToken 
		Remote,					// "Remote"					RemoteToken 
		Reply,					// "Reply"					ReplyToken 
		ReservedGroup,			// "ReservedGroup"			ReservedGroupToken 
		ReservedValue,			// "ReservedValue"			ReservedValueToken 
		ResetEventsDescriptor,	// "ResetEventsDescriptor"	ResetEventsDescriptorToken
		Restart,					// "Restart"				RestartToken 

		Segment,					// "Segment" ,				MessageSegmentToken
		SendOnly,				// "SendOnly"				SendonlyToken 
		SendReceive,				// "SendReceive"			SendrecvToken 
		Services,				// "Services"				ServicesToken 
		ServiceStates,			// "ServiceStates"			ServiceStatesToken 
		ServiceChange,			// "ServiceChange"			ServiceChangeToken 
		ServiceChangeAddress,	// "ServiceChangeAddress"	ServiceChangeAddressToken 
		ServiceChangeInc,		// "ServiceChangeInc"		ServiceChangeIncompleteToken 
		SignalList,				// "SignalList"				SignalListToken 
		Signals,					// "Signals"				SignalsToken 
		SignalType,				// "SignalType"				SignalTypeToken 
		SPADirection,			// "SPADirection",			DirectionToken
		SPARequestID,			// "SPARequestID"			RequestIDToken 
		Statistics,				// "Statistics"				StatsToken 
		Stream,					// "Stream"					StreamToken 
		Subtract,				// "Subtract"				SubtractToken 
		SynchISDN,				// "SynchISDN"				SynchISDNToken 

		TerminationState,		// "TerminationState"		TerminationStateToken 
		Test,					// "Test"					TestToken 
		TimeOut,					// "TimeOut"				TimeOutToken 
		Topology,				// "Topology"				TopologyToken 
		Transaction,				// "Transaction"			TransToken 
		TransactionResponseAck,	// "TransactionResponseAck"	ResponseAckToken

		V18,						// "V18"					V18Token 
		V22,						// "V22"					V22Token 
		V22b,					// "V22b"					V22bisToken 
		V32,						// "V32"					V32Token 
		V32b,					// "V32b"					V32bisToken 
		V34,						// "V34"					V34Token 
		V76,						// "V76"					V76Token 
		V90,						// "V90"					V90Token 
		V91,						// "V91"					V91Token 
		Version,					// "Version"				VersionToken

		_Last,			// last token for checking range of known tokens
		_Custom = -2,	// custom token
		_Error = -1,		// error
	};
	//--------------------------------------
	//
	//--------------------------------------
	enum class TerminationStreamMode
	{
		SendOnly = (int)Token::SendOnly,
		RecvOnly = (int)Token::ReceiveOnly,
		SendRecv = (int)Token::SendReceive,
		Loopback = (int)Token::Loopback,
		Inactive = (int)Token::Inactive,
		Error = (int)Token::Error
	};
	//--------------------------------------
	//
	//--------------------------------------
	enum class TopologyDirection
	{
		Bothway = (int)Token::Bothway,
		Isolate = (int)Token::Isolate,
		Oneway = (int)Token::Oneway,
		OnewayBoth = (int)Token::OnewayBoth,
		OnewayExternal = (int)Token::OnewayExternal,
		Error = (int)Token::_Error,
	};
	//--------------------------------------
	//
	//--------------------------------------
	enum class TerminationServiceState
	{
		InService = (int)Token::InService,
		OutOfService = (int)Token::OutOfService,
		Test = (int)Token::Test,
	};
	//--------------------------------------
	//
	//--------------------------------------
	enum class TerminationType
	{
		Root,
		SRTP,
		RTP,
		WebRTC,
		All,			// wildcard ALL '*'
		Choose,		// wildcard CHOOSE '$'
		IVR_Player,
		IVR_Record,
		Fax_T30,
		Fax_T38,
		Device,
		Dummy
	};
	//--------------------------------------
	//
	//--------------------------------------
	enum class TransportType
	{
		Nil = 0,			// неопределенный тип
		// non reliable
		UDP = 1,			// стандартный канал на udp
		// reliable
		TCP = 2,			// стандартный канал на tcp
		SCTP = 3,			// стандартный канал на sctp
		// unknown
		Error = -1,			// ошибка определения
	};
	//--------------------------------------
	/*
	Error code #: 400
	Name: Syntax error in message

	code #: 401
	Name: Protocol error

	Error code #: 402
	Name: Unauthorized

	Error code #: 403
	Name: Syntax error in TransactionRequest

	4.2.5 Error code #: 406
	Name: Version not supported

	4.2.6 Error code #: 410
	Name: Incorrect identifier

	4.2.7 Error code #: 411
	Name: The transaction refers to an unknown ContextID

	4.2.8 Error code #: 412
	Name: No ContextIDs available

	4.2.9 Error code #: 413
	Name: Number of transactions in message exceeds maximum

	4.2.10 Error code #: 421
	Name: Unknown action or illegal combination of actions

	4.2.11 Error code #: 422
	Name: Syntax error in action

	4.2.12 Error code #: 430
	Name: Unknown TerminationID

	4.2.13 Error code #: 431
	Name: No TerminationID matched a wildcard

	4.2.14 Error code #: 432
	Name: Out of TerminationIDs or no TerminationID available

	4.2.15 Error code #: 433
	Name: TerminationID is already in a context

	4.2.16 Error code #: 434
	Name: Max number of terminations in a context exceeded

	4.2.17 Error code #: 435
	Name: Termination ID is not in specified context

	4.2.18 Error code #: 440
	Name: Unsupported or unknown package

	4.2.19 Error code #: 441
	Name: Missing remote or local descriptor

	4.2.20 Error code #: 442
	Name: Syntax error in command

	4.2.21 Error code #: 443
	Name: Unsupported or unknown command

	4.2.22 Error code #: 444
	Name: Unsupported or unknown descriptor

	4.2.23 Error code #: 445
	Name: Unsupported or unknown property

	4.2.24 Error code #: 446
	Name: Unsupported or unknown parameter

	4.2.25 Error code #: 447
	Name: Descriptor not legal in this command

	4.2.26 Error code #: 448
	Name: Descriptor appears twice in a command

	4.2.27 Error code #: 449
	Name: Unsupported or unknown parameter or property value

	4.2.28 Error code #: 450
	Name: No such property in this package

	4.2.29 Error code #: 451
	Name: No such event in this package

	4.2.30 Error code #: 452
	Name: No such signal in this package

	4.2.31 Error code #: 453
	Name: No such statistic in this package

	4.2.32 Error code #: 454
	Name: No such parameter value in this package

	4.2.33 Error code #: 455
	Name: Property illegal in this descriptor

	4.2.34 Error code #: 456
	Name: Property appears twice in this descriptor

	4.2.35 Error code #: 457
	Name: Missing parameter in signal or event

	4.2.36 Error code #: 458
	Name: Unexpected event/request ID

	4.2.37 Error code #: 460
	Name: Unable to set statistic on stream

	4.2.38 Error code #: 471
	Name: Implied add for multiplex failure

	4.2.39 Error code #: 472
	Name: Required information missing

	4.2.40 Error code #: 473
	Name: Conflicting property values

	4.2.41 Error code #: 500
	Name: Internal software failure in the MG or MGC

	4.2.42 Error code #: 501
	Name: Not implemented

	4.2.43 Error code #: 502
	Name: Not ready

	4.2.44 Error code #: 503
	Name: Service unavailable

	4.2.45 Error code #: 504
	Name: Command received from unauthorized entity

	4.2.46 Error code #: 505
	Name: Transaction request received before a ServiceChange reply has been received

	4.2.47 Error code #: 506
	Name: Number of TransactionPendings exceeded

	4.2.48 Error code #: 507
	Name: Unknown Control Association

	4.2.49 Error code #: 510
	Name: Insufficient resources

	4.2.50 Error code #: 511
	Name: Temporarily busy

	4.2.51 Error code #: 512
	Name: Media gateway unequipped to detect requested event

	4.2.52 Error code #: 513
	Name: Media gateway unequipped to generate requested signals

	4.2.53 Error code #: 514
	Name: Media gateway cannot send the specified announcement

	4.2.54 Error code #: 515
	Name: Unsupported media type

	4.2.55 Error code #: 517
	Name: Unsupported or invalid mode

	4.2.56 Error code #: 518
	Name: Event buffer full

	4.2.57 Error code #: 519
	Name: Out of space to store digit map

	4.2.58 Error code #: 520
	Name: Digit map undefined in the MG

	4.2.59 Error code #: 521
	Name: Termination is "Service Changing"

	4.2.60 Error code #: 522
	Name: Functionality requested in topology triple not supported

	4.2.61 Error code #: 526
	Name: Insufficient bandwidth

	4.2.62 Error code #: 529
	Name: Internal hardware failure in MG

	4.2.63 Error code #: 530
	Name: Temporary network failure

	4.2.64 Error code #: 531
	Name: Permanent network failure

	4.2.65 Error code #: 532
	Name: Audited property, statistic, event or signal does not exist

	4.2.66 Error code #: 533
	Name: Response exceeds maximum transport PDU size

	4.2.67 Error code #: 534
	Name: Illegal write or read-only property

	4.2.68 Error code #: 542
	Name: Command is not allowed on this termination

	4.2.69 Error code #: 543
	Name: MGC requested event detection timestamp not supported

	4.2.70 Error code #: 581
	Name: Does not exist
	*/
	//--------------------------------------
	enum ErrorCode
	{
		c400_Syntax_error = 400,
		c401_Protocol_error = 401,
		c402_Unauthorized = 402,
		c403_Syntax_error_in_TransactionRequest = 403,
		c406_Version_not_supported = 406,
		c410_Incorrect_identifier = 410,
		c411_The_transaction_refers_to_an_unknown_ContextID = 411,
		c412_No_ContextIDs_available = 412,
		c413_Number_of_transactions_in_message_exceeds_maximum = 413,
		c421_Unknown_action_or_illegal_combination_of_actions = 421,
		c422_Syntax_error_in_action = 422,
		c430_Unknown_TerminationID = 430,
		c431_No_TerminationID_matched_a_wildcard = 431,
		c432_Out_of_TerminationIDs_or_no_TerminationID_available = 432,
		c433_TerminationID_is_already_in_a_context = 433,
		c434_Max_number_of_terminations_in_a_context_exceeded = 434,
		c435_Termination_ID_is_not_in_specified_context = 435,
		c440_Unsupported_or_unknown_package = 440,
		c441_Missing_remote_or_local_descriptor = 441,
		c442_Syntax_error_in_command = 442,
		c443_Unsupported_or_unknown_command = 443,
		c444_Unsupported_or_unknown_descriptor = 444,
		c445_Unsupported_or_unknown_property = 445,
		c446_Unsupported_or_unknown_parameter = 446,
		c447_Descriptor_not_legal_in_this_command = 447,
		c448_Descriptor_appears_twice_in_a_command = 448,
		c449_Unsupported_or_unknown_parameter_or_property_value = 449,
		c450_No_such_property_in_this_package = 450,
		c451_No_such_event_in_this_package = 451,
		c452_No_such_signal_in_this_package = 452,
		c453_No_such_statistic_in_this_package = 453,
		c454_No_such_parameter_value_in_this_package = 454,
		c455_Property_illegal_in_this_descriptor = 455,
		c456_Property_appears_twice_in_this_descriptor = 456,
		c457_Missing_parameter_in_signal_or_event = 457,
		c458_Unexpected_event_request_ID = 458,
		c460_Unable_to_set_statistic_on_stream = 460,
		c471_Implied_add_for_multiplex_failure = 471,
		c472_Required_information_missing = 472,
		c473_Conflicting_property_values = 473,
		c500_Internal_software_failure_in_the_MG_or_MGC = 500,
		c501_Not_implemented = 501,
		c502_Not_ready = 502,
		c503_Service_unavailable = 503,
		c504_Command_received_from_unauthorized_entity = 504,
		c505_Transaction_request_received_before_a_ServiceChange_reply_has_been_received = 505,
		c506_Number_of_TransactionPendings_exceeded = 506,
		c507_Unknown_Control_Association = 507,
		c510_Insufficient_resources = 510,
		c511_Temporarily_busy = 511,
		c512_Media_gateway_unequipped_to_detect_requested_event = 512,
		c513_Media_gateway_unequipped_to_generate_requested_signals = 513,
		c514_Media_gateway_cannot_send_the_specified_announcement = 514,
		c515_Unsupported_media_type = 515,
		c517_Unsupported_or_invalid_mode = 517,
		c518_Event_buffer_full = 518,
		c519_Out_of_space_to_store_digit_map = 519,
		c520_Digit_map_undefined_in_the_MG = 520,
		c521_Termination_is_Service_Changing = 521,
		c522_Functionality_requested_in_topology_triple_not_supported = 522,
		c526_Insufficient_bandwidth = 526,
		c529_Internal_hardware_failure_in_MG = 529,
		c530_Temporary_network_failure = 530,
		c531_Permanent_network_failure = 531,
		c532_Audited_property_statistic_event_or_signal_does_not_exist = 532,
		c533_Response_exceeds_maximum_transport_PDU_size = 533,
		c534_Illegal_write_or_readonly_property = 534,
		c542_Command_is_not_allowed_on_this_termination = 542,
		c543_MGC_requested_event_detection_timestamp_not_supported = 543,
		c581_Does_not_exist = 581,

		_Custom = 0,
	};

	/*
	5.2.1 Reason #: 900
	Name: Service restored

	5.2.2 Reason #: 901
	Name: Cold boot

	Comment: This reason code only applies for TerminationID root.
	5.2.3 Reason #: 902
	Name: Warm boot

	5.2.4 Reason #: 903
	Name: MGC directed change

	5.2.5 Reason #: 904
	Name: Termination malfunctioning

	5.2.6 Reason #: 905
	Name: Termination taken out of service

	5.2.7 Reason #: 906
	Name: Loss of lower layer connectivity (e.g., downstream sync)

	5.2.8 Reason #: 907
	Name: Transmission failure

	5.2.9 Reason #: 908
	Name: MG impending failure

	5.2.10 Reason #: 909
	Name: MGC impending failure

	5.2.11 Reason #: 910
	Name: Media capability failure

	5.2.12 Reason #: 911
	Name: Modem capability failure

	5.2.13 Reason #: 912
	Name: MUX capability failure

	5.2.14 Reason #: 913
	Name: Signal capability failure

	5.2.15 Reason #: 914
	Name: Event capability failure

	5.2.16 Reason #: 915
	Name: State loss

	5.2.17 Reason #: 916
	Name: Packages change

	5.2.18 Reason #: 917
	Name: Capabilities change

	5.2.19 Reason #: 918
	Name: Cancel graceful

	5.2.20 Reason #: 919
	Name: Warm failover

	5.2.21 Reason #: 920
	Name: Cold failover
	*/

	//--------------------------------------
	//
	//--------------------------------------
	enum class Reason
	{
		c900_Service_restored = 900,
		c901_Cold_boot = 901,
		c902_Warm_boot = 902,
		c903_MGC_directed_change = 903,
		c904_Termination_malfunctioning = 904,
		c905_Termination_taken_out_of_service = 905,
		c906_Loss_of_lower_layer_connectivity = 906, // (e.g., _downstream_sync)
		c907_Transmission_failure = 907,
		c908_MG_impending_failure = 908,
		c909_MGC_impending_failure = 909,
		c910_Media_capability_failure = 910,
		c911_Modem_capability_failure = 911,
		c912_MUX_capability_failure = 912,
		c913_Signal_capability_failure = 913,
		c914_Event_capability_failure = 914,
		c915_State_loss = 915,
		c916_Packages_change = 916,
		c917_Capabilities_change = 917,
		c918_Cancel_graceful = 918,
		c919_Warm_failover = 919,
		c920_Cold_failover = 920,

		_Error = 0,
	};
}
//--------------------------------------
