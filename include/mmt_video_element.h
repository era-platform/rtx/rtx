﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "std_json.h"

namespace media
{
	using namespace rtl;
	//-----------------------------------------------
	// известные типы
	//-----------------------------------------------
	enum class VideoElementType
	{
		Text,				// текстовое поле
		Bitmap,				// картина
		Time,				// текстовое поле с отрисовкой текущего времени
		VideoStream,		// видео поток 
		Custom,				// для будущего использования, рисование с помощью колбеков
	};
	VideoElementType VideoElementType_parse(const char* text);
	const char* VideoElementType_toString(VideoElementType type);
	//-----------------------------------------------
	// video mode
	//-----------------------------------------------
	enum class VideoStreamMode
	{
		Auto,		// авто подбор  из стека незанятых
		Term,		// жесткая привязка терма к полю вывода потока видео
		Vad,		// поле рисует активнеых в данный момент
		Error,		// ошибка -- ничего не выводим
	};
	VideoStreamMode VideoStreamMode_parse(const char* text);
	const char* VideoStreamMode_toString(VideoStreamMode type);
	//-----------------------------------------------
	// raw data from JSON
	//-----------------------------------------------
	struct VideoRawElement
	{
		char* id;
		int left, top, width, height, zOrder;
		VideoElementType type;
		Color backColor;
		Color foreColor;

		// text, format, bitmap(base64), termId
		char* stringValue;
		int stringLength;
		char* faceName;
		TextBackMode textMode;
		TextFormatFlags textFlags;
		// video
		uint32_t videoTermId;	// id from 
		int titleHeight;
		VideoStreamMode videoMode;

		// time
		int timezone;

	public:
		RTX_MMT_API VideoRawElement();
		~VideoRawElement() { clean(); }

		RTX_MMT_API void read(JsonParser& parser);
		RTX_MMT_API void clean();

		void setString(const char* text) { rtl::strupdate(stringValue, text); }
	};
	//-----------------------------------------------
	// предварительная декларация
	//-----------------------------------------------
	class VideoFrameGenerator;
	//-----------------------------------------------
	// базовый элемент кадра видео потока
	//-----------------------------------------------
	class VideoFrameElement
	{
	protected:
		rtl::String m_id;					// произвольный идентификатор. задается из вне. дубликаты запрещены
		VideoFrameGenerator& m_frame;	// ссылка на фрей для обратной связи
		Rectangle m_bounds;				// расположение на фрейме
		VideoElementType m_type;
		Color m_backColor;				// видео, текст и картинки используют для заполнения неиспользуемого пространства элемента
		uint32_t m_z;					// номер плоскости при рисовании. порядок рисования от 0 (нижний слой, начало списка)
										// к большим номерам (конец списка)
		bool m_updated;

	public:
		// !!! Временно -- после написания специализированных элементов доступ к конструктору убрать!!!
		RTX_MMT_API VideoFrameElement(VideoFrameGenerator& frame, VideoElementType type, const rtl::String& id);
		RTX_MMT_API virtual ~VideoFrameElement();

		RTX_MMT_API virtual void assign(const VideoRawElement& data);
		RTX_MMT_API virtual void drawOn(ImageDrawer& draw);
		RTX_MMT_API virtual bool isUpdated();

		const Rectangle& getBounds() { return m_bounds; }
		int getX() const { return m_bounds.left; }
		int getY() const { return m_bounds.top; }
		int getWidth() const { return m_bounds.right - m_bounds.left; }
		int getHeight() const { return m_bounds.bottom - m_bounds.top; }

		const char* getId() { return m_id; }
		VideoElementType getType() const { return m_type; }
		const Color& getBackColor() const { return m_backColor; }
		int getZOrder() const { return m_z; }
	};
}
