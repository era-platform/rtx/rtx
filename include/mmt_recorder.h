﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

namespace media
{
	//--------------------------------------
	//
	//--------------------------------------
	class RecorderMono
	{
		RecorderMono() { }

	protected:
		int m_frequency;
		int m_stat_PCMWritten;

		RecorderMono(int freq) : m_frequency(freq) { }

	public:
		RTX_MMT_API virtual ~RecorderMono();

		virtual bool create(const char* filePath, const char* outEncoding, int outFrequency, const char* outFMTP) = 0;
		virtual void close() = 0;
		virtual bool flush() = 0;
		virtual int write(const short* samples, int count) = 0;

		int statSamplesWritten() { return m_stat_PCMWritten; }

		RTX_MMT_API static RecorderMono* create(int inFrequency, const char* outEncoding);
	};
	//--------------------------------------
	//
	//--------------------------------------
	class RecorderStereo
	{
		RecorderStereo() { }

	protected:
		int m_frequency;
//		int m_inChannels;

		RecorderStereo(int frequency) : m_frequency(frequency) { }

	public:

		RTX_MMT_API virtual ~RecorderStereo();

		RTX_MMT_API virtual bool create(const char* filePath, const char* outEncoding, int outFrequency, const char* outFMTP) = 0;
		RTX_MMT_API virtual void close() = 0;
		RTX_MMT_API virtual bool flush() = 0;
		RTX_MMT_API virtual int write(const short* leftChannel, const short* rightChannel, int samplesPerChannel) = 0;

		RTX_MMT_API static RecorderStereo* create(int inFrequency, const char* outEncoding);
	};
}
//--------------------------------------
