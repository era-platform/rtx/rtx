﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "std_stream_base.h"

namespace rtl
{
	//--------------------------------------
	//
	//--------------------------------------
	class FileStream : public Stream
	{
#if defined(TARGET_OS_WINDOWS)
		HANDLE m_handle;
#define INVALID_FILE_HANDLE INVALID_HANDLE_VALUE
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		int m_handle;
#define INVALID_FILE_HANDLE -1
#endif

		FileStream(const FileStream&);
		FileStream& operator=(const FileStream&);

	public:
		RTX_STD_API FileStream();
		RTX_STD_API virtual ~FileStream();

		// открывает файл для чтения.
		// если файл не существует - возвращает false.
		RTX_STD_API bool open(const char* filename);

		// открывает файл для записи.
		// если файл не существует, создает новый.
		// если файл не существует, перезаписывает его, обнуляя размер.
		RTX_STD_API bool createNew(const char* filename);

		// открывает файл для записи.
		// если файл не существует, создает новый.
		// если файл существует, открывает для дозаписи в конец.
		RTX_STD_API bool create_or_append(const char* filename);

		RTX_STD_API virtual void close();

		RTX_STD_API virtual size_t getLength();
		RTX_STD_API virtual size_t setLength(size_t length);

		RTX_STD_API virtual size_t getPosition();
		RTX_STD_API virtual size_t seek(SeekOrigin origin, intptr_t pos);

		RTX_STD_API virtual size_t write(const void* data, size_t len);
		RTX_STD_API virtual size_t read(void* buffer, size_t size);

		bool isOpened() { return m_handle != INVALID_FILE_HANDLE; }
		bool isVpened() { return m_handle != INVALID_FILE_HANDLE; }

	private:
		bool _set_file_size(size_t size);
		bool _seek_position_begin(intptr_t pos);
		bool _seek_position_current(intptr_t pos);
		bool _seek_position_end(intptr_t pos);
	};
}
//-----------------------------------------------
