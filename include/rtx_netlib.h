﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "std_basedef.h"
#include "std_typedef.h"
#include "std_funcdef.h"
#if defined(TARGET_OS_FREEBSD)
#include <sys/socket.h>
#endif
//--------------------------------------
// The following ifdef block is the standard way of creating macros which make exporting
// from a DLL simpler. All files within this DLL are compiled with the RTX_NETLIB_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see
// RTX_NET_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
//--------------------------------------
#if defined (TARGET_OS_WINDOWS)
#ifdef RTX_NETLIB_EXPORTS
#define RTX_NET_API __declspec(dllexport)
#else
#define RTX_NET_API __declspec(dllimport)
#endif
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
#define RTX_NET_API
#endif

#if defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
typedef int SOCKET;
#define INVALID_SOCKET (-1)
#define SOCKET_ERROR (-1)
#define closesocket close

#define SD_BOTH     SHUT_RDWR
#define SD_SEND     SHUT_WR
#define SD_RECEIVE  SHUT_RD

#define std_get_error errno
#define sockaddrlen_t uint32_t

#define ioctlsocket ioctl

#elif defined(TARGET_OS_WINDOWS)
#define std_get_error GetLastError()
#define sockaddrlen_t int
#endif
//--------------------------------------
//
//--------------------------------------
inline static bool operator == (in_addr l, in_addr r) { return l.s_addr == r.s_addr; }
inline static bool operator != (in_addr l, in_addr r) { return l.s_addr != r.s_addr; }
//--------------------------------------
//
//--------------------------------------
inline rtl::MemoryStream& operator << (rtl::MemoryStream& stream, in_addr address) { char buff[32]; stream << inet_ntop(AF_INET, &address, buff, 32); return stream; }
inline rtl::MemoryStream& operator << (rtl::MemoryStream& stream, in6_addr address) { char buff[64];  stream << inet_ntop(AF_INET6, &address, buff, 64); return stream; }

//--------------------------------------
//
//--------------------------------------
#include "net_address.h"
#include "net_sock.h"
#include "net_manager.h"
//--------------------------------------
//
//--------------------------------------
RTX_NET_API uint32_t netlib_get_version();
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
class socket_address;
struct net_sender_handler
{
	virtual bool send_data(const uint8_t* data, uint32_t len, const socket_address* saddr) = 0;
};
//--------------------------------------
