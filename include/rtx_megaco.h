﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "std_basedef.h"
#include "std_typedef.h"
#include "std_funcdef.h"

 //--------------------------------------
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the RTX_MEGACO_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// RTX_MG_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
//--------------------------------------
#if defined (TARGET_OS_WINDOWS)
#ifdef RTX_MG_EXPORTS
#define RTX_MG_API __declspec(dllexport)
#else
#define RTX_MG_API __declspec(dllimport)
#endif
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
#define RTX_MG_API
#endif
//--------------------------------------
//
//--------------------------------------
#include "mg_types.h"
#include "mg_message.h"
//--------------------------------------
//
//--------------------------------------
extern "C"
{
	RTX_MG_API uint32_t megaco_get_version();
	RTX_MG_API void megaco_set_output_mode(const h248::MessageWriterParams* params);
	RTX_MG_API void megaco_get_output_mode(h248::MessageWriterParams* params);
	RTX_MG_API void megaco_register_counters();
}

namespace h248
{
	// for mg3 functions only
	RTX_MG_API void MegacoAsyncCall(rtl::async_call_target_t* target, uint16_t callId, intptr_t intParam, void* ptrParam);
	RTX_MG_API void MegacoAsyncCall(rtl::pfn_async_callback_t callback, void* user, uint16_t callId, intptr_t intParam, void* ptrParam);
	//--------------------------------------
	//
	//--------------------------------------
	extern RTX_MG_API rtl::Logger LogProto;
}
//--------------------------------------
