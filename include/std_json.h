/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

namespace rtl {

	//-----------------------------------------------
	//
	//-----------------------------------------------
	enum class JsonTypes
	{
		Object,
		Array,
		Boolean,
		Integer,
		Float,
		String,
		Null,
	};
	//-----------------------------------------------
	//
	//-----------------------------------------------
	enum class JsonState
	{
		Initial,	// -> Array, Object
		Array,		// -> ArrayValue
		ArrayEnd,	//    pop
		Object,		// -> Member
		ObjectEnd,	//    pop
		Member,		// -> Member | ObjectEnd
		ArrayValue,	// -> ArrayValue | ArrayEnd
		Eof,		// -> Eof
		Error		// -> Error
	};
	//-----------------------------------------------
	//
	//-----------------------------------------------
	class JsonParser
	{
		// stream
		const char* m_json;
		const char* m_readPtr;
		// property
		String m_fieldName;
		JsonTypes m_fieldType;
		// value
		bool m_b_value;
		int64_t m_i_value;
		double m_f_value;
		String m_s_value;
		// current state
		JsonState m_state;
		// state stack (Member, Array)
		Stack<JsonState> m_stateStack;

	public:
		RTX_STD_API JsonParser();
		RTX_STD_API ~JsonParser();

		RTX_STD_API bool start(const char* text);
		RTX_STD_API void close();

		RTX_STD_API JsonState read(); // array, object property, value

		const String& getFieldName() { return m_fieldName; }
		JsonTypes getFieldType() { return m_fieldType; }
		bool getBoolean() { return m_b_value; }
		int64_t getInteger() { return m_i_value; }
		double getFloat() { return m_f_value; }
		const String& getString() { return m_s_value; }
		RTX_STD_API String getValueAsString();

	private:
		JsonState readMember();
		JsonState readValue(); // ?
		JsonState readArrayValue();
		JsonState readStringValue();
		JsonState readNumberValue();
	};
}
//-----------------------------------------------
