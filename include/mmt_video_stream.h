/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mmt_video_element.h"
#include "mmt_yuv_image.h"

namespace media
{
	//-----------------------------------------------
	// ����� ����� ������
	//-----------------------------------------------
	class VideoFrameStream : public VideoFrameElement
	{
		uint64_t m_bindId;			// �������� ����� ������ � �������������
		int m_titleHeight;
		rtl::String m_videoTitle;
		VideoStreamMode m_mode;
		Bitmap m_picture;
		bool m_hasStream;
		//uint8_t m_vadLevel;
		bool m_vad;
		int m_updateCounter;
		rtl::Mutex m_sync;

	public:
		VideoFrameStream(VideoFrameGenerator& frame, const rtl::String id) : VideoFrameElement(frame, VideoElementType::VideoStream, id),
			m_bindId(0), m_mode(VideoStreamMode::Auto), m_hasStream(false), m_vad(false), m_updateCounter(0) { } // , m_vadLevel(0)
		RTX_MMT_API virtual ~VideoFrameStream();

		uint64_t getVideoTermId() const { return m_bindId; }
		void setVideoTitle(const rtl::String& videoTitle) { m_videoTitle = videoTitle; }
		VideoStreamMode getMode() { return m_mode; }

		RTX_MMT_API virtual void assign(const VideoRawElement& data);
		RTX_MMT_API virtual void drawOn(ImageDrawer& draw);
		RTX_MMT_API virtual bool isUpdated();

		RTX_MMT_API void updateImage(const media::VideoImage* image);

		RTX_MMT_API void updateVAD(bool vadDetected);
		RTX_MMT_API void stopStream();

		static const uint64_t EmptyId = 0;

	public: // internal
		uint64_t bindToTermimation(uint64_t termId);

	private:
		void drawVadBar(ImageDrawer& draw);

	};

	//-----------------------------------------------
	// ����� ����� ������
	//-----------------------------------------------
	class VideoFrameVadStream : public VideoFrameElement
	{
		Bitmap m_picture;
		bool m_hasStream;
		bool m_vad;
		int m_updateCounter;
		rtl::Mutex m_sync;

	public:
		VideoFrameVadStream(VideoFrameGenerator& frame, const rtl::String id) : VideoFrameElement(frame, VideoElementType::VideoStream, id),
			m_hasStream(false), m_vad(false), m_updateCounter(0) { }
		RTX_MMT_API ~VideoFrameVadStream();

		RTX_MMT_API virtual void assign(const VideoRawElement& data);
		RTX_MMT_API virtual void drawOn(ImageDrawer& draw);
		RTX_MMT_API virtual bool isUpdated();

		RTX_MMT_API void updateImage(const media::VideoImage* image);

		RTX_MMT_API void updateVAD(bool vadDetected);
		RTX_MMT_API void stopStream();

	};
}
//-----------------------------------------------
