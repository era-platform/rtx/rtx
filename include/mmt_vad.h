﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_media_tools.h"

namespace media
{
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	class VAD_Detecter
	{
		struct VAD_Analyzer* m_vadAnalyzer;
		rtl::Logger* m_log;
	public:
		RTX_MMT_API VAD_Detecter(rtl::Logger* log);
		RTX_MMT_API ~VAD_Detecter();

		RTX_MMT_API bool create(int frequency, int threshold, int vadDuration, int silenceDuration);
		RTX_MMT_API void destroy();

		RTX_MMT_API bool addSilence(int sample_count, bool& vad, int& duration);
		RTX_MMT_API bool addPacket(const short* samples, int length, bool& vad_detected, int& duration);
	};
}
//-----------------------------------------------
