﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mmt_recorder.h"
#include "mmt_file_mp3.h"

namespace media
{
	//--------------------------------------
	//
	//--------------------------------------
	class RecorderMonoMp3 : public RecorderMono
	{
		FileWriterMP3 m_file;
	public:
		RTX_MMT_API RecorderMonoMp3(int freq) : RecorderMono(freq) { }
		RTX_MMT_API virtual ~RecorderMonoMp3();

		RTX_MMT_API virtual bool create(const char* filePath, const char* outEncoding, int outFrequency, const char* outFMTP);
		RTX_MMT_API virtual void close();
		RTX_MMT_API virtual bool flush();
		RTX_MMT_API virtual int write(const short* samples, int count);
	};
	//--------------------------------------
	//
	//--------------------------------------
	class RecorderStereoMp3 : public RecorderStereo
	{
		FileWriterMP3 m_file;
	public:
		RTX_MMT_API RecorderStereoMp3(int freq) : RecorderStereo(freq) { }
		RTX_MMT_API virtual ~RecorderStereoMp3();

		RTX_MMT_API virtual bool create(const char* filePath, const char* outEncoding, int outFrequency, const char* outFMTP);
		RTX_MMT_API virtual void close();
		RTX_MMT_API virtual bool flush();
		RTX_MMT_API virtual int write(const short* leftChannel, const short* rightChannel, int samplesPerChannel);
	};
}
//--------------------------------------
