﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

namespace h248
{
	//--------------------------------------
	//
	//--------------------------------------
	class Field
	{
		Token m_type;						// стандартный тип или MEGACO_CUSTOM_Token
		rtl::String m_name;							// 'имя' или 'пакет/имя'
		Relation m_relation;					// отношение имени и значения
		rtl::String m_value;							// если значения нет то пустая строка!
		rtl::ArrayT<Field*> m_subList;	// ветки если есть
		bool m_hasRawData;						// признак сырых данных в подгруппе
		rtl::String m_rawData;						// для хранения сырых данных в нодах Local, Remote, Error
		Field* m_parent;				// ссылка на верхний уровень (может и быть NULL)

	public:
		RTX_MG_API Field();
		RTX_MG_API Field(const char* name, const char* value = nullptr);
		RTX_MG_API Field(const char* name, uint32_t value);
		RTX_MG_API Field(Token type, const char* value = nullptr);
		RTX_MG_API Field(Token type, uint32_t value);
		RTX_MG_API Field(const Field& node);

		RTX_MG_API ~Field();
		RTX_MG_API void cleanup();

		RTX_MG_API Field& operator = (const Field& node);

		Field* getParent() const { return m_parent; }

		const rtl::String& getName() const { return m_name; }
		RTX_MG_API void setName(const char* name);

		Token getType() const { return m_type; }
		RTX_MG_API void setType(Token type);

		Relation getRelation() const { return m_relation; }
		void setRelation(Relation relation) { m_relation = relation; }

		const rtl::String& getValue() const { return m_value; }

		RTX_MG_API void setValue(const char* value);
		RTX_MG_API void setValue(uint32_t value);

		bool hasRawData() const { return m_hasRawData; }
		const rtl::String& getRawData() const { return m_rawData; }
		RTX_MG_API void setRawData(const char* value, bool quoted = false);

		int getFieldCount() const { return m_subList.getCount(); }
		const Field* getFieldAt(int index) const { return index >= 0 && index < m_subList.getCount() ? m_subList.getAt(index) : nullptr; }
		Field* getFieldAt(int index) { return m_subList.getAt(index); }

		RTX_MG_API Field* addField(const Field* node);
		RTX_MG_API Field* addField(Field* node, bool attach);
		RTX_MG_API Field* addField(const char* name, const char* value = nullptr);
		RTX_MG_API Field* addField(const char* name, uint32_t value);
		RTX_MG_API Field* addField(Token type, const char* value = nullptr);
		RTX_MG_API Field* addField(Token type, uint32_t value);
		RTX_MG_API Field* addErrorDescriptor(int error_code, const char* error_reason = nullptr);

		RTX_MG_API void remove(const char* path);
		RTX_MG_API void removeField(const char* name);
		RTX_MG_API void removeField(Token type);
		RTX_MG_API void removeField(Field* node);
		RTX_MG_API void removeFieldAt(int index);
		RTX_MG_API void removeAllFields();

		RTX_MG_API const Field* getField(const char* name, int length = -1) const;
		RTX_MG_API Field* getField(const char* name, int length = -1);
		RTX_MG_API const Field* getField(Token type) const;
		RTX_MG_API Field* getField(Token type);

		RTX_MG_API const Field* findField(const char* path) const;
		RTX_MG_API Field* findField(const char* path);

		RTX_MG_API bool read(Parser& parser);
		RTX_MG_API rtl::MemoryStream& writeTo(rtl::MemoryStream& stream, MessageWriterParams* params = nullptr) const;
	};
}
//--------------------------------------
