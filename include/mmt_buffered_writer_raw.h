﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mmt_buffered_writer.h"

namespace media
{
	//--------------------------------------
	// запись в raw файл входящего звукогого потока как есть
	//--------------------------------------
	class BufferedMediaWriterRaw : public BufferedMediaWriter
	{
		rtl::FileStream m_file;	// дескриптор файла записи

	//#ifdef _DEBUG
		rtl::FileStream m_dout;
		//#endif

	public:
		RTX_MMT_API BufferedMediaWriterRaw(rtl::Logger* log);
		RTX_MMT_API virtual ~BufferedMediaWriterRaw();

		RTX_MMT_API bool create(const char* path, int bufsize_ms);
		RTX_MMT_API void close();

		RTX_MMT_API bool write_packet(const rtp_packet* packet);

	private:
		virtual void recFileStarted();
		virtual void recFileStopped();
		virtual bool recFileWrite(void* data_block, uint32_t length, uint32_t* written);
	};
}
//--------------------------------------

