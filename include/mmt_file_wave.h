﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_media_tools.h"
#include "mmt_wave_format.h"

namespace media
{
	//--------------------------------------
	//
	//--------------------------------------
	class FileReaderWAV
	{
		rtl::FileStream m_file;

		WAVFormat* m_format;			// ms format type
		uint8_t* m_formatExtra;			// extra format parameters

		uint8_t* m_audioBuffer;			// кеш для чтения
		uint32_t m_audioBufferPtr;		// указатель в кеше
		uint32_t m_audioBufferWritten;	// размер

		uint32_t m_audioDataBegin;		// указатель позиции начала данных файлаж
		uint32_t m_audioRead;			// сколько всего считанно байт
		uint32_t m_audioLength;			// длина аудио данных в байтах

	public:
		RTX_MMT_API FileReaderWAV();
		~FileReaderWAV() { close(); }

		RTX_MMT_API bool open(const char* path);
		RTX_MMT_API void close();

		RTX_MMT_API bool eof();

		uint32_t getFormatTag() { return m_format != nullptr ? m_format->tag : -1; }
		uint32_t getChannelCount() { return m_format != nullptr ? m_format->channels : 0; }
		uint32_t getFrequency() { return m_format != nullptr ? m_format->samplesRate : 0; }
		uint32_t getFormatExtraSize() { return m_format != nullptr ? m_format->extraSize : 0; }
		const uint8_t* getExtraParameters() { return m_formatExtra; }
		const WAVFormat* getFormat() { return m_format; }
		uint32_t getAudioRead() { return m_audioRead; }
		uint32_t getAudioDataLength() { return m_audioLength; }

		RTX_MMT_API void reset();
		RTX_MMT_API int readAudioData(void* buffer, int size);

	private:
		void fillCache();
		int copyBlock(uint8_t* buffer, int size);
	};
	//--------------------------------------
	//
	//--------------------------------------
	class FileWriterWAV
	{
		rtl::FileStream m_file;			// файл
		uint32_t m_fileSizeOffset;		// смещение в файле для записи длины файла (riff)
		uint32_t m_audioSizeOffset;		// смещение в файле для записи длины аудио данных (data)
		uint32_t m_audioLength;			// длина аудио данных в байтах
		uint32_t m_factSizeOffset;
		uint8_t* m_audioBuffer;			// буфер накопления размером WAVE_BUFFER_SIZE
		uint32_t m_audioBufferSize;		// размер буфера
		uint32_t m_audioBufferPtr;		// указатель в буфере на свободный участок

	public:
		RTX_MMT_API FileWriterWAV(rtl::Logger* log);
		~FileWriterWAV() { close(); }

		RTX_MMT_API bool create(const char* filename, const WAVFormat* format);
		RTX_MMT_API void close();

		RTX_MMT_API bool flush();
		RTX_MMT_API int write(const void* data, int length);

	private:
		int writeBlock(const uint8_t* data, int size);
	};
}
//--------------------------------------
