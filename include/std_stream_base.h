﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#pragma once

namespace rtl
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	enum class SeekOrigin
	{
		Begin,
		Current,
		End,
	};
	//-----------------------------------------------
	//
	//-----------------------------------------------
	class Stream
	{
	public:
		RTX_STD_API ~Stream();

		virtual void close() = 0;

		virtual size_t getLength() = 0;
		virtual size_t setLength(size_t length) = 0;

		virtual size_t getPosition() = 0;
		void setPosition(size_t newPos) { seek(SeekOrigin::Begin, newPos); }
		virtual size_t seek(SeekOrigin origin, intptr_t pos) = 0;
		void seekEnd(size_t newPos) { seek(SeekOrigin::End, newPos); }
		void seekCurrent(size_t newPos) { seek(SeekOrigin::Current, newPos); }

		virtual size_t write(const void* data, size_t len) = 0;
		virtual size_t read(void* buffer, size_t size) = 0;

		bool read(uint8_t* var) { return read(&var, sizeof(*var)); }
		bool read(char* var) { return read(&var, sizeof(*var)); }
		bool read(int16_t* var) { return read(&var, sizeof(*var)); }
		bool read(uint16_t* var) { return read(&var, sizeof(*var)); }
		bool read(int32_t* var) { return read(&var, sizeof(*var)); }
		bool read(uint32_t* var) { return read(&var, sizeof(*var)); }
		bool read(int64_t* var) { return read(&var, sizeof(*var)); }
		bool read(uint64_t* var) { return read(&var, sizeof(*var)); }

		uint8_t readByte() { uint8_t v; read(&v, sizeof(v)); return v; }
		char readChar() { char v; read(&v, sizeof(v)); return v; }
		uint16_t readUInt16() { uint16_t v; read(&v, sizeof(v)); return v; }
		uint32_t readUInt32() { uint32_t v; read(&v, sizeof(v)); return v; }
		uint64_t readUInt64() { uint64_t v; read(&v, sizeof(v)); return v; }
		int16_t readInt16() { int16_t v; read(&v, sizeof(v)); return v; }
		int32_t readInt32() { int32_t v; read(&v, sizeof(v)); return v; }
		int64_t readInt64() { int64_t v; read(&v, sizeof(v)); return v; }

		void write(const char* zstr) { write(zstr, strlen(zstr)); }

		RTX_STD_API void writeFormatArgs(const char* fmt, va_list args);
		RTX_STD_API void writeFormat(const char* fmt, ...);
	};
}
//-----------------------------------------------
