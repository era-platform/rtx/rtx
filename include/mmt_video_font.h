/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mmt_winfon.h"

namespace media
{
	class ImageDrawer;

	class PixelMap
	{
	private:
		int m_width, m_height;
		int m_map_length;
		uint8_t* m_map;

		void writeByte(int x, int y, uint8_t bits);

	public:
		PixelMap() : m_width(0), m_height(0), m_map_length(0), m_map(nullptr) { }
		~PixelMap() { clean(); }

		RTX_MMT_API void load(const GlyphInfo& glyph);
		RTX_MMT_API void clean();

		int getWidth() const { return m_width; }
		int getHeight() const { return m_height; }
		bool getPixelState(int x, int y) const { return x < m_width && y < m_height ? m_map[y * m_width + x]  != 0 : false; }
		
	};
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	class Font
	{
		volatile int m_refCount;
		int m_fontHeight;
		rtl::String m_faceName;
		int m_firstChar;
		const FontInfo* m_info;
		rtl::ArrayT<PixelMap*> m_pixmap;

	public:
		Font() : m_refCount(1), m_fontHeight(0), m_firstChar(0), m_info(nullptr) { }
		~Font() { destroy(); }

		RTX_MMT_API void createFont(const char* faceName, int height, const FontInfo* info);
		RTX_MMT_API void destroy();

		RTX_MMT_API int addRef();
		RTX_MMT_API int release();

		RTX_MMT_API int getTextWidth(const char* text) const;
		
		const PixelMap* getCharPixelMap(int ch) const { return m_pixmap[ch - m_firstChar]; }
		const rtl::String& getFaceName() const { return m_faceName; }
		int getHeight() const { return m_fontHeight; }
	};
}
//-----------------------------------------------


