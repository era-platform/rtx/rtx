/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mmt_lazy_writer_rtp.h"
#include "mmt_file_rtp.h"
#include "mmt_video_jitter.h"
#include "mmt_video_reader.h"

namespace media
{
	//--------------------------------------
	//
	//--------------------------------------
	class VideoReaderRTP : public VideoReader
	{
		rtl::Mutex m_sync;
		FileReaderRTP m_file;

		/// ������� RTP �������
		struct RTPSorter
		{
			uint16_t seqBegin;
			uint16_t seqEnd;

			int packet_count;
			static const int PAKET_PER_SECOND = 100;
			RTPFilePacketHeader packets[PAKET_PER_SECOND];
		};

		bool m_newStream;
		RTPSorter *m_work, *m_wait;
		int m_workPtr;
		RTPFilePacketHeader* m_thirdBuffer;

		/// Jitter ����� ��� ������ �������
		VideoJitter m_jitter;

		struct DecoderInfo
		{
			RTPFileMediaFormat format;
			mg_video_decoder_t* decoder;
		};

		rtl::ArrayT<DecoderInfo> m_decoderList;

	public:
		RTX_MMT_API VideoReaderRTP(rtl::Logger* log);
		RTX_MMT_API virtual ~VideoReaderRTP() override;

		uint64_t get_start_time() { return m_file.get_header().rec_start_time; }
		uint32_t get_start_timestamp() { return m_file.get_header().rec_start_timestamp; }
		uint32_t get_stop_timestam() { return m_file.get_header().rec_stop_timestamp; }

	private:
		/// reader interface
		virtual bool openSource(const char* path) override;
		virtual void closeSource() override;
		virtual bool readImage(VideoImage* image) override;

		/// Image fill functions
		bool readFrame(VideoImage* image);
		bool fillJitterBuffer();
		bool getNextPacket(rtp_packet*);
		bool decodeFrame(VideoImage* image, VideoJitterBuffer* frame);
		/// rtp_packet sorter functions
		bool fillRTPBuffers();
		void updateRTPBuffers();
		/// private routines
		void initialize();
		void cleanup();
		/// decoders set
		void cleanupDecoders();
		void updateDecoders(const RTPFileMediaFormat* formats, int count);
		DecoderInfo* findDecoder(int payloadId);
		void addDecoder(const RTPFileMediaFormat* format);
	};
}
//-----------------------------------------------
