/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

namespace media
{
	//--------------------------------------
	// interlaced streo to mono
	// mono_buffer[samples_count]
	// stereo_i[samples_count * 2]
	//------------------------------------------------------------------------------
	RTX_MMT_API void mixer__stereo_to_mono_i(const short* stereo_i, short* mono_buffer, int samples_count);
	//------------------------------------------------------------------------------
	// different channels stereo to mono
	// stereo_left[samples_count]
	// stereo_right[samples_count]
	// mono_buffer[samples_count]
	//------------------------------------------------------------------------------
	RTX_MMT_API void mixer__stereo_to_mono_d(const short* stereo_left, const short* stereo_right, short* mono_buffer, int samples_count);
	//------------------------------------------------------------------------------
	// mono to interlaced stereo
	// mono[samples_count]
	// stereo_i_buffer[samples_count * 2]
	//------------------------------------------------------------------------------
	RTX_MMT_API void mixer__mono_to_stereo_i(const short* mono, short* stereo_i_buffer, int samples_count);
	//------------------------------------------------------------------------------
	// mono to different channels stereo
	// mono[samples_count]
	// stereo_left_buffer[samples_count]
	// stereo_right_buffer[samples_count]
	//------------------------------------------------------------------------------
	RTX_MMT_API void mixer__mono_to_stereo_d(const short* mono, short* stereo_left_buffer, short* stereo_right_buffer, int samples_count);
	//------------------------------------------------------------------------------
	// different channels stereo to interlaced stereo
	// stereo_left[samples_count]
	// stereo_right[samples_count]
	// stereo_i_buffer[samples_count * 2]
	//------------------------------------------------------------------------------
	RTX_MMT_API void mixer__stereo_d_to_i(const short* stereo_left, const short* stereo_right, short* stereo_i_buffer, int samples_count);
	//------------------------------------------------------------------------------
	// interlaced stereo to different channels stereo
	// stereo_i[samples_count * 2]
	// stereo_left_buffer[samples_count]
	// stereo_right_buffer[samples_count]
	//------------------------------------------------------------------------------
	RTX_MMT_API void mixer__stereo_i_to_d(const short* stereo_i, short* stereo_left_buffer, short* stereo_right_buffer, int samples_count);
}
//------------------------------------------------------------------------------
