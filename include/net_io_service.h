﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "net_sock.h"
//--------------------------------------
//
//--------------------------------------
class net_io_event_handler_t
{
public:
	virtual void net_io_data_ready(net_socket_t* sock) = 0;
	virtual void net_io_data_received(net_socket_t* sock, const uint8_t* data, uint32_t length, const sockaddr* from, int sockaddr_len) = 0;
};
//--------------------------------------
//
//--------------------------------------
#if defined (TARGET_OS_WINDOWS)
struct iocp_user_key;
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
#if defined(TARGET_OS_LINUX)
struct epoll_event;
#elif defined(TARGET_OS_FREEBSD)
struct kevent;
#endif
struct netio_thread_context;
#endif
//--------------------------------------
//
//--------------------------------------
class  net_io_service_t
{
public:
	RTX_NET_API net_io_service_t(rtl::Logger* log);
	RTX_NET_API ~net_io_service_t();

	RTX_NET_API bool create(uint32_t threads);
	RTX_NET_API void destroy();

	RTX_NET_API bool add(net_socket_t* sock, net_io_event_handler_t* handler);
	RTX_NET_API void remove(net_socket_t* sock);

	RTX_NET_API bool send_data_to(net_socket_t* sock, const uint8_t* data, uint32_t length, const sockaddr* to, int sockaddr_len);
private:
#if defined (TARGET_OS_WINDOWS)
	void main_loop();
	void start_async_recv(iocp_user_key* key);
	static void thread_start(rtl::Thread* thread, void* param);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	netio_thread_context* get_least_busy_ctx();
	void main_loop(netio_thread_context* ctx);
#if defined(TARGET_OS_LINUX)
	void on_epoll_event(epoll_event* epollev);
#elif defined(TARGET_OS_FREEBSD)
	void on_kevent(struct kevent* kev);
#endif
	static void thread_start(rtl::Thread* thread, void* param);
#endif

private:
	rtl::Logger* m_log;
	rtl::MutexWatch m_sync;
#if defined (TARGET_OS_WINDOWS)
	rtl::ArrayT<rtl::Thread*> m_thread_list;
	HANDLE m_iocp_handle;
	volatile bool m_stopping;
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	rtl::ArrayT<netio_thread_context*> m_context_list;
#endif
};

//--------------------------------------
