﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_codec.h"

#include "mmt_media_writer.h"
#include "mmt_payload_set.h"
#include "mmt_media_buffers.h"

namespace media
{
	//--------------------------------------
	// запись в файл входящего звукогого потока
	//--------------------------------------
	class BufferedMediaWriter : public MediaWriter
	{
		const int BUFFER_SIZE = 32 * 1024;	// размер блока

	protected:
		volatile bool m_created;			// признак создания файла
		volatile bool m_started;			// признак начала записи
		volatile bool m_flash;

		/// кеш записи
		media::AudioBuffer m_cache;				// буфер записи

		volatile size_t m_written_to_buffer;
		volatile size_t m_lost_in_buffer;
		volatile size_t m_written_to_file;
		volatile size_t m_lost_in_file;

	public:
		RTX_MMT_API bool start();
		RTX_MMT_API void stop();

	protected:
		BufferedMediaWriter(rtl::Logger* log);
		virtual ~BufferedMediaWriter();

		bool isStarted() { return m_started; }

		void initialize(int bufsize_ms);
		void destroy();

		virtual void recFileStarted() = 0;
		virtual void recFileStopped() = 0;
		virtual bool recFileWrite(void* data_block, uint32_t length, uint32_t* written) = 0;

		virtual void flush(bool close = false) override;
	};
}
//--------------------------------------

