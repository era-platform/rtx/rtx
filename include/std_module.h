﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

namespace rtl
{
	//--------------------------------------
	//
	//--------------------------------------
	class RTX_STD_API Module
	{
		String m_name;
#if defined(TARGET_OS_WINDOWS)
		HMODULE m_lib;
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		void* m_lib;
#endif

	public:
		Module();
		~Module();

		bool load(const char* module_path);
		void unload();

		void* bindSymbol(const char* symbol);
	};
}
//--------------------------------------
