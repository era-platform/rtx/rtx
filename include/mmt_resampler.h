/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

namespace media
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	class Resampler
	{
	public:
		virtual bool init(int inFreq, int inChannelCount, int outFreq, int outChannelCount) = 0;
		virtual void dispose() = 0;
		virtual int	resample(const short* source, int sourceLength, int* sourceUsed, short* destBuffer, int destBufferSize) = 0;
		virtual bool isSimple() = 0;
	};
	//-----------------------------------------------
	//
	//-----------------------------------------------
	RTX_MMT_API Resampler* createResampler(int inFreq, int inChannelCount, int outFreq, int outChannelCount);
	//-----------------------------------------------
	//
	//-----------------------------------------------
	RTX_MMT_API void destroyResampler(Resampler* resampler);
}
//-----------------------------------------------

