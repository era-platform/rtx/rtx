/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_media_tools.h"

//#include "libyuv.h"

#include "mmt_drawer.h"

namespace media
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	enum class VideoStandard
	{
		SQCIF,		// 128 x 96
		QCIF,		// 176 x 144
		CIF,		// 352 x 288
		CIF4,		// 704 X 576
		CIF16,		// 1408 X 1152

		QVGA,		// 320 x 240
		HVGA,		// 480 X 320
		VGA,		// 640 X 480
		SVGA,		// 800 X 600

		HDTV480p,	// 852 X 480
		HDTV720p,	// 1280 X 720
		HDTV1080p,	// 1920 x 1080

		Custom = 100,
	};
	//-----------------------------------------------
	//
	//-----------------------------------------------
	enum class VideoChroma
	{
		None = 0,
		RGB24,		// will be stored as bgr24 on x86 (little endians) machines; e.g. WindowsPhone7
		BGR24,		// used by windows consumer (DirectShow) -
		RGB32,       // used by iOS4 consumer (iPhone and iPod touch)
		RGB565le,	// (used by both android and wince consumers)
		RGB565be,
		NV12,		// used by iOS4 producer (iPhone and iPod Touch 3GS and 4)
		NV21,		// Yuv420 SP (used by android producer)
		YUV422p,
		UYVY422,		// used by iOS4 producer (iPhone and iPod Touch 3G) - Microsoft: MFVideoFormat_YUY2
		YUV420p,		// Default
		MJPEG,		// VirtualBox default camera mode (Windows as host and Linux as guest)
		YUYV422,		// YUYV422 (V4L2 preferred format)
	};

	//-----------------------------------------------
	//
	//-----------------------------------------------
	struct VideoSize
	{
		int width;
		int height;
	};
	struct VideoPixel
	{
		uint8_t cc[4];
	};
	//-----------------------------------------------
	//
	//-----------------------------------------------
	RTX_MMT_API VideoSize videoStandardDimention(VideoStandard pref_vs);
	RTX_MMT_API VideoSize parseVideoFMTP(const char* fmtp, VideoStandard pref_vs, int* fps);
	RTX_MMT_API VideoSize parseVideoImageAttr(const char* imageattr, VideoStandard pref_vs);
	RTX_MMT_API rtl::String getVideoFMTP(VideoStandard pref_vs);
	RTX_MMT_API int getVideoBandwidthKbps(int width, int height, int fps, int motion_rank);
	RTX_MMT_API int getVideoBandwidthKbps2(int width, int height, int fps);
	RTX_MMT_API int getDefaultsVideoMotionRank();

	RTX_MMT_API int calcImageSize(int width, int height);
	RTX_MMT_API int calcImageSize(const VideoSize& inageSize);
	RTX_MMT_API int getNearestCIF(const VideoSize& img_size, VideoSize& cif);
	//-----------------------------------------------
	//
	//-----------------------------------------------
#define VIDEO_CLAMP(nMin, nVal, nMax)		((nVal) > (nMax)) ? (nMax) : (((nVal) < (nMin)) ? (nMin) : (nVal))
//-----------------------------------------------
//
//-----------------------------------------------
	class VideoImage
	{
		int m_width;
		int m_height;

		bool m_ownData;
		int m_imageSize;
		uint8_t* m_YData;
		uint8_t* m_UData;
		uint8_t* m_VData;

	public:
		VideoImage() : m_width(0), m_height(0),
			m_ownData(true), m_imageSize(0),
			m_YData(nullptr), m_UData(nullptr), m_VData(nullptr) { }
		VideoImage(int width, int height) : VideoImage() { makeImage(width, height); }
		~VideoImage() { destroyImage(); }

		RTX_MMT_API void makeImage(int width, int height);
		RTX_MMT_API void makeImage(int width, int height, const uint8_t* image, int length);

		RTX_MMT_API void copyImage(const VideoImage& image);
		RTX_MMT_API void storeImage(int width, int height, const uint8_t* image_data, int size);
		RTX_MMT_API void destroyImage();
		RTX_MMT_API void importARGB(const Bitmap& argb);
		RTX_MMT_API void exportARGB(Bitmap& argb) const;
		RTX_MMT_API bool loadImageFile(const char* filename);
		RTX_MMT_API void saveImageFile(const char* filename, bool raw = false);


		RTX_MMT_API void transformTo(VideoImage& image, int width, int height, Transform mode, int params = 0) const;

		inline int getWidth() const { return m_width; }
		inline int getHeight() const { return m_height; }

		inline bool isWritable() const { return m_ownData; }
		inline bool isEmpty() const { return m_imageSize == 0; }

		inline uint8_t* getImageBuffer() { return (uint8_t*)m_YData; }

		inline const uint8_t* getYData() const { return (const uint8_t*)m_YData; }
		inline const uint8_t* getUData() const { return (const uint8_t*)m_UData; }
		inline const uint8_t* getVData() const { return (const uint8_t*)m_VData; }

		inline int getImageSize() const { return m_imageSize; }

		int getYSize() const;
		int getUSize() const;
		int getVSize() const;

		VideoPixel getPixel(int x, int y) const;
		void setPixel(int x, int y, VideoPixel pixel);
		void fill(VideoPixel color);
		void clearImage() { fill({ 0,0,0,0 }); }

	private:
		void transform_copy(VideoImage& image, int width, int height) const;
		void transform_stretch(VideoImage& image, int width, int height, int params) const;
		void transform_scale_v(VideoImage& image, int width, int height, int params) const;
		void transform_scale_h(VideoImage& image, int width, int height, int params) const;
	};

	//--------------------------------------
	// ����� (�������) ������ ��� �����
	// ������ ������� ��� ������ � ������ ������ ���� �������� 10 �� ��� 80 ������� (80/160 ����)
	// ������� ����� ����� ������ 80 ����. ��� ��� ����� ������������� �� 2 �����
	//--------------------------------------
	class VideoImageQueue
	{
		struct QueueImage
		{
			volatile bool ready;		// TRUE -- ����� ��� ������, FALSE -- ����� ��� ������
			VideoImage* image;			// pointer to image
		};

		QueueImage* m_queue;
		int m_queueSize;
		int m_writeIdx;
		int m_readIdx;
		volatile int m_count;

		QueueImage* getWritingImage();
		void imageWritten();
		QueueImage* getReadingImage();
		void imageRead();

	public:
		VideoImageQueue() : m_queue(nullptr), m_queueSize(0), m_writeIdx(0), m_readIdx(0), m_count(0) { }
		~VideoImageQueue() { reset(); }

		RTX_MMT_API void create(int queueDepth);
		RTX_MMT_API void reset();

		RTX_MMT_API void push(VideoImage* image);
		RTX_MMT_API VideoImage* pop();

		int hasImage() { return m_count > 0; }
	};
}

//-----------------------------------------------
