/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mmt_audio_reader.h"
#include "mmt_file_mp3.h"

namespace media
{
	//--------------------------------------
	//
	//--------------------------------------
	class AudioReaderMP3 : public AudioReader
	{
		FileReaderMP3 m_file;

		int m_stereo_cvt_pos;
		int m_stereo_cvt_length;
		short* m_stereo_cvt_left;
		short* m_stereo_cvt_right;

	public:
		AudioReaderMP3(rtl::Logger* log);
		virtual ~AudioReaderMP3() override;

	private:
		virtual bool openSource(const char* path) override;
		virtual void closeSource() override;
		virtual int readRawPCM(short* buffer, int size) override;

		bool fillCache();
	};
}
