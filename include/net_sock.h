﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
 //--------------------------------------
//
//--------------------------------------
enum net_socket_state_t
{
	net_shutdown_recv = SD_RECEIVE,
	net_shutdown_send = SD_SEND,
	net_shutdown_both = SD_BOTH,
	net_shutdown_closed,
};
//--------------------------------------
//
//--------------------------------------
class net_socket_t
{
	SOCKET m_handle;
	volatile void* m_tag;
	volatile net_socket_state_t m_shutdown;
	const char* m_profile;

	net_socket_t(const net_socket_t& socket);
	net_socket_t& operator = (const net_socket_t& socket);

public:
	static const int PAYLOAD_BUFFER_SIZE = 4096;

	net_socket_t() : m_handle(INVALID_SOCKET), m_tag(nullptr), m_shutdown(net_shutdown_closed), m_profile(nullptr) { }
	net_socket_t(SOCKET handle) : m_handle(handle), m_tag(nullptr), m_shutdown(net_shutdown_closed), m_profile(nullptr) { }
	~net_socket_t() { close(); }

	net_socket_t& operator = (SOCKET handle) { close(); m_handle = handle; return *this; }
	operator SOCKET() { return m_handle; }
	SOCKET get_handle() const { return m_handle; }
	net_socket_state_t get_state() { return m_shutdown; }
	void set_profile(const char* profile) { m_profile = profile; }

	RTX_NET_API bool open(int af, int type, int protocol);
	bool bind(sockaddr* iface, int length) { return ::bind(m_handle, iface, length) == 0; }
	RTX_NET_API bool close();
	bool shutdown(int direction) { m_shutdown = (net_socket_state_t)direction; return ::shutdown(m_handle, direction) == 0; }
	bool connect(sockaddr* addr, int length) { return ::connect(m_handle, addr, length) == 0; }
	bool listen(int backlog) { return ::listen(m_handle, backlog) == 0; }
	SOCKET accept(sockaddr* addr, sockaddrlen_t* length) { return ::accept(m_handle, addr, length); }
	int receive(void* buffer, int size, int flags = 0) { return ::recv(m_handle, (char*)buffer, size, flags); }
	RTX_NET_API int receive_select(void* buffer, int size, uint32_t timeout = 0);
	int receive_from(void* buffer, int size, int flags, sockaddr* addr, sockaddrlen_t* length) { return ::recvfrom(m_handle, (char*)buffer, size, flags, addr, length); }
	int send(const void* packet, int size, int flags) { return ::send(m_handle, (const char*)packet, size, flags); }
	int send(const void* packet, int size) { return ::send(m_handle, (const char*)packet, size, 0); }
	RTX_NET_API int send_to(const void* packet, int size, int flags, const sockaddr* addr, int length);
	RTX_NET_API int send_to(const void* packet, int size, in_addr addr, uint16_t port);
	bool set_option(int level, int name, const void *value, int size) { return ::setsockopt(m_handle, level, name, (const char*)value, size) == 0; }
	bool get_option(int level, int name, void *value, sockaddrlen_t* size) { return ::getsockopt(m_handle, level, name, (char*)value, size) == 0; }
	bool get_interface(sockaddr *name, sockaddrlen_t *length) { return ::getsockname(m_handle, name, length) == 0; }
	bool io_control(long cmd, uint32_t *argp) { return ::ioctlsocket(m_handle, cmd, (u_long*)argp) == 0; }
	bool get_available(unsigned long* available) { return ::ioctlsocket(m_handle, FIONREAD, available) == 0; }

	void set_tag(void* tag) { m_tag = tag; }
	void* get_tag() { return (void*)m_tag; }

	RTX_NET_API static int select(rtl::ArrayT<net_socket_t*>& readfds, uint32_t timeout);
	RTX_NET_API static int select(rtl::ArrayT<net_socket_t*>* readfds, rtl::ArrayT<net_socket_t*>* writefds, rtl::ArrayT<net_socket_t*>* exceptfds, uint32_t timeout);

    class net_io_event_handler_t*	m_iosvctag_user;
    struct netio_thread_context*	m_iosvctag_netioctx;

#if defined(TARGET_OS_LINUX)

private:
	static uint32_t add_events(epoll_event* events, uint32_t count, rtl::ArrayT<net_socket_t*>* sockets, int flag);

#elif defined(TARGET_OS_FREEBSD)

	static uint32_t add_events(struct kevent* events, uint32_t count, rtl::ArrayT<net_socket_t*>* sockets, short filter);
#endif
};
//--------------------------------------
