﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

//#include "std_event.h"
//#include "std_mutex.h"
//#include "std_semaphore.h"

inline int32_t std_interlocked_inc(volatile int32_t* value)
{
#if defined (TARGET_OS_WINDOWS)
	return InterlockedIncrement((volatile unsigned*)value);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	return __sync_add_and_fetch(value, 1);
#endif
}
inline uint32_t std_interlocked_inc(volatile uint32_t* value)
{
#if defined (TARGET_OS_WINDOWS)
	return InterlockedIncrement(value);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	return __sync_add_and_fetch(value, 1);
#endif
}
inline int64_t std_interlocked_inc(volatile int64_t* value)
{
#if defined (TARGET_OS_WINDOWS)
	return InterlockedIncrement64(value);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	return __sync_add_and_fetch(value, 1);
#endif
}
inline uint64_t std_interlocked_inc(volatile uint64_t* value)
{
#if defined (TARGET_OS_WINDOWS)
	return InterlockedIncrement64((volatile int64_t*)value);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	return __sync_add_and_fetch(value, 1);
#endif
}
//--------------------------------------
//
//--------------------------------------
inline int32_t std_interlocked_dec(volatile int32_t* value)
{
#if defined (TARGET_OS_WINDOWS)
	return InterlockedDecrement((volatile unsigned*)value);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	return __sync_sub_and_fetch(value, 1);
#endif
}
inline uint32_t std_interlocked_dec(volatile uint32_t* value)
{
#if defined (TARGET_OS_WINDOWS)
	return InterlockedDecrement(value);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	return __sync_sub_and_fetch(value, 1);
#endif
}
inline int64_t std_interlocked_dec(volatile int64_t* value)
{
#if defined (TARGET_OS_WINDOWS)
	return InterlockedDecrement64(value);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	return __sync_sub_and_fetch(value, 1);
#endif
}
inline uint64_t std_interlocked_dec(volatile uint64_t* value)
{
#if defined (TARGET_OS_WINDOWS)
	return (uint64_t)InterlockedDecrement64((volatile int64_t*)value);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	return __sync_sub_and_fetch(value, 1);
#endif
}

inline int std_interlocked_add(volatile int* value, int summand)
{
#if defined (TARGET_OS_WINDOWS)
	return (int)InterlockedExchangeAdd((volatile long*)value, summand);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	return __sync_add_and_fetch(value, summand);
#endif
}
inline uint32_t std_interlocked_add(volatile uint32_t* value, uint32_t summand)
{
#if defined (TARGET_OS_WINDOWS)
	return InterlockedExchangeAdd(value, summand);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	return __sync_add_and_fetch(value, summand);
#endif
}
inline int64_t std_interlocked_add(volatile int64_t* value, int64_t summand)
{
#if defined (TARGET_OS_WINDOWS)
	return InterlockedExchangeAdd64(value, summand);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	return __sync_add_and_fetch(value, summand);
#endif
}
inline uint64_t std_interlocked_add(volatile uint64_t* value, uint64_t summand)
{
#if defined (TARGET_OS_WINDOWS)
	return InterlockedExchangeAdd64((volatile int64_t*)value, summand);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	return __sync_add_and_fetch(value, summand);
#endif
}
//--------------------------------------
//
//--------------------------------------
inline int std_interlocked_sub(volatile int* value, int subtrahend)
{
#if defined (TARGET_OS_WINDOWS)
	return InterlockedExchangeSubtract((volatile unsigned*)value, subtrahend);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	return __sync_sub_and_fetch(value, subtrahend);
#endif
}
inline uint32_t std_interlocked_sub(volatile uint32_t* value, uint32_t subtrahend)
{
#if defined (TARGET_OS_WINDOWS)
	return InterlockedExchangeSubtract(value, subtrahend);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	return __sync_sub_and_fetch(value, subtrahend);
#endif
}
inline int64_t std_interlocked_sub(volatile int64_t* value, int64_t subtrahend)
{
#if defined (TARGET_OS_WINDOWS)
	return (int64_t)InterlockedExchangeSubtract((volatile uint64_t*)value, subtrahend);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	return __sync_sub_and_fetch(value, subtrahend);
#endif
}
inline uint64_t std_interlocked_sub(volatile uint64_t* value, uint64_t subtrahend)
{
#if defined (TARGET_OS_WINDOWS)
	return InterlockedExchangeSubtract(value, subtrahend);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	return __sync_sub_and_fetch(value, subtrahend);
#endif
}

template<class T> class auto_counter_t
{
	auto_counter_t(const auto_counter_t& c) { }
	auto_counter_t& operator = (const auto_counter_t& c) { }

	T _t;
public:
	auto_counter_t(T t) : _t(t) { std_interlocked_inc(&_t); }
	~auto_counter_t() { std_interlocked_dec(&_t); }
};
//--------------------------------------
