/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mmt_lazy_writer_rtp.h"
#include "mmt_buffered_writer.h"

namespace media
{
	//--------------------------------------
   // запись в RTP файл входящего звукогого потока
   //--------------------------------------
	class BufferedMediaWriterRTP : public BufferedMediaWriter
	{
		rtl::FileStream m_file;					// дескриптор файла записи
		rtl::FileStream m_dout;
		RTPFileHeader m_file_header;		// информация о ходе записи в файл
		/// для расчета времени
		bool m_timestamp_flag;
		uint32_t m_timestamp_begin;
		uint32_t m_timestamp_end;
		uint32_t m_timestamp_last_pack;
		// действует только если рекордер стартовал!
		volatile bool m_format_changed;			// признак изменения формата!
		media::PayloadSet m_additional_formats;		// измененный набор форматов!

	public:
		RTX_MMT_API BufferedMediaWriterRTP(rtl::Logger* log);
		RTX_MMT_API virtual ~BufferedMediaWriterRTP();

		RTX_MMT_API bool create(const char* path, int bufsize_ms);
		RTX_MMT_API bool create(const char* path, int bufsize_ms, const media::PayloadSet& formats);
		RTX_MMT_API void close();

		RTX_MMT_API bool write_packet(const rtp_packet* packet);
		RTX_MMT_API void update_format(const media::PayloadSet& formats);

	private:
		// to cache
		void write_format(const media::PayloadSet& formats);
		// to file
		void rec_write_format(const media::PayloadSet& formats);
		void static payload_to_rtpfmt(const media::PayloadFormat& pl_fmt, RTPFileMediaFormat* rtp_fmt);
		virtual void recFileStarted();
		virtual void recFileStopped();
		virtual bool recFileWrite(void* data_block, uint32_t length, uint32_t* written);
	};
}
//--------------------------------------
