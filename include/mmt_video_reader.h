/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_codec.h"

namespace media
{
	//--------------------------------------
	// ����� yuv420p ?
	//--------------------------------------
	class VideoReader
	{
	protected:
		rtl::String m_filePath;
		bool m_eof;
		bool m_readingStarted;
		uint64_t m_GTTStartTime;
		rtl::Logger* m_log;

		rtl::String m_sourceEncoding;	// "yuv420p"

		VideoSize m_dimention;
		Transform m_mode;
		VideoImage m_outImage;

		int m_statFrames;
		int m_statPackets;

	public:
		RTX_MMT_API virtual ~VideoReader();

		const rtl::String& getFilePath() { return m_filePath; }
		uint64_t getStartTime() { return m_GTTStartTime; }
		bool eof() { return m_eof; }

		int statFrameRead() { return m_statFrames; }
		int statPacketsRead() { return m_statPackets; }

		/// �������� �����,
		RTX_MMT_API bool open(const char* filePath);
		/// �������� ��������
		RTX_MMT_API void close();
		/// ���������� ����� �������� ����� � �� ������� ������
		RTX_MMT_API bool setupReader(VideoSize dimention, Transform transform); // ?
		/// ������ ���������������� ������ -- ���� setup_reader() �� ��������� �� �� ������ ����� ������������ ������ �����
		RTX_MMT_API bool readNext(VideoImage* image);
		/// �������� ��������
		RTX_MMT_API static VideoReader* create(const char* path, rtl::Logger* log);
		RTX_MMT_API static VideoReader* create(const char* path, VideoSize dimention, Transform transform, rtl::Logger* log);

	protected:
		VideoReader(rtl::Logger* log);

		/// interface for derived objects
		virtual bool openSource(const char* path) = 0;
		virtual void closeSource() = 0;
		virtual bool readImage(VideoImage* image) = 0;

	private:
		void resetReader();
	};
}
