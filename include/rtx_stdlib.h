﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "std_basedef.h"
#include "std_typedef.h"
#include "std_funcdef.h"
//--------------------------------------
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the RTX_STDLIB_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// RTX_STD_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
//--------------------------------------
#if defined (TARGET_OS_WINDOWS)
	#ifdef RTX_STDLIB_EXPORTS
		#define RTX_STD_API __declspec(dllexport)
	#else
		#define RTX_STD_API __declspec(dllimport)
	#endif
#elif defined (TARGET_OS_LINUX) or defined (TARGET_OS_FREEBSD)
	#define RTX_STD_API
#endif
//--------------------------------------
//
//--------------------------------------
#define API_VERSION_MAJOR	3
#define API_VERSION_MINOR	1
#define API_VERSION_TEST	3
#define API_VERSION_BUILD	9
//--------------------------------------
//
//--------------------------------------
#define STR_BOOL(b) (b ? "true" : "false")
#define STR_COPY(d, s) (strncpy(d, s, sizeof(d)-1), d[sizeof(d)-1] = 0)
#define STR_COPY_LEN(d, s, l) {int to_copy = l < sizeof(d) ? l : sizeof(d) - 1; strncpy(d, s, to_copy); d[to_copy] = 0;}
#define PTR_DIFF(p1, p2) ((int)(p1-p2))
#define STRING_IS_EMPTY(s) (s == nullptr || s[0] == 0)
#define STRING_IS_NOT_EMPTY(s) (s == nullptr || s[0] == 0)
//--------------------------------------
//
//--------------------------------------
#include "std_synchro.h"
#include "std_array_templates.h"
#include "std_exception.h"
#include "std_mutex.h"
#include "std_semaphore.h"
#include "std_event.h"
#include "std_res_counter.h"
#include "std_thread.h"
#include "std_logger.h"
#include "std_async_call.h"
#include "std_string_utils.h"
#include "std_string.h"
#include "std_crypto.h"
#include "std_timer.h"
#include "std_config.h"
#include "std_memory_stream.h"
#include "std_filesystem.h"
//--------------------------------------
//
//--------------------------------------
RTX_STD_API uint32_t stdlib_get_version();
//--------------------------------------
