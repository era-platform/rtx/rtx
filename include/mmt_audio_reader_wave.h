﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mmt_audio_reader.h"
#include "mmt_file_wave.h"
#include "rtx_audio_codec.h"

namespace media
{
	//--------------------------------------
	//
	//--------------------------------------
	class AudioReaderWave : public AudioReader
	{
		FileReaderWAV m_file;

		// декодер
		mg_audio_decoder_t* m_decoder;
		uint8_t* m_dec_buffer;
		int m_dec_buffer_size;	// общий размер буфера чтения декодера
		int m_dec_buffer_pos;	// текущая позиция в буфере декодирования
		int m_dec_buffer_read;	// количество считанных байт в буфер декодирования
		int m_dec_block_size;	// размер едениного блока декодирования

		int m_bytes_read;

	public:
		AudioReaderWave(rtl::Logger* log);
		virtual ~AudioReaderWave() override;

	private:
		virtual bool openSource(const char* filePath) override;
		virtual void closeSource() override;
		virtual int readRawPCM(short* buffer, int size) override;

		//int readDecoded(short* buffer, int size);
		int readRaw(void* buffer, int size);

		//void setupPCM(const WAVFormat* fmt);
		void setupGSM610(const WAVFormat* fmt);
		void setupG711(const WAVFormat* fmt);
	};
}
//--------------------------------------
