﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mmt_lazy_writer_rtp.h"
#include "mmt_file_rtp.h"
#include "mmt_audio_reader.h"

namespace media
{
	//--------------------------------------
	//
	//--------------------------------------
	class AudioReaderLinearRTP : public AudioReader
	{
		rtl::Mutex m_sync;
		FileReaderRTP m_file;

		/// сборщик RTP пакетов
		struct RTPSorter
		{
			uint16_t seqBegin;
			uint16_t seqEnd;

			int packet_count;
			static const int PAKET_PER_SECOND = 50;
			RTPFilePacketHeader packets[PAKET_PER_SECOND];
		};

		bool m_newStream;
		RTPSorter *m_work, *m_wait;
		RTPFilePacketHeader* m_third_second;

		/// готовый PCM
		static const int COMFORT_NOISE_20 = 160;	// (samples) 20 millisecond for 8Kz mono 16-bit PCM
		static const int INITIAL_PCM_SIZE = 16000;	// (bytes)   1 second for 8Kz mono 16-bit PCM
		short* m_pcmCache;
		int m_pcmUsed;
		int m_pcmSize;
		int m_pcmLength;

		struct DecoderInfo
		{
			RTPFileMediaFormat format;
			mg_audio_decoder_t* decoder;
			Resampler* resampler;
		};

		rtl::ArrayT<DecoderInfo> m_decoderList;

		static const int MAX_PCM_BUFFER = 1920; // 48 kz mono 16-bit 40 ms
		int m_decSize;
		short* m_decBuffer;

		//rtl::FileStream m_out;

	public:
		RTX_MMT_API AudioReaderLinearRTP(rtl::Logger* log);
		RTX_MMT_API virtual ~AudioReaderLinearRTP() override;

		uint64_t get_start_time() { return m_file.get_header().rec_start_time; }
		uint32_t get_start_timestamp() { return m_file.get_header().rec_start_timestamp; }
		uint32_t get_stop_timestam() { return m_file.get_header().rec_stop_timestamp; }

	private:
		/// reader interface
		virtual bool openSource(const char* path) override;
		virtual void closeSource() override;
		virtual int readRawPCM(short* buffer, int size) override;

		/// pcm fill functions
		int read_pcm(short* buffer, int size);
		int fill_pcm_buffer();
		bool fill_rtp_buffers();
		void update_rtp_buffers();

		/// rtp_packet sorter functions

		/// private routines
		void initialize();
		void cleanup();
		void cleanupDecoders();
		///---
		void processPacket(const RTPFilePacketHeader& packet);
		void decodeRTP(rtp_header_t* packet, int length);
		void decodeEmpty();
		///---
		void updateDecoders(const RTPFileMediaFormat* formats, int count);
		DecoderInfo* findDecoder(int payloadId);
		void addDecoder(const RTPFileMediaFormat* format);
		void checkCacheSize(int needSize);
		void appendInitialSilence();
	};
}
//--------------------------------------

