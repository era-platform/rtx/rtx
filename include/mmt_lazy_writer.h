﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_codec.h"
#include "mmt_payload_set.h"
#include "mmt_media_writer.h"

namespace media
{
	//--------------------------------------
   // константы
   //--------------------------------------
#define CACHE_BLOCK_COUNT		8		// количество блоков в кеше
#define CACHE_BLOCK_SIZE		16384	// размер блока

#define CACHE_BLOCK_FREE		0		// признак свободного блока для кеширования пакетов
#define CACHE_BLOCK_READY		1		// признак готового блока для записи на диск
#define CACHE_BLOCK_RECORD		2		// признак текущего записываемого блока
//--------------------------------------
// структура блока в кеше
//--------------------------------------
	struct LazyBlock
	{
		volatile long rec_state;				// состояние накопительного буфера : 0 - свободен, 1 - готов
		int rec_length;							// длина записанных данных в байтах
		uint32_t rec_start_timestamp;			// время начала записи
		uint8_t* rec_pos;						// указатель записи в буфере
		uint8_t rec_buffer[CACHE_BLOCK_SIZE];	// 32 килобайтный буфер записи
	};
	//--------------------------------------
	// запись в файл входящего звукогого потока
	//--------------------------------------
	class LazyMediaWriter : public MediaWriter
	{
	protected:
		volatile bool m_created;			// признак создания файла
		volatile bool m_started;			// признак начала записи
		volatile bool m_write;				// признак записи в кеш (вход в функцию записи в кеш)
		volatile bool m_flash;				// признак записи в файл (вход в функцию записи в файл)

		/// кеш записи
		LazyBlock* m_record_cache;	// список кеш-блоков
		int m_current_block;				// индекс текущего блока для записи в кеш
		int m_record_block;					// индекс блока для записи на диск из кеша

		volatile size_t m_written_to_buffer;
		volatile size_t m_lost_in_buffer;
		volatile size_t m_written_to_file;
		volatile size_t m_lost_in_file;

	public:
		RTX_MMT_API bool start();
		RTX_MMT_API void stop();

	protected:
		LazyMediaWriter(rtl::Logger* log);
		virtual ~LazyMediaWriter();

		bool isStarted() { return m_started; }

		void initialize();
		void destroy();

		virtual void rec_file_started() = 0;
		virtual void rec_file_stopped() = 0;
		virtual bool rec_file_write(void* data_block, uint32_t length, uint32_t* written) = 0;

		virtual void flush(bool close = false) override;

		LazyBlock* get_current_block(int packetSize);
		void print_cache_statistics();
	};
}
//--------------------------------------
