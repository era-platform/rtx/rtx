﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_media_tools.h"

namespace media
{
	//--------------------------------------
	//
	//--------------------------------------
	/*
	PT   encoding    media type  clock rate   channels
	name                    (Hz)
	___________________________________________________
	0    PCMU        A            8,000       1
	1    reserved    A
	2    reserved    A
	3    GSM         A            8,000       1
	4    G723        A            8,000       1
	5    DVI4        A            8,000       1
	6    DVI4        A           16,000       1
	7    LPC         A            8,000       1
	8    PCMA        A            8,000       1
	9    G722        A            8,000       1
	10   L16         A           44,100       2
	11   L16         A           44,100       1
	12   QCELP       A            8,000       1
	13   CN          A            8,000       1
	14   MPA         A           90,000       (see text)
	15   G728        A            8,000       1
	16   DVI4        A           11,025       1
	17   DVI4        A           22,050       1
	18   G729        A            8,000       1
	19   reserved    A
	20   unassigned  A
	21   unassigned  A
	22   unassigned  A
	23   unassigned  A
	dyn  G726-40     A            8,000       1
	dyn  G726-32     A            8,000       1
	dyn  G726-24     A            8,000       1
	dyn  G726-16     A            8,000       1
	dyn  G729D       A            8,000       1
	dyn  G729E       A            8,000       1
	dyn  GSM-EFR     A            8,000       1
	dyn  L8          A            var.        var.
	dyn  RED         A                        (see text)
	dyn  VDVI        A            var.        1

	Table 4: Payload types (PT) for audio encodings

	PT      encoding    media type  clock rate
	name                    (Hz)
	_____________________________________________
	24      unassigned  V
	25      CelB        V           90,000
	26      JPEG        V           90,000
	27      unassigned  V
	28      nv          V           90,000
	29      unassigned  V
	30      unassigned  V
	31      H261        V           90,000
	32      MPV         V           90,000
	33      MP2T        AV          90,000
	34      H263        V           90,000
	35-71   unassigned  ?
	72-76   reserved    N/A         N/A
	77-95   unassigned  ?
	96-127  dynamic     ?
	dyn     H263-1998   V           90,000

	Table 5: Payload types (PT) for video and combined encodings
	*/
	//--------------------------------------
	//
	//--------------------------------------
	enum class PayloadId
	{
		PCMU = 0,
		GSM610 = 3,
		G723 = 4,
		PCMA = 8,
		G722 = 9,
		L16S = 10,
		L16M = 11,
		CN = 13,
		G728 = 15,
		G729 = 18,

		H261 = 31,
		H263 = 34,

		Dynamic = 96,
		Max = 127,
		Error = 255,
	};

	//--------------------------------------
	//
	//--------------------------------------
	class PayloadFormat
	{
		bool m_dtmf;
		rtl::String m_encoding;
		PayloadId m_id;
		int m_freq;
		int m_channels;
		rtl::String m_params;
		rtl::String m_fmtp;

	public:
		PayloadFormat() : m_dtmf(false), m_id(PayloadId::Dynamic), m_freq(8000), m_channels(1) { }
		PayloadFormat(const PayloadFormat& format) { assign(format); }
		PayloadFormat(const char* enc, PayloadId pl, int freq, int chan, const char* fmtp = nullptr) { assign(enc, pl, freq, chan, fmtp); }
		PayloadFormat(const char* enc, PayloadId pl, int freq, const char* params, const char* fmtp = nullptr) { assign(enc, pl, freq, params, fmtp); }
		~PayloadFormat() { clear(); }

		PayloadFormat& operator = (const PayloadFormat& format) { return assign(format); }

		RTX_MMT_API PayloadFormat& assign(const PayloadFormat& format);
		RTX_MMT_API void assign(const char* enc, PayloadId pl, int freq, int channels, const char* fmtp = nullptr);
		RTX_MMT_API void assign(const char* enc, PayloadId pl, int freq, const char* params, const char* fmtp = nullptr);

		PayloadId getId() const { return m_id; }
		uint8_t getId_u8() const { return (uint8_t)m_id; }
		const rtl::String& getEncoding() const { return m_encoding; }
		int getFrequency() const { return m_freq; }
		int getChannelCount() const { return m_channels; }
		const rtl::String& getParams() const { return m_params; }
		const rtl::String& getFmtp() const { return m_fmtp; }
		bool isDtmf() const { return m_dtmf; }
		bool isEmpty() const { return m_encoding.isEmpty(); }

		void setId(PayloadId payloadId) { m_id = payloadId; }
		void setEncoding(const char* encoding, int length = -1) { m_encoding.assign(encoding, length); }
		void setFrequency(int clockRate) { m_freq = clockRate; }
		void setChannelCount(int channels) { m_channels = channels; }
		void setParams(const char* params, int length = -1) { m_params.assign(params, length); }
		void setFmtp(const char* fmtp, int length = -1) { m_fmtp.assign(fmtp, length); }
		void setDtmfFlag(bool flag) { m_dtmf = flag; }

		bool operator == (const PayloadFormat& format) const { return isEqual(format); }
		bool operator != (const PayloadFormat& format) const { return !isEqual(format); }
		RTX_MMT_API bool isEqual(const PayloadFormat& format) const;

		RTX_MMT_API void clear();

		RTX_MMT_API static PayloadFormat Empty;
	};
}
//--------------------------------------
