﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#pragma once


#include "std_basedef.h"

#if defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	#if !defined(__STDC_LIMIT_MACROS)
		#define __STDC_LIMIT_MACROS // включает в <stdint.h> такие дефайны как INT16_MAX, INT16_MIN и т.п.
	#endif
#endif

#include <stdint.h>

//--------------------------------------
//
//--------------------------------------
#if defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)

	#if !defined(NULL)
		#define NULL (0)
	#endif

	#ifndef nullptr
#define nullptr (0)
	#endif

	#if !defined(BOOL)
		typedef int BOOL;
	#endif

	#if !defined(TRUE)
		#define TRUE (1)
	#endif

	#if !defined(FALSE)
		#define FALSE (0)
	#endif

#endif
//--------------------------------------
