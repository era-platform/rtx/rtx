﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

namespace rtl
{
	//--------------------------------------
	// константа неверного индекса
	//--------------------------------------
	#define BAD_INDEX	(-1) //0xFFFFFFFF
	#define GROW_SIZE	0x00000020
	#define GROW_MASK	0x0000001F
	//--------------------------------------
	// массив для встроенных типов и простых структур
	//--------------------------------------
	template<class T> class ArrayT
	{
	protected:
		T m_empty;				// ссылка на пустой элемент
		T* volatile m_items;	// массив элементов
		volatile int m_size;	// размер массива
		volatile int m_count;	// кол-во элементов в массиве

		void resize(int size)
		{
			if (size <= 0)
			{
				clear();
				return;
			}

			if (m_size < size)
			{
				if ((size & GROW_MASK) != 0)
				{
					size &= ~GROW_MASK;
					size += GROW_SIZE;
				}

				T* arr = (T*)MALLOC(sizeof(T) *size);

				memset(arr + m_count, 0, (size - m_count) * sizeof(T));

				if (m_items != nullptr)
				{
					if (m_count > 0)
						memcpy(arr, m_items, m_count * sizeof(T));
					FREE(m_items);
				}
				m_items = arr;
				m_size = size;
			}
		}

	public:
		// конструктор
		ArrayT(int capacity = 0) : m_items(nullptr), m_size(0), m_count(0)
		{
			if (capacity > 0)
				resize(capacity);

			memset (&m_empty, 0, sizeof(m_empty));
		}
		ArrayT(const ArrayT& array) : m_items(nullptr), m_size(0), m_count(0)
		{
			assign(array);
		}
		// деструктор
		virtual ~ArrayT()
		{
			clear();
		}
		//
		ArrayT& operator = (const ArrayT& array)
		{
			assign(array);
			return *this;
		}
		//
		void assign(T* items, int count)
		{
			clear();
			resize(m_count = count);
			memcpy(m_items, items, m_count * sizeof(T));
		}
		//
		void assign(const ArrayT<T>& items)
		{
			clear();
			resize(m_count = items.m_count);
			memcpy(m_items, items.getBase(), m_count * sizeof(T));
		}
		//
		void expand(int count)
		{
			resize(count);
			if (m_size > 0 && count > m_count)
			{
				if (count > m_count)
					memset(m_items + m_count, 0, (count - m_count) * sizeof(T));
				m_count = count;
			}
		}
		//
		const T& getEmpty() const { return m_empty; }
		T& getEmpty() { return m_empty; }
		//
		T* getBase() { return m_items; }
		const T* getBase() const { return m_items; }

		T& begin() { return m_items; }
		const T& begin() const { return m_items; }

		T& end() { return m_items + m_count; }
		const T& end() const { return m_items + m_count; }
		// получить количество элементов
		int getCount() const
		{
			return m_count;
		}
		// получить ссылку на i элемент
		const T& getAt(int index) const
		{
			if (index >= 0 && index < m_count)
				return m_items[index];

			return m_empty;
		}
		const T& getFirst() const
		{
			return m_count > 0 ? m_items[0] : m_empty;
		}
		const T& getLast() const
		{
			return m_count > 0 ? m_items[m_count - 1] : m_empty;
		}
		// получить ссылку на i элемент
		const T& operator [] (int index) const
		{
			if (index >= 0 && index < m_count)
				return m_items[index];

			return m_empty;
		}
		// получить ссылку на i элемент
		T& getAt(int index)
		{
			return index >= 0 && index < m_count ? m_items[index] : m_empty;
		}
		T& getFirst() 
		{
			return m_count > 0 ? m_items[0] : m_empty;
		}
		T& getLast()
		{
			return m_count > 0 ? m_items[m_count - 1] : m_empty;
		}
		// получить ссылку на i элемент
		T& operator [] (int index)
		{
			return index >= 0 && index < m_count ? m_items[index] : m_empty;
		}
		// заменить значение по индексу
		void setAt(int index, T& item)
		{
			if (index >= 0 && index < m_count)
				m_items[index] = item;
		}
		// добавить новый элемент в конец массива
		int add(const T& item)
		{
			if (m_count >= m_size)
			{
				resize(m_count+1);
			}
			m_items[m_count] = item;
			return m_count++;
		}
		// вставить элемент в указанную позицию
		int insert(int index, const T& item)
		{
			if (index >= m_count)
				return add(item);

			if (index < 0)
				index = 0;

			if (m_count >= m_size)
			{
				resize(m_count + 1);
			}
			// сдвинем часть массива на одну позицию
			memmove(m_items + index + 1, m_items + index, (m_count - index) * sizeof(T));
			m_count++;
			m_items[index] = item;

			return index;
		}
		// удалить элемент по индексу
		void removeAt(int index)
		{
			if (index < 0 || index >= m_count)
				return;

			if (index < m_count - 1)
				memmove(m_items + index, m_items + index + 1, (m_count - index - 1) * sizeof(T));
			memset(&m_items[--m_count], 0, sizeof(T));
		}
		// удалить элемент по индексу
		void removeLast()
		{
			if (m_count == 0)
				return;

			memset(&m_items[--m_count], 0, sizeof(T));
		}
		// удалить диапозон индексов
		void removeRange(int begin, int count)
		{
			if (count == 1)
			{
				removeAt(begin);
				return;
			}

			if (begin < 0 || begin >= m_count)
				return;

			if (begin + count >= m_count)
				count = m_count - begin;

			int to_remove_idx = begin + count;

			if (to_remove_idx < m_count - 1)
			{
				memmove(m_items + begin, m_items + begin + count, (m_count - to_remove_idx) * sizeof(T));
			}

			memset(m_items + (m_count -= count), 0, sizeof(T) * count);
		}
		// удалить элемент по адресу
		void remove(T& item)
		{
			for (int i = 0; i < m_count; i++)
				if (m_items[i] == item)
				{
					removeAt(i);
					break;
				}
		}
		// очистить массив
		void clear()
		{
			if (m_items != nullptr)
			{
				FREE(m_items);
				m_items = nullptr;
				m_size = m_count = 0;
			}
		}
		//
		int indexOf(T& item) const
		{
			for (int i = 0; i < m_count; i++)
				if (m_items[i] == item)
				{
					return i;
				}
			return BAD_INDEX;
		}
		//
		bool isEmpty() const { return m_count == 0; }
		//
		inline bool contains(T& item) const { return indexOf(item) != BAD_INDEX; }
	};
	//--------------------------------------
	// массив для встроенных типов и простых структур
	//--------------------------------------
	template<class T> class PonterArrayT
	{
	protected:
		T** volatile m_items;	// массив элементов
		volatile int m_size;	// размер массива
		volatile int m_count;	// кол-во элементов в массиве

		void resize(int size)
		{
			if (size <= 0)
			{
				clear();
				return;
			}

			if (m_size < size)
			{
				if ((size & GROW_MASK) != 0)
				{
					size &= ~GROW_MASK;
					size += GROW_SIZE;
				}

				T** arr = (T**)MALLOC(sizeof(T*) * size);

				memset(arr + m_count, 0, (size - m_count) * sizeof(T*));

				if (m_items != nullptr)
				{
					if (m_count > 0)
						memcpy(arr, m_items, m_count * sizeof(T*));
					FREE(m_items);
				}
				m_items = arr;
				m_size = size;
			}
		}

	public:
		// конструктор
		PonterArrayT(int capacity = 0) : m_items(nullptr), m_size(0), m_count(0)
		{
			if (capacity > 0)
				resize(capacity);
		}
		// деструктор
		virtual ~PonterArrayT()
		{
			clear();
		}
		//
		T** getBase() { return m_items; }
		const T** getBase() const { return m_items; }
		// получить количество элементов
		int getCount() const
		{
			return m_count;
		}
		// получить ссылку на i элемент
		const T* getAt(int index) const
		{
			return index >= 0 && index < m_count ? m_items[index] : nullptr;
		}
		// получить ссылку на i элемент
		const T* operator [] (int index) const
		{
			return index >= 0 && index < m_count ? m_items[index] : nullptr;
		}
		// получить ссылку на i элемент
		T* getAt(int index)
		{
			return index >= 0 && index < m_count ? m_items[index] : nullptr;
		}
		// получить ссылку на i элемент
		T* operator [] (int index)
		{
			return index >= 0 && index < m_count ? m_items[index] : nullptr;
		}
		// заменить значение по индексу
		void setAt(int index, T* item)
		{
			if (index >= 0 && index < m_count)
			{
				DELETEO(m_items[index]);
				m_items[index] = item;
			}
		}
		// добавить новый элемент в конец массива
		int add(T* item)
		{
			if (m_count >= m_size)
			{
				resize(m_count + 1);
			}
			m_items[m_count] = item;
			return m_count++;
		}
		// вставить элемент в указанную позицию
		int insert(int index, T* item)
		{
			if (index >= m_count)
				return add(item);

			if (index < 0)
				index = 0;

			if (m_count >= m_size)
			{
				resize(m_count + 1);
			}
			// сдвинем часть массива на одну позицию
			memmove(m_items + index + 1, m_items + index, (m_count - index) * sizeof(T*));
			m_count++;
			m_items[index] = item;

			return index;
		}
		// переместить объект в массиве на новую позицию
		int move(int index_from, int index_to)
		{
			if (index_from < 0 || index_from >= m_count)
				return BAD_INDEX;

			if (index_to < 0 || index_to >= m_count)
				return BAD_INDEX;

			T* tmp = m_items[index_from];

			if (index_from < m_count - 1)
				memmove(m_items + index_from, m_items + index_from + 1, (m_count - index_from - 1) * sizeof(T*));
			m_items[--m_count] = nullptr;

			return insert(index_to, tmp);
		}
		// удалить элемент по индексу
		void removeAt(int index)
		{
			if (index < 0 || index >= m_count)
				return;

			DELETEO(m_items[index]);

			if (index < m_count - 1)
				memmove(m_items + index, m_items + index + 1, (m_count - index - 1) * sizeof(T*));

			m_items[--m_count] = nullptr;
		}
		// удалить элемент по адресу
		void remove(T* item)
		{
			for (int i = 0; i < m_count; i++)
			{
				if (m_items[i] == item)
				{
					removeAt(i);
					break;
				}
			}
		}
		// очистить массив
		void clear()
		{
			if (m_items != nullptr)
			{
				for (int i = 0; i < m_count; i++)
				{
					DELETEO(m_items[i]);
				}
				FREE(m_items);
				m_items = nullptr;
				m_size = m_count = 0;
			}
		}
		//
		int indexOf(T* item) const
		{
			for (int i = 0; i < m_count; i++)
			{
				if (m_items[i] == item)
				{
					return i;
				}
			}
			return BAD_INDEX;
		}
		//
		inline bool contains(T& item) const { return indexOf(item) != BAD_INDEX; }
	};
	//--------------------------------------
	// сортированный массив указателей
	//--------------------------------------
	template<class T, class K=T> class SortedArrayT
	{
		typedef int (*PFN_COMPARE)(const T& left, const T& right);
		typedef int (*PFN_COMPAREKEY)(const K& left, const T& right);

		ArrayT<T>		m_items;
		PFN_COMPARE		m_pfn_compare;
		PFN_COMPAREKEY	m_pfn_compare_key;

		T* search(T& item)
		{
			T* lo = m_items.getBase();
			T* hi = lo + (m_items.getCount() - 1);
			T* mid;
			int half;
			int num = getCount();
			int result;

			while (lo <= hi)
				if ((half = num / 2) != 0)
				{
					mid = lo + (num & 1 ? half : (half - 1));
					if (!(result = m_pfn_compare(item, *mid)))
						return mid;
					else if (result < 0)
					{
						hi = mid - 1;
						num = num & 1 ? half : half-1;
					}
					else
					{
						lo = mid + 1;
						num = half;
					}
				}
				else if (num)
					return m_pfn_compare(item, *lo) ? nullptr : lo;
				else
					break;

			return nullptr;
		}
		const T* search(const T& item) const
		{
			const T* lo = m_items.getBase();
			const T* hi = lo + (m_items.getCount() - 1);
			const T* mid;
			int half;
			int num = getCount();
			int result;

			while (lo <= hi)
				if ((half = num / 2) != 0)
				{
					mid = lo + (num & 1 ? half : (half - 1));
					if (!(result = m_pfn_compare(item, *mid)))
						return mid;
					else if (result < 0)
					{
						hi = mid - 1;
						num = num & 1 ? half : half-1;
					}
					else
					{
						lo = mid + 1;
						num = half;
					}
				}
				else if (num)
					return m_pfn_compare(item, *lo) ? nullptr : lo;
				else
					break;

			return nullptr;
		}
		T* search_by_key(K& key)
		{
			T* lo = m_items.getBase();
			T* hi = lo + (getCount() - 1);
			T* mid;
			int half;
			int num = getCount();
			int result;

			while (lo <= hi)
				if ((half = num / 2) != 0)
				{
					mid = lo + (num & 1 ? half : (half - 1));
					if (!(result = m_pfn_compare_key(key,*mid)))
						return mid;
					else if (result < 0)
					{
						hi = mid - 1;
						num = num & 1 ? half : half-1;
					}
					else
					{
						lo = mid + 1;
						num = half;
					}
				}
				else if (num)
					return m_pfn_compare_key(key, *lo) ? nullptr : lo;
				else
					break;

			return nullptr;
		}
		const T* search_by_key(const K& key) const
		{
			const T* lo = m_items.getBase();
			const T* hi = lo + (getCount() - 1);
			const T* mid;
			int half;
			int num = getCount();
			int result;

			while (lo <= hi)
				if ((half = num / 2) != 0)
				{
					mid = lo + (num & 1 ? half : (half - 1));
					if (!(result = m_pfn_compare_key(key,*mid)))
						return mid;
					else if (result < 0)
					{
						hi = mid - 1;
						num = num & 1 ? half : half-1;
					}
					else
					{
						lo = mid + 1;
						num = half;
					}
				}
				else if (num)
					return m_pfn_compare_key(key, *lo) ? nullptr : lo;
				else
					break;

			return nullptr;
		}

	public:

		// конструктор
		inline SortedArrayT(PFN_COMPARE pfnCompare, PFN_COMPAREKEY pfnCompareKey, int capacity = 0) :
			m_items(capacity), m_pfn_compare(pfnCompare), m_pfn_compare_key(pfnCompareKey)
		{
		}
		// деструктор
		inline ~SortedArrayT()
		{
			clear();
		}
		inline int indexOf(const T& item) const
		{
			const T* found = search(item); //, m_items.Base(), m_items.getCount(), sizeof T, m_pfn_compare);
			return found != nullptr ? PTR_DIFF(found, m_items.getBase()) : BAD_INDEX;
		}
		inline int indexOfKey(K& key)
		{
			T* found = find(key);
			return found ? PTR_DIFF(found, m_items.getBase()) : BAD_INDEX;
		}
		inline T* find(K& key)
		{
			return search_by_key(key); //, m_items.Base(), m_items.getCount(), sizeof T, m_pfn_compare);
		}
		inline T* find(K& key, int& index)
		{
			T* ptr = search_by_key(key);
		
			if (ptr != nullptr)
			{
				index = PTR_DIFF(ptr, m_items.getBase());
			}
			else
			{
				index = BAD_INDEX;
			}
		
			return ptr;
		}
		inline const T* find(const K& key) const
		{
			return search_by_key(key); //, m_items.Base(), m_items.getCount(), sizeof T, m_pfn_compare);
		}
		inline const T* find(const K& key, int& index) const 
		{
			T* ptr = search_by_key(key);
		
			if (ptr != nullptr)
			{
				index = ptr - m_items.getBase();
			}
			else
			{
				index = BAD_INDEX;
			}
		
			return ptr;
		}
		const T* getBase() const { return m_items.getBase(); }
		T* getBase() { return m_items.getBase(); }
		// получить количество элементов
		int getCount() const
		{
			return m_items.getCount();
		}
		// получить ссылку на i элемент
		const T& getAt(int index) const
		{
			return m_items.getAt(index);
		}
		T& getAt(int index)
		{
			return m_items.getAt(index);
		}
		// получить ссылку на i элемент
		T& operator [] (int index)
		{
			return m_items.getAt(index);
		}
		const T& operator [] (int index) const
		{
			return m_items.getAt(index);
		}
		// получить ссылку на элемент по ключу
		T& operator [] (const K& key)
		{
			return *find(key);
		}
		const T& operator [] (const K& key) const
		{
			return *find(key);
		}
		// добавить указатель на объект в массив
		int add(T& item)
		{
			if (m_pfn_compare == nullptr)
				return BAD_INDEX;

			if (getCount() == 0)
			{
				return m_items.add(item);
			}
			else if (getCount() == 1)
			{
				int res = m_pfn_compare(item, m_items[0]);
			
				// если уже существует то вернем ошибку
				if (!res)
					return BAD_INDEX;

				return m_items.insert( res < 0 ? 0 : 1, item);
			}

			T* lo = m_items.getBase();
			T* hi = lo + (getCount() - 1);
			T* mid;
			int half, num = getCount();
			int result;

			while (lo <= hi)
				if ((half = num / 2) != 0)
				{
					mid = lo + (num & 1 ? half : (half - 1));
					// если уже существует то вернем ошибку
					if (!(result = m_pfn_compare(item, *mid)))
						return BAD_INDEX;
					else if (result < 0)
					{
						hi = mid - 1;
						num = num & 1 ? half : half-1;
					}
					else
					{
						lo = mid + 1;
						num = half;
					}
				}
				else
					break;
			// получим индекс
			int index = (int)(lo - m_items.getBase());
			// проверим на если надо то увеличим счетчик
			if (m_pfn_compare(m_items[index], item) < 0)
				index++;

			return m_items.insert(index, item);
		}
		void removeAt(int index)
		{
			m_items.removeAt(index);
		}
		// удалить элемент по адресу
		void remove(T& item)
		{
			int index = indexOf(item);
			if (index != BAD_INDEX)
			{
				m_items.removeAt(index);
			}
		}
		// очистить массив
		void clear()
		{
			m_items.clear();
		}
	};
	//--------------------------------------
	// очередь для встроенных типов, указателей и простых структур
	//--------------------------------------
	template<class T> class QueueT
	{
	protected:
		T m_empty;					// ссылка на пустой элемент
		T* volatile m_items;		// массив элементов
		volatile int m_size;		// размер массива
		volatile int m_count;		// колво элементов в массиве
		volatile int m_head;		// голова очереди
		volatile int m_tail;		// хвост очереди

		void resize(int size)
		{
			if (size <= 0)
			{
				clear();
				return;
			}

			if (m_size < size)
			{
				if ((size & GROW_MASK) != 0)
				{
					size &= ~GROW_MASK;
					size += GROW_SIZE;
				}

				T* arr = (T*)MALLOC(sizeof(T) * size);

				if (m_items != nullptr)
				{
					if (m_count > 0)
					{
						if (m_tail == 0)
						{
							memcpy(arr, m_items, m_count * sizeof(T));
							m_head = m_count;
							// !!!
							memset(arr + m_count, 0, (size - m_count) * sizeof(T));
						}
						else
						{
							memcpy(arr, m_items, m_head * sizeof(T));
							memcpy(arr + (m_head + (size - m_size)),
								m_items + m_tail,
								(m_count - m_tail) * sizeof(T));
							m_tail += size - m_size;
							memset(arr + m_head, 0, (m_tail - m_head) * sizeof(T));
						}
					}
					else
						memset(arr, 0, size * sizeof(T));
					FREE(m_items);
				}
				else
					memset(arr, 0, size * sizeof(T));

				m_size = size;
				m_items = arr;
			}
		}
		int idx_to_ptr(int index)
		{
			int ptr = BAD_INDEX;

			if (index < 0 || index >= m_count || m_count == 0)
				return ptr;

			ptr = m_tail + index;

			if (ptr >= m_size)
			{
				ptr -= m_size;
			}

			return ptr;
		}
		int ptr_to_idx(int ptr)
		{
			int index = BAD_INDEX;

			if (ptr < 0 || m_count == 0 || ptr >= m_size)
				return index;

			index = ptr - m_tail;

			if (index < 0)
			{
				index += m_size;
			}

			return index;
		}
	public:
		QueueT(int capacity = 0) : m_items(nullptr), m_size(0), m_count(0), m_head(0), m_tail(0)
		{
			if (capacity > 0)
				resize(capacity);
			memset (&m_empty, 0, sizeof(m_empty));
		}
		// деструктор
		~QueueT()
		{
			if (m_items != nullptr)
			{
				FREE(m_items);
				m_items = nullptr;
			}

			m_count = 0;
			m_head = 0;
			m_tail = 0;
			m_size = 0;
		}
		int getCount() { return m_count; }
		// очистить массив
		void clear(bool force = false)
		{
			m_count = 0;
			m_head = 0;
			m_tail = 0;

			if (force)
			{
				if (m_items != nullptr)
				{
					FREE(m_items);
					m_items = nullptr;
					m_size = 0;
				}
			}
			else if (m_items != nullptr)
			{
				memset(m_items, 0, sizeof(T) * m_size);
			}
		}
		// добавить объект в очередь
		void push(T& item)
		{
			if (m_count >= m_size)
			{
				resize(m_size + 1);
			}

			m_items[m_head++] = item;
			if (m_head >= m_size)
				m_head = 0;
			m_count++;
		}
		// получим объект из очереди
		T& pop()
		{
			if (m_count == 0)
				return m_empty;
	
			T& item = m_items[m_tail++];
			if (m_tail >= m_size)
				m_tail = 0;
			m_count--;
			return item;
		}
		T& viewTail() { return m_count == 0? m_empty : m_items[m_tail++]; }
		T& viewHead() { return m_count == 0 ? m_empty : m_items[m_head++]; }

		void remove(T& item)
		{
			int idx = indexOf(item);
			if (idx != BAD_INDEX)
				removeAt(idx);
		}
		void removeAt(int index)
		{
			int ptr = idx_to_ptr(index);

			// расмотрим граничные случаи
			if (ptr == m_tail)
			{
				m_tail++;
				if (m_tail >= m_size)
					m_tail = 0;
				m_count--;
			}
			else if (m_head == 0 && ptr == m_size - 1)
			{
				m_head = m_size-1;
				m_count--;
			}
			else if (ptr == m_head - 1)
			{
				m_head--;
				m_count--;
			}
			else if (m_tail < m_head)
			{
				// цельная очередь -- подтянем хвост
				memmove(m_items + m_tail + 1, m_items + m_tail, sizeof(T) * (ptr - m_tail));
				m_tail += 1;
				m_count--;
			}
			else
			{
				// разорванная очередь -- в зависимости от области
				if (ptr >= m_tail) // подтягиваем хвост
				{
					memmove(m_items + m_tail + 1, m_items + m_tail, sizeof(T) * (ptr - m_tail));
					m_tail += 1;
					m_count--;
				}
				else  // или втягиваем голову
				{
					memmove(m_items + ptr, m_items + ptr + 1, sizeof(T) * (m_head - ptr - 1));
					m_head -= 1;
					m_count--;
				}
			}
		}
		T& getAt(int index)
		{
			int ptr = idx_to_ptr(index);
			return ptr != BAD_INDEX ? m_items[ptr] : m_empty;
		}
		T& operator[] (int index)
		{
			return getAt(index);
		}
		int indexOf(T& item)
		{
			int ptr = BAD_INDEX;
		
			if (m_count == 0)
				return BAD_INDEX;

			if (m_tail < m_head)
			{
				for (int i = m_tail; i < m_head; i++)
					if (item == m_items[i])
						return ptr_to_idx(i);
			}
			else
			{
				for (int i = 0; i < m_head; i++)
					if (item == m_items[i])
						return ptr_to_idx(i);

				for (int i = m_tail; i < m_size; i++)
					if (item == m_items[i])
						return ptr_to_idx(i);
			}

			return BAD_INDEX;
		}

		int getCount() const { return m_count; }
		int getHead() const { return m_head; }
		int getTail() const { return m_tail; }
	};
	//--------------------------------------
	// буфер (очередь) обмена без локов
	//--------------------------------------
	template<class T> class PtrQueueWOL
	{
		struct QueueItem
		{
			volatile bool ready;	// TRUE -- готов для записи, FALSE -- готов для чтения
			T* ptr;					// pointer to an object
		};

		QueueItem* m_queue;
		int m_queueSize;
		int m_writeIdx;
		int m_readIdx;
		volatile int m_count;

		QueueItem* getWritingItem()
		{
			return !m_queue[m_writeIdx].ready ? m_queue + m_writeIdx : nullptr;
		}
		void itemWritten()
		{
			if (!m_queue[m_writeIdx].ready)
			{
				m_queue[m_writeIdx].ready = true;
				if (++m_writeIdx >= m_queueSize)
				{
					m_writeIdx = 0;
				}
				std_interlocked_inc(&m_count);
			}
		}
		QueueItem* getReadingItem()
		{
			return m_queue[m_readIdx].ready ? m_queue + m_readIdx : nullptr;
		}
		void itemRead()
		{
			if (m_queue[m_readIdx].ready)
			{
				m_queue[m_readIdx].ready = false;
				if (++m_readIdx >= m_queueSize)
				{
					m_readIdx = 0;
				}
				std_interlocked_dec(&m_count);
			}
		}

	public:
		PtrQueueWOL(int queueDepth) : m_queue(nullptr), m_queueSize(0), m_writeIdx(0), m_readIdx(0), m_count(0)
		{
			if (queueDepth > 0)
				create(queueDepth);
		}
		~PtrQueueWOL()
		{
			reset();
		}
		int getCount()
		{
			return m_count;
		}
		void create(int queueDepth)
		{
			reset();

			m_queueSize = queueDepth;
			m_queue = (QueueItem*)MALLOC(m_queueSize * sizeof(QueueItem));
			::memset(m_queue, 0, m_queueSize * sizeof(QueueItem));
		}
		void reset()
		{
			if (m_queue != nullptr)
			{
				FREE(m_queue);
				m_queue = nullptr;
			}

			m_queueSize = m_writeIdx = m_readIdx = m_count = 0;
		}

		bool push(T* obj)
		{
			QueueItem* item = getWritingItem();

			if (item == nullptr)
				return false;

			item->ptr = obj;
			itemWritten();

			return true;
		}
	
		T* pop()
		{
			T* obj = nullptr;
			QueueItem* queueItem = getReadingItem();

			if (queueItem != nullptr)
			{
				obj = queueItem->ptr;
				queueItem->ptr = nullptr;
			
				itemRead();
			}

			return obj;
		}
	};
	//--------------------------------------
	// стек для встроенных типов, указателей и простых структур
	//--------------------------------------
	template<class T> class Stack
	{
	protected:
		T m_empty;		// ссылка на пустой элемент
		T* volatile m_items;		// массив элементов
		volatile int m_size;		// размер массива
		volatile int m_count;		// колво элементов в массиве

		void resize(int size)
		{
			if (size == 0)
			{
				clear();
				return;
			}

			if (m_size < size)
			{
				if ((size & GROW_MASK) != 0)
				{
					size &= ~GROW_MASK;
					size += GROW_SIZE;
				}

				T* arr = (T*)MALLOC(sizeof(T) * size);

				memset(arr + m_count, 0, (size - m_count) * sizeof(T));

				if (m_items != nullptr)
				{
					if (m_count > 0)
						memcpy(arr, m_items, m_count * sizeof(T));
					FREE(m_items);
				}
				m_items = arr;
				m_size = size;
			}
		}

	public:
		Stack(int capacity = 0) : m_items(nullptr), m_size(0), m_count(0)
		{
			if (capacity > 0)
				resize(capacity);
			memset (&m_empty, 0, sizeof(m_empty));
		}
		// деструктор
		virtual ~Stack()
		{
			clear();
		}
		int getCount()
		{
			return m_count;
		}
		// очистить массив
		void clear()
		{
			if (m_items != nullptr)
			{
				FREE(m_items);
				m_items = nullptr;
			}
			m_size = m_count = 0;
		}
		T& getAt(int index)
		{
			if (index >= m_count)
				return m_empty;

			return m_items[m_count-(index+1)];
		}
		T& operator[] (int index)
		{
			return getAt(index);
		}
		// добавить объект в очередь
		void push(T& item)
		{
			if (m_count >= m_size)
			{
				resize(m_size + 1);
			}

			m_items[m_count++] = item;
		}
		// получим объект из очереди
		T& pop()
		{
			if (m_count == 0)
				return m_empty;
	
			return m_items[--m_count];
		}
	};
	//--------------------------------------
	//
	//--------------------------------------
	template<class T, class K> class tree_t
	{
		struct NODE
		{
			NODE*	parent;
			NODE*	right;
			NODE*	left;
			int		color;
			K		key;
			T		value;

			NODE()
			{
				parent = right = left = nullptr;
				color = 0;
			}
		};

		NODE			m_empty;
	//	#define NIL_NODE (&m_empty)

		NODE*			m_root;
		int				m_count;
		mutable NODE*	m_last;

		#define BLACK_NODE	0
		#define RED_NODE	1
		#define NIL_NODE	&m_empty

		void rotate_left(NODE* x)
		{
			NODE* y = x->right;

			/* establish x->right link */
			x->right = y->left;

			if (y->left != nullptr)
			{
				y->left->parent = x;
			}

			/* establish y->parent link */
			if (y != NIL_NODE)
			{
				y->parent = x->parent;
			}

			if (x->parent)
			{
				if (x == x->parent->left)
				{
					x->parent->left = y;
				}
				else
				{
					x->parent->right = y;
				}
			}
			else
			{
				m_root = y;
			}

			/* link x and y */
			y->left = x;
	    
			if (x != NIL_NODE)
			{
				x->parent = y;
			}
		}
		void rotate_right(NODE* x)
		{
			/****************************
			*  rotate node x to right  *
			****************************/

			NODE *y = x->left;

			/* establish x->left link */
			x->left = y->right;
			if (y->right != NIL_NODE)
			{
				y->right->parent = x;
			}

			/* establish y->parent link */
			if (y != NIL_NODE)
			{
				y->parent = x->parent;
			}

			if (x->parent)
			{
				if (x == x->parent->right)
					x->parent->right = y;
				else
					x->parent->left = y;
			}
			else
			{
				m_root = y;
			}

			/* link x and y */
			y->right = x;
			if (x != NIL_NODE)
			{
				x->parent = y;
			}
		}
		void insert_fixup(NODE* x)
		{
			/*************************************
			*  maintain Red-Black tree balance  *
			*  after inserting node x           *
			*************************************/

			/* check Red-Black properties */
			while (x != m_root && x->parent->color == RED_NODE)
			{
				/* we have a violation */
				if (x->parent == x->parent->parent->left)
				{
					NODE *y = x->parent->parent->right;
					if (y->color == RED_NODE)
					{
						/* uncle is RED_NODE */
						x->parent->color = BLACK_NODE;
						y->color = BLACK_NODE;
						x->parent->parent->color = RED_NODE;
						x = x->parent->parent;
					}
					else
					{
						/* uncle is BLACK_NODE */
						if (x == x->parent->right)
						{
							/* make x a left child */
							x = x->parent;
							rotate_left(x);
						}

						/* recolor and rotate */
						x->parent->color = BLACK_NODE;
						x->parent->parent->color = RED_NODE;
						rotate_right(x->parent->parent);
					}
				}
				else
				{
					/* mirror image of above code */
					NODE *y = x->parent->parent->left;
					if (y->color == RED_NODE)
					{
						/* uncle is RED_NODE */
						x->parent->color = BLACK_NODE;
						y->color = BLACK_NODE;
						x->parent->parent->color = RED_NODE;
						x = x->parent->parent;
					}
					else
					{
						/* uncle is BLACK_NODE */
						if (x == x->parent->left)
						{
							x = x->parent;
							rotate_right(x);
						}
						x->parent->color = BLACK_NODE;
						x->parent->parent->color = RED_NODE;
						rotate_left(x->parent->parent);
					}
				}
			}

			m_root->color = BLACK_NODE;
		}
		void delete_fixup(NODE* x)
		{
			/*************************************
			 *  maintain Red-Black tree balance  *
			 *  after deleting node x            *
			 *************************************/

			while (x != m_root && x->color == BLACK_NODE)
			{
				if (x == x->parent->left)
				{
					NODE *w = x->parent->right;
					if (w->color == RED_NODE)
					{
						w->color = BLACK_NODE;
						x->parent->color = RED_NODE;
						rotate_left (x->parent);
						w = x->parent->right;
					}

					if (w->left->color == BLACK_NODE && w->right->color == BLACK_NODE)
					{
						w->color = RED_NODE;
						x = x->parent;
					}
					else
					{
						if (w->right->color == BLACK_NODE)
						{
							w->left->color = BLACK_NODE;
							w->color = RED_NODE;
							rotate_right (w);
							w = x->parent->right;
						}

						w->color = x->parent->color;
						x->parent->color = BLACK_NODE;
						w->right->color = BLACK_NODE;
						rotate_left (x->parent);
						x = m_root;
					}
				}
				else
				{
					NODE *w = x->parent->left;
					if (w->color == RED_NODE)
					{
						w->color = BLACK_NODE;
						x->parent->color = RED_NODE;
						rotate_right (x->parent);
						w = x->parent->left;
					}

					if (w->right->color == BLACK_NODE && w->left->color == BLACK_NODE)
					{
						w->color = RED_NODE;
						x = x->parent;
					}
					else
					{
						if (w->left->color == BLACK_NODE)
						{
							w->right->color = BLACK_NODE;
							w->color = RED_NODE;
							rotate_left (w);
							w = x->parent->left;
						}
					
						w->color = x->parent->color;
						x->parent->color = BLACK_NODE;
						w->left->color = BLACK_NODE;
						rotate_right (x->parent);
						x = m_root;
					}
				}
			}

			x->color = BLACK_NODE;
		}
		const NODE* search(const K& key) const
		{
			/*******************************
			 *  find node containing data  *
			 *******************************/

			if (m_last != nullptr && key == m_last->key)
			{
				return m_last;
			}

			NODE *current = m_root;
			while (current != NIL_NODE)
			{
				if (key == current->key)
					return m_last = current;
				else
					current = key < current->key ? current->left : current->right;
			}

			m_last = nullptr;
			return nullptr;
		}
		NODE* search(const K& key)
		{
			/*******************************
			 *  find node containing data  *
			 *******************************/

			if (m_last != nullptr && key == m_last->key)
			{
				return m_last;
			}

			NODE *current = m_root;
			while (current != NIL_NODE)
			{
				if (key == current->key)
					return m_last = current;
				else
					current = key < current->key ? current->left : current->right;
			}

			m_last = nullptr;
			return nullptr;
		}
		const T& getValue(const NODE* x) const
		{
			return x != nullptr ? x->value : m_empty.value;
		}
		T& getValue(NODE* x)
		{
			return x != nullptr ? x->value : m_empty.value;
		}
		void delete_branch(NODE* x)
		{
			if (x != NIL_NODE)
			{
				if (m_last == x)
				{
					m_last = NIL_NODE;
				}

				if (x->left != NIL_NODE)
				{
					delete_branch(x->left);
					x->left = NIL_NODE;
				}

				if (x->right != NIL_NODE)
				{
					delete_branch(x->right);
					x->right = NIL_NODE;
				}

				DELETEO(x);

				m_count--;
			}
		}

	public:
		tree_t(const T& empty_value, const K& empty_key)
		{
			m_empty.left = m_empty.right = NIL_NODE;
			m_empty.key = empty_key;
			m_empty.value = empty_value;
			m_root = NIL_NODE;
			m_count = 0;
			m_last = NIL_NODE;
		}
		tree_t(const tree_t& tree)
		{
			m_empty.left = m_empty.right = NIL_NODE;
			m_empty.key = tree.m_empty.key;
			m_empty.value = tree.m_empty.value;
			m_root = NIL_NODE;
			m_count = 0;
			m_last = NIL_NODE;
		}
		~tree_t()
		{
			clear();
		}

		tree_t& operator = (const tree_t& map)
		{
			return assign(map);
		}
		tree_t& assign(const tree_t& map)
		{
			return *this;
		}

		int add(const K& key, const T& value)
		{
			NODE *current, *parent, *x;

			/***********************************************
			*  allocate node for data and insert in tree  *
			***********************************************/

			/* find where node belongs */
			current = m_root;
			parent = 0;

			while (current != NIL_NODE)
			{
				if (key ==  current->key)
					return 0;

				parent = current;
				current = key < current->key ? current->left : current->right;
			}

			/* setup New node */
			x = NEW NODE;

			x->parent = parent;
			x->left = NIL_NODE;
			x->right = NIL_NODE;
			x->color = RED_NODE;
			x->key = key;
			x->value = value;

			/* insert node in tree */
			if (parent != nullptr)
			{
				if (key < parent->key)
					parent->left = x;
				else
					parent->right = x;
			}
			else
			{
				m_root = x;
			}

			insert_fixup(x);

			m_count++;

			return 1;
		}
		void clear()
		{
			delete_branch(m_root);
			m_root = m_last = NIL_NODE;
		}
		T remove(const K& key)
		{
			NODE* z = search(key);

			/*****************************
			 *  delete node z from tree  *
			 *****************************/

			if (z == nullptr || z == NIL_NODE)
				return m_empty.value;

			NODE *x, *y;

			if (z->left == NIL_NODE || z->right == NIL_NODE)
			{
				/* y has a NIL_NODE node as a child */
				y = z;
			}
			else
			{
				/* find tree successor with a NIL_NODE node as a child */
				y = z->right;
				while (y->left != NIL_NODE)
				{
					y = y->left;
				}
			}

			/* x is y's only child */
			if (y->left != NIL_NODE)
			{
				x = y->left;
			}
			else
			{
				x = y->right;
			}

			/* remove y from the parent chain */
			x->parent = y->parent;
			if (y->parent)
			{
				if (y == y->parent->left)
				{
					y->parent->left = x;
				}
				else
				{
					y->parent->right = x;
				}
			}
			else
			{
				m_root = x;
			}

			if (y != z)
			{
				z->key = y->key;
				z->value = y->value;
			}


			if (y->color == BLACK_NODE)
			{
				delete_fixup(x);
			}

			m_count--;

			T value = y->value;
			DELETEO(y);

			if (y == m_last)
				m_last = nullptr;

			return value;
		}
		const T& get(const K& key) const
		{
			return getValue(search(key));
		}
		T& get(const K& key)
		{
			return getValue(search(key));
		}
		const T& operator [] (const K& key) const
		{
			return get(key);
		}
		T& operator [] (const K& key)
		{
			return get(key);
		}
		bool contains(const K& key) const
		{
			return search(key) != nullptr;
		}
		int getCount() const
		{
			return m_count;
		}
	};
}
//--------------------------------------
