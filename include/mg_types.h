﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_enums.h"

namespace h248
{
	//--------------------------------------
	// ключи MEGACO
	//--------------------------------------
#define CONF_MEGACO_MESSAGE_PRETTY				"megaco-message-pretty"
#define CONF_MEGACO_MESSAGE_PRETTY_INDENT_TAB	"megaco-message-pretty-indent-tab"
#define CONF_MEGACO_MESSAGE_PRETTY_INDENT		"megaco-message-pretty-indent"
#define CONF_MEGACO_DEF_TEXT_PORT				"megaco-def-text-port"
#define CONF_MEGACO_DEF_TEXT_INTERFACE			"megaco-def-text-interface"
#define CONF_MEGACO_CONTEXT_LIFESPAN			"megaco-def-context_lifespan"

#define MEGACO_ERROR(errorCode, errorText) if (reply != nullptr) reply->addErrorDescription(errorCode, errorText); /
	//--------------------------------------
	// описатель конечных точек маршрутов
	//--------------------------------------
	struct EndPoint
	{
		TransportType type;
		in_addr address;
		uint16_t port;
	};
	//--------------------------------------
	// описатель маршрутов
	//--------------------------------------
	struct Route
	{
		TransportType type;
		in_addr localAddress;
		uint16_t localPort;
		in_addr remoteAddress;
		uint16_t remotePort;
	};
	//--------------------------------------
	// описатель транзакции
	// номер танзакции и номер контроллера = идентификатор транзакции
	//--------------------------------------
	struct TransactionID
	{
		union
		{
			struct { uint32_t _tid, _cid; }; //
			uint64_t id;
		};
	};

	inline bool operator == (const TransactionID& left, const TransactionID& right) { return left.id == right.id; }
	inline bool operator != (const TransactionID& left, const TransactionID& right) { return left.id != right.id; }
	//--------------------------------------
	//
	//--------------------------------------
	class TerminationID
	{
		// id of termination
		TerminationType m_type;
		char* m_interface;
		uint32_t m_id;

	public:
		TerminationID() : m_type(TerminationType::Root), m_interface(nullptr), m_id(0) { }
		TerminationID(const TerminationID& id) : m_interface(nullptr) { *this = id; }
		TerminationID(TerminationType type, const char* iface, uint32_t id) : m_type(type), m_interface(rtl::strdup(iface)), m_id(id) { }
		TerminationID(const char* text) : m_interface(nullptr) { parse(text); }
		~TerminationID() { FREE(m_interface); }

		TerminationID& operator = (const TerminationID& id) { m_type = id.m_type; rtl::strupdate(m_interface, id.m_interface); m_id = id.m_id; return *this; }

		void setType(TerminationType type) { m_type = type; }
		void setInterface(const char* iface) { rtl::strupdate(m_interface, iface); }
		void setId(uint32_t id) { m_id = id; }

		RTX_MG_API bool parse(const char* text);
		RTX_MG_API const char* toString(char* buffer, int size) const;

		TerminationType getType() const { return m_type; }
		const char* getInterface() const { return m_interface; }
		uint32_t getId() const { return m_id; }

		RTX_MG_API bool operator == (const TerminationID& id) const;
		RTX_MG_API bool operator != (const TerminationID& id) const;

		bool isChoose() { return isInterfaceChoose() && m_type == TerminationType::Choose && m_id == MG_TERM_CHOOSE; }
		bool isInterfaceChoose() { return m_interface != nullptr && m_interface[0] == '$'; }
		bool isTypeChoose() { return m_type == TerminationType::Choose; }
		bool isIdChoose() { return m_id == MG_TERM_CHOOSE; }

		bool isAll() { return isInterfaceAll() || m_type == TerminationType::All || m_id == MG_TERM_ALL; }
		bool isInterfaceAll() { return m_interface != nullptr && m_interface[0] == '*'; }
		bool isTypeAll() { return m_type == TerminationType::All; }
		bool isIdAll() { return m_id == MG_TERM_ALL; }

		bool isRoot() { return m_interface == nullptr || m_type == TerminationType::Root || m_id == 0; }
	};
	//--------------------------------------
	// правила оформления сообщения в тестовом виде
	//--------------------------------------
	struct MessageWriterParams
	{
		bool human_readable; // if false used compact name forms
		bool use_tab;
		int spaces_count;	// if !tab
		int level;
	};
	//--------------------------------------
	//
	//--------------------------------------
	RTX_MG_API Token Token_parse(const char* token, int length = -1);
	RTX_MG_API const char* Token_toString(Token token);
	RTX_MG_API const char* Token_toStringLong(Token token);
	RTX_MG_API const char* Token_toStringShort(Token token);
	RTX_MG_API const char* ErrorCode_toString(ErrorCode);
	RTX_MG_API const char* Reason_toString(Reason);
	RTX_MG_API Reason Reason_parse(const char* reason, rtl::String& text);
	RTX_MG_API TerminationStreamMode TerminationStreamMode_parse(const char* text);
	RTX_MG_API const char* TerminationStreamMode_toString(TerminationStreamMode dir);
	RTX_MG_API const char* TerminationServiceState_toString(TerminationServiceState state);
	RTX_MG_API const char* TerminationType_toString(TerminationType type);

	RTX_MG_API TopologyDirection TopologyDirection_parse(const char* text);
	RTX_MG_API const char* TopologyDirection_toString(TopologyDirection dir);
	RTX_MG_API const char* TransportType_toString(TransportType type);

	RTX_MG_API void setTokenNameMode(bool long_names);
	RTX_MG_API bool isValidCommandToken(Token token);

	//--------------------------------------
	// ;The values 0x0, 0xFFFFFFFE and 0xFFFFFFFF are reserved.
	// ; '-' is used for NULL context. '*' is ALL. '$' is CHOOSE.
	// ContextID = (UINT32 / "*" / "-" / "$")
	//--------------------------------------
	//RTX_MG_API bool parseContextId(const char* text, uint32_t& id);
	//--------------------------------------
	// 0 - NULL, 0xFFFFFFFE - CHOOSE, 0xFFFFFFFF - ALL
	//--------------------------------------
}
//--------------------------------------
