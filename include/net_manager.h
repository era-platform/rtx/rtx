﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "net_address.h"
#include "net_sock.h"
//--------------------------------------
// управляющий менеджер для сетевых соединений
//--------------------------------------

struct net_monitor_t
{
	virtual void net_transport_network_failed() = 0;
	virtual void net_transport_addresses_added(const rtl::ArrayT<in_addr>& added_addresses) = 0;
	virtual void net_transport_addresses_removed(const rtl::ArrayT<in_addr>& removed_addresses) = 0;
};

class net_t
{
public:
	//<public>
	RTX_NET_API static bool start();
	RTX_NET_API static void stop();

	/*inline static bool ip_is_private(const in_addr& addr)
	{
		return (addr.s_net == 10) || ((addr.s_net == 172) && (addr.s_host >= 16) && (addr.s_host <= 31)) ||
			((addr.s_net == 192) && (addr.s_host == 168));
	}*/
	inline static bool ip_is_any(const in_addr& addr) { return addr.s_addr == INADDR_ANY; }
	inline static bool ip_is_valid(const in_addr& addr) { return addr.s_addr != INADDR_ANY && addr.s_addr != INADDR_NONE; }
	inline static bool ip_is_none(in_addr addr) { return addr.s_addr == INADDR_NONE; }
	inline static bool ip_is_broadcast(in_addr& addr) { return addr.s_addr == htonl(INADDR_BROADCAST); }
	inline static bool ip_is_loopback(const in_addr& addr) { return addr.s_addr == htonl(INADDR_LOOPBACK); }
	RTX_NET_API static bool ip_is_localhost(in_addr addr);

	RTX_NET_API static bool get_best_interface(in_addr destination, in_addr& iface);
	inline static bool get_best_interface(const char* domain, in_addr& domain_ip, in_addr& iface) { return get_host_by_name(domain, domain_ip, iface); }
	RTX_NET_API static bool get_host_by_name(const char* domain, in_addr& domain_ip, in_addr& iface);
	RTX_NET_API static in_addr get_host_by_name(const char* host);
	RTX_NET_API static int get_host_by_name(const char* host, rtl::ArrayT<in_addr>& ip_table);
//	RTX_NET_API static bool get_default_interface(in_addr& iface);
	RTX_NET_API static int get_interface_count();
	RTX_NET_API static bool get_interface_at(int index, in_addr& iface);
	RTX_NET_API static bool get_interface_table(rtl::ArrayT<in_addr>& iface_table);
//	RTX_NET_API static bool get_gateway(in_addr& gateway);
	RTX_NET_API static in_addr select_address_by_mask(const rtl::ArrayT<in_addr>& list, in_addr mask);
};
//--------------------------------------
