﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "std_stream_base.h"

namespace rtl
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	class MemoryStream : public Stream
	{
		MemoryStream(const MemoryStream&);
		MemoryStream& operator=(const MemoryStream&);

	public:
		static const uint32_t DEFAULT_MEMORY_BLOCK_SIZE = 8192;

		RTX_STD_API MemoryStream(uint32_t mem_block_size = DEFAULT_MEMORY_BLOCK_SIZE);
		RTX_STD_API virtual ~MemoryStream() { close(); }

		RTX_STD_API virtual void close();

		RTX_STD_API virtual size_t getLength();
		RTX_STD_API virtual size_t setLength(size_t length);

		RTX_STD_API virtual size_t getPosition();
		RTX_STD_API virtual size_t seek(SeekOrigin origin, intptr_t pos);

		RTX_STD_API virtual size_t write(const void* data, size_t len);
		RTX_STD_API virtual size_t read(void* buffer, size_t size);

		const uint8_t* getBuffer() const { return m_mem; }

		// sugar
		MemoryStream& operator << (const char* value) { write(value, strlen(value)); return *this; }
		MemoryStream& operator << (char value) { write(&value, 1); return *this; }
		RTX_STD_API MemoryStream& operator << (int32_t value);
		RTX_STD_API MemoryStream& operator << (uint32_t value);
		RTX_STD_API MemoryStream& operator << (int64_t value);
		RTX_STD_API MemoryStream& operator << (uint64_t value);

	private:
		uint8_t* m_mem;
		size_t m_mem_pos;
		size_t m_mem_length;
		uint32_t m_block_size;

		bool realloc(size_t new_size);
	};

	//-----------------------------------------------
	//
	//-----------------------------------------------
	class MemoryReader : public Stream
	{
	public:
		MemoryReader() : m_mem(nullptr), m_mem_pos(0), m_mem_length(0) { }
		MemoryReader(const uint8_t* memory, int length) : m_mem(memory), m_mem_pos(0), m_mem_length(length) { }
		RTX_STD_API virtual ~MemoryReader();

		void initialize(const uint8_t* memory, int length) { m_mem = memory; m_mem_pos = 0; m_mem_length = length; }

		RTX_STD_API virtual void close();

		RTX_STD_API virtual size_t getLength();
		RTX_STD_API virtual size_t setLength(size_t length);

		RTX_STD_API virtual size_t getPosition();
		RTX_STD_API virtual size_t seek(SeekOrigin origin, intptr_t pos);

		RTX_STD_API virtual size_t write(const void* data, size_t len);
		RTX_STD_API virtual size_t read(void* buffer, size_t size);

		const uint8_t* getBuffer() const { return m_mem; }

	private:
		const uint8_t* m_mem;
		size_t m_mem_pos;
		size_t m_mem_length;
	};
}
//-----------------------------------------------
