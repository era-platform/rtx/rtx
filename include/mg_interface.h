﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_enums.h"

namespace h248
{
	//--------------------------------------
	//
	//--------------------------------------
#define MG_TERM_INVALID			0x0			// 0 invalid
#define MG_TERM_ERR_CTX			0xFFFFFFFF	// -1 unlocked context!
#define MG_TERM_ERR_FAILED		0xFFFFFFFE	// -2
#define MG_TERM_ERR_RES			0xFFFFFFFD	// -3
//--------------------------------------
//
//--------------------------------------
	struct IMediaContext;
	class Field;
	//--------------------------------------
	//
	//--------------------------------------
	struct TopologyTriple
	{
		uint32_t term_from;
		uint32_t term_to;
		TopologyDirection direction;
	};
	//--------------------------------------
	//
	//--------------------------------------
	struct TerminationLocalControl
	{
		TerminationStreamMode stream_mode;
		bool reserve_group;
		bool reserve_value;
	};
	//--------------------------------------
	// базовое событие!
	//--------------------------------------
	class EventParams
	{
	public:
		enum Type { Notify, ServiceChange }; // Dtmf, VAD, EndPlay, Fax, NoVoice, VoiceRestored, NetError

	private:
		uint32_t m_ctxId;
		TerminationID m_termId;
		uint16_t m_streamId;
		Type m_type;

	public:
		EventParams(Type type) : m_type(type), m_ctxId(0), m_streamId(0) { }
		RTX_MG_API virtual ~EventParams();

		uint32_t getContextId() { return m_ctxId; }
		const TerminationID& getTermId() { return m_termId; }
		uint16_t getStreamId() { return m_streamId; }
		Type getType() { return m_type; }

		void setCtxId(uint32_t id) { m_ctxId = id; }
		void setTermId(TerminationID id) { m_termId = id; }
		void setStreamId(uint16_t id) { m_streamId = id; }

		virtual Field* makeEvent() = 0;
	};
	//--------------------------------------
	// базовое событие Notify!
	//--------------------------------------
	class ObservedEventParams : public EventParams
	{
	protected:
		const char* m_eventName;
		uint16_t m_requestId;
		Field* m_eventNode;

	public:
		ObservedEventParams(uint16_t requestId, const char* eventName) :
			EventParams(Notify), m_requestId(requestId), m_eventName(eventName), m_eventNode(nullptr) { }

		RTX_MG_API virtual Field* makeEvent();
	};
	//--------------------------------------
	// событие Notify DTMF!
	//--------------------------------------
	class DTMF_EventParams : public ObservedEventParams
	{
		char m_dtmf;
		uint32_t m_duration;

	public:
		DTMF_EventParams(uint16_t requestId, char dtmf, uint32_t duration) :
			ObservedEventParams(requestId, "dd/ce"), m_dtmf(dtmf), m_duration(duration) { }

		char getDTMF() { return m_dtmf; };

		RTX_MG_API virtual Field* makeEvent();
	};
	//--------------------------------------
	// событие Notify VAD!
	//--------------------------------------
	class VAD_EventParams : public ObservedEventParams
	{
		bool m_state;
		uint32_t m_duration;

	public:
		VAD_EventParams(uint16_t requestId, bool state, uint32_t duration) :
			ObservedEventParams(requestId, "vdp/vad"), m_state(state), m_duration(duration) { }

		RTX_MG_API virtual Field* makeEvent();
	};

	//--------------------------------------
	//
	//--------------------------------------
	class IVR_EventParams : public ObservedEventParams
	{
		rtl::StringList m_fileList;
		uint32_t m_stopTime;

	public:
		IVR_EventParams(uint32_t stopTime) :
			ObservedEventParams(1235, "ivr/playend"), m_stopTime(stopTime) { }
		IVR_EventParams(uint32_t stopTime, const rtl::StringList& fileList) :
			ObservedEventParams(1235, "ivr/playend"), m_stopTime(stopTime) {
			copyFileList(fileList);
		}
		RTX_MG_API virtual ~IVR_EventParams() override;

		RTX_MG_API virtual Field* makeEvent() override;

	private:
		RTX_MG_API void copyFileList(const rtl::StringList& fileList);
	};
	//--------------------------------------
	//
	//--------------------------------------
	class FAX_EventParams : public ObservedEventParams
	{
		bool m_error;

	public:
		FAX_EventParams(bool failed) :
			ObservedEventParams(1236, "fax/end"), m_error(failed) { }

		RTX_MG_API virtual Field* makeEvent();
	};
	//--------------------------------------
	//
	//--------------------------------------
	class ServiceChangedEventParams : public EventParams
	{
	protected:
		Token m_method;
		Reason m_reason;
		Field* m_services;
	public:
		ServiceChangedEventParams(Token method, Reason reason) :
			EventParams(ServiceChange), m_method(method), m_reason(reason), m_services(nullptr) { }

		RTX_MG_API virtual Field* makeEvent();
	};
	//--------------------------------------
	//
	//--------------------------------------
	class NoVoiceEventParams : public ServiceChangedEventParams
	{
		uint32_t m_xtime;
	public:
		NoVoiceEventParams(uint32_t duration) :
			ServiceChangedEventParams(Token::Forced, Reason::c905_Termination_taken_out_of_service) { }

		RTX_MG_API virtual Field* makeEvent();
	};
	//--------------------------------------
	//
	//--------------------------------------
	class VoiceRestoredEventParams : public ServiceChangedEventParams
	{
	public:
		VoiceRestoredEventParams() :
			ServiceChangedEventParams(Token::Restart, Reason::c918_Cancel_graceful) { }
	};
	//--------------------------------------
	//
	//--------------------------------------
	class NetErrorEventParams : public ServiceChangedEventParams
	{
	public:
		NetErrorEventParams() :
			ServiceChangedEventParams(Token::Forced, Reason::c904_Termination_malfunctioning) { }
	};
	//--------------------------------------
	//
	//--------------------------------------
	struct ITerminationEventHandler
	{
		virtual void TerminationEvent_callback(EventParams* eventData) = 0;
	};
	//--------------------------------------
	// внешнее управление доступа к контексту по очередности
	//--------------------------------------
	struct IMediaContext
	{
		virtual bool mg_context_initialize(ITerminationEventHandler* handler, Field* reply) = 0;

		virtual uint32_t mg_context_get_id() = 0;

		virtual bool mg_context_lock() = 0;
		virtual bool mg_context_unlock() = 0;

		virtual uint32_t mg_context_add_termination(TerminationID& termId, Field* reply) = 0;
		virtual void mg_context_remove_termination(uint32_t termId) = 0;

		virtual bool mg_termination_lock(uint32_t termId) = 0;
		virtual bool mg_termination_unlock(uint32_t termId) = 0;

		// настройки TerminationState
		virtual bool mg_termination_set_TerminationState(uint32_t termId, TerminationServiceState state, Field* reply) = 0;
		virtual bool mg_termination_set_TerminationState_EventControl(uint32_t termId, bool eventBufferControl, Field* reply) = 0;
		virtual bool mg_termination_set_TerminationState_Property(uint32_t termId, const Field* property, Field* reply) = 0;
		// настройки LocalControl Local Remote
		virtual bool mg_termination_set_LocalControl_Mode(uint32_t termId, uint16_t streamId, TerminationStreamMode mode, Field* reply) = 0;
		virtual bool mg_termination_set_LocalControl_ReserveValue(uint32_t termId, uint16_t streamId, bool value, Field* reply) = 0;
		virtual bool mg_termination_set_LocalControl_ReserveGroup(uint32_t termId, uint16_t streamId, bool value, Field* reply) = 0;
		virtual bool mg_termination_set_LocalControl_Property(uint32_t termId, uint16_t streamId, const Field* property, Field* reply) = 0;
		// настройки Local и Remote
		virtual bool mg_termination_set_Local(uint32_t termId, uint16_t streamId, const rtl::String& localOffer, rtl::String& localAnswer, Field* reply) = 0;
		virtual bool mg_termination_set_Remote(uint32_t termId, uint16_t streamId, const rtl::String& remote, Field* reply) = 0;
		virtual bool mg_termination_set_Local_Remote(uint32_t termId, uint16_t streamId, const rtl::String& remote, const rtl::String& local, Field* reply) = 0;
		virtual bool mg_termination_get_Local(uint32_t termId, uint16_t streamId, rtl::String& local, Field* reply) = 0;

		// настройки событий
		virtual bool mg_termination_set_events(uint32_t termId, uint16_t request_id, const Field* eventsDescriptor, Field* reply) = 0;
		virtual bool mg_termination_set_signals(uint32_t termId, uint16_t streamId, const Field* signalsDescriptor, Field* reply) = 0;
		virtual bool mg_termination_set_property(uint32_t termId, const Field* property, Field* reply) = 0;

		virtual bool mg_context_set_topology(const rtl::ArrayT<TopologyTriple>& topologyTriple, Field* reply) = 0;
		virtual bool mg_context_set_property(const Field* property, Field* reply) = 0;
	};
	//--------------------------------------
	//
	//--------------------------------------
	struct IMediaGateEventHandler
	{
		virtual void mge__event_raised() = 0;
		virtual void mge__error_raised() = 0;
	};
	//--------------------------------------
	//
	//--------------------------------------
	struct IAudioDeviceInputCallback
	{
		virtual void deviceInput__dataReady(const uint8_t* data, uint32_t length, uint32_t ts) = 0;
	};
	//--------------------------------------
	struct IAudioDeviceInput
	{
		virtual uintptr_t deviceInput__addHandler(IAudioDeviceInputCallback* cb, uint32_t frequency, uint32_t channels) = 0;
		virtual void deviceInput__removeHandler(uintptr_t device) = 0;
	};
	//--------------------------------------
	struct IAudioDeviceOutput
	{
		virtual uintptr_t deviceOutput__addHandler(uint32_t frequency, uint32_t channels) = 0;
		virtual void deviceOutput__removeHandler(uintptr_t device) = 0;
		virtual void deviceOutput__playAudio(uintptr_t device, const uint8_t* data, uint32_t length, uint16_t seqNo) = 0;
	};
	//--------------------------------------
	//
	//--------------------------------------
	struct IMediaGate
	{
		virtual bool MediaGate_initialize(IMediaGateEventHandler* callback) = 0;
		virtual void MediaGate_destroy() = 0;

		virtual void MediaGate_setInterfaceName(const char* iface, const char* addressKey) = 0;
		virtual void MediaGate_removeInterfaceName(const char* iface) = 0;

		virtual bool MediaGate_addPortRangeRTP(const char* addressKey, uint16_t portBegin, uint16_t portCount) = 0;
		virtual void MediaGate_freePortRangeRTP(const char* addressKey) = 0;

		// простая блокировка
		virtual IMediaContext* MediaGate_createContext() = 0;
		virtual void MediaGate_destroyContext(IMediaContext* ctx) = 0;

		// RTX-48
		virtual void MediaGate_setDeviceIn(IAudioDeviceInput* device) = 0;
		virtual void MediaGate_setDeviceOut(IAudioDeviceOutput* device) = 0;
	};
	//--------------------------------------
	//
	//--------------------------------------
	typedef	IMediaGate* (*pfn_createMediaGate)(const char* logPath);
	typedef	void(*pfn_removeMediaGate)();
}
//--------------------------------------
