﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "std_memory_stream.h"

namespace rtl
{
	//--------------------------------------
	// константы
	//--------------------------------------
#define STR_BADINDEX	-1
#define STR_GROW_SIZE	0x00000040
#define STR_GROW_MASK	0x0000003F
//--------------------------------------
// структуры для работы класса String
//--------------------------------------
#pragma pack(push, 1)
	struct string_t__ptr
	{
		void* address;
		volatile uint32_t ref;
		int size;
		int length;
		char string[1];
	};
#pragma pack(pop)
	//--------------------------------------
	// обращения к инфо полям String
	//--------------------------------------
#define AOFFSET_BASE (sizeof(void*) + sizeof(uint32_t) + sizeof(int) + sizeof(int)) // 20
#define AOFFSET_LEN sizeof(int)
//---
	inline string_t__ptr* STR_PTR(char* ptr) { return ((string_t__ptr*)(ptr - AOFFSET_BASE)); }
	inline int& STR_LEN(char* ptr) { return (*(int*)(ptr - AOFFSET_LEN)); }
	//---
	string_t__ptr* string_t__allocate(int str_len);
	string_t__ptr* string_t__reallocate(string_t__ptr* ptr, int new_str_len);
	void string_t__addref(char* strptr);
	void string_t__release(char* strptr);
	void string_t__set_length(char* strptr, int length);
	char* string_t__allocate_new(int capacity);
	char* string_t__copy(char* strptr);
	char* string_t__set(char* strptr, const char* str, int str_len);
	char* string_t__add(char* strptr, const char* str, int str_len);
	char* string_t__insert(char* strptr, int index, const char* str, int str_len);
	char* string_t__remove(char* strptr, int start, int count);
	//--------------------------------------
	//
	//--------------------------------------
	typedef const char* ZString;
	//--------------------------------------
	//
	//--------------------------------------
	class StringList;
	//--------------------------------------
	//
	//--------------------------------------
	class String
	{
	public:
		String() : m_ptr(nullptr) { }
		String(const String& str) : m_ptr(nullptr) { assign(str); }
		String(const char* str, int length = STR_BADINDEX) : m_ptr(nullptr) { assign(str, length); }
		String(char ch) : m_ptr(nullptr) { assign(&ch, 1); }
		~String() { setEmpty(); }

		operator const char* () const { return m_ptr != nullptr ? m_ptr : ""; }

		String& operator = (const String& str) { return assign(str); }
		String& operator = (const char* str) { return assign(str); }
		String& operator = (char ch) { return assign(&ch, 1); }

		String& operator += (const String& str) { return append(str); }
		String& operator += (const char* str) { return append(str); }
		String& operator += (char ch) { return append(&ch, 1); }

		String& operator << (const String& str) { return append(str); }
		String& operator << (const char* str) { return append(str); }
		String& operator << (char ch) { return append(&ch, 1); }

		char front() const { return getLength() == 0 ? 0 : m_ptr[0]; }
		char last() const { int l = getLength(); return l == 0 ? 0 : m_ptr[l - 1]; }
		char operator [] (int index) const { return m_ptr && index >= 0 && index < *(int*)(m_ptr - 4) ? m_ptr[index] : 0; }
		uint32_t getHashCode() const { return m_ptr ? *(int*)(m_ptr - 8) : 0; }
		RTX_STD_API String& setAt(int index, char ch);
		RTX_STD_API String setAt(int index, char ch) const;

		RTX_STD_API String& setEmpty();
		RTX_STD_API String& assign(const String& str);
		RTX_STD_API String& assign(const char* str, int length = -1);
		RTX_STD_API void allocate_new(int capacity);
		RTX_STD_API void setLength(int length);

		int getLength() const { return m_ptr ? *(int*)(m_ptr - AOFFSET_LEN) : 0; }
		bool isEmpty() const { return getLength() == 0; }
		RTX_STD_API String substring(int start, int count = STR_BADINDEX) const;
		RTX_STD_API int indexOf(const char* str, int start = 0) const;
		RTX_STD_API int indexOf(char ch, int start = 0) const;
		RTX_STD_API int indexOfAny(const char* str, int start = 0) const;
		RTX_STD_API int lastIndexOf(const char* str, int start = STR_BADINDEX) const;
		RTX_STD_API int lastIndexOf(char ch, int start = STR_BADINDEX) const;
		RTX_STD_API void split(StringList& arr, const char* delimeter) const;
		RTX_STD_API void split(StringList& arr, char delimeter) const;

		RTX_STD_API String& append(const String& str);
		RTX_STD_API String append(const String& str) const;
		RTX_STD_API String& append(const char* str, int length = STR_BADINDEX);
		RTX_STD_API String append(const char* str, int length = STR_BADINDEX) const;

		RTX_STD_API String& remove(int start, int count = 1);
		RTX_STD_API String remove(int start, int count = 1) const;
		RTX_STD_API String& insert(int index, const String& str);
		RTX_STD_API String insert(int index, const String& str) const;
		RTX_STD_API String& insert(int index, const char* str, int length = STR_BADINDEX);
		RTX_STD_API String insert(int index, const char* str, int length = STR_BADINDEX) const;
		RTX_STD_API String& insert(int index, char ch);
		RTX_STD_API String insert(int index, char ch) const;
		RTX_STD_API String& trim(const char* charSet = " \t\r\n");
		RTX_STD_API String trim(const char* charSet = " \t\r\n") const;
		RTX_STD_API String& toUpper();
		RTX_STD_API String toUpper() const;
		RTX_STD_API String& toLower();
		RTX_STD_API String toLower() const;
		RTX_STD_API String& replace(const char* toFind, const char* toReplace);
		RTX_STD_API String replace(const char* toFind, const char* toReplace) const;
		RTX_STD_API String& replace(char toFind, char toReplace);
		RTX_STD_API String replace(char toFind, char toReplace) const;

		static RTX_STD_API int compare(const String& left, const String& right, bool ignoreCase);
		static RTX_STD_API int compare(const String& left, const char* right, bool ignoreCase);
		static RTX_STD_API int compare(const char* left, const String& right, bool ignoreCase);
		static uint32_t calc_string_hash(const char* key, int len = -1);


		//--------------------------------------
		// операторы равенства
		//--------------------------------------
		friend inline bool operator == (const String& strLeft, const String& strRight)
		{
			return String::compare(strLeft, strRight, false) == 0;
		}
		friend inline bool operator == (const char* strLeft, const String& strRight)
		{
			return String::compare(strLeft, strRight, false) == 0;
		}
		friend inline bool operator == (const String& strLeft, const char* strRight)
		{
			return String::compare(strLeft, strRight, false) == 0;
		}
		//--------------------------------------
		// операторы равенства без учета регистра
		//--------------------------------------
		friend inline bool operator *= (const String& strLeft, const String& strRight)
		{
			return String::compare(strLeft, strRight, true) == 0;
		}
		friend inline bool operator *= (const char* strLeft, const String& strRight)
		{
			return String::compare(strLeft, strRight, true) == 0;
		}
		friend inline bool operator *= (const String& strLeft, const char* strRight)
		{
			return String::compare(strLeft, strRight, true) == 0;
		}
		//--------------------------------------
		// операторы неравенства
		//--------------------------------------
		friend inline bool	operator != (const String& strLeft, const String& strRight)
		{
			return String::compare(strLeft, strRight, false) != 0;
		}
		friend inline bool operator != (const char* strLeft, const String& strRight)
		{
			return String::compare(strLeft, strRight, false) != 0;
		}
		friend inline bool operator != (const String& strLeft, const char* strRight)
		{
			return String::compare(strLeft, strRight, false) != 0;
		}
		//--------------------------------------
		// операторы меньше-равно
		//--------------------------------------
		friend inline bool operator <= (const String& strLeft, const String& strRight)
		{
			return String::compare(strLeft, strRight, false) <= 0;
		}
		friend inline bool operator <= (const char* strLeft, const String& strRight)
		{
			return String::compare(strLeft, strRight, false) <= 0;
		}
		friend inline bool	operator <= (const String& strLeft, const char* strRight)
		{
			return String::compare(strLeft, strRight, false) <= 0;
		}
		//--------------------------------------
		// операторы меньше
		//--------------------------------------
		friend inline bool	operator < (const String& strLeft, const String& strRight)
		{
			return String::compare(strLeft, strRight, false) < 0;
		}
		friend inline bool operator < (const char* strLeft, const String& strRight)
		{
			return String::compare(strLeft, strRight, false) < 0;
		}
		friend inline bool operator < (const String& strLeft, const char* strRight)
		{
			return String::compare(strLeft, strRight, false) < 0;
		}
		//--------------------------------------
		// операторы больше-равно
		//--------------------------------------
		friend inline bool	operator >= (const String& strLeft, const String& strRight)
		{
			return String::compare(strLeft, strRight, false) >= 0;
		}
		friend inline bool	operator >= (const char* strLeft, const String& strRight)
		{
			return String::compare(strLeft, strRight, false) >= 0;
		}
		friend inline bool	operator >= (const String& strLeft, const char* strRight)
		{
			return String::compare(strLeft, strRight, false) >= 0;
		}
		//--------------------------------------
		// операторы больше
		//--------------------------------------
		friend inline bool	operator > (const String& strLeft, const String& strRight)
		{
			return String::compare(strLeft, strRight, false) > 0;
		}
		friend inline bool	operator > (const char* strLeft, const String& strRight)
		{
			return String::compare(strLeft, strRight, false) > 0;
		}
		friend inline bool	operator > (const String& strLeft, const char* strRight)
		{
			return String::compare(strLeft, strRight, false) > 0;
		}
		//--------------------------------------
		// операторы сложения
		//--------------------------------------
		friend inline String operator + (const String& strLeft, const String& strRight)
		{
			String result = strLeft;
			return result += strRight;
		}
		friend inline String operator + (const char* strLeft, const String& strRight)
		{
			String result = strLeft;
			return result += strRight;
		}
		friend inline String operator + (const String& strLeft, const char* strRight)
		{
			String result = strLeft;
			return result.append(strRight);
		}
		friend inline String operator + (const String& strLeft, char charRight)
		{
			String result = strLeft;
			return result.append(&charRight, 1);
		}
		friend inline String operator + (char charLeft, const String& strRight)
		{
			String result = charLeft;
			return result += strRight;
		}
		//--------------------------------------
		// операторы к
		//--------------------------------------
		friend inline String& operator << (String& stream, signed char value)
		{
			char tmp[32];
			std_snprintf(tmp, 32, "%d", (int32_t)value);
			stream << tmp;
			return stream;
		}
		friend inline String& operator << (String& stream, unsigned char value)
		{
			char tmp[32];
			std_snprintf(tmp, 32, "%u", (uint32_t)value);
			stream << tmp;
			return stream;
		}
		friend inline String& operator << (String& stream, short value)
		{
			char tmp[32];
			std_snprintf(tmp, 32, "%d", (int32_t)value);
			stream << tmp;
			return stream;
		}
		friend inline String& operator << (String& stream, unsigned short value)
		{
			char tmp[32];
			std_snprintf(tmp, 32, "%u", (uint32_t)value);
			stream << tmp;
			return stream;
		}
		friend inline String& operator << (String& stream, int value)
		{
			char tmp[32];
			std_snprintf(tmp, 32, "%d", value);
			stream << tmp;
			return stream;
		}
		friend inline String& operator << (String& stream, unsigned int value)
		{
			char tmp[32];
			std_snprintf(tmp, 32, "%u", value);
			stream << tmp;
			return stream;
		}
		friend inline String& operator << (String& stream, int64_t value)
		{
			char tmp[32];
			std_snprintf(tmp, 32, "%" PRId64, value);
			stream << tmp;
			return stream;
		}
		friend inline String& operator << (String& stream, uint64_t value)
		{
			char tmp[32];
			std_snprintf(tmp, 32, "%" PRIu64, value);
			stream << tmp;
			return stream;
		}
		//--------------------------------------
		// запись в память
		//--------------------------------------
		friend inline MemoryStream& operator << (MemoryStream& stream, const String& value)
		{
			stream.write((const char*)value, value.getLength()); return stream;
		}
		//--------------------------------------
		// константная пустая строка
		//--------------------------------------
		RTX_STD_API static const String empty;

	private:
		friend StringList;
		char* m_ptr;
	};
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API String utf8_to_locale(const String& str);
	RTX_STD_API int utf8_to_wcs(wchar_t* wcs, int size, const char* utf8, int length = -1);
	RTX_STD_API int wcs_to_utf8(char* utf8, int size, const wchar_t* wcs, int length = -1);
	RTX_STD_API String escape_rfc3986(const String& str);
	RTX_STD_API String unescape_rfc3986(const String& str);

	static inline String int_to_string(uint32_t v) { String s; s << v; return s; }
	static inline String int_to_string(int32_t v) { String s; s << v; return s; }
	static inline String int_to_string(uint64_t v) { String s; s << v; return s; }
	static inline String int_to_string(int64_t v) { String s; s << v; return s; }
	static inline String float_to_string(float f) { char t[128]; std_snprintf(t, 128, "%f", f); return String(t); }
	static inline String float_to_string(double d) { char t[128]; std_snprintf(t, 128, "%f", d); return String(t); }
	//--------------------------------------
	//
	//--------------------------------------
	class StringList
	{
		char** m_array;
		int m_size;
		int m_count;

		RTX_STD_API void init(int newSize = 0);
		void resize(int newSize);

	public:
		StringList(int capacity = 0) { init(capacity); }
		StringList(const StringList& arr) { init(); assign(arr); }
		StringList(const String* strings, int count) { init(); assign(strings, count); }
		StringList(const char** strings, int count) { init(); assign(strings, count); }
		~StringList() { clear(); }

		StringList& operator = (const StringList& arr) { return assign(arr); }
		const String* getBase() const { return (String*)m_array; }

		int getCount() const { return m_count; }

		const String& operator [] (int index) { return index >= 0 && index < m_count ? *(String*)&m_array[index] : String::empty; }
		const String& operator [] (int index) const { return  index >= 0 && index < m_count ? *(String*)&m_array[index] : String::empty; }

		const String& getFirst() { return m_count > 0 ? *(String*)&m_array[0] : String::empty; }
		const String& getLast() { return m_count > 0 ? *(String*)&m_array[m_count - 1] : String::empty; }

		const String& getAt(int index) { return  index >= 0 && index < m_count ? *(String*)&m_array[index] : String::empty; }
		const String& getAt(int index) const { return  index >= 0 && index < m_count ? *(String*)&m_array[index] : String::empty; }

		RTX_STD_API void setAt(int index, const String& str);
		RTX_STD_API void setAt(int index, const char* str);

		RTX_STD_API StringList& assign(const StringList& arr);
		RTX_STD_API StringList& assign(const String* pStrings, int count);
		RTX_STD_API StringList& assign(const char** pStrings, int count);

		RTX_STD_API void append(const StringList& arr);
		RTX_STD_API void append(const String* strings, int count);
		RTX_STD_API void append(const char** strings, int count);

		RTX_STD_API int add(const String& str);
		RTX_STD_API int add(const char* str);

		RTX_STD_API void insert(int index, const String& str);
		RTX_STD_API void insert(int index, const char* str);

		RTX_STD_API int indexOf(const String& str, bool ignoreCase = false) const;
		RTX_STD_API int indexOf(const char* str, bool ignoreCase = false) const;

		bool contains(const String& str, bool ignoreCase = false) const { return indexOf(str, ignoreCase) != STR_BADINDEX; }
		bool contains(const char* str, bool ignoreCase = false) const { return indexOf(str, ignoreCase) != STR_BADINDEX; }

		RTX_STD_API void removeAt(int index);
		RTX_STD_API void clear();
	};
}
//--------------------------------------
