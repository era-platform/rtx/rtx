﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "net/net_socket.h"

class socket_io_service_impl;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class RTX_NET_API socket_io_service_t
{
	socket_io_service_t(const socket_io_service_t&);
	socket_io_service_t& operator = (const socket_io_service_t&);

public:
	socket_io_service_t(rtl::Logger* log);
	virtual ~socket_io_service_t();

	bool create(uint32_t threads);
	void destroy();

	bool add(net_socket* sock, i_socket_event_handler* user);
	void remove(net_socket* sock);

	bool send_data_to(net_socket* sock, const uint8_t* data, uint32_t len, const socket_address* to);

private:
	socket_io_service_impl*	m_implementation;
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
typedef rtl::ArrayT<socket_io_service_t*> socket_io_service_list;
//------------------------------------------------------------------------------
