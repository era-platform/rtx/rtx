﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "net/ip_address.h"
//------------------------------------------------------------------------------
// Field								Length		Example
//------------------------------------- ----------- ----------------------------
// Version								4 bits		4
// Header length						4 bits		5
// Type of Service						8 bits		0
// Total length of the whole datagram	16 bits		45
// Identification						16 bits		43211
// Flags								3 bits		0
// Fragment Offset						13 bits		0
// Time to Live(a.k.a TTL)				8 bits		64
// Layer III Protocol					8 bits		6[TCP]
// Checksum								16 bits		0x3a43
// Source IP address					32 bits		192.168.1.1
// Destination IP address				32 bits		192.168.1.2
//------------------------------------------------------------------------------
class RTX_NET_API ip_packet
{
							ip_packet(const ip_packet&);
	ip_packet&				operator=(const ip_packet&);

public:
	static const uint32_t	FIXED_IP_HEADER_LENGTH = 20;
	static const uint32_t	BUFFER_SIZE = 4096;

							ip_packet();
	virtual					~ip_packet();

	// распаковка ip-пакета из буфера.
	bool					read_packet(const void* data, uint32_t length);
	// упаковка ip-пакета в буфер.
	uint32_t				write_packet(void* data, uint32_t size) const;

	// сброс всех полей.
	void					reset();

	uint8_t					get_header_length() const;
	void					set_header_length(uint8_t value);

	uint8_t					get_ip_version() const;
	void					set_ip_version(uint8_t value);

	uint8_t					get_type_of_service() const;
	void					set_type_of_service(uint8_t value);

	/*uint16_t				get_total_length() const;
	void					set_total_length(uint16_t value);*/

	uint16_t				get_identification() const;
	void					set_identification(uint16_t value);

	uint8_t					get_flags() const;
	void					set_flags(uint8_t value);

	uint16_t				get_fragment_offset() const;
	void					set_fragment_offset(uint16_t value);

	uint8_t					get_time_to_live() const;
	void					set_time_to_live(uint8_t value);

	uint8_t					get_protocol() const;
	void					set_protocol(uint8_t value);

	uint16_t				get_checksum() const;
	void					set_checksum(uint16_t value);

	const ip_address_t*		get_source_address() const;
	void					set_source_address(const ip_address_t* value);

	const ip_address_t*		get_destination_address() const;
	void					set_destination_address(const ip_address_t* value);

	const uint8_t*			get_buffer() const;
	uint32_t				get_buffer_length() const;
	uint32_t				set_buffer(const void* data, uint32_t length);

	static uint16_t			in_cksum(uint16_t* addr, int len);

private:
	uint8_t					m_buff[BUFFER_SIZE];
	uint32_t				m_buff_length;

	uint8_t					m_hl;		/* header length */
	uint8_t					m_v;		/* ip version */
	uint8_t					m_tos;		/* type of service */
	uint16_t				m_len;		/* total length */
	uint16_t				m_id;		/* identification */
	uint8_t					m_flags;	/* Flags */
	uint16_t				m_off;		/* fragment offset */
	uint8_t					m_ttl;		/* time to live */
	uint8_t					m_p;		/* protocol */
	uint16_t				m_sum;		/* checksum */
	ip_address_t			m_src;		/* source address */
	ip_address_t			m_dst;		/* dest address */
};
//--------------------------------------
