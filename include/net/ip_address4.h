﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#pragma once

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class ip_address4
{
	ip_address4(const ip_address4& ipaddr);
	ip_address4& operator=(const ip_address4&);
							
public:
	ip_address4() { m_native.s_addr = INADDR_ANY; strcpy(m_text, "0.0.0.0"); }
	ip_address4(const ip_address4* ipaddr) { copy_from(ipaddr); }
	ip_address4(const void* native) { set_native(native); }
	ip_address4(const char* str) { parse(str); }

	RTX_NET_API void copy_from(const ip_address4* ipaddr);
	RTX_NET_API void set_native(const void* native);
	RTX_NET_API void parse(const char* str);

	const void* get_native() const { return &m_native; }
	static uint32_t get_native_len() { return sizeof(in_addr); }

	RTX_NET_API const char* to_string() const;
	RTX_NET_API bool equals(const ip_address4* ip) const;

	const bool empty() const;
	const bool fill_suitable_local_mask(char* str, uint32_t len) const;

private:
	void first_init();
	void update_text();

private:
	in_addr m_native;
	char m_text[16];
};
//--------------------------------------
