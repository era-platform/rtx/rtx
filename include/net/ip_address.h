﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "net/ip_address4.h"
#include "net/ip_address6.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
enum e_ip_address_family
{
	E_IP_ADDRESS_FAMILY_IPV4,
	E_IP_ADDRESS_FAMILY_IPV6,
};

#define DEFAULT_IP_FAMILY E_IP_ADDRESS_FAMILY_IPV4
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class ip_address_t
{
	ip_address_t(const ip_address_t&);
	ip_address_t& operator=(const ip_address_t&);

public:
	ip_address_t() : m_family(DEFAULT_IP_FAMILY) { }
	ip_address_t(e_ip_address_family family) : m_family(family) { }
	ip_address_t(const ip_address_t* ipaddr) : m_family(DEFAULT_IP_FAMILY) { copy_from(ipaddr); }
	ip_address_t(const void* native, uint32_t size) : m_family(DEFAULT_IP_FAMILY) { set_native(native, size); }
	ip_address_t(const char* str) : m_family(DEFAULT_IP_FAMILY) { parse(str); }
	~ip_address_t() { }

	RTX_NET_API void copy_from(const ip_address_t* addr);
	RTX_NET_API void set_native(const void* native, uint32_t size);
	RTX_NET_API void parse(const char* str);

	e_ip_address_family get_family() const { return m_family; }
	const void* get_native() const { return (m_family == E_IP_ADDRESS_FAMILY_IPV4) ? m_ip4.get_native() : m_ip6.get_native(); }
	uint32_t get_native_len() const { return m_family == E_IP_ADDRESS_FAMILY_IPV4 ? ip_address4::get_native_len() : ip_address6::get_native_len(); }

	RTX_NET_API const char* to_string() const;
	RTX_NET_API bool equals(const ip_address_t* ip) const;

	RTX_NET_API const bool empty()const;

	RTX_NET_API const bool fill_suitable_local_mask(char* str, uint32_t len) const;

private:
	friend class socket_address;

	e_ip_address_family m_family;
	ip_address4 m_ip4;
	ip_address6 m_ip6;
};
//--------------------------------------
