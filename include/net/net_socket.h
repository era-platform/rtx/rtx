﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "net/socket_address.h"

#if defined (TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
    typedef int SOCKET;
#endif

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
enum class e_socket_shutdown_type
{
	E_SOCKET_SHUTDOWN_TYPE_RECEIVE,
    E_SOCKET_SHUTDOWN_TYPE_SEND,
    E_SOCKET_SHUTDOWN_TYPE_BOTH,
};
//------------------------------------------------------------------------------
enum class e_ip_protocol_type
{
	E_IP_PROTOCOL_TCP,
	E_IP_PROTOCOL_UDP,
	E_IP_PROTOCOL_RAW_UDP,
};
//------------------------------------------------------------------------------
//typedef std::vector<class net_socket*> socket_vector_t;
typedef rtl::ArrayT<class net_socket*> socket_vector_t;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class RTX_NET_API i_socket_event_handler
{
							i_socket_event_handler(const i_socket_event_handler&);
	i_socket_event_handler&	operator=(const i_socket_event_handler&);

public:
							i_socket_event_handler();
	virtual					~i_socket_event_handler();

	virtual void			socket_data_available(net_socket* sock) = 0;
	virtual void			socket_data_received(net_socket* sock, const uint8_t* data, uint32_t len, const socket_address* from) = 0;
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class RTX_NET_API net_socket
{
							net_socket(const net_socket&);
	net_socket&				operator=(const net_socket&);

public:

#if defined(TARGET_OS_WINDOWS)
	static const int	MAX_SELECT_COUNT = 64;
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	static const int	MAX_SELECT_COUNT = 64;
#endif
	static const int	PAYLOAD_BUFFER_SIZE = 4096;

							net_socket(rtl::Logger* log);
	virtual					~net_socket();

	SOCKET					get_native_handle() const;
	const socket_address*	get_local_address() const;
	const socket_address*	get_remote_address() const;

	int						get_back_log() const;
	void					set_back_log(int value);

	bool					set_option(int level, int name, const void *value, int size) { return ::setsockopt(m_socket, level, name, (const char*)value, size) == 0; }
	bool					get_option(int level, int name, void *value, sockaddrlen_t* size) { return ::getsockopt(m_socket, level, name, (char*)value, size) == 0; }

    bool					create(e_ip_address_family family, e_ip_protocol_type protocol);
        
	void					close();

	void					shutdown(e_socket_shutdown_type shutdown_type);

	bool					bind(const socket_address* local_addr);

	//bool					listen();

	//bool					connect(const socket_address* remote_addr);

	net_socket*				accept(rtl::Logger* log);

	uint32_t				send(const void* data, uint32_t len) const;
	uint32_t				recv(void* buffer, uint32_t size);

	uint32_t				send_to(const void* data, uint32_t len, const socket_address* remote_addr) const;
	uint32_t				recv_from(void* buffer, uint32_t size, socket_address* remote_addr);

	uint32_t				get_available_bytes_count();

	static bool				select(socket_vector_t* read, socket_vector_t* write, socket_vector_t* error, uint32_t timeout, rtl::Logger* log);

	// ��������� ���� ������������ ip-������ �������.
	bool					set_raw();

private:

#if defined(TARGET_OS_WINDOWS)

	static bool				select_win32(socket_vector_t* read, socket_vector_t* write, socket_vector_t* error, uint32_t timeout, rtl::Logger* log);
	static int				fill_fd_set(const socket_vector_t* vector, void* fdset);
	static void				fill_vector(const void* fdset, socket_vector_t* vector);

#elif defined(TARGET_OS_LINUX)
    
	static bool				epoll_linux(socket_vector_t* read, socket_vector_t* write, socket_vector_t* error, uint32_t timeout, rtl::Logger* log);
    static uint32_t			add_events(struct epoll_event* events, uint32_t count, socket_vector_t* sockets, int flag);

#elif defined(TARGET_OS_FREEBSD)

    static bool				kqueue_freebsd(socket_vector_t* read, socket_vector_t* write, socket_vector_t* error, uint32_t timeout, rtl::Logger* m_log);
    static uint32_t         add_events(struct kevent* events, uint32_t count, socket_vector_t* sockets, short filter);

#endif

private:
	rtl::Logger*					m_log;

	SOCKET					m_socket;
	socket_address			m_local_address;
	socket_address			m_remote_address;

	int						m_backlog;

	friend class socket_reader;
    friend class socket_io_service_impl;

	i_socket_event_handler*	m_event_handler;
	void*					m_user_tag;
    
#if defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
    
    class i_socket_event_handler*	m_iosvctag_user;
    struct netio_thread_context*	m_iosvctag_netioctx;
    
#endif
};
//------------------------------------------------------------------------------
