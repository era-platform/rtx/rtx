﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "net/ip_address4.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class RTX_NET_API socket_address4
{
						socket_address4(const socket_address4&);
	socket_address4&	operator=(const socket_address4&);

public:
						socket_address4();
						socket_address4(const socket_address4* saddr);
						socket_address4(const void* native);
						socket_address4(const ip_address4* ipaddr, uint16_t port);
	virtual				~socket_address4();

	//----------------------------------------------------------------------

	void				copy_from(const socket_address4* saddr);

	void				set_native(const void* native);

	void				set_ip_address_with_port(const ip_address4* ipaddr, uint16_t port);
	void				set_ip_address(const ip_address4* ipaddr);
	void				set_port(uint16_t port);

	//----------------------------------------------------------------------

	const void*			get_native() const;
	static uint32_t		get_native_len();

	const ip_address4*	get_ip_address() const;
	uint16_t			get_port() const;

	const char*			to_string() const;

	//----------------------------------------------------------------------

	bool				equals(const socket_address4* saddr);

private:
	void				first_init();
	void				fill_native();
	void				extract_native();
	void				update_text();

private:
	ip_address4			m_ip_address;
	uint16_t			m_port;

	sockaddr_in			m_native;

	//ip-адрес + ':' + порт + \0 
	char				m_text[16+1+5+1];
};
//------------------------------------------------------------------------------
