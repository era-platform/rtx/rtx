﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#pragma once

//----------------------------------------------------------------------
//
//----------------------------------------------------------------------
class RTX_NET_API ip_address6
{
							ip_address6(const ip_address6& ipaddr);
	ip_address6&			operator=(const ip_address6&);

public:
							ip_address6();
							ip_address6(const ip_address6* ipaddr);
							ip_address6(const void* native);
							ip_address6(const char* str);
	virtual					~ip_address6();

	void					copy_from(const ip_address6* ipaddr);
	void					set_native(const void* native);
	void					parse(const char* str);

	const void*				get_native() const;
	static uint32_t			get_native_len();

	const char*				to_string() const;

	bool					equals(const ip_address6* ip) const;

	const bool				empty() const;
	const bool				fill_suitable_local_mask(char* str, uint32_t len) const;
private:
	void					first_init();
	void					update_text();
	void					copy_internal(const ip_address6* ipaddr);

#if defined(TARGET_OS_WINDOWS)
	bool					is_any() const;
	bool					is_loopback() const;
	bool					is_ipv4_compatible() const;
	bool					is_ipv4_mapped() const;
#endif
		
private:
	in6_addr				m_native;

	char					m_text[INET6_ADDRSTRLEN+1]; 
};
//--------------------------------------
