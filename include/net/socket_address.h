﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "net/ip_address.h"
#include "net/socket_address4.h"
#include "net/socket_address6.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class RTX_NET_API socket_address
{
						socket_address(const socket_address&);
	socket_address&		operator=(const socket_address&);

public:
	static const uint32_t MAX_NATIVE_SOCKADDR_SIZE = MAX(sizeof(sockaddr_in6), sizeof(sockaddr_in));

						socket_address();
						socket_address(e_ip_address_family family);
						socket_address(const socket_address* saddr);
						socket_address(const void* native_addr, uint32_t size);
						socket_address(const ip_address_t* ipaddr, uint16_t port);
	virtual				~socket_address();

	//----------------------------------------------------------------------

	void				copy_from(const socket_address* saddr);

	void				set_native(const void* native, uint32_t size);

	void				set_ip_address_with_port(const ip_address_t* ipaddr, uint16_t port);
	void				set_ip_address(const ip_address_t* ipaddr);
	void				set_port(uint16_t port);

	//----------------------------------------------------------------------

	const void*			get_native() const;
	uint32_t			get_native_len() const;

	const ip_address_t*	get_ip_address() const;
	uint16_t			get_port() const;

	const char*			to_string() const;

	//----------------------------------------------------------------------

	bool				equals(const socket_address* saddr);

private:
	ip_address_t			m_ip_address;
	socket_address4		m_saddr4;
	socket_address6		m_saddr6;
};

//------------------------------------------------------------------------------
