﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

//------------------------------------------------------------------------------
// Field                           Length          Example
//-------------------------------- --------------- -----------------------------
// Source Port                     16 bits         12831
// Destination Port                16 bits	       53
// UDP datagram length             16 bits         321
// UDP checksum                    16 bits         0xeb8a
//------------------------------------------------------------------------------
class RTX_NET_API udp_packet
{
							udp_packet(const udp_packet&);
	udp_packet&				operator=(const udp_packet&);

public:
	static const uint32_t	FIXED_UDP_HEADER_LENGTH = 8;
	static const uint32_t	BUFFER_SIZE = 4096;

							udp_packet();
	virtual					~udp_packet();

	// распаковка udp из буфера.
	bool					read(const void* data, uint32_t length);
	// упаковка udp в буфер.
	uint32_t				write(void* data, uint32_t size) const;

	// сброс всех полей.
	void					reset();

	uint16_t				get_source_port() const;
	void					set_source_port(uint16_t value);

	uint16_t				get_destination_port() const;
	void					set_destination_port(uint16_t value);

	uint16_t				get_udp_length() const;

	uint16_t				in_cksum_udp(int src, int dst);

	const uint8_t*			get_buffer() const;
	uint32_t				get_buffer_length() const;
	uint32_t				set_buffer(const void* data, uint32_t length);

private:
	uint8_t					m_buff[BUFFER_SIZE];
	uint32_t				m_buff_length;

	uint16_t				m_sport;               /* source port */
	uint16_t				m_dport;               /* destination port */
	uint16_t				m_ulen;                /* udp length */
	uint16_t				m_sum;                 /* udp checksum */
};
//------------------------------------------------------------------------------
