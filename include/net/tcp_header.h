﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class RTX_NET_API tcp_header
{
							tcp_header(const tcp_header&);
	tcp_header&				operator=(const tcp_header&);

public:
	static const uint32_t	FIXED_TCP_HEADER_LENGTH = 12;

							tcp_header();
	virtual					~tcp_header();

	// распаковка TCP из буфера.
	bool					read(const void* data, uint32_t length);
	// упаковка TCP в буфер.
	uint32_t				write(void* data, uint32_t size) const;

	// сброс всех полей.
	void					reset();


private:
	
};
//------------------------------------------------------------------------------
