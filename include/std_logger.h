﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "std_file_stream.h"
#include "std_date_time.h"

 //-------------------------------------
 // ключи для логирования
 //-------------------------------------
#define CONF_LOG_ROOT_PATH	"log-root-path"
#define CONF_LOG_TRACE		"log-trace"
#define CONF_LOG_MAX_SIZE	"log-max-size"
#define CONF_LOG_PART_SIZE	"log-part-size"
#define CONF_LOG_PART_COUNT	"log-part-count"
//-------------------------------------
//
//-------------------------------------
#define LOG_REWRITE			0x00000000
#define LOG_APPEND			0x00000001
#define LOG_CREATE_TIMEDIR	0x00000002
#define LOG_LOCAL			0x00000004
//-------------------------------------
// константы с длинами
//-------------------------------------
#define LOG_MAXNAME			16
#define	LOG_BUFFER_SIZE		32768
#define LOG_BUFFER_LEN		32767
#define LOG_BUFFER_END		32766

 //--------------------------------------
 // типы рассировки -- новые флаги "NET IP SESSION INFO CALL MEDIA STREAM MAIN TRUNK TRANSACTION PROTO"
 //--------------------------------------
enum TRACE_FLAGS
{
	TRF_CALL = 0x00000001,		// 1 функции
	TRF_EVENTS = 0x00000002,		// 2 события
	TRF_MEDIA_FLOW = 0x00000004,		// 3 голосовой трафик
	TRF_NET = 0x00000008,		// 4
	TRF_VAD = 0x00000010,		// 5
	TRF_PROTO = 0x00000020,		// 6
	TRF_TIMER = 0x00000040,		// 7
	TRF_SESSION = 0x00000080,		// 8
	TRF_TRANS = 0x00000100,		// 9
	TRF_TRUNK = 0x00000200,		// 10
	TRF_STREAM = 0x00000400,		// 11
	TRF_WARNING = 0x00000800,		// 12
	TRF_ERRORS = 0x00001000,		// 13
	TRF_FLAG1 = 0x00002000,		// 14
	TRF_FLAG2 = 0x00004000,		// 15
	TRF_FLAG3 = 0x00008000,		// 16
	TRF_FLAG4 = 0x00010000,		// 17
	TRF_FLAG5 = 0x00020000,		// 18
	TRF_FLAG6 = 0x00040000,		// 19
	TRF_FLAG7 = 0x00080000,		// 20
	TRF_MIXER = 0x00100000,		// 21
	TRF_MQ = 0x00200000,		// 22
	TRF_FLAG10 = 0x00400000,		// 23
	TRF_STAT = 0x00800000,		// 24
	TRF_FLAG12 = 0x01000000,		// 25
	TRF_FLAG13 = 0x02000000,		// 26
	TRF_FLAG14 = 0x04000000,		// 27
	TRF_RTP_FLOW = 0x08000000,		// 28
	TRF_BANNED = 0x10000000,		// 29
	TRF_RTP = 0x20000000,		// 30
	TRF_ASYNC = 0x40000000,		// 31
	TRF_FAX = 0x80000000,		// 32

	TRF_STREAM_CALL = TRF_STREAM | TRF_CALL,
	TRF_STREAM_EVENT = TRF_STREAM | TRF_EVENTS,
	TRF_STREAM_MFLOW = TRF_STREAM | TRF_MEDIA_FLOW,
	TRF_STREAM_NET = TRF_STREAM | TRF_NET,
	TRF_STREAM_WARN = TRF_STREAM | TRF_WARNING,
	TRF_STREAM_ERROR = TRF_STREAM | TRF_ERRORS,
	TRF_STREAM_FAX = TRF_STREAM | TRF_FAX,
	TRF_STREAM_RTP = TRF_STREAM | TRF_RTP,
	TRF_STREAM_RFLOW = TRF_STREAM | TRF_RTP_FLOW,

	TRF_TRUNK_CALL = TRF_TRUNK | TRF_CALL,
	TRF_TRUNK_EVENT = TRF_TRUNK | TRF_EVENTS,
	TRF_TRUNK_VOICE = TRF_TRUNK | TRF_MEDIA_FLOW,
	TRF_TRUNK_NET = TRF_TRUNK | TRF_NET,
	TRF_TRUNK_WARN = TRF_TRUNK | TRF_WARNING,
	TRF_TRUNK_ERROR = TRF_TRUNK | TRF_ERRORS,

	TRF_ALL = 0xFFFFFFFF,
};
#ifndef MAX_PATH
#define MAX_PATH 260
#endif


namespace rtl
{
	//-------------------------------------
	// логирование информации в файл в кодировке UTF-8
	//-------------------------------------
	class Logger
	{
		Mutex m_sync;
		FileStream m_out;
		DateTime m_open_time;
		volatile bool m_created;
		volatile bool m_clean;
		uint32_t m_flags;
		char m_folder[MAX_PATH];
		char m_log_name[LOG_MAXNAME];

		char* m_utf8;
		char* m_buffer;							// LOG_BUFFER_SIZE
		volatile size_t m_buffer_length;		// char
		volatile size_t m_log_written;			// byte
		volatile int m_part_number;

		static uint64_t s_log_max_size;			// максимальный размер файла за день
		static uint64_t s_log_part_size;		// размер частей для деления файла. 0 - не делить файл на части
		static uint64_t s_log_part_count;		// количество частей (если заполнены все части, то запись продолжается в первый лог). 0 - не зацикливать запись.

		RTX_STD_API static uint32_t g_trace_flags;

	private:
		Logger(const Logger& log);
		Logger& operator = (const Logger& log);

		void write_buffer();
		void write_format(const char* tag, const char* type, const char* format, va_list argptr);
		void write_format_message(const char* tag, const char* type, int lastError, const char* format, va_list argptr);
		void write_binary(const char* tag, const uint8_t* data, int data_len, const char* format, va_list argptr);

		bool open_file();
		void close_file();
		static bool get_existing_filename(const char* fmask, int* fpart, uint64_t* filesize, char* fname_buffer, int size);
		void generate_file_name(char* fname, int size);
		bool check_and_reopen_file(uint64_t file_size);

	public:
		void check_log_expired_day(const DateTime& current_time);

	public:
		RTX_STD_API Logger();
		RTX_STD_API ~Logger();

		RTX_STD_API void create(const char* folder, const char* logName, uint32_t flags = 0);
		RTX_STD_API void destroy();

		RTX_STD_API void log_error(const char* tag, const char* format, ...);
		RTX_STD_API void log_error_message(const char* tag, int last_error, const char* format, ...);
		RTX_STD_API void log_error_formatted(const char* tag, const char* message, const char* format, va_list argptr);
		RTX_STD_API void log(const char* tag, const char* format, ...);
		RTX_STD_API void log_event(const char* tag, const char* format, ...);
		RTX_STD_API void log_warning(const char* tag, const char* format, ...);
		RTX_STD_API void log_binary(const char* tag, const uint8_t* data, int datalen, const char* format, ...);
		RTX_STD_API void log_format(const char* tag, const char* format, va_list argptr);

		const char* get_folder() { return m_folder; }
		const char* getName() { return m_log_name; }
		bool is_created() { return m_created; }

		RTX_STD_API static void set_log_sizes(uint64_t max_log_size, uint64_t max_part_size, uint64_t max_part_count);
		RTX_STD_API static int get_log_max_size();
		RTX_STD_API static int get_log_part_size();
		RTX_STD_API static int get_log_part_count();

		RTX_STD_API static uint32_t set_trace_flags(const char* str_flags);
		static inline uint32_t get_trace_flags() { return g_trace_flags; }
		static inline uint32_t set_trace_flags(uint32_t flags) { return g_trace_flags = flags; }
		static inline uint32_t add_trace_flags(uint32_t flags) { return g_trace_flags | flags; }
		static inline uint32_t remove_trace_flags(uint32_t flags) { return g_trace_flags & (~flags); }
		RTX_STD_API static const char* trace_to_string(char* buffer, int size, bool bits);
		RTX_STD_API static bool check_trace(uint32_t flags);
#ifdef _DEBUG
		static inline bool is_debug() { return true; }
#else
		static inline bool is_debug() { return false; }
#endif
		static inline bool check_trace_or(uint32_t flags) { return (g_trace_flags & flags) != 0; }
	};
}
	//--------------------------------------
	// общий лог доступный всей системе
	//--------------------------------------
	extern RTX_STD_API rtl::Logger Log;
	extern RTX_STD_API rtl::Logger Err;
	//--------------------------------------
	// макросы для логирования информации
	//--------------------------------------
#define ERR_E			Err.log_error
#define ERR_ERR_MSG		Log.log_error_message
#define ERR_L			Err.log
#define ERR_W			Err.log_warning

#define LOG_CALL		if (rtl::Logger::check_trace(TRF_CALL))		Log.log
#define LOG_BINARY		if (rtl::Logger::check_trace(TRF_CALL))		Log.log_binary
#define LOG_EVENT		if (rtl::Logger::check_trace(TRF_EVENTS))		Log.log_event
#define LOG_MEDIA_FLOW	if (rtl::Logger::check_trace(TRF_MEDIA_FLOW))	Log.log
#define LOG_NET			if (rtl::Logger::check_trace(TRF_NET))		Log.log
#define LOG_NET_EVENT	if (rtl::Logger::check_trace(TRF_NET | TRF_EVENTS)) Log.log_event
#define LOG_NET_WARN	if (rtl::Logger::check_trace_or(TRF_NET | TRF_WARNING)) Log.log_warning
#define LOG_NET_ERROR	if (rtl::Logger::check_trace_or(TRF_NET | TRF_ERRORS)) Log.log_error
#define LOG_TIMER		if (rtl::Logger::check_trace(TRF_TIMER))		Log.log
#define LOG_SESSION		if (rtl::Logger::check_trace(TRF_SESSION))	Log.log
#define LOG_TRANS		if (rtl::Logger::check_trace(TRF_TRANS))		Log.log
#define LOG_WARN		/*if (rtl::Logger::check_trace(TRF_WARNING))*/	Log.log_warning
#define LOG_ERROR		/*if (rtl::Logger::check_trace(TRF_ERRORS))*/	Log.log_error
#define LOG_ERR_MSG		/*if (rtl::Logger::check_trace(TRF_ERRORS))*/	Log.log_error_message
#define LOG_STREAM		if (rtl::Logger::check_trace(TRF_STREAM))	Log.log
#define LOG_TRUNK		if (rtl::Logger::check_trace(TRF_TRUNK))	Log.log
#define LOG_FAX			if (rtl::Logger::check_trace(TRF_FAX))		Log.log
#define LOG_RTP			if (rtl::Logger::check_trace(TRF_RTP))		Log.log
#define LOG_RTP_FLOW	if (rtl::Logger::check_trace(TRF_RTP_FLOW))	Log.log
#define LOG_ASYNC		if (rtl::Logger::check_trace(TRF_ASYNC))	Log.log
#define LOG_FLAG1		if (rtl::Logger::check_trace(TRF_FLAG1))	Log.log
#define LOG_FLAG2		if (rtl::Logger::check_trace(TRF_FLAG2))	Log.log
#define LOG_FLAG3		if (rtl::Logger::check_trace(TRF_FLAG3))	Log.log
#define LOG_FLAG4		if (rtl::Logger::check_trace(TRF_FLAG4))	Log.log
#define LOG_FLAG5		if (rtl::Logger::check_trace(TRF_FLAG5))	Log.log
#define LOG_FLAG6		if (rtl::Logger::check_trace(TRF_FLAG6))	Log.log
#define LOG_FLAG7		if (rtl::Logger::check_trace(TRF_FLAG7))	Log.log
#define LOG_MIXER		if (rtl::Logger::check_trace(TRF_MIXER))	Log.log
#define LOG_MQ			if (rtl::Logger::check_trace(TRF_MQ))		Log.log

#define MLOG_CALL		if (rtl::Logger::check_trace(TRF_CALL))		m_log.log
#define MLOG_BINARY		if (rtl::Logger::check_trace(TRF_CALL))		m_log.log_binary
#define MLOG_EVENT		if (rtl::Logger::check_trace(TRF_EVENTS))	m_log.log_event
#define MLOG_MEDIA_FLOW	if (rtl::Logger::check_trace(TRF_MEDIA_FLOW))	m_log.log
#define MLOG_NET		if (rtl::Logger::check_trace(TRF_NET))		m_log.log
#define MLOG_WARN		/*if (rtl::Logger::check_trace(TRF_WARNING))*/	m_log.log_warning
#define MLOG_ERROR		/*if (rtl::Logger::check_trace(TRF_ERRORS))*/	m_log.log_error
#define MLOG_ERR_MSG	/*if (rtl::Logger::check_trace(TRF_ERRORS))*/	m_log.log_error_message
#define MLOG_TIMER		if (rtl::Logger::check_trace(TRF_TIMER))	m_log.log
#define MLOG_FAX		if (rtl::Logger::check_trace(TRF_FAX))		m_log.log
#define MLOG_RTP		if (rtl::Logger::check_trace(TRF_RTP))		m_log.log
#define MLOG_RTP_FLOW	if (rtl::Logger::check_trace(TRF_RTP_FLOW))	m_log.log
#define MLOG_FLAG1		if (rtl::Logger::check_trace(TRF_FLAG1))	m_log.log
#define MLOG_FLAG2		if (rtl::Logger::check_trace(TRF_FLAG2))	m_log.log
#define MLOG_FLAG3		if (rtl::Logger::check_trace(TRF_FLAG3))	m_log.log
#define MLOG_FLAG4		if (rtl::Logger::check_trace(TRF_FLAG4))	m_log.log
#define MLOG_FLAG5		if (rtl::Logger::check_trace(TRF_FLAG5))	m_log.log
#define MLOG_FLAG6		if (rtl::Logger::check_trace(TRF_FLAG6))	m_log.log
#define MLOG_FLAG7		if (rtl::Logger::check_trace(TRF_FLAG7))	m_log.log
#define MLOG_MIXER		if (rtl::Logger::check_trace(TRF_MIXER))	m_log.log
#define MLOG_MQ			if (rtl::Logger::check_trace(TRF_MQ))	m_log.log

#define PLOG_CALL		if (m_log && rtl::Logger::check_trace(TRF_CALL)) m_log->log
#define PLOG_DEBUG		if (m_log && rtl::Logger::is_debug()) m_log->log
#define PLOG_WRITE		if (m_log && rtl::Logger::check_trace(TRF_CALL)) m_log->log
#define PLOG_EVENT		if (m_log && rtl::Logger::check_trace(TRF_EVENTS)) m_log->log_event
#define PLOG_WARNING	rtl::Logger* log = m_log != nullptr ? m_log : &Log; log->log_warning
#define PLOG_ERROR		rtl::Logger* log = m_log != nullptr ? m_log : &Log; log->log_error
#define PLOG_ERR_FMT	rtl::Logger* log = m_log != nullptr ? m_log : &Log; log->log_error_message
#define PLOG_FAX		if (m_log && rtl::Logger::check_trace(TRF_FAX)) m_log->log
#define PLOG_NET		if (m_log && rtl::Logger::check_trace(TRF_NET)) m_log->log
#define PLOG_RTP		if (m_log && rtl::Logger::check_trace(TRF_RTP)) m_log->log
#define PLOG_RTP_FLOW	if (m_log && rtl::Logger::check_trace(TRF_RTP_FLOW)) m_log->log
#define PLOG_FLAG1		if (m_log && rtl::Logger::check_trace(TRF_FLAG1))	m_log->log
#define PLOG_FLAG2		if (m_log && rtl::Logger::check_trace(TRF_FLAG2))	m_log->log
#define PLOG_FLAG3		if (m_log && rtl::Logger::check_trace(TRF_FLAG3))	m_log->log
#define PLOG_FLAG4		if (m_log && rtl::Logger::check_trace(TRF_FLAG4))	m_log->log
#define PLOG_FLAG5		if (m_log && rtl::Logger::check_trace(TRF_FLAG5))	m_log->log
#define PLOG_FLAG6		if (m_log && rtl::Logger::check_trace(TRF_FLAG6))	m_log->log
#define PLOG_FLAG7		if (m_log && rtl::Logger::check_trace(TRF_FLAG7))	m_log->log
#define PLOG_MIXER		if (m_log && rtl::Logger::check_trace(TRF_MIXER))	m_log->log
#define PLOG_MQ		if (m_log && rtl::Logger::check_trace(TRF_MQ))	m_log->log


#define PLOG_NET_WRITE		if (m_log && rtl::Logger::check_trace(TRF_NET)) m_log->log
#define PLOG_NET_ERROR		if (m_log && rtl::Logger::check_trace_or(TRF_NET | TRF_ERRORS)) m_log->log_error
#define PLOG_NET_WARNING	if (m_log && rtl::Logger::check_trace_or(TRF_NET | TRF_WARNING)) m_log->log_warning
#define PLOG_NET_BIN		if (m_log && rtl::Logger::check_trace_or(TRF_NET)) m_log->log_binary

#define PLOG_MEDIA_WRITE	if (m_log && rtl::Logger::check_trace(TRF_MEDIA_FLOW)) m_log->log
#define PLOG_MEDIA_ERROR	if (m_log && rtl::Logger::check_trace_or(TRF_MEDIA_FLOW | TRF_ERRORS)) m_log->log_error
#define PLOG_MEDIA_WARNING	if (m_log && rtl::Logger::check_trace_or(TRF_MEDIA_FLOW | TRF_WARNING)) m_log->log_warning

#define PLOG_RTP_WRITE		if (m_log && rtl::Logger::check_trace(TRF_RTP)) m_log->log
#define PLOG_RTP_ERROR		if (m_log && rtl::Logger::check_trace_or(TRF_RTP| TRF_ERRORS)) m_log->log_error
#define PLOG_RTP_WARNING	if (m_log && rtl::Logger::check_trace_or(TRF_RTP| TRF_WARNING)) m_log->log_warning

//--------------------------------------
