/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

namespace media
{
	//-----------------------------------------------
	// *.fnt file format structures
	//-----------------------------------------------

#define FNT_TYPE_VECTOR			0x0001	/* If set a vector FNT, else raster (we only parse rasters) */
#define FNT_TYPE_MEMORY			0x0004	/* If set font is in ROM */
#define FNT_TYPE_DEVICE			0x0080	/* If set font is "realized by a device" whatever that means */

#define FNT_PITCH_VARIABLE		0x01	/* Variable width font */
#define FNT_FAMILY_MASK			0xf0
#define FNT_FAMILY_DONTCARE		0x00
#define FNT_FAMILY_SERIF		0x10
#define FNT_FAMILY_SANSSERIF	0x20
#define FNT_FAMILY_FIXED		0x30
#define FNT_FAMILY_SCRIPT		0x40
#define FNT_FAMILY_DECORATIVE	0x50

#define FNT_FLAGS_FIXED			0x0001
#define FNT_FLAGS_PROPORTIONAL	0x0002
#define FNT_FLAGS_ABCFIXED		0x0004
#define FNT_FLAGS_ABCPROP		0x0008
#define FNT_FLAGS_1COLOR		0x0010
#define FNT_FLAGS_16COLOR		0x0020
#define FNT_FLAGS_256COLOR		0x0040
#define FNT_FLAGS_RGBCOLOR		0x0080

#define FNT_MAX_GLYPHS 258
#define FNT_HEADER_v2_LEN 118
#define FNT_HEADER_v3_LEN 147

#pragma pack(push, 1)
	//-----------------------------------------------
	// File header
	//-----------------------------------------------
	struct FontHeader
	{
		uint16_t dfVersion;					// x0000	0
		uint32_t dfSize;					// x0002	2
		char dfCopyright[60];				// x0006	6
		uint16_t dfType;					// x0042	66
		uint16_t dfPoints;					// x0044	68
		uint16_t dfVertRes;					// x0046	70
		uint16_t dfHorizRes;				// x0048	72
		uint16_t dfAscent;					// x004A	74
		uint16_t dfInternalLeading;			// x004C	76
		uint16_t dfExternalLeading;			// x004E	78
		uint8_t dfItalic;					// x0050	80
		uint8_t dfUnderline;				// x0051	81
		uint8_t dfStrikeOut;				// x0052	82
		uint16_t dfWeight;					// x0053	83
		uint8_t dfCharSet;					// x0055	85
		uint16_t dfPixWidth;				// x0056	86
		uint16_t dfPixHeight;				// x0058	88
		uint8_t dfPitchAndFamily;			// x005A	90
		uint16_t dfAvgWidth;				// x005B	91
		uint16_t dfMaxWidth;				// x005D	93
		uint8_t dfFirstChar;				// x005F	95
		uint8_t dfLastChar;					// x0060	96
		uint8_t dfDefaultChar;				// x0061	97
		uint8_t dfBreakChar;				// x0062	98
		uint16_t dfWidthBytes;				// x0063	99
		uint32_t dfDevice;					// x0065	101
		uint32_t dfFace;					// x0069	105
		uint32_t dfBitsPointer;				// x006D	109
		uint32_t dfBitsOffset;				// x0071	113
		uint8_t dfReserved;					// x0075	117
		// v2 end x0076 118
		// v3
		uint32_t dfFlags;					// x0076	118
		uint16_t dfAspace;					// x007A	122
		uint16_t dfBspace;					// x007C	124
		uint16_t dfCspace;					// x007E	126
		uint32_t dfColorPointer;			// x0080	128
		uint8_t dfReserved3[16];			// x0084	132
	}; // x0094 148

#pragma pack(pop)

	//-----------------------------------------------
	// CharacterInfo (v3 + bitmap ref)
	//-----------------------------------------------
	struct GlyphEntryV2
	{
		uint16_t chWidth;
		uint16_t chOffset;
	};
	struct GlyphEntryV3
	{
		uint16_t chWidth;
		uint32_t chOffset;
	};

	struct GlyphInfo
	{
		uint16_t width;
		uint16_t height;
		int rowWidth;
		const uint8_t* bitstream;
	};

	class FontInfo
	{
		FontHeader* m_header;
		const char* m_faceName;
		const char* m_deviceName;
		int m_glyphCount;
		GlyphInfo m_glyphes[FNT_MAX_GLYPHS];
		int m_dataLength;
		uint8_t* m_data;

	public:
		FontInfo();
		~FontInfo();

		bool load(rtl::Stream* fnt);

		const FontHeader* getHeader() const { return m_header; }
		const char* getDeviceName() const { return m_deviceName; }
		const char* getFaceName() const { return m_faceName; }
		int getFirstChar() const { return m_header != nullptr ? m_header->dfFirstChar : 0; }
		int GetLastChar() const { return m_header != nullptr ? m_header->dfLastChar : 0; }
		int GetDefaultChar() const { return m_header != nullptr ? m_header->dfDefaultChar : 0; }
		int getGlyphCount() const { return m_glyphCount; }
		const GlyphInfo& getGlyph(int ch) const { return m_glyphes[ch]; }
	};

	class FontFile
	{
		rtl::String m_fileName;
		rtl::ArrayT<FontInfo*> m_fontList;

	public:
		FontFile() { }
		~FontFile() { free(); }

		
		RTX_MMT_API bool load2(const char* filepath);
		RTX_MMT_API void free();

		RTX_MMT_API const char* getFaceName() const;
		RTX_MMT_API const char* getDeviceName() const;

		int getFontCount() const { return m_fontList.getCount(); }
		const FontInfo* getFontAt(int index) const { return m_fontList[index]; }
		RTX_MMT_API const FontInfo* getFontByHeight(int height) const;
	private:
		RTX_MMT_API bool load(const char* filepath);
	};

	/*
		The Windows 2.x version of dfCharTable has a GlyphEntry structure with the following format:
		GlyphEntry    struc
			geWidth     dw ? ;width of character bitmap in pixels
			geOffset    dw ? ;pointer to the bits
		GlyphEntry    ends

		The Windows 3.00 version of the dfCharTable is dependent on the format of the Glyph bitmap.
		Note: The only formats supported in Windows 3.00 will be DFF_FIXED and DFF_PROPORTIONAL.
			DFF_FIXED
			DFF_PROPORTIONAL

			GlyphEntry    struc
				geWidth     dw ? ;width of character bitmap in pixels
				geOffset    dd ? ;pointer to the bits
			GlyphEntry    ends

			DFF_ABCFIXED
			DFF_ABCPROPORTIONAL

			GlyphEntry    struc
				geWidth     dw ? ;width of character bitmap in pixels
				geOffset    dd ? ;pointer to the bits
				geAspace    dd ? ;A space in fractional pixels (16.16)
				geBspace    dd ? ;B space in fractional pixels (16.16)
				geCspace    dw ? ;C space in fractional pixels (16.16)
			GlyphEntry    ends

		The fractional pixels are expressed as a 32-bit signed number with an implicit binary point between bits 15 and 16. This is referred to as a 16.16 ("sixteen dot sixteen") fixed-point number.
		The ABC spacing here is the same as that defined above. However, here there are specific sets for each character.
			DFF_1COLOR
			DFF_16COLOR
			DFF_256COLOR
			DFF_RGBCOLOR
			GlyphEntry    struc
			  geWidth     dw ? ;width of character bitmap in pixels
			  geOffset    dd ? ;pointer to the bits
			  geHeight    dw ? ;height of character bitmap in pixels
			  geAspace    dd ? ;A space in fractional pixels (16.16)
			  geBspace    dd ? ;B space in fractional pixels (16.16)
			  geCspace    dd ? ;C space in fractional pixels (16.16)
			GlyphEntry    ends
			DFF_1COLOR means 8 pixels per byte
			DFF_16COLOR means 2 pixels per byte
			DFF_256COLOR means 1 pixel per byte
			DFF_RGBCOLOR means RGBquads
	*/
}
//-----------------------------------------------

