/* RTX H.248 Media Gate. Codec Interface Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_codec.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
struct mg_audio_decoder_t
{
	virtual const char* getEncoding() = 0;
	virtual uint32_t depacketize(const rtp_packet* packet, uint8_t* buff, uint32_t buff_size) = 0;
	virtual uint32_t decode(const uint8_t* buff_from, uint32_t size_from, uint8_t* buff_to, uint32_t size_to) = 0;
	// �������� ������ ������ � ������� �������� ������� � ������.
	virtual uint32_t get_frame_size_pcm() = 0;
	virtual uint32_t get_frame_size_cod() = 0;
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
struct mg_audio_encoder_t
{
	virtual const char* getEncoding() = 0;
	virtual uint32_t encode(const uint8_t* buff_from, uint32_t size_from, uint8_t* buff_to, uint32_t size_to) = 0;
	virtual uint32_t packetize(rtp_packet* packet, const uint8_t* buff, uint32_t buff_size) = 0;
	virtual uint32_t get_frame_size() = 0; // �������� ������ ������ � ������� �������� ������� � ������.
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_CODEC_API mg_audio_decoder_t* rtx_codec__create_audio_decoder(const char* encoding, const media::PayloadFormat* format);
RTX_CODEC_API void rtx_codec__release_audio_decoder(mg_audio_decoder_t* decoder);
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_CODEC_API mg_audio_encoder_t* rtx_codec__create_audio_encoder(const char* encoding, const media::PayloadFormat* format);
RTX_CODEC_API void rtx_codec__release_audio_encoder(mg_audio_encoder_t* encoder);
