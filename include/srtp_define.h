﻿/* RTX H.248 Media Gate RTP Lib
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
///--------------------------------------
// An srtp_profile_t enumeration is used to identify a particular SRTP
// profile (that is, a set of algorithms and parameters).  These
// profiles are defined in the DTLS-SRTP draft.
//--------------------------------------
enum srtp_profile_t
{
	srtp_profile_reserved           = 0,
	srtp_profile_aes128_cm_sha1_80  = 1,
	srtp_profile_aes128_cm_sha1_32  = 2,
	srtp_profile_f8128_sha1_80      = 3,
	srtp_profile_null_sha1_80       = 5,
	srtp_profile_null_sha1_32       = 6,
};
