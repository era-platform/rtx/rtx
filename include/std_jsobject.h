/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "std_json.h"
#include <math.h>

namespace rtl {
	//-----------------------------------------------
	//
	//-----------------------------------------------
	class JObject;
	class JArray;
	//-----------------------------------------------
	//
	//-----------------------------------------------
	class JValue
	{
		JsonTypes m_type;
		// value
		union
		{
			bool m_b_value;
			int64_t m_i_value;
			double m_f_value;
			char* m_s_value;
			JObject* m_o_value;
			JArray* m_a_value;
		};

	public:
		JValue() : m_type(JsonTypes::Null), m_i_value(0) { }
		JValue(const JValue& value) : JValue() { assign(value); }
		JValue(JsonTypes type) : JValue() { makeNew(type); }
		JValue(bool b) : JValue() { setBoolean(b); }
		JValue(int64_t i) : JValue() { setInteger(i); }
		JValue(double f) : JValue() { setFloat(f); }
		JValue(const char* s) : JValue() { setString(s); }
		JValue(const JObject* o) : JValue() { setObject(o); }
		JValue(const JArray* a) : JValue() { setArray(a); }

		~JValue() { setNull(); }

		JValue& operator = (const JValue& value) { return assign(value); }
		JValue& operator = (bool b) { return assign(b); }
		JValue& operator = (int64_t i) { return assign(i); }
		JValue& operator = (double f) { return assign(f); }
		JValue& operator = (const char* s) { return assign(s); }
		JValue& operator = (const JObject* o) { return assign(o); }
		JValue& operator = (const JArray* a) { return assign(a); }

		JValue& assign(const JValue& value) { setValue(value); return *this; }
		JValue& assign(bool b) { setBoolean(b); return *this; }
		JValue& assign(int64_t i) { setInteger(i); return *this; }
		JValue& assign(double f) { setFloat(f); return *this; }
		JValue& assign(const char* s) { setString(s); return *this; }
		JValue& assign(const JObject* o) { setObject(o);  return *this; }
		JValue& assign(const JArray* a) { setArray(a); return *this; }

		RTX_STD_API void makeNew(JsonTypes type);
		RTX_STD_API void setValue(const JValue& value);
		RTX_STD_API void setNull();

		void setBoolean(bool b) { setNull(); m_type = JsonTypes::Boolean; m_b_value = b; }
		void setInteger(int64_t i) { setNull(); m_type = JsonTypes::Integer; m_i_value = i; }
		void setFloat(double f) { setNull(); m_type = JsonTypes::Float; m_f_value = f; }
		void setString(const char* s) { setNull(); m_type = JsonTypes::String; m_s_value = strdup(s); }
		RTX_STD_API void setObject(const JObject* o);
		RTX_STD_API void setArray(const JArray* a);

		JValue* clone() const { return NEW JValue(*this); }
		bool isNull() const { return m_type == JsonTypes::Null; }
		JsonTypes getType() const { return m_type; }

		operator bool() const { return toBoolean(); }
		operator int64_t() const { return toInteger(); }
		operator double() const { return toFloat(); }
		operator const char*() const { return toString(); }
		operator const JObject*() const { return toObject(); }
		operator JObject*() { return toObject(); }
		operator const JArray*() const { return toArray(); }
		operator JArray*() { return toArray(); }

		bool toBoolean() const { return m_type == JsonTypes::Boolean ? m_b_value : false; }
		int64_t toInteger() const { return m_type == JsonTypes::Integer ? m_i_value : -1; }
		double toFloat() const { return m_type == JsonTypes::Float ? m_f_value : NAN; }
		const char* toString() const { return m_type == JsonTypes::String ? m_s_value : nullptr; }
		const JObject* toObject() const { return m_type == JsonTypes::Object ? m_o_value : nullptr; }
		JObject* toObject() { return m_type == JsonTypes::Object ? m_o_value : nullptr; }
		const JArray* toArray() const { return m_type == JsonTypes::Array ? m_a_value : nullptr; }
		JArray* toArray() { return m_type == JsonTypes::Array ? m_a_value : nullptr; }
	};
	//-----------------------------------------------
	//
	//-----------------------------------------------
	class JField
	{
		String m_name;
		JValue m_value;

	public:
		JField() { }
		JField(const String& name) : m_name(name) { }
		JField(const String& name, const JValue& value) : m_name(name), m_value(value) { }
		JField(const String& name, bool b) : m_name(name), m_value(b) { }
		JField(const String& name, int64_t b) : m_name(name), m_value(b) { }
		JField(const String& name, double f) : m_name(name), m_value(f) { }
		JField(const String& name, const char* s) : m_name(name), m_value(s) { }
		JField(const String& name, const JObject* b) : m_name(name), m_value(b) { }
		JField(const String& name, const JArray* a) : m_name(name), m_value(a) { }

		JField& operator = (bool b) { return assign(b); }
		JField& operator = (int64_t i) { return assign(i); }
		JField& operator = (double f) { return assign(f); }
		JField& operator = (const char* s) { return assign(s); }
		JField& operator = (const JObject* o) { return assign(o); }
		JField& operator = (const JArray* a) { return assign(a); }

		JField& assign(bool b) { setBoolean(b); return *this; }
		JField& assign(int64_t i) { setInteger(i); return *this; }
		JField& assign(double f) { setFloat(f); return *this; }
		JField& assign(const char* s) { setString(s); return *this; }
		JField& assign(const JObject* o) { setObject(o);  return *this; }
		JField& assign(const JArray* a) { setArray(a); return *this; }

		void setNull() { m_value.setNull(); }
		void setBoolean(bool b) { m_value.setBoolean(b); }
		void setInteger(int64_t i) { m_value.setInteger(i); }
		void setFloat(double f) { m_value.setFloat(f); }
		void setString(const char* s) { m_value.setString(s); }
		void setObject(const JObject* o) { m_value.setObject(o); }
		void setArray(const JArray* a) { m_value.setArray(a); }

		bool isNull() const { return m_value.isNull(); }
		const String& getName() const { return m_name; }
		JValue& getValue() { return m_value; }
		const JValue& getValue() const { return m_value; }
		JField* clone() const { return NEW JField(m_name, m_value); }

		operator bool() const { return m_value.toBoolean(); }
		operator int64_t() const { return m_value.toInteger(); }
		operator double() const { return m_value.toFloat(); }
		operator const char*() const { return m_value.toString(); }
		operator const JObject*() const { return m_value.toObject(); }
		operator JObject*() { return m_value.toObject(); }
		operator const JArray*() const { return m_value.toArray(); }
		operator JArray*() { return m_value.toArray(); }

		bool toBoolean() const { return m_value.toBoolean(); }
		int64_t toInteger() const { return m_value.toInteger(); }
		double toFloat() const { return m_value.toFloat(); }
		const char* toString() const { return m_value.toString(); }
		const JObject* toObject() const { return m_value.toObject(); }
		JObject* toObject() { return m_value.toObject(); }
		const JArray* toArray() const { return m_value.toArray(); }
		JArray* toArray() { return m_value.toArray(); }
	};
	//-----------------------------------------------
	//
	//-----------------------------------------------
	class JObject
	{
		typedef JField* JFieldPtr;
		SortedArrayT<JFieldPtr, ZString> m_fields;

		static int compare_jobj(const JFieldPtr& l, const JFieldPtr& r);
		static int compare_jobjkey(const ZString& l, const JFieldPtr& r);


	public:
		JObject() : m_fields(compare_jobj, compare_jobjkey) { }
		JObject(const JObject& obj) : JObject() { assign(obj); }
		~JObject() { }

		JObject& operator = (const JObject& obj) { return assign(obj); }

		RTX_STD_API JObject& assign(const JObject& obj);

		bool read(const char* text);
		void write(Stream* stream) const;

		JObject* clone() const { return NEW JObject(*this); }

		int getCount() const { m_fields.getCount(); }
		JField* getAt(int index) { return m_fields.getAt(index); }
		const JField* getAt(int index) const { return m_fields.getAt(index); }
		JField* get(ZString key) { JField** ptr = m_fields.find(key); return ptr ? *ptr : nullptr; }
		const JField* get(ZString key) const { JField* const * ptr = m_fields.find(key); return ptr ? *ptr : nullptr; }

		bool addField(const JField& field) { return addField(field.clone()); }
		bool addField(const String& name, const JValue& value) { return addField(NEW JField(name, value)); }
		bool addField(const String& name, bool b) { return addField(NEW JField(name, b)); }
		bool addField(const String& name, int64_t i) { return addField(NEW JField(name, i)); }
		bool addField(const String& name, double f) { return addField(NEW JField(name, f)); }
		bool addField(const String& name, const char* s) { return addField(NEW JField(name, s)); }
		bool addField(const String& name, const JObject* o) { return addField(NEW JField(name, o)); }
		bool addField(const String& name, const JArray* a) { return addField(NEW JField(name, a)); }

		bool addNullField(const String& name) { return addField(NEW JField(name)); }
		JObject* addNewObject(const String& name) { JField* field = NEW JField(name, JsonTypes::Object);  return addField(field) ? field->toObject() : nullptr; }
		JArray* addNewArray(const String& name) { JField* field = NEW JField(name, JsonTypes::Array);  return addField(field) ? field->toArray() : nullptr; }
		JField* addNew(const String& name, JsonTypes type) { JField* field = NEW JField(name, type); return addField(field) ? field : nullptr; }

	private:
		bool addField(JField* field) { return m_fields.add(field) != BAD_INDEX; }
	};

	class JArray
	{
		ArrayT<JValue*> m_data;
	public:
		JArray() { }
		~JArray() { clean(); }

		RTX_STD_API JArray* clone() const;
		RTX_STD_API void clean();

		int getSize() { return m_data.getCount(); }

	};
}
//-----------------------------------------------
