/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

namespace media
{
	//--------------------------------------
	// ������ � ���� ��������� ��������� ������
	//--------------------------------------
	class MediaWriter
	{
	protected:
		rtl::Logger* m_log;
		rtl::MutexWatch m_sync;

	public:
		RTX_MMT_API static void set_record_max_time(uint32_t max_time);
		RTX_MMT_API static uint32_t getRecordMaxTime();

		void setTimerPeriod(uint32_t period) { m_timerPeriod = period; }
		uint32_t getTimerPeriod() { return m_timerPeriod; }

	protected:
		MediaWriter(rtl::Logger* log);
		virtual ~MediaWriter();

		virtual void flush(bool close = false) = 0;

		void startTimer();
		void stopTimer();

	private:
		/// ��������������� ���������� ��������� ������ ��� ������� ������� � ��������� ������
		/// ���� ��� ��������� ������ (������� ���������� ������)
		friend class MediaWriterManager;

		MediaWriter* m_nextLink;
		uint32_t m_timerPeriod;
		uint32_t m_timerLastTick;
	};
}
//--------------------------------------

