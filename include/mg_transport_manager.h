﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

namespace h248
{
#define MEGACO_DEFAULT_TEXT_PORT		2944
#define MEGACO_DEFAULT_BIN_PORT			2945

	//--------------------------------------
	//
	//--------------------------------------
	class Transport;
	class TransportListener;
	class TransportObject;
	class TransportReader;
	//--------------------------------------
	// события от менеджера сети
	//--------------------------------------
	struct ITransportUser
	{
		virtual void TransportUser_disconnected(const Route& route) = 0;
		virtual void TransportUser_packetArrival(const Route& route, Message* message) = 0;
		virtual void TransportUser_netFailure(const Route& route, int netError) = 0;
		virtual void TransportUser_parserError(const Route& route) = 0;
	};
	//--------------------------------------
	// async transport events
	//--------------------------------------
	enum TransportEventType
	{
		TransportEvent_NewConnection,
		TransportEvent_Disconnected,
		TransportEvent_PacketArrival,
		TransportEvent_Failure,
		TransportEvent_Invalid,
	};
	//--------------------------------------
	// сырой пакет -> длина пакета = sizeof(RawPacket) + RawPacket::length
	//--------------------------------------
	struct RawPacket
	{
		uint32_t ts_1; // время прибытия
		Route route;
		uint16_t length;
		char packet[1];
	};
	//--------------------------------------
	// все события передаются Service как главному диспетчеру!
	//--------------------------------------
	class TransportManager
	{
		static rtl::MutexWatch m_listenerSync;
		static rtl::ArrayT<TransportListener*> m_listenerList;

		static rtl::MutexWatch m_transportSync;
		typedef Transport* TransportPtr;
		static rtl::SortedArrayT<TransportPtr, uint64_t> m_transportList;
		static rtl::SortedArrayT<TransportPtr, uint64_t> m_connectingList;

		static rtl::ArrayT<Transport*> m_purgatory;
		static rtl::MutexWatch m_purgatorySync;

		static rtl::MutexWatch m_readerSync;
		static rtl::ArrayT<TransportReader*> m_readerList;

		static ITransportUser* m_newRouteHandler;

	private:
		// async transport events
		static void async_newConnection(Transport* sender);
		static void async_disconnected(Transport* sender);
		static void async_packetArrival(Transport* sender, RawPacket* packet);
		static void async_failure(Transport* sender, int netError);
		static void async_parserError(Transport* sender);

		// pfn_async_callback_t
		static void pfn_asyncCallback(void* userData, uint16_t callId, uintptr_t intParam, void* ptrParam);

		static TransportListener* findListener(const EndPoint& listenIface, int* index = nullptr);
		static TransportListener* getListener(const EndPoint& listenPoint);

		static bool isRouteToHimself(const Route& route);
		static bool addTransport(Transport* transport);
		static bool removeTransport(Transport* transport);
		static bool isTransportValid(Transport* transport);
		static Transport* findTransport(const Route& route);

		static int compareTransportObj(const TransportPtr& left, const TransportPtr& right);
		static int compareTransportKey(const uint64_t& left, const TransportPtr& right);

		static void pushConnecting(Transport* transport);
		static bool moveConnected(Transport* transport);
		static bool dropConnecting(Transport* transport);
		static Transport* popConnecting(const Route& route);

		static void pushToPurgatory(Transport* transport);
		static void timerEventCallback(rtl::Timer* sender, uint32_t elapsed, void* userData, int tid);
		static void clearPurgatory();

	public:

		RTX_MG_API static bool create(ITransportUser* eventHandler);
		RTX_MG_API static void stop();
		RTX_MG_API static void destroy();

		// подключение слушателя новых соединений TCP или пакетов UDP
		RTX_MG_API static bool addListener(const EndPoint& listenPoint);
		RTX_MG_API static bool removeListener(const EndPoint& listenPoint);
		// подключение к серверам по TCP (connect) по UDP (listener)
		RTX_MG_API static bool createRoute(Route& route, int timeout);
		RTX_MG_API static bool removeRoute(const Route& route, bool force);
		// отправить сообщение по существующему маршруту
		RTX_MG_API static bool sendMessage(const Route& route, const void* message, int length);

		RTX_MG_API static bool isAddressBanned(in_addr remoteAddress);
		RTX_MG_API static bool getBestInterface(in_addr remote_ip, in_addr& local_ip);
		RTX_MG_API static in_addr getDefaultInterface();
		RTX_MG_API static uint16_t getDefaultPort(TransportType type);

		// internal interface
		static void raiseAsyncEvent(int event_id, Transport* sender, uintptr_t int_param = 0, void* ptr_param = nullptr);
		static bool startReading(TransportObject* transport);
		static bool attachTransportToListenerUDP(const EndPoint& localPoint, Transport* transport);
		static bool detachTransportFromListenerUDP(Transport* transport);
	};
}
//--------------------------------------
