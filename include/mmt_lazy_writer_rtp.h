﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mmt_lazy_writer.h"

namespace media
{
#define		RTYPE_EMPTY					0	// empty packet (for invalid or missing packets)
#define		RTYPE_FORMAT				1
#define		RTYPE_RTP					2
//#define		RTYPE_DATA					3

#define 	RTP_FILE_FMT_ENCODING_SIZE		32
#define 	RTP_FILE_FMT_PARAMS_SIZE		32
#define 	RTP_FILE_FMT_FMTP_SIZE			64
//--------------------------------------
// структуры файла записи
//--------------------------------------
#pragma pack(push, 1)
	struct RTPFileHeader				// структура заголовка файла
	{
		uint32_t sign;						// признак формата файла ('RTPA' 'RTPV')
		uint32_t packets_rx;				// количество полученных пакетов
		uint32_t dropped_rec_packets;		// количество отброшенных пакетов (на запись)
		uint32_t dropped_net_packets;		// количество отброшенных пакетов (из сети) опоздавшие и.т.д
		uint32_t dropped_invalid_packets;	// количество отброшенных пакетов (из сети) c невалидной структурой

		uint64_t rec_start_time;			// время начала записи в файл (network time)
		uint32_t rec_start_timestamp;		// таймштамп начала записи (в миллисекундах)
		uint32_t rec_stop_timestamp;		// конец записи (в миллисекундах)
		uint32_t rx_start_timestamp;		// начало приема пакетов (в миллисекундах)
		uint32_t rx_stop_timestamp;			// конец приема пакетов (в миллисекундах)
		uint32_t media_length;				// длина потока в байтах
	};
	//--------------------------------------
	//структуры файла записи
	//--------------------------------------
	struct RTPFileMediaFormat
	{
		char encoding[32];						// кодировка
		uint8_t payload_id;						// номер кодировки
		uint16_t clock_rate;					// частота звука
		uint16_t channels;						// кол-вл каналов
		char params[32];						// параметры формата
		char fmtp[64];							// параметры кодека
	};

	struct RTPFilePacketHeader					// структура пакета в файле
	{
		uint32_t timestamp;					// время получения пакета в миллисекундах от начала первого пакета
		uint16_t type;						// тип пакета
		uint16_t length;					// длина пакета в байтах

		union
		{
			uint8_t buffer[rtp_packet::PAYLOAD_BUFFER_SIZE + rtp_packet::FIXED_RTP_HEADER_LENGTH];	// сам пакет
			RTPFileMediaFormat format[1];	// описание кодирования
		};
	};
#define		RTP_FILE_PACKET_HEADER_LENGTH	8
#pragma pack(pop)
	//--------------------------------------
	// запись в RTP файл входящего звукогого потока
	//--------------------------------------
	class LazyMediaWriterRTP : public LazyMediaWriter
	{
		rtl::FileStream m_file;					// дескриптор файла записи
		RTPFileHeader m_file_header;		// информация о ходе записи в файл
		/// для расчета времени
		bool m_timestamp_flag;
		uint32_t m_timestamp_begin;
		uint32_t m_timestamp_end;
		uint32_t m_timestamp_last_pack;
		// действует только если рекордер стартовал!
		volatile bool m_format_changed;			// признак изменения формата!
		media::PayloadSet m_additional_formats;		// измененный набор форматов!

	public:
		RTX_MMT_API LazyMediaWriterRTP(rtl::Logger* log);
		RTX_MMT_API virtual ~LazyMediaWriterRTP();

		RTX_MMT_API bool create(const char* path);
		RTX_MMT_API bool create(const char* path, const media::PayloadSet& formats);
		RTX_MMT_API void close();

		RTX_MMT_API bool write_packet(const rtp_packet* packet);
		RTX_MMT_API void update_format(const media::PayloadSet& formats);

	private:
		// to cache
		void write_format(const media::PayloadSet& formats);
		// to file
		void rec_write_format(const media::PayloadSet& formats);
		void static payload_to_rtpfmt(const media::PayloadFormat& pl_fmt, RTPFileMediaFormat* rtp_fmt);
		virtual void rec_file_started();
		virtual void rec_file_stopped();
		virtual bool rec_file_write(void* data_block, uint32_t length, uint32_t* written);
	};
}
//--------------------------------------
