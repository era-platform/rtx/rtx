﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

namespace rtl
{

	//--------------------------------------
	// колбак для таймера
	//--------------------------------------
	class Timer;
	//--------------------------------------
	//
	//--------------------------------------
	struct ITimerEventHandler
	{
		virtual void timer_elapsed_event(Timer* sender, int tid) = 0;
	};
	//--------------------------------------
	//
	//--------------------------------------
	typedef void(*TimerEventCallback)(Timer* sender, uint32_t elapsed, void* user_data, int tid);
	//--------------------------------------
	// интерфейс таймера
	//--------------------------------------
	class Timer : IThreadUser
	{
		volatile int m_resolution;
		volatile bool m_started;
		volatile bool m_stopping;
		volatile bool m_pause;

		Event m_stopEvent;
		Thread m_thread;
		uint32_t m_idealTime;
		bool m_realtime;

		ITimerEventHandler* m_handler;

		TimerEventCallback m_callback;
		void* m_userdata;

		Logger& m_log;
		char m_name[64];

	public:
		RTX_STD_API Timer(bool realtime, ITimerEventHandler* handler, Logger& log, const char* name);
		RTX_STD_API Timer(bool realtime, TimerEventCallback handler, void* userdata, Logger& log, const char* name);
		RTX_STD_API ~Timer();

		RTX_STD_API bool start(int resolution);
		RTX_STD_API void stop();

		const char* getName() { return m_name; }
		bool isStarted() { return m_started && !m_stopping; }
		bool isStopped() { return !m_started || m_stopping; }
		void pause() { m_pause = true; }
		void resume() { m_pause = false; }
		int getResolution() { return m_resolution; }
		bool isPaused() { return m_pause; }

		/// ���������� ������ ��� ����
		RTX_STD_API static void setTimer(ITimerEventHandler* handler, uint32_t tid, uint32_t time, bool repeat);
		RTX_STD_API static void setTimer(TimerEventCallback callback, void* user_data, uint32_t tid, uint32_t time, bool repeat);
		RTX_STD_API static void stopTimer(ITimerEventHandler* handler, uint32_t tid);
		RTX_STD_API static void stopTimer(TimerEventCallback callback, void* user_data, uint32_t tid);

		RTX_STD_API static void initializeGlobalTimer(bool high_res);
		RTX_STD_API static void destroyGlobalTimer();

	private:
		virtual void thread_run(Thread* thread);
	};
}
//--------------------------------------
