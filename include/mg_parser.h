﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

namespace h248
{
	//--------------------------------------
	//
	//--------------------------------------
	enum class Relation
	{
		Assignment = '=',		// '=' именно присвоение а не сравнение на равенство
		NotEqual = '#',			// '#' использовать значения не равные
		GreaterThan = '>',		// '>' использовать значения больше чем
		LessThan = '<',			// '<' использовать значения меньше чем
	};
	//--------------------------------------
	//
	//--------------------------------------
	enum class ParserState
	{
		Begin,		// начальное состояние
		Auth,			// Authentication = параметры 
		Startline,	// MEGACO/x  = адрес отправителя

		Token,		// имя [= значение]
		//group,		// {
		EndGroup,	// }
		//next_token,	// ,
		EndOfFile,	// 0
		Error,		// ошибка парсера. дальнейшая работа невозможна
		False,		// если вызвана функция неприемлемая в данном состоянии
	};
	/*
		альтернативная машина состояний
		begin		-> nothing
		auth		-> auth params
		start_line	-> proto + sender address
		value		-> token = value
		group		-> {
		end_group	-> }
		next		-> ,
		end_of_file	-> end of packet
		error		-> parse error + end of packet
		false		-> nothing
	*/
	//--------------------------------------
	//
	//--------------------------------------
	class Parser
	{
		const char* m_packet;
		const char* m_readPointer;
		ParserState m_state;

		int m_tokenNameSize;
		char* m_tokenName;
		Relation m_tokenRelation;
		int m_tokenValueSize;
		char* m_tokenValue;
		bool m_hasGroup;
		int m_groupLevelCounter;

	public:
		RTX_MG_API Parser();
		RTX_MG_API ~Parser();

		RTX_MG_API bool startReading(const char* packet);
		RTX_MG_API void close();

		RTX_MG_API ParserState readNext();
		RTX_MG_API ParserState readTokenGroup(); // для чтения SDP

		bool isReadable() { return m_state < ParserState::EndOfFile; }
		ParserState getState() { return m_state; }
		const char* getTokenName() { return m_tokenName != nullptr ? m_tokenName : ""; }
		Relation getTokenRelation() { return m_tokenRelation; }
		const char* getTokenValue() { return m_tokenValue != nullptr ? m_tokenValue : ""; }
		bool hasGroup() { return m_hasGroup; }

	private:
		ParserState readAuth(const char* stream);
		ParserState readStartLine(const char* stream);
		ParserState readToken(const char* stream);

		const char* readTokenValue(const char* stream, char end_sign);
		const char* readTokenValue(const char* stream);
		const char* readTokenName(const char* stream);

		void setTokenName(const char* name, int length);
		void setTokenValue(const char* value, int length);
	};
}
//--------------------------------------
