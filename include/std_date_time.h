﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <time.h>

namespace rtl
{

	//--------------------------------------
	//
	//--------------------------------------
	class DateTime
	{
		union
		{
			struct
			{
				uint64_t m_millisecond : 15, m_second : 6, m_minute : 6, m_hour : 5, m_day : 8, m_month : 8, m_year : 16;
			};
			struct
			{
				uint32_t m_date;
				uint32_t m_time;
			};
			uint64_t m_timestamp;
		};

	public:
		DateTime(uint64_t timestamp) { m_timestamp = timestamp; }
		DateTime() { *this = now(); }
		DateTime(const DateTime& dt) { assign(dt); }

		DateTime& operator = (const DateTime& dt) { return assign(dt); }
		DateTime& operator = (const ::tm& tm) { return assign(tm); }

		DateTime& assign(const DateTime& dt) { m_timestamp = dt.m_timestamp; return *this; }
		RTX_STD_API DateTime& assign(const ::tm& tm);

		void setYear(uint32_t year) { m_year = year; }
		uint32_t getYear() const { return m_year; }

		// порядковый номер месяца в году. отсчет с 1.
		void setMonth(uint32_t month) { m_month = month; }
		uint32_t getMonth() const { return m_month; }

		// порядковый номер дня в месяце. отсчет с 1.
		void setDay(uint32_t day) { m_day = day; }
		uint32_t getDay() const { return m_day; }

		void setHour(uint32_t hour) { m_hour = hour; }
		uint32_t getHour() const { return m_hour; }

		void setMinute(uint32_t minute) { m_minute = minute; }
		uint32_t getMinute() const { return m_minute; }

		void getSecond(uint32_t second) { m_second = second; }
		uint32_t setSecond() const { return m_second; }

		void setMilliseconds(uint32_t millisecond) { m_millisecond = millisecond; }
		uint32_t getMilliseconds() const { return m_millisecond; }
		uint64_t getTimestamp() const { return m_timestamp; }
		uint32_t getDate() const { return m_date; }
		uint32_t getTime() const { return m_date; }
		RTX_STD_API uint64_t getGTS() const;

		bool isZero() const { return m_timestamp == 0; }

		bool operator < (const DateTime& dt) const { return m_timestamp < dt.m_timestamp; }
		bool operator <= (const DateTime& dt) const { return m_timestamp <= dt.m_timestamp; }
		bool operator > (const DateTime& dt) const { return m_timestamp > dt.m_timestamp; }
		bool operator >= (const DateTime& dt) const { return m_timestamp >= dt.m_timestamp; }
		bool operator == (const DateTime& dt) const { return m_timestamp == dt.m_timestamp; }
		bool operator != (const DateTime& dt) const { return m_timestamp != dt.m_timestamp; }

		RTX_STD_API static const DateTime now();
		// GEORGE : for timestamps : global time ticks in milliseconds since 1970
		RTX_STD_API static uint64_t getCurrentGTS();	// Unix time in milliseconds
		RTX_STD_API static DateTime parseGTS(uint64_t gtt);	// Unix time in milliseconds
		RTX_STD_API static int64_t subtractGTS(uint64_t gttA, uint64_t gttB); // Unix time in milliseconds (abc)
		RTX_STD_API static int64_t addGTS(uint64_t gttA, uint64_t gttB); // Unix time in milliseconds (abc)
		RTX_STD_API static int compareGTS(uint64_t gttA, uint64_t gttB); // Unix time in milliseconds

		RTX_STD_API static uint32_t getTicks();		// milliseconds from start
		RTX_STD_API static uint64_t getTicks64();
		RTX_STD_API static uint64_t getNTP();
	};
}
//------------------------------------------------------------------------------
