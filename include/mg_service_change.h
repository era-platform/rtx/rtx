/* RTX H.248 Media Gate
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

namespace h248
{
	/*
	� ServiceChangeMethod;
			Token::Restart,
			Token::Forced,
			Token::Graceful,
			Token::Failover,
			Token::HandOff,
			Token::Disconnected,

	� ServiceChangeReason;
			900 Service Restored
			901 Cold Boot
			902 Warm Boot
			903 MGC Directed Change
			904 Term Malfunction
			905 Term Taken OOS
			906 Loss of lower layer	connectivity
			907 Transmission failure
			908 MG Impending Failure
			909 MGC Impending Failure
			910 Media Capability Failure
			911 Modem Capability Failure
			912 Mux Capability Failure
			913 Signal Capability Failure
			914 Event Capability Failure
			915 State Loss
			916 Packages Change
			917 Capability Change
			918 X Cancel Graceful
			919 Warm Failover
			920 Cold Failover
	� ServiceChangeAddress;
			mid | port

	� ServiceChangeDelay;
			int seconds

	� ServiceChangeProfile;
			char* profile_name

	� ServiceChangeVersion;
			int version
	� ServiceChangeMGCID;
			mid

	� TimeStamp;
			Date "T" Time ; per ISO 8601:2004

	� Extension;

	� ServiceChangeInfo;
	� ServiceChangeIncompleteFlag
	*/
	//--------------------------------------
	// ������������ ��� ��������� �������� ServiceChange
	//--------------------------------------
	class ServiceChangeDescriptor
	{
		Token m_method;			// serviceChangeMethod
		Reason m_reasonCode;	// serviceChangeReason
		rtl::String m_reasonText;
		uint32_t m_delay;					// serviceChangeDelay
		Token m_midType;			// serviceChangeAddress | serviceChangeMgcId
		MID m_mid;						// mid
		uint16_t m_port;					// port number
		rtl::String m_profile;					// serviceChangeProfile
		rtl::DateTime m_timestamp;			// TimeStamp
		int m_version;						// serviceChangeVersion
		bool m_SIC;							// ServiceChangeIncompleteToken

	public:
		RTX_MG_API ServiceChangeDescriptor();
		RTX_MG_API ~ServiceChangeDescriptor();

		RTX_MG_API bool read(const Field& services_descriptor);

		Token get_method() const { return m_method; }
		Reason get_reason_code() const { return m_reasonCode; }
		const rtl::String& get_reasdon_text() const { return m_reasonText; }
		uint32_t get_delay() const { return m_delay; }
		Token get_mid_type() const { return m_midType; }
		const MID& get_mid() const { return m_mid; }
		bool only_port() const { return m_port != 0; }
		uint16_t get_port() const { return m_port; }
		const rtl::String& get_profile() const { return m_profile; }
		rtl::DateTime getTimestamp() const { return m_timestamp; }
		int get_version() const { return m_version; }
		bool get_SIC() const { return m_SIC; }

	private:
		bool parse_mid(const char* mid, Token type);
		bool parse_timestamp(const char* timestamp);
	};
}
//--------------------------------------
