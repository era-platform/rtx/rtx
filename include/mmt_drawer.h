/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_media_tools.h"
#include "mmt_bitmap.h"
#include "mmt_video_font.h"

namespace media
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	struct Point
	{
		int x;
		int y;
	};
	//-----------------------------------------------
	//
	//-----------------------------------------------
	struct Size
	{
		int width;
		int height;
	};
	//-----------------------------------------------
	//
	//-----------------------------------------------
	struct Rectangle
	{
		int left, top;
		int right, bottom;
		//---
		int getWidth() const { return right - left; }
		int getHeight() const { return bottom - top; }
		//---
		void normalize()
		{
			if (right < left)
			{
				swap(left, right);
			}

			if (bottom < top)
			{
				swap(top, bottom);
			}
		}
		//---
		Rectangle normalize() const
		{
			Rectangle r = *this;
			r.normalize();
			return r;
		}
		//---
		bool contains(int x, int y)
		{
			return x >= left && x < right &&
				y >= top && y < bottom;
		}
		//---
		Size size() const { return { getWidth(), getHeight() }; }
		Point location() const { return { left, top }; }
		//---
		void move(int x, int y) { left += x; right += x; top += y; bottom += y; }
		void resize(int width, int height) { right = left + width; bottom = top + height; }
		void inflate(int dx, int dy) { left -= dx; top -= dy; right += dx; bottom += dx; }
		void set(int l, int t, int r, int b) { left = l; top = t; right = r; bottom = b; }

		RTX_MMT_API bool intersect(const Rectangle& rect, Rectangle& result) const;
	};
	//-----------------------------------------------
	//
	//-----------------------------------------------
	enum class TextBackMode
	{
		Transparent = 0,
		Contoured = 1,
		Opaque = 2,
	};
	const char* TextBackMode_toString(TextBackMode mode);
	TextBackMode TextBackMode_parse(const char* mode);
	//-----------------------------------------------
	//
	//-----------------------------------------------
	enum TextFormatFlags
	{
		// Vertical alignment
		TextFormatTop = 0,
		TextFormatBottom = 1,
		TextFormatMiddleV = 2,
		TextFormatVertMask = 3,

		// Horizontal alignment
		TextFormatLeft = 0,
		TextFormatRight = 4,
		TextFormatMiddleH = 8,
		TextFormatHorzMask = 12, // 0x0C

		TextFormatSingleLine = 0,
		TextFormatMultiline = 16 // 0x10
	};
	rtl::String TextFormatFlags_toString(TextFormatFlags flags);
	TextFormatFlags TextFormatFlags_parse(const char* flags_str);
	//-----------------------------------------------
	//
	//-----------------------------------------------
	class ImageDrawer
	{
	private:
		Bitmap& m_image;
		int  m_penWidth;
		Color m_penColor;
		Rectangle m_clipRect;

		Color m_backColor;
		Color m_foreColor;
		TextBackMode m_backMode;
		int m_marginLeft, m_marginTop, m_marginRight, m_marginBottom;
		const Font* m_font;

	private:
		void drawCharContoured(int x, int y, const PixelMap* map);
		void drawCharOpaque(int x, int y, const PixelMap* map);
		void drawCharTransparent(int x, int y, const PixelMap* map);
		int drawText(int x, int y, const char* text);
		int drawTextContoured(int x, int y, const char* text);
		int drawTextOpaque(int x, int y, const char* text);
		int drawTextTransparent(int x, int y, const char* text);
		int calculateVertAlign(uint32_t flags);
		int calculateHorzAlign(const char* text, uint32_t flags);
	public:

		RTX_MMT_API ImageDrawer(Bitmap& image);

		void setPixel(int x, int y) { if (m_clipRect.contains(x,y)) m_image.setPixel(x, y, m_penColor); }
		void setPixel(int x, int y, Color color) { if (m_clipRect.contains(x, y)) m_image.setPixel(x, y, color); }

		RTX_MMT_API void setClipRect(const Rectangle& clipRect);
		void getClipRect(Rectangle& clipRect) const { clipRect = m_clipRect; }

		void setPenWidth(int width) { if ((width > 0) && (width < 4)) { m_penWidth = width; } }
		int getPenWidth() { return m_penWidth; }
		void setPenColor(Color color) { m_penColor = color; }
		const Color& getPenColor() const { return m_penColor; }
		Color& getPenColor() { return m_penColor; }

		void setTextBackMode(TextBackMode mode) { m_backMode = mode; }
		void setTextMarginLeft(int value) { m_marginLeft = value; }
		void setTextMarginRight(int value) { m_marginRight = value; }
		void setTextMarginTop(int value) { m_marginTop = value; }
		void setTextMarginBottom(int value) { m_marginBottom = value; }
		void setTextBackColor(Color backColor) { m_backColor = backColor; }
		void setTextForeColor(Color foreColor) { m_foreColor = foreColor; }
		const char* getFontName() { return m_font->getFaceName(); }
		int getFontHeight() { return m_font->getHeight(); }
		RTX_MMT_API int getTextWidth(const char* zstr);
		RTX_MMT_API void setTextMargins(int left, int top, int right, int bottom);
		RTX_MMT_API void setFont(const char* facename, int height);

		RTX_MMT_API void drawImage(int x, int y, const Bitmap& image);
		RTX_MMT_API void drawImage(const Point& loc, const Bitmap& image) { drawImage(loc.x, loc.y, image); }
		RTX_MMT_API void drawRectangle(int x1, int y1, int x2, int y2);
		RTX_MMT_API void drawRectangle(const Rectangle& rect);
		RTX_MMT_API void drawTriangle(int x1, int y1, int x2, int y2, int x3, int y3);
		RTX_MMT_API void drawTriangle(const Point& p1, const Point& p2, const Point& p3);
		RTX_MMT_API void drawQuadix(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4);
		RTX_MMT_API void drawQuadix(const Point& p1, const Point& p2, const Point& p3, const Point& p4);
		RTX_MMT_API void drawLine(int x1, int y1, int x2, int y2);
		RTX_MMT_API void drawLine(const Point p1, const Point& p2) { drawLine(p1.x, p1.y, p2.x, p2.y); }
		RTX_MMT_API void drawLineHorz(int x1, int x2, int y);
		RTX_MMT_API void drawLineVert(int y1, int y2, int x);
		RTX_MMT_API void drawEllipse(int centerx, int centery, int a, int b);
		RTX_MMT_API void drawCircle(int centerx, int centery, int radius);
		RTX_MMT_API void drawDot(int x, int y);
		RTX_MMT_API void fillRectangle(int x, int y, int width, int height);
		RTX_MMT_API void fillRectangle(const Rectangle& rect);
		RTX_MMT_API int drawText(int x, int y, int width, int height, const char* text, uint32_t flags = 0);
		RTX_MMT_API int drawText(const Rectangle& bounds, const char* text, uint32_t flags = 0);

		RTX_MMT_API Size drawString(int x, int y, const char* zstr);
		RTX_MMT_API Size drawChar(int x, int y, char ch);
	};
}

