﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mmt_lazy_writer.h"
#include "mmt_file_wave.h"

namespace media
{
	//--------------------------------------
	// запись в wave файл входящего звукогого потока
	//--------------------------------------
	class LazyMediaWriterWAVE : public LazyMediaWriter
	{
		media::FileWriterWAV m_file;					// дескриптор файла записи
		bool m_pcm_format;

	public:
		LazyMediaWriterWAVE(rtl::Logger* log);
		virtual ~LazyMediaWriterWAVE() override;

		bool create(const char* path, const media::PayloadFormat& format);
		void close();

		bool write_samples(short* samples, int count);

	private:
		virtual void rec_file_started() override;
		virtual void rec_file_stopped() override;
		virtual bool rec_file_write(void* data_block, uint32_t length, uint32_t* written) override;
	};
}
//--------------------------------------
