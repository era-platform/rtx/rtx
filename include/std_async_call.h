﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

//--------------------------------------
//
//--------------------------------------
#define CONF_ASYNC_THREAD_INITIAL	"async-thread-initial"
#define CONF_ASYNC_THREAD_MAX		"async-thread-max"
//--------------------------------------
//
//--------------------------------------
#define ASYNC_THREADS_MAX			256
#define ASYNC_THREADS				4
#define ASYNC_CALL_QUEUE_RED_LINE	16
namespace rtl
{
	//--------------------------------------
	// интерфейс для вызова обработчика
	//--------------------------------------
	struct async_call_target_t
	{
		virtual const char* async_get_name() = 0;
		virtual void async_handle_call(uint16_t call_id, uintptr_t int_param, void* ptr_param) = 0;
	};
	//--------------------------------------
	// callback для вызова обработчика
	//--------------------------------------
	typedef void(*pfn_async_callback_t)(void* userdata, uint16_t call_id, uintptr_t int_param, void* ptr_param);
	//--------------------------------------
	// упаковка параметров
	//--------------------------------------
	struct async_call_info_t
	{
		// callback interface
		union
		{
			async_call_target_t* target;
			pfn_async_callback_t callback;
		};

		void* userdata;	// user data. NULL for async_call_target_t

		// callback parameters
		uint16_t call_id[2];	// idx_0 - type of callback (0 - interface, 1 - callback). idx_1 - call_id
		uintptr_t int_param;
		void* ptr_param;
	};
	//--------------------------------------
	// worker thread for async caller
	//--------------------------------------
	class async_call_manager_t;
	//--
	class async_call_thread_t : IThreadUser
	{
		Thread m_thread;
		async_call_manager_t& m_owner;

	public:
		async_call_thread_t(async_call_manager_t& owner);
		virtual ~async_call_thread_t();

		uint32_t get_thread_id() { return m_thread.get_id(); }

		bool start(ThreadPriority priority);
		void stop();

	private:
		virtual void thread_run(Thread* thread);
	};
	//--------------------------------------
	// статистика использования менеджера асинхронных вызовов
	//--------------------------------------
	struct async_call_statistics_t
	{
		int queue_size;
		int waiting_thread_count;
		int all_thread_count;
	};
	//--------------------------------------
	// асинхронные вызовы на семафорах!
	//--------------------------------------
	class async_call_manager_t
	{
		volatile bool m_created;
		volatile bool m_started;

		QueueT<async_call_info_t> m_call_queue;
		Semaphore m_call_queue_sem;
		Mutex m_sync;
		ArrayT<async_call_thread_t*> m_thread_pool;
		ThreadPriority m_priority;
		int m_thread_max;

		volatile uint32_t m_thread_waiting_count;
		
	public:
		RTX_STD_API async_call_manager_t();
		RTX_STD_API ~async_call_manager_t();

		RTX_STD_API bool create(int initial_thread_count, int max_threads = 0);
		RTX_STD_API void destroy();

		RTX_STD_API bool start(ThreadPriority priority = ThreadPriority::Normal);
		RTX_STD_API void stop();

		RTX_STD_API bool call(async_call_target_t* target, uint16_t call_id, uintptr_t int_param, void* ptr_param);
		RTX_STD_API bool call(pfn_async_callback_t callback, void* userdata, uint16_t call_id, uintptr_t int_param, void* ptr_param);

		RTX_STD_API void get_statistics(async_call_statistics_t& stat);

		RTX_STD_API static void createAsyncCall(int initial_thread_count, int max_threads = 0);
		RTX_STD_API static void destroyAsyncCall();

		RTX_STD_API static bool callAsync(async_call_target_t* target, uint16_t call_id, uintptr_t int_param, void* ptr_param);
		RTX_STD_API static bool callAsync(pfn_async_callback_t callback, void* userdata, uint16_t call_id, uintptr_t int_param, void* ptr_param);

	private:
		friend class async_call_thread_t;
		bool get_call(async_call_info_t& call_info);
		void check_thread_count();
	};

	//--------------------------------------
	// callback для вызова обработчика
	//--------------------------------------
	typedef void(*pfnQueueHandlerCallback)(void* userData, uint16_t messageId, uintptr_t intParam, void* ptrParam);

	class MessageQueue : public IThreadUser
	{
		volatile bool m_started;

		QueueT<async_call_info_t> m_queue;
		Semaphore m_queueSemaphore;
		Event m_startEvent;
		Mutex m_sync;
		Thread m_handlingThread;
		ThreadPriority m_priority;

	public:
		RTX_STD_API MessageQueue();
		RTX_STD_API virtual ~MessageQueue();

		RTX_STD_API bool start(ThreadPriority priority = ThreadPriority::Normal);
		RTX_STD_API void stop();

		RTX_STD_API bool push(pfnQueueHandlerCallback callback, void* userData, uint16_t messageId, uintptr_t intParam, void* ptrParam);

	private:
		bool getCall(async_call_info_t& call_info);

		// <IThreadUser>
		virtual void thread_run(Thread* thread);
	};
}
//--------------------------------------
