﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_media_tools.h"

#include "mmt_video_frame.h"
#include "mmt_video_font_man.h"

namespace media
{
	class VideoFrameTextBase : public VideoFrameElement
	{
	protected:
		rtl::String m_text;
		rtl::String m_faceName;
		TextBackMode m_textMode;
		TextFormatFlags m_formatFlags;
		const Font* m_font;
		Color m_foreColor;				// цвет текста

	public:
		VideoFrameTextBase(VideoFrameGenerator& frame, VideoElementType type, const rtl::String& id) : VideoFrameElement(frame, type, id) { }
		RTX_MMT_API virtual ~VideoFrameTextBase();

		RTX_MMT_API virtual void assign(const VideoRawElement& data);
		RTX_MMT_API virtual void drawOn(ImageDrawer& draw);
	};

	class VideoFrameText : public VideoFrameTextBase
	{
	public:
		VideoFrameText(VideoFrameGenerator& frame, const rtl::String& id) : VideoFrameTextBase(frame, VideoElementType::Text, id) { }
		RTX_MMT_API virtual ~VideoFrameText();

		RTX_MMT_API virtual void assign(const VideoRawElement& data);
	};

	class VideoFrameTimer : public VideoFrameTextBase
	{
		int m_timeZone;
		rtl::String m_format;
		time_t m_lastTick;
		void printTime(time_t ts);

	public:
		VideoFrameTimer(VideoFrameGenerator& frame, const rtl::String& id) :
			VideoFrameTextBase(frame, VideoElementType::Time, id),
			m_timeZone(0), m_lastTick(0) { }
		RTX_MMT_API virtual ~VideoFrameTimer();

		RTX_MMT_API virtual void assign(const VideoRawElement& data);
		RTX_MMT_API virtual bool isUpdated();
		RTX_MMT_API virtual void drawOn(ImageDrawer& draw);
	};
}
//-----------------------------------------------

