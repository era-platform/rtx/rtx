﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mmt_lazy_writer_rtp.h"
#include "mmt_file_rtp.h"
#include "mmt_audio_reader.h"

namespace media
{
	//--------------------------------------
	//
	//--------------------------------------
	struct JitterRecord;
	//--------------------------------------
	//
	//--------------------------------------
	class AudioReaderRTP : public AudioReader
	{
		rtl::FileStream m_file;				// мультимедиа файл
		uint32_t m_frameTime;
		bool m_disposed;
		bool m_isVideo;

		uint8_t* m_raw_cache;				// сырые данные
		uint32_t m_raw_used;				// использованно данных в кеше
		uint32_t m_raw_length;				// всего данных считанно из файла
		uint32_t m_raw_size;				// размер кеша

		uint8_t* m_pcm_cache;				// данные PCM
		uint32_t m_pcm_used;
		uint32_t m_pcm_size;
		uint32_t m_pcm_length;

		RTPFileHeader m_header;

		struct decoder_info_t
		{
			int payload_id;
			int channels;
			int freq;
			char encoding[32];

			mg_audio_decoder_t* decoder;
			Resampler* resampler;
		};
		rtl::ArrayT<decoder_info_t> m_decoder_list;
		int m_last_pid;

		rtl::MutexWatch m_lock;

		/*эмуляция текущего времени, время относительно начала записи.
		нужно для использования джиттер-буфера.
		tick'и происходят при чтении пакетов в ReadNext.*/
		uint32_t m_current_time;
		RTPFilePacketHeader* m_current_packet;

		bool m_jitter_ready;
		JitterRecord* m_jitter;		//массив rtp_record_t'ов
		uint8_t m_jitter_depth;
		uint8_t m_jitter_writer;
		uint8_t m_jitter_reader;
		uint8_t m_jitter_count;

	public:
		RTX_MMT_API AudioReaderRTP(rtl::Logger* log);
		RTX_MMT_API ~AudioReaderRTP() override;

		uint32_t getStartTimestamp() { return m_header.rec_start_timestamp; }
		uint32_t getStopTimestam() { return m_header.rec_stop_timestamp; }

	private:
		virtual bool openSource(const char* path) override;
		virtual void closeSource() override;
		virtual int readRawPCM(short* buffer, int size) override;

		uint32_t read_next_pcm(short* samples, int size);
		uint32_t decode_rtp_to_pcm();
		void fill_jitter();

		bool get_next_rtp();
		bool read_rtp_packet(bool allowSwap);

		void read_raw_data();
		void update_decoder_list(RTPFileMediaFormat* fmt_list, int fmt_count);

		void resize_jitter(int index, uint32_t minLen);
		void push_to_jitter(RTPFilePacketHeader* pRecPack);
		RTPFilePacketHeader* pop_from_jitter();

		void reset();
		decoder_info_t* get_decoder(uint8_t payload_type);
	};
}
//--------------------------------------
