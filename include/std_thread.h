﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

namespace rtl
{

	//--------------------------------------
	//
	//--------------------------------------
	enum class ThreadPriority
	{
		Lowest,
		Low,
		Normal,
		High,
		Highest,
		Realtime,
	};
	//--------------------------------------
	//
	//--------------------------------------
	typedef uint32_t ThreadId;
	class Thread;
	//--------------------------------------
	// интерфейсы обратного вызова для запуска потока.
	//--------------------------------------
	typedef void(*ThreadCallback)(Thread* thread, void* context);
	//--------------------------------------
	//
	//--------------------------------------
	struct IThreadUser
	{
		virtual void thread_run(Thread* thread) = 0;
	};
#if defined(TARGET_OS_WINDOWS)
	typedef HANDLE ThreadHandle;
	typedef uint32_t ThreadResult;
#define IS_THREAD_VALID(t) (t != 0)
#define THREAD_CALL __stdcall
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	typedef pthread_t ThreadHandle;
	typedef void* ThreadResult;
#if defined (__MINGW32__)
#define IS_THREAD_VALID(t) (t.x != 0)
#else
#define IS_THREAD_VALID(t) (t != 0)
#endif
#define THREAD_CALL
#endif
	//--------------------------------------
	//
	//--------------------------------------
	class Thread
	{
	public:
		RTX_STD_API Thread();
		~Thread() { }

		ThreadId get_id() { return m_id; }

		ThreadPriority get_priority() { return m_priority; }
		RTX_STD_API void set_priority(ThreadPriority priority);

		bool get_stopping_flag() { return m_stop_flag; }
		bool isStarted() { return IS_THREAD_VALID(m_handle); }

		RTX_STD_API bool start(ThreadCallback user_handler, void* user_context = nullptr);
		RTX_STD_API bool start(IThreadUser* handler);
		RTX_STD_API void stop();
		RTX_STD_API bool wait(int period = -1);

		RTX_STD_API static void sleep(uint32_t timeout);
		RTX_STD_API static ThreadId get_current_tid();

		RTX_STD_API static void set_last_error(uintptr_t error);
		RTX_STD_API static void reset_error();
		RTX_STD_API static uintptr_t get_last_error();

	private:
		static ThreadResult THREAD_CALL thread_proc(void* thread_context);
		bool start_thread();
		static int map_to_OS_priority(ThreadPriority priority);

	private:
		MutexWatch m_sync;
		ThreadHandle m_handle;
		ThreadId m_id;
		volatile bool m_stop_flag;
		ThreadCallback m_user_function;
		void* m_user_context;
		IThreadUser* m_handler;
		ThreadPriority m_priority;
	};
}
//--------------------------------------
