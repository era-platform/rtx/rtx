﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_stdlib.h"

namespace rtl
{
	//--------------------------------------
	// 
	//--------------------------------------
#define STD_RES_COUNTER_MAX_INDEX	64
//--------------------------------------
// 
//--------------------------------------
	class res_counter_t
	{
		struct res_counter_entry_t
		{
			char res_name[32];
			uint64_t counter;
			uint64_t max_value;
		};

		static Mutex s_sync;
		static res_counter_entry_t s_res[STD_RES_COUNTER_MAX_INDEX];

	public:
		RTX_STD_API static void initialize();
		RTX_STD_API static int add(const char* res_name);
		RTX_STD_API static void add_ref(int type);
		RTX_STD_API static void release(int type);
		RTX_STD_API static int get_statistics(char* buffer, int size);
	};
}
//--------------------------------------
