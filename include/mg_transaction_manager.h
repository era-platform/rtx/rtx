﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

namespace h248
{

	//--------------------------------------
	// предварительное объявление для определения событийного интерфейса
	//--------------------------------------
	class Transaction;
	//--------------------------------------
	// интерфейс отправителя запросов
	//--------------------------------------
	struct ITransactionUser
	{
		// client transaction events
		virtual bool TransactionUser_responseReceived(Transaction* sender, const Field* response) = 0;
		virtual bool TransactionUser_pendingReceived(Transaction* sender) = 0;
		virtual void TransactionUser_requestTimeout(Transaction* sender) = 0;

		// common transaction events
		virtual void TransactionUser_errorReceived(Transaction* sender, const Field* error) = 0;
		virtual void TransactionUser_terminated(Transaction* sender) = 0;

		// server transaction events
		virtual void TransactionUser_requestReceived(Transaction* sender, const Field* request) = 0;
	};
	//--------------------------------------
	//
	//--------------------------------------
	enum mg_out_mid_type_t
	{
		mg_out_mid_ipv4_e,
		mg_out_mid_ipv6_e,
		mg_out_mid_domain_e,
		mg_out_mid_device_e,
		mg_out_mid_unknown_e = -1
	};
	RTX_MG_API mg_out_mid_type_t parse_mg_out_mid_type(const char* text, int len = -1);
	//--------------------------------------
	//
	//--------------------------------------
	class TransactionManager
	{
		// в основном входящие запросы
		static rtl::MutexWatch s_sync;
		typedef Transaction* TransactionPtr;
		static rtl::SortedArrayT<TransactionPtr, TransactionID> s_timerList;
		static rtl::SortedArrayT<TransactionPtr, TransactionID> s_clientList;
		static rtl::SortedArrayT<TransactionPtr, TransactionID> s_serverList;
		static int s_longTimerValue;
		static int s_initialRetransmissionValue;
		static int s_maxRetransmissionCount;
		static volatile bool s_stopped;
		static volatile uint32_t s_activeTransactions;
		static h248::mg_out_mid_type_t m_midType;
		static rtl::String m_midDomain;

	private:
		static void timerHandler(rtl::Timer* sender, uint32_t elapsed, void* user_data, int tid);
		static int compareObj(const TransactionPtr& left, const TransactionPtr& right);
		static int compareKey(const TransactionID& left, const TransactionPtr& right);
		static void asyncHandler(void* userdata, uint16_t call_id, uintptr_t int_param, void* ptr_param);

		static Transaction* findClientTransaction(TransactionID trn_key);
		static Transaction* findServerTransaction(TransactionID trn_key);

		static void terminateTransaction(Transaction* transaction);
		static void checkTransactionTimes();

	public:
		// управляющие транзакциями функции
		RTX_MG_API static bool create(h248::mg_out_mid_type_t mid_type, const char* mid_domain);
		RTX_MG_API static void stop();
		RTX_MG_API static void destroy();

		static bool startClientTransaction(ITransactionUser* user, TransactionID tid, int mgc_version,
			const Field* message, const Route& route);
		static void cancelMgcTransactions(uint32_t mgc_id);
		static void sendReply(TransactionID tid, Field* reply);
		static bool sendBackError(TransactionID tid, ErrorCode error_code, const char* reason);
		static bool processServerTransaction(ITransactionUser* user, TransactionID tid, int mgc_version,
			const Field* request, const Route& route); // Field* reply, 
		static bool processReply(TransactionID trn_key, const Field* reply);
		static bool processAck(TransactionID trn_key);

		// внутренние функции
		static void startLongTimer(Transaction* trn);
		static void addClientTransaction(Transaction* trn);
		static void addServerTransaction(Transaction* trn);
		static void removeTransaction(Transaction* trn);

		static int getLongTimerValue() { return s_longTimerValue; }
		static int getInitialRetransmissionValue() { return s_initialRetransmissionValue; }
		static int getMaxRetransmissionCount() { return s_maxRetransmissionCount; }

		static h248::mg_out_mid_type_t getMidType() { return m_midType; }
		static const rtl::String& getMidDomain() { return m_midDomain; }
		static const char* makeMid(char* buffer, int size, in_addr addr, uint16_t port);

		static bool isStopped() { return s_stopped; }
		static void addActiveCounter() { std_interlocked_inc(&s_activeTransactions); }
		static void subActiveCounter() { std_interlocked_dec(&s_activeTransactions); }
	};
}
//--------------------------------------
