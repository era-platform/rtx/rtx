﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "mmt_audio_reader.h"
#include "mmt_timer.h"
#include "std_logger.h"
#include "std_string.h"

namespace media
{
	//------------------------------------------------------------------------------
	// Структура возвращается при вызове метода стоп плеера. 
	// И несет информацию на каком файле и на какой секунде закончили воспроизведение.
	//------------------------------------------------------------------------------
	struct PlayerReport
	{
		rtl::StringList errorFiles;	// список файлов с ошибками, кроме последнего,
									// последняя строка указывает на каком файле становили воспроизведение.
									// если в списке один файл то значит ошибок не было.
		uint32_t stopTime;
	};
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	struct IPlayerEventHandler
	{
		virtual void eventDataReady(const char* data, int length) = 0;
		virtual void eventEndPlay(PlayerReport* report) = 0;
	};
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	class AudioPlayer : public Metronome
	{
		AudioPlayer(const AudioPlayer&);
		AudioPlayer& operator=(const AudioPlayer&);

	public:
		RTX_MMT_API AudioPlayer(rtl::Logger* log);
		RTX_MMT_API ~AudioPlayer();

		RTX_MMT_API bool init(int ticksPeriod, int sampleRate, int channelCount, IPlayerEventHandler* handler);
		RTX_MMT_API void destroy();

		RTX_MMT_API void setStartAt(int startAt);
		RTX_MMT_API void setStopAt(int stopAt);
		RTX_MMT_API void setLoop(bool loop);
		RTX_MMT_API void setRandom(bool random);

		RTX_MMT_API bool startPlay();
		RTX_MMT_API PlayerReport* stopPlay();

		RTX_MMT_API bool addFile(const char* filePath);
		RTX_MMT_API bool addDir(const char* dir);

		RTX_MMT_API void setVolume(int volume);

	private:
		virtual	void metronomeTicks();
		
		bool openFile(int index);
		bool openNextFile();
		bool openRandomFile();

		static void async_handle_call(void* userData, uint16_t callId, uintptr_t intParam, void* ptrParam);
		void raiseEndplayEvent();
	private:
		rtl::Logger* m_log;
		AudioReader* m_reader;

		// Флаг указывает на то что поток таймера запущен.
		volatile bool m_started;
		// Флаг потока таймера. Указывает что поток в работе.
		volatile bool m_work;
		// Флаг указывает что уже был отправлен асинхронный вызов стопа.

		rtl::StringList m_fileList;

		int m_volume;
		bool m_loop;
		bool m_random;
		int m_indexCurrentFile; // индекс текущего файла в списке файлов.

		int m_startAt;
		int m_stopAt;
		int m_alreadyRead; // количество уже обработанных семплов.

		int m_ticksPeriod;
		int m_sampleRate;
		int m_channelCount;

		IPlayerEventHandler* m_handler;
		PlayerReport m_report;
	};
}
//------------------------------------------------------------------------------
