/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_parser.h"

namespace h248
{
	//--------------------------------------
	//
	//--------------------------------------
	class MID
	{
	public:
		enum AddressType
		{
			IPv4,
			IPv6,
			Domain,
			Device,
			MTP,
			None = -1
		};

	private:
		AddressType m_type;
		union
		{
			in_addr m_ipv4;
			in6_addr m_ipv6;
			char* m_domain; // + device

		};
		uint16_t m_port;

		RTX_MG_API static const in_addr IPv4_Error;
		RTX_MG_API static const in6_addr IPv6_Error;

	public:

		MID() : m_type(IPv4), m_port(0) { memset(&m_ipv6, 0, sizeof(m_ipv6)); }
		MID(const MID& mid) : m_type(IPv4), m_port(0) { memset(&m_ipv6, 0, sizeof(m_ipv6)); *this = mid; }
		~MID() { clear(); }

		RTX_MG_API MID& operator = (const MID& mid);

		RTX_MG_API void clear();

		AddressType getType() const { return m_type; }
		in_addr getAddressIPv4() const { return m_type >= IPv4 ? m_ipv4 : IPv4_Error; }
		in6_addr getAddressIPv6() const { return m_type >= IPv4 ? m_ipv6 : IPv6_Error; }
		const char* getDomain() const { return m_type >= Domain ? m_domain : ""; }
		const char* getDevice() const { return m_type >= Device ? m_domain : ""; }
		const char* getMTP() const { return m_type >= MTP ? m_domain : ""; }
		uint16_t getPort() const { return m_port; }

		void setPort(uint16_t port) { m_port = port; }

		void setAddressIPv4(in_addr address, uint16_t port = 0) { clear(); m_type = IPv4; m_ipv4 = address; m_port = port; }
		void setAddressIPv6(in6_addr address, uint16_t port = 0) { clear(); m_type = IPv6; m_ipv6 = address; m_port = port; }
		void setDomain(const char* domain, uint16_t port = 0) { clear(); m_type = Domain; m_domain = rtl::strdup(domain); m_port = port; }
		void setDevice(const char* device) { clear(); m_type = Device; m_domain = rtl::strdup(device); m_port = 0; }
		void setMTP(const char* mtp) { clear(); m_type = MTP; m_domain = rtl::strdup(mtp); m_port = 0; }

		// for other types parse function
		RTX_MG_API bool parse(const char* value);
		RTX_MG_API rtl::MemoryStream& writeTo(rtl::MemoryStream& stream, MessageWriterParams* params);
	};
}
//--------------------------------------
