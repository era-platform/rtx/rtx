/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mmt_video_font.h"


namespace media
{
	class FontManager
	{
	public:
		static RTX_MMT_API bool loadFontDirectory(const char* path);
		static RTX_MMT_API bool loadFont(const char* path);
		static RTX_MMT_API void destroy();

		static RTX_MMT_API const Font* createFont(const char* facename, int pixHeight);
		static RTX_MMT_API void releaseFont(const Font* font);

		static RTX_MMT_API const Font* getDefaultFont();
	};

	RTX_MMT_API void printFontInfo(const char* filepath);
	RTX_MMT_API void printDefFontInfo();
	RTX_MMT_API void printFont(const char* filepath);
	RTX_MMT_API void printDefFont();
	RTX_MMT_API void testBitmapChars(const char* bmpPath);
	RTX_MMT_API void testBitmapText(const char* bmpPath);
	RTX_MMT_API void testBitmapText2(const char* bmpPath);
}
//-----------------------------------------------

