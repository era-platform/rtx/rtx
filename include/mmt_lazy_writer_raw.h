/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mmt_lazy_writer.h"

namespace media
{
	//--------------------------------------
	// ������ � raw ���� ��������� ��������� ������ ��� ����
	//--------------------------------------
	class LazyMediaWriterRaw : public LazyMediaWriter
	{
		rtl::FileStream m_file;					// ���������� ����� ������

	public:
		RTX_MMT_API LazyMediaWriterRaw(rtl::Logger* log);
		RTX_MMT_API virtual ~LazyMediaWriterRaw();

		RTX_MMT_API bool create(const char* path, int bufsize);
		RTX_MMT_API void close();

		RTX_MMT_API bool write_packet(const rtp_packet* packet);

	private:
		virtual void rec_file_started();
		virtual void rec_file_stopped();
		virtual bool rec_file_write(void* data_block, uint32_t length, uint32_t* written);
	};
}
//--------------------------------------
