﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

 //--------------------------------------
// configuration files
//--------------------------------------
#include "std_basedef.h"
#include "std_typedef.h"
#include "std_funcdef.h"
//--------------------------------------
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the RTX_MEDIA_TOOLS_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// RTX_MEDIA_TOOLS_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
//--------------------------------------
#if defined (TARGET_OS_WINDOWS)
#ifdef RTX_MEDIA_TOOLS_EXPORTS
  #define RTX_MMT_API __declspec(dllexport)
  #ifdef _DEBUG
    #define RTX_DEBUG_API __declspec(dllexport)
  #else
    #define RTX_DEBUG_API
  #endif
#else
  #define RTX_MMT_API __declspec(dllimport)
  #define RTX_DEBUG_API
#endif
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
#define RTX_MMT_API
#define RTX_DEBUG_API
#endif

#include "rtp_packet.h"

//--------------------------------------
//
//--------------------------------------
#define MG_MAKEFOURCC(ch0, ch1, ch2, ch3)									\
    ((uint32_t)(uint8_t)(ch0) | ((uint32_t)(uint8_t)(ch1) << 8) |			\
    ((uint32_t)(uint8_t)(ch2) << 16) | ((uint32_t)(uint8_t)(ch3) << 24 ))
//--------------------------------------
// сигнатура файла записи медийных данных
//--------------------------------------
#define		MG_REC_AUDIO_SIGNATURE		MG_MAKEFOURCC('R','T','P','A')
#define		MG_REC_VIDEO_SIGNATURE		MG_MAKEFOURCC('R','T','P','V')
#define		MG_REC_WAVE_SIGNATURE		MG_MAKEFOURCC('W','A','V','E')

//-----------------------------------------------
