/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

//--------------------------------------
// standard C string functions macro redefinitions
//--------------------------------------
#if defined(TARGET_OS_WINDOWS)

#define std_snprintf _snprintf
#define std_snwprintf _snwprintf
#define std_stricmp _stricmp
#define std_vsnprintf _vsnprintf
#define std_strnicmp _strnicmp
#define std_strlen(s) ((int)strlen(s))

#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
#define std_snprintf snprintf
#define std_snwprintf swprintf
#define std_vsnprintf vsnprintf
#define std_stricmp strcasecmp
#define std_strnicmp strncasecmp
#define std_strlen strlen
#endif

namespace rtl
{

	typedef const char* ZString;

	//--------------------------------------
	//
	//--------------------------------------
	inline const char* strchr(const char* text, int symbol, int length) { return (const char*)memchr(text, symbol, length); }
	//--------------------------------------
	// zstring
	//--------------------------------------
	RTX_STD_API char* strdup(const char* src, int len = -1);
	RTX_STD_API char* strupdate(char*& ptr, const char* src, int len = -1);
	RTX_STD_API int strupdatel(char*& ptr, const char* src, int len = -1);
	RTX_STD_API char* strupr(char* str);
	RTX_STD_API char* strlwr(char* str);
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API char* strtrim(char* text, const char* char_set);
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API int sprintf(char*& ptr, const char* format, ...);
	RTX_STD_API int vsprintf(char*& ptr, const char* format, va_list* ap);
	RTX_STD_API const char* strtok(const char* str, int delimeter);
	RTX_STD_API int strcmp(const char* l, const char* r);
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API const char* skip_LWSP(const char* text);
	RTX_STD_API const char* skip_SEP(const char* text);
	RTX_STD_API const char* next_LWSP(const char* text);
	RTX_STD_API const char* next_SEP(const char* text);
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API uint32_t hex_to_uint(const char* text, int length);
	RTX_STD_API int hex_to_bytes(const char* text, int length, uint8_t* buffer, int size);
	RTX_STD_API int bytes_to_hex(char* stream, int size, const uint8_t* buffer, int length);
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API int utf8_to_locale(char* buffer, int size, const char* str, int length);
	RTX_STD_API int utf8_to_wcs(wchar_t* dest, int size, const char* utf8, int length);
	//--------------------------------------
	//
	//--------------------------------------
	RTX_STD_API int escape_rfc3986(char* buffer, int size, const char* str, int length = -1);
	RTX_STD_API char* unescape_rfc3986(char* buffer, int size, const char* str, int length = -1);
	//--------------------------------------
	// ������� ������ ����� c ������ ��� ���(=) �������� ���(;)
	//--------------------------------------
	struct str_scanner_t
	{
		const char* ptr;
		int nvd; // name value div
		int pd;	// pair div
		char name[128];
		char value[128];
	};
	RTX_STD_API bool str_scanner_init(str_scanner_t* ctx, const char* str, char nvd = '=', char pv = ';');
	RTX_STD_API bool str_scanner_next(str_scanner_t* ctx);
	//-----------------------------------------------
	// math helpers
	//-----------------------------------------------
	union Number
	{
		int64_t integer;
		double real;
	};
	enum class NumberType { Real, Integer, Error };
	NumberType StrToNumber(const char* str, const char** end, Number& num_ref);
	bool StrToInt64(const char* str, char** end, int64_t& int_ref);
	bool StrToInt32(const char* str, char** end, int32_t& int_ref);
	NumberType StrToNumber(const wchar_t* str, wchar_t** end, Number& num_ref);
	bool StrToInt64(const wchar_t* str, wchar_t** end, int64_t& int_ref);
	bool StrToInt32(const wchar_t* str, wchar_t** end, int32_t& int_ref);
}
//-----------------------------------------------
