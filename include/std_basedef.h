﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#pragma once

//--------------------------------------
// Операционная система
//--------------------------------------

#if defined(_WIN32)

	#if !defined(TARGET_OS_WINDOWS)
		#define TARGET_OS_WINDOWS
	#endif

#elif defined(__linux__)

	#if !defined(TARGET_OS_LINUX)
		#define TARGET_OS_LINUX
	#endif

#elif defined(__FreeBSD__)

	#if !defined(TARGET_OS_FREEBSD)
		#define TARGET_OS_FREEBSD
	#endif

#else

	#error unsupported operating system!

#endif

//--------------------------------------
// Компилятор
//--------------------------------------

#if defined(_MSC_VER)
	#if !defined(COMPILER_MSVC)
		#define	COMPILER_MSVC
		#define DECLARE extern
	#endif
#elif defined(__GNUC__)
	#if !defined(COMPILER_GCC)
		#define	COMPILER_GCC
		#define DECLARE
	#endif
#else
	#error unsupported compiler!
#endif

//--------------------------------------
// Архитектура
//--------------------------------------

#if defined(COMPILER_MSVC)

	#if defined(WIN64) || defined(_M_X64) || defined(_M_AMD64)

		#if !defined(TARGET_ARCH_X64)
			#define TARGET_ARCH_X64
		#endif

	#elif defined(_M_I86) || defined(_M_IX86)

		#if !defined(TARGET_ARCH_X86)
			#define TARGET_ARCH_X86
		#endif

	#else

		#error unsupported architecture!

	#endif

#elif defined(COMPILER_GCC)

	#if defined(__amd64__) || defined(__amd64) || defined(__x86_64__) || defined(__x86_64)

		#if !defined(TARGET_ARCH_X64)
			#define TARGET_ARCH_X64
		#endif

	#elif defined(i386) || defined(__i386) || defined(__i386__)

		#if !defined(TARGET_ARCH_X86)
			#define TARGET_ARCH_X86
		#endif

	#else

		#error unsupported architecture!

	#endif
	
#endif

//--------------------------------------
// Соглашение вызова для экспортируемого интерфейса.
//--------------------------------------

#if !defined(RTX_CALLCONV)

	#if defined(COMPILER_MSVC)

        #define RTX_CALLCONV __cdecl

    #elif defined(COMPILER_GCC)

        #if defined(TARGET_ARCH_X86)
            #define RTX_CALLCONV __attribute__((cdecl))
        #else
            #define RTX_CALLCONV // для x64 какое-то другое соглашение, компилер ругается на cdecl
        #endif

    #endif

#endif

//--------------------------------------
// Главный хэдер винды
//--------------------------------------

#if defined(TARGET_OS_WINDOWS)

	#if !defined(WIN32_LEAN_AND_MEAN)
		#define WIN32_LEAN_AND_MEAN
	#endif

	#if !defined (_CRT_SECURE_NO_WARNINGS)
		#define _CRT_SECURE_NO_WARNINGS
	#endif

	// !!! отключение варнинга при экспортировании STL-классов
	#pragma warning(disable:4251)

	//#include <Windows.h>

#endif
//--------------------------------------
