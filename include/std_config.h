﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

namespace rtl
{
	//--------------------------------------
	//
	//--------------------------------------
	class std_config_t
	{
		struct config_param_t
		{
			char* name;
			char* value;
		};
		typedef const char* ZString;
		Mutex m_sync;
		SortedArrayT<config_param_t, ZString> m_param_list;

	public:
		RTX_STD_API std_config_t();
		RTX_STD_API ~std_config_t();

		RTX_STD_API bool read(const char* config_file);
		RTX_STD_API void close();

		RTX_STD_API const char* get_config_value(const char* key, bool* result = nullptr);
		RTX_STD_API bool set_config_value(const char* key, const char* value);

		RTX_STD_API bool print(FILE* out);

	private:
		void add_param(const char* param);
		void add_new_param(const char* key, const char* value);
		void update_param(config_param_t* param, const char* new_value);
		static int compare_param(const config_param_t& left, const config_param_t& right);
		static int compare_key(const ZString& key, const config_param_t& item);
	};
}
extern RTX_STD_API rtl::std_config_t Config;
//--------------------------------------
