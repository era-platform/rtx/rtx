/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_value.h"
//--------------------------------------
// AuthenticationInfo, Authorization, ProxyAuthenticate, ProxyAuthorization,
// WWWAuthenticate
//--------------------------------------
class sip_value_auth_t : public sip_value_t
{
protected:
	rtl::String m_auth_scheme;
	mime_param_list_t m_auth_params;

protected:
	virtual rtl::String& prepare(rtl::String& stream) const;
	virtual bool parse();
	const char* read_parameter(const char* stream, bool& result);

public:
	sip_value_auth_t() { }
	sip_value_auth_t(const sip_value_auth_t& v) { assign(v); }
	virtual ~sip_value_auth_t();

	virtual void cleanup();
	virtual sip_value_t* clone() const;

	sip_value_auth_t& operator = (const sip_value_auth_t& v) { return assign(v); }
	sip_value_auth_t& assign(const sip_value_auth_t& v);

	virtual const char* read_from(const char* stream, bool& result);

	void set_scheme(const rtl::String& scheme) { m_auth_scheme = scheme; }
	const rtl::String& get_scheme() const { return m_auth_scheme; }

	const rtl::String& get_auth_param(const char* name, bool& result) const { return m_auth_params.get(name, result); }
	const rtl::String& get_auth_param(const char* name) const { return m_auth_params.get(name); }
	bool get_auth_param(const char* name, rtl::String& result) const { return m_auth_params.get(name, result); }
	void set_auth_param(const char* name, const rtl::String& value) { m_auth_params.set(name, value); }
	void remove_auth_param(const char* name) { m_auth_params.remove(name); }

	int get_auth_param_count() const { return m_auth_params.getCount(); }
	const mime_param_t* get_auth_param_at(int index) const { return m_auth_params.getAt(index); }
	void remove_auth_param_at(int index) { m_auth_params.removeAt(index); }
	void cleanup_auth_params() { m_auth_params.cleanup(); }
};
//--------------------------------------
