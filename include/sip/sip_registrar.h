/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_message.h"
//-----------------------------------------------
// contact information
//-----------------------------------------------
class sip_registrar_contact_info_t
{
	sip_transport_route_t m_route;
	sip_transport_point_t m_via;
	//
	sip_transport_point_t m_contact;
	uint32_t m_expires;
	//
	in_addr m_real_address;
	bool m_is_over_nat;
	//
	rtl::String m_callid;
	rtl::String m_contact_params;
	rtl::String m_user;

public:
	sip_registrar_contact_info_t();
	~sip_registrar_contact_info_t() { }

	sip_registrar_contact_info_t(const sip_registrar_contact_info_t& info);
	sip_registrar_contact_info_t& operator = (const sip_registrar_contact_info_t& info);

	const sip_transport_route_t& getRoute() const { return m_route; }
	sip_transport_type_t getType() const { return m_route.type; }
	in_addr get_remote_address() const { return m_route.remoteAddress; }
	uint16_t get_remote_port() const { return m_route.remotePort; }
	in_addr get_real_address() const { return m_real_address; }
	in_addr get_if_address() const { return m_route.if_address; }
	uint16_t get_if_port() const { return m_route.if_port; }
	const sip_transport_point_t& get_via() const { return m_via; }
	const sip_transport_point_t& get_contact() const { return m_contact; }
	uint32_t get_expires() const { return m_expires; }
	bool is_over_nat() const { return m_is_over_nat; }
	const char* get_contact_params() const { return m_contact_params; }
	const char* get_callid() const { return m_callid; }
	const char* get_user() const { return m_user; }

	void set_route(const sip_transport_route_t& route) { m_route = route; }
	void set_via(const sip_transport_point_t& point) { m_via = point; }
	void set_contact(const sip_transport_point_t& point) { m_contact = point; }
	void set_contact_port(uint16_t port) { m_contact.port = port; }
	void set_real_address(in_addr addr) { m_real_address = addr; }
	void set_expires(uint32_t expires) { m_expires = expires; }
	void set_over_nat_flag(bool flag) { m_is_over_nat = flag; }
	void set_contact_params(const char* params) { m_contact_params = params; }
	void set_callid(const char* callid) { m_callid = callid; }
	void set_user(const char* user) { m_user = user; }
};
//--------------------------------------
//
//--------------------------------------
class sip_registrar_account_t;
class sip_registrar_session_t;
class sip_user_agent_t;
class sip_stack_t;
class sip_stack_event_t;
//--------------------------------------
//
//--------------------------------------
enum sip_unregister_event_reason_t
{
	sip_unregister_request_e,
	sip_unregister_replace_e,
	sip_unregister_disabled_e,
	sip_unregister_reset_e,
	sip_unregister_failed_e,
	sip_unregister_expires_e,
};
const char* get_sip_unregister_event_reason(sip_unregister_event_reason_t reason);
//--------------------------------------
// contact_uri = "callid\r\ncontact_uri"
//--------------------------------------
struct sip_registrar_event_handler_t
{
	virtual void sip_registrar_account_registered(const sip_registrar_account_t& sender, sip_registrar_contact_info_t* contact_info, int registered_count) = 0;
	/// ��������� �������� replaced: true - ������ ���� �������� � ������ ��������, false - ������ ������!
	virtual void sip_registrar_account_unregistered(const sip_registrar_account_t& sender, sip_registrar_contact_info_t* contact_info, int registered_count, sip_unregister_event_reason_t reason) = 0;
	virtual void sip_registrar_account_subscribed(const sip_registrar_account_t& sender, const sip_message_t& subscribe, sip_message_t& response) = 0;
};
//--------------------------------------
// registrar serializer
//--------------------------------------
struct sip_registrar_serializer_t
{
	virtual void sip_registrar_write_regdb(const char* xml_record) = 0;
	virtual bool sip_registrar_is_over_nat(const char* request_host, in_addr iface, in_addr r_addr) = 0;
};
//--------------------------------------
// subscribe info
//--------------------------------------
struct sip_registrar_subscribe_info_t
{
	char			call_id[256];					//  0 !Call-Id
	int				expires;						//  4 !Expires
	int				min_expires;					//  8 Min-Expires
	char			to_uri[256];					// 12 !To
	char			from_uri[256];					// 16 !From
	char			accept[256];					// 20 Accept
	char			accept_encoding[256];			// 24 Accept-Encoding
	char			accept_language[256];			// 28 Accept-Language
	char			alert_info[256];				// 32 Alert-Info
	char			retry_after[256];				// 36 Retry-After
	char			event[256];						// 40 !Event
	char			allow_events[256];				// 44 Allow-Events
	char			subscription_state[256];		// 48 !Subscription-State
	char			content_disposition[256];		// 52 Content-Disposition
	char			content_encoding[256];			// 56 Content-Encoding
	char			content_language[256];			// 60 Content-Language
	char			content_type[256];				// 64 Content-Type
	uint8_t*		content;						// 68 Content
	int				content_size;					// 72 Content size (internal)
	int				content_length;					// 76 Content-Length
	int				response_code;					// 80 response code
	char			response_text[256];				// 84 response text
};
//--------------------------------------
//
//--------------------------------------
typedef const char* ZString;
typedef sip_registrar_account_t* sip_registrar_account_ptr;
//--------------------------------------
// registrar
//--------------------------------------
class sip_registrar_t : public rtl::ITimerEventHandler, public rtl::async_call_target_t
{
	rtl::Mutex m_sync;
	rtl::SortedArrayT<sip_registrar_account_ptr, ZString> m_account_list;
	sip_user_agent_t& m_user_agent;
	sip_registrar_serializer_t* m_serializer;
	volatile bool m_started;
	volatile bool m_write_changes_to_regdb;

public:
	sip_registrar_t(sip_user_agent_t& useragent);
	~sip_registrar_t();

	void setup(sip_registrar_serializer_t* serializer);

	void start();
	void stop();

	bool set_account(sip_registrar_event_handler_t* event_handler, const char* username, const char* auth_id, const char* password, int max_expires = 0);
	void remove_account(const char* username);
	void enable_account(const char* username);
	void disable_account(const char* username);
	//sip_registrar_account_t* get_account(const char* username);
	void set_max_expires(const char* username, int max_expires);

	int get_contacts(const char* username, rtl::PonterArrayT<sip_registrar_contact_info_t>& contacts);
	bool check_contact(const char* username, sip_registrar_contact_info_t* contact);

	void send_subscription_notify(const char* username, const sip_registrar_subscribe_info_t* notify_info);

	///
	void process_transport_disconnected(const sip_transport_route_t& route);
	void process_incoming_request(sip_stack_event_t* sip_event);
	
	void send_response(const sip_message_t& request, sip_status_t answer_code);
	void send_response(sip_message_t& response);
	void send_request(sip_message_t& response);

	sip_stack_t& get_stack();

	bool is_over_nat(const char* request_host, in_addr iface, in_addr r_addr);

private:
	// timer_event_handler_t
	virtual void timer_elapsed_event(rtl::Timer* sender, int tid);

	// async_call_target_t
	virtual const char* async_get_name();
	virtual void async_handle_call(uint16_t call_id, uintptr_t int_param, void* ptr_param);

	sip_registrar_account_t* find_account(const char* username, int* pindex = nullptr);
	
	static int compare_by_obj(const sip_registrar_account_ptr& left, const sip_registrar_account_ptr& right);
	static int compare_by_key(const ZString& left, const sip_registrar_account_ptr& right);
};
//--------------------------------------
