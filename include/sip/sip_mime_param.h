/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_def.h"
//--------------------------------------
//
//--------------------------------------
class mime_param_t
{
private:
	rtl::String m_name;
	rtl::String m_value;

public:
	mime_param_t() { }
	mime_param_t(const mime_param_t& param) { *this = param; }
	mime_param_t(const rtl::String& name, const rtl::String& value) { m_name = name; m_value = value; }

	mime_param_t& operator = (const mime_param_t& param) { m_name = param.m_name; m_value = param.m_value; return *this; }

	const rtl::String& getName() const { return m_name; }
	void set_name(const rtl::String& name) { m_name = name; }
	const rtl::String& getValue() const { return m_value; }
	void setValue(const rtl::String& value) { m_value = value; }
};
//--------------------------------------
//
//--------------------------------------
class mime_param_list_t
{
private:
	rtl::ArrayT<mime_param_t*> m_array;

private:	
	const mime_param_t* find(const char* name) const;
	mime_param_t* find(const char* name);

public:
	mime_param_list_t() { }
	mime_param_list_t(const mime_param_list_t& param_array) { assign(param_array); }
	~mime_param_list_t() { cleanup(); }

	mime_param_list_t& operator = (const mime_param_list_t& params_array) { return assign(params_array); }
	mime_param_list_t& assign(const mime_param_list_t& params_array);

	const rtl::String& get(const char* name, bool& result) const;
	const rtl::String& get(const char* name) const;
	bool get(const char* name, rtl::String& result) const;

	void set(const char* name, const rtl::String& value);
	bool contains(const char* name) const { return find(name) != nullptr; }
	void remove(const char* name);

	int getCount() const { return m_array.getCount(); }
	mime_param_t* getAt(int index) { return m_array[index]; }
	const mime_param_t* getAt(int index) const { return m_array[index]; }
	void removeAt(int index);
	void cleanup();

	static int read_from(const char* stream, mime_param_list_t& list);
};
//--------------------------------------
