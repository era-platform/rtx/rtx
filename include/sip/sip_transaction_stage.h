/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_transaction.h"
//--------------------------------------
//
//--------------------------------------
class sip_transaction_stage_t;
//--------------------------------------
//
//--------------------------------------
class SIPTransactionWorker : rtl::IThreadUser
{
	sip_transaction_stage_t& m_owner;
	rtl::Thread m_thread;

	void process_transaction();
	virtual void thread_run(rtl::Thread* thread);

public:
	SIPTransactionWorker(sip_transaction_stage_t& owner);
	~SIPTransactionWorker();

	bool start();
	void stop();
};
//--------------------------------------
//
//--------------------------------------
typedef rtl::ArrayT<SIPTransactionWorker*> SIPTransactionWorkerList;
typedef rtl::QueueT<sip_transaction_t*> SIPTransactionQueue;
typedef rtl::ArrayT<sip_transaction_t*> SIPTransactionArray;
//--------------------------------------
//
//--------------------------------------
class sip_transaction_stage_t : public rtl::ITimerEventHandler, public rtl::async_call_target_t
{
	bool m_started;
	/// Event Queue
	rtl::Semaphore m_queue_sem;
	rtl::MutexWatch m_queue_lock;
	SIPTransactionQueue m_queue;
	SIPTransactionWorkerList m_queue_thread_pool;

	/// Cleaner thread
	rtl::MutexWatch m_cleaner_lock;
	SIPTransactionArray m_cleaner_list;

public:
	sip_transaction_stage_t(int threadCount);
	virtual ~sip_transaction_stage_t();

	bool start();
	void stop();

	void push_transaction(sip_transaction_t* transaction);
	sip_transaction_t* get_transaction();
	void terminate_transaction(sip_transaction_t* transaction);

	/// ITimerEvents
	virtual void timer_elapsed_event(rtl::Timer* sender, int tid);

	/// async_call_target_t
	virtual const char* async_get_name();
	virtual void async_handle_call(uint16_t call_id, uintptr_t int_param, void* ptr_param);
};
//--------------------------------------
