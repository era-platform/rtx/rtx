/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_uri.h"
//--------------------------------------
//
//--------------------------------------
class sip_request_line_t
{
	rtl::String m_method;
	sip_uri_t m_uri;
	rtl::String m_version;

public:
	sip_request_line_t() { }
	sip_request_line_t(const rtl::String& method, const sip_uri_t& uri, const rtl::String& version) : m_method(method), m_uri(uri), m_version(version) { }

	const rtl::String& get_method() const { return m_method; }
	void set_method(const rtl::String& method) { m_method = method; }
	const sip_uri_t& get_uri() const { return m_uri; }
	void set_uri(const sip_uri_t& uri) { m_uri = uri; }
	const rtl::String& get_version() const { return m_version; }
	void set_version(const rtl::String& version) { m_version = version; }
};
//--------------------------------------
//
//--------------------------------------
class sip_status_line_t
{
	rtl::String m_version;
	uint16_t m_code;
	rtl::String m_reason;

public:
	sip_status_line_t() { }
	sip_status_line_t(const rtl::String& version, uint16_t code, const rtl::String& reason) : m_version(version), m_code(code), m_reason(reason) { }

	const rtl::String& get_version() const { return m_version; }
	void set_version(const rtl::String& version) { m_version = version; }
	uint16_t get_code() const { return m_code; }
	void set_code(uint16_t code) { m_code = code; }
	const rtl::String& get_reason() const { return m_reason; }
	void set_reason(const rtl::String& reason) { m_reason = reason; }
};
//--------------------------------------
//
//--------------------------------------
class sip_start_line_t
{
	sip_request_line_t* m_request;
	sip_status_line_t* m_response;

	void cleanup();

public:
	sip_start_line_t() : m_request(nullptr), m_response(nullptr) { }
	sip_start_line_t(const rtl::String& method, const sip_uri_t& uri, const rtl::String& version) : m_response(nullptr) { m_request = NEW sip_request_line_t(method, uri, version); }
	sip_start_line_t(const rtl::String& version, uint16_t code, const rtl::String& reason) : m_request(nullptr) { m_response = NEW sip_status_line_t(version, code, reason); }

	~sip_start_line_t() { cleanup(); }

	void set_request_line(const rtl::String& method, const sip_uri_t& uri, const rtl::String& version);
	void set_status_line(const rtl::String& version, uint16_t code, const rtl::String& reason);

	bool is_request() const { return m_request != nullptr; }
	bool is_response() const { return m_response != nullptr; }

	const rtl::String& get_method() const { return m_request != nullptr ? m_request->get_method() : rtl::String::empty; }
	void set_method(const rtl::String& method) { m_request != nullptr ? m_request->set_method(method) : (void)0; }
	const sip_uri_t& get_uri() const { return m_request != nullptr ? m_request->get_uri() : sip_uri_t::empty; }
	void set_uri(const sip_uri_t& uri) { m_request != nullptr ? m_request->set_uri(uri) : (void)0; }

	uint16_t get_code() const { return m_response != nullptr ? m_response->get_code() : 0; }
	void set_code(uint16_t code) { m_response != nullptr ? m_response->set_code(code) : (void)0; }
	const rtl::String& get_reason() const { return m_response != nullptr ? m_response->get_reason() : rtl::String::empty; }
	void set_reason(const rtl::String& reason) { m_response != nullptr ? m_response->set_reason(reason) : (void)0; }

	const rtl::String& get_version() const { return m_request != nullptr ? m_request->get_version() : m_response != nullptr ? m_response->get_version() : rtl::String::empty; }
	void set_version(const rtl::String& version) const { m_request != nullptr ? m_request->set_version(version) : m_response != nullptr ? m_response->set_version(version) : (void)0; }
};
//--------------------------------------
