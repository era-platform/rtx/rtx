﻿/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_session.h"
#include "sip_stack.h"
#include "sip_registrar.h"
#include "sip_profile.h"
//--------------------------------------
//
//--------------------------------------
class sip_call_session_manager_t;
class sip_call_session_t;
//--------------------------------------
//
//--------------------------------------
enum reinvite_response_t
{
	reinvite_response_wait_e,
	reinvite_response_ok_e,				// 200
	reinvite_response_not_accepted_e,	// 488
	reinvite_response_error_e,			// 500
};
const char* get_reinvite_response_name(reinvite_response_t code);
//--------------------------------------
//
//--------------------------------------
struct call_session_event_handler_t
{
	//--------------------------
	// получено ответ 1xx Trying
	virtual void call_session__recv_1xx(sip_call_session_t& session, const sip_message_t& message) = 0;
	// соединение установленно (получен или отправлени 200 OK)
	virtual void call_session__call_established(sip_call_session_t& session, const sip_message_t& message) = 0;
	// соединение разорванно
	virtual void call_session__call_disconnected(sip_call_session_t& session, const sip_message_t& message) = 0;

	// подтверждение ACK отправленно в сеть
	virtual void call_session__ack_sent(sip_call_session_t& session) = 0;
	// получено подтверждение ACK
	virtual void call_session__recv_ack(sip_call_session_t& session, const sip_message_t& msg) = 0;

	// входящий реинвайт
	virtual reinvite_response_t call_session__recv_reinvite(sip_call_session_t& session, const sip_message_t& request) = 0;
	// получен ответ на исходящий реинвайт
	virtual void call_session__call_reestablished(sip_call_session_t& session, const sip_message_t& message) = 0;

	// входящее предложение
	virtual bool call_session__apply_sdp_offer(sip_call_session_t& session, const sip_message_t& offer) = 0;
	// исходящий ответ
	virtual bool call_session__prepare_sdp_answer(sip_call_session_t& session, sip_message_t& answer) = 0;
	// исходящее предложение
	virtual bool call_session__prepare_sdp_offer(sip_call_session_t& session, sip_message_t& offer) = 0;
	// входящий ответ
	virtual bool call_session__apply_sdp_answer(sip_call_session_t& session, const sip_message_t& answer) = 0;

	// входящее предложение REINVITE
	virtual bool call_session__apply_sdp_reoffer(sip_call_session_t& session, const sip_message_t& offer) = 0;
	// исходящий ответ REINVITE
	virtual bool call_session__prepare_sdp_reanswer(sip_call_session_t& session, sip_message_t& answer) = 0;
	// исходящее предложение REINVITE
	virtual bool call_session__prepare_sdp_reoffer(sip_call_session_t& session, sip_message_t& offer, bool hold) = 0;
	// входящий ответ REINVITE
	virtual bool call_session__apply_sdp_reanswer(sip_call_session_t& session, const sip_message_t& anser) = 0;

	/// сообщения в диалоге
	//--------------------------
	// получен DTMF
	//virtual void call_session__recv_dtmf(sip_call_session_t& session, const rtl::String& digit, int duration) = 0;
	//--------------------------
	// получен НЕ INVITE запрос 
	virtual bool call_session__recv_ni_request(sip_call_session_t& session, const sip_message_t& request, sip_message_t& response) = 0;
	virtual void call_session__recv_ni_response(sip_call_session_t& session, const sip_message_t& response) = 0;
	//--------------------------
	// ошибка сип сессии
	virtual void call_session__failure(sip_call_session_t& session) = 0;

};
//--------------------------------------
//
//--------------------------------------
class sip_call_session_t : public sip_session_t 
{
public:
	enum SessionEvents
	{
		// Invite Client Transaction Events
		ICT_Recv1xx = 101,	ICT_Recv2xx,		ICT_Recv3xx,
		ICT_Recv4xx,		ICT_Recv5xx,		ICT_Recv6xx,
		ICT_RecvUnknown,	ICT_InviteSent,		ICT_AckSent,
		ICT_UnknownSent,
		// None Invite Client Transaction Events
		NICT_Recv1xx,		NICT_Recv2xx,		NICT_Recv3xx,
		NICT_Recv4xx,		NICT_Recv5xx,		NICT_Recv6xx,
		NICT_RecvUnknown,	NICT_ByeSent,		NICT_OptionsSent,
		NICT_InfoSent,		NICT_CancelSent,	NICT_NotifySent,
		NICT_SubscribeSent,	NICT_UnknownSent,
		// Invite Server Transaction Events
		IST_RecvInvite,		IST_RecvMoreInvite,	IST_RecvAck,
		IST_RecvUnknown,	IST_1xxSent,		IST_2xxSent,
		IST_3xxSent,		IST_4xxSent,		IST_5xxSent,
		IST_6xxSent,		IST_UnknownSent,
		// None Invite Server Transaction Events
		NIST_RecvBye,		NIST_RecvOptions,	NIST_RecvInfo,
		NIST_RecvCancel,	NIST_RecvNotify,	NIST_RecvSubscribe,
		NIST_RecvRefer,		NIST_RecvMessage,	NIST_RecvUpdate,
		NIST_RecvUnknown,	NIST_1xxSent,		NIST_2xxSent,
		NIST_3xxSent,		NIST_4xxSent,		NIST_5xxSent,
		NIST_6xxSent,		NIST_UnknownSent,
		// answer call events
		IST_AnswerCallNow,                // Sends 200 OK back with SDP after receipt of INVITE
		IST_AnswerCallDeferred,           // Answers with 180 ringing
		IST_AnswerCallDeferredWithMedia,  // Like AnswerCallDefered Only media is sent in 183 progress
		IST_AnswerCallQueued,             // Tell remote that the call is queued
		IST_AnswerCallDenied,             // Reject the call
		IST_200OkRetransmitTimeout,       // Retransmision of 200 Ok  has expired
		IST_SendUnauthorized,             // Sends a proxy authentication challenge
		ICT_DisconnectCall,				  // cancel or bye call
	};
	static const char* GetEventName(int event);

	enum SessionState
	{
		StateIdle,			StateNewCall,			StateProceeding,
		StateAlerting,		StateQueued,			StateMaxAnswerCall,
		StateCancelling,	StateConnected,			StateDisconnected
	};
	static const char* GetSessionStateName(SessionState state);

	enum AnswerCallResponse
	{
		AnswerCallNow,                // Sends 200 OK back with SDP after receipt of INVITE
		AnswerCallDeferred,           // Answers with 180 ringing
		AnswerCallDeferredWithMedia,  // Like AnswerCallDefered Only media is sent in 183 progress
		AnswerCallQueued,             // Tell remote that the call is queued
		AnswerCallDenied,             // Reject the call
		NumAnswerCall
	};
	static const char* GetAnswerCallResponseName(AnswerCallResponse mode);

	enum ICT_State
	{
		ICT_StateIdle,
		ICT_StateTrying,
		ICT_StateAlerting,
		ICT_StateConfirmed
	};
	
	enum ReinviteType
	{
		ReinviteNone,
		ReinviteIncoming,
		ReinviteOutgoing,
	};

private:
	friend class sip_call_session_manager_t;

	call_session_event_handler_t* m_trunk;		// обработчик событий
	sip_call_session_manager_t& m_call_manager;	// менеджер сессий

	rtl::MutexWatch m_event_mutex;				// ?
	rtl::MutexWatch m_answer_call_mutex;			// ?

	SessionState m_state;						// текущее состояние сессии

	rtl::MutexWatch m_custom_headers_lock;		// блокировка списка дополнительных полей заголовка соосбщений
	rtl::ArrayT<sip_header_t*> m_custom_headers;	// добавочные поля в заголовке запросов или ответов (в зависимости от типа сессии)

	int m_call_end_reason;						// причина отбоя (код или метод, напрвление)
	bool m_disconnect_cancel_reason;			// причина отмены вызова (добавление reason=)
	sip_status_t m_disconnect_status_code;		// код отбоя в респонсе
	rtl::String m_disconnect_status_reason;		// строка отбоя

	uint32_t m_last_ist_request_cseq;			// счетчик входящих запросов
	uint32_t m_last_ict_response_cseq;			// счетчик исходящих запросов

	sip_message_t m_current_uac_invite;			// текущий исходящий запрос
	sip_message_t m_current_uas_invite;			// текущий входящий запрос

	bool m_can_send_cancel;						// можно ли отправлять CANCEL
	bool m_send_cancel;							// нужно ли отправлять CANCEL при 1xx (try, e.t.c)
	bool m_cancel_param;						// сохранение параметра SednCancel(reason_200)

	rtl::MutexWatch m_proxy_auth_lock;			// блокировка всего что связанно с авторизацией
	bool m_has_sent_proxy_auth;					// отправлялось ли авторизация
	bool m_has_auth;							// наличие авторизации
	sip_value_auth_t m_auth;					// авторизация
	bool m_has_www_auth;						// наличие WWW авторизации
	sip_value_auth_t m_www_auth;				// WWW авторизация WWWAuthenticate
	uint32_t m_invite_qop_count;				// количестов запросов авторизации с qop
	bool m_ack_received;						// получен ACK
	rtl::MutexWatch m_reinvite_lock;
	ReinviteType m_reinvite_state;				// флаг занятости сессии на реинвайте и упдейте

public:
	sip_call_session_t(sip_call_session_manager_t& manager, const sip_message_t& request);
	sip_call_session_t(sip_call_session_manager_t& manager, const sip_profile_t& profile, const char* callId);
	virtual ~sip_call_session_t();

public:
	bool make_call(call_session_event_handler_t* pTrunk);
	bool make_call_to_target(call_session_event_handler_t* pTrunk, const sip_value_uri_t& referTo, const sip_value_uri_t* referredBy);
	void unbind_trunk_handler();
	/// answers the call.  based on the mode.  May be called several times
	bool AnswerCall(AnswerCallResponse mode);
	bool AnswerReinvite(bool success);
	bool RedirectCall(uint16_t code_3xx, const char* contact, int expires);
	bool SendAcceptByRejection(int statusCode = sip_403_Forbidden, const rtl::String& resonPhrase = rtl::String::empty, const rtl::String& warning = rtl::String::empty);
	bool DisconnectCall(bool cancel_reason, int reject_code, const char* reject_reason);
	bool SendBye();
	bool SendTrying();
	bool SendCancel(bool reason_200);
	bool SendRefer(const sip_value_uri_t& referTo);
	bool SendNotify(const sip_value_t& state, const sip_value_t& sip_event, const sip_value_t& contentType, const rtl::String& body);
	bool SendOptions();
	bool SendHold(bool hold);
	bool SendReinvite();
	bool SendDTMF(char digit, int duration);
	bool SendTextMessage(const char* message);
	void set_custom_headers(const rtl::ArrayT<sip_header_t*>& headers);
	void clear_custom_headers();
	void OnAckFor200OkExpire(const sip_message_t& ret_200ok);

	bool prepare_dialog_request(sip_message_t& request);
	bool send_dialog_request(sip_message_t& request);
	
private:
	bool InternalAnswerCall(AnswerCallResponse mode, const sip_message_t& invite);
	/// sends and retransmits 200 Ok for an INVITE with SDP
	/// the actual response will be generated within the function
	bool SendConnect(const sip_message_t& request);
	/// Sends 182 Call Queued
	bool SendCallQueued(const sip_message_t& request);
	/// Sends 180 Ringing
	bool SendAlerting(const sip_message_t& request);
	/// sends 183 Progress with SDP
	bool SendProgress(const sip_message_t& request);
	/// Sends 100 Trying
	bool SendTrying(const sip_message_t& invite);
	/// Sends a forbidden by default. or GetAnswerCallresponse() if explicitly set
	bool SendReject(const sip_message_t& request, sip_status_t statusCode = sip_403_Forbidden,
					const rtl::String& reasonPhrase = rtl::String::empty, const rtl::String& warning = rtl::String::empty);
	bool SendAck(const sip_message_t& ack);
	///ProxyAuthenticate
	void MakeProxyAuthorization(sip_message_t& message, const sip_value_auth_t& auth);
	///WWWAuthenticate
	void MakeWWWAuthorization(sip_message_t& message, const sip_value_auth_t& auth);

	bool OnReinvite(const sip_message_t& invite);
	bool OnProxyAuthentication(const sip_message_t& auth);
	bool OnWWWAuthentication(const sip_message_t& auth);
	void CheckAndReplaceTransport(const sip_message_t& request, sip_message_t& response);

protected:
	virtual void process_timer_expires_event(sip_event_timer_expires_t& timerEvent);
	virtual void process_transport_failure_event(int failureType);
		
	//// LOW LEVEL CALLBACKS
	virtual bool process_incoming_sip_message(sip_event_message_t& messageEvent);
	virtual void process_sip_message_sent_event(sip_event_tx_t& messageEvent);
	virtual void process_session_event(int event, const sip_message_t& eventMsg);
	virtual void process_session_expire_event();
	virtual void process_destroy_event();

protected:
	void TransportFailure();
	
	void IST_OnReceivedInvite(const sip_message_t& msg);
	void IST_OnReceivedMoreInvite(const sip_message_t& msg);
	void IST_OnReceivedAck(const sip_message_t& msg);
	void IST_On1xxSent(const sip_message_t& msg);
	void IST_On2xxSent(const sip_message_t& msg);
	void IST_On3xxSent(const sip_message_t& msg);
	void IST_On4xxSent(const sip_message_t& msg);
	void IST_On5xxSent(const sip_message_t& msg);
	void IST_On6xxSent(const sip_message_t& msg);
	void IST_OnUnknownSent(const sip_message_t& msg);
	void IST_OnReceivedUnknown(const sip_message_t& msg);
	void IST_200OkRetransmitTimeoutReceived(const sip_message_t& msg);

	void NIST_OnReceivedRegister(const sip_message_t& msg);
	void NIST_OnReceivedBye(const sip_message_t& msg);
	void NIST_OnReceivedOptions(const sip_message_t& msg);
	void NIST_OnReceivedInfo(const sip_message_t& msg);
	void NIST_OnReceivedCancel(const sip_message_t& msg);
	void NIST_OnReceivedNotify(const sip_message_t& msg);
	void NIST_OnReceivedSubscribe(const sip_message_t& msg);
	void NIST_OnReceivedRefer(const sip_message_t& msg);
	void NIST_OnReceivedMessage(const sip_message_t& msg);
	void NIST_OnReceivedUpdate(const sip_message_t& msg);
	void NIST_OnReceivedUnknown(const sip_message_t& msg);

	void NIST_On1xxSent(const sip_message_t& msg);
	void NIST_On2xxSent(const sip_message_t& msg);
	void NIST_On3xxSent(const sip_message_t& msg);
	void NIST_On4xxSent(const sip_message_t& msg);
	void NIST_On5xxSent(const sip_message_t& msg);
	void NIST_On6xxSent(const sip_message_t& msg);
	void NIST_OnUnknownSent(const sip_message_t& msg);

	void NICT_OnRegisterSent(const sip_message_t& msg);
	void NICT_OnByeSent(const sip_message_t& msg);
	void NICT_OnOptionsSent(const sip_message_t& msg);
	void NICT_OnInfoSent(const sip_message_t& msg);
	void NICT_OnCancelSent(const sip_message_t& msg);
	void NICT_OnNotifySent(const sip_message_t& msg);
	void NICT_OnSubscribeSent(const sip_message_t& msg);
	void NICT_OnUnknownSent(const sip_message_t& msg);

	void NICT_OnReceived1xx(const sip_message_t& msg);
	void NICT_OnReceived2xx(const sip_message_t& msg);
	void NICT_OnReceived3xx(const sip_message_t& msg);
	void NICT_OnReceived4xx(const sip_message_t& msg);
	void NICT_OnReceived5xx(const sip_message_t& msg);
	void NICT_OnReceived6xx(const sip_message_t& msg);
	void NICT_OnReceivedUnknown(const sip_message_t& msg);

	void ICT_OnInviteSent(const sip_message_t& msg);
	void ICT_OnAckSent(const sip_message_t& msg);
	void ICT_OnUnknownSent(const sip_message_t& msg);

	void ICT_OnReceived1xx(const sip_message_t& msg);
	void ICT_OnReceived2xx(const sip_message_t& msg);
	void ICT_OnReceived3xx(const sip_message_t& msg);
	void ICT_OnReceived4xx(const sip_message_t& msg);
	void ICT_OnReceived5xx(const sip_message_t& msg);
	void ICT_OnReceived6xx(const sip_message_t& msg);
	void ICT_OnReceivedUnknown(const sip_message_t& msg);
	void ICT_OnDisconnectCall();

	void EnqueueMessageEvent(int event, const sip_message_t& msg);

public:
	SessionState GetState() const { return m_state; }
		
	int GetCallEndReason() const { return m_call_end_reason; }
};
//--------------------------------------
