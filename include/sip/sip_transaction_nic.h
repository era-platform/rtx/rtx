/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_transaction.h" 
//--------------------------------------
//
//--------------------------------------
class sip_transaction_nic_t : public sip_transaction_t
{
public:

	enum nic_transaction_state_t
	{
		nic_transaction_state_idle_e = 0,
		nic_transaction_state_trying_e,
		nic_transaction_state_proceeding_e,
		nic_transaction_state_completed_e,
		nic_transaction_state_terminated_e
	};

	sip_transaction_nic_t(const rtl::String& id, sip_transaction_manager_t& manager);
	virtual ~sip_transaction_nic_t();

	virtual void process_sip_timer_event(sip_transaction_timer_t timerType, uint32_t interval);
	virtual void process_sip_transaction_event(sip_message_t& message);
		
	void process_idle_state(sip_message_t& message);
	void process_trying_state(sip_message_t& message);
	void process_proceeding_state(sip_message_t& message);
	void process_completed_state(sip_message_t& message);

protected:
	bool m_has_sent_initial_request;
};
//--------------------------------------
