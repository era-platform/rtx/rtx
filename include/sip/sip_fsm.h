/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_transaction_manager.h"
//--------------------------------------
//
//--------------------------------------
class sip_transaction_fsm_t : public sip_transaction_manager_t
{
public:
	sip_transaction_fsm_t();

	virtual sip_transaction_t* create_new_transaction(const sip_message_t& event, const trans_id& transId, sip_transaction_type_t type);
};
//--------------------------------------
