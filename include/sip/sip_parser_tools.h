/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

class sip_utils
{
public:
	static void SliceMIME(const rtl::String& mime, rtl::String& name, rtl::String& value);
	static bool IsKnownHeader(const rtl::String&header);
	static bool IsNumeric(const rtl::String&value);
	static rtl::String EscapeAsRFC2396(const rtl::String&str);
	static rtl::String UnescapeAsRFC2396( const rtl::String&str );
	static rtl::String GenGUID();
	static rtl::String GenCallId(const char* cookie);
	static rtl::String GenBranchParameter();
	static rtl::String GenTagParameter();
	static rtl::String GetRFC1123Date();
	static rtl::String GetReasonPhrase(uint16_t statusCode);
	static rtl::String GetExpandedHeader(const rtl::String&compactHeader);
	static const char* get_expanded_header(const char* compactHeader);
	static rtl::String GetCompactedHeader(const rtl::String&expandedHeader);
	static rtl::String AsHex(const uint8_t* digest);
	static rtl::String& Quote(rtl::String& str);
	static rtl::String Quote(const rtl::String& str);
	static rtl::String& UnQuote(rtl::String& str);
	static rtl::String UnQuote(const rtl::String& str);
	static bool WildCardCompare(const char* wild, const char* string); 
	static rtl::String StripHTMLTags(const rtl::String& html);

protected:
	static uint16_t m_DefaultHashKey1;
	static uint8_t m_DefaultHashKey2;
};
//--------------------------------------
