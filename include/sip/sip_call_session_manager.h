﻿/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_session_manager.h"
#include "sip_call_session.h"

class sip_call_session_manager_t;

//--------------------------------------
// 200 OK retransmit classes
//--------------------------------------
class retransmit_record_t
{
	sip_call_session_manager_t& m_manager;

	rtl::String m_call_id;
	bool m_is_expired;
	uint32_t m_start_time;
	uint32_t m_timer_value;
	int m_retransmit_count;
	sip_message_t m_200_ok;

public:
	retransmit_record_t(const sip_message_t& msg, const rtl::String& call_id, sip_call_session_manager_t& manager);
	~retransmit_record_t();

	bool retransmit();
	void set_expired() { m_is_expired = true; }
	const char* get_call_id() { return m_call_id; }
};
//--------------------------------------
//
//--------------------------------------
class retransmit_record_list_t
{
public:
	struct SESS_KEY
	{
		char* call_id;
		retransmit_record_t* record;
	};

	retransmit_record_list_t();
	~retransmit_record_list_t();

	bool add(const char* call_id, retransmit_record_t* session);
	void removeAt(int index);

	retransmit_record_t* find(const char* call_id) const;

	int getCount() { return m_pool.getCount(); }
	retransmit_record_t* getAt(int index) { return m_pool.getAt(index).record; }
	
	void clear() { m_pool.clear(); }

private:
	rtl::SortedArrayT<SESS_KEY, ZString> m_pool;
};
//--------------------------------------
//
//--------------------------------------
class retransmit_thread_t : rtl::IThreadUser
{
	sip_call_session_manager_t& m_manager;
	rtl::MutexWatch m_ret_sync;
	bool m_exit_flag;
	rtl::Thread m_thread;
	rtl::Event m_event;
	retransmit_record_list_t m_retransmit_list;

public:
	retransmit_thread_t(sip_call_session_manager_t& manager);
	~retransmit_thread_t();

	void start_retransmission(const char* call_id, const sip_message_t& message);
	void stop_retransmission(const char* call_id);

private:
	void handle_retransmission();

	virtual void thread_run(rtl::Thread* thread);
};
//--------------------------------------
//
//--------------------------------------
class sip_call_session_manager_t : public sip_session_manager_t
{
private:
	retransmit_thread_t* m_200_ok_retransmit_thread;

	/// constructors and destructors
public:
	sip_call_session_manager_t(sip_user_agent_t& ua, int sessionThreadCount);
	virtual ~sip_call_session_manager_t();

	/// public interface
	virtual void stop();
	virtual call_session_event_handler_t* process_incoming_connection(const sip_message_t& invite, sip_call_session_t& session);
	virtual bool process_incoming_im_message(const sip_message_t& message);
	retransmit_thread_t* get_retransmit_thread() { return m_200_ok_retransmit_thread; }

	/// protected overrides
protected:
	virtual sip_session_t* create_new_client_session(const sip_profile_t& profile, const char* sessionId);
	virtual sip_session_t* create_new_server_session(sip_message_t& request);
	virtual void process_unknown_transaction(sip_event_unknown_transaction_t& event);
	virtual void process_orphaned_message(const sip_message_t& message, sip_status_t scode);
	virtual bool options_set_sdp_info(const sip_message_t& request, sip_message_t& response);

	/// private routines
private:
	void process_received_options(const sip_message_t& options);
	void process_received_message(const sip_message_t& message);
};
//--------------------------------------
