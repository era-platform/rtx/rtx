/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_registrar.h"
//--------------------------------------
//
//--------------------------------------
#define REG_NO_EXPIRES_VALUE	0xFFFFFFFF
#define REG_DEF_EXPIRES_VALUE	3600
#define REG_DEF_SIP_PORT		5060

//--------------------------------------
// contact life state
//--------------------------------------
enum sip_registrar_contact_state_t
{
	sip_registrar_contact_live_e,		// live time less than expires
	sip_registrar_contact_overtime_e,	// live time equal or greater abave 10 sec than expires
	sip_registrar_contact_expires_e,	// live time greater than expires over 10 sec
};
//--------------------------------------
// ���������� ����������
//--------------------------------------
class sip_registrar_contact_t
{
	rtl::String m_display_name;		// ������������ ��� ��������
	sip_transport_point_t m_cp;		// ���������� �����
	uint32_t m_reg_expires;			// ����������������� ����� ������
	rtl::String m_uri_params;		//

public:
	sip_registrar_contact_t();
	sip_registrar_contact_t(const sip_registrar_contact_t& contact);

	~sip_registrar_contact_t();

	sip_registrar_contact_t& operator = (const sip_registrar_contact_t& contact);

	const char* get_display_name() const { return m_display_name; }
	void set_display_name(const char* display_name) { m_display_name = display_name; }
	void set_display_name(const rtl::String& display_name) { m_display_name = display_name; }
	const sip_transport_point_t& get_point() const { return m_cp; }
	/*sip_transport_type_t getType() const { return m_cp.type; }
	void set_type(sip_transport_type_t type) { m_cp.type = type; }
	in_addr get_address() const { return m_cp.address; }
	void set_address(in_addr address) { m_cp.address = address; }
	uint16_t get_port() const { return m_cp.port; }*/
	void set_point(sip_transport_type_t type, in_addr address, uint16_t port) { m_cp.type = type; m_cp.address = address; m_cp.port = port; }
	void set_point(const sip_transport_point_t& point) { m_cp = point; }
	//void set_type(uint16_t port) { m_cp.port = port; }
	void set_port(uint16_t port) { m_cp.port = port; }

	uint32_t get_expires() const { return m_reg_expires; }
	void set_expires(uint32_t expires) { m_reg_expires = expires; }
	const char* get_uri_params() const { return m_uri_params; }
	void set_uri_params(const char* params) { m_uri_params = params; }
	void set_uri_params(const rtl::String& params) { m_uri_params = params; }

	sip_registrar_contact_info_t* get_info();
};
//--------------------------------------
//
//--------------------------------------
class sip_registrar_account_t;
//--------------------------------------
//
//--------------------------------------
class sip_registrar_session_t
{
	sip_registrar_account_t& m_account;
	bool m_registered;
	rtl::String m_call_id;					// ���������� � ��������� �������
	uint32_t m_cseq;
	in_addr m_real_sender_address;			// ���� ���� ���������� ���� OktellProxy-RcvFromAddr: 192.168.0.12
	sip_transport_route_t m_route;
	sip_transport_point_t m_via;
	bool m_over_nat;
	time_t m_last_time;
	rtl::ArrayT<sip_registrar_contact_t*> m_contact_list;

public:
	sip_registrar_session_t(sip_registrar_account_t& account);
	~sip_registrar_session_t();

	const char* get_call_id() const { return m_call_id; }
	uint32_t get_cseq() const { return m_cseq; }

	const sip_transport_point_t& get_via() const { return m_via; }
	const sip_transport_route_t& getRoute() const { return m_route; }
	in_addr get_real_sender_address() const { return m_real_sender_address; }

	bool is_over_nat() const { return m_over_nat; }
	bool behind_nat() const { return m_route.type == sip_transport_udp_e && m_route.remoteAddress.s_addr != m_via.address.s_addr; }

	uint64_t get_reg_time() { return m_last_time; }
	
	int get_top_contact(char* buffer, int size, sip_transport_type_t type = sip_transport_udp_e);
	int get_contacts(rtl::PonterArrayT<sip_registrar_contact_info_t>& contacts) const;
	bool check_contact(sip_registrar_contact_info_t* contact) const;
	sip_registrar_contact_info_t* get_sip_contact() const;
	void reset_expires();

	bool check_transport_disconnected(const sip_transport_route_t& route);
	bool process_request(const sip_message_t& request, bool over_nat);

	bool check_contacts(time_t current_time);

private:
	sip_registrar_contact_state_t check_expired_time(const sip_registrar_contact_t& conact, time_t current_time);
	void clear_contacts();
	bool uri_to_contact(const sip_value_contact_t& contact_uri, sip_registrar_contact_t& contact);
	int contact_to_string(const sip_registrar_contact_t& contact, char* buffer, int size);
};
//--------------------------------------
