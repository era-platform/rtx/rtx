/* RTX SIP Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

//--------------------------------------
// sip header std types
//--------------------------------------

/*
rfc3968.txt

The following are the initial values for this sub-registry.

Header Field                  Parameter Name   Predefined  Reference
Values
_____________________________________________________________________
Accept                        q                    No     [RFC 3261]
Accept-Encoding               q                    No     [RFC 3261]
Accept-Language               q                    No     [RFC 3261]
Authorization                 algorithm           Yes     [RFC 3261]
[[RFC 3310]]
Authorization                 auts                 No     [RFC 3310]
Authorization                 cnonce               No     [RFC 3261]
Authorization                 nc                   No     [RFC 3261]
Authorization                 nonce                No     [RFC 3261]
Authorization                 opaque               No     [RFC 3261]
Authorization                 qop                 Yes     [RFC 3261]
Authorization                 realm                No     [RFC 3261]
Authorization                 response             No     [RFC 3261]
Authorization                 uri                  No     [RFC 3261]
Authorization                 username             No     [RFC 3261]
Authentication-Info           cnonce               No     [RFC 3261]
Authentication-Info           nc                   No     [RFC 3261]
Authentication-Info           nextnonce            No     [RFC 3261]
Authentication-Info           qop                 Yes     [RFC 3261]
Authentication-Info           rspauth              No     [RFC 3261]
Call-Info                     purpose             Yes     [RFC 3261]
Contact                       expires              No     [RFC 3261]
Contact                       q                    No     [RFC 3261]
Content-Disposition           handling            Yes     [RFC 3261]
Event                         id                   No     [RFC 3265]
From                          tag                  No     [RFC 3261]
P-Access-Network-Info         cgi-3gpp             No     [RFC 3455]
P-Access-Network-Info         utran-cell-id-3gpp   No     [RFC 3455]
P-Charging-Function-Addresses ccf                  No     [RFC 3455]
P-Charging-Function-Addresses ecf                  No     [RFC 3455]
P-Charging-Vector             icid-value           No     [RFC 3455]
P-Charging-Vector             icid-generated-at    No     [RFC 3455]
P-Charging-Vector             orig-ioi             No     [RFC 3455]
P-Charging-Vector             term-ioi             No     [RFC 3455]



Camarillo                Best Current Practice                  [Page 4]

 RFC 3968                 SIP Parameter Registry            December 2004


 P-DCS-Billing-Info            called               No     [RFC 3603]
 P-DCS-Billing-Info            calling              No     [RFC 3603]
 P-DCS-Billing-Info            charge               No     [RFC 3603]
 P-DCS-Billing-Info            locroute             No     [RFC 3603]
 P-DCS-Billing-Info            rksgroup             No     [RFC 3603]
 P-DCS-Billing-Info            routing              No     [RFC 3603]
 P-DCS-LAES                    content              No     [RFC 3603]
 P-DCS-LAES                    key                  No     [RFC 3603]
 P-DCS-Redirect                count                No     [RFC 3603]
 P-DCS-Redirect                redirector-uri       No     [RFC 3603]
 Proxy-Authenticate            algorithm           Yes     [RFC 3261]
 [[RFC 3310]]
 Proxy-Authenticate            domain               No     [RFC 3261]
 Proxy-Authenticate            nonce                No     [RFC 3261]
 Proxy-Authenticate            opaque               No     [RFC 3261]
 Proxy-Authenticate            qop                 Yes     [RFC 3261]
 Proxy-Authenticate            realm                No     [RFC 3261]
 Proxy-Authenticate            stale               Yes     [RFC 3261]
 Proxy-Authorization           algorithm           Yes     [RFC 3261]
 [[RFC 3310]]
 Proxy-Authorization           auts                 No     [RFC 3310]
 Proxy-Authorization           cnonce               No     [RFC 3261]
 Proxy-Authorization           nc                   No     [RFC 3261]
 Proxy-Authorization           nonce                No     [RFC 3261]
 Proxy-Authorization           opaque               No     [RFC 3261]
 Proxy-Authorization           qop                 Yes     [RFC 3261]
 Proxy-Authorization           realm                No     [RFC 3261]
 Proxy-Authorization           response             No     [RFC 3261]
 Proxy-Authorization           uri                  No     [RFC 3261]
 Proxy-Authorization           username             No     [RFC 3261]
 Reason                        cause               Yes     [RFC 3326]
 Reason                        text                 No     [RFC 3326]
 Retry-After                   duration             No     [RFC 3261]
 Security-Client               alg                 Yes     [RFC 3329]
 Security-Client               ealg                Yes     [RFC 3329]
 Security-Client               d-alg               Yes     [RFC 3329]
 Security-Client               d-qop               Yes     [RFC 3329]
 Security-Client               d-ver                No     [RFC 3329]
 Security-Client               mod                 Yes     [RFC 3329]
 Security-Client               port1                No     [RFC 3329]
 Security-Client               port2                No     [RFC 3329]
 Security-Client               prot                Yes     [RFC 3329]
 Security-Client               q                    No     [RFC 3329]
 Security-Client               spi                  No     [RFC 3329]
 Security-Server               alg                 Yes     [RFC 3329]
 Security-Server               ealg                Yes     [RFC 3329]
 Security-Server               d-alg               Yes     [RFC 3329]
 Security-Server               d-qop               Yes     [RFC 3329]



 Camarillo                Best Current Practice                  [Page 5]
 
  RFC 3968                 SIP Parameter Registry            December 2004


  Security-Server               d-ver                No     [RFC 3329]
  Security-Server               mod                 Yes     [RFC 3329]
  Security-Server               port1                No     [RFC 3329]
  Security-Server               port2                No     [RFC 3329]
  Security-Server               prot                Yes     [RFC 3329]
  Security-Server               q                    No     [RFC 3329]
  Security-Server               spi                  No     [RFC 3329]
  Security-Verify               alg                 Yes     [RFC 3329]
  Security-Verify               ealg                Yes     [RFC 3329]
  Security-Verify               d-alg               Yes     [RFC 3329]
  Security-Verify               d-qop               Yes     [RFC 3329]
  Security-Verify               d-ver                No     [RFC 3329]
  Security-Verify               mod                 Yes     [RFC 3329]
  Security-Verify               port1                No     [RFC 3329]
  Security-Verify               port2                No     [RFC 3329]
  Security-Verify               prot                Yes     [RFC 3329]
  Security-Verify               q                    No     [RFC 3329]
  Security-Verify               spi                  No     [RFC 3329]
  Subscription-State            expires              No     [RFC 3265]
  Subscription-State            reason              Yes     [RFC 3265]
  Subscription-State            retry-after          No     [RFC 3265]
  To                            tag                  No     [RFC 3261]
  Via                           branch               No     [RFC 3261]
  Via                           comp                Yes     [RFC 3486]
  Via                           maddr                No     [RFC 3261]
  Via                           received             No     [RFC 3261]
  Via                           rport                No     [RFC 3581]
  Via                           ttl                  No     [RFC 3261]
  WWW-Authenticate              algorithm           Yes     [RFC 3261]
  [[RFC 3310]]
  WWW-Authenticate              domain              Yes     [RFC 3261]
  WWW-Authenticate              nonce                No     [RFC 3261]
  WWW-Authenticate              opaque               No     [RFC 3261]
  WWW-Authenticate              qop                 Yes     [RFC 3261]
  WWW-Authenticate              realm                No     [RFC 3261]
  WWW-Authenticate              stale               Yes     [RFC 3261]
  */

/*
--- rfc3261.txt ---
1  Accept
2  Accept-Encoding
3  Accept-Language
4  Alert-Info
5  Allow
6  Authentication-Info
7  Authorization
8  Call-ID
9  Call-Info
10 Contact
11 Content-Disposition
12 Content-Encoding
13 Content-Language
14 Content-Length
15 Content-Type
16 CSeq
17 Date
18 Error-Info
19 Expires
20 From
21 In-Reply-To
22 Max-Forwards
23 Min-Expires
24 MIME-Version
25 Organization
26 Priority
27 Proxy-Authenticate
28 Proxy-Authorization
29 Proxy-Require
30 Record-Route
31 Reply-To
32 Require
33 Retry-After
34 Route
35 Server
36 Subject
37 Supported
38 Timestamp
39 To
40 Unsupported
41 User-Agent
42 Via
43 Warning
44 WWW-Authenticate
--- rfc3262.txt ---
45 RAck
46 RSeq
--- rfc3265.txt ---
47 Allow-Events
48 Event
49 Subscription-State
--- rfc3325.txt ---
50 P-Asserted-Identity
--- rfc3326.txt ---
82 Reason
--- rfc3327.txt ---
83 Path
--- rfc3329.txt ---
51 Security-Client
52 Security-Server
53 Security-Verify
--- rfc3455.txt ---
54 P-Associated-URI
55 P-Called-Party-ID
56 P-Visited-Network-ID
57 P-Access-Network-Info
58 P-Charging-Vector
59 P-Charging-Function-Addresses
--- rfc3603.txt ---
60 P-DCS-Trace-Party-ID
61 P-DCS-OSPS
62 P-DCS-BILLING-INFO
63 P-DCS-LAES
64 P-DCS-REDIRECT
--- rfc3608.txt ---
65 Service-Route
--- rfc3841.txt ---
66 Accept-Contact
67 Reject-Contact
68 Request-Disposition
--- rfc3891.txt ---
84 Replaces
--- rfc3903.txt ---
69 SIP-ETag
70 SIP-If-Match
--- rfc3911.txt ---
85 Join
--- rfc4028.txt ---
71 Session-Expires
72 Min-SE
--- rfc4474.txt ---
73 Identity
74 Identity-Info
--- rfc4538.txt ---
75 Target-Dialog
--- rfc5002.txt
76 P-Profile-Key
--- rfc5009.txt ---
86 P-Early-Media
-- rfc5318.txt ---
87 P-Refused-URI-List
--- rfc5502.txt ---
88 P-Served-User
--- rfc5839.txt ---
77 Suppress-If-Match
--- rfc6044.txt ---
89 History-Info
90 Diversion
--- rfc6050.txt ---
78 P-Asserted-Service
79 P-Preferred-Service
--- rfc6086.txt ---
80 Info-Package
81 Recv-Info
--- end
*/
enum sip_header_type_t
{
	sip_Accept_e,						// [RFC 3261]
	sip_Accept_Contact_e,				// [RFC 3841]
	sip_Accept_Encoding_e,				// [RFC 3261]
	sip_Accept_Language_e,				// [RFC 3261]
	sip_Alert_Info_e,					// [RFC 3261]
	sip_Allow_e,						// [RFC 3261]
	sip_Allow_Events_e,					// [RFC 3265]
	sip_Authentication_Info_e,			// [RFC 3261]
	sip_Authorization_e,				// [RFC 3261]
	sip_Call_ID_e,						// [RFC 3261]
	sip_Call_Info_e,					// [RFC 3261]
	sip_Contact_e,						// [RFC 3261]
	sip_Content_Disposition_e,			// [RFC 3261]
	sip_Content_Encoding_e,				// [RFC 3261]
	sip_Content_Language_e,				// [RFC 3261]
	sip_Content_Length_e,				// [RFC 3261]
	sip_Content_Type_e,					// [RFC 3261]
	sip_CSeq_e,							// [RFC 3261]
	sip_Date_e,							// [RFC 3261]
	sip_Diversion_e,					// [RFC 6044]
	sip_Error_Info_e,					// [RFC 3261]
	sip_Event_e,						// [RFC 3265]
	sip_Expires_e,						// [RFC 3261]
	sip_From_e,							// [RFC 3261]
	sip_History_Info_e,					// [RFC 6044]
	sip_Identity_e,						// [RFC 4474]
	sip_Identity_Info_e,				// [RFC 4474]
	sip_In_Reply_To_e,					// [RFC 3261]
	sip_Info_Package_e,					// [RFC 6086]
	sip_Join_e,							// [RFC 3911]
	sip_Max_Forwards_e,					// [RFC 3261]
	sip_MIME_Version_e,					// [RFC 3261]
	sip_Min_Expires_e,					// [RFC 3261]
	sip_Min_SE_e,						// [RFC 4028]
	sip_Organization_e,					// [RFC 3261]
	sip_P_Access_Network_Info_e,		// [RFC 3455]
	sip_P_Asserted_Identity_e,			// [RFC 3325]
	sip_P_Asserted_Service_e,			// [RFC 6050]
	sip_P_Associated_URI_e,				// [RFC 3455]
	sip_P_Called_Party_ID_e,			// [RFC 3455]
	sip_P_Charging_Function_Addresses_e,// [RFC 3455]
	sip_P_Charging_Vector_e,			// [RFC 3455]
	sip_P_DCS_BILLING_INFO_e,			// [RFC 3603]
	sip_P_DCS_LAES_e,					// [RFC 3603]
	sip_P_DCS_OSPS_e,					// [RFC 3603]
	sip_P_DCS_REDIRECT_e,				// [RFC 3603]
	sip_P_DCS_Trace_Party_ID_e,			// [RFC 3603]
	sip_P_Early_Media_e,				// [RFC 5009]
	sip_P_Preferred_Service_e,			// [RFC 6050]
	sip_P_Profile_Key_e,				// [RFC 5002]
	sip_P_Refused_URI_List_e,			// [RFC 5318]
	sip_P_Served_User_e,				// [RFC 5502]
	sip_P_Visited_Network_ID_e,			// [RFC 3455]
	sip_Path_e,							// [RFC 3327]
	sip_Priority_e,						// [RFC 3261]
	sip_Proxy_Authenticate_e,			// [RFC 3261]
	sip_Proxy_Authorization_e,			// [RFC 3261]
	sip_Proxy_Require_e,				// [RFC 3261]
	sip_RAck_e,							// [RFC 3262]
	sip_Reason_e,						// [RFC 3326]
	sip_Record_Route_e,					// [RFC 3261]
	sip_Recv_Info_e,					// [RFC 6086]
	sip_Refer_To_e,						// [RFC 3515]
	sip_Referred_By_e,					// [RFC 3892]
	sip_Reject_Contact_e,				// [RFC 3841]
	sip_Replaces_e,						// [RFC 3891]
	sip_Reply_To_e,						// [RFC 3261]
	sip_Request_Disposition_e,			// [RFC 3841]
	sip_Require_e,						// [RFC 3261]
	sip_Retry_After_e,					// [RFC 3261]
	sip_Route_e,						// [RFC 3261]
	sip_RSeq_e,							// [RFC 3262]
	sip_Security_Client_e,				// [RFC 3329]
	sip_Security_Server_e,				// [RFC 3329]
	sip_Security_Verify_e,				// [RFC 3329]
	sip_Server_e,						// [RFC 3261]
	sip_Service_Route_e,				// [RFC 3608]
	sip_Session_Expires_e,				// [RFC 4028]
	sip_SIP_ETag_e,						// [RFC 3903]
	sip_SIP_If_Match_e,					// [RFC 3903]
	sip_Subject_e,						// [RFC 3261]
	sip_Subscription_State_e,			// [RFC 3265]
	sip_Supported_e,					// [RFC 3261]
	sip_Suppress_If_Match_e,			// [RFC 5839]
	sip_Target_Dialog_e,				// [RFC 4538]
	sip_Timestamp_e,					// [RFC 3261]
	sip_To_e,							// [RFC 3261]
	sip_Unsupported_e,					// [RFC 3261]
	sip_User_Agent_e,					// [RFC 3261]
	sip_Via_e,							// [RFC 3261]
	sip_Warning_e,						// [RFC 3261]
	sip_WWW_Authenticate_e,				// [RFC 3261]
	sip_header_max_e,
	sip_custom_header_e = 1000,					// custom header
};
////--------------------------------------
//// header type functions
////--------------------------------------
const char* sip_get_header_name(sip_header_type_t type);
sip_header_type_t sip_parse_header_type(const char* type_name);
//--------------------------------------
// standard methods
//--------------------------------------
enum sip_method_type_t
{
	sip_INVITE_e,						// [RFC 3261]
	sip_ACK_e,							// [RFC 3261]
	sip_OPTIONS_e,						// [RFC 3261]
	sip_BYE_e,							// [RFC 3261]
	sip_CANCEL_e,						// [RFC 3261]
	sip_REGISTER_e,						// [RFC 3261]
	sip_INFO_e,							// [RFC 2976, 6089]
	sip_UPDATE_e,						// [RFC 3311]
	sip_SUBSCRIBE_e,
	sip_NOTIFY_e,
	sip_REFER_e,
	sip_PRACK_e,
	sip_MESSAGE_e,
	sip_PUBLISH_e,
	sip_KEEP_ALIVE_e,

	sip_custom_method_e,					// custom method
};
//--------------------------------------
// method functions
//--------------------------------------
const rtl::String& sip_get_method_name(sip_method_type_t type);
sip_method_type_t sip_parse_method_type(const char* type_name);
//--------------------------------------
// response codes & reason text
//--------------------------------------
enum sip_status_t
{
	sip_000 = 0,
	sip_100_Trying = 100,				// Trying

	sip_180_Ringing = 180,				// Ringing
	sip_181_BeingForwarded = 181,		// Call Is Being Forwarded
	sip_182_Queued = 182,				// Queued
	sip_183_SessionProgress = 183,		// Session Progress

	sip_200_OK = 200,					// OK
	sip_202_Accepted = 202,				// Accepted

	sip_300_MultipleChoices = 300,		// Multiple Choices
	sip_301_MovedPermanently = 301,		// Moved Permanently
	sip_302_MovedTemporarily = 302,		// Moved Temporarily
	sip_305_UseProxy = 305,				// Use Proxy
	sip_380_AlternativeService = 380,	// Alternative Service

	sip_400_BadRequest = 400,			// Bad Request
	sip_401_Unauthorized = 401,			// Unauthorized
	sip_402_PaymentRequired = 402,		// Payment Required
	sip_403_Forbidden = 403,			// Forbidden
	sip_404_NotFound = 404,				// Not Found
	sip_405_MethodNotAllowed = 405,		// Method Not Allowed
	sip_406_NotAcceptable = 406,		// Not Acceptable
	sip_407_ProxyAuthenticationRequired = 407,	// Proxy Authentication Required
	sip_408_RequestTimeout = 408,		// Request Timeout
	sip_409_Conflict = 409,				// Conflict

	sip_410_Gone = 410,	// Gone
	sip_411_LengthRequired = 411,		// Length Required
	sip_412_ConditionalRequestFailed = 412,	// Conditional Request Failed
	sip_413_RequestEntityTooLarge = 413,	// Request Entity Too Large
	sip_414_RequestURITooLong = 414,	// Request-URI Too Long
	sip_415_UnsupportedMediaType = 415,	// Unsupported Media Type
	sip_416_UnsupportedURIScheme = 416,	// Unsupported URI Scheme

	sip_420_BadExtension = 420,			// Bad Extension
	sip_421_ExtensionRequired = 421,	// Extension Required
	sip_422_SessionIntervalTooSmall = 422,	// Session Interval Too Small
	sip_423_IntervalTooBrief = 423,		// Interval Too Brief

	sip_480_TemporarilyUnavailable = 480,	// Temporarily Unavailable
	sip_481_TransactionDoesNotExist = 481,	// Call/Transaction Does Not Exist
	sip_482_LoopDetected = 482,			// Loop Detected
	sip_483_TooManyHops = 483,			// Too Many Hops
	sip_484_AddressIncomplete = 484,	// Address Incomplete
	sip_485_Ambiguous = 485,			// Ambiguous
	sip_486_BusyHere = 486,				// Busy Here
	sip_487_RequestTerminated = 487,	// Request Terminated
	sip_488_NotAcceptableHere = 488,	// Not Acceptable Here
	sip_489_BadEvent = 489,				// Bad Event/Request

	sip_491_RequestPending = 491,		// Request Pending
	sip_493_Undecipherable = 493,		// Undecipherable

	sip_500_ServerInternalError = 500,	// Server Internal Error
	sip_501_NotImplemented = 501,		// Not Implemented
	sip_502_BadGateway = 502,			// Bad Gateway
	sip_503_ServiceUnavailable = 503,	// Service Unavailable
	sip_504_ServerTimeout = 504,		// Server Time-out
	sip_505_VersionNotSupported = 505,	// Version Not Supported

	sip_513_MessageTooLarge = 513,		// Message Too Large

	sip_600_BusyEverywhere = 600,		// Busy Everywhere
	sip_603_Decline = 603,				// Decline
	sip_604_DoesNotExistAnywhere = 604,	// Does Not Exist Anywhere
	sip_606_NotAcceptable = 606,		// Not Acceptable

	sip_MaxCode = 699
};
//--------------------------------------
//
//--------------------------------------
const rtl::String& sip_get_reason_text(sip_status_t code);

rtl::String sip_unescape_rfc3986(rtl::String str);
rtl::String sip_escape_rfc3986(rtl::String str);
bool selftest_sipdef();
//--------------------------------------
