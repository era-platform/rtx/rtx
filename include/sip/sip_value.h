/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_mime_param.h"
//--------------------------------------
//
//--------------------------------------
class sip_value_t
{
protected:
	rtl::String m_value;
	mime_param_list_t m_params;

protected:
	virtual rtl::String& prepare(rtl::String& stream) const;
	virtual bool parse();

	void extract_parameters();
	void parse_parameters(const char* param);

	friend class sip_header_t;

	// ��� �������� �� ������
	bool read_from(const char* string, int length = -1);
	// ��� ����������������� �������� �� ������ ������������� �������� ( name: value, value )
	virtual const char* read_from(const char* stream, bool& result);

public:
	sip_value_t() { }
	sip_value_t(const sip_value_t& v) { assign(v); }
	virtual ~sip_value_t();

	virtual void cleanup();
	virtual sip_value_t* clone() const;

	sip_value_t& operator = (const sip_value_t& v) { return assign(v); }
	sip_value_t& assign(const sip_value_t& v);
	sip_value_t& assign(const rtl::String& v);

	const rtl::String& value() const { return m_value; }
	
	rtl::String to_string() const { rtl::String value; write_to(value); return value; }
	virtual rtl::String& write_to(rtl::String& stream) const;

	const rtl::String& param_get(const char* name) const { return m_params.get(name); }
	const rtl::String& param_get(const char* name, bool& r) const { return m_params.get(name, r); }
	bool param_get(const char* name, rtl::String& r) const { return m_params.get(name, r); }
	void param_set(const char* name, const rtl::String& value) { m_params.set(name, value); }
	void param_remove(const char* name) { m_params.remove(name); }

	/// virtual constructor for typed value
	static sip_value_t* create(sip_header_type_t type);
};
//--------------------------------------
