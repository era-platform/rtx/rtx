/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_def.h"
#include "sip_transport_packet.h"
#include "sip_value.h"
//--------------------------------------
// universal SIP header
//--------------------------------------
class sip_header_t
{
protected:
	sip_header_type_t m_type;
	rtl::String m_name;
	rtl::ArrayT<sip_value_t*> m_values;

protected:
	rtl::String& write_values(rtl::String& stream) const;
	sip_value_t* create_value();

public:
	sip_header_t(sip_header_type_t type) : m_type(type) { }
	sip_header_t(sip_header_type_t type, const rtl::String& value) : m_type(type) { add_value(value); }
	sip_header_t(const char* name) : m_type(sip_custom_header_e), m_name(name) { }
	sip_header_t(const char* name, const rtl::String& value) : m_type(sip_custom_header_e), m_name(name) { add_value(value); }
	sip_header_t(const sip_header_t& header) { assign(header); }
	virtual ~sip_header_t();

	virtual void cleanup();

	sip_header_t& operator = (const sip_header_t& header) { return assign(header); }
	sip_header_t& assign(const sip_header_t& header);
	
	sip_header_t* clone() const;

	rtl::String to_string() const;
	rtl::String& write_to(rtl::String& stream) const;

	sip_header_type_t getType() const { return m_type; }
	const char* getName() const { return m_type == sip_custom_header_e ? (const char*)m_name : sip_get_header_name(m_type); }

	int get_value_count() const { return m_values.getCount(); }
	const sip_value_t* get_value_at(int index) const { return m_values.getAt(index); }
	sip_value_t* get_value_at(int index) { return m_values.getAt(index); }
	const sip_value_t* get_top_value() const { return m_values.getAt(0); }

	sip_value_t* make_value(int index = 0);
	sip_value_t* add_value() { return make_value(-1); }

	bool insert_value(int index, const char* value);
	bool add_value(const char* value);
	bool add_top_value(const char* value) { return insert_value(0, value); }
	bool setValue(int index, const char* value);

	bool add_value(sip_value_t* value);

	void remove_value_at(int index);
	void remove_all_values();
};
//--------------------------------------
