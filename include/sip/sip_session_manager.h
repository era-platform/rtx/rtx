/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_user_agent.h"
#include "sip_session.h"
//--------------------------------------
//
//--------------------------------------
#define SESSION_TIMER_ALL	0xffffffff
//--------------------------------------
//
//--------------------------------------
class sip_session_manager_t : rtl::ITimerEventHandler, rtl::async_call_target_t // IEventQueueHandler, 
{
protected: /// protected fields
	rtl::MutexWatch m_sync;
	sip_user_agent_t& m_ua;
	char m_ua_core_name[64];
	const char* m_log_tag;
	volatile bool m_stop_flag;

	rtl::MutexWatch m_session_sync;
	sip_session_list_t m_session_list;
	sip_session_list_t m_session_ref_list;
	rtl::ArrayT<sip_session_t*> m_purgatory;

	struct SESSION_TIMER
	{
		sip_session_t* tm_session;
		uint32_t tm_start;
		uint32_t tm_period;
		uint32_t tm_id;
		bool tm_single;
		bool tm_bad_handle;
	};

	rtl::ArrayT<SESSION_TIMER> m_timer_users;

	/// constructors and destructors
public:
	sip_session_manager_t(sip_user_agent_t& ua, int sessionThreadCount, const char* mgrName);
	virtual ~sip_session_manager_t();

	/// public interface
	virtual void stop();

	sip_session_t* create_client_session(const sip_profile_t& profile, const char* sessionId);
	sip_session_t* create_server_session(sip_message_t& request);

	void process_stack_event(sip_stack_event_t* stackEvent);
	void process_stack_event(const sip_transport_route_t& route, sip_stack_event_t* stackEvent);
	void process_stack_event(sip_session_t* session, sip_stack_event_t* stackEvent);

	void queue_for_destruction(const char* callId);

	void start_session_timer(sip_session_t* session, uint32_t id, uint32_t period, bool single = true);
	void stop_session_timer(sip_session_t* session, uint32_t id = SESSION_TIMER_ALL);
	void update_session_timer(sip_session_t* session, uint32_t id, uint32_t period, bool single = true);

	sip_session_t* find_session_by_call_id(const char* callId);
	sip_session_t* find_session_by_ref(const char* sessionRef);

	void push_session_event(sip_session_t& session, sip_session_event_t* event);
	bool send_message_to_transport(const sip_message_t& request, sip_session_t* session = NULL);

	bool is_valid_session(sip_session_t* session);

	sip_user_agent_t& get_user_agent() { return m_ua; }
	const char* get_UA_name() const { return m_ua_core_name; }
	void set_UA_name(const char* name) { STR_COPY(m_ua_core_name, name); }

protected: /// protected virtual interface
	virtual sip_session_t*	create_new_client_session(const sip_profile_t& profile, const char* sessionId);
	virtual sip_session_t*	create_new_server_session(sip_message_t& request);

	virtual void process_incoming_message(sip_event_message_t& messageEvent, sip_session_t* session);
	virtual void process_orphaned_message(const sip_message_t& message, sip_status_t scode = sip_404_NotFound);
	virtual void process_unknown_transaction(sip_event_unknown_transaction_t& event);
	virtual void process_transport_disconnected(const sip_transport_route_t& route);
	virtual void process_transport_failure(const sip_transport_route_t& route, int net_error);

protected: /// protected routines
	virtual void timer_elapsed_event(rtl::Timer* sender, int tid);

	virtual const char* async_get_name();
	virtual void async_handle_call(uint16_t call_id, uintptr_t int_param, void* ptr_param);

	void EventQueue_OnDequeue(void* _event);

	void remove_session();

	void check_session_timers(uint32_t currentTime);

	void check_purgatory(uint32_t currentTime);
	void add_to_purgatory(sip_session_t* session);
	void cleanup_purgatory();
};
//--------------------------------------
