/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_stack.h"
#include "sip_profile.h"
#include "sip_registrar.h"
//--------------------------------------
//
//--------------------------------------
class sip_user_agent_t : public rtl::async_call_target_t
{
public:
	sip_user_agent_t();
	virtual ~sip_user_agent_t();

	bool initialize(int work_threads, int trn_threads);
	void destroy();

	virtual void process_sip_stack_event(sip_stack_event_t* sip_event);

	bool push_message_to_transaction(const sip_message_t& request, trans_id& transactionId);
	void push_message_to_transport(const sip_message_t& request);

	sip_stack_t& get_sip_stack() { return m_sip_stack; }
	sip_registrar_t& get_registrar() { return m_registrar; }
	
	void setup_registrar(sip_registrar_serializer_t* serializer);
	
	const char* get_UA_name() { return m_ua_name; }
	void set_UA_name(const char* name);

	static sip_user_agent_t* get_UA();

protected:
	const char* async_get_name();
	void async_handle_call(uint16_t call_id, uintptr_t int_param, void* ptr_param);

	bool add_transport(sip_transport_type_t type, in_addr if_address, uint16_t if_port = 5060);

	sip_stack_t m_sip_stack;
	sip_registrar_t m_registrar;

	char m_ua_name[64];
	bool m_initialized;

	static sip_user_agent_t* g_UA;
};

void set_sip_worker_thread_max_dellay(int value);

extern rtl::Logger SLog;
extern rtl::Logger TransLog;
//--------------------------------------
