/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_stack.h"
#include "sip_profile.h"
//--------------------------------------
//
//--------------------------------------
class sip_session_manager_t;
class sip_session_event_t;
class sip_session_t;
//--------------------------------------
//
//--------------------------------------
#define SESSION_TIMER_ALL				0xffffffff
#define SESSION_TIMER_KEEPALIVE			1
#define SESSION_TIMER_AUTODESTROY		2
#define SESSION_TIMER_EXPIRES			3
//--------------------------------------
//
//--------------------------------------
enum sip_session_type_t
{
	sip_session_client_e,
	sip_session_server_e,
};
//--------------------------------------
//
//--------------------------------------
enum sip_dialog_type_t
{
	sip_dialog_none_e, // Not In Dialog
	sip_dialog_UAC_e, // User Agent Client
	sip_dialog_UAS_e  // User Agent Server
};
//--------------------------------------
//
//--------------------------------------
class sip_session_t : rtl::async_call_target_t
{
protected:
	friend class sip_session_manager_t;
	
	static volatile int32_t s_internal_id_counter;	// ��������� ���� ������
	static const char* s_dialog_type_name[3];		// ������� ��� ���� ������

	const char* m_log_tag;							// ������ ��� �����������
	sip_profile_t m_profile;						// ������� � �����������
	sip_session_manager_t& m_manager;				// ��������
	char m_session_ref[64];							// ��� ������

	/// User Agent common fields
	rtl::String m_call_id;								// ������������� �������
	sip_session_type_t m_type;						// ��� ������ ��� ������
	bool m_will_process_events;						// ������� ������ ������
	bool m_new_session;

	/// Dialog parameters
	rtl::MutexWatch m_dialog_lock;
	sip_dialog_type_t m_dialog_type;				// ��� ������� 
	sip_header_t m_remote_route_set;			// ����
	rtl::String m_local_tag;				// ��� ������� from_tag, ��� ������� to_tag
	rtl::String m_remote_tag;				// ��� ������� to_tag, ��� ������� from_tag
	sip_header_t m_local_contact_list;		// ������ ���������
	sip_header_t m_remote_contact_list;		// ������ ���������
	sip_header_t m_local_via;
	uint32_t m_local_sequaence;
	uint32_t m_remote_sequaence;

	/// UA Client fields
	sip_message_t m_uac_request;				// ��������� ������
	sip_message_t m_uac_response;				// �������� �����
	rtl::MutexWatch m_uac_request_lock;
	rtl::MutexWatch m_uac_response_lock;

	/// UA Server fields
	sip_message_t m_uas_request;				// �������� ������
	sip_message_t m_uas_response;				// ��������� �����
	rtl::MutexWatch m_uas_request_lock;
	rtl::MutexWatch m_uas_response_lock;

	sip_uri_t m_local_uri;				// from URI to be used for request
	sip_uri_t m_remote_uri;				// to URI to be used for requests
	sip_uri_t m_contact_uri;				// dialog uri from response

	sip_transport_route_t m_route;

	/// Destroying
	rtl::MutexWatch m_destroy_lock;
	bool m_destroyed;
	bool m_ready_to_destroy;			// moved to purgatory
	uint32_t m_purgatory_time;			// moving timestamp
	uint32_t m_destroy_time;			// destroying timestamp;

public:
	// 1..100
	enum SessionEvent
	{
		TransportFault = 1,
		TransportDisconnected = 2,
	};

public:

	sip_session_t(sip_session_manager_t& manager, const sip_profile_t& profile, const char* sessionId);
	sip_session_t(sip_session_manager_t& manager, const sip_message_t& reqeust);

protected:
	virtual ~sip_session_t();

	/// event processinf
	virtual bool process_incoming_sip_message(sip_event_message_t& messageEvent);
	virtual void process_session_event(int sip_event, const sip_message_t& eventMsg);
	virtual void process_sip_message_sent_event(sip_event_tx_t& messageEvent);
	/// timer processing
	virtual void process_session_expire_event();
	virtual void process_timer_expires_event(sip_event_timer_expires_t& timerEvent);
	/// error processing
	virtual void process_transport_failure_event(int failureType);
	/// destructor processing
	virtual void process_destroy_event();

	void push_session_event(sip_session_event_t* sip_event);		
	bool CreateRequestWithinDialog(sip_method_type_t method, sip_message_t& request);

	void StartNATKeepAlive(int expires);
	void StopNATKeepAlive();

	void StopAutoDestructTimer();

	void StartSessionExpireTimer(uint32_t period, bool single);
	void StopSessionExpireTimer();

	void timer_event(uint32_t timer_id);

	void check_transport_route_disconnected(const sip_transport_route_t& route);
	void check_transport_route_failure(const sip_transport_route_t& route, int net_error);

	virtual const char* async_get_name();
	virtual void async_handle_call(uint16_t call_id, uintptr_t int_param, void* ptr_param);

public:
	void StartAutoDestructTimer(int expires);

	void destroy_session(bool wait_answer = false);

	bool InitializeDialogAsUAC(const sip_message_t& request, const sip_message_t& respone);
	bool InitializeDialogAsUAS(const sip_message_t& request, const sip_message_t& response);

	bool push_message_to_transaction(const sip_message_t& request);
	bool SendAcceptByRejection(const sip_message_t& requestToReject, int statusCode = sip_403_Forbidden, const rtl::String& resonPhrase = rtl::String::empty, const rtl::String& warning = rtl::String::empty);
	bool SendAcceptByRejection(int statusCode = sip_403_Forbidden, const rtl::String& resonPhrase = rtl::String::empty, const rtl::String& warning = rtl::String::empty);

	const char* get_call_id() const { return !m_call_id.isEmpty() ? m_call_id : ""; }

	sip_session_type_t getType() const { return m_type; }
	const sip_profile_t& get_profile() const { return m_profile; }
	void set_profile(const sip_profile_t& profile) { m_profile = profile; }
	
	bool will_process_events() const { return m_will_process_events; }
	const char* getName() { return m_session_ref; }
	bool check_route(const sip_transport_route_t& route);
	/// Dialog Parameter Accessors
	const rtl::String& GetLocalTag() const { return m_local_tag; }
	const sip_uri_t& GetLocalURI() const { return m_local_uri; } // to URI to be used for requests
	const rtl::String& GetRemoteTag() const { return m_remote_tag; }
	const sip_uri_t& GetRemoteURI() const { return m_remote_uri; } // to URI to be used for requests

	sip_message_t GetCurrentUACRequestCopy() const
	{
		sip_message_t tmp;
		{
			rtl::MutexWatchLock lock(m_uac_request_lock, __FUNCTION__);
			tmp = m_uac_request;			
		}
		return tmp;
	}
	void SetCurrentUACRequest(const sip_message_t& request);
	// 18.03.2013 renat. FMC. ������ ���������� ������� �����.
	bool GetCurrentUACResponse_IsValid ( ) const
	{
		bool isvalid;
		{
			rtl::MutexWatchLock lock(m_uac_response_lock, __FUNCTION__);
			isvalid = m_uac_response.IsValid();
		}
		return isvalid;
	}
	// 18.03.2013 renat. FMC. ������ ���������� ������� �����.
	rtl::String GetCurrentUACResponse_GetStartLine ( ) const
	{
		rtl::String startline;
		{
			rtl::MutexWatchLock lock(m_uac_response_lock, __FUNCTION__);
			startline = m_uac_response.GetStartLine();
		}
		return startline;
	}
	// 18.03.2013 renat. FMC. ������ ���������� ������� �����.
	sip_status_t GetCurrentUACResponse_GetStatusCode ( ) const
	{
		sip_status_t statuscode;
		{
			rtl::MutexWatchLock lock(m_uac_response_lock, __FUNCTION__);
			statuscode = (sip_status_t)m_uac_response.get_response_code();
		}
		return statuscode;
	}
};
//--------------------------------------
//
//--------------------------------------
class sip_session_event_t
{
protected:
	char m_session_ref[64];
	int m_event;
	sip_message_t m_message;

public:

	sip_session_event_t(sip_session_t& session, int event, const sip_message_t& msg = sip_message_t()) :
		m_event(event), m_message(msg) { STR_COPY(m_session_ref, session.getName()); /*RC_IncrementObject(RC_SESSION_EVENTS);*/ }
	~sip_session_event_t() { /*RC_DecrementObject(RC_SESSION_EVENTS);*/ }

	const char* get_ref() { return m_session_ref; }
	int get_event() const { return m_event; }
	const sip_message_t& get_message() const { return m_message; }
};
//--------------------------------------
//
//--------------------------------------
class sip_session_list_t
{
public:
	struct sess_key_t
	{
		char* key;
		sip_session_t* session;
	};

private:
	const char* m_tag;
	rtl::SortedArrayT<sess_key_t, ZString> m_pool;

public:
	sip_session_list_t(const char* tag);
	~sip_session_list_t();

	bool add(sip_session_t* session, const char* key);
	sip_session_t* remove(const char* key);
	void remove(sip_session_t* session);
	sip_session_t* removeAt(int index);
	sip_session_t* find(const char* key);
	sip_session_t* find(const char* key, int& index);
	int getCount() { return m_pool.getCount(); }
	sip_session_t* getAt(uint32_t index) { return m_pool.getAt(index).session; }
	void clear();
};
//--------------------------------------
