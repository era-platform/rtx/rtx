/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_transport_listener.h"
//--------------------------------------
//
//--------------------------------------
class sip_transport_listener_tcp_t : public sip_transport_listener_t
{
	net_socket_t m_tcp_listener;

public:
	sip_transport_listener_tcp_t();
	virtual ~sip_transport_listener_tcp_t();

	/// transport listener public overrides
	virtual bool create(const sip_transport_point_t& iface);
	virtual void destroy();
	
	virtual bool start_listen();

private:
	/// transport object protected override
	virtual void data_ready();
};
//--------------------------------------
