/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_transport_manager.h"
#include "sip_transport.h"
//--------------------------------------
//
//--------------------------------------
class sip_transport_listener_t : public sip_transport_object_t
{
protected:
	sip_transport_point_t m_if;

public:
	sip_transport_listener_t(net_socket_t& sock);
	virtual ~sip_transport_listener_t();

	virtual bool create(const sip_transport_point_t& iface) = 0;
	virtual void destroy();

	virtual bool start_listen() = 0;
	virtual void stop_listen(bool failure = false);

	sip_transport_type_t getType() { return m_if.type; }
	in_addr get_interface_address() { return m_if.address; }
	uint16_t get_interface_port() { return m_if.port; }

protected:
	friend class sip_transport_binding_t;

	long add_ref() { return sip_transport_object_t::add_ref(); }
	long release_ref() { return sip_transport_object_t::release_ref(); }
};
//--------------------------------------
