/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_transaction.h"
//--------------------------------------
//
//--------------------------------------
class sip_transaction_manager_t : public sip_timer_manager_t
{
public:
	sip_transaction_manager_t();
	virtual ~sip_transaction_manager_t();

	virtual bool initialize(int work_threads);
	virtual void destroy();

	bool cancel_invite_client_transaction(const sip_message_t& invite, bool reason_200);
	void stop_cancel_invite_client_transaction(const sip_message_t& invite);
	bool check_transaction(const sip_message_t& event, trans_id& id);
	bool find_transaction_and_add_event(sip_message_t& sip_event, trans_id& id, bool fromTransport);
	bool find_ack_transaction_and_add_event(const sip_message_t& ack, trans_id& id);
	bool add_ack_transaction(const sip_message_t& response, sip_transaction_t* transaction);
	bool remove_ack_transaction(const sip_message_t& response, sip_transaction_t* transaction);

	virtual void process_received_message_event(const sip_message_t& message, sip_transaction_t& transaction);
	virtual void process_timer_expire_event(sip_transaction_timer_t timer, sip_transaction_t& transaction);
	virtual void process_unknown_transaction_event(const sip_message_t& event, bool fromTransport);
	virtual void send_message_to_transport(const sip_message_t& message, sip_transaction_t& transaction);

	void begin_work() { m_started = true; }
	void end_work() { m_started = false; }

protected:

	bool remove_transaction(sip_transaction_t* transaction);
	bool create_transaction(const sip_message_t& event, bool fromTransport);
	void force_terminate_transactions();

	virtual sip_transaction_t* create_new_transaction(const sip_message_t& request, const trans_id& TID, sip_transaction_type_t type);

protected:
	sip_transaction_stage_t* m_stage;
	volatile bool m_terminating;
	volatile bool m_started;

private:
	rtl::MutexWatch m_pool_lock;
	sip_transaction_list_t m_pool;

	rtl::MutexWatch m_ack_pool_lock;
	sip_transaction_list_t m_ack_pool;

	friend class sip_transaction_t;
};
//--------------------------------------
