/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_registrar_session.h"
#include "sip_subscribe_session.h"
//--------------------------------------
//
//--------------------------------------
class sip_registrar_t;
struct sip_registrar_event_handler_t;
//--------------------------------------
// ����������� � �������������� ������
//--------------------------------------
struct sip_registrar_auth_t
{
	char* call_id;
	char* nonce;
	time_t m_auth_time;
};
//--------------------------------------
// record-of-address
//--------------------------------------
class sip_registrar_account_t
{
	sip_registrar_t& m_registrar;
	sip_registrar_event_handler_t* m_event_handler;

	volatile bool m_enabled;
	char* m_username;
	char* m_auth_id;
	char* m_password;
	int m_max_expires;

	rtl::Mutex m_sync;	// ��� ������������� ���������� ����������
	rtl::ArrayT<sip_registrar_session_t*> m_register_list;
	rtl::ArrayT<sip_subscribe_session_t*> m_subscribe_list;
	rtl::ArrayT<sip_registrar_auth_t> m_auth_list;

	volatile uint32_t m_ref_count;

public:
	sip_registrar_account_t(sip_registrar_t& registrar, sip_registrar_event_handler_t* event_handler) :
		m_registrar(registrar), m_event_handler(event_handler), m_enabled(true), m_username(nullptr),
		m_auth_id(nullptr), m_password(nullptr), m_ref_count(1) { }
	

	void add_ref();
	void release();

	const char* get_username() const { return m_username; }
	const char* get_auth_id() const { return m_auth_id; }
	const char* get_password() const { return m_password; }

	int get_max_expires() { return m_max_expires; }
	void set_max_expires(int new_value) { m_max_expires = new_value; }
	void set_enabled(bool enabled = true);
	bool is_enabled() const { return m_enabled; }

	sip_registrar_t& get_registrar() { return m_registrar; }

	/// locked methods
	void setup(const char* username, const char* auth_id, const char* password, int max_expires = 0);
	void update(const char* auth_id, const char* password);
	void clear();

	void process_transport_disconnected(const sip_transport_route_t& route);
	bool process_incoming_request(const sip_message_t& request);
	bool process_register_request(const sip_message_t& request, bool over_nat);
	bool process_subscribe_request(const sip_message_t& request, bool over_nat);
	
	void check_contacts(time_t current_time);
	int get_contacts(rtl::PonterArrayT<sip_registrar_contact_info_t>& contacts) const;
	bool check_contact(sip_registrar_contact_info_t* contact) const;

	void send_sub_notify(const sip_registrar_subscribe_info_t* subscribe_info);

	/// not locked method -- called from internal or session functions
	void send_reg_200_ok(sip_registrar_session_t& sender, const sip_message_t& request, int expires_value);
	void send_answer(sip_message_t& response) { m_registrar.send_response(response); }
	void send_request(sip_message_t& request) { m_registrar.send_request(request); }

	const sip_registrar_auth_t* get_auth(const char* call_id);
	bool is_over_nat(const char* request_host, in_addr iface, in_addr r_addr) { return m_registrar.is_over_nat(request_host, iface, r_addr); }

private:
	~sip_registrar_account_t() { clear(); }

	void send_401_Unauthorized(const sip_message_t& request);
	bool check_athorization(const sip_message_t& request, bool& checked);
	bool check_for_asterisk(const sip_message_t& request);

	sip_registrar_session_t* find_reg_session(const char* call_id, int* index = nullptr);
	sip_registrar_session_t* find_reg_session(in_addr r_addr, uint16_t remotePort, int* index = nullptr);
	const sip_registrar_session_t* find_reg_session(in_addr r_addr, uint16_t remotePort, int* index = nullptr) const;

	sip_subscribe_session_t* find_sub_session(const char* call_id, int* index = nullptr);

	sip_registrar_session_t* create_reg_session(const sip_message_t& request);
	void remove_reg_session(sip_registrar_session_t* session);
	
	sip_subscribe_session_t* create_sub_session(const sip_message_t& request);
	void remove_sub_session(sip_subscribe_session_t* session);

	bool update_reg_session(sip_registrar_session_t* session, const sip_message_t& request, bool over_nat);
	bool replace_reg_session(sip_registrar_session_t* session, const sip_message_t& request, bool over_nat);
	bool create_new_reg_session(const sip_message_t& request, bool over_nat);

	bool update_sub_session(sip_subscribe_session_t* session, const sip_message_t& request, bool over_nat);
	bool create_new_sub_session(const sip_message_t& request, bool over_nat);

	void raise_register_event(const sip_registrar_session_t& session, sip_registrar_contact_info_t* contact_info);
	void raise_unregister_event(sip_registrar_contact_info_t* contact_info, sip_unregister_event_reason_t reason);

	void raise_subscribe_event(sip_subscribe_session_t& session, const sip_message_t& request, sip_message_t& response);

	char* generate_nonce(const sip_message_t& request);
	const sip_registrar_auth_t& get_auth(const sip_message_t& request);
	void remove_auth(const char* call_id);
	void add_read_auth(const char* call_id, const char* nonce, const char* nc);

	void write_to_regdb(sip_registrar_session_t* session, bool reset_expires);
};
//--------------------------------------
