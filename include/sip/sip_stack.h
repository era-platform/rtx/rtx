/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_fsm.h"
#include "sip_transport_manager.h"
//--------------------------------------
//
//--------------------------------------
const char* sip_event_get_type_name(int type);
//--------------------------------------
//
//--------------------------------------
class sip_stack_event_t
{
public:
	enum sip_event_type_t
	{
		sip_event_message_e,
		sip_event_timer_e,
		sip_event_tx_e,
		sip_event_rx_e,
		sip_event_unknown_trn_e,
		sip_event_tx_error_e,
		sip_event_rx_error_e,
		sip_event_transport_disconnected_e,
		sip_event_transport_new_interface_e,
		sip_event_transport_failure_e,
		sip_event_final_e
	};

	sip_stack_event_t(sip_event_type_t type, const trans_id& transactionId) : m_type(type), m_trn_id(transactionId) { /*RC_IncrementObject(RC_STACK_EVENTS);*/ }
	virtual ~sip_stack_event_t() { /*RC_DecrementObject(RC_STACK_EVENTS);*/ }

	sip_event_type_t getType() const { return m_type; }
	const char* get_type_name() const { return sip_event_get_type_name(m_type); }
	const trans_id& get_transaction_id() const { return m_trn_id; }
	const rtl::String& get_call_id() const { return m_trn_id.GetCallId(); }
	const rtl::String& get_method() const { return m_trn_id.GetMethod(); }

	virtual rtl::String to_string() = 0;

protected:
	sip_event_type_t m_type;
	trans_id m_trn_id;
};
//--------------------------------------
//
//--------------------------------------
class sip_event_final_t : public sip_stack_event_t
{
public:
	sip_event_final_t() : sip_stack_event_t(sip_stack_event_t::sip_event_final_e, rtl::String::empty) { }

	virtual rtl::String to_string();
};
//--------------------------------------
//
//--------------------------------------
class sip_event_message_t : public sip_stack_event_t
{
public:
	sip_event_message_t(const sip_message_t& message, const trans_id& transactionId) :
		sip_stack_event_t(sip_stack_event_t::sip_event_message_e, transactionId) { m_message = message; }

	const sip_message_t& get_message() const { return m_message; }

	virtual rtl::String to_string();

protected:
	sip_message_t m_message;
};
//--------------------------------------
//
//--------------------------------------
class sip_event_unknown_transaction_t : public sip_stack_event_t
{
public:
	sip_event_unknown_transaction_t(const sip_message_t& message, const trans_id& transactionId, bool fromTransport) :
	  sip_stack_event_t(sip_stack_event_t::sip_event_unknown_trn_e, transactionId) { m_message = message; m_from_transport = fromTransport; }

	const sip_message_t& get_message() const { return m_message; }
	bool is_from_transport() const { return m_from_transport; }

	virtual rtl::String to_string();

protected:
	sip_message_t m_message;
	bool m_from_transport;
};
//--------------------------------------
//
//--------------------------------------
class sip_event_timer_expires_t : public sip_stack_event_t
{
public:
	sip_event_timer_expires_t(sip_transaction_timer_t timer, const trans_id& transactionId) :
		sip_stack_event_t(sip_stack_event_t::sip_event_timer_e, transactionId) { m_timer_type = timer; }

	sip_transaction_timer_t get_timer() { return m_timer_type; }

	virtual rtl::String to_string();

protected:
	sip_transaction_timer_t m_timer_type;
};
//--------------------------------------
//
//--------------------------------------
class sip_event_tx_t : public sip_stack_event_t
{
public:
	sip_event_tx_t(const sip_message_t& message, const trans_id& transactionId) :
	  sip_stack_event_t(sip_stack_event_t::sip_event_tx_e, transactionId) { m_message = message; m_trn_id = transactionId; };
	virtual ~sip_event_tx_t();
	const sip_message_t& get_message() const { return m_message; };

	virtual rtl::String to_string();

protected:
	sip_message_t m_message;
};
//--------------------------------------
//
//--------------------------------------
class sip_event_tx_error_t : public sip_stack_event_t
{
public:
	sip_event_tx_error_t(const sip_message_t& message, const trans_id& transactionId, int net_error = 0) :
		sip_stack_event_t(sip_stack_event_t::sip_event_tx_error_e, transactionId) { m_message = message; m_net_error = net_error; };

	const sip_message_t& get_message() const { return m_message; };
	int get_net_error() const { return m_net_error; }
	virtual rtl::String to_string();

protected:
	sip_message_t m_message;
	int m_net_error;
};
//--------------------------------------
//
//--------------------------------------
class sip_event_rx_t : public sip_stack_event_t
{
public:
	sip_event_rx_t(const sip_message_t& message, const rtl::String& transactionId) :
	  sip_stack_event_t(sip_stack_event_t::sip_event_rx_e, transactionId) { m_message = message; };

	const sip_message_t& get_message()const{ return m_message; };

	virtual rtl::String to_string();

protected:
	sip_message_t m_message;
};
//--------------------------------------
//
//--------------------------------------
class sip_event_rx_error_t : public sip_stack_event_t
{
public:
	enum rx_error_t
	{
		rx_error_unknown,
		rx_error_address_e,
		rx_error_user_e,
		rx_error_domain_e,
		rx_error_password_e,
		rx_error_parser_e,
	};

	sip_event_rx_error_t(const sip_message_t& message, in_addr addr, uint16_t port, rx_error_t type) :
		sip_stack_event_t(sip_stack_event_t::sip_event_rx_error_e, rtl::String::empty), m_message(message)
	{
		m_trn_id = m_message.get_transaction();
		m_recv_address = addr; m_recv_port = port; m_error_type = type;
	}

	const sip_message_t& get_message() const { return m_message; }
	in_addr get_address() const { return m_recv_address; }
	uint16_t get_port() const { return m_recv_port; }
	rx_error_t get_error_type() { return m_error_type; }

	virtual rtl::String to_string();

protected:
	sip_message_t m_message;
	in_addr m_recv_address;
	uint16_t m_recv_port;
	rx_error_t m_error_type;
};
//--------------------------------------
//
//--------------------------------------
class sip_event_transport_disconnected_t : public sip_stack_event_t
{
public:
	sip_event_transport_disconnected_t(const sip_transport_route_t& route) :
		sip_stack_event_t(sip_stack_event_t::sip_event_transport_disconnected_e, rtl::String::empty)
	{
		m_route = route;
	}

	const sip_transport_route_t& getRoute() { return m_route; }

	virtual rtl::String to_string();

protected:
	sip_transport_route_t m_route;
};
//--------------------------------------
//
//--------------------------------------
class sip_event_transport_new_interface_t : public sip_stack_event_t
{
public:
	sip_event_transport_new_interface_t(const sip_transport_point_t& point) :
		sip_stack_event_t(sip_stack_event_t::sip_event_transport_new_interface_e, rtl::String::empty)
	{
		m_point = point;
	}

	const sip_transport_point_t& get_point() { return m_point; }

	virtual rtl::String to_string();

protected:
	sip_transport_point_t m_point;
};
//--------------------------------------
//
//--------------------------------------
class sip_event_transport_failure_t : public sip_stack_event_t
{
public:
	sip_event_transport_failure_t(const sip_transport_route_t& route, int net_error) :
		sip_stack_event_t(sip_stack_event_t::sip_event_transport_failure_e, rtl::String::empty)
	{
		m_route = route;
		m_net_error = net_error;
	}

	const sip_transport_route_t& getRoute() { return m_route; }
	int get_net_error() const { return m_net_error; }

	virtual rtl::String to_string();

protected:
	sip_transport_route_t m_route;
	int m_net_error;
};
//--------------------------------------
//
//--------------------------------------
class sip_user_agent_t;
//--------------------------------------
//
//--------------------------------------
class sip_stack_t : public sip_transaction_fsm_t, public sip_transport_manager_event_handler_t
{
public:
	sip_stack_t(sip_user_agent_t& ua);
	virtual ~sip_stack_t();

	void initialize_stack(int work_threads);
	void destroy_stack();

	/// ���������
	bool add_transport(const sip_transport_point_t& iface);

	/// ������� �������
	bool open_route(sip_transport_route_t& transport_info, const char* user);
	bool set_route_keep_alive_timer(sip_transport_route_t& route, int keep_alive_value);
	void release_route(const sip_transport_route_t& transport_info, const char* user);

	/// ������ � ��� ������
	void add_to_banned_list(const char* addr);
	void remove_from_banned_list(const char* addr);
	void add_check_domain(const char* domain);
	void add_check_user(const char* user);

	/// ����������� ������� �� ��������� ����������
	virtual void process_received_message_event(const sip_message_t& message, sip_transaction_t & transaction);
	virtual void process_timer_expire_event(sip_transaction_timer_t timer, sip_transaction_t & transaction);
	virtual void process_unknown_transaction_event(const sip_message_t& event, bool fromTransport);

	void send_message_to_transport(const sip_message_t& message);
	virtual void send_message_to_transport(const sip_message_t& message, sip_transaction_t & transaction);

	void set_discard_subscribes(bool enable);
protected:

	virtual void sip_transport_man_packet_arrival(sip_message_t* message);
	virtual void sip_transport_man_packet_sent(const sip_message_t& message);
	virtual void sip_transport_man_send_error(const sip_message_t& message, int net_error);
	virtual void sip_transport_man_recv_error(const sip_transport_route_t& transport, int net_error);
	virtual void sip_transport_man_disconnected(const sip_transport_route_t& route);
	virtual void sip_transport_man_new_interface(const sip_transport_point_t& point);

	virtual bool sip_transport_man_check_account(in_addr remoteAddress, uint16_t remotePort, const sip_message_t& message);
	virtual bool sip_transport_man_is_address_banned(in_addr remoteAddress);
	virtual bool sip_transport_man_is_useragent_banned(const char* useragent);

	void ban_ip_address(in_addr address);
	void unban_ip_address(in_addr address);
	void ban_user_agent(const char* user);
	void unban_user_agent(const char* user);

	bool filter_ip_address(in_addr remoteAddress);
	bool filter_account(const sip_message_t& message, sip_event_rx_error_t::rx_error_t& type);
	bool filter_user_agent(const char* usera_gent);

	void push_sip_stack_event(sip_stack_event_t* event);

	sip_user_agent_t& m_ua;

	rtl::Mutex m_banned_lock;
	rtl::ArrayT<in_addr> m_banned_ip_list;
	rtl::ArrayT<char*> m_banned_user_agent_list;

	rtl::Mutex m_check_lock;
	rtl::ArrayT<char*> m_check_domain_list;
	rtl::ArrayT<char*> m_check_user_list;
};
//--------------------------------------
