/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_transport_manager.h"
//--------------------------------------
// ��������������� ��������
//--------------------------------------
class sip_transport_reader_t;
//--------------------------------------
// ������ ���� ��� ������
//--------------------------------------
class sip_transport_object_t
{
	// ���������� ��� ������������ ������� ���������
	friend sip_transport_reader_t;
	sip_transport_object_t* m_next_link;
	volatile int32_t m_ref_count;

protected:
	sip_transport_reader_t* m_reader;
	volatile bool m_readable;

	uint32_t m_oid;
	char m_name[16];
	static volatile int32_t m_id_generator;
	net_socket_t& m_socket;

	long add_ref();
	long release_ref();

protected:
	virtual void data_ready();
	long get_ref_count() { return m_ref_count; }
	void generate_name(const char* prefix);
	void update_name(const char* prefix);
public:
	sip_transport_object_t(net_socket_t& sock);
	virtual ~sip_transport_object_t();

	void start_reading(sip_transport_reader_t* reader) { m_reader = reader; }
	void stop_reading();

	const char* get_id() { return m_name; }
};
//--------------------------------------
// ������ ����� ��� ��������� ����
//--------------------------------------
class sip_transport_reader_t : rtl::IThreadUser
{
	rtl::Mutex m_sync;					// �������������

	volatile sip_transport_object_t* m_first;	// ��������� �� ������ �����
	volatile sip_transport_object_t* m_last;	// ��������� �� ��������� �����
	volatile int m_count;				// ���������� ������� � ������

	rtl::ArrayT<net_socket_t*> m_read_list;
	rtl::ArrayT<sip_transport_object_t*> m_captured_list;

	rtl::Thread m_thread;							// ����� ���������

	rtl::Event m_thread_wake_up_event;				// ������� �� ����������� ������
	rtl::Event m_thread_started_event;				// ������� ������ ������ -- ������� Start ���� ���� ����� �� ���������

	volatile bool m_thread_enter;				// ���� ����� ������ � ������

	uint32_t m_id;
	static volatile int32_t m_id_generator;

public:
	sip_transport_reader_t();
	~sip_transport_reader_t();

	void stop();

	int get_free_slots() { return 64 - m_count; }
	int getCount() { return m_count; }
	
	bool add_object(sip_transport_object_t* object);
	void remove_object(sip_transport_object_t* object);

private:
	bool start();
	bool prepare_selection();
	void process_read_sockets();

	virtual void thread_run(rtl::Thread* thread);
};
//--------------------------------------
