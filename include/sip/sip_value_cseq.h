/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_value.h"
//--------------------------------------
// CSeq
//--------------------------------------
class sip_value_cseq_t : public sip_value_t
{
private:
	uint32_t m_cseq;
	rtl::String m_method;

private:
	virtual rtl::String& prepare(rtl::String& stream) const;
	virtual bool parse();

public:
	sip_value_cseq_t() : m_cseq(0) { }
	sip_value_cseq_t(const sip_value_cseq_t& cseq) { assign(cseq); }
	virtual ~sip_value_cseq_t();

	virtual void cleanup();
	virtual sip_value_t* clone() const;

	sip_value_cseq_t& operator = (const sip_value_cseq_t& v) { return assign(v); }

	sip_value_cseq_t& assign(const sip_value_cseq_t& value);
	sip_value_cseq_t& assign(uint32_t seq, const rtl::String& method) { m_cseq = seq; m_method = method; return *this; }

	uint32_t get_number() const { return m_cseq; }
	void set_number(uint32_t cseq) { m_cseq = cseq; }
	void increment_number() { m_cseq++; }

	const rtl::String& get_method() const { return m_method; }
	void set_method(const rtl::String& method) { m_method = method; }
};
//--------------------------------------
