/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_transport_pool.h"
//--------------------------------------
// ������� ��������� ������������� ������
//--------------------------------------
struct sip_transport_manager_event_handler_t
{
	virtual void sip_transport_man_packet_arrival(sip_message_t* message) = 0;
	virtual void sip_transport_man_packet_sent(const sip_message_t& message) = 0;
	virtual void sip_transport_man_send_error(const sip_message_t& message, int net_error) = 0;
	virtual void sip_transport_man_recv_error(const sip_transport_route_t& route, int net_error) = 0;
	virtual void sip_transport_man_disconnected(const sip_transport_route_t& route) = 0;
	virtual void sip_transport_man_new_interface(const sip_transport_point_t& point) = 0;
	
	virtual bool sip_transport_man_check_account(in_addr remoteAddress, uint16_t remotePort, const sip_message_t& message) = 0;
	virtual bool sip_transport_man_is_address_banned(in_addr remoteAddress) = 0;
	virtual bool sip_transport_man_is_useragent_banned(const char* useragent) = 0;
};
//--------------------------------------
//
//--------------------------------------
class sip_transport_manager_t : rtl::async_call_target_t, rtl::ITimerEventHandler
{
protected:
	/// ������� ��� ��������� ����������
	static sip_transport_manager_event_handler_t* m_event_handler;

	/// ������ ��� ������ �� ����
	static rtl::Mutex m_readerSync;
	static rtl::ArrayT<sip_transport_reader_t*> m_reader_list;

	/// ��������� ����
	static rtl::Mutex m_binding_list_sync;
	static rtl::ArrayT<sip_transport_binding_t*> m_binding_list;

	/// ����� �����������
	static sip_transport_pool_t m_pool;

	static volatile bool m_discard_subscribes;

public: /// public interface

	static void create(sip_transport_manager_event_handler_t* event_handler);
	static void destroy();

	/// ��� tcp, ws, sctp ��������� ���� ��� �����������, �� ��� ������ �������!
	/// ��� udp �������� �������
	static bool start_listener(const sip_transport_point_t& point);
	static void stop_listener(const sip_transport_point_t& point);

	/// �������� ���������
	static void send_message_async(const sip_message_t& message);

	/// ��������� ����������� ������ -> ���������� ��������� � ���� ���� �� ��� �����
	static bool open_route(sip_transport_route_t& route, const char* user);
	/// ��������� ����������� ������ � ��������� ������� ��������� (keep-alive)
	static bool set_route_keep_alive_timer(sip_transport_route_t& route, int keep_alive_value);
	/// �������� ������������ ������
	static void release_route(const sip_transport_route_t& route, const char* user);

	/// ������ �� ��������� SUBSCRIBE PUBLISH e.t.c
	static void set_discard_subscribes(bool enable) { m_discard_subscribes = enable; }

public: /// internal
	/// sip_transport event handler routines
	static void sip_transport_packet_read(sip_transport_t* sender, raw_packet_t* packet);
	static void sip_transport_parser_error(sip_transport_t* sender);

	static void sip_transport_packet_sent(sip_transport_t* sender, const sip_message_t& message);
	static void sip_transport_send_error(sip_transport_t* sender, const sip_message_t& message, int net_error);
	static void sip_transport_recv_error(sip_transport_t* sender, int net_error);
	static void sip_transport_disconnected(sip_transport_t* sender);
	/// ������ ��� ���������� ������������� ����
	/// sip_listener event handler routines
	static void sip_listener_new_connection(sip_transport_listener_t* sender, sip_transport_t* new_transport);
	static void sip_listener_transport_failure(sip_transport_listener_t* sender, int failure, const sip_transport_route_t& route);
	static void sip_listener_transport_started(sip_transport_listener_t* sender);
	static bool sip_listener_is_address_banned(sip_transport_listener_t* sender, in_addr recv_addr);
	static bool sip_listener_is_useragent_banned(sip_transport_listener_t* sender, const char* useragent);

	static void async_call(rtl::async_call_target_t* caller, uint16_t command, uintptr_t int_param, void* ptr_param);

	/// ���������� ���������� � ������-��������
	static bool start_reading(sip_transport_object_t* object);

	/// ���������� ������� ��� ���������� � ����������
	static sip_transport_pool_t& get_pool() { return m_pool; }
	static void destroy_sip_transports(sip_transport_point_t& iface);
protected:
	/// ��������/����� ������������ ������
	static bool bind_listener(uint16_t if_port, sip_transport_t type);
	static sip_transport_binding_t* bind_listener(const sip_transport_point_t& point);
	static sip_transport_binding_t* find_binding(const sip_transport_point_t& point);
	static sip_transport_binding_t* get_binding(const sip_transport_point_t& point);

	static sip_transport_t* capture_transport(sip_transport_route_t& route);
	static sip_transport_t* create_transport(sip_transport_route_t& route);
	static int send_message(sip_message_t& message);

	/// ��������� sip_async_object_t
	static void async_handle_call(void* userdata, uint16_t call_id, uintptr_t int_param, void* ptr_param);
	///---
	static void sip_async_call_packet_arrival(sip_transport_t* sender, raw_packet_t* packet);
	static void sip_async_call_parser_error(sip_transport_t* sender);
	static void sip_async_call_transport_send_error(sip_transport_t* sender, sip_message_t* message, int net_error);
	static void sip_async_call_transport_recv_error(sip_transport_t* sender, int net_error, sip_transport_route_t* route);
	static void sip_async_call_transport_disconnected(sip_transport_t* transport);
	static void sip_async_call_check_lifetimes();

	/// ��������� timer_event_handler_t
	static void timer_elapsed_event(rtl::Timer* sender, uint32_t elapsed, void* user_data, int tid);

	/// ���������� ������
	static void destroy_bindings();
	static void stop_readers();

	/// ���������� �����������
	static void update_interface_table();
};
//--------------------------------------
