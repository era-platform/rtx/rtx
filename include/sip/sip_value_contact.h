/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_uri.h"
#include "sip_value_user.h"
//--------------------------------------
// No Expires value
//--------------------------------------
#define CONTACT_HAS_NO_EXPORES	0xFFFFFFFFU
//--------------------------------------
// Contact
//--------------------------------------
class sip_value_contact_t : public sip_value_user_t
{
private:
	bool m_all_contact; // Contact: *
	uint32_t m_expires; // -1 signs having no expires parameter

private:
	virtual rtl::String& prepare(rtl::String& stream) const;
	virtual bool parse();

public:
	sip_value_contact_t() : m_all_contact(false), m_expires(CONTACT_HAS_NO_EXPORES) { }
	sip_value_contact_t(const sip_value_contact_t& v) { assign(v); }

	virtual void cleanup();
	virtual sip_value_t* clone() const;

	sip_value_contact_t& operator = (const sip_value_contact_t& v) { return assign(v); }
	sip_value_contact_t& assign(const sip_value_contact_t& v);

	bool get_asterisk_flag() const { return m_all_contact; }
	void set_asterisk_flag(bool all_contact) { m_all_contact = all_contact; }

	bool has_expires() const { return m_expires != CONTACT_HAS_NO_EXPORES; }
	uint32_t get_expires() const { return m_expires; }
	void set_expires(uint32_t expires);
};
//--------------------------------------
