/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_uri.h"
#include "sip_value.h"
//--------------------------------------
// To, From, Contact, Reply-to, Refer-to, Route, Record-Route
//--------------------------------------
class sip_value_uri_t : public sip_value_t
{
protected:
	rtl::String m_display_name;
	sip_uri_t m_uri;

protected:
	virtual rtl::String& prepare(rtl::String& stream) const;
	virtual bool parse();

public:
	sip_value_uri_t() { }
	sip_value_uri_t(const sip_value_uri_t& v) { assign(v); }
	virtual ~sip_value_uri_t();

	virtual void cleanup();
	virtual sip_value_t* clone() const;

	sip_value_uri_t& operator = (const sip_value_uri_t& value) { return assign(value); }
	sip_value_uri_t& assign(const sip_value_uri_t& value);
	sip_value_uri_t& assign(const rtl::String& value) { sip_value_t::assign(value); return *this; }

	const rtl::String& get_display_name() const { return m_display_name; }
	void set_display_name(const rtl::String& displayName) { m_display_name = displayName; }

	sip_uri_t& get_uri() { return m_uri; }
	const sip_uri_t& get_uri() const { return m_uri; }
	void set_uri(const sip_uri_t& uri) { m_uri = uri; }
};
//--------------------------------------

