/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_registrar_session.h"
//--------------------------------------
//
//--------------------------------------
class sip_subscribe_session_t
{
	sip_registrar_account_t& m_account;

	// ������ �������
	char* m_call_id;		// �� ������� SUBSCRIBE
	sip_uri_t m_from_uri;
	char* m_from_tag;		// �� ������� SUBSCRIBE
	sip_uri_t m_to_uri;
	char* m_to_tag;			// �� ������� SUBSCRIBE
	uint32_t m_cseq_in;		// ����� ���������� ������� SUBSCRIBE
	uint32_t m_cseq_out;	// ����� ���������� ������� NOTIFY

	sip_transport_route_t m_route;

	sip_transport_type_t m_via_type;		// ���� ����� ������
	in_addr m_via_address;
	uint16_t m_via_port;

	sip_transport_type_t m_contact_type;	// ���� ����� �������
	in_addr m_contact_address;
	uint16_t m_contact_port;

	bool m_over_nat;						// ������� ��������� ������� �� �� ���������� NAT

	uint32_t m_expires_value;
	time_t m_last_time;					// ������ ������� ����� �������

public:
	sip_subscribe_session_t(sip_registrar_account_t& account);
	~sip_subscribe_session_t();

	const char* get_call_id() const { return m_call_id; }
	uint64_t get_reg_time() { return m_last_time; }
	bool is_over_nat() const { return m_over_nat; }
	bool behind_nat() const { return m_route.type == sip_transport_udp_e && m_route.remoteAddress.s_addr != m_via_address.s_addr; }
	uint32_t get_expires_value() const { return m_expires_value; }

	bool check_transport_disconnected(const sip_transport_route_t& route);
	bool process_request(const sip_message_t& request, bool over_nat);
	void update_response(sip_message_t& response);

	// true - alive, false - expired
	bool check_expire_timer(time_t current_time);

	void send_notify(const sip_registrar_subscribe_info_t* notify_info);

private:
	void cleanup();
	bool uri_to_contact(const sip_value_contact_t& contact_uri);
};
//--------------------------------------
