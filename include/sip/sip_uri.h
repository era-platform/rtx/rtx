/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_mime_param.h"
//--------------------------------------
//
//--------------------------------------
class sip_uri_header_list_t
{
private:
	rtl::ArrayT<mime_param_t*> m_array;

private:
	const mime_param_t* find(const rtl::String& name, int index) const;
	mime_param_t* find(const rtl::String& name, int index);

public:
	sip_uri_header_list_t() { }
	sip_uri_header_list_t(const sip_uri_header_list_t& list) { assign(list); }
	~sip_uri_header_list_t() { clear(); }

	const rtl::String& get(const rtl::String& name, bool& found, int index = 0) const;
	void add(const rtl::String& name, const rtl::String& value);
	bool set(const rtl::String& name, const rtl::String& value, int index = 0);
	bool contains(const rtl::String& name, int index = 0) const { return find(name, index) != nullptr; }
	bool remove(const rtl::String& name, int index = 0);
	void clear();

	sip_uri_header_list_t& operator = (const sip_uri_header_list_t& list) { return assign(list); }
	sip_uri_header_list_t& assign(const sip_uri_header_list_t& list);

	int getCount() const { return m_array.getCount(); }
	mime_param_t* getAt(int index) { return m_array[index]; }
	const mime_param_t* getAt(int index) const { return m_array[index]; }
	void removeAt(int index);
};
//--------------------------------------
// sip:user:password@host:port;uri-parameters?headers
//--------------------------------------
class sip_uri_t
{
private:
	rtl::String m_scheme;
	rtl::String m_user;
	rtl::String m_password;
	rtl::String m_host;
	rtl::String m_port;

	mime_param_list_t m_params;
	sip_uri_header_list_t m_headers;

private:
	void parse_colon_pairs(const char* text, int length, rtl::String& left, rtl::String& right);
	void parse_param_set(const char* params, int length);
	void parse_param(const char* params, int length);
	void parse_header_set(const char* header_set, int length);
	void parse_header(const char* header, int length);

	rtl::String& params_to_string(rtl::String& stream) const;
	rtl::String& headers_to_string(rtl::String& stream) const;

public:
	sip_uri_t() : m_scheme("sip") { }
	sip_uri_t(const sip_uri_t& uri) { *this = uri; }
	sip_uri_t(const rtl::String& uri) { *this = uri; }
	sip_uri_t(const char* uri, int length = -1) { assign(uri, length); }

	sip_uri_t& operator = (const sip_uri_t& uri);
	sip_uri_t& operator = (const rtl::String& uri) { assign(uri, uri.getLength()); return *this; }
	sip_uri_t& operator = (const char* uri) { assign(uri, -1); return *this; }

	bool assign(const char* uri, int length = -1);

	void cleanup();

	rtl::String to_string(bool includeURIParams = true, bool includeUser = true, bool includePort = true) const;
	rtl::String& write_to(rtl::String& stream, bool includeURIParams = true, bool includeUser = true, bool includePort = true) const;

	const rtl::String& get_scheme() const { return  m_scheme; }
	void set_scheme(const rtl::String& scheme) {  m_scheme = scheme; }
	const rtl::String& get_user() const { return m_user; }
	void set_user(const rtl::String& user) { m_user = user; }
	const rtl::String& get_host() const { return m_host; }
	void set_host(const rtl::String& host) { m_host = host; }
	const rtl::String& get_password() const { return m_password; }
	void set_password(const rtl::String& password) { m_password = password; }
	const rtl::String& get_port() const { return m_port; }
	void set_port(const rtl::String& port) { m_port = port; }

	mime_param_list_t& get_parameters() { return m_params; }
	const mime_param_list_t& get_parameters() const { return m_params; }

	sip_uri_header_list_t& get_headers() { return m_headers; }
	const sip_uri_header_list_t& get_headers() const { return m_headers; }

	static const sip_uri_t empty;
};
//--------------------------------------
