/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once

#include "sip_parser_tools.h"
//--------------------------------------
//
//--------------------------------------
class sip_message_t;
//--------------------------------------
//
//--------------------------------------
class trans_id
{
public:
	trans_id();
	trans_id(const trans_id& transactionId);
	trans_id(const rtl::String& transactionId);
	trans_id(const sip_message_t& msg);

	trans_id& operator = (const trans_id& transactionId);
	trans_id& operator = (const rtl::String& transactionId);
	trans_id& operator = (const sip_message_t& msg);

	rtl::String AsString() const;
	rtl::String& PrintOn(rtl::String& strm) const;

	const rtl::String& GetCallId() const { return m_callid; }
	void SetCallId(const rtl::String& callId) { m_callid = callId; }
	const rtl::String& GetBranch() const { return m_branch; }
	void SetBranch(const rtl::String& branch) { m_branch = branch; }
	const rtl::String& GetMethod() const { return m_method; }
	void SetMethod(const rtl::String& method) { m_method = method; }
	const rtl::String& GetStateMachine() const { return m_state_machine; }
	void SetStateMachine(const rtl::String& stateMachine) { m_state_machine = stateMachine; }

	/// RFC 2543 support
	void SetFromTag(const rtl::String& tag) { m_from_tag = tag; }
	const rtl::String& GetFromTag() const { return m_from_tag; }
	bool IsRFC3261Compliant() const { return m_is_rfc3261_compliant; }

protected:
	rtl::String m_callid;
	rtl::String m_branch;
	rtl::String m_method;
	rtl::String m_state_machine;

	/// RFC 2543 support
	rtl::String m_from_tag;
	rtl::String m_cseq_number;
	bool m_is_rfc3261_compliant;
};
//--------------------------------------
