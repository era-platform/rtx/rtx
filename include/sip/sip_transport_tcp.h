/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_transport.h"
#include "tls_ctx.h"
#include "sip_transport_packet.h"
//--------------------------------------
// WebSocket header
//--------------------------------------
struct ws_header_t
{
	/*
      0                   1                   2                   3
      0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     +-+-+-+-+-------+-+-------------+-------------------------------+
     |F|R|R|R| opcode|M| Payload len |    Extended payload length    |
     |I|S|S|S|  (4)  |A|     (7)     |             (16/64)           |
     |N|V|V|V|       |S|             |   (if payload len==126/127)   |
     | |1|2|3|       |K|             |                               |
     +-+-+-+-+-------+-+-------------+ - - - - - - - - - - - - - - - +
     |     Extended payload length continued, if payload len == 127  |
     + - - - - - - - - - - - - - - - +-------------------------------+
     |                               |Masking-key, if MASK set to 1  |
     +-------------------------------+-------------------------------+
     | Masking-key (continued)       |          Payload Data         |
     +-------------------------------- - - - - - - - - - - - - - - - +
     :                     Payload Data continued ...                :
     + - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +
     |                     Payload Data continued ...                |
     +---------------------------------------------------------------+
	*/

	uint8_t opcode:4, reserverd:3, f_fin:1;
	uint8_t p_len_7: 7, f_mask:1;
};
//--------------------------------------
//
//--------------------------------------
enum ws_opcode_e
{
	ws_opcode_frame_continue = 0,	//  %x0 denotes a continuation frame
	ws_opcode_frame_text = 1,		//  %x1 denotes a text frame
    ws_opcode_frame_binary = 2,		//  %x2 denotes a binary frame
	ws_opcode_frame_reserved = 3,	//  %x3-7 are reserved for further non-control frames
	ws_opcode_ctrl_close = 8,		//  %x8 denotes a connection close
	ws_opcode_ctrl_ping = 9,		//  %x9 denotes a ping
	ws_opcode_ctrl_pong = 10,		//  %xA denotes a pong
	ws_opcode_ctrl_reserver = 11,	//  %xB-F are reserved for further control frames
};
//--------------------------------------
//
//--------------------------------------
enum ws_read_state_t
{
	ws_read_header_e,
	ws_read_length_e,
	ws_read_mask_e,
	ws_read_frame_e,
};
/*
7.4.1.  Defined Status Codes

   Endpoints MAY use the following pre-defined status codes when sending
   a Close frame.

   1000

      1000 indicates a normal closure, meaning that the purpose for
      which the connection was established has been fulfilled.

   1001

      1001 indicates that an endpoint is "going away", such as a server
      going down or a browser having navigated away from a page.

   1002

      1002 indicates that an endpoint is terminating the connection due
      to a protocol error.

   1003

      1003 indicates that an endpoint is terminating the connection
      because it has received a type of data it cannot accept (e.g., an
      endpoint that understands only text data MAY send this if it
      receives a binary message).

   1004

      Reserved.  The specific meaning might be defined in the future.

   1005

      1005 is a reserved value and MUST NOT be set as a status code in a
      Close control frame by an endpoint.  It is designated for use in
      applications expecting a status code to indicate that no status
      code was actually present.

   1006

      1006 is a reserved value and MUST NOT be set as a status code in a
      Close control frame by an endpoint.  It is designated for use in
      applications expecting a status code to indicate that the
      connection was closed abnormally, e.g., without sending or
      receiving a Close control frame.

   1007

      1007 indicates that an endpoint is terminating the connection
      because it has received data within a message that was not
      consistent with the type of the message (e.g., non-UTF-8 [RFC3629]
      data within a text message).

   1008

      1008 indicates that an endpoint is terminating the connection
      because it has received a message that violates its policy.  This
      is a generic status code that can be returned when there is no
      other more suitable status code (e.g., 1003 or 1009) or if there
      is a need to hide specific details about the policy.

   1009

      1009 indicates that an endpoint is terminating the connection
      because it has received a message that is too big for it to
      process.

   1010

      1010 indicates that an endpoint (client) is terminating the
      connection because it has expected the server to negotiate one or
      more extension, but the server didn't return them in the response
      message of the WebSocket handshake.  The list of extensions that
      are needed SHOULD appear in the /reason/ part of the Close frame.
      Note that this status code is not used by the server, because it
      can fail the WebSocket handshake instead.

   1011

      1011 indicates that a server is terminating the connection because
      it encountered an unexpected condition that prevented it from
      fulfilling the request.

   1015

      1015 is a reserved value and MUST NOT be set as a status code in a
      Close control frame by an endpoint.  It is designated for use in
      applications expecting a status code to indicate that the
      connection was closed due to a failure to perform a TLS handshake
      (e.g., the server certificate can't be verified).

7.4.2.  Reserved Status Code Ranges

   0-999

      Status codes in the range 0-999 are not used.

   1000-2999

      Status codes in the range 1000-2999 are reserved for definition by
      this protocol, its future revisions, and extensions specified in a
      permanent and readily available public specification.

   3000-3999

      Status codes in the range 3000-3999 are reserved for use by
      libraries, frameworks, and applications.  These status codes are
      registered directly with IANA.  The interpretation of these codes
      is undefined by this protocol.

   4000-4999

      Status codes in the range 4000-4999 are reserved for private use
      and thus can't be registered.  Such codes can be used by prior
      agreements between WebSocket applications.  The interpretation of
      these codes is undefined by this protocol.
*/

enum ws_status_code_e
{
	ws_status_code_normal_closure_1000	= 1000,
	ws_status_code_going_away_1001		= 1001,
	ws_status_code_protocol_error_1002	= 1002,
	ws_status_code_not_acceptable_1003	= 1003,
	ws_status_code_reserver_1004		= 1004,
	ws_status_code_expected_1005		= 1005,	// �� ��� ��������
	ws_status_code_expected_1006		= 1006,	// �� ��� ��������
	ws_status_code_bad_message_1007		= 1007,
	ws_status_code_unknown_1008			= 1008,
	ws_status_code_too_big_1009			= 1009,
	ws_status_code_expected_1010		= 1010,
	ws_status_code_unexpected_1011		= 1011,
	ws_status_code_expected_1015		= 1000,	// ������ ��� ��������, �� ��� ��������
};
//--------------------------------------
//
//--------------------------------------
enum sip_transport_read_state_t
{
	sip_transport_read_continue_e,
	sip_transport_read_need_more_data_e,
	sip_transport_read_parser_error_e,
	sip_transport_read_failure_e,
};
//--------------------------------------
//
//--------------------------------------
enum sip_transport_tcp_state_t
{
	sip_transport_tcp_state_idle, // client
	sip_transport_tcp_state_connecting,
	sip_transport_tcp_state_connected,
	sip_transport_tcp_state_sh_send,
	sip_transport_tcp_state_sh_both,
	sip_transport_tcp_state_closed,
};
enum sip_transport_tcp_type_t
{
	sip_transport_tcp_client,
	sip_transport_tcp_server,
};
//--------------------------------------
//
//--------------------------------------
class sip_transport_tcp_t : public sip_transport_t
{
protected:
	rtl::Mutex m_sender_sync;			// ������������� ������

	/// common
	net_socket_t m_tcp_socket;
	sip_transport_tcp_state_t m_state;
	sip_transport_tcp_type_t m_type;

	bool m_ws_proto;							// ������� ��������� WebSocket
	bool m_wait_first_packet_in;				// ������� ������ ������� ������ ��� ����������� ������� SIP ��� WebSocket
	raw_packet_t* m_packet;						// ��������� �� SIP ����� ��� ������
	
	int m_rx_buffer_size;						// ������ ���������� ������
	int m_rx_buffer_read_ptr;					// ��������� �� ������������� �������� ������
	uint8_t m_rx_buffer[SIP_PACKET_MAX + 4];	// ����� ��� ������ ����� ������ �� ����

	int (sip_transport_tcp_t::* receiver)(uint8_t* buffer, int size, int& written);	// ��������� �� ������� ������ �� ������
	int (sip_transport_tcp_t::* writer)(const void* data, int length);	// ��������� �� ������� ������ � �����

	/// tcp
	bool m_tcp_read_headers;					// ������� ������ SIP ����������
	int m_tcp_body_read;						// ������ ��������� ������ ���� SIP ���������

	/// tls
	bool m_tls_initial_packet;
	tls_ctx_t m_tls;

	uint8_t* m_tls_tx_buffer;

	uint8_t* m_tls_rx_buffer;
	int m_tls_rx_buffer_length;

	/// websocket
	bool m_ws_close_sent;						// ������� ���� ��� CLOSE ��� ���������
	ws_header_t m_ws_header;					// ��������� WebSocket ������
	ws_read_state_t m_ws_read_state;			// ��������� �������� WebSocket ������
	uint8_t m_ws_payload_mask[4];				// ����� ��� ��������
	uint32_t m_ws_payload_length_high;			// ����� ���� ������. ������� ������ ��� 64 ������ �����
	uint32_t m_ws_payload_length;				// ����� ���� ������. ������� ������ ��� 64 ������ �����, ��� ������ ����� ��� 32/16/8 ������� ���������
	int m_ws_payload_offset;					// �������� ���� ��������� ������������ �����
	uint8_t* m_ws_packet_buffer;				// ����� ��� ������ ������������� ������ (������� �� raw_packet_t)
	int m_ws_packet_written;					// ���������� ���������� ������

protected:
	virtual void data_ready();
	virtual void raise_packet_read(raw_packet_t* packet);
	/// ��������� sip_async_object_t
	virtual void async_handle_call(uint16_t command, uintptr_t int_param, void* ptr_param);

	virtual void destroy();

	void signal_transport_failure(int socket_result, const char* func);

	bool try_to_bind();

	// ������ �� �������������� ������ : true ���� ���������� ������, false ���� ������������
	bool rx_buffer_read(void* buffer, int size);

	// read sip or http message
	void tcp_process_data();
	bool tcp_read_sip_headers();
	bool tcp_read_sip_body();

	// read web_socket frame
	void ws_process_data();
	sip_transport_read_state_t ws_read_header();
	sip_transport_read_state_t ws_read_length();
	sip_transport_read_state_t ws_read_mask();
	sip_transport_read_state_t ws_read_frame();

	void ws_process_handshake_request(raw_packet_t* packet);
	void ws_send_handshake_accept(const char* key);
	void ws_send_handshake_reject();

	bool ws_process_frame();
	bool ws_process_sip_packet();
	bool ws_process_close();
	bool ws_process_ping();
	bool ws_process_pong();

	//bool ws_check_packet_header();
	
	int ws_send_frame(ws_opcode_e op_code, const void* packet, int packet_length);
	int ws_send_message(const sip_message_t& message);

	// read tls record
	void tls_read_socket(int amount);

	// output interface for "write to" tcp or tls type
	int write_tcp_socket(const void* data, int length);
	int write_tls_socket(const void* data, int length);

	int read_tcp_socket(uint8_t* buffer, int size, int& written);
	int read_tls_socket(uint8_t* buffer, int size, int& written);

	int read_socket(uint8_t* buffer, int size, int& written) { return (this->*receiver)(buffer, size, written); }
	int write_socket(const void* data, int length) { return (this->*writer)(data, length); }

	// ��������� ����������� ������ � ������ TLS
	int tls_process_incoming_data(uint8_t* buffer, int size, int& written);
	bool tls_connect();


	void initialize();
	
	/// internal interface
	virtual void disconnect();
	virtual void send_keep_alive();
	virtual bool is_valid();
public:
	sip_transport_tcp_t(const sip_transport_point_t& iface, const sip_transport_point_t& endpoint, SOCKET handle = INVALID_SOCKET);
	virtual ~sip_transport_tcp_t();

	virtual bool connect();
	virtual int send_message(const sip_message_t& message);
};
//--------------------------------------
