/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_transport_defs.h"
//--------------------------------------
//
//--------------------------------------
class sip_transport_pool_t
{
	/// набор транспортов
	rtl::Mutex m_sync;
	typedef sip_transport_t* transport_ptr;
	rtl::SortedArrayT<transport_ptr, uint64_t> m_list;

public:
	sip_transport_pool_t();
	~sip_transport_pool_t();

	void cleanup();

	bool add_transport(sip_transport_t* transport);
	void remove_transport(sip_transport_t* transport);
	sip_transport_t* find_transport(const sip_transport_point_t& endpoint);
	sip_transport_t* find_transport(const sip_transport_route_t& route);
	void reset_transport_type(sip_transport_t* transport, sip_transport_type_t new_type);
	void check_transport_timeouts();
	void disable_transports(sip_transport_point_t& iface);

private:
	static int compare_key(const uint64_t& left, const transport_ptr& right);
	static int compare_transport(const transport_ptr& left, const transport_ptr& right);
};
//--------------------------------------
