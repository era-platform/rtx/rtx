/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_transport_manager.h"
#include "sip_transport_reader.h"

#define MAKE_KEY64(a, p, t) uint64_t((uint64_t(a.s_addr) << 32) + (uint64_t(p) << 16) + t)

#define ASYNC_TRANSP_BASE		100
#define ASYNC_TRANSP_KEEPALIVE	1
//--------------------------------------
//
//--------------------------------------
class sip_transport_t : public sip_transport_object_t, public rtl::async_call_target_t
{
protected:
	sip_transport_route_t m_route;			// �������� ��������
	volatile int32_t m_user_count;				// ���������� ������������� ��� ������������ ������� �����
	bool m_keep_alive_started;
	int m_keep_alive_value;					// ��������� �������� � NAT ������. 0 - disabled
	uint64_t m_key;							// ���� ������ (���)
	uint32_t m_last_activity;				// ����� ��������� ���������� (�����������, ��������� � �������� �������)

protected:
	friend class sip_transport_manager_t;
	friend class sip_transport_pool_t;

	/// transport object overrides
	virtual void data_ready();

	/// event handler wrapper functions
	virtual void raise_packet_read(raw_packet_t* packet);
	virtual void raise_parser_error();
	virtual void raise_packet_sent(const sip_message_t& message);
	virtual void raise_send_error(const sip_message_t& message, int net_error);
	virtual void raise_recv_error(int net_error);
	virtual void raise_stopped();

	/// ��������� sip_async_object_t
	virtual const char* async_get_name();
	virtual void async_handle_call(uint16_t command, uintptr_t int_param, void* ptr_param);

	/// ���������� �������
	bool filter_packet(raw_packet_t* packet);
	virtual void destroy();
	void reset_type(sip_transport_type_t type) { m_route.type = type; m_key = MAKE_KEY64(m_route.remoteAddress, m_route.remotePort, m_route.type); }
	void set_keep_alive_timer(int value);
	static void timer_event_callback(rtl::Timer* sender, uint32_t elapsed, void* user_data, int tid);

	void start_keep_alive();
	void stop_keep_alive();

public:
	sip_transport_t(const sip_transport_point_t& iface, const sip_transport_point_t& endpoint, net_socket_t& sock);
	virtual ~sip_transport_t();

	/// public interface
	virtual bool connect();
	virtual void disconnect();
	virtual int send_message(const sip_message_t& message);
	virtual void send_keep_alive();
	virtual bool is_valid();

	void capture_(const char* user);
	void release_(const char* user);

	long get_user_count() const { return m_user_count; }
	const char* getName() const { return m_name; }
	uint64_t get_key() const { return m_key; }
	uint32_t timestamp() const { return m_last_activity; }
	const sip_transport_route_t& getRoute() { return m_route; }
};
//--------------------------------------
