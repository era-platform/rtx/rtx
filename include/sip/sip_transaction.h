/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_timer_manager.h"
#include "sip_message.h"
//--------------------------------------
//
//--------------------------------------
enum sip_transaction_event_t
{
	sip_transaction_event_unknown_e,
	sip_transaction_event_timer_e,
	sip_transaction_event_message_e,
	sip_transaction_event_final_e,
	sip_transaction_event_cancel_e
};
const char* get_sip_transaction_event_name(sip_transaction_event_t type);
//--------------------------------------
//
//--------------------------------------
enum sip_transaction_type_t
{
	sip_transaction_ict_e,
	sip_transaction_ist_e,
	sip_transaction_nict_e,
	sip_transaction_nist_e
};
const char* get_sip_transaction_type_name(sip_transaction_type_t type);
//--------------------------------------
//
//--------------------------------------
struct sip_transaction_event_info_t
{
	sip_transaction_event_t type;
	uintptr_t param1;
	uintptr_t param2;
};
//--------------------------------------
//
//--------------------------------------
class sip_transaction_stage_t;
class sip_transaction_manager_t;
//--------------------------------------
//
//--------------------------------------
class sip_transaction_t : public sip_timer_user_t
{
public:

	sip_transaction_t(const trans_id& id, sip_transaction_type_t type, sip_transaction_manager_t& manager);
	virtual ~sip_transaction_t();

public:
	void push_event(sip_transaction_event_t event, uintptr_t param1, uintptr_t param2);
	bool pop_event(sip_transaction_event_info_t* event);
	void flush_events();
	void process_events();

	long add_ref() { return std_interlocked_inc(&m_ref_count); }
	long release_ref() { return std_interlocked_dec(&m_ref_count); }
	long get_ref_count() { return m_ref_count; }

	virtual void process_sip_timer_event(sip_transaction_timer_t timerType, uint32_t intrval);
	virtual void process_sip_transaction_event(sip_message_t& message);
	virtual void process_sip_transaction_final_event();
	virtual void process_sip_transaction_cancel_event();

	void set_opening_request(const sip_message_t& message);
	void set_ack_id(const rtl::String& id) { m_ack_id = id; }
	void set_state(int state) { m_state = state; }

	//sip_transaction_type_t getType() { return m_type; }
	const trans_id& get_identifier() { return m_id; }
	const rtl::String& get_identifier_string() { return m_id_str; }
	const rtl::String& get_ack_id() { return m_ack_id; }
	const char* get_iid() { return m_internal_id; };
	const char* get_core_name() { return m_ua_core_name; };
	const char* get_type_name() { return get_sip_transaction_type_name(m_type); }

protected:
	void destroy();
	bool send_message_to_transport(sip_message_t& msg);
	bool is_reliable_transport() { return m_reliable_transport; };

protected:
	sip_transaction_manager_t& m_manager;
	sip_transaction_type_t m_type;
	rtl::MutexWatch m_sync;
	volatile int32_t m_ref_count;
	rtl::MutexWatch m_event_queue_lock;
	rtl::QueueT<sip_transaction_event_info_t> m_event_queue;
	rtl::String m_ack_id;
	trans_id m_id;
	rtl::String m_id_str;
	int m_state;
	sip_message_t m_opening_request;
	bool m_terminating;
	bool m_terminated;
	bool m_reliable_transport;
	char m_ua_core_name[65];
	rtl::MutexWatch m_send_lock;
	static volatile int32_t m_internal_id_gen;

	friend class sip_transaction_manager_t;
	friend class sip_transaction_stage_t;

private:
	virtual void sip_timer_elapsed(sip_transaction_timer_t type, uint32_t intrval);
};
//--------------------------------------
//
//--------------------------------------
class sip_transaction_list_t
{
public:
	struct TRN_KEY
	{
		char* key;
		sip_transaction_t* transaction;
	};
	
	sip_transaction_list_t();
	~sip_transaction_list_t();

	bool add(sip_transaction_t* transaction, const char* key);
	sip_transaction_t* remove(rtl::ZString key);
	void remove(sip_transaction_t* transaction);
	sip_transaction_t* find(rtl::ZString key);

	int getCount() { return m_pool.getCount(); }
	sip_transaction_t* getAt(int index) { return m_pool.getAt(index).transaction; }

	void clear();

private:
	rtl::SortedArrayT<TRN_KEY, rtl::ZString> m_pool;
};
//--------------------------------------
