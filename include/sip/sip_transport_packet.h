/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_transport.h"
//--------------------------------------
//
//--------------------------------------
#define SIP_PACKET_MAX (64 * 1024)
//--------------------------------------
//
//--------------------------------------
class raw_start_line_t
{
	bool m_request;
	const char* m_part1;
	const char* m_part2;
	const char* m_part3;

public:
	raw_start_line_t() : m_request(false), m_part1(nullptr), m_part2(nullptr), m_part3(nullptr) {}

	char* parse(char* stream);
	void cleanup() { m_part1 = m_part2 = m_part3 = nullptr; m_request = false; }

	bool is_request() const { return m_request; }
	bool is_valid() const { return m_part1 != nullptr && m_part2 != nullptr && m_part3 != nullptr; }

	const char* get_version() const { return m_request ? m_part3 : m_part1; }
	const char* get_method() const { return m_request ? m_part1 : nullptr; }
	const char* get_request_uri() const { return m_request ? m_part2 : nullptr; }
	const char* get_code() const { return !m_request ? m_part2 : nullptr; }
	const char* get_reason() const { return !m_request ? m_part3 : nullptr; }

	int to_string(char* buffer, int size);
};
//--------------------------------------
//
//--------------------------------------
struct raw_header_t
{
	const char* name;
	const char* value;
};
//--------------------------------------
//
//--------------------------------------
//enum sip_transport_type_t;
//--------------------------------------
//
//--------------------------------------
class raw_packet_t
{
protected:
	// ������ ����������� � ���������� (�������)
	sip_transport_route_t m_route;
	//sip_transport_ptr m_route;

	// ��������� ������, ��������� � ���� ����� ���������� ������
	raw_start_line_t m_start_line;
	rtl::ArrayT<raw_header_t> m_headers;
	// ������ �� ������������ ���� ������� ����� ����������� ���
	// ��������������� ���������
	const char* m_useragent;		// ��� ���������� �� ���� User-Agent
	uint8_t* m_body;
	int m_body_length;
	int m_body_written;

	// �������� ������
	char* m_buffer;
	int m_buffer_length;

	char* parse_next_header(raw_header_t& header, char* stream);
	void add_header(raw_header_t& header);
	void cleanup();

	static rtl::Mutex s_pool_sync;
	static rtl::QueueT<raw_packet_t*> s_pool_queue;

public:
	raw_packet_t(bool is_udp);
	virtual ~raw_packet_t();

	// ������ ����������� � ����������
	sip_transport_type_t get_route_type() const { return m_route.type; }
	in_addr get_receive_address() const { return m_route.remoteAddress; }
	uint16_t get_receive_port() const { return m_route.remotePort; }
	in_addr get_interface_address() const { return m_route.if_address; }
	uint16_t get_interface_port() const { return m_route.if_port; }
	
	const sip_transport_route_t& getRoute() const { return m_route; }
	void set_route(const sip_transport_route_t& route);
	void set_route(sip_transport_type_t type, in_addr ifaddr, uint16_t ifport, in_addr raddr, uint16_t rport);

	int get_packet_length() const { return m_buffer_length; }

	const raw_start_line_t& get_start_line() const { return m_start_line; }
	int get_header_count() const { return m_headers.getCount(); }
	const raw_header_t& get_header_at(int index) const { return m_headers.getAt(index); }
	const raw_header_t* get_header(const char* name);
	const char* get_useragent() const { return m_useragent; }

	int get_body_length() const { return m_body_length; }
	const uint8_t* get_body() const { return m_body_length > 0 ? m_body : nullptr; }
	bool is_valid() const { return m_start_line.is_valid(); }

	// for transports with framed reads
	char* get_buffer();
	bool parse(int length);
	bool parse(const uint8_t* packet, int length);
	// for transports with streamed reads
	int add_line(const char* line, int length);
	int parse_headers();
	int add_body(const uint8_t* body, int length);

	int to_string(char* buffer, int size);

	static raw_packet_t* get_free(bool is_udp = true);
	static void release(raw_packet_t* packet);
	//static void create_pool();
	//static void destroy_pool();
};
//--------------------------------------
