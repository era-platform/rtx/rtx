/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_value_uri.h"
//--------------------------------------
// To, From
//--------------------------------------
class sip_value_user_t : public sip_value_uri_t
{
protected:
	rtl::String m_tag;

protected:
	virtual rtl::String& prepare(rtl::String& stream) const;
	virtual bool parse();

public:
	sip_value_user_t() { }
	sip_value_user_t(const sip_value_user_t& v) { assign(v); }
	virtual ~sip_value_user_t();

	virtual void cleanup();
	virtual sip_value_t* clone() const;

	sip_value_user_t& operator = (const sip_value_user_t& value) { return assign(value); }
	sip_value_user_t& assign(const sip_value_user_t& value);

	const rtl::String& get_tag() const { return m_tag; }
	void set_tag(const rtl::String& tag) { m_tag = tag; }
};
//--------------------------------------
