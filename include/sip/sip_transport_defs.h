/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

//--------------------------------------
// ��������������� ���������� �������
//--------------------------------------
class sip_message_t;
class raw_packet_t;					// ������ ������ SIP �������
class sip_transport_object_t;		// ������� ������ ��� ������ �� ����
class sip_transport_reader_t;		// �����-�������� ������� �� ����
class sip_transport_manager_t;		// ���������� ���� �������� ����������
class sip_transport_t;				// ��������� ��� ����������� � ��������� �������
class sip_transport_listener_t;		// ��������� ����� ����������
class sip_transport_binding_t;		// �������� ���������� � ������/����� ��� ����� ����������
class sip_transport_pool_t;
//--------------------------------------
//
//--------------------------------------
enum sip_transport_type_t
{
	sip_transport_udp_e = 0,			// ����������� ����� �� udp
	sip_transport_tcp_e = 1,			// ����������� ����� �� tcp
	sip_transport_tls_e = 2,			// ����������� ����� �� tcp
	sip_transport_ws_e = 3,			// ����������� ����� �� WebSocket
	sip_transport_wss_e = 4,			// ����������� WebSocket
	//	sip_transport_sctp_e,			// ����������� ����� �� sctp
};
const char* TransportType_toString(sip_transport_type_t type);
sip_transport_type_t parse_transport_type(const char* name);
sip_transport_type_t parse_transport_type(const char* name, int length);
inline bool compare_listener_type(sip_transport_type_t left, sip_transport_type_t right)
{
	return left == sip_transport_udp_e ?
		right == sip_transport_udp_e :
		right != sip_transport_udp_e;
}
//--------------------------------------
// ��������� �������� ����� ���������
//--------------------------------------
struct sip_transport_point_t
{
	sip_transport_type_t type;
	in_addr address;
	uint16_t port;
};
//--------------------------------------
// ��������� ���������
//--------------------------------------
struct sip_transport_route_t
{
	sip_transport_type_t type;
	in_addr if_address;
	uint16_t if_port;
	in_addr remoteAddress;
	uint16_t remotePort;
};
const char* route_to_string(char* buffer, int length, const sip_transport_route_t& route);
const char* point_to_string(char* buffer, int length, const sip_transport_point_t& point);
inline bool operator == (const sip_transport_route_t& left, const sip_transport_route_t& right)
{
	return left.type == right.type && left.if_address.s_addr == right.if_address.s_addr && left.if_port == right.if_port &&
		left.remoteAddress.s_addr == right.remoteAddress.s_addr && left.remotePort == right.remotePort;
}
inline bool operator != (const sip_transport_route_t& left, const sip_transport_route_t& right)
{
	return left.type != right.type || left.if_address.s_addr != right.if_address.s_addr || left.if_port != right.if_port ||
		left.remoteAddress.s_addr != right.remoteAddress.s_addr || left.remotePort != right.remotePort;
}
inline bool is_route_empty(const sip_transport_route_t& route)
{
	return route.if_address.s_addr == 0 && route.remoteAddress.s_addr == 0;
}
//--------------------------------------
