/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

//--------------------------------------
//
//--------------------------------------
#define SIP_TIMER_RESOLUTION 100

#define __SECONDS__(v)	(v*1000)
#define __MINUTE__(v)	(v*60000)

// T1		500ms default	Section 17.1.1.1		RTT Estimate
#define SIP_INTERVAL_T1	500

// T2		4s				Section 17.1.2.2		The maximum retransmit interval for non-INVITE
//													requests and INVITE responses
#define SIP_INTERVAL_T2 __SECONDS__(4)

// T4		5s				Section 17.1.2.2		Maximum duration a message will remain in the network
#define SIP_INTERVAL_T4 __SECONDS__(5)

// Timer A	initially T1	Section 17.1.1.2		INVITE request retransmit interval, for UDP only
#define SIP_INTERVAL_A SIP_INTERVAL_T1

// Timer B	64*T1			Section 17.1.1.2		INVITE transaction timeout timer
#ifndef CLIENT_TRANSACTION_RETRANSMISSION_UNIT
#define SIP_INTERVAL_B SIP_INTERVAL_T1 * 64
#else
#define SIP_INTERVAL_B SIP_INTERVAL_T1 * CLIENT_TRANSACTION_RETRANSMISSION_UNIT
#endif

// Timer C	>3min			Section 16.6			proxy INVITE transaction
//							bullet 11				timeout
#define SIP_INTERVAL_C __MINUTE__(3) + __SECONDS__(1)

// Timer D	> 32s for UDP	Section 17.1.1.2		Wait time for response
//			0s for TCP/SCTP							retransmits
#define SIP_INTERVAL_D __SECONDS__(32)

// Timer E	initially T1	Section 17.1.2.2		non-INVITE request retransmit interval, UDP only
#define SIP_INTERVAL_E SIP_INTERVAL_T1

// Timer F	64*T1			Section 17.1.2.2		non-INVITE transaction timeout timer
#ifndef CLIENT_TRANSACTION_RETRANSMISSION_UNIT
#define SIP_INTERVAL_F SIP_INTERVAL_T1 * 64
#else
#define SIP_INTERVAL_F SIP_INTERVAL_T1 * CLIENT_TRANSACTION_RETRANSMISSION_UNIT
#endif

// Timer G	initially T1	Section 17.2.1			INVITE response retransmit interval
#define SIP_INTERVAL_G SIP_INTERVAL_T1

// Timer H	64*T1			Section 17.2.1			Wait time for ACK receipt
#define SIP_INTERVAL_H 64*SIP_INTERVAL_T1

// Timer I	T4 for UDP		Section 17.2.1			Wait time for
//			0s for TCP/SCTP							ACK retransmits
#define SIP_INTERVAL_I SIP_INTERVAL_T4

// Timer J	64*T1 for UDP	Section 17.2.2			Wait time for
//         0s for TCP/SCTP							non-INVITE request retransmits
#define SIP_INTERVAL_J 64*SIP_INTERVAL_T1

// Timer K	T4 for UDP		Section 17.1.2.2		Wait time for response retransmits
//			0s for TCP/SCTP
#define SIP_INTERVAL_K SIP_INTERVAL_T4
//--------------------------------------
//
//--------------------------------------
enum sip_transaction_timer_t
{
	sip_timer_A_e, sip_timer_B_e, sip_timer_C_e, sip_timer_D_e,
	sip_timer_E_e, sip_timer_F_e, sip_timer_G_e, sip_timer_H_e,
	sip_timer_I_e, sip_timer_J_e, sip_timer_K_e, sip_timer_life_span_e,
	sip_timer_reg_e, sip_timer_ping_e,
};
//--------------------------------------
//
//--------------------------------------
class sip_timer_user_t;
//--------------------------------------
//
//--------------------------------------
struct sip_timer_info_t
{
	sip_timer_user_t* handler;
	sip_transaction_timer_t type;
	uint32_t interval;
	uint32_t start_time;
	bool enabled;
	char name[32];
};
struct sip_timer_key_t
{
	sip_timer_user_t* handler;
	sip_transaction_timer_t type;
};
//--------------------------------------
//
//--------------------------------------
typedef rtl::SortedArrayT<sip_timer_info_t, sip_timer_key_t> sip_timer_user_list_t;
//--------------------------------------
//
//--------------------------------------
class sip_timer_manager_t : rtl::ITimerEventHandler
{
	rtl::MutexWatch m_sip_timer_sync;
	sip_timer_user_list_t m_sip_timer_list;
	rtl::Timer* m_sip_timer;

	uint32_t m_sip_timer_last_tick;
	volatile uint32_t m_sip_timer_resolution;

public:
	sip_timer_manager_t();
	virtual ~sip_timer_manager_t();

	bool start_timer_manager(uint32_t dwResolution = SIP_TIMER_RESOLUTION);
	void stop_timer_manager();

	bool timer_manager_started() { return m_sip_timer->isStarted(); }
	uint32_t get_resolution() { return m_sip_timer_resolution; }

	void start_timer(sip_timer_user_t* handler, sip_transaction_timer_t type, uint32_t interval);
	void stop_timer(sip_timer_user_t* handler, sip_transaction_timer_t type);
	void remove_timer(sip_timer_user_t* handler, sip_transaction_timer_t type);
	void remove_all_timers(sip_timer_user_t* handler);
	
	static const char* GetTimerName(sip_transaction_timer_t type);

private:
	virtual void timer_elapsed_event(rtl::Timer* sender, int tid);

	sip_timer_info_t* find_timer(sip_timer_user_t* handler, sip_transaction_timer_t type);
};
//--------------------------------------
//
//--------------------------------------
class sip_timer_user_t
{
protected:
	char m_internal_id[32];
	sip_timer_manager_t& m_timer_manager;

public:
	sip_timer_user_t(sip_timer_manager_t& manager);
	virtual ~sip_timer_user_t();

	virtual void sip_timer_elapsed(sip_transaction_timer_t type, uint32_t interval) = 0;

	const char*	get_timer_id() { return m_internal_id; }

protected:
	/// 2 minutes
	void start_lifespan_timer(uint32_t interval = 120000) { m_timer_manager.start_timer(this, sip_timer_life_span_e, interval); }
	void stop_lifespan_timer() { m_timer_manager.stop_timer(this, sip_timer_life_span_e); }

	void start_timer(sip_transaction_timer_t type, uint32_t interval) { m_timer_manager.start_timer(this, type, interval); }
	void stop_timer(sip_transaction_timer_t type) { m_timer_manager.stop_timer(this, type); }

	void stop_all_timers() { m_timer_manager.remove_all_timers(this); }
};

//--------------------------------------
