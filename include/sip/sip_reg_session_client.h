/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_stack.h"
#include "sip_profile.h"
#include "sip_session.h"
#include "sip_reg_session_manager.h"
#include "std_crypto.h"
//--------------------------------------
//
//--------------------------------------
class sip_reg_session_client_t : public sip_session_t
{
	#pragma region private fields & types
	bool m_registered;
	bool m_for_unregister;
	int m_last_expires;		// 
	int m_authorization_attempts;
	char m_auth_nonce[MD5_HASH_HEX_SIZE+1];
	sip_reg_session_event_handler_t* m_event_handler;
	uint32_t m_register_qop_count;
	rtl::String m_nonce;
	sip_message_t m_register;

	enum SessionEvent
	{
		OptionsRequest = 101,
	};
	#pragma endregion

	#pragma region constructors & destructor
public:
	sip_reg_session_client_t(sip_reg_session_manager_t& sessionManager, const sip_profile_t& profile, const char* callId);
	sip_reg_session_client_t(sip_reg_session_manager_t& sessionManager, const sip_message_t& request);
	virtual ~sip_reg_session_client_t();
	#pragma endregion

	#pragma region public interface
	void send_register(sip_reg_session_event_handler_t* handler);
	void send_unregister();

	bool is_registered() const { return m_registered; }
	void set_nat_keep_alive(uint32_t interval);
	void set_expires(int expires);
	#pragma endregion

	#pragma region protected overrides
protected:
	virtual bool process_incoming_sip_message(sip_event_message_t& messageEvent);
	virtual void process_session_event(int event, const sip_message_t& eventMsg);

	virtual void process_timer_expires_event(sip_event_timer_expires_t& /*timerEvent*/);
	virtual void process_session_expire_event();

	virtual void process_destroy_event();
	#pragma endregion

	#pragma region private routines
private:
	friend sip_reg_session_manager_t;

	/// server callbacks
	void process_response(const sip_message_t& response);
	void process_failure(const sip_message_t& response);
	void process_2xx_response(const sip_message_t& response);
	void process_authenticate(const sip_message_t& response);
	void process_interval_to_brief(const sip_message_t& response);
	void process_options(const sip_message_t& request);
	void process_transport_failure();

	bool calc_expires_from_response(const sip_message_t& response);
	void start_expires_timer(int delta);

	bool construct_register(sip_message_t& request);
	bool construct_authorization(const sip_message_t& registerRequest, const sip_message_t& challengeResponse, sip_message_t& wwwAuthenticate);
	bool construct_authentication(const sip_message_t& registerRequest, const sip_message_t& challengeResponse, sip_message_t& wwwAuthenticate);

	in_addr get_via_address();
	uint16_t get_via_port();
	#pragma endregion
};
//--------------------------------------
