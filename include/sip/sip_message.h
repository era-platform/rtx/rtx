/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_uri.h"
#include "sip_trans_id.h"
#include "sip_header.h"
#include "sip_value_uri.h"
#include "sip_value_auth.h"
#include "sip_value_num.h"
#include "sip_value_user.h"
#include "sip_value_via.h"
#include "sip_value_cseq.h"
#include "sip_value_contact.h"
#include "sip_transport_packet.h"
#include "sip_parser_tools.h"
//--------------------------------------
//
//--------------------------------------
#define SIP_DEFAULT_MAX_FORWARDS 70
//--------------------------------------
//
//--------------------------------------
class sip_message_t
{
private:
	/// ��������� �����
	bool m_request;
	sip_method_type_t m_request_method;	// ������������ ����� � ������� ��� ���������� ������ �������
	rtl::String m_request_method_name;
	sip_uri_t m_request_uri;
	rtl::String m_version;
	sip_status_t m_response_code;
	rtl::String m_response_reason;

	/// ��������� �� �������� � ����������� �������������� �����
	sip_header_t* m_std_headers[sip_header_max_e];

	/// ������������� ����
	rtl::ArrayT<sip_header_t*> m_custom_headers;

	/// ���� ���������
	int m_body_length;
	uint8_t* m_body;

	/// ��������� ������
	sip_transport_route_t m_route;
	bool m_over_nat;
	rtl::String m_trans_id;
	bool m_has_trans;
	rtl::String m_ua_name;
	bool m_retransmission;

private:
	void add_header(const raw_header_t& raw_header);
	rtl::String& write_header(rtl::String& stream, const sip_header_t* header) const;
	rtl::String& write_header_milti(rtl::String& stream, const sip_header_t& header) const;
	rtl::String& write_header_single(rtl::String& stream, const sip_header_t& header) const;

public:
	sip_message_t();
	sip_message_t(const sip_message_t& sip_packet);
	sip_message_t(const raw_packet_t& raw_packet);

	~sip_message_t();

	sip_message_t& operator = (const sip_message_t& message) { return assign(message); }

	sip_message_t& assign(const sip_message_t& sip_packet);
	sip_message_t& assign(const raw_packet_t& raw_packet);
	void cleanup();

	rtl::String to_string() const { rtl::String value; write_to(value); return value; }
	rtl::String& write_to(rtl::String& stream) const;

	// ����� ������� ��� ��������� ����� � ���������� ������ ����� ��������
	bool is_request() const { return m_request; }
	bool is_response() const { return !m_request; }
	sip_method_type_t get_request_method() const { return m_request_method; }
	const rtl::String& get_request_method_name() const { return m_request_method != sip_custom_method_e ? sip_get_method_name(m_request_method) : m_request_method_name; }
	const sip_uri_t& get_request_uri() const { return m_request ? m_request_uri : sip_uri_t::empty; }

	sip_status_t get_response_code() const { return !m_request ? m_response_code : sip_000; }
	const rtl::String& get_response_reason() const { return !m_request ? m_response_reason : rtl::String::empty; }
	const rtl::String& get_version() const { return m_version; }

	void set_request_line(sip_method_type_t method, sip_uri_t& request_uri, const rtl::String& version = rtl::String::empty);
	void set_request_line(const rtl::String& custom, sip_uri_t& request_uri, const rtl::String& version = rtl::String::empty);
	void set_request_method(sip_method_type_t type);
	void set_request_method(const rtl::String& custom_method);
	void set_request_uri(const sip_uri_t& uri) { m_request_uri = uri; }
	void set_request_uri(const rtl::String& uri) { m_request_uri = uri; }
	void set_response_line(sip_status_t code, const rtl::String& reson = rtl::String::empty, const rtl::String& version = rtl::String::empty);

	/// Standart Message Headers
	sip_header_t* make_std_header(sip_header_type_t type);
	sip_header_t* get_std_header(sip_header_type_t type) { return type >= sip_Accept_e && type < sip_custom_header_e ? m_std_headers[type] : nullptr; }
	const sip_header_t* get_std_header(sip_header_type_t type) const { return type >= sip_Accept_e && type < sip_custom_header_e ? m_std_headers[type] : nullptr; }

	/// Custom Message Headers
	sip_header_t* get_custom_header(const char* name);
	const sip_header_t* get_custom_header(const char* name) const;
	
	const sip_header_t* get_custom_header_at(int index) const { return m_custom_headers[index]; }
	int get_custom_header_count() const { return m_custom_headers.getCount(); }
	

	/// Common Message Header routines
	void set_std_header(const sip_header_t& h);
	void set_std_header(const sip_header_t* h);
	void set_custom_header(const sip_header_t& h);

	sip_value_t* make_std_value(sip_header_type_t h_name);
	
	void remove_std_header(sip_header_type_t type);
	void remove_custom_header(const char* name);

	/// Message Body
	int get_body_length() const { return m_body_length; }
	const void* get_body() const { return m_body; }
	void set_body(const void* body, int length);
	void set_body(const char* body);
	bool has_sdp() const;
	bool has_body() const { return m_body != nullptr && m_body_length > 0; }

	/// Message transport
	sip_transport_type_t get_transport_type() const { return m_route.type; }
	
	in_addr get_remote_address() const { return m_route.remoteAddress; }
	uint16_t get_remote_port() const { return m_route.remotePort; }
	
	in_addr get_interface_address() const { return m_route.if_address; }
	uint16_t get_interface_port() const { return m_route.if_port; }
	
	void set_route(const sip_transport_route_t& route) { m_route = route; }
	const sip_transport_route_t& getRoute() const { return m_route; }

	void set_over_nat_flag(bool over_nat) { m_over_nat = over_nat; }
	bool get_over_nat_flag() const { return m_over_nat; }
	
	bool FixViaNATParameters();
	

	/// spesial sugar functions
	const rtl::String& CallId() const { const sip_value_t* v = get_Call_ID_value(); return v != nullptr ? v->value() : rtl::String::empty; }
	void CallId(const rtl::String& callid) { make_Call_ID_value()->assign(callid); }
	
	const rtl::String& Via_Branch() const { const sip_value_via_t* v = get_Via_value(); return v != nullptr ? v->get_branch() : rtl::String::empty; }
	void Via_Branch(const rtl::String& branch) { make_Via_value()->set_branch(branch); }

	const rtl::String& CSeq_Method() const { const sip_value_cseq_t* v = get_CSeq_value(); return v != nullptr ? v->get_method() : rtl::String::empty; }
	uint32_t CSeq_Number() const { const sip_value_cseq_t* v = get_CSeq_value(); return v != nullptr ? v->get_number() : 0; }
	void CSeq(uint32_t cseq, const rtl::String& method) { make_CSeq_value()->assign(cseq, method); }
	void CSeq_Method(const rtl::String& method) { make_CSeq_value()->set_method(method); }
	void CSeq_Number(uint32_t cseq) { make_CSeq_value()->set_number(cseq); }
	void CSeq_Increment() { make_CSeq_value()->increment_number(); }
	
	const rtl::String& From_Tag() const { const sip_value_user_t* v = get_From_value(); return v != nullptr ? v->get_tag() : rtl::String::empty; }
	void From_Tag(const rtl::String& tag) { make_From_value()->set_tag(tag); }
	const sip_uri_t& From_URI() const { const sip_value_user_t* v = get_From_value(); return v != nullptr ? v->get_uri() : sip_uri_t::empty; }
	sip_uri_t& From_URI() { return make_From_value()->get_uri(); }

	const rtl::String& To_Tag() const { const sip_value_user_t* v = get_To_value(); return v != nullptr ? v->get_tag() : rtl::String::empty; }
	void To_Tag(const rtl::String& tag) { make_To_value()->set_tag(tag); }
	const sip_uri_t& To_URI() const { const sip_value_user_t* v = get_To_value(); return v != nullptr ? v->get_uri() : sip_uri_t::empty; }
	sip_uri_t& To_URI() { return make_To_value()->get_uri(); }

	rtl::String GetStartLine() const {
		rtl::String st;
		if (m_request)
		{
			st << get_request_method_name() << ' ' << m_request_uri.to_string() << ' ' << m_version;
		}
		else
		{
			st << m_version << ' ' << m_response_code << ' ' << m_response_reason;
		}

		return st;
	}
	
	bool IsKeepAlive() const { return IsMethod(sip_KEEP_ALIVE_e); }
	bool IsInvite() const { return IsMethod(sip_INVITE_e); }
	bool IsBye() const { return IsMethod(sip_BYE_e); }
	bool IsAck() const { return IsMethod(sip_ACK_e); }
	bool IsRegister() const { return IsMethod(sip_REGISTER_e); }
	bool IsUnregister() const;
	bool IsCancel() const { return IsMethod(sip_CANCEL_e); }
	bool IsNotify() const { return IsMethod(sip_NOTIFY_e); }
	bool IsRefer() const { return IsMethod(sip_REFER_e); }
	bool IsUpdate() const { return IsMethod(sip_UPDATE_e); }
	bool IsSubscribe() const { return IsMethod(sip_SUBSCRIBE_e); }
	bool IsOptions() const { return IsMethod(sip_OPTIONS_e); }
	bool IsMessage() const { return IsMethod(sip_MESSAGE_e); }
	bool IsInfo() const { return IsMethod(sip_INFO_e); }

	bool IsMethod(sip_method_type_t type) const { return type == m_request_method; }
	bool IsMethod(const char* method) const { return get_request_method_name() *= method; }
	bool IsValid(bool minimal=true) const;

	bool Is1xx() const { return m_response_code >= sip_100_Trying && m_response_code < sip_200_OK; }
	bool Is2xx() const { return m_response_code >= sip_200_OK && m_response_code < sip_300_MultipleChoices; }
	bool Is3xx() const { return m_response_code >= sip_300_MultipleChoices && m_response_code < sip_400_BadRequest; }
	bool Is4xx() const { return m_response_code >= sip_400_BadRequest && m_response_code < sip_500_ServerInternalError; }
	bool Is5xx() const { return m_response_code >= sip_500_ServerInternalError && m_response_code < sip_600_BusyEverywhere; }
	bool Is6xx() const { return m_response_code >= sip_600_BusyEverywhere && m_response_code < sip_MaxCode; }

	trans_id get_trans_id() const { return trans_id(*this); }
	void set_transaction(const rtl::String& tId) { m_trans_id = tId; }
	const rtl::String& get_transaction() const { return m_trans_id; }
	bool has_transaction() const { return m_has_trans; }
	void has_transaction(bool flag) { m_has_trans = flag; }

	void set_retransmission() { m_retransmission = true; }
	bool is_retransmission() const { return m_retransmission; }


	rtl::String GetUACoreName() const { return m_ua_name; }
	void SetUACoreName(const rtl::String& name) { m_ua_name = name; }

	bool make_response(sip_message_t& response, uint16_t statusCode, const rtl::String& reasonPhrase = rtl::String::empty) const;

	///---------------------------------
	/// ������� ������� � ����������� �������������� ����������
	///---------------------------------
#define DEFINE_HEADER_X(type, ret) \
	sip_header_t* get_##type() { return m_std_headers[sip_##type##_e]; } \
	const sip_header_t* get_##type() const { return m_std_headers[sip_##type##_e]; } \
	ret* get_##type##_value(int index = 0) { return (ret*)(m_std_headers[sip_##type##_e] != nullptr ? m_std_headers[sip_##type##_e]->get_value_at(index) : nullptr); } \
	const ret* get_##type##_value(int index = 0) const { return (ret*)(m_std_headers[sip_##type##_e] != nullptr ? m_std_headers[sip_##type##_e]->get_value_at(index) : nullptr); } \
	ret* make_##type##_value() { return (ret*)make_std_value(sip_##type##_e); } \
	sip_header_t* make_##type() { return make_std_header(sip_##type##_e); } \
	bool has_##type() const { return m_std_headers[sip_##type##_e] != nullptr ? m_std_headers[sip_##type##_e]->get_value_count() > 0 : 0; }

#define DEFINE_HEADER(type) DEFINE_HEADER_X(type, sip_value_t)
#define DEFINE_HEADER_URI(type) DEFINE_HEADER_X(type, sip_value_uri_t)
#define DEFINE_HEADER_USER(type) DEFINE_HEADER_X(type, sip_value_user_t)
#define DEFINE_HEADER_AUTH(type) DEFINE_HEADER_X(type, sip_value_auth_t)
#define DEFINE_HEADER_NUM(type) DEFINE_HEADER_X(type, sip_value_number_t)

	DEFINE_HEADER(Accept);													//sip_std_Accept_e,							// [RFC 3261]
	DEFINE_HEADER(Accept_Contact);											//sip_std_Accept_Contact_e,					// [RFC 3841]
	DEFINE_HEADER(Accept_Encoding);											//sip_std_Accept_Encoding_e,				// [RFC 3261]
	DEFINE_HEADER(Accept_Language);											//sip_std_Accept_Language_e,				// [RFC 3261]
	DEFINE_HEADER(Alert_Info);												//sip_std_Alert_Info_e,						// [RFC 3261]
	DEFINE_HEADER(Allow);													//sip_std_Allow_e,							// [RFC 3261]
	DEFINE_HEADER(Allow_Events);											//sip_std_Allow_Events_e,					// [RFC 3265]
	DEFINE_HEADER(Authentication_Info);										//sip_std_Authentication_Info_e,			// [RFC 3261]
	DEFINE_HEADER_AUTH(Authorization);										//sip_std_Authorization_e,					// [RFC 3261]
	DEFINE_HEADER(Call_ID);													//sip_std_Call_ID_e,						// [RFC 3261]
	DEFINE_HEADER(Call_Info);												//sip_std_Call_Info_e,						// [RFC 3261]
	DEFINE_HEADER_X(Contact, sip_value_contact_t);							//sip_std_Contact_e,						// [RFC 3261]
	DEFINE_HEADER(Content_Disposition);										//sip_std_Content_Disposition_e,			// [RFC 3261]
	DEFINE_HEADER(Content_Encoding);										//sip_std_Content_Encoding_e,				// [RFC 3261]
	DEFINE_HEADER(Content_Language);										//sip_std_Content_Language_e,				// [RFC 3261]
	DEFINE_HEADER_NUM(Content_Length);										//sip_std_Content_Length_e,					// [RFC 3261]
	DEFINE_HEADER(Content_Type);											//sip_std_Content_Type_e,					// [RFC 3261]
	DEFINE_HEADER_X(CSeq, sip_value_cseq_t);								//sip_std_CSeq_e,							// [RFC 3261]
	DEFINE_HEADER(Date);													//sip_std_Date_e,							// [RFC 3261]
																			//sip_std_Diversion_e,						// [RFC 6044]
	DEFINE_HEADER(Error_Info);												//sip_std_Error_Info_e,						// [RFC 3261]
	DEFINE_HEADER(Event);													//sip_std_Event_e,							// [RFC 3265]
	DEFINE_HEADER_NUM(Expires);												//sip_std_Expires_e,						// [RFC 3261]
	DEFINE_HEADER_USER(From);												//sip_std_From_e,							// [RFC 3261]
	DEFINE_HEADER(History_Info);											//sip_std_History_Info_e,					// [RFC 6044]
																			//sip_std_Identity_e,						// [RFC 4474]
																			//sip_std_Identity_Info_e,					// [RFC 4474]
	DEFINE_HEADER(In_Reply_To);												//sip_std_In_Reply_To_e,					// [RFC 3261]
																			//sip_std_Info_Package_e,					// [RFC 6086]
																			//sip_std_Join_e,							// [RFC 3911]
	DEFINE_HEADER_NUM(Max_Forwards);										//sip_std_Max_Forwards_e,					// [RFC 3261]
	DEFINE_HEADER_NUM(Min_Expires);											//sip_std_Min_Expires_e,					// [RFC 3261]
	DEFINE_HEADER_NUM(Min_SE)												//sip_std_Min_SE_e,							// [RFC 4028]
	DEFINE_HEADER(MIME_Version);											//sip_std_MIME_Version_e,					// [RFC 3261]
	DEFINE_HEADER(Organization);											//sip_std_Organization_e,					// [RFC 3261]
																			//sip_std_P_Access_Network_Info_e,			// [RFC 3455]
																			//sip_std_P_Asserted_Identity_e,			// [RFC 3325]
																			//sip_std_P_Asserted_Service_e,				// [RFC 6050]
																			//sip_std_P_Associated_URI_e,				// [RFC 3455]
																			//sip_std_P_Called_Party_ID_e,				// [RFC 3455]
																			//sip_std_P_Charging_Function_Addresses_e,	// [RFC 3455]
																			//sip_std_P_Charging_Vector_e,				// [RFC 3455]
																			//sip_std_P_DCS_BILLING_INFO_e,				// [RFC 3603]
																			//sip_std_P_DCS_LAES_e,						// [RFC 3603]
																			//sip_std_P_DCS_OSPS_e,						// [RFC 3603]
																			//sip_std_P_DCS_REDIRECT_e,					// [RFC 3603]
																			//sip_std_P_DCS_Trace_Party_ID_e,			// [RFC 3603]
																			//sip_std_P_Early_Media_e,					// [RFC 5009]
																			//sip_std_P_Preferred_Service_e,			// [RFC 6050]
																			//sip_std_P_Profile_Key_e,					// [RFC 5002]
																			//sip_std_P_Refused_URI_List_e,				// [RFC 5318]
																			//sip_std_P_Served_User_e,					// [RFC 5502]
																			//sip_std_P_Visited_Network_ID_e,			// [RFC 3455]
	DEFINE_HEADER_URI(Path);												//sip_std_Path_e,							// [RFC 3327]
	DEFINE_HEADER(Priority);												//sip_std_Priority_e,						// [RFC 3261]
	DEFINE_HEADER_AUTH(Proxy_Authenticate);									//sip_std_Proxy_Authenticate_e,				// [RFC 3261]
	DEFINE_HEADER_AUTH(Proxy_Authorization);								//sip_std_Proxy_Authorization_e,			// [RFC 3261]
	DEFINE_HEADER(Proxy_Require);											//sip_std_Proxy_Require_e,					// [RFC 3261]
	DEFINE_HEADER(RAck);													//sip_std_RAck_e,							// [RFC 3262]
	DEFINE_HEADER(Reason);													//sip_std_Reason_e,							// [RFC 3326]
	DEFINE_HEADER_URI(Record_Route);										//sip_std_Record_Route_e,					// [RFC 3261]
																			//sip_std_Recv_Info_e,						// [RFC 6086]
	DEFINE_HEADER_URI(Refer_To);											//sip_std_Refer_To_e						// [RFC 3515]
	DEFINE_HEADER_URI(Referred_By);											//sip_std_Refer_To_e						// [RFC 3515]
																			//sip_std_Reject_Contact_e,					// [RFC 3841]
																			//sip_std_Request_Disposition_e,			// [RFC 3841]
	DEFINE_HEADER(Require);													//sip_std_Require_e,						// [RFC 3261]
	DEFINE_HEADER(Replaces);												//sip_std_Replaces_e,						// [RFC 3891]
	DEFINE_HEADER(Reply_To);												//sip_std_Reply_To_e,						// [RFC 3261]
	DEFINE_HEADER(Retry_After);												//sip_std_Retry_After_e,					// [RFC 3261]
	DEFINE_HEADER_NUM(RSeq);												//sip_std_RSeq_e,							// [RFC 3262]
	DEFINE_HEADER_URI(Route);												//sip_std_Route_e,							// [RFC 3261]
																			//sip_std_Security_Client_e,				// [RFC 3329]
																			//sip_std_Security_Server_e,				// [RFC 3329]
																			//sip_std_Security_Verify_e,				// [RFC 3329]
	DEFINE_HEADER(Server);													//sip_std_Server_e,							// [RFC 3261]
	DEFINE_HEADER_URI(Service_Route)										//sip_std_Service_Route_e,					// [RFC 3608]
	DEFINE_HEADER_NUM(Session_Expires)										//sip_std_Session_Expires_e,				// [RFC 4028]
																			//sip_std_SIP_ETag_e,						// [RFC 3903]
																			//sip_std_SIP_If_Match_e,					// [RFC 3903]
	DEFINE_HEADER(Subject);													//sip_std_Subject_e,						// [RFC 3261]
	DEFINE_HEADER(Subscription_State)										//sip_std_Subscription_State_e,				// [RFC 3265]
	DEFINE_HEADER(Supported);												//sip_std_Supported_e,						// [RFC 3261]
																			//sip_std_Suppress_If_Match_e,				// [RFC 5839]
																			//sip_std_Target_Dialog_e,					// [RFC 4538]
	DEFINE_HEADER_NUM(Timestamp);											//sip_std_Timestamp_e,						// [RFC 3261]
	DEFINE_HEADER_USER(To);													//sip_std_To_e,								// [RFC 3261]
	DEFINE_HEADER(Unsupported);												//sip_std_Unsupported_e,					// [RFC 3261]
	DEFINE_HEADER(User_Agent);												//sip_std_User_Agent_e,						// [RFC 3261]
	DEFINE_HEADER_X(Via, sip_value_via_t);									//sip_std_Via_e,							// [RFC 3261]
	DEFINE_HEADER(Warning);													//sip_std_Warning_e,						// [RFC 3261]
	DEFINE_HEADER_AUTH(WWW_Authenticate);									//sip_std_WWW_Authenticate_e,				// [RFC 3261]
};
//--------------------------------------
