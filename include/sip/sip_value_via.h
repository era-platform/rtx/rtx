/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_value.h"
#include "sip_transport_defs.h"
/*
Via: SIP/2.0/UDP proxy.example.com;branch=z9hG4bKkjsh77
Via: SIP/2.0/UDP 10.1.1.1:4540;received=192.0.2.1;rport=9988;branch=z9hG4bKkjshdyff
*/
//--------------------------------------
// Via
//--------------------------------------
class sip_value_via_t : public sip_value_t
{
private:
	sip_transport_type_t m_transport_type;
	rtl::String m_address;
	uint16_t m_port;

	rtl::String m_branch;	// must be
	in_addr m_maddr;		// 0 - not present
	uint32_t m_ttl;			// 0 - not present
	uint32_t m_rport;		// 0 - not present, 1 - present as flag
	in_addr m_received;		// 0 - not present

private:
	virtual rtl::String& prepare(rtl::String& stream) const;
	virtual bool parse();

public:
	sip_value_via_t();
	sip_value_via_t(const sip_value_via_t& v) { assign(v); }
	virtual ~sip_value_via_t();

	virtual void cleanup();
	virtual sip_value_t* clone() const;

	sip_value_via_t& operator = (const sip_value_via_t& v) { return assign(v); }
	sip_value_via_t& operator = (const rtl::String& value) { sip_value_t::assign(value); return *this; }

	sip_value_via_t& assign(const sip_value_via_t& v);
	sip_value_via_t& assign(const rtl::String& address, uint16_t port, sip_transport_type_t type);

	sip_transport_type_t get_protocol() const { return m_transport_type; }
	void set_protocol(sip_transport_type_t type) { m_transport_type = type; }
	const rtl::String& get_address() const { return m_address; }
	void set_address(const rtl::String& address) { m_address = address; }
	uint16_t get_port() const { return m_port; }
	void set_port(uint16_t port) { m_port = port; }
	const rtl::String& get_branch() const { return m_branch; }
	void set_branch(const rtl::String& branch) { m_branch = branch; }
	in_addr get_maddr() const { return m_maddr; }
	bool has_maddr() const { return m_maddr.s_addr != INADDR_ANY && m_maddr.s_addr != INADDR_NONE; }
	void set_maddr(in_addr address) { m_maddr = address; }
	uint16_t get_ttl() const { return m_ttl; }
	bool has_ttl() const { return m_ttl > 0; }
	void set_ttl(uint16_t ttl) { m_ttl = ttl; }
	uint16_t get_rport() const { return uint16_t(m_rport & 0xFFFF0000); }
	bool has_rport() const { return m_rport < 0x10000; }
	void set_rport(uint16_t port) { m_rport = port; }
	void set_rport() { m_rport = 0; }
	void drop_rport() { m_rport = 0xFFFFFFFF; }
	in_addr get_received_address() const { return m_received; }
	bool has_received_address() const { return m_received.s_addr != INADDR_ANY && m_received.s_addr != INADDR_NONE; }
	void set_received_address(in_addr address) { m_received = address; }
};
//--------------------------------------
