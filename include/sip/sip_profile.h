/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_transport_defs.h"
//-----------------------------------------------
//
//-----------------------------------------------
class sip_profile_t
{
public:

	enum auth_e
	{
		auth_basic,
		auth_digest,
		auth_ntlm,
	};

	sip_profile_t();
	sip_profile_t(const sip_profile_t& profile) { operator = (profile); }
	~sip_profile_t() { }

	sip_profile_t& operator = (const sip_profile_t& profile);

	// ����� ���� ������������ ������ (������, ����� ��������� DNS ���)
	const rtl::String& get_proxy() const { return m_proxy; }
	void set_proxy(const rtl::String& proxy) { m_proxy = proxy; } // ????

	// ���������� ip4 �����
	in_addr get_proxy_address() const { return m_proxy_address; }
	void set_proxy_address(in_addr address) { m_proxy_address = address; }

	// ���������� port
	uint16_t get_proxy_port() const { return m_proxy_port == 0 ? 5060 : m_proxy_port; }
	void set_proxy_port(uint16_t port) { m_proxy_port = port; }

	// ��� ����������
	void set_transport(sip_transport_type_t transport) { m_transport = transport; }
	sip_transport_type_t get_transport() const { return m_transport; }

	// ��������� ������������ ��� �������� �������
	in_addr get_interface() const { return m_interface; }
	uint16_t get_interface_port() const { return m_interface_port/* == 0 ? 5060 : m_interface_port*/; }
	void set_interface(in_addr address) { m_interface = address; }
	void set_interface(in_addr address, uint16_t port) { m_interface = address; m_interface_port = port; }
	void set_interface_port(uint16_t port) { m_interface_port = port; }
	// ��������� ������������ ��� ��������� �������
	in_addr get_listener() const { return m_listener.s_addr != INADDR_ANY ? m_listener : m_interface; }
	uint16_t get_listener_port() const { return m_listener.s_addr == INADDR_ANY ? get_interface_port() : m_listener_port == 0 ? 5060 : m_listener_port; }
	void set_listener(in_addr address) { m_listener = address; }
	void set_listener(in_addr address, uint16_t port) { m_listener = address; m_listener_port = port; }
	void set_listener_port(uint16_t port) { m_listener_port = port; }

	// ������������ ���
	const rtl::String& get_display_name() const { return m_display_name; }
	void set_display_name(const rtl::String& displayName) { m_display_name = displayName; }

	// ����� ��� ������������ ���������� ������ URI
	const rtl::String& get_domain() const { return m_domain; }
	void set_domain(const rtl::String& domain) { m_domain = domain; }

	// ��� ������� ������ � ������������
	const rtl::String& get_username() const { return m_username; }
	void set_username(const rtl::String& user) { m_username = user; }

	// ������� ��� ��� ������ � � ��������� ������ ������
	const rtl::String& get_request_user() const { return !m_request_user.isEmpty() ? m_request_user : get_to_user(); }
	void set_request_user(const rtl::String& user) { m_request_user = user; }

	// ��� ���������� �������
	const rtl::String& get_to_user() const { return !m_to_user.isEmpty() ? m_to_user : get_username(); }
	void set_to_user(const rtl::String& user) { m_to_user = user; }

	// ����� ��������������
	auth_e get_auth_type() const { return m_auth_type; }
	void set_auth_type(auth_e type) { m_auth_type = type; }

	// ��� ������������ ��� ��������������
	const rtl::String& get_auth_id() const { return !m_auth_id.isEmpty() ? m_auth_id : get_username(); }
	void set_auth_id(const rtl::String& account) { m_auth_id = account; }

	// ������ ������������ ��� ��������������
	const rtl::String& get_auth_pwd() const { return m_auth_pwd; }
	void set_auth_pwd(const rtl::String& password) { m_auth_pwd = password; }

	// ����� ����� ������?
	int get_expires() { return m_expires; }
	void set_expires(int expire) { m_expires = expire; }

	bool get_bye_auth() const { return m_bye_auth; }
	void set_bye_auth(bool flag) { m_bye_auth = flag; }

	// �������� ������?
	int get_keep_alive_interval() { return m_nat_keep_alive_interval; }
	void set_keep_alive_interval(int value) { m_nat_keep_alive_interval = value; }

protected:
	rtl::String m_domain;					// ���������� ����� ��������
	rtl::String m_proxy;					// ��� �����/ip ����� ��� �������/����������
	in_addr m_proxy_address;			// ����� ��� ������ �������/����������
	uint16_t m_proxy_port;				// ���� ��� ������ �������/����������
	sip_transport_type_t m_transport;	// ��������� ��� ������
	in_addr m_interface;				// ��������� � �������� �������� �������
	uint16_t m_interface_port;			// ���� ����������
	in_addr m_listener;					// ��������� �� ������� ����� ��������� (ANY ���� ��������� � ������������)
	uint16_t m_listener_port;			// ���� ��������� (���� �� ANY)

	rtl::String m_request_user;			// ��� ������ user � RequestURI
	rtl::String m_to_user;					// ��� ������ user � To

	rtl::String m_username;				// username �������
	rtl::String m_display_name;			// ������������ ��� �������

	auth_e m_auth_type;					// ��� �����������
	rtl::String m_auth_id;					// ������������� ��� �����������
	rtl::String m_auth_pwd;				// ������ ��� �����������
	int m_expires;						// ����� ����� ������ �����������
	bool m_bye_auth;					// ���������� �� ����������� BYE

	int m_nat_keep_alive_interval;		// ������ �������� ������� ��� ��������� NAT, 0 - ����������
};
//-----------------------------------------------
