/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_session_manager.h"
//--------------------------------------
//
//--------------------------------------
class sip_reg_session_client_t;
//--------------------------------------
// ������ ����������
//--------------------------------------
struct sip_reg_session_event_handler_t
{
	virtual void sip_reg_session__accepted(sip_reg_session_client_t& session, const sip_message_t& request) = 0;
	virtual void sip_reg_session__rejected(sip_reg_session_client_t& session, const sip_message_t& request) = 0;
	virtual void sip_reg_session__timeout(sip_reg_session_client_t& session) = 0;
	virtual void sip_reg_session__options_request(sip_reg_session_client_t& session, const sip_message_t& request, sip_message_t& response) = 0;
	virtual void sip_reg_session__options_response(sip_reg_session_client_t& session, const sip_message_t& response) = 0;
	virtual void sip_reg_session__destroying(sip_reg_session_client_t& session) = 0;
	virtual void sip_reg_session__transport_failure(sip_reg_session_client_t& session) = 0;
};
//--------------------------------------
//
//--------------------------------------
class sip_reg_session_manager_t : public sip_session_manager_t
{
public:
	sip_reg_session_manager_t(sip_user_agent_t& ua, int sessionThreadCount);
	virtual ~sip_reg_session_manager_t();

	virtual void stop();
	virtual void process_incoming_message(sip_event_message_t& messageEvent, sip_session_t* session);
	virtual sip_session_t* create_new_client_session(const sip_profile_t& profile, const char* callId);
	virtual sip_session_t* create_new_server_session(sip_message_t& request);
};
//--------------------------------------
