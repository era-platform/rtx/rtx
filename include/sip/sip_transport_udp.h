/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_transport.h"
//--------------------------------------
//
//--------------------------------------
class sip_transport_udp_t : public sip_transport_t
{
public:
	sip_transport_udp_t(const sip_transport_point_t& iface, const sip_transport_point_t& endpoint, net_socket_t& socket);
	virtual ~sip_transport_udp_t();

	virtual bool connect();
	virtual void disconnect();
	virtual int send_message(const sip_message_t& message);
	virtual void send_keep_alive();
	virtual bool is_valid();

	void rx_packet(const uint8_t* packet, int packet_length, in_addr remoteAddress, uint16_t remotePort);
	void rx_error(int net_error, in_addr remoteAddress, uint16_t remotePort);

	
};
//--------------------------------------
