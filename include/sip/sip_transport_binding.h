/* RTX H.248 Media Gate SIP engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_transport_listener.h"
//--------------------------------------
//
//--------------------------------------
class sip_transport_binding_t : rtl::async_call_target_t
{
	sip_transport_type_t m_type;
	bool m_listen_all;
	in_addr m_listen_address;
	uint16_t m_listen_port;
	uint32_t m_disable_time;

	rtl::Mutex m_list_sync;
	rtl::ArrayT<sip_transport_listener_t*> m_list;
	rtl::ArrayT<in_addr> m_if_list;

	static volatile int32_t s_id_gen;

	char m_id[32];

public:
	sip_transport_binding_t(sip_transport_type_t type);
	~sip_transport_binding_t();

	bool create(in_addr if_address, uint16_t if_port);
	void destroy();

	bool open();
	void close();

	void check_transport_life_times();
	void update_interface_table(const rtl::ArrayT<in_addr>& iftable);
	
	void event_interface_added(const rtl::ArrayT<in_addr>& list);
	void event_interface_removed(const rtl::ArrayT<in_addr>& list);
	void event_network_fault();

	sip_transport_t* create_transport(const sip_transport_route_t& route);

	const char* get_id() const { return m_id; }
	bool listen_all() { return m_listen_all; }
	sip_transport_type_t getType() { return m_type; }
	in_addr get_listen_address() { return m_listen_address; }
	uint16_t get_listen_port() { return m_listen_port; }

private:
	sip_transport_listener_t* add_listener(in_addr if_address);
	void update_zero_interface(const rtl::ArrayT<in_addr>& iftable);

	virtual const char* async_get_name();
	virtual void async_handle_call(uint16_t call_id, uintptr_t int_param, void* ptr_param);

	void async_remove_listener(sip_transport_listener_t* listener);
	void async_add_listener(uintptr_t listener);
};
//--------------------------------------
