﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_netlib.h"
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
enum dtls_connection_role_t
{
	// client
	dtls_connection_role_active_e,
	// server
	dtls_connection_role_passive_e,
	dtls_connection_role_actpass_e,
	dtls_connection_role_invalid_e = -1,
};
//------------------------------------------------------------------------------
// параметры шифрования
//------------------------------------------------------------------------------
struct rtp_crypto_param_t
{
	int crypto_suite;
	uint8_t key[30];
	bool null_rtp_auth;
	bool null_rtp_cipher;
	bool null_rtcp_cipher;
};
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
RTX_NET_API dtls_connection_role_t dtls_connection_role_parse(const char* val);
//------------------------------------------------------------------------------
RTX_NET_API const char* dtls_connection_role_get_name(dtls_connection_role_t val);
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
