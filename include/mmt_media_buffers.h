﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mmt_payload_set.h"
#include "rtp_packet.h"

namespace media
{

	//--------------------------------------
	// буфер для псм данных (очередь FIFO) с разными длинами блоков чтения/записи
	//--------------------------------------
	class SamplesBuffer
	{
		short* m_buffer;
		int m_size;
		int m_length;
		int m_head;
		int m_tail;

	public:
		SamplesBuffer() : m_buffer(nullptr), m_size(0), m_length(0), m_head(0), m_tail(0) { }
		~SamplesBuffer() { FREE(m_buffer); }

		RTX_MMT_API bool create(int depth);
		RTX_MMT_API void reset();

		int getWritten() { return m_length; }

		RTX_MMT_API int write(const short* samples, int count);
		RTX_MMT_API int read(short* buffer, int sisze);
	};
	//--------------------------------------
	// буфер для любых аудио данных (очередь FIFO) с разными длинами блоков чтения/записи
	// для блочных данных (например GSM - 33/65 байтов) выравнивание на совести пользователя буфера
	//--------------------------------------
	class AudioBuffer
	{
		uint8_t* m_buffer;
		int m_size;
		int m_length;
		int m_head;
		int m_tail;

	public:
		AudioBuffer() : m_buffer(nullptr), m_size(0), m_length(0), m_head(0), m_tail(0) { }
		~AudioBuffer() { FREE(m_buffer); m_buffer = nullptr; }

		RTX_MMT_API bool create(int depth);
		RTX_MMT_API void reset();

		int getWritten() { return m_length; }

		RTX_MMT_API int write(const uint8_t* audioData, int length);
		RTX_MMT_API int read(uint8_t* buffer, int size);
	};
	//--------------------------------------
	// размер буфера 80 семплов
	//--------------------------------------
#define SAMPLES_QUEUE_BSIZE 80
//--------------------------------------
// буфер (очередь) обмена без локов
// размер буферов для чтения и записи должны быть кратными 10 мс или 80 семплов (80/160 байт)
// поэтому блоки имеют размер 80 байт. для псм будут использоватся по 2 блока
//--------------------------------------
	class SamplesQueue
	{
		struct QueueBlock
		{
			bool ready;							// TRUE -- готов для записи, FALSE -- готов для чтения
			short buffer[SAMPLES_QUEUE_BSIZE];	// 1 block 80 samples - 10 ms for 8 kz, 5 ms for 16 kz, 2,5 ms for 32 kz 
		};

		QueueBlock* m_queue;
		int m_queueSize;
		int m_writeIdx;
		int m_readIdx;
		volatile int32_t m_data_count;

		QueueBlock* getWritingBlock() { return !m_queue[m_writeIdx].ready ? m_queue + m_writeIdx : nullptr; }
		void blockWritten();

		QueueBlock* getReadingBlock() { return m_queue[m_readIdx].ready ? m_queue + m_readIdx : nullptr; }
		void blockRead();

	public:
		SamplesQueue() : m_queue(nullptr), m_queueSize(0), m_writeIdx(0), m_readIdx(0), m_data_count(0) { }
		~SamplesQueue() { reset(); }

		RTX_MMT_API void create(int queue_length);
		RTX_MMT_API void reset();

		RTX_MMT_API int write(const short* audioData, int length);
		RTX_MMT_API int read(short* audioData, int size);

		int getAvailable() { return m_data_count * SAMPLES_QUEUE_BSIZE; }
		int getFree() { return (m_queueSize - m_data_count) * SAMPLES_QUEUE_BSIZE; }
	};
	//--------------------------------------
	// буфер (очередь) обмена без локов
	// размер буферов для чтения и записи должны быть кратными 10 мс или 80 семплов (80/160 байт)
	// поэтому блоки имеют размер 80 байт. для псм будут использоватся по 2 блока
	//--------------------------------------
	class AudioQueue
	{
		struct QueueBlock
		{
			bool ready;								// TRUE -- готов для записи, FALSE -- готов для чтения
			uint8_t buffer[SAMPLES_QUEUE_BSIZE];	// bytes!!!
		};

		QueueBlock* m_queue;
		int m_queueSize;
		int m_writeIdx;
		int m_readIdx;
		volatile int32_t m_blockCount;

		QueueBlock* getWritingBlock() { return !m_queue[m_writeIdx].ready ? m_queue + m_writeIdx : nullptr; }
		void blockWritten();

		QueueBlock* getReadingBlock() { return m_queue[m_readIdx].ready ? m_queue + m_readIdx : nullptr; }
		void blockRead();

	public:
		AudioQueue() : m_queue(nullptr), m_queueSize(0), m_writeIdx(0), m_readIdx(0), m_blockCount(0) { }
		~AudioQueue() { reset(); }

		RTX_MMT_API void create(int queue_length);
		RTX_MMT_API void reset();

		RTX_MMT_API int write(const uint8_t* audioData, int length);
		RTX_MMT_API int read(uint8_t* audioData, int size);

		int getAvailable() { return m_blockCount * SAMPLES_QUEUE_BSIZE; }
		int getFree() { return (m_queueSize - m_blockCount) * SAMPLES_QUEUE_BSIZE; }
	};
	//--------------------------------------
	// состояние джиттер буфера
	//--------------------------------------
	enum class JitterState
	{
		Pass,			// передаем сразу в буфер накопления
		Captured,		// пакет записан в джиттер
		Ready,			// в джиттере находятся пакеты готовые к передаче в буфер напопления
		Drop,			// пакет опоздал и его нужно отбросить
	};
	//--------------------------------------
	//
	//--------------------------------------
#define MAX_JITTER_SIZE		16
#define MIN_JITTER_SIZE		4
//--------------------------------------
//
//--------------------------------------
	struct JitterPacket
	{
		int seqNo;
		int count;
		short samples[640]; // 640 - 20-ms 32-kz mono 16-bit, 160 - 20-ms 8-kz mono 16-bit
	};
	//--------------------------------------
	// буфер упорядовачивания ртп пакетов
	// глубина измеряется в семплах!
	//--------------------------------------
	class JitterQueue
	{
		rtl::Logger& m_log;

		bool m_new;										// признак новой очереди

		uint32_t m_lastPacketTime;						// время прихода последнего нормального пакета!
		uint16_t m_numberOut;							// номер последнего ушедшего пакета
		uint16_t m_numberIn;							// номер последнего вошедшего пакета

		int m_depthPackets;								// максимальная глубина джиттера в пакетах,
														// максимальный размер 16 пакетов!
		int m_depthMedia;								// максимальная глубина джиттера в семплах.

		JitterPacket* m_inList[MAX_JITTER_SIZE];		// входящий джиттер буфер

		int m_inPacketSize;							// длина первого пакета
		int m_inMediaDepth;							// длина накопленных медия данных

		int m_outPacketCount;							// количество пакетов в исходящем буфере
		int m_outIndex;								// позиция в исходящем буфере для считывания
		JitterPacket* m_outList[MAX_JITTER_SIZE];	// исходящий буфер

	public:
		RTX_MMT_API JitterQueue(rtl::Logger& log);
		RTX_MMT_API ~JitterQueue();

		RTX_MMT_API void setPacketDepth(int depth);
		int getPacketDepth() { return m_depthPackets; }
		int getMediaDepth() { return m_depthMedia; }

		RTX_MMT_API JitterState push(JitterPacket* packet);

		RTX_MMT_API JitterPacket* getFirst();
		RTX_MMT_API JitterPacket* getNext();

		RTX_MMT_API void reset();

	private:
		void shiftToOuter(int count);
	};
}
//--------------------------------------
