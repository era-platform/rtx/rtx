﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mmt_payload_format.h"

namespace media
{
	//--------------------------------------
	// список доступных кодеков и фильтров -- функции работы с ними
	//--------------------------------------
	class PayloadSet
	{
		rtl::ArrayT<PayloadFormat*> m_set;

		RTX_MMT_API int find(PayloadId payloadId) const;
		RTX_MMT_API int find(const char* name) const;

	public:
		RTX_MMT_API PayloadSet(const PayloadSet& set) { assign(set); }
		RTX_MMT_API PayloadSet() { }
		RTX_MMT_API ~PayloadSet() { removeAll(); }

		// <public members>	
		PayloadSet& operator = (const PayloadSet& set) { return assign(set); }
		RTX_MMT_API PayloadSet& assign(const PayloadSet& set);

		const PayloadFormat& getAt(int index) const { return (index >= 0 && index < m_set.getCount()) ? *m_set[index] : PayloadFormat::Empty; }
		const PayloadFormat& getTopFormat() const { return getAt(0); }
		const PayloadFormat& getFormat(const char* name) const { return getAt(find(name)); }
		const PayloadFormat& getFormat(PayloadId payloadId) const { return getAt(find(payloadId)); }
		PayloadFormat& getFormat(const char* name) { return getAt(find(name)); }
		PayloadFormat& getFormat(PayloadId payloadId) { return getAt(find(payloadId)); }
		PayloadFormat& getAt(int index) { return (index >= 0 && index < m_set.getCount()) ? *(m_set[index]) : PayloadFormat::Empty; }
		int getCount() const { return m_set.getCount(); }
		bool hasFormat(const char* name) const { return find(name) != BAD_INDEX; }
		bool hasFormat(PayloadId payloadId) const { return find(payloadId) != BAD_INDEX; }
		void bringToTop(PayloadId payloadId) { setPreferredFormat(find(payloadId));	}
		void bringToTop(const char* encoding) { setPreferredFormat(find(encoding)); }

		RTX_MMT_API void setPreferredFormat(int preferredIndex);

		RTX_MMT_API int getDtmfCount() const;
		RTX_MMT_API int getMediaCount() const;

		RTX_MMT_API bool addFormat(const char* name, PayloadId payloadId, int clockRate, int channels, const char* fmtp = nullptr, bool top = false);
		RTX_MMT_API bool addFormat(const char* name, PayloadId payloadId, int clockRate, const char* params, const char* fmtp = nullptr, bool top = false);
		RTX_MMT_API bool addFormat(const PayloadFormat& format, bool top = false);
		RTX_MMT_API bool addStandardFormat(PayloadId payloadId);

		RTX_MMT_API void removeAt(int index);
		RTX_MMT_API void removeFormat(PayloadId payloadId) { removeAt(find(payloadId)); }
		RTX_MMT_API void removeFormat(const char* encoding) { removeAt(find(encoding)); }
		RTX_MMT_API void removeAll();

		RTX_MMT_API static int intersect(const PayloadSet& left, const PayloadSet& right, PayloadSet& result);
		RTX_MMT_API static const PayloadFormat& getStandardFormat(PayloadId payloadId);
		RTX_MMT_API static const PayloadFormat& getStandardFormat(const char* encoding);
		RTX_MMT_API static bool isStandardId(PayloadId payloadId);
		RTX_MMT_API static bool isStandardId(uint8_t payloadId);
	};
}
//--------------------------------------
