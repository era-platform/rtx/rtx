﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mmt_resampler.h"

namespace media
{
	//--------------------------------------
	// выход всегда моно! частота 8000/16000/32000
	//--------------------------------------
	class AudioReader
	{
	protected:
		rtl::String m_filePath;
		bool m_eof;
		bool m_readingStarted;
		uint64_t m_GTTStartTime;
		rtl::Logger* m_log;

		// выходные данные: всегда L16, частота
		int m_outputFrequency; // 8000 12000 16000 24000 32000 48000 // 22000 44000
		//int m_out_channels; всегда моно!

		// инфо источника: кодировка, частота, кол-во каналов
		int m_sourceFrequency;
		int m_sourceChannels;
		rtl::String m_sourceEncoding;	// "MP3", "L16", "G711", "GSM", "rtp-current-payload-format"

		// Ресамплер: всегда вызывается в цепочке первым (стерео делится по каналам а потом собирается обратно!)
		Resampler* m_resampler;
		short* m_resamplerCache;
		int m_resamplerCachePos;
		int m_resamplerCacheLength;
		// 
		short* m_resamplerOutput;
		int m_resamplerOutputSize;
		int m_resamplerOutputPos;

		// преобразователь моно/стерео
		bool m_cvtMono;		// source is stereo but target is mono
		short* m_cvtPCM;		// буфер для преобразования 
		int m_cvtPCMRead;		// количество считанных семплов
		int m_cvtPCMPos;		// текущее положение в буфере

		int m_statSamples;
		int m_statPackets;

	public:
		RTX_MMT_API virtual ~AudioReader();

		int getOutputFrequency() const { return m_outputFrequency; }

		int getSourceChannelCount() const { return m_sourceChannels; }
		int getSourceFrequency() const { return m_sourceFrequency; }
		const rtl::String getSourceEncoding() const { return m_sourceEncoding; }
		const rtl::String& getFilePath() { return m_filePath; }
		uint64_t getStartTime() { return m_GTTStartTime; }
		bool eof() { return m_eof; }

		int statSamplesRead() { return m_statSamples; }
		int statPacketsRead() { return m_statPackets; }

		/// открытие файла,
		RTX_MMT_API bool open(const char* filePath);
		/// закрытие читателя
		RTX_MMT_API void close();
		/// вызывается после открытия файла и до первого чтения
		RTX_MMT_API bool setupReader(int outFrequency);
		/// читает конвертированные данные -- если setup_reader() не вызывался то на выходе будут оригинальный L16
		RTX_MMT_API int readNext(short* buffer, int size);
		/// создание читателя
		RTX_MMT_API static AudioReader* create(const char* path, rtl::Logger* log);
		RTX_MMT_API static AudioReader* create(const char* path, int frequency, rtl::Logger* log);
		RTX_MMT_API static bool setRTPReaderType(bool linear);

	protected:
		AudioReader(rtl::Logger* log);

		/// interface for derived objects
		virtual bool openSource(const char* path) = 0;
		virtual void closeSource() = 0;
		virtual int readRawPCM(short* buffer, int size) = 0;

	private:
		void resetReader();
		int readResampled(short* buffer, int size);
		bool fillResamplerCache();
		int readConverted(short* buffer, int size);
	};
}
//--------------------------------------
