﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "std_array_templates.h"
#include "std_timer.h"
#include "std_typedef.h"
#include "std_logger.h"
#include "rtx_media_tools.h"

namespace media
{
#define PLAY_FREQUENCY 20

	//--------------------------------------
	// пользователь мультимедийного таймера
	//--------------------------------------
	class Metronome
	{
		friend class MetronomeDistributor;

		uintptr_t m_timerId;
		Metronome* m_mt_nextLink;
		volatile bool m_timerPaused;
		volatile bool m_timerStopping;
		volatile bool m_timerStopped;
		rtl::Logger& m_timerLog;

		void timerElapsed();

	public:
		RTX_MMT_API	Metronome(rtl::Logger& log);
		virtual RTX_MMT_API ~Metronome();

		// время срабатывания таймера в миллисекундах.
		RTX_MMT_API	bool metronomeStart(uint32_t ticks_period, bool individual);
		RTX_MMT_API	void metronomeStop();
		RTX_MMT_API	void metronomePause();
		RTX_MMT_API	void metronomeResume();

		bool isMetronomeStarted() { return m_timerId != 0; }
		bool isMetronomePaused() { return m_timerPaused; }
		bool isMetronomeStopping() { return m_timerStopping; }

		//--------------------------------------
		// менеджер мультимедия таймеров
		//--------------------------------------
		static RTX_MMT_API void initializeMetronomeManager();
		static RTX_MMT_API void destroyMetronomeManager();

	protected:
		virtual void metronomeTicks() = 0;
	};
}
//--------------------------------------
