﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "std_mutex.h"

#if defined(TARGET_OS_WINDOWS)
#include <eh.h>
//--------------------------------------
//
//--------------------------------------
#define EX_BASE		0
#define EX_SEH		1
#endif

namespace rtl
{
	//--------------------------------------
	//
	//--------------------------------------
	typedef void(*rtx_exception_callback_t)(const char* module, const char* catcher, void* user_data, const char* text);
	//--------------------------------------
	//
	//--------------------------------------
	class Exception
	{
	protected:
		uint32_t m_code;
		char* m_text;
		Exception* m_inner;

		static rtx_exception_callback_t g_callback;

	public:
		RTX_STD_API Exception(const char* text, uint32_t code = 0, Exception* inner = nullptr);
		RTX_STD_API virtual ~Exception();

		const char* get_message() { return m_text; }
		uint32_t get_code() { return m_code; }
		Exception* get_inner() { return m_inner; }
		RTX_STD_API void raise_notify(const char* module, const char* catcher, void* user_data = nullptr);

		RTX_STD_API static void	set_callback(rtx_exception_callback_t callback);
		RTX_STD_API static void	kill();
	};
	//--------------------------------------
	//
	//--------------------------------------
#if defined(TARGET_OS_WINDOWS)
	struct exception_thread_info_t
	{
		uint32_t thread_id;
		_se_translator_function pfn_old_function;
		uint32_t ref;
	};
	//--------------------------------------
	//
	//--------------------------------------
	class SEH_Exception : public Exception
	{
		static Mutex s_lock;
		void construct(EXCEPTION_POINTERS* pointer);
	public:
		RTX_STD_API SEH_Exception(EXCEPTION_POINTERS* pointer);

		RTX_STD_API static void	start_handling();
		RTX_STD_API static void	stop_handling();
		RTX_STD_API static void	translate(uint32_t ec, EXCEPTION_POINTERS* seh);
	};
	//--------------------------------------
	//
	//--------------------------------------
	class pure_call_exception_t : public Exception
	{
		static _purecall_handler s_old_handler;
	public:
		RTX_STD_API pure_call_exception_t();
		RTX_STD_API ~pure_call_exception_t();

		RTX_STD_API static void	start_handling();
		RTX_STD_API static void	stop_handling();
		RTX_STD_API static void	translate();
	};
}
#define EXCEPTION_ENTER rtl::SEH_Exception::start_handling()
#define EXCEPTION_LEAVE rtl::SEH_Exception::stop_handling()

#else

	void intialize_signal_handlers();
}
#define EXCEPTION_ENTER rtl::intialize_signal_handlers()
#define EXCEPTION_LEAVE
#endif


//--------------------------------------
