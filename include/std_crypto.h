﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "std_sha1.h"
//--------------------------------------
//
//--------------------------------------
RTX_STD_API int Base64_CalcDecodedSize(int encoded);
RTX_STD_API int Base64_CalcEncodedSize(int original);
RTX_STD_API int Base64Encode(const uint8_t* input, int in_len, char* output, int out_size, int lf = -1);
RTX_STD_API int Base64Decode(const char* input, int in_len, uint8_t* out, int out_size);
//--------------------------------------
//
//--------------------------------------
#define MD5_HASH_SIZE 16
#define MD5_HASH_HEX_SIZE 32
//--------------------------------------
// MD5 context.
//--------------------------------------
class md5_context_t
{
	uint32_t m_state[4];		// state (ABCD)
	uint32_t m_count[2];		// number of bits, modulo 2^64 (lsb first)
	uint8_t m_buffer[64];		// input buffer

public:
	RTX_STD_API md5_context_t();

	RTX_STD_API void init();
	RTX_STD_API void update(const void* data, int length);
	RTX_STD_API void final(uint8_t digest[MD5_HASH_SIZE]);
	RTX_STD_API void final(char hash[MD5_HASH_HEX_SIZE]);

	RTX_STD_API uint32_t* state() const;
};
//--------------------------------------
// calculate H(A1) as per HTTP Digest spec
//--------------------------------------
RTX_STD_API void DigestCalcHA1(const char* alg, const char* userName, const char* realm, const char* pswd, char* hash1);

RTX_STD_API void DigestCalcHA1_Sess(const char* alg, const char* userName, const char* realm, const char* pswd, const char* nonce, const char* cnonce, char* hash1);
RTX_STD_API void DigestCalcHA2(const char* method, const char* uri, char* hash2);

RTX_STD_API void DigestCalcHA2_Int(const char* method, const char* uri, const char* qop, const char* entiny, char* hash2);
//--------------------------------------
// calculate request-digest/response-digest as per HTTP Digest spec
//--------------------------------------
RTX_STD_API void DigestCalcResponse(const char* HA1, const char* nonce, const char* HA2, char* response);
RTX_STD_API void DigestCalcResponse_Qop(const char* HA1, const char* nonce, const char* nonceCount, const char* cnonce, const char* qop, const char* HA2, char* response);
//--------------------------------------
//
//--------------------------------------
RTX_STD_API bool crypto_get_random(uint8_t *buffer, uint32_t length);
//--------------------------------------
//
//--------------------------------------
#define TSK_PPPINITFCS32  0xffffffff   /* Initial FCS value */
#define TSK_PPPGOODFCS32  0xdebb20e3   /* Good final FCS value */
RTX_STD_API uint32_t calculate_crc32(uint32_t fcs, const uint8_t* cp, int len);
//--------------------------------------
