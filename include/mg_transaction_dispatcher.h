﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_transaction_manager.h"

namespace h248
{
	//--------------------------------------
	//
	//--------------------------------------
	struct IRequestSender
	{
		virtual bool RequestSender_responseReceived(TransactionID tid, const Field* response) = 0;
		virtual void RequestSender_errorReceived(TransactionID tid, const Field* error) = 0;
		virtual void RequestSender_responseTimeout(TransactionID tid) = 0;
	};
	//--------------------------------------
	//
	//--------------------------------------
	struct IRequestReceiver
	{
		virtual void IRequestReceiver_requestReceived(TransactionID tid, const Field* request) = 0; // , Field* response
		virtual void IRequestReceiver_transportFailure(int net_error, const Route& route) = 0;
		virtual void IRequestReceiver_transportDisconnected(const Route& route) = 0;
	};
	//--------------------------------------
	//
	//--------------------------------------
	class TransactionDispatcher : ITransactionUser
	{
		uint32_t m_id;
		Route m_route;
		uint32_t m_mgcVersion; // 1, 2, 3
		IRequestReceiver* m_requestReceiver;

		// transaction managment variables
		rtl::MutexWatch m_tranSync;
		volatile uint32_t m_tranIdGen;
		// список запросов и инициаторов на которые ожидаются ответы
		struct RequestSender
		{
			TransactionID tid;
			IRequestSender* sender;
		};
		rtl::SortedArrayT<RequestSender, TransactionID> m_requestList;

	public:
		RTX_MG_API TransactionDispatcher();
		RTX_MG_API ~TransactionDispatcher();

		RTX_MG_API void initialize(uint32_t id, IRequestReceiver* receiver, int mgcVersion);
		RTX_MG_API void destroy();

		RTX_MG_API void stop();

		RTX_MG_API void setMgcVersion(int version) { m_mgcVersion = version; }
		RTX_MG_API void setControlRoute(const Route newRoute) { m_route = newRoute; }

		// 3. обработка сообщений из сети
		RTX_MG_API void handle_Disconnected(const Route& route);
		RTX_MG_API void handle_PacketArrival(const Message* message, const Route& route);
		RTX_MG_API void handle_NetFailureEvent(int netError, const Route& route);
		RTX_MG_API void handle_ParserErrorEvent(const Route& route);

		// 4. отправка запросов в сеть 
		RTX_MG_API bool sendRequest(Field* request, IRequestSender* handler, TransactionID tid);
		RTX_MG_API void sendReply(Field* response, TransactionID tid);
		RTX_MG_API void sendError(TransactionID tid, ErrorCode errorCode, const char* reason);

		RTX_MG_API void generateTranId(TransactionID& tid);

	private:
		uint32_t generateTranId();

		// network handlers
		void handle_Error(const Field* error, const Route& route);
		void handle_Request(const Field* request, const Route& route, int version);
		void handle_Reply(const Field* reply, const Route& route);
		void handle_Ack(const Field* ack, const Route& route);

		// transaction_user_interface

		// client transaction events
		virtual bool TransactionUser_responseReceived(Transaction* sender, const Field* response);
		virtual bool TransactionUser_pendingReceived(Transaction* sender);
		virtual void TransactionUser_requestTimeout(Transaction* sender);

		// common transaction events
		virtual void TransactionUser_errorReceived(Transaction* sender, const Field* error);
		virtual void TransactionUser_terminated(Transaction* sender);

		// server transaction events
		virtual void TransactionUser_requestReceived(Transaction* sender, const Field* request); // , Field* reply

		// binding requests and handlers
		void addSender(RequestSender& handler);
		bool findSender(TransactionID tid, RequestSender& handler);
		void releaseSender(TransactionID tid);

		static int compareRequestByObj(const RequestSender& left, const RequestSender& right);
		static int compareRequestByKey(const TransactionID& left, const RequestSender& right);
	};
}
//--------------------------------------
