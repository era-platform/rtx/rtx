﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#pragma pack(push, 1)
struct guid_t
{
	unsigned int   _l_data1;
	unsigned short _s_data2;
	unsigned short _s_data3;
	unsigned char  _ch_data4[8];
};
#pragma pack(pop)
//------------------------------------------------------------------------------
// API для получения информации об окружении (ОС, архитектура, системные переменные, и т.п.)
//------------------------------------------------------------------------------
RTX_STD_API uint32_t std_get_processors_count();
RTX_STD_API bool std_get_application_startup_path(char* buffer, int size);
RTX_STD_API guid_t std_generate_guid();
RTX_STD_API bool std_parse_guid(guid_t& guid, const char* guid_str, int length = -1);
RTX_STD_API const char* std_guid_to_string(const guid_t& guid, char* buffer, int size);
RTX_STD_API bool std_is_guid_empty(const guid_t& guid);
RTX_STD_API bool std_is_guid_equal(const guid_t& l_guid, const guid_t& r_guid);
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
