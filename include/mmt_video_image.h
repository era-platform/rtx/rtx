﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_media_tools.h"

#include "mmt_video_frame.h"

namespace media
{
	class VideoFrameImage : public VideoFrameElement
	{
		Bitmap m_picture;

	public:
		VideoFrameImage(VideoFrameGenerator& frame, const rtl::String& id) : VideoFrameElement(frame, VideoElementType::Bitmap, id) { }
		RTX_MMT_API virtual ~VideoFrameImage();

		RTX_MMT_API virtual void assign(const VideoRawElement& data);
		RTX_MMT_API virtual void drawOn(ImageDrawer& draw) override;
	};
}
//-----------------------------------------------
