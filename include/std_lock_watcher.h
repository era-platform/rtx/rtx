﻿/* RTX H.248 Media Gate Base library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

namespace rtl
{

#define CONF_WATCHER_ENABLED "mutex-watcher-enabled"
#define CONF_WATCHER_PERIOD "mutex-watcher-period"
	//--------------------------------------
	// интерфейс часового Родины (сержант Вася Батарейкин)
	//--------------------------------------
	// инициализация/останов таймера просмоторщика дедлоков (в секундах)
	//--------------------------------------
	RTX_STD_API bool Watch_Start(int period);
	RTX_STD_API void Watch_Stop();
	RTX_STD_API void Watch_Destroy();
	//--------------------------------------
	// регистрация/разрегистрация блокировок
	//--------------------------------------
	RTX_STD_API uintptr_t Watch_RegisterLock(const char* lockName);
	RTX_STD_API int Watch_UnregisterLock(uintptr_t lock);
	RTX_STD_API void Watch_SetLockName(uintptr_t lockId, const char* lockName);
	RTX_STD_API const char*	Watch_GetLockName(uintptr_t lockId);
	//--------------------------------------
	// запись о состоянии блокировок в таблицу
	//--------------------------------------
	RTX_STD_API void Watch_Locking(uintptr_t lock, const char* funcName);
	RTX_STD_API void Watch_Locked(uintptr_t lock, const char* funcName);
	RTX_STD_API void Watch_Unlock(uintptr_t lock);
	RTX_STD_API void Watch_Cancel(uintptr_t lock, const char* funcName);
}
//--------------------------------------
