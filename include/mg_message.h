﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_parser.h"
#include "mg_auth.h"
#include "mg_mid.h"
#include "mg_message_node.h"

namespace h248
{
	//--------------------------------------
	//
	//--------------------------------------
	class Message
	{
		Authentication* m_ah;
		int m_version;
		MID m_mid;
		//message_tree_t m_nodes;
		rtl::ArrayT<Field*> m_fieldList;

	public:
		RTX_MG_API Message();
		RTX_MG_API ~Message();
		RTX_MG_API void cleanup();

		// if it received from address
		const Authentication* getAuthentication() const { return m_ah; }

		int getVersion() const { return m_version; }
		const MID& getMID() const { return m_mid; }

		void setVersion(int version) { m_version = version; }
		void setMID(in_addr source_addr, uint16_t source_port = 0) { m_mid.setAddressIPv4(source_addr, source_port); }
		void setMID(in6_addr source_addr, uint16_t source_port = 0) { m_mid.setAddressIPv6(source_addr, source_port); }
		void setMID(const char* source_domain, uint16_t source_port = 0) { m_mid.setDomain(source_domain, source_port); }
		bool setMID(const char* mid) { return m_mid.parse(mid); }
		bool setMID(const MID& mid) { m_mid = mid; return true; }

		// path: node names separated by backslash '\\' symbol
		const rtl::ArrayT<Field*>& getFieldList(const char* path = nullptr) const { return m_fieldList; }
		rtl::ArrayT<Field*>& getFiledList(const char* path = nullptr) { return m_fieldList; }

		int getTopFieldCount() const { return m_fieldList.getCount(); }
		const Field* getTopFieldAt(int index) const { return m_fieldList.getAt(index); }
		Field* getTopFieldAt(int index) { return m_fieldList.getAt(index); }

		RTX_MG_API const Field* findField(const char* path) const;
		RTX_MG_API Field* findField(const char* path);

		RTX_MG_API Field* addField(const char* path, const Field* node);
		RTX_MG_API Field* addField(const char* path, Field* node, bool attach);

		RTX_MG_API Field* createField(const char* path, const char* name, const char* value = nullptr);
		RTX_MG_API Field* createField(const char* path, Token type, const char* value = nullptr);
		RTX_MG_API Field* createErrorDescriptor(const char* path, int error_code, const char* reason);

		RTX_MG_API void removeField(const char* path);

		RTX_MG_API bool read(const char* packet);
		RTX_MG_API rtl::MemoryStream& writeTo(rtl::MemoryStream& stream, MessageWriterParams* params);

	private:
		bool setStartLine(const char* token, const char* value);
		rtl::MemoryStream& writeStartLine(rtl::MemoryStream& stream, MessageWriterParams* params);
		const Field* getField(const char* name, int length = -1) const;
		Field* getField(const char* name, int length = -1);
		const Field* getField(Token type) const;
		Field* getField(Token type);

		void removeTopField(const char* name);
		void removeTopField(Token type);
	};
}
//--------------------------------------

