﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <lame/lame.h>

namespace media
{
	//--------------------------------------
	// синхронный режим
	//--------------------------------------
	class FileReaderMP3
	{
		static const int LEFT = 0;
		static const int RIGHT = 0;
		static const int PCM_BUFFER_SIZE = 64 * 1024;
		static const int MP3_FRAME_SIZE = 1152;

		hip_t m_hip;					// кодек
		rtl::FileStream m_file;			// файл для чтения
		bool m_eof;
		mp3data_struct m_mp3Data;		// настройки кодека

		int m_pcmSkipStart;
		int m_pcmSkipEnd;
		int m_pcmSampleCount;

		int m_pcmBufferSize;
		int m_pcmBufferPos;
		short* m_pcmBufferLeft;
		short* m_pcmBufferRight;

	public:
		RTX_MMT_API FileReaderMP3();
		~FileReaderMP3() { close(); }

		// исходящий формат PCM
		int getFrequency() const { return m_mp3Data.samplerate; }
		int getChannelCount() const { return m_mp3Data.stereo; }
		int getSampleCount() const { return m_mp3Data.nsamp; }

		bool eof() const { return m_eof; }

		RTX_MMT_API bool open(const char* path);
		RTX_MMT_API void close();

		RTX_MMT_API int readNext(short* mono_buffer, int size);
		RTX_MMT_API int readNext(short* pcm_l, short* pcm_r, int size);

	private:
		bool openFile(int *enc_delay, int *enc_padding);
		bool readHeaders(int* enc_delay, int* enc_padding);
		void setSkipStartAndEnd(int enc_delay, int enc_padding);
		bool fillBufferPCM();
		int readFrame(short* pcm_l, short* pcm_r);
		int readDataPCM(short* pcm_l, short* pcm_r, int size);
	};
	//--------------------------------------
	//
	//--------------------------------------
	class FileWriterMP3
	{
		lame_global_flags* m_lame;		// кодек
		rtl::FileStream m_file;			// файл для записи

		// максимальное количество сохраняемых семплов равно 2 фреймам mp3
		short* m_pcmLeftStorage;		// промежуточное хранилище семплов
		short* m_pcmRightStorage;		// промежуточное хранилище семплов
		int m_pcmStorageSize;			// размер хранилища
		int m_pcmStored;				// количество сохраненых семплов
		int m_pcmFrameLength;
		int m_mp3FrameSize;
		uint8_t* m_mp3Frame;

		bool m_isStereo;
		int m_samplesEncoded;			// общее количество семплов записанных в файл
		int m_mp3FrameCount;
		int m_mp3BytesWritten;

	public:
		RTX_MMT_API FileWriterMP3();
		~FileWriterMP3() { close(); }

		bool isRecording() { return m_file.isOpened(); }

		RTX_MMT_API bool create(const char* filename, int frequency, int channelCount, int bitRateFlag);
		RTX_MMT_API void close();

		RTX_MMT_API bool flush();
		RTX_MMT_API int write(const short* leftChannel, const short* rightChannel, int samplesPerChannel);
		RTX_MMT_API int write(const short* monoChannel, int samplesCount);

	private:
		void initializeEventHandlers();
		bool addSamplesToStorage(const short* left_channel, const short* right_channel, int samples_per_channel);
		void resetStorage(int samples_written);
	};
}
//--------------------------------------
