/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

namespace media
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	typedef rtl::ArrayT<rtp_packet*> VideoJitterBuffer;
	//-----------------------------------------------
	//
	//-----------------------------------------------
	enum class VideoJitterState
	{
		Empty,	// ������ �����
		Wait,	// �������� �����
		Ready,	// ������ ����� � �������� ��������
	};
	//-----------------------------------------------
	//
	//-----------------------------------------------
	class VideoJitterFrame
	{
		VideoJitterState m_state;
		VideoJitterBuffer m_buffer;

		uint32_t m_rts;					// first packet receiving system timestamp
		uint32_t m_ts;					// first packet video clock
		uint16_t m_seq;					// first packet sequence number

	public:
		VideoJitterFrame() : m_state(VideoJitterState::Empty), m_rts(0), m_ts(0), m_seq(0) { }
		~VideoJitterFrame() { clear(); }

		VideoJitterState getState() { return m_state; }
		VideoJitterBuffer* getFrame() { return m_state == VideoJitterState::Ready ? &m_buffer : nullptr; }
		uint32_t getTimestamp() { return m_ts; }

		RTX_MMT_API bool push(const rtp_packet* packet);
		RTX_MMT_API void clear();

	private:
		bool addPacket(const rtp_packet* packet);
	};
	//-----------------------------------------------
	//
	//-----------------------------------------------
	class VideoJitter
	{
		rtl::Logger* m_log;
		VideoJitterFrame m_frame1;
		VideoJitterFrame m_frame2;

		VideoJitterFrame* m_current;
		VideoJitterFrame* m_next;

	public:
		RTX_MMT_API VideoJitter(rtl::Logger* log);
		RTX_MMT_API ~VideoJitter();

		RTX_MMT_API void pushPacket(const rtp_packet* packet);
		RTX_MMT_API bool hasReadyFrame();
		RTX_MMT_API bool getReadyFrame(VideoJitterBuffer& frame);
		RTX_MMT_API void releaseFrame();

	private:
		bool pushToCurrent(const rtp_packet* packet);
		void pushToNext(const rtp_packet* packet);
		void dropCurrent();
	};
}
//-----------------------------------------------
