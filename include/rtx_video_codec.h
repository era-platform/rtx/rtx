/* RTX H.248 Media Gate. Codec Interface Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_codec.h"
#include "mmt_yuv_image.h"
//-----------------------------------------------
//
//-----------------------------------------------
typedef void(*rtx_video_encoder_cb_t)(const void* rtp_payload, int payload_length, uint32_t ts, bool marker, void* user);
//-----------------------------------------------
//
//-----------------------------------------------
struct mg_video_encoder_t
{
	virtual const char*	getEncoding() = 0;
	//virtual bool init(rtx_video_encoder_cb_t cb, void* user) = 0;
	virtual void reset() = 0;
	virtual bool encode(const media::VideoImage* picture) = 0;
};
//-----------------------------------------------
//
//-----------------------------------------------
struct mg_video_decoder_t
{
	virtual const char*	getEncoding() = 0;
	virtual media::VideoImage* decode(const rtp_packet* rtp_pack) = 0;
};
//-----------------------------------------------
//
//-----------------------------------------------
RTX_CODEC_API mg_video_decoder_t* rtx_codec__create_video_decoder(const char* encoding, const media::PayloadFormat* format);
RTX_CODEC_API void rtx_codec__release_video_decoder(mg_video_decoder_t* decoder);
//-----------------------------------------------
//
//-----------------------------------------------
RTX_CODEC_API mg_video_encoder_t* rtx_codec__create_video_encoder(const char* encoding, const media::PayloadFormat* format,
	rtx_video_encoder_cb_t cb, void* usr);
RTX_CODEC_API void rtx_codec__release_video_encoder(mg_video_encoder_t* encoder);
//-----------------------------------------------
