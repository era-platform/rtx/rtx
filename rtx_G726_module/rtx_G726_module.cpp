/* RTX H.248 Media Gate. G.726 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtx_G726_module.h"
#include "G726_codec.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#if defined (DEBUG_MEMORY)
void* operator new (size_t size, char* file, int line)
{
	return std_mem_checker_malloc(size, file, line, rc_malloc_new_e);
}
void* operator new[](size_t size, char* file, int line)
{
	return std_mem_checker_malloc(size, file, line, rc_malloc_newar_e);
}
void operator delete (void* mem_ptr)
{
	std_mem_checker_free(mem_ptr, rc_malloc_delete_e);
}
void operator delete[](void* mem_ptr)
{
	std_mem_checker_free(mem_ptr, rc_malloc_deletear_e);
}
#endif
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_G726_MODULE_API const char* get_codec_list()
{
	return "G726-16 G726-24 G726-32 G726-40";
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_G726_MODULE_API bool get_available_audio_payloads(media::PayloadSet& set)
{
	set.addFormat("G726-16", media::PayloadId::Dynamic, 8000, 1);
	set.addFormat("G726-24", media::PayloadId::Dynamic, 8000, 1);
	set.addFormat("G726-32", media::PayloadId::Dynamic, 8000, 1);
	set.addFormat("G726-40", media::PayloadId::Dynamic, 8000, 1);

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_G726_MODULE_API bool initlib()
{
	g_rtx_g726_decoder_counter = rtl::res_counter_t::add("mge_g726_decoder");
	g_rtx_g726_encoder_counter = rtl::res_counter_t::add("mge_g726_encoder");

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_G726_MODULE_API void freelib()
{
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_G726_MODULE_API mg_audio_decoder_t* create_audio_decoder(const media::PayloadFormat& format)
{
	rtl::String codec_name(format.getEncoding());
	if (codec_name.isEmpty())
	{
		return nullptr;
	}

	if (std_strnicmp("g726-", codec_name, 5) == 0)
	{
		g726_decoder_t* decoder = NEW g726_decoder_t();
		if (decoder->initialize(format))
		{
			return decoder;
		}

		DELETEO(decoder);
	}

	return nullptr;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_G726_MODULE_API void release_audio_decoder(mg_audio_decoder_t* decoder)
{
	if (decoder == nullptr)
	{
		return;
	}

	const char* type = decoder->getEncoding();

	if (std_strnicmp("g726-", type, 5) == 0)
	{
		DELETEO(decoder);
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_G726_MODULE_API mg_audio_encoder_t* create_audio_encoder(const media::PayloadFormat& format)
{
	/*if (param == nullptr)
	{
	return nullptr;
	}*/

	rtl::String codec_name(format.getEncoding());
	if (codec_name.isEmpty())
	{
		return nullptr;
	}

	if (std_strnicmp("g726-", codec_name, 5) == 0)
	{
		g726_encoder_t* encoder = NEW g726_encoder_t();
		if (encoder->initialize(format))
		{
			return encoder;
		}

		DELETEO(encoder);
	}

	return nullptr;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_G726_MODULE_API void release_audio_encoder(mg_audio_encoder_t* encoder)
{
	if (encoder == nullptr)
	{
		return;
	}

	const char* type = encoder->getEncoding();

	if (std_strnicmp("g726-", type, 5) == 0)
	{
		DELETEO(encoder);
	}
}
//------------------------------------------------------------------------------

