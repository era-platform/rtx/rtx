/* RTX H.248 Media Gate. G.726 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "G726_codec.h"

extern int g_rtx_g726_decoder_counter = -1;
extern int g_rtx_g726_encoder_counter = -1;

g726_encoder_t::g726_encoder_t() : m_ctx(nullptr), m_frame_size(0), m_encoding(nullptr)
{
}

g726_encoder_t::~g726_encoder_t()
{
	destroy();
}

bool g726_encoder_t::initialize(const media::PayloadFormat& format)
{
	const char* encoding = format.getEncoding();
	int bit_rate = atoi(encoding+5);

	m_frame_size = 20 * 8000 / 1000;
	
	m_payload_id = format.getId_u8();

	switch (bit_rate)
	{
	case 16:	m_encoding = "g726-16"; break;
	case 24:	m_encoding = "g726-24"; break;
	case 32:	m_encoding = "g726-32"; break;
	case 40:	m_encoding = "g726-40"; break;
		break;
	default:
		return false;
	}


	m_ctx = g726_init(nullptr, bit_rate * 1000, G726_ENCODING_LINEAR, G726_PACKING_RIGHT);

	return true;
}

void g726_encoder_t::destroy()
{
	g726_release(m_ctx);
	g726_free(m_ctx);
	m_ctx = nullptr;
}

const char* g726_encoder_t::getEncoding()
{
	return m_encoding;
}

uint32_t g726_encoder_t::encode(const uint8_t* buff_from, uint32_t size_from, uint8_t* buff_to, uint32_t size_to)
{
	return g726_encode(m_ctx, buff_to, (const int16_t*)buff_from, size_from/sizeof(int16_t));
}

uint32_t g726_encoder_t::packetize(rtp_packet* packet, const uint8_t* buff, uint32_t buff_size)
{
	if (packet == nullptr)
	{
		return 0;
	}

	if (buff == nullptr)
	{
		return 0;
	}

	if (buff_size > rtp_packet::PAYLOAD_BUFFER_SIZE)
	{
		return 0;
	}

	if (packet->set_payload(buff, buff_size) == 0)
	{
		return 0;
	}

	packet->set_payload_type(m_payload_id);

	packet->set_samples(m_frame_size);

	return buff_size;
}

uint32_t g726_encoder_t::get_frame_size()
{
	return m_frame_size * sizeof(int16_t);
}

g726_decoder_t::g726_decoder_t() : m_ctx(nullptr), m_frame_size(0), m_encoding(nullptr), m_bit_rate(0)
{
}

g726_decoder_t::~g726_decoder_t()
{
	destroy();
}

bool g726_decoder_t::initialize(const media::PayloadFormat& format)
{
	const char* encoding = format.getEncoding();
	m_bit_rate = atoi(encoding + 5);

	m_frame_size = 20 * 8000 / 1000;
	m_payload_id = format.getId_u8();

	switch (m_bit_rate)
	{
	case 16:	m_encoding = "g726-16"; break;
	case 24:	m_encoding = "g726-24"; break;
	case 32:	m_encoding = "g726-32"; break;
	case 40:	m_encoding = "g726-40"; break;
		break;
	default:
		return false;
	}


	m_ctx = g726_init(nullptr, m_bit_rate * 1000, G726_ENCODING_LINEAR, G726_PACKING_RIGHT);

	return true;
}

void g726_decoder_t::destroy()
{
	if (!m_ctx)
	{
		g726_free(m_ctx);
		m_ctx = nullptr;
	}
}

const char* g726_decoder_t::getEncoding()
{
	return m_encoding;
}

uint32_t g726_decoder_t::depacketize(const rtp_packet* packet, uint8_t* buff, uint32_t buff_size)
{
	if (packet == nullptr)
	{
		return 0;
	}

	if (buff == nullptr)
	{
		return 0;
	}

	if (buff_size < packet->get_payload_length())
	{
		return 0;
	}

	uint32_t payload_length = packet->get_payload_length();

	memcpy(buff, packet->get_payload(), payload_length);

	return payload_length;
}

uint32_t g726_decoder_t::decode(const uint8_t* buff_from, uint32_t size_from, uint8_t* buff_to, uint32_t size_to)
{
	return g726_decode(m_ctx, (int16_t*)buff_to, buff_from, size_from) * sizeof(short);
}

uint32_t g726_decoder_t::get_frame_size_pcm()
{
	return m_frame_size * sizeof(uint16_t);
}

uint32_t g726_decoder_t::get_frame_size_cod()
{
	switch (m_bit_rate)
	{
	case 16: return 40;
	case 24: return 60;
	case 32: return 80;
	case 40: return 100;
	}

	return 0;
}
