/* RTX H.248 Media Gate. G.726 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_audio_codec.h"
#include "spandsp.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
extern int g_rtx_g726_decoder_counter;
extern int g_rtx_g726_encoder_counter;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class g726_encoder_t : public mg_audio_encoder_t
{
public:
	g726_encoder_t();
	virtual ~g726_encoder_t();

	bool initialize(const media::PayloadFormat& format);
	void destroy();

	virtual const char* getEncoding();
	virtual uint32_t encode(const uint8_t* buff_from, uint32_t size_from, uint8_t* buff_to, uint32_t size_to);
	virtual uint32_t packetize(rtp_packet* packet, const uint8_t* buff, uint32_t buff_size);
	virtual uint32_t get_frame_size();

private:
	g726_state_t* m_ctx;
	const char* m_encoding;
	int m_payload_id;
	uint32_t m_frame_size;
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class g726_decoder_t : public mg_audio_decoder_t
{
public:
	g726_decoder_t();
	virtual ~g726_decoder_t();

	bool initialize(const media::PayloadFormat& format);
	void destroy();

	virtual const char* getEncoding();
	virtual uint32_t depacketize(const rtp_packet* packet, uint8_t* buff, uint32_t buff_size);
	virtual uint32_t decode(const uint8_t* buff_from, uint32_t size_from, uint8_t* buff_to, uint32_t size_to);
	virtual uint32_t get_frame_size_pcm();
	virtual uint32_t get_frame_size_cod();

private:
	g726_state_t* m_ctx;
	const char* m_encoding;
	int m_payload_id;
	uint32_t m_frame_size;
	uint32_t m_bit_rate;
};
//------------------------------------------------------------------------------
