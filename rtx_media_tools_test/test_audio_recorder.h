/* RTX H.248 Media Gate Media Processing Test Module 
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "test_rtx_media_tools.h"
#include "mmt_recorder_wave.h"
#include "mmt_recorder_mp3.h"
//-----------------------------------------------
//
//-----------------------------------------------
namespace rtx_media_tools
{
	void audio_mono_recorder_test(const char* rtp_file, media::RecorderMono* rec_file);
	void audio_stereo_recorder_test(const char* left_file, const char* right_file, media::RecorderStereo* rec_file);
}
//-----------------------------------------------
