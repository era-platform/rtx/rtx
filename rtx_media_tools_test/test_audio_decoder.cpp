/* RTX H.248 Media Gate Media Processing Test Module 
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "test_audio_decoder.h"
#include "rtx_audio_codec.h"

//-----------------------------------------------
//
//-----------------------------------------------
namespace rtx_media_tools
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	int l_decode(mg_audio_decoder_t* dec, const uint8_t* cod, int length, uint8_t* pcm_buffer, int size)
	{
		int dec_len = 0;
		int pcm_len = 0;
		int dec_frame_size = dec->get_frame_size_cod();

		while (dec_len < length)
		{
			int res = dec->decode(cod + dec_len, dec_frame_size, pcm_buffer + pcm_len, size - pcm_len);

			pcm_len += res;
			dec_len += dec_frame_size;
		}

		return pcm_len;
	}

	int calc_pcm_buffer_size(int dec_size, int dec_frame_size, int pcm_frame_size)
	{
		return (dec_size / dec_frame_size) * pcm_frame_size;
	}

	//-----------------------------------------------
	//
	//-----------------------------------------------
	void audio_decode_test(media::PayloadFormat* format)
	{
		if (format == nullptr)
		{
			ERRORF("audio_decode_test....format is null\n");
			return;
		}

		mg_audio_decoder_t* decoder = rtx_codec__create_audio_decoder(format->getEncoding(), format);

		if (decoder == nullptr)
		{
			ERRORF("audio_decode_test....unknown codec %s\n", (const char*)format->getEncoding());
			return;
		}

		// 

		char vector_name[128];

		uint8_t* dec_data = nullptr;
		int dec_data_len = 0;
		//printf("start decode test for %s, %d, %d...\n", (const char*)format->getEncoding(), format->getFrequency(), format->getChannelCount());
		std_snprintf(vector_name, 128, "%s_%d_%d.vec", (const char*)format->getEncoding(), format->getFrequency(), format->getChannelCount());

		if (!load_test_vector_t(vector_name, &dec_data, &dec_data_len))
		{
			ERRORF("audio_decode_test....invalid path to test decoder vector '%s'\n", vector_name);
			return;
		}

		uint8_t* pcm_data = nullptr;
		int pcm_data_len = 0;

		std_snprintf(vector_name, 128, "%s_%d_%d.pcm", (const char*)format->getEncoding(), format->getFrequency(), format->getChannelCount());
	
		if (!load_test_vector_t(vector_name, &pcm_data, &pcm_data_len))
		{
			PRINTF("audio_decode_test....invalid path to test vector pcm '%s' -- generating new file\n", vector_name);
			return;
		}

		uint32_t pcm_buffer_len = calc_pcm_buffer_size(dec_data_len, decoder->get_frame_size_cod(), decoder->get_frame_size_pcm());

		uint8_t* pcm_buffer = (uint8_t*)MALLOC(pcm_buffer_len);

		int res = l_decode(decoder, dec_data, dec_data_len, pcm_buffer, pcm_buffer_len);

		rtx_codec__release_audio_decoder(decoder);

		ASSERT_EQ(res, pcm_data_len);

		int cmp_res = memcmp(pcm_data, pcm_buffer, pcm_data_len);

		//rtl::String fn = vector_name;
		//fn += "_out";
		//save_test_vector(fn, pcm_buffer, res);

		PRINTF("audio_decode_test....test result %d %d\n", res, cmp_res);

		ASSERT_EQ(cmp_res, 0);

		FREE(pcm_data);
		FREE(dec_data);
		FREE(pcm_buffer);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	TEST(test_tools_decoder, g711a_decode)
	{
		PRINTF("Test G711 alaw decoder...\n");
		media::PayloadFormat fmt("PCMA", media::PayloadId::PCMA, 8000, 1);
		audio_decode_test(&fmt);
	}

	TEST(test_tools_decoder, g711u_decode)
	{
		PRINTF("Test G711 mulaw decoder...\n");
		media::PayloadFormat fmt("PCMU", media::PayloadId::PCMU, 8000, 1);
		audio_decode_test(&fmt);
	}

	TEST(test_tools_decoder, g726_16_decode)
	{
		PRINTF("Test G726-16 decoder...\n");
		media::PayloadFormat fmt("G726-16", media::PayloadId::Dynamic, 8000, 1);
		audio_decode_test(&fmt);
	}

	TEST(test_tools_decoder, g726_24_decode)
	{
		PRINTF("Test G726-24 decoder...\n");
		media::PayloadFormat fmt("G726-24", media::PayloadId::Dynamic, 8000, 1);
		audio_decode_test(&fmt);
	}

	TEST(test_tools_decoder, g726_32_decode)
	{
		PRINTF("Test G726-32 decoder...\n");
		media::PayloadFormat fmt("G726-32", media::PayloadId::Dynamic, 8000, 1);
		audio_decode_test(&fmt);
	}

	TEST(test_tools_decoder, g726_40_decode)
	{
		PRINTF("Test G726-40 decoder...\n");
		media::PayloadFormat fmt("G726-40", media::PayloadId::Dynamic, 8000, 1);
		audio_decode_test(&fmt);
	}

	/*TEST(test_tools_decoder, g729_decode)
	{
		PRINTF("Test G729 decoder...\n");
		media::PayloadFormat fmt("G729", media::PayloadId::G729, 8000, 1);
		audio_decode_test(&fmt);
	}

	TEST(test_tools_decoder, gsm_decode)
	{
		PRINTF("Test GSM decoder...\n");
		media::PayloadFormat fmt("GSM", media::PayloadId::GSM610, 8000, 1);
		audio_decode_test(&fmt);
	}*/

	//TEST(test_tools_decoder, iLBC_decode)
	//{
	//	PRINTF("Test iLBC decoder...\n");
	//	media::PayloadFormat fmt("ilbc", media::PayloadId::Dynamic, 8000, 1);
	//	audio_decode_test(&fmt);
	//}

	TEST(test_tools_decoder, g722_decode)
	{
		PRINTF("Test G722 decoder...\n");
		media::PayloadFormat fmt("G722", media::PayloadId::G722, 16000, 1);
		audio_decode_test(&fmt);
	}
}
//-----------------------------------------------
