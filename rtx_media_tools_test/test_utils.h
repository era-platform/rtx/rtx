﻿/* RTX H.248 Media Gate Media Processing Test Module 
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

//-----------------------------------------------
//
//-----------------------------------------------
namespace rtx_media_tools
{
	rtl::String get_path_to_tests_data_t();
	int read_data_from_file_t(const char* path, uint8_t* buff, int buff_size);
	bool load_test_vector_t(const char* name, uint8_t** dataptr, int* datasize);
	void save_test_vector_t(const char* name, uint8_t* dataptr, int datasize);
}
//-----------------------------------------------
