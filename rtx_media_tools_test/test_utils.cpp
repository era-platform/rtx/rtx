﻿/* RTX H.248 Media Gate Media Processing Test Module 
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "test_rtx_media_tools.h"
#include "test_utils.h"
#include "std_sys_env.h"
#include "std_file_stream.h"

 //------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
namespace rtx_media_tools
{
	rtl::String get_path_to_tests_data_t()
	{
		const char* vec_path = Config.get_config_value("vecpath");

		if (vec_path == nullptr || vec_path[0] == 0)
		{
			char buffer[256];

			return std_get_application_startup_path(buffer, 256) ? rtl::String(buffer) : rtl::String::empty;
		}

		return rtl::String(vec_path);
	}
	//--------------------------------------------------------------------------
	int read_data_from_file_t(const char* path, uint8_t* buff, int buff_size)
	{
		rtl::FileStream file;
		if (!file.open(path))
		{
			return 0;
		}
		int read = int(file.read(buff, buff_size));
		file.close();
		return read;
	}
	bool load_test_vector_t(const char* name, uint8_t** dataptr, int* datasize)
	{
		rtl::String path = get_path_to_tests_data_t();
		path << FS_PATH_DELIMITER << name;

		rtl::FileStream file;
		if (!file.open(path))
		{
			return false;
		}

		*datasize = (int)file.getLength();
		*dataptr = (uint8_t*)MALLOC(*datasize);
		int read = (int)file.read(*dataptr, *datasize);
		file.close();
		if (read != *datasize)
		{
			FREE(*dataptr);
			*datasize = 0;
		}
		return *datasize != 0;
	}
	void save_test_vector_t(const char* name, uint8_t* dataptr, int datasize)
	{
		rtl::String path = get_path_to_tests_data_t();
		path << FS_PATH_DELIMITER << name;

		rtl::FileStream file;

		if (file.createNew(path))
		{
			file.write(dataptr, datasize);
			file.close();
		}
	}
}
//-----------------------------------------------
