/* RTX H.248 Media Gate Media Processing Test Module 
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "test_rtx_media_tools.h"
#include "test_audio_encoder.h"
#include "test_utils.h"
#include "rtx_audio_codec.h"

 //-----------------------------------------------
//
//-----------------------------------------------
namespace rtx_media_tools
{
//-----------------------------------------------
//
//-----------------------------------------------
int l_encode(mg_audio_encoder_t* enc, const uint8_t* pcm, int length, int frame_size, uint8_t* buffer, int size)
{
	int pcm_len = 0;
	int enc_len = 0;

	while (pcm_len + frame_size <= length)
	{
		int res = enc->encode((uint8_t*)pcm+pcm_len, frame_size, buffer + enc_len, size - enc_len);

		enc_len += res;
		pcm_len += frame_size;
	}

	return enc_len;
}
//-----------------------------------------------
//
//-----------------------------------------------
void audio_encode_test(media::PayloadFormat* format)
{
	if (format == nullptr)
	{
		ERRORF("audio_encode_test....format is null\n");
		return;
	}

	mg_audio_encoder_t* encoder = rtx_codec__create_audio_encoder(format->getEncoding(), format);

	if (encoder == nullptr)
	{
		ERRORF("audio_encode_test....unknown codec %s\n", (const char*)format->getEncoding());
		return;
	}

	uint8_t* pcm_data = nullptr;
	int pcm_data_len = 0;
	char vector_name[128];
	
	std_snprintf(vector_name, 128, "pcm_%d_%d.pcm", format->getFrequency(), format->getChannelCount());

	if (!load_test_vector_t(vector_name, &pcm_data, &pcm_data_len))
	{
		ERRORF("audio_encode_test....invalid path to test vector '%s'\n", vector_name);
		return;
	}

	uint8_t* enc_data = nullptr;
	int enc_data_len = 0;
	
	std_snprintf(vector_name, 128, "%s_%d_%d.vec", (const char*)format->getEncoding(), format->getFrequency(), format->getChannelCount());

	if (!load_test_vector_t(vector_name, &enc_data, &enc_data_len))
	{
		//uint8_t* enc_buffer = (uint8_t*)MALLOC(pcm_data_len);
		//int fs = (format->getFrequency() / 1000 * 20) * 2;
		//int res = l_encode(encoder, pcm_data, pcm_data_len, fs, enc_buffer, pcm_data_len);
		//save_test_vector(vector_name, enc_buffer, res);

		ERRORF("audio_encode_test....invalid path to test vector '%s'\n", vector_name);
		return;
	}

	uint8_t* enc_buffer = (uint8_t*)MALLOC(enc_data_len);

	int fs = (format->getFrequency() / 1000 * 20) * 2;
	int res = l_encode(encoder, pcm_data, pcm_data_len, fs, enc_buffer, enc_data_len);


	rtx_codec__release_audio_encoder(encoder);

	ASSERT_EQ(res, enc_data_len);

	int cmp_res = memcmp(enc_data, enc_buffer, enc_data_len);

	//rtl::String fn = vector_name;
	//fn += "_out";
	//save_test_vector(fn, enc_buffer, enc_data_len);

	PRINTF("audio_encode_test....test result %d %d\n", res, cmp_res);

	ASSERT_EQ(cmp_res, 0);

	FREE(pcm_data);
	FREE(enc_data);
	FREE(enc_buffer);
}
//-----------------------------------------------
//
//-----------------------------------------------
TEST(test_tools_encoder, g711a_encode)
{
	media::PayloadFormat fmt("PCMA", media::PayloadId::PCMA, 8000, 1);

	PRINTF("Test G711 alaw encoder...\n");

	audio_encode_test(&fmt);
}

TEST(test_tools_encoder, g711u_encode)
{
	PRINTF("Test G711 mulaw encoder...\n");
	media::PayloadFormat fmt( "PCMU", media::PayloadId::PCMU, 8000, 1);
	audio_encode_test(&fmt);
}

TEST(test_tools_encoder, g726_16_encode)
{
	PRINTF("Test G726-16 encoder...\n");
	media::PayloadFormat fmt( "G726-16", media::PayloadId::Dynamic, 8000, 1);
	audio_encode_test(&fmt);
}

TEST(test_tools_encoder, g726_24_encode)
{
	PRINTF("Test G726-24 encoder...\n");
	media::PayloadFormat fmt( "G726-24", media::PayloadId::Dynamic, 8000, 1);
	audio_encode_test(&fmt);
}

TEST(test_tools_encoder, g726_32_encode)
{
	PRINTF("Test G726-32 encoder...\n");
	media::PayloadFormat fmt( "G726-32", media::PayloadId::Dynamic, 8000, 1);
	audio_encode_test(&fmt);
}

TEST(test_tools_encoder, g726_40_encode)
{
	PRINTF("Test G726-40 encoder...\n");
	media::PayloadFormat fmt( "G726-40", media::PayloadId::Dynamic, 8000, 1);
	audio_encode_test(&fmt);
}

//TEST(test_tools_encoder, g729_encode)
//{
//	PRINTF("Test G729 encoder...\n");
//	media::PayloadFormat fmt( "G729", media::PayloadId::G729, 8000, 1);
//	audio_encode_test(&fmt);
//}
//
//TEST(test_tools_encoder, gsm_encode)
//{
//	PRINTF("Test GSM encoder...\n");
//	media::PayloadFormat fmt( "GSM", media::PayloadId::GSM610, 8000, 1);
//	audio_encode_test(&fmt);
//}

//TEST(test_tools_encoder, iLBC_encode)
//{
//	PRINTF("Test iLBC encoder...\n");
//	media::PayloadFormat fmt( "ilbc", media::PayloadId::Dynamic, 8000, 1);
//	audio_encode_test(&fmt);
//}

//TEST(test_tools_encoder, g722_encode)
//{
//	PRINTF("Test G722 encoder...\n");
//	media::PayloadFormat fmt( "G722", media::PayloadId::G722, 16000, 1);
//	audio_encode_test(&fmt);
//}


//TEST(test_speex8_encoder, encode)
//{
//	PRINTF("test Speex(8) encoder...\n");
//	PayloadFormat fmt( "speex", 96, 8000, 1);
//	audio_encode_test(&fmt);
//}
//
//TEST(test_speex16_encoder, encode)
//{
//	PRINTF("test Speex(16) encoder...\n");
//	PayloadFormat fmt( "speex", 96, 16000, 1);
//	audio_encode_test(&fmt);
//}
//
//TEST(test_speex32_encoder, encode)
//{
//	PRINTF("test Speex(32) encoder...\n");
//	PayloadFormat fmt( "speex", 96, 32000, 1);
//	audio_encode_test(&fmt);
//}

//TEST(test_opus_encoder, encode)
//{
//	PRINTF("test Opus encoder...\n");
//	PayloadFormat fmt( "opus", 96, 8000, 1);
//	audio_encode_test(&fmt);
//}

}
//-----------------------------------------------
