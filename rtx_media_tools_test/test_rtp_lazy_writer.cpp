/* RTX H.248 Media Gate Media Processing Test Module 
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "test_rtx_media_tools.h"
#include "mmt_file_rtp.h"
#include "test_rtp_lazy_writer.h"
#include "test_utils.h"

 //-----------------------------------------------
//
//-----------------------------------------------
namespace rtx_media_tools
{
	void test_run_lazy_writer(const char* src_path, const char* dst_path)
	{
		media::FileReaderRTP reader;
		media::LazyMediaWriterRTP writer(&Log);

		const media::RTPFilePacketHeader* packet = nullptr;

		reader.open(src_path);
		writer.create(dst_path);

		writer.start();

		const media::RTPFileHeader& header = reader.get_header();
		uint32_t last = header.rec_start_timestamp;
		int cycles = 0, cycles_last = last;

		while ((packet = reader.readNext()) != nullptr)
		{
			if (packet->type == RTYPE_FORMAT)
			{
				int count = packet->length / sizeof(media::RTPFileMediaFormat);
				media::PayloadSet set;

				for (int i = 0; i < count; i++)
				{
					const media::RTPFileMediaFormat& fmt = packet->format[i];

					set.addFormat(fmt.encoding, (media::PayloadId)fmt.payload_id, fmt.clock_rate,
						fmt.channels, fmt.fmtp);
				}

				writer.update_format(set);
			}
			else if (packet->type == RTYPE_RTP)
			{
				rtp_packet rtp_pack;
				rtp_pack.read_packet(packet->buffer, packet->length);
				writer.write_packet(&rtp_pack);

				if ((cycles % 25) == 0)
				{
					PRINTF("sleep(%d) %d\n", cycles, int(packet->timestamp - cycles_last));
					cycles_last = packet->timestamp;
				}

				cycles++;

				rtl::Thread::sleep(int(packet->timestamp - last));
				last = packet->timestamp;
			}
		}

		PRINTF("writing: %d packets processed\n", cycles);

		writer.stop();
		writer.close();
		reader.close();
	}
	static void test_compare_files(const char* src_path, const char* dst_path)
	{
		media::FileReaderRTP src, dst;
		int cycles = 0;

		ASSERT_TRUE(src.open(src_path) && dst.open(dst_path));
		
		while (!src.eof() && !dst.eof())
		{
			const media::RTPFilePacketHeader* l = src.readNext();
			const media::RTPFilePacketHeader* r = dst.readNext();

			if (l->type == RTYPE_RTP)
			{
				ASSERT_EQ(memcmp(l->buffer, r->buffer, l->length), 0);
				cycles++;
			}
		}

		PRINTF("comparing: %d packets processed\n", cycles);
	}

	TEST(test_tools_lazy_rec, rtp_writer_pcma)
	{
		rtl::String data_path = get_path_to_tests_data_t();

		rtl::String src_path = data_path + FS_PATH_DELIMITER + "pcma_src.rtp";
		rtl::String dst_path = data_path + FS_PATH_DELIMITER + "pcma_dst.rtp";

		test_run_lazy_writer(src_path, dst_path);

		test_compare_files(src_path, dst_path);

		fs_delete_file(dst_path);
	}

	TEST(test_tools_lazy_rec, rtp_writer_g722)
	{
		rtl::String data_path = get_path_to_tests_data_t();

		rtl::String src_path = data_path + FS_PATH_DELIMITER + "G722_src.rtp";
		rtl::String dst_path = data_path + FS_PATH_DELIMITER + "G722_dst.rtp";

		test_run_lazy_writer(src_path, dst_path);

		test_compare_files(src_path, dst_path);

		fs_delete_file(dst_path);
	}
}
