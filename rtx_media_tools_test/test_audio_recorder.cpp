/* RTX H.248 Media Gate Media Processing Test Module 
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "test_rtx_media_tools.h"
#include "mmt_file_rtp.h"
#include "rtx_audio_codec.h"
#include "test_audio_recorder.h"
#include "test_utils.h"

 //-----------------------------------------------
//
//-----------------------------------------------
namespace rtx_media_tools
{
	struct codec_info_t { int pid; mg_audio_decoder_t* dec; };
	typedef rtl::ArrayT<codec_info_t> codec_list_t;

	static mg_audio_decoder_t* find_codec(int pid, codec_list_t& codec_set)
	{
		for (int j = 0; j < codec_set.getCount(); j++)
		{
			codec_info_t& info = codec_set.getAt(j);

			if (info.pid == pid)
			{
				return info.dec;
			}
		}

		return nullptr;
	}

	static mg_audio_decoder_t* find_codec(const char* encoding, codec_list_t& codec_set)
	{
		for (int j = 0; j < codec_set.getCount(); j++)
		{
			codec_info_t& info = codec_set.getAt(j);

			if (std_stricmp(encoding, info.dec->getEncoding()) == 0)
			{
				return info.dec;
			}
		}

		return nullptr;
	}

	static void add_codec(codec_list_t& codec_set, const media::PayloadSet& set)
	{
		for (int i = 0; i < set.getCount(); i++)
		{
			const media::PayloadFormat& pf = set.getAt(i);
			const char* enc = pf.getEncoding();

			if (find_codec(enc, codec_set) == nullptr)
			{
				mg_audio_decoder_t* dec = rtx_codec__create_audio_decoder(pf.getEncoding(), &pf);

				if (dec != nullptr)
				{
					codec_set.add({ pf.getId_u8(), dec });
				}
			}
		}
	}

	static void clear_codec_list(codec_list_t& codec_set)
	{
		for (int i = 0; i < codec_set.getCount(); i++)
		{
			rtx_codec__release_audio_decoder(codec_set[i].dec);
		}

		codec_set.clear();
	}

	static int decode(short* samples, int size, const media::RTPFilePacketHeader* packet, codec_list_t& codec_set)
	{
		rtp_packet rtp_pack;
		rtp_pack.read_packet(packet->buffer, packet->length);

		mg_audio_decoder_t* dec = find_codec(rtp_pack.get_payload_type(), codec_set);

		if (dec != nullptr)
		{
			return dec->decode(rtp_pack.get_payload(), rtp_pack.get_payload_length(), (uint8_t*)samples, size * sizeof(short)) / sizeof(short);
		}

		return 0;
	}

	static void test_compare_files(const char* dst_path, const char* ref_path)
	{
		rtl::FileStream dst, ref;
		int cycles = 0;

		uint8_t dst_buf[2048], ref_buf[2048];

		ASSERT_TRUE(dst.open(dst_path) && ref.open(ref_path));

		while (true)
		{
			size_t dst_read = dst.read(dst_buf, 2048);
			size_t ref_read = ref.read(ref_buf, 2048);
			bool is_eq_1 = dst_read == ref_read;

			if (dst_read == 0 && ref_read == 0)
			{
				// OK;
				return;
			}

			if (dst_read == 0 || ref_read == 0)
			{
				ASSERT_TRUE(dst_read > 0 && ref_read > 0);
				return;
			}

			if (!is_eq_1)
			{
				ASSERT_EQ(dst_read, ref_read);
				return;
			}

			int cmp_res = memcmp(dst_buf, ref_buf, dst_read);

			if (cmp_res)
			{
				ASSERT_TRUE(cmp_res == 0);
				return;
			}

			cycles++;
		}

		PRINTF("comparing: %d packets processed\n", cycles);
	}

	void audio_mono_recorder_test(const char* rtp_file, media::RecorderMono* rec_file)
	{
		media::FileReaderRTP reader;
		codec_list_t codec_set;
		const media::RTPFilePacketHeader* packet = nullptr;

		reader.open(rtp_file);

		const media::RTPFileHeader& header = reader.get_header();

		while ((packet = reader.readNext()) != nullptr)
		{
			if (packet->type == RTYPE_FORMAT)
			{
				int count = packet->length / sizeof(media::RTPFileMediaFormat);
				media::PayloadSet set;

				for (int i = 0; i < count; i++)
				{
					const media::RTPFileMediaFormat& fmt = packet->format[i];

					set.addFormat(fmt.encoding, (media::PayloadId)fmt.payload_id, fmt.clock_rate,
						fmt.channels, fmt.fmtp);
				}

				add_codec(codec_set, set);
			}
			else if (packet->type == RTYPE_RTP)
			{
				short samples[4096];
				int count = decode(samples, 4096, packet, codec_set);

				if (count > 0)
				{
					rec_file->write(samples, count);
				}
			}
		}

		rec_file->close();
		reader.close();

		clear_codec_list(codec_set);
	}

	static const media::RTPFilePacketHeader* readNext(media::FileReaderRTP& reader, codec_list_t& codec_set)
	{
		const media::RTPFilePacketHeader* packet = nullptr;

		while ((packet = reader.readNext()) != nullptr)
		{
			if (packet->type == RTYPE_FORMAT)
			{
				int count = packet->length / sizeof(media::RTPFileMediaFormat);
				media::PayloadSet set;

				for (int i = 0; i < count; i++)
				{
					const media::RTPFileMediaFormat& fmt = packet->format[i];

					set.addFormat(fmt.encoding, (media::PayloadId)fmt.payload_id, fmt.clock_rate,
						fmt.channels, fmt.fmtp);
				}

				add_codec(codec_set, set);
			}
			else
			{
				return packet;
			}
		}

		return nullptr;
	}

	void audio_stereo_recorder_test(const char* left_file, const char* right_file, media::RecorderStereo* rec_file)
	{
		media::FileReaderRTP l_reader, r_reader;
		
		codec_list_t l_codec_set, r_codec_set;
		const media::RTPFilePacketHeader* l_packet = nullptr, *r_packet = nullptr;

		l_reader.open(left_file);
		r_reader.open(right_file);

		const media::RTPFileHeader& l_header = l_reader.get_header();
		const media::RTPFileHeader& r_header = r_reader.get_header();

		while (!l_reader.eof() && !r_reader.eof())
		{
			l_packet = readNext(l_reader, l_codec_set);
			r_packet = readNext(r_reader, r_codec_set);

			if (l_packet == nullptr || r_packet == nullptr)
				break;

			short l_samples[4096];
			short r_samples[4096];
			
			int l_count = decode(l_samples, 4096, l_packet, l_codec_set);
			int r_count = decode(r_samples, 4096, r_packet, r_codec_set);

			if (l_count > 0 && l_count == r_count)
			{
				rec_file->write(l_samples, r_samples, l_count);
			}
		}

		rec_file->close();
		l_reader.close();
		r_reader.close();

		clear_codec_list(l_codec_set);

		clear_codec_list(r_codec_set);
	}

	TEST(test_tools_audio_rec, wave_mono_recorder)
	{
		rtl::String data_path = get_path_to_tests_data_t();

		rtl::String src_path = data_path + FS_PATH_DELIMITER + "pcma_src.rtp";
		rtl::String dst_path = data_path + FS_PATH_DELIMITER + "pcma_dst.wav";
		rtl::String ref_path = data_path + FS_PATH_DELIMITER + "pcma_ref.wav";

		media::RecorderMonoWave writer(8000);

		writer.create(dst_path, "L16", 8000, nullptr);

		audio_mono_recorder_test(src_path, &writer);

		test_compare_files(dst_path, ref_path);

		fs_delete_file(dst_path);
	}

	TEST(test_tools_audio_rec, wave_gsm_mono_recorder)
	{
		rtl::String data_path = get_path_to_tests_data_t();

		rtl::String src_path = data_path + FS_PATH_DELIMITER + "pcma_src.rtp";
		rtl::String dst_path = data_path + FS_PATH_DELIMITER + "gsm_dst.wav";
		rtl::String ref_path = data_path + FS_PATH_DELIMITER + "gsm_ref.wav";

		media::RecorderMonoWave writer(8000);

		writer.create(dst_path, "gsm", 8000, nullptr);

		audio_mono_recorder_test(src_path, &writer);

		test_compare_files(dst_path, ref_path);

		fs_delete_file(dst_path);
	}

	/*TEST(test_tools_audio_rec, mp3_mono_recorder)
	{
		rtl::String data_path = get_path_to_tests_data_t();

		rtl::String src_path = data_path + FS_PATH_DELIMITER + "pcma_src.rtp";
		rtl::String dst_path = data_path + FS_PATH_DELIMITER + "pcma_dst.mp3";
		rtl::String ref_path = data_path + FS_PATH_DELIMITER + "pcma_ref.mp3";

		media::RecorderMonoMp3 writer(8000);

		writer.create(dst_path, "L16", 8000, nullptr);

		audio_mono_recorder_test(src_path, &writer);

		test_compare_files(dst_path, ref_path);

		fs_delete_file(dst_path);
	}*/

	TEST(test_tools_audio_rec, wave_stereo_recorder)
	{
		rtl::String data_path = get_path_to_tests_data_t();

		rtl::String left_path = data_path + FS_PATH_DELIMITER + "pcma_left_src.rtp";
		rtl::String right_path = data_path + FS_PATH_DELIMITER + "pcma_right_src.rtp";
		rtl::String dst_path = data_path + FS_PATH_DELIMITER + "pcma_stereo_dst.wav";
		rtl::String ref_path = data_path + FS_PATH_DELIMITER + "pcma_stereo_ref.wav";

		media::RecorderStereoWave writer(8000);

		writer.create(dst_path, "L16", 8000, nullptr);

		audio_stereo_recorder_test(left_path, right_path, &writer);

		test_compare_files(dst_path, ref_path);

		fs_delete_file(dst_path);
	}

	/*TEST(test_tools_audio_rec, mp3_stereo_recorder)
	{
		rtl::String data_path = get_path_to_tests_data_t();

		rtl::String left_path = data_path + FS_PATH_DELIMITER + "pcma_left_src.rtp";
		rtl::String right_path = data_path + FS_PATH_DELIMITER + "pcma_right_src.rtp";
		rtl::String dst_path = data_path + FS_PATH_DELIMITER + "pcma_stereo_dst.mp3";
		rtl::String ref_path = data_path + FS_PATH_DELIMITER + "pcma_stereo_ref.mp3";

		media::RecorderStereoMp3 writer(8000);

		writer.create(dst_path, "L16", 8000, nullptr);

		audio_stereo_recorder_test(left_path, right_path, &writer);

		test_compare_files(dst_path, ref_path);

		fs_delete_file(dst_path);
	}*/
}
