/* RTX H.248 Media Gate Media Processing Test Module 
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "test_rtx_media_tools.h"
#include "test_audio_reader.h"
#include "test_utils.h"

 //-----------------------------------------------
//
//-----------------------------------------------
namespace rtx_media_tools
{
	void audio_reader_test(const char* src_path, const char* ref_path)
	{
		media::AudioReader* reader = media::AudioReader::create(src_path, 8000, &Log);
		rtl::FileStream ref;
		bool generate = false;
		short pcm_buffer[160];
		short ref_buffer[160];

		if (!ref.open(ref_path))
		{
			generate = ref.createNew(ref_path);
		}

		while (!reader->eof())
		{
			int count = reader->readNext(pcm_buffer, 160);

			if (count <= 0)
				break;

			if (generate)
			{
				ref.write(pcm_buffer, count * sizeof(short));
			}
			else
			{
				int ref_count = (int)ref.read(ref_buffer, count * sizeof(short)) / sizeof(short);

				if (ref_count != count)
				{
					ASSERT_TRUE(ref_count == count);
					reader->close();
					DELETEO(reader);
					ref.close();
					return;
				}

				int res = memcmp(pcm_buffer, ref_buffer, count * sizeof(short));

				if (res)
				{
					ASSERT_EQ(res, 0);
					reader->close();
					DELETEO(reader);
					ref.close();
					return;
				}
			}
			
		}

		ref.close();
		reader->close();
		DELETEO(reader);
	}

	TEST(test_tools_audio_reader, rtp_reader)
	{
		rtl::String data_path = get_path_to_tests_data_t();

		media::AudioReader::setRTPReaderType(false);

		rtl::String src_path = data_path + FS_PATH_DELIMITER + "pcma_src.rtp";
		rtl::String ref_path = data_path + FS_PATH_DELIMITER + "pcma_ref_rtp.pcm";



		audio_reader_test(src_path, ref_path);
	}

	TEST(test_tools_audio_reader, linear_rtp_reader)
	{
		rtl::String data_path = get_path_to_tests_data_t();

		media::AudioReader::setRTPReaderType(true);

		rtl::String src_path = data_path + FS_PATH_DELIMITER + "pcma_src_shuffled.rtp";
		rtl::String ref_path = data_path + FS_PATH_DELIMITER + "pcma_ref_rtp_sorted.pcm";

		audio_reader_test(src_path, ref_path);

		media::AudioReader::setRTPReaderType(false);
	}

	TEST(test_tools_audio_reader, wave_reader)
	{
		rtl::String data_path = get_path_to_tests_data_t();

		rtl::String src_path = data_path + FS_PATH_DELIMITER + "pcma_src.wav";
		rtl::String ref_path = data_path + FS_PATH_DELIMITER + "pcma_ref_wav.pcm";

		audio_reader_test(src_path, ref_path);
	}

/*	TEST(test_tools_audio_reader, wave_gsm_reader)
	{
		rtl::String data_path = get_path_to_tests_data_t();

		rtl::String src_path = data_path + FS_PATH_DELIMITER + "gsm_src.wav";
		rtl::String ref_path = data_path + FS_PATH_DELIMITER + "gsm_ref_wav.pcm";

		audio_reader_test(src_path, ref_path);
	}*/

	/*TEST(test_tools_audio_reader, mp3_reader)
	{
		rtl::String data_path = get_path_to_tests_data_t();

		rtl::String src_path = data_path + FS_PATH_DELIMITER + "pcma_src.mp3";
		rtl::String ref_path = data_path + FS_PATH_DELIMITER + "pcma_ref_mp3.pcm";

		audio_reader_test(src_path, ref_path);
	}*/
}
