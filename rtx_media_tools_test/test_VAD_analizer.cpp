/* RTX H.248 Media Gate Media Processing Test Module 
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "test_rtx_media_tools.h"
#include "mmt_file_wave.h"
#include "mmt_vad.h"
#include "test_utils.h"

 //-----------------------------------------------
//
//-----------------------------------------------
namespace rtx_media_tools
{
	void test_run_vad_CN(const char* src_path);

	static const int RBUF_SIZE = 160;

	struct event_time { int vad; int offset; int duration; };

	int test_run_vad_stream(const char* src_path, rtl::ArrayT<event_time>& result)
	{
		short pcm[RBUF_SIZE];
		media::VAD_Detecter detector(&Log);
		media::FileReaderWAV reader;

		if (reader.open(src_path))
		{
			ERRORF("aset file not found");
			return 0;
		}

		int read = 0;
		bool vad = false;
		int duration = 0;
		int offset;
		detector.create(8000, 40, 40, 500);

		while ((read = reader.readAudioData(pcm, sizeof(short) * RBUF_SIZE)) > 0)
		{
			if ((offset = detector.addPacket(pcm, read, vad, duration)))
			{
				// print event;
				result.add({vad, offset, duration});
			}
		}

		reader.close();

		return result.getCount();
	}

	TEST(test_tools_vad, vad_detector_stream)
	{
		rtl::String data_path = get_path_to_tests_data_t();

		rtl::String src_path = data_path + FS_PATH_DELIMITER + "CN_test_full.wav";

		rtl::ArrayT<event_time> result;

		int rcount = test_run_vad_stream(src_path, result);

		for (int i = 0; i < rcount; i++)
		{
			event_time& t = result[i];

			PRINTF("VAD event : %s %d %d", t.vad ? "VAD" : "SIL", t.duration, t.offset);
		}
	}

	TEST(test_tools_vad, vad_detector_CN)
	{
		/*rtl::String data_path = get_path_to_tests_data_t();

		rtl::String src_path = data_path + FS_PATH_DELIMITER + "G722_src.rtp";
		
		test_run_vad_CN(src_path);*/
	}
}
