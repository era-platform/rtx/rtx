﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_context.h"
#include "mg_direction_audio_conference.h"
#include "mg_tube_manager.h"
#include "mg_rx_stream.h"
#include "mg_tx_stream.h"
#include "mg_audio_conference_ctrl.h"
#include "mg_audio_conference_out_tube.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define LOG_PREFIX m_tag // "MG-DIRA" // Media Gateway Direction
#define MG_DTMF_EVENT "mg-dtmf"

#include "logdef.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	AudioConferenceDirection::AudioConferenceDirection(MediaContext* owner, uint32_t id, const char* parent_id) :
		Direction(owner, id, parent_id, DirectionType::conference)
	{
		m_sampleRate = 8000;
		m_ptime = 20;
		m_ctrl = nullptr;
		m_audioAnalyzer = nullptr;
		m_pathRecord = rtl::String::empty;
		strcpy(m_tag, "DIR-ACON");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	AudioConferenceDirection::~AudioConferenceDirection()
	{
		if (m_ctrl != nullptr)
		{
			DELETEO(m_ctrl);
			m_ctrl = nullptr;
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool AudioConferenceDirection::initialize()
	{
		m_sampleRate = m_owner->getConf_frequency();
		m_ptime = m_owner->getConf_ptime();

		//PRINT_FMT("processor type: %d, sample rate: %d, ptime: %d.", (const char*)m_name, m_processor_type, m_sampleRate, m_ptime);

		if (m_ctrl != nullptr)
		{
			DELETEO(m_ctrl);
			m_ctrl = nullptr;
		}

		m_ctrl = NEW AudioConferenceController(m_log, m_name);



		if (!m_ctrl->init(m_sampleRate, m_ptime, 3))
		{
			DELETEO(m_ctrl);
			m_ctrl = nullptr;
			//ERROR_MSG("create conference tube fail.");
			return false;
		}

		if (m_pathRecord.isEmpty())
		{
			return true;
		}

		/*media::PayloadSet record_formats;
		record_formats.addFormat("L16", media::PayloadId::L16M, m_sampleRate, 1);
		record_formats.addFormat("CN", media::PayloadId::CN, m_sampleRate, 1);*/

		//mg_media_element_list_t list;
		//mg_media_element_t* element = mg_create_media_element_audio_conf_recorder(getName(), &list);

		//Tube* recorder_tube = nullptr;
		//if (!mg_tube_manager__create_audio_conf_record_tube(this, &recorder_tube, nullptr, element, m_pathRecord, record_formats))
		//{
		//	//PLOG_ERROR(LOG_PREFIX, "init -- ID(%s): create conference record tube fail.", (const char*)m_name);
		//	record_formats.removeAll();
		//	return false;
		//}
		//record_formats.removeAll();

		//m_ctrl->setRecorderHandler(recorder_tube);

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool AudioConferenceDirection::addTermination(Termination* term)
	{
		//PLOG_WRITE(LOG_PREFIX, "add_termination -- ID(%s): begin.", (const char*)m_name);

		if (term == nullptr)
		{
			//PLOG_ERROR(LOG_PREFIX, "add_termination -- ID(%s): termination is null.", (const char*)m_name);
			return false;
		}

		int check_res = checkTermination(term);
		if (check_res < 0)
		{
			return false;
		}

		cleanupTerminationTubes(term);

		h248::TerminationType termination_type = term->getType();
		const char* termination_id = term->getName();
		MediaSession* mg_stream = term->findMediaSession(getProcessorType());
		if (mg_stream == nullptr)
		{
			ERROR_FMT("mg_stream is null in: %s.", termination_id);
			return false;
		}

		const rtl::ArrayT<mg_rx_stream_t*>& list_rx = mg_stream->get_rx_streams();
		const rtl::ArrayT<mg_tx_stream_t*>& list_tx = mg_stream->get_tx_streams();

		int count_rx = list_rx.getCount();
		int count_tx = list_tx.getCount();

		int count = (count_rx > count_tx) ? count_rx : count_tx;

		for (int i = 0; i < count; i++)
		{
			mg_rx_stream_t* rx_stream = (count_rx <= i) ? nullptr : list_rx.getAt(i);
			mg_tx_stream_t* tx_stream = (count_tx <= i) ? nullptr : list_tx.getAt(i);

			Tube* conference_in_tube = buildRx(termination_type, termination_id, rx_stream, tx_stream);
			Tube* conference_out_tube = buildTx(termination_type, termination_id, rx_stream, tx_stream);

			// audio and video conferencing tubes must be labeled for video conferencing
			if (conference_in_tube)
			{
				conference_in_tube->set_tag(term->getId());
				conference_in_tube->setNextHandler(m_owner->getConf_audioAnalyzer());
			}
			else
			{
				ERROR_FMT("conference_in_tube is null in: %s.", termination_id);
			}

			if (!m_ctrl->addMember(conference_in_tube, conference_out_tube, termination_id))
			{
				cleanupTerminationTubes(term);
				return false;
			}

			if ((tx_stream != nullptr) && (conference_out_tube != nullptr))
			{
				tx_stream->set_conference_out_tube(conference_out_tube);
			}
		}

		if (!term->mg_stream_start((getProcessorType())))
		{
			cleanupTerminationTubes(term);
			return false;
		}

		if (check_res == 1)
		{
			Direction::addTermination(term);
		}

		//PLOG_WRITE(LOG_PREFIX, "add_termination -- ID(%s): end. (id term: %s)", (const char*)m_name, term->getName());

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool AudioConferenceDirection::searchTopologyTubes(const char* termIdFrom, mg_audio_conference_out_tube_t*& fromTube, const char* termIdTo, mg_audio_conference_out_tube_t*& toTube)
	{
		for (int i = 0; i < m_tubeList.getCount(); i++)
		{
			Tube* tube = m_tubeList[i];
			if (tube != nullptr && tube->getType() == HandlerType::ConfAudioOutput)
			{
				mg_audio_conference_out_tube_t* out_tube = (mg_audio_conference_out_tube_t*)tube;
				if (strstr(out_tube->get_termination_name(), termIdFrom) != nullptr)
				{
					fromTube = out_tube;
				}
				else if (strstr(out_tube->get_termination_name(), termIdTo) != nullptr)
				{
					toTube = out_tube;
				}

				// complite
				if (fromTube != nullptr && toTube != nullptr)
				{
					return true;
				}
			}
		}

		return false;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void AudioConferenceDirection::updateTopology(const rtl::ArrayT<h248::TopologyTriple>& triple_list)
	{
		// пробегаем по всему списку топологий.
		for (int index_topol = 0; index_topol < triple_list.getCount(); index_topol++)
		{
			const h248::TopologyTriple& trip = triple_list.getAt(index_topol);

			//----------------------------------------------------------------------------------------
			// ищем терминэйшены указанные в топологии.
			Termination* term_to = findTermination(trip.term_to);
			Termination* term_from = findTermination(trip.term_from);

			if (term_from == nullptr || term_to == nullptr)
			{
				continue;
			}
			//----------------------------------------------------------------------------------------

			//----------------------------------------------------------------------------------------
			// ищем тубы соответсвующие терминэйшнам в конференции.
			mg_audio_conference_out_tube_t* tube_from = nullptr;
			mg_audio_conference_out_tube_t* tube_to = nullptr;

			if (searchTopologyTubes(term_from->getName(), tube_from, term_to->getName(), tube_to))
			{
				tube_from->changeTopologyFrom(tube_to->get_termination_name(), trip.direction);
				tube_to->changeTopologyTo(tube_from->get_termination_name(), trip.direction);
			}
			else if (tube_from != nullptr && term_to->getType() == h248::TerminationType::IVR_Player)
			{
				tube_from->changeTopologyFrom(term_to->getName(), trip.direction);
			}
			else if (tube_to != nullptr && term_from->getType() == h248::TerminationType::IVR_Player)
			{
				tube_to->changeTopologyTo(term_from->getName(), trip.direction);
			}
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	int AudioConferenceDirection::checkTermination(Termination* term)
	{
		if (m_ctrl == nullptr)
		{
			return -1;
		}

		if (!term->isReady(getProcessorType()))
		{
			return -2;
		}

		if (containsTermination(term->getId()))
		{
			//PLOG_ERROR(LOG_PREFIX, "add_termination -- ID(%s): termination already exist.", (const char*)m_name);
			//remove_termination(term->get_id());
			return 0;
		}

		return 1;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	Tube* AudioConferenceDirection::buildRx(h248::TerminationType termination_type, const char* termination_id, mg_rx_stream_t* rx_stream, mg_tx_stream_t* tx_stream)
	{
		mg_media_element_list_t fill_list_rx;
		mg_media_element_list_t fill_list_tx;

		fillElementListRx(termination_type, termination_id, rx_stream, tx_stream, &fill_list_rx, &fill_list_tx);

		if ((fill_list_rx.getCount() == 0) || (fill_list_tx.getCount() == 0))
		{
			WARNING("RX media element lists is empty!");
			return nullptr;
		}

		mg_media_element_t* element_out = fill_list_tx.get_priority();
		uint32_t mixer_frame_size = m_ptime * m_sampleRate / 1000;

		Tube* tube_out = nullptr;
		for (int index = 0; index < fill_list_rx.getCount(); index++)
		{
			mg_media_element_t* element_in = fill_list_rx.getAt(index);
			if ((element_in == nullptr) || (element_in->isNull()))
			{
				//PLOG_ERROR(LOG_PREFIX, "associate_streams -- ID(%s): elements is null.", (const char*)m_name);
				continue;
			}

			bool clear_out = false;
			if (tube_out == nullptr)
			{
				if ((tube_out = createInputTube(element_in, element_out, mixer_frame_size)) == nullptr)
				{
					WARNING("TubeManager::create_conference_in_tube fail.");
					return nullptr;
				}
				clear_out = true;
			}

			mg_splitter_params_t p;
			bool pres = MediaSession::vad_isActive(rx_stream, &p);

			if (termination_type == h248::TerminationType::RTP && pres)
			{
				element_in->setSplitterParams(&p);
			}

			PRINT_FMT("check VAD (term:%s vad:%s)", termination_id, STR_BOOL(pres));

			Tube* tube_in = nullptr;

			PRINT_FMT("TubeManager::prepare_tubes_chain (%s %s)",
				element_in ? (const char*)element_in->get_element_name() : "(null)",
				element_out ? (const char*)element_out->get_element_name() : "(null)");

			if (!TubeManager::prepare_tubes_chain(this, element_in, element_out, &tube_in, &tube_out, true, clear_out))
			{
				//PLOG_ERROR(LOG_PREFIX, "associate_streams -- ID(%s): prepare tubes chain fail.", (const char*)m_name);
				continue;
			}

			if ((tube_in == nullptr) || (tube_out == nullptr))
			{
				//PLOG_ERROR(LOG_PREFIX, "associate_streams -- ID(%s): bad tubes.", (const char*)m_name);
				continue;
			}

			if (!customizeStreamRx(rx_stream, tube_in))
			{
				//PLOG_ERROR(LOG_PREFIX, "associate_streams -- ID(%s): customize rx fail.", (const char*)m_name);
				// достаточно вызвать очистку первого туба. остальные отцепятся по цепочке.
				// единственное то что сами объекты будут в списке тубов.
				// удаление прозойдет при полной очистки дирекшина.
				// пока так... 
				//TubeManager::clear_tube(this, tube_in);
				tube_in->release();
				continue;
			}
		}

		return tube_out;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_audio_conference_in_tube_t* AudioConferenceDirection::createInputTube(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out, int frameSize)
	{
		mg_audio_conference_in_tube_t* tube = (mg_audio_conference_in_tube_t*)createTube(
			HandlerType::ConfAudioInput,
			payload_el_in,
			payload_el_out);

		tube->update_frame_size(frameSize);

		return tube;
	}
	mg_audio_conference_out_tube_t* AudioConferenceDirection::createOutputTube(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out, int frameSize)
	{
		mg_audio_conference_out_tube_t* tube = (mg_audio_conference_out_tube_t*)createTube(
			HandlerType::ConfAudioOutput,
			payload_el_in,
			payload_el_out);

		tube->update_frame_size(frameSize);

		return tube;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	Tube* AudioConferenceDirection::buildTx(h248::TerminationType termination_type, const char* termination_id, mg_rx_stream_t* rx_stream, mg_tx_stream_t* tx_stream)
	{
		mg_media_element_list_t fill_list_rx;
		mg_media_element_list_t fill_list_tx;
		
		fillElementListTx(termination_type, termination_id, rx_stream, tx_stream, &fill_list_rx, &fill_list_tx);
		
		if ((fill_list_rx.getCount() == 0) || (fill_list_tx.getCount() == 0))
		{
			WARNING("mg_fill_media_elements_lists_tx fail.");
			return nullptr;
		}

		mg_media_element_t* element_in = fill_list_rx.get_priority();
		uint32_t mixer_frame_size = m_ptime * m_sampleRate / 1000;

		Tube* tube_in = nullptr;
		for (int index = 0; index < fill_list_tx.getCount(); index++)
		{
			mg_media_element_t* element_out = fill_list_tx.getAt(index);
			if ((element_out == nullptr) || (element_out->isNull()))
			{
				//PLOG_ERROR(LOG_PREFIX, "associate_streams -- ID(%s): elements is null.", (const char*)m_name);
				continue;
			}

			if (element_out->get_payload_id() == 13)
			{
				continue;
			}

			if (tube_in == nullptr)
			{
				if ((tube_in = createOutputTube(element_in, element_out, mixer_frame_size)) == nullptr)
				{
					WARNING("TubeManager::create_conference_out_tube fail.");
					return nullptr;
				}
			}

			Tube* tube_out = nullptr;

			PRINT_FMT("TubeManager::prepare_tubes_chain (%s %s)",
				element_in ? (const char*)element_in->get_element_name() : "(null)",
				element_out ? (const char*)element_out->get_element_name() : "(null)");

			if (!TubeManager::prepare_tubes_chain(this, element_in, element_out, &tube_in, &tube_out, true, false))
			{
				//PLOG_ERROR(LOG_PREFIX, "associate_streams -- ID(%s): prepare tubes chain fail.", (const char*)m_name);
				continue;
			}

			if ((tube_in == nullptr) || (tube_out == nullptr))
			{
				//PLOG_ERROR(LOG_PREFIX, "associate_streams -- ID(%s): bad tubes.", (const char*)m_name);
				continue;
			}

			if (!customizeStreamTx(tube_out, tx_stream))
			{
				//PLOG_ERROR(LOG_PREFIX, "associate_stream -- ID(%s): customize tx fail.", (const char*)m_name);
				//TubeManager::clear_tube(this, tube_out);
				tube_out->release();
				continue;
			}
		}

		return tube_in;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void AudioConferenceDirection::fillElementListRx(h248::TerminationType termination_type, const char* termination_id,
		mg_rx_stream_t* rx_stream, mg_tx_stream_t* tx_stream, mg_media_element_list_t* list_rx, mg_media_element_list_t* list_tx)
	{
		if ((termination_id == nullptr) || (list_rx == nullptr) || (list_tx == nullptr))
		{
			return;
		}

		list_rx->clear();
		list_tx->clear();

		uint32_t sample_rate = getMixer_frequency();

		if ((termination_type == h248::TerminationType::IVR_Player) ||
			(termination_type == h248::TerminationType::IVR_Record))
		{
			//------------------------------------------------------------------------------
			if (rx_stream != nullptr)
			{
				list_rx->create_with_copy_from(rx_stream->getMediaParams().get_media_elements()->get_priority(), 0);
			}

			//------------------------------------------------------------------------------
			mg_media_element_t* element_in = mg_create_media_element_audio_conf_in(termination_id, list_tx);
			if (element_in == nullptr)
			{
				list_tx->clear();
			}
			else
			{
				element_in->set_sample_rate(sample_rate);
				list_tx->set_priority_index(0);
			}
			//------------------------------------------------------------------------------
		}
		else
		{
			//------------------------------------------------------------------------------
			if (rx_stream != nullptr)
			{
				list_rx->copy_from(rx_stream->getMediaParams().get_media_elements());
			}
			//------------------------------------------------------------------------------
			mg_media_element_t* element_in = mg_create_media_element_audio_conf_in(termination_id, list_tx);
			if (element_in == nullptr)
			{
				list_tx->clear();
			}
			else
			{
				element_in->set_sample_rate(sample_rate);
				list_tx->set_priority_index(0);
			}
			//------------------------------------------------------------------------------
		}
	}
	void AudioConferenceDirection::fillElementListTx(h248::TerminationType termination_type, const char* termination_id,
		mg_rx_stream_t* rx_stream, mg_tx_stream_t* tx_stream, mg_media_element_list_t* list_rx, mg_media_element_list_t* list_tx)
	{
		if ((termination_id == nullptr) || (list_rx == nullptr) || (list_tx == nullptr))
		{
			return;
		}

		list_rx->clear();
		list_tx->clear();

		uint32_t sample_rate = getMixer_frequency();

		mg_media_element_t* element_out = mg_create_media_element_audio_conf_out(termination_id, list_rx);
		if (element_out == nullptr)
		{
			list_rx->clear();
		}
		else
		{
			element_out->set_sample_rate(sample_rate);
			list_rx->set_priority_index(0);
		}

		if (tx_stream != nullptr)
		{
			list_tx->create_with_copy_from(tx_stream->getMediaParams().get_media_elements()->get_priority(), 0);
		}
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
