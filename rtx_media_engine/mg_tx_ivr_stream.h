﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtp_packet.h"
#include "mg_tx_stream.h"
#include "mg_tube.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	class mg_tx_ivr_stream_t : public mg_tx_stream_t
	{
		mg_tx_ivr_stream_t(const mg_tx_ivr_stream_t&);
		mg_tx_ivr_stream_t& operator=(const mg_tx_ivr_stream_t&);

	public:
		mg_tx_ivr_stream_t(rtl::Logger* log, uint32_t id, uint32_t parent_id, const char* term_id);
		virtual ~mg_tx_ivr_stream_t();

		virtual bool mg_stream_set_LocalControl_Property(const h248::Field* LC_property);

		virtual void send(const INextDataHandler* out_handler, const rtp_packet* packet);
		virtual void send(const INextDataHandler* out_handler, const uint8_t* data, uint32_t len);
		virtual void send(const INextDataHandler* out_handler, const rtcp_packet_t* packet);

		void set_buffer_size(uint32_t buffer_size) { m_buffer_size_ms = buffer_size; }
		void set_type(const char* type) { m_type = type; }
		void set_record_path(const char* path) { m_rec_path = path; }

		bool create_recorder();

		virtual void setMediaParams(mg_sdp_media_base_t& media);

	private:
		void send_to_rtp_lock(const INextDataHandler* out_handler, const rtp_packet* packet);

		virtual int rtcp_session_tx(const rtcp_packet_t* packet) override;

	private:
		rtl::String m_rec_path;
		rtl::String m_type;
		uint32_t m_buffer_size_ms;
		media::BufferedMediaWriterRTP* m_recorder_rtp;
		media::BufferedMediaWriterRaw* m_recorder_raw;
	};
}
//------------------------------------------------------------------------------
