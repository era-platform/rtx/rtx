﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "mg_fax_t30.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define LOG_PREFIX "MGFT30"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
static int phase_b_handler_rx(t30_state_t *s, void *user_data, int result)
{
	try
	{
		if (user_data != nullptr)
		{
			mg_fax_t30_t* receiver = (mg_fax_t30_t*)user_data;
			receiver->phase_b_rx(s, result);
		}
	}
	catch (rtl::Exception ex)
	{
		if (g_mg_fax_log != nullptr)
		{
			g_mg_fax_log->log_error(LOG_PREFIX, "Unhandled exception phase_b_handler_rx -->\n%s", ex.get_message());
		}

		ex.raise_notify(__FILE__, __FUNCTION__);
	}
	return 0;
}
//------------------------------------------------------------------------------
static int phase_d_handler_rx(t30_state_t *s, void *user_data, int result)
{
	try
	{
		if (user_data)
		{
			mg_fax_t30_t* receiver = (mg_fax_t30_t*)user_data;
			receiver->phase_d_rx(s, result);
		}
	}
	catch (rtl::Exception ex)
	{
		if (g_mg_fax_log != nullptr)
		{
			g_mg_fax_log->log_error(LOG_PREFIX, "Unhandled exception phase_d_handler_rx -->\n%s", ex.get_message());
		}
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	return 0;
}
//------------------------------------------------------------------------------
static void phase_e_handler_rx(t30_state_t *s, void *user_data, int completion_code)
{
	try
	{
		if (user_data)
		{
			mg_fax_t30_t* receiver = (mg_fax_t30_t*)user_data;
			receiver->phase_e_rx(s, completion_code);
		}
	}
	catch (rtl::Exception ex)
	{
		if (g_mg_fax_log != nullptr)
		{
			g_mg_fax_log->log_error(LOG_PREFIX, "Unhandled exception phase_e_handler_rx -->\n%s", ex.get_message());
		}
		ex.raise_notify(__FILE__, __FUNCTION__);
	}
}
//------------------------------------------------------------------------------
static int phase_b_handler_tx(t30_state_t *s, void *user_data, int result)
{
	try
	{
		if (user_data != nullptr)
		{
			mg_fax_t30_t* transmitter = (mg_fax_t30_t*)user_data;
			transmitter->phase_b_tx(s, result);
		}
	}
	catch (rtl::Exception ex)
	{
		if (g_mg_fax_log != nullptr)
		{
			g_mg_fax_log->log_error(LOG_PREFIX, "Unhandled exception phase_b_handler_tx -->\n%s", ex.get_message());
		}
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	return 0;
}
//------------------------------------------------------------------------------
static int phase_d_handler_tx(t30_state_t *s, void *user_data, int result)
{
	try
	{
		if (user_data != nullptr)
		{
			mg_fax_t30_t* transmitter = (mg_fax_t30_t*)user_data;
			transmitter->phase_d_tx(s, result);
		}
	}
	catch (rtl::Exception ex)
	{
		if (g_mg_fax_log != nullptr)
		{
			g_mg_fax_log->log_error(LOG_PREFIX, "Unhandled exception phase_d_handler_tx -->\n%s", ex.get_message());
		}
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	return 0;
}
//------------------------------------------------------------------------------
static void phase_e_handler_tx(t30_state_t *s, void *user_data, int result)
{
	try
	{
		if (user_data != nullptr)
		{
			mg_fax_t30_t* transmitter = (mg_fax_t30_t*)user_data;
			transmitter->phase_e_tx(s, result);
		}
	}
	catch (rtl::Exception ex)
	{
		if (g_mg_fax_log != nullptr)
		{
			g_mg_fax_log->log_error(LOG_PREFIX, "Unhandled exception phase_e_handler_tx -->\n%s", ex.get_message());
		}
		ex.raise_notify(__FILE__, __FUNCTION__);
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool mg_initialize_fax_receiver_t30(mg_fax_t* fax, const char* file_mask)
{
	if (g_mg_fax_log != nullptr)
	{
		g_mg_fax_log->log(LOG_PREFIX, "mg_initialize_fax_receiver_t30 : creating fax receiver...");
	}

	if (fax == nullptr)
	{
		if (g_mg_fax_log != nullptr)
		{
			g_mg_fax_log->log_warning(LOG_PREFIX, "mg_initialize_fax_receiver_t30 : fax is NULL.");
		}
		return false;
	}
	mg_fax_t30_t* fax_t30 = (mg_fax_t30_t*)fax;
	fax_t30->set_type(true);

	if (fax_t30->isMetronomeStarted())
	{
		fax_t30->metronomeStop();
	}

	fax_state_t* fax_t4 = fax_t30->get_fax();
	if (fax_t4 != nullptr)
	{
		fax_free(fax_t4);
		fax_t4 = nullptr;
	}
	fax_t4 = fax_init(nullptr, FALSE);
	if (fax_t4 == nullptr)
	{
		if (g_mg_fax_log != nullptr)
		{
			g_mg_fax_log->log_warning(LOG_PREFIX, "mg_initialize_fax_receiver_t30 : fax receiver creation FAILED.");
		}
		return false;
	}

	fax_set_transmit_on_idle(fax_t4, TRUE);
	span_set_message_handler(span_message);
	span_set_error_handler(span_error);

	logging_state_t* log = fax_get_logging_state(fax_t4);

	span_log_set_message_handler(log, span_message);
	span_log_set_error_handler(log, span_error);
	span_log_set_level(log, 0xFFFF/*SPAN_LOG_SHOW_SEVERITY | SPAN_LOG_SHOW_PROTOCOL | SPAN_LOG_SHOW_VARIANT |
								  SPAN_LOG_SHOW_TAG | SPAN_LOG_DEBUG_3*/);

	t30_state_t* t30 = fax_get_t30_state(fax_t4);
	log = t30_get_logging_state(t30);

	span_log_set_message_handler(log, span_message);
	span_log_set_error_handler(log, span_error);
	span_log_set_level(log, 0xFFFF/*, SPAN_LOG_SHOW_SEVERITY | SPAN_LOG_SHOW_PROTOCOL | SPAN_LOG_SHOW_VARIANT |
								  SPAN_LOG_SHOW_TAG | SPAN_LOG_DEBUG_3*/);

	//t30_set_initial_timeout(t30, m_outgoing_timeout);

	t30_set_rx_file(t30, file_mask, -1);
	t30_set_phase_b_handler(t30, phase_b_handler_rx, fax_t30);
	t30_set_phase_d_handler(t30, phase_d_handler_rx, fax_t30);
	t30_set_phase_e_handler(t30, phase_e_handler_rx, fax_t30);

	// Default Support ALL
	t30_set_supported_modems(t30, T30_SUPPORT_V29 | T30_SUPPORT_V27TER | T30_SUPPORT_V17 | T30_SUPPORT_V34HDX | T30_SUPPORT_IAF);

	/* Support for different image sizes && resolutions*/
	t30_set_supported_image_sizes(t30, T30_SUPPORT_US_LETTER_LENGTH | T30_SUPPORT_US_LEGAL_LENGTH |
		T30_SUPPORT_UNLIMITED_LENGTH | T30_SUPPORT_215MM_WIDTH | T30_SUPPORT_255MM_WIDTH | T30_SUPPORT_303MM_WIDTH);
	t30_set_supported_resolutions(t30, T30_SUPPORT_STANDARD_RESOLUTION | T30_SUPPORT_FINE_RESOLUTION |
		T30_SUPPORT_SUPERFINE_RESOLUTION | T30_SUPPORT_R8_RESOLUTION | T30_SUPPORT_R16_RESOLUTION);

	t30_set_ecm_capability(t30, TRUE);

	t30_set_supported_compressions(t30, T30_SUPPORT_T4_1D_COMPRESSION | T30_SUPPORT_T4_2D_COMPRESSION | T30_SUPPORT_T6_COMPRESSION | T30_SUPPORT_T85_COMPRESSION);

	fax_t30->set_fax_notified(false);
	fax_t30->set_fax(fax_t4);

	if (g_mg_fax_log != nullptr)
	{
		g_mg_fax_log->log(LOG_PREFIX, "mg_initialize_fax_receiver_t30 : fax receiver created.");
	}

	return fax_t30->metronomeStart(20, false);
}
//------------------------------------------------------------------------------
bool mg_initialize_fax_transmitter_t30(mg_fax_t* fax, const char* file_name, bool ecm)
{
	if (g_mg_fax_log != nullptr)
	{
		g_mg_fax_log->log(LOG_PREFIX, "mg_initialize_fax_transmitter_t30 : creating fax transmitter...");
	}

	if (fax == nullptr)
	{
		if (g_mg_fax_log != nullptr)
		{
			g_mg_fax_log->log_warning(LOG_PREFIX, "mg_initialize_fax_transmitter_t30 : fax is NULL.");
		}
		return false;
	}
	mg_fax_t30_t* fax_t30 = (mg_fax_t30_t*)fax;

	fax_t30->set_type(false);

	if (fax_t30->isMetronomeStarted())
	{
		fax_t30->metronomeStop();
	}

	span_set_message_handler(span_message);
	span_set_error_handler(span_error);

	/* make sure they are initialized to zero */
	fax_state_t* fax_t4 = fax_t30->get_fax();
	if (fax_t4 != nullptr)
	{
		fax_free(fax_t4);
		fax_t4 = nullptr;
	}
	fax_t4 = fax_init(nullptr, TRUE);
	
	if (fax_t4 == nullptr)
	{
		if (g_mg_fax_log != nullptr)
		{
			g_mg_fax_log->log_warning(LOG_PREFIX, "mg_initialize_fax_transmitter_t30 : fax transmitter creation FAILED.");
		}
		return false;
	}

	fax_set_transmit_on_idle(fax_t4, TRUE);

	logging_state_t* log = fax_get_logging_state(fax_t4);

	span_log_set_message_handler(log, span_message);
	span_log_set_error_handler(log, span_error);
	span_log_set_level(log, 0xFFFF/*SPAN_LOG_SHOW_SEVERITY | SPAN_LOG_SHOW_PROTOCOL | SPAN_LOG_SHOW_VARIANT |
								  SPAN_LOG_SHOW_TAG | SPAN_LOG_DEBUG_3*/); //, SPAN_LOG_SHOW_SEVERITY | SPAN_LOG_SHOW_PROTOCOL | SPAN_LOG_FLOW);

	t30_state_t* t30 = fax_get_t30_state(fax_t4);
	log = t30_get_logging_state(t30);

	span_log_set_message_handler(log, span_message);
	span_log_set_error_handler(log, span_error);
	span_log_set_level(log, 0xffff/*SPAN_LOG_SHOW_SEVERITY | SPAN_LOG_SHOW_PROTOCOL | SPAN_LOG_SHOW_VARIANT |
								  SPAN_LOG_SHOW_TAG | SPAN_LOG_DEBUG_3*/); //, SPAN_LOG_SHOW_SEVERITY | SPAN_LOG_SHOW_PROTOCOL | SPAN_LOG_FLOW);


	//t30_set_initial_timeout(t30, m_outgoing_timeout);
	t30_set_tx_file(t30, file_name, -1, -1);
	t30_set_phase_b_handler(t30, phase_b_handler_tx, fax_t30);
	t30_set_phase_d_handler(t30, phase_d_handler_tx, fax_t30);
	t30_set_phase_e_handler(t30, phase_e_handler_tx, fax_t30);

	// Default Support ALL
	t30_set_supported_modems(t30, T30_SUPPORT_V29 | T30_SUPPORT_V27TER | T30_SUPPORT_V17 | T30_SUPPORT_V34HDX | T30_SUPPORT_IAF);

	/* Support for different image sizes && resolutions*/
	t30_set_supported_image_sizes(t30,
		T30_SUPPORT_US_LETTER_LENGTH |
		T30_SUPPORT_US_LEGAL_LENGTH |
		T30_SUPPORT_UNLIMITED_LENGTH |
		T30_SUPPORT_215MM_WIDTH |
		T30_SUPPORT_255MM_WIDTH |
		T30_SUPPORT_303MM_WIDTH);

	t30_set_supported_resolutions(t30,
		T30_SUPPORT_STANDARD_RESOLUTION |
		T30_SUPPORT_FINE_RESOLUTION |
		T30_SUPPORT_SUPERFINE_RESOLUTION |
		T30_SUPPORT_R8_RESOLUTION |
		T30_SUPPORT_R16_RESOLUTION);

	t30_set_ecm_capability(t30, ecm);

	t30_set_supported_compressions(t30, T30_SUPPORT_T4_1D_COMPRESSION | T30_SUPPORT_T4_2D_COMPRESSION | T30_SUPPORT_T6_COMPRESSION);

	if (g_mg_fax_log != nullptr)
	{
		g_mg_fax_log->log(LOG_PREFIX, "mg_initialize_fax_transmitter_t30 : fax transmitter created.");
	}

	fax_t30->set_fax_notified(false);
	fax_t30->set_fax(fax_t4);

	return fax_t30->metronomeStart(20, false);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
mg_fax_t30_t::mg_fax_t30_t(rtl::Logger* log)
	: mg_fax_t(log), m_log(log)
{
	m_pages_received = 0;
	m_pages_transmitted = 0;

	m_fax_t4 = nullptr;

	m_receiver = true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
mg_fax_t30_t::~mg_fax_t30_t()
{
	destroy();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mg_fax_t30_t::destroy()
{
	if (!fax_notified())
	{
		fax_signal(FAX_SIGNAL_DESTROED);

		set_fax_notified(true);
	}

	if (m_fax_t4 != nullptr)
	{
		fax_free(m_fax_t4);
		m_fax_t4 = nullptr;
	}

	PLOG_FAX(LOG_PREFIX, "destroy : fax %s destroyed.", (m_receiver) ? "RX" : "TX");
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mg_fax_t30_t::set_type(bool receiver)
{
	m_receiver = receiver;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
FAX_SIGNALS mg_fax_t30_t::get_fax_signal_state(bool stop_signaling)
{
	FAX_SIGNALS signal = FAX_SIGNAL_OFF;

	if (!fax_notified())
	{
		bool error = (m_receiver) ? m_pages_received == 0 : m_pages_transmitted == 0;
		signal = (error) ? FAX_SIGNAL_RX_ERROR : FAX_SIGNAL_RX_DONE;

		if (stop_signaling)
		{
			set_fax_notified(true);
		}
	}

	return signal;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mg_fax_t30_t::phase_b_rx(t30_state_t *s, int result)
{
	PLOG_FAX(LOG_PREFIX, "rx : phase B mark.");
}
//------------------------------------------------------------------------------
void mg_fax_t30_t::phase_d_rx(t30_state_t *s, int result)
{
	t30_stats_t t;

	if (result)
	{
		t30_get_transfer_statistics(s, &t);

		const char* fmt = "rx : phase D statistics :\n\
							  \t\t\t+--------------------------------------\n\
							  \t\t\t| Result..............%s(%d)\n\
							  \t\t\t| Current status......%s(%d)\n\
							  \t\t\t| Bit rate............%d\n\
							  \t\t\t| Pages received......%d\n\
							  \t\t\t| Pages in file.......%d\n\
							  \t\t\t| Image resolution....%d x %d\n\
							  \t\t\t| Image size..........%d x %d\n\
							  \t\t\t| Image length........%d\n\
							  \t\t\t| Encoding............%s(%d)\n\
							  \t\t\t| ECM.................%s\n\
							  \t\t\t| ECM retries.........%d\n\
							  \t\t\t| Bad rows............%d\n\
							  \t\t\t| Longest bad row.....%d\n\
							  \t\t\t+--------------------------------------";
							  /*\t\t\t| RTP events..........%d\n\
							  \t\t\t| RTN events..........%d\n\
							  \t\t\t+--------------------------------------";*/
		PLOG_FAX(LOG_PREFIX, fmt,
			t30_frametype(result), result,
			t30_completion_code_to_str(t.current_status), t.current_status,
			t.bit_rate,
			t.pages_rx,
			t.pages_in_file,
			t.x_resolution, t.y_resolution,
			t.width, t.length,
			t.image_size,
			t4_encoding_to_str(t.encoding), t.encoding,
			t.error_correcting_mode ? "ON" : "OFF",
			t.error_correcting_mode_retries,
			t.bad_rows,
			t.longest_bad_row_run);
			//t.rtp_events,
			//t.rtn_events);

		m_pages_received = t.pages_rx;
	}
	else
	{
		PLOG_FAX(LOG_PREFIX, "rx : phase D\n\
							  \t\t\t+--------------------------------------\n\
							  \t\t\t| unknown result (%d)\n\
							  \t\t\t+--------------------------------------", result);
	}
}
//------------------------------------------------------------------------------
void mg_fax_t30_t::phase_e_rx(t30_state_t *s, int completion_code)
{
	const char* local_ident;
	const char* far_ident;

	t30_stats_t t;

	t30_get_transfer_statistics(s, &t);
	local_ident = t30_get_tx_ident(s);
	far_ident = t30_get_rx_ident(s);

	set_fax_notified(true);

	if (completion_code == T30_ERR_OK || m_pages_received > 0)
	{
		PLOG_FAX(LOG_PREFIX, "rx : phase E\n\
							  \t\t\t+--------------------------------------\n\
							  \t\t\t| Fax successfully received (%d pages).\n\
							  \t\t\t| Remote station id: %s\n\
							  \t\t\t| Local station id:  %s\n\
							  \t\t\t+--------------------------------------", m_pages_received, far_ident, local_ident);

		fax_signal(FAX_SIGNAL_RX_DONE);
	}
	else
	{
		PLOG_FAX(LOG_PREFIX, "rx: phase E\n\
							  \t\t\t+--------------------------------------\n\
							  \t\t\t| Fax receive not successful : %s (%d)\n\
							  \t\t\t+--------------------------------------", 
							  t30_completion_code_to_str(completion_code), completion_code);

		fax_signal(FAX_SIGNAL_RX_ERROR);
	}
}
//------------------------------------------------------------------------------
void mg_fax_t30_t::phase_b_tx(t30_state_t *s, int result)
{
	PLOG_FAX(LOG_PREFIX, "tx : phase B mark.");
}
//------------------------------------------------------------------------------
void mg_fax_t30_t::phase_d_tx(t30_state_t *s, int result)
{
	t30_stats_t t;

	t30_get_transfer_statistics(s, &t);

	const char* fmt = "tx : phase D statistics:\n\
						  \t\t\t+--------------------------------------\n\
						  \t\t\t| Result..............%s(%d)\n\
						  \t\t\t| Current status......%s(%d)\n\
						  \t\t\t| Bit rate............%d\n\
						  \t\t\t| Pages transfered....%d\n\
						  \t\t\t| Pages in file.......%d\n\
						  \t\t\t| Image resolution....%d x %d\n\
						  \t\t\t| Image size..........%d x %d\n\
						  \t\t\t| Image length........%d\n\
						  \t\t\t| Encoding............%s(%d)\n\
						  \t\t\t| ECM.................%s\n\
						  \t\t\t| ECM retries.........%d\n\
						  \t\t\t| Bad rows............%d\n\
						  \t\t\t| Longest bad row.....%d\n\
						  \t\t\t+--------------------------------------";
						 /* \t\t\t| RTP events..........%d\n\
						  \t\t\t| RTN events..........%d\n\
						  \t\t\t+--------------------------------------";*/

	PLOG_FAX(LOG_PREFIX, fmt,
		t30_frametype(result), result,
		t30_completion_code_to_str(t.current_status), t.current_status,
		t.bit_rate,
		t.pages_tx,
		t.pages_in_file,
		t.x_resolution, t.y_resolution,
		t.width, t.length,
		t.image_size,
		t4_encoding_to_str(t.encoding), t.encoding,
		t.error_correcting_mode ? "ON" : "OFF",
		t.error_correcting_mode_retries,
		t.bad_rows,
		t.longest_bad_row_run/*,
		t.rtp_events,
		t.rtn_events*/);

	m_pages_transmitted = t.pages_tx;
}
//------------------------------------------------------------------------------
void mg_fax_t30_t::phase_e_tx(t30_state_t *s, int completion_code)
{
	const char* local_ident;
	const char* far_ident;

	local_ident = t30_get_tx_ident(s);
	far_ident = t30_get_rx_ident(s);

	set_fax_notified(true);

	if (completion_code == T30_ERR_OK || m_pages_transmitted > 0)
	{
		PLOG_FAX(LOG_PREFIX, "tx : phase E\n\
							  \t\t\t+--------------------------------------\n\
							  \t\t\t| Fax successfully sent (%d pages)\n\
							  \t\t\t| Remote station id: %s\n\
							  \t\t\t| Local station id:  %s\n\
							  \t\t\t+--------------------------------------", m_pages_transmitted, far_ident, local_ident);

		fax_signal(FAX_SIGNAL_TX_DONE);
	}
	else
	{
		PLOG_FAX(LOG_PREFIX, "tx : phase E\n\
							  \t\t\t+--------------------------------------\n\
							  \t\t\t| Fax sening error : %s (%d).\n\
							  \t\t\t+--------------------------------------", t30_completion_code_to_str(completion_code), completion_code);

		fax_signal(FAX_SIGNAL_TX_ERROR);
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t mg_fax_t30_t::write_fax(const uint8_t* data, uint32_t length)
{
	if (data == nullptr)
	{
		PLOG_WARNING(LOG_PREFIX, "write_fax : data is null.");
		return 0;
	}

	if (!lock())
	{
		return 0;
	}

	fax_state_t* fax_t4 = get_fax();
	if (fax_t4 == nullptr)
	{
		PLOG_WARNING(LOG_PREFIX, "write_fax : fax state is null.");
		unlock();
		return 0;
	}

	short* samples = (short*)data;
	int count = length / sizeof(short);

	fax_rx(fax_t4, samples, count);

	unlock();

	//PLOG_FAX(LOG_PREFIX, "write_fax : write data fax (%d samples).", count);
	return count * sizeof(short);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t mg_fax_t30_t::read_fax(uint8_t* data, uint32_t length)
{
	if (data == nullptr)
	{
		PLOG_WARNING(LOG_PREFIX, "read_fax : data is null.");
		return 0;
	}

	if (!lock())
	{
		return 0;
	}

	fax_state_t* fax_t4 = get_fax();
	if (fax_t4 == nullptr)
	{
		PLOG_WARNING(LOG_PREFIX, "read_fax : fax state is null.");
		unlock();
		return 0;
	}

	short* samples = (short*)data;
	int count = length / sizeof(short);

	int tx = fax_tx(fax_t4, samples, count);

	unlock();

	//PLOG_FAX(LOG_PREFIX, "read_fax : read fax data (%d samples).", tx);
	return uint32_t(tx * sizeof(short));
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
fax_state_t* mg_fax_t30_t::get_fax()
{
	return m_fax_t4;
}
//------------------------------------------------------------------------------
void mg_fax_t30_t::set_fax(fax_state_t* fax)
{
	m_fax_t4 = fax;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
