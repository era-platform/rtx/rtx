﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#pragma once

#include "mg_rx_stream.h"
#include "mmt_audio_player.h"

namespace mge
{
	//------------------------------------------------------------------------------
	// Однонаправленный поток в тубы для работы с ivr.
	//------------------------------------------------------------------------------
	class mg_rx_ivr_stream_t : public mg_rx_stream_t, media::IPlayerEventHandler
	{
	public:
		mg_rx_ivr_stream_t(rtl::Logger* log, uint32_t id, const char* parent_id, MediaSession& session);
		virtual ~mg_rx_ivr_stream_t();

		virtual bool lock();
		virtual void unlock();

		virtual void setMediaParams(mg_sdp_media_base_t& media) override;

		//----------------------------------------------------
		bool create_player();
		void destroy_player();
		void pause();
		void resume();
		void set_start_at(int start_at);
		void set_stop_at(int stop_at);
		void set_loop(bool loop);
		void set_random(bool random);
		bool add_file(const char* file_path);
		bool add_dir(const char* dir);
		void set_volume(int volume);
		//----------------------------------------------------

	private:
		void change_timestamp_step(uint32_t timestamp_step);

		virtual int rtcp_session_rx(const rtcp_packet_t* packet);
		/// <media::IPlayerEventHandler>
		virtual void eventDataReady(const char* data, int data_length);
		virtual void eventEndPlay(media::PlayerReport* report);

	private:
		// переменные для работы с пакетами на отправку.
		uint32_t m_ssrc;
		uint16_t m_seq_num;			// порядковый номер пакета для отправки в ртп канал.
		uint16_t m_seq_num_last;		// номер последнего отправленного пакета в ртп канал.
		uint32_t m_timestamp;		// таймштамп пакета для отправки в ртп канал.
		uint32_t m_timestamp_last;	// таймштамп последнего отправленного пакета в ртп канал.
		uint32_t m_samples_last;		// последнее количество семплов которое было отправлено.
		uint32_t m_timestamp_step;
		std::atomic_flag m_rx_lock;
		// переменные для работы с плееров.
		media::AudioPlayer* m_player;

	};
}
//------------------------------------------------------------------------------
