﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "mg_direction.h"
#include "mg_audio_analyzer.h"
#include "mg_audio_conference_ctrl.h"

namespace mge
{

	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	class AudioConferenceDirection : public Direction
	{
		AudioConferenceDirection(const AudioConferenceDirection&);
		AudioConferenceDirection& operator=(const AudioConferenceDirection&);

		friend class TubeManager;

	public:
		AudioConferenceDirection(MediaContext* owner, uint32_t id, const char* parent_id);
		virtual	~AudioConferenceDirection();

		uint32_t getMixer_frequency() { return m_sampleRate; }
		uint32_t getMixer_ptime() { return m_ptime; }

		virtual bool initialize() override;
		virtual bool addTermination(Termination* term) override;
		virtual void updateTopology(const rtl::ArrayT<h248::TopologyTriple>& triple_list) override;

	private:
		virtual int checkTermination(Termination* term) override;

		// построить маршрут в сторону микшера.
		Tube* buildRx(h248::TerminationType termination_type, const char* termination_id, mg_rx_stream_t* rx_stream, mg_tx_stream_t* tx_stream);
		// построить маршрут от микшера.
		Tube* buildTx(h248::TerminationType termination_type, const char* termination_id, mg_rx_stream_t* rx_stream, mg_tx_stream_t* tx_stream);

		void fillElementListRx(h248::TerminationType termination_type, const char* termination_id,
			mg_rx_stream_t* rx_stream, mg_tx_stream_t* tx_stream, mg_media_element_list_t* list_rx, mg_media_element_list_t* list_tx);
		void fillElementListTx(h248::TerminationType termination_type, const char* termination_id,
			mg_rx_stream_t* rx_stream, mg_tx_stream_t* tx_stream, mg_media_element_list_t* list_rx, mg_media_element_list_t* list_tx);
		//...
		bool searchTopologyTubes(const char* termIdFrom, mg_audio_conference_out_tube_t*& out, const char* termIdTo, mg_audio_conference_out_tube_t*& in);

		mg_audio_conference_in_tube_t* createInputTube(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out, int frameSize);
		mg_audio_conference_out_tube_t* createOutputTube(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out, int frameSize);

	private:
		AudioConferenceController* m_ctrl;
		AudioAnalyzer* m_audioAnalyzer;	// анализ входящего звука для видео конференций
		uint32_t m_sampleRate;				// Частота конференции. Все терминаторы будут обрабатываться с этой частотой.
		uint32_t m_ptime;					// Период отправки пакетов.
	};
}
//------------------------------------------------------------------------------
