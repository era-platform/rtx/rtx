﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_pcm_to_codec_tube.h"
#include "mg_media_element.h"
#include "rtp_packet.h"

#include "logdef_tube.h"


namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_pcm_to_codec_tube_t::mg_pcm_to_codec_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id) :
		Tube(HandlerType::AudioEncoder, id, log)
	{
		makeName(parent_id, "-pcmc-tu"); // PCM to Codec

		m_encoder = nullptr;

		rtl::res_counter_t::add_ref(g_mge_tube_counter);

		setLogTag("TUB-ACOD");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_pcm_to_codec_tube_t::~mg_pcm_to_codec_tube_t()
	{
		unsubscribe(this);

		if (m_encoder != nullptr)
		{
			rtx_codec__release_audio_encoder(m_encoder);
			m_encoder = nullptr;
		}

		rtl::res_counter_t::release(g_mge_tube_counter);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_pcm_to_codec_tube_t::init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out)
	{
		if (payload_el_out == nullptr)
		{
			ERROR_MSG("payload element is null!");
			return false;
		}
		m_payload_id = payload_el_out->get_payload_id();

		media::PayloadFormat format;

		format.setEncoding(payload_el_out->get_element_name());
		format.setFrequency(payload_el_out->get_sample_rate());
		format.setChannelCount(payload_el_out->getChannelCount());
		format.setFmtp(payload_el_out->get_param_fmtp());
		format.setId((media::PayloadId)payload_el_out->get_payload_id());

		PRINT_FMT("payload out: %s %d", (const char*)format.getEncoding(), m_payload_id);

		m_encoder = rtx_codec__create_audio_encoder(format.getEncoding(), &format);

		if (m_encoder == nullptr)
		{
			ERROR_MSG("failed to create audio encoder!");
			return false;
		}

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_pcm_to_codec_tube_t::is_suited(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out)
	{
		if (payload_el_out == nullptr)
		{
			return false;
		}
		return m_payload_id == payload_el_out->get_payload_id();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_pcm_to_codec_tube_t::send(const INextDataHandler* out_handler, const rtp_packet* packet)
	{
		if (packet == nullptr)
		{
			ERROR_MSG("nothing to send!");
			return;
		}

		if (m_encoder == nullptr)
		{
			ERROR_MSG("encoder is null!");
			return;
		}

		uint32_t payload_length = packet->get_payload_length();
		const uint8_t* payload = packet->get_payload();
		if (payload_length == 0)
		{
			ERROR_MSG("payload length is zero.");
			return;
		}

		uint32_t frame_size = m_encoder->get_frame_size();
		if (payload_length != frame_size)
		{
			ERROR_MSG("payload length must be equal frame size.");
			return;
		}

		const uint32_t codec_buff_size = rtp_packet::PAYLOAD_BUFFER_SIZE;
		uint8_t codec_buff[codec_buff_size] = { 0 };

		uint32_t encode_size = m_encoder->encode(payload, payload_length, codec_buff, codec_buff_size);

		rtp_packet packet_new;
		packet_new.copy_from(packet);

		if (!m_encoder->packetize(&packet_new, codec_buff, encode_size))
		{
			ERROR_MSG("packetizer failed!");
			return;
		}

		INextDataHandler* out = getNextHandler();
		if (out != nullptr)
		{
			out->send(this, &packet_new);
		}
	}
}
//------------------------------------------------------------------------------
