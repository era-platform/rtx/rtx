﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_resampler_tube.h"
#include "mg_media_element.h"
#include "rtp_packet.h"

#include "logdef_tube.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_resampler_tube_t::mg_resampler_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id) :
		Tube(HandlerType::AudioResampler, id, log)
	{
		m_resampler = nullptr;

		makeName(parent_id, "-res-tu"); // Resampler Tube

		m_koef = 0;
		m_rising = true;

		rtl::res_counter_t::add_ref(g_mge_tube_counter);

		setLogTag("TUB-RES");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_resampler_tube_t::~mg_resampler_tube_t()
	{
		unsubscribe(this);

		if (m_resampler != nullptr)
		{
			media::destroyResampler(m_resampler);
			m_resampler = nullptr;
		}

		m_koef = 0;
		m_rising = true;

		rtl::res_counter_t::release(g_mge_tube_counter);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_resampler_tube_t::init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out)
	{
		if (payload_el_in == nullptr || payload_el_out == nullptr)
		{
			ERROR_MSG("Bad parameters : payloads is undefined!");
			return false;
		}
		m_payload_id = payload_el_in->get_payload_id();

		int rate_in = payload_el_in->get_sample_rate();
		int chan_in = payload_el_in->getChannelCount();
		int rate_out = payload_el_out->get_sample_rate();
		int chan_out = payload_el_out->getChannelCount();

		PRINT_FMT("payload : %s %d --> %s %d",
			payload_el_in->get_element_name(),
			m_payload_id,
			payload_el_out->get_element_name(),
			payload_el_out->get_payload_id()
			);

		return create_resampler(rate_in, chan_in, rate_out, chan_out);
	}
	//------------------------------------------------------------------------------
	bool mg_resampler_tube_t::create_resampler(int rate_in, int chan_in, int rate_out, int chan_out)
	{
		if (rate_in < rate_out)
		{
			m_koef = rate_out / rate_in;
			m_rising = true;
		}
		else
		{
			m_koef = rate_in / rate_out;
			m_rising = false;
		}

		m_resampler = media::createResampler(rate_in, chan_in, rate_out, chan_out);
		if (m_resampler == nullptr)
		{
			ERROR_MSG("failed to create audio resampler!");
			return false;
		}

		PRINT_FMT("sample rate in: %d, sample rate out: %d", rate_in, rate_out);
		
		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_resampler_tube_t::send(const INextDataHandler* out_handler, const rtp_packet* packet)
	{
		ENTER_FMT("payload:%d", packet->get_payload_type());

		if (packet == nullptr)
		{
			ERROR_MSG("nothing to send");
			return;
		}

		if (m_resampler == nullptr)
		{
			ERROR_MSG("resampler is null.");
			return;
		}

		const uint8_t* buff = packet->get_payload();
		uint32_t payload_length = packet->get_payload_length();
		if (payload_length == 0)
		{
			ERROR_MSG("depacketize fail.");
			return;
		}

		const uint32_t resample_buff_size = rtp_packet::PAYLOAD_BUFFER_SIZE / 2;
		short resample_buff[resample_buff_size] = { 0 };
		uint32_t size_in_samples_in = payload_length / m_koef;
		uint32_t size_in_samples_out = (m_rising) ? size_in_samples_in * m_koef : size_in_samples_in / m_koef;
		if (resample_buff_size < size_in_samples_out)
		{
			ERROR_MSG("small resample buffer.");
			return;
		}

		int used = 0;
		int resample_size = m_resampler->resample((const short*)buff, size_in_samples_in, &used, resample_buff, resample_buff_size);
		if (resample_size <= 0)
		{
			ERROR_MSG("resample fail");
			return;
		}

		rtp_packet packet_new;
		packet_new.copy_from(packet);
		packet_new.set_payload_type((uint8_t)m_payload_id);
		uint32_t read_size = packet_new.set_payload((uint8_t*)resample_buff, resample_size * 2); // resample_size в семплах
		if (read_size == 0)
		{
			ERROR_MSG("read packet fail");
			return;
		}

		INextDataHandler* out = getNextHandler();
		if (out != nullptr)
		{
			out->send(this, &packet_new);
		}
	}
}
//------------------------------------------------------------------------------
