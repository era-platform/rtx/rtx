﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtp_packet.h"
#include "mg_tube.h"
#include "mg_tx_stream.h"
#include "mg_rx_stream.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	extern rtl::Logger* g_mg_stream_log;
	extern rtl::Logger* g_mg_statistics_log;
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
#define STREAM_LOGGING  m_log	// && rtl::Logger::check_trace(TRF_FLAG7) потому что проверяется при создании файла лога,
								// если файла лога нет, то и писать некуда.
#define PLOG_STREAM_WRITE		if (STREAM_LOGGING && rtl::Logger::check_trace(TRF_STREAM | TRF_CALL))	m_log->log
#define PLOG_STREAM_WARNING		if (STREAM_LOGGING && rtl::Logger::check_trace(TRF_STREAM | TRF_WARNING)) m_log->log_warning
#define PLOG_STREAM_ERROR		if (STREAM_LOGGING && rtl::Logger::check_trace(TRF_STREAM | TRF_ERRORS)) m_log->log_error

	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	class mg_rx_stream_t;
	class mg_tx_stream_t;
	class Termination;


	//------------------------------------------------------------------------------
	// Класс контейнер для однопоточных стримов. 
	// Обычно содержит два однопоточных стрима mg_rx_stream_t и mg_tx_stream_t.
	//------------------------------------------------------------------------------
	class MediaSession
	{
	public:
		MediaSession(rtl::Logger* log, uint32_t id, const char* parent_id, Termination* termination);
		virtual	~MediaSession();

		static bool	vad_isActive(const mg_rx_stream_t* rx_stream, mg_splitter_params_t* p);

		bool mg_stream_set_LocalControl_Mode(h248::TerminationStreamMode mode);
		bool mg_stream_set_LocalControl_ReserveValue(bool value);
		bool mg_stream_set_LocalControl_ReserveGroup(bool value);
		bool mg_stream_set_LocalControl_Property(const h248::Field* LC_property);

		void mg_stream__event(h248::EventParams* eventData);

		const uint32_t getId() const;
		const char* getName() const;
		const char* getTag() const { return "SESS"; }

		// Блокировка всех направлений.
		bool lock();
		// Разблокировка всех направлений.
		void unlock();

		// Проверка готовности стрима.
		bool isReady();
		// Очищаем стримы. Не удаляем.
		// Просто сбрасываем все связи с текущим стримом.
		void clear();

		// Создания направления из канала в туб.
		// Подписываемся на канал.
		bool create_rx_stream(mg_rx_stream_type_t type);
		const rtl::ArrayT<mg_rx_stream_t*>& get_rx_streams() { return m_rx_streams; }
		mg_rx_stream_t* get_rx_stream_at(int index) { return m_rx_streams[index]; }
		mg_rx_stream_t* get_rx_stream_first() { return m_rx_streams.getAt(0); }  // first stream
		//mg_rx_stream_t* get_rx_stream() { return m_rx_streams.getAt(0); }  // first stream
		void destroy_rx_streams();

		// Создания направления из туба в канал.
		// Передаем на какой адрес отправлять пакеты.
		bool create_tx_stream(mg_tx_stream_type_t type);
		const rtl::ArrayT<mg_tx_stream_t*>& get_tx_streams() { return m_tx_streams; }
		mg_tx_stream_t* get_tx_stream_at(int index) { return m_tx_streams[index]; }
		mg_tx_stream_t* get_tx_stream() { return m_tx_streams.getAt(0); }  // first stream
		void destroy_tx_streams();

		//-----------------------------------------------------------------------
		// методы взаимодействия входящего и исходящего стримов

		void reset_first_packet_recive_flag() { m_fist_packet_recive = false; }
		void check_address_from(const socket_address* sockaddr);
		void set_remote_address(const socket_address* sockaddr);
		void set_remote_rtcp_address(const socket_address* sockaddr);

		//-----------------------------------------------------------------------

		bool start_rtcp_monitors();
		bool stop_rtcp_monitors();
		bool create_rtcp_monitor(const char* addr, mg_rx_stream_t* rx_stream, mg_tx_stream_t* tx_stream, const char* cname_tx, bool webrtc);
		bool update_rtcp_monitor(uint32_t ssrc_rx_old, mg_rx_stream_t* rx_stream, mg_tx_stream_t* tx_stream);

		//-----------------------------------------------------------------------
		bool add_statistics_in_streams(const char* addr, mg_rx_stream_t* rx_stream, mg_tx_stream_t* tx_stream);

		rtl::ArrayT<rtcp_monitor_t*>* get_rtcp_monitors() { return &m_rtcp_monitor_list; }

		void raiseRecevierEvent(int eventId, int param);

	private:
		void update_rx_LocalControl_Mode(mg_rx_stream_t* rx_stream, h248::TerminationStreamMode mode);
		void update_tx_LocalControl_Mode(mg_tx_stream_t* tx_stream, h248::TerminationStreamMode mode);

		void subscribe_novoice_manager();
		void subscribe_novoice_manager(mg_rx_stream_t* rx_stream);
		void unsubscribe_novoice_manager();
		void unsubscribe_novoice_manager(mg_rx_stream_t* rx_stream);
		void clean(mg_rx_stream_t* rx_stream);

	private:
		rtl::Logger* m_log;
		uint32_t m_id;
		rtl::String m_name;
		rtl::ArrayT<mg_tx_stream_t*> m_tx_streams; // поток для отправки в канал.
		rtl::ArrayT<mg_rx_stream_t*> m_rx_streams; // поток для отправки в туб.
		bool m_fist_packet_recive;
		Termination* m_termination;
		// структура контроля - описание как ведет себя стрим, в каком он состоянии.. и т.д.
		volatile h248::TerminationLocalControl m_local_control;
		// предыдущая структура контроля к которой можно вернуться.
		volatile h248::TerminationLocalControl m_local_control_old;
		rtl::ArrayT<rtcp_monitor_t*> m_rtcp_monitor_list;
		rtl::ArrayT<rtp_statistics_t*> m_rtp_stat_list;
	};
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	typedef rtl::ArrayT<MediaSession*> MediaSessionList;
}
//------------------------------------------------------------------------------
