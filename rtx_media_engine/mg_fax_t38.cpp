﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "mg_fax_t38.h"
//#include "mg_udptl_rx_tube.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define LOG_PREFIX "MGFT38"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
static int mg_fax_transmitter_packet_handler_t38(t38_core_state_t *s, void *user_data, const uint8_t *buf, int len, int transmissions)
{
	if (user_data == nullptr)
	{
		return 0;
	}

	//int result = 0;
	mg_fax_t38_t* fax = (mg_fax_t38_t*)user_data;

	if (fax != nullptr)
	{
		//result =
		fax->t38_packet_tx(s, buf, len, transmissions);
	}

	return 0;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
static int phase_b_handler_rx(t30_state_t *s, void *user_data, int result)
{
	try
	{
		if (user_data != nullptr)
		{
			mg_fax_t38_t* receiver = (mg_fax_t38_t*)user_data;
			receiver->phase_b_rx(s, result);
		}
	}
	catch (rtl::Exception ex)
	{
		if (g_mg_fax_log != nullptr)
		{
			g_mg_fax_log->log_error(LOG_PREFIX, "Unhandled exception phase_b_handler_rx -->\n%s", ex.get_message());
		}

		ex.raise_notify(__FILE__, __FUNCTION__);
	}
	return 0;
}
//------------------------------------------------------------------------------
static int phase_d_handler_rx(t30_state_t *s, void *user_data, int result)
{
	try
	{
		if (user_data)
		{
			mg_fax_t38_t* receiver = (mg_fax_t38_t*)user_data;
			receiver->phase_d_rx(s, result);
		}
	}
	catch (rtl::Exception ex)
	{
		if (g_mg_fax_log != nullptr)
		{
			g_mg_fax_log->log_error(LOG_PREFIX, "Unhandled exception phase_d_handler_rx -->\n%s", ex.get_message());
		}
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	return 0;
}
//------------------------------------------------------------------------------
static void phase_e_handler_rx(t30_state_t *s, void *user_data, int completion_code)
{
	try
	{
		if (user_data)
		{
			mg_fax_t38_t* receiver = (mg_fax_t38_t*)user_data;
			receiver->phase_e_rx(s, completion_code);
		}
	}
	catch (rtl::Exception ex)
	{
		if (g_mg_fax_log != nullptr)
		{
			g_mg_fax_log->log_error(LOG_PREFIX, "Unhandled exception phase_e_handler_rx -->\n%s", ex.get_message());
		}
		ex.raise_notify(__FILE__, __FUNCTION__);
	}
}
//------------------------------------------------------------------------------
static int phase_b_handler_tx(t30_state_t *s, void *user_data, int result)
{
	try
	{
		if (user_data != nullptr)
		{
			mg_fax_t38_t* transmitter = (mg_fax_t38_t*)user_data;
			transmitter->phase_b_tx(s, result);
		}
	}
	catch (rtl::Exception ex)
	{
		if (g_mg_fax_log != nullptr)
		{
			g_mg_fax_log->log_error(LOG_PREFIX, "Unhandled exception phase_b_handler_tx -->\n%s", ex.get_message());
		}
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	return 0;
}
//------------------------------------------------------------------------------
static int phase_d_handler_tx(t30_state_t *s, void *user_data, int result)
{
	try
	{
		if (user_data != nullptr)
		{
			mg_fax_t38_t* transmitter = (mg_fax_t38_t*)user_data;
			transmitter->phase_d_tx(s, result);
		}
	}
	catch (rtl::Exception ex)
	{
		if (g_mg_fax_log != nullptr)
		{
			g_mg_fax_log->log_error(LOG_PREFIX, "Unhandled exception phase_d_handler_tx -->\n%s", ex.get_message());
		}
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	return 0;
}
//------------------------------------------------------------------------------
static void phase_e_handler_tx(t30_state_t *s, void *user_data, int result)
{
	try
	{
		if (user_data != nullptr)
		{
			mg_fax_t38_t* transmitter = (mg_fax_t38_t*)user_data;
			transmitter->phase_e_tx(s, result);
		}
	}
	catch (rtl::Exception ex)
	{
		if (g_mg_fax_log != nullptr)
		{
			g_mg_fax_log->log_error(LOG_PREFIX, "Unhandled exception phase_e_handler_tx -->\n%s", ex.get_message());
		}
		ex.raise_notify(__FILE__, __FUNCTION__);
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mg_fax_t38_set_default_params(mg_fax_t38_params_t* params)
{
	if (params == nullptr)
	{
		return;
	}

	params->version = 0;
	params->max_bit_rate = 9600;
	params->fill_bit_removal = false;
	params->transcoding_mmr = false;
	params->transcoding_jbig = false;
	params->rate_management = T38_TCF_TRANSFERRED;
	params->max_buffer = 1024;
	params->max_datagram = 320;
	params->vendor_info = rtl::String::empty;
	params->udp_ec = 1;// T38_ECM_NONE;
	params->ecm_entries = 5;
	params->ecm_span = 1;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool mg_initialize_fax_receiver_t38(mg_fax_t* fax, const char* file_mask)
{
	if (g_mg_fax_log != nullptr)
	{
		g_mg_fax_log->log(LOG_PREFIX, "mg_initialize_fax_receiver_t38 : creating fax receiver...");
	}

	if (fax == nullptr)
	{
		if (g_mg_fax_log != nullptr)
		{
			g_mg_fax_log->log_warning(LOG_PREFIX, "mg_initialize_fax_receiver_t38 : fax is NULL.");
		}
		return false;
	}
	mg_fax_t38_t* fax_t38 = (mg_fax_t38_t*)fax;
	fax_t38->set_type(true);
	if (fax_t38->isMetronomeStarted())
	{
		fax_t38->metronomeStop();
	}

	t38_terminal_state_t* t38 = fax_t38->get_fax();
	if (t38 != nullptr)
	{
		t38_terminal_free(t38);
		t38 = nullptr;
	}
	t38 = t38_terminal_init(nullptr, FALSE, mg_fax_transmitter_packet_handler_t38, fax_t38);
	if (t38 == nullptr)
	{
		if (g_mg_fax_log != nullptr)
		{
			g_mg_fax_log->log_warning(LOG_PREFIX, "mg_initialize_fax_receiver_t38 : fax receiver creation FAILED.");
		}
		return false;
	}

	t30_state_t* t30 = t38_terminal_get_t30_state(t38);
	t38_core_state_t* t38_core = t38_terminal_get_t38_core_state(t38);

	t38_set_t38_version(t38_core, 1);
	t38_terminal_set_config(t38, T38_TERMINAL_OPTION_REGULAR_INDICATORS | T38_TERMINAL_OPTION_2S_REPEATING_INDICATORS);
	t38_terminal_set_tep_mode(t38, FALSE);

	logging_state_t* log = t38_terminal_get_logging_state(t38);
	span_log_set_level(log, 0xFFFF);//SPAN_LOG_DEBUG | SPAN_LOG_FLOW | SPAN_LOG_SHOW_TAG | SPAN_LOG_SHOW_SAMPLE_TIME);
	span_log_set_tag(log, "T.38T");
	span_log_set_message_handler(log, span_message);
	span_log_set_error_handler(log, span_error);

	log = t38_core_get_logging_state(t38_core);
	span_log_set_level(log, 0xFFFF);//SPAN_LOG_DEBUG | SPAN_LOG_FLOW | SPAN_LOG_SHOW_TAG | SPAN_LOG_SHOW_SAMPLE_TIME);
	span_log_set_tag(log, "T.38C");
	span_log_set_message_handler(log, span_message);
	span_log_set_error_handler(log, span_error);

	log = t30_get_logging_state(t30);
	span_log_set_level(log, 0xFFFF);//SPAN_LOG_DEBUG | SPAN_LOG_FLOW | SPAN_LOG_SHOW_TAG | SPAN_LOG_SHOW_SAMPLE_TIME);
	span_log_set_tag(log, "T.30 ");
	span_log_set_message_handler(log, span_message);
	span_log_set_error_handler(log, span_error);

	t30_set_rx_file(t30, file_mask, -1);

	t30_set_phase_b_handler(t30, phase_b_handler_rx, fax_t38);
	t30_set_phase_d_handler(t30, phase_d_handler_rx, fax_t38);
	t30_set_phase_e_handler(t30, phase_e_handler_rx, fax_t38);

	//t30_set_tx_ident(t30, "rx:spandsp");
	t30_set_tx_nsf(t30, (const uint8_t *) "\x50\x00\x00\x00Spandsp\x00", 12);

	t30_set_supported_modems(t30, T30_SUPPORT_V29 | T30_SUPPORT_V27TER | T30_SUPPORT_IAF);// T30_SUPPORT_V17 | T30_SUPPORT_V34HDX |

	t30_set_ecm_capability(t30, TRUE);

	t30_set_supported_compressions(t30, T30_SUPPORT_T4_1D_COMPRESSION | T30_SUPPORT_T4_2D_COMPRESSION | T30_SUPPORT_T6_COMPRESSION | T30_SUPPORT_T85_COMPRESSION);

	/* Support for different image sizes && resolutions*/
	t30_set_supported_image_sizes(t30, T30_SUPPORT_US_LETTER_LENGTH | T30_SUPPORT_US_LEGAL_LENGTH |
		T30_SUPPORT_UNLIMITED_LENGTH | T30_SUPPORT_215MM_WIDTH | T30_SUPPORT_255MM_WIDTH | T30_SUPPORT_303MM_WIDTH);
	t30_set_supported_resolutions(t30, T30_SUPPORT_STANDARD_RESOLUTION | T30_SUPPORT_FINE_RESOLUTION |
		T30_SUPPORT_SUPERFINE_RESOLUTION | T30_SUPPORT_R8_RESOLUTION | T30_SUPPORT_R16_RESOLUTION);

	if (g_mg_fax_log != nullptr)
	{
		g_mg_fax_log->log(LOG_PREFIX, "mg_initialize_fax_receiver_t38 : fax receiver created.");
	}

	mg_fax_t38_params_t param;
	fax_t38->get_parameters(&param);

	t38_terminal_set_fill_bit_removal(t38, param.fill_bit_removal);
	t38_set_data_rate_management_method(t38_core, param.rate_management);
	t38_set_jbig_transcoding(t38_core, param.transcoding_jbig ? 1 : 0);
	t38_set_max_buffer_size(t38_core, param.max_buffer);
	t38_set_max_datagram_size(t38_core, param.max_datagram);
	t38_set_mmr_transcoding(t38_core, param.transcoding_mmr);
	t38_set_t38_version(t38_core, param.version);
	t38_set_fastest_image_data_rate(t38_core, param.max_bit_rate);

	fax_t38->set_fax_notified(false);
	fax_t38->set_fax(t38);

	return fax_t38->metronomeStart(20, false);
}
//------------------------------------------------------------------------------
bool mg_initialize_fax_transmitter_t38(mg_fax_t* fax, const char* file_name, bool ecm)
{
	if (g_mg_fax_log != nullptr)
	{
		g_mg_fax_log->log(LOG_PREFIX, "mg_initialize_fax_transmitter_t38 : creating fax transmitter...");
	}

	if (fax == nullptr)
	{
		if (g_mg_fax_log != nullptr)
		{
			g_mg_fax_log->log_warning(LOG_PREFIX, "mg_initialize_fax_transmitter_t38 : fax is NULL.");
		}
		return false;
	}
	mg_fax_t38_t* fax_t38 = (mg_fax_t38_t*)fax;
	fax_t38->set_type(false);

	if (fax_t38->isMetronomeStarted())
	{
		fax_t38->metronomeStop();
	}

	span_set_message_handler(span_message);
	span_set_error_handler(span_error);

	t38_terminal_state_t* t38 = fax_t38->get_fax();
	if (t38 != nullptr)
	{
		t38_terminal_free(t38);
		t38 = nullptr;
	}
	t38 = t38_terminal_init(nullptr, TRUE, mg_fax_transmitter_packet_handler_t38, fax_t38);
	if (t38 == nullptr)
	{
		if (g_mg_fax_log != nullptr)
		{
			g_mg_fax_log->log_warning(LOG_PREFIX, "mg_initialize_fax_transmitter_t38 : fax transmitter creation FAILED.");
		}
		return false;
	}

	t30_state_t* t30 = t38_terminal_get_t30_state(t38);
	t38_core_state_t* t38_core = t38_terminal_get_t38_core_state(t38);
	t38_set_t38_version(t38_core, 1);
	t38_terminal_set_config(t38, T38_TERMINAL_OPTION_REGULAR_INDICATORS | T38_TERMINAL_OPTION_2S_REPEATING_INDICATORS);
	t38_terminal_set_tep_mode(t38, TRUE);

	logging_state_t* log = t38_terminal_get_logging_state(t38);
	span_log_set_level(log, 0xFFFF);//SPAN_LOG_DEBUG | SPAN_LOG_SHOW_TAG | SPAN_LOG_SHOW_SAMPLE_TIME);
	span_log_set_tag(log, "T.38T");
	span_log_set_message_handler(log, span_message);
	span_log_set_error_handler(log, span_error);

	log = t38_core_get_logging_state(t38_core);
	span_log_set_level(log, 0xFFFF);//SPAN_LOG_DEBUG | SPAN_LOG_SHOW_TAG | SPAN_LOG_SHOW_SAMPLE_TIME);
	span_log_set_tag(log, "T.38C");
	span_log_set_message_handler(log, span_message);
	span_log_set_error_handler(log, span_error);

	log = t30_get_logging_state(t30);
	span_log_set_level(log, 0xFFFF);//SPAN_LOG_DEBUG | SPAN_LOG_SHOW_TAG | SPAN_LOG_SHOW_SAMPLE_TIME);
	span_log_set_tag(log, "T.30 ");
	span_log_set_message_handler(log, span_message);
	span_log_set_error_handler(log, span_error);

	t30_set_supported_modems(t30, T30_SUPPORT_V29 | T30_SUPPORT_V27TER |T30_SUPPORT_IAF);//| T30_SUPPORT_V34HDX  |  T30_SUPPORT_V17
	//t30_set_tx_ident(t30, "tx:spandsp");
	t30_set_tx_nsf(t30, (const uint8_t *) "\x50\x00\x00\x00Spandsp\x00", 12);
	t30_set_tx_file(t30, file_name, -1, -1);
	t30_set_phase_b_handler(t30, phase_b_handler_tx, fax_t38);
	t30_set_phase_d_handler(t30, phase_d_handler_tx, fax_t38);
	t30_set_phase_e_handler(t30, phase_e_handler_tx, fax_t38);

	if (ecm)
	{
		t30_set_ecm_capability(t30, TRUE);
	}

	mg_fax_t38_params_t param;
	fax_t38->get_parameters(&param);

	t38_terminal_set_fill_bit_removal(t38, param.fill_bit_removal);
	t38_set_data_rate_management_method(t38_core, param.rate_management);
	t38_set_jbig_transcoding(t38_core, param.transcoding_jbig);
	t38_set_max_buffer_size(t38_core, param.max_buffer);
	t38_set_max_datagram_size(t38_core, param.max_datagram);
	t38_set_mmr_transcoding(t38_core, param.transcoding_mmr);
	t38_set_t38_version(t38_core, param.version);
	t38_set_fastest_image_data_rate(t38_core, param.max_bit_rate);

	t30_set_supported_compressions(t30, T30_SUPPORT_T4_1D_COMPRESSION | T30_SUPPORT_T4_2D_COMPRESSION | T30_SUPPORT_T6_COMPRESSION | T30_SUPPORT_T85_COMPRESSION);

	/* Support for different image sizes && resolutions*/
	t30_set_supported_image_sizes(t30, T30_SUPPORT_US_LETTER_LENGTH | T30_SUPPORT_US_LEGAL_LENGTH |
		T30_SUPPORT_UNLIMITED_LENGTH | T30_SUPPORT_215MM_WIDTH | T30_SUPPORT_255MM_WIDTH | T30_SUPPORT_303MM_WIDTH);
	t30_set_supported_resolutions(t30, T30_SUPPORT_STANDARD_RESOLUTION | T30_SUPPORT_FINE_RESOLUTION |
		T30_SUPPORT_SUPERFINE_RESOLUTION | T30_SUPPORT_R8_RESOLUTION | T30_SUPPORT_R16_RESOLUTION);

	if (g_mg_fax_log != nullptr)
	{
		g_mg_fax_log->log(LOG_PREFIX, "tx : fax transmitter created.");
	}

	fax_t38->set_fax_notified(false);
	fax_t38->set_fax(t38);

	return fax_t38->metronomeStart(20, false);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
mg_fax_t38_t::mg_fax_t38_t(rtl::Logger* log)
	: mg_fax_t(log), m_log(log)
{
	m_t38 = nullptr;
	memset(&m_params, 0, sizeof m_params);

	mg_fax_t38_set_default_params(&m_params);

	memset(&m_buff, 0, m_buff_size);
	m_buff_write = 0;

	m_receiver = true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
mg_fax_t38_t::~mg_fax_t38_t()
{
	destroy();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mg_fax_t38_t::destroy()
{
	if (m_t38 != nullptr)
	{
		t38_terminal_free(m_t38);
		m_t38 = nullptr;
	}

	if (!fax_notified())
	{
		// стоп с выше :)
		fax_signal(FAX_SIGNAL_DESTROED);
		set_fax_notified(true);
	}

	m_buff_write = 0;

	PLOG_FAX(LOG_PREFIX, "destroy : fax %s destroyed.", (m_receiver) ? "RX" : "TX");
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mg_fax_t38_t::set_type(bool receiver)
{
	m_receiver = receiver;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mg_fax_t38_t::phase_b_rx(t30_state_t *s, int result)
{
	PLOG_FAX(LOG_PREFIX, "rx : phase B mark.");
}
//------------------------------------------------------------------------------
void mg_fax_t38_t::phase_d_rx(t30_state_t *s, int result)
{
	t30_stats_t t;

	/*
	// The current bit rate for image transfer.
	int bit_rate;
	// TRUE if error correcting mode is used.
	int error_correcting_mode;
	// The number of pages sent so far.
	int pages_tx;
	// The number of pages received so far.
	int pages_rx;
	// The number of pages in the file (<0 if not known).
	int pages_in_file;
	// The horizontal column-to-column resolution of the most recent page, in pixels per metre
	int x_resolution;
	// The vertical row-to-row resolution of the most recent page, in pixels per metre
	int y_resolution;
	// The number of horizontal pixels in the most recent page.
	int width;
	// The number of vertical pixels in the most recent page.
	int length;
	// The size of the image, in bytes
	int image_size;
	// The type of compression used between the FAX machines
	int encoding;
	// The number of bad pixel rows in the most recent page.
	int bad_rows;
	// The largest number of bad pixel rows in a block in the most recent page.
	int longest_bad_row_run;
	// The number of HDLC frame retries, if error correcting mode is used.
	int error_correcting_mode_retries;
	// Current status.
	int current_status;
	// The number of RTP events in this call.
	int rtp_events;
	// The number of RTN events in this call.
	int rtn_events;
	*/

	if (result)
	{
		t30_get_transfer_statistics(s, &t);

		const char* fmt = "rx : phase D statistics :\n\
+--------------------------------------\n\
| Result..............%s(%d)\n\
| Current status......%s(%d)\n\
| Bit rate............%d\n\
| Pages received......%d\n\
| Pages in file.......%d\n\
| Image resolution....%d x %d\n\
| Image size..........%d x %d\n\
| Image length........%d\n\
| Encoding............%s(%d)\n\
| ECM.................%s\n\
| ECM retries.........%d\n\
| Bad rows............%d\n\
| Longest bad row.....%d\n\
+--------------------------------------";
	/*\t\t\t| RTP events..........%d\n\
| RTN events..........%d\n\
+--------------------------------------";*/
							  
		PLOG_FAX(LOG_PREFIX, fmt,
			t30_frametype(result), result,
			t30_completion_code_to_str(t.current_status), t.current_status,
			t.bit_rate,
			t.pages_rx,
			t.pages_in_file,
			t.x_resolution, t.y_resolution,
			t.width, t.length,
			t.image_size,
			t4_encoding_to_str(t.encoding), t.encoding,
			t.error_correcting_mode ? "ON" : "OFF",
			t.error_correcting_mode_retries,
			t.bad_rows,
			t.longest_bad_row_run);/*,
			t.rtp_events,
			t.rtn_events);*/
	}
}
//------------------------------------------------------------------------------
void mg_fax_t38_t::phase_e_rx(t30_state_t *s, int completion_code)
{
	const char* local_ident;
	const char* far_ident;

	local_ident = t30_get_tx_ident(s);
	far_ident = t30_get_rx_ident(s);

	set_fax_notified(true);

	if (completion_code == T30_ERR_OK)
	{
		PLOG_FAX(LOG_PREFIX, "rx : phase E\n\
+--------------------------------------\n\
| Fax successfully received.\n\
| Remote station id: '%s'\n\
| Local station id:  '%s'\n\
+--------------------------------------", far_ident, local_ident);

		fax_signal(FAX_SIGNAL_RX_DONE);
	}
	else
	{
		PLOG_FAX(LOG_PREFIX, "rx : phase E\n\
+--------------------------------------\n\
| Fax receiving error : %s (%d).\n\
+--------------------------------------", t30_completion_code_to_str(completion_code), completion_code);

		fax_signal(FAX_SIGNAL_RX_ERROR);
	}
}
//------------------------------------------------------------------------------
void mg_fax_t38_t::phase_b_tx(t30_state_t *s, int result)
{
	PLOG_FAX(LOG_PREFIX, "tx : phase B mark.");
}
//------------------------------------------------------------------------------
void mg_fax_t38_t::phase_d_tx(t30_state_t *s, int result)
{
	t30_stats_t t;

	t30_get_transfer_statistics(s, &t);

	const char* fmt = "tx : phase D statistics:\n\
						+--------------------------------------\n\
						| Result..............%s(%d)\n\
						| Current status......%s(%d)\n\
						| Bit rate............%d\n\
						| Pages transfered....%d\n\
						| Pages in file.......%d\n\
						| Image resolution....%d x %d\n\
						| Image size..........%d x %d\n\
						| Image length........%d\n\
						| Encoding............%s(%d)\n\
						| ECM.................%s\n\
						| ECM retries.........%d\n\
						| Bad rows............%d\n\
						| Longest bad row.....%d\n\
						+--------------------------------------";
						 /* \t\t\t| RTP events..........%d\n\
						| RTN events..........%d\n\
						+--------------------------------------";*/

	PLOG_FAX(LOG_PREFIX, fmt,
		t30_frametype(result), result,
		t30_completion_code_to_str(t.current_status), t.current_status,
		t.bit_rate,
		t.pages_tx,
		t.pages_in_file,
		t.x_resolution, t.y_resolution,
		t.width, t.length,
		t.image_size,
		t4_encoding_to_str(t.encoding), t.encoding,
		t.error_correcting_mode ? "ON" : "OFF",
		t.error_correcting_mode_retries,
		t.bad_rows,
		t.longest_bad_row_run); /*,
		t.rtp_events,
		t.rtn_events);*/
}
//------------------------------------------------------------------------------
void mg_fax_t38_t::phase_e_tx(t30_state_t *s, int completion_code)
{
	const char* local_ident;
	const char* far_ident;


	local_ident = t30_get_tx_ident(s);
	far_ident = t30_get_rx_ident(s);
	set_fax_notified(true);

	if (completion_code == T30_ERR_OK)
	{
		PLOG_FAX(LOG_PREFIX, "tx : phase E\n\
+--------------------------------------\n\
| Fax successfully sent.\n\
| Remote station id: %s\n\
| Local station id:  %s\n\
+--------------------------------------", far_ident, local_ident);

		fax_signal(FAX_SIGNAL_TX_DONE);
	}
	else
	{
		PLOG_FAX(LOG_PREFIX, "tx : phase E\n\
+--------------------------------------\n\
| Fax sending error : %s (%d)\n\
+--------------------------------------", t30_completion_code_to_str(completion_code), completion_code);

		fax_signal(FAX_SIGNAL_TX_ERROR);
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t mg_fax_t38_t::write_fax(const uint8_t* data, uint32_t length)
{
	if (data == nullptr)
	{
		PLOG_WARNING(LOG_PREFIX, "write_fax : data is null.");
		return 0;
	}

	if (m_t38 == nullptr)
	{
		PLOG_WARNING(LOG_PREFIX, "write_fax : t38 state is null.");
		return 0;
	}

	if (!lock())
	{
		PLOG_WARNING(LOG_PREFIX, "write_fax : write fax lock !!! (%d bytes).", length);
		return 0;
	}

	udptl_rx_frame_t* frame = (udptl_rx_frame_t*)data;

	PLOG_FAX(LOG_PREFIX, "write_fax : write data fax (seq:%d len:%d).", frame->seq_no, frame->ifp_length);
	t38_core_state_t* t38_core = t38_terminal_get_t38_core_state(m_t38);
	t38_core_rx_ifp_packet(t38_core, frame->ifp, frame->ifp_length, frame->seq_no);

	unlock();

	return length;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t mg_fax_t38_t::read_fax(uint8_t* data, uint32_t length)
{
	if (data == nullptr)
	{
		PLOG_WARNING(LOG_PREFIX, "read_fax : data is null.");
		return 0;
	}

	if (m_t38 == nullptr)
	{
		PLOG_WARNING(LOG_PREFIX, "read_fax : t38 state is null.");
		return 0;
	}

	if (!lock())
	{
		PLOG_WARNING(LOG_PREFIX, "read_fax : read fax lock !!! (%d bytes).", m_buff_write);
		return 0;
	}

	if (!t38_terminal_send_timeout(m_t38, m_buff_size / sizeof(short)))
	{
		//PLOG_FAX(LOG_PREFIX, "read_fax : t38_terminal_send_timeout false.");
	}

	if (m_buff_write > 0)
	{
		memcpy(data, m_buff, m_buff_write);
	}

	unlock();

	if (m_buff_write > 0)
	{
		PLOG_FAX(LOG_PREFIX, "read_fax : read fax data (%d bytes).", m_buff_write);
	}

	uint32_t read = m_buff_write;
	m_buff_write = 0;
	return read;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mg_fax_t38_t::set_parameters(mg_fax_t38_params_t* params)
{
	if (params == nullptr)
	{
		return;
	}

	memcpy(&m_params, params, sizeof(mg_fax_t38_params_t));

	if (m_t38 != NULL)
	{
		t38_core_state_t* t38_core = t38_terminal_get_t38_core_state(m_t38);

		t38_terminal_set_fill_bit_removal(m_t38, params->fill_bit_removal);
		t38_set_data_rate_management_method(t38_core, params->rate_management);
		t38_set_jbig_transcoding(t38_core, params->transcoding_jbig);
		t38_set_max_buffer_size(t38_core, params->max_buffer);
		t38_set_max_datagram_size(t38_core, params->max_datagram);
		t38_set_mmr_transcoding(t38_core, params->transcoding_mmr);
		t38_set_t38_version(t38_core, params->version);
		t38_set_fastest_image_data_rate(t38_core, params->max_bit_rate);
	}
}
//------------------------------------------------------------------------------
void mg_fax_t38_t::get_parameters(mg_fax_t38_params_t* params)
{
	if (params == nullptr)
	{
		return;
	}

	memcpy(params, &m_params, sizeof(mg_fax_t38_params_t));
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
t38_terminal_state_t* mg_fax_t38_t::get_fax()
{
	return m_t38;
}
//------------------------------------------------------------------------------
void mg_fax_t38_t::set_fax(t38_terminal_state_t* t38)
{
	m_t38 = t38;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int mg_fax_t38_t::t38_packet_tx(t38_core_state_t *s, const uint8_t *buf, int len, int transmissions)
{
	if (buf == nullptr)
	{
		return 0;
	}

	try
	{
		PLOG_FAX(LOG_PREFIX, "t38_packet_tx : len:%d trans:%d", len, transmissions);
		m_buff_write = MIN((int)(m_buff_size - 1), len);
		memcpy(m_buff + 1, buf, m_buff_write);
		m_buff[0] = transmissions;
		m_buff_write++;
	}
	catch (rtl::Exception ex)
	{
		PLOG_FAX(LOG_PREFIX, "t38_packet_tx : raised exception %s", ex.get_message());
	}

	return 0;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
