﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "std_timer.h"
#include "std_mutex.h"
#include "std_typedef.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	struct mg_novoice_timer_manager_element;
	class mg_rx_common_stream_t;
	//------------------------------------------------------------------------------
	// Класс для управления таймерами отсутствия данных в rx стримах.
	//------------------------------------------------------------------------------
	class mg_novoice_timer_manager_t : rtl::ITimerEventHandler
	{
	public:
		mg_novoice_timer_manager_t();
		~mg_novoice_timer_manager_t();

		bool add(mg_rx_common_stream_t* rx_stream);
		void remove(mg_rx_common_stream_t* rx_stream);

	private:
		bool start();
		void stop();

		/// <timer_event_handler_t>
		virtual void timer_elapsed_event(rtl::Timer* timer, int tid);

	private:
		rtl::MutexWatch m_sync;							// синхронизация
		rtl::Timer* m_timer;							// таймер
		volatile bool m_timer_enter;					// признак входа в функцию таймера

		mg_novoice_timer_manager_element* m_link_first;	// первый линк на стримы (для обхода при удалении или таймировании)
		mg_novoice_timer_manager_element* m_link_last;	// последний линк на стримы (для добавления)
	};
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	extern mg_novoice_timer_manager_t* g_mg_novoice_timer_manager;
}
//------------------------------------------------------------------------------
