/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_video_conference_out_tube.h"

namespace mge
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	class VideoMixerEncoder
	{
	private:
		rtl::Logger* m_log;
		media::PayloadFormat m_format;
		rtl::Mutex m_sync;
		mg_video_encoder_t* m_encoder;
		rtl::ArrayT<mg_video_conference_out_tube_t*> m_list;
		uint32_t m_ssrc;
		uint16_t m_lastSeqNumber;

	private:
		static void videoEncoderCallback(const void* rtpPayload, int payloadLength, uint32_t ts, bool marker, void* user);
		void sendPacket(const void* rtpPayload, int payloadLength, uint32_t ts, bool marker);

	public:
		VideoMixerEncoder(rtl::Logger* log, const media::PayloadFormat& vformat);
		~VideoMixerEncoder();

		void destroy();

		void addMember(mg_video_conference_out_tube_t* handler);
		void removeMember(mg_video_conference_out_tube_t* handler);
		int getMemberCount() { return m_list.getCount(); }
		void updateImage(media::VideoImage* image);

		const media::PayloadFormat& getFormat() const { return m_format; }
	};
}
//-----------------------------------------------
