﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_sdp_ice_param.h"
#include "rtp_channel.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_sdp_ice_param_t::mg_sdp_ice_param_t()
	{
		m_ice_pwd = rtl::String::empty;
		m_ice_ufrag = rtl::String::empty;
		m_candidate_list.clear();

		clear();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_sdp_ice_param_t::~mg_sdp_ice_param_t()
	{
		clear();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_sdp_ice_param_t::generate(rtp_channel_t* channel)
	{
		clear();

		if (channel == nullptr)
		{
			return false;
		}

		m_ice_pwd.append(sdp_ice_candidate_t::generate_pwd());
		m_ice_ufrag.append(sdp_ice_candidate_t::generate_ufrag());

		//--------------------------------------------------------------

		sdp_ice_candidate_t* rtp_candidate1 = NEW sdp_ice_candidate_t;

		rtp_candidate1->set_type(sdp_ice_candidate_host_e);

		rtp_candidate1->generate_foundation();
		rtp_candidate1->set_component_id(SDP_ICE_CANDIDATE_RTP);
		rtp_candidate1->set_transport(sdp_ice_transport_udp_e);
		rtp_candidate1->set_priority(sdp_ice_candidate_t::generate_priority(sdp_ice_candidate_host_e, 1, true));
		rtp_candidate1->set_conn_address(channel->get_net_interface(), channel->get_data_port());
		rtp_candidate1->add_ice_param("generation", "0");
		rtp_candidate1->add_ice_param("network-id", "1");

		//--------------------------------------------------------------

		m_candidate_list.add(rtp_candidate1);

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_sdp_ice_param_t::copy_from(const mg_sdp_ice_param_t* param)
	{
		clear();

		if (param == nullptr)
		{
			return false;
		}

		m_ice_ufrag = param->m_ice_ufrag;
		m_ice_pwd = param->m_ice_pwd;

		for (int i = 0; i < param->m_candidate_list.getCount(); i++)
		{
			sdp_ice_candidate_t* candidate_at_list = param->m_candidate_list.getAt(i);
			if (candidate_at_list == nullptr)
			{
				continue;
			}

			sdp_ice_candidate_t* new_candidate = NEW sdp_ice_candidate_t(*candidate_at_list);
			m_candidate_list.add(new_candidate);
		}

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_sdp_ice_param_t::clear()
	{
		m_ice_pwd = rtl::String::empty;
		m_ice_ufrag = rtl::String::empty;

		for (int i = 0; i < m_candidate_list.getCount(); i++)
		{
			sdp_ice_candidate_t* candidate = m_candidate_list.getAt(i);
			if (candidate == nullptr)
			{
				continue;
			}

			DELETEO(candidate);
			candidate = nullptr;
		}
		m_candidate_list.clear();
	}
	//------------------------------------------------------------------------------
	// a=candidate:1434301788 1 udp 2122260223 192.168.0.10 61488 typ host generation 0
	// a=candidate:2999745851 1 udp 2122194687 192.168.56.1 61489 typ host generation 0
	// a=candidate:1434301788 2 udp 2122260222 192.168.0.10 61490 typ host generation 0
	// a=candidate:2999745851 2 udp 2122194686 192.168.56.1 61491 typ host generation 0
	// a=candidate:2741881992 2 udp 1686052606 87.117.163.31 53634 typ srflx raddr 192.168.0.10 rport 61490 generation 0
	// a=candidate:2741881992 1 udp 1686052607 87.117.163.31 53601 typ srflx raddr 192.168.0.10 rport 61488 generation 0
	// a=candidate:469649836 1 tcp 1518280447 192.168.0.10 9 typ host tcptype active generation 0
	// a=candidate:4233069003 1 tcp 1518214911 192.168.56.1 9 typ host tcptype active generation 0
	// a=candidate:469649836 2 tcp 1518280446 192.168.0.10 9 typ host tcptype active generation 0
	// a=candidate:4233069003 2 tcp 1518214910 192.168.56.1 9 typ host tcptype active generation 0
	// a=ice-ufrag:urEVLoQJXV1Be0mM
	// a=ice-pwd:ABBj2dSKZubDAXZW/CjPcdN3
	//------------------------------------------------------------------------------
	bool mg_sdp_ice_param_t::add_ice_line(const rtl::String& media_line)
	{
		if (media_line.isEmpty())
		{
			return false;
		}

		rtl::String candidate_line(media_line);

		//--------------------a=candidate---------------------
		int index_b = 0;
		int index = candidate_line.indexOf('=');
		if (index == BAD_INDEX)
		{
			return false;
		}
		rtl::String a = candidate_line.substring(0, index);

		//--------------------type-----------------------
		index_b = index + 1;
		index = candidate_line.indexOf(':');
		if (index == BAD_INDEX)
		{
			return false;
		}
		rtl::String type = candidate_line.substring(index_b, index - index_b);

		//-------------------candidate-----------------------
		if (type.indexOf("candidate") != BAD_INDEX)
		{
			sdp_ice_candidate_t* rtp_candidate = NEW sdp_ice_candidate_t;
			if (!rtp_candidate->parse(media_line))
			{
				DELETEO(rtp_candidate);
				rtp_candidate = nullptr;
				return false;
			}
			m_candidate_list.add(rtp_candidate);
		}
		//-------------------ice-ufrag-----------------------
		else if (type.indexOf("ice-ufrag") != BAD_INDEX)
		{
			index_b = index + 1;
			m_ice_ufrag = candidate_line.substring(index_b);
		}
		//------------------ice-pwd-----------------------
		else if (type.indexOf("ice-pwd") != BAD_INDEX)
		{
			index_b = index + 1;
			m_ice_pwd = candidate_line.substring(index_b);
		}

		return true;
	}
	//------------------------------------------------------------------------------
	// a=candidate:1434301788 1 udp 2122260223 192.168.0.10 61488 typ host generation 0
	// a=candidate:2999745851 1 udp 2122194687 192.168.56.1 61489 typ host generation 0
	// a=candidate:1434301788 2 udp 2122260222 192.168.0.10 61490 typ host generation 0
	// a=candidate:2999745851 2 udp 2122194686 192.168.56.1 61491 typ host generation 0
	// a=candidate:2741881992 2 udp 1686052606 87.117.163.31 53634 typ srflx raddr 192.168.0.10 rport 61490 generation 0
	// a=candidate:2741881992 1 udp 1686052607 87.117.163.31 53601 typ srflx raddr 192.168.0.10 rport 61488 generation 0
	// a=candidate:469649836 1 tcp 1518280447 192.168.0.10 9 typ host tcptype active generation 0
	// a=candidate:4233069003 1 tcp 1518214911 192.168.56.1 9 typ host tcptype active generation 0
	// a=candidate:469649836 2 tcp 1518280446 192.168.0.10 9 typ host tcptype active generation 0
	// a=candidate:4233069003 2 tcp 1518214910 192.168.56.1 9 typ host tcptype active generation 0
	// a=ice-ufrag:urEVLoQJXV1Be0mM
	// a=ice-pwd:ABBj2dSKZubDAXZW/CjPcdN3
	//------------------------------------------------------------------------------
	bool mg_sdp_ice_param_t::write_ice_lines(rtl::String& ss)
	{
		if ((m_ice_pwd.isEmpty()) || (m_ice_ufrag.isEmpty()))
		{
			return false;
		}

		ss << "\r\n";

		//--------------------------------------------------------------------------
		// a=ice-ufrag:urEVLoQJXV1Be0mM
		ss << "a=ice-ufrag:" << m_ice_ufrag << "\r\n";
		//--------------------------------------------------------------------------

		//--------------------------------------------------------------------------
		// a=ice-pwd:ABBj2dSKZubDAXZW/CjPcdN3
		ss << "a=ice-pwd:" << m_ice_pwd;
		//--------------------------------------------------------------------------

		bool one = false;
		for (int i = 0; i < m_candidate_list.getCount(); i++)
		{
			sdp_ice_candidate_t* candidate = m_candidate_list.getAt(i);
			if (candidate == nullptr)
			{
				continue;
			}

			char buff[256] = { 0 };
			int size = candidate->write_to(buff, 256);
			if (size <= 0)
			{
				continue;
			}
			buff[size] = 0;
			rtl::String candidate_str(buff);
			//--------------------------------------------------------------------------
			// a=candidate:1434301788 1 udp 2122260223 192.168.0.10 61488 typ host generation 0
			ss << "\r\n";
			ss << "a=candidate:" << candidate_str;
			//--------------------------------------------------------------------------
			one = true;
		}

		return one;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	const char* mg_sdp_ice_param_t::get_ufrag()
	{
		return m_ice_ufrag;
	}
	//------------------------------------------------------------------------------
	void mg_sdp_ice_param_t::set_ufrag(const char* ufrag)
	{
		if (ufrag == nullptr)
		{
			return;
		}

		m_ice_ufrag = rtl::String::empty;
		m_ice_ufrag.append(ufrag);
	}
	//------------------------------------------------------------------------------
	const char* mg_sdp_ice_param_t::get_pwd()
	{
		return m_ice_pwd;
	}
	//------------------------------------------------------------------------------
	void mg_sdp_ice_param_t::set_pwd(const char* pwd)
	{
		if (pwd == nullptr)
		{
			return;
		}

		m_ice_pwd = rtl::String::empty;
		m_ice_pwd.append(pwd);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	sdp_ice_candidate_t* mg_sdp_ice_param_t::get_top_candidate(rtp_channel_t* channel)
	{
		const uint32_t len = 256;
		char mask1[len] = { 0 };
		bool channel_is_local = channel->get_net_interface()->fill_suitable_local_mask(mask1, len);

		int index = -1;
		for (int i = 0; i < m_candidate_list.getCount(); i++)
		{
			sdp_ice_candidate_t* cand = m_candidate_list.getAt(i);
			if (cand == nullptr)
			{
				continue;
			}

			if (cand->getType() == sdp_ice_candidate_host_e && cand->get_component_id() == 1)
			{
				if (index < 0)
				{
					index = i;
				}

				char mask2[len] = { 0 };
				if (channel_is_local && cand->get_conn_address()->fill_suitable_local_mask(mask2, len))
				{
					if (strcmp(mask1, mask2) == 0)
					{
						return cand;
					}
				}
				else if (!channel_is_local && !cand->get_conn_address()->fill_suitable_local_mask(mask2, len))
				{
					return cand;
				}
			}
		}

		if (index >= 0)
		{
			return m_candidate_list.getAt(index);
		}

		return nullptr;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_sdp_ice_param_t::compare_sdp_ice_line(const rtl::String& media_line)
	{
		if (m_candidate_list.getCount() > 0)
		{
			for (int i = 0; i < m_candidate_list.getCount(); i++)
			{
				sdp_ice_candidate_t* candidate = m_candidate_list.getAt(i);
				if (candidate == nullptr)
				{
					continue;
				}

				char buff[256] = { 0 };
				if (candidate->write_to(buff, 256) <= 0)
				{
					continue;
				}

				if (rtl::String::compare(media_line, buff, false) == 0)
				{
					return true;
				}
			}
		}

		return false;
	}
}
//------------------------------------------------------------------------------
