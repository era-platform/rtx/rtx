﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_media_engine.h"

namespace mge
{
	//-----------------------------------------------
	// элемент массива микширования
	//-----------------------------------------------
	struct AudioMixerMember
	{
		void* member;
		const short* samples;
		int enabled;
		int play_background;
	};
	//-----------------------------------------------
	//
	//-----------------------------------------------
	class AudioMixer
	{
	private:
		uint32_t m_frameSize;
		int m_mixAlgorithm;

	public:
		// frame_size: размер фрейма для микшера.
		// mix_algorithm: 0-просто сложение, 1-линейный модуль, 2-круг никитинский, 3-круг шамилича.
		AudioMixer(uint32_t frameSize, int mixAlgorithm);
		virtual	~AudioMixer();

		uint32_t get_audio_frame_size();

		// Микширование данных.
		int mix_samples(const AudioMixerMember* members, int members_count, double* mix_buffer);
		void sub_samples(const AudioMixerMember* members, int members_count, double* mix_buffer, const double* mixedvoice);
		void cvt_samples(const double* mixed_voice, short* samples);
	};
}
//-----------------------------------------------
