﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_common_termination.h"
#include "rtp_simple_channel.h"
#include "rtp_secure_channel.h"
#include "udptl_proxy_channel.h"
#include "mg_rx_common_stream.h"
#include "mg_tx_common_stream.h"
#include "mg_rx_fax_proxy_stream.h"
#include "mg_tx_fax_proxy_stream.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define LOG_PREFIX m_tag // Media Gateway Termination
#define RTCP_MONITOR "rtcp-monitor"

#include "logdef.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	static mg_rx_stream_t* get_rx_stream(mg_rx_stream_type_t type_rx, MediaSession* mg_stream)
	{
		const rtl::ArrayT<mg_rx_stream_t*>& rx_streams = mg_stream->get_rx_streams();

		if (rx_streams.getCount() == 0)
		{
			if (!mg_stream->create_rx_stream(type_rx))
			{
				return nullptr;
			}
		}

		mg_rx_stream_t* rx_stream = rx_streams[0];
		return rx_stream; // (rx_stream == nullptr || rx_stream->getType() != type_rx) ? nullptr : rx_stream;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	static mg_tx_stream_t* get_tx_stream(mg_tx_stream_type_t type_tx, MediaSession* mg_stream)
	{
		const rtl::ArrayT<mg_tx_stream_t*>& tx_streams = mg_stream->get_tx_streams();
		if (tx_streams.getCount() == 0)
		{
			if (!mg_stream->create_tx_stream(type_tx))
			{
				return nullptr;
			}
		}

		mg_tx_stream_t* tx_stream = tx_streams[0];

		return (tx_stream == nullptr || tx_stream->getType() != type_tx) ? nullptr : tx_stream;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	TerminationRTP::TerminationRTP(rtl::Logger* log, const h248::TerminationID& termId, const char* parentId) :
		Termination(log, termId, parentId),
		m_sdp_local(log), m_sdp_remote(log)
	{
		strcpy(m_tag, "TERM-RTP");
		m_service_state = h248::TerminationServiceState::Test;
		m_event_buffer_control = false;
		m_conference = false;
		m_vId = 0;
		m_vTitle = "Unnamed";
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	TerminationRTP::~TerminationRTP()
	{
		mg_termination_destroy(nullptr); // ???
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool TerminationRTP::mg_termination_initialize(h248::Field* _reply)
	{
		PLOG_WRITE(LOG_PREFIX, "ID(%s) : mg_termination_initialize().", getName());

		m_sdp_local.set_termination_id(getName());
		m_sdp_remote.set_termination_id(getName());

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool TerminationRTP::mg_termination_destroy(h248::Field* _reply)
	{
		PLOG_WRITE(LOG_PREFIX, "ID(%s) : mg_termination_destroy().", getName());

		Termination::mg_termination_lock();

		stop_all();

		Termination::destroyMediaSession();

		clear_cannels();

		m_sdp_local.clear();
		m_sdp_remote.clear();

		return true;
	}
	//------------------------------------------------------------------------------
	bool TerminationRTP::mg_termination_set_TerminationState_Property(const h248::Field* property, h248::Field* reply)
	{
		const rtl::String& propName = property->getName();
		const rtl::String& propValue = property->getValue();

		PLOG_WRITE(LOG_PREFIX, "ID(%s) : mg_termination_set_TerminationState_Property(name:%s, value:%s)",
			getName(), (const char*)propName, (const char*) propValue);


		if (propName == "mode")
		{
			m_conference = propValue == "conf";
		}
		else if (propName == "v-id")
		{
			m_vId = strtoul(propValue, nullptr, 10);
		}
		else if(propName == "v-title")
		{
			m_vTitle = propValue;
		}
		else
		{
			PLOG_WARNING(LOG_PREFIX, "ID(%s) : mg_termination_set_TerminationState_Property() : unsupported or unknown property!", getName());
			reply->addErrorDescriptor(h248::ErrorCode::c445_Unsupported_or_unknown_property);
			return false;
		}

		return true;
	}
	//------------------------------------------------------------------------------
	bool TerminationRTP::mg_termination_set_Local_Remote(uint16_t streamId, const rtl::String& remote_sdp, const rtl::String& local_sdp_offer, h248::Field* reply)
	{
		ENTER();

		rtp_channel_t* channel = getChannel(streamId);
		if (channel == nullptr)
		{
			reply->addErrorDescriptor(h248::ErrorCode::c510_Insufficient_resources, "RTP Manager returns error - not enough udp ports");
			return false;
		}

		MediaSession* mg_stream = makeMediaSession(streamId);
		if (mg_stream == nullptr)
		{
			ERROR_FMT("stream %d not created!", streamId);
			reply->addErrorDescriptor(h248::ErrorCode::c510_Insufficient_resources, "Media session not started");
			return false;
		}

		bool result = true;
		if (streamId == MG_STREAM_TYPE_AUDIO)
		{
			result = prepare_audio(channel, mg_stream, remote_sdp, local_sdp_offer);
		}
		else if (streamId == MG_STREAM_TYPE_VIDEO)
		{
			result = prepare_video(channel, mg_stream, remote_sdp, local_sdp_offer);
		}
		else if (streamId == MG_STREAM_TYPE_IMAGE)
		{
			result = prepare_image(channel, mg_stream, remote_sdp, local_sdp_offer);
		}

		if (!result)
		{
			ERROR_MSG("preparation of media data failed");
			reply->addErrorDescriptor(h248::ErrorCode::c500_Internal_software_failure_in_the_MG_or_MGC);
			return false;
		}

		RETURN_BOOL(true);
	}
	//------------------------------------------------------------------------------
	bool TerminationRTP::mg_termination_get_Local(uint16_t streamId, rtl::String& local_sdp_answer, h248::Field* reply)
	{
		ENTER_FMT("streamId: %d", streamId);

		rtl::String ss;
		m_sdp_local.write_base(ss);

		ss << "\r\n";

		if (streamId == MG_STREAM_TYPE_AUDIO)
		{
			mg_sdp_media_base_t* audio = m_sdp_local.get_media(streamId);
			if (audio == nullptr)
			{
				WARNING("SDP audio part not found!");
			}
			else
			{
				audio->write(ss);
			}
		}
		else if (streamId == MG_STREAM_TYPE_VIDEO)
		{
			mg_sdp_media_base_t* video = m_sdp_local.get_media(streamId);
			if (video == nullptr)
			{
				WARNING("SDP video part not found!");
			}
			
			video->write(ss);
		}
		else if (streamId == MG_STREAM_TYPE_IMAGE)
		{
			mg_sdp_media_base_t* image = m_sdp_local.get_media(streamId);
			if (image == nullptr)
			{
				WARNING("SDP image part not found!");
			}
			else
			{
				image->write(ss);
			}
		}

		local_sdp_answer = ss;

		RETURN_BOOL(true);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool TerminationRTP::mg_termination_set_events(uint16_t reqId, const h248::Field* requestedEvent, h248::Field* reply)
	{
		bool vad = true;
		AudioAnalyzerParams vadParams; // { 40, 30, 500 };
		uint16_t streamId = MG_STREAM_TYPE_AUDIO;
		const char* evtName = requestedEvent->getName();

		ENTER_FMT("reqId:%d, evtName:%s", reqId, evtName);

		if (rtl::strcmp(requestedEvent->getName(), "vdp/empty") == 0)
		{
			vad = false;
		}
		else if (rtl::strcmp(requestedEvent->getName(), "vdp/vad") == 0)
		{
			const h248::Field* vnode;

			if ((vnode = requestedEvent->getField("vthres")) != nullptr)
			{
				vadParams.threshold = (uint16_t)strtoul(vnode->getValue(), nullptr, 10);
			}

			if ((vnode = requestedEvent->getField("vad_min_duration")) != nullptr)
			{
				vadParams.vadDuration = strtoul(vnode->getValue(), nullptr, 10);
			}

			if ((vnode = requestedEvent->getField("silence_min_duration")) != nullptr)
			{
				vadParams.silenceDuration = strtoul(vnode->getValue(), nullptr, 10);
			}

			if ((vnode = requestedEvent->getField(h248::Token::Stream)) != nullptr)
			{
				streamId = (uint16_t)strtoul(vnode->getValue(), nullptr, 10);
			}
		}
		else
		{
			ERROR_MSG("unknown event descriptor!");
			reply->addErrorDescriptor(h248::ErrorCode::c445_Unsupported_or_unknown_property);
			return false;
		}

		MediaSession* mg_stream = Termination::findMediaSession(streamId);
		if (mg_stream == nullptr)
		{
			ERROR_MSG("media session not found!");
			reply->addErrorDescriptor(h248::ErrorCode::c502_Not_ready, "media session not found!");
			/// ??? unknown behavior!
			return true;
		}

		for (int i = 0; i < mg_stream->get_rx_streams().getCount(); i++)
		{
			mg_rx_stream_t* rx = mg_stream->get_rx_stream_at(i);
			if (rx != nullptr && rx->getType() == mg_rx_stream_type_t::mg_rx_simple_audio_e)
			{
				mg_rx_common_stream_t* rx_common_stream = (mg_rx_common_stream_t*)rx;
				rx_common_stream->vad_activate(vad);
				rx_common_stream->vad_setParams(vadParams);

				PRINT_FMT("{ vthres=%d, vad=%d, silence=%d }",
					vadParams.threshold,
					vadParams.vadDuration,
					vadParams.silenceDuration);
			}
		}

		RETURN_FMT(true, "(vad: %s)", (vad) ? "ON" : "OFF");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	rtp_channel_t* TerminationRTP::createChannel(uint32_t streamId)
	{
		rtp_channel_t* channel = nullptr;

		const char* addressKey = getTermId().getInterface();

		if (streamId == MG_STREAM_TYPE_IMAGE)
		{
			channel = rtp_port_manager_t::get_channel(addressKey, rtp_channel_type_t::rtp_channel_type_udptl_proxy_e);
		}
		else if (getType() == h248::TerminationType::RTP)
		{
			channel = rtp_port_manager_t::get_channel(addressKey, rtp_channel_type_t::rtp_channel_type_simple_e);
		}
		else if (getType() == h248::TerminationType::WebRTC)
		{
			channel = rtp_port_manager_t::get_channel(addressKey, rtp_channel_type_t::rtp_channel_type_webrtc_e);
		}
		else if (getType() == h248::TerminationType::SRTP)
		{
			channel = rtp_port_manager_t::get_channel(addressKey, rtp_channel_type_t::rtp_channel_type_secure_e);
		}

		if (channel == nullptr)
		{
			ERROR_FMT("channel with interface: %s and type:%s wasn't created",
				(const char*)addressKey, h248::TerminationType_toString(getType()));
			return nullptr;
		}

		PRINT_FMT("channel with interface: %s and type:%s is created", 
				(const char*)addressKey, h248::TerminationType_toString(getType()));

		mg_channel_at_stream_t* chst = NEW mg_channel_at_stream_t();
		chst->stream_id = streamId;
		chst->channel = channel;
		m_channel_list.add(chst);

		if (streamId == MG_STREAM_TYPE_IMAGE)
		{
			mg_channel_at_stream_t* last = nullptr;
			int index_last = m_channel_list.getCount() - 1;
			index_last--;
			if (index_last >= 0)
			{
				last = m_channel_list.getAt(index_last);
			}
			if (last != nullptr)
			{
				if (last->channel != nullptr)
				{
					last->channel->set_next_channel(channel);
				}
			}
		}

		return channel;
	}
	//------------------------------------------------------------------------------
	rtp_channel_t* TerminationRTP::findChannel(uint32_t streamId)
	{
		ENTER_FMT("streamId: %d", streamId);

		rtp_channel_t* channel = nullptr;
		for (int i = 0; i < m_channel_list.getCount(); i++)
		{
			mg_channel_at_stream_t* chst = m_channel_list.getAt(i);
			if (chst != nullptr && chst->stream_id == streamId)
			{
				channel = chst->channel;
				break;
			}
		}

		if (channel == nullptr)
		{
			WARNING("channel not found or channel list corrupted!");
			return nullptr;
		}

		return channel;
	}
	//------------------------------------------------------------------------------
	rtp_channel_t* TerminationRTP::getChannel(uint32_t streamId)
	{
		rtp_channel_t* channel = nullptr;

		for (int i = 0; i < m_channel_list.getCount(); i++)
		{
			mg_channel_at_stream_t* chst = m_channel_list.getAt(i);
			if (chst != nullptr && chst->stream_id == streamId)
			{
				channel = chst->channel;
				break;
			}
		}

		if (channel == nullptr)
		{
			return createChannel(streamId);
		}

		return channel;
	}
	//------------------------------------------------------------------------------
	void TerminationRTP::release_channel(rtp_channel_t* channel)
	{
		ENTER_FMT("%s", (const char*)channel->getName());

		bool found = false;

		for (int i = 0; i < m_channel_list.getCount(); i++)
		{
			mg_channel_at_stream_t* chst = m_channel_list.getAt(i);
			rtp_channel_t* channel_at_list = chst->channel;

			if (rtp_port_manager_t::compare_channels(channel, channel_at_list))
			{
				m_channel_list.removeAt(i);
				rtp_port_manager_t::release_channel(channel);
				channel = nullptr;
				found = true;

				chst->channel = nullptr;
				DELETEO(chst);
				chst = nullptr;
				m_channel_list.removeAt(i);

				if (i > 0)
				{
					mg_channel_at_stream_t* chst_last = m_channel_list.getAt(i-1);
					if (chst_last != nullptr)
					{
						if (chst_last->channel != nullptr)
						{
							chst_last->channel->set_next_channel(nullptr);
						}
					}
				}

				break;
			}
		}

		if (!found)
		{
			ERROR_FMT("channel '%s' not found!", channel->getName());
		}
	}
	//------------------------------------------------------------------------------
	void TerminationRTP::clear_cannels()
	{
		ENTER();

		for (int i = 0; i < m_channel_list.getCount(); i++)
		{
			mg_channel_at_stream_t* chst = m_channel_list.getAt(i);
			rtp_channel_t* channel = chst->channel;

			rtp_port_manager_t::release_channel(channel);
			channel = nullptr;
			chst->channel = nullptr;
			DELETEO(chst);
			chst = nullptr;
		}

		m_channel_list.clear();

		LEAVE();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool TerminationRTP::isReady(uint32_t processor_id)
	{
		return Termination::isReady(processor_id);
		//MediaSession* mg_stream = Termination::findMediaSession(processor_id);

		//return mg_stream == nullptr ? false : mg_stream->isReady();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool TerminationRTP::start_all()
	{
		ENTER();

		bool no_one_started = true;

		for (int i = 0; i < m_channel_list.getCount(); i++)
		{
			mg_channel_at_stream_t* chst = m_channel_list.getAt(i);
			rtp_channel_t* channel = chst->channel;

			if (rtp_port_manager_t::start_channel_listen(channel))
			{
				no_one_started = false;
			}
			else
			{
				WARNING_FMT("channel with index: %d not started!", i);
			}
		}

		if (no_one_started)
		{
			ERROR_MSG("no channel has been started!");
			return false;
		}

		return true;
	}
	//------------------------------------------------------------------------------
	bool TerminationRTP::mg_stream_start(uint32_t streamId)
	{
		if (!Termination::mg_stream_start(streamId))
		{
			return false;
		}

		rtp_channel_t* channel = findChannel(streamId);
		if (channel == nullptr)
		{
			//ERROR_MSG("channel is null!");
			return false;
		}

		bool result = rtp_port_manager_t::start_channel_listen(channel);

		if (!result)
		{
			WARNING_FMT("channel '%s' not started!", (const char*)channel->getName());
		}

		return result;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void TerminationRTP::stop_all()
	{
		ENTER();

		int count;

		for (count = 0; count < m_channel_list.getCount(); count++)
		{
			mg_channel_at_stream_t* chst = m_channel_list.getAt(count);
			rtp_channel_t* channel = chst->channel;
			
			rtp_port_manager_t::stop_channel_listen(channel);
		}

		PRINT_FMT("%d channels stopped", count);
	}
	//------------------------------------------------------------------------------
	void TerminationRTP::mg_stream_stop(uint32_t streamId)
	{
		Termination::mg_stream_stop(streamId);

		rtp_channel_t* channel = findChannel(streamId);

		if (channel)
		{
			rtp_port_manager_t::stop_channel_listen(channel);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_rx_stream_t* TerminationRTP::find_rx_stream_by_type(mg_rx_stream_type_t type_rx, MediaSession* mg_stream)
	{
		const rtl::ArrayT<mg_rx_stream_t*>& rx_stream_list = mg_stream->get_rx_streams();
		if (rx_stream_list.getCount() == 0)
		{
			if (!mg_stream->create_rx_stream(type_rx))
			{
				ERROR_MSG("failed to create rx stream!");
				return nullptr;
			}
		}
		const rtl::ArrayT<mg_rx_stream_t*>& rx_streams = mg_stream->get_rx_streams();
		if (rx_streams.getCount() > 0)
		{
			return rx_streams[0];
		}
		
		ERROR_MSG("failed to create rx stream!");
		
		return nullptr;
	}
	//------------------------------------------------------------------------------
	mg_tx_stream_t*	TerminationRTP::find_tx_stream_by_type(mg_tx_stream_type_t type_tx, MediaSession* mg_stream)
	{
		const rtl::ArrayT<mg_tx_stream_t*>& tx_streams_list = mg_stream->get_tx_streams();
		if (tx_streams_list.getCount() == 0)
		{
			if (!mg_stream->create_tx_stream(type_tx))
			{
				ERROR_MSG("failed to create tx stream!");
				return nullptr;
			}
		}
		if (tx_streams_list.getCount() == 0)
		{
			ERROR_MSG("tx stream list is empty!");
			return nullptr;
		}
		
		mg_tx_stream_t* tx_stream = tx_streams_list[0];
		
		if (tx_stream == nullptr)
		{
			ERROR_MSG("tx stream list contains null stream!");
			return nullptr;
		}
		if (tx_stream->getType() != type_tx)
		{
			ERROR_FMT("Mismatch of tx stream type with required! %s != %s",
				mg_tx_stream_type_toString(tx_stream->getType()),
				mg_tx_stream_type_toString(type_tx));
			return nullptr;
		}

		return tx_stream;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool TerminationRTP::prepare_audio(rtp_channel_t* channel, MediaSession* audio_stream, const rtl::String& remote_sdp, const rtl::String& local_sdp)
	{
		if (remote_sdp.isEmpty())
		{
			return prepare_offer_audio(channel, audio_stream, local_sdp);
		}
		else if (local_sdp.isEmpty())
		{
			return apply_answer_audio(channel, audio_stream, remote_sdp);
		}

		if (!apply_offer_audio(channel, audio_stream, remote_sdp))
		{
			return false;
		}

		return prepare_answer_audio(channel, audio_stream, local_sdp);
	}
	//------------------------------------------------------------------------------
	bool TerminationRTP::prepare_video(rtp_channel_t* channel, MediaSession* video_stream, const rtl::String& remote_sdp, const rtl::String& local_sdp)
	{
		ENTER();

		if (remote_sdp.isEmpty())
		{
			return prepare_offer_video(channel, video_stream, local_sdp);
		}
		else if (local_sdp.isEmpty())
		{
			return apply_answer_video(channel, video_stream, remote_sdp);
		}

		if (!apply_offer_video(channel, video_stream, remote_sdp))
		{
			return false;
		}

		return prepare_answer_video(channel, video_stream, local_sdp);
	}
	//------------------------------------------------------------------------------
	bool TerminationRTP::prepare_image(rtp_channel_t* channel, MediaSession* image_stream, const rtl::String& remote_sdp, const rtl::String& local_sdp)
	{
		if (remote_sdp.isEmpty())
		{
			return prepare_offer_image(channel, image_stream, local_sdp);
		}
		else if (local_sdp.isEmpty())
		{
			return apply_answer_image(channel, image_stream, remote_sdp);
		}

		if (!apply_offer_image(channel, image_stream, remote_sdp))
		{
			return false;
		}

		return prepare_answer_image(channel, image_stream, local_sdp);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool TerminationRTP::prepare_offer_audio(rtp_channel_t* channel, MediaSession* audio_stream, const rtl::String& local_sdp)
	{
		mg_tx_stream_type_t type_tx = mg_tx_stream_type_t::mg_tx_simple_audio_e;
		rtl::String type_str(MG_STREAM_TYPE_AUDIO_STR);

		if (local_sdp.isEmpty())
		{
			ERROR_MSG("error local SDP!");
			return false;
		}

		if (!m_sdp_local.read(local_sdp, false, audio_stream->getId()))
		{
			ERROR_MSG("error reading local SDP!");
			return false;
		}

		mg_tx_common_stream_t* tx_common_stream = (mg_tx_common_stream_t*)find_tx_stream_by_type(type_tx, audio_stream);
		if (tx_common_stream == nullptr)
		{
			ERROR_MSG("tx_common_stream not found");
			return false;
		}

		mg_sdp_media_base_t* audio_l = m_sdp_local.get_media(audio_stream->getId());
		if (audio_l == nullptr)
		{
			ERROR_MSG("audio not found");
			return false;
		}
		const ip_address_t* ipaddr = m_sdp_local.get_address();
		if (ipaddr->empty())
		{
			ipaddr = channel->get_net_interface();
			m_sdp_local.set_address(ipaddr->to_string());
		}

		uint16_t port = audio_l->get_port();
		if (port == UINT16_MAX)
		{
			port = channel->get_data_port();
			audio_l->set_port(port);
		}

		audio_l->clear_webrtc_param();
		audio_l->get_extmaps().clear();
		audio_l->set_rtcp_mux(false);
		audio_l->set_mid_type("");

		m_sdp_local.clear_bundle_param();

		if (channel->getType() == rtp_channel_type_t::rtp_channel_type_simple_e)
		{
			rtp_simple_channel_t* simple_channel = (rtp_simple_channel_t*)channel;
			tx_common_stream->set_channel_event(simple_channel);

			audio_l->set_rtcp_port(simple_channel->get_ctrl_port());
			audio_l->set_rtcp_address(simple_channel->get_net_interface());
		}
		else if (channel->getType() == rtp_channel_type_t::rtp_channel_type_secure_e)
		{
			rtp_secure_channel_t* secure_channel = (rtp_secure_channel_t*)channel;
			audio_l->get_crypto_params().clear();
			secure_channel->prepare_crypto_out_params(audio_l->get_crypto_params());
			tx_common_stream->set_channel_event(secure_channel);
		}
		else
		{
			ERROR_MSG("unknown type of channel!");
			return false;
		}

		return true;
	}
	//------------------------------------------------------------------------------
	bool TerminationRTP::apply_answer_audio(rtp_channel_t* channel, MediaSession* audio_stream, const rtl::String& remote_sdp)
	{
		mg_rx_stream_type_t type_rx = mg_rx_stream_type_t::mg_rx_simple_audio_e;
		mg_tx_stream_type_t type_tx = mg_tx_stream_type_t::mg_tx_simple_audio_e;
		rtl::String type_str(MG_STREAM_TYPE_AUDIO_STR);

		if (remote_sdp.isEmpty())
		{
			return false;
		}
		socket_address sockaddr_old("0.0.0.0",0);
		mg_sdp_media_base_t* audio_r = m_sdp_remote.get_media(audio_stream->getId());
		if (audio_r != nullptr)
		{
			const ip_address_t* ipaddr_old = audio_r->get_address();
			if (ipaddr_old->empty())
			{
				ipaddr_old = m_sdp_remote.get_address();
			}
			uint16_t port_old = audio_r->get_port();
			sockaddr_old.set_ip_address_with_port(ipaddr_old, port_old);
		}
		if (!m_sdp_remote.read(remote_sdp, false, audio_stream->getId()))
		{
			ERROR_MSG("error reading remote SDP!");
			return false;
		}

		audio_r = m_sdp_remote.get_media(audio_stream->getId());
		if (audio_r == nullptr)
		{
			return false;
		}
		const ip_address_t* ipaddr = audio_r->get_address();
		if (ipaddr->empty())
		{
			ipaddr = m_sdp_remote.get_address();
		}
		uint16_t port = audio_r->get_port();
		socket_address sockaddr(ipaddr, port);
		bool difference_address = !sockaddr.equals(&sockaddr_old);

		mg_rx_common_stream_t* rx_common_stream = (mg_rx_common_stream_t*)find_rx_stream_by_type(type_rx, audio_stream);
		if (rx_common_stream == nullptr)
		{
			return false;
		}

		mg_tx_common_stream_t* tx_common_stream = (mg_tx_common_stream_t*)find_tx_stream_by_type(type_tx, audio_stream);
		if (tx_common_stream == nullptr)
		{
			return false;
		}
	
		// filter
		mg_sdp_media_base_t* audio_l = m_sdp_local.get_media(audio_stream->getId());
		if (audio_l == nullptr)
		{
			rx_common_stream->setMediaParams(*audio_r);
			tx_common_stream->setMediaParams(*audio_r);
		}
		else
		{
			rx_common_stream->setMediaParams(*audio_l);
			tx_common_stream->setMediaParams(*audio_r);

			mg_media_element_list_t* elemens_tx = tx_common_stream->getMediaParams().get_media_elements();
			for (int i = 0; i < elemens_tx->getCount(); i++)
			{
				mg_media_element_t* el = elemens_tx->getAt(i);
				if (el == nullptr)
				{
					continue;
				}
				bool find = false;
				for (int j = 0; j < audio_l->get_media_elements()->getCount(); j++)
				{
					mg_media_element_t* el_l = audio_l->get_media_elements()->getAt(j);
					if (el_l == nullptr)
					{
						continue;
					}

					if (el_l->compare_with(el))
					{
						find = true;
						break;
					}
				}
				if (!find)
				{
					elemens_tx->removeAt(i);
					i--;
				}
			}
		}

		if (difference_address)
		{
			audio_stream->reset_first_packet_recive_flag();
			tx_common_stream->set_remote_address(&sockaddr);
		}

		if (channel->getType() == rtp_channel_type_t::rtp_channel_type_simple_e)
		{
			rtp_simple_channel_t* simple_channel = (rtp_simple_channel_t*)channel;
			rx_common_stream->set_channel_event(simple_channel);
			simple_channel->set_event_handler(rx_common_stream);
		}
		else if (channel->getType() == rtp_channel_type_t::rtp_channel_type_secure_e)
		{
			rtp_secure_channel_t* secure_channel = (rtp_secure_channel_t*)channel;
			bool one = false;
			for (int i = 0; i < audio_r->get_crypto_params().getCount(); i++)
			{
				rtl::String crypto = audio_r->get_crypto_params().getAt(i);
				if (secure_channel->setup_srtp_policy_in(&crypto))
				{
					one = true;
					break;
				}
			}
			if (!one)
			{
				return false;
			}

			rx_common_stream->set_channel_event(secure_channel);
			secure_channel->set_event_handler(rx_common_stream);
		}
		else
		{
			return false;
		}

		//-------------------------------------------------------------------------

		if (rtl::Logger::check_trace(TRF_STAT))
		{
			rtl::String monitor(Config.get_config_value(RTCP_MONITOR));
			if (!monitor.isEmpty() && (monitor.indexOf("1") != BAD_INDEX))
			{
				audio_stream->create_rtcp_monitor(sockaddr.to_string(), rx_common_stream, tx_common_stream, nullptr, false);
			}

			audio_stream->add_statistics_in_streams(sockaddr.to_string(), rx_common_stream, tx_common_stream);
		}
	
		const ip_address_t* ipaddr_rtcp = audio_r->get_rtcp_address();
		uint16_t port_rtcp = audio_r->get_rtcp_port();
		if (ipaddr_rtcp->empty())
		{
			ipaddr_rtcp = ipaddr;
			port_rtcp = port + 1;
		}
		socket_address sockaddr_rtcp(ipaddr_rtcp, port_rtcp);
		tx_common_stream->set_remote_rtcp_address(&sockaddr_rtcp);

		//-------------------------------------------------------------------------

		if (!m_conference)
		{
			if (!m_rtp_record_file_path.isEmpty())
			{
				// создаем рекордер только когда есть обратная сторона и терминатор настроен полностью.
				if (!rx_common_stream->createRecorder(m_rtp_record_file_path))
				{
					WARNING("recorder creation error!");
					return false;
				}
			}

			if (rx_common_stream->vad_isActive())
			{
				if (!rx_common_stream->vad_createAnalyzer())
				{
					WARNING("vad analyzer creation error!");
					return false;
				}
			}
		}

		return true;
	}
	//------------------------------------------------------------------------------
	bool TerminationRTP::prepare_answer_audio(rtp_channel_t* channel, MediaSession* audio_stream, const rtl::String& local_sdp)
	{
		mg_tx_stream_type_t type_tx = mg_tx_stream_type_t::mg_tx_simple_audio_e;
		mg_rx_stream_type_t type_rx = mg_rx_stream_type_t::mg_rx_simple_audio_e;
		rtl::String type_str(MG_STREAM_TYPE_AUDIO_STR);

		if (local_sdp.isEmpty())
		{
			return false;
		}

		if (!m_sdp_local.read(local_sdp, false, audio_stream->getId()))
		{
			ERROR_MSG("error reading local SDP!");
			return false;
		}

		mg_tx_stream_t* tx_stream = find_tx_stream_by_type(type_tx, audio_stream);
		if (tx_stream == nullptr)
		{
			return false;
		}
		mg_tx_common_stream_t* tx_common_stream = (mg_tx_common_stream_t*)tx_stream;

		mg_rx_stream_t* rx_stream = find_rx_stream_by_type(type_rx, audio_stream);
		if (rx_stream == nullptr)
		{
			return false;
		}

		mg_sdp_media_base_t* audio_l = m_sdp_local.get_media(audio_stream->getId());
		if (audio_l == nullptr)
		{
			return false;
		}
		if (audio_l->get_media_elements()->getCount() != 0)
		{
			if (audio_l->get_media_elements()->getAt(0)->get_payload_id() == 101)
			{
				if (audio_l->get_media_elements()->create_with_copy_from(audio_l->get_media_elements()->getAt(0), mg_media_element_list_t::POSITION_END))
				{
					audio_l->get_media_elements()->removeAt(0);
				}
			}
		}

		rx_stream->setMediaParams(*audio_l);

		mg_sdp_media_base_t* audio_r = m_sdp_remote.get_media(audio_stream->getId());
		if (audio_r == nullptr)
		{
			return false;
		}
		tx_stream->setMediaParams(*audio_r);
		mg_media_element_list_t* elemens_tx = tx_stream->getMediaParams().get_media_elements();

		for (int i = 0; i < elemens_tx->getCount(); i++)
		{
			mg_media_element_t* el = elemens_tx->getAt(i);
			if (el == nullptr)
			{
				continue;
			}
			bool find = false;
			for (int j = 0; j < audio_l->get_media_elements()->getCount(); j++)
			{
				mg_media_element_t* el_l = audio_l->get_media_elements()->getAt(j);
				if (el_l == nullptr)
				{
					continue;
				}

				if (el_l->compare_with(el))
				{
					find = true;
					break;
				}
			}
			if (!find)
			{
				elemens_tx->removeAt(i);
				i--;
			}
		}

		const ip_address_t* ipaddr = m_sdp_local.get_address();
		if (ipaddr->empty())
		{
			ipaddr = channel->get_net_interface();
			m_sdp_local.set_address(ipaddr->to_string());
		}

		uint16_t port = audio_l->get_port();
		if (port == UINT16_MAX)
		{
			port = channel->get_data_port();
			audio_l->set_port(port);
		}

		audio_l->clear_webrtc_param();
		audio_l->get_extmaps().clear();
		audio_l->set_rtcp_mux(false);
		audio_l->set_mid_type("");

		m_sdp_local.clear_bundle_param();

		if (channel->getType() == rtp_channel_type_t::rtp_channel_type_simple_e)
		{
			rtp_simple_channel_t* simple_channel = (rtp_simple_channel_t*)channel;
			tx_common_stream->set_channel_event(simple_channel);

			audio_l->set_rtcp_port(simple_channel->get_ctrl_port());
			audio_l->set_rtcp_address(simple_channel->get_net_interface());

			// RTX-82. linphone шлет с кандидатами.
			if (audio_r->get_ice_param() != nullptr)
			{
				mg_sdp_ice_param_t* ice = NEW mg_sdp_ice_param_t();
				if (!ice->generate(simple_channel))
				{
					DELETEO(ice);
					return false;
				}
				audio_l->set_ice_param(ice);
				DELETEO(ice);
				audio_l->set_transport(audio_r->get_transport()); // "RTP/AVPF" linphone
			}
		}
		else if (channel->getType() == rtp_channel_type_t::rtp_channel_type_secure_e)
		{
			rtp_secure_channel_t* secure_channel = (rtp_secure_channel_t*)channel;
			audio_l->get_crypto_params().clear();
			audio_l->get_crypto_params().add(secure_channel->get_sdp_crypto_param_out());

			tx_common_stream->set_channel_event(secure_channel);
		}
		else
		{
			return false;
		}

		return true;
	}
	//------------------------------------------------------------------------------
	bool TerminationRTP::apply_offer_audio(rtp_channel_t* channel, MediaSession* audio_stream, const rtl::String& remote_sdp)
	{
		mg_rx_stream_type_t type_rx = mg_rx_stream_type_t::mg_rx_simple_audio_e;
		mg_tx_stream_type_t type_tx = mg_tx_stream_type_t::mg_tx_simple_audio_e;

		rtl::String type_str(MG_STREAM_TYPE_AUDIO_STR);

		if (remote_sdp.isEmpty())
		{
			return false;
		}
		socket_address sockaddr_old("0.0.0.0", 0);
		mg_sdp_media_base_t* audio_r = m_sdp_remote.get_media(audio_stream->getId());
		if (audio_r != nullptr)
		{
			const ip_address_t* ipaddr_old = audio_r->get_address();
			if (ipaddr_old->empty())
			{
				ipaddr_old = m_sdp_remote.get_address();
			}
			uint16_t port_old = audio_r->get_port();
			sockaddr_old.set_ip_address_with_port(ipaddr_old, port_old);
		}
		if (!m_sdp_remote.read(remote_sdp, false, audio_stream->getId()))
		{
			ERROR_MSG("reading remote SDP aborted by mistake!");
			return false;
		}
		audio_r = m_sdp_remote.get_media(audio_stream->getId());
		if (audio_r == nullptr)
		{
			return false;
		}
		const ip_address_t* ipaddr = audio_r->get_address();
		if (ipaddr->empty())
		{
			ipaddr = m_sdp_remote.get_address();
		}
		uint16_t port = audio_r->get_port();
		socket_address sockaddr(ipaddr, port);
		bool difference_address = !sockaddr.equals(&sockaddr_old);

		mg_rx_stream_t* rx_stream = find_rx_stream_by_type(type_rx, audio_stream);
		if (rx_stream == nullptr)
		{
			return false;
		}
		mg_rx_common_stream_t* rx_common_stream = (mg_rx_common_stream_t*)rx_stream;

		mg_tx_stream_t* tx_stream = find_tx_stream_by_type(type_tx, audio_stream);
		if (tx_stream == nullptr)
		{
			return false;
		}
		mg_tx_common_stream_t* tx_common_stream = (mg_tx_common_stream_t*)tx_stream;

		// filter
		mg_sdp_media_base_t* audio_l = m_sdp_local.get_media(audio_stream->getId());
		if (audio_l == nullptr)
		{
			rx_stream->setMediaParams(*audio_r);
			tx_stream->setMediaParams(*audio_r);
		}
		else
		{
			rx_stream->setMediaParams(*audio_l);
			tx_stream->setMediaParams(*audio_l);
		}
	
		if (difference_address)
		{
			audio_stream->reset_first_packet_recive_flag();
			tx_common_stream->set_remote_address(&sockaddr);
		}

		if (channel->getType() == rtp_channel_type_t::rtp_channel_type_simple_e)
		{
			rtp_simple_channel_t* simple_channel = (rtp_simple_channel_t*)channel;
			rx_common_stream->set_channel_event(simple_channel);
			simple_channel->set_event_handler(rx_common_stream);
		}
		else if (channel->getType() == rtp_channel_type_t::rtp_channel_type_secure_e)
		{
			rtp_secure_channel_t* secure_channel = (rtp_secure_channel_t*)channel;
			bool one = false;
			for (int i = 0; i < audio_r->get_crypto_params().getCount(); i++)
			{
				rtl::String crypto = audio_r->get_crypto_params().getAt(i);
				if (secure_channel->setup_srtp_policy_in(&crypto))
				{
					one = true;
					break;
				}
			}
			if (!one)
			{
				return false;
			}

			rx_common_stream->set_channel_event(secure_channel);
			secure_channel->set_event_handler(rx_common_stream);
		}
		else
		{
			return false;
		}

		//-------------------------------------------------------------------------
	
		if (rtl::Logger::check_trace(TRF_STAT))
		{
			rtl::String monitor(Config.get_config_value(RTCP_MONITOR));
			if (!monitor.isEmpty() && (monitor.indexOf("1") != BAD_INDEX))
			{
				audio_stream->create_rtcp_monitor(sockaddr.to_string(), rx_stream, tx_stream, nullptr, false);
			}
		
			audio_stream->add_statistics_in_streams(sockaddr.to_string(), rx_stream, tx_stream);
		}
	
		const ip_address_t* ipaddr_rtcp = audio_r->get_rtcp_address();
		uint16_t port_rtcp = audio_r->get_rtcp_port();
		if (ipaddr_rtcp->empty())
		{
			ipaddr_rtcp = ipaddr;
			port_rtcp = port + 1;
		}
		socket_address sockaddr_rtcp(ipaddr_rtcp, port_rtcp);
		tx_common_stream->set_remote_rtcp_address(&sockaddr_rtcp);
	
		//-------------------------------------------------------------------------

		if (!m_conference)
		{
			if (!m_rtp_record_file_path.isEmpty())
			{
				// создаем рекордер только когда есть обратная сторона и терминатор настроен полностью.
				if (!rx_common_stream->createRecorder(m_rtp_record_file_path))
				{
					WARNING("create recorder fail.");
					return false;
				}
			}

			if (rx_common_stream->vad_isActive())
			{
				if (!rx_common_stream->vad_createAnalyzer())
				{
					WARNING("create vad analyzer fail.");
					return false;
				}
			}
		}

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool TerminationRTP::prepare_offer_video(rtp_channel_t* channel, MediaSession* video_stream, const rtl::String& local_sdp)
	{
		ENTER();

		mg_tx_stream_type_t type_tx = mg_tx_stream_type_t::mg_tx_simple_video_e;
		rtl::String type_str(MG_STREAM_TYPE_VIDEO_STR);

		if (local_sdp.isEmpty())
		{
			ERROR_MSG("local sdp is empty!");
			return false;
		}

		if (!m_sdp_local.read(local_sdp, false, video_stream->getId()))
		{
			ERROR_MSG("local sdp read error!");
			return false;
		}

		mg_tx_stream_t* tx_stream = find_tx_stream_by_type(type_tx, video_stream);
		if (tx_stream == nullptr)
		{
			ERROR_MSG("tx_stream not found!");
			return false;
		}
		mg_tx_common_stream_t* tx_common_stream = (mg_tx_common_stream_t*)tx_stream;

		mg_sdp_media_base_t* video_l = m_sdp_local.get_media(video_stream->getId());
		if (video_l == nullptr)
		{
			ERROR_MSG("video sdp not found!");
			return false;
		}
	
		const ip_address_t* ipaddr = m_sdp_local.get_address();
		if (ipaddr->empty())
		{
			ipaddr = channel->get_net_interface();
			m_sdp_local.set_address(ipaddr->to_string());
		}

		uint16_t port = video_l->get_port();
		if (port == UINT16_MAX)
		{
			port = channel->get_data_port();
			video_l->set_port(port);
		}

		video_l->clear_webrtc_param();
		video_l->get_extmaps().clear();
		video_l->set_rtcp_mux(false);
		video_l->set_mid_type("");

		m_sdp_local.clear_bundle_param();

		if (channel->getType() == rtp_channel_type_t::rtp_channel_type_simple_e)
		{
			rtp_simple_channel_t* simple_channel = (rtp_simple_channel_t*)channel;
			tx_common_stream->set_channel_event(simple_channel);
			tx_common_stream->change_flag_recalc_packet(false);
			video_l->set_rtcp_port(simple_channel->get_ctrl_port());
			video_l->set_rtcp_address(simple_channel->get_net_interface());
		}
		else if (channel->getType() == rtp_channel_type_t::rtp_channel_type_secure_e)
		{
			rtp_secure_channel_t* secure_channel = (rtp_secure_channel_t*)channel;
			video_l->get_crypto_params().clear();
			video_l->get_crypto_params().add(secure_channel->get_sdp_crypto_param_out());

			tx_common_stream->set_channel_event(secure_channel);
		}
		else
		{
			ERROR_MSG("unknown channel type!");
			return false;
		}

		return true;
	}
	//------------------------------------------------------------------------------
	bool TerminationRTP::apply_answer_video(rtp_channel_t* channel, MediaSession* video_stream, const rtl::String& remote_sdp)
	{
		ENTER();

		mg_rx_stream_type_t type_rx = mg_rx_stream_type_t::mg_rx_simple_video_e;
		mg_tx_stream_type_t type_tx = mg_tx_stream_type_t::mg_tx_simple_video_e;
		rtl::String type_str(MG_STREAM_TYPE_VIDEO_STR);

		if (remote_sdp.isEmpty())
		{
			return false;
		}
		socket_address sockaddr_old("0.0.0.0", 0);
		mg_sdp_media_base_t* video_r = m_sdp_remote.get_media(video_stream->getId());
		if (video_r != nullptr)
		{
			const ip_address_t* ipaddr_old = video_r->get_address();
			if (ipaddr_old->empty())
			{
				ipaddr_old = m_sdp_remote.get_address();
			}
			uint16_t port_old = video_r->get_port();
			sockaddr_old.set_ip_address_with_port(ipaddr_old, port_old);
		}
		if (!m_sdp_remote.read(remote_sdp, false, video_stream->getId()))
		{
			ERROR_MSG("remote sdp read fail.");
			return false;
		}
		video_r = m_sdp_remote.get_media(video_stream->getId());
		if (video_r == nullptr)
		{
			return false;
		}
		const ip_address_t* ipaddr = video_r->get_address();
		if (ipaddr->empty())
		{
			ipaddr = m_sdp_remote.get_address();
		}
		uint16_t port = video_r->get_port();
		mg_sdp_ice_param_t* ice_r = video_r->get_ice_param();
		if (ice_r != nullptr)
		{
			sdp_ice_candidate_t* cand = ice_r->get_top_candidate(channel);
			if (cand != nullptr)
			{
				ipaddr = cand->get_conn_address();
				port = cand->get_conn_port();
			}
		}

		socket_address sockaddr(ipaddr, port);
		bool difference_address = !sockaddr.equals(&sockaddr_old);

		mg_rx_stream_t* rx_stream = find_rx_stream_by_type(type_rx, video_stream);
		if (rx_stream == nullptr)
		{
			return false;
		}
		mg_rx_common_stream_t* rx_common_stream = (mg_rx_common_stream_t*)rx_stream;

		mg_tx_stream_t* tx_stream = find_tx_stream_by_type(type_tx, video_stream);
		if (tx_stream == nullptr)
		{
			return false;
		}
		mg_tx_common_stream_t* tx_common_stream = (mg_tx_common_stream_t*)tx_stream;

		mg_sdp_media_base_t* video_l = m_sdp_local.get_media(video_stream->getId());
		if (video_l == nullptr)
		{
			rx_stream->setMediaParams(*video_r);
			tx_stream->setMediaParams(*video_r);
		}
		else
		{
			rx_stream->setMediaParams(*video_l);
			tx_stream->setMediaParams(*video_r);

			mg_media_element_list_t* elemens_tx = tx_stream->getMediaParams().get_media_elements();
			for (int i = 0; i < elemens_tx->getCount(); i++)
			{
				mg_media_element_t* el = elemens_tx->getAt(i);
				if (el == nullptr)
				{
					continue;
				}
				bool find = false;
				for (int j = 0; j < video_l->get_media_elements()->getCount(); j++)
				{
					mg_media_element_t* el_l = video_l->get_media_elements()->getAt(j);
					if (el_l == nullptr)
					{
						continue;
					}

					if (el_l->compare_with(el))
					{
						find = true;
						break;
					}
				}
				if (!find)
				{
					elemens_tx->removeAt(i);
					i--;
				}
			}
		}

		if (difference_address)
		{
			video_stream->reset_first_packet_recive_flag();
			tx_common_stream->change_flag_recalc_packet(false);
			tx_common_stream->set_remote_address(&sockaddr);
		}

		if (channel->getType() == rtp_channel_type_t::rtp_channel_type_simple_e)
		{
			rtp_simple_channel_t* simple_channel = (rtp_simple_channel_t*)channel;
			rx_common_stream->set_channel_event(simple_channel);
			simple_channel->set_event_handler(rx_common_stream);
		}
		else if (channel->getType() == rtp_channel_type_t::rtp_channel_type_secure_e)
		{
			rtp_secure_channel_t* secure_channel = (rtp_secure_channel_t*)channel;
			bool one = false;
			for (int i = 0; i < video_r->get_crypto_params().getCount(); i++)
			{
				rtl::String crypto = video_r->get_crypto_params().getAt(i);
				if (secure_channel->setup_srtp_policy_in(&crypto))
				{
					one = true;
					break;
				}
			}
			if (!one)
			{
				return false;
			}

			rx_common_stream->set_channel_event(secure_channel);
			secure_channel->set_event_handler(rx_common_stream);
		}
		else
		{
			return false;
		}

		//-------------------------------------------------------------------------

		const ip_address_t* ipaddr_rtcp = video_r->get_rtcp_address();
		uint16_t port_rtcp = video_r->get_rtcp_port();
		if (ipaddr_rtcp->empty())
		{
			ipaddr_rtcp = ipaddr;
			port_rtcp = port + 1;
		}
		socket_address sockaddr_rtcp(ipaddr_rtcp, port_rtcp);
		tx_common_stream->set_remote_rtcp_address(&sockaddr_rtcp);

		//-------------------------------------------------------------------------
		// @@George 19.08.19 : временно замороженно. Видезапись будет продолжена позже
		//if (!m_conference && isVideoRecordingEnabled())
		//{
		//	if (!m_rtp_record_file_path.isEmpty())
		//	{
		//		// создаем рекордер только когда есть обратная сторона и терминатор настроен полностью.
		//		if (!rx_common_stream->create_recorder(m_rtp_record_file_path + 'v'))
		//		{
		//			PLOG_WARNING(LOG_PREFIX, "apply_answer_video -- ID(%s): create recorder fail.", getName());
		//			return false;
		//		}
		//	}
		//}

		return true;
	}
	//------------------------------------------------------------------------------
	bool TerminationRTP::prepare_answer_video(rtp_channel_t* channel, MediaSession* video_stream, const rtl::String& local_sdp)
	{
		ENTER();

		mg_rx_stream_type_t type_rx = mg_rx_stream_type_t::mg_rx_simple_video_e;
		mg_tx_stream_type_t type_tx = mg_tx_stream_type_t::mg_tx_simple_video_e;
		rtl::String type_str(MG_STREAM_TYPE_VIDEO_STR);

		if (local_sdp.isEmpty())
		{
			return false;
		}

		if (!m_sdp_local.read(local_sdp, false, video_stream->getId()))
		{
			ERROR_MSG("local sdp read fail.");
			return false;
		}

		mg_tx_stream_t* tx_stream = find_tx_stream_by_type(type_tx, video_stream);
		if (tx_stream == nullptr)
		{
			return false;
		}
		mg_tx_common_stream_t* tx_common_stream = (mg_tx_common_stream_t*)tx_stream;

		mg_sdp_media_base_t* video_l = m_sdp_local.get_media(video_stream->getId());
		if (video_l == nullptr)
		{
			return false;
		}

		const ip_address_t* ipaddr = m_sdp_local.get_address();
		if (ipaddr->empty())
		{
			ipaddr = channel->get_net_interface();
			m_sdp_local.set_address(ipaddr->to_string());
		}

		uint16_t port = video_l->get_port();
		if (port == UINT16_MAX)
		{
			port = channel->get_data_port();
			video_l->set_port(port);
		}

		video_l->clear_webrtc_param();
		video_l->get_extmaps().clear();
		video_l->set_rtcp_mux(false);
		video_l->set_mid_type("");

		m_sdp_local.clear_bundle_param();

		mg_rx_stream_t* rx_stream = find_rx_stream_by_type(type_rx, video_stream);
		if (rx_stream == nullptr)
		{
			return false;
		}
		rx_stream->setMediaParams(*video_l);

		mg_sdp_media_base_t* video_r = m_sdp_remote.get_media(video_stream->getId());
		if (video_r == nullptr)
		{
			return false;
		}

		if (channel->getType() == rtp_channel_type_t::rtp_channel_type_simple_e)
		{
			rtp_simple_channel_t* simple_channel = (rtp_simple_channel_t*)channel;
			tx_common_stream->set_channel_event(simple_channel);
			tx_common_stream->change_flag_recalc_packet(false);

			video_l->set_rtcp_port(simple_channel->get_ctrl_port());
			video_l->set_rtcp_address(simple_channel->get_net_interface());

			// RTX-82. Видео на linphone c кандидатами.
			if (video_r->get_ice_param() != nullptr)
			{
				mg_sdp_ice_param_t* ice = NEW mg_sdp_ice_param_t();
				if (!ice->generate(simple_channel))
				{
					DELETEO(ice);
					return false;
				}
				video_l->set_ice_param(ice);
				DELETEO(ice);
			}
			video_l->set_transport(video_r->get_transport()); // "RTP/AVPF" linphone
		}
		else if (channel->getType() == rtp_channel_type_t::rtp_channel_type_secure_e)
		{
			rtp_secure_channel_t* secure_channel = (rtp_secure_channel_t*)channel;
			video_l->get_crypto_params().clear();
			video_l->get_crypto_params().add(secure_channel->get_sdp_crypto_param_out());

			tx_common_stream->set_channel_event(secure_channel);
		}
		else
		{
			return false;
		}

		return true;
	}
	//------------------------------------------------------------------------------
	bool TerminationRTP::apply_offer_video(rtp_channel_t* channel, MediaSession* video_stream, const rtl::String& remote_sdp)
	{
		ENTER();

		mg_rx_stream_type_t type_rx = mg_rx_stream_type_t::mg_rx_simple_video_e;
		mg_tx_stream_type_t type_tx = mg_tx_stream_type_t::mg_tx_simple_video_e;
		rtl::String type_str(MG_STREAM_TYPE_VIDEO_STR);

		if (remote_sdp.isEmpty())
		{
			return false;
		}
		socket_address sockaddr_old("0.0.0.0", 0);
		mg_sdp_media_base_t* video_r = m_sdp_remote.get_media(video_stream->getId());
		if (video_r != nullptr)
		{
			const ip_address_t* ipaddr_old = video_r->get_address();
			if (ipaddr_old->empty())
			{
				ipaddr_old = m_sdp_remote.get_address();
			}
			uint16_t port_old = video_r->get_port();
			sockaddr_old.set_ip_address_with_port(ipaddr_old, port_old);
		}
		if (!m_sdp_remote.read(remote_sdp, false, video_stream->getId()))
		{
			ERROR_MSG("remote sdp read fail.");
			return false;
		}

		video_r = m_sdp_remote.get_media(video_stream->getId());
		if (video_r == nullptr)
		{
			return false;
		}
		const ip_address_t* ipaddr = video_r->get_address();
		if (ipaddr->empty())
		{
			ipaddr = m_sdp_remote.get_address();
		}
		uint16_t port = video_r->get_port();
		mg_sdp_ice_param_t* ice_r = video_r->get_ice_param();
		if (ice_r != nullptr)
		{
			sdp_ice_candidate_t* cand = ice_r->get_top_candidate(channel);
			if (cand != nullptr)
			{
				ipaddr = cand->get_conn_address();
				port = cand->get_conn_port();
			}
		}
		socket_address sockaddr(ipaddr, port);
		bool difference_address = !sockaddr.equals(&sockaddr_old);

		mg_rx_stream_t* rx_stream = find_rx_stream_by_type(type_rx, video_stream);
		if (rx_stream == nullptr)
		{
			return false;
		}
		mg_rx_common_stream_t* rx_common_stream = (mg_rx_common_stream_t*)rx_stream;

		mg_tx_stream_t* tx_stream = find_tx_stream_by_type(type_tx, video_stream);
		if (tx_stream == nullptr)
		{
			return false;
		}
		mg_tx_common_stream_t* tx_common_stream = (mg_tx_common_stream_t*)tx_stream;
	
		rx_stream->setMediaParams(*video_r);
		tx_stream->setMediaParams(*video_r);

		if (difference_address)
		{
			video_stream->reset_first_packet_recive_flag();
			tx_common_stream->change_flag_recalc_packet(false);
			tx_common_stream->set_remote_address(&sockaddr);
		}

		if (channel->getType() == rtp_channel_type_t::rtp_channel_type_simple_e)
		{
			rtp_simple_channel_t* simple_channel = (rtp_simple_channel_t*)channel;
			rx_common_stream->set_channel_event(simple_channel);
			simple_channel->set_event_handler(rx_common_stream);
		}
		else if (channel->getType() == rtp_channel_type_t::rtp_channel_type_secure_e)
		{
			rtp_secure_channel_t* secure_channel = (rtp_secure_channel_t*)channel;
			bool one = false;
			for (int i = 0; i < video_r->get_crypto_params().getCount(); i++)
			{
				rtl::String crypto = video_r->get_crypto_params().getAt(i);
				if (secure_channel->setup_srtp_policy_in(&crypto))
				{
					one = true;
					break;
				}
			}
			if (!one)
			{
				return false;
			}

			rx_common_stream->set_channel_event(secure_channel);
			secure_channel->set_event_handler(rx_common_stream);
		}
		else
		{
			return false;
		}

		//-------------------------------------------------------------------------

		const ip_address_t* ipaddr_rtcp = video_r->get_rtcp_address();
		uint16_t port_rtcp = video_r->get_rtcp_port();
		if (ipaddr_rtcp->empty())
		{
			ipaddr_rtcp = ipaddr;
			port_rtcp = port + 1;
		}
		socket_address sockaddr_rtcp(ipaddr_rtcp, port_rtcp);
		tx_common_stream->set_remote_rtcp_address(&sockaddr_rtcp);

		//-------------------------------------------------------------------------
		// @@George 19.08.19 : временно замороженно. Видезапись будет продолжена позже
		//if (!m_conference && isVideoRecordingEnabled())
		//{
		//	if (!m_rtp_record_file_path.isEmpty())
		//	{
		//		// создаем рекордер только когда есть обратная сторона и терминатор настроен полностью.
		//		if (!rx_common_stream->create_recorder(m_rtp_record_file_path + 'v'))
		//		{
		//			PLOG_WARNING(LOG_PREFIX, "apply_offer_video -- ID(%s): create recorder fail.", getName());
		//			return false;
		//		}
		//	}
		//}

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool TerminationRTP::prepare_offer_image(rtp_channel_t* channel, MediaSession* image_stream, const rtl::String& local_sdp)
	{
		mg_tx_stream_type_t type_tx = mg_tx_stream_type_t::mg_tx_fax_proxy_e;
		rtl::String type_str(MG_STREAM_TYPE_IMAGE_STR);

		if (local_sdp.isEmpty())
		{
			return false;
		}

		if (!m_sdp_local.read(local_sdp, false, image_stream->getId()))
		{
			ERROR_MSG("local sdp read fail.");
			return false;
		}

		mg_tx_fax_proxy_stream_t* tx_fax_proxy_stream = (mg_tx_fax_proxy_stream_t*)find_tx_stream_by_type(type_tx, image_stream);
		if (tx_fax_proxy_stream == nullptr)
		{
			return false;
		}

		mg_sdp_media_base_t* image_l = m_sdp_local.get_media(image_stream->getId());
		if (image_l == nullptr)
		{
			return false;
		}
		const ip_address_t* ipaddr = m_sdp_local.get_address();
		if (ipaddr->empty())
		{
			ipaddr = channel->get_net_interface();
			m_sdp_local.set_address(ipaddr->to_string());
		}

		uint16_t port = image_l->get_port();
		if (port == UINT16_MAX)
		{
			port = channel->get_data_port();
			image_l->set_port(port);
		}

		image_l->clear_webrtc_param();
		image_l->get_extmaps().clear();
		image_l->set_rtcp_mux(false);
		image_l->set_mid_type("");

		m_sdp_local.clear_bundle_param();

		if (channel->getType() == rtp_channel_type_t::rtp_channel_type_udptl_proxy_e)
		{
			udptl_proxy_channel_t* udptl_proxy_channel = (udptl_proxy_channel_t*)channel;
			tx_fax_proxy_stream->set_channel_event(udptl_proxy_channel);
		}
		else
		{
			return false;
		}

		return true;
	}
	//------------------------------------------------------------------------------
	bool TerminationRTP::apply_answer_image(rtp_channel_t* channel, MediaSession* image_stream, const rtl::String& remote_sdp)
	{
		mg_rx_stream_type_t type_rx = mg_rx_stream_type_t::mg_rx_fax_proxy_e;
		mg_tx_stream_type_t type_tx = mg_tx_stream_type_t::mg_tx_fax_proxy_e;
		rtl::String type_str(MG_STREAM_TYPE_IMAGE_STR);

		if (remote_sdp.isEmpty())
		{
			return false;
		}
		socket_address sockaddr_old("0.0.0.0", 0);
		mg_sdp_media_base_t* image_r = m_sdp_remote.get_media(image_stream->getId());
		if (image_r != nullptr)
		{
			const ip_address_t* ipaddr_old = image_r->get_address();
			if (ipaddr_old->empty())
			{
				ipaddr_old = m_sdp_remote.get_address();
			}
			uint16_t port_old = image_r->get_port();
			sockaddr_old.set_ip_address_with_port(ipaddr_old, port_old);
		}
		if (!m_sdp_remote.read(remote_sdp, false, image_stream->getId()))
		{
			ERROR_MSG("remote sdp read fail.");
			return false;
		}
		image_r = m_sdp_remote.get_media(image_stream->getId());
		if (image_r == nullptr)
		{
			return false;
		}
		const ip_address_t* ipaddr = image_r->get_address();
		if (ipaddr->empty())
		{
			ipaddr = m_sdp_remote.get_address();
		}
		uint16_t port = image_r->get_port();
		socket_address sockaddr(ipaddr, port);
		bool difference_address = !sockaddr.equals(&sockaddr_old);

		mg_rx_stream_t* rx_stream = find_rx_stream_by_type(type_rx, image_stream);
		if (rx_stream == nullptr)
		{
			return false;
		}
		mg_rx_fax_proxy_stream_t* rx_fax_proxy_stream = (mg_rx_fax_proxy_stream_t*)rx_stream;

		mg_tx_stream_t* tx_stream = find_tx_stream_by_type(type_tx, image_stream);
		if (tx_stream == nullptr)
		{
			return false;
		}
		mg_tx_fax_proxy_stream_t* tx_fax_proxy_stream = (mg_tx_fax_proxy_stream_t*)tx_stream;

		rx_stream->setMediaParams(*image_r);
		tx_stream->setMediaParams(*image_r);

		if (difference_address)
		{
			image_stream->reset_first_packet_recive_flag();
			tx_fax_proxy_stream->set_remote_address(&sockaddr);
		}

		if (channel->getType() == rtp_channel_type_t::rtp_channel_type_udptl_proxy_e)
		{
			udptl_proxy_channel_t* udptl_proxy_channel = (udptl_proxy_channel_t*)channel;
			rx_fax_proxy_stream->set_channel_event(udptl_proxy_channel);
			udptl_proxy_channel->set_event_handler(rx_fax_proxy_stream);
		}
		else
		{
			return false;
		}

		//-------------------------------------------------------------------------

		return true;
	}
	//------------------------------------------------------------------------------
	bool TerminationRTP::prepare_answer_image(rtp_channel_t* channel, MediaSession* image_stream, const rtl::String& local_sdp)
	{
		mg_tx_stream_type_t type_tx = mg_tx_stream_type_t::mg_tx_fax_proxy_e;
		rtl::String type_str(MG_STREAM_TYPE_IMAGE_STR);

		if (local_sdp.isEmpty())
		{
			return false;
		}

		if (!m_sdp_local.read(local_sdp, false, image_stream->getId()))
		{
			ERROR_MSG("local sdp read fail.");
			return false;
		}

		mg_tx_stream_t* tx_stream = find_tx_stream_by_type(type_tx, image_stream);
		if (tx_stream == nullptr)
		{
			return false;
		}
		mg_tx_fax_proxy_stream_t* tx_fax_proxy_stream = (mg_tx_fax_proxy_stream_t*)tx_stream;

		mg_sdp_media_base_t* image_l = m_sdp_local.get_media(image_stream->getId());
		if (image_l == nullptr)
		{
			return false;
		}
	
		const ip_address_t* ipaddr = m_sdp_local.get_address();
		if (ipaddr->empty())
		{
			ipaddr = channel->get_net_interface();
			m_sdp_local.set_address(ipaddr->to_string());
		}

		uint16_t port = image_l->get_port();
		if (port == UINT16_MAX)
		{
			port = channel->get_data_port();
			image_l->set_port(port);
		}

		image_l->clear_webrtc_param();
		image_l->get_extmaps().clear();
		image_l->set_rtcp_mux(false);
		image_l->set_mid_type("");

		m_sdp_local.clear_bundle_param();

		if (channel->getType() == rtp_channel_type_t::rtp_channel_type_udptl_proxy_e)
		{
			udptl_proxy_channel_t* udptl_proxy_channel = (udptl_proxy_channel_t*)channel;
			tx_fax_proxy_stream->set_channel_event(udptl_proxy_channel);
		}
		else
		{
			return false;
		}

		return true;
	}
	//------------------------------------------------------------------------------
	bool TerminationRTP::apply_offer_image(rtp_channel_t* channel, MediaSession* image_stream, const rtl::String& remote_sdp)
	{
		mg_rx_stream_type_t type_rx = mg_rx_stream_type_t::mg_rx_fax_proxy_e;
		mg_tx_stream_type_t type_tx = mg_tx_stream_type_t::mg_tx_fax_proxy_e;
		rtl::String type_str(MG_STREAM_TYPE_IMAGE_STR);

		if (remote_sdp.isEmpty())
		{
			return false;
		}
		socket_address sockaddr_old("0.0.0.0", 0);
		mg_sdp_media_base_t* image_r = m_sdp_remote.get_media(image_stream->getId());
		if (image_r != nullptr)
		{
			const ip_address_t* ipaddr_old = image_r->get_address();
			if (ipaddr_old->empty())
			{
				ipaddr_old = m_sdp_remote.get_address();
			}
			uint16_t port_old = image_r->get_port();
			sockaddr_old.set_ip_address_with_port(ipaddr_old, port_old);
		}
		if (!m_sdp_remote.read(remote_sdp, false, image_stream->getId()))
		{
			ERROR_MSG("remote sdp read fail.");
			return false;
		}
		image_r = m_sdp_remote.get_media(image_stream->getId());
		if (image_r == nullptr)
		{
			return false;
		}
		const ip_address_t* ipaddr = image_r->get_address();
		if (ipaddr->empty())
		{
			ipaddr = m_sdp_remote.get_address();
		}
		uint16_t port = image_r->get_port();
		socket_address sockaddr(ipaddr, port);
		bool difference_address = !sockaddr.equals(&sockaddr_old);

		mg_rx_stream_t* rx_stream = find_rx_stream_by_type(type_rx, image_stream);
		if (rx_stream == nullptr)
		{
			return false;
		}
		mg_rx_fax_proxy_stream_t* rx_fax_proxy_stream = (mg_rx_fax_proxy_stream_t*)rx_stream;

		mg_tx_stream_t* tx_stream = find_tx_stream_by_type(type_tx, image_stream);
		if (tx_stream == nullptr)
		{
			return false;
		}
		mg_tx_fax_proxy_stream_t* tx_fax_proxy_stream = (mg_tx_fax_proxy_stream_t*)tx_stream;

		rx_stream->setMediaParams(*image_r);
		tx_stream->setMediaParams(*image_r);
	
		if (difference_address)
		{
			image_stream->reset_first_packet_recive_flag();
			tx_fax_proxy_stream->set_remote_address(&sockaddr);
		}

		if (channel->getType() == rtp_channel_type_t::rtp_channel_type_udptl_proxy_e)
		{
			udptl_proxy_channel_t* udptl_proxy_channel = (udptl_proxy_channel_t*)channel;
			rx_fax_proxy_stream->set_channel_event(udptl_proxy_channel);
			udptl_proxy_channel->set_event_handler(rx_fax_proxy_stream);
		}
		else
		{
			return false;
		}

		//-------------------------------------------------------------------------

		return true;
	}
}
//------------------------------------------------------------------------------
