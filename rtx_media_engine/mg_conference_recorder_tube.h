﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_tube.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//class mg_audio_conference_out_tube_t;
////------------------------------------------------------------------------------
//// Труба записи микшированных данных в фаил в rtp формате.
////------------------------------------------------------------------------------
//class mg_conference_record_tube_t : public Tube
//{
//	mg_conference_record_tube_t(const mg_conference_record_tube_t&);
//	mg_conference_record_tube_t& operator=(const mg_conference_record_tube_t&);
//
//public:
//	mg_conference_record_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id);
//	virtual	~mg_conference_record_tube_t();
//
//	bool create_recorder(const char* path, const media::PayloadSet& record_formats);
//
//	virtual bool init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out) override;
//	virtual bool set_out_handler(INextDataHandler* out_handler) override;
//	virtual const uint32_t get_id() const override;
//	virtual const char* getName() const override;
//	virtual const HandlerType getType() const override;
//	//virtual const uint16_t get_payload_id() const override;
//	virtual bool is_suited(uint16_t payload_id) override;
//	virtual bool is_suited(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out) override;
//	virtual void send(const INextDataHandler* out_handler, const rtp_packet* packet) override;
//	virtual bool unsubscribe(const INextDataHandler* out_handler) override;
//
//private:
//	uint32_t					m_id;
//	rtl::String					m_name;
//	uint16_t					m_payload_id;
//
//	//-----------------------------------------------------
//	// Переменные для формирования ртп пакета.
//
//	uint16_t					m_seq_num_to_rtp;			// порядковый номер пакета.
//	uint16_t					m_seq_num_to_rtp_last;		// номер последнего отправленного пакета.
//	uint32_t					m_timestamp_to_rtp;			// таймштамп пакета.
//	uint32_t					m_timestamp_to_rtp_last;	// таймштамп последнего отправленного пакета.
//
//	//-----------------------------------------------------
//
//	media::LazyMediaWriterRTP* m_recorder;
//};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
