﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_stereo_to_mono_tube.h"
#include "rtp_packet.h"
#include "rtx_codec.h"
#include "mg_media_element.h"

#include "logdef_tube.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_stereo_to_mono_tube_t::mg_stereo_to_mono_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id) :
		Tube(HandlerType::MonoResampler, id, log)
	{
		makeName(parent_id, "-stom-tu"); // Stereo To Mono Tube

		//m_out = nullptr;

		rtl::res_counter_t::add_ref(g_mge_tube_counter);

		setLogTag("TUB-MONO");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_stereo_to_mono_tube_t::~mg_stereo_to_mono_tube_t()
	{
		unsubscribe(this);

		rtl::res_counter_t::release(g_mge_tube_counter);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_stereo_to_mono_tube_t::init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out)
	{
		if (payload_el_in == nullptr)
		{
			ERROR_MSG("payload element is null.");
			return false;
		}

		m_payload_id = payload_el_in->get_payload_id();

		PRINT_FMT("payload in: %s %d", payload_el_in->get_element_name(), m_payload_id);

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_stereo_to_mono_tube_t::send(const INextDataHandler* out_handler, const rtp_packet* packet)
	{
		if (packet == nullptr)
		{
			ERROR_MSG("nothing to send!");
			return;
		}

		const uint8_t* buff = packet->get_payload();
		uint32_t payload_length = packet->get_payload_length();
		if (payload_length == 0)
		{
			ERROR_MSG("depacketize failed!");
			return;
		}

		const uint32_t mono_buff_size = rtp_packet::PAYLOAD_BUFFER_SIZE;
		uint8_t mono_buff[mono_buff_size] = { 0 };
		media::mixer__stereo_to_mono_i((const short*)buff, (short*)mono_buff, payload_length / 2);

		rtp_packet packet_new;
		packet_new.copy_from(packet);
		packet_new.set_payload_type((uint8_t)m_payload_id);
		uint32_t read_size = packet_new.set_payload(mono_buff, payload_length / 2);

		if (read_size == 0)
		{
			ERROR_MSG("read packet fail.");
			return;
		}

		INextDataHandler* out = getNextHandler();
		if (out != nullptr)
		{
			out->send(this, &packet_new);
		}
	}
}
//------------------------------------------------------------------------------
