﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg.h"
#include "std_thread.h"
#include "mg_tx_device_stream.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "logdef_stream.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_tx_device_stream_t::mg_tx_device_stream_t(rtl::Logger* log, uint32_t id, uint32_t parent_id, const char* term_id) :
		mg_tx_stream_t(log, id, mg_tx_device_audio_e, term_id)
	{
		rtl::String name;
		name << term_id << "-s" << parent_id << "-txdevs" << id;
		mg_tx_stream_t::setName(name);
		mg_tx_stream_t::setTag("TXS_DEV");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_tx_device_stream_t::~mg_tx_device_stream_t()
	{

	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_tx_device_stream_t::send(const INextDataHandler* out_handler, const rtp_packet* packet)
	{
		if (checkAndLock())
		{
			stat_filtered();
			WARNING("stream is locked!");
			return;
		}

		send_data(packet->get_payload(), packet->get_payload_length(), packet->get_sequence_number());

		releaseLock();
	}
	//------------------------------------------------------------------------------
	void mg_tx_device_stream_t::send(const INextDataHandler* out_handler, const uint8_t* data, uint32_t len)
	{
		if (checkAndLock())
		{
			WARNING("stream is locked!");
			return;
		}

		send_data(data, len, 0);

		releaseLock();
	}
	//------------------------------------------------------------------------------
	void mg_tx_device_stream_t::send(const INextDataHandler* out_handler, const rtcp_packet_t* packet)
	{

	}
	//------------------------------------------------------------------------------
	void mg_tx_device_stream_t::send_data(const uint8_t* data, uint32_t len, uint16_t seq_no)
	{
		if (data == nullptr)
		{
			ERROR_MSG("data is null.");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::Loopback)
		{
			WARNING("stream mode is loopback!");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::Inactive)
		{
			WARNING("stream mode is inactive!");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::RecvOnly)
		{
			WARNING("stream mode is recvonly!");
			return;
		}

		if (mge::g_media_gateway != nullptr)
		{
			h248::IAudioDeviceOutput* spk = mge::g_media_gateway->getDeviceOut();
			if (spk != nullptr)
			{
				spk->deviceOutput__playAudio(0, data, len, seq_no);
			}
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	int mg_tx_device_stream_t::rtcp_session_tx(const rtcp_packet_t* packet)
	{
		return 1;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_tx_device_stream_t::lock()
	{
		if (isLocked())
		{
			WARNING("main lock already lock!");
			return true;
		}

		bool locked = mg_tx_stream_t::lock();
		if (locked)
		{
			if (mge::g_media_gateway != nullptr)
			{
				h248::IAudioDeviceOutput* spk = mge::g_media_gateway->getDeviceOut();
				if (spk != nullptr)
				{
					spk->deviceOutput__removeHandler(0);
				}
			}
		}

		return locked;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_tx_device_stream_t::unlock()
	{
		if (isLocked())
		{
			mg_tx_stream_t::unlock();

			if (mge::g_media_gateway != nullptr)
			{
				h248::IAudioDeviceOutput* spk = mge::g_media_gateway->getDeviceOut();
				if (spk != nullptr)
				{
					// uintptr_t dev_id = 
					spk->deviceOutput__addHandler(8000, 1);
				}
			}
		}
	}
}
//------------------------------------------------------------------------------
