﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_tube.h"

namespace mge
{
	//------------------------------------------------------------------------------
	// Труба трансодинга.
	// На входе стрим. На выходе туб.
	// На входе кодированные данные. На выходе pcm.
	//------------------------------------------------------------------------------
	class ME_DEBUG_API mg_codec_to_pcm_tube_t : public Tube
	{
		mg_codec_to_pcm_tube_t(const mg_codec_to_pcm_tube_t&);
		mg_codec_to_pcm_tube_t&	operator=(const mg_codec_to_pcm_tube_t&);

	public:
		mg_codec_to_pcm_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id);
		virtual	~mg_codec_to_pcm_tube_t();

		virtual bool init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out) override;
		virtual void send(const INextDataHandler* out_handler, const rtp_packet* packet) override;

	private:
		mg_audio_decoder_t*			m_decoder;
		uint8_t*					m_codec_buffer;
		uint32_t					m_codec_buffer_read;
	};
}
//------------------------------------------------------------------------------
