﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_audio_conference_ctrl.h"
#include "mg_audio_conference_in_tube.h"
#include "mg_audio_conference_out_tube.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define LOG_PREFIX "ACONF"
#define MAX_CONFERENCE_TERMINATION_COUNT 512

#include "logdef.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define TIMER_LOGGING  m_log && rtl::Logger::check_trace(TRF_FLAG3)
#define PLOG_TIMER_WRITE		if (TIMER_LOGGING)	m_log->log
#define PLOG_TIMER_WARNING		if (TIMER_LOGGING)	m_log->log_warning
#define PLOG_TIMER_ERROR		if (TIMER_LOGGING)	m_log->log_error

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	const double g_mixcoefmax = 32767.0;
	const double g_mixcoef0 = 32769.0;
	const double g_mixcoef1 = 1.0 / g_mixcoef0;
	const double g_mixcoef2 = 1.0 / g_mixcoefmax;
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	AudioConferenceController::AudioConferenceController(rtl::Logger* log, const char* parent_id) :
		media::Metronome(*log), m_log(log)
	{
		m_name << parent_id << "-aconf-mixer"; // Conference Mixer

		setFirst(nullptr);

		m_start_time = 0;
		m_tick_counter = 0;
		m_max_processing_time = 0;
		m_mixer_work = false;
		m_mixer_counter = 0;

		m_mixer = NEW AudioMixerMember[MAX_CONFERENCE_TERMINATION_COUNT];
		memset(m_mixer, 0, sizeof(AudioMixerMember) * MAX_CONFERENCE_TERMINATION_COUNT);

		m_audio_frame_size = 160;
		m_ptime = 20;
		m_mixalgorithm = 3;
		m_frequency = 8000;

		m_member_first = nullptr;
		m_member_last = nullptr;

		m_lock.clear();

		m_recorder = nullptr;

		rtl::res_counter_t::add_ref(g_mge_tube_counter);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	AudioConferenceController::~AudioConferenceController()
	{
		metronomeStop();

		setFirst(nullptr);

		bool locked = lock();

		for (int index = 0; index < m_member_list.getCount(); index++)
		{
			mg_conference_member_t* member = m_member_list.getAt(index);
			if (member == nullptr)
			{
				continue;
			}

			DELETEO(member);
			member = nullptr;
		}
		m_member_list.clear();

		if (locked)
		{
			unlock();
		}

		if (m_mixer != nullptr)
		{
			DELETEAR(m_mixer);
			m_mixer = nullptr;
		}

		if (m_recorder != nullptr)
		{
			m_recorder->unsubscribe(m_recorder);
			m_recorder = nullptr;
		}

		rtl::res_counter_t::release(g_mge_tube_counter);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool AudioConferenceController::init(uint32_t sample_rate, uint32_t ptime, uint32_t mixing_algorithm)
	{
		m_frequency = sample_rate;
		m_ptime = ptime;
		m_audio_frame_size = m_ptime * m_frequency / 1000;
		m_mixalgorithm = int(mixing_algorithm);

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void AudioConferenceController::metronomeTicks()
	{
		mg_conference_member_t* current_member = getFirst();
		if (current_member == nullptr)
		{
			return;
		}

		uint32_t currentstart = rtl::DateTime::getTicks();
		uint32_t current = currentstart;
		int currentTime = int(current - m_start_time);
		int idealTime = int(m_tick_counter * m_ptime);
		int counter = 0;
		int maxcounter = 3;
		bool while_do = true;

		PLOG_TIMER_WRITE(LOG_PREFIX, "ID(%s) : metronomeTicks() : tick:%u current:%u ideal:%u diff:%d",
			(const char*)m_name, current, currentTime, idealTime, int(currentTime - idealTime));

		do
		{
			m_mixer_work = true;
			exchangeVoices();
			m_mixer_work = false;
			m_mixer_counter++;

			m_tick_counter++;
			counter++;

			idealTime += m_ptime;

			// peter 17.07.2012 
			//   пока работали - время прошло. нужно не спать уходить (плюс к разрешающей способности таймера 
			//   еще производилась обработка нескольких дополнительных пакетов, возможно долго обработка идет (близко к 20мс)
			//   и таким образом можем пропустить 1-2 пакета в джиттере, как в музенидисе. там получалось, что время обработки
			//   ~17мс, да задержки таймера - отрабатываем три пакета и уходим в сон, а нужно еще один пришедший пакет обработать, он уже в свое время идет)
			current = rtl::DateTime::getTicks();
			currentTime = int(current - m_start_time);
			int x = (current - currentstart) / m_ptime;
			maxcounter = 3 + x;

			while_do = (currentTime - idealTime > m_ptime) && counter < maxcounter;

			PLOG_TIMER_WRITE(LOG_PREFIX, "ID(%s) : metronomeTicks() : tick:%u current:%u ideal:%u diff:%d while_do:%s",
				(const char*)m_name, current, currentTime, idealTime, int(currentTime - idealTime), STR_BOOL(while_do));

		} while (while_do);

		if (counter >= maxcounter)
		{
			m_tick_counter = currentTime / m_ptime + 1;

			int skippedcnt = (currentTime - idealTime) / m_ptime;
			PLOG_TIMER_WRITE(LOG_PREFIX, "ID(%s) : metronomeTicks() : Timer correction skipped %d ticks", (const char*)m_name, skippedcnt);
		}

		uint32_t duration = (rtl::DateTime::getTicks() - currentstart) / counter;

		if (duration > m_max_processing_time)
		{
			m_max_processing_time = duration;

			PLOG_TIMER_WRITE(LOG_PREFIX, "ID(%s) : metronomeTicks() : Timer tick max processing time %u ms", (const char*)m_name, m_max_processing_time);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void AudioConferenceController::exchangeVoices()
	{
		mg_conference_member_t* current_member = getFirst();
		if (current_member == nullptr)
		{
			//PLOG_TUBE_WRITE(LOG_PREFIX, "exchangeVoices -- ID(%s): getFirst null.", (const char*)m_name);
			return;
		}

		// сбор пакетов
		int mix_count = 0;

		while (current_member != nullptr)
		{
			current_member->setWork();
			if (current_member->isActive())
			{
				mg_audio_conference_in_tube_t* in_tube = current_member->getTubeIn();
				//PLOG_TUBE_WRITE(LOG_PREFIX, "exchangeVoices -- ID(%s): member -- tube in: %s.", (const char*)m_name,
				//(in_tube == nullptr) ? "-" : in_tube->getName());

				const short* samples = nullptr;
				if (in_tube != nullptr)
				{
					samples = (const short*)in_tube->get_media();
					if ((in_tube->get_subscribers_count() == 0) && (samples == nullptr))
					{
						in_tube->deleted_from_conference();
						current_member->detachTubeIn();
						in_tube = nullptr;
					}
				}

				m_mixer[mix_count].enabled = true;
				m_mixer[mix_count].member = current_member;
				m_mixer[mix_count++].samples = samples;

				if (samples == nullptr)
				{
					current_member->addReceiveError();
				}
			}
			current_member->setWork(false);
			current_member = current_member->getNext();
		}

		if (mix_count > 0)
		{
			//PLOG_TUBE_WRITE(LOG_PREFIX, "exchangeVoices -- ID(%s): mix_count: %d.", (const char*)m_name, mix_count);

			AudioMixer mixer(m_audio_frame_size, m_mixalgorithm);

			//нужно сложить все в микшере, вернуть сумму неприведенную и количество сложенных элементов. оно может быть меньше mix_count.
			int sumcount = 0;
			//double* sumvoice = NEW double[m_audio_frame_size];
			memset(m_sumvoice_buff, 0, m_sumvoice_buff_size * sizeof(double));
			sumcount = mixer.mix_samples(m_mixer, mix_count, m_sumvoice_buff);

			// раздача голоса по каналам
			for (int i = 0; i < mix_count; i++)
			{
				mg_conference_member_t* member = (mg_conference_member_t*)m_mixer[i].member;
				if ((member == nullptr) || (member->getTubeOut() == nullptr))
				{
					continue;
				}

				member->setWork();
				if (!member->getConf()->send_mixing_data(m_mixer, mix_count, m_sumvoice_buff, sumcount))
				{
					member->addTransmissionError();
				}
				member->setWork(false);
			}

			// передаем микшированные данные дальше (рекордер туб)
			if (m_recorder == nullptr)
			{
				//PLOG_TUBE_ERROR(LOG_PREFIX, "exchangeVoices -- ID(%s):  recorder is null.", (const char*)m_name);
				return;
			}

			uint8_t buff[rtp_packet::PAYLOAD_BUFFER_SIZE] = { 0 };
			if (m_audio_frame_size > rtp_packet::PAYLOAD_BUFFER_SIZE / 2)
			{
				ERROR_MSG("big data.");
				return;
			}
			mixer.cvt_samples(m_sumvoice_buff, (int16_t*)buff);

			rtp_packet packet;
			uint32_t size = packet.set_payload(buff, m_audio_frame_size * sizeof(int16_t));
			if (size == 0)
			{
				packet.set_payload_type(13);
			}
			packet.set_samples(m_audio_frame_size);

			m_recorder->send(m_recorder, &packet);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool AudioConferenceController::addMember(Tube* in_tube, Tube* out_tube, const char* term_id)
	{
		if ((in_tube == nullptr) && (out_tube == nullptr))
		{
			WARNING("conference tubes is null!");
			return false;
		}

		mg_audio_conference_in_tube_t* conference_in_tube = (mg_audio_conference_in_tube_t*)in_tube;
		mg_audio_conference_out_tube_t* conference_out_tube = (mg_audio_conference_out_tube_t*)out_tube;

		if (conference_in_tube != nullptr)
		{
			conference_in_tube->set_termination_name(term_id);
		}
		if (conference_out_tube != nullptr)
		{
			conference_out_tube->set_termination_name(term_id);
		}

		mg_conference_member_t* member = nullptr;
		int result = contains(conference_in_tube, conference_out_tube, &member);
		if (result >= 0)
		{
			WARNING("already exist");
			if (result == 1)
			{
				while (member->inWork())
				{
					rtl::Thread::sleep(1);
				}
				member->setTubeOut(conference_out_tube);
				member->activate();
				member->resetErrors();
			}
			else if (result == 2)
			{
				while (member->inWork())
				{
					rtl::Thread::sleep(1);
				}
				member->setTubeIn(conference_in_tube);
				if (conference_in_tube != nullptr)
				{
					conference_in_tube->add_to_conference();
				}
				member->activate();
				member->resetErrors();
			}
			else
			{
				while (member->inWork())
				{
					rtl::Thread::sleep(1);
				}
				member->activate();
				member->resetErrors();
			}
			return true;
		}

		PRINT_FMT("tube in: %s, tube out: %s",
			(in_tube == nullptr) ? "-" : in_tube->getName(),
			(out_tube == nullptr) ? "-" : out_tube->getName());

		mg_conference_member_t* new_member = NEW mg_conference_member_t();
		new_member->setTubeIn(conference_in_tube);
		if (conference_in_tube != nullptr)
		{
			conference_in_tube->add_to_conference();
		}
		new_member->setTubeOut(conference_out_tube);
		new_member->setNext(nullptr);
		new_member->deactivate();
		new_member->setWork(false);
		new_member->resetErrors();

		if (lock())
		{
			m_member_list.add(new_member);

			unlock();

			if (m_member_list.getCount() == 1)
			{
				m_start_time = rtl::DateTime::getTicks();
				m_tick_counter = 0;
			}

			new_member->activate();

			setNext(new_member);

			RETURN_BOOL(true);
		}

		DELETEO(new_member);

		WARNING("timeout!");
		
		return false;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	int AudioConferenceController::contains(mg_audio_conference_in_tube_t* in_tube, mg_audio_conference_out_tube_t* out_tube, mg_conference_member_t** member)
	{
		for (int i = 0; i < m_member_list.getCount(); i++)
		{
			mg_conference_member_t* member_at_list = m_member_list.getAt(i);
			if (member_at_list == nullptr)
			{
				continue;
			}

			if ((in_tube != nullptr) && (out_tube != nullptr))
			{
				if ((member_at_list->getTubeIn() == in_tube) && (member_at_list->getTubeOut() == out_tube))
				{
					*member = member_at_list;
					return 0;
				}
				else if ((member_at_list->getTubeIn() == in_tube) && (member_at_list->getTubeOut() == nullptr))
				{
					*member = member_at_list;
					return 1;
				}
				else if ((member_at_list->getTubeIn() == nullptr) && (member_at_list->getTubeOut() == out_tube))
				{
					*member = member_at_list;
					return 2;
				}
			}
			else if (in_tube != nullptr)
			{
				if (member_at_list->getTubeIn() == in_tube)
				{
					*member = member_at_list;
					return 1;
				}
			}
			else if (out_tube != nullptr)
			{
				if (member_at_list->getTubeOut() == out_tube)
				{
					*member = member_at_list;
					return 2;
				}
			}
		}

		return -1;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool AudioConferenceController::lock()
	{
		uint32_t time_diff = 0;
		while (time_diff <= 100)
		{
			if (m_lock.test_and_set(std::memory_order_acquire))
			{
				rtl::Thread::sleep(5);
				time_diff += 5;
				continue;
			}

			PRINT("conference locked");
			return true;
		}

		WARNING("conference not locked");

		return false;
	}
	//------------------------------------------------------------------------------
	void AudioConferenceController::unlock()
	{
		m_lock.clear(std::memory_order_release);
		
		PRINT("stream unlocked");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_conference_member_t* AudioConferenceController::getFirst()
	{
		return m_member_first;
	}
	//------------------------------------------------------------------------------
	void AudioConferenceController::setNext(mg_conference_member_t* member)
	{
		if (m_member_last == nullptr)
		{
			setFirst(member);
			m_member_last = getFirst();

			//PLOG_TUBE_WRITE(LOG_PREFIX, "add_conference_member -- ID(%s): setFirst -- tube in: %s, tube out: %s.",
			//	(const char*)m_name,
			//	(member->in_tube == nullptr) ? "-" : member->in_tube->getName(),
			//	(member->out_tube == nullptr) ? "-" : member->out_tube->getName());
		}
		else
		{
			//PLOG_TUBE_WRITE(LOG_PREFIX, "add_conference_member -- ID(%s): setNext_prev -- tube in: %s, tube out: %s.",
			//	(const char*)m_name,
			//	(m_member_last->in_tube == nullptr) ? "-" : m_member_last->in_tube->getName(),
			//	(m_member_last->out_tube == nullptr) ? "-" : m_member_last->out_tube->getName());

			m_member_last->setNext(member);
			m_member_last = member;

			/*PLOG_TUBE_WRITE(LOG_PREFIX, "add_conference_member -- ID(%s): setNext -- tube in: %s, tube out: %s.",
				(const char*)m_name,
				(member->in_tube == nullptr) ? "-" : member->in_tube->getName(),
				(member->out_tube == nullptr) ? "-" : member->out_tube->getName());*/
		}
	}
	//------------------------------------------------------------------------------
	void AudioConferenceController::setFirst(mg_conference_member_t* member)
	{
		bool start = (m_member_first == nullptr);

		m_member_first = member;

		if (start)
		{
			if (!metronomeStart(m_ptime, true))
			{
				ERROR_MSG("metronomeStart fail");
			}
			else
			{
				PRINT("timer started");
			}
		}

		if (m_member_first == nullptr)
		{
			metronomeStop();
			PRINT("timer stopped");
		}
	}
}
//-----------------------------------------------
