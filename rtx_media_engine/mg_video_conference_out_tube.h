﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_tube.h"
#include "mmt_video_frame.h"

namespace mge
{
	//-----------------------------------------------
	// Труба исходящих данных от микшера конференции.
	// Поток микшера приходит в туб, пишет микшированные данные в буфер.
	// Микширует данные еще раз (убирает голос текущего termination'a и 
	// голоса termination'ов ненужные по топологии).
	// Если следующий туб транскодига "тяжелый" то вызывает асинхронный вызов и уходит.
	// В обратном случае сам передает данные дальше.
	//-----------------------------------------------
	class mg_video_conference_out_tube_t : public Tube
	{
	public:
		mg_video_conference_out_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id);
		virtual	~mg_video_conference_out_tube_t();

		virtual bool init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out) override;
		virtual bool is_suited(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out) override;

		// Добавление учатсника конференции в черный список. (Которого не будет слышно в рамках текущего туба).
		void add_except_termination_id(const char* term_id);
		void remove_except_termination_id(const char* term_id);

		void set_termination_name(const char* termination_id) { m_termination_id = termination_id; }
		const char* get_termination_name() { return m_termination_id; }

		const media::PayloadFormat& getFormat() const { return m_payload; }

		void changeTopologyFrom(const char* termId, h248::TopologyDirection topology);
		void changeTopologyTo(const char* termId, h248::TopologyDirection topology);

	private:
		// Проверка в списке исключений.
		bool is_permissible_source(const char* source);

	private:
		media::PayloadFormat m_payload;
		rtl::String m_termination_id;
		rtl::Mutex m_except_sync;
		rtl::StringList m_except_list;						// Список участников коференции которых не должен видеть текущий.
	};
}
//-----------------------------------------------
