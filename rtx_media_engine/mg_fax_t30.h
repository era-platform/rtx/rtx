﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "mg_fax.h"
//------------------------------------------------------------------------------
// file_mask - Путь до файла. (все каталоги должны быть созданы, создается только сам фаил .tiff)
//------------------------------------------------------------------------------
bool mg_initialize_fax_receiver_t30(mg_fax_t* fax, const char* file_mask);
//------------------------------------------------------------------------------
// file_name - Путь до айла .tiff
// ecm - error correction mode
//------------------------------------------------------------------------------
bool mg_initialize_fax_transmitter_t30(mg_fax_t* fax, const char* file_name, bool ecm);
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class mg_fax_t30_t : public mg_fax_t
{
	mg_fax_t30_t(const mg_fax_t30_t&);
	mg_fax_t30_t& operator=(const mg_fax_t30_t&);

public:
	mg_fax_t30_t(rtl::Logger* log);
	~mg_fax_t30_t();

	virtual void destroy();

	void set_type(bool receiver);

	virtual FAX_SIGNALS get_fax_signal_state(bool stop_signaling);

	void phase_b_rx(t30_state_t *s, int result);
	void phase_d_rx(t30_state_t *s, int result);
	void phase_e_rx(t30_state_t *s, int completion_code);

	void phase_b_tx(t30_state_t *s, int result);
	void phase_d_tx(t30_state_t *s, int result);
	void phase_e_tx(t30_state_t *s, int completion_code);

	virtual uint32_t write_fax(const uint8_t* data, uint32_t length);
	virtual uint32_t read_fax(uint8_t* data, uint32_t length);

	fax_state_t* get_fax();
	void set_fax(fax_state_t* fax);

private:
	rtl::Logger* m_log;

	fax_state_t* m_fax_t4;

	bool m_receiver;
	int m_pages_received;
	int m_pages_transmitted;
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
