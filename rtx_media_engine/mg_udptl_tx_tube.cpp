﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_udptl_tx_tube.h"
#include "mg_media_element.h"
#include "mg_fax_t38.h"

#include "logdef_tube.h"

 //--------------------------------------
//
//--------------------------------------
#define UDPTL_ECM_BUFFER_SIZE 16
#define UDPTL_PACKET_SIZE 1400
#define UDPTL_BUF_MASK 0x0000000F

namespace mge
{
	//--------------------------------------
	//
	//--------------------------------------
	mg_udptl_tx_tube_t::mg_udptl_tx_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id) :
		Tube(HandlerType::UDPTL_Transmitter, id, log),
		media::Metronome(*log)
	{
		makeName(parent_id, "-udptltxtu"); // Simple Tube

		rtl::res_counter_t::add_ref(g_mge_tube_counter);

		setLogTag("TUB_TXFX");

		m_tx_seq_no = 0;
		m_tx_buffer = (udptl_tx_frame_t*)MALLOC(sizeof(udptl_tx_frame_t) * UDPTL_ECM_BUFFER_SIZE);
		memset(m_tx_buffer, 0, sizeof(udptl_tx_frame_t) * UDPTL_ECM_BUFFER_SIZE);
		m_tx_ecm_entries = 1;
		m_tx_ecm_span = 3;
		m_tx_ecm_mode = t38_ecm_mode_t::None;

		memset(m_packets, 0, sizeof(udptl_tx_transmission_packet_t) * 5);
		m_packet_write = 0;

		m_lock.clear();
	}
	//--------------------------------------
	//
	//--------------------------------------
	mg_udptl_tx_tube_t::~mg_udptl_tx_tube_t()
	{
		metronomeStop();

		if (m_tx_buffer != nullptr)
		{
			DELETEAR(m_tx_buffer);
			m_tx_buffer = nullptr;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void mg_udptl_tx_tube_t::set_tx_ecm_mode(t38_ecm_mode_t mode, int entries, int span)
	{
		m_tx_ecm_mode = mode;
		if (entries >= 0)
			m_tx_ecm_entries = entries < 1 ? 1 : entries > 4 ? 4 : entries;
		if (span >= 0)
			m_tx_ecm_span = span < 2 ? 2 : span > 4 ? 4 : span;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void mg_udptl_tx_tube_t::set_tx_ecm_span(int span)
	{
		m_tx_ecm_span = span < 2 ? 2 : span > 4 ? 4 : span;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void mg_udptl_tx_tube_t::set_tx_ecm_entries(int entries)
	{
		m_tx_ecm_entries = entries < 1 ? 1 : entries > 5 ? 5 : entries;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void mg_udptl_tx_tube_t::send(const INextDataHandler* out_handler, const rtp_packet* packet)
	{
		//...
	}
	//--------------------------------------
	//
	//--------------------------------------
	void mg_udptl_tx_tube_t::send(const INextDataHandler* out_handler, const uint8_t* data, uint32_t length)
	{
		if (data == nullptr)
		{
			return;
		}

		if (lock())
		{
			int transmissions = data[0];
			if (m_packet_write == 0)
			{
				build_udptl_packet(data + 1, length - 1);

				if (transmissions > 1)
				{
					uint32_t write = (udptl_tx_transmission_packet_t::block_size) > (length - 1) ? (length - 1) : (udptl_tx_transmission_packet_t::block_size);
					memcpy(m_packets[m_packet_write].buff, data + 1, write);
					m_packets[m_packet_write].length = write;
					m_packets[m_packet_write].transmissions = transmissions - 1;
					m_packet_write++;

					if (!isMetronomeStarted())
					{
						metronomeStart(10, false);
					}
					metronomeResume();
				}
			}
			else
			{
				if (m_packet_write + 1 >= 5)
				{
					unlock();
					return;
				}

				uint32_t write = (udptl_tx_transmission_packet_t::block_size) > (length - 1) ? (length - 1) : (udptl_tx_transmission_packet_t::block_size);
				memcpy(m_packets[m_packet_write].buff, data + 1, write);
				m_packets[m_packet_write].length = write;
				m_packets[m_packet_write].transmissions = transmissions;
				m_packet_write++;
			}

			unlock();
		}
		else
		{
			if (g_mg_fax_log != nullptr)
			{
				g_mg_fax_log->log("MGFT38", "build_udptl_packet -- lock: length:%d.", length);
			}
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	static int write_frame_length(uint8_t* buffer, int* index, int value)
	{
		// 7 bits (127) |0xxx xxxx|
		if (value < 0x80)
		{
			buffer[*index] = value;
			(*index)++;
			return value;
		}

		// 14 bits (16384) |10xx xxxx xxxx xxxx|
		if (value < 0x4000)
		{
			buffer[*index] = ((0x8000 | value) >> 8) & 0xFF;
			(*index)++;
			buffer[*index] = value & 0xFF;
			(*index)++;
			return value;
		}

		// 6 bits (fragmentation) |11xx xxxx|
		int multiplier = (value < 0x10000) ? (value >> 14) : 4;

		buffer[*index] = 0xC0 | multiplier;
		(*index)++;

		return multiplier << 14;
	}
	//--------------------------------------
	//
	//--------------------------------------
	static bool write_frame(uint8_t *packet, int length, int *index, const uint8_t *ifp, int ifp_len)
	{
		int written;
		uint8_t zero_byte;

		/* If open type is of zero length, add a single zero byte (10.1) */
		if (ifp_len == 0)
		{
			zero_byte = 0;
			ifp = &zero_byte;
			ifp_len = 1;
		}
		/* Encode the open type */
		for (int octet_idx = 0;; ifp_len -= written, octet_idx += written)
		{
			if ((written = write_frame_length(packet, index, ifp_len)) < 0)
				return false;

			if (written + *index > length)
			{
				return false;
			}

			if (written > 0)
			{
				memcpy(&packet[*index], &ifp[octet_idx], written);
				*index += written;
			}

			if (written >= ifp_len)
			{
				return true;
			}
		}

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int mg_udptl_tx_tube_t::build_tx_packet(uint8_t* buffer, int size, const uint8_t* ifp, int ifp_len)
	{
		if (ifp == nullptr || ifp_len < 0 || ifp_len > UDPTL_PACKET_SIZE - 5)
		{
			//WARNING_FMT("TX packet failed : primary IFP packet length has wrong length(%d)", ifp_len);
			return -1;
		}

		int length = 0;

		// write sequence nuber
		buffer[length++] = uint8_t(m_tx_seq_no >> 8);
		buffer[length++] = uint8_t(m_tx_seq_no & 0xFF);

		// write IFP packet
		if (!write_frame(buffer, size, &length, ifp, ifp_len))
		{
			//WARNING("TX packet failed : primary IFP packet building failed!");
			return -1;
		}

		buffer[length++] = 0x00;
		buffer[length++] = 0x00;

		m_tx_seq_no++;

		return length;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int mg_udptl_tx_tube_t::build_tx_packet_red(uint8_t* buffer, int size, const uint8_t* ifp, int ifp_length)
	{
		int length = 0;
		int entry = m_tx_seq_no & UDPTL_BUF_MASK;

		m_tx_buffer[entry].ifp_length = ifp_length;
		memcpy(m_tx_buffer[entry].ifp, ifp, ifp_length);

		// write sequence number
		buffer[length++] = uint8_t(m_tx_seq_no >> 8);
		buffer[length++] = uint8_t(m_tx_seq_no & 0xFF);

		// write top (primary) IFP packet
		if (!write_frame(buffer, size, &length, ifp, ifp_length))
		{
			//WARNING("TX packet failed : primary IFP packet building failed!");
			return -1;
		}

		// write type of error recovery information
		buffer[length++] = 0;
		if (write_frame_length(buffer, &length, m_tx_ecm_entries) < 0)
			return -1;

		// write secondary ifp packets

		for (int i = 1; i <= m_tx_ecm_entries; i++)
		{
			int j = (entry - i) & UDPTL_BUF_MASK;
			udptl_tx_frame_t* frame = &m_tx_buffer[j];

			if (!write_frame(buffer, size, &length, frame->ifp, frame->ifp_length))
			{
				return -1;
			}
		}

		m_tx_seq_no++;

		return length;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int mg_udptl_tx_tube_t::build_tx_packet_fec(uint8_t* buffer, int size, const uint8_t* ifp, int ifp_length)
	{
		int		length = 0;
		uint8_t	fec[UDPTL_PACKET_SIZE];
		int		high_tide;
		int		limit;
		int		entry = m_tx_seq_no & UDPTL_BUF_MASK;

		m_tx_buffer[entry].ifp_length = ifp_length;
		memcpy(m_tx_buffer[entry].ifp, ifp, ifp_length);

		// write sequence number
		buffer[length++] = uint8_t(m_tx_seq_no >> 8);
		buffer[length++] = uint8_t(m_tx_seq_no & 0xFF);

		// write top (primary) IFP packet
		if (!write_frame(buffer, size, &length, ifp, ifp_length))
		{
			//WARNING("TX packet failed : primary IFP packet building failed!");
			return -1;
		}

		// prepare parameters
		int span = m_tx_ecm_span;
		int	entries = m_tx_ecm_entries;

		if (m_tx_seq_no < m_tx_ecm_span * m_tx_ecm_entries)
		{
			// In the initial stages, wind up the FEC smoothly
			entries = m_tx_seq_no / m_tx_ecm_span;
			if (m_tx_seq_no < m_tx_ecm_span)
				span = 0;
		}
		// write type of error recovery information
		buffer[length++] = 0x80;
		buffer[length++] = 1;
		buffer[length++] = (uint8_t)span;
		buffer[length++] = (uint8_t)entries;

		// make recovery information
		for (int m = 0; m < entries; m++)
		{
			// Make an XOR'ed entry the maximum length */
			limit = (entry + m) & UDPTL_BUF_MASK;
			high_tide = 0;
			for (int i = (limit - span * entries) & UDPTL_BUF_MASK; i != limit; i = (i + entries) & UDPTL_BUF_MASK)
			{
				if (high_tide < m_tx_buffer[i].ifp_length)
				{
					int j = 0;

					for (; j < high_tide; j++)
						fec[j] ^= m_tx_buffer[i].ifp[j];

					for (; j < m_tx_buffer[i].ifp_length; j++)
						fec[j] = m_tx_buffer[i].ifp[j];
					high_tide = m_tx_buffer[i].ifp_length;
				}
				else
				{
					for (int j = 0; j < m_tx_buffer[i].ifp_length; j++)
						fec[j] ^= m_tx_buffer[i].ifp[j];
				}
			}

			if (!write_frame(buffer, size, &length, fec, high_tide))
				return -1;
		}

		m_tx_seq_no++;

		return length;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_udptl_tx_tube_t::build_udptl_packet(const uint8_t* data, uint32_t length)
	{
		/* Build the UDPTLPacket */
		uint8_t udptl[UDPTL_PACKET_SIZE] = { 0 };

		int udptl_length = 0;

		switch (m_tx_ecm_mode)
		{
		case t38_ecm_mode_t::None:
			udptl_length = build_tx_packet(udptl, UDPTL_PACKET_SIZE, data, length);
			break;
		case t38_ecm_mode_t::Redundancy:
			udptl_length = build_tx_packet_red(udptl, UDPTL_PACKET_SIZE, data, length);
			break;
		case t38_ecm_mode_t::FEC:
			udptl_length = build_tx_packet_fec(udptl, UDPTL_PACKET_SIZE, data, length);
			break;
		}

		if (udptl_length > 0)
		{
			// трасировка
			//m_log.log_binary("UDPTLS", udptl, udptl_length, "stream %s : SEND UDPTL sn:%u len:%d", m_name, m_tx_seq_no-1, udptl_length);
			// отправим пакет дальше
			INextDataHandler* out = getNextHandler();
			if (out != nullptr)
			{
				out->send(this, udptl, udptl_length);
				if (g_mg_fax_log != nullptr)
				{
					g_mg_fax_log->log("MGFT38", "build_udptl_packet -- send: udptl_length:%d.", udptl_length);
				}
			}
		}
		//else
		//{
		//	WARNING_FMT("Packet not sent : building tx packet failed : length %d", udptl_length);
		//}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_udptl_tx_tube_t::init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out)
	{
		if (payload_el_in == nullptr)
		{
			ERROR_MSG("payload element is null.");
			return false;
		}
		m_payload_id = payload_el_in->get_payload_id();
		PRINT_FMT("payload in: %s %d", payload_el_in->get_element_name(), m_payload_id);

		mg_fax_t38_params_t* param = payload_el_in->get_fax_param();
		if (param == nullptr)
		{
			return false;
		}

		if (param->udp_ec == 0)
		{
			set_tx_ecm_mode(t38_ecm_mode_t::None);
		}
		else if (param->udp_ec == 1)
		{
			set_tx_ecm_mode(t38_ecm_mode_t::Redundancy);
		}
		else if (param->udp_ec == 2)
		{
			set_tx_ecm_mode(t38_ecm_mode_t::FEC);
		}

		set_tx_ecm_entries(param->ecm_entries);

		set_tx_ecm_span(param->ecm_span);

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_udptl_tx_tube_t::metronomeTicks()
	{
		if (lock())
		{
			if (m_packet_write == 0)
			{
				unlock();
				metronomePause();
				return;
			}

			int transmissions = m_packets[0].transmissions;

			if (m_tx_seq_no > 0)
			{
				m_tx_seq_no--;
			}

			build_udptl_packet(m_packets[0].buff, m_packets[0].length);

			transmissions--;

			if (transmissions == 0)
			{
				bool send = true;
				while (send)
				{
					m_packet_write--;

					uint32_t copy = ((m_packet_write) <= 0 ? 0 : (m_packet_write)) * sizeof(udptl_tx_transmission_packet_t);
					if (copy == 0)
					{
						send = false;
						break;
					}
					memcpy(m_packets, m_packets + 1, copy);

					if (m_packet_write != 0)
					{
						if (m_packets[0].transmissions == 1)
						{
							build_udptl_packet(m_packets[0].buff, m_packets[0].length);
						}
						else
						{
							send = false;
							break;
						}
					}
					else
					{
						send = false;
						break;
					}
				}
			}
			else
			{
				m_packets[0].transmissions = transmissions;
			}

			unlock();
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_udptl_tx_tube_t::lock()
	{
		uint32_t time_diff = 0;
		while (time_diff <= 5)
		{
			if (m_lock.test_and_set(std::memory_order_acquire))
			{
				rtl::Thread::sleep(1);
				time_diff += 1;
				continue;
			}

			return true;
		}

		return false;
	}
	//------------------------------------------------------------------------------
	void mg_udptl_tx_tube_t::unlock()
	{
		m_lock.clear(std::memory_order_release);
	}
}
//------------------------------------------------------------------------------
