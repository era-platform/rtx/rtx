﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_audio_conference_out_tube.h"
#include "mg_media_element.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "logdef_tube.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_conference_member_t::mg_conference_member_t()
	{
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_conference_member_t::~mg_conference_member_t()
	{
		if (in_tube != nullptr)
		{
			in_tube->unsubscribe(in_tube);
		}
		if (out_tube != nullptr)
		{
			out_tube->unsubscribe(out_tube);
		}

		in_tube = nullptr;
		out_tube = nullptr;
	}
	const char* mg_conference_member_t::get_termination_name()
	{
		return out_tube != nullptr ?
			out_tube->get_termination_name() :
			in_tube != nullptr ?
			in_tube->get_termination_name() :
			nullptr;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_conference_t* mg_conference_member_t::getConf()
	{
		return out_tube ? out_tube->get_conference() : nullptr;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_conference_t::mg_conference_t(rtl::Logger* log, mg_audio_conference_out_tube_t* owner) : m_log(log), m_owner(owner)
	{
		m_hard = false;
		m_samples_count = 160;
		m_is_permissible = false;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_conference_t::~mg_conference_t()
	{
		m_except_list.clear();
		m_freelock_buffer.reset();
		m_samples_count = 0;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_conference_t::initialize(int frameSize, bool hard)
	{
		m_samples_count = frameSize;
		m_hard = hard;
		m_freelock_buffer.create(MaxFrameSize * 6 * sizeof(short));
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_conference_t::update_frame_size(uint32_t mixer_frame_size)
	{
		m_mixer_samples_count = mixer_frame_size;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	const char* mg_conference_t::getName()
	{
		return m_owner->getName();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	const char* mg_conference_t::getLogTag()
	{
		return m_owner->getLogTag();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	int mg_conference_t::readData(uint16_t* buffer, int bufferSize)
	{
		int size = m_samples_count * sizeof(uint16_t);
		const int size_max = MaxFrameSize * sizeof(uint16_t);

		if (size > size_max)
		{
			size = size_max;
		}

		return m_freelock_buffer.read((uint8_t*)buffer, size);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_conference_t::send_mixing_data(AudioMixerMember* members, int members_count, const double* mixed_voice, int mixed_count)
	{
		//INextDataHandler* out = get_out();
		//if (out == nullptr)
		//{
		//	PLOG_TUBE_WARNING(LOG_PREFIX, "send_mixing_data -- ID(%s): out handler is null.", getName());
		//	return false;
		//}

		int real_sources = 0;

		// проставим флаги разрешений
		for (int j = 0; j < members_count; j++)
		{
			AudioMixerMember member = members[j];
			mg_conference_member_t* m = (mg_conference_member_t*)member.member;

			member.enabled = is_permissible_source(m);
			if (member.samples == nullptr)
			{
				member.enabled = false;
			}
			members[j] = member;
			if (member.enabled)
			{
				real_sources++;
			}
		}

		if (real_sources <= 0)
		{
			return true;
		}

		memset(m_mixer_buffer, 0, MaxFrameSize * sizeof(double));
		memset(m_cvt_samples, 0, MaxFrameSize * sizeof(uint16_t));

		//Будем складывать или вычитать?
		//  Если сурсов разрешенных больше половины имеющихся источников - вычитать. А если меньше - складывать.
		//  Чаще всего или все слышат всех (тогда N+N операций), или все слышат 2х-3х, тогда N+N*2. 
		//    Худший случай N+N^2/2, когда все слышат половину, притом вразнобой.
		//    Если у половины выключены микрофоны - то это отдельный случай. У выключенного samples=nullptr, это вычисляется заранее 
		//      и нужно только удалить себя из суммы для тех, у кого не выключен микрофон впридачу.
		//  При сложении же для каждого как раньше было - N^2 операций в случае "каждый слышит каждого".

		bool usesum = (real_sources <= 2) || (real_sources <= (mixed_count >> 1)) || (mixed_count == 0) || (mixed_voice == nullptr);

		AudioMixer mixer(m_mixer_samples_count, 3);

		if (usesum)
		{
			//Складываем разрешенные
			//  Исходное значение 0

			//int mixed_count =
			mixer.mix_samples(members, members_count, m_mixer_buffer);
		}
		else
		{
			//Вычитаем запрещенные
			//  Исходное значение mixed_voice
			//  Складываем с противоположными значениями

			mixer.sub_samples(members, members_count, m_mixer_buffer, mixed_voice);
		}

		mixer.cvt_samples(m_mixer_buffer, (int16_t*)m_cvt_samples);

		int size_write = m_mixer_samples_count * sizeof(int16_t);
		if (m_freelock_buffer.write((uint8_t*)m_cvt_samples, size_write) != size_write)
		{
			ERROR_MSG("buffer write error!");
			return true; // возможно просто набралось много данных для отправки и
			//через какое то время снова можно будет принимать.
		}

		if (m_hard)
		{
			rtl::async_call_manager_t::callAsync(this, 0, 0, nullptr);
		}
		else if (m_owner != nullptr)
		{
			m_owner->packetize();
		}

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_conference_t::add_except_termination_id(const char* termination_id)
	{
		for (int i = 0; i < m_except_list.getCount(); i++)
		{
			rtl::String id_from_list = m_except_list.getAt(i);
			if (rtl::String::compare(id_from_list, termination_id, false) == 0)
			{
				return;
			}
		}

		if (wait_mixer_tread())
		{
			m_except_list.add(termination_id);
			PRINT_FMT("term_id - %s. except_list size %d",
				termination_id, m_except_list.getCount());
		}
		else
		{
			ERROR_MSG("timeout. (member not added!)!");
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_conference_t::remove_except_termination_id(const char* termination_id)
	{
		int i = 0;
		bool find = false;
		for (; i < m_except_list.getCount(); i++)
		{
			rtl::String id_from_list = m_except_list.getAt(i);
			if (rtl::String::compare(id_from_list, termination_id, false) == 0)
			{
				find = true;
				break;
			}
		}

		PRINT_FMT("term_id - %s (%s). except_list size %d",
			termination_id, find ? "found" : "not found", m_except_list.getCount());

		if (!find)
		{
			return;
		}

		if (wait_mixer_tread())
		{
			m_except_list.removeAt(i);
		}
		else
		{
			ERROR_MSG("timeout. (member not remove).");
		}
	}
	//------------------------------------------------------------------------------
	// проверка источника в списке исключений
	//------------------------------------------------------------------------------
	bool mg_conference_t::is_permissible_source(mg_conference_member_t* source)
	{
		if (source == nullptr)
		{
			return false;
		}

		if (source->getTubeOut() == m_owner)
		{
			return false;
		}

		rtl::String name;
		if (source->getTubeOut() == nullptr)
		{
			if (source->getTubeIn() == nullptr)
			{
				return false;
			}

			name = source->getTubeIn()->get_termination_name();
		}
		else
		{
			name = source->getTubeOut()->get_termination_name();
		}

		m_is_permissible = true;

		for (int i = 0; i < m_except_list.getCount(); i++)
		{
			rtl::String id_from_list = m_except_list.getAt(i);
			if (rtl::String::compare(id_from_list, name, false) == 0)
			{
				m_is_permissible = false;
				return false;
			}
		}

		m_is_permissible = false;

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	const char* mg_conference_t::async_get_name()
	{
		return "conference";
	}
	//------------------------------------------------------------------------------
	void mg_conference_t::async_handle_call(uint16_t call_id, uintptr_t int_param, void* ptr_param)
	{
		if (m_owner)
		{
			m_owner->packetize();
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_conference_t::wait_mixer_tread()
	{
		uint32_t count = 0;

		while (count < 50)
		{
			if (!m_is_permissible)
			{
				return true;
			}
			rtl::Thread::sleep(2);
			count++;
		}

		return false;
	}

	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_audio_conference_out_tube_t::mg_audio_conference_out_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id) :
		Tube(HandlerType::ConfAudioOutput, id, log), m_conf(log, this)
	{
		makeName(parent_id, "-conf-out-tu"); // Conference Out Tube

		m_ssrc_to_rtp = (rtl::DateTime::getTicks() + rand()) | 0x833000b0;
		m_seq_num_to_rtp = (uint16_t)(rtl::DateTime::getTicks() ^ 0x000000D5D);;
		m_seq_num_to_rtp_last = 0;
		m_timestamp_to_rtp = rtl::DateTime::getTicks() & 0x005D5D5D;
		m_timestamp_to_rtp_last = 0;

		m_termination_id = rtl::String::empty;

		rtl::res_counter_t::add_ref(g_mge_tube_counter);

		setLogTag("TUB-OCON");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_audio_conference_out_tube_t::~mg_audio_conference_out_tube_t()
	{
		unsubscribe(this);

		rtl::res_counter_t::release(g_mge_tube_counter);
	}
	//------------------------------------------------------------------------------
	bool mg_audio_conference_out_tube_t::is_suited(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out)
	{
		if (payload_el_out == nullptr)
		{
			return false;
		}
		return m_payload_id == payload_el_out->get_payload_id();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_audio_conference_out_tube_t::init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out)
	{
		if (payload_el_out == nullptr)
		{
			ERROR_MSG("payload element is null.");
			return false;
		}
		m_payload_id = payload_el_out->get_payload_id();
		PRINT_FMT("payload out: %s %d", payload_el_out->get_element_name(), m_payload_id);

		uint32_t ptime = payload_el_out->get_ptime();
		uint32_t sample_rate = payload_el_in->get_sample_rate(); // рабочая частота конференции равна частотк в in

		bool hard = false;

		if (payload_el_out->get_element_name().toLower().indexOf("speex") != BAD_INDEX)
		{
			hard = true;
		}
		else if (payload_el_out->get_element_name().toLower().indexOf("g729") != BAD_INDEX)
		{
			hard = true;
		}

		m_conf.initialize(ptime * sample_rate / 1000, hard);


		set_termination_name(payload_el_out->get_termination_id());

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_audio_conference_out_tube_t::packetize()
	{
		uint16_t samples[mg_conference_t::MaxFrameSize] = { 0 };
		int read = 0;

		while ((read = m_conf.readData(samples, mg_conference_t::MaxFrameSize)) > 0)
		{
			rtp_packet packet;
			packet.set_version(2);
			if ((m_seq_num_to_rtp_last == 0) || (m_timestamp_to_rtp_last == 0))
			{
				packet.set_marker(true);
			}

			packet.set_payload_type(uint8_t(m_payload_id));
			packet.set_ssrc(m_ssrc_to_rtp);
			packet.set_samples(read);

			m_seq_num_to_rtp_last = m_seq_num_to_rtp;
			m_seq_num_to_rtp++;
			packet.set_sequence_number(m_seq_num_to_rtp);

			m_timestamp_to_rtp_last = m_timestamp_to_rtp;
			m_timestamp_to_rtp += read;
			packet.set_timestamp(m_timestamp_to_rtp);

			packet.set_payload(samples, read);

			INextDataHandler* out = getNextHandler();
			if (out == nullptr)
			{
				ERROR_MSG("out handler is null.");
				return;
			}
			out->send(this, &packet);
			//PLOG_TUBE_WRITE(LOG_PREFIX, "packetize -- ID(%s): send packet:%d with size:%d.", getName(), m_seq_num_to_rtp, size);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_audio_conference_out_tube_t::changeTopologyFrom(const char* id, h248::TopologyDirection topology)
	{
		switch (topology)
		{
		case h248::TopologyDirection::Bothway:
			remove_except_termination_id(id);
			break;;
		case h248::TopologyDirection::Isolate:
			add_except_termination_id(id);
			break;
		case h248::TopologyDirection::Oneway:
			add_except_termination_id(id);
			break;
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_audio_conference_out_tube_t::changeTopologyTo(const char* id, h248::TopologyDirection topology)
	{
		switch (topology)
		{
		case h248::TopologyDirection::Bothway:
			remove_except_termination_id(id);
			break;
		case h248::TopologyDirection::Isolate:
			add_except_termination_id(id);
			break;
		case h248::TopologyDirection::Oneway:
			remove_except_termination_id(id);
			break;
		}
	}
}
//------------------------------------------------------------------------------
