﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "std_logger.h"
#include "mg_media_element.h"
#include "mg_sdp_ssrc_param.h"
#include "mg_sdp_ice_param.h"
#include "sdp_fingerprint.h"

//-----------------------------------------------
//
//-----------------------------------------------
class rtp_channel_t;

namespace mge
{
	class mg_tx_stream_t;
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
#define MG_STREAM_TYPE_AUDIO 1
#define MG_STREAM_TYPE_VIDEO 2
#define MG_STREAM_TYPE_IMAGE 3
//------------------------------------------------------------------------------
#define MG_STREAM_TYPE_AUDIO_STR "audio"
#define MG_STREAM_TYPE_VIDEO_STR "video"
#define MG_STREAM_TYPE_IMAGE_STR "image"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
	enum class mg_sdp_difference_type
	{
		base,			// v=0
						// o=carol 28908764872 28908764872 IN IP4 100.3.6.6
						// s=-
						// t=0 0
						// c=IN IP4 192.0.2.4
		address,		// c=IN IP4 87.117.163.31
		media,			// m=audio 64993 UDP/TLS/RTP/SAVPF 111 103 104 9 0 8 106 105 13 126
						// a=rtpmap:111 opus/48000/2
						// a=fmtp:111 minptime=10; useinbandfec=1
						// a=rtcp-fb:101 nack
		mode,			// a=sendrecv
		crypto,			// a=crypto:1 AES_CM_128_HMAC_SHA1_80 inline:5sDcCo6NtcKqf9qi3AFCHXz9F2qZIdpCFCYIQbek
						// a=fingerprint:sha-256 C4:AB:13:41:6C:8D:3B:EC:EE:DB:AA:A4:08:B3:01:5D:49:F8:57:0F:DC:82:B2:55:31:13:C9:95:E5:E7:0A:AB
						// a=setup:actpass
		ssrc,			// a=ssrc:1061712719 cname:NCuce0OHjVRX4MAI
						// a=ssrc:1061712719 msid:6n36IqVDOliFUE63N0W6CgBebhGRDxx8wWqS 281c0c2f-fd25-4b7e-a32c-de9d8dead668
						// a=ssrc:1061712719 mslabel:6n36IqVDOliFUE63N0W6CgBebhGRDxx8wWqS
						// a=ssrc:1061712719 label:281c0c2f-fd25-4b7e-a32c-de9d8dead668
		ice,			// a=candidate:3364417298 1 udp 2122260223 192.168.0.73 60326 typ host generation 0
						// a=candidate:2248872930 1 tcp 1518280447 192.168.0.73 9 typ host tcptype active generation 0
						// a=candidate:1050187462 1 udp 1686052607 87.117.163.31 13665 typ srflx raddr 192.168.0.73 rport 60326 generation 0
						// a=ice-ufrag:iVAb+WAnUnzAVvea
						// a=ice-pwd:1iiV4HvsIuU5w6If45go9wwM
		ptime,			// a=ptime:20
						// a=maxptime:60
		rtcp,			// a=rtcp-mux
						// a=rtcp:2049 IN IP4 87.117.163.31
		other			// a=mid:audio
						// a=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level
						// a=extmap:3 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time
	};
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	struct ME_DEBUG_API mg_sdp_difference_t
	{
		mg_sdp_difference_type type;
		rtl::String line;
		uint32_t stream_id;
	};
	typedef rtl::ArrayT<mg_sdp_difference_t*> mg_sdp_diff_list;
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	class ME_DEBUG_API mg_sdp_media_base_t
	{
		mg_sdp_media_base_t(const mg_sdp_media_base_t&);
		mg_sdp_media_base_t&	operator=(const mg_sdp_media_base_t&);

	public:
		mg_sdp_media_base_t(rtl::Logger* log, const char* term_id, uint32_t stream_id);
		~mg_sdp_media_base_t();

		static int				stream_type(int id);
		uint32_t				get_stream_id();

		bool read(const rtl::String& media);

		void write(rtl::String& media);
		void clear();

		// послк чтения фильтруем неизвестные элементы
		void filer_media_elements();

		// После чтения новой sdp в памяти хранится различия между старой, если старая sdp не была пустой.
		mg_sdp_diff_list*		get_differences();
		void					clear_differences_list();
		bool					apply_differences();

		// Добавляем строки медиа из sdp. 
		// Флаг compare указывает сравнивать ли строку с той которая уже в sdp (если sdp не пуста).
		bool					add_media_line(const rtl::String& media_line, bool compare);

		// возвращает полностью строку медиа. Пример:
		// "m=audio 64993 UDP/TLS/RTP/SAVPF 111 103 104 9 0 8 106 105 13 126"
		const rtl::String&			get_media_line() const;

		// пример:
		// из строки "m=audio 64993 UDP/TLS/RTP/SAVPF 111 103 104 9 0 8 106 105 13 126"
		// возвращаем "audio"
		const rtl::String&			getType() const;
		void					set_type(const char* type);

		// полностью списко строк crypto.
		// пример: "a=crypto:1 AES_CM_128_HMAC_SHA1_80 inline:5sDcCo6NtcKqf9qi3AFCHXz9F2qZIdpCFCYIQbek"
		rtl::StringList&			get_crypto_params();

		// пример:
		// из строки "m=audio 64993 UDP/TLS/RTP/SAVPF 111 103 104 9 0 8 106 105 13 126"
		// возвращаем "64993"
		const uint16_t			get_port() const;
		void					set_port(uint16_t port);

		// пример:
		// из строки "a=rtcp:2049 IN IP4 87.117.163.31"
		// возвращаем "2049"
		const uint16_t			get_rtcp_port() const;
		void					set_rtcp_port(uint16_t port);
		// пример:
		// из строки "a=rtcp:2049 IN IP4 87.117.163.31"
		// возвращаем "87.117.163.31"
		const ip_address_t*		get_rtcp_address() const;
		void					set_rtcp_address(const ip_address_t* addr);

		// включение и выключение строки "a=rtcp-mux"
		void					set_rtcp_mux(bool mux);
		bool					check_rtcp_mux();

		//c=IN IP4 87.117.163.31
		const ip_address_t*		get_address() const;
		void					set_address(const ip_address_t* addr);

		// пример: 
		// из строки "a=ptime:60"
		// возвращаем "60"
		const uint32_t			get_ptime() const;
		void					set_ptime(uint32_t ptime);
		// пример: 
		// из строки "a=maxptime:60"
		// возвращаем "60"
		const uint32_t			get_maxptime() const;
		void					set_maxptime(uint32_t maxptime);

		// пример:
		// из строки "m=audio 64993 UDP/TLS/RTP/SAVPF 111 103 104 9 0 8 106 105 13 126"
		// возвращаем "UDP/TLS/RTP/SAVPF"
		const rtl::String&			get_transport() const;
		void					set_transport(const char* transport);

		// пример:
		// из строки "a=sendrecv" возвращаем "sendrecv"
		const rtl::String&			get_mode() const;
		void					set_mode(const char* mode);

		// список пайлоад элементов
		mg_media_element_list_t* get_media_elements();

		// список параметров a=extmap:
		rtl::StringList&			get_extmaps();

		// Параметр из строки:
		// "a=mid:audio" возвращаем "audio"
		const rtl::String&			get_mid_type();
		void					set_mid_type(const char* type);

		// Возвращаем msid для строки "a=msid-semantic:" если есть ssrc парамемтры.
		const char*				get_msid_semantic();

		// Возвращаем список ssrc параметров.
		rtl::ArrayT<mg_sdp_ssrc_param_t*>* get_ssrc_params();
		void					clear_ssrc_params();

		// список параметров fingerprint:
		// a=fingerprint:sha-256 C4:AB:13:41:6C:8D:3B:EC:EE:DB:AA:A4:08:B3:01:5D:49:F8:57:0F:DC:82:B2:55:31:13:C9:95:E5:E7:0A:AB
		// a=setup:actpass
		void					set_fingerprint_param(sdp_fingerprint_t* fingerprint_param);
		sdp_fingerprint_t*		get_fingerprint_param();
		void					clear_fingerprint_param();

		// список параметров ice:
		// a=candidate:469649836 2 tcp 1518280446 192.168.0.10 9 typ host tcptype active generation 0
		// a=candidate:4233069003 2 tcp 1518214910 192.168.56.1 9 typ host tcptype active generation 0
		// a=ice-ufrag:FHB6YiMTdBwm8gwc
		// a=ice-pwd:Wp0IUTOlgKY/gBjp05PvMMs1
		void					set_ice_param(mg_sdp_ice_param_t* ice_param);
		mg_sdp_ice_param_t*		get_ice_param();
		void					clear_ice_param();

		void					clear_webrtc_param();

	private:
		bool					read_m_line(const rtl::String& media_line, bool compare);
		void write_m_line(rtl::String& ss);
		void					update_m_line();

		bool					read_a_line(const rtl::String& media_line, bool compare);
		bool					read_rtpmap(const rtl::String& media_line);
		bool					read_fmtp(const rtl::String& media_line);
		bool					read_rtcp_fb(const rtl::String& media_line);
		bool					read_ptime(const rtl::String& media_line, bool compare);
		bool					read_maxptime(const rtl::String& media_line, bool compare);
		bool					read_crypto(const rtl::String& media_line, bool compare);
		bool					read_ssrc(const rtl::String&media_line, bool compare);
		bool					read_fingerprint(const rtl::String&media_line, bool compare);
		bool					read_fingerprint_setup(const rtl::String&media_line, bool compare);
		bool					read_candidate(const rtl::String&media_line, bool compare);
		bool					read_ufrag(const rtl::String&media_line, bool compare);
		bool					read_pwd(const rtl::String&media_line, bool compare);
		bool					read_rtcp_mux(const rtl::String&media_line, bool compare);
		bool					read_mid(const rtl::String&media_line, bool compare);
		bool					read_extmap(const rtl::String&media_line, bool compare);
		bool					read_c_line(const rtl::String&media_line, bool compare);
		bool					read_rtcp(const rtl::String&media_line, bool compare);
		bool					read_mode(const rtl::String&media_line, bool compare);

		void write_a_lines(rtl::String& ss);
		bool					write_ptime(rtl::String& ss);
		bool					write_maxptime(rtl::String& ss);
		bool					write_crypto(rtl::String& ss);
		bool					write_ssrc(rtl::String& ss);
		bool					write_fingerprint(rtl::String& ss);
		bool					write_candidate(rtl::String& ss);
		bool					write_rtcp(rtl::String& ss);
		bool					write_mid(rtl::String& ss);
		bool					write_extmap(rtl::String& ss);
		void write_c_line(rtl::String& ss);
		bool					write_mode(rtl::String& ss);

		bool					compare_media_param(const rtl::String&media_line);

	private:
		rtl::Logger*			m_log;
		rtl::String				m_term_id;

		//----------------------------------------------------------------
		// m=audio 64993 UDP/TLS/RTP/SAVPF 111 103 104 9 0 8 106 105 13 126
		rtl::String				m_m_line;
		rtl::String				m_type; // audio
		uint32_t				m_stream_id; // соответствие id stream текущему типу медиа.
		uint16_t				m_port; // 64993
		rtl::String				m_transport; //UDP/TLS/RTP/SAVPF
		mg_media_element_list_t m_media_elements; //111 103 104 9 0 8 106 105 13 126
		//----------------------------------------------------------------

		//----------------------------------------------------------------
		// c=IN IP4 87.117.163.31
		ip_address_t			m_address;
		//----------------------------------------------------------------

		//----------------------------------------------------------------
		// список нераспарсенных линий
		rtl::StringList			m_atributes;
		//----------------------------------------------------------------

		//----------------------------------------------------------------
		// распарсенный параметр из атрибут.
		// a=ptime:60
		uint32_t				m_ptime;
		// a=maxptime:60
		uint32_t				m_maxptime;
		//----------------------------------------------------------------

		//----------------------------------------------------------------
		// список параметров crypto:
		// a=crypto:1 AES_CM_128_HMAC_SHA1_80 inline:5sDcCo6NtcKqf9qi3AFCHXz9F2qZIdpCFCYIQbek
		rtl::StringList			m_crypto_params;
		//----------------------------------------------------------------

		//----------------------------------------------------------------
		// параметры RTCP:

		// a=rtcp:2049 IN IP4 87.117.163.31
		ip_address_t			m_rtcp_address; // 87.117.163.31
		uint16_t				m_rtcp_port; // 2049
		// включение или выключение строки: a=rtcp-mux
		bool					m_rtcp_mux;
		//----------------------------------------------------------------

		//----------------------------------------------------------------
		// распарсеный тип mid. Если он пуст то строка не выводится.
		// a=mid:audio
		rtl::String				m_mid_type; // audio
		//----------------------------------------------------------------

		//----------------------------------------------------------------
		// список параметров a=extmap:
		rtl::StringList			m_extmaps;
		//----------------------------------------------------------------

		//----------------------------------------------------------------
		//"a=sendrecv"
		rtl::String				m_mode;
		//----------------------------------------------------------------

		//----------------------------------------------------------------
		// webrct параметры:
		rtl::ArrayT<mg_sdp_ssrc_param_t*> m_ssrc_param_list;
		sdp_fingerprint_t*		m_fingerprint;
		mg_sdp_ice_param_t*		m_ice;
		//----------------------------------------------------------------

		mg_sdp_diff_list		m_differences_list; // переменная для работы с сравнением.
	};
	typedef rtl::ArrayT<mg_sdp_media_base_t*> mg_sdp_media_list;
}
//------------------------------------------------------------------------------
