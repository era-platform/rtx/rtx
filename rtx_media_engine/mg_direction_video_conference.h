/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_direction.h"
#include "mg_video_conference_ctrl.h"
#include "mg_video_conference_in_tube.h"

namespace mge
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	class VideoConferenceDirection : public Direction
	{
		friend class TubeManager;

	public:
		VideoConferenceDirection(MediaContext* owner, uint32_t id, const char* parent_id);
		virtual	~VideoConferenceDirection();

		virtual bool initialize() override;
		virtual bool addTermination(Termination* term) override;
		virtual void removeTermination(uint32_t termId) override;
		virtual void checkTerminationProxy(uint32_t termId) override;
		virtual void updateTopology(const rtl::ArrayT<h248::TopologyTriple>& triple_list) override;
		virtual void updateConfSchema(const char* jsonSchema) override;

	private:
		virtual int checkTermination(Termination* term) override;

		// ��������� ������� � ������� �������.
		Tube* buildRx(const char* termId, mg_rx_stream_t* rxStream, mg_tx_stream_t* txStream);
		// ��������� ������� �� �������.
		Tube* buildTx(const char* termId, mg_rx_stream_t* rxStream, mg_tx_stream_t* txStream);
		void fillElementListRx(const char* termId, mg_rx_stream_t* rxStream, mg_tx_stream_t* txStream,
			mg_media_element_list_t& rxList, mg_media_element_list_t& txList);
		void fillElementListTx(const char* termId, mg_rx_stream_t* rxStream, mg_tx_stream_t* txStream,
			mg_media_element_list_t& rxList, mg_media_element_list_t& txList);
		//...
		bool searchTopologyTubes(const char* termIdFrom, mg_video_conference_out_tube_t*& out, const char* termIdTo, mg_video_conference_out_tube_t*& in);

		mg_video_conference_in_tube_t* createInputTube(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out);
		mg_video_conference_out_tube_t* createOutputTube(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out);

	private:
		VideoConferenceController* m_ctrl;
	};
}
//-----------------------------------------------
