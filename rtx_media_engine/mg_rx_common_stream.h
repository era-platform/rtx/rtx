﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_rx_stream.h"

namespace mge
{
	//------------------------------------------------------------------------------
	// Однонаправленный поток из канала в тубы.
	//------------------------------------------------------------------------------
	class mg_rx_common_stream_t : public mg_rx_stream_t, public i_rx_channel_event_handler, public mge::VadDetector
	{
		friend class mg_novoice_timer_manager_t;

	public:
		mg_rx_common_stream_t(rtl::Logger* log, uint32_t id, mg_rx_stream_type_t type, const char* parent_id, MediaSession& session);
		virtual ~mg_rx_common_stream_t();

		void set_ssrc_from_rtp(uint32_t ssrc) { m_ssrc_from_rtp = ssrc; }
		uint32_t get_ssrc_from_rtp() { return m_ssrc_from_rtp; }
		// устанавливаем канал у котороги мы в подписках.
		// нужно для того что б отписаться при останвке стрима.
		void set_channel_event(i_tx_channel_event_handler* rtp);
		// вызываем функцию канала что б он отписал текущий стрим.
		void reset_stream_from_channel_event();
		bool activity();
		bool createRecorder(const char* path);

		// принимаем пакеты из канала.
		virtual bool rx_packet_stream(const rtp_channel_t* channel, const rtp_packet* packet, const socket_address* from);
		virtual bool rx_data_stream(const rtp_channel_t* channel, const uint8_t* data, uint32_t len, const socket_address* from);
		virtual bool rx_ctrl_stream(const rtp_channel_t* channel, const rtcp_packet_t* packet, const socket_address* from);

		virtual void VadDetector__audioSignalChanged(uint32_t termId, bool VAD, int volume) override;

		virtual void setMediaParams(mg_sdp_media_base_t& media) override;

		// RTX-97. Параметры VAD
		bool vad_isActive() { return m_vad; }
		void vad_activate(bool vad) { m_vad = vad; }
		mge::AudioAnalyzer* vad_getAnalyzer() { return m_audio_analyzer; }
		bool vad_createAnalyzer();
		void vad_setParams(const mge::AudioAnalyzerParams& params);

	private:
		bool rx_packet_stream_lock(const rtp_channel_t* channel, const rtp_packet* packet, const socket_address* from);
		bool rx_data_stream_lock(const rtp_channel_t* channel, const uint8_t* data, uint32_t len, const socket_address* from);
		bool rx_ctrl_stream_lock(const rtp_channel_t* channel, const rtcp_packet_t* packet, const socket_address* from);

		bool check_ssrc(rtp_packet* packet);
		bool check_sequence_number(rtp_packet* packet);
		bool check_timestamp(rtp_packet* packet);

		void change_timestamp_step(uint32_t timestamp_step);

		virtual int rtcp_session_rx(const rtcp_packet_t* packet) override;

	private:
		i_tx_channel_event_handler* m_rtp;
		//-----------------------------------------------------
		// переменные для работы с принятыми пакетами из сети.
		uint32_t m_ssrc_from_rtp;			// ssrc из входящих пакетов
		uint32_t m_new_ssrc_from_rtp_counter;// счетчик пакетов с новым ssrc
		uint16_t m_seq_num_from_rtp;			// порядковый номер пакета пришедшего из ртп канала.
		uint16_t m_seq_num_from_rtp_last;	// номер последнего пришедшего пакета из ртп канала.
		uint16_t m_seq_num_from_rtp_diff;	// разница между последним и пришедшим.
		uint32_t m_timestamp_from_rtp;		// таймштамп пришедшего пакета из ртп канала.
		uint32_t m_timestamp_from_rtp_last;	// таймштамп последнего пришедшего пакета из ртп канала.
		uint32_t m_timestamp_step;			// частота присылаемых пакетов.
	
		//-----------------------------------------------------

		//-----------------------------------------------------
		// переменные для обнаружения отсутствия звука.

		volatile bool m_cn_packet;				// флаг угазывает что был пакет comfort noise.
		volatile bool m_no_voice;				// флаг указфвает что нет звука и мы шлем novoice.
		volatile uint32_t m_last_tick;			// последний тик таймера.
		volatile uint32_t m_last_send_novoice;	// время последней отправки события novoice!
		//-----------------------------------------------------
		media::LazyMediaWriterRTP* m_recorder;
		//-----------------------------------------------------
		// RTX-97. Параметры VAD
		bool m_vad;
		mge::AudioAnalyzer* m_audio_analyzer;
		mge::AudioAnalyzerParams m_vad_params;
	};
}
//------------------------------------------------------------------------------
