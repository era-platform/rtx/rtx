﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "rtx_media_engine.h"
#include "std_string.h"
#include "spandsp.h"
#include "mmt_timer.h"
#include <atomic>
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
extern rtl::Logger* g_mg_fax_log;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
enum FAX_SIGNALS
{
	FAX_SIGNAL_ON,
	FAX_SIGNAL_DESTROED,
	FAX_SIGNAL_RX_ERROR,
	FAX_SIGNAL_TX_ERROR,
	FAX_SIGNAL_RX_DONE,
	FAX_SIGNAL_TX_DONE,
	FAX_SIGNAL_OFF,
};
const char* mg_get_fax_signal_name(int signal);
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void span_message(int level, const char *msg);
void span_error(const char *msg);
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
struct mg_fax_handler
{
	virtual void send(const char* data, int data_length) = 0;
	virtual void fax_end(bool error) = 0;
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class mg_fax_t: public media::Metronome
{
	mg_fax_t(const mg_fax_t&);
	mg_fax_t& operator=(const mg_fax_t&);

public:
	mg_fax_t(rtl::Logger* log);
	~mg_fax_t();
	
	void set_fax_handler(mg_fax_handler* handler);

	virtual void destroy();
	virtual FAX_SIGNALS get_fax_signal_state(bool stop_signaling);

	void fax_signal(int signal);

	virtual uint32_t write_fax(const uint8_t* data, uint32_t length) = 0;
	virtual uint32_t read_fax(uint8_t* data, uint32_t length) = 0;

	rtl::Logger* get_logger();

	bool fax_notified();
	void set_fax_notified(bool notified);

protected:
	bool lock();
	void unlock();

private:
	// <<Timer>>
	virtual void metronomeTicks();

private:
	rtl::Logger* m_log;
	bool m_fax_notified;
	mg_fax_handler* m_handler;
	std::atomic_flag m_lock;
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------