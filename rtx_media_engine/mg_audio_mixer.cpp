﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_audio_mixer.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	const double g_mixcoefmax = 32767.0;
	const double g_mixcoef0 = 32769.0;
	const double g_mixcoef1 = 1.0 / g_mixcoef0;
	const double g_mixcoef2 = 1.0 / g_mixcoefmax;
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	AudioMixer::AudioMixer(uint32_t frame_size, int mix_algorithm)
	{
		m_frameSize = frame_size;
		m_mixAlgorithm = mix_algorithm;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	AudioMixer::~AudioMixer()
	{

	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	int AudioMixer::mix_samples(const AudioMixerMember* members, int members_count, double* mix_buffer)
	{
		// Подсчет количества, которое подлежит сложению
		int mixed_count = 0;
		for (int j = 0; j < members_count; j++)
		{
			// если пакет разрешен то добавим в итоговый пакет
			if (members[j].enabled && members[j].samples != nullptr)
			{
				mixed_count++;
			}
		}

		// микшер
		// Все сэмплы в пакете поочередно обрабатываем
		for (uint32_t i = 0; i < m_frameSize; i++)
		{
			//-----------------------------------------------------------------
			//Сложение волн через линейное проектирование отрезка [-32768; 32767] -> [-1; 1] -> (-inf; +inf). 
			//  Или через круг - квадратичное преобразование к [-1; 1], работа с числами на этом же отрезке.
			//График y=1-|x| на отрезке [-1; 1]. Проектирование на прямую у=1.
			//При 25 загрузка большая, при 50 ядро вылетает в полную загрузку. 
			//  Требуется оптимизация N+N вместо N+(N-1)^2. Достигается путем вычитания неслышимых из общей суммы.
			//    Если в списке на сложение >= N/2, то вычитаем неслышимых, если < N/2, то складываем слышимых.
			//    Необходимо учесть, что некоторых пакетов в принципе может не быть, поэтому N для каждого сэмпла вычисляется.
			//  Также есть еще функция write_next_block, в ней тоже подсчет аналогичный ведется. Надо функцию общую сделать. В трех местах нужна итого.
			//-----------------------------------------------------------------
			double sample = .0;

			for (int j = 0; j < members_count; j++)
			{
				// если пакет разрешен то добавим в итоговый пакет
				if (members[j].enabled && members[j].samples != nullptr)
				{
					//[-32768-32767] -> (-1;1)
					double a = sample;// * mixcoef1;
					double b = (double)members[j].samples[i] * g_mixcoef1;

					//Подсчет суммы
					//  для разных алгоритмов
					switch (m_mixAlgorithm)
					{
					case 0://Простое сложение георга
						sample += b;
						break;

					case 1://линейный модуль петра
					{
						double absa = fabs(a);
						double absb = fabs(b);
						//m+n и m-n могут быть 0 только при значениях а,b равных 1. Мы это исключаем нормировкой по завышенной базе.
						double n = a + b - a * absb - b * absa;
						double m = 1 - absa - absb + absa * absb;
						//модуль - поэтому два варианта
						if (((n >= 0) && (m >= 0)) || ((n < 0) && (m < 0)))
							sample = n / (m + n);
						else
							sample = n / (m - n);
					}
					break;


					case 2://Круг никитина
					{
						double ab = a * b;
						double aplusb = a + b;
						int action = (ab > 0) ? 1 : (ab < 0) ? -1 : 0;
						int sgn = (aplusb > 0) ? 1 : (aplusb < 0) ? -1 : 0;
						double a1 = 1 - sqrt(1 - a * a);
						double b1 = 1 - sqrt(1 - b * b);
						double c1 = 1 - fabs(b1 + a1 * action);
						sample = sqrt(1 - c1 * c1) * sgn;
					}
					break;

					case 3://Круг шамилича
						//преобразуем к плоскости и складываем
						sample += b / sqrt(1 - b * b);
						break;
					}
				}
			}

			//Преобразования общие над итогом
			//  для разных алгоритмов
			switch (m_mixAlgorithm)
			{
			case 3://Круг шамилича
				int sgn = (sample > 0) ? 1 : (sample < 0) ? -1 : 0;
				double samplesqr = sample * sample;
				sample = sgn * sqrt(samplesqr / (1 + samplesqr));
				break;
			}

			//Восстанавливаем уровень
			sample *= g_mixcoefmax;

			//Запись результата
			mix_buffer[i] = sample;
		}

		return mixed_count;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void AudioMixer::sub_samples(const AudioMixerMember* members, int members_count, double* mix_buffer, const double* mixedvoice)
	{
		// микшер
		for (uint32_t i = 0; i < m_frameSize; i++)
		{
			//-----------------------------------------------------------------
			//Сложение волн через линейное проектирование отрезка [-32768, 32767] -> [-1, 1] -> (-inf, +inf). 
			//График y=1-|x| на отрезке [-1; 1]. Проектирование на прямую у=1.
			//При 25 загрузка большая, при 50 ядро вылетает в полную загрузку. 
			//  Требуется оптимизация N+N вместо N+(N-1)^2. Достигается путем вычитания неслышимых из общей суммы.
			//    Если в списке на сложение >= N/2, то вычитаем неслышимых, если < N/2, то складываем слышимых.
			//    Необходимо учесть, что некоторых пакетов в принципе может не быть, поэтому N для каждого сэмпла вычисляется.
			//  Также есть еще функция write_next_block, в ней тоже подсчет аналогичный ведется. Надо функцию общую сделать. В трех местах нужна итого.
			//-----------------------------------------------------------------
			double sample = mixedvoice[i] * g_mixcoef2;

			//Начальное значение точно также рассчитывается. 
			//  Через ту же процедуру, только сложение с нулем (преобразование к прямой необходимо)
			switch (m_mixAlgorithm)
			{
			case 0:
				break;

			case 1:
			{
				double b = sample;
				double absb = fabs(b);
				//m+n и m-n могут быть 0 только при значениях а,b равных 1. Мы это исключаем нормировкой по завышенной базе.
				double n = b;
				double m = 1 - absb;
				//модуль - поэтому два варианта
				if (((n >= 0) && (m >= 0)) || ((n < 0) && (m < 0)))
					sample = n / (m + n);
				else
					sample = n / (m - n);
				break;
			}

			case 2:
			{
				double b = sample;
				int sgn = (b > 0) ? 1 : (b < 0) ? -1 : 0;
				double b1 = 1 - sqrt(1 - b * b);
				double c1 = 1 - fabs(b1);
				sample = sqrt(1 - c1 * c1) * sgn;
				break;
			}

			case 3:
			{
				double b = sample;
				sample = b / sqrt(1 - b * b);
				break;
			}
			}


			for (int j = 0; j < members_count; j++)
			{
				// если пакет разрешен то добавим в итоговый пакет
				if (!members[j].enabled && members[j].samples != nullptr)
				{
					//-32768-32767
					double a = sample;// * mixcoef1;
					double b = -(double)members[j].samples[i] * g_mixcoef1;

					//Подсчет суммы
					//  для разных алгоритмов
					switch (m_mixAlgorithm)
					{
					case 0://Простое сложение георга
						sample += b;
						break;

					case 1://линейный модуль петра
					{
						double absa = fabs(a);
						double absb = fabs(b);
						//m+n и m-n могут быть 0 только при значениях а,b равных 1. Мы это исключаем нормировкой по завышенной базе.
						double n = a + b - a * absb - b * absa;
						double m = 1 - absa - absb + absa * absb;
						//модуль - поэтому два варианта
						if (((n >= 0) && (m >= 0)) || ((n < 0) && (m < 0)))
							sample = n / (m + n);
						else
							sample = n / (m - n);
					}
					break;

					case 2://Круг никитина
					{
						double ab = a * b;
						double aplusb = a + b;
						int action = (ab > 0) ? 1 : (ab < 0) ? -1 : 0;
						int sgn = (aplusb > 0) ? 1 : (aplusb < 0) ? -1 : 0;
						double a1 = 1 - sqrt(1 - a * a);
						double b1 = 1 - sqrt(1 - b * b);
						double c1 = 1 - fabs(b1 + a1 * action);
						sample = sqrt(1 - c1 * c1) * sgn;
					}
					break;

					case 3://Круг шамилича
						//преобразуем к плоскости и складываем
						sample += b / sqrt(1 - b * b);
						break;
					}
				}
			}

			//Преобразования общие над итогом
			//  для разных алгоритмов
			switch (m_mixAlgorithm)
			{
			case 3://Круг шамилича
				int sgn = (sample > 0) ? 1 : (sample < 0) ? -1 : 0;
				double samplesqr = sample * sample;
				sample = sgn * sqrt(samplesqr / (1 + samplesqr));
				break;
			}

			//Восстанавливаем уровень
			sample *= g_mixcoefmax;

			//Запись результата
			mix_buffer[i] = sample;
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void AudioMixer::cvt_samples(const double* mixed_voice, short* samples)
	{
		double sample;

		for (uint32_t i = 0; i < m_frameSize; i++)
		{
			sample = mixed_voice[i];
			if (sample > 32767.0)
				sample = 32767.0;
			else if (sample < -32768.0)
				sample = -32768.0;

			samples[i] = (short)sample;
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	uint32_t AudioMixer::get_audio_frame_size()
	{
		return m_frameSize;
	}
}
//------------------------------------------------------------------------------
