﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg.h"
#include "mg_device_termination.h"
#include "mg_rx_device_stream.h"
#include "mg_tx_device_stream.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define LOG_PREFIX m_tag //"MG-TDEV" // Media Gateway Termination

#include "logdef.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	DeviceTermination::DeviceTermination(rtl::Logger* log, const h248::TerminationID& termId, const char* parentId) :
		Termination(log, termId, parentId),
		m_sdp_local(log), m_sdp_remote(log)
	{
		m_service_state = h248::TerminationServiceState::Test;
		m_event_buffer_control = false;
		strcpy(m_tag, "TERM-DEV");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	DeviceTermination::~DeviceTermination()
	{
		mg_termination_destroy(nullptr);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool DeviceTermination::mg_termination_initialize(h248::Field* _reply)
	{
		ENTER();

		m_sdp_local.set_termination_id(getName());
		m_sdp_remote.set_termination_id(getName());

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool DeviceTermination::mg_termination_destroy(h248::Field* _reply)
	{
		mge::Termination::mg_termination_lock();

		mge::Termination::destroyMediaSession();

		m_sdp_local.clear();
		m_sdp_remote.clear();

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	//------------------------------------------------------------------------------
	bool DeviceTermination::mg_termination_set_Local_Remote(uint16_t stream_id, const rtl::String& remote_sdp, const rtl::String& local_sdp_offer, h248::Field* reply)
	{
		ENTER();

		MediaSession* mg_stream = makeMediaSession(stream_id);
		if (mg_stream == nullptr)
		{
			ERROR_MSG("create stream fail!");
			return false;
		}

		bool result = true;
		if (stream_id == MG_STREAM_TYPE_AUDIO)
		{
			result = prepare_audio(mg_stream, remote_sdp, local_sdp_offer);
		}
		else if (stream_id == MG_STREAM_TYPE_VIDEO)
		{
			ERROR_MSG("VIDEO not implemented!");
			result = false;
		}
		else if (stream_id == MG_STREAM_TYPE_IMAGE)
		{
			ERROR_MSG("IMAGE not implemented.");
			result = false;
		}

		return result;
	}
	//------------------------------------------------------------------------------
	bool DeviceTermination::mg_termination_get_Local(uint16_t stream_id, rtl::String& answer, h248::Field* reply)
	{
		ENTER();

		m_sdp_local.write_base(answer);

		answer << "\r\n";

		if (stream_id == MG_STREAM_TYPE_AUDIO)
		{
			mg_sdp_media_base_t* audio = m_sdp_local.get_media(stream_id);
			if (audio == nullptr)
			{
				WARNING("audio not found!");
			}
			else
			{
				audio->write(answer);
			}
		}
		else if (stream_id == MG_STREAM_TYPE_VIDEO)
		{
			mg_sdp_media_base_t* video = m_sdp_local.get_media(stream_id);
			if (video == nullptr)
			{
				WARNING("video not found!");
			}
			else
			{
				video->write(answer);
			}
		}
		else if (stream_id == MG_STREAM_TYPE_IMAGE)
		{
			mg_sdp_media_base_t* image = m_sdp_local.get_media(stream_id);
			if (image == nullptr)
			{
				WARNING("image not found!");
			}
			else
			{
				image->write(answer);
			}
		}

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_rx_stream_t* DeviceTermination::find_rx_stream_by_type(mg_rx_stream_type_t type_rx, MediaSession* mg_stream)
	{
		const rtl::ArrayT<mg_rx_stream_t*>& rx_streams = mg_stream->get_rx_streams();
		if (rx_streams.getCount() == 0)
		{
			if (!mg_stream->create_rx_stream(type_rx))
			{
				ERROR_MSG("create rx stream fail!");
				return nullptr;
			}
		}

		if (rx_streams.getCount() == 0)
		{
			ERROR_MSG("create rx stream fail!");
			return nullptr;
		}

		mg_rx_stream_t* rx_stream = rx_streams[0];

		if (rx_stream == nullptr)
		{
			ERROR_MSG("create rx stream fail");
			return nullptr;
		}

		if (rx_stream->getType() != type_rx)
		{
			ERROR_MSG("created stream type mismatch!");
			return nullptr;
		}

		return rx_stream;
	}
	//------------------------------------------------------------------------------
	mg_tx_stream_t*	DeviceTermination::find_tx_stream_by_type(mg_tx_stream_type_t type_tx, MediaSession* mg_stream)
	{
		const rtl::ArrayT<mg_tx_stream_t*>& tx_streams = mg_stream->get_tx_streams();
		if (tx_streams.getCount() == 0)
		{
			if (!mg_stream->create_tx_stream(type_tx))
			{
				ERROR_MSG("create tx stream fail!");
				return nullptr;
			}
		}
		if (tx_streams.getCount() == 0)
		{
			ERROR_MSG("create tx stream fail!");
			return nullptr;
		}

		mg_tx_stream_t* tx_stream = tx_streams[0];

		if (tx_stream == nullptr)
		{
			ERROR_MSG("create tx stream fail.");
			return nullptr;
		}
		if (tx_stream->getType() != type_tx)
		{
			return nullptr;
		}

		return tx_stream;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool DeviceTermination::prepare_audio(MediaSession* audio_stream, const rtl::String& remote_sdp, const rtl::String& local_sdp)
	{
		if (remote_sdp.isEmpty())
		{
			return prepare_offer_audio(audio_stream, local_sdp);
		}
		else if (local_sdp.isEmpty())
		{
			return apply_answer_audio(audio_stream, remote_sdp);
		}

		if (!apply_offer_audio(audio_stream, remote_sdp))
		{
			return false;
		}

		return prepare_answer_audio(audio_stream, local_sdp);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool DeviceTermination::prepare_offer_audio(MediaSession* audio_stream, const rtl::String& local_sdp)
	{
		mg_tx_stream_type_t type_tx = mg_tx_stream_type_t::mg_tx_device_audio_e;
		rtl::String type_str(MG_STREAM_TYPE_AUDIO_STR);

		if (local_sdp.isEmpty())
		{
			return false;
		}

		if (!m_sdp_local.read(local_sdp, false, audio_stream->getId()))
		{
			ERROR_MSG("local sdp read fail!");
			return false;
		}

		mg_tx_stream_t* tx_stream = find_tx_stream_by_type(type_tx, audio_stream);
		if (tx_stream == nullptr)
		{
			return false;
		}
		//mg_tx_device_stream_t* tx_common_stream = (mg_tx_device_stream_t*)tx_stream;

		mg_sdp_media_base_t* audio_l = m_sdp_local.get_media(audio_stream->getId());
		if (audio_l == nullptr)
		{
			return false;
		}

		return true;
	}
	//------------------------------------------------------------------------------
	bool DeviceTermination::apply_answer_audio(MediaSession* audio_stream, const rtl::String& remote_sdp)
	{
		mg_rx_stream_type_t type_rx = mg_rx_stream_type_t::mg_rx_device_audio_e;
		mg_tx_stream_type_t type_tx = mg_tx_stream_type_t::mg_tx_device_audio_e;
		rtl::String type_str(MG_STREAM_TYPE_AUDIO_STR);

		if (remote_sdp.isEmpty())
		{
			return false;
		}

		if (!m_sdp_remote.read(remote_sdp, false, audio_stream->getId()))
		{
			ERROR_MSG("remote sdp read fail!");
			return false;
		}

		mg_sdp_media_base_t* audio_r = m_sdp_remote.get_media(audio_stream->getId());
		if (audio_r == nullptr)
		{
			return false;
		}

		mg_rx_stream_t* rx_stream = find_rx_stream_by_type(type_rx, audio_stream);
		if (rx_stream == nullptr)
		{
			return false;
		}
		//mg_rx_device_stream_t* rx_common_stream = (mg_rx_device_stream_t*)rx_stream;

		mg_tx_stream_t* tx_stream = find_tx_stream_by_type(type_tx, audio_stream);
		if (tx_stream == nullptr)
		{
			return false;
		}
		//mg_tx_device_stream_t* tx_common_stream = (mg_tx_device_stream_t*)tx_stream;


		// filter
		mg_sdp_media_base_t* audio_l = m_sdp_local.get_media(audio_stream->getId());
		if (audio_l == nullptr)
		{
			rx_stream->setMediaParams(*audio_r);
			tx_stream->setMediaParams(*audio_r);
		}
		else
		{
			rx_stream->setMediaParams(*audio_l);
			tx_stream->setMediaParams(*audio_r);

			mg_media_element_list_t* elemens_tx = tx_stream->getMediaParams().get_media_elements();
			for (int i = 0; i < elemens_tx->getCount(); i++)
			{
				mg_media_element_t* el = elemens_tx->getAt(i);
				if (el == nullptr)
				{
					continue;
				}
				bool find = false;
				for (int j = 0; j < audio_l->get_media_elements()->getCount(); j++)
				{
					mg_media_element_t* el_l = audio_l->get_media_elements()->getAt(j);
					if (el_l == nullptr)
					{
						continue;
					}

					if (el_l->compare_with(el))
					{
						find = true;
						break;
					}
				}
				if (!find)
				{
					elemens_tx->removeAt(i);
					i--;
				}
			}
		}

		return true;
	}
	//------------------------------------------------------------------------------
	bool DeviceTermination::prepare_answer_audio(MediaSession* audio_stream, const rtl::String& local_sdp)
	{
		mg_tx_stream_type_t type_tx = mg_tx_stream_type_t::mg_tx_device_audio_e;
		mg_rx_stream_type_t type_rx = mg_rx_stream_type_t::mg_rx_device_audio_e;
		rtl::String type_str(MG_STREAM_TYPE_AUDIO_STR);

		if (local_sdp.isEmpty())
		{
			return false;
		}

		if (!m_sdp_local.read(local_sdp, false, audio_stream->getId()))
		{
			ERROR_MSG("local sdp read fail!");
			return false;
		}

		mg_tx_stream_t* tx_stream = find_tx_stream_by_type(type_tx, audio_stream);
		if (tx_stream == nullptr)
		{
			return false;
		}
		//mg_tx_device_stream_t* tx_common_stream = (mg_tx_device_stream_t*)tx_stream;

		mg_rx_stream_t* rx_stream = find_rx_stream_by_type(type_rx, audio_stream);
		if (rx_stream == nullptr)
		{
			return false;
		}

		mg_sdp_media_base_t* audio_l = m_sdp_local.get_media(audio_stream->getId());
		if (audio_l == nullptr)
		{
			return false;
		}
		if (audio_l->get_media_elements()->getCount() != 0)
		{
			if (audio_l->get_media_elements()->getAt(0)->get_payload_id() == 101)
			{
				if (audio_l->get_media_elements()->create_with_copy_from(audio_l->get_media_elements()->getAt(0), mg_media_element_list_t::POSITION_END))
				{
					audio_l->get_media_elements()->removeAt(0);
				}
			}
		}

		rx_stream->setMediaParams(*audio_l);

		mg_sdp_media_base_t* audio_r = m_sdp_remote.get_media(audio_stream->getId());
		if (audio_r == nullptr)
		{
			return false;
		}
		tx_stream->setMediaParams(*audio_r);
		mg_media_element_list_t* elemens_tx = tx_stream->getMediaParams().get_media_elements();

		for (int i = 0; i < elemens_tx->getCount(); i++)
		{
			mg_media_element_t* el = elemens_tx->getAt(i);
			if (el == nullptr)
			{
				continue;
			}
			bool find = false;
			for (int j = 0; j < audio_l->get_media_elements()->getCount(); j++)
			{
				mg_media_element_t* el_l = audio_l->get_media_elements()->getAt(j);
				if (el_l == nullptr)
				{
					continue;
				}

				if (el_l->compare_with(el))
				{
					find = true;
					break;
				}
			}
			if (!find)
			{
				elemens_tx->removeAt(i);
				i--;
			}
		}

		return true;
	}
	//------------------------------------------------------------------------------
	bool DeviceTermination::apply_offer_audio(MediaSession* audio_stream, const rtl::String& remote_sdp)
	{
		mg_rx_stream_type_t type_rx = mg_rx_stream_type_t::mg_rx_device_audio_e;
		mg_tx_stream_type_t type_tx = mg_tx_stream_type_t::mg_tx_device_audio_e;
		rtl::String type_str(MG_STREAM_TYPE_AUDIO_STR);

		if (remote_sdp.isEmpty())
		{
			return false;
		}

		if (!m_sdp_remote.read(remote_sdp, false, audio_stream->getId()))
		{
			ERROR_MSG("remote sdp read fail!");
			return false;
		}

		mg_sdp_media_base_t*audio_r = m_sdp_remote.get_media(audio_stream->getId());
		if (audio_r == nullptr)
		{
			return false;
		}

		mg_rx_stream_t* rx_stream = find_rx_stream_by_type(type_rx, audio_stream);
		if (rx_stream == nullptr)
		{
			return false;
		}
		//mg_rx_device_stream_t* rx_common_stream = (mg_rx_device_stream_t*)rx_stream;

		mg_tx_stream_t* tx_stream = find_tx_stream_by_type(type_tx, audio_stream);
		if (tx_stream == nullptr)
		{
			return false;
		}
		//mg_tx_device_stream_t* tx_common_stream = (mg_tx_device_stream_t*)tx_stream;

		// filter
		mg_sdp_media_base_t* audio_l = m_sdp_local.get_media(audio_stream->getId());
		if (audio_l == nullptr)
		{
			rx_stream->setMediaParams(*audio_r);
			tx_stream->setMediaParams(*audio_r);
		}
		else
		{
			rx_stream->setMediaParams(*audio_l);
			tx_stream->setMediaParams(*audio_l);
		}

		return true;
	}
}
//------------------------------------------------------------------------------
