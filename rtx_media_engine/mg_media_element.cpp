﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_media_element.h"
#include "mg_sdp_base.h"
#include "mg_fax_t38.h"
#include "std_string_utils.h"

//------------------------------------------------------------------------------
// Dinamic Codec Payload Type 
//------------------------------------------------------------------------------
#define MG_CPT_DYNAMIC 96

namespace mge
{
	//------------------------------------------------------------------------------
	// Список соответствия пайлоад айди к кодеку.
	//------------------------------------------------------------------------------
	const char* g_mg_codec_payload_types[MG_CPT_DYNAMIC]
	{
			"PCMU",							// G.711 ulaw
			"-", "-",
			"GSM",		// pt_gsm_e			// STD GSM
			"ADPCM",	// pt_g723_e		// ADPCM 4 bit
			"DIV48",	// pt_dvi4_8_e
			"DIV416",	// pt_dvi4_16_e
			"LPC",		// pt_lpc_e			// not supported
			"PCMA",		// pt_pcma_e		// G.711 alaw
			"G722",		// pt_g722_e 
			"L16",		// pt_l16_s_e 
			"L16",		// pt_l16_m_e		// pcm
			"QCELP",	// pt_qcelp_e 
			"CN",		// pt_cn_e 
			"MPA",		// pt_mpa_e 
			"G728",		// pt_g728_e 
			"DIV411",	// pt_dvi4_11_e 
			"DIV422",	// pt_dvi4_22_e 
			"G729",		// pt_g729_e 
			"CISCO",	// pt_cisco_cn_e	// cisco systems comfort noise (unofficial)
			"-", "-", "-", "-", "-",
			"CELB",		// pt_celb_e		// sun systems cell-b video
			"JPEG",		// pt_jpeg_e		// motion jpeg
			"-", "-", "-", "-",
			"H261",		// pt_h261_e		// h.261
			"MPV",		// pt_mpv_e			// mpeg1 or mpeg2 video
			"MP2T",		// pt_mp2t_e		// mpeg2 transport system
			"H263",		// pt_h263_e		// h.263
			"-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-",
			"-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-",
			"DYNA"
	};
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_media_element_t::mg_media_element_t(const char* terminatoin_id, uint16_t payload_id, void* ud)
	{
		m_fax_param = nullptr;

		clear();

		m_payload_id = payload_id;
	
		m_termination.setEmpty();
		m_termination << terminatoin_id;

		if (payload_id < MG_CPT_DYNAMIC)
		{
			set_element_name(g_mg_codec_payload_types[payload_id]);
		}
		else if (payload_id == NULL_ELEMENT)
		{
			set_element_name("NULL");
		}

		m_dtmf = false;
		if (payload_id == 101)
		{
			m_dtmf = true;
		}

		m_handmade = false;
		if (payload_id > 200)
		{
			m_handmade = true;
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_media_element_t::clear()
	{
		m_name.setEmpty();

		clearParams();
	
		m_sample_rate = 0;
		m_channels = 0;
		m_ptime = 0;

		m_dtmf = false;

		if (m_fax_param != nullptr)
		{
			DELETEO(m_fax_param);
			m_fax_param = nullptr;
		}

		m_splitter_params.vad = false;
		m_splitter_params.event_handler = nullptr;
	}

	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_media_element_t::addParam(const char* name)
	{
		rtl::String name_str = name;
		name_str.trim();

		if (name_str.indexOf("T38") != BAD_INDEX)
		{
			parse_t38(name_str);
		}
		else
		{
			m_param_list.add(name_str);
		}

		if (name_str.indexOf("rtpmap") != BAD_INDEX)
		{
			int index = name_str.indexOf('/');
			if (index == BAD_INDEX)
			{
				m_sample_rate = 0;
				m_channels = 0;
			}
			else
			{
				int index_b = index + 1;
				index = name_str.indexOf('/', index_b);
				int count = -1;
				if (index != BAD_INDEX)
				{
					count = index - index_b;
					int index_c = index + 1;
					rtl::String channels_str = name_str.substring(index_c);
					m_channels = (uint32_t)strtoul(channels_str, nullptr, 10);
				}
				else
				{
					m_channels = 1;
				}
				rtl::String sample_rate_str = name_str.substring(index_b, count);
				uint16_t sample_rate = (uint32_t)strtoul(sample_rate_str, nullptr, 10);

				update_samplerate(sample_rate);
			}
		}
		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_media_element_t::compare_with(mg_media_element_t* element) const
	{
		if (element == nullptr)
		{
			return false;
		}

		if (m_name *= element->get_element_name())
		{
			if (m_sample_rate == element->get_sample_rate())
			{
				return (m_channels == element->getChannelCount());
			}
		}

		if (m_payload_id < MG_CPT_DYNAMIC)
		{
			return m_payload_id == element->get_payload_id();
		}

		return false;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_media_element_t::copy_from(mg_media_element_t* element)
	{
		clear();

		if (element == nullptr)
		{
			return false;
		}

		m_termination.setEmpty();
		m_termination.assign(element->get_termination_id());

		m_payload_id = element->get_payload_id();
		m_name.assign(element->get_element_name());

		m_dtmf = element->isDtmf();
		m_handmade = element->isHandmade();

		m_channels = element->getChannelCount();
		update_samplerate(element->get_sample_rate());

		m_param_list.assign(element->m_param_list);

		if (m_fax_param != nullptr)
		{
			memset(m_fax_param, 0, sizeof(mg_fax_t38_params_t));
		}
		mg_fax_t38_params_t* param = element->get_fax_param();
		if (param != nullptr)
		{
			if (m_fax_param == nullptr)
			{
				m_fax_param = NEW mg_fax_t38_params_t();
			}
			memcpy(m_fax_param, param, sizeof(mg_fax_t38_params_t));
		}
		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	uint32_t mg_media_element_t::get_sample_rate()
	{
		if (m_sample_rate == 0)
		{
			parse_rtpmap();
		}

		if (m_sample_rate == 0)
		{
			//LOG_ERROR("M-EL", "MEDIA ELEMENT HAS NO SAMPLE RATE VALUE!!!!");
			return 8000;
		}

		return m_sample_rate;
	}
	//------------------------------------------------------------------------------
	void mg_media_element_t::set_sample_rate(uint32_t sample_rate)
	{
		if (m_sample_rate == sample_rate)
		{
			return;
		}
	
		update_samplerate(sample_rate);

		rtl::String param = get_param_rtpmap();
		if (param.isEmpty())
		{
			write_rtpmap();
			return;
		}

		// a=rtpmap:97 speex/8000/2
		int index = param.indexOf("/");
		if (index == BAD_INDEX)
		{
			write_rtpmap();
			return;
		}

		rtl::String sample_rate_str_old;

		rtl::String sample_rate_str_new;
		sample_rate_str_new << '/' << sample_rate;

		int index_next = param.indexOf("/", index + 1);
		if (index_next == BAD_INDEX)
		{
			sample_rate_str_old << param.substring(index);
		}
		else
		{
			sample_rate_str_old << param.substring(index, index_next - index);
			sample_rate_str_new << '/';
		}

		param.replace(sample_rate_str_old, sample_rate_str_new);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	uint32_t mg_media_element_t::getChannelCount()
	{
		if (m_channels == 0)
		{
			parse_rtpmap();
		}

		if (m_channels == 0)
		{
			return 1;
		}

		return m_channels;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	uint32_t mg_media_element_t::get_ptime()
	{
		if (m_ptime == 0)
		{
			parse_ptime();
		}

		if (m_ptime == 0)
		{
			return 20;
		}

		return m_ptime;
	}
	//------------------------------------------------------------------------------
	void mg_media_element_t::set_ptime(uint32_t ptime)
	{
		if (m_ptime == ptime)
		{
			return;
		}

		m_ptime = ptime;

		for (int i = 0; i < getParamCount(); i++)
		{
			rtl::String param = getParam(i);
			if (param.indexOf("a=ptime") != BAD_INDEX)
			{
				// a=ptime:20
				int index = param.indexOf(":");
				if (index != BAD_INDEX)
				{
					rtl::String time = param.substring(index + 1);
					char buff[10] = { 0 };
					rtl::String ptime_str(rtl::strchr(buff, m_ptime, 10));
					param.replace(time, ptime_str);
				}
				break;
			}
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	rtl::String mg_media_element_t::prepare_param_for_speex()
	{
		// payload_id, channels, sample_rate, bit_rate, vbr
		// пример: 97, 1, 8000, 16000, false
	
		/*
		+------+---------------+-------------+
		| mode | Speex quality |   bit-rate  |
		+------+---------------+-------------+
		|   1  |       0       | 2.15 kbit/s |
		|   2  |       2       | 5.95 kbit/s |
		|   3  |     3 or 4    | 8.00 kbit/s |
		|   4  |     5 or 6    | 11.0 kbit/s |
		|   5  |     7 or 8    | 15.0 kbit/s |
		|   6  |       9       | 18.2 kbit/s |
		|   7  |       10      | 24.6 kbit/s |
		|   8  |       1       | 3.95 kbit/s |
		+------+---------------+-------------+
		Table 1: Mode vs.Bit - Rate for Narrowband

		+------+---------------+-------------------+------------------------+
		| mode | Speex quality | wideband bit-rate |     ultra wideband     |
		|      |               |                   |       bit - rate       |
		+------+---------------+-------------------+------------------------+
		|   0  |       0       |    3.95 kbit/s    |       5.75 kbit/s      |
		|   1  |       1       |    5.75 kbit/s    |       7.55 kbit/s      |
		|   2  |       2       |    7.75 kbit/s    |       9.55 kbit/s      |
		|   3  |       3       |    9.80 kbit/s    |       11.6 kbit/s      |
		|   4  |       4       |    12.8 kbit/s    |       14.6 kbit/s      |
		|   5  |       5       |    16.8 kbit/s    |       18.6 kbit/s      |
		|   6  |       6       |    20.6 kbit/s    |       22.4 kbit/s      |
		|   7  |       7       |    23.8 kbit/s    |       25.6 kbit/s      |
		|   8  |       8       |    27.8 kbit/s    |       29.6 kbit/s      |
		|   9  |       9       |    34.2 kbit/s    |       36.0 kbit/s      |
		|  10  |       10      |    42.2 kbit/s    |       44.0 kbit/s      |
		+------+---------------+-------------------+------------------------+
		Table 2: Mode vs. Bit-Rate for Wideband and Ultra-Wideband
		*/


		// sdp:
		//	m=audio 8088 RTP/AVP 97
		//	a=rtpmap:97 speex/8000
		//	a=fmtp:97 mode="4,any"

		uint16_t payload_id = m_payload_id;
		uint32_t channels = 1;
		uint32_t sample_rate = 8000;
		uint32_t bit_rate = 15000;
		rtl::String vbr("false");

		// сначала нужно получить sample_rate
		// для этого первоначально ищем rtpmap
		sample_rate = get_sample_rate();

		// а уже потом fmtp
		rtl::String line_str = get_param_fmtp();
		if (line_str.isEmpty())
		{
			return rtl::String::empty;
		}
		//-------------------------------------------------------------------
		// a=fmtp:97 mode="4,any"
		rtl::String mode;
		rtl::String first_mode_str;
		int mode_index = line_str.indexOf("mode");
		if (mode_index != BAD_INDEX)
		{
			mode = line_str.substring(mode_index + 5);
			mode.replace('\"', ' ');
			mode.trim();

			int index = mode.indexOf(',');
			if (index == BAD_INDEX)
			{
				first_mode_str = mode;
			}
			else
			{
				first_mode_str = mode.substring(0, index + 1);
			}
		}
		uint32_t first_mode = (uint16_t)strtoul(first_mode_str, nullptr, 10);
		if (sample_rate == 8000)
		{
			/*
			+------+---------------+-------------+
			| mode | Speex quality |   bit-rate  |
			+------+---------------+-------------+
			|   1  |       0       | 2.15 kbit/s |
			|   2  |       2       | 5.95 kbit/s |
			|   3  |     3 or 4    | 8.00 kbit/s |
			|   4  |     5 or 6    | 11.0 kbit/s |
			|   5  |     7 or 8    | 15.0 kbit/s |
			|   6  |       9       | 18.2 kbit/s |
			|   7  |       10      | 24.6 kbit/s |
			|   8  |       1       | 3.95 kbit/s |
			+------+---------------+-------------+
			Bit-Rate for Narrowband
			*/
			if ((first_mode > 8) || (first_mode < 1))
			{
				return rtl::String::empty;
			}

			uint32_t bit_rate_table[9] = { 0, 2150, 5950, 8000, 11000, 15000, 18200, 24600, 3950 };
			bit_rate = bit_rate_table[first_mode];
		}
		else if (sample_rate == 16000)
		{
			/*
			+------+---------------+-------------------+
			| mode | Speex quality | wideband bit-rate |
			|      |               |                   |
			+------+---------------+-------------------+
			|   0  |       0       |    3.95 kbit/s    |
			|   1  |       1       |    5.75 kbit/s    |
			|   2  |       2       |    7.75 kbit/s    |
			|   3  |       3       |    9.80 kbit/s    |
			|   4  |       4       |    12.8 kbit/s    |
			|   5  |       5       |    16.8 kbit/s    |
			|   6  |       6       |    20.6 kbit/s    |
			|   7  |       7       |    23.8 kbit/s    |
			|   8  |       8       |    27.8 kbit/s    |
			|   9  |       9       |    34.2 kbit/s    |
			|  10  |       10      |    42.2 kbit/s    |
			+------+---------------+-------------------+
			Bit-Rate for Wideband
			*/

			if (first_mode > 10)
			{
				return rtl::String::empty;
			}

			uint32_t bit_rate_table[11] = { 3950, 5750, 7750, 9800, 12800, 16800, 20600, 23800, 27800, 34200, 42200 };
			bit_rate = bit_rate_table[first_mode];
		}
		else if (sample_rate == 32000)
		{
			/*
			+------+---------------+------------------------+
			| mode | Speex quality |     ultra wideband     |
			|      |               |       bit - rate       |
			+------+---------------+------------------------+
			|   0  |       0       |       5.75 kbit/s      |
			|   1  |       1       |       7.55 kbit/s      |
			|   2  |       2       |       9.55 kbit/s      |
			|   3  |       3       |       11.6 kbit/s      |
			|   4  |       4       |       14.6 kbit/s      |
			|   5  |       5       |       18.6 kbit/s      |
			|   6  |       6       |       22.4 kbit/s      |
			|   7  |       7       |       25.6 kbit/s      |
			|   8  |       8       |       29.6 kbit/s      |
			|   9  |       9       |       36.0 kbit/s      |
			|  10  |       10      |       44.0 kbit/s      |
			+------+---------------+------------------------+
			Bit-Rate for Ultra-Wideband
			*/
			if (first_mode > 10)
			{
				return rtl::String::empty;
			}

			uint32_t bit_rate_table[11] = { 5750, 7550, 9550, 11600, 14600, 18600, 22400, 25600, 29600, 36000, 44000 };
			bit_rate = bit_rate_table[first_mode];
		}
		//-------------------------------------------------------------------

		//-------------------------------------------------------------------
		// a=fmtp:97 vbr=on;cng=on
		int vbr_index = line_str.indexOf("vbr");
		if (vbr_index != BAD_INDEX)
		{
			int vbr_end = line_str.indexOf(';');
			rtl::String vbr_str = (vbr_end == BAD_INDEX)
				? line_str.substring(vbr_index + 4)
				: line_str.substring(vbr_index + 4, vbr_end - vbr_index - 4);

			vbr = rtl::String::empty;
			if (rtl::String::compare("on", vbr_str, false) == 0)
			{
				vbr.assign("true");
			}
			else
			{
				vbr.assign("false");
			}
		}
		//-------------------------------------------------------------------

		rtl::String param;

		param << payload_id << ", " << channels << ", " << sample_rate << ", " << bit_rate << ", " << vbr;

		return param;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	rtl::String mg_media_element_t::get_param_fmtp()
	{
		// ищем rtpmap
		for (int i = 0; i < getParamCount(); i++)
		{
			rtl::String line_str = getParam(i);
			if (line_str.isEmpty())
			{
				continue;
			}

			if (line_str.indexOf("fmtp") != BAD_INDEX)
			{
				return line_str;
			}
		}

		return rtl::String::empty;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	rtl::String mg_media_element_t::get_param_rtpmap()
	{
		// ищем rtpmap
		for (int i = 0; i < getParamCount(); i++)
		{
			rtl::String line_str = getParam(i);
			if (line_str.isEmpty())
			{
				continue;
			}

			if (line_str.indexOf("rtpmap") != BAD_INDEX)
			{
				return line_str;
			}
		}

		return rtl::String::empty;
	}
	//------------------------------------------------------------------------------
	void mg_media_element_t::parse_rtpmap()
	{
		rtl::String line_str = get_param_rtpmap();
		if (line_str.isEmpty())
		{
			return;
		}

		// a=rtpmap:97 speex/8000/2
		int index = line_str.indexOf("/");
		if (index == BAD_INDEX)
		{
			return;
		}

		rtl::String sample_rate_str;
		rtl::String channels_str;

		int index_next = line_str.indexOf("/", index + 1);
		if (index_next == BAD_INDEX)
		{
			sample_rate_str = line_str.substring(index + 1);
		}
		else
		{
			sample_rate_str = line_str.substring(index + 1, index_next - index - 1);
			channels_str = line_str.substring(index_next + 1);
		}

		sample_rate_str.trim();
		uint16_t sample_rate = (uint16_t)strtoul(sample_rate_str, nullptr, 10);

		channels_str.trim();
		m_channels = (uint32_t)strtoul(channels_str, nullptr, 10);
	
		update_samplerate(sample_rate);
	}
	void mg_media_element_t::update_samplerate(int rate)
	{
		if (m_payload_id == 9 && (m_name.toLower().indexOf("g722") != BAD_INDEX))
		{
			// exception for G722.
			m_sample_rate = 16000;
		}
		else if (m_name.toLower().indexOf("opus") != BAD_INDEX)
		{
			// exception for OPUS.
			m_sample_rate = 16000;
			m_channels = 1;
		}
		else
		{
			m_sample_rate = rate;
		}
	}
	//------------------------------------------------------------------------------
	void mg_media_element_t::write_rtpmap()
	{
		// ищем rtpmap
		int index =-1;
		for (int i = 0; i < getParamCount(); i++)
		{
			rtl::String line_str = getParam(i);
			if (line_str.isEmpty())
			{
				continue;
			}

			if (line_str.indexOf("rtpmap") != BAD_INDEX)
			{
				index = i;
				break;
			}
		}

		rtl::String rtpmap;
		int rate = m_sample_rate;
		int channels = m_channels;

		// exception for G722.
		if (m_payload_id == 9 && (m_name.toLower().indexOf("g722") != BAD_INDEX))
		{
			// exception for G722.
			if (m_sample_rate == 16000)
				rate = 8000;
		}
		else if (m_name.toLower().indexOf("opus") != BAD_INDEX)
		{
			// exception for OPUS
			rate = 48000;
			channels = 2;
		}
	
		int indexR = m_name.indexOf("/");
		if (indexR == BAD_INDEX)
		{
			rtpmap << "a=rtpmap:" << m_payload_id << ' ' << m_name << '/' << rate << '/' << channels;
		}
		else
		{
			rtl::String name = m_name.substring(0, indexR);
			rtpmap << "a=rtpmap:" << m_payload_id << ' ' << name << '/' << rate << '/' << channels;
		}

		if (index >= 0)
		{
			deleteParam(index);
			m_param_list.insert(index, rtpmap);
		}
		else
		{
			addParam(rtpmap);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_media_element_t::parse_ptime()
	{
		for (int i = 0; i < getParamCount(); i++)
		{
			rtl::String param = getParam(i);
			if (param.indexOf("a=ptime") != BAD_INDEX)
			{
				// a=ptime:20
				int index = param.indexOf(":");
				if (index != BAD_INDEX)
				{
					rtl::String time = param.substring(index + 1);
					m_ptime = (uint32_t)strtoul(time, nullptr, 10);
				}
				break;
			}
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_media_element_t::parse_t38(const rtl::String&media_line)
	{
		int index = BAD_INDEX;

		if (m_fax_param == nullptr)
		{
			m_fax_param = NEW mg_fax_t38_params_t();
			memset(m_fax_param, 0, sizeof(mg_fax_t38_params_t));
		}

		//------------------------------------------------------------------------------
		// T38FaxVersion
		index = media_line.indexOf("T38FaxVersion");
		if (index != BAD_INDEX)
		{
			m_fax_param->version = 0;
			index = media_line.indexOf(':');
			if (index == BAD_INDEX)
			{
				return;
			}

			int index_b = index + 1;
			if (media_line[index_b] == ' ')
			{
				index_b++;
			}

			rtl::String value = media_line.substring(index_b);
			uint16_t version = (uint16_t)strtoul(value, nullptr, 10);
			m_fax_param->version = version;
			return;
		}
		//------------------------------------------------------------------------------

		//------------------------------------------------------------------------------
		// T38MaxBitRate
		index = media_line.indexOf("T38MaxBitRate");
		if (index != BAD_INDEX)
		{
			m_fax_param->max_bit_rate = 14400;
			index = media_line.indexOf(':');
			if (index == BAD_INDEX)
			{
				return;
			}

			int index_b = index + 1;
			if (media_line[index_b] == ' ')
			{
				index_b++;
			}

			rtl::String value = media_line.substring(index_b);
			uint16_t max_bit_rate = (uint16_t)strtoul(value, nullptr, 10);
			m_fax_param->max_bit_rate = max_bit_rate;
			return;
		}
		//------------------------------------------------------------------------------

		//------------------------------------------------------------------------------
		// T38FaxFillBitRemoval
		index = media_line.indexOf("T38FaxFillBitRemoval");
		if (index != BAD_INDEX)
		{
			m_fax_param->fill_bit_removal = true;
			return;
		}
		//------------------------------------------------------------------------------

		//------------------------------------------------------------------------------
		// T38FaxTranscodingMMR
		index = media_line.indexOf("T38FaxTranscodingMMR");
		if (index != BAD_INDEX)
		{
			m_fax_param->transcoding_mmr = true;
			return;
		}
		//------------------------------------------------------------------------------

		//------------------------------------------------------------------------------
		// T38FaxTranscodingJBIG
		index = media_line.indexOf("T38FaxTranscodingJBIG");
		if (index != BAD_INDEX)
		{
			m_fax_param->transcoding_jbig = true;
			return;
		}
		//------------------------------------------------------------------------------

		//------------------------------------------------------------------------------
		// T38FaxRateManagement
		index = media_line.indexOf("T38FaxRateManagement");
		if (index != BAD_INDEX)
		{
			m_fax_param->rate_management = T38_TCF_NONE;
			if (media_line.indexOf("localTCF") != BAD_INDEX)
			{
				m_fax_param->rate_management = T38_TCF_LOCAL;
			}
			else if (media_line.indexOf("transferredTCF") != BAD_INDEX)
			{
				m_fax_param->rate_management = T38_TCF_TRANSFERRED;
			}
			return;
		}
		//------------------------------------------------------------------------------

		//------------------------------------------------------------------------------
		// T38FaxMaxBuffer
		index = media_line.indexOf("T38FaxMaxBuffer");
		if (index != BAD_INDEX)
		{
			m_fax_param->max_buffer = 1024;
			index = media_line.indexOf(':');
			if (index == BAD_INDEX)
			{
				return;
			}

			int index_b = index + 1;
			if (media_line[index_b] == ' ')
			{
				index_b++;
			}

			rtl::String value = media_line.substring(index_b);
			uint16_t max_buffer = (uint16_t)strtoul(value, nullptr, 10);
			m_fax_param->max_buffer = max_buffer;
			return;
		}
		//------------------------------------------------------------------------------

		//------------------------------------------------------------------------------
		// T38FaxMaxDatagram
		index = media_line.indexOf("T38FaxMaxDatagram");
		if (index != BAD_INDEX)
		{
			m_fax_param->max_datagram = 320;
			index = media_line.indexOf(':');
			if (index == BAD_INDEX)
			{
				return;
			}

			int index_b = index + 1;
			if (media_line[index_b] == ' ')
			{
				index_b++;
			}

			rtl::String value = media_line.substring(index_b);
			uint16_t max_datagram = (uint16_t)strtoul(value, nullptr, 10);
			m_fax_param->max_datagram = max_datagram;
			return;
		}
		//------------------------------------------------------------------------------

		//------------------------------------------------------------------------------
		// T38FaxMaxIFP
		//------------------------------------------------------------------------------

		//------------------------------------------------------------------------------
		// T38VendorInfo
		index = media_line.indexOf("T38VendorInfo");
		if (index != BAD_INDEX)
		{
			m_fax_param->vendor_info = rtl::String::empty;
			index = media_line.indexOf(':');
			if (index == BAD_INDEX)
			{
				return;
			}

			int index_b = index + 1;
			if (media_line[index_b] == ' ')
			{
				index_b++;
			}

			rtl::String value = media_line.substring(index_b);
			m_fax_param->vendor_info = value;
			return;
		}
		//------------------------------------------------------------------------------
	
		//------------------------------------------------------------------------------
		// T38FaxUdpEC
		index = media_line.indexOf("T38FaxUdpEC");
		if (index != BAD_INDEX)
		{
			if (media_line.indexOf("Redundancy") != BAD_INDEX)
			{
				m_fax_param->udp_ec = 1;// Redundancy;
			}
			else if (media_line.indexOf("FEC") != BAD_INDEX)
			{
				m_fax_param->udp_ec = 2;// FEC;
			}
			else
			{
				m_fax_param->udp_ec = 0;// None;
			}
			return;
		}
		//------------------------------------------------------------------------------

		//------------------------------------------------------------------------------
		// T38FaxUdpECDepth
		index = media_line.indexOf("T38FaxUdpECDepth");
		if (index != BAD_INDEX)
		{
			m_fax_param->ecm_entries = 5;

			index = media_line.indexOf(':');
			if (index == BAD_INDEX)
			{
				return;
			}

			int index_b = index + 1;
			if (media_line[index_b] == ' ')
			{
				index_b++;
			}

			uint16_t entries = 1;
			rtl::String value = media_line.substring(index_b);
			int index_c = value.indexOf(' ');
			if (index_c == BAD_INDEX)
			{
				entries = (uint16_t)strtoul(value, nullptr, 10);
			}
			else
			{
				rtl::String entries_str = media_line.substring(index_b, index_c);
				entries = (uint16_t)strtoul(entries_str, nullptr, 10);
			}
			m_fax_param->ecm_entries = entries;
			return;
		}
		//------------------------------------------------------------------------------

		//------------------------------------------------------------------------------
		// T38FaxUdpFECMaxSpan
		index = media_line.indexOf("T38FaxUdpFECMaxSpan");
		if (index != BAD_INDEX)
		{
			m_fax_param->ecm_span = 1;

			index = media_line.indexOf(':');
			if (index == BAD_INDEX)
			{
				return;
			}

			int index_b = index + 1;
			if (media_line[index_b] == ' ')
			{
				index_b++;
			}

			rtl::String value = media_line.substring(index_b);
			uint16_t span = (uint16_t)strtoul(value, nullptr, 10);
			m_fax_param->ecm_span = span;
			return;
		}
		//------------------------------------------------------------------------------
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	struct mg_fax_t38_params_t* mg_media_element_t::get_fax_param() const
	{
		return m_fax_param;
	}
	//------------------------------------------------------------------------------
	void mg_media_element_t::set_fax_param(mg_fax_t38_params_t* param)
	{
		if (param == nullptr)
		{
			return;
		}

		if (m_fax_param == nullptr)
		{
			m_fax_param = NEW mg_fax_t38_params_t();
			memset(m_fax_param, 0, sizeof(mg_fax_t38_params_t));
		}

		memcpy(m_fax_param, param, sizeof(mg_fax_t38_params_t));
	}
	//------------------------------------------------------------------------------
	bool mg_media_element_t::write_fax_param(rtl::String& ss) const
	{
		if (m_fax_param == nullptr)
		{
			return true;
		}

		ss << "\r\n";

		//------------------------------------------------------------------------------
		// T38FaxVersion
		ss << "a=T38FaxVersion:" << m_fax_param->version;
		//------------------------------------------------------------------------------

		//------------------------------------------------------------------------------
		// T38MaxBitRate
		ss << "\r\n";
		ss << "a=T38MaxBitRate:" << m_fax_param->max_bit_rate;
		//------------------------------------------------------------------------------

		//------------------------------------------------------------------------------
		// T38FaxFillBitRemoval
		if (m_fax_param->fill_bit_removal)
		{
			ss << "\r\n";
			ss << "a=T38FaxFillBitRemoval";
		}
		//------------------------------------------------------------------------------

		//------------------------------------------------------------------------------
		// T38FaxTranscodingMMR
		if (m_fax_param->transcoding_mmr)
		{
			ss << "\r\n";
			ss << "a=T38FaxTranscodingMMR";
		}
		//------------------------------------------------------------------------------

		//------------------------------------------------------------------------------
		// T38FaxTranscodingJBIG
		if (m_fax_param->transcoding_jbig)
		{
			ss << "\r\n";
			ss << "a=T38FaxTranscodingJBIG";
		}
		//------------------------------------------------------------------------------

		//------------------------------------------------------------------------------
		// T38FaxRateManagement
		if (m_fax_param->rate_management == T38_TCF_LOCAL)
		{
			ss << "\r\n";
			ss << "a=T38FaxRateManagement:localTCF";
		}
		else if (m_fax_param->rate_management == T38_TCF_TRANSFERRED)
		{
			ss << "\r\n";
			ss << "a=T38FaxRateManagement:transferredTCF";
		}
		//------------------------------------------------------------------------------

		//------------------------------------------------------------------------------
		// T38FaxMaxBuffer
		ss << "\r\n";
		ss << "a=T38FaxMaxBuffer:" << m_fax_param->max_buffer;
		//------------------------------------------------------------------------------

		//------------------------------------------------------------------------------
		// T38FaxMaxDatagram
		ss << "\r\n";
		ss << "a=T38FaxMaxDatagram:" << m_fax_param->max_datagram;
		//------------------------------------------------------------------------------

		//------------------------------------------------------------------------------
		// T38FaxMaxIFP
		//------------------------------------------------------------------------------

		//------------------------------------------------------------------------------
		// T38VendorInfo
		if (!m_fax_param->vendor_info.isEmpty())
		{
			ss << "\r\n";
			ss << "a=T38VendorInfo:" << m_fax_param->vendor_info;
		}
		//------------------------------------------------------------------------------

		//------------------------------------------------------------------------------
		// T38FaxUdpEC
		if (m_fax_param->udp_ec == 1)
		{
			ss << "\r\n";
			ss << "a=T38FaxUdpEC:t38UDPRedundancy";
		}
		else if (m_fax_param->udp_ec == 2)
		{
			ss << "\r\n";
			ss << "a=T38FaxUdpEC:t38UDPFEC";
		}
		//------------------------------------------------------------------------------

		//------------------------------------------------------------------------------
		// T38FaxUdpECDepth
		//if (m_fax_param->ecm_entries > 0)
		//{
		//	ss << "\r\n";
		//	ss << "a=T38FaxUdpECDepth:" << m_fax_param->ecm_entries;
		//}
		////------------------------------------------------------------------------------

		////------------------------------------------------------------------------------
		//// T38FaxUdpFECMaxSpan
		//if (m_fax_param->ecm_span > 0)
		//{
		//	ss << "\r\n";
		//	ss << "a=T38FaxUdpFECMaxSpan:" << m_fax_param->ecm_span;
		//}
		//------------------------------------------------------------------------------

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_media_element_list_t::mg_media_element_list_t()
	{
		m_elements.clear();

		m_priority_element_index = -1;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_media_element_list_t::~mg_media_element_list_t()
	{
		clear();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_media_element_t* mg_media_element_list_t::create(const char* term_id, uint16_t payload_id, int position)
	{
		mg_media_element_t* element = NEW mg_media_element_t(term_id, payload_id, nullptr);

		if ((position < 0) || (m_elements.getCount() <= position))
		{
			m_elements.add(element);
		}
		else
		{
			m_elements.insert(position, element);
		}

		return element;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_media_element_list_t::create_with_copy_from(mg_media_element_t* element, int position)
	{
		if (element == nullptr)
		{
			return false;
		}

		if (!rtx_codec__isKnownFormat(element->get_element_name()))
		{
			return false;
		}

		mg_media_element_t* new_element = create(element->get_termination_id(), element->get_payload_id(), position);
	
		
		if (!new_element->copy_from(element))
		{
			int index = 0;
			if ((position < 0) || (m_elements.getCount() <= position))
			{
				index = m_elements.getCount() - 1;
			}
			else
			{
				index = position;
			}
			removeAt(index);
			return false;
		}

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	const int mg_media_element_list_t::getCount() const
	{
		return m_elements.getCount();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_media_element_t* mg_media_element_list_t::getAt(int index) const
	{
		if (m_elements.getCount() <= index)
		{
			return nullptr;
		}

		return m_elements.getAt(index);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_media_element_list_t::set_priority_index(uint32_t index)
	{
		m_priority_element_index = index;
	}
	//------------------------------------------------------------------------------
	mg_media_element_t*	mg_media_element_list_t::get_priority() const
	{
		if (m_elements.getCount() == 1)
		{
			return m_elements.getAt(0);
		}

		if (m_priority_element_index == -1)
		{
			return nullptr;
		}

		if (m_priority_element_index >= m_elements.getCount())
		{
			return nullptr;
		}

		return m_elements.getAt(m_priority_element_index);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_media_element_list_t::removeAt(int index)
	{
		int count = m_elements.getCount();
		if (count <= index)
		{
			return;
		}

		mg_media_element_t* el = m_elements.getAt(index);
		if (el != nullptr)
		{
			DELETEO(el);
			el = nullptr;
		}
	
		m_elements.removeAt(index);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_media_element_list_t::clear()
	{
		int count = m_elements.getCount();
		if (count == 0)
		{
			return;
		}

		for (int i = 0; i < count; i ++)
		{
			mg_media_element_t* el = m_elements.getAt(i);
			if (el == nullptr)
			{
				continue;
			}
			DELETEO(el);
			el = nullptr;
		}

		m_elements.clear();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_media_element_list_t::compare_sdp_media_param(const rtl::String&media_line)
	{
		for (int i = 0; i < m_elements.getCount(); i++)
		{
			mg_media_element_t* payload_element = m_elements.getAt(i);
			if (payload_element == nullptr)
			{
				continue;
			}

			for (int j = 0; j < payload_element->getParamCount(); j++)
			{
				rtl::String param = payload_element->getParam(j);
				if (rtl::String::compare(media_line, param, false) == 0)
				{
					return true;
				}
			}
		}
	
		return false;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_media_element_t* mg_media_element_list_t::find(uint16_t payload_id)
	{
		int count = m_elements.getCount();
		if (count == 0)
		{
			return nullptr;
		}

		for (int i = 0; i < count; i++)
		{
			mg_media_element_t* el = m_elements.getAt(i);
			if (el == nullptr)
			{
				continue;
			}

			if (el->get_payload_id() == payload_id)
			{
				return el;
			}
		}

		return nullptr;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_media_element_t* mg_media_element_list_t::find(const char* encoding)
	{
		int count = m_elements.getCount();

		for (int i = 0; i < count; i++)
		{
			mg_media_element_t* el = m_elements.getAt(i);

			if (rtl::String::compare(el->get_element_name(), encoding, true) == 0)
			{
				return el;
			}
		}

		return nullptr;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_media_element_list_t::copy_from(const mg_media_element_list_t* element_list)
	{
		clear();

		if (element_list == nullptr)
		{
			return false;
		}

		const int count = element_list->getCount();
		if (count == 0)
		{
			return false;
		}

		for (int i = 0; i < count; i++)
		{
			mg_media_element_t* el = element_list->getAt(i);
			if (el == nullptr)
			{
				continue;
			}

			if (!rtx_codec__isKnownFormat(el->get_element_name()))
			{
				LOG_CALL("MEDIA", "Format %s (%u) filtered!", el->get_element_name(), el->get_payload_id());
				continue;
			}

			mg_media_element_t* new_el = NEW mg_media_element_t(el->get_termination_id(), el->get_payload_id(), nullptr);
			new_el->copy_from(el);
			m_elements.add(new_el);
		}

		m_priority_element_index = element_list->m_priority_element_index;

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	const rtl::String& mg_media_element_list_t::get_termination_id() const
	{
		if (m_elements.getCount() == 0)
		{
			return rtl::String::empty;
		}

		mg_media_element_t* el = m_elements.getAt(0);
		if (el == nullptr)
		{
			return rtl::String::empty;
		}

		return el->get_termination_id();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	const rtl::String	mg_media_element_list_t::to_string()
	{
		rtl::String str;
		for (int i = 0; i < m_elements.getCount(); i++)
		{
			mg_media_element_t* el = m_elements.getAt(i);
			str += "\n";
			str += el->get_element_name();
		}
		return str;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_media_element_t*	mg_create_media_element_cn(const char* termination_id, mg_media_element_list_t* list)
	{
		if (list == nullptr)
		{
			return nullptr;
		}

		for (int i = 0; i < list->getCount(); i++)
		{
			mg_media_element_t* el = list->getAt(i);
			if (el == nullptr)
			{
				continue;
			}
			if (el->get_payload_id() == 13)
			{
				return el;
			}
		}

		return list->create(termination_id, 13, mg_media_element_list_t::POSITION_END);
	}
	//------------------------------------------------------------------------------
	mg_media_element_t*	mg_create_media_element_audio_conf_recorder(const char* termination_id, mg_media_element_list_t* list)
	{
		if (list == nullptr)
		{
			return nullptr;
		}

		mg_media_element_t* element_rec = list->create(termination_id, 11, mg_media_element_list_t::POSITION_END);
		if (element_rec != nullptr)
		{
			element_rec->set_element_name("CONFERENCE_RECORDER");
		}
		return element_rec;
	}
	//------------------------------------------------------------------------------
	mg_media_element_t*	mg_create_media_element_audio_conf_in(const char* termination_id, mg_media_element_list_t* list)
	{
		if (list == nullptr)
		{
			return nullptr;
		}
	
		mg_media_element_t* element = list->create(termination_id, 202, mg_media_element_list_t::POSITION_END);
		if (element != nullptr)
		{
			element->set_element_name("conf_in");
			element->addParam("a=rtpmap: 202 CONF_IN/8000/1");
			element->addParam("a=ptime:20");
		}

		return element;
	}
	//------------------------------------------------------------------------------
	mg_media_element_t*	mg_create_media_element_audio_conf_out(const char* termination_id, mg_media_element_list_t* list)
	{
		if (list == nullptr)
		{
			return nullptr;
		}

		mg_media_element_t* element = list->create(termination_id, 203, mg_media_element_list_t::POSITION_END);
		if (element != nullptr)
		{
			element->set_element_name("conf_out");
			element->addParam("a=rtpmap: 203 CONF_OUT/8000/1");
			element->addParam("a=ptime:20");
		}

		return element;
	}
	mg_media_element_t*	mg_create_media_element_video_conf_in(const char* termination_id, mg_media_element_list_t* list)
	{
		if (list == nullptr)
		{
			return nullptr;
		}

		mg_media_element_t* element = list->create(termination_id, 202, mg_media_element_list_t::POSITION_END);
		if (element != nullptr)
		{
			element->set_element_name("vconf_in");
			element->addParam("a=rtpmap: 202 VCONF_IN/90000");
		}

		return element;
	}
	//------------------------------------------------------------------------------
	mg_media_element_t*	mg_create_media_element_video_conf_out(const char* termination_id, mg_media_element_list_t* list)
	{
		if (list == nullptr)
		{
			return nullptr;
		}

		mg_media_element_t* element = list->create(termination_id, 203, mg_media_element_list_t::POSITION_END);
		if (element != nullptr)
		{
			element->set_element_name("vconf_out");
			element->addParam("a=rtpmap: 203 VCONF_OUT/90000");
		}

		return element;
	}
	//------------------------------------------------------------------------------
	mg_media_element_t*	mg_create_media_element_ivr(const char* termination_id, mg_media_element_list_t* list)
	{
		if (list == nullptr)
		{
			return nullptr;
		}

		mg_media_element_t* element = list->create(termination_id, 201, mg_media_element_list_t::POSITION_END);
		if (element != nullptr)
		{
			element->set_element_name("IVR");
			element->addParam("a=rtpmap: 201 IVR/8000/1");
			element->addParam("a=ptime:20");
			list->set_priority_index(0);
		}

		return element;
	}
	//------------------------------------------------------------------------------
	mg_media_element_t*	mg_create_media_element_t30(const char* termination_id, mg_media_element_list_t* list)
	{
		if (list == nullptr)
		{
			return nullptr;
		}

		mg_media_element_t* element = list->create(termination_id, 204, mg_media_element_list_t::POSITION_END);
		if (element != nullptr)
		{
			element->set_element_name("t30");
			list->set_priority_index(0);
		}

		return element;
	}
	//------------------------------------------------------------------------------
	mg_media_element_t*	mg_create_media_element_t38(const char* termination_id, mg_media_element_list_t* list, bool from_sdp)
	{
		if (list == nullptr)
		{
			return nullptr;
		}

		mg_media_element_t* element = list->create(termination_id, 205, mg_media_element_list_t::POSITION_END);
		if (element != nullptr)
		{
			element->set_element_name("t38");
			if (from_sdp)
			{
				element->setHandmade(false);
			}
			else
			{
				// в сдп сами параметры придут.
				mg_fax_t38_params_t params;
				mg_fax_t38_set_default_params(&params);
				element->set_fax_param(&params);
			}

			list->set_priority_index(0);
		}

		return element;
	}
	//------------------------------------------------------------------------------
	mg_media_element_t*	mg_create_media_element_dummy(const char* termination_id, mg_media_element_list_t* list)
	{
		if (list == nullptr)
		{
			return nullptr;
		}

		mg_media_element_t* element = list->create(termination_id, 206, mg_media_element_list_t::POSITION_END);
		if (element != nullptr)
		{
			element->set_element_name("dummy");
			list->set_priority_index(0);
		}

		return element;
	}
}
//------------------------------------------------------------------------------
