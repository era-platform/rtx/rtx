﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_freelock_buffer.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_free_lock_memory_queue_t::mg_free_lock_memory_queue_t()
	{
		m_data_count = 0;
		m_read_idx = 0;
		m_write_idx = 0;
		m_queue_size = 0;
		m_queue = nullptr;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_free_lock_memory_queue_t::~mg_free_lock_memory_queue_t()
	{
		reset();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_free_lock_memory_queue_t::create(int queue_length)
	{
		if (m_queue != nullptr)
		{
			DELETEAR(m_queue);
		}

		m_queue_size = queue_length / 80;
		m_queue = NEW mg_queue_block_t[m_queue_size];
		memset(m_queue, 0, m_queue_size * sizeof(mg_queue_block_t));
		m_write_idx = m_read_idx = 0;
		m_data_count = 0;
	}
	//------------------------------------------------------------------------------
	void mg_free_lock_memory_queue_t::reset()
	{
		if (m_queue != nullptr)
		{
			DELETEAR(m_queue);
			m_queue = nullptr;
		}

		m_queue_size = m_write_idx = m_read_idx = 0;
		m_data_count = 0;
	}
	//------------------------------------------------------------------------------
	// запись в очередь. если данные не кратны 80 байтам то лишние данные отбрасываются
	//------------------------------------------------------------------------------
	int mg_free_lock_memory_queue_t::write(const uint8_t* data, int length)
	{
		mg_queue_block_t* block;
		int offset = 0;//, to_copy = 80;

		while ((block = get_writing_block()) != 0)
		{
			if (length - offset < 80)
			{
				break;
			}

			memcpy(block->buffer, data + offset, 80);
			offset += 80;

			block_written();
		}

		return offset;
	}
	//------------------------------------------------------------------------------
	int mg_free_lock_memory_queue_t::read(uint8_t* data, int size)
	{
		mg_queue_block_t* block;
		int offset = 0;//, to_copy = 80;

		while ((block = get_reading_block()) != 0)
		{
			if (size - offset < 80)
				break;

			memcpy(data + offset, block->buffer, 80);
			offset += 80;

			block_read();
		}

		return offset;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	int mg_free_lock_memory_queue_t::get_available()
	{
		return m_data_count * 80;
	}
	//------------------------------------------------------------------------------
	int mg_free_lock_memory_queue_t::get_free()
	{
		return (m_queue_size - m_data_count) * 80;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_queue_block_t* mg_free_lock_memory_queue_t::get_writing_block()
	{
		if (m_queue[m_write_idx].ready)
		{
			return nullptr;
		}

		return m_queue + m_write_idx;
	}
	//------------------------------------------------------------------------------
	// получение свободного блока для записи: если текущий блок свободен то вернем его
	// audio_queue_t::BLOCK* audio_queue_t::GetNextWriteBlock();
	// отметим блок как записанный
	//------------------------------------------------------------------------------
	void mg_free_lock_memory_queue_t::block_written()
	{
		if (!m_queue[m_write_idx].ready)
		{
			m_queue[m_write_idx++].ready = true;
			if (m_write_idx >= m_queue_size)
			{
				m_write_idx = 0;
			}
			std_interlocked_inc(&m_data_count);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_queue_block_t* mg_free_lock_memory_queue_t::get_reading_block()
	{
		if (m_queue[m_read_idx].ready)
		{
			return m_queue + m_read_idx;
		}

		return nullptr;
	}
	//------------------------------------------------------------------------------
	// получение заполненного блока для чтения: если текущий блок свободен то вернем его
	// audio_queue_t::BLOCK* audio_queue_t::GetNextReadBlock();
	// отметим блок как прочитанный
	//------------------------------------------------------------------------------
	void mg_free_lock_memory_queue_t::block_read()
	{
		if (m_queue[m_read_idx].ready)
		{
			m_queue[m_read_idx++].ready = false;
			if (m_read_idx >= m_queue_size)
			{
				m_read_idx = 0;
			}
			std_interlocked_dec(&m_data_count);
		}
	}
}
//------------------------------------------------------------------------------
