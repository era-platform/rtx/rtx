﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_thread.h"
#include "mg_rx_fax_proxy_stream.h"
#include "mg_session.h"

#include "logdef_stream.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_rx_fax_proxy_stream_t::mg_rx_fax_proxy_stream_t(rtl::Logger* log, uint32_t id, const char* parent_id, MediaSession& session) :
		mg_rx_stream_t(log, id, mg_rx_fax_proxy_e, parent_id, session)
	{
		m_rtp = nullptr;
		rtl::String name;
		name << session.getName() << "-rxfps" << id;
		setName(name);
		setTag("RXS_FXP");
		m_rx_lock.clear();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_rx_fax_proxy_stream_t::~mg_rx_fax_proxy_stream_t()
	{
		m_rtp = nullptr;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_rx_fax_proxy_stream_t::set_channel_event(i_tx_channel_event_handler* rtp)
	{
		if (!isLocked())
		{
			WARNING("stream unlocked.");
			return;
		}

		m_rtp = rtp;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_rx_fax_proxy_stream_t::reset_stream_from_channel_event()
	{
		if (!isLocked())
		{
			WARNING("stream unlocked.");
			return;
		}

		if (m_rtp != nullptr)
		{
			m_rtp->dec_tx_subscriber(0);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_rx_fax_proxy_stream_t::rx_packet_stream(const rtp_channel_t* channel, const rtp_packet* packet, const socket_address* from)
	{
		if (checkAndLock())
		{
			WARNING("stream locked.");
			return false;
		}

		bool result = rx_packet_stream_lock(channel, packet, from);

		releaseLock();
		return result;
	}
	//------------------------------------------------------------------------------
	bool mg_rx_fax_proxy_stream_t::rx_data_stream(const rtp_channel_t* channel, const uint8_t* data, uint32_t len, const socket_address* from)
	{
		if (checkAndLock())
		{
			WARNING("stream locked.");
			return false;
		}

		bool result = rx_data_stream_lock(channel, data, len, from);

		releaseLock();

		return result;
	}
	//------------------------------------------------------------------------------
	bool mg_rx_fax_proxy_stream_t::rx_ctrl_stream(const rtp_channel_t* channel, const rtcp_packet_t* packet, const socket_address* from)
	{
		if (checkAndLock())
		{
			WARNING("stream locked.");
			return false;
		}

		bool result = rx_ctrl_stream_lock(channel, packet, from);

		releaseLock();
		return result;
	}
	//------------------------------------------------------------------------------
	bool mg_rx_fax_proxy_stream_t::rx_packet_stream_lock(const rtp_channel_t* channel, const rtp_packet* packet, const socket_address* from)
	{
		//PLOG_STREAM_WRITE(LOG_PREFIX, "rx_data_stream_lock -- ID(%s): begin.", getName());
		bool result = false;
		if (mg_rx_stream_t::getState() == h248::TerminationStreamMode::Loopback)
		{
			WARNING("stream loopback.");

			if (channel != nullptr)
			{
				rtp_channel_t* channel_loop = (rtp_channel_t*)channel;
				result = channel_loop->send_packet(packet, from);
			}

			return result;
		}

		if (!isActive())
		{
			return result;
		}

		mg_rx_stream_t::getSession().check_address_from(from);
		

		int tube_count = getTubeCount();
		for (int i = 0; i < tube_count; i++)
		{
			Tube* tube_at_list = getTube(i);
			if (tube_at_list == nullptr)
			{
				continue;
			}

			HandlerType type = tube_at_list->getType();
			if (type == HandlerType::Simple)
			{
				tube_at_list->send(tube_at_list, packet);
				result = true;
			}
		}

		//PLOG_STREAM_WRITE(LOG_PREFIX, "rx_data_stream_lock -- ID(%s): end.", getName());
		return result;
	}
	//------------------------------------------------------------------------------
	bool mg_rx_fax_proxy_stream_t::rx_data_stream_lock(const rtp_channel_t* channel, const uint8_t* data, uint32_t len, const socket_address* from)
	{
		bool result = false;
		if (!isActive())
		{
			return result;
		}

		int tube_count = getTubeCount();
		for (int i = 0; i < tube_count; i++)
		{
			Tube* tube_at_list = getTube(i);
			if (tube_at_list == nullptr)
			{
				continue;
			}

			if ((tube_at_list->getType() == HandlerType::Simple) ||
				(tube_at_list->getType() == HandlerType::UDPTL_Recevier) ||
				(tube_at_list->getType() == HandlerType::UDPTL_Transmitter))
			{
				tube_at_list->send(tube_at_list, data, len);
				result = true;
				break;
			}
		}

		return result;
	}
	//------------------------------------------------------------------------------
	bool mg_rx_fax_proxy_stream_t::rx_ctrl_stream_lock(const rtp_channel_t* channel, const rtcp_packet_t* packet, const socket_address* from)
	{
		bool result = false;
		if (!isActive())
		{
			return result;
		}

		//	bool find = false;
		int tube_count = getTubeCount();
		for (int i = 0; i < tube_count; i++)
		{
			Tube* tube_at_list = getTube(i);
			if (tube_at_list == nullptr)
			{
				continue;
			}

			if (tube_at_list->getType() == HandlerType::Simple)
			{
				tube_at_list->send(tube_at_list, packet);
				//find = true;
				result = true;
				break;
			}
		}

		return result;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_rx_fax_proxy_stream_t::isActive()
	{
		if (mg_rx_stream_t::getState() == h248::TerminationStreamMode::SendOnly)
		{
			WARNING("stream is in a send-only state!.");
			return false;
		}

		if (mg_rx_stream_t::getState() == h248::TerminationStreamMode::Inactive)
		{
			WARNING("stream is inactive.");
			return false;
		}

		return true;
	}
}
//------------------------------------------------------------------------------
