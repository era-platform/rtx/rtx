﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_termination.h"
#include "mg_common_termination.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define LOG_PREFIX m_tag //"MG-TERM" // Media Gateway Termination

#include "logdef.h"

namespace mge {
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	Termination::Termination(rtl::Logger* log, const h248::TerminationID& termId, const char* parent_id) :
		m_log(log), m_termId(termId)
	{
		m_name << parent_id << "-" << m_termId.getId();
		
		strcpy(m_tag, "TERM-BAS");

		m_megaco_handler = nullptr;

		m_lock = true;

		m_conference = false;
		m_vId = 0;
		m_vTitle = "Unnamed";

		rtl::res_counter_t::add_ref(g_mge_termination_counter);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	Termination::~Termination()
	{
		rtl::res_counter_t::release(g_mge_termination_counter);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void Termination::mg_termination_add_topology(const h248::TopologyTriple& topology)
	{
		if ((topology.term_from != m_termId.getId()) && (topology.term_to != m_termId.getId()))
		{
			return;
		}

		bool find = false;
		for (int i = 0; i < m_topology_triple_list.getCount(); i++)
		{
			h248::TopologyTriple topology_from_list = m_topology_triple_list.getAt(i);
			if ((topology_from_list.term_from == topology.term_from) && (topology_from_list.term_to == topology.term_to))
			{
				topology_from_list.direction = topology.direction;
				find = true;
				break;
			}
			else if ((topology_from_list.term_from == topology.term_to) && (topology_from_list.term_to == topology.term_from))
			{
				if ((topology.direction == h248::TopologyDirection::Bothway) ||
					(topology.direction == h248::TopologyDirection::Isolate))
				{
					topology_from_list.direction = topology.direction;
					find = true;
					break;
				}
				else
				{
					topology_from_list.direction = h248::TopologyDirection::Isolate;
					find = true;
					break;
				}
			}
		}

		if (!find)
		{
			if ((topology.term_to == m_termId.getId()) && ((topology.direction == h248::TopologyDirection::OnewayBoth) ||
				(topology.direction == h248::TopologyDirection::Oneway) ||
				(topology.direction == h248::TopologyDirection::OnewayExternal)))
			{
				h248::TopologyTriple topol;
				topol.term_to = topology.term_from;
				topol.term_from = topology.term_to;
				topol.direction = h248::TopologyDirection::Isolate;
				m_topology_triple_list.add(topol);
			}
			else
			{
				m_topology_triple_list.add(topology);
			}
		}
	}
	//------------------------------------------------------------------------------
	rtl::ArrayT<h248::TopologyTriple>& Termination::mg_termination_get_topology_list()
	{
		return m_topology_triple_list;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void Termination::mg_termination_set_event_handler(h248::ITerminationEventHandler* handler, uint32_t ctxId)
	{
		m_eventCtxId = ctxId;
		m_megaco_handler = handler;
	}
	//------------------------------------------------------------------------------
	static void megaco__event_handler(void* userData, uint16_t callId, uintptr_t intParam, void* ptrParam)
	{
		h248::ITerminationEventHandler* megaco_handler = (h248::ITerminationEventHandler*)userData;
		h248::EventParams* params = (h248::EventParams*)ptrParam;

		megaco_handler->TerminationEvent_callback(params);
	}

	void Termination::TerminationEvent_callback(h248::EventParams* params)
	{
		ENTER_FMT("type:%d", params->getType());

		if (m_megaco_handler != nullptr)
		{
			params->setCtxId(m_eventCtxId);
			params->setTermId(m_termId);

			rtl::async_call_manager_t::callAsync(megaco__event_handler, m_megaco_handler, 0, 0, params);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool Termination::mg_termination_lock()
	{
		m_lock = true;

		for (int k = 0; k < m_stream_list.getCount(); k++)
		{
			MediaSession* mg_stream_at_list = m_stream_list.getAt(k);
			if (mg_stream_at_list == nullptr)
			{
				continue;
			}

			if (!mg_stream_at_list->lock())
			{
				ERROR_FMT("set lock in stream: %s fail.", mg_stream_at_list->getName());
			}
		}

		return m_lock;
	}
	//------------------------------------------------------------------------------
	bool Termination::mg_termination_unlock()
	{
		if (!m_lock)
		{
			return true;
		}

		m_lock = false;

		for (int j = 0; j < m_stream_list.getCount(); j++)
		{
			MediaSession* mg_stream_at_list = m_stream_list.getAt(j);
			if (mg_stream_at_list == nullptr)
			{
				continue;
			}

			mg_stream_at_list->unlock();
		}

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	MediaSession* Termination::findMediaSession(uint32_t streamId)
	{
		for (int i = 0; i < m_stream_list.getCount(); i++)
		{
			MediaSession* stream_at_list = m_stream_list.getAt(i);
			if (stream_at_list == nullptr)
			{
				continue;
			}

			if (stream_at_list->getId() == streamId)
			{
				return stream_at_list;
			}
		}

		return nullptr;
	}
	//------------------------------------------------------------------------------
	MediaSession* Termination::makeMediaSession(uint32_t streamId)
	{
		// Look for an existing session
		MediaSession* mg_session = findMediaSession(streamId);
		
		if (mg_session == nullptr)
		{
			// if we don’t find it, we will make a new one
			mg_session = NEW MediaSession(m_log, streamId, getName(), this); // g_mg_stream_log

			m_stream_list.add(mg_session);
		}
		
		return mg_session;
	}
	//------------------------------------------------------------------------------
	void Termination::releaseMediaSession(const uint32_t streamId)
	{
		ENTER_FMT("%d", streamId);

		if (m_stream_list.getCount() == 0)
		{
			WARNING("media session list is empty");
			return;
		}

		bool find = false;
		for (int k = 0; k < m_stream_list.getCount(); k++)
		{
			MediaSession* stream = m_stream_list.getAt(k);

			if (stream->getId() == streamId)
			{
				PRINT_FMT("releasing session '%s'", stream->getName());

				stream->lock();
				stream->clear();

				DELETEO(stream);

				find = true;
				break;
			}
		}

		if (!find)
		{
			WARNING("stream not found!");
		}
	}
	//------------------------------------------------------------------------------
	void Termination::destroyMediaSession()
	{
		ENTER();

		if (m_stream_list.isEmpty())
		{
			return;
		}

		for (int i = 0; i < m_stream_list.getCount(); i++)
		{
			MediaSession* session = m_stream_list[i];
			if (session != nullptr)
			{
				session->lock();
				session->clear();

				// TODO: check for rtl::PonterArrayT
				DELETEO(session);
			}
		}

		m_stream_list.clear();

		LEAVE();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool Termination::mg_termination_set_TerminationState(h248::TerminationServiceState state, h248::Field* _reply)
	{
		ENTER_FMT("old: %s, new: %s",
			h248::TerminationServiceState_toString(m_service_state),
			h248::TerminationServiceState_toString(state));

		m_service_state = state;

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	// reply->addErrorDescriptor(h248::ErrorCode::c449_Unsupported_or_unknown_parameter_or_property_value, "TerminationState EventControl has invalid value");
	bool Termination::mg_termination_set_TerminationState_EventControl(bool eventBufferControl, h248::Field* _reply)
	{
		ENTER_FMT("value: %s", STR_BOOL(eventBufferControl));

		m_event_buffer_control = eventBufferControl;

		return true;
	}
	//------------------------------------------------------------------------------
	bool Termination::mg_termination_set_TerminationState_Property(const h248::Field* property, h248::Field* reply)
	{
		WARNING("termination abstract method called!");

		reply->addErrorDescriptor(h248::ErrorCode::c501_Not_implemented, "termination abstract method called!");

		return false;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool Termination::mg_termination_set_LocalControl_Mode(uint16_t streamId, h248::TerminationStreamMode mode, h248::Field* reply)
	{
		ENTER_FMT("streamId:%d, mode:%s", streamId, h248::TerminationStreamMode_toString(mode));

		MediaSession* mg_session = makeMediaSession(streamId);
		if (mg_session == nullptr)
		{
			ERROR_FMT("stream with id:%d not created.", streamId);
			reply->addErrorDescriptor(h248::ErrorCode::c510_Insufficient_resources, "Termination initialization failed");
			return false;
		}

		return mg_session->mg_stream_set_LocalControl_Mode(mode);
	}
	//------------------------------------------------------------------------------
	bool Termination::mg_termination_set_LocalControl_ReserveValue(uint16_t streamId, bool value, h248::Field* reply)
	{
		ENTER_FMT("streamId:%d, value:%s", streamId, STR_BOOL(value));

		MediaSession* mg_session = Termination::findMediaSession(streamId);
		if (mg_session == nullptr)
		{
			ERROR_FMT("stream with id:%d not found", streamId);
			reply->addErrorDescriptor(h248::ErrorCode::c500_Internal_software_failure_in_the_MG_or_MGC, "MediaSession is null");
			return false;
		}

		return mg_session->mg_stream_set_LocalControl_ReserveValue(value);
	}
	//------------------------------------------------------------------------------
	bool Termination::mg_termination_set_LocalControl_ReserveGroup(uint16_t streamId, bool value, h248::Field* reply)
	{
		ENTER_FMT("streamId:%d, value:%s", streamId, STR_BOOL(value));

		MediaSession* mg_session = Termination::findMediaSession(streamId);
		if (mg_session == nullptr)
		{
			ERROR_FMT("stream with id:%d not found", streamId);
			reply->addErrorDescriptor(h248::ErrorCode::c500_Internal_software_failure_in_the_MG_or_MGC, "MediaSession is null");
			return false;
		}

		return mg_session->mg_stream_set_LocalControl_ReserveGroup(value);
	}
	//------------------------------------------------------------------------------
	bool Termination::mg_termination_set_LocalControl_Property(uint16_t streamId, const h248::Field* property, h248::Field* reply)
	{
		ENTER_FMT("streamId:%d", streamId);

		MediaSession* mg_session = Termination::findMediaSession(streamId);
		if (mg_session == nullptr)
		{
			ERROR_FMT("stream with id:%d not found", streamId);
			reply->addErrorDescriptor(h248::ErrorCode::c500_Internal_software_failure_in_the_MG_or_MGC, "MediaSession is null");
			return false;
		}

		return mg_session->mg_stream_set_LocalControl_Property(property);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool Termination::mg_termination_set_Local(uint16_t streamId, const rtl::String& local, rtl::String& answer, h248::Field* reply)
	{
		WARNING("termination abstract method called!");

		reply->addErrorDescriptor(h248::ErrorCode::c501_Not_implemented);

		return false;
	}
	//------------------------------------------------------------------------------
	bool Termination::mg_termination_set_Remote(uint16_t streamId, const rtl::String& remote, h248::Field* reply)
	{
		WARNING("termination abstract method called!");
		
		reply->addErrorDescriptor(h248::ErrorCode::c501_Not_implemented);

		return false;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool Termination::isReady(uint32_t processor_id)
	{
		MediaSession* mg_session = Termination::findMediaSession(processor_id);
		bool cond1 = mg_session != nullptr;
		bool cond2 = mg_session ? mg_session->isReady() : false;

		PRINT_FMT("processorId:%d -> (%s | %s)", processor_id, STR_BOOL(cond1), STR_BOOL(cond2));

		return cond1 && cond2;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool Termination::mg_stream_start(uint32_t streamId)
	{
		MediaSession* mg_session = findMediaSession(streamId);
		if (mg_session == nullptr)
		{
			ERROR_FMT("stream with id:%d not found", streamId);
			return false;
		}

		mg_session->unlock();

		return true;
	}
	//------------------------------------------------------------------------------
	void Termination::mg_stream_stop(uint32_t streamId)
	{
		MediaSession* mg_session = findMediaSession(streamId);
		if (mg_session == nullptr)
		{
			ERROR_FMT("stream with id:%d not found", streamId);
			return;
		}

		mg_session->lock();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool Termination::mg_termination_set_events(uint16_t streamId, const h248::Field* _propertySet, h248::Field* reply)
	{
		ERROR_MSG("function not implemented");
		reply->addErrorDescriptor(h248::ErrorCode::c449_Unsupported_or_unknown_parameter_or_property_value);
		return false;
	}
	//------------------------------------------------------------------------------
	bool Termination::mg_termination_set_signals(uint16_t streamId, const h248::Field* _propertySet, h248::Field* reply)
	{
		ERROR_MSG("function not implemented");
		reply->addErrorDescriptor(h248::ErrorCode::c449_Unsupported_or_unknown_parameter_or_property_value);
		return false;
	}
	//------------------------------------------------------------------------------
	bool Termination::mg_termination_set_property(const h248::Field* _property, h248::Field* reply)
	{
		ERROR_MSG("function not implemented");
		reply->addErrorDescriptor(h248::ErrorCode::c449_Unsupported_or_unknown_parameter_or_property_value);
		return false;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool Termination::g_enableVideoRecording = false;
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void Termination::enableVideoRecording(bool flag)
	{
		g_enableVideoRecording = flag;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool Termination::isVideoRecordingEnabled()
	{
		return g_enableVideoRecording;
	}
}
