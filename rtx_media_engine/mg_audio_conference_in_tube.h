﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_tube.h"
#include "mg_freelock_buffer.h"

namespace mge
{
	//------------------------------------------------------------------------------
	// Труба входящих данный от termination'а в микшер конференции.
	// RTP поток складывает тут данные в буфер и уходит.
	// Сам туб подписан на микшер и поток микшера забирает данные.
	//------------------------------------------------------------------------------
	// !!! ВНИМАНИЕ !!! Поток микшера из объекта "mg_conference_mixer_tube_t"
	// использует указатель на буффер m_samples.
	//------------------------------------------------------------------------------
	class mg_audio_conference_in_tube_t : public Tube
	{
		mg_audio_conference_in_tube_t(const mg_audio_conference_in_tube_t&);
		mg_audio_conference_in_tube_t& operator=(const mg_audio_conference_in_tube_t&);

	public:
		mg_audio_conference_in_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id);
		virtual	~mg_audio_conference_in_tube_t();


		void update_frame_size(uint32_t mixer_frame_size);

		virtual bool init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out) override;
		virtual void send(const INextDataHandler* out_handler, const rtp_packet* packet) override;
		virtual void send(const INextDataHandler* out_handler, const uint8_t* data, uint32_t len) override;
		virtual bool unsubscribe(const INextDataHandler* out_handler) override;
		virtual bool free() override;

		// Получить данные для микшера.
		const uint16_t* get_media();

		// Увеличить счетчик подписанных на данный туб.
		void subscriber_add();
		uint64_t get_subscribers_count();
		bool can_delete();
		void add_to_conference();
		void deleted_from_conference();

		// помечаем к на какой терминэйшн подписан туб.
		void set_termination_name(const char* termination_id);
		const char* get_termination_name();

	private:
		// К какому терминатору подключен текущий туб.
		rtl::String m_termination_id;
		mg_free_lock_memory_queue_t m_freelock_buffer;
		// Буффер используется в микшере.
		// ptime - 30, sample rate - 32000
		static const uint32_t m_mixer_samples_count_max = 30 * 32000 / 1000;
		uint32_t m_mixer_samples_count;
		// !!! ВНИМАНИЕ !!!
		// При работе поток микшера использует указатель на этот объект!!!
		uint16_t m_mixer_samples[m_mixer_samples_count_max];
		// На этот туб может ссылатся несколько входящих тубов,
		// во избежания удаления туба пока он используется другими,
		// добавлен этот счетчик.
		volatile uint64_t m_subscriber_count;
		volatile bool m_deleted_from_conference;
	};
}
//------------------------------------------------------------------------------
