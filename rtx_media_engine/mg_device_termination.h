﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "mg_termination.h"
#include "mg_session.h"

namespace mge {
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	class DeviceTermination : public Termination//, public IAudioDeviceInputCallback
	{
	public:
		DeviceTermination(rtl::Logger* log, const h248::TerminationID& termId, const char* parent_id);
		virtual ~DeviceTermination() override;

		//virtual bool mg_stream_start(uint32_t streamId) override;
		//virtual void mg_stream_stop(uint32_t streamId) override;

		//------------------------------------------------------------------------------
		// методы контекста

		// конструктор и деструктор
		virtual bool mg_termination_initialize(h248::Field* reply) override;
		virtual bool mg_termination_destroy(h248::Field* reply) override;

		// настройки Local и Remote
		virtual bool mg_termination_set_Local_Remote(uint16_t streamId, const rtl::String& remote_sdp, const rtl::String& local_sdp_offer, h248::Field* reply) override;
		virtual bool mg_termination_get_Local(uint16_t streamId, rtl::String& local_sdp_answer, h248::Field* reply) override;

		//------------------------------------------------------------------------------
		bool prepare_audio(MediaSession* audio_stream, const rtl::String& remote_sdp, const rtl::String& local_sdp);

		bool prepare_offer_audio(MediaSession* audio_stream, const rtl::String& local_sdp);
		bool apply_answer_audio(MediaSession* audio_stream, const rtl::String& remote_sdp);
		bool prepare_answer_audio(MediaSession* audio_stream, const rtl::String& local_sdp);
		bool apply_offer_audio(MediaSession* audio_stream, const rtl::String& remote_sdp);

		mg_rx_stream_t*	find_rx_stream_by_type(mg_rx_stream_type_t type_rx, MediaSession* mg_stream);
		mg_tx_stream_t* find_tx_stream_by_type(mg_tx_stream_type_t type_tx, MediaSession* mg_stream);

	private:
		bool							m_event_buffer_control;
		mg_sdp_session_t				m_sdp_local;
		mg_sdp_session_t				m_sdp_remote;
	};
}
//------------------------------------------------------------------------------
