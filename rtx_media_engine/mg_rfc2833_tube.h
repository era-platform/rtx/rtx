﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_tube.h"

namespace mge
{
	class mg_rx_stream_t;

	//------------------------------------------------------------------------------
	// Труба DTMF детектор.
	// На входе стрим. На выходе ничего.
	// Приходит пакет и если дтмф, то генерируем сигнал.
	//------------------------------------------------------------------------------
	class mg_rfc2833_tube_t : public Tube
	{
	public:
		mg_rfc2833_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id);
		virtual	~mg_rfc2833_tube_t();

		void set_stream(mg_rx_stream_t* rx_stream);

		virtual bool init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out) override;
		virtual void send(const INextDataHandler* out_handler, const rtp_packet* packet) override;
		virtual bool unsubscribe(const INextDataHandler* out_handler) override;

	private:
		void raise_dtmf_received_event(char tone, uint32_t duration);

	private:
		mg_rx_stream_t* m_rx_stream;
		bool m_recv_complete;				// символ распознан и передан обработчикам -- ожидаем новый символ!
		uint8_t m_recv_tone;				// получаемый символ
		uint32_t m_recv_duration;			// продолжительность тона
		uint32_t m_recv_timestamp;			// таймштамп текущего тона
		uint32_t m_recv_last_packet_time;	// время последнего пакета
	};
}
//------------------------------------------------------------------------------
