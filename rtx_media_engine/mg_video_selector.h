/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_audio_analyzer.h"

namespace mge
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	struct VideoSelectorEvents
	{
		virtual void videoSelector__priorityChanged(uint32_t termId, bool vadDetected) = 0;
	};
	//-----------------------------------------------
	//
	//-----------------------------------------------
	class VideoSelector : public VadDetector
	{
	private:
		VideoSelectorEvents* m_handler;

	private:
		virtual void VadDetector__audioSignalChanged(uint32_t termId, bool VAD, int volume) override;

	public:
		VideoSelector() : m_handler(nullptr) { }
		virtual ~VideoSelector();

		void setEventHandler(VideoSelectorEvents* handler) { m_handler = handler; }
	};
}
//-----------------------------------------------
