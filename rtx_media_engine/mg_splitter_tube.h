﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_tube.h"
#include "mg_audio_analyzer.h"

namespace mge
{
	//------------------------------------------------------------------------------
	// Разветвитель. В этом месте пакет идет по двум разным трубам.
	// RTX-97.
	//------------------------------------------------------------------------------
	class mg_splitter_tube_t : public Tube
	{
		mg_splitter_tube_t(const mg_splitter_tube_t&);
		mg_splitter_tube_t& operator=(const mg_splitter_tube_t&);

	public:
		mg_splitter_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id);
		virtual	~mg_splitter_tube_t();

		bool set_out_handler_2(INextDataHandler* out_handler);

		virtual bool init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out) override;
		virtual void send(const INextDataHandler* out_handler, const rtp_packet* packet) override;
		virtual bool unsubscribe(const INextDataHandler* out_handler) override;

	private:
		uint32_t					m_id;
		rtl::String					m_name;
		INextDataHandler*		m_out_2;
	};
}
//------------------------------------------------------------------------------
