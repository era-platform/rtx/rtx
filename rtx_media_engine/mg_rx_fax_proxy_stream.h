﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_rx_stream.h"

namespace mge
{
	//------------------------------------------------------------------------------
	// 
	//------------------------------------------------------------------------------
	class mg_rx_fax_proxy_stream_t : public mg_rx_stream_t, public i_rx_channel_event_handler
	{
	public:
		mg_rx_fax_proxy_stream_t(rtl::Logger* log, uint32_t id, const char* parent_id, MediaSession& session);
		virtual ~mg_rx_fax_proxy_stream_t();

		// устанавливаем канал у котороги мы в подписках.
		// нужно для того что б отписаться при останвке стрима.
		void set_channel_event(i_tx_channel_event_handler* rtp);
		// вызываем функцию канала что б он отписал текущий стрим.
		void reset_stream_from_channel_event();

		bool isActive();

		//------------------------------------------------------------------------------
		// << i_rx_channel_event_handler >>
		virtual bool rx_packet_stream(const rtp_channel_t* channel, const rtp_packet* packet, const socket_address* from);
		virtual bool rx_data_stream(const rtp_channel_t* channel, const uint8_t* data, uint32_t len, const socket_address* from);
		virtual bool rx_ctrl_stream(const rtp_channel_t* channel, const rtcp_packet_t* packet, const socket_address* from);
		//------------------------------------------------------------------------------
	private:
		bool rx_packet_stream_lock(const rtp_channel_t* channel, const rtp_packet* packet, const socket_address* from);
		bool rx_data_stream_lock(const rtp_channel_t* channel, const uint8_t* data, uint32_t len, const socket_address* from);
		bool rx_ctrl_stream_lock(const rtp_channel_t* channel, const rtcp_packet_t* packet, const socket_address* from);

	private:
		i_tx_channel_event_handler* m_rtp;
		std::atomic_flag m_rx_lock;
	};
}
//------------------------------------------------------------------------------
