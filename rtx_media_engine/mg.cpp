﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg.h"
#include "mmt_timer.h"
#include "tls_certificate_manager.h"
#include "mg_fax_t30.h"

namespace mge {
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	media_gateway_t* g_media_gateway = nullptr;
	uint32_t g_id_mg_last = 0;
	rtl::FileStream rtpStat;
	static void stat_timer_proc(rtl::Timer* sender, uint32_t elapsed, void* user_data, int tid);
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
#define LOG_PREFIX "MG" // Media Gateway
#define MAX_MG_CONTEXT_COUNT 10000
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	media_gateway_t::media_gateway_t(rtl::Logger* log) :
		m_log(log), m_mutex("media-gateway")
	{
		g_id_mg_last++;

		m_id = g_id_mg_last;

		m_megaco_callback = nullptr;

		srand((int32_t)time(nullptr));

		m_context_id_counter = ((uint32_t)rand() & 0x000005D5);

		rtp_port_manager_t::initialize(log);

		m_device_in_man = nullptr;
		m_device_out_man = nullptr;

		rtl::String path = Log.get_folder();
		path += "/rtp_stat.txt";
		rtpStat.create_or_append(path);

		rtl::Timer::setTimer(stat_timer_proc, nullptr, 0, 5000, true);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	media_gateway_t::~media_gateway_t()
	{
		rtl::Timer::stopTimer(stat_timer_proc, nullptr, 0);

		rtpStat.close();

		MediaGate_destroy();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	uint32_t media_gateway_t::mg_get_id()
	{
		return m_id;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool media_gateway_t::MediaGate_initialize(h248::IMediaGateEventHandler* callback)
	{
		PLOG_MEDIA_WRITE(LOG_PREFIX, "MediaGate_initialize -- ID(%d): begin.", m_id);

		if (callback == nullptr)
		{
			PLOG_MEDIA_ERROR(LOG_PREFIX, "MediaGate_initialize -- ID(%d): callback is null.", m_id);
			return false;
		}

		if (m_context_list.getCount() != 0)
		{
			PLOG_MEDIA_WARNING(LOG_PREFIX, "MediaGate_initialize -- ID(%d): context list not empty.", m_id);
			destroy_all_contexts();
		}

		m_megaco_callback = callback;

		media::Metronome::initializeMetronomeManager();

		rtl::String cert(Config.get_config_value(MG_PATH_TO_CERT));
		if (!cert.isEmpty())
		{
			if (cert.indexOf("\\") != BAD_INDEX)
			{
				cert.replace('\\', FS_PATH_DELIMITER);
			}
		}

		rtl::String key(Config.get_config_value(MG_PATH_TO_KEY));
		if (!key.isEmpty())
		{
			if (key.indexOf("\\") != BAD_INDEX)
			{
				key.replace('\\', FS_PATH_DELIMITER);
			}
		}

		rtl::String pass(Config.get_config_value(MG_CERT_PASS));

		if (cert.indexOf(".pfx") == BAD_INDEX)
		{
			init_openssl_lib(cert, key, pass, m_log);
		}
		else
		{
			init_openssl_lib(cert, pass, m_log);
		}

		PLOG_MEDIA_WRITE(LOG_PREFIX, "MediaGate_initialize -- ID(%d): end.", m_id);
		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void media_gateway_t::MediaGate_destroy()
	{
		PLOG_MEDIA_WRITE(LOG_PREFIX, "MediaGate_destroy -- ID(%d): begin.", m_id);

		//rtl::Timer::stopTimer(this, 0);

		destroy_all_contexts();

		rtp_port_manager_t::destroy();

		destroy_openssl_lib();

		m_device_in_man = nullptr;
		m_device_out_man = nullptr;

		media::Metronome::destroyMetronomeManager();

		PLOG_MEDIA_WRITE(LOG_PREFIX, "MediaGate_destroy -- ID(%d): end.", m_id);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void media_gateway_t::MediaGate_setInterfaceName(const char* iface, const char* addressKey)
	{
		PLOG_MEDIA_WRITE(LOG_PREFIX, "mg_module_set_interface_name -- ID(%d): begin.", m_id);

		rtp_port_manager_t::set_interface(iface, addressKey);

		PLOG_MEDIA_WRITE(LOG_PREFIX, "mg_module_set_interface_name -- ID(%d): end.", m_id);
		return;
	}
	//------------------------------------------------------------------------------
	void media_gateway_t::MediaGate_removeInterfaceName(const char* iface)
	{
		PLOG_MEDIA_WRITE(LOG_PREFIX, "mg_module_remove_interface_name -- ID(%d): begin.", m_id);

		rtp_port_manager_t::free_interface(iface);

		PLOG_MEDIA_WRITE(LOG_PREFIX, "mg_module_remove_interface_name -- ID(%d): end.", m_id);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool media_gateway_t::MediaGate_addPortRangeRTP(const char* addressKey, uint16_t port_begin, uint16_t port_count)
	{
		PLOG_MEDIA_WRITE(LOG_PREFIX, "mg_module_add_rtp_range -- ID(%d): begin.", m_id);

		if (!rtp_port_manager_t::set_port_range(addressKey, port_begin, port_count))
		{
			PLOG_MEDIA_ERROR(LOG_PREFIX, "mg_module_add_rtp_range -- ID(%d): port_manager fail set port range.", m_id);
			return false;
		}

		PLOG_MEDIA_WRITE(LOG_PREFIX, "mg_module_add_rtp_range -- ID(%d): end.", m_id);
		return true;
	}
	//------------------------------------------------------------------------------
	void media_gateway_t::MediaGate_freePortRangeRTP(const char* addressKey)
	{
		PLOG_MEDIA_WRITE(LOG_PREFIX, "mg_module_free_rtp_range -- ID(%d): begin.", m_id);

		rtp_port_manager_t::free_port_range(addressKey);

		PLOG_MEDIA_WRITE(LOG_PREFIX, "mg_module_free_rtp_range -- ID(%d): end.", m_id);
		return;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	h248::IMediaContext* media_gateway_t::MediaGate_createContext()
	{
		PLOG_MEDIA_WRITE(LOG_PREFIX, "MediaGate_createContext -- ID(%d): begin.", m_id);

		MediaContext* context = create_context();
		if (context == nullptr)
		{
			PLOG_MEDIA_ERROR(LOG_PREFIX, "MediaGate_createContext -- ID(%d): create context fail.", m_id);
			return nullptr;
		}

		PLOG_MEDIA_WRITE(LOG_PREFIX, "MediaGate_createContext -- ID(%d): end.", m_id);
		return context;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void media_gateway_t::MediaGate_destroyContext(h248::IMediaContext* mg_context)
	{
		PLOG_MEDIA_WRITE(LOG_PREFIX, "MediaGate_destroyContext -- ID(%d): begin.", m_id);

		if (mg_context == nullptr)
		{
			PLOG_MEDIA_ERROR(LOG_PREFIX, "MediaGate_destroyContext -- ID(%d): context is null.", m_id);
			return;
		}

		MediaContext* context = (MediaContext*)mg_context;
		destroy_context(context->getId());

		PLOG_MEDIA_WRITE(LOG_PREFIX, "MediaGate_destroyContext -- ID(%d): end.", m_id);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------	
	MediaContext* media_gateway_t::create_context()
	{
		PLOG_MEDIA_WRITE(LOG_PREFIX, "create_context -- ID(%d): begin.", m_id);

		if ((m_context_list.getCount() + 1) >= MAX_MG_CONTEXT_COUNT)
		{
			PLOG_MEDIA_ERROR(LOG_PREFIX, "create_context -- ID(%d): the maximum number of context.", m_id);
			return nullptr;
		}

		m_context_id_counter++;

		MediaContext* context = NEW MediaContext(m_log, m_context_id_counter, m_id);

		{
			//rtl::MutexLock lock(m_mutex);
			rtl::MutexWatchLock lock(m_mutex, __FUNCTION__);

			m_context_list.add(context);
		}

		PLOG_MEDIA_WRITE(LOG_PREFIX, "create_context -- ID(%d): end.(%s)", m_id, context->getName());
		return context;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void media_gateway_t::destroy_context(uint32_t context_id)
	{
		PLOG_MEDIA_WRITE(LOG_PREFIX, "destroy_context -- ID(%d): begin.", m_id);

		if (m_context_list.getCount() == 0)
		{
			PLOG_MEDIA_WARNING(LOG_PREFIX, "destroy_context -- ID(%d): context list empty.", m_id);
			return;
		}

		{
			//rtl::MutexLock lock(m_mutex);
			rtl::MutexWatchLock lock(m_mutex, __FUNCTION__);

			for (int i = 0; i < m_context_list.getCount(); i++)
			{
				MediaContext* cont = m_context_list.getAt(i);
				if ((cont != nullptr) && (cont->getId() == context_id))
				{
					cont->mg_context_lock();
					m_context_list.removeAt(i);
					cont = nullptr;
					PLOG_MEDIA_WRITE(LOG_PREFIX, "destroy_context -- ID(%d): delete context with id %d.", m_id, context_id);
					break;
				}
			}
		}

		PLOG_MEDIA_WRITE(LOG_PREFIX, "destroy_context -- ID(%d): end.", m_id);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void media_gateway_t::destroy_all_contexts()
	{
		PLOG_MEDIA_WRITE(LOG_PREFIX, "destroy_all_contexts -- ID(%d): begin.", m_id);

		if (m_context_list.getCount() == 0)
		{
			PLOG_MEDIA_WARNING(LOG_PREFIX, "destroy_all_contexts -- ID(%d): context list empty.", m_id);
			return;
		}

		{
			rtl::MutexWatchLock lock(m_mutex, __FUNCTION__);

			m_context_list.clear();
		}

		PLOG_MEDIA_WRITE(LOG_PREFIX, "destroy_all_contexts -- ID(%d): end.", m_id);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void media_gateway_t::MediaGate_setDeviceIn(h248::IAudioDeviceInput* callback)
	{
		m_device_in_man = callback;
	}
	//------------------------------------------------------------------------------
	h248::IAudioDeviceInput* media_gateway_t::getDeviceIn()
	{
		return m_device_in_man;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void media_gateway_t::MediaGate_setDeviceOut(h248::IAudioDeviceOutput* callback)
	{
		m_device_out_man = callback;
	}
	//------------------------------------------------------------------------------
	h248::IAudioDeviceOutput* media_gateway_t::getDeviceOut() const
	{
		return m_device_out_man;
	}

	static void stat_timer_proc(rtl::Timer* sender, uint32_t elapsed, void* user_data, int tid)
	{
		rtp_channel_t::print_statistics(rtpStat);
	}

	//--------------------------------------
	//
	//--------------------------------------
	uint32_t media_gateway_t::generateContextId()
	{
		uint32_t id = MG_CONTEXT_NULL;

		while (id == MG_CONTEXT_NULL || id == MG_CONTEXT_CHOOSE || id == MG_CONTEXT_ALL)
		{
			id = std_interlocked_inc(&m_context_id_counter);
		}

		return id;
	}

}
//------------------------------------------------------------------------------
