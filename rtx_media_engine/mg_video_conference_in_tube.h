﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_tube.h"
#include "mg_video_mixer.h"

namespace mge
{
	//-----------------------------------------------
	// Труба входящих видео в микшер видео конференции.
	// Собирает полный видео фрейм из пакетов RTP.
	// Как только фрейм готов, он отправляется в микшер.
	//-----------------------------------------------
	class mg_video_conference_in_tube_t : public Tube
	{
	public:
		mg_video_conference_in_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id);
		virtual	~mg_video_conference_in_tube_t();

		virtual bool init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out) override;
		virtual void send(const INextDataHandler* out_handler, const uint8_t* data, uint32_t size) override;
		virtual bool unsubscribe(const INextDataHandler* out_handler) override;
		virtual bool free() override;

		// привязка входного потока видео к выводу в видео микшере
		void setConferenceBindId(mge::VideoMixer* mixer, uint64_t bindId) { m_videoMixer = mixer, m_bindId = bindId; }
		uint64_t getConferenceBindId() const { return m_bindId; }

		// Увеличить счетчик подписанных на данный туб.
		void subscriber_add();
		uint64_t get_subscribers_count();
		bool can_delete();

		void add_to_conference();
		void deleted_from_conference();

		const char* get_termination_name() { return m_termination_name; }

	private:
		rtl::String m_termination_name;
		mge::VideoMixer* m_videoMixer;
		uint64_t m_bindId; // для связи с видео стримом конференции

		// На этот туб может ссылатся несколько входящих тубов,
		// во избежания удаления туба пока он используется другими,
		// добавлен этот счетчик.
		volatile uint64_t m_subscriber_count;
		volatile bool m_deleted_from_conference;
	};
}
//------------------------------------------------------------------------------
