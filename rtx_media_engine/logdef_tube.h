#pragma once

#define LOG_PREFIX getLogTag()

#define __OBJ_NAME__ getName()

#if defined TARGET_OS_WINDOWS

#define ENTER() \
	PLOG_TUBE_WRITE(LOG_PREFIX,"ID(%s) : %s()...", __OBJ_NAME__, __func__);

#define ENTER_FMT(fmt, ...) \
	PLOG_TUBE_WRITE(LOG_PREFIX,"ID(%s) : %s(" fmt ")...", __OBJ_NAME__, __func__, __VA_ARGS__);

#define RETURN_BOOL(res) \
	PLOG_TUBE_WRITE(LOG_PREFIX,"ID(%s) : %s() -> %s", __OBJ_NAME__, __func__, STR_BOOL(res)); \
	return res;

#define RETURN_STR(res) \
	PLOG_TUBE_WRITE(LOG_PREFIX,"ID(%s) : %s() -> %s", __OBJ_NAME__, __func__, res); \
	return res;

#define RETURN_INT(res) \
	PLOG_TUBE_WRITE(LOG_PREFIX,"ID(%s) : %s() -> %ll", __OBJ_NAME__, __func__, res); \
	return res;

#define EXIT() \
	PLOG_TUBE_WRITE(LOG_PREFIX,"ID(%s) : %s() exit", __OBJ_NAME__, __func__); \
	return;

#define RETURN_FMT(res, fmt, ...) \
	PLOG_TUBE_WRITE(LOG_PREFIX,"ID(%s) : %s() -> " fmt, __OBJ_NAME__, __func__, __VA_ARGS__); \
	return res;

#define WARNING(w) \
	PLOG_TUBE_WARNING(LOG_PREFIX,"ID(%s) : %s() : %s", __OBJ_NAME__, __func__, w)

#define WARNING_FMT(fmt, ...) \
	PLOG_TUBE_WARNING(LOG_PREFIX,"ID(%s) : %s() : " fmt, __OBJ_NAME__, __func__, __VA_ARGS__)

#define ERROR_MSG(e) \
	PLOG_TUBE_ERROR(LOG_PREFIX,"ID(%s) : %s() : %s", __OBJ_NAME__, __func__, e)

#define ERROR_FMT(fmt, ...) \
	PLOG_TUBE_ERROR(LOG_PREFIX,"ID(%s) : %s() : " fmt, __OBJ_NAME__, __func__, __VA_ARGS__)

#define PRINT(t) \
	PLOG_TUBE_WRITE(LOG_PREFIX,"ID(%s) : %s() : %s", __OBJ_NAME__, __func__, t)

#define PRINT_FMT(fmt, ...) \
	PLOG_TUBE_WRITE(LOG_PREFIX, "ID(%s) : %s() : " fmt, __OBJ_NAME__, __func__, __VA_ARGS__)

#else

#define ENTER() \
	PLOG_TUBE_WRITE(LOG_PREFIX,"ID(%s) : %s()...", __OBJ_NAME__, __func__);

#define ENTER_FMT(fmt, ...) \
	PLOG_TUBE_WRITE(LOG_PREFIX,"ID(%s) : %s(" fmt ")...", __OBJ_NAME__, __func__, __VA_ARGS__);

#define RETURN_BOOL(res) \
	PLOG_TUBE_WRITE(LOG_PREFIX,"ID(%s) : %s() -> %s", __OBJ_NAME__, __func__, STR_BOOL(res)); \
	return res;

#define RETURN_STR(res) \
	PLOG_TUBE_WRITE(LOG_PREFIX,"ID(%s) : %s() -> %s", __OBJ_NAME__, __func__, res); \
	return res;

#define RETURN_INT(res) \
	PLOG_TUBE_WRITE(LOG_PREFIX,"ID(%s) : %s() -> %ll", __OBJ_NAME__, __func__, res); \
	return res;

#define LEAVE() \
	PLOG_TUBE_WRITE(LOG_PREFIX,"ID(%s) : %s() exit", __OBJ_NAME__, __func__); \
	return;

#define RETURN_FMT(res, fmt, ...) \
	PLOG_TUBE_WRITE(LOG_PREFIX,"ID(%s) : %s() -> " fmt, __OBJ_NAME__, __func__, __VA_ARGS__); \
	return res;

#define WARNING(w) \
	PLOG_TUBE_WARNING(LOG_PREFIX,"ID(%s) : %s() : %s", __OBJ_NAME__, __func__, w)

#define WARNING_FMT(fmt, ...) \
	PLOG_TUBE_WARNING(LOG_PREFIX,"ID(%s) : %s() : " fmt, __OBJ_NAME__, __func__, __VA_ARGS__)

#define ERROR_MSG(e) \
	PLOG_TUBE_ERROR(LOG_PREFIX,"ID(%s) : %s() : %s", __OBJ_NAME__, __func__, e)

#define ERROR_FMT(fmt, ...) \
	PLOG_TUBE_ERROR(LOG_PREFIX,"ID(%s) : %s() : " fmt, __OBJ_NAME__, __func__, __VA_ARGS__)

#define PRINT(t) \
	PLOG_TUBE_WRITE(LOG_PREFIX,"ID(%s) : %s() : %s", __OBJ_NAME__, __func__, t)

#define PRINT_FMT(fmt, ...) \
	PLOG_TUBE_WRITE(LOG_PREFIX, "ID(%s) : %s() : " fmt, __OBJ_NAME__, __func__, __VA_ARGS__)

#endif#pragma once
