﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_direction.h"

namespace mge
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	class BothwayDirection : public Direction
	{
//		friend class TubeManager;

	public:
		BothwayDirection(MediaContext* owner, uint32_t id, const char* parent_id);
		virtual ~BothwayDirection();

		virtual bool addTermination(Termination* term) override;

	private:
		virtual int checkTermination(Termination* term) override;

		// построить маршрут в одну сторону.
		int buildRx();
		// построить маршрут в другую сторону.
		int buildTx();

		int build(Termination* term_1, Termination* term_2);

		// Сравниваем два списка пайлоадов (list_1 и list_2).
		// Заполняем списки пайлоадов по индексному соответствию (list_1_out и list_2_out)
		void comparePayloads(const mg_media_element_list_t* list_1, mg_media_element_list_t* list_1_out,
			const mg_media_element_list_t* list_2, mg_media_element_list_t* list_2_out) const;

		// Выводим в лог то что получилось после сравнения.
		void printPayloads(const mg_media_element_list_t* list_1_out, const mg_media_element_list_t* list_2_out);

		// Тут создаются тубы и связываются со стримами.
		bool associateStreams(const mg_media_element_list_t* list_in, mg_rx_stream_t* mg_stream_in,
			const mg_media_element_list_t* list_out, mg_tx_stream_t* mg_stream_out);
		bool associateElements(mg_media_element_t* element_in, mg_rx_stream_t* mg_stream_in,
			mg_media_element_t* element_out, mg_tx_stream_t* mg_stream_out);

		void fillElementListRx(h248::TerminationType termination_type, mg_rx_stream_t* rx_stream, mg_tx_stream_t* tx_stream,
			mg_media_element_list_t* list_rx, mg_media_element_list_t* list_tx);
		void fillElementListTx(h248::TerminationType termination_type, mg_rx_stream_t* rx_stream, mg_tx_stream_t* tx_stream,
			mg_media_element_list_t* list_rx, mg_media_element_list_t* list_tx);

		// RTX-97
		void copyElementsToIvrRecord(Termination* term_1, Termination* term_2);
	};
}
//-----------------------------------------------
