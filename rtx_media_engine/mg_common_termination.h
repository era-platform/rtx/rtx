﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "rtp_port_manager.h"

#include "mg_termination.h"
#include "mg_session.h"
#include "net/ip_address.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	class TerminationRTP : public Termination
	{
	public:
		TerminationRTP(rtl::Logger* log, const h248::TerminationID& termId, const char* parentId);
		virtual ~TerminationRTP();

		virtual bool isReady(uint32_t stream_id) override;

		virtual bool mg_stream_start(uint32_t stream_id) override;
		virtual void mg_stream_stop(uint32_t stream_id) override;

		//------------------------------------------------------------------------------
		// методы контекста

		// конструктор и деструктор
		virtual bool mg_termination_initialize(h248::Field* reply) override;
		virtual bool mg_termination_destroy(h248::Field* reply) override;

		// настройки TerminationState
		virtual bool mg_termination_set_TerminationState_Property(const h248::Field* property, h248::Field* reply) override;
		// настройки Local и Remote
		virtual bool mg_termination_set_Local_Remote(uint16_t stream_id, const rtl::String& remote_sdp, const rtl::String& local_sdp_offer, h248::Field* reply) override;
		virtual bool mg_termination_get_Local(uint16_t stream_id, rtl::String& local_sdp_answer, h248::Field* reply) override;

		virtual bool mg_termination_set_events(uint16_t stream_id, const h248::Field* propertySet, h248::Field* reply) override;

		//------------------------------------------------------------------------------
	private:
		bool prepare_audio(rtp_channel_t* channel, MediaSession* audio_stream, const rtl::String& remote_sdp, const rtl::String& local_sdp);
		bool prepare_video(rtp_channel_t* channel, MediaSession* video_stream, const rtl::String& remote_sdp, const rtl::String& local_sdp);
		bool prepare_image(rtp_channel_t* channel, MediaSession* image_stream, const rtl::String& remote_sdp, const rtl::String& local_sdp);

		bool prepare_offer_audio(rtp_channel_t* channel, MediaSession* audio_stream, const rtl::String& local_sdp);
		bool apply_answer_audio(rtp_channel_t* channel, MediaSession* audio_stream, const rtl::String& remote_sdp);
		bool prepare_answer_audio(rtp_channel_t* channel, MediaSession* audio_stream, const rtl::String& local_sdp);
		bool apply_offer_audio(rtp_channel_t* channel, MediaSession* audio_stream, const rtl::String& remote_sdp);

		bool prepare_offer_video(rtp_channel_t* channel, MediaSession* video_stream, const rtl::String& local_sdp);
		bool apply_answer_video(rtp_channel_t* channel, MediaSession* video_stream, const rtl::String& remote_sdp);
		bool prepare_answer_video(rtp_channel_t* channel, MediaSession* video_stream, const rtl::String& local_sdp);
		bool apply_offer_video(rtp_channel_t* channel, MediaSession* video_stream, const rtl::String& remote_sdp);

		bool prepare_offer_image(rtp_channel_t* channel, MediaSession* image_stream, const rtl::String& local_sdp);
		bool apply_answer_image(rtp_channel_t* channel, MediaSession* image_stream, const rtl::String& remote_sdp);
		bool prepare_answer_image(rtp_channel_t* channel, MediaSession* image_stream, const rtl::String& local_sdp);
		bool apply_offer_image(rtp_channel_t* channel, MediaSession* image_stream, const rtl::String& remote_sdp);

		rtp_channel_t* createChannel(uint32_t streamId);
		rtp_channel_t* findChannel(uint32_t streamId);
		rtp_channel_t* getChannel(uint32_t streamId);
		void release_channel(rtp_channel_t* channel);
		void clear_cannels();

		bool start_all();
		void stop_all();

		mg_rx_stream_t* find_rx_stream_by_type(mg_rx_stream_type_t type_rx, MediaSession* mg_stream);
		mg_tx_stream_t* find_tx_stream_by_type(mg_tx_stream_type_t type_tx, MediaSession* mg_stream);

	private:
		rtp_channel_list m_channel_list;
		mg_sdp_session_t m_sdp_local;
		mg_sdp_session_t m_sdp_remote;
	};
}
//------------------------------------------------------------------------------
