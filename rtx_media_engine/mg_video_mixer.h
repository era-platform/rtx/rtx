/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_video_selector.h"
#include "mg_video_mixer_encoder.h"
#include "mg_termination.h"
#include "mmt_timer.h"
#include "mmt_drawer.h"
#include "mmt_video_frame.h"

namespace mge
{
	//-----------------------------------------------
	// �������-������������� termination'� � ����������.
	//-----------------------------------------------
	struct VideoMixerMember
	{
		uint32_t termId;
		uint32_t videoId;
		uint64_t bindId;
		Tube* inputTube;
		Tube* outputTube;
	};
	//-----------------------------------------------
	//
	//-----------------------------------------------
	class VideoMixer :
		public VideoSelectorEvents,
		public rtl::ITimerEventHandler
	{
	private:
		rtl::Logger* m_log;
		rtl::String m_name;
		bool m_started;
		rtl::Mutex m_memberListSync;
		rtl::ArrayT<VideoMixerMember*> m_memberList;	// members list
		rtl::ArrayT<VideoMixerEncoder*> m_codecList;	// outgoing encoders and out tubes
		rtl::Timer m_timer;
		media::VideoFrameGenerator m_frame;
		rtl::MessageQueue m_encoderQueue;
		uint32_t m_lastDrawTS;

	protected:
		void attachToCodec(VideoMixerMember* newMember);
		void detachFromEncoder(mg_video_conference_out_tube_t* newMember);
		VideoMixerMember* findById(uintptr_t termId);
		//void sendOutgoingFrame(media::VideoImage* frame);
		// VideoSelectorEvents
		virtual void videoSelector__priorityChanged(uint32_t termId, bool vadDetected) override;
		// Timer
		virtual void timer_elapsed_event(rtl::Timer* sender, int tid) override;
		// Message queue
		static void messageQueueHandler(void* userData, uint16_t messageId, uintptr_t intParam, void* ptrParam);
		void encodeQueue(media::VideoImage* image);

	public:
		VideoMixer(rtl::Logger* log, const char* instname);
		virtual ~VideoMixer();

		void start(const char* schema);
		void restart(const char* schema);
		void stop();

		bool addMember(Termination* term, Tube* in_tube, Tube* out_tube);
		void removeMember(uint32_t termId);
		void attachMember(uint32_t termId);
		void updateVideoStream(uint64_t videoStreamId, const media::VideoImage* image);

		const char* getName() const { return m_name; }
	};
}
//-----------------------------------------------
