﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_fax_termination.h"
#include "udptl_proxy_channel.h"
#include "rtp_simple_channel.h"
#include "mg_rx_common_stream.h"
#include "mg_rx_fax_t30_stream.h"
#include "mg_rx_fax_t38_stream.h"
#include "mg_rx_fax_proxy_stream.h"
#include "mg_tx_common_stream.h"
#include "mg_tx_fax_proxy_stream.h"
#include "mg_tx_fax_t30_stream.h"
#include "mg_tx_fax_t30_stream.h"
#include "mg_fax_t30.h"
#include "mg_fax_t38.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define LOG_PREFIX m_tag // "MG-TFAX" // Media Gateway Fax Termination

#include "logdef.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	FaxTermination::FaxTermination(rtl::Logger* log, const h248::TerminationID& termId, const char* parent_id) :
		Termination(log, termId, parent_id), m_sdp_local(log), m_sdp_remote(log)
	{
		m_service_state = h248::TerminationServiceState::Test;
		m_event_buffer_control = false;
		m_fax = nullptr;
		m_receiver = true;
		strcpy(m_tag, "TERM-FAX");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	FaxTermination::~FaxTermination()
	{
		mg_termination_destroy(nullptr);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool FaxTermination::mg_termination_initialize(h248::Field* _reply)
	{
		ENTER();

		m_sdp_local.set_termination_id(getName());
		m_sdp_remote.set_termination_id(getName());

		if (getType() == h248::TerminationType::Fax_T30)
		{
			m_fax = NEW mg_fax_t30_t(g_mg_fax_log);
		}
		else if (getType() == h248::TerminationType::Fax_T38)
		{
			m_fax = NEW mg_fax_t38_t(g_mg_fax_log);
		}

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool FaxTermination::mg_termination_destroy(h248::Field* reply)
	{
		ENTER();

		Termination::mg_termination_lock();

		if (m_fax != nullptr)
		{
			m_fax->metronomeStop();
			m_fax->destroy();
		}

		Termination::destroyMediaSession();

		m_sdp_local.clear();
		m_sdp_remote.clear();

		if (m_fax != nullptr)
		{
			m_fax->destroy();
			DELETEO(m_fax);
			m_fax = nullptr;
		}

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool FaxTermination::mg_termination_set_TerminationState_Property(const h248::Field* property, h248::Field* reply)
	{
		const rtl::String& propName = property->getName();
		const rtl::String& propValue = property->getValue();

		ENTER_FMT("name:%s, value:%s", (const char*)propName, (const char*)propValue);
		
		rtl::String value = unescape_rfc3986(propValue);
		bool result = true;

		if (rtl::String::compare("mode", propName, false) == 0)
		{
			m_receiver = rtl::String::compare("r", value, false) == 0;
		}
		else if (rtl::String::compare("path", propName, false) == 0)
		{
			result = setPath(value, reply);
		}
		else
		{
			WARNING("unsupported or unknown property!");
			// @error code
			reply->addErrorDescriptor(h248::ErrorCode::c445_Unsupported_or_unknown_property);
			return false;
		}

		RETURN_BOOL(result);
	}
	//------------------------------------------------------------------------------
	bool FaxTermination::setPath(const rtl::String& value, h248::Field* reply)
	{
		rtl::String path = utf8_to_locale(value);
		path.replace('\\', FS_PATH_DELIMITER);
		path.replace('/', FS_PATH_DELIMITER);
		rtl::String full_path = get_local_path(path);

		ENTER_FMT("path: %s", (const char*)full_path);

		bool result = false;

		if (getType() == h248::TerminationType::Fax_T30)
		{
			result = setupFaxT30(full_path, reply);
			/*
			if (m_receiver)
			{
				if (!fs_check_path_and_create(full_path))
				{
					ERROR_MSG("directory create fail!");
					// @error code
					reply->addErrorDescriptor(h248::ErrorCode::c454_No_such_parameter_value_in_this_package);
					return false;
				}

				if (!mg_initialize_fax_receiver_t30(m_fax, full_path))
				{
					ERROR_MSG("initialize fax receiver T.30 fail!");
					// @error code
					reply->addErrorDescriptor(h248::ErrorCode::c500_Internal_software_failure_in_the_MG_or_MGC, "fax receiver initialization failed");
					return false;
				}
			}
			else
			{
				if (!fs_is_file_exist(full_path))
				{
					ERROR_MSG("file not exist!");
					// @error code
					reply->addErrorDescriptor(h248::ErrorCode::c454_No_such_parameter_value_in_this_package);
					return false;
				}

				if (!mg_initialize_fax_transmitter_t30(m_fax, full_path, true))
				{
					ERROR_MSG("initialize fax transmitter T.30 fail!");
					// @error code
					reply->addErrorDescriptor(h248::ErrorCode::c500_Internal_software_failure_in_the_MG_or_MGC, "fax transmitter initialization failed");
					return false;
				}
			}

			MediaSession* mg_stream = makeMediaSession(MG_STREAM_TYPE_AUDIO);

			mg_tx_stream_t* tx_stream = find_tx_stream_by_type(mg_tx_stream_type_t::mg_tx_fax_t30_e, mg_stream);
			if (tx_stream == nullptr)
			{
				ERROR_MSG("transmitter stream not found");
				// @error code
				reply->addErrorDescriptor(h248::ErrorCode::c502_Not_ready, "transmitter stream not found");
				return false;
			}
			mg_tx_fax_t30_stream_t* tx_fax_stream = (mg_tx_fax_t30_stream_t*)tx_stream;
			tx_fax_stream->set_fax(m_fax);

			mg_rx_stream_t* rx_stream = find_rx_stream_by_type(mg_rx_stream_type_t::mg_rx_fax_t30_e, mg_stream);
			if (rx_stream == nullptr)
			{
				ERROR_MSG("receiver stream not found");
				// @error code
				reply->addErrorDescriptor(h248::ErrorCode::c502_Not_ready, "reveiver stream not found");
				return false;
			}
			mg_rx_fax_t30_stream_t* t30_fax_stream = (mg_rx_fax_t30_stream_t*)rx_stream;
			m_fax->set_fax_handler(t30_fax_stream);
			*/
		}
		else if (getType() == h248::TerminationType::Fax_T38)
		{
			result = setupFaxT38(full_path, reply);
			/*
			if (m_receiver)
			{
				if (!fs_check_path_and_create(full_path))
				{
					ERROR_MSG("directory create fail!");
					// @error code
					reply->addErrorDescriptor(h248::ErrorCode::c454_No_such_parameter_value_in_this_package);
					return false;
				}

				if (!mg_initialize_fax_receiver_t38(m_fax, full_path))
				{
					ERROR_MSG("initialize fax receiver T.38 fail!");
					// @error code
					reply->addErrorDescriptor(h248::ErrorCode::c500_Internal_software_failure_in_the_MG_or_MGC, "fax receiver initialization failed");
					return false;
				}
			}
			else
			{
				if (!fs_is_file_exist(full_path))
				{
					ERROR_MSG("file not exist!");
					// @error code
					reply->addErrorDescriptor(h248::ErrorCode::c454_No_such_parameter_value_in_this_package);
					return false;
				}

				if (!mg_initialize_fax_transmitter_t38(m_fax, full_path, true))
				{
					ERROR_MSG("initialize fax transmitter T.38 fail!");
					// @error code
					reply->addErrorDescriptor(h248::ErrorCode::c500_Internal_software_failure_in_the_MG_or_MGC, "fax transmitter initialization failed");
					return false;
				}
			}

			MediaSession* mg_stream = makeMediaSession(MG_STREAM_TYPE_IMAGE);

			mg_tx_stream_t* tx_stream = find_tx_stream_by_type(mg_tx_stream_type_t::mg_tx_fax_t38_e, mg_stream);
			if (tx_stream == nullptr)
			{
				ERROR_MSG("transmitter stream not found");
				// @error code
				reply->addErrorDescriptor(h248::ErrorCode::c502_Not_ready, "transmitter stream not found");
				return false;
			}
			mg_tx_fax_t30_stream_t* tx_fax_stream = (mg_tx_fax_t30_stream_t*)tx_stream;
			mg_fax_t38_t* t38 = (mg_fax_t38_t*)m_fax;
			mg_media_element_list_t* ellist = tx_fax_stream->getMediaParams().get_media_elements();
			if (ellist != nullptr)
			{
				mg_media_element_t* el = ellist->get_priority();
				if (el != nullptr)
				{
					t38->set_parameters(el->get_fax_param());
				}
			}
			tx_fax_stream->set_fax(m_fax);

			mg_rx_stream_t* rx_stream = find_rx_stream_by_type(mg_rx_stream_type_t::mg_rx_fax_t38_e, mg_stream);
			if (rx_stream == nullptr)
			{
				ERROR_MSG("receiver stream not found");
				// @error code
				reply->addErrorDescriptor(h248::ErrorCode::c502_Not_ready, "receiver stream not found");
				return false;
			}
			mg_rx_fax_t38_stream_t* t38_fax_stream = (mg_rx_fax_t38_stream_t*)rx_stream;
			m_fax->set_fax_handler(t38_fax_stream);
			*/
		}

		RETURN_BOOL(result);
	}
	bool FaxTermination::setupFaxT30(const rtl::String& full_path, h248::Field* reply)
	{
		if (m_receiver)
		{
			if (!fs_check_path_and_create(full_path))
			{
				ERROR_MSG("directory create fail!");
				// @error code
				reply->addErrorDescriptor(h248::ErrorCode::c454_No_such_parameter_value_in_this_package);
				return false;
			}

			if (!mg_initialize_fax_receiver_t30(m_fax, full_path))
			{
				ERROR_MSG("initialize fax receiver T.30 fail!");
				// @error code
				reply->addErrorDescriptor(h248::ErrorCode::c500_Internal_software_failure_in_the_MG_or_MGC, "fax receiver initialization failed");
				return false;
			}
		}
		else
		{
			if (!fs_is_file_exist(full_path))
			{
				ERROR_MSG("file not exist!");
				// @error code
				reply->addErrorDescriptor(h248::ErrorCode::c454_No_such_parameter_value_in_this_package);
				return false;
			}

			if (!mg_initialize_fax_transmitter_t30(m_fax, full_path, true))
			{
				ERROR_MSG("initialize fax transmitter T.30 fail!");
				// @error code
				reply->addErrorDescriptor(h248::ErrorCode::c500_Internal_software_failure_in_the_MG_or_MGC, "fax transmitter initialization failed");
				return false;
			}
		}

		MediaSession* mg_stream = makeMediaSession(MG_STREAM_TYPE_AUDIO);

		mg_tx_fax_t30_stream_t* tx_fax_stream = (mg_tx_fax_t30_stream_t*)find_tx_stream_by_type(mg_tx_stream_type_t::mg_tx_fax_t30_e, mg_stream);
		if (tx_fax_stream == nullptr)
		{
			ERROR_MSG("transmitter stream not found");
			// @error code
			reply->addErrorDescriptor(h248::ErrorCode::c502_Not_ready, "transmitter stream not found");
			return false;
		}
		tx_fax_stream->set_fax(m_fax);

		mg_rx_fax_t30_stream_t* rx_fax_stream = (mg_rx_fax_t30_stream_t*)find_rx_stream_by_type(mg_rx_stream_type_t::mg_rx_fax_t30_e, mg_stream);
		if (rx_fax_stream == nullptr)
		{
			ERROR_MSG("receiver stream not found");
			// @error code
			reply->addErrorDescriptor(h248::ErrorCode::c502_Not_ready, "reveiver stream not found");
			return false;
		}
		
		m_fax->set_fax_handler(rx_fax_stream);

		return true;
	}
	bool FaxTermination::setupFaxT38(const rtl::String& full_path, h248::Field* reply)
	{
		if (m_receiver)
		{
			if (!fs_check_path_and_create(full_path))
			{
				ERROR_MSG("directory create fail!");
				// @error code
				reply->addErrorDescriptor(h248::ErrorCode::c454_No_such_parameter_value_in_this_package);
				return false;
			}

			if (!mg_initialize_fax_receiver_t38(m_fax, full_path))
			{
				ERROR_MSG("initialize fax receiver T.38 fail!");
				// @error code
				reply->addErrorDescriptor(h248::ErrorCode::c500_Internal_software_failure_in_the_MG_or_MGC, "fax receiver initialization failed");
				return false;
			}
		}
		else
		{
			if (!fs_is_file_exist(full_path))
			{
				ERROR_MSG("file not exist!");
				// @error code
				reply->addErrorDescriptor(h248::ErrorCode::c454_No_such_parameter_value_in_this_package);
				return false;
			}

			if (!mg_initialize_fax_transmitter_t38(m_fax, full_path, true))
			{
				ERROR_MSG("initialize fax transmitter T.38 fail!");
				// @error code
				reply->addErrorDescriptor(h248::ErrorCode::c500_Internal_software_failure_in_the_MG_or_MGC, "fax transmitter initialization failed");
				return false;
			}
		}

		MediaSession* mg_stream = makeMediaSession(MG_STREAM_TYPE_IMAGE);

		mg_tx_fax_t30_stream_t* tx_fax_stream = (mg_tx_fax_t30_stream_t*)find_tx_stream_by_type(mg_tx_stream_type_t::mg_tx_fax_t38_e, mg_stream);
		if (tx_fax_stream == nullptr)
		{
			ERROR_MSG("transmitter stream not found");
			// @error code
			reply->addErrorDescriptor(h248::ErrorCode::c502_Not_ready, "transmitter stream not found");
			return false;
		}
		
		mg_fax_t38_t* t38 = (mg_fax_t38_t*)m_fax;
		mg_media_element_list_t* ellist = tx_fax_stream->getMediaParams().get_media_elements();
		if (ellist != nullptr)
		{
			mg_media_element_t* el = ellist->get_priority();
			if (el != nullptr)
			{
				t38->set_parameters(el->get_fax_param());
			}
		}
		tx_fax_stream->set_fax(m_fax);

		mg_rx_fax_t38_stream_t* rx_fax_stream = (mg_rx_fax_t38_stream_t*)find_rx_stream_by_type(mg_rx_stream_type_t::mg_rx_fax_t38_e, mg_stream);
		if (rx_fax_stream == nullptr)
		{
			ERROR_MSG("receiver stream not found");
			// @error code
			reply->addErrorDescriptor(h248::ErrorCode::c502_Not_ready, "receiver stream not found");
			return false;
		}
		
		m_fax->set_fax_handler(rx_fax_stream);

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool FaxTermination::mg_termination_set_Local_Remote(uint16_t stream_id, const rtl::String& remote_sdp, const rtl::String& local_sdp_offer, h248::Field* reply)
	{
		return true;
	}
	//------------------------------------------------------------------------------
	bool FaxTermination::mg_termination_get_Local(uint16_t stream_id, rtl::String& local_sdp_answer, h248::Field* reply)
	{
		ENTER();

		rtl::String ss;
		m_sdp_local.write_base(ss);

		ss << "\r\n";

		if (stream_id == MG_STREAM_TYPE_AUDIO)
		{
			mg_sdp_media_base_t* audio = m_sdp_local.get_media(stream_id);
			if (audio == nullptr)
			{
				WARNING("audio not found.");
			}
			else
			{
				audio->write(ss);
			}
		}
		else if (stream_id == MG_STREAM_TYPE_VIDEO)
		{
			mg_sdp_media_base_t* video = m_sdp_local.get_media(stream_id);
			if (video == nullptr)
			{
				WARNING("video not found.");
			}
			else
			{
				video->write(ss);
			}
		}
		else if (stream_id == MG_STREAM_TYPE_IMAGE)
		{
			mg_sdp_media_base_t* image = m_sdp_local.get_media(stream_id);
			if (image == nullptr)
			{
				WARNING("image not found");
			}
			else
			{
				image->write(ss);
			}
		}

		local_sdp_answer = ss;

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool FaxTermination::mg_stream_start(uint32_t stream_id)
	{
		return Termination::mg_stream_start(stream_id);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void FaxTermination::mg_stream_stop(uint32_t stream_id)
	{
		Termination::mg_stream_stop(stream_id);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_rx_stream_t* FaxTermination::find_rx_stream_by_type(mg_rx_stream_type_t type_rx, MediaSession* mg_stream)
	{
		const rtl::ArrayT<mg_rx_stream_t*>& rx_streams = mg_stream->get_rx_streams();

		if (rx_streams.getCount() == 0)
		{
			if (!mg_stream->create_rx_stream(type_rx))
			{
				ERROR_MSG("create rx stream fail.");
				return nullptr;
			}
		}

		mg_rx_stream_t* rx_stream = rx_streams[0];

		if (mg_stream->getId() == MG_STREAM_TYPE_AUDIO)
		{
			mg_media_element_t* el = mg_create_media_element_t30(getName(), rx_stream->getMediaParams().get_media_elements());
			mg_rx_fax_t30_stream_t* t30_fax_stream = (mg_rx_fax_t30_stream_t*)rx_stream;
			if ((t30_fax_stream != nullptr) && (el != nullptr))
			{
				if (!t30_fax_stream->init(el->get_payload_id(), 160))
				{
					ERROR_MSG("init rx stream fail");
					return nullptr;
				}
			}
		}
		else
		{
			mg_create_media_element_t38(getName(), rx_stream->getMediaParams().get_media_elements(), false);
		}

		return rx_stream;
	}
	//------------------------------------------------------------------------------
	mg_tx_stream_t*	FaxTermination::find_tx_stream_by_type(mg_tx_stream_type_t type_tx, MediaSession* mg_stream)
	{
		bool add_element = false;
		const rtl::ArrayT<mg_tx_stream_t*>& tx_streams = mg_stream->get_tx_streams();
		
		if (tx_streams.getCount() == 0)
		{
			if (!mg_stream->create_tx_stream(type_tx))
			{
				ERROR_MSG("create tx stream fail.");
				return nullptr;
			}
		}

		mg_tx_stream_t* tx_stream = tx_streams[0];

		if (mg_stream->getId() == MG_STREAM_TYPE_AUDIO)
		{
			mg_create_media_element_t30(getName(), tx_stream->getMediaParams().get_media_elements());
		}
		else
		{
			mg_create_media_element_t38(getName(), tx_stream->getMediaParams().get_media_elements(), false);
		}

		return tx_stream;
	}
}
//------------------------------------------------------------------------------
