﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_tube.h"

namespace mge
{
	//------------------------------------------------------------------------------
	// Труба пересчета количества данных по мс. 
	// На входе труба. На выходе всегда труба pcm_to_codec.
	// На входе pcm. На выходе несколько пакетов (если пересчет в меньшую сторону, например из 30мс в 10мс).
	// Или наоборот дальше не проходит пока не наберется полный пакет (если из 10мс в 30мс).
	//------------------------------------------------------------------------------
	class ME_DEBUG_API mg_pcm_ptime_tube_t : public Tube
	{
		mg_pcm_ptime_tube_t(const mg_pcm_ptime_tube_t&);
		mg_pcm_ptime_tube_t& operator=(const mg_pcm_ptime_tube_t&);

	public:
		mg_pcm_ptime_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id);
		virtual	~mg_pcm_ptime_tube_t();

		virtual bool init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out) override;
		virtual bool is_suited(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out) override;
		virtual void send(const INextDataHandler* out_handler, const rtp_packet* packet) override;

	private:
		void accumulation_data(const uint8_t* payload, uint32_t payload_length);
		uint32_t send_parts_packet(const rtp_packet* packet);
		uint8_t send_from_buffer(const rtp_packet* packet);
		void send_packet(rtp_packet* packet);
		void copy_packet(rtp_packet* new_packet, const rtp_packet* packet, rtp_packet_priv_type type, uint8_t count);
		rtp_packet_priv_type analyze_type(uint32_t payload_length);

	private:
		uint16_t m_frame_size;
		uint8_t* m_pcm_buffer;
		uint32_t m_pcm_buffer_write;
		uint16_t m_seqno;
	};
}
//------------------------------------------------------------------------------
