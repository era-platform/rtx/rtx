﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_dummy_termination.h"
#include "mg_rx_stream.h"
#include "mg_tx_stream.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define LOG_PREFIX m_tag //"MG-TDUM" // Media Gateway Dummy Termination

#include "logdef.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	DummyTermination::DummyTermination(rtl::Logger* log, const h248::TerminationID& termId, const char* parentId) :
		Termination(log, termId, parentId), m_log(log)
	{
		m_service_state = h248::TerminationServiceState::Test;
		m_event_buffer_control = false;
		strcpy(m_tag, "TERM-DUM");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	DummyTermination::~DummyTermination()
	{
		mg_termination_destroy(nullptr);
	}
	//------------------------------------------------------------------------------
	// Last Error
	//------------------------------------------------------------------------------
	bool DummyTermination::mg_termination_initialize(h248::Field* reply)
	{
		ENTER();

		if (!createSession(MG_STREAM_TYPE_AUDIO, reply))
			return false;

		if (!createSession(MG_STREAM_TYPE_VIDEO, reply))
			return false;

		if (!createSession(MG_STREAM_TYPE_IMAGE, reply))
			return false;

		return true;
	}
	//------------------------------------------------------------------------------
	bool DummyTermination::createSession(int sessionType, h248::Field* reply)
	{
		MediaSession* mg_stream = makeMediaSession(sessionType);

		if (mg_stream == nullptr)
		{
			ERROR_MSG("create session fail!");
			reply->addErrorDescriptor(h248::ErrorCode::c510_Insufficient_resources, "Not enough memory to create a media session");
			return false;
		}

		if (!mg_stream->create_rx_stream(mg_rx_stream_type_t::mg_rx_dummy_e))
		{
			ERROR_MSG("create dummy rx stream fail!");
			reply->addErrorDescriptor(h248::ErrorCode::c510_Insufficient_resources, "Not enough memory to create a receiver stream");
			return false;
		}

		if (!mg_stream->create_tx_stream(mg_tx_stream_type_t::mg_tx_dummy_e))
		{
			ERROR_MSG("create dummy tx stream fail!");
			reply->addErrorDescriptor(h248::ErrorCode::c510_Insufficient_resources, "Not enough memory to create a transmitter stream");
			return false;
		}

		if (!mg_stream->mg_stream_set_LocalControl_Mode(h248::TerminationStreamMode::SendRecv))
		{
			ERROR_MSG("set local control mode fail!");
			reply->addErrorDescriptor(h248::ErrorCode::c500_Internal_software_failure_in_the_MG_or_MGC, "Media session in invalid state");
			return false;
		}

		mg_rx_stream_t* dummy_rx_stream = mg_stream->get_rx_stream_at(0);

		mg_create_media_element_dummy(getName(), dummy_rx_stream->getMediaParams().get_media_elements());

		mg_tx_stream_t* dummy_tx_stream = mg_stream->get_tx_stream_at(0);

		mg_create_media_element_dummy(getName(), dummy_tx_stream->getMediaParams().get_media_elements());

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool DummyTermination::mg_termination_destroy(h248::Field* reply)
	{
		Termination::mg_termination_lock();

		Termination::destroyMediaSession();

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool DummyTermination::mg_termination_set_Local_Remote(uint16_t stream_id, const rtl::String& remote_sdp, const rtl::String& local_sdp_offer, h248::Field* reply)
	{
		return true;
	}
	//------------------------------------------------------------------------------
	bool DummyTermination::mg_termination_get_Local(uint16_t stream_id, rtl::String& local_sdp_answer, h248::Field* reply)
	{
		return true;
	}
}
//------------------------------------------------------------------------------
