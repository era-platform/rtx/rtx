﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_tube.h"
#include "mmt_timer.h"

namespace mge
{
	//--------------------------------------
	// error correction mode
	//--------------------------------------
	enum class t38_ecm_mode_t
	{
		None = 0,
		Redundancy = 1,
		FEC = 2,
	};
	//--------------------------------------
	//
	//--------------------------------------
	struct udptl_tx_frame_t
	{
		int				seq_no;
		int				ifp_length;
		uint8_t			ifp[rtp_packet::PAYLOAD_BUFFER_SIZE];
	};
	//--------------------------------------
	//
	//--------------------------------------
	struct udptl_tx_transmission_packet_t
	{
		uint32_t		transmissions;
		uint32_t		length;
		static const uint32_t block_size = 320;
		uint8_t			buff[block_size];
	};

	//--------------------------------------
	//
	//--------------------------------------
	class mg_udptl_tx_tube_t : public Tube, private media::Metronome
	{
		// tx for red & fec
		volatile int m_tx_seq_no;				// номер отправляемого пакета
		udptl_tx_frame_t* m_tx_buffer;			// [UDPTL_ECM_BUFFER_SIZE] буфер отравленных пакетов
		// tx parameters
		t38_ecm_mode_t m_tx_ecm_mode;			// режим коррекции ошибок
		int m_tx_ecm_entries;					// fec : количество проверочных ключей
		int m_tx_ecm_span;						// fec/red : количество ifp_пакетов на ключ/udptl_пакет
		// common
		udptl_tx_transmission_packet_t	m_packets[5];
		volatile uint32_t			m_packet_write;
		std::atomic_flag			m_lock;

	public:
		mg_udptl_tx_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id);
		virtual ~mg_udptl_tx_tube_t();

		void					set_tx_ecm_mode(t38_ecm_mode_t mode, int entries = -1, int span = -1);
		t38_ecm_mode_t			get_tx_ecm_mode() { return m_tx_ecm_mode; }
		void					set_tx_ecm_span(int span);
		int						get_tx_ecm_span() { return m_tx_ecm_span; }
		void					set_tx_ecm_entries(int entries);
		int						get_tx_ecm_entries() { return m_tx_ecm_entries; }

		virtual bool init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out) override;
		virtual void send(const INextDataHandler* out_handler, const rtp_packet* packet) override;
		virtual void send(const INextDataHandler* out_handler, const uint8_t* data, uint32_t len) override;

	private:
		int build_tx_packet(uint8_t* buffer, int size, const uint8_t* ifp, int ifp_len);
		int build_tx_packet_red(uint8_t* buffer, int size, const uint8_t* ifp, int ifp_len);
		int build_tx_packet_fec(uint8_t* buffer, int size, const uint8_t* ifp, int ifp_len);

		void build_udptl_packet(const uint8_t* data, uint32_t length);

		bool lock();
		void unlock();

		//--------------------------------------
		// << Timer >>
		virtual void metronomeTicks();
		//--------------------------------------
	};
}
//--------------------------------------
