﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtp_channel.h"
#include "mg_tx_stream.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	class mg_tx_common_stream_t : public mg_tx_stream_t
	{
	public:
		mg_tx_common_stream_t(rtl::Logger* log, uint32_t id, mg_tx_stream_type_t type, uint32_t parent_id, const char* term_id);
		virtual ~mg_tx_common_stream_t() override;

		virtual bool mg_stream_set_LocalControl_Property(const h248::Field* LC_property);

		virtual void send(const INextDataHandler* out_handler, const rtp_packet* packet) override;
		virtual void send(const INextDataHandler* out_handler, const uint8_t* data, uint32_t len) override;
		virtual void send(const INextDataHandler* out_handler, const rtcp_packet_t* packet) override;

		void set_channel_event(i_tx_channel_event_handler* rtp);
		void set_remote_address(const socket_address* address) { m_remote_address.copy_from(address); }
		const socket_address* get_remote_address() { return &m_remote_address; }
		void set_remote_rtcp_address(const socket_address* address) { m_remote_rtcp_address.copy_from(address); }
		uint32_t get_ssrc_to_rtp() { return m_ssrc_to_rtp; }

		// сбрасываем счетчики пересчета таймштампов при новом конекте.
		virtual void reset_before_new_connect();
		virtual void change_flag_recalc_packet(bool recalc_pckt);

	private:
		void send_packet_to_rtp_lock(const INextDataHandler* out_handler, const rtp_packet* packet);
		void send_data_to_rtp_lock(const INextDataHandler* out_handler, const uint8_t* data, uint32_t len);
		void send_ctrl_to_rtp_lock(const INextDataHandler* out_handler, const rtcp_packet_t* packet);

		bool recalc_packet(rtp_packet* packet);
		int recalc_seq_num(rtp_packet* packet);
		int calc_diff(rtp_packet* packet);
		bool recalc_timestamp(uint32_t count_packet, rtp_packet* packet);

		virtual int rtcp_session_tx(const rtcp_packet_t* packet);

	private:
		// Флаг указывает что это первый запуск стрима.
		bool m_first_send;
		// Флаг указывает что это новое соединение.
		bool m_new_connect;
		//-----------------------------------------------------
		// переменные для работы с пакетами на отправку в сеть.
		//-----------------------------------------------------
		i_tx_channel_event_handler*	m_rtp;
		socket_address m_remote_address;			// адрес получателя пакета.
		socket_address m_remote_rtcp_address;		// адрес получателя rtcp пакета.

		uint32_t m_ssrc_to_rtp;
		uint16_t m_seq_num_to_rtp;			// порядковый номер пакета для отправки в ртп канал.
		uint16_t m_seq_num_to_rtp_last;		// номер последнего отправленного пакета в ртп канал.
		uint32_t m_timestamp_to_rtp;		// таймштамп пакета для отправки в ртп канал.
		uint32_t m_timestamp_to_rtp_last;	// таймштамп последнего отправленного пакета в ртп канал.
		uint32_t m_in_timestamp_last;
		uint32_t m_samples_to_rtp_last;		// последнее количество семплов которое было отправлено.

		// включение/выключение перерасчета таймштампов при отправке в ртп канал.
		bool m_recalc_packet_to_rtp;
		bool m_last_packet_cn;				// флаг указывает что последний отправленый пакет был комфортным шумом.
		uint64_t m_last_tick;
	};
}
//------------------------------------------------------------------------------
