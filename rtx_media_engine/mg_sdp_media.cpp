﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_sdp_media.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define LOG_PREFIX "SDPM"
#define PLOG_SDP PLOG_FLAG3

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_sdp_media_base_t::mg_sdp_media_base_t(rtl::Logger* log, const char* term_id, uint32_t stream_id) :
		m_log(log), m_term_id(term_id), m_stream_id(stream_id)
	{
		m_port = 0;
		m_ptime = 0;
		m_maxptime = 0;

		m_fingerprint = nullptr;
		m_ice = nullptr;

		m_m_line = rtl::String::empty;

		clear_differences_list();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_sdp_media_base_t::~mg_sdp_media_base_t()
	{
		clear();
	}
	//------------------------------------------------------------------------------
	// RTX-66
	//------------------------------------------------------------------------------
	int	mg_sdp_media_base_t::stream_type(int id)
	{
		if (id == MG_STREAM_TYPE_AUDIO)
		{
			return MG_STREAM_TYPE_AUDIO;
		}
		else if (id == MG_STREAM_TYPE_VIDEO)
		{
			return MG_STREAM_TYPE_VIDEO;
		}
		else if (id == MG_STREAM_TYPE_IMAGE)
		{
			return MG_STREAM_TYPE_IMAGE;
		}

		for (int i = 10; i <= 100; i += 10)
		{
			if ((i + MG_STREAM_TYPE_AUDIO) == id)
			{
				return MG_STREAM_TYPE_AUDIO;
			}
			else if ((i + MG_STREAM_TYPE_VIDEO) == id)
			{
				return MG_STREAM_TYPE_VIDEO;
			}
			else if ((i + MG_STREAM_TYPE_IMAGE) == id)
			{
				return MG_STREAM_TYPE_IMAGE;
			}
		}

		return -1;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	uint32_t mg_sdp_media_base_t::get_stream_id()
	{
		return m_stream_id;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::read(const rtl::String& media)
	{
		bool compare = !m_m_line.isEmpty();

		if (media.isEmpty())
		{
			return false;
		}

		if (compare)
		{
			clear_differences_list();
		}

		rtl::String media_str(media);
		if (media_str.isEmpty())
		{
			return false;
		}

		media_str.trim(" \t");

		bool one_fail = false;
		int index_b = 0;
		int index = media_str.indexOf('\n');
		while (index != BAD_INDEX)
		{
			rtl::String item = media_str.substring(index_b, index - index_b);

			if (!add_media_line(item, compare))
			{
				one_fail = true;
			}

			index_b = index + 1;
			index = media_str.indexOf('\n', index_b);
		}

		filer_media_elements();

		return !one_fail;
	}
	//------------------------------------------------------------------------------
	void mg_sdp_media_base_t::write(rtl::String& media)
	{
		write_m_line(media);

		write_c_line(media);

		write_a_lines(media);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_sdp_media_base_t::clear()
	{
		m_m_line = rtl::String:: empty;

		m_atributes.clear();

		m_type = rtl::String::empty;
		m_port = 0;
		m_transport = rtl::String::empty;
		m_media_elements.clear();

		m_mode = rtl::String::empty;

		clear_webrtc_param();

		clear_differences_list();
	}
	//------------------------------------------------------------------------------
	void mg_sdp_media_base_t::clear_webrtc_param()
	{
		clear_ssrc_params();
		clear_fingerprint_param();
		clear_ice_param();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::add_media_line(const rtl::String& media_line, bool compare)
	{
		if (media_line.isEmpty())
		{
			PLOG_SDP(LOG_PREFIX, "(%s) add_media_line -- 'm' line is empty!", (const char*)m_term_id);
			return false;
		}

		PLOG_SDP(LOG_PREFIX, "(%s) add_media_line -- '%s'", (const char*)m_term_id, (const char*)media_line);

		rtl::String string_line(media_line);

		bool compare_diff = (m_m_line.isEmpty()) ? false : compare;

		string_line.trim(" \t");

		char first_symbol = string_line.front();

		if (first_symbol == 'm')
		{
			if (!read_m_line(string_line, compare_diff))
			{
				PLOG_SDP(LOG_PREFIX, "(%s) add_media_line -- FAIL.", (const char*)m_term_id);
				return false;
			}
		}
		else if (first_symbol == 'a')
		{
			if (!read_a_line(string_line, compare_diff))
			{
				PLOG_SDP(LOG_PREFIX, "(%s) add_media_line -- FAIL.", (const char*)m_term_id);
				return false;
			}
		}
		else if (first_symbol == 'c')
		{
			if (!read_c_line(string_line, compare_diff))
			{
				return false;
			}
		}

		return true;
	}
	//------------------------------------------------------------------------------
	const rtl::String& mg_sdp_media_base_t::get_media_line() const
	{
		return m_m_line;
	}
	//------------------------------------------------------------------------------
	// audio or video
	//------------------------------------------------------------------------------
	const rtl::String& mg_sdp_media_base_t::getType() const
	{
		return m_type;
	}
	//------------------------------------------------------------------------------
	void  mg_sdp_media_base_t::set_type(const char* type)
	{
		if (type == nullptr)
		{
			return;
		}

		m_type = type;
		update_m_line();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	rtl::StringList& mg_sdp_media_base_t::get_crypto_params()
	{
		return m_crypto_params;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	const uint16_t mg_sdp_media_base_t::get_port() const
	{
		return m_port;
	}
	//------------------------------------------------------------------------------
	void mg_sdp_media_base_t::set_port(uint16_t port)
	{
		m_port = port;
		update_m_line();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	const uint32_t mg_sdp_media_base_t::get_ptime() const
	{
		return m_ptime;
	}
	//------------------------------------------------------------------------------
	void mg_sdp_media_base_t::set_ptime(uint32_t ptime)
	{
		m_ptime = ptime;
	}
	//------------------------------------------------------------------------------
	const uint32_t mg_sdp_media_base_t::get_maxptime() const
	{
		return m_maxptime;
	}
	//------------------------------------------------------------------------------
	void mg_sdp_media_base_t::set_maxptime(uint32_t maxptime)
	{
		m_maxptime = maxptime;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	const rtl::String& mg_sdp_media_base_t::get_transport() const
	{
		return m_transport;
	}
	//------------------------------------------------------------------------------
	void mg_sdp_media_base_t::set_transport(const char* transport)
	{
		if (transport == nullptr)
		{
			return;
		}

		m_transport = transport;
		update_m_line();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	const rtl::String&	mg_sdp_media_base_t::get_mode() const
	{
		return m_mode;
	}
	//------------------------------------------------------------------------------
	void mg_sdp_media_base_t::set_mode(const char* mode)
	{
		m_mode = mode;
		m_mode.trim();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_media_element_list_t* mg_sdp_media_base_t::get_media_elements()
	{
		return &m_media_elements;
	}
	//------------------------------------------------------------------------------
	// m=audio 0 RTP/AVP 0 1 3
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::read_m_line(const rtl::String& media_line, bool compare)
	{
		if (media_line.isEmpty())
		{
			return false;
		}

		if (compare)
		{
			if (rtl::String::compare(media_line, m_m_line, false) != 0)
			{
				mg_sdp_difference_t* diff = NEW mg_sdp_difference_t();
				diff->type = mg_sdp_difference_type::media;
				diff->line.append(media_line);
				diff->stream_id = m_stream_id;
				m_differences_list.add(diff);
			}

			return true;
		}

		rtl::String text;
		m_m_line = media_line;
		
		int index_b = 0;
		int index = m_m_line.indexOf('=');

		if (index == BAD_INDEX)
			return false;

		index_b = index + 1;
		index = m_m_line.indexOf(' ');

		if (index == BAD_INDEX)
			return false;

		m_type = m_m_line.substring(index_b, index - index_b);

		text << "m=" << m_type << ' ';

		index_b = index + 1;
		index = m_m_line.indexOf(' ', index_b);

		if (index == BAD_INDEX)
			return false;

		rtl::String item = m_m_line.substring(index_b, index - index_b);

		m_port = item.front() == '$' ? UINT16_MAX : (uint16_t)strtoul(item, nullptr, 10);

		if (m_port == UINT16_MAX)
			text << "$ ";
		else
			text << m_port << ' ';

		index_b = index + 1;
		index = m_m_line.indexOf(' ', index_b);

		if (index == BAD_INDEX)
			return false;

		m_transport = m_m_line.substring(index_b, index - index_b);

		text << m_transport << ' ';

		index_b = index + 1;

		if (m_stream_id == MG_STREAM_TYPE_IMAGE)
		{
			item = m_m_line.substring(index_b);
			if (item.indexOf("t38") != BAD_INDEX)
			{
				mg_media_element_t* element_t38 = mg_create_media_element_t38(m_term_id, &m_media_elements, (m_port > 0));
				if (element_t38 == nullptr)
				{
					return false;
				}
			}

			text << item;

		}
		else
		{
			bool first = true;
			while ((index = m_m_line.indexOf(' ', index_b)) != BAD_INDEX)
			{
				item = m_m_line.substring(index_b, index - index_b);

				uint16_t payload = (uint16_t)strtoul(item, nullptr, 10);

				text << payload << ' ';

				mg_media_element_t* melem = m_media_elements.create(m_term_id, payload, mg_media_element_list_t::POSITION_END);

				if (melem != nullptr)
				{
					if ((first) && (!melem->isDtmf()) && (!melem->isNull()))
					{
						int index_priority = m_media_elements.getCount() - 1;
						if (index_priority >= 0)
						{
							m_media_elements.set_priority_index(index_priority);
						}
						first = false;
					}
				}

				index_b = index + 1;
			}

			item = m_m_line.substring(index_b);

			uint16_t payload = (uint16_t)strtoul(item, nullptr, 10);

			text << payload;

			mg_media_element_t* elem_last = m_media_elements.create(m_term_id, payload, mg_media_element_list_t::POSITION_END);

			if (elem_last != nullptr)
			{
				if ((first) && (!elem_last->isDtmf()) && (!elem_last->isNull()))
				{
					int index_priority = m_media_elements.getCount() - 1;
					if (index_priority >= 0)
					{
						m_media_elements.set_priority_index(index_priority);
					}
					first = false;
				}
			}
		}

		PLOG_SDP(LOG_PREFIX, "(%s) read_m_line -- '%s'", (const char*)m_term_id, (const char*)text);

		return true;
	}
	//------------------------------------------------------------------------------
	void mg_sdp_media_base_t::write_m_line(rtl::String& ss)
	{
		int eCount = m_media_elements.getCount();

		uint16_t port = (m_port == UINT16_MAX || eCount == 0) ? 0 : m_port;

		ss << "m=" << m_type << ' ';
		if (port == UINT16_MAX)
			ss << "$ ";
		else
			ss << port << ' ';
		ss << m_transport;

		for (int i = 0; i < m_media_elements.getCount(); i++)
		{
			const mg_media_element_t* payload_el = m_media_elements.getAt(i);
			if (payload_el == nullptr)
			{
				continue;
			}
			ss << ' ';
			if (payload_el->get_payload_id() > 200)
			{
				ss << payload_el->get_element_name();
			}
			else
			{
				ss << payload_el->get_payload_id();
			}
		}

		PLOG_SDP(LOG_PREFIX, "(%s) write_m_line -- '%s'", (const char*)m_term_id, (const char*)ss);
	}
	//------------------------------------------------------------------------------
	void mg_sdp_media_base_t::update_m_line()
	{
		rtl::String ss;

		write_m_line(ss);

		m_m_line = ss;
	}
	//------------------------------------------------------------------------------
	// 
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::read_a_line(const rtl::String& media_line, bool compare)
	{
		if (media_line.isEmpty())
		{
			return false;
		}

		rtl::String a_line = media_line;
		
		PLOG_SDP(LOG_PREFIX, "(%s) read_a_line -- '%s'", (const char*)m_term_id, (const char*)a_line);

		if (a_line.indexOf("a=rtpmap:") != BAD_INDEX)
		{
			if (compare)
			{
				compare_media_param(media_line);
				return true;
			}

			return read_rtpmap(media_line);
		}
		else if (a_line.indexOf("a=fmtp:") != BAD_INDEX)
		{
			if (compare)
			{
				compare_media_param(media_line);
				return true;
			}

			return read_fmtp(media_line);
		}
		else if (a_line.indexOf("a=ptime:") != BAD_INDEX)
		{
			return read_ptime(media_line, compare);
		}
		else if (a_line.indexOf("a=maxptime:") != BAD_INDEX)
		{
			return read_maxptime(media_line, compare);
		}
		else if (a_line.indexOf("a=crypto:") != BAD_INDEX)
		{
			return read_crypto(media_line, compare);
		}
		else if (a_line.indexOf("a=ssrc-group:FID") != BAD_INDEX)
		{
			return true;
		}
		else if (a_line.indexOf("a=ssrc:") != BAD_INDEX)
		{
			return read_ssrc(media_line, compare);
		}
		else if (a_line.indexOf("a=fingerprint:") != BAD_INDEX)
		{
			return read_fingerprint(media_line, compare);
		}
		else if (a_line.indexOf("a=setup:") != BAD_INDEX)
		{
			return read_fingerprint_setup(media_line, compare);
		}
		else if (a_line.indexOf("a=candidate:") != BAD_INDEX)
		{
			return read_candidate(media_line, compare);
		}
		else if (a_line.indexOf("a=ice-ufrag:") != BAD_INDEX)
		{
			return read_ufrag(media_line, compare);
		}
		else if (a_line.indexOf("a=ice-pwd:") != BAD_INDEX)
		{
			return read_pwd(media_line, compare);
		}
		else if (a_line.indexOf("a=rtcp-mux") != BAD_INDEX)
		{
			return read_rtcp_mux(media_line, compare);
		}
		else if (a_line.indexOf("a=mid:") != BAD_INDEX)
		{
			return read_mid(media_line, compare);
		}
		else if (a_line.indexOf("a=extmap:") != BAD_INDEX)
		{
			return read_extmap(media_line, compare);
		}
		else if (a_line.indexOf("a=rtcp:") != BAD_INDEX)
		{
			return read_rtcp(media_line, compare);
		}
		else if (a_line.indexOf("a=rtcp-fb:") != BAD_INDEX)
		{
			if (compare)
			{
				compare_media_param(media_line);
				return true;
			}
			return read_rtcp_fb(media_line);
		}
		else if ((a_line.indexOf("a=sendonly") != BAD_INDEX) ||
			(a_line.indexOf("a=recvonly") != BAD_INDEX) ||
			(a_line.indexOf("a=sendrecv") != BAD_INDEX) ||
			(a_line.indexOf("a=inactive") != BAD_INDEX))
		{
			return read_mode(media_line, compare);
		}
		else if (a_line.indexOf("a=T38") != BAD_INDEX)
		{
			for (int i = 0; i < m_media_elements.getCount(); i++)
			{
				mg_media_element_t* el = m_media_elements.getAt(i);
				if (el == nullptr)
				{
					continue;
				}
				if (el->get_element_name().toLower().indexOf("t38") != BAD_INDEX)
				{
					el->addParam(a_line);
					break;
				}
			}
		}
		else if (a_line.indexOf("a=no_transcode") != BAD_INDEX)
		{
			return true;
		}
		else
		{
			bool find = false;
			for (int j = 0; j < m_atributes.getCount(); j++)
			{
				rtl::String str = m_atributes.getAt(j);
				if (str.indexOf(media_line) != BAD_INDEX)
				{
					find = true;
					break;
				}

			}
			if (!find)
			{
				m_atributes.add(media_line);
			}
		}

		return true;
	}
	//------------------------------------------------------------------------------
	// a=rtpmap: 101 telephone-event/8000
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::read_rtpmap(const rtl::String& rtpmap_line)
	{
		if (rtpmap_line.isEmpty())
		{
			return false;
		}

		rtl::String text;

		int index_b = 0;
		int index = rtpmap_line.indexOf('=');
		if (index == BAD_INDEX)
			return false;

		index_b = index + 1;
		index = rtpmap_line.indexOf(':');
		if (index == BAD_INDEX)
			return false;

		index_b = index + 1;
		if (rtpmap_line[index_b] == ' ')
		{
			index_b++;
		}
		index = rtpmap_line.indexOf(' ', index_b);

		if (index == BAD_INDEX)
			return false;

		rtl::String item = rtpmap_line.substring(index_b, index - index_b);
		
		if (item.isEmpty())
			return false;

		uint16_t payload_id = (uint16_t)strtoul(item, nullptr, 10);
		text << "a=rtpmap:" << payload_id;

		mg_media_element_t* payload_element = m_media_elements.find(payload_id);
		if (payload_element == nullptr)
			return false;

		index_b = index + 1;
		index = rtpmap_line.indexOf('/', index_b);

		if (index == BAD_INDEX)
			return false;

		item = rtpmap_line.substring(index_b, index - index_b);

		payload_element->set_element_name(item);
		text << ' ' << item;
	
		payload_element->addParam(rtpmap_line);
		
		PLOG_SDP(LOG_PREFIX, "(%s) read_rtpmap -- '%s'", (const char*)m_term_id, (const char*)text);
		
		return true;
	}
	//------------------------------------------------------------------------------
	// a=fmtp: 101 0-15
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::read_fmtp(const rtl::String& fmtp_line)
	{
		if (fmtp_line.isEmpty())
		{
			return false;
		}

		rtl::String text;

		int index_b = 0;
		int index = fmtp_line.indexOf('=');

		if (index == BAD_INDEX)
			return false;

		index_b = index + 1;
		index = fmtp_line.indexOf(':');

		if (index == BAD_INDEX)
			return false;

		index_b = index + 1;
		if (fmtp_line[index_b] == ' ')
		{
			index_b++;
		}
		index = fmtp_line.indexOf(' ', index_b);

		if (index == BAD_INDEX)
			return false;

		rtl::String item = fmtp_line.substring(index_b, index - index_b);
		if (item.isEmpty())
		{
			return false;
		}

		uint16_t payload_id = (uint16_t)strtoul(item, nullptr, 10);
		
		text << "a=fmtp:" << payload_id;

		mg_media_element_t* payload_element = m_media_elements.find(payload_id);
		if (payload_element == nullptr)
		{
			return false;
		}

		payload_element->addParam(fmtp_line);
		PLOG_SDP(LOG_PREFIX, "(%s) read_fmtp -- '%s'", (const char*)m_term_id, (const char*)text);
		
		return true;
	}
	//------------------------------------------------------------------------------
	// a=rtcp-fb:100 ccm fir
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::read_rtcp_fb(const rtl::String& media_line)
	{
		if (media_line.isEmpty())
		{
			return false;
		}

		rtl::String rtcp_fb(media_line);

		int index_b = 0;
		int index = rtcp_fb.indexOf(':');
		if (index == BAD_INDEX)
		{
			return false;
		}

		index_b = index + 1;
		index = rtcp_fb.indexOf(' ');
		if (index == BAD_INDEX)
		{
			return false;
		}

		rtl::String pt_str = rtcp_fb.substring(index_b, index - index_b);
		if (pt_str.isEmpty())
		{
			return false;
		}

		uint16_t payload_id = (uint16_t)strtoul(pt_str, nullptr, 10);

		mg_media_element_t* payload_element = m_media_elements.find(payload_id);
		if (payload_element == nullptr)
		{
			return false;
		}

		payload_element->addParam(media_line);
		return true;
	}
	//------------------------------------------------------------------------------
	// a=ptime: 20
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::read_ptime(const rtl::String& media_line, bool compare)
	{
		if (media_line.isEmpty())
		{
			return false;
		}

		rtl::String ptime_line(media_line);

		if (compare)
		{
			ptime_line.trim();
			ptime_line.replace(": " , ":");

			rtl::String ptime_compare;
			write_ptime(ptime_compare);
			ptime_compare.trim();
			if (rtl::String::compare(ptime_line, ptime_compare, false) != 0)
			{
				mg_sdp_difference_t* diff = NEW mg_sdp_difference_t();
				diff->type = mg_sdp_difference_type::ptime;
				diff->line.append(media_line);
				diff->stream_id = m_stream_id;
				m_differences_list.add(diff);
			}
			return true;
		}

		int index_b = 0;
		int index = ptime_line.indexOf(':');
		if (index == BAD_INDEX)
			return false;

		index_b = index + 1;
		if (ptime_line[index_b] == ' ')
		{
			index_b++;
		}
	
		rtl::String step = ptime_line.substring(index_b);

		if (step.isEmpty())
		{
			PLOG_SDP(LOG_PREFIX, "(%s) read_fmtp -- step.isEmpty().", (const char*)m_term_id);
			return false;
		}

		m_ptime = (uint16_t)strtoul(step, nullptr, 10);

		PLOG_SDP(LOG_PREFIX, "(%s) read_fmtp -- set m_ptime: %u).", (const char*)m_term_id, m_ptime);

		return true;
	}
	//------------------------------------------------------------------------------
	// a=maxptime:60
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::read_maxptime(const rtl::String& media_line, bool compare)
	{
		if (media_line.isEmpty())
		{
			return false;
		}

		rtl::String maxptime_line(media_line);

		if (compare)
		{
			maxptime_line.trim();
			maxptime_line.replace(": ", ":");

			rtl::String maxptime_compare;
			write_maxptime(maxptime_compare);
			maxptime_compare.trim();
			if (rtl::String::compare(maxptime_line, maxptime_compare, false) != 0)
			{
				mg_sdp_difference_t* diff = NEW mg_sdp_difference_t();
				diff->type = mg_sdp_difference_type::ptime;
				diff->line.append(media_line);
				diff->stream_id = m_stream_id;
				m_differences_list.add(diff);
			}
			return true;
		}

		int index_b = 0;
		int index = maxptime_line.indexOf(':');
		if (index == BAD_INDEX)
		{
			return false;
		}

		index_b = index + 1;
		if (maxptime_line[index_b] == ' ')
		{
			index_b++;
		}

		rtl::String maxptime = maxptime_line.substring(index_b);
		if (maxptime.isEmpty())
		{
			return false;
		}

		m_maxptime = (uint16_t)strtoul(maxptime, nullptr, 10);
		return true;
	}
	//------------------------------------------------------------------------------
	// a=crypto:1 AES_CM_128_HMAC_SHA1_80 inline:5sDcCo6NtcKqf9qi3AFCHXz9F2qZIdpCFCYIQbek
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::read_crypto(const rtl::String& media_line, bool compare)
	{
		if (media_line.isEmpty())
		{
			return false;
		}

		if (compare)
		{
			bool find = false;
			for (int i = 0; i < m_crypto_params.getCount(); i++)
			{
				rtl::String crypto = m_crypto_params.getAt(i);
				if (rtl::String::compare(crypto, media_line, false) == 0)
				{
					find = true;
					break;
				}
			}

			if (!find)
			{
				mg_sdp_difference_t* diff = NEW mg_sdp_difference_t();
				diff->type = mg_sdp_difference_type::crypto;
				diff->line.append(media_line);
				diff->stream_id = m_stream_id;
				m_differences_list.add(diff);
			}

			return true;
		}

		m_crypto_params.add(media_line);

		return true;
	}
	//------------------------------------------------------------------------------
	// a=ssrc:1061712719 cname:NCuce0OHjVRX4MAI
	// a=ssrc:1061712719 msid:6n36IqVDOliFUE63N0W6CgBebhGRDxx8wWqS 281c0c2f-fd25-4b7e-a32c-de9d8dead668
	// a=ssrc:1061712719 mslabel:6n36IqVDOliFUE63N0W6CgBebhGRDxx8wWqS
	// a=ssrc:1061712719 label:281c0c2f-fd25-4b7e-a32c-de9d8dead668
	//    or firefox
	// a=ssrc:1890385820 cname:{a858d452-49bc-454d-b8b9-9216952a7c24\}
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::read_ssrc(const rtl::String& media_line, bool compare)
	{
		uint32_t ssrc = mg_sdp_ssrc_param_t::get_ssrc_in_line(media_line);
		if (ssrc == 0)
		{
			return false;
		}

		mg_sdp_ssrc_param_t* current_param = nullptr;
		for (int i = 0; i < m_ssrc_param_list.getCount(); i++)
		{
			mg_sdp_ssrc_param_t* ssrc_param = m_ssrc_param_list.getAt(i);
			if (ssrc_param == nullptr)
			{
				continue;
			}

			if (ssrc_param->get_ssrc() == ssrc)
			{
				current_param = ssrc_param;
				break;
			}
		}

		if (current_param == nullptr)
		{
			if (compare)
			{
				mg_sdp_difference_t* diff = NEW mg_sdp_difference_t();
				diff->type = mg_sdp_difference_type::ssrc;
				diff->line.append(media_line);
				diff->stream_id = m_stream_id;
				m_differences_list.add(diff);
				return true;
			}

			current_param = NEW mg_sdp_ssrc_param_t();
			m_ssrc_param_list.add(current_param);
		}
	
		return current_param->add_ssrc_line(media_line);
	}
	//------------------------------------------------------------------------------
	// a=fingerprint:sha-256 8B:1A:7A:ED:F5:5C:D0:66:36:A6:3F:41:4C:F4:12:AF:A2:40:8B:FA:D9:69:F1:1B:0E:9F:DC:24:0B:74:F1:59
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::read_fingerprint(const rtl::String& media_line, bool compare)
	{
		if (m_fingerprint == nullptr)
		{
			if (compare)
			{
				mg_sdp_difference_t* diff = NEW mg_sdp_difference_t();
				diff->type = mg_sdp_difference_type::crypto;
				diff->line.append(media_line);
				diff->stream_id = m_stream_id;
				m_differences_list.add(diff);
				return true;
			}
			m_fingerprint = NEW sdp_fingerprint_t();
		}

		if (compare)
		{
			char buff[256] = { 0 };
			//int write =
			m_fingerprint->write_to(buff, 256);

			if (rtl::String::compare(buff, media_line, false) != 0)
			{
				mg_sdp_difference_t* diff = NEW mg_sdp_difference_t();
				diff->type = mg_sdp_difference_type::crypto;
				diff->line.append(media_line);
				diff->stream_id = m_stream_id;
				m_differences_list.add(diff);
			}
			return true;
		}

		m_fingerprint->parse(media_line);
		return true;
	}
	//------------------------------------------------------------------------------
	// a=setup:active
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::read_fingerprint_setup(const rtl::String& media_line, bool compare)
	{
		if (m_fingerprint == nullptr)
		{
			if (compare)
			{
				mg_sdp_difference_t* diff = NEW mg_sdp_difference_t();
				diff->type = mg_sdp_difference_type::crypto;
				diff->line.append(media_line);
				diff->stream_id = m_stream_id;
				m_differences_list.add(diff);
				return true;
			}
			m_fingerprint = NEW sdp_fingerprint_t();
		}

		if (compare)
		{
			rtl::String role(m_fingerprint->write_role());
			if (rtl::String::compare(role, media_line, false) != 0)
			{
				mg_sdp_difference_t* diff = NEW mg_sdp_difference_t();
				diff->type = mg_sdp_difference_type::crypto;
				diff->line.append(media_line);
				diff->stream_id = m_stream_id;
				m_differences_list.add(diff);
			}
		}

		return m_fingerprint->read_role(media_line);
	}
	//------------------------------------------------------------------------------
	// a=candidate:3364417298 1 udp 2122260223 192.168.0.73 60326 typ host generation 0
	// a=candidate:2248872930 1 tcp 1518280447 192.168.0.73 9 typ host tcptype active generation 0
	// a=candidate:1050187462 1 udp 1686052607 87.117.163.31 13665 typ srflx raddr 192.168.0.73 rport 60326 
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::read_candidate(const rtl::String& media_line, bool compare)
	{
		if (m_ice == nullptr)
		{
			if (compare)
			{
				mg_sdp_difference_t* diff = NEW mg_sdp_difference_t();
				diff->type = mg_sdp_difference_type::ice;
				diff->line.append(media_line);
				diff->stream_id = m_stream_id;
				m_differences_list.add(diff);
				return true;
			}
			m_ice = NEW mg_sdp_ice_param_t();
		}

		if (compare)
		{
			if (!m_ice->compare_sdp_ice_line(media_line))
			{
				mg_sdp_difference_t* diff = NEW mg_sdp_difference_t();
				diff->type = mg_sdp_difference_type::ice;
				diff->line.append(media_line);
				diff->stream_id = m_stream_id;
				m_differences_list.add(diff);
			}
			return true;
		}

		return m_ice->add_ice_line(media_line);
	}
	//------------------------------------------------------------------------------
	// a=ice-ufrag:iVAb+WAnUnzAVvea
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::read_ufrag(const rtl::String& media_line, bool compare)
	{
		if (m_ice == nullptr)
		{
			if (compare)
			{
				mg_sdp_difference_t* diff = NEW mg_sdp_difference_t();
				diff->type = mg_sdp_difference_type::ice;
				diff->line.append(media_line);
				diff->stream_id = m_stream_id;
				m_differences_list.add(diff);
				return true;
			}
			m_ice = NEW mg_sdp_ice_param_t();
		}

		if (compare)
		{
			rtl::String ufrag("a=ice-ufrag:");
			ufrag << m_ice->get_ufrag();

			if (rtl::String::compare(media_line, ufrag, false) != 0)
			{
				mg_sdp_difference_t* diff = NEW mg_sdp_difference_t();
				diff->type = mg_sdp_difference_type::ice;
				diff->line.append(media_line);
				diff->stream_id = m_stream_id;
				m_differences_list.add(diff);
			}
			return true;
		}

		return m_ice->add_ice_line(media_line);
	}
	//------------------------------------------------------------------------------
	// a=ice-pwd:1iiV4HvsIuU5w6If45go9wwM
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::read_pwd(const rtl::String& media_line, bool compare)
	{
		if (m_ice == nullptr)
		{
			if (compare)
			{
				mg_sdp_difference_t* diff = NEW mg_sdp_difference_t();
				diff->type = mg_sdp_difference_type::ice;
				diff->line.append(media_line);
				diff->stream_id = m_stream_id;
				m_differences_list.add(diff);
				return true;
			}
			m_ice = NEW mg_sdp_ice_param_t();
		}

		if (compare)
		{
			rtl::String pwd("a=ice-pwd:");
			pwd << m_ice->get_pwd();

			if (rtl::String::compare(media_line, pwd, false) != 0)
			{
				mg_sdp_difference_t* diff = NEW mg_sdp_difference_t();
				diff->type = mg_sdp_difference_type::ice;
				diff->line.append(media_line);
				diff->stream_id = m_stream_id;
				m_differences_list.add(diff);
			}
			return true;
		}

		return m_ice->add_ice_line(media_line);
	}
	//------------------------------------------------------------------------------
	// a=rtcp-mux
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::read_rtcp_mux(const rtl::String&media_line, bool compare)
	{
		if (compare)
		{
			if (!m_rtcp_mux)
			{
				mg_sdp_difference_t* diff = NEW mg_sdp_difference_t();
				diff->type = mg_sdp_difference_type::rtcp;
				diff->line.append(media_line);
				diff->stream_id = m_stream_id;
				m_differences_list.add(diff);
			}
			return true;
		}

		m_rtcp_mux = !media_line.isEmpty();

		return true;
	}
	//------------------------------------------------------------------------------
	// a=mid:audio
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::read_mid(const rtl::String&media_line, bool compare)
	{
		if (media_line.isEmpty())
		{
			return false;
		}

		// a=mid:audio

		rtl::String mid_line(media_line);

		if (compare)
		{
			rtl::String mid;
			write_mid(mid);

			if (rtl::String::compare(mid, media_line, false) != 0)
			{
				mg_sdp_difference_t* diff = NEW mg_sdp_difference_t();
				diff->type = mg_sdp_difference_type::other;
				diff->line.append(media_line);
				diff->stream_id = m_stream_id;
				m_differences_list.add(diff);
			}
			return true;
		}

		int index_b = 0;
		int index = mid_line.indexOf(':');
		if (index == BAD_INDEX)
		{
			return false;
		}

		//rtl::String a_mid = mid_line.substring(0, index);

		index_b = index + 1;

		m_mid_type = mid_line.substring(index_b);
		
		return true;
	}
	//------------------------------------------------------------------------------
	// a=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level
	// a=extmap:3 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::read_extmap(const rtl::String&media_line, bool compare)
	{
		//if (media_line.isEmpty())
		//{
		//	return false;
		//}

		//if (compare)
		//{
		//	for (int i = 0; i < m_extmaps.getCount(); i++)
		//	{
		//		rtl::String ext = m_extmaps.getAt(i);
		//		ext.trim();
		//		rtl::String ext_str(media_line);
		//		ext_str.trim();

		//		if (rtl::String::compare(ext, ext_str, false) == 0)
		//		{
		//			return true;
		//		}
		//	}

		//	mg_sdp_difference_t* diff = NEW mg_sdp_difference_t();
		//	diff->type = mg_sdp_difference_type::other;
		//	diff->line.append(media_line);
		//	diff->stream_id = m_stream_id;
		//	m_differences_list.add(diff);

		//	return true;
		//}

		//m_extmaps.add(media_line);

		return true;
	}
	//------------------------------------------------------------------------------
	// c=IN IP4 87.117.163.31
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::read_c_line(const rtl::String&media_line, bool compare)
	{
		if (media_line.isEmpty())
		{
			return false;
		}

		//c=IN IP4 87.117.163.31

		rtl::String c_line(media_line);
		if (compare)
		{
			rtl::String address;
			write_c_line(address);

			if (rtl::String::compare(address, c_line, false) != 0)
			{
				mg_sdp_difference_t* diff = NEW mg_sdp_difference_t();
				diff->type = mg_sdp_difference_type::address;
				diff->line.append(media_line);
				diff->stream_id = m_stream_id;
				m_differences_list.add(diff);
			}

			return true;
		}

		int index_b = 0;
		int index = c_line.indexOf(' ');
		if (index == BAD_INDEX)
		{
			return false;
		}

		rtl::String in = c_line.substring(0, index);

		index_b = index + 1;
		index = c_line.indexOf(' ');
		if (index == BAD_INDEX)
		{
			return false;
		}

		rtl::String type = c_line.substring(index_b, index - index_b);

		index_b = index + 1;

		rtl::String addr = c_line.substring(index_b);
		if (addr.isEmpty())
		{
			return false;
		}

		m_address.parse(addr);

		return true;
	}
	//------------------------------------------------------------------------------
	// a=rtcp:2049 IN IP4 87.117.163.31
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::read_rtcp(const rtl::String&media_line, bool compare)
	{
		// a=rtcp:2049 IN IP4 87.117.163.31

		if (media_line.isEmpty())
		{
			return false;
		}

		rtl::String rtcp_line(media_line);

		if (compare)
		{
			rtl::String rtcp("a=rtcp:");
			rtcp << m_rtcp_port << " IN IP4 " << m_rtcp_address.to_string();

			if (rtl::String::compare(rtcp, rtcp_line, false) != 0)
			{
				mg_sdp_difference_t* diff = NEW mg_sdp_difference_t();
				diff->type = mg_sdp_difference_type::rtcp;
				diff->line.append(media_line);
				diff->stream_id = m_stream_id;
				m_differences_list.add(diff);
			}

			return true;
		}

		int index_b = 0;
		int index = rtcp_line.indexOf(':');
		if (index == BAD_INDEX)
		{
			return false;
		}

		rtl::String rtcp = rtcp_line.substring(0, index);

		index_b = index + 1;
		index = rtcp_line.indexOf(' ');
		if (index == BAD_INDEX)
		{
			return false;
		}

		rtl::String port_str = rtcp_line.substring(index_b, index - index_b);
		m_rtcp_port = (uint16_t)strtoul(port_str, nullptr, 10);

		return true;
	}
	//------------------------------------------------------------------------------
	// a=sendrecv
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::read_mode(const rtl::String&media_line, bool compare)
	{
		// a=sendonly
		if (media_line.isEmpty())
		{
			return false;
		}

		rtl::String mode_line(media_line);

		if (compare)
		{
			rtl::String mode;
			write_mode(mode);

			if (rtl::String::compare(mode, mode_line, false) != 0)
			{
				mg_sdp_difference_t* diff = NEW mg_sdp_difference_t();
				diff->type = mg_sdp_difference_type::mode;
				diff->line.append(media_line);
				diff->stream_id = m_stream_id;
				m_differences_list.add(diff);
			}
			return true;
		}

	//	int index_b = 0;
		int index = mode_line.indexOf('=');
		if (index == BAD_INDEX)
		{
			return false;
		}

		m_mode = mode_line.substring(index + 1);

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_sdp_media_base_t::write_a_lines(rtl::String& ss)
	{
		write_rtcp(ss);

		for (int i = 0; i < m_media_elements.getCount(); i++)
		{
			const mg_media_element_t* payload_el = m_media_elements.getAt(i);
			if (payload_el == nullptr)
			{
				continue;
			}

			for (int j = 0; j < payload_el->getParamCount(); j++)
			{
				rtl::String param = payload_el->getParam(j);
				ss << "\r\n";
				ss << param;
			}

			if (payload_el->get_element_name().toLower().indexOf("t38") != BAD_INDEX)
			{
				payload_el->write_fax_param(ss);
			}
		}

		write_mode(ss);

		for (int i = 0; i < m_atributes.getCount(); i++)
		{
			rtl::String param = m_atributes.getAt(i);
			if (param.indexOf("handmade") != BAD_INDEX)
			{
				continue;
			}
			ss << "\r\n";
			ss << param;
		}

		write_candidate(ss);
		if (!write_crypto(ss))
		{
			write_fingerprint(ss);
		}
		write_mid(ss);
		write_extmap(ss);
		write_ptime(ss);
		write_maxptime(ss);
		write_ssrc(ss);

		PLOG_SDP(LOG_PREFIX, "(%s) write_a_line -- '%s'", (const char*)m_term_id, (const char*)ss);
	}
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::write_ptime(rtl::String& ss)
	{
		if (m_ptime == 0)
		{
			return true;
		}

		ss << "\r\n";
		ss << "a=ptime:" << m_ptime;

		return true;
	}
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::write_maxptime(rtl::String& ss)
	{
		if (m_maxptime == 0)
		{
			return true;
		}

		ss << "\r\n";
		ss << "a=maxptime:" << m_maxptime;

		return true;
	}
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::write_crypto(rtl::String& ss)
	{
		if (m_crypto_params.getCount() == 0 || m_transport != "RTP/SAVP")
		{
			return false;
		}

		for (int i = 0; i < m_crypto_params.getCount(); i++)
		{
			rtl::String crypto = m_crypto_params.getAt(i);

			ss << "\r\n";
			ss << crypto;
		}
	
		return true;
	}
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::write_ssrc(rtl::String& ss)
	{
		if (m_ssrc_param_list.getCount() == 0)
		{
			return true;
		}

		rtl::String ssrc_param_str;
		rtl::String ssrc_param_group_str;
		if (m_ssrc_param_list.getCount() > 1)
		{
			//a=ssrc-group:FID 609648622 3385947471
			ssrc_param_group_str << "\r\n";
			ssrc_param_group_str << "a=ssrc-group:FID";
		}

		for (int i = 0; i < m_ssrc_param_list.getCount(); i++)
		{
			mg_sdp_ssrc_param_t* ssrc_param = m_ssrc_param_list.getAt(i);
			if (ssrc_param == nullptr)
			{
				continue;
			}

			if (!ssrc_param_group_str.isEmpty())
			{
				ssrc_param_group_str << ' ' << ssrc_param->get_ssrc();
			}

			if (!ssrc_param->write_ssrc_lines(ssrc_param_str))
			{
				return false;
			}
		}

		if (!ssrc_param_group_str.isEmpty())
		{
			ss << ssrc_param_group_str;
		}
		ss << ssrc_param_str;

		return true;
	}
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::write_fingerprint(rtl::String& ss)
	{
		if (m_fingerprint == nullptr)
		{
			return true;
		}

		char buff[256] = { 0 };
		int write = m_fingerprint->write_to(buff, 256);
		if (write <= 0)
		{
			return false;
		}
		buff[write] = 0;

		ss << "\r\n";
		ss << buff;
		//ss << "\r\n";
		ss << m_fingerprint->write_role();

		return true;
	}
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::write_candidate(rtl::String& ss)
	{
		if (m_ice == nullptr)
		{
			return true;
		}

		return m_ice->write_ice_lines(ss);
	}
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::write_rtcp(rtl::String& ss)
	{
		// a=rtcp:2049 IN IP4 87.117.163.31

		if ((!m_rtcp_address.empty()) && (m_rtcp_port != 0))
		{
			ss << "\r\n" << "a=rtcp:" << m_rtcp_port << " IN ";

			if (m_rtcp_address.get_family() == e_ip_address_family::E_IP_ADDRESS_FAMILY_IPV4)
			{
				ss << "IP4 ";
			}
			else
			{
				ss << "IP6 ";
			}
			ss << m_rtcp_address.to_string();
		}

		if (m_rtcp_mux)
		{
			ss << "\r\n";
			ss << "a=rtcp-mux";
		}

		return true;
	}
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::write_mid(rtl::String& ss)
	{
		if (m_mid_type.isEmpty())
		{
			return true;
		}

		ss << "\r\n";
		ss << "a=mid:" << m_mid_type;

		return true;
	}
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::write_extmap(rtl::String& ss)
	{
		for (int i = 0; i < m_extmaps.getCount(); i++)
		{
			rtl::String extmap = m_extmaps.getAt(i);
		
			ss << "\r\n";
			ss << extmap;
		}

		return true;
	}
	//------------------------------------------------------------------------------
	void mg_sdp_media_base_t::write_c_line(rtl::String& ss)
	{
		if (m_address.empty())
		{
			return;
		}

		//c=IN IP4 87.117.163.31
		ss << "\r\n" << "c=IN ";

		if (m_address.get_family() == e_ip_address_family::E_IP_ADDRESS_FAMILY_IPV4)
		{
			ss << "IP4 ";
		}
		else
		{
			ss << "IP6 ";
		}

		ss << m_address.to_string();
	}
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::write_mode(rtl::String& ss)
	{
		if (m_mode.isEmpty())
		{
			return true;
		}

		// a=sendonly
		ss << "\r\n" << "a=" << m_mode;

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::compare_media_param(const rtl::String&media_line)
	{
		if (!m_media_elements.compare_sdp_media_param(media_line))
		{
			mg_sdp_difference_t* diff = NEW mg_sdp_difference_t();
			diff->type = mg_sdp_difference_type::media;
			diff->line.append(media_line);
			diff->stream_id = m_stream_id;
			m_differences_list.add(diff);
			return false;
		}
		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_sdp_media_base_t::set_rtcp_mux(bool mux)
	{
		m_rtcp_mux = mux;
	}
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::check_rtcp_mux()
	{
		return m_rtcp_mux;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	rtl::StringList& mg_sdp_media_base_t::get_extmaps()
	{
		return m_extmaps;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	const rtl::String& mg_sdp_media_base_t::get_mid_type()
	{
		return m_mid_type;
	}
	//------------------------------------------------------------------------------
	void mg_sdp_media_base_t::set_mid_type(const char* type)
	{
		m_mid_type = rtl::String::empty;
		m_mid_type.append(type);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	const char* mg_sdp_media_base_t::get_msid_semantic()
	{
		if (m_ssrc_param_list.getCount() == 0)
		{
			return "";
		}

		return m_ssrc_param_list.getAt(0)->get_mslabel();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	const uint16_t mg_sdp_media_base_t::get_rtcp_port() const
	{
		return m_rtcp_port;
	}
	//------------------------------------------------------------------------------
	void mg_sdp_media_base_t::set_rtcp_port(uint16_t port)
	{
		m_rtcp_port = port;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	const ip_address_t* mg_sdp_media_base_t::get_address() const
	{
		return &m_address;
	}
	//------------------------------------------------------------------------------
	void mg_sdp_media_base_t::set_address(const ip_address_t* addr)
	{
		m_address.copy_from(addr);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	const ip_address_t* mg_sdp_media_base_t::get_rtcp_address() const
	{
		return &m_rtcp_address;
	}
	//------------------------------------------------------------------------------
	void mg_sdp_media_base_t::set_rtcp_address(const ip_address_t* addr)
	{
		m_rtcp_address.copy_from(addr);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	rtl::ArrayT<mg_sdp_ssrc_param_t*>* mg_sdp_media_base_t::get_ssrc_params()
	{
		return &m_ssrc_param_list;
	}
	//------------------------------------------------------------------------------
	void mg_sdp_media_base_t::clear_ssrc_params()
	{
		for (int i = 0; i < m_ssrc_param_list.getCount(); i++)
		{
			mg_sdp_ssrc_param_t* ssrc_param = m_ssrc_param_list.getAt(i);
			if (ssrc_param == nullptr)
			{
				continue;
			}

			DELETEO(ssrc_param);
			ssrc_param = nullptr;
		}
		m_ssrc_param_list.clear();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_sdp_media_base_t::set_fingerprint_param(sdp_fingerprint_t* fingerprint_param)
	{
		clear_fingerprint_param();

		m_fingerprint = NEW sdp_fingerprint_t();
		m_fingerprint->copy_from(fingerprint_param);
	}
	//------------------------------------------------------------------------------
	sdp_fingerprint_t* mg_sdp_media_base_t::get_fingerprint_param()
	{
		return m_fingerprint;
	}
	//------------------------------------------------------------------------------
	void mg_sdp_media_base_t::clear_fingerprint_param()
	{
		if (m_fingerprint != nullptr)
		{
			DELETEO(m_fingerprint);
			m_fingerprint = nullptr;
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_sdp_media_base_t::set_ice_param(mg_sdp_ice_param_t* ice_param)
	{
		clear_ice_param();

		m_ice = NEW mg_sdp_ice_param_t();
		m_ice->copy_from(ice_param);
	}
	//------------------------------------------------------------------------------
	mg_sdp_ice_param_t*	mg_sdp_media_base_t::get_ice_param()
	{
		return m_ice;
	}
	//------------------------------------------------------------------------------
	void mg_sdp_media_base_t::clear_ice_param()
	{
		if (m_ice != nullptr)
		{
			DELETEO(m_ice);
			m_ice = nullptr;
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_sdp_media_base_t::clear_differences_list()
	{
		for (int i = 0; i < m_differences_list.getCount(); i++)
		{
			mg_sdp_difference_t* diff = m_differences_list.getAt(i);
			if (diff == nullptr)
			{
				continue;
			}

			DELETEO(diff);
			diff = nullptr;
		}

		m_differences_list.clear();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_sdp_media_base_t::filer_media_elements()
	{
		for (int i = m_media_elements.getCount() - 1; i >= 0; i--)
		{
			mg_media_element_t* el = m_media_elements.getAt(i);

			if (!rtx_codec__isKnownFormat(el->get_element_name()))
			{
				PLOG_WRITE("SDPM", "removing unknown element '%s' %d", (const char*)el->get_element_name(), el->get_payload_id());
				m_media_elements.removeAt(i);
			}
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_sdp_diff_list* mg_sdp_media_base_t::get_differences()
	{
		return &m_differences_list;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_sdp_media_base_t::apply_differences()
	{
		for (int i = 0; i < m_differences_list.getCount(); i ++)
		{
			mg_sdp_difference_t* diff = m_differences_list.getAt(i);
			if (diff == nullptr)
			{
				continue;
			}

			if (!add_media_line(diff->line, false))
			{
				return false;
			}
		}

		filer_media_elements();

		return true;
	}
}
//------------------------------------------------------------------------------
