﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_tx_dummy_stream.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_tx_dummy_stream_t::mg_tx_dummy_stream_t(rtl::Logger* log, uint32_t id, uint32_t parent_id, const char* term_id) :
		mg_tx_stream_t(log, id, mg_tx_dummy_e, term_id)
	{
		rtl::String name;
		name << term_id << "-s" << parent_id << "-txds" << id;
		mg_tx_stream_t::setName(name);
		mg_tx_stream_t::setTag("TXS_DUM");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_tx_dummy_stream_t::~mg_tx_dummy_stream_t()
	{

	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_tx_dummy_stream_t::send(const INextDataHandler* out_handler, const rtp_packet* packet)
	{
	}
	//------------------------------------------------------------------------------
	void mg_tx_dummy_stream_t::send(const INextDataHandler* out_handler, const uint8_t* data, uint32_t len)
	{
	}
	//------------------------------------------------------------------------------
	void mg_tx_dummy_stream_t::send(const INextDataHandler* out_handler, const rtcp_packet_t* packet)
	{
	}
}
//------------------------------------------------------------------------------
