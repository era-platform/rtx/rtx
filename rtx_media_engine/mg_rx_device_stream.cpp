﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg.h"
#include "std_thread.h"
#include "mg_rx_device_stream.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "logdef_stream.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_rx_device_stream_t::mg_rx_device_stream_t(rtl::Logger* log, uint32_t id, const char* parent_id, MediaSession& session) :
		mg_rx_stream_t(log, id, mg_rx_device_audio_e, parent_id, session)
	{
		rtl::String name;
		name << session.getName() << "-rxdevs" << id;
		setName(name);
		setTag("RXS_DEV");

		if (mge::g_media_gateway != nullptr)
		{
			h248::IAudioDeviceInput* mic = mge::g_media_gateway->getDeviceIn();
			if (mic != nullptr)
			{
				// uintptr_t dev_id = 
				mic->deviceInput__addHandler(this, 8000, 1);
			}
		}

		m_ssrc = (rtl::DateTime::getTicks() + rand()) | 0x833000b0;
		m_seq_num = (uint16_t)(rtl::DateTime::getTicks() ^ 0x000000D5D);;
		m_seq_num_last = 0;
		m_timestamp = rtl::DateTime::getTicks() & 0x005D5D5D;
		m_timestamp_last = 0;
		m_samples_last = 160;

		m_timestamp_step = 20;


		m_rx_lock.clear();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_rx_device_stream_t::~mg_rx_device_stream_t()
	{
		if (mge::g_media_gateway != nullptr)
		{
			h248::IAudioDeviceInput* mic = mge::g_media_gateway->getDeviceIn();
			if (mic != nullptr)
			{
				mic->deviceInput__removeHandler(0);
			}
		}
	}
	//------------------------------------------------------------------------------
	//const mg_rx_stream_type_t mg_rx_device_stream_t::getType() const
	//{
	//	return m_type;
	//}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_rx_device_stream_t::deviceInput__dataReady(const uint8_t* data, uint32_t len, uint32_t ts)
	{
		if (checkAndLock())
		{
			WARNING("stream is already locked!");
			return;
		}

		rx_data_stream_lock(data, len);

		releaseLock();
	}
	//------------------------------------------------------------------------------
	bool mg_rx_device_stream_t::rx_data_stream_lock(const uint8_t* data, uint32_t len)
	{
		if (!activity_())
		{
			return false;
		}

		bool result = false;
		uint32_t samples = len / sizeof(short);

		rtp_packet packet;
		packet.set_version(2);
		if ((m_seq_num_last == 0) || (m_timestamp_last == 0))
		{
			packet.set_marker(true);
		}

		packet.set_payload_type(11);
		packet.set_ssrc(m_ssrc);
		packet.set_samples(samples);

		m_seq_num_last = m_seq_num;
		m_seq_num++;
		packet.set_sequence_number(m_seq_num);

		m_timestamp_last = m_timestamp;
		m_timestamp += samples;
		packet.set_timestamp(m_timestamp);

		packet.set_payload(data, len);

		int tube_count = getTubeCount();
		for (int i = 0; i < tube_count; i++)
		{
			Tube* tube_at_list = getTube(i);
			if (tube_at_list == nullptr)
			{
				continue;
			}

			if (tube_at_list->getType() == HandlerType::AudioEncoder)
			{
				tube_at_list->send(tube_at_list, &packet); // data, len
				result = true;
				break;
			}
		}

		return result;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	int mg_rx_device_stream_t::rtcp_session_rx(const rtcp_packet_t* packet)
	{
		bool send = false;
		int tube_count = getTubeCount();
		for (int i = 0; i < tube_count; i++)
		{
			Tube* tube_at_list = getTube(i);
			if (tube_at_list == nullptr)
			{
				continue;
			}

			if (tube_at_list->getType() == HandlerType::Simple)
			{
				tube_at_list->send(tube_at_list, packet);
				send = true;
				break;
			}
		}

		if ((!send) && (tube_count != 0))
		{
			Tube* tube_at_list = getTube(0);
			if (tube_at_list != nullptr)
			{
				tube_at_list->send(tube_at_list, packet);
				send = true;
			}
		}

		return 1;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_rx_device_stream_t::activity_()
	{
		if (mg_rx_stream_t::getState() == h248::TerminationStreamMode::SendOnly)
		{
			WARNING("stream is sendonly.");
			return false;
		}

		if (mg_rx_stream_t::getState() == h248::TerminationStreamMode::Inactive)
		{
			WARNING("stream is inactive.");
			return false;
		}

		return true;
	}
}
//------------------------------------------------------------------------------
