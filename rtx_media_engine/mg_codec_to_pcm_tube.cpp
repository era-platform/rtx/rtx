﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_codec_to_pcm_tube.h"
#include "mg_media_element.h"
#include "rtp_packet.h"
#include "mg_media_element.h"

#include "logdef_tube.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_codec_to_pcm_tube_t::mg_codec_to_pcm_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id) :
		Tube(HandlerType::AudioDecoder, id, log)

	{
		makeName(parent_id, "-cpcm-tu"); // Codec to PCM Tube

		m_decoder = nullptr;
		m_codec_buffer = nullptr;
		m_codec_buffer_read = 0;

		rtl::res_counter_t::add_ref(g_mge_tube_counter);

		setLogTag("TUB-ADEC");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_codec_to_pcm_tube_t::~mg_codec_to_pcm_tube_t()
	{
		unsubscribe(this);

		if (m_decoder != nullptr)
		{
			rtx_codec__release_audio_decoder(m_decoder);
			m_decoder = nullptr;
		}

		if (m_codec_buffer != nullptr)
		{
			DELETEAR(m_codec_buffer);
			m_codec_buffer = nullptr;
		}

		rtl::res_counter_t::release(g_mge_tube_counter);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_codec_to_pcm_tube_t::init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out)
	{
		if (payload_el_in == nullptr)
		{
			ERROR_MSG("payload element is null!");
			return false;
		}
		m_payload_id = payload_el_in->get_payload_id();

		media::PayloadFormat format;
		format.setEncoding(payload_el_in->get_element_name());
		format.setFrequency(payload_el_in->get_sample_rate());
		format.setChannelCount(payload_el_in->getChannelCount());
		format.setFmtp(payload_el_in->get_param_fmtp());
		format.setId((media::PayloadId)payload_el_in->get_payload_id());

		PRINT_FMT("payload in: %s %d", (const char*)format.getEncoding(), m_payload_id);

		m_decoder = rtx_codec__create_audio_decoder(format.getEncoding(), &format);
		if (m_decoder == nullptr)
		{
			ERROR_MSG("failed to create audio decoder!");
			return false;
		}

		if (m_codec_buffer == nullptr)
		{
			m_codec_buffer = NEW uint8_t[rtp_packet::PAYLOAD_BUFFER_SIZE]{ 0 };
			m_codec_buffer_read = 0;
		}

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_codec_to_pcm_tube_t::send(const INextDataHandler* out_handler, const rtp_packet* packet)
	{
		if (packet == nullptr)
		{
			ERROR_MSG("nothing ti send");
			return;
		}

		if (m_decoder == nullptr)
		{
			ERROR_MSG("decoder is null!");
			return;
		}

		const uint32_t buff_size = rtp_packet::PAYLOAD_BUFFER_SIZE;
		uint8_t buff[buff_size] = { 0 };
		uint32_t payload_length = m_decoder->depacketize(packet, buff, buff_size);
		if (payload_length == 0)
		{
			ERROR_MSG("depacketize fail!");
			return;
		}

		//PLOG_TUBE_WRITE(LOG_PREFIX, "send -- ID(%s): codec_to_pcm send:%d pid:%d.", getName(), payload_length, packet->get_payload_type());

		if ((payload_length + m_codec_buffer_read) >= rtp_packet::PAYLOAD_BUFFER_SIZE)
		{
			WARNING("codec buffer max!");
			payload_length = rtp_packet::PAYLOAD_BUFFER_SIZE - m_codec_buffer_read;
		}

		memcpy(m_codec_buffer + m_codec_buffer_read, buff, payload_length);
		m_codec_buffer_read += payload_length;

		const uint32_t codec_buff_size = rtp_packet::PAYLOAD_BUFFER_SIZE;
		uint8_t codec_buff[codec_buff_size] = { 0 };
		uint32_t decode_size = m_decoder->decode(m_codec_buffer, m_codec_buffer_read, codec_buff, codec_buff_size);
		if (decode_size == 0)
		{
			WARNING("decoder returned error");

			uint32_t frame_size = m_decoder->get_frame_size_pcm();
			if (frame_size < m_codec_buffer_read)
			{
				memcpy(m_codec_buffer, m_codec_buffer + frame_size, m_codec_buffer_read - frame_size);
				m_codec_buffer_read -= frame_size;
			}
			else if (frame_size == m_codec_buffer_read)
			{
				m_codec_buffer_read = 0;
			}

			return;
		}

		m_codec_buffer_read = 0;

		rtp_packet packet_new;
		packet_new.copy_from(packet);
		packet_new.set_payload_type((uint8_t)m_payload_id);
		uint32_t read_size = packet_new.set_payload(codec_buff, decode_size);
		if (read_size == 0)
		{
			ERROR_MSG("read packet fail?");
			return;
		}

		INextDataHandler* out = getNextHandler();
		if (out != nullptr)
		{
			out->send(this, &packet_new);
		}
	}
}
//------------------------------------------------------------------------------
