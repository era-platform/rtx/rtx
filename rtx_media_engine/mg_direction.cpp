﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_context.h"
#include "mg_rx_stream.h"
#include "mg_tx_stream.h"
#include "mg_tube_manager.h"
#include "mg_rfc2833_tube.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define LOG_PREFIX m_tag //"MG-DIR" // Media Gateway Direction

#include "logdef.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	Direction::Direction(MediaContext* owner, uint32_t id, const char* parent_id, DirectionType type) :
		m_owner(owner), m_id(id),
		m_log(owner->getLog()),
		m_type(type),
		m_extraEngine(nullptr)
	{
		m_tubeIdCounter = 0;
		m_name << parent_id << "-d" << id;
		m_processorType = 0;
		strcpy(m_tag, "DIR-BAS");

		rtl::res_counter_t::add_ref(g_mge_direction_counter);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	Direction::~Direction()
	{
		if (m_terminationList.getCount() != 0)
		{
			ERROR_MSG("remained termination!");
			clearTubesForAll();
		}

		removeAllTubes();

		m_terminationList.clear();

		rtl::res_counter_t::release(g_mge_direction_counter);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool Direction::initialize()
	{
		return true;
	}
	//------------------------------------------------------------------------------
	bool Direction::addTermination(Termination* term)
	{
		return m_terminationList.add(term) != BAD_INDEX;
	}
	//------------------------------------------------------------------------------
	void Direction::removeTermination(uint32_t termId)
	{
		for (int i = 0; i < m_terminationList.getCount(); i++)
		{
			Termination* term = m_terminationList.getAt(i);
			if (term->getId() == termId)
			{
				cleanupTerminationTubes(term);
				m_terminationList.removeAt(i);
				PRINT("termination removed");
				break;
			}
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void Direction::updateTopology(const rtl::ArrayT<h248::TopologyTriple>& triple_list)
	{
	}
	void Direction::updateConfSchema(const char* jsonSchema)
	{
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	Termination* Direction::findTermination(uint32_t termId)
	{
		for (int i = 0; i < m_terminationList.getCount(); i++)
		{
			Termination* term = m_terminationList[i];
			if (term->getId() == termId)
				return term;
		}

		return nullptr;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void Direction::cleanupTerminationTubes(Termination* term)
	{
		MediaSession* mg_stream = term->findMediaSession(getProcessorType());
		if (mg_stream == nullptr)
		{
			return;
		}

		PRINT_FMT("cleanup %s termination's tubes", term->getName());

		const rtl::ArrayT<mg_rx_stream_t*>& rx_streams = mg_stream->get_rx_streams();
		for (int i = 0; i < rx_streams.getCount(); i++)
		{
			resetStreamRxTubes(rx_streams[i]);
		}

		const rtl::ArrayT<mg_tx_stream_t*>& tx_streams = mg_stream->get_tx_streams();
		for (int j = 0; j < tx_streams.getCount(); j++)
		{
			mg_tx_stream_t* tx_stream = tx_streams[j];
			tx_stream->lock();
			tx_stream->disconnected_from_tubes();
		}
	}
	//------------------------------------------------------------------------------
	void Direction::resetStreamRxTubes(mg_rx_stream_t* mg_stream)
	{
		mg_stream->lock();

		for (int i = 0; i < m_tubeList.getCount(); i++)
		{
			Tube* tube_at_list = m_tubeList.getAt(i);
			if (tube_at_list == nullptr)
			{
				continue;
			}

			if (mg_stream->unsubscribe_tube(tube_at_list->getId()))
			{
				tube_at_list->unsubscribe(tube_at_list);
			}
		}
	}
	////------------------------------------------------------------------------------
	////
	////------------------------------------------------------------------------------
	//Tube* Direction::create_tube(HandlerType tube_type, mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out)
	//{
	//	Tube* tube = findTube(tube_type, payload_el_in, payload_el_out);

	//	if (tube != nullptr)
	//	{
	//		return tube;
	//	}

	//	m_tubeIdCounter++;

	//	tube = TubeManager::create_tube(tube_type, payload_el_in, payload_el_out, m_name, m_tubeIdCounter);

	//	if (tube == nullptr)
	//	{
	//		m_tubeIdCounter--;
	//		PLOG_ERROR(LOG_PREFIX, "create_tube -- ID(%s): mg_tube_manager__create_tube (%s %s %s) fail.", (const char*)m_name,
	//			HandlerType_toString(tube_type),
	//			payload_el_in ? (const char*)payload_el_in->get_element_name() : "(null)",
	//			payload_el_out ? (const char*)payload_el_out->get_element_name() : "(null)");
	//		return nullptr;
	//	}
	//	else
	//	{
	//		m_tubeList.add(tube);
	//	}

	//	return tube;
	//}
	Tube* Direction::createTube(HandlerType type, mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out)
	{
		ENTER_FMT("type:%s, in:%s, out:%s",
			HandlerType_toString(type),
			payload_el_in ? (const char*)payload_el_in->get_element_name() : "(null)",
			payload_el_out ? (const char*)payload_el_out->get_element_name() : "(null)");

		Tube* tube = Direction::findTube(type, payload_el_in, payload_el_out);

		if (tube == nullptr)
		{

			m_tubeIdCounter++;

			tube = TubeManager::createTube(type, payload_el_in, payload_el_out, m_name, m_tubeIdCounter);

			if (tube == nullptr)
			{
				m_tubeIdCounter--;
				ERROR_FMT("mg_tube_manager__create_tube (%s %s %s) fail.",
					HandlerType_toString(HandlerType::ConfAudioInput),
					payload_el_in ? (const char*)payload_el_in->get_element_name() : "(null)",
					payload_el_out ? (const char*)payload_el_out->get_element_name() : "(null)");
				return nullptr;
			}

			m_tubeList.add(tube);
		}

		return tube;
	}

	//------------------------------------------------------------------------------
	Tube* Direction::findTube(HandlerType tube_type, mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out)
	{
		if (payload_el_in == nullptr && payload_el_out == nullptr)
		{
			return nullptr;
		}

		if (m_tubeList.getCount() == 0)
		{
			return nullptr;
		}

		for (int i = 0; i < m_tubeList.getCount(); i++)
		{
			Tube* tube_at_list = m_tubeList.getAt(i);
			if (tube_at_list == nullptr)
			{
				continue;
			}

			if (!tube_at_list->free())
			{
				continue;
			}

			if (tube_at_list->getType() != tube_type)
			{
				continue;
			}

			if (tube_at_list->is_suited(payload_el_in, payload_el_out))
			{
				return tube_at_list;
			}
		}

		return nullptr;
	}
	//------------------------------------------------------------------------------
	void Direction::releaseTube(uint32_t tube_id)
	{
		if (m_tubeList.getCount() == 0)
		{
			WARNING("tube list is empty!");
			return;
		}

		bool find = false;

		for (int i = 0; i < m_tubeList.getCount(); i++)
		{
			Tube* tube_at_list = m_tubeList.getAt(i);
			if (tube_at_list == nullptr)
			{
				WARNING_FMT("tube with index: %d is null.", i);
				continue;
			}

			if (tube_id == tube_at_list->getId())
			{
				//TubeManager::clear_tube(this, tube_at_list);
				tube_at_list->release();
				for (int j = 0; j < m_tubeList.getCount(); j++)
				{
					Tube* tube_at_list2 = m_tubeList.getAt(j);
					if ((tube_at_list2 != nullptr) && (tube_at_list2->isNextHandler(tube_at_list)))
					{
						tube_at_list2->setNextHandler(nullptr);
					}
				}
				m_tubeList.removeAt(i);
				find = true;
				PRINT_FMT("tube with id: %d released.", tube_id);
				break;
			}
		}

		if (!find)
		{
			ERROR_FMT("tube with id: %d not found.", tube_id);
			return;
		}
	}
	//------------------------------------------------------------------------------
	void Direction::removeAllTubes()
	{
		ENTER();

		for (int i = 0; i < m_tubeList.getCount(); i++)
		{
			Tube* tube = m_tubeList.getAt(i);
			tube->release();
			tube->unsubscribe(tube); // ?
		}

		m_tubeList.clear();

		PRINT("all tubes removed");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool Direction::customizeStreamRx(mg_rx_stream_t* mg_stream_rx, Tube* tube)
	{
		if ((tube == nullptr) || (mg_stream_rx == nullptr))
		{
			ERROR_MSG("wrong params!");
			return false;
		}

		mg_stream_rx->subscribe_tube(tube);

		if (tube->getType() == HandlerType::DTMF2833)
		{
			((mg_rfc2833_tube_t*)tube)->set_stream(mg_stream_rx);
		}

		RETURN_FMT(true, "stream:%s --> tube:%s", mg_stream_rx->getName(), tube->getName());
	}
	//------------------------------------------------------------------------------
	bool Direction::customizeStreamTx(Tube* tube, mg_tx_stream_t* mg_stream_tx)
	{
		ENTER();

		if ((tube == nullptr) || (mg_stream_tx == nullptr))
		{
			ERROR_MSG("wrong params");
			return false;
		}

		if (tube->setNextHandler(mg_stream_tx))
		{
			mg_stream_tx->mark_tube(tube);
		}
		else
		{
			tube->release();

			ERROR_FMT("tube:%s -/- stream:%s", tube->getName(), mg_stream_tx->getName());
			return false;
		}

		mg_stream_tx->reset_before_new_connect();

		RETURN_FMT(true, "tube:%s --> stream:%s", tube->getName(), mg_stream_tx->getName());
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void Direction::checkTerminationProxy(uint32_t termId)
	{
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void Direction::clearTubesForAll()
	{
		for (int i = 0; i < m_terminationList.getCount(); i++)
		{
			cleanupTerminationTubes(m_terminationList[i]);
		}
	}
}
//------------------------------------------------------------------------------
