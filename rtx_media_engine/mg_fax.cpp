﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "mg_fax.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
DECLARE rtl::Logger* g_mg_fax_log = nullptr;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define LOG_PREFIX "MGF"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void span_message(int level, const char *msg)
{
	if (msg == nullptr)
		return;

	rtl::String mes(msg);
	mes.trim();

	rtl::Logger* log = g_mg_fax_log;
	if (log != nullptr)
	{
		if (level == SPAN_LOG_ERROR)
			log->log_error(LOG_PREFIX, "span_message : fax error(%d): %s", level, (const char*)mes);
		else if (level == SPAN_LOG_WARNING)
			log->log_warning(LOG_PREFIX, "span_message : fax warning(%d): %s", level, (const char*)mes);
		else if (rtl::Logger::check_trace(TRF_FAX))
			log->log(LOG_PREFIX, "span_message : fax info(%d): %s", level, (const char*)mes);
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void span_error(const char *msg)
{
	if (msg == nullptr)
		return;

	rtl::String mes(msg);
	mes.trim();

	rtl::Logger* log = g_mg_fax_log;
	if (log != nullptr)
	{
		log->log_error(LOG_PREFIX, "span_error : fax error: %s", (const char*)mes);
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const char* mg_get_fax_signal_name(int signal)
{
	const char* names[] =
	{
		"FAX_SIGNAL_ON","FAX_SIGNAL_DESTROED", "FAX_SIGNAL_RX_ERROR", "FAX_SIGNAL_TX_ERROR",
		"FAX_SIGNAL_RX_DONE", "FAX_SIGNAL_TX_DONE",
	};

	return signal >= 0 && signal < (int)(sizeof(names) / sizeof(const char*)) ? names[signal] : "FAX_UNKNOWN";
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
mg_fax_t::mg_fax_t(rtl::Logger* log)
	: media::Metronome(*log), m_log(log)
{
	m_fax_notified = false;
	m_handler = nullptr;

	m_lock.clear();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
mg_fax_t::~mg_fax_t()
{
	destroy();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mg_fax_t::set_fax_handler(mg_fax_handler* handler)
{
	if (lock())
	{
		m_handler = handler;
		unlock();
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mg_fax_t::destroy()
{
	metronomeStop();

	if (lock())
	{
		m_handler = nullptr;
		unlock();
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
FAX_SIGNALS mg_fax_t::get_fax_signal_state(bool stop_signaling)
{
	return FAX_SIGNAL_OFF;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mg_fax_t::fax_signal(int signal)
{
	PLOG_FAX(LOG_PREFIX, "fax signal %s", mg_get_fax_signal_name(signal));

	if ((signal == FAX_SIGNAL_RX_ERROR) || (signal == FAX_SIGNAL_TX_ERROR))
	{
		if (m_handler != nullptr)
		{
			m_handler->fax_end(true);
		}
	}
	else if ((signal == FAX_SIGNAL_RX_DONE) || (signal == FAX_SIGNAL_TX_DONE))
	{
		if (m_handler != nullptr)
		{
			m_handler->fax_end(false);
		}
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtl::Logger* mg_fax_t::get_logger()
{
	return m_log;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool mg_fax_t::fax_notified()
{
	return m_fax_notified;
}
//------------------------------------------------------------------------------
void mg_fax_t::set_fax_notified(bool notified)
{
	m_fax_notified = notified;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool mg_fax_t::lock()
{
	uint32_t time_diff = 0;
	while (time_diff <= 3000)
	{
		if (m_lock.test_and_set(std::memory_order_acquire))
		{
			rtl::Thread::sleep(5);
			time_diff += 5;
			continue;
		}

		return true;
	}

	return false;
}
//------------------------------------------------------------------------------
void mg_fax_t::unlock()
{
	m_lock.clear(std::memory_order_release);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mg_fax_t::metronomeTicks()
{
	short buff[160] = { 0 };

	uint32_t read = read_fax((uint8_t*)buff, sizeof(short) * 160);
	if ((m_handler != nullptr) && (read != 0))
	{
		m_handler->send((const char*)&buff, read);
	}

	unlock();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
