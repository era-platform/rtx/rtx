/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "srtp_crypto.h"
#include "mg_sdp_candidate.h"

#define SDP_ICE_CANDIDATE_PREF_HOST			126
#define SDP_ICE_CANDIDATE_PREF_SRFLX		100
#define SDP_ICE_CANDIDATE_PREF_PRFLX		110
#define SDP_ICE_CANDIDATE_PREF_RELAY		0

#define SDP_ICE_CANDIDATE_FOUND_SIZE_PREF	8

namespace mge
{
	//--------------------------------------
	//
	//--------------------------------------
	sdp_ice_candidate_t::sdp_ice_candidate_t() : m_foundation(nullptr), m_component_id(1), m_transport_type(sdp_ice_transport_udp_e),
		m_priority(0), m_cand_type(sdp_ice_candidate_host_e), m_conn_port(0)
	{
	}
	//--------------------------------------
	//
	//--------------------------------------
	sdp_ice_candidate_t::sdp_ice_candidate_t(const sdp_ice_candidate_t& candidate) : m_foundation(nullptr)
	{
		m_foundation = rtl::strdup(candidate.m_foundation);
		m_component_id = candidate.m_component_id;
		m_transport_type = candidate.m_transport_type;
		m_priority = candidate.m_priority;
		m_cand_type = candidate.m_cand_type;
		m_conn_address.copy_from(&candidate.m_conn_address);
		m_conn_port = candidate.m_conn_port;

		for (int i = 0; i < candidate.m_ice_params.getCount(); i++)
		{
			const sdp_ice_param_t& param = candidate.m_ice_params.getAt(i);
			add_ice_param(param.name, param.value);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	sdp_ice_candidate_t::~sdp_ice_candidate_t()
	{
		if (m_foundation != nullptr)
		{
			FREE(m_foundation);
			m_foundation = nullptr;
		}

		for (int i = 0; i < m_ice_params.getCount(); i++)
		{
			sdp_ice_param_t& param = m_ice_params.getAt(i);

			if (param.name)
			{
				FREE(param.name);
			}

			if (param.value)
			{
				FREE(param.value);
			}
		}

		m_ice_params.clear();
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool sdp_ice_candidate_t::parse(const char* sdp_line)
	{
		if (sdp_line == nullptr || sdp_line[0] == 0)
		{
			return false;
		}

		const char *cp;
		if ((cp = strchr(sdp_line, ':')) == nullptr)
			return false;
		sdp_line = cp + 1;

		// foundation
		if ((cp = strchr(sdp_line, ' ')) == nullptr)
			return false;

		if (m_foundation != nullptr)
			FREE(m_foundation);
		m_foundation = rtl::strdup(sdp_line, int(cp - sdp_line));

		// component id
		sdp_line = cp + 1;
		if ((cp = strchr(sdp_line, ' ')) == nullptr)
			return false;

		m_component_id = strtoul(sdp_line, nullptr, 10);

		// transport
		sdp_line = cp + 1;
		if ((cp = strchr(sdp_line, ' ')) == nullptr)
			return false;

		m_transport_type = get_sdp_ice_transport_type(sdp_line, int(cp - sdp_line));

		// priority
		sdp_line = cp + 1;
		if ((cp = strchr(sdp_line, ' ')) == nullptr)
			return false;
		m_priority = strtoul(sdp_line, nullptr, 10);

		// conn-address
		sdp_line = cp + 1;
		if ((cp = strchr(sdp_line, ' ')) == nullptr)
			return false;
		m_conn_address.parse(sdp_line);
		if (m_conn_address.empty())
		{
			return false;
		}

		// conn-port
		sdp_line = cp + 1;
		if ((cp = strchr(sdp_line, ' ')) == nullptr)
			return false;
		m_conn_port = (uint16_t)strtoul(sdp_line, nullptr, 10);

		// cand type
		sdp_line = cp + 1;
		if ((cp = strchr(sdp_line, ' ')) == nullptr)
			return false;

		if (std_strnicmp(sdp_line, "typ ", 4) != 0)
			return false;
		sdp_line = cp + 1;
		cp = strchr(sdp_line, ' ');

		m_cand_type = get_sdp_ice_candidate_type(sdp_line, int(cp != nullptr ? cp - sdp_line : -1));

		if (cp == nullptr)
			return true;

		// read optional extended parameters
		sdp_line = cp + 1;

		while ((cp = strchr(sdp_line, ' ')) != nullptr)
		{
			sdp_ice_param_t param;
			param.name = nullptr;
			param.value = nullptr;

			param.name = rtl::strdup(sdp_line, int(cp - sdp_line));
			sdp_line = cp + 1;

			cp = strchr(sdp_line, ' ');
			bool end = (cp == nullptr);
			param.value = end ? rtl::strdup(sdp_line) : rtl::strdup(sdp_line, int(cp - sdp_line));
			m_ice_params.add(param);

			if (end)
			{
				break;
			}
			else
			{
				sdp_line = cp + 1;
			}
		}

		return true;
	}
	//--------------------------------------
	// ������
	//   a=candidate:foundation SP component-id SP transport SP priority SP connection-address SP port SP cand-type [SP rel-addr] [SP rel-port]
	//   a=candidate:1 1 UDP 2130706431 10.0.1.1 8998 typ host
	//   a=candidate:2 1 UDP 1694498815 192.0.2.3 45664 typ srflx raddr 10.0.1.1 rport 8998
	//--------------------------------------
	int sdp_ice_candidate_t::write_to(char* buffer, int size)
	{
		int len = std_snprintf(buffer, size, "%s %u %s %u %s %u %s",
			m_foundation != nullptr ? m_foundation : "12345678",
			m_component_id,
			get_sdp_ice_transport_type_name(m_transport_type),
			m_priority,
			m_conn_address.to_string(),
			m_conn_port,
			get_sdp_ice_candidate_type_name(m_cand_type));

		for (int i = 0; i < m_ice_params.getCount(); i++)
		{
			sdp_ice_param_t& param = m_ice_params[i];

			len += std_snprintf(buffer + len, size - len, " %s %s", param.name, param.value);
		}

		buffer[len] = 0;

		return len;
	}
	//--------------------------------------
	//
	//--------------------------------------
	sdp_ice_candidate_t& sdp_ice_candidate_t::operator = (const sdp_ice_candidate_t& candidate)
	{
		if (m_foundation != nullptr)
		{
			FREE(m_foundation);
		}
		m_foundation = rtl::strdup(candidate.m_foundation);

		m_component_id = candidate.m_component_id;
		m_transport_type = candidate.m_transport_type;
		m_priority = candidate.m_priority;
		m_cand_type = candidate.m_cand_type;
		m_conn_address.copy_from(&candidate.m_conn_address);
		m_conn_port = candidate.m_conn_port;

		for (int i = 0; i < m_ice_params.getCount(); i++)
		{
			sdp_ice_param_t& param = m_ice_params.getAt(i);

			if (param.name)
				FREE(param.name);
			if (param.value)
				FREE(param.value);
		}

		m_ice_params.clear();

		for (int i = 0; i < candidate.m_ice_params.getCount(); i++)
		{
			const sdp_ice_param_t& param = candidate.m_ice_params.getAt(i);
			add_ice_param(param.name, param.value);
		}

		return *this;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void sdp_ice_candidate_t::set_foundation(const char* foundation)
	{
		if (m_foundation != nullptr)
		{
			FREE(m_foundation);
			m_foundation = nullptr;
		}

		m_foundation = rtl::strdup(foundation);
	}
	//--------------------------------------
	//
	//--------------------------------------
	const char* sdp_ice_candidate_t::generate_foundation()
	{
		if (m_foundation != nullptr)
		{
			FREE(m_foundation);
			m_foundation = nullptr;
		}

		return m_foundation = compute_foundation();
	}
	//--------------------------------------
	//
	//--------------------------------------
	void sdp_ice_candidate_t::add_ice_param(const char* name, const char* value)
	{
		if (name == nullptr || name[0] == 0 || value == nullptr || value[0] == 0)
			return;

		sdp_ice_param_t param;

		param.name = rtl::strdup(name);
		param.value = rtl::strdup(value);

		m_ice_params.add(param);
	}
	//--------------------------------------
	// static utilites
	//--------------------------------------
	uint32_t sdp_ice_candidate_t::generate_priority(sdp_ice_candidate_type_t type, uint16_t local_pref, bool is_rtp)
	{
		uint32_t pref;

		switch (type)
		{
		case sdp_ice_candidate_host_e: pref = SDP_ICE_CANDIDATE_PREF_HOST; break;
		case sdp_ice_candidate_srflx_e: pref = SDP_ICE_CANDIDATE_PREF_SRFLX; break;
		case sdp_ice_candidate_prflx_e: pref = SDP_ICE_CANDIDATE_PREF_PRFLX; break;
		case sdp_ice_candidate_relay_e: default: pref = SDP_ICE_CANDIDATE_PREF_RELAY; break;
		}

		return (pref << 24) +
			(local_pref << 8) +
			((256 - (is_rtp ? SDP_ICE_CANDIDATE_RTP : SDP_ICE_CANDIDATE_RTCP)) << 0);
	}
	//--------------------------------------
	//
	//--------------------------------------
	char* sdp_ice_candidate_t::compute_foundation()
	{
		char temp[16];
		uint32_t foundation_value;

		foundation_value = rand();
		if (rand() != 0)
		{
			foundation_value = rand() | (rand() << 16);
		}

		int len = std_snprintf(temp, 15, "%u", foundation_value);
		temp[len] = 0;

		return rtl::strdup(temp);
	}
	//--------------------------------------
	//
	//--------------------------------------
	static const char ice_chars[] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'k', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
									 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'K', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
									 '0','1', '2', '3', '4', '5', '6', '7', '8', '9' }; // /!\do not add '/' and '+' because of WebRTC password

	static const int ice_chars_count = sizeof(ice_chars);
	//--------------------------------------
	// Abramov 12.02.2015 for test vp8 �������-�������
	//--------------------------------------
	void sdp_ice_candidate_t::generate_hex_text(char* tmp, uint32_t size)
	{
		uint32_t i;
		for (i = 0; i < size - 1; ++i)
		{
			tmp[i] = ice_chars[(rand() ^ rand()) % ice_chars_count];
		}

		tmp[i] = '\0';
	}
	//--------------------------------------
	//
	//--------------------------------------
	char* sdp_ice_candidate_t::generate_ufrag()
	{
		char tmp[SDP_ICE_UFRAG_SIZE + 1];

		int i;
		int chars = (sizeof(tmp) / sizeof(tmp[0])) - 1;
		for (i = 0; i < chars; ++i)
		{
			tmp[i] = ice_chars[(rand() ^ rand()) % ice_chars_count];
		}

		tmp[i] = '\0';

		return rtl::strdup(tmp);
	}
	//--------------------------------------
	//
	//--------------------------------------
	char* sdp_ice_candidate_t::generate_pwd()
	{
		char tmp[SDP_ICE_PWD_SIZE + 1];
		int i;
		int chars = (sizeof(tmp) / sizeof(tmp[0])) - 1;
		for (i = 0; i < chars; ++i)
		{
			tmp[i] = ice_chars[(rand() ^ rand()) % ice_chars_count];
		}
		tmp[i] = '\0';

		return rtl::strdup(tmp);
	}
	//--------------------------------------
	//
	//--------------------------------------
	const char* get_sdp_ice_candidate_type_name(sdp_ice_candidate_type_t type)
	{
		static const char* cand_names[] = { "typ host", "typ srflx", "typ prflx", "typ relay", "typ unknown" };
		return type >= 0 && type < sdp_ice_candidate_unknown_e ? cand_names[type] : cand_names[sdp_ice_candidate_unknown_e];
	}
	//--------------------------------------
	//
	//--------------------------------------
	sdp_ice_candidate_type_t get_sdp_ice_candidate_type(const char* line, int len)
	{
		if (len == -1)
			len = int(strlen(line));

		sdp_ice_candidate_type_t type = sdp_ice_candidate_unknown_e;

		if (len == 4 && std_strnicmp(line, "host", 4) == 0)
			type = sdp_ice_candidate_host_e;
		else if (len == 5)
		{
			if (std_strnicmp(line, "srflx", 5) == 0)
				type = sdp_ice_candidate_srflx_e;
			else if (std_strnicmp(line, "prflx", 5) == 0)
				type = sdp_ice_candidate_prflx_e;
			else if (std_strnicmp(line, "relay", 5) == 0)
				type = sdp_ice_candidate_relay_e;
		}

		return type;
	}
	//--------------------------------------
	//
	//--------------------------------------
	const char* get_sdp_ice_transport_type_name(sdp_ice_transport_type_t type)
	{
		static const char* trans_names[] = { "UDP", "TCP", "TLS", "WS", "WSS", "INVALID" };
		return type >= 0 && type < sdp_ice_transport_invalid_e ? trans_names[type] : trans_names[sdp_ice_transport_invalid_e];
	}
	//--------------------------------------
	//
	//--------------------------------------

#define UDP_STAMP	0x00504455
#define TCP_STAMP	0x00504354
#define TLS_STAMP	0x00534C54
#define WS_STAMP	0x00005357
#define WSS_STAMP	0x00535357

	sdp_ice_transport_type_t get_sdp_ice_transport_type(const char* line, int len)
	{
		if (len == -1)
			len = int(strlen(line));

		sdp_ice_transport_type_t type = sdp_ice_transport_invalid_e;

		if (len < 4)
		{
			char tname[4] = { 0 };
			memcpy(tname, line, len);
			rtl::strupr(tname);

			uint32_t stamp = *(uint32_t*)tname;

			if (stamp == UDP_STAMP)
				type = sdp_ice_transport_udp_e;
			else if (stamp == TCP_STAMP)
				type = sdp_ice_transport_tcp_e;
			else if (stamp == TLS_STAMP)
				type = sdp_ice_transport_tls_e;
			else if (stamp == WS_STAMP)
				type = sdp_ice_transport_ws_e;
			else if (stamp == WSS_STAMP)
				type = sdp_ice_transport_wss_e;
			// else invalid
		}

		return type;
	}
}
//--------------------------------------
