/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "mg_audio_analyzer.h"

//-----------------------------------------------
//
//-----------------------------------------------
#if TRACE_VAD
#define SLOG_VAD  s_log.log // if (rtl::Logger::check_trace(TRF_VAD))
#endif

namespace mge {

	volatile uint32_t AudioAnalyzer::s_idGen = 0;
	#if TRACE_VAD
		rtl::Logger AudioAnalyzer::s_log;
		bool AudioAnalyzer::s_logInitialized = false;
	#endif
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VadDetector::~VadDetector()
	{
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	AudioAnalyzer::AudioAnalyzer(rtl::Logger* log) : m_log(log), m_handler(nullptr)
	{
		#if TRACE_VAD
		if (!s_logInitialized)
		{
			s_logInitialized = true;
			const char* folder = Log.get_folder();
			s_log.create(folder, "vad", LOG_APPEND);
		}
		#endif
		static int cnt = 0;

		m_id = std_interlocked_inc(&s_idGen);

		#if TRACE_VAD
			SLOG_VAD("VAD", "%5u : detector created.(%d)", m_id, ++cnt);
		#endif

		PLOG_WRITE("VAD", "%5u : detector created.(%d)", m_id, cnt);

		m_params.threshold = 40;
		m_params.vadDuration = 30;
		m_params.silenceDuration = 500;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	AudioAnalyzer::~AudioAnalyzer()
	{
		cleanup();

		static int cnt = 0;

		#if TRACE_VAD
			SLOG_VAD("VAD", "%5u : detector destroyed.(%d)", m_id, ++cnt);
		#endif

		PLOG_WRITE("VAD", "%5u : detector destroyed.(%d)", m_id, cnt);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool AudioAnalyzer::initialize(VadDetector* handler, const AudioAnalyzerParams& params)
	{
		m_handler = handler;
		m_params = params;

		return true;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void AudioAnalyzer::cleanup()
	{
		rtl::MutexLock lock(m_sync);

		for (int i = 0; i < m_detectorList.getCount(); i++)
		{
			AudioDetector& detector = m_detectorList.getAt(i);

			if (detector.detector != nullptr)
			{
				DELETEO(detector.detector);
			}
		}

		m_detectorList.clear();
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void AudioAnalyzer::send(const INextDataHandler* sender, const rtp_packet* packet)
	{
		Tube* tube = (Tube*)sender;
		uint32_t termTag = (uint32_t)tube->get_tag();

		rtl::MutexLock lock(m_sync);

		AudioDetector* detector = getDetector(termTag);

		if (detector != nullptr)
		{
			bool vad;
			int duration;
			bool raise_event;

			uint32_t ts = packet->getTimestamp();


			if (packet->get_payload_type() == 13)
			{
				#if TRACE_VAD
					SLOG_VAD("VAD", "%5u : CN received .....%u", m_id, tube->get_id());
				#endif

				int sample_count = detector->lastTsInited ? ts - detector->lastTs : 20 * 8;
				raise_event = detector->detector->addSilence(sample_count, vad, duration);
			}
			else
			{
				#if TRACE_VAD
					//SLOG_VAD("VAD", "%5u : data received .....%u", m_id, tube->get_id());
				#endif
				raise_event = detector->detector->addPacket((const short*)packet->get_payload(), packet->get_payload_length() / sizeof(short), vad, duration);
			}

			if (m_handler != nullptr && raise_event)
			{
				PLOG_WRITE("VAD", "%5u : Term %u ----------> event %s (duration %d msec)", m_id, termTag, vad ? "VAD" : "Silence", duration);
#if TRACE_VAD
				SLOG_VAD("VAD", "%5u : Term %u ----------> event %s (duration %d msec)", m_id, termTag, vad ? "VAD" : "Silence", duration);
#endif
				m_handler->VadDetector__audioSignalChanged(termTag, vad, duration);
			}

			detector->lastTs = ts;
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void AudioAnalyzer::send(const INextDataHandler* sender, const uint8_t* data, uint32_t len)
	{
		//...
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void AudioAnalyzer::send(const INextDataHandler* sender, const rtcp_packet_t* packet)
	{
		//...
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool AudioAnalyzer::unsubscribe(const INextDataHandler* sender)
	{
		return true;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void AudioAnalyzer::updateParams(const AudioAnalyzerParams& params)
	{
		m_params = params;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	AudioAnalyzer::AudioDetector* AudioAnalyzer::getDetector(uintptr_t termTag)
	{
		for (int i = 0; i < m_detectorList.getCount(); i++)
		{
			if (m_detectorList[i].termId == termTag)
				return &m_detectorList[i];
		}

		AudioDetector detector = { termTag, NEW media::VAD_Detecter(
			#if TRACE_VAD
					&s_log
			#else
					nullptr
			#endif
				) };

		detector.detector->create(8000, m_params.threshold, m_params.vadDuration, m_params.silenceDuration);

		m_detectorList.add(detector);

		return &m_detectorList.getLast();
	}
}
//-----------------------------------------------
