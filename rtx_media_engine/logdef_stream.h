#pragma once

#define LOG_PREFIX getTag()

#if defined TARGET_OS_WINDOWS

#define ENTER() \
	PLOG_STREAM_WRITE(LOG_PREFIX,"ID(%s) : %s()...", getName(), __func__);

#define ENTER_FMT(fmt, ...) \
	PLOG_STREAM_WRITE(LOG_PREFIX,"ID(%s) : %s(" fmt ")...", getName(), __func__, __VA_ARGS__);

#define RETURN_BOOL(res) \
	PLOG_STREAM_WRITE(LOG_PREFIX,"ID(%s) : %s() -> %s", getName(), __func__, STR_BOOL(res)); \
	return res;

#define RETURN_STR(res) \
	PLOG_STREAM_WRITE(LOG_PREFIX,"ID(%s) : %s() -> %s", getName(), __func__, res); \
	return res;

#define RETURN_INT(res) \
	PLOG_STREAM_WRITE(LOG_PREFIX,"ID(%s) : %s() -> %ll", getName(), __func__, res); \
	return res;

#define LEAVE() \
	PLOG_STREAM_WRITE(LOG_PREFIX,"ID(%s) : %s() exit", getName(), __func__); \
	return;

#define RETURN_FMT(res, fmt, ...) \
	PLOG_STREAM_WRITE(LOG_PREFIX,"ID(%s) : %s() -> " fmt, getName(), __func__, __VA_ARGS__); \
	return res;

#define WARNING(w) \
	PLOG_STREAM_WARNING(LOG_PREFIX,"ID(%s) : %s() : %s", getName(), __func__, w)

#define WARNING_FMT(fmt, ...) \
	PLOG_STREAM_WARNING(LOG_PREFIX,"ID(%s) : %s() : " fmt, getName(), __func__, __VA_ARGS__)

#define ERROR_MSG(e) \
	PLOG_STREAM_ERROR(LOG_PREFIX,"ID(%s) : %s() : %s", getName(), __func__, e)

#define ERROR_FMT(fmt, ...) \
	PLOG_STREAM_ERROR(LOG_PREFIX,"ID(%s) : %s() : " fmt, getName(), __func__, __VA_ARGS__)

#define PRINT(t) \
	PLOG_STREAM_WRITE(LOG_PREFIX,"ID(%s) : %s() : %s", getName(), __func__, t)

#define PRINT_FMT(fmt, ...) \
	PLOG_STREAM_WRITE(LOG_PREFIX, "ID(%s) : %s() : " fmt, getName(), __func__, __VA_ARGS__)

#else

#define ENTER() \
	PLOG_STREAM_WRITE(LOG_PREFIX,"ID(%s) : %s()...", getName(), __func__);

#define ENTER_FMT(fmt, ...) \
	PLOG_STREAM_WRITE(LOG_PREFIX,"ID(%s) : %s(" fmt ")...", getName(), __func__, __VA_ARGS__);

#define LEAVE() \
	PLOG_STREAM_WRITE(LOG_PREFIX,"ID(%s) : %s() exit", getName(), __func__); \
	return;

#define RETURN_BOOL(res) \
	PLOG_STREAM_WRITE(LOG_PREFIX,"ID(%s) : %s() -> %s", getName(), __func__, STR_BOOL(res)); \
	return res;

#define RETURN_STR(res) \
	PLOG_STREAM_WRITE(LOG_PREFIX,"ID(%s) : %s() -> %s", getName(), __func__, res); \
	return res;

#define RETURN_INT(res) \
	PLOG_STREAM_WRITE(LOG_PREFIX,"ID(%s) : %s() -> %ll", getName(), __func__, res); \
	return res;

#define EXIT() \
	PLOG_STREAM_WRITE(LOG_PREFIX,"ID(%s) : %s() exit", getName(), __func__); \
	return;

#define RETURN_FMT(res, fmt, ...) \
	PLOG_STREAM_WRITE(LOG_PREFIX,"ID(%s) : %s() -> " fmt, getName(), __func__, __VA_ARGS__); \
	return res;

#define WARNING(w) \
	PLOG_STREAM_WARNING(LOG_PREFIX,"ID(%s) : %s() : %s", getName(), __func__, w)

#define WARNING_FMT(fmt, ...) \
	PLOG_STREAM_WARNING(LOG_PREFIX,"ID(%s) : %s() : " fmt, getName(), __func__, __VA_ARGS__)

#define ERROR_MSG(e) \
	PLOG_STREAM_ERROR(LOG_PREFIX,"ID(%s) : %s() : %s", getName(), __func__, e)

#define ERROR_FMT(fmt, ...) \
	PLOG_STREAM_ERROR(LOG_PREFIX,"ID(%s) : %s() : " fmt, getName(), __func__, __VA_ARGS__)

#define PRINT(t) \
	PLOG_STREAM_WRITE(LOG_PREFIX,"ID(%s) : %s() : %s", getName(), __func__, t)

#define PRINT_FMT(fmt, ...) \
	PLOG_STREAM_WRITE(LOG_PREFIX, "ID(%s) : %s() : " fmt, getName(), __func__, __VA_ARGS__)

#endif
