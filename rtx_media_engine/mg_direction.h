﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "mg_termination.h"
#include "mg_tube.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//#define PLOG_WRITE		PLOG_MEDIA_WRITE
//#define PLOG_WARNING		PLOG_MEDIA_WARNING
//#define PLOG_ERROR		PLOG_MEDIA_ERROR

#define STREAM_AUDIO 1
#define STREAM_VIDEO 2
#define STREAM_IMAGE 3

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	class MediaContext;
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	enum class DirectionType
	{
		bothway,
		conference
	};
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	static inline const char* Direction_toString(DirectionType dir) { return dir == DirectionType::bothway ? "bothway" : "conference"; }
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	class Direction
	{
		friend class TubeManager;
	public:
		Direction(MediaContext* owner, uint32_t id, const char* parent_id, DirectionType type);
		virtual ~Direction();

		uint32_t getId() { return m_id; }
		const char*	getName() const { return m_name; }
		DirectionType getType() { return m_type; }
		// указывает к какому процессору принадлежит дирекшн.
		void setProcessorType(uint32_t ptype) { m_processorType = ptype; }
		const uint32_t getProcessorType() const { return m_processorType; }

		void setExtraEngine(void* engine) { m_extraEngine = engine; }
		void* getExtraEngine() { return m_extraEngine; }

		int getTerminationCount() { return m_terminationList.getCount(); }
		Termination* getTermination(int index) { return m_terminationList[index]; }

		bool containsTermination(uint32_t termId) { return findTermination(termId) != nullptr; }
		Termination* findTermination(uint32_t termId);

		virtual bool initialize();
		virtual bool addTermination(Termination* term);
		virtual void removeTermination(uint32_t termId);
		virtual void checkTerminationProxy(uint32_t termId);

		virtual void updateTopology(const rtl::ArrayT<h248::TopologyTriple>& triple_list);
		virtual void updateConfSchema(const char* jsonSchema);

		void setRecordPath(const char* path) { m_pathRecord = path; }

		// internal
		void releaseTube(uint32_t tube_id);
		// moved from protected to public, especially for the stupid gcc compiler
		Tube* createTube(HandlerType type, mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out);

	private:
		// private routines
		virtual int checkTermination(Termination* term) = 0;

		void clearTubesForAll();
		void removeAllTubes();

		Tube* findTube(HandlerType tube_type, mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out);
		// сбрасываем указатели тубов из стрима.
		void resetStreamRxTubes(mg_rx_stream_t* mg_stream);

	protected:
		//
		void cleanupTerminationTubes(Termination* term);
		// связываем стрим с тубом. (это голова цепочки тубов)
		bool customizeStreamRx(mg_rx_stream_t* mg_stream_rx, Tube* tube);
		// связываем туб со стримом. (хвост тубов)
		bool customizeStreamTx(Tube* tube, mg_tx_stream_t* mg_stream_tx);

	protected:
		char m_tag[9];
		rtl::Logger* m_log;
		MediaContext* m_owner;
		rtl::String m_name;
		DirectionType m_type;
		void* m_extraEngine;
		rtl::String m_pathRecord;		// used by conferencec

	//private:
		uint32_t m_processorType;
		uint32_t m_id;
		uint32_t m_tubeIdCounter;
		TerminationList m_terminationList;
		TubeList m_tubeList;
	};
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	typedef rtl::PonterArrayT<Direction> DirectionList;
}
//------------------------------------------------------------------------------
