﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_tube.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	struct mg_element_chain_tube;
	struct mg_elements_changes_info;
	struct mg_splitter_params_t;
	//------------------------------------------------------------------------------
	// Отвечает за создание нужного туба и все что связано с ним.
	//------------------------------------------------------------------------------
	class TubeManager
	{
	public:
		static void setLogger(rtl::Logger* log);

		// Создаем туб с конкретным типом. Передаем входящий пайлоад элемент и выходящий (для кодеков с разными paylaod_id).
		// Передаем идетификатор объекта из которого создается туб и порядковый номер туба.
		static Tube* createTube(HandlerType h_type, mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out,
			const char* parent_id, uint32_t tube_number);

		//static const char* handler_type_to_string(HandlerType h_type);

		// Настраиваем цепочку тубов для элементов дирекшина.
		// tube_in и tube_out создаются внутри метода и помещаются в дирекшн.
		// Флаги clear_in и clear_out определяют очистку тубов ин и аут в случае неверного построения цепочки.
		// (т.к. иногда туб ин(аут) может использоватся в нескольких цепочках)
		static bool prepare_tubes_chain(mge::Direction* direction, mg_media_element_t* element_in, mg_media_element_t* element_out,
			Tube** tube_in, Tube** tube_out, bool clear_in, bool clear_out);

		//static bool change_conference_topology(mge::Direction* direction, Tube* tube_from, Tube* tube_to, h248::TopologyDirection topology);
		//static bool change_conference_topology_from(mge::Direction* direction, Tube* tube_from, const char* id, h248::TopologyDirection topology);
		//static bool change_conference_topology_to(mge::Direction* direction, Tube* tube_to, const char* id, h248::TopologyDirection topology);

	private:
		static bool prepare_audio_tubes_chain(mge::Direction* direction, mg_media_element_t* element_in, mg_media_element_t* element_out,
			Tube** tube_in, Tube** tube_out, bool clear_in, bool clear_out);
		static bool prepare_video_tubes_chain(mge::Direction* direction, mg_media_element_t* element_in, mg_media_element_t* element_out,
			Tube** tube_in, Tube** tube_out, bool clear_in, bool clear_out);
		static bool prepare_chain(mge::Direction* direction, rtl::ArrayT<mg_element_chain_tube*>* chain);
		static bool create_tube_in_direction(mge::Direction* direction, mg_element_chain_tube* chain_element);
		// Простая очистка списка. Обнуляются указатели и удаляется элемент цепочки.
		// Вызывается в конце настройки цепи.
		static void clear_chain(rtl::ArrayT<mg_element_chain_tube*>* chain);
		// Список очищается удаляя и отписывая тубы. Так же удаляются элементы цепочки.
		// Вызывается при ошибки.
		static void clear_chain(mge::Direction* direction, rtl::ArrayT<mg_element_chain_tube*>* chain, bool clear_out);
		static void release_tube_in_direction(mge::Direction* direction, mg_element_chain_tube* chain_element);
		static void release_tube_in_direction(mge::Direction* direction, Tube* tube);

		static int calculate_splitter_location(mg_media_element_t* element_in, mg_media_element_t* element_out);
		static bool prepare_splitter_tubes_chain(mge::Direction* direction, mg_media_element_t* element_in, mg_media_element_t* element_out, Tube* splitter_tube);
		static bool prepare_splitter_chain(mge::Direction* direction, rtl::ArrayT<mg_element_chain_tube*>* chain, mg_splitter_params_t* param);
		static bool bind_splitter(Tube* splitter_tube, Tube* in_tube, Tube* out_tube, mg_splitter_params_t* param);

		static void diff_elements(mg_elements_changes_info* info, mg_media_element_t* element_in, mg_media_element_t* element_out);

	private:
		// Настраиваем туб. Подключаем хендл выхода.
		static bool set_out_handler(Tube* tube, INextDataHandler* out_handler);

		static const char* getName() { return "tube-man"; }
		static const char* getLogTag() { return "TUB-MAN"; }

	private:
		static rtl::Logger* m_log;
	};
}
//------------------------------------------------------------------------------
