﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "mg_context.h"
#include "mg_direction_bothway.h"
#include "mg_direction_audio_conference.h"
#include "mg_direction_video_conference.h"

namespace mge
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
#define LOG_PREFIX "PROC" // Media Gateway Processor

#include "logdef.h"
//-----------------------------------------------
//
//-----------------------------------------------
	class MediaProcessorElementList
	{
	public:
		MediaProcessorElementList() { }
		~MediaProcessorElementList() { m_list.clear(); }

		void add(Direction* dir, Termination* term) { m_list.add({ dir, term }); }
		Direction* getDirectionAt(int index) { return index < m_list.getCount() ? m_list[index].direction : nullptr; }
		Termination* getTerminationAt(int index) { return index < m_list.getCount() ? m_list[index].term : nullptr; }
		int getCount() { return m_list.getCount(); }

	private:
		struct Pair
		{
			Direction* direction;
			Termination* term;
		};
		rtl::ArrayT<Pair> m_list;
	};
	//-----------------------------------------------
	//
	//-----------------------------------------------
	MediaProcessor::MediaProcessor(MediaContext* owner, uint32_t id, const char* parent_id) :
		m_owner(owner), m_id(id), m_log(owner->getLog())
	{
		m_direction_id_counter = 0;
		m_name << parent_id << "-p" << id;
		m_conference = false;
		m_pathRecord = rtl::String::empty;
		m_extraEngine = nullptr;

		rtl::res_counter_t::add_ref(g_mge_processor_counter);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	MediaProcessor::~MediaProcessor()
	{
		clearAllDirections();

		rtl::res_counter_t::release(g_mge_processor_counter);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	uint32_t MediaProcessor::getTerminationCount()
	{
		uint32_t count = 0;

		for (int i = 0; i < m_directionList.getCount(); i++)
		{
			count += m_directionList[i]->getTerminationCount();
		}

		return count;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool MediaProcessor::addTermination(Termination* mg_term, DirectionType direction_type)
	{
		ENTER_FMT("streamId:%s", Direction_toString(direction_type));

		if (mg_term == nullptr)
		{
			ERROR_MSG("termination is null.");
			return false;
		}

		if (!mg_term->isReady(m_type))
		{
			//WARNING_FMT("termination %s not ready!", mg_term->getName());
			return true;
		}

		if ((m_conference) || (direction_type == DirectionType::conference))
		{
			Direction* direction = findDirection(direction_type);

			if (direction == nullptr)
			{
				direction = createDirection(direction_type);

				if (direction == nullptr)
				{
					ERROR_MSG("conference direction create FAIL.");
					return false;
				}

				if (!direction->initialize())
				{
					ERROR_MSG("init conference direction FAIL.");
					return false;
				}
			}

			if (!addTerminationToDirection(mg_term, direction))
			{
				ERROR_FMT("add termination %s in conference direction FAIL.", mg_term->getName());
				return false;
			}

			RETURN_FMT(true, "(term: %s, dir: conference)", mg_term->getName());
		}

		// Считаем количество терминаторов в дирекшинах.
		MediaProcessorElementList list;
		uint32_t terminations_count_out_conference = 0;
		Direction* conference_direction = nullptr;

		for (int i = 0; i < m_directionList.getCount(); i++)
		{
			Direction* direction_at_list = m_directionList.getAt(i);

			if (direction_at_list->getType() == DirectionType::conference)
			{
				conference_direction = direction_at_list;
				continue;
			}

			for (int j = 0; j < direction_at_list->getTerminationCount(); j++)
			{
				Termination* termination_at_list = direction_at_list->getTermination(j);

				list.add(direction_at_list, termination_at_list);

				if (termination_at_list != mg_term)
				{
					terminations_count_out_conference++;
				}
			}
		}

		// Если уже есть 2 терминатора то создается конференция.
		if (terminations_count_out_conference == 2)
		{
			m_conference = true;

			if (conference_direction == nullptr)
			{
				conference_direction = createDirection(DirectionType::conference);
				if (conference_direction == nullptr)
				{
					ERROR_MSG("create conference direction FAIL.");
					return false;
				}

				if (!conference_direction->initialize())
				{
					ERROR_MSG("init conference direction FAIL.");
					return false;
				}
			}

			bool already_insert = false;
			// переводим всех терминаторов в конференцию.
			for (int j = 0; j < list.getCount(); j++)
			{
				Termination* termination_at_list = list.getTerminationAt(j);
				if (termination_at_list == nullptr)
				{
					continue;
				}

				if (mg_term == termination_at_list)
				{
					already_insert = true;
				}

				list.getDirectionAt(j)->removeTermination(termination_at_list->getId());
				if (!addTerminationToDirection(termination_at_list, conference_direction))
				{
					ERROR_FMT("add termination %s in conference direction FAIL.", termination_at_list->getName());
					return false;
				}
				if (j > 0)
				{
					conference_direction->updateTopology(termination_at_list->mg_termination_get_topology_list());
				}
			}

			if (!already_insert)
			{
				if (!addTerminationToDirection(mg_term, conference_direction))
				{
					ERROR_FMT("add termination %s in conference direction FAIL.", mg_term->getName());
					return false;
				}
			}

			PRINT("transferred all terminations in conference");

			return true;
		}

		Direction* direction = findDirection(direction_type);
		if (direction == nullptr)
		{
			direction = createDirection(direction_type);
			if (direction == nullptr)
			{
				ERROR_FMT("create direction with type: %s FAIL.", Direction_toString(direction_type));
				return false;
			}
		}

		if (!addTerminationToDirection(mg_term, direction))
		{
			ERROR_FMT("add termination %s in direction with type: %s FAIL.",
				mg_term->getName(),
				Direction_toString(direction_type));
			return false;
		}

		RETURN_FMT(true, "(term: %s, dir: %s)", mg_term->getName(), Direction_toString(direction_type));
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void MediaProcessor::removeTermination(uint32_t termId)
	{
		ENTER_FMT("termId: %d", termId);

		for (int i = 0; i < m_directionList.getCount(); i++)
		{
			m_directionList[i]->removeTermination(termId);
		}

		PRINT("termination removed");
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void MediaProcessor::updateTopology(const rtl::ArrayT<h248::TopologyTriple>& triple_list)
	{
		for (int i = 0; i < m_directionList.getCount(); i++)
		{
			m_directionList[i]->updateTopology(triple_list);
		}
	}
	//----------------------------------------------
	//
	//----------------------------------------------
	void MediaProcessor::updateConfSchema(const char* jsonSchema)
	{
		for (int i = 0; i < m_directionList.getCount(); i++)
		{
			m_directionList[i]->updateConfSchema(jsonSchema);
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	Direction*	MediaProcessor::createDirection(DirectionType direction_type)
	{
		ENTER_FMT("type: %s", Direction_toString(direction_type));

		m_direction_id_counter++;
		Direction* direction = nullptr;

		if (direction_type == DirectionType::bothway)
		{
			direction = NEW BothwayDirection(m_owner, m_direction_id_counter, (const char*)m_name);
		}
		else if (direction_type == DirectionType::conference)
		{
			direction = m_type == MG_STREAM_TYPE_AUDIO ?
				(Direction*)NEW AudioConferenceDirection(m_owner, m_direction_id_counter, (const char*)m_name) :
				(Direction*)NEW VideoConferenceDirection(m_owner, m_direction_id_counter, (const char*)m_name);
		}

		direction->setProcessorType(m_type);
		direction->setExtraEngine(m_extraEngine);
		direction->setRecordPath(m_pathRecord);

		m_directionList.add(direction);

		RETURN_FMT(direction, "type: %s", Direction_toString(direction->getType()));
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	Direction*	MediaProcessor::findDirection(DirectionType direction_type)
	{
		ENTER();

		if (!m_directionList.getCount() == 0)
		{
			for (int i = 0; i < m_directionList.getCount(); i++)
			{
				Direction* direction_at_list = m_directionList.getAt(i);
				if (direction_at_list->getType() == direction_type)
				{
					RETURN_FMT(direction_at_list, "%s", direction_at_list->getName());
				}
			}
		}

		RETURN_FMT(nullptr, "direction with topology '%s' not found.",
			Direction_toString(direction_type));
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void MediaProcessor::releaseDirection(uint32_t dirId)
	{
		ENTER_FMT("%u", dirId);

		bool find = false;
		for (int i = 0; i < m_directionList.getCount(); i++)
		{
			Direction* direction_at_list = m_directionList.getAt(i);

			if (direction_at_list->getId() == dirId)
			{
				m_directionList.removeAt(i);
				find = true;
				PRINT_FMT("direction with id: %d released.", dirId);
				break;
			}
		}

		if (!find)
		{
			ERROR_FMT("direction with id: %d not found", dirId);
		}
		else
		{
			LEAVE();
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void MediaProcessor::clearAllDirections()
	{
		ENTER_FMT("%d", m_directionList.getCount());

		m_directionList.clear();

		LEAVE();
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool MediaProcessor::addTerminationToDirection(Termination* mg_term, Direction* mg_direction)
	{
		ENTER();

		if (mg_direction->getType() == DirectionType::bothway)
		{
			// B2B direction does not fail!
			mg_direction->addTermination(mg_term);

			//if (!mg_direction->addTermination(mg_term))
			//{
				// WARNING_FMT("add_termination (%s) fail.", mg_term->getName());
			//}
		}
		else
		{
			// conference can fail!
			if (!mg_direction->addTermination(mg_term))
			{
				WARNING_FMT("add_termination (%s) fail.", mg_term->getName());
				return false;
			}
		}

		RETURN_FMT(true, "(term: %s, dir: %s)", mg_term->getName(), mg_direction->getName());
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void MediaProcessor::resetConference()
	{
		m_conference = false;

		for (int i = 0; i < m_directionList.getCount(); i++)
		{
			Direction* direction_at_list = m_directionList.getAt(i);
			if (direction_at_list->getType() == DirectionType::conference)
			{
				releaseDirection(direction_at_list->getId());
				break;
			}
		}
	}
}
//-----------------------------------------------
