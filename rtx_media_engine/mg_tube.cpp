﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_tube.h"
#include "mg_tube_manager.h"
#include "mg_direction.h"
#include "std_thread.h"

#define LOG_PERFIX "TUBE"

#include "logdef_tube.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	Tube::Tube(HandlerType type, uint32_t id, rtl::Logger* log)
	{
		m_type = type;
		m_id = id;
		m_log = log;
		m_nextHandler = nullptr;
		m_lock.clear();
		m_in_free = true;
		m_out_free = true;
		m_tag = 0;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	Tube::~Tube()
	{
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool Tube::is_suited(uint16_t payload_id)
	{
		return m_payload_id == payload_id;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool Tube::is_suited(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out)
	{
		return payload_el_in != nullptr && m_payload_id == payload_el_in->get_payload_id();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool Tube::setNextHandler(INextDataHandler* handler)
	{
		uint32_t count = 0;
		while (count < 5)
		{
			if (m_lock.test_and_set(std::memory_order_acquire))
			{
				rtl::Thread::sleep(2);
				count++;
				continue;
			}

			m_nextHandler = handler;
			m_out_free = (handler == nullptr);

			m_lock.clear(std::memory_order_release);

			return true;
		}

		return false;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool Tube::unsubscribe(const INextDataHandler* out_handler)
	{
		uint32_t count = 0;
		while (count < 100)
		{
			if (m_lock.test_and_set(std::memory_order_acquire))
			{
				rtl::Thread::sleep(2);
				count++;
				continue;
			}

			if (m_nextHandler != nullptr)
			{
				m_nextHandler->unsubscribe(this);
				m_nextHandler = nullptr;
			}
			m_out_free = true;

			if (out_handler != this)
			{
				m_in_free = true;
			}

			m_lock.clear(std::memory_order_release);

			return true;
		}

		return false;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool Tube::isNextHandler(const INextDataHandler* out_handler)
	{
		return !(out_handler == nullptr || m_nextHandler != out_handler);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	INextDataHandler* Tube::getNextHandler()
	{
		bool lock;

		return getNextHandler(&lock);
	}
	//------------------------------------------------------------------------------
	INextDataHandler* Tube::getNextHandler(bool* lock)
	{
		if (m_lock.test_and_set(std::memory_order_acquire))
		{
			if (lock != nullptr)
			{
				*lock = true;
			}
			return nullptr;
		}

		INextDataHandler* out = m_nextHandler;

		m_lock.clear(std::memory_order_release);

		if (lock != nullptr)
		{
			*lock = false;
		}

		return out;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool Tube::free()
	{
		return (m_in_free && m_out_free);
	}
	//------------------------------------------------------------------------------
	void Tube::set_free_in(bool in)
	{
		m_in_free = in;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	DECLARE rtl::Logger* g_mg_tube_log = nullptr;
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void Tube::send(const INextDataHandler* out_handler, const rtp_packet* packet)
	{
		INextDataHandler* out = getNextHandler();
		if (out != nullptr)
		{
			out->send(out_handler, packet);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void Tube::send(const INextDataHandler* out_handler, const uint8_t* data, uint32_t len)
	{
		INextDataHandler* out = getNextHandler();
		if (out != nullptr)
		{
			out->send(out_handler, data, len);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void Tube::send(const INextDataHandler* out_handler, const rtcp_packet_t* packet)
	{
		INextDataHandler* out = getNextHandler();
		if (out != nullptr)
		{
			out->send(out_handler, packet);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	const char* HandlerType_toString(HandlerType type)
	{
		static const char* __tube_names[] =
		{
			"mg_simple_e",
			"AudioDecoder",
			"AudioEncoder",
			"ComfortNoise",
			"Resampler",
			"StereoResampler",
			"MonoResampler",
			"DTMF2833",
			"mg_conference_in_e",
			"mg_conference_out_e",
			"mg_conference_record_e",
			"UDPTL_Recevier",
			"UDPTL_Transmitter",
			"VideoEncoder",
			"VideoDecoder",
			"ImageTransformer",
			"ConfVideoInput",
			"ConfVideoOutput",
			"ConfVideoRecorder",
			"AudioTimer",
			"AudioSplitter"
		};

		int index = (int)type;

		return index >= 0 && index < sizeof(__tube_names) / sizeof(const char*) ?
			__tube_names[index] : "unknow tube type!";
	}
}
//-----------------------------------------------
