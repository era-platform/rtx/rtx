﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

//------------------------------------------------------------------------------
// configuration files
//------------------------------------------------------------------------------
#include "std_basedef.h"
#include "std_typedef.h"
#include "std_funcdef.h"

//------------------------------------------------------------------------------
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the RTX_MEDIA_ENGINE_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// RTX_MEDIA_ENGINE_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
//------------------------------------------------------------------------------
#if defined (TARGET_OS_WINDOWS)
	#ifdef RTX_MGE_EXPORTS
		#ifdef _DEBUG
			#define ME_DEBUG_API __declspec(dllexport)
		#else
			#define ME_DEBUG_API
		#endif
		#define RTX_MGE_API __declspec(dllexport)
	#else
		#define RTX_MGE_API __declspec(dllimport)
		#define ME_DEBUG_API
	#endif
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	#define RTX_MGE_API
	#define ME_DEBUG_API
#endif
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "mg_interface.h"
#include "rtx_codec.h"
//
//------------------------------------------------------------------------------
RTX_MGE_API uint32_t me_get_version();
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
extern "C"
{
	RTX_MGE_API h248::IMediaGate* create_media_gateway_module(const char* log_path);
	//--------------------------------------------------------------------------
	RTX_MGE_API void remove_media_gateway();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
