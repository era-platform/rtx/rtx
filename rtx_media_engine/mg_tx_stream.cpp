﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_thread.h"
#include "mg_tx_stream.h"
#include "mg_session.h"

#include "logdef_stream.h"

namespace mge
{
	const char* mg_tx_stream_type_toString(mg_tx_stream_type_t type)
	{
		static const char* names[] = {
			"mg_tx_simple_audio_e",
			"mg_tx_device_audio_e",
			"mg_tx_dummy_e",
			"mg_tx_simple_video_e",
			"mg_tx_webrtc_video_e",
			"mg_tx_fax_proxy_e",
			"mg_tx_fax_t30_e",
			"mg_tx_fax_t38_e",
			"mg_tx_ivr_e"
		};

		return type < (sizeof(names) / sizeof(const char*)) ? names[type] : "unknown_type";
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_tx_stream_t::mg_tx_stream_t(rtl::Logger* log, uint32_t id, mg_tx_stream_type_t type, const char* term_id) :
		m_log(log), m_type(type), m_media_sdp(log, term_id, id)
	{
		m_id = id;
		strcpy(m_logTag, "TXS");
		m_local_control.stream_mode = h248::TerminationStreamMode::Inactive;
		m_local_control.reserve_group = false;
		m_local_control.reserve_value = false;

		m_main_lock = false;
		m_tx_lock.clear();

		m_stat = nullptr;

		m_rtcp_monitor = nullptr;

		m_conference_out_tube = nullptr;

		rtl::res_counter_t::add_ref(g_mge_stream_counter);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_tx_stream_t::~mg_tx_stream_t()
	{
		m_local_control.stream_mode = h248::TerminationStreamMode::Inactive;

		m_stat = nullptr;

		disconnected_from_tubes();

		if (m_rtcp_monitor != nullptr)
		{
			m_rtcp_monitor->set_session_event_handler_tx(nullptr);
			m_rtcp_monitor = nullptr;
		}

		rtl::res_counter_t::release(g_mge_stream_counter);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_tx_stream_t::mg_stream_set_LocalControl_Mode(h248::TerminationStreamMode mode)
	{
		ENTER();

		if (!m_main_lock)
		{
			WARNING("stream unlocked!");
			return false;
		}

		m_local_control.stream_mode = mode;

		return true;
	}
	//------------------------------------------------------------------------------
	bool mg_tx_stream_t::mg_stream_set_LocalControl_ReserveValue(bool value)
	{
		ENTER();

		if (!m_main_lock)
		{
			WARNING("stream unlocked!");
			return false;
		}

		m_local_control.reserve_value = value;

		return true;
	}
	//------------------------------------------------------------------------------
	bool mg_tx_stream_t::mg_stream_set_LocalControl_ReserveGroup(bool value)
	{
		ENTER();

		if (!m_main_lock)
		{
			WARNING("stream unlocked!");
			return false;
		}

		m_local_control.reserve_group = value;

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_tx_stream_t::mg_stream_set_LocalControl_Property(const h248::Field* LC_property)
	{
		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_tx_stream_t::mark_tube(Tube* tube)
	{
		if (tube == nullptr)
		{
			return;
		}

		HandlerType t = tube->getType();

		m_rx_tubes.add(tube);

		PRINT_FMT("this stream in tube: %s (%d).", tube->getName(), t);
	}
	//------------------------------------------------------------------------------
	void mg_tx_stream_t::disconnected_from_tubes()
	{
		ENTER_FMT("m_rx_tubes length: %d", m_rx_tubes.getCount());

		if (m_conference_out_tube != nullptr)
		{
			m_conference_out_tube->unsubscribe(m_conference_out_tube);
			m_conference_out_tube = nullptr;
		}

		for (int i = 0; i < m_rx_tubes.getCount(); i++)
		{
			Tube* tube_at_list = m_rx_tubes.getAt(i);
			if (tube_at_list == nullptr)
			{
				continue;
			}

			PRINT_FMT("unsubscribe from %p", tube_at_list); // ->getName()

			if (tube_at_list->unsubscribe(this))
			{
				// внимание!!!
				// функция туба unsubscribe вернется в функцию текущего стрима unsubscribe
				// и список m_rx_tubes будет изменен.
				i--;

				PRINT_FMT("this stream dropped from the tube: %s.", tube_at_list->getName());
			}
			else
			{
				ERROR_FMT("this stream not access from the tube: %s.", tube_at_list->getName());
			}
		}

		m_rx_tubes.clear();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_tx_stream_t::setMediaParams(mg_sdp_media_base_t& media)
	{
		m_media_sdp.clear();

		rtl::String sdp;
		
		media.write(sdp);

		m_media_sdp.read(sdp);
	}
	//------------------------------------------------------------------------------
	int mg_tx_stream_t::rtcp_session_tx(const rtcp_packet_t* packet)
	{
		return 0;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_tx_stream_t::onReceiverStarted()
	{
		//...
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_tx_stream_t::reset_before_new_connect()
	{

	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_tx_stream_t::unsubscribe(const INextDataHandler* out_handler)
	{
		if (out_handler == nullptr)
		{
			return false;
		}

		Tube* out_tube = (Tube*)out_handler;
		if (out_tube == nullptr)
		{
			return false;
		}

		for (int i = 0; i < m_rx_tubes.getCount(); i++)
		{
			Tube* tube_at_list = m_rx_tubes.getAt(i);
			if (tube_at_list == nullptr)
			{
				continue;
			}

			if (tube_at_list->getId() == out_tube->getId())
			{
				m_rx_tubes.removeAt(i);
				PRINT_FMT("tube '%s'", tube_at_list->getName());
				break;
			}
		}

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_tx_stream_t::lock()
	{
		if (isLocked())
		{
			return true;
		}

		uint32_t time_diff = 0;
		while (time_diff <= 3000)
		{
			if (checkAndLock())
			{
				rtl::Thread::sleep(5);
				time_diff += 5;
				continue;
			}

			m_main_lock = true;
			return true;
		}

		ERROR_MSG("stream not locked.");
		return false;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_tx_stream_t::unlock()
	{
		if (isLocked())
		{
			m_main_lock = false;

			m_tx_lock.clear(std::memory_order_release);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_tx_stream_t::rtcp_analize(rtcp_packet_t* packet)
	{
		if (m_rtcp_monitor == nullptr)
		{
			return false;
		}

		return m_rtcp_monitor->recalc_packet(packet);
	}
	//------------------------------------------------------------------------------
	void mg_tx_stream_t::rtcp_analize(const rtp_packet* packet)
	{
		if (m_rtcp_monitor == nullptr)
		{
			return;
		}

		m_rtcp_monitor->rtcp_event_tx_data(packet);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_tx_stream_t::stat_analize(const rtp_packet* packet)
	{
		if ((m_stat == nullptr) || (packet == nullptr))
		{
			return;
		}

		m_stat->packets++;
		m_stat->bytes += packet->get_full_length();
	}
	//------------------------------------------------------------------------------
	void mg_tx_stream_t::stat_out_of_order()
	{
		if (m_stat == nullptr)
		{
			return;
		}

		m_stat->out_of_order++;
	}
	//------------------------------------------------------------------------------
	void mg_tx_stream_t::stat_lost()
	{
		if (m_stat == nullptr)
		{
			return;
		}

		m_stat->lost++;
	}
	//------------------------------------------------------------------------------
	void mg_tx_stream_t::stat_invalid()
	{
		if (m_stat == nullptr)
		{
			return;
		}

		m_stat->invalid++;
	}
	//------------------------------------------------------------------------------
	void mg_tx_stream_t::stat_filtered()
	{
		if (m_stat == nullptr)
		{
			return;
		}

		m_stat->filtered++;
	}
}
//------------------------------------------------------------------------------
