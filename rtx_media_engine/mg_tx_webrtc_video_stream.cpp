﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_tx_webrtc_video_stream.h"
#include "mg_session.h"
#include "logdef_stream.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_tx_webrtc_video_stream_t::mg_tx_webrtc_video_stream_t(rtl::Logger* log, uint32_t id, uint32_t parent_id, const char* term_id) :
		mg_tx_stream_t(log, id, mg_tx_webrtc_video_e, term_id)
	{
		m_rtp = nullptr;
		m_recalc_packets = true;
		uint32_t ticks = rtl::DateTime::getTicks();
		m_ssrc_to_rtp = (ticks + rand()) | 0x8a3b00b0;
		m_seqno = (uint16_t)(ticks + rand()) | 0xb53a;
		m_timestamp = (ticks + rand()) | 0x5dd500ba;

		m_first_packet = false;
		m_in_recvtime_last = ticks;
		m_in_ssrc_last = 0;
		m_in_seqno_diff = 0;
		m_in_ts_diff = 0;

		rtl::String name;
		name << term_id << "-s" << parent_id << "-txwvs" << id;
		mg_tx_stream_t::setName(name);
		mg_tx_stream_t::setTag("TXS_FX38");

		m_storeFirstPackets = false;
		m_receiverStarted = false;
		m_waitReceiver = true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_tx_webrtc_video_stream_t::~mg_tx_webrtc_video_stream_t()
	{
		if (m_rtp != nullptr)
		{
			m_rtp->dec_tx_subscriber(m_ssrc_to_rtp);
		}

		m_rtp = nullptr;

	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_tx_webrtc_video_stream_t::send(const INextDataHandler* out_handler, const rtp_packet* packet)
	{
		if (checkAndLock())
		{
			WARNING("stream locked!");
			return;
		}

		send_packet_to_rtp_lock(out_handler, packet);

		releaseLock();

		//PLOG_STREAM_WRITE(LOG_PREFIX, "send -- ID(%s): end.", getName());
	}
	//------------------------------------------------------------------------------
	void mg_tx_webrtc_video_stream_t::send(const INextDataHandler* out_handler, const uint8_t* data, uint32_t len)
	{
		if (checkAndLock())
		{
			WARNING("stream locked!");
			return;
		}

		send_data_to_rtp_lock(out_handler, data, len);

		releaseLock();
	}
	//------------------------------------------------------------------------------
	void mg_tx_webrtc_video_stream_t::send(const INextDataHandler* out_handler, const rtcp_packet_t* packet)
	{
		if (checkAndLock())
		{
			WARNING("stream locked!");
			return;
		}

		send_ctrl_to_rtp_lock(out_handler, packet);

		releaseLock();
	}
	//------------------------------------------------------------------------------
	void mg_tx_webrtc_video_stream_t::send_packet_to_rtp_lock(const INextDataHandler* out_handler, const rtp_packet* packet)
	{
		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::Loopback)
		{
			WARNING("stream mode is loopback!");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::Inactive)
		{
			WARNING("stream mode is inactive!");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::RecvOnly)
		{
			WARNING("stream mode is recvonly!");
			return;
		}

		if (packet == nullptr)
		{
			ERROR_MSG("packet is null!");
			return;
		}

		if (m_rtp == nullptr)
		{
			ERROR_MSG("rtp handler is null!");
			return;
		}
	
		if (!m_storeFirstPackets || m_receiverStarted)
		{
			// отправляем сразу -- нормальный режим
			rtp_packet* pckt = (rtp_packet*)packet;

			if (m_recalc_packets)
				recalc_packet(pckt);

			m_rtp->tx_packet_channel(pckt, &m_remote_address);
		}
		else
		{
			if (m_waitReceiver)
			{
				PRINT("wait receiver...");
				// ждем пакет от другой стороны
				rtp_packet* pckt = NEW rtp_packet();
				pckt->copy_from(packet);

				recalc_packet(pckt);

				m_sendQueue.add(pckt);
			}
			else
			{
				PRINT("packet received");

				// пакет от другой стороны получен!
				m_waitReceiver = false;

				for (int i = 0; i < m_sendQueue.getCount(); i++)
				{
					rtp_packet* pckt = m_sendQueue[i];

					m_rtp->tx_packet_channel(pckt, &m_remote_address);

					DELETEO(pckt);
				}

				m_sendQueue.clear();

				rtp_packet* pckt = (rtp_packet*)packet;

				m_rtp->tx_packet_channel(pckt, &m_remote_address);
			}
		}
	}
	//------------------------------------------------------------------------------
	void mg_tx_webrtc_video_stream_t::send_data_to_rtp_lock(const INextDataHandler* out_handler, const uint8_t* data, uint32_t len)
	{
		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::Loopback)
		{
			WARNING("stream mode is loopback!");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::Inactive)
		{
			WARNING("stream mode is inactive!");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::RecvOnly)
		{
			WARNING("stream mode is recvonly!");
			return;
		}

		if (data == nullptr)
		{
			ERROR_MSG("data is null!");
			return;
		}

		if (m_rtp == nullptr)
		{
			ERROR_MSG("rtp handler is null!");
			return;
		}

		m_rtp->tx_data_channel(data, len, &m_remote_rtcp_address);
	}
	//------------------------------------------------------------------------------
	void mg_tx_webrtc_video_stream_t::send_ctrl_to_rtp_lock(const INextDataHandler* out_handler, const rtcp_packet_t* packet)
	{
		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::Loopback)
		{
			WARNING("stream mode is loopback!");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::Inactive)
		{
			WARNING("stream mode is inactive!");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::RecvOnly)
		{
			WARNING("stream mode is recvonly!");
			return;
		}

		if (packet == nullptr)
		{
			ERROR_MSG("packet is null!");
			return;
		}

		if (m_rtp == nullptr)
		{
			ERROR_MSG("rtp handler is null!");
			return;
		}

		rtcp_packet_t new_packet;
		new_packet.copy(packet);

		if (rtcp_analize(&new_packet))
		{
			m_rtp->tx_ctrl_channel(&new_packet, &m_remote_rtcp_address);
		}
		else
		{
			m_rtp->tx_ctrl_channel(packet, &m_remote_rtcp_address);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------	
	void mg_tx_webrtc_video_stream_t::set_channel_event(i_tx_channel_event_handler* rtp)
	{
		if (!isLocked())
		{
			WARNING("stream mode is unlocked!");
			return;
		}
		if (rtp == nullptr)
		{
			if (m_rtp != nullptr)
			{
				m_rtp->dec_tx_subscriber(m_ssrc_to_rtp);
			}
		}
		else
		{
			if (m_rtp == nullptr)
			{
				rtp->inc_tx_subscriber(m_ssrc_to_rtp);
			}
			else
			{
				if (m_rtp != rtp)
				{
					m_rtp->dec_tx_subscriber(m_ssrc_to_rtp);
					rtp->inc_tx_subscriber(m_ssrc_to_rtp);
				}
			}
		}
		m_rtp = rtp;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_tx_webrtc_video_stream_t::onReceiverStarted()
	{
		if (m_waitReceiver)
		{
			PRINT("recieverStart event handled.");
			m_waitReceiver = false;
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_tx_webrtc_video_stream_t::recalc_packet(const rtp_packet* packet)
	{
		// если ssrc Изменилось то
		//     расчитываем новую разницу между seq_no пакета и seq_no сохраненный с последней отправкой
		//     для timestamp вычесляем фактическое время прошедшее с момента последней отправки и на его основе вычисляем новую разницу
		//         stream.seq_diff = stream.seq_last - packet.seq
		//         time_shift = (datetime.now - stream.recvtime) * 90
		//         stream.ts_diff = (stream.ts_last + time_shift) - packet.ts

		// добавляем ранее вычисленные разницы в seq_no и timestamp
		//    packet.seq += stream.seq_diff
		//    packet.timestamp += stream.ts_diff
		//    stream.recvtime = datetime.now

		rtp_packet* ref = const_cast<rtp_packet*>(packet);

		uint32_t ssrc = ref->get_ssrc();
		uint16_t seq = ref->get_sequence_number();
		uint32_t ts = ref->getTimestamp();
		uint32_t ticks = rtl::DateTime::getTicks();

		if (m_first_packet)
		{
			m_first_packet = false;
			m_in_ssrc_last = ssrc;
		}
		else if (ssrc != m_in_ssrc_last)
		{
			m_in_ssrc_last = ssrc;
			m_in_seqno_diff = m_seqno - seq + 1;
			int time_diff = ticks - m_in_recvtime_last;
			m_in_ts_diff = (m_timestamp - ts) + (time_diff * 90);
		}

		ref->set_sequence_number(m_seqno = seq + m_in_seqno_diff);
		ref->set_timestamp(m_timestamp = ts + m_in_ts_diff);
		ref->set_ssrc(m_ssrc_to_rtp);

		m_in_recvtime_last = ticks;
	}
}
//------------------------------------------------------------------------------
