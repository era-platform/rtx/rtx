﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_thread.h"
#include "mg_tx_fax_t38_stream.h"
#include "mg_fax.h"
#include "mg_session.h"

#include "logdef_stream.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_tx_fax_t38_stream_t::mg_tx_fax_t38_stream_t(rtl::Logger* log, uint32_t id, uint32_t parent_id, const char* term_id) :
		mg_tx_stream_t(log, id, mg_tx_fax_t38_e, term_id)
	{
		rtl::String name;
		name << term_id << "-s" << parent_id << "-txft38s" << id;
		mg_tx_stream_t::setName(name);
		mg_tx_stream_t::setTag("TXS_FX38");

		m_fax = nullptr;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_tx_fax_t38_stream_t::~mg_tx_fax_t38_stream_t()
	{
		m_fax = nullptr;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_tx_fax_t38_stream_t::send(const INextDataHandler* out_handler, const rtp_packet* packet)
	{
		if (checkAndLock())
		{
			WARNING("stream locked!");
			return;
		}

		send_packet_to_rtp_lock(out_handler, packet);

		releaseLock();
	}
	//------------------------------------------------------------------------------
	void mg_tx_fax_t38_stream_t::send(const INextDataHandler* out_handler, const uint8_t* data, uint32_t len)
	{
		if (checkAndLock())
		{
			WARNING("stream locked!");
			return;
		}

		send_data_to_rtp_lock(out_handler, data, len);

		releaseLock();
	}
	//------------------------------------------------------------------------------
	void mg_tx_fax_t38_stream_t::send(const INextDataHandler* out_handler, const rtcp_packet_t* packet)
	{
		if (checkAndLock())
		{
			WARNING("stream locked!");
			return;
		}

		send_ctrl_to_rtp_lock(out_handler, packet);

		releaseLock();
	}
	//------------------------------------------------------------------------------
	void mg_tx_fax_t38_stream_t::send_packet_to_rtp_lock(const INextDataHandler* out_handler, const rtp_packet* packet)
	{
		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::Loopback)
		{
			WARNING("stream mode is loopback!");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::Inactive)
		{
			WARNING("stream mode is inactive!");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::RecvOnly)
		{
			WARNING("stream mode is recvonly!");
			return;
		}

		if (packet == nullptr)
		{
			ERROR_MSG("packet is null!");
			return;
		}

		if (m_fax == nullptr)
		{
			ERROR_MSG("fax is null!");
			return;
		}

		uint32_t length = packet->get_payload_length();
		uint32_t write = m_fax->write_fax(packet->get_payload(), length);
		if (write != length)
		{
			WARNING_FMT("write fax fail (write: %d, payload length: %d).", write, length);
			return;
		}

		PRINT_FMT("write fax: %d", write);
	}
	//------------------------------------------------------------------------------
	void mg_tx_fax_t38_stream_t::send_data_to_rtp_lock(const INextDataHandler* out_handler, const uint8_t* data, uint32_t len)
	{
		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::Loopback)
		{
			WARNING("stream mode is loopback!");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::Inactive)
		{
			WARNING("stream mode is inactive!");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::RecvOnly)
		{
			WARNING("stream mode is recvonly!");
			return;
		}

		if (data == nullptr)
		{
			ERROR_MSG("data is null!");
			return;
		}

		if (m_fax == nullptr)
		{
			ERROR_MSG("fax is null!");
			return;
		}

		uint32_t write = m_fax->write_fax(data, len);
		if (write != len)
		{
			WARNING_FMT("write fax fail (write: %d, payload length: %d).", write, len);
			return;
		}

		PRINT_FMT("write fax: %d.", write);
	}
	//------------------------------------------------------------------------------
	void mg_tx_fax_t38_stream_t::send_ctrl_to_rtp_lock(const INextDataHandler* out_handler, const rtcp_packet_t* packet)
	{
		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::Loopback)
		{
			WARNING("stream mode is loopback!");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::Inactive)
		{
			WARNING("stream mode is inactive!");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::RecvOnly)
		{
			WARNING("stream mode is recvonly.");
			return;
		}

		if (packet == nullptr)
		{
			ERROR_MSG("packet is null!");
			return;
		}

		WARNING("what is this?");
	}
}
//------------------------------------------------------------------------------
