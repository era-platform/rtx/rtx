/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_context.h"
#include "mg_direction_video_conference.h"
#include "mg_tube_manager.h"
#include "mg_rx_stream.h"
#include "mg_tx_stream.h"
#include "mg_video_conference_ctrl.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define LOG_PREFIX m_tag // "MG-DIRV" // Media Gateway Direction
#define MG_DTMF_EVENT "mg-dtmf"

#include "logdef.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	VideoConferenceDirection::VideoConferenceDirection(MediaContext* owner, uint32_t id, const char* parent_id) :
		Direction(owner, id, parent_id, DirectionType::conference)
	{
		m_ctrl = nullptr;
		strcpy(m_tag, "DIR-VCON");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	VideoConferenceDirection::~VideoConferenceDirection()
	{
		if (m_ctrl != nullptr)
		{
			DELETEO(m_ctrl);
			m_ctrl = nullptr;
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool VideoConferenceDirection::initialize()
	{
		if (m_ctrl != nullptr)
		{
			DELETEO(m_ctrl);
		}

		m_ctrl = NEW VideoConferenceController(m_log, m_name);

		m_ctrl->create(m_owner->getConf_videoSchema(), m_owner->getConf_videoSelector());

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool VideoConferenceDirection::addTermination(Termination* term)
	{
		//PLOG_WRITE(LOG_PREFIX, "add_termination -- ID(%s): begin.", (const char*)m_name);

		int check_res = checkTermination(term);
		if (check_res < 0)
		{
			return false;
		}

		cleanupTerminationTubes(term);

		const char* termId = term->getName();
		MediaSession* mg_stream = term->findMediaSession(getProcessorType());
		if (mg_stream == nullptr)
		{
			ERROR_FMT("mg_stream is null in termination '%s'.", termId);
			return false;
		}

		const rtl::ArrayT<mg_rx_stream_t*>& rxList = mg_stream->get_rx_streams();
		const rtl::ArrayT<mg_tx_stream_t*>& txList = mg_stream->get_tx_streams();

		int count_rx = rxList.getCount();
		int count_tx = txList.getCount();

		int count = (count_rx > count_tx) ? count_rx : count_tx;

		for (int i = 0; i < count; i++)
		{
			mg_rx_stream_t* rxStream = (count_rx <= i) ? nullptr : rxList.getAt(i);
			mg_tx_stream_t* txStream = (count_tx <= i) ? nullptr : txList.getAt(i);

			Tube* conference_in_tube = buildRx(termId, rxStream, txStream);
			Tube* conference_out_tube = buildTx(termId, rxStream, txStream);

			// audio and video conferencing tubes must be labeled for video conferencing
			if (conference_in_tube != nullptr)
				conference_in_tube->set_tag(term->getId());

			// tubes maybe null. we add term nor tubes!
			if (!m_ctrl->addConfMember(term, conference_in_tube, conference_out_tube))
			{
				cleanupTerminationTubes(term);
				return false;
			}

			if ((txStream != nullptr) && (conference_out_tube != nullptr))
			{
				txStream->set_conference_out_tube(conference_out_tube);
				conference_out_tube->setNextHandler(txStream);
				txStream->reset_before_new_connect();
			}
		}

		if (!term->mg_stream_start((getProcessorType())))
		{
			cleanupTerminationTubes(term);
			return false;
		}

		if (check_res == 1)
		{
			Direction::addTermination(term);
		}

		RETURN_FMT(true, "(id term: %s)", term->getName());
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void VideoConferenceDirection::removeTermination(uint32_t termId)
	{
		m_ctrl->removeConfMember(termId);

		Direction::removeTermination(termId);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void VideoConferenceDirection::checkTerminationProxy(uint32_t termId)
	{
		m_ctrl->attachConfMember(termId);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool VideoConferenceDirection::searchTopologyTubes(const char* termIdFrom, mg_video_conference_out_tube_t*& fromTube, const char* termIdTo, mg_video_conference_out_tube_t*& toTube)
	{
		for (int i = 0; i < m_tubeList.getCount(); i++)
		{
			Tube* tube = m_tubeList[i];
			if (tube != nullptr && tube->getType() == HandlerType::ConfAudioOutput)
			{
				mg_video_conference_out_tube_t* out_tube = (mg_video_conference_out_tube_t*)tube;
				if (strstr(out_tube->get_termination_name(), termIdFrom) != nullptr)
				{
					fromTube = out_tube;
				}
				else if (strstr(out_tube->get_termination_name(), termIdTo) != nullptr)
				{
					toTube = out_tube;
				}

				// complite
				if (fromTube != nullptr && toTube != nullptr)
				{
					return true;
				}
			}
		}

		return false;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void VideoConferenceDirection::updateTopology(const rtl::ArrayT<h248::TopologyTriple>& triple_list)
	{
		// ��������� �� ����� ������ ���������.
		for (int index_topol = 0; index_topol < triple_list.getCount(); index_topol++)
		{
			h248::TopologyTriple trip = triple_list.getAt(index_topol);

			//----------------------------------------------------------------------------------------
			// ���� ������������ ��������� � ���������.
			Termination* term_to = findTermination(trip.term_to);
			Termination* term_from = findTermination(trip.term_from);

			if (term_from == nullptr || term_to == nullptr)
			{
				continue;
			}

			//----------------------------------------------------------------------------------------
			// ���� ���� �������������� ������������ � �����������.
			mg_video_conference_out_tube_t* tube_from;
			mg_video_conference_out_tube_t* tube_to;

			if (!searchTopologyTubes(term_from->getName(), tube_from, term_to->getName(), tube_to))
			{
				//PLOG_ERROR(LOG_PREFIX, "update_topology -- ID(%s): not found tubes.", (const char*)m_name);
				continue;
			}

			tube_from->changeTopologyFrom(tube_to->get_termination_name(), trip.direction);
			tube_to->changeTopologyTo(tube_from->get_termination_name(), trip.direction);
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoConferenceDirection::updateConfSchema(const char* jsonSchema)
	{
		m_ctrl->updateSchema(jsonSchema);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	int VideoConferenceDirection::checkTermination(Termination* term)
	{
		if (m_ctrl == nullptr)
		{
			ERROR_MSG("not ready : m_ctrl is null!");
			return -1;
		}

		uint32_t processorType = getProcessorType();

		if (!term->isReady(processorType))
		{
			ERROR_FMT("not ready : unsuitable processor type %d!", processorType);
			return -2;
		}

		if (containsTermination(term->getId()))
		{
			//PLOG_ERROR(LOG_PREFIX, "add_termination -- ID(%s): termination already exist.", (const char*)m_name);
			//remove_termination(term->get_id());
			return 0;
		}

		return 1;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_video_conference_in_tube_t* VideoConferenceDirection::createInputTube(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out)
	{
		mg_video_conference_in_tube_t* tube = (mg_video_conference_in_tube_t*)createTube(
			HandlerType::ConfVideoInput,
			payload_el_in,
			payload_el_out);

		return tube;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_video_conference_out_tube_t* VideoConferenceDirection::createOutputTube(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out)
	{
		mg_video_conference_out_tube_t* tube = (mg_video_conference_out_tube_t*)createTube(
			HandlerType::ConfVideoOutput,
			payload_el_in,
			payload_el_out);

		return tube;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	Tube* VideoConferenceDirection::buildRx(const char* termId, mg_rx_stream_t* rxStream, mg_tx_stream_t* txStream)
	{
		mg_media_element_list_t fill_list_rx;
		mg_media_element_list_t fill_list_tx;

		fillElementListRx(termId, rxStream, txStream, fill_list_rx, fill_list_tx);

		if ((fill_list_rx.getCount() == 0) || (fill_list_tx.getCount() == 0))
		{
			WARNING("TX media element lists is empty!");
			return nullptr;
		}

		if (fill_list_tx.get_priority() - 1)
			fill_list_tx.set_priority_index(0);

		mg_media_element_t* element_out = fill_list_tx.get_priority();

		Tube* tube_out = nullptr;
		for (int index = 0; index < fill_list_rx.getCount(); index++)
		{
			mg_media_element_t* element_in = fill_list_rx.getAt(index);
			if ((element_in == nullptr) || (element_in->isNull()))
			{
				ERROR_MSG("elements is null!");
				continue;
			}

			bool clear_out = false;
			if (tube_out == nullptr)
			{
				if ((tube_out = createInputTube(element_in, element_out)) == nullptr)
				{
					WARNING("TubeManager::create_conference_in_tube fail.");
					return nullptr;
				}

				clear_out = true;
			}
			PRINT_FMT("TubeManager::prepare_tubes_chain (%s %s)",
				element_in ? (const char*)element_in->get_element_name() : "(null)",
				element_out ? (const char*)element_out->get_element_name() : "(null)");

			Tube* tube_in = nullptr;
			if (!TubeManager::prepare_tubes_chain(this, element_in, element_out, &tube_in, &tube_out, true, clear_out))
			{
				//PLOG_ERROR(LOG_PREFIX, "associate_streams -- ID(%s): prepare tubes chain fail.", (const char*)m_name);
				continue;
			}

			if ((tube_in == nullptr) || (tube_out == nullptr))
			{
				//PLOG_ERROR(LOG_PREFIX, "associate_streams -- ID(%s): bad tubes.", (const char*)m_name);
				continue;
			}

			if (!customizeStreamRx(rxStream, tube_in))
			{
				//PLOG_ERROR(LOG_PREFIX, "associate_streams -- ID(%s): customize rx fail.", (const char*)m_name);

				// ���������� ������� ������� ������� ����. ��������� ��������� �� �������.
				// ������������ �� ��� ���� ������� ����� � ������ �����.
				// �������� ��������� ��� ������ ������� ���������.
				// ���� ���... 
				//TubeManager::clear_tube(this, tube_in);
				tube_in->release();
				continue;
			}
		}

		return tube_out;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	Tube* VideoConferenceDirection::buildTx(const char* termId, mg_rx_stream_t* rxStream, mg_tx_stream_t* txStream)
	{
		mg_media_element_list_t fill_list_rx;
		mg_media_element_list_t fill_list_tx;

		fillElementListTx(termId, rxStream, txStream, fill_list_rx, fill_list_tx);

		if ((fill_list_rx.getCount() == 0) || (fill_list_tx.getCount() == 0))
		{
			WARNING("mg_fill_media_elements_lists_tx fail!");
			return nullptr;
		}

		mg_media_element_t* element_in = fill_list_rx.get_priority();

		Tube* tube_in = nullptr;
		for (int index = 0; index < fill_list_tx.getCount(); index++)
		{
			mg_media_element_t* element_out = fill_list_tx.getAt(index);
			if ((element_out == nullptr) || (element_out->isNull()))
			{
				//PLOG_ERROR(LOG_PREFIX, "associate_streams -- ID(%s): elements is null.", (const char*)m_name);
				continue;
			}

			if (tube_in == nullptr)
			{
				if ((tube_in = createOutputTube(element_in, element_out)) == nullptr)
				{
					WARNING("TubeManager::create_conference_out_tube fail!");
					return nullptr;
				}
			}
			PRINT_FMT("TubeManager::prepare_tubes_chain (%s %s)",
				element_in ? (const char*)element_in->get_element_name() : "(null)",
				element_out ? (const char*)element_out->get_element_name() : "(null)");

			Tube* tube_out = nullptr;
			if (!TubeManager::prepare_tubes_chain(this, element_in, element_out, &tube_in, &tube_out, true, false))
			{
				//PLOG_ERROR(LOG_PREFIX, "associate_streams -- ID(%s): prepare tubes chain fail.", (const char*)m_name);
				continue;
			}

			if ((tube_in == nullptr) || (tube_out == nullptr))
			{
				//PLOG_ERROR(LOG_PREFIX, "associate_streams -- ID(%s): bad tubes.", (const char*)m_name);
				continue;
			}

			// RTX-82. � ����� ����������� ��� ����� � ��������������, ������ �������� ����� ��������� � �������, 
			// �� ����� �� ������ ��� �� ��� (conference_out_tube). ��� � ������ ��������� �� �����, �� �������� �������������� (set_conference_out_tube)
			if (tube_out->getType() != HandlerType::ConfVideoOutput)
			{
				if (!customizeStreamTx(tube_out, txStream))
				{
					//PLOG_ERROR(LOG_PREFIX, "associate_stream -- ID(%s): customize tx fail.", (const char*)m_name);
					//TubeManager::clear_tube(this, tube_out);
					tube_out->release();
					continue;
				}
			}
		}

		return tube_in;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoConferenceDirection::fillElementListRx(const char* termId, mg_rx_stream_t* rxStream, mg_tx_stream_t* txStream,
		mg_media_element_list_t& rxList, mg_media_element_list_t& txList)
	{
		rxList.clear();
		txList.clear();

		// prepare rx
		if (rxStream != nullptr)
		{
			rxList.copy_from(rxStream->getMediaParams().get_media_elements());
		}

		// prepare tx
		mg_media_element_t* element = txList.create(termId, 202, mg_media_element_list_t::POSITION_END);
		element->set_element_name("vconf_in");
		element->addParam("a=rtpmap: 202 VCONF_IN/90000");
		txList.set_priority_index(0);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoConferenceDirection::fillElementListTx(const char* termId,
		mg_rx_stream_t* rxStream, mg_tx_stream_t* txStream, mg_media_element_list_t& rxList, mg_media_element_list_t& txList)
	{
		rxList.clear();
		txList.clear();

		// prepare rx
		mg_media_element_t* element = rxList.create(termId, 203, mg_media_element_list_t::POSITION_END);
		element->set_element_name("vconf_out");
		element->addParam("a=rtpmap: 203 VCONF_OUT/90000");
		rxList.set_priority_index(0);

		// prepare tx
		if (txStream != nullptr)
		{
			txList.create_with_copy_from(txStream->getMediaParams().get_media_elements()->get_priority(), 0);
		}
	}
}
//-----------------------------------------------
