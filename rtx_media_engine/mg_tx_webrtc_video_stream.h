﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtp_channel.h"
#include "mg_tx_stream.h"

namespace mge
{
	class mg_tx_webrtc_video_stream_t : public mg_tx_stream_t
	{
	public:
		mg_tx_webrtc_video_stream_t(rtl::Logger* log, uint32_t id, uint32_t parent_id, const char* term_id);
		virtual ~mg_tx_webrtc_video_stream_t();

		virtual void send(const INextDataHandler* out_handler, const rtp_packet* packet);
		virtual void send(const INextDataHandler* out_handler, const uint8_t* data, uint32_t len);
		virtual void send(const INextDataHandler* out_handler, const rtcp_packet_t* packet);

		void set_channel_event(i_tx_channel_event_handler* rtp);
		void set_remote_address(const socket_address* address) { m_remote_address.copy_from(address); }
		const socket_address* get_remote_address() { return &m_remote_address; }
		void set_remote_rtcp_address(const socket_address* address) { m_remote_rtcp_address.copy_from(address); }
		uint32_t get_ssrc_to_rtp() { return m_ssrc_to_rtp; }

		virtual void onReceiverStarted();

	private:
		void send_packet_to_rtp_lock(const INextDataHandler* out_handler, const rtp_packet* packet);
		void send_data_to_rtp_lock(const INextDataHandler* out_handler, const uint8_t* data, uint32_t len);
		void send_ctrl_to_rtp_lock(const INextDataHandler* out_handler, const rtcp_packet_t* packet);

		void recalc_packet(const rtp_packet* packet);
	private:
		//-----------------------------------------------------
		// переменные для работы с пакетами на отправку в сеть.

		i_tx_channel_event_handler*	m_rtp;

		socket_address m_remote_address;			// адрес получателя пакета.
		socket_address m_remote_rtcp_address;		// адрес получателя rtcp пакета.

		bool m_recalc_packets;
		uint32_t m_ssrc_to_rtp;
		uint16_t m_seqno;
		uint32_t m_timestamp;
		bool m_first_packet;
		uint32_t m_in_recvtime_last;		// время получения последнего пакета
		uint32_t m_in_ssrc_last;			// текущий входящий ssrc
		// packets recalculation -- 90000 / 1000 = 90 ticks per millisecond
		int16_t m_in_seqno_diff;			// разница между входящим и исходящим seq no
		int16_t m_in_ts_diff;				// разница между входящим и исходящим timestamp
		//-----------------------------------------------------

		volatile bool m_storeFirstPackets;	// Флаг: режим сохранения первых пакетов
		volatile bool m_receiverStarted;	// Флаг: признак получения пакетов от другой стороны
		volatile bool m_waitReceiver;		// Флаг: ожидание события от получателя...

		rtl::ArrayT<rtp_packet*> m_sendQueue;
	};
}
//-----------------------------------------------
