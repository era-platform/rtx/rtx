﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "rtx_media_engine.h"
#include "std_string.h"
#include "std_array_templates.h"
#include "mg_audio_analyzer.h"
#include "mg_fax_t38.h"

namespace mge
{
	//------------------------------------------------------------------------------
	// RTX-97
	//------------------------------------------------------------------------------
	struct mg_splitter_params_t
	{
		bool vad = false;
		uint32_t vad_channels = 1;
		uint32_t vad_rate = 8000;
		void* event_handler = nullptr;
	};
	//------------------------------------------------------------------------------
	// Аналог пайлоад элемента. В отличии от пайлоад элемента может применятся нетолько в сдп.
	// Содержит всю информацию о конкретном пайлоад эелементе в сдп.
	//------------------------------------------------------------------------------
	class RTX_MGE_API mg_media_element_t
	{
		mg_media_element_t(const mg_media_element_t&);
		mg_media_element_t&	operator=(const mg_media_element_t&);

	public:
		static const int NULL_ELEMENT = UINT16_MAX;

		// Каждый элемент принадлежит конкретному терминатору. И имеет идентификаитор.
		// Для однозначности идентификатор соответствует пайлоад элементу.
		mg_media_element_t(const char* termId, uint16_t payloadId, void* ud);
		~mg_media_element_t() { clear(); }

		void clear();

		const uint16_t get_payload_id() const { return m_payload_id; }
		const rtl::String& get_termination_id() const { return m_termination; }
		const rtl::String& get_element_name() const { return m_name; }
		void set_element_name(const char* name) { m_name = name; m_dtmf = m_name *= "telephone-event"; }


		// Все параметры элемента хранятся в строковом состоянии. 
		// В таком виде в котором они пришли из сдп.
		const int getParamCount() const { return m_param_list.getCount(); }
		rtl::String getParam(int index) const { return m_param_list[index]; }
		bool addParam(const char* param);
		void deleteParam(int index) { m_param_list.removeAt(index); }
		void clearParams() { m_param_list.clear(); }


		const bool isDtmf() const { return m_dtmf; }
		const bool isNull () const { return m_payload_id == NULL_ELEMENT; }
		const bool isHandmade() const { return m_handmade; }
		void setHandmade(bool value) { m_handmade = value; }

		// RTX-97
		bool needSplitter() { return m_splitter_params.vad; }
		void setSplitterParams(const mg_splitter_params_t* params)
		{
			m_splitter_params.vad = params->vad;
			m_splitter_params.event_handler = params->event_handler;
		}
		mg_splitter_params_t* get_splitter_params() { return &m_splitter_params; }

		//
		bool compare_with(mg_media_element_t* element) const;
		bool copy_from(mg_media_element_t* element);

		// Подготовка параметров для передачи одной строкой в декодер(энкодер).
		rtl::String prepareParam() { return m_name *= "speex" ? prepare_param_for_speex() : rtl::String::empty; }

		uint32_t get_sample_rate();
		void set_sample_rate(uint32_t sample_rate);

		uint32_t getChannelCount();

		uint32_t get_ptime();
		void set_ptime(uint32_t ptime);

		// Получить всю строку fmtp из сдп по текущему элементу.
		rtl::String get_param_fmtp();
		// Получить всю строку rtpmap из сдп по текущему элементу.
		rtl::String get_param_rtpmap();

		mg_fax_t38_params_t* get_fax_param() const;
		void set_fax_param(mg_fax_t38_params_t* param);
		bool write_fax_param(rtl::String& ss) const;

	private:
		rtl::String prepare_param_for_speex();

		// Парсим строку rtpmap и заполняем локальные переменные.
		void parse_rtpmap();
		// Создаем строку rtpmap по локальным переменным.
		void write_rtpmap();
		// Парсим строку rtpmap и заполняем локальные переменные.
		void parse_ptime();

		// Парсим строку t38 и заполняем параметры.
		void parse_t38(const rtl::String&media_line);

		void update_samplerate(int rate);

	private:
		uint16_t m_payload_id;
		rtl::String m_name;

		// К какому терминатору принадлежит данный элемент.
		rtl::String m_termination;

		rtl::StringList m_param_list;

		// флаг указывающи на то что текущий элемен dtmf.
		bool m_dtmf;

		// флаг указывающи на то что текущий элемен был создан нами и не соответствует сдп.
		bool m_handmade;

		uint32_t m_sample_rate;
		uint32_t m_channels;
		uint32_t m_ptime;

		mg_fax_t38_params_t* m_fax_param;

		// RTX-97
		mg_splitter_params_t m_splitter_params;
	};
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	class RTX_MGE_API mg_media_element_list_t
	{
		mg_media_element_list_t(const mg_media_element_list_t&);
		mg_media_element_list_t& operator=(const mg_media_element_list_t&);

	public:
		mg_media_element_list_t();
		~mg_media_element_list_t();

		static const int POSITION_END = -1;

		// Создает элемент по пайлоад айди и добавляет его в список по позиции.
		mg_media_element_t* create(const char* term_id, uint16_t payload_id, int position);
		// Создает копию элемента и добавляет его в список по позиции.
		bool create_with_copy_from(mg_media_element_t* element, int position);
		const int getCount() const;
		mg_media_element_t* getAt(int index) const;
		void removeAt(int index);

		mg_media_element_t* get_priority() const;
		void set_priority_index(uint32_t index);

		void clear();

		// Пробегает по всему списку элементов и сравнивает параметры с переданной строкой.
		// Возвращает true при полном соответствии строки с параметрами элемента.
		bool compare_sdp_media_param(const rtl::String&media_line);

		mg_media_element_t* find(uint16_t payload_id);
		mg_media_element_t* find(const char* encoding);

		bool copy_from(const mg_media_element_list_t* element_list);

		// Возврящает айди терминатора к которому принадлежит данный список.
		const rtl::String& get_termination_id() const;

		const rtl::String to_string();

	private:
		rtl::ArrayT<mg_media_element_t*> m_elements;
		int m_priority_element_index;
	};
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_media_element_t*	mg_create_media_element_cn(const char* termination_id, mg_media_element_list_t* list);
	//------------------------------------------------------------------------------
	mg_media_element_t*	mg_create_media_element_audio_conf_recorder(const char* termination_id, mg_media_element_list_t* list);
	mg_media_element_t*	mg_create_media_element_video_conf_recorder(const char* termination_id, mg_media_element_list_t* list);
	//------------------------------------------------------------------------------
	mg_media_element_t*	mg_create_media_element_audio_conf_in(const char* termination_id, mg_media_element_list_t* list);
	//------------------------------------------------------------------------------
	mg_media_element_t*	mg_create_media_element_audio_conf_out(const char* termination_id, mg_media_element_list_t* list);
	//------------------------------------------------------------------------------
	mg_media_element_t*	mg_create_media_element_video_conf_in(const char* termination_id, mg_media_element_list_t* list);
	//------------------------------------------------------------------------------
	mg_media_element_t*	mg_create_media_element_video_conf_out(const char* termination_id, mg_media_element_list_t* list);
	//------------------------------------------------------------------------------
	mg_media_element_t*	mg_create_media_element_ivr(const char* termination_id, mg_media_element_list_t* list);
	//------------------------------------------------------------------------------
	mg_media_element_t* mg_create_media_element_t30(const char* termination_id, mg_media_element_list_t* list);
	//------------------------------------------------------------------------------
	mg_media_element_t* mg_create_media_element_t38(const char* termination_id, mg_media_element_list_t* list, bool from_sdp);
	//------------------------------------------------------------------------------
	mg_media_element_t*	mg_create_media_element_dummy(const char* termination_id, mg_media_element_list_t* list);
}
//------------------------------------------------------------------------------
