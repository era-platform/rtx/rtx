﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtp_channel.h"
#include "rtp_packet.h"
#include "rtcp_packet.h"
#include "rtcp_monitor.h"
#include "mg_tube.h"
#include "mg_sdp_base.h"
#include "rtp_statistics.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	enum mg_rx_stream_type_t
	{
		mg_rx_simple_audio_e,
		mg_rx_device_audio_e,
		mg_rx_dummy_e,
		mg_rx_simple_video_e,
		mg_rx_webrtc_video_e,
		mg_rx_fax_t30_e,
		mg_rx_fax_t38_e,
		mg_rx_fax_proxy_e,
		mg_rx_ivr_e,
	};

	const char* mg_rx_stream_type_toString(mg_rx_stream_type_t type);

	class MediaSession;
	//------------------------------------------------------------------------------
	// Однонаправленный поток из канала в тубы.
	//------------------------------------------------------------------------------
	class mg_rx_stream_t : public rtcp_monitor_event_handler_rx_t
	{
	public:
		mg_rx_stream_t(rtl::Logger* log, uint32_t id, mg_rx_stream_type_t type, const char* term_id, MediaSession& session);
		virtual ~mg_rx_stream_t();

		virtual bool mg_stream_set_LocalControl_Mode(h248::TerminationStreamMode mode);
		virtual bool mg_stream_set_LocalControl_ReserveValue(bool value);
		virtual bool mg_stream_set_LocalControl_ReserveGroup(bool value);
		virtual bool mg_stream_set_LocalControl_Property(const h248::Field* LC_property);

		virtual void mg_termination__dtmf_event(char dtmf, int duration);
		virtual void mg_termination__vad_event(bool vad, int volume);
		virtual void mg_termination__no_voice_event(int duration);
		virtual void mg_termination__voice_restored_event();

		virtual bool lock();
		virtual void unlock();
		
		virtual void setMediaParams(mg_sdp_media_base_t& media);
		
		mg_sdp_media_base_t& getMediaParams() { return m_media_sdp; }
		mg_rx_stream_type_t getType() const { return m_type; }
		const uint32_t getId() const { return m_id; }
		const char* getName() const { return m_name; }
		const char* getTag() const { return m_logTag; }
		MediaSession& getSession() { return m_session; }

		void subscribe_tube(Tube* tube);
		bool unsubscribe_tube(uint32_t tubeId);
		
		void set_rtcp_monitor(rtcp_monitor_t* monitor) { m_rtcp_monitor = monitor; }
		void add_statistics(stream_statistics_t* stat) { m_stat = stat; }

	private:
		//------------------------------------------------------------------------------
		// << rtcp_monitor_event_handler_rx_t >>
		virtual int rtcp_session_rx(const rtcp_packet_t* packet);
		//------------------------------------------------------------------------------

	protected:
		void setName(const char* name) { m_name = name; }
		void setTag(const char* tag) { strcpy(m_logTag, tag); }

		int getTubeCount() { return m_tubes.getCount(); }
		Tube* getTube(int index) { return m_tubes.getAt(index); }
		h248::TerminationStreamMode getState() { return m_local_control.stream_mode; }
		
		bool isLocked() { return m_main_lock; }
		bool checkAndLock() { return m_rx_lock.test_and_set(std::memory_order_acquire); }
		void releaseLock() { m_rx_lock.clear(std::memory_order_release); }

		void rtcp_analize(const rtcp_packet_t* packet);
		void rtcp_analize(const rtp_packet* packet);

		void stat_analize(const rtp_packet* packet);
		void stat_out_of_order();
		void stat_lost();
		void stat_invalid();
		void stat_filtered();

	protected:
		rtl::Logger* m_log;

	private:
		mg_rx_stream_type_t m_type;
		uint32_t m_id;
		rtl::String m_name;
		char m_logTag[9];
		MediaSession& m_session;
		
		volatile bool m_main_lock;
		std::atomic_flag m_rx_lock;

		// структура контроля - описание как ведет себя стрим, в каком он состоянии.. и т.д.
		volatile h248::TerminationLocalControl m_local_control;
		rtcp_monitor_t* m_rtcp_monitor;
		stream_statistics_t* m_stat;
		rtl::ArrayT<Tube*> m_tubes;			// переменные для работы с пакетами на отправку в туб.
		mg_sdp_media_base_t m_media_sdp;	// Список медиа элементов с которым работает данный стрим.
	};
}
//------------------------------------------------------------------------------
