﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

//#include "mg_conference_recorder_tube.h"
//#include "mg_media_element.h"
////------------------------------------------------------------------------------
////
////------------------------------------------------------------------------------
//#define LOG_PREFIX "CONF"
////------------------------------------------------------------------------------
////
////------------------------------------------------------------------------------
//mg_conference_record_tube_t::mg_conference_record_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id) :
//	Tube(log)
//{
//	m_id = id;
//
//	m_name << parent_id << "-conf-rec-tu" << id; // Conference Recorder Tube
//
//	//m_ssrc_to_rtp = (rtl::DateTime::getTicks() + rand()) | 0x833000b0;
//	m_seq_num_to_rtp = (uint16_t)(rtl::DateTime::getTicks() ^ 0x000000D5D);;
//	m_seq_num_to_rtp_last = 0;
//	m_timestamp_to_rtp = rtl::DateTime::getTicks() & 0x005D5D5D;
//	m_timestamp_to_rtp_last = 0;
//
//	m_recorder = nullptr;
//
//	rtl::res_counter_t::add_ref(g_mge_tube_counter);
//}
////------------------------------------------------------------------------------
////
////------------------------------------------------------------------------------
//mg_conference_record_tube_t::~mg_conference_record_tube_t()
//{
//	unsubscribe(this);
//
//	if (m_recorder != nullptr)
//	{
//		m_recorder->stop();
//		m_recorder->close();
//		DELETEO(m_recorder);
//		m_recorder = nullptr;
//	}
//
//	rtl::res_counter_t::release(g_mge_tube_counter);
//}
////------------------------------------------------------------------------------
////
////------------------------------------------------------------------------------
//bool mg_conference_record_tube_t::init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out)
//{
//	if (payload_el_out == nullptr)
//	{
//		PLOG_TUBE_ERROR(LOG_PREFIX, "mg_conference_record_tube_t::init -- payload element is null.");
//		return false;
//	}
//	m_payload_id = payload_el_out->get_payload_id();
//	PLOG_TUBE_WRITE(LOG_PREFIX, "mg_conference_record_tube_t::init -- payload element: %d", m_payload_id);
//	return true;
//}
////------------------------------------------------------------------------------
////
////------------------------------------------------------------------------------
//bool mg_conference_record_tube_t::create_recorder(const char* path, const media::PayloadSet& record_formats)
//{
//	if (path == nullptr)
//	{
//		PLOG_TUBE_WRITE(LOG_PREFIX, "create_recorder -- ID(%s): end (not record in config).", (const char*)m_name);
//		return false;
//	}
//
//	m_recorder = NEW media::LazyMediaWriterRTP(nullptr);
//	if (m_recorder == nullptr)
//	{
//		PLOG_TUBE_ERROR(LOG_PREFIX, "create_recorder -- ID(%s): failed to create audio recorder.", (const char*)m_name);
//		return false;
//	}
//
//	if (record_formats.getCount() == 0)
//	{
//		PLOG_TUBE_ERROR(LOG_PREFIX, "create_recorder -- ID(%s): record formats not found.", (const char*)m_name);
//		return false;
//	}
//
//	if (!m_recorder->create(path, record_formats))
//	{
//		PLOG_TUBE_WARNING(LOG_PREFIX, "create_recorder -- ID(%s): create fail.", (const char*)m_name);
//		return false;
//	}
//
//	if (!m_recorder->start())
//	{
//		PLOG_TUBE_WARNING(LOG_PREFIX, "create_recorder -- ID(%s): start fail.", (const char*)m_name);
//		return false;
//	}
//
//	PLOG_TUBE_WRITE(LOG_PREFIX, "create_recorder -- ID(%s): end.", (const char*)m_name);
//	return true;
//}
////------------------------------------------------------------------------------
////
////------------------------------------------------------------------------------
//void mg_conference_record_tube_t::send(const INextDataHandler* out_handler, const rtp_packet* packet)
//{
//	if (packet == nullptr)
//	{
//		PLOG_TUBE_ERROR(LOG_PREFIX, "send -- ID(%s): nothing send.", (const char*)m_name);
//		return;
//	}
//
//	if (m_recorder == nullptr)
//	{
//		PLOG_TUBE_ERROR(LOG_PREFIX, "send -- ID(%s): recorder is null.", (const char*)m_name);
//		return;
//	}
//
//	rtp_packet new_packet;
//	if (packet->get_payload_type() == 13)
//	{
//		new_packet.set_payload_type(13);
//	}
//	else
//	{
//		new_packet.set_payload_type(uint8_t(m_payload_id));
//		new_packet.set_payload(packet->get_payload(), packet->get_payload_length());
//	}
//
//	new_packet.set_version(2);
//	if ((m_seq_num_to_rtp_last == 0) || (m_timestamp_to_rtp_last == 0))
//	{
//		new_packet.set_marker(true);
//	}
//
//	uint32_t samples = packet->get_samples();
//
//	//new_packet.set_ssrc(m_ssrc_to_rtp);
//	new_packet.set_samples(samples);
//
//	m_seq_num_to_rtp_last = m_seq_num_to_rtp;
//	m_seq_num_to_rtp++;
//	new_packet.set_sequence_number(m_seq_num_to_rtp);
//
//	m_timestamp_to_rtp_last = m_timestamp_to_rtp;
//	m_timestamp_to_rtp += samples;
//	new_packet.set_timestamp(m_timestamp_to_rtp);
//
//	PLOG_TUBE_WRITE(LOG_PREFIX, "ID(%s) : write packet to recorder.", (const char*)m_name);
//
//	m_recorder->write_packet(&new_packet);
//}
////------------------------------------------------------------------------------
////
////------------------------------------------------------------------------------
//const uint32_t mg_conference_record_tube_t::get_id() const
//{
//	return m_id;
//}
////------------------------------------------------------------------------------
//const char*	mg_conference_record_tube_t::getName() const
//{
//	return m_name;
//}
////------------------------------------------------------------------------------
//const HandlerType mg_conference_record_tube_t::getType() const
//{
//	return HandlerType::ConfAudioRecorder;
//}
////------------------------------------------------------------------------------
////
////------------------------------------------------------------------------------
////const uint16_t  mg_conference_record_tube_t::get_payload_id() const
////{
////	return m_payload_id;
////}
////------------------------------------------------------------------------------
