﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_context.h"
#include "mg_common_termination.h"
#include "mg_device_termination.h"
#include "mg_webrtc_termination.h"
#include "mg_ivr_player_termination.h"
#include "mg_ivr_record_termination.h"
#include "mg_dummy_termination.h"
#include "mg_fax_termination.h"
#include "std_filesystem.h"

 //------------------------------------------------------------------------------
 //
 //------------------------------------------------------------------------------
#define LOG_PREFIX "MG-CTX" // Media Gateway Context
#define MAX_MG_TERMINATION_COUNT 512

#include "logdef.h"

#define CHECK_LOCKED(r) \
	if (!m_lock) \
	{ \
		ERROR_MSG("Context in invalid state (not locled)!"); \
		reply->addErrorDescriptor(h248::ErrorCode::c500_Internal_software_failure_in_the_MG_or_MGC, "Context in invalid state (not locked)"); \
		return r; \
	}

#define GET_TERMINATION(termId, r) \
	Termination* mg_term = findTermination(uint32_t(termId)); \
	if (mg_term == nullptr) \
	{ \
		ERROR_MSG("termination not found!"); \
		reply->addErrorDescriptor(h248::ErrorCode::c430_Unknown_TerminationID, "Termination not found in internal context"); \
		return r; \
	}

namespace mge
{

	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	MediaContext::MediaContext(rtl::Logger* log, uint32_t ctxId, uint32_t gateId) :
		m_log(log),
		m_id(ctxId)
	{
		m_termination_id_counter = 0;
		m_processor_id_counter = 0;

		m_name << gateId << "-c" << m_id;

		m_megaco_handler = nullptr;
		m_lock = true;
		m_mode = MediaContextMode::PeerToPeer;
		m_video_conf = false;
		m_conf_sample_rate = 8000;
		m_conf_ptime = 20;
		m_conf_audio_analyzer = nullptr;
		m_conf_video_selector = nullptr;
		m_new_record_path = true;
		m_dummy_termination = nullptr;

		rtl::res_counter_t::add_ref(g_mge_context_counter);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	MediaContext::~MediaContext()
	{
		m_lock = true;
	
		destroyAllProcessors();
		destroyAllTerminations();

		if (m_dummy_termination != nullptr)
		{
			m_dummy_termination->mg_termination_destroy(nullptr);
			DELETEO(m_dummy_termination);
			m_dummy_termination = nullptr;
		}

		if (m_conf_audio_analyzer != nullptr)
		{
			m_conf_audio_analyzer->cleanup();
			DELETEO(m_conf_audio_analyzer);
			m_conf_audio_analyzer = nullptr;
		}

		if (m_conf_video_selector != nullptr)
		{
			DELETEO(m_conf_video_selector);
			m_conf_video_selector = nullptr;
		}

		rtl::res_counter_t::release(g_mge_context_counter);
	}
	//------------------------------------------------------------------------------
	// 
	//------------------------------------------------------------------------------
	bool MediaContext::mg_context_initialize(h248::ITerminationEventHandler* handler, h248::Field* reply)
	{
		ENTER();

		m_megaco_handler = handler;

		if (m_dummy_termination == nullptr)
		{
			m_dummy_termination = NEW DummyTermination(m_log, h248::TerminationID(), (const char*)m_name);
		}

		bool result = m_dummy_termination->mg_termination_initialize(reply);
		
		if (!result)
		{
			DELETEO(m_dummy_termination);
			m_dummy_termination = nullptr;
		}

		RETURN_BOOL(result);
	}
	uint32_t MediaContext::mg_context_get_id()
	{
		return m_id;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	uint32_t MediaContext::mg_context_add_termination(h248::TerminationID& termId, h248::Field* reply)
	{
		char tmp[128];
		ENTER_FMT("termId:%s", termId.toString(tmp, 128));

		CHECK_LOCKED(MG_TERM_INVALID);

		Termination* termination = createTermination(termId, reply);
		
		if (termination == nullptr)
		{
			ERROR_MSG("create_termination FAIL!");
			return MG_TERM_INVALID;
		}

		if (!termination->mg_termination_initialize(reply))
		{
			ERROR_MSG("mg_termination_initialize FAIL!");
			destroyTermination(termination->getId());
			return MG_TERM_INVALID;
		}

//		if (m_mode == MediaContextMode::Conference)
//		{
//			h248::Field node("mode", "conf");
//			if (!termination->mg_termination_set_TerminationState_Property(&node, reply))
//			{
//				PLOG_ERROR(LOG_PREFIX, "ID(%s) : %s : mg_termination_set_TerminationState_Property() failed!", (const char*)m_name, __FUNCTION__);
//				destroy_termination(termination->getId());
//				return MG_TERM_INVALID;
//			}
//		}

		RETURN_FMT(termination->getId(), "%s", termination->getName());
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void MediaContext::mg_context_remove_termination(uint32_t termId)
	{
		ENTER_FMT("termId:%u", termId);

		if (!m_lock) 
		{
			ERROR_MSG("Context not locked!");
			return;
		}

		destroyTermination(uint32_t(termId));

		LEAVE();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool MediaContext::mg_termination_lock(uint32_t termId)
	{
		//ENTER_FMT("termId: %u", termId);

		if (!m_lock)
		{
			ERROR_MSG("Context not locked!");
			return false;
		}

		Termination* mg_term = findTermination(uint32_t(termId));
		if (mg_term == nullptr)
		{
			ERROR_MSG("termination not found");
			return false;
		}

		bool result = mg_term->mg_termination_lock();

		//RETURN_BOOL(result);

		return result;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool MediaContext::mg_termination_unlock(uint32_t termId)
	{
		//ENTER_FMT("termId:%u", termId);

		if (!m_lock)
		{
			ERROR_MSG("Context not locked!");
			return false;
		}

		Termination* mg_term = findTermination(uint32_t(termId));
		if (mg_term == nullptr)
		{
			ERROR_MSG("termination not found!");
			return false;
		}

		bool result = mg_term->mg_termination_unlock();

		if (result && m_termination_list.getCount() == 2)
		{
			Termination* t1 = m_termination_list.getAt(0);
			Termination* t2 = m_termination_list.getAt(1);
			if ((t1->getType() == h248::TerminationType::WebRTC) &&
				(t2->getType() == h248::TerminationType::WebRTC))
			{
				load_rtcp_ssrc_for_webrtc(t1, t2);
			}
		}

		//RETURN_BOOL(result);
		return result;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool MediaContext::mg_context_set_topology(const rtl::ArrayT<h248::TopologyTriple>& triple_list, h248::Field* reply)
	{
		ENTER();

		CHECK_LOCKED(false);

		if (triple_list.getCount() == 0)
		{
			ERROR_MSG("empty triple list!");
			reply->addErrorDescriptor(h248::ErrorCode::c473_Conflicting_property_values, "Empty triple list!");
			return false;
		}

		for (int index_topology = 0; index_topology < triple_list.getCount(); index_topology++)
		{
			h248::TopologyTriple topology = triple_list.getAt(index_topology);

			Termination* mg_term_ptr_from = findTermination(uint32_t(topology.term_from));
			if (mg_term_ptr_from == nullptr)
			{
				ERROR_MSG("not found termination 'from'");
				reply->addErrorDescriptor(h248::ErrorCode::c430_Unknown_TerminationID, "Termination not found in internal context");
				return false;
			}
			mg_term_ptr_from->mg_termination_add_topology(topology);

			Termination* mg_term_ptr_to = findTermination(uint32_t(topology.term_to));
			if (mg_term_ptr_to == nullptr)
			{
				ERROR_MSG(" not found termination 'to'.");
				reply->addErrorDescriptor(h248::ErrorCode::c430_Unknown_TerminationID, "Termination not found in internal context");
				return false;
			}
			mg_term_ptr_to->mg_termination_add_topology(topology);
		}
	

		for (int i = 0; i < m_processor_list.getCount(); i ++)
		{
			m_processor_list[i]->updateTopology(triple_list);
		}

		RETURN_BOOL(true);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool MediaContext::mg_context_set_property(const h248::Field* prop, h248::Field* reply)
	{

		const rtl::String& propName = prop->getName();
		const rtl::String& propValue = unescape_rfc3986(prop->getValue());

		ENTER_FMT("name:%s, value:%s", (const char*)propName, (const char*)propValue);

		CHECK_LOCKED(false)

		//mode=conf,
		if (propName == "mode")
		{
			if (propValue == "conf")
			{
				m_mode = MediaContextMode::Conference;
			}

			return true;
		}

		//recpath=Srv-005/2016-02-19/10-58
		if (propName == "recpath")
		{
			m_record_path = get_local_path(propValue);
			m_new_record_path = true;

			return true;
		}

		//timestamp=1455879486220
		if (propName == "timestamp")
		{
			m_timestamp_record = propValue;
			m_new_record_path = true;

			return true;
		}

		rtl::StringList path;
		propName.split(path, '/');
		int parts = path.getCount();

		//conf/*,
		if (parts > 1 && path[0] == "conf")
		{
			//conf/freq=8000,
			if (path[1] == "freq")
			{
				m_conf_sample_rate = strtoul(propValue, nullptr, 10);

				if (m_conf_sample_rate == 0)
					m_conf_sample_rate = 8000;

			}
			//conf/video=true,
			else if (parts == 2 && path[1] == "video" && propValue == "true")
			{
				m_video_conf = true;
				m_conf_audio_analyzer = NEW AudioAnalyzer(m_log);
				m_conf_video_selector = NEW VideoSelector();
				m_conf_audio_analyzer->initialize(m_conf_video_selector, AudioAnalyzerParams());
			}
			//conf/video/schema=e30g,
			else if (parts == 3 && path[1] == "video" && path[2] == "schema")
			{
				int text_size = Base64_CalcDecodedSize(propValue.getLength());
				char* text = NEW char[text_size + 1];
				int len = Base64Decode(propValue, propValue.getLength(), (uint8_t*)text, text_size);
				m_conf_video_schema.assign(text, len);
				DELETEAR(text);

				PRINT_FMT("conf/video/schema:\r\n%s", (const char*)m_conf_video_schema);

				if (m_conf_video_schema.getLength() > 0)
				{
					// we hope to get video_conference processor
					for (int i = 0; i < m_processor_list.getCount(); i++)
					{
						m_processor_list[i]->updateConfSchema(m_conf_video_schema);
					}
				}
			}
		}

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	Termination* MediaContext::findTermination(uint32_t termId)
	{
		for (int i = 0; i < m_termination_list.getCount(); i++)
		{
			Termination* termination = m_termination_list.getAt(i);
			if (termination != nullptr && termination->getId() == termId) 
			{
				return termination;
			}
		}

		return nullptr;
	}
	//------------------------------------------------------------------------------
	// Last error
	//------------------------------------------------------------------------------
	Termination* MediaContext::createTermination(h248::TerminationID& termId, h248::Field* reply)
	{
		if (m_termination_list.getCount() + 1 >= MAX_MG_TERMINATION_COUNT) 
		{
			ERROR_MSG("the maximum number of termination!");
			reply->addErrorDescriptor(h248::ErrorCode::c434_Max_number_of_terminations_in_a_context_exceeded);
			return nullptr;
		}

		m_termination_id_counter++;
		if (m_termination_id_counter == MG_TERM_ERR_RES)
		{
			m_termination_id_counter = 1;
		}

		termId.setId(m_termination_id_counter);

		Termination* termination = nullptr;

		switch (termId.getType())
		{
		case h248::TerminationType::IVR_Record:
			termination = NEW IVRRecorderTermination(m_log, termId, m_name);
			break;
		case h248::TerminationType::IVR_Player:
			termination = NEW IVRPlayerTermination(m_log, termId, m_name);
			break;
		case h248::TerminationType::Fax_T30:
		case h248::TerminationType::Fax_T38:
			termination = NEW FaxTermination(m_log, termId, m_name);
			break;
		case h248::TerminationType::Device:
			termination = NEW DeviceTermination(m_log, termId, m_name);
			break;
		case h248::TerminationType::WebRTC:
			termination = NEW WebRTCTermination(m_log, termId, m_name);
			makeRecordPath(termination);
			break;
		case h248::TerminationType::RTP:
			termination = NEW TerminationRTP(m_log, termId, m_name);
			makeRecordPath(termination);
			break;
		default:
			ERROR_FMT("unknown termination type: %s(%d).", h248::TerminationType_toString(termId.getType()), termId.getType());
			reply->addErrorDescriptor(h248::ErrorCode::c430_Unknown_TerminationID, "Unknow termination type requested");
			return nullptr;
		}

		termination->mg_termination_set_event_handler(m_megaco_handler, m_id);

		m_termination_list.add(termination);

		rtl::Thread::reset_error();

		RETURN_FMT(termination, "%s.", termination->getName());
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void MediaContext::destroyTermination(uint32_t termId) 
	{
		ENTER_FMT("termId:%u", termId);

		if (m_termination_list.getCount() == 0)
		{
			ERROR_MSG("termination list empty!");
			return;
		}

		bool find = false;

		for (int i = 0; i < m_termination_list.getCount(); i++) 
		{
			Termination* termination = m_termination_list.getAt(i);

			if (termination->getId() == termId)
			{
				for (int j = m_processor_list.getCount() - 1; j >= 0; j--)
				{
					MediaProcessor* mg_proc = m_processor_list[j];

					mg_proc->removeTermination(termId);
					if (mg_proc->getTerminationCount() == 0)
					{
						DELETEO(mg_proc);
						m_processor_list.removeAt(j);
					}
				}

				rtl::String termName = termination->getName();
				m_termination_list.removeAt(i);

				DELETEO(termination);
				termination = nullptr;

				PRINT_FMT("termination '%s' destroed.", (const char*)termName);

				find = true;
				break;
			}
		}

		if (find)
		{
			if ((m_termination_list.getCount() == 2) && (m_mode == MediaContextMode::PeerToPeer))
			{
				for (int k = 0; k < m_termination_list.getCount(); k++)
				{
					Termination* termination = m_termination_list.getAt(k);
					if (termination == nullptr)
					{
						continue;
					}

					for (int j = 0; j < m_processor_list.getCount(); j++)
					{
						MediaProcessor* mg_proc = m_processor_list[j];
						mg_proc->removeTermination(termination->getId());
						mg_proc->resetConference();
						if (!mg_proc->addTermination(termination, DirectionType::bothway))
						{
							//ERROR_FMT("transfer to bothway term:%s FAIL.", termination->getName());
						}
					}
				}
			}
		}
		else
		{
			ERROR_FMT("termination #%d not found.", termId);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void MediaContext::destroyAllTerminations() 
	{
		ENTER();

		for (int i = 0; i < m_termination_list.getCount(); i++)
		{
			Termination* termination = m_termination_list.getAt(i);
			if (termination == nullptr) 
			{
				continue;
			}

			for (int j = m_processor_list.getCount() - 1; j >= 0; j--)
			{
				MediaProcessor* mg_proc = m_processor_list[j];
				mg_proc->removeTermination(termination->getId());
				if (mg_proc->getTerminationCount() == 0)
				{
					DELETEO(mg_proc);
					m_processor_list.removeAt(j);
				}
			}

			DELETEO(termination);
			termination = nullptr;
		}

		m_termination_list.clear();

		LEAVE();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	MediaProcessor* MediaContext::createProcessor(uint32_t type)
	{
		ENTER_FMT("procType:%u", type);

		MediaProcessor* processor = nullptr;

		//if (type == 1)
		{
			m_processor_id_counter++;
			processor = NEW MediaProcessor(this, m_processor_id_counter, m_name);
		
			processor->setType(type);

			if (m_video_conf)
			{
				processor->setExtraEngine(type == 1 ?
					(void*)m_conf_audio_analyzer :
					type == 2 ?
						(void*)m_conf_video_selector :
						nullptr);
			}

			//if (prepare_record_path())
			//{
			//	processor->set_record_path(m_record_path_conference);
			//}
		}

	/*	if (processor == nullptr) 
		{
			ERROR_FMT("unknown termination type: %d).",(const char*) m_name, __FUNCTION__, type);
			return nullptr;
		}*/

		m_processor_list.add(processor);

		RETURN_FMT(processor, "processor '%s'", processor->getName());
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	MediaProcessor* MediaContext::findProcessor(uint32_t type)
	{
		for (int i = 0; i < m_processor_list.getCount(); i++) 
		{
			MediaProcessor* proc = m_processor_list[i];
			if (proc->getType() == type)
			{
				return proc;
			}
		}

		return nullptr;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void MediaContext::destroyProcessor(uint32_t procId)
	{
		ENTER_FMT("procId: %u", procId);

		for (int i = 0; i < m_processor_list.getCount(); i++) 
		{
			MediaProcessor* processor = m_processor_list[i];
			if (processor->getId() == procId)
			{
				PRINT_FMT("processor %s destroying...", processor->getName());
				
				DELETEO(processor);
				m_processor_list.removeAt(i);
				
				PRINT_FMT("processor %s destroyed.", processor->getName());

				return;
			}
		}

		WARNING_FMT("processor with id: %d not found.", procId);
	}
	//-------------------------------------------
	//
	//-------------------------------------------
	void MediaContext::destroyAllProcessors()
	{
		ENTER();

		for (int i = 0; i < m_processor_list.getCount(); i++)
		{
			DELETEO(m_processor_list[i]);
		}

		m_processor_list.clear();

		LEAVE();
	}
	//-------------------------------------------
	//
	//-------------------------------------------
	bool MediaContext::mg_termination_set_TerminationState(uint32_t termId, h248::TerminationServiceState state, h248::Field* reply)
	{
		ENTER();

		CHECK_LOCKED(false);

		GET_TERMINATION(termId, false);

		// there is no error in this implementation
		bool result = mg_term->mg_termination_set_TerminationState(state, reply);
	
		RETURN_BOOL(result);
	}
	//-------------------------------------------
	//
	//-------------------------------------------
	bool MediaContext::mg_termination_set_TerminationState_EventControl(uint32_t termId, bool eventBufferControl, h248::Field* reply)
	{
		ENTER();

		CHECK_LOCKED(false);

		GET_TERMINATION(termId, false);

		bool result = mg_term->mg_termination_set_TerminationState_EventControl(eventBufferControl, reply);

		RETURN_BOOL(result);
	}
	//-------------------------------------------
	//
	//-------------------------------------------
	bool MediaContext::mg_termination_set_TerminationState_Property(uint32_t termId, const h248::Field* property, h248::Field* reply)
	{
		ENTER();

		CHECK_LOCKED(false);

		GET_TERMINATION(termId, false);

		bool result = mg_term->mg_termination_set_TerminationState_Property(property, reply);

		RETURN_BOOL(result);
	}
	//-------------------------------------------
	//
	//-------------------------------------------
	bool MediaContext::mg_termination_set_LocalControl_Mode(uint32_t termId, uint16_t streamId, h248::TerminationStreamMode mode, h248::Field* reply)
	{
		ENTER();

		CHECK_LOCKED(false);

		GET_TERMINATION(termId, false);

		bool result = mg_term->mg_termination_set_LocalControl_Mode(streamId, mode, reply);

		RETURN_BOOL(result);
	}
	//-------------------------------------------
	//
	//-------------------------------------------
	bool MediaContext::mg_termination_set_LocalControl_ReserveValue(uint32_t termId, uint16_t streamId, bool value, h248::Field* reply)
	{
		ENTER();

		CHECK_LOCKED(false);

		GET_TERMINATION(termId, false);

		bool result = mg_term->mg_termination_set_LocalControl_ReserveValue(streamId, value, reply);

		RETURN_BOOL(result);
	}
	//-------------------------------------------
	//
	//-------------------------------------------
	bool MediaContext::mg_termination_set_LocalControl_ReserveGroup(uint32_t termId, uint16_t streamId, bool value, h248::Field* reply)
	{
		ENTER();

		CHECK_LOCKED(false);

		GET_TERMINATION(termId, false);

		bool result = mg_term->mg_termination_set_LocalControl_ReserveGroup(streamId, value, reply);

		RETURN_BOOL(result);
	}
	//-------------------------------------------
	//
	//-------------------------------------------
	bool MediaContext::mg_termination_set_LocalControl_Property(uint32_t termId, uint16_t streamId,const h248::Field* property, h248::Field* reply)
	{
		ENTER();

		CHECK_LOCKED(false);

		GET_TERMINATION(termId, false);

		bool result = mg_term->mg_termination_set_LocalControl_Property(streamId, property, reply);

		RETURN_BOOL(result);
	}
	//-------------------------------------------
	//
	//-------------------------------------------
	bool MediaContext::mg_termination_set_Local(uint32_t termId, uint16_t streamId, const rtl::String& local, rtl::String& localAnswer, h248::Field* reply)
	{
		ENTER();

		CHECK_LOCKED(false);

		GET_TERMINATION(termId, false);

		bool result = mg_term->mg_termination_set_Local(streamId, local, localAnswer, reply);

		RETURN_BOOL(result);
	}
	//-------------------------------------------
	//
	//-------------------------------------------
	bool MediaContext::mg_termination_set_Remote(uint32_t termId, uint16_t streamId, const rtl::String& remote, h248::Field* reply)
	{
		ENTER();

		CHECK_LOCKED(false);

		GET_TERMINATION(termId, false);

		bool result = mg_term->mg_termination_set_Remote(streamId, remote, reply);

		if (result)
		{
			MediaProcessor* processor = findProcessor(streamId);
			if (processor == nullptr)
			{
				processor = createProcessor(streamId);
				if (processor == nullptr)
				{
					ERROR_MSG("processor is null");
					return false;
				}
			}

			if (m_mode == MediaContextMode::Conference)
			{
				if (!processor->addTermination(mg_term, DirectionType::conference))
				{
					ERROR_MSG("processor add return error");
					return false;
				}
			}
			else
			{
				if (!processor->addTermination(mg_term, DirectionType::bothway))
				{
					ERROR_MSG("processor add return errror");
					return false;
				}
			}
		}

		RETURN_BOOL(result);
	}
	//-------------------------------------------
	//
	//-------------------------------------------
	bool MediaContext::mg_termination_set_Local_Remote(uint32_t termId, uint16_t streamId, const rtl::String& remote_sdp, const rtl::String& local_sdp_offer, h248::Field* reply)
	{
		ENTER();

		CHECK_LOCKED(false);

		GET_TERMINATION(termId, false);

		bool result = mg_term->mg_termination_set_Local_Remote(streamId, remote_sdp, local_sdp_offer, reply);

		if (result)
		{
			mg_termination_ready_analyzer transaction;
			memset(&transaction, 0, sizeof(mg_termination_ready_analyzer));
			transaction.term_id = termId;
			transaction.stream_id = streamId;
			transaction.remote_sdp_exists = true;
			m_transaction_ready.add(transaction);
		}

		RETURN_BOOL(result);
	}
	//-------------------------------------------
	//
	//-------------------------------------------
	bool MediaContext::mg_termination_get_Local(uint32_t termId, uint16_t streamId, rtl::String& local, h248::Field* reply)
	{
		ENTER();

		CHECK_LOCKED(false);

		GET_TERMINATION(termId, false);

		bool result = mg_term->mg_termination_get_Local(streamId, local, reply);
		
		RETURN_BOOL(result);
	}
	//-------------------------------------------
	//
	//-------------------------------------------
	bool MediaContext::mg_termination_set_events(uint32_t termId,uint16_t requestId, const h248::Field* propertySet, h248::Field* reply)
	{
		ENTER();

		CHECK_LOCKED(false);

		GET_TERMINATION(termId, false);

		bool result = mg_term->mg_termination_set_events(requestId, propertySet, reply);

		if (result)
		{
			mg_termination_ready_analyzer transaction;
			memset(&transaction, 0, sizeof(mg_termination_ready_analyzer));
			transaction.term_id = termId;
			transaction.stream_id = MG_STREAM_TYPE_AUDIO;
			transaction.need_build_chain = true;
			m_transaction_ready.add(transaction);
		}
		
		RETURN_BOOL(result);
	}
	//-------------------------------------------
	//
	//-------------------------------------------
	bool MediaContext::mg_termination_set_signals(uint32_t termId,uint16_t streamId, const h248::Field* property_set, h248::Field* reply)
	{
		ENTER();

		CHECK_LOCKED(false);

		GET_TERMINATION(termId, false);

		bool result = mg_term->mg_termination_set_signals(streamId, property_set, reply);

		RETURN_BOOL(result);
	}
	//-------------------------------------------
	//
	//-------------------------------------------
	bool MediaContext::mg_termination_set_property(uint32_t termId, const h248::Field* property, h248::Field* reply)
	{
		ENTER();

		CHECK_LOCKED(false);

		GET_TERMINATION(termId, false);

		bool result = mg_term->mg_termination_set_property(property, reply);
		
		RETURN_BOOL(result);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool MediaContext::mg_context_lock() 
	{
		//ENTER();

		m_lock = true;

		m_transaction_ready.clear();

		for (int i = 0; i < m_termination_list.getCount(); i++)
		{
			Termination* termination = m_termination_list.getAt(i);
			if (termination == nullptr)
			{
				WARNING_FMT("termination by index: %d is NULL", i);
				continue;
			}

			termination->mg_termination_lock();
		}
	
		//RETURN_BOOL(m_lock);

		return m_lock;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool MediaContext::mg_context_unlock()
	{
		if (!checkDummy())
		{
			return false;
		}

		//ENTER();

		for (int i = 0; i < m_termination_list.getCount(); i++)
		{
			Termination* termination = m_termination_list.getAt(i);
			if (termination == nullptr)
			{
				WARNING_FMT("termination with index: %d is NULL.", i);
				continue;
			}

			if ((termination->getType() == h248::TerminationType::IVR_Player) || 
				(termination->getType() == h248::TerminationType::IVR_Record) ||
				(termination->getType() == h248::TerminationType::Fax_T30) ||
				(termination->getType() == h248::TerminationType::Fax_T38))
			{
				int result = addSpecialTermination(termination);
				if (result < 0)
				{
					RETURN_BOOL(false);
				}
				else if (result == 0)
				{
					continue;
				}
			}
			else if (!maybe_add_to_direction(termination))
			{
				//RETURN_BOOL(false);
				return false;
			}

			if (!termination->mg_termination_unlock())
			{
				ERROR_FMT("%s not unlocked", termination->getName());
				continue;
			}
		}

		m_lock = false;

		//RETURN_BOOL(true); // m_lock = false, !m_lock == true!
		return true;
	}
	//------------------------------------------------------------------------------
	bool MediaContext::checkDummy()
	{
		if (m_mode == MediaContextMode::PeerToPeer)
		{
			if (m_termination_list.getCount() == 1)
			{
				if (m_processor_list.getCount() == 0)
				{
					Termination* termination = m_termination_list.getAt(0);
					if (termination == nullptr)
					{
						WARNING("termination by index: 0 is NULL.");
						return false;
					}

					uint32_t streamId = (termination->getType() == h248::TerminationType::Fax_T38) ? MG_STREAM_TYPE_IMAGE : MG_STREAM_TYPE_AUDIO;
					MediaProcessor* processor = findProcessor(streamId);
					if (processor == nullptr)
					{
						processor = createProcessor(streamId);
						if (processor == nullptr)
						{
							ERROR_MSG("processor is null");
							return false;
						}
					}

					if (!processor->addTermination(m_dummy_termination, DirectionType::bothway))
					{
						ERROR_MSG("processor add termination!");
						return false;
					}
				}
				else
				{
					for (int i = 0; i < m_processor_list.getCount(); i++)
					{
						MediaProcessor* processor = m_processor_list[i];
						if (!processor->addTermination(m_dummy_termination, DirectionType::bothway))
						{
							ERROR_MSG("processor add termination failed");
							return false;
						}
					}
				}
			}
			else
			{
				for (int i = 0; i < m_processor_list.getCount(); i++)
				{
					m_processor_list[i]->removeTermination(m_dummy_termination->getId());
				}
			}
		}

		return true;
	}
	//------------------------------------------------------------------------------
	int MediaContext::addSpecialTermination(Termination* termination)
	{
		uint32_t streamId = (termination->getType() == h248::TerminationType::Fax_T38) ? MG_STREAM_TYPE_IMAGE : MG_STREAM_TYPE_AUDIO;

		MediaProcessor* processor = findProcessor(streamId);
		if (processor == nullptr)
		{
			processor = createProcessor(streamId);
			if (processor == nullptr)
			{
				ERROR_MSG("processor is null");
				return -1;
			}
		}

		if (!termination->mg_termination_lock())
		{
			ERROR_FMT("%s not locked", termination->getName());
			return 0;
		}

		if (m_mode == MediaContextMode::Conference)
		{
			if (!processor->addTermination(termination, DirectionType::conference))
			{
				ERROR_MSG("processor add termination failed");
				return -1;
			}
		}
		else
		{
			if (!processor->addTermination(termination, DirectionType::bothway))
			{
				ERROR_MSG("processor add termination failed");
				return -1;
			}
		}

		if ((termination->getType() == h248::TerminationType::Fax_T30) ||
			(termination->getType() == h248::TerminationType::Fax_T38))
		{
			termination->mg_termination_set_LocalControl_Mode(streamId, h248::TerminationStreamMode::SendRecv, nullptr);
		}

		return 1;
	}
	//------------------------------------------------------------------------------
	bool MediaContext::maybe_add_to_direction(Termination* termination)
	{
		uint32_t type = 0;
	
		ENTER_FMT("term: %s", termination->getName());

		for (int k = 0; k < m_transaction_ready.getCount(); k++)
		{
			mg_termination_ready_analyzer tr = m_transaction_ready.getAt(k);
			if (tr.term_id == termination->getId())
			{
				if (!tr.already_added && (tr.remote_sdp_exists || tr.need_build_chain))
				{
					type = mg_sdp_media_base_t::stream_type(tr.stream_id);

					MediaProcessor* processor = findProcessor(type);

					if (processor == nullptr)
					{
						processor = createProcessor(type);
						if (processor == nullptr)
						{
							ERROR_MSG("processor is null");
							return false;
						}
					}

					if (m_mode == MediaContextMode::Conference)
					{
						if (tr.need_build_chain)
						{
							processor->removeTermination(termination->getId());
						}

						if (!processor->addTermination(termination, DirectionType::conference))
						{
							ERROR_MSG("processor add termination failed");
							return false;
						}
					}
					else
					{
						if (m_termination_list.getCount() != 1)
						{
							processor->removeTermination(m_dummy_termination->getId());
						}
						if (tr.need_build_chain)
						{
							processor->removeTermination(termination->getId());
						}

						if (!processor->addTermination(termination, DirectionType::bothway))
						{
							if (termination->getType() == h248::TerminationType::WebRTC)
							{
								// не добавился в дирекшн, потому что не готовы все стримы (нет данных по стриму 11),
								// т.к. remote sdp должна придти в следующей итерации, там он (терминэйшн) и добавится.
								return true;
							}
							PRINT("processor add termination failed -- it will be added when it received full sdp!");
							return false;
						}
					}
					m_transaction_ready.getAt(k).already_added = true;
				}
			}
		}

		RETURN_BOOL(true);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	/*void MediaContext::info()
	//{
	//	PLOG_CALL(LOG_PREFIX,"INFO -- ID(%s): --------------------------------------------------------",(const char*) m_name);

	//	PLOG_CALL(LOG_PREFIX,"INFO -- ID(%s): ------------------ terminations (%d) -------------------",(const char*) m_name, m_termination_list.getCount());

	//	const int buff_size = 200;
	//	char buff[buff_size] = { 0 };
	//	int last = 0;
	//	bool first = true;
	//	const int count = 4;
	//	int count_current = 0;
	//	for (int i = 0; i < m_termination_list.getCount(); i++)
	//	{
	//		Termination* term = m_termination_list.getAt(i);
	//		if (term == nullptr) 
	//		{
	//			continue;
	//		}

	//		if (!first)
	//		{
	//			buff[last] = ',';
	//			last++;
	//		}

	//		last += std_snprintf(buff + last, buff_size - last, "%s",term->getName());
	//		first = false;

	//		if ((count_current >= count) || ((i + 1) == m_termination_list.getCount())) 
	//		{
	//			buff[last] = 0;

	//			PLOG_CALL(LOG_PREFIX, "INFO -- ID(%s): %s",(const char*) m_name, buff);

	//			last = 0;
	//			count_current = 0;
	//		}
	//		count_current++;
	//	}

	//	PLOG_CALL(LOG_PREFIX,"INFO -- ID(%s): ------------------- processors (%d) --------------------",(const char*) m_name, m_processor_list.getCount());

	//	last = 0;
	//	first = true;
	//	count_current = 0;
	//	for (int i = 0; i < m_processor_list.getCount(); i++)
	//	{
	//		MediaProcessor* proc = m_processor_list[i];
	//		if (!first)
	//		{
	//			buff[last] = ',';
	//			last++;
	//		}

	//		last += std_snprintf(buff + last, buff_size - last, "%s", proc->getName());
	//		first = false;

	//		if ((count_current >= count) || ((i + 1) == m_processor_list.getCount()))
	//		{
	//			buff[last] = 0;

	//			PLOG_CALL(LOG_PREFIX, "INFO -- ID(%s): %s", (const char*)m_name, buff);

	//			last = 0;
	//			count_current = 0;
	//		}
	//		count_current++;
	//	}

	//	PLOG_CALL(LOG_PREFIX,"INFO -- ID(%s): --------------------------------------------------------.",(const char*) m_name);
	//}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	//void MediaContext::getIdRange(char* buff, int size) 
	//{
	//	uint32_t min_id = 0;
	//	uint32_t max_id = 0;

	//	for (int i = 0; i < m_termination_list.getCount(); i++)
	//	{
	//		Termination* term = m_termination_list.getAt(i);
	//		if (term != nullptr)
	//		{
	//			uint32_t id = term->getId();
	//			if (min_id > id)
	//			{
	//				min_id = id;
	//			}
	//			if (id > max_id)
	//			{
	//				max_id = id;
	//			}
	//		}
	//	}

	//	std_snprintf(buff, size, "term min:%d, term max:%d", min_id, max_id);
	//} */
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool MediaContext::prepareRecordPath()
	{
		if (!m_new_record_path)
		{
			return true;
		}

		if (m_record_path.isEmpty())
		{
			return false;
		}
		
		m_record_path.replace('/', FS_PATH_DELIMITER);
		rtl::String temp;
		temp << m_record_path << FS_PATH_DELIMITER;
		m_record_path = temp;
		
		if (!fs_check_path_and_create(m_record_path))
		{
			ERROR_FMT("directory create fail! (%s)", (const char*)m_record_path);
			return false;
		}

		//m_record_path_conference = rtl::String::empty;
		//m_record_path_conference = m_record_path;
		//m_record_path_conference << m_megaco_id << "-";
		//if (!m_timestamp_record.isEmpty())
		//{
		//	m_record_path_conference << m_timestamp_record;
		//}
		//m_record_path_conference << "-conf.rtp";

		m_new_record_path = false;

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void MediaContext::makeRecordPath(Termination* term)
	{
		if ((prepareRecordPath()) && (m_mode == MediaContextMode::PeerToPeer))
		{
			if (!m_record_path.isEmpty())
			{
				rtl::String path_rec = m_record_path;
				path_rec << m_id << "-";
				if (!m_timestamp_record.isEmpty())
				{
					path_rec << m_timestamp_record;
				}
				char tmp[128];
				rtl::String megaco_term_id = term->getTermId().toString(tmp, 128);
				megaco_term_id.replace('\\', '_');
				megaco_term_id.replace('/', '_');
				path_rec << "-" << megaco_term_id << ".rtp";
				term->set_rtp_record_file_path(path_rec);
			}
		}
	}
	//------------------------------------------------------------------------------
	// Пока пробегаем по всем каналам, собираем ssrc и передаем их во все rtcp мониторы.
	//------------------------------------------------------------------------------
	void MediaContext::load_rtcp_ssrc_for_webrtc(Termination* t1, Termination* t2)
	{
		mg_rtcp_triple_list trilist_1;
		MediaSession* audio_stream_1 = t1->findMediaSession(MG_STREAM_TYPE_AUDIO);
		MediaSession* video_stream_1 = t1->findMediaSession(MG_STREAM_TYPE_VIDEO);

		if (audio_stream_1 != nullptr)
		{
			for (int i = 0; i < audio_stream_1->get_rtcp_monitors()->getCount(); i++)
			{
				rtcp_monitor_t* mon = audio_stream_1->get_rtcp_monitors()->getAt(i);
				rtcp_triple_t tr;
				tr.ssrc = mon->get_ssrc();
				tr.ssrc_opposite = mon->get_ssrc_opposite();
				memcpy(tr.cname, mon->get_cname(), MD5_HASH_HEX_SIZE);
				tr.cname[MD5_HASH_HEX_SIZE] = 0;
				trilist_1.add(tr);

				PLOG_WRITE(LOG_PREFIX, "t1 -- | audio | %10u --> %10u", tr.ssrc, tr.ssrc_opposite);
			}
		}
		if (video_stream_1 != nullptr)
		{
			for (int i = 0; i < video_stream_1->get_rtcp_monitors()->getCount(); i++)
			{
				rtcp_monitor_t* mon = video_stream_1->get_rtcp_monitors()->getAt(i);
				rtcp_triple_t tr;
				tr.ssrc = mon->get_ssrc();
				tr.ssrc_opposite = mon->get_ssrc_opposite();
				memcpy(tr.cname, mon->get_cname(), MD5_HASH_HEX_SIZE);
				tr.cname[MD5_HASH_HEX_SIZE] = 0;
				trilist_1.add(tr);

				PLOG_WRITE(LOG_PREFIX, "t1 -- | video | %10u --> %10u", tr.ssrc, tr.ssrc_opposite);
			}
		}

		mg_rtcp_triple_list trilist_2;
		MediaSession* audio_stream_2 = t2->findMediaSession(MG_STREAM_TYPE_AUDIO);
		MediaSession* video_stream_2 = t2->findMediaSession(MG_STREAM_TYPE_VIDEO);

		if (audio_stream_2 != nullptr)
		{
			for (int i = 0; i < audio_stream_2->get_rtcp_monitors()->getCount(); i++)
			{
				rtcp_monitor_t* mon = audio_stream_2->get_rtcp_monitors()->getAt(i);
				rtcp_triple_t tr;
				tr.ssrc = mon->get_ssrc();
				tr.ssrc_opposite = mon->get_ssrc_opposite();
				memcpy(tr.cname, mon->get_cname(), MD5_HASH_HEX_SIZE);
				tr.cname[MD5_HASH_HEX_SIZE] = 0;
				trilist_2.add(tr);

				PLOG_WRITE(LOG_PREFIX, "t2 -- | audio | %10u --> %10u", tr.ssrc, tr.ssrc_opposite);
			}
		}
		if (video_stream_2 != nullptr)
		{
			for (int i = 0; i < video_stream_2->get_rtcp_monitors()->getCount(); i++)
			{
				rtcp_monitor_t* mon = video_stream_2->get_rtcp_monitors()->getAt(i);
				rtcp_triple_t tr;
				tr.ssrc = mon->get_ssrc();
				tr.ssrc_opposite = mon->get_ssrc_opposite();
				memcpy(tr.cname, mon->get_cname(), MD5_HASH_HEX_SIZE);
				tr.cname[MD5_HASH_HEX_SIZE] = 0;
				trilist_2.add(tr);

				PLOG_WRITE(LOG_PREFIX, "t2 -- | video | %10u --> %10u", tr.ssrc, tr.ssrc_opposite);
			}
		}

		mg_rtcp_triple_list trilist_3;
		mg_rtcp_triple_list trilist_4;
		for (int i = 0; i < trilist_2.getCount(); i++)
		{
			rtcp_triple_t tr1 = trilist_1.getAt(i);
			rtcp_triple_t tr2 = trilist_2.getAt(i);

			rtcp_triple_t tr3;
			tr3.ssrc = tr1.ssrc;
			tr3.ssrc_opposite = tr2.ssrc_opposite;
			trilist_3.add(tr3);

			rtcp_triple_t tr4;
			tr4.ssrc = tr2.ssrc;
			tr4.ssrc_opposite = tr1.ssrc_opposite;
			trilist_4.add(tr4);
		}

		for (int i = 0; i < trilist_3.getCount(); i++)
		{
			rtcp_triple_t tr = trilist_3.getAt(i);
			PLOG_WRITE(LOG_PREFIX, "tr3 | %10u --> %10u", tr.ssrc, tr.ssrc_opposite);
		}
		for (int i = 0; i < trilist_4.getCount(); i++)
		{
			rtcp_triple_t tr = trilist_4.getAt(i);
			PLOG_WRITE(LOG_PREFIX, "tr4 | %10u --> %10u", tr.ssrc, tr.ssrc_opposite);
		}

		if (audio_stream_1 != nullptr)
		{
			for (int i = 0; i < audio_stream_1->get_rtcp_monitors()->getCount(); i++)
			{
				rtcp_monitor_t* mon = audio_stream_1->get_rtcp_monitors()->getAt(i);
				mon->set_triple(&trilist_4);
			}
		}
		if (video_stream_1 != nullptr)
		{
			for (int i = 0; i < video_stream_1->get_rtcp_monitors()->getCount(); i++)
			{
				rtcp_monitor_t* mon = video_stream_1->get_rtcp_monitors()->getAt(i);
				mon->set_triple(&trilist_4);
			}
		}
		if (audio_stream_2 != nullptr)
		{
			for (int i = 0; i < audio_stream_2->get_rtcp_monitors()->getCount(); i++)
			{
				rtcp_monitor_t* mon = audio_stream_2->get_rtcp_monitors()->getAt(i);
				mon->set_triple(&trilist_3);
			}
		}
		if (video_stream_2 != nullptr)
		{
			for (int i = 0; i < video_stream_2->get_rtcp_monitors()->getCount(); i++)
			{
				rtcp_monitor_t* mon = video_stream_2->get_rtcp_monitors()->getAt(i);
				mon->set_triple(&trilist_3);
			}
		}
	}
}
//------------------------------------------------------------------------------
