﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtp_channel.h"
#include "mg_tx_stream.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	class mg_tx_fax_proxy_stream_t : public mg_tx_stream_t
	{
	public:
		mg_tx_fax_proxy_stream_t(rtl::Logger* log, uint32_t id, uint32_t parent_id, const char* term_id);
		virtual ~mg_tx_fax_proxy_stream_t();

		//------------------------------------------------------------------------------
		// << INextDataHandler >>
		virtual void send(const INextDataHandler* out_handler, const rtp_packet* packet);
		virtual void send(const INextDataHandler* out_handler, const uint8_t* data, uint32_t len);
		virtual void send(const INextDataHandler* out_handler, const rtcp_packet_t* packet);
		//------------------------------------------------------------------------------

		void set_channel_event(i_tx_channel_event_handler* rtp);
		void set_remote_address(const socket_address* address) { m_remote_address.copy_from(address); }
		const socket_address* get_remote_address() { return &m_remote_address; }

	private:
		void send_packet_to_rtp_lock(const INextDataHandler* out_handler, const rtp_packet* packet);
		void send_data_to_rtp_lock(const INextDataHandler* out_handler, const uint8_t* data, uint32_t len);
		void send_ctrl_to_rtp_lock(const INextDataHandler* out_handler, const rtcp_packet_t* packet);

	private:
		i_tx_channel_event_handler*	m_rtp;		// udptl канал
		socket_address m_remote_address;		// адрес получателя пакета.
	};
}
//------------------------------------------------------------------------------
