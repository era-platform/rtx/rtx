﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_thread.h"
#include "mg_rx_webrtc_video_stream.h"
#include "mg_session.h"

#include "logdef_stream.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_rx_webrtc_video_stream_t::mg_rx_webrtc_video_stream_t(rtl::Logger* log, uint32_t id, const char* parent_id, MediaSession& session) :
		mg_rx_stream_t(log, id, mg_rx_webrtc_video_e, parent_id, session)
	{
		m_rtp = nullptr;
		m_ssrc_from_rtp = 0;

		rtl::String name;
		name << session.getName() << "-rxwvs" << id;
		setName(name);
		setTag("RXS_WEB");

		m_rx_lock.clear();

		m_firstPacketReceived = false;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_rx_webrtc_video_stream_t::~mg_rx_webrtc_video_stream_t()
	{
		m_rtp = nullptr;

		m_ssrc_from_rtp = 0;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_rx_webrtc_video_stream_t::set_channel_event(i_tx_channel_event_handler* rtp)
	{
		if (!isLocked())
		{
			WARNING("stream is unlocked.");
			return;
		}

		m_rtp = rtp;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_rx_webrtc_video_stream_t::reset_stream_from_channel_event()
	{
		if (!isLocked())
		{
			WARNING("stream is unlocked.");
			return;
		}

		if (m_rtp != nullptr)
		{
			m_rtp->dec_tx_subscriber(m_ssrc_from_rtp);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_rx_webrtc_video_stream_t::rx_packet_stream(const rtp_channel_t* channel, const rtp_packet* packet, const socket_address* from)
	{
		if (checkAndLock())
		{
			WARNING("stream is locked.");
			return false;
		}

		bool result = rx_packet_stream_lock(channel, packet, from);

		releaseLock();
		return result;
	}
	//------------------------------------------------------------------------------
	bool mg_rx_webrtc_video_stream_t::rx_data_stream(const rtp_channel_t* channel, const uint8_t* data, uint32_t len, const socket_address* from)
	{
		if (checkAndLock())
		{
			WARNING("stream is locked.");
			return false;
		}

		bool result = rx_data_stream_lock(channel, data, len, from);

		releaseLock();
		return result;
	}
	//------------------------------------------------------------------------------
	bool mg_rx_webrtc_video_stream_t::rx_ctrl_stream(const rtp_channel_t* channel, const rtcp_packet_t* packet, const socket_address* from)
	{
		if (checkAndLock())
		{
			WARNING("stream is locked.");
			return false;
		}

		bool result = rx_ctrl_stream_lock(channel, packet, from);

		releaseLock();
		return result;
	}
	//------------------------------------------------------------------------------
	bool mg_rx_webrtc_video_stream_t::rx_packet_stream_lock(const rtp_channel_t* channel, const rtp_packet* packet, const socket_address* from)
	{
		bool result = false;
		if (mg_rx_stream_t::getState() == h248::TerminationStreamMode::Loopback)
		{
			WARNING("stream is loopback.");

			if (channel != nullptr)
			{
				rtp_channel_t* channel_loop = (rtp_channel_t*)channel;
				result = channel_loop->send_packet(packet, from);
			}

			return result;
		}

		uint16_t payload_id = (uint16_t)packet->get_payload_type();

		if (!activity())
		{
			return result;
		}

		int tube_count = getTubeCount();
		if (tube_count > 0)
		{
			mg_rx_stream_t::getSession().check_address_from(from);
		}

		for (int i = 0; i < tube_count; i++)
		{
			Tube* tube_at_list = getTube(i);
			if (tube_at_list == nullptr)
			{
				continue;
			}

			// передаем только тубу конкретного пайлоад айди.
			if (tube_at_list->is_suited(payload_id))
			{
				tube_at_list->send(tube_at_list, packet);
				result = true;
			}
		}

		if (result && !m_firstPacketReceived)
		{
			getSession().raiseRecevierEvent(0, 0);
			m_firstPacketReceived = true;
		}

		//PLOG_STREAM_WRITE(LOG_PREFIX, "rx_data_stream_lock -- ID(%s): end.", getName());
		return result;
	}
	//------------------------------------------------------------------------------
	bool mg_rx_webrtc_video_stream_t::rx_data_stream_lock(const rtp_channel_t* channel, const uint8_t* data, uint32_t len, const socket_address* from)
	{
		bool result = false;
		if (!activity())
		{
			return result;
		}

		int tube_count = getTubeCount();
		for (int i = 0; i < tube_count; i++)
		{
			Tube* tube_at_list = getTube(i);
			if (tube_at_list == nullptr)
			{
				continue;
			}

			if (tube_at_list->getType() == HandlerType::Simple)
			{
				tube_at_list->send(tube_at_list, data, len);
				result = true;
				break;
			}
		}
		return result;
	}
	//------------------------------------------------------------------------------
	bool mg_rx_webrtc_video_stream_t::rx_ctrl_stream_lock(const rtp_channel_t* channel, const rtcp_packet_t* packet, const socket_address* from)
	{
		bool result = false;
		if (!activity())
		{
			return result;
		}

		/*if (m_rtcp_monitor != nullptr)
		{
			m_rtcp_monitor->rtcp_event_rx_ctrl(packet);
		}*/

		bool find = false;
		int tube_count = getTubeCount();
		for (int i = 0; i < tube_count; i++)
		{
			Tube* tube_at_list = getTube(i);
			if (tube_at_list == nullptr)
			{
				continue;
			}

			if (tube_at_list->getType() == HandlerType::Simple)
			{
				tube_at_list->send(tube_at_list, packet);
				find = true;
				result = true;
				break;
			}
		}

		if (!find)
		{
			if (tube_count != 0)
			{
				Tube* tube_at_list = getTube(0);
				if (tube_at_list == nullptr)
				{
					return false;
				}

				tube_at_list->send(tube_at_list, packet);
				result = true;
			}
		}

		return result;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	uint32_t mg_rx_webrtc_video_stream_t::get_ssrc_from_rtp()
	{
		return m_ssrc_from_rtp;
	}
	//------------------------------------------------------------------------------
	void mg_rx_webrtc_video_stream_t::set_ssrc_from_rtp(uint32_t ssrc)
	{
		m_ssrc_from_rtp = ssrc;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_rx_webrtc_video_stream_t::create_recorder(const char* path)
	{
		ENTER();

		if (m_recorder != nullptr)
		{
			PRINT("already created");
			return true;
		}

		mg_media_element_list_t* el_list = getMediaParams().get_media_elements();
		if (el_list == nullptr)
		{
			ERROR_MSG("not found actual format.");
			return false;
		}

		media::PayloadSet record_formats;
		for (int i = 0; i < el_list->getCount(); i++)
		{
			mg_media_element_t* element = el_list->getAt(i);
			if (element == nullptr)
			{
				continue;
			}

			rtl::String rtpmap = element->get_param_rtpmap();
			if (rtpmap.isEmpty())
			{
				record_formats.addFormat(element->get_element_name(), (media::PayloadId)element->get_payload_id(), element->get_sample_rate(), element->getChannelCount(), element->get_param_fmtp());
			}
			else
			{
				record_formats.addFormat(element->get_element_name(), (media::PayloadId)element->get_payload_id(), element->get_sample_rate(), rtpmap, element->get_param_fmtp());
			}
		}

		if (record_formats.getCount() == 0)
		{
			ERROR_MSG("record formats not found!");
			return false;
		}

		if (path == nullptr)
		{
			WARNING("record not found in config!");
			record_formats.removeAll();
			return false;
		}

		m_recorder = NEW media::LazyMediaWriterRTP(m_log);
		if (m_recorder == nullptr)
		{
			WARNING("failed to create audio recorder!");
			record_formats.removeAll();
			return false;
		}

		if (!m_recorder->create(path, record_formats))
		{
			WARNING("create fail!");
			record_formats.removeAll();
			DELETEO(m_recorder);
			m_recorder = nullptr;
			return false;
		}

		if (!m_recorder->start())
		{
			WARNING("start fail!");
			record_formats.removeAll();
			m_recorder->close();
			DELETEO(m_recorder);
			m_recorder = nullptr;
			return false;
		}

		record_formats.removeAll();

		RETURN_BOOL(true);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_rx_webrtc_video_stream_t::activity()
	{
		if (mg_rx_stream_t::getState() == h248::TerminationStreamMode::SendOnly)
		{
			WARNING("stream шы sendonly!");
			return false;
		}

		if (mg_rx_stream_t::getState() == h248::TerminationStreamMode::Inactive)
		{
			WARNING("stream is inactive");
			return false;
		}

		return true;
	}
}
//------------------------------------------------------------------------------
