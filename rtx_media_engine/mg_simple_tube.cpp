﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_simple_tube.h"
#include "rtp_packet.h"
#include "mg_media_element.h"

#include "logdef_tube.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_simple_tube_t::mg_simple_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id) :
		Tube(HandlerType::Simple, id, log)
	{
		makeName(parent_id, "-stu"); // Simple Tube

		rtl::res_counter_t::add_ref(g_mge_tube_counter);

		setLogTag("TUB_THR");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_simple_tube_t::~mg_simple_tube_t()
	{
		unsubscribe(this);

		rtl::res_counter_t::release(g_mge_tube_counter);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_simple_tube_t::init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out)
	{
		if (payload_el_in != nullptr)
		{
			m_payload_id = payload_el_in->get_payload_id();

			PRINT_FMT("payload in : %s %d", payload_el_in->get_element_name(), m_payload_id);
		}
		else
		{
			ERROR_MSG("wrong params!");
		}

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_simple_tube_t::send(const INextDataHandler* out_handler, const rtp_packet* packet)
	{
		if (packet == nullptr)
		{
			ERROR_MSG("there is nothing to send!");
			return;
		}

		rtp_packet* packet_non_const = (rtp_packet*)packet;
		packet_non_const->set_payload_type((uint8_t)m_payload_id); // out

		INextDataHandler* out = getNextHandler();

		if (out != nullptr)
		{
			out->send(this, packet_non_const);
		}
		else
		{
			WARNING("nowhere to send");
		}
	}
	bool mg_simple_tube_t::is_suited(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out)
	{
		if ((payload_el_in != nullptr) && (payload_el_in->get_payload_id() != m_payload_id))
		{
			return false;
		}
		if ((payload_el_out != nullptr) && (payload_el_out->get_payload_id() != m_payload_id))
		{
			return false;
		}
		return true;
	}
}
//------------------------------------------------------------------------------
