﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "std_logger.h"
#include "net/ip_address.h"
#include "mg_sdp_media.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	class ME_DEBUG_API mg_sdp_session_t
	{
		mg_sdp_session_t(const mg_sdp_session_t&);
		mg_sdp_session_t&	operator=(const mg_sdp_session_t&);

	public:
		mg_sdp_session_t(rtl::Logger* log);
		~mg_sdp_session_t();

		void set_termination_id(const char* term_id);
		bool read(const rtl::String& spd, bool compare, uint32_t stream_id);
		bool write_full(rtl::String& spd);
		void write_base(rtl::String& spd);
		void clear();

		const ip_address_t*		get_address() const;
		void					set_address(const ip_address_t* addr);
		void					set_address(const char* addr);

		bool					is_empty();
		mg_sdp_media_base_t*	get_media(int stream_id);

		//a=group:BUNDLE audio
		//a=msid-semantic: WMS R1LlUK7HD2rbIEQgk3FqBhJN0QaJybEhNawG
		bool					fill_bundle_param();
		void					clear_bundle_param();

		// После чтения новой sdp в памяти хранится различия между старой, если старая sdp не была пустой.
		mg_sdp_diff_list*		get_differences();
		void					clear_differences_list();
		bool					apply_differences();

		sdp_fingerprint_t*		get_fingerprint();

	private:
		bool					read_line(const rtl::String& string_line, bool compare);
		void					write_lines(rtl::String& ss);

		bool					read_v_line(const rtl::String& string_line, bool compare);
		bool					read_o_line(const rtl::String& string_line, bool compare);
		bool					read_s_line(const rtl::String& string_line, bool compare);
		bool					read_t_line(const rtl::String& string_line, bool compare);
		bool					read_c_line(const rtl::String& string_line, bool compare);

		void					write_v_line(rtl::String& ss);
		void					write_o_line(rtl::String& ss);
		void					write_s_line(rtl::String& ss);
		void					write_t_line(rtl::String& ss);
		void					write_c_line(rtl::String& ss);

		bool					insert_media_differences(const mg_sdp_diff_list* media_diff);
		bool					read_fingerprint(const rtl::String& media_line, bool compare);

	protected:
		void					set_fingerprint(sdp_fingerprint_t* fingerprint);

	private:
		rtl::Logger*					m_log;
		rtl::String				m_term_id;

		rtl::String				m_v_line;		// v=0
		rtl::String				m_o_line;		// o=carol 28908764872 28908764872 IN IP4 100.3.6.6
		rtl::String				m_s_line;		// s=-
		rtl::String				m_t_line;		// t=0 0

		rtl::String				m_c_line;		// c=IN IP4 192.0.2.4
		ip_address_t			m_address;

		rtl::String				m_group_line;	// a=group:BUNDLE audio
		rtl::String				m_msid_line;	// a=msid-semantic: WMS R1LlUK7HD2rbIEQgk3FqBhJN0QaJybEhNawG
		sdp_fingerprint_t*		m_fingerprint;

		mg_sdp_media_list		m_media_list; // удаляется корректно!

		mg_sdp_diff_list		m_difference_list;
	};
}
//------------------------------------------------------------------------------
