﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_webrtc_termination.h"
#include "rtp_webrtc_channel.h"
#include "mg_rx_common_stream.h"
#include "mg_tx_common_stream.h"
#include "mg_rx_webrtc_video_stream.h"
#include "mg_tx_webrtc_video_stream.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define LOG_PREFIX m_tag //"MG-TWEB" // Media Gateway WEBRTC Termination

#include "logdef.h"

namespace mge 
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	WebRTCTermination::WebRTCTermination(rtl::Logger* log, const h248::TerminationID& termId, const char* parent_id) :
		Termination(log, termId, parent_id),
		m_sdp_local(log),
		m_sdp_remote(log)
	{
		m_service_state = h248::TerminationServiceState::Test;
		m_event_buffer_control = false;

		strcpy(m_tag, "TERM-WEB");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	WebRTCTermination::~WebRTCTermination()
	{
		mg_termination_destroy(nullptr);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool WebRTCTermination::mg_termination_initialize(h248::Field* _reply)
	{
		ENTER();

		m_sdp_local.set_termination_id(getName());
		m_sdp_remote.set_termination_id(getName());

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool WebRTCTermination::mg_termination_destroy(h248::Field* reply)
	{
		Termination::mg_termination_lock();

		stop_all();

		Termination::destroyMediaSession();

		clear_cannels();

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool WebRTCTermination::mg_termination_set_LocalControl_Mode(uint16_t stream_id, h248::TerminationStreamMode mode, h248::Field* reply)
	{
		if (IS_ADDITIONAL_STREAM(stream_id))
		{
			return true;
		}

		return Termination::mg_termination_set_LocalControl_Mode(stream_id, mode, reply);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool WebRTCTermination::mg_termination_set_TerminationState_Property(const h248::Field* property, h248::Field* reply)
	{
		const rtl::String& propName = property->getName();
		const rtl::String& propValue = property->getValue();

		ENTER_FMT("name:%s, value:%s", (const char*)propName, (const char*)propValue);

		if (propName == "mode")
		{
			m_conference = propValue == "conf";
		}
		else if (propName == "v-id")
		{
			m_vId = strtoul(propValue, nullptr, 10);
		}
		else if (propName == "v-title")
		{
			m_vTitle = propValue;
		}
		else
		{
			ERROR_MSG("Unsupported or unknown property");
			reply->addErrorDescriptor(h248::ErrorCode::c445_Unsupported_or_unknown_property);
			return false;
		}

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool WebRTCTermination::mg_termination_set_Local_Remote(uint16_t stream_id, const rtl::String& remote_sdp, const rtl::String& local_sdp_offer, h248::Field* reply)
	{
		ENTER();

		int type = mg_sdp_media_base_t::stream_type(stream_id);
		// crutch... RTX-66 additional stream use main channel. If need your change on stream id.
		rtp_channel_t* channel = get_channel(type);
		if (channel == nullptr)
		{
			channel = create_channel(type);
			if (channel == nullptr)
			{
				ERROR_MSG("channel is null.");
				return false;
			}
		}

		bool result = true;
		if (type == MG_STREAM_TYPE_AUDIO)
		{
			result = prepare_audio(channel, stream_id, remote_sdp, local_sdp_offer);
		}
		else if (type == MG_STREAM_TYPE_VIDEO)
		{
			if (channel->getType() == rtp_channel_type_t::rtp_channel_type_webrtc_bundle_e)
			{
				result = prepare_video(stream_id, remote_sdp, local_sdp_offer);
			}
			else
			{
				ERROR_MSG("wrong channel type");
			}
		}


		RETURN_BOOL(result);
	}
	//------------------------------------------------------------------------------
	bool WebRTCTermination::mg_termination_get_Local(uint16_t stream_id, rtl::String& local, h248::Field* reply)
	{
		ENTER();

		local.setEmpty();

		m_sdp_local.write_base(local);

		local << "\r\n";

		int type = mg_sdp_media_base_t::stream_type(stream_id);
		if (type == MG_STREAM_TYPE_AUDIO)
		{
			mg_sdp_media_base_t* audio = m_sdp_local.get_media(stream_id);
			if (audio == nullptr)
			{
				WARNING("audio not found");
			}
			else
			{
				for (int i = 0; i < audio->get_differences()->getCount(); i++)
				{
					mg_sdp_difference_t* diff = audio->get_differences()->getAt(i);
					if (diff == nullptr)
					{
						continue;
					}

					if (diff->type == mg_sdp_difference_type::mode)
					{
						audio->set_mode(diff->line.substring(2));
					}
				}

				audio->write(local);
			}
		}
		else if (type == MG_STREAM_TYPE_VIDEO)
		{
			mg_sdp_media_base_t* video = m_sdp_local.get_media(stream_id);
			if (video == nullptr)
			{
				WARNING("video not found");
			}
			else
			{
				for (int i = 0; i < video->get_differences()->getCount(); i++)
				{
					mg_sdp_difference_t* diff = video->get_differences()->getAt(i);
					if (diff == nullptr)
					{
						continue;
					}

					if (diff->type == mg_sdp_difference_type::mode)
					{
						video->set_mode(diff->line.substring(2));
					}
				}

				video->write(local);
			}
		}

		RETURN_BOOL(true);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	rtp_channel_t* WebRTCTermination::create_channel(uint32_t stream_id)
	{
		ENTER();

		rtp_channel_t* channel = rtp_port_manager_t::get_channel(m_adress_key, rtp_channel_type_t::rtp_channel_type_webrtc_bundle_e);

		if (channel == nullptr)
		{
			ERROR_FMT("channel with interface: %s and type:%s not created",
				(const char*)m_adress_key, h248::TerminationType_toString(getType()));
			return nullptr;
		}
		else
		{
			PRINT_FMT("channel with interface: %s and type:%s created.",
				(const char*)m_adress_key, h248::TerminationType_toString(getType()));
		}

		mg_channel_at_stream_t* chst = NEW mg_channel_at_stream_t();
		chst->stream_id = stream_id;
		chst->channel = channel;
		m_channel_list.add(chst);

		RETURN_FMT(channel, "%s.", channel->getName());
	}
	//------------------------------------------------------------------------------
	rtp_channel_t* WebRTCTermination::get_channel(uint32_t stream_id)
	{
		ENTER_FMT("streamId:%d", stream_id);

		if (m_channel_list.getCount() == 0)
		{
			WARNING("channel list empty.");
			return nullptr;
		}

		rtp_channel_t* channel = nullptr;
		rtp_channel_t* audio_channel = nullptr;

		for (int i = 0; i < m_channel_list.getCount(); i++)
		{
			mg_channel_at_stream_t* chst = m_channel_list.getAt(i);
			if (chst == nullptr)
			{
				continue;
			}
			if (chst->stream_id == MG_STREAM_TYPE_AUDIO)
			{
				audio_channel = chst->channel;
			}
			if (chst->stream_id == stream_id)
			{
				channel = chst->channel;
				break;
			}
		}
		if (channel == nullptr)
		{
			if ((audio_channel != nullptr) && (audio_channel->getType() == rtp_channel_type_t::rtp_channel_type_webrtc_bundle_e))
			{
				RETURN_FMT(audio_channel, "%s", audio_channel->getName());
			}
			WARNING("not found or type mismatch");
			return nullptr;
		}

		RETURN_FMT(channel, "%s", getName(), channel->getName());
	}
	//------------------------------------------------------------------------------
	void WebRTCTermination::release_channel(rtp_channel_t* channel)
	{
		ENTER();

		if (channel == nullptr)
		{
			ERROR_MSG("channel is null");
			return;
		}

		if (m_channel_list.getCount() == 0)
		{
			WARNING("channel list empty");
			return;
		}

		bool find = false;
		const ip_address_t* iface = channel->get_net_interface();

		for (int i = 0; i < m_channel_list.getCount(); i++)
		{
			mg_channel_at_stream_t* chst = m_channel_list.getAt(i);
			if (chst == nullptr)
			{
				continue;
			}

			rtp_channel_t* channel_at_list = chst->channel;
			if (channel_at_list == nullptr)
			{
				WARNING_FMT("channel with index: %d is null", i);
				continue;
			}

			if (rtp_port_manager_t::compare_channels(channel, channel_at_list))
			{
				PRINT_FMT("channel %s with interface %s will by released.", channel->getName(), iface->to_string());

				m_channel_list.removeAt(i);
				rtp_port_manager_t::release_channel(channel);
				channel = nullptr;
				find = true;
			
				chst->channel = nullptr;
				DELETEO(chst);
				chst = nullptr;
				m_channel_list.removeAt(i);

				break;
			}
		}

		if (!find)
		{
			ERROR_FMT("channel %s with interface %s not found!", channel->getName(), iface->to_string());
			return;
		}

		LEAVE();
	}
	//------------------------------------------------------------------------------
	void WebRTCTermination::clear_cannels()
	{
		ENTER();

		for (int i = 0; i < m_channel_list.getCount(); i ++)
		{
			mg_channel_at_stream_t* chst = m_channel_list.getAt(i);
			if (chst == nullptr)
			{
				continue;
			}
			rtp_channel_t* channel = chst->channel;
			if (channel == nullptr)
			{
				WARNING_FMT("channel with index: %d is null.", i);
				continue;
			}

			rtp_port_manager_t::release_channel(channel);
			channel = nullptr;
			chst->channel = nullptr;
			DELETEO(chst);
			chst = nullptr;
		}

		m_channel_list.clear();

		LEAVE();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool WebRTCTermination::isReady(uint32_t processor_id)
	{
		rtp_channel_t* channel = get_channel(processor_id);
		if (channel == nullptr)
		{
			return false;
		}
	
		return Termination::isReady(processor_id);

		//MediaSession* mg_stream = Termination::findMediaSession(processor_id);
		//if (mg_stream == nullptr)
		//{
		//	WARNING_FMT("stream is null. %d.", processor_id);
		//	return false;
		//}

		//return mg_stream->isReady();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool WebRTCTermination::mg_stream_start(uint32_t stream_id)
	{
		ENTER_FMT("streamId:%)...", stream_id);

		if (!Termination::mg_stream_start(stream_id))
		{
			ERROR_MSG("base stream starting failed");
			return false;
		}

		rtp_channel_t* channel = get_channel(stream_id);
		if (channel == nullptr)
		{
			ERROR_MSG("channel is null!");
			return false;
		}

		if (channel->started())
		{
			RETURN_FMT(true, "%s already started!", (const char*)channel->getName());
			return true;
		}

		if (!rtp_port_manager_t::start_channel_listen(channel))
		{
			WARNING_FMT("channel: %s not started!", (const char*)channel->getName());
			return false;
		}

		RETURN_BOOL(true);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void WebRTCTermination::stop_all()
	{
		ENTER();

		if (m_channel_list.getCount() == 0)
		{
			WARNING("channel list empty");
			return;
		}

		for (int i = 0; i < m_channel_list.getCount(); i++)
		{
			mg_channel_at_stream_t* chst = m_channel_list.getAt(i);
			if (chst == nullptr)
			{
				continue;
			}
			rtp_channel_t* channel = chst->channel;
			if (channel == nullptr)
			{
				WARNING_FMT("channel with index: %d is null", i);
				continue;
			}
			
			rtp_port_manager_t::stop_channel_listen(channel);
		}

		LEAVE();
	}
	//------------------------------------------------------------------------------
	void WebRTCTermination::mg_stream_stop(uint32_t stream_id)
	{
		Termination::mg_stream_stop(stream_id);

		rtp_channel_t* channel = get_channel(stream_id);
		if (channel == nullptr)
		{
			ERROR_MSG("channel is null!");
		}
		else
		{
			rtp_port_manager_t::stop_channel_listen(channel);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool WebRTCTermination::prepare_audio(rtp_channel_t* channel, uint16_t stream_id, const rtl::String& remote_sdp, const rtl::String& local_sdp)
	{
		uint32_t type = IS_ADDITIONAL_STREAM(stream_id) ? mg_sdp_media_base_t::stream_type(stream_id) : stream_id;
		MediaSession* audio_stream = Termination::findMediaSession(type);
		if (audio_stream == nullptr)
		{
			return false;
		}

		rtl::String loc(local_sdp);
		rtl::String rem(remote_sdp);
		if (IS_ADDITIONAL_STREAM(stream_id) && loc.isEmpty())
		{
			mg_sdp_media_base_t* m = m_sdp_local.get_media(MG_STREAM_TYPE_AUDIO);
			m->write(loc);
		}

		if (rem.isEmpty())
		{
			return prepare_offer_audio(channel, audio_stream, stream_id, loc);
		}
		else if (loc.isEmpty())
		{
			return apply_answer_audio(channel, audio_stream, stream_id, rem);
		}

		if (!apply_offer_audio(channel, audio_stream, stream_id, rem))
		{
			return false;
		}

		return prepare_answer_audio(channel, audio_stream, stream_id, loc);
	}
	//------------------------------------------------------------------------------
	bool WebRTCTermination::prepare_video(uint16_t stream_id, const rtl::String& remote_sdp, const rtl::String& local_sdp)
	{
		uint32_t type = IS_ADDITIONAL_STREAM(stream_id) ? mg_sdp_media_base_t::stream_type(stream_id) : stream_id;
		MediaSession* video_stream = Termination::findMediaSession(type);

		if (video_stream == nullptr)
		{
			return false;
		}
		
		bool first_local = (m_sdp_local.get_media(stream_id) == nullptr);
		bool first_remote = (m_sdp_remote.get_media(stream_id) == nullptr);

		if (remote_sdp.isEmpty())
		{
			return prepare_offer_webrtc_video(video_stream, stream_id, local_sdp, first_local);
		}
		
		if (local_sdp.isEmpty())
		{
			return apply_answer_webrtc_video(video_stream, stream_id, remote_sdp, first_remote);
		}
		
		return apply_offer_webrtc_video(video_stream, stream_id, remote_sdp, first_remote) &&
			prepare_answer_webrtc_video(video_stream, stream_id, local_sdp, first_local);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_rx_stream_t* WebRTCTermination::find_rx_stream_by_type(mg_rx_stream_type_t type_rx, MediaSession* mg_stream, int index)
	{
		if (index < 0)
		{
			return nullptr;
		}
		const rtl::ArrayT<mg_rx_stream_t*>& rx_streams = mg_stream->get_rx_streams();
		if (rx_streams.getCount() <= index)
		{
			if (!mg_stream->create_rx_stream(type_rx))
			{
				ERROR_MSG("create rx stream fail!");
				return nullptr;
			}
		}
		
		if (rx_streams.getCount() <= index)
		{
			ERROR_MSG("create rx stream fail!");
			return nullptr;
		}
		mg_rx_stream_t* rx_stream = rx_streams.getAt(index);
		if (rx_stream == nullptr)
		{
			ERROR_MSG("create rx stream fail!");
			return nullptr;
		}
		if (rx_stream->getType() != type_rx)
		{
			return nullptr;
		}

		return rx_stream;
	}
	//------------------------------------------------------------------------------
	mg_tx_stream_t* WebRTCTermination::find_tx_stream_by_type(mg_tx_stream_type_t type_tx, MediaSession* mg_stream, int index)
	{
		if (index < 0)
		{
			return nullptr;
		}
		const rtl::ArrayT<mg_tx_stream_t*>& tx_streams = mg_stream->get_tx_streams();
		if (tx_streams.getCount() <= index)
		{
			if (!mg_stream->create_tx_stream(type_tx))
			{
				ERROR_MSG("create tx stream fail!");
				return nullptr;
			}
		}
		
		if (tx_streams.getCount() <= index)
		{
			ERROR_MSG("create tx stream fail!");
			return nullptr;
		}
		mg_tx_stream_t* tx_stream = tx_streams.getAt(index);
		if (tx_stream == nullptr)
		{
			ERROR_MSG("create tx stream fail!");
			return nullptr;
		}

		if (tx_stream->getType() != type_tx)
		{
			return nullptr;
		}

		return tx_stream;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool WebRTCTermination::prepare_offer_audio(rtp_channel_t* channel, MediaSession* audio_stream, uint16_t stream_id, const rtl::String& local_sdp)
	{
		mg_tx_stream_type_t type_tx = mg_tx_stream_type_t::mg_tx_simple_audio_e;
		int stream_index = STREAM_INDEX_BY_ID(stream_id);

		if (local_sdp.isEmpty())
		{
			return false;
		}

		mg_sdp_media_base_t* audio_l = m_sdp_local.get_media(stream_id);
		bool update = (audio_l == nullptr);

		if (!m_sdp_local.read(local_sdp, !update, stream_id))
		{
			ERROR_MSG("local sdp read fail!");
			return false;
		}

		mg_tx_stream_t* tx_stream = find_tx_stream_by_type(type_tx, audio_stream, stream_index);
		if (tx_stream == nullptr)
		{
			return false;
		}
		mg_tx_common_stream_t* tx_common_stream = (mg_tx_common_stream_t*)tx_stream;
		tx_common_stream->change_flag_recalc_packet(true);
		audio_stream->reset_first_packet_recive_flag();

		audio_l = m_sdp_local.get_media(stream_id);
		if (audio_l == nullptr)
		{
			return false;
		}

		const ip_address_t* ipaddr = m_sdp_local.get_address();
		if (ipaddr->empty())
		{
			ipaddr = channel->get_net_interface();
			m_sdp_local.set_address(ipaddr->to_string());
		}

		uint16_t port = audio_l->get_port();
		if (port == UINT16_MAX)
		{
			port = channel->get_data_port();
			audio_l->set_port(port);
		}

		rtp_webrtc_channel_t* web_channel = (rtp_webrtc_channel_t*)channel;
	
		if (update)
		{
			mg_sdp_media_base_t* audio_l_0 = nullptr;
			if (IS_ADDITIONAL_STREAM(stream_id))
			{
				audio_l_0 = m_sdp_local.get_media(MG_STREAM_TYPE_AUDIO);
			}

			//-------------------------------------------------------------------------
			// ssrc
			prepare_ssrc_local_1(stream_id, tx_common_stream->get_ssrc_to_rtp(), audio_l);
			tx_common_stream->set_channel_event(web_channel);
			//-------------------------------------------------------------------------

			//-------------------------------------------------------------------------
			// ICE
			if (!prepare_ice_local_1(stream_id, channel, audio_l))
			{
				return false;
			}
			//-------------------------------------------------------------------------

			//-------------------------------------------------------------------------
			// fingerprint
			sdp_fingerprint_t* fingerprint = NEW sdp_fingerprint_t();
			if (IS_ADDITIONAL_STREAM(stream_id))
			{
				sdp_fingerprint_t* fp_0 = (audio_l_0 == nullptr) ? nullptr : audio_l_0->get_fingerprint_param();
				fingerprint->copy_from(fp_0);
			}
			else
			{
				if (!web_channel->fill_fingerprint_param(fingerprint))
				{
					return false;
				}
			}

			mg_sdp_media_base_t* audio_r = m_sdp_remote.get_media(stream_id);
			if (audio_r == nullptr)
			{
				fingerprint->set_role(fingerprint_role_t::actpass);
			}
			else
			{
				sdp_fingerprint_t* fingerprint_r = audio_r->get_fingerprint_param();
				if ((fingerprint_r != nullptr) && (fingerprint_r->get_role() == fingerprint_role_t::passive))
				{
					fingerprint->set_role(fingerprint_role_t::active);
				}
				else
				{
					fingerprint->set_role(fingerprint_role_t::passive);
				}
			}

			audio_l->clear_fingerprint_param();
			audio_l->set_fingerprint_param(fingerprint);

			//-------------------------------------------------------------------------

			audio_l->set_transport("UDP/TLS/RTP/SAVPF");
			audio_l->set_rtcp_mux(true);

			//-------------------------------------------------------------------------

			audio_l->set_rtcp_port(web_channel->get_ctrl_port());
			audio_l->set_rtcp_address(web_channel->get_net_interface());

			//-------------------------------------------------------------------------

			// ---------------------------------------------------------------
			// update plan in local sdp
			rtl::String index;
			index << stream_index;
			audio_l->set_mid_type(index); // for unified plan

			if (audio_r != nullptr)
			{
				if (audio_r->get_mid_type().getLength() > 1) // audio -> plan b; 0 -> unified plan.
				{
					audio_l->set_mid_type(MG_STREAM_TYPE_AUDIO_STR);
				}
			}
			m_sdp_local.fill_bundle_param();
			// ---------------------------------------------------------------
		}
		else
		{
			sdp_fingerprint_t* fingerprint_l = audio_l->get_fingerprint_param();
			if (fingerprint_l != nullptr)
			{
				fingerprint_l->set_role(fingerprint_role_t::actpass);
			}
		}

		return true;
	}
	//------------------------------------------------------------------------------
	bool WebRTCTermination::apply_answer_audio(rtp_channel_t* channel, MediaSession* audio_stream, uint16_t stream_id, const rtl::String& remote_sdp)
	{
		mg_rx_stream_type_t type_rx = mg_rx_stream_type_t::mg_rx_simple_audio_e;
		mg_tx_stream_type_t type_tx = mg_tx_stream_type_t::mg_tx_simple_audio_e;

		int stream_index = STREAM_INDEX_BY_ID(stream_id);

		if (remote_sdp.isEmpty())
		{
			return false;
		}

		mg_sdp_media_base_t* audio_r = m_sdp_remote.get_media(stream_id);
		bool update = (audio_r == nullptr);

		if (!m_sdp_remote.read(remote_sdp, !update, stream_id))
		{
			ERROR_MSG("remote sdp read fail");
			return false;
		}

		mg_rx_stream_t* rx_stream = find_rx_stream_by_type(type_rx, audio_stream, stream_index);
		if (rx_stream == nullptr)
		{
			return false;
		}
		mg_rx_common_stream_t* rx_common_stream = (mg_rx_common_stream_t*)rx_stream;

		mg_tx_stream_t* tx_stream = find_tx_stream_by_type(type_tx, audio_stream, stream_index);
		if (tx_stream == nullptr)
		{
			return false;
		}
		mg_tx_common_stream_t* tx_common_stream = (mg_tx_common_stream_t*)tx_stream;
		tx_common_stream->change_flag_recalc_packet(true);
		audio_stream->reset_first_packet_recive_flag();

		audio_r = m_sdp_remote.get_media(stream_id);
		if (audio_r == nullptr)
		{
			return false;
		}
	
		mg_sdp_media_base_t* audio_l = m_sdp_local.get_media(stream_id);
		if (audio_l == nullptr)
		{
			rx_stream->setMediaParams(*audio_r);
			tx_stream->setMediaParams(*audio_r);
		}
		else
		{
			rx_stream->setMediaParams(*audio_l);
			tx_stream->setMediaParams(*audio_r);

			// ---------------------------------------------------------------
			// filter
			mg_media_element_list_t* elemens_tx = tx_stream->getMediaParams().get_media_elements();
			for (int i = 0; i < elemens_tx->getCount(); i++)
			{
				mg_media_element_t* el = elemens_tx->getAt(i);
				if (el == nullptr)
				{
					continue;
				}
				bool find = false;
				for (int j = 0; j < audio_l->get_media_elements()->getCount(); j++)
				{
					mg_media_element_t* el_l = audio_l->get_media_elements()->getAt(j);
					if (el_l == nullptr)
					{
						continue;
					}

					if (el_l->compare_with(el))
					{
						find = true;
						break;
					}
				}
				if (!find)
				{
					elemens_tx->removeAt(i);
					i--;
				}
			}
			// ---------------------------------------------------------------
		}

		rtp_webrtc_channel_t* web_channel = (rtp_webrtc_channel_t*)channel;

		if (update)
		{
			//-------------------------------------------------------------------------
			// ssrc
			for (int i = 0; i < audio_r->get_ssrc_params()->getCount(); i++)
			{
				mg_sdp_ssrc_param_t* ssrc_param = audio_r->get_ssrc_params()->getAt(i);
				if (ssrc_param == nullptr)
				{
					continue;
				}

				bind_ssrc(ssrc_param, web_channel, rx_common_stream);
			}
			//-------------------------------------------------------------------------

			//-------------------------------------------------------------------------
			// ice
			socket_address saddr;
			socket_address saddr_rtcp;
			if (!analyze_remote_addr_ice_1(audio_r, web_channel, &saddr, &saddr_rtcp))
			{
				return false;
			}
			tx_common_stream->set_remote_address(&saddr);
			tx_common_stream->set_remote_rtcp_address(&saddr_rtcp);
			//-------------------------------------------------------------------------

			//-------------------------------------------------------------------------
			// fingerprint
			sdp_fingerprint_t* fingerprint = audio_r->get_fingerprint_param();
			if (fingerprint == nullptr || fingerprint->get_hash_length() == 0)
			{
				fingerprint = m_sdp_remote.get_fingerprint();
			}
			if (fingerprint == nullptr || fingerprint->get_hash_length() == 0)
			{
				return false;
			}
			if (!web_channel->apply_fingerprint_param(fingerprint))
			{
				return false;
			}
			//-------------------------------------------------------------------------

			//-------------------------------------------------------------------------
			// rtcp monitor
			if (audio_l == nullptr)
			{
				if (IS_ADDITIONAL_STREAM(stream_id))
				{
					audio_l = m_sdp_local.get_media(MG_STREAM_TYPE_AUDIO);
				}
				if (audio_l == nullptr)
				{
					return false;
				}
			}
			for (int i = 0; i < audio_l->get_ssrc_params()->getCount(); i++)
			{
				mg_sdp_ssrc_param_t* ssrc_param_l = audio_l->get_ssrc_params()->getAt(i);
				if (ssrc_param_l == nullptr)
				{
					continue;
				}
				mg_sdp_ssrc_param_t* ssrc_param_r = (audio_r->get_ssrc_params()->getCount() <= i) ? nullptr :
					audio_r->get_ssrc_params()->getAt(i);
				if (ssrc_param_r == nullptr)
				{
					continue;
				}

				rtl::String cname(ssrc_param_l->get_cname());
			
				if (!audio_stream->create_rtcp_monitor(saddr.to_string(), rx_stream, tx_stream, cname, true))
				{
					WARNING("create rtcp monitor fail!");
					continue;
				}
			}
			//-------------------------------------------------------------------------

			if (!m_conference && !IS_ADDITIONAL_STREAM(stream_id))
			{
				if (!m_rtp_record_file_path.isEmpty())
				{
					// создаем рекордер только когда есть обратная сторона и терминатор настроен полностью.
					if (!rx_common_stream->createRecorder(m_rtp_record_file_path))
					{
						WARNING("create recorder fail");
						return false;
					}
				}
			}
		}

		return true;
	}
	//------------------------------------------------------------------------------
	bool WebRTCTermination::prepare_answer_audio(rtp_channel_t* channel, MediaSession* audio_stream, uint16_t stream_id, const rtl::String& local_sdp)
	{
		mg_tx_stream_type_t type_tx = mg_tx_stream_type_t::mg_tx_simple_audio_e;
		mg_rx_stream_type_t type_rx = mg_rx_stream_type_t::mg_rx_simple_audio_e;

		int stream_index = STREAM_INDEX_BY_ID(stream_id);

		if (local_sdp.isEmpty())
		{
			return false;
		}

		mg_sdp_media_base_t* audio_l = m_sdp_local.get_media(stream_id);
		bool update = (audio_l == nullptr);

		if (!m_sdp_local.read(local_sdp, !update, stream_id))
		{
			ERROR_MSG("local sdp read fail");
			return false;
		}

		mg_tx_stream_t* tx_stream = find_tx_stream_by_type(type_tx, audio_stream, stream_index);
		if (tx_stream == nullptr)
		{
			return false;
		}
		mg_tx_common_stream_t* tx_common_stream = (mg_tx_common_stream_t*)tx_stream;
		tx_common_stream->change_flag_recalc_packet(true);
		audio_stream->reset_first_packet_recive_flag();

		mg_rx_stream_t* rx_stream = find_rx_stream_by_type(type_rx, audio_stream, stream_index);
		if (rx_stream == nullptr)
		{
			return false;
		}

		audio_l = m_sdp_local.get_media(stream_id);
		if (audio_l == nullptr)
		{
			return false;
		}
		mg_sdp_media_base_t* audio_r = m_sdp_remote.get_media(stream_id);
		if (audio_r == nullptr)
		{
			return false;
		}

		rx_stream->setMediaParams(*audio_l);
		tx_stream->setMediaParams(*audio_r);
		mg_media_element_list_t* elemens_tx = tx_stream->getMediaParams().get_media_elements();
		for (int i = 0; i < elemens_tx->getCount(); i++)
		{
			mg_media_element_t* el = elemens_tx->getAt(i);
			if (el == nullptr)
			{
				continue;
			}
			bool find = false;
			for (int j = 0; j < audio_l->get_media_elements()->getCount(); j++)
			{
				mg_media_element_t* el_l = audio_l->get_media_elements()->getAt(j);
				if (el_l == nullptr)
				{
					continue;
				}

				if (el_l->compare_with(el))
				{
					find = true;
					break;
				}
			}
			if (!find)
			{
				elemens_tx->removeAt(i);
				i--;
			}
		}

		const ip_address_t* ipaddr = m_sdp_local.get_address();
		if (ipaddr->empty())
		{
			ipaddr = channel->get_net_interface();
			m_sdp_local.set_address(ipaddr->to_string());
		}
		uint16_t port = channel->get_data_port();
		audio_l->set_port(port);

		rtp_webrtc_channel_t* web_channel = (rtp_webrtc_channel_t*)channel;

		if (update)
		{
			mg_sdp_media_base_t* audio_l_0 = nullptr;
			if (IS_ADDITIONAL_STREAM(stream_id))
			{
				audio_l_0 = m_sdp_local.get_media(MG_STREAM_TYPE_AUDIO);
			}

			//-------------------------------------------------------------------------
			// ssrc
			prepare_ssrc_local_1(stream_id, tx_common_stream->get_ssrc_to_rtp(), audio_l);
			tx_common_stream->set_channel_event(web_channel);
			//-------------------------------------------------------------------------

			//-------------------------------------------------------------------------
			// ICE
			if (!prepare_ice_local_1(stream_id, channel, audio_l))
			{
				return false;
			}
			//-------------------------------------------------------------------------

			//-------------------------------------------------------------------------
			// fingerprint
			sdp_fingerprint_t* fingerprint = NEW sdp_fingerprint_t();
			if (IS_ADDITIONAL_STREAM(stream_id))
			{
				sdp_fingerprint_t* fp_0 = (audio_l_0 == nullptr) ? nullptr : audio_l_0->get_fingerprint_param();
				fingerprint->copy_from(fp_0);
			}
			else
			{
				if (!web_channel->fill_fingerprint_param(fingerprint))
				{
					return false;
				}
			}
			fingerprint->set_role(fingerprint_role_t::passive);
		
			audio_l->clear_fingerprint_param();
			audio_l->set_fingerprint_param(fingerprint);
			//-------------------------------------------------------------------------

			audio_l->set_transport("UDP/TLS/RTP/SAVPF");
			audio_l->set_rtcp_mux(true);

			//-------------------------------------------------------------------------

			audio_l->set_rtcp_port(web_channel->get_ctrl_port());
			audio_l->set_rtcp_address(web_channel->get_net_interface());

			//-------------------------------------------------------------------------

			// ---------------------------------------------------------------
			// update plan in local sdp
			rtl::String index;
			index << stream_index;
			audio_l->set_mid_type(index); // for unified plan

			if (audio_r->get_mid_type().getLength() > 1) // audio -> plan b; 0 -> unified plan.
			{
				audio_l->set_mid_type(MG_STREAM_TYPE_AUDIO_STR);
			}
			m_sdp_local.fill_bundle_param();
			// ---------------------------------------------------------------

			//-------------------------------------------------------------------------

			uint32_t ssrc_rx = 0;
			for (int i = 0; i < audio_r->get_ssrc_params()->getCount(); i++)
			{
				mg_sdp_ssrc_param_t* ssrc_param = audio_r->get_ssrc_params()->getAt(i);
				if (ssrc_param == nullptr)
				{
					continue;
				}
				ssrc_rx = ssrc_param->get_ssrc();
				break;
			}

			if (ssrc_rx != 0)
			{
				mg_rx_stream_t* rx_stream = find_rx_stream_by_type(type_rx, audio_stream, 0);
				if (rx_stream == nullptr)
				{
					return false;
				}
				socket_address saddr(ipaddr, port);
			
				rtl::String cname;
				if (audio_l->get_ssrc_params()->getCount() > 0)
				{
					cname.append(audio_l->get_ssrc_params()->getAt(0)->get_cname());
				}
				if (!audio_stream->create_rtcp_monitor(saddr.to_string(), rx_stream, tx_stream, cname, true))
				{
					return false;
				}
			}
		}
		else
		{
			sdp_fingerprint_t* fingerprint_l = audio_l->get_fingerprint_param();
			if (fingerprint_l != nullptr)
			{
				fingerprint_l->set_role(fingerprint_role_t::passive);
			}

			rtl::String mode = find_diff(audio_l->get_differences(), mg_sdp_difference_type::mode);
			if (mode.indexOf("sendrecv") != BAD_INDEX)
			{
				if (audio_r->get_ssrc_params()->getCount() != 0)
				{
					uint32_t ssrc_r = audio_r->get_ssrc_params()->getAt(0)->get_ssrc();
					audio_stream->update_rtcp_monitor(ssrc_r, rx_stream, tx_stream);
				}
			}
		}

		return true;
	}
	//------------------------------------------------------------------------------
	bool WebRTCTermination::apply_offer_audio(rtp_channel_t* channel, MediaSession* audio_stream, uint16_t stream_id, const rtl::String& remote_sdp)
	{
		mg_rx_stream_type_t type_rx = mg_rx_stream_type_t::mg_rx_simple_audio_e;
		mg_tx_stream_type_t type_tx = mg_tx_stream_type_t::mg_tx_simple_audio_e;
	
		int stream_index = STREAM_INDEX_BY_ID(stream_id);

		if (remote_sdp.isEmpty())
		{
			return false;
		}

		mg_sdp_media_base_t* audio_r = m_sdp_remote.get_media(stream_id);
		bool update = (audio_r == nullptr);

		if (!m_sdp_remote.read(remote_sdp, !update, stream_id))
		{
			ERROR_MSG("remote sdp read fail.");
			return false;
		}

		mg_rx_stream_t* rx_stream = find_rx_stream_by_type(type_rx, audio_stream, stream_index);
		if (rx_stream == nullptr)
		{
			return false;
		}
		mg_rx_common_stream_t* rx_common_stream = (mg_rx_common_stream_t*)rx_stream;

		mg_tx_stream_t* tx_stream = find_tx_stream_by_type(type_tx, audio_stream, stream_index);
		if (tx_stream == nullptr)
		{
			return false;
		}
		mg_tx_common_stream_t* tx_common_stream = (mg_tx_common_stream_t*)tx_stream;
		tx_common_stream->change_flag_recalc_packet(false);

		audio_r = m_sdp_remote.get_media(stream_id);
		if (audio_r == nullptr)
		{
			return false;
		}
	
		mg_sdp_media_base_t* audio_l = m_sdp_local.get_media(stream_id);
		if (audio_l == nullptr)
		{
			rx_stream->setMediaParams(*audio_r);
			tx_stream->setMediaParams(*audio_r);
		}
		else
		{
			rx_stream->setMediaParams(*audio_l);
			tx_stream->setMediaParams(*audio_l);
		}

		rtp_webrtc_channel_t* web_channel = (rtp_webrtc_channel_t*)channel;
	
		if (update)
		{
			//-------------------------------------------------------------------------
			// ssrc
			for (int i = 0; i < audio_r->get_ssrc_params()->getCount(); i++)
			{
				mg_sdp_ssrc_param_t* ssrc_param = audio_r->get_ssrc_params()->getAt(i);
				if (ssrc_param == nullptr)
				{
					continue;
				}
				bind_ssrc(ssrc_param, web_channel, rx_common_stream);
			}
			//-------------------------------------------------------------------------

			//-------------------------------------------------------------------------
			// ice
			socket_address saddr;
			socket_address saddr_rtcp;
			if (!analyze_remote_addr_ice_1(audio_r, web_channel, &saddr, &saddr_rtcp))
			{
				return false;
			}
			tx_common_stream->set_remote_address(&saddr);
			tx_common_stream->set_remote_rtcp_address(&saddr_rtcp);
			//-------------------------------------------------------------------------

			//-------------------------------------------------------------------------
			// fingerprint
			sdp_fingerprint_t* fingerprint = audio_r->get_fingerprint_param();
			if (fingerprint == nullptr || fingerprint->get_hash_length() == 0)
			{
				fingerprint = m_sdp_remote.get_fingerprint();
			}
			if (fingerprint == nullptr || fingerprint->get_hash_length() == 0)
			{
				return false;
			}
			if (!web_channel->apply_fingerprint_param(fingerprint))
			{
				return false;
			}
			//-------------------------------------------------------------------------

			//-------------------------------------------------------------------------
			if (!m_conference && !IS_ADDITIONAL_STREAM(stream_id))
			{
				if (!m_rtp_record_file_path.isEmpty())
				{
					// создаем рекордер только когда есть обратная сторона и терминатор настроен полностью.
					if (!rx_common_stream->createRecorder(m_rtp_record_file_path))
					{
						WARNING("create recorder fail");
						return false;
					}
				}
			}
		}
		else
		{
			rtl::String ssrc_str = find_diff(audio_r->get_differences(), mg_sdp_difference_type::ssrc);
			if (!ssrc_str.isEmpty())
			{
				mg_sdp_ssrc_param_t param;
				if (param.add_ssrc_line(ssrc_str))
				{
					uint32_t ssrc_old = rx_common_stream->get_ssrc_from_rtp();
					subscribe_ssrc(param.get_ssrc(), web_channel, rx_common_stream);
					audio_stream->update_rtcp_monitor(ssrc_old, rx_stream, tx_stream);
				}
			}
		}

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool WebRTCTermination::prepare_offer_webrtc_video(MediaSession* video_stream, uint16_t stream_id, const rtl::String& local_sdp, bool first)
	{
		mg_tx_stream_type_t type_tx = mg_tx_stream_type_t::mg_tx_webrtc_video_e;
		int stream_index = STREAM_INDEX_BY_ID(stream_id);

		if (local_sdp.isEmpty())
		{
			return false;
		}

		if (!m_sdp_local.read(local_sdp, !first, stream_id))
		{
			ERROR_MSG("local sdp read fail.");
			return false;
		}

		mg_sdp_media_base_t* video_l = m_sdp_local.get_media(stream_id);
		if (video_l == nullptr)
		{
			return false;
		}

		if (first)
		{
			mg_sdp_media_base_t* audio_l = m_sdp_local.get_media(MG_STREAM_TYPE_AUDIO);
			if (audio_l == nullptr)
			{
				return false;
			}

			rtp_webrtc_channel_t* audio_channel = (rtp_webrtc_channel_t*)get_channel(MG_STREAM_TYPE_AUDIO);
			if (audio_channel == nullptr)
			{
				return false;
			}

			const ip_address_t* ipaddr = m_sdp_local.get_address();
			if (ipaddr->empty())
			{
				ipaddr = audio_channel->get_net_interface();
				m_sdp_local.set_address(ipaddr->to_string());
			}
			uint16_t port = video_l->get_port();
			if (port == UINT16_MAX)
			{
				port = audio_channel->get_video_port();
				video_l->set_port(port);
			}

			//-------------------------------------------------------------------------
			// ssrc
			uint32_t local_ssrc_count = video_l->get_ssrc_params()->getCount();

			video_l->clear_webrtc_param();

			if (local_ssrc_count == 0)
			{
				local_ssrc_count = 1;
			}

			rtl::ArrayT<mg_sdp_ssrc_param_t*>* ssrc_params_audio = audio_l->get_ssrc_params();
			mg_sdp_ssrc_param_t* local_ssrc_param_audio_first = ssrc_params_audio->getAt(0);
			rtl::String label = rtl::String::empty;

			for (uint32_t i = stream_index; i < local_ssrc_count; i++)
			{
				mg_tx_stream_t* tx_stream = find_tx_stream_by_type(type_tx, video_stream, i);
				if (tx_stream == nullptr)
				{
					return false;
				}
				mg_tx_webrtc_video_stream_t* tx_webrtc_stream = (mg_tx_webrtc_video_stream_t*)tx_stream;
				tx_webrtc_stream->set_channel_event(audio_channel);
				uint32_t ssrc = tx_webrtc_stream->get_ssrc_to_rtp();

				mg_sdp_ssrc_param_t* ssrc_param = NEW mg_sdp_ssrc_param_t();
				ssrc_param->generate();
				if (label.isEmpty())
				{
					label = ssrc_param->get_label();
				}
				ssrc_param->copy_from(local_ssrc_param_audio_first);
				ssrc_param->set_ssrc(ssrc);
				ssrc_param->set_label(label);

				video_l->get_ssrc_params()->add(ssrc_param);
			}
			//-------------------------------------------------------------------------
		
			//-------------------------------------------------------------------------
			// ICE
			mg_sdp_ice_param_t* ice_audio = audio_l->get_ice_param();
			if (ice_audio == nullptr)
			{
				return false;
			}
			mg_sdp_ice_param_t* new_ice = NEW mg_sdp_ice_param_t();
			new_ice->set_ufrag(ice_audio->get_ufrag());
			new_ice->set_pwd(ice_audio->get_pwd());
			video_l->set_ice_param(new_ice);
			//-------------------------------------------------------------------------

			//-------------------------------------------------------------------------
			// fingerprint
			sdp_fingerprint_t* fingerprint_audio = audio_l->get_fingerprint_param();
			video_l->set_fingerprint_param(fingerprint_audio);
			//-------------------------------------------------------------------------

			video_l->set_transport("UDP/TLS/RTP/SAVPF");
			video_l->set_rtcp_mux(true);
		
			//-------------------------------------------------------------------------

			video_l->set_rtcp_port(audio_channel->get_ctrl_video_port());
			video_l->set_rtcp_address(audio_channel->get_net_interface());

			//-------------------------------------------------------------------------
					// update plan in local sdp
			rtl::String index;
			index << stream_id - 1;
			video_l->set_mid_type(index); // for unified plan

			mg_sdp_media_base_t* video_r = m_sdp_remote.get_media(stream_id);

			if (video_r != nullptr)
			{
				if (video_r->get_mid_type().getLength() > 1) // audio -> plan b; 0 -> unified plan.
				{
					video_l->set_mid_type(MG_STREAM_TYPE_VIDEO_STR);
				}
			}
			m_sdp_local.fill_bundle_param();
			// ------------------------------------------------------------------------
		}
		else
		{
			sdp_fingerprint_t* fingerprint_l = video_l->get_fingerprint_param();
			if (fingerprint_l != nullptr)
			{
				fingerprint_l->set_role(fingerprint_role_t::actpass);
			}
		}

		return true;
	}
	//------------------------------------------------------------------------------
	bool WebRTCTermination::apply_answer_webrtc_video(MediaSession* video_stream, uint16_t stream_id, const rtl::String& remote_sdp, bool first)
	{
		mg_rx_stream_type_t type_rx = mg_rx_stream_type_t::mg_rx_webrtc_video_e;
		mg_tx_stream_type_t type_tx = mg_tx_stream_type_t::mg_tx_webrtc_video_e;

		if (remote_sdp.isEmpty())
		{
			return false;
		}

		if (!m_sdp_remote.read(remote_sdp, !first, stream_id))
		{
			ERROR_MSG("remote sdp read fail");
			return false;
		}

		mg_sdp_media_base_t* video_r = m_sdp_remote.get_media(stream_id);
		if (video_r == nullptr)
		{
			return false;
		}
	
		mg_sdp_media_base_t* video_l = m_sdp_local.get_media(stream_id);
		if (video_l == nullptr)
		{
			return false;
		}

		rtp_webrtc_channel_t* audio_channel = (rtp_webrtc_channel_t*)get_channel(MG_STREAM_TYPE_AUDIO);
		if (audio_channel == nullptr)
		{
			return false;
		}

		video_stream->reset_first_packet_recive_flag();

		if (first)
		{
			//-------------------------------------------------------------------------
			// ice
			mg_sdp_ice_param_t* ice = video_r->get_ice_param();
			if (ice == nullptr)
			{
				return false;
			}
			const ip_address_t* ipaddr = nullptr;
			uint16_t port = 0;
			sdp_ice_candidate_t* cand = ice->get_top_candidate(audio_channel);
			if (cand != nullptr)
			{
				ipaddr = cand->get_conn_address();
				port = cand->get_conn_port();
			}
			if ((ipaddr == nullptr) || (ipaddr->empty()))
			{
				mg_sdp_media_base_t* audio_r = m_sdp_remote.get_media(MG_STREAM_TYPE_AUDIO);
				if (audio_r == nullptr)
				{
					return false;
				}
				mg_sdp_ice_param_t* ice_audio = audio_r->get_ice_param();
				if (ice_audio == nullptr)
				{
					return false;
				}
				cand = ice_audio->get_top_candidate(audio_channel);
				if (cand != nullptr)
				{
					ipaddr = cand->get_conn_address();
					port = cand->get_conn_port();
				}
			}
			socket_address saddr(ipaddr, port);
		
			const ip_address_t* ipaddr_rtcp = video_r->get_rtcp_address();
			uint16_t port_rtcp = video_r->get_rtcp_port();
			if (ipaddr_rtcp->empty())
			{
				ipaddr_rtcp = ipaddr;
				port_rtcp = port;
			}
			socket_address sockaddr_rtcp(ipaddr_rtcp, port_rtcp);
			//-------------------------------------------------------------------------

			//-------------------------------------------------------------------------
			// fingerprint
			sdp_fingerprint_t* fingerprint = video_r->get_fingerprint_param();
			if (fingerprint == nullptr || fingerprint->get_hash_length() == 0)
			{
				fingerprint = m_sdp_remote.get_fingerprint();
			}
			if (fingerprint == nullptr || fingerprint->get_hash_length() == 0)
			{
				return false;
			}
			//-------------------------------------------------------------------------

			//-------------------------------------------------------------------------
			// rtcp monitor
			mg_sdp_ssrc_param_t* ssrc_param_l_first = video_l->get_ssrc_params()->getAt(0);
			if (ssrc_param_l_first == nullptr)
			{
				return false;
			}
			rtl::String cname(ssrc_param_l_first->get_cname());
			//-------------------------------------------------------------------------

			//-------------------------------------------------------------------------
			// ssrc
			for (int i = 0; i < video_r->get_ssrc_params()->getCount(); i++)
			{
				mg_sdp_ssrc_param_t* ssrc_param = video_r->get_ssrc_params()->getAt(i);
				if (ssrc_param == nullptr)
				{
					continue;
				}
				mg_rx_stream_t* rx_stream = find_rx_stream_by_type(type_rx, video_stream, i);
				if (rx_stream == nullptr)
				{
					return false;
				}

				mg_tx_stream_t* tx_stream = find_tx_stream_by_type(type_tx, video_stream, i);
				if (tx_stream == nullptr)
				{
					return false;
				}
				mg_tx_webrtc_video_stream_t* tx_webrtc_stream = (mg_tx_webrtc_video_stream_t*)tx_stream;

				rx_stream->setMediaParams(*video_r);
				tx_stream->setMediaParams(*video_r);

				tx_webrtc_stream->set_remote_address(&saddr);
				tx_webrtc_stream->set_remote_rtcp_address(&sockaddr_rtcp);
			
				bind_ssrc(ssrc_param, audio_channel, rx_stream);

				//-------------------------------------------------------------------------
				// rtcp monitor
				if (!video_stream->create_rtcp_monitor(saddr.to_string(), rx_stream, tx_stream, cname, true))
				{
					WARNING("create rtcp monitor fail.");
					return false;
				}
				//-------------------------------------------------------------------------
			}
			//-------------------------------------------------------------------------	
		}
	
		// @@George 19.08.19 : временно замороженно. Видезапись будет продолжена позже
		//if (!m_conference && !IS_ADDITIONAL_STREAM(stream_id) && isVideoRecordingEnabled())
		//{
		//	if (!m_rtp_record_file_path.isEmpty())
		//	{
		//		int stream_index = STREAM_INDEX_BY_ID(stream_id);
		//		mg_rx_stream_t* rx_stream = find_rx_stream_by_type(type_rx, video_stream, stream_index);
		//		if (rx_stream != nullptr)
		//		{
		//			mg_rx_webrtc_video_stream_t* rx_webrtc_video_stream = (mg_rx_webrtc_video_stream_t*)rx_stream;
		//			// создаем рекордер только когда есть обратная сторона и терминатор настроен полностью.
		//			if (!rx_webrtc_video_stream->create_recorder(m_rtp_record_file_path + 'v'))
		//			{
		//				PLOG_WARNING(LOG_PREFIX, "apply_answer_webrtc_video -- ID(%s): create recorder fail.", getName());
		//				//return false;
		//			}
		//		}
		//	}
		//}

		update_rtcp_monitors();

		return true;
	}
	//------------------------------------------------------------------------------
	bool WebRTCTermination::prepare_answer_webrtc_video(MediaSession* video_stream, uint16_t stream_id, const rtl::String& local_sdp, bool first)
	{
		mg_tx_stream_type_t type_tx = mg_tx_stream_type_t::mg_tx_webrtc_video_e;
		mg_rx_stream_type_t type_rx = mg_rx_stream_type_t::mg_rx_webrtc_video_e;
		int stream_index = STREAM_INDEX_BY_ID(stream_id);

		if (local_sdp.isEmpty())
		{
			return false;
		}

		if (!m_sdp_local.read(local_sdp, !first, stream_id))
		{
			ERROR_MSG("local sdp read fail.");
			return false;
		}

		mg_sdp_media_base_t* video_l = m_sdp_local.get_media(stream_id);
		if (video_l == nullptr)
		{
			return false;
		}

		mg_sdp_media_base_t* video_r = m_sdp_remote.get_media(stream_id);
		if (video_r == nullptr)
		{
			return false;
		}

		mg_sdp_media_base_t* audio_l = m_sdp_local.get_media(MG_STREAM_TYPE_AUDIO);
		if (audio_l == nullptr)
		{
			return false;
		}

		rtp_webrtc_channel_t* audio_channel = (rtp_webrtc_channel_t*)get_channel(MG_STREAM_TYPE_AUDIO);
		if (audio_channel == nullptr)
		{
			return false;
		}

		const ip_address_t* ipaddr = m_sdp_local.get_address();
		if (ipaddr->empty())
		{
			ipaddr = audio_channel->get_net_interface();
			m_sdp_local.set_address(ipaddr->to_string());
		}
		uint16_t port = video_l->get_port();
		if (port == UINT16_MAX)
		{
			port = audio_channel->get_video_port();
			video_l->set_port(port);
		}

		if (first)
		{
			//-------------------------------------------------------------------------
			// ssrc
			uint32_t local_ssrc_count = video_l->get_ssrc_params()->getCount();
			video_l->clear_webrtc_param();

			if (local_ssrc_count == 0)
			{
				local_ssrc_count = 1;
			}

			rtl::ArrayT<mg_sdp_ssrc_param_t*>* ssrc_params_audio = audio_l->get_ssrc_params();
			mg_sdp_ssrc_param_t* local_ssrc_param_audio_first = ssrc_params_audio->getAt(0);
			rtl::String label = rtl::String::empty;

			for (uint32_t i = stream_index; i < local_ssrc_count; i++)
			{
				mg_tx_stream_t* tx_stream = find_tx_stream_by_type(type_tx, video_stream, i);
				if (tx_stream == nullptr)
				{
					return false;
				}
				mg_tx_webrtc_video_stream_t* tx_webrtc_stream = (mg_tx_webrtc_video_stream_t*)tx_stream;
				tx_webrtc_stream->set_channel_event(audio_channel);
				uint32_t ssrc = tx_webrtc_stream->get_ssrc_to_rtp();

				mg_sdp_ssrc_param_t* ssrc_param = NEW mg_sdp_ssrc_param_t();
				ssrc_param->generate();
				if (label.isEmpty())
				{
					label = ssrc_param->get_label();
				}
				ssrc_param->copy_from(local_ssrc_param_audio_first);
				ssrc_param->set_ssrc(ssrc);
				ssrc_param->set_label(label);

				video_l->get_ssrc_params()->add(ssrc_param);

				//-------------------------------------------------------------------------
				// rtcp monitor
				mg_rx_stream_t* rx_stream = find_rx_stream_by_type(type_rx, video_stream, i);
				if (rx_stream == nullptr)
				{
					return false;
				}

				socket_address saddr(ipaddr, port);
				if (!video_stream->create_rtcp_monitor(saddr.to_string(), rx_stream, tx_stream, ssrc_param->get_cname(), true))
				{
					return false;
				}
				//-------------------------------------------------------------------------
			}
			//-------------------------------------------------------------------------
	

			//-------------------------------------------------------------------------
			// ICE
			mg_sdp_ice_param_t* ice_audio = audio_l->get_ice_param();
			if (ice_audio == nullptr)
			{
				return false;
			}
			mg_sdp_ice_param_t* new_ice = NEW mg_sdp_ice_param_t();
			new_ice->set_ufrag(ice_audio->get_ufrag());
			new_ice->set_pwd(ice_audio->get_pwd());
			video_l->set_ice_param(new_ice);
			//-------------------------------------------------------------------------

			//-------------------------------------------------------------------------
			// fingerprint
			sdp_fingerprint_t* fingerprint_audio = audio_l->get_fingerprint_param();
			video_l->set_fingerprint_param(fingerprint_audio);
			//-------------------------------------------------------------------------

			video_l->set_transport("UDP/TLS/RTP/SAVPF");
			video_l->set_rtcp_mux(true);

			//-------------------------------------------------------------------------

			video_l->set_rtcp_port(audio_channel->get_ctrl_video_port());
			video_l->set_rtcp_address(audio_channel->get_net_interface());

			//-------------------------------------------------------------------------
			// update plan in local sdp
			rtl::String index;
			index << stream_id - 1;
			video_l->set_mid_type(index); // for unified plan

			if (video_r->get_mid_type().getLength() > 1) // audio -> plan b; 0 -> unified plan.
			{
				video_l->set_mid_type(MG_STREAM_TYPE_VIDEO_STR);
			}

			m_sdp_local.fill_bundle_param();
		}

		update_rtcp_monitors();

		return true;
	}
	//------------------------------------------------------------------------------
	bool WebRTCTermination::apply_offer_webrtc_video(MediaSession* video_stream, uint16_t stream_id, const rtl::String& remote_sdp, bool first)
	{
		mg_rx_stream_type_t type_rx = mg_rx_stream_type_t::mg_rx_webrtc_video_e;
		mg_tx_stream_type_t type_tx = mg_tx_stream_type_t::mg_tx_webrtc_video_e;

		if (remote_sdp.isEmpty())
		{
			return false;
		}
	
		if (!m_sdp_remote.read(remote_sdp, !first, stream_id))
		{
			ERROR_MSG("remote sdp read fail");
			return false;
		}

		mg_sdp_media_base_t* video_r = m_sdp_remote.get_media(stream_id);
		if (video_r == nullptr)
		{
			return false;
		}

		mg_sdp_media_base_t* audio_r = m_sdp_remote.get_media(MG_STREAM_TYPE_AUDIO);
		if (audio_r == nullptr)
		{
			return false;
		}

		rtp_webrtc_channel_t* audio_channel = (rtp_webrtc_channel_t*)get_channel(MG_STREAM_TYPE_AUDIO);
		if (audio_channel == nullptr)
		{
			return false;
		}

		video_stream->reset_first_packet_recive_flag();

		if (first)
		{
			//-------------------------------------------------------------------------
			// ice
			mg_sdp_ice_param_t* ice = audio_r->get_ice_param();
			if (ice == nullptr)
			{
				return false;
			}
			const ip_address_t* ipaddr = nullptr;
			uint16_t port = 0;
			sdp_ice_candidate_t* cand = ice->get_top_candidate(audio_channel);
			if (cand != nullptr)
			{
				ipaddr = cand->get_conn_address();
				port = cand->get_conn_port();
			}
			socket_address saddr(ipaddr, port);
			
			//-------------------------------------------------------------------------
			mg_sdp_ice_param_t* ice_v = video_r->get_ice_param();
			const ip_address_t* ipaddr_from_candidat = nullptr;
			uint16_t port_from_candidat = 0;
			sdp_ice_candidate_t* cand_v = (ice_v == nullptr) ? nullptr : ice->get_top_candidate(audio_channel);
			if (cand_v != nullptr)
			{
				ipaddr_from_candidat = cand_v->get_conn_address();
				port_from_candidat = cand_v->get_conn_port();
			}

			const ip_address_t* ipaddr_rtcp = (ipaddr_from_candidat == nullptr || ipaddr_from_candidat->empty()) ? video_r->get_rtcp_address() : ipaddr_from_candidat;
			uint16_t port_rtcp = (ipaddr_from_candidat == nullptr || ipaddr_from_candidat->empty()) ? video_r->get_rtcp_port() : port_from_candidat;

			if (ipaddr_rtcp == nullptr || ipaddr_rtcp->empty())
			{
				ipaddr_rtcp = saddr.get_ip_address();
				port_rtcp = saddr.get_port();
			}
			socket_address sockaddr_rtcp(ipaddr_rtcp, port_rtcp);

			//-------------------------------------------------------------------------

			//-------------------------------------------------------------------------
			// ssrc
			for (int i = 0; i < video_r->get_ssrc_params()->getCount(); i++)
			{
				mg_sdp_ssrc_param_t* ssrc_param = video_r->get_ssrc_params()->getAt(i);
				if (ssrc_param == nullptr)
				{
					continue;
				}

				mg_rx_stream_t* rx_stream = find_rx_stream_by_type(type_rx, video_stream, i);
				if (rx_stream == nullptr)
				{
					return false;
				}

				mg_tx_stream_t* tx_stream = find_tx_stream_by_type(type_tx, video_stream, i);
				if (tx_stream == nullptr)
				{
					return false;
				}
				mg_tx_webrtc_video_stream_t* tx_webrtc_stream = (mg_tx_webrtc_video_stream_t*)tx_stream;

				rx_stream->setMediaParams(*video_r);
				tx_stream->setMediaParams(*video_r);

				tx_webrtc_stream->set_remote_address(&saddr);
				tx_webrtc_stream->set_remote_rtcp_address(&saddr);

				bind_ssrc(ssrc_param, audio_channel, rx_stream);
			}
			//-------------------------------------------------------------------------

			//-------------------------------------------------------------------------
			// fingerprint
			sdp_fingerprint_t* fingerprint = video_r->get_fingerprint_param();
			if (fingerprint == nullptr || fingerprint->get_hash_length() == 0)
			{
				fingerprint = m_sdp_remote.get_fingerprint();
			}
			if (fingerprint == nullptr || fingerprint->get_hash_length() == 0)
			{
				return false;
			}
			//-------------------------------------------------------------------------
		}
		else
		{
			int index_ssrc_stream = -1;
			mg_sdp_diff_list* diff_list = video_r->get_differences();
			if (diff_list != nullptr)
			{
				for (int i = 0; i < diff_list->getCount(); i++)
				{
					mg_sdp_difference_t* diff = diff_list->getAt(i);
					if (diff == nullptr)
					{
						continue;
					}

					if (diff->type == mg_sdp_difference_type::ssrc)
					{
						mg_sdp_ssrc_param_t param;
						if (param.add_ssrc_line(diff->line))
						{
							mg_rx_stream_t* rx_stream = nullptr;
							mg_tx_stream_t* tx_stream = nullptr;
							if (diff->line.indexOf("cname") != BAD_INDEX)
							{
								index_ssrc_stream++;
							}
							rx_stream = find_rx_stream_by_type(type_rx, video_stream, index_ssrc_stream);
							tx_stream = find_tx_stream_by_type(type_tx, video_stream, index_ssrc_stream);
							if (rx_stream == nullptr || tx_stream == nullptr)
							{
								continue;
							}
							mg_rx_webrtc_video_stream_t* rx_webrtc_stream = (mg_rx_webrtc_video_stream_t*)rx_stream;
							uint32_t ssrc_old = rx_webrtc_stream->get_ssrc_from_rtp();
							subscribe_ssrc(param.get_ssrc(), audio_channel, rx_webrtc_stream);
							video_stream->update_rtcp_monitor(ssrc_old, rx_stream, tx_stream);
						}
						break;
					}
				}
			}
		}

		// @@George 19.08.19 : временно замороженно. Видезапись будет продолжена позже
		//if (!m_conference && !IS_ADDITIONAL_STREAM(stream_id) && isVideoRecordingEnabled())
		//{
		//	if (!m_rtp_record_file_path.isEmpty())
		//	{
		//		int stream_index = STREAM_INDEX_BY_ID(stream_id);
		//		mg_rx_stream_t* rx_stream = find_rx_stream_by_type(type_rx, video_stream, stream_index);
		//		if (rx_stream != nullptr)
		//		{
		//			mg_rx_webrtc_video_stream_t* rx_webrtc_video_stream = (mg_rx_webrtc_video_stream_t*)rx_stream;
		//			// создаем рекордер только когда есть обратная сторона и терминатор настроен полностью.
		//			if (!rx_webrtc_video_stream->create_recorder(m_rtp_record_file_path + 'v'))
		//			{
		//				PLOG_WARNING(LOG_PREFIX, "apply_offer_webrtc_video -- ID(%s): create recorder fail.", getName());
		//				//return false;
		//			}
		//		}
		//	}
		//}

		return true;
	}
	//------------------------------------------------------------------------------
	// в пакете rtcp по аудио стриму пришла информация о стриме видео. по этому необходимо научить мониторы знанию о других потоках
	//------------------------------------------------------------------------------
	void WebRTCTermination::update_rtcp_monitors()
	{
		mg_rtcp_triple_list trilist;
	
		MediaSession* audio_stream = Termination::findMediaSession(MG_STREAM_TYPE_AUDIO);
		MediaSession* video_stream = Termination::findMediaSession(MG_STREAM_TYPE_VIDEO);

		if (audio_stream != nullptr)
		{
			for (int i = 0; i < audio_stream->get_rtcp_monitors()->getCount(); i++)
			{
				rtcp_monitor_t* mon = audio_stream->get_rtcp_monitors()->getAt(i);
				rtcp_triple_t tr;
				tr.ssrc = mon->get_ssrc();
				tr.ssrc_opposite = mon->get_ssrc_opposite();
				memcpy(tr.cname, mon->get_cname(), MD5_HASH_HEX_SIZE);
				tr.cname[MD5_HASH_HEX_SIZE] = 0;
				trilist.add(tr);

				PRINT_FMT("| audio | %10u --> %10u", tr.ssrc, tr.ssrc_opposite);
			}
		}
		if (video_stream != nullptr)
		{
			for (int i = 0; i < video_stream->get_rtcp_monitors()->getCount(); i++)
			{
				rtcp_monitor_t* mon = video_stream->get_rtcp_monitors()->getAt(i);
				rtcp_triple_t tr;
				tr.ssrc = mon->get_ssrc();
				tr.ssrc_opposite = mon->get_ssrc_opposite();
				memcpy(tr.cname, mon->get_cname(), MD5_HASH_HEX_SIZE);
				tr.cname[MD5_HASH_HEX_SIZE] = 0;
				trilist.add(tr);

				PRINT_FMT("| video | %10u --> %10u", tr.ssrc, tr.ssrc_opposite);
			}
		}

		if (audio_stream != nullptr)
		{
			for (int i = 0; i < audio_stream->get_rtcp_monitors()->getCount(); i++)
			{
				rtcp_monitor_t* mon = audio_stream->get_rtcp_monitors()->getAt(i);
				mon->set_triple(&trilist);
			}
		}
		if (video_stream != nullptr)
		{
			for (int i = 0; i < video_stream->get_rtcp_monitors()->getCount(); i++)
			{
				rtcp_monitor_t* mon = video_stream->get_rtcp_monitors()->getAt(i);
				mon->set_triple(&trilist);
			}
		}
	}
	//------------------------------------------------------------------------------
	bool WebRTCTermination::bind_ssrc(mg_sdp_ssrc_param_t* ssrc_param, rtp_channel_t* channel, mg_rx_stream_t* rx_stream)
	{
		if (ssrc_param == nullptr)
		{
			return false;
		}

		subscribe_ssrc(ssrc_param->get_ssrc(), channel, rx_stream);

		rtp_webrtc_channel_t* web_channel = (rtp_webrtc_channel_t*)channel;
		if (rx_stream->getType() == mg_rx_stream_type_t::mg_rx_webrtc_video_e)
		{
			mg_rx_webrtc_video_stream_t* stream = (mg_rx_webrtc_video_stream_t*)rx_stream;
			stream->set_channel_event(web_channel);
		}
		else if ((rx_stream->getType() == mg_rx_stream_type_t::mg_rx_simple_audio_e) ||
			(rx_stream->getType() == mg_rx_stream_type_t::mg_rx_simple_video_e))
		{
			mg_rx_common_stream_t* stream = (mg_rx_common_stream_t*)rx_stream;
			stream->set_channel_event(web_channel);
		}
		else
		{
			return false;
		}

		return true;
	}
	//------------------------------------------------------------------------------
	bool WebRTCTermination::subscribe_ssrc(uint32_t ssrc, rtp_channel_t* channel, mg_rx_stream_t* rx_stream)
	{
		rtp_webrtc_channel_t* web_channel = (rtp_webrtc_channel_t*)channel;
		const rtp_webrtc_subscriber_t* sub = web_channel->get_event_subscriber(ssrc);

		if (rx_stream->getType() == mg_rx_stream_type_t::mg_rx_webrtc_video_e)
		{
			mg_rx_webrtc_video_stream_t* stream = (mg_rx_webrtc_video_stream_t*)rx_stream;
			if (sub == nullptr)
			{
				web_channel->set_event_handler_rx(ssrc, stream);
			}
			stream->set_ssrc_from_rtp(ssrc);
		}
		else if ((rx_stream->getType() == mg_rx_stream_type_t::mg_rx_simple_audio_e) ||
			(rx_stream->getType() == mg_rx_stream_type_t::mg_rx_simple_video_e))
		{
			mg_rx_common_stream_t* stream = (mg_rx_common_stream_t*)rx_stream;
			if (sub == nullptr)
			{
				web_channel->set_event_handler_rx(ssrc, stream);
			}
			stream->set_ssrc_from_rtp(ssrc);
		}
		else
		{
			return false;
		}

		return true;
	}
	//------------------------------------------------------------------------------
	// analyze the address using ice (AUDIO)
	//------------------------------------------------------------------------------
	bool WebRTCTermination::analyze_remote_addr_ice_1(mg_sdp_media_base_t* audio_r, rtp_channel_t* channel, socket_address* saddr, socket_address* saddr_rtcp)
	{
		rtp_webrtc_channel_t* web_channel = (rtp_webrtc_channel_t*)channel;

		if (audio_r == nullptr)
		{
			return false;
		}

		mg_sdp_ice_param_t* ice = audio_r->get_ice_param();
		if (ice == nullptr)
		{
			return false;
		}

		const ip_address_t* ipaddr = nullptr;
		uint16_t port = 0;
		sdp_ice_candidate_t* cand = ice->get_top_candidate(channel);
		if (cand != nullptr)
		{
			ipaddr = cand->get_conn_address();
			port = cand->get_conn_port();
		}

		const ip_address_t* ipaddr_rtcp = audio_r->get_rtcp_address();
		uint16_t port_rtcp = audio_r->get_rtcp_port();

		saddr->set_ip_address_with_port(ipaddr, port);
		if ((audio_r->get_port() == 9) && (IS_ADDITIONAL_STREAM(audio_r->get_stream_id())))
		{
			mg_sdp_media_base_t* audio_r_0 = m_sdp_remote.get_media(MG_STREAM_TYPE_AUDIO);
			mg_sdp_ice_param_t* ice_0 = nullptr;
			if (audio_r_0 != nullptr)
			{
				ice_0 = audio_r_0->get_ice_param();
				ipaddr_rtcp = audio_r_0->get_rtcp_address();
				port_rtcp = audio_r_0->get_rtcp_port();
			}
			if (ice_0 == nullptr)
			{
				return false;
			}
			cand = ice_0->get_top_candidate(channel);
			if (cand != nullptr)
			{
				ipaddr = cand->get_conn_address();
				port = cand->get_conn_port();
			}
			saddr->set_ip_address_with_port(ipaddr, port);
		}
		else if (!web_channel->setup_ice_params_remote(ice->get_ufrag(), ice->get_pwd()))
		{
			return false;
		}

		if (ipaddr_rtcp->empty())
		{
			ipaddr_rtcp = ipaddr;
			port_rtcp = port;
		}
		saddr_rtcp->set_ip_address_with_port(ipaddr_rtcp, port_rtcp);

		return true;
	}
	//------------------------------------------------------------------------------
	// (AUDIO)
	//------------------------------------------------------------------------------
	void WebRTCTermination::prepare_ssrc_local_1(uint16_t stream_id, uint32_t ssrc, mg_sdp_media_base_t* media)
	{
		ENTER_FMT("streamId: %u, ssrc:%u", stream_id, ssrc);

		media->clear_ssrc_params();
		mg_sdp_ssrc_param_t* ssrc_param = NEW mg_sdp_ssrc_param_t();
		if (IS_ADDITIONAL_STREAM(stream_id))
		{
			mg_sdp_media_base_t* audio_l_0 = m_sdp_local.get_media(MG_STREAM_TYPE_AUDIO);
			if ((audio_l_0 != nullptr) && (audio_l_0->get_ssrc_params()->getCount() > 0))
			{
				ssrc_param->copy_from(audio_l_0->get_ssrc_params()->getAt(0));
			}
		}
		else
		{
			ssrc_param->generate();
		}
		ssrc_param->set_ssrc(ssrc);
		media->get_ssrc_params()->add(ssrc_param);

		PRINT_FMT("ssrc_param:%u", getName(), ssrc_param->get_ssrc());
	}
	//------------------------------------------------------------------------------
	// (AUDIO)
	//------------------------------------------------------------------------------
	bool WebRTCTermination::prepare_ice_local_1(uint16_t stream_id, rtp_channel_t* channel, mg_sdp_media_base_t* media)
	{
		rtp_webrtc_channel_t* web_channel = (rtp_webrtc_channel_t*)channel;
		bool need_delete = false;
		mg_sdp_ice_param_t* ice = nullptr;
		if (IS_ADDITIONAL_STREAM(stream_id))
		{
			mg_sdp_media_base_t* audio_l_0 = m_sdp_local.get_media(MG_STREAM_TYPE_AUDIO);
			ice = (audio_l_0 == nullptr) ? nullptr : audio_l_0->get_ice_param();
		}
		if (ice == nullptr)
		{
			ice = NEW mg_sdp_ice_param_t();
			need_delete = true;
			if (!ice->generate(web_channel))
			{
				DELETEO(ice);
				return false;
			}
		}

		if (!web_channel->setup_ice_params_local(ice->get_ufrag(), ice->get_pwd()))
		{
			return false;
		}
		media->set_ice_param(ice);

		if (need_delete)
		{
			DELETEO(ice);
		}
		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	rtl::String WebRTCTermination::find_diff(mg_sdp_diff_list* diff_list, mg_sdp_difference_type type)
	{
		if (diff_list == nullptr)
		{
			return rtl::String::empty;
		}

		for (int i = 0; i < diff_list->getCount(); i++)
		{
			mg_sdp_difference_t* diff = diff_list->getAt(i);
			if (diff == nullptr)
			{
				continue;
			}

			if (diff->type == type)
			{
				return diff->line;
			}
		}

		return rtl::String::empty;
	}
}
//------------------------------------------------------------------------------
