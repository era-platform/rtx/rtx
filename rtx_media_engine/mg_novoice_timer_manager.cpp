﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_novoice_timer_manager.h"
#include "mg_rx_common_stream.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	DECLARE mg_novoice_timer_manager_t* g_mg_novoice_timer_manager = nullptr;
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	struct mg_novoice_timer_manager_element
	{
		mg_rx_common_stream_t* rx_stream = nullptr;
		mg_novoice_timer_manager_element* next = nullptr;
		uint32_t duration = 0;
		uint32_t timer_timer_period = 1000;
	};
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_novoice_timer_manager_t::mg_novoice_timer_manager_t() :
		m_sync("nov-man"), m_timer(nullptr), m_timer_enter(false), m_link_first(nullptr), m_link_last(nullptr)

	{
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_novoice_timer_manager_t::~mg_novoice_timer_manager_t()
	{
		mg_novoice_timer_manager_element* current = m_link_first;

		while (current != nullptr)
		{
			mg_novoice_timer_manager_element* next = current->next;

			current->rx_stream = nullptr;
			DELETEO(current);
			current = next;
		}

		stop();
	}
	//------------------------------------------------------------------------------
	// старт манагера (таймер отключен)
	//------------------------------------------------------------------------------
	bool mg_novoice_timer_manager_t::start()
	{
		LOG_EVENT("nov-man", "Start.....enter");

		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		if (m_timer == nullptr)
		{
			m_timer_enter = false;
			m_timer = NEW rtl::Timer(false, this, Log, "m-rec-man");
			bool res = m_timer->start(1000);

			LOG_EVENT("nov-man", "Start.....timer started %s %d", m_timer->getName(), res);
		}
		else
		{
			LOG_EVENT("nov-man", "Start.....Already started!");
		}

		LOG_EVENT("nov-man", "Start.....leave");

		return true;
	}
	//------------------------------------------------------------------------------
	// останов манагера
	//------------------------------------------------------------------------------
	void mg_novoice_timer_manager_t::stop()
	{
		LOG_EVENT("nov-man", "Stop.....enter");

		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		if (m_timer != nullptr)
		{
			m_link_first = m_link_last = nullptr;
			m_timer->stop();
			DELETEO(m_timer);

			LOG_EVENT("nov-man", "Stop.....timer stopped");

			m_timer = nullptr;
		}

		LOG_EVENT("nov-man", "Stop.....leave");
	}
	//------------------------------------------------------------------------------
	// добавление стрима (старт таймера)
	//------------------------------------------------------------------------------
	bool mg_novoice_timer_manager_t::add(mg_rx_common_stream_t* rx_stream)
	{
		mg_novoice_timer_manager_element* link = NEW mg_novoice_timer_manager_element();
		link->rx_stream = rx_stream;

		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		if (m_link_last == nullptr)
		{
			m_link_last = link;
			m_link_first = m_link_last;

			start();
		}
		else
		{
			m_link_last->next = link;
			m_link_last = link;
		}

		rx_stream->m_last_tick = rtl::DateTime::getTicks();
		rx_stream->m_last_send_novoice = rx_stream->m_last_tick;

		return true;
	}
	//------------------------------------------------------------------------------
	// отключение стрима от таймера (останов таймера если больше нет стримов)
	//------------------------------------------------------------------------------
	void mg_novoice_timer_manager_t::remove(mg_rx_common_stream_t* rx_stream)
	{
		if (rx_stream == nullptr)
		{
			return;
		}

		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		mg_novoice_timer_manager_element* current = m_link_first;
		mg_novoice_timer_manager_element* prev = nullptr;

		while (current != nullptr)
		{
			if (current->rx_stream == rx_stream)
			{
				if (prev != nullptr)
				{
					prev->next = current->next;

					// если был последним то обновим ссылку на последнего
					if (current == m_link_last)
					{
						m_link_last = prev;
					}
				}
				else
				{
					m_link_first = current->next;

					// если единственный то ссылка на последнего тоже обнуляется
					if (m_link_first == nullptr)
					{
						m_link_last = nullptr;
					}
				}

				// ждем окончания работы таймера
				while (m_timer_enter)
				{
					rtl::Thread::sleep(5);
				}

				mg_novoice_timer_manager_element* next = nullptr;
				next = current->next;

				current->rx_stream = nullptr;
				DELETEO(current);

				current = next;
			}
			else
			{
				prev = current;
				current = current->next;
			}
		}

		if (m_link_first == nullptr)
		{
			stop();
		}
	}
	//------------------------------------------------------------------------------
	// обход стримов
	//------------------------------------------------------------------------------
	void mg_novoice_timer_manager_t::timer_elapsed_event(rtl::Timer* timer, int tid)
	{
		m_timer_enter = true;

		uint32_t currentTime = rtl::DateTime::getTicks();
		//	LOG_EVENT("nov-man", "tick...... currentTime:%u", currentTime);

		mg_novoice_timer_manager_element* current = m_link_first;

		while (current != nullptr)
		{
			mg_rx_common_stream_t* rx_stream = current->rx_stream;
			if (rx_stream == nullptr)
			{
				LOG_ERROR("nov-man", "timer_elapsed_event -- rx_stream is null.");
				current = current->next;
				continue;
			}

			if (!rx_stream->activity())
			{
				current = current->next;
				continue;
			}

			uint32_t tick_duration = currentTime - rx_stream->m_last_tick;
			current->duration += tick_duration;
			//LOG_EVENT("nov-man", "tick...... rx_stream:%s, last_tick:%u, tick_duration:%u.", rx_stream->getName(), rx_stream->m_last_tick, tick_duration);

			if (tick_duration >= (current->timer_timer_period - 10))
			{
				if (!rx_stream->m_cn_packet)
				{
					if ((currentTime - rx_stream->m_last_send_novoice) >= 10000)
					{
						rx_stream->mg_termination__no_voice_event(current->duration);
						rx_stream->m_last_send_novoice = currentTime;
						rx_stream->m_no_voice = true;
					}
				}
			}

			current = current->next;
		}

		m_timer_enter = false;
	}
}
//------------------------------------------------------------------------------
