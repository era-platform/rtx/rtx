﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_tube.h"
#include "mg_fax_t38.h"

namespace mge
{
	//--------------------------------------
	//
	//--------------------------------------
	class mg_udptl_rx_tube_t : public Tube
	{
		// rx common
		udptl_rx_frame_t* m_rx_queue;					// [UDPTL_ECM_BUFFER_SIZE] очередь пакетов на обработку
		int m_rx_queue_size;							// размер очереди
		// rx fec
		int m_rx_seq_no;								// номер ожидаемого пакета
		udptl_rx_frame_fec_t* m_rx_buffer;				// [UDPTL_ECM_BUFFER_SIZE] буфер входящих пакетов
	public:
		mg_udptl_rx_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id);
		virtual ~mg_udptl_rx_tube_t();

		virtual bool init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out) override;
		virtual void send(const INextDataHandler* out_handler, const rtp_packet* packet) override;
		virtual void send(const INextDataHandler* out_handler, const uint8_t* data, uint32_t len) override;

	private:
		bool parse_rx_packet(const uint8_t* packet, int length);
		bool parse_rx_packet_red(int seq_no, const uint8_t* packet, int length, int index);
		bool parse_rx_packet_fec(int seq_no, const uint8_t* ifp, int ifp_length, const uint8_t* packet, int length, int index);

		int parse_frame_length(const uint8_t* packet, int length, int *index, int *ret_value);
		bool parse_frame(const uint8_t *packet, int length, int *index, const uint8_t **ifp, int *ifp_length);
	};
}
//--------------------------------------
