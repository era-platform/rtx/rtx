/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_codec_to_img_tube.h"
#include "mg_media_element.h"
#include "rtp_packet.h"

#include "logdef_tube.h"


namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_codec_to_img_tube_t::mg_codec_to_img_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id) :
		Tube(HandlerType::VideoDecoder, id, log), m_jitter(log)
	{
		m_decoder = nullptr;

		makeName(parent_id, "-cimg-tu"); // Codec to IMG Tube

		rtl::res_counter_t::add_ref(g_mge_tube_counter);

		setLogTag("TUB-VDEC");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_codec_to_img_tube_t::~mg_codec_to_img_tube_t()
	{
		unsubscribe(this);

		if (m_decoder != nullptr)
		{
			rtx_codec__release_video_decoder(m_decoder);
			m_decoder = nullptr;
		}

		rtl::res_counter_t::release(g_mge_tube_counter);

#if defined TEST_VIDEO_CODEC
		m_out.close();
#endif // TEST_VIDEO_CODEC

	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_codec_to_img_tube_t::init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out)
	{
		if (payload_el_in == nullptr)
		{
			WARNING("payload element is null!");
			return false;
		}

		m_payload_id = payload_el_in->get_payload_id();

		PRINT_FMT("payload in: %s %d", (const char*)payload_el_in->get_element_name(), m_payload_id);

		media::PayloadFormat format;
		format.setEncoding(payload_el_in->get_element_name());
		format.setFrequency(payload_el_in->get_sample_rate());
		format.setChannelCount(payload_el_in->getChannelCount());
		format.setFmtp(payload_el_in->get_param_fmtp());
		format.setId((media::PayloadId)payload_el_in->get_payload_id());

		m_decoder = rtx_codec__create_video_decoder(format.getEncoding(), &format);
		if (m_decoder == nullptr)
		{
			ERROR_FMT("failed to create video decoder for %s.", (const char*)format.getEncoding());
			return false;
		}

#if defined TEST_VIDEO_CODEC
		char path[260];
		int written = std_snprintf(path, 260, "%s%c%s_%u_in.raw", Log.get_folder(), FS_PATH_DELIMITER, (const char*)format.getEncoding(), m_id);
		m_out.create(path);
#endif // TEST_VIDEO_CODEC

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_codec_to_img_tube_t::send(const INextDataHandler* out_handler, const rtp_packet* packet)
	{
		if (packet == nullptr)
		{
			ERROR_MSG("nothing to send!");
			return;
		}

		if (m_decoder == nullptr)
		{
			ERROR_MSG("decoder is null!");
			return;
		}

#ifdef TEST_VIDEO_CODEC
		uint8_t buffer[2048];
		uint32_t sign = 0x80011008;
		uint32_t written = packet->write_packet(buffer, 2048);
		m_out.write(&sign, sizeof(uint32_t));
		m_out.write(&written, sizeof(uint32_t));
		m_out.write(buffer, written);
#endif

		m_jitter.pushPacket(packet);

		rtl::ArrayT<rtp_packet*> frame;

		if (!m_jitter.getReadyFrame(frame))
		{
			return;
		}

		//PLOG_TUBE_WRITE(LOG_PREFIX, "%s frame ready... %d packets", m_decoder->getEncoding(), frame.getCount());

		INextDataHandler* out = getNextHandler();
		if (out != nullptr)
		{
			for (int i = 0; i < frame.getCount(); i++)
			{
				rtp_packet* out_packet = frame.getAt(i);

				media::VideoImage* image = m_decoder->decode(out_packet);
				if (image != nullptr)
				{
					out->send(this, (uint8_t*)image, 0);
				}
			}
		}

		m_jitter.releaseFrame();
	}
}
//------------------------------------------------------------------------------
