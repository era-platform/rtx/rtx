﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_thread.h"
#include "mg_session.h"
#include "mg_rx_stream.h"
#include "mg_rx_common_stream.h"
#include "mg_rx_device_stream.h"
#include "mg_rx_dummy_stream.h"
#include "mg_rx_fax_t30_stream.h"
#include "mg_rx_fax_t38_stream.h"
#include "mg_rx_fax_proxy_stream.h"
#include "mg_rx_webrtc_video_stream.h"
#include "mg_rx_ivr_stream.h"
#include "mg_tx_common_stream.h"
#include "mg_tx_device_stream.h"
#include "mg_tx_dummy_stream.h"
#include "mg_tx_fax_proxy_stream.h"
#include "mg_tx_fax_t38_stream.h"
#include "mg_tx_fax_t30_stream.h"
#include "mg_tx_webrtc_video_stream.h"
#include "mg_tx_ivr_stream.h"
#include "mg_termination.h"
#include "mg_novoice_timer_manager.h"

 //------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//#define LOG_PREFIX "SESS" // Media Gateway Stream

#include "logdef_stream.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	DECLARE rtl::Logger* g_mg_stream_log = nullptr;
	DECLARE rtl::Logger* g_mg_statistics_log = nullptr;
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	MediaSession::MediaSession(rtl::Logger* log, uint32_t id, const char* parent_id, mge::Termination* termination) :
		m_log(log)
	{
		m_id = id;

		m_name << parent_id << "-s" << id;
	
		m_local_control.stream_mode = h248::TerminationStreamMode::Inactive;
		m_local_control.reserve_group = false;
		m_local_control.reserve_value = false;

		m_local_control_old.stream_mode = h248::TerminationStreamMode::Inactive;
		m_local_control_old.reserve_group = false;
		m_local_control_old.reserve_value = false;

		m_termination = termination;

		m_fist_packet_recive = false;

		rtl::res_counter_t::add_ref(g_mge_stream_counter);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	MediaSession::~MediaSession()
	{
		ENTER();

		m_local_control.stream_mode = h248::TerminationStreamMode::Inactive;

		stop_rtcp_monitors();

		for (int i = 0; i < m_rtcp_monitor_list.getCount(); i ++)
		{
			rtcp_monitor_t* monitor = m_rtcp_monitor_list.getAt(i);
			if (monitor == nullptr)
			{
				continue;
			}
			monitor->print(g_mg_statistics_log);
			monitor->reset();
		}

		destroy_rx_streams();
		destroy_tx_streams();

		for (int i = 0; i < m_rtcp_monitor_list.getCount(); i++)
		{
			rtcp_monitor_t* monitor = m_rtcp_monitor_list.getAt(i);
			if (monitor == nullptr)
			{
				continue;
			}
			DELETEO(monitor);
			monitor = nullptr;
		}
		m_rtcp_monitor_list.clear();

		for (int i = 0; i < m_rtp_stat_list.getCount(); i++)
		{
			rtp_statistics_t* statistics = m_rtp_stat_list.getAt(i);
			if (statistics == nullptr)
			{
				continue;
			}

			statistics->print_statistics(g_mg_statistics_log);
			DELETEO(statistics);
			statistics = nullptr;
		}
		m_rtp_stat_list.clear();

		rtl::res_counter_t::release(g_mge_stream_counter);

		LEAVE();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool MediaSession::vad_isActive(const mg_rx_stream_t* rx_stream, mg_splitter_params_t* p)
	{
		if (rx_stream == nullptr)
		{
			return false;
		}

		mg_rx_common_stream_t* common = nullptr;

		switch (rx_stream->getType())
		{
		case mg_rx_stream_type_t::mg_rx_simple_audio_e:
			common = (mg_rx_common_stream_t*)rx_stream;
			p->event_handler = common->vad_getAnalyzer();
			return p->vad = common->vad_isActive();

		default:
			break;
		}

		return false;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	const uint32_t	MediaSession::getId() const
	{
		return m_id;
	}
	//------------------------------------------------------------------------------
	const char*	MediaSession::getName() const
	{
		return m_name;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void MediaSession::mg_stream__event(h248::EventParams* params)
	{
		if (m_termination != nullptr)
		{
			params->setStreamId(m_id);
			m_termination->TerminationEvent_callback(params);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool MediaSession::create_rx_stream(mg_rx_stream_type_t type)
	{
		uint32_t count = 0;

		const char* typeName = mg_rx_stream_type_toString(type);

		ENTER_FMT("%s", typeName);
		
		for (int i = 0; i < m_rx_streams.getCount(); i++)
		{
			if (m_rx_streams[i]->getType() == type)
			{
				count++;
			}
		}
	
		uint16_t id = count * 10 + m_id;
		mg_rx_stream_t* rx_stream = nullptr;

		switch (type)
		{
		case mg_rx_stream_type_t::mg_rx_ivr_e:
			rx_stream = NEW mg_rx_ivr_stream_t(m_log, id, m_termination->getName(), *this);
			break;
		case mg_rx_stream_type_t::mg_rx_webrtc_video_e:
			rx_stream = NEW mg_rx_webrtc_video_stream_t(m_log, id, m_termination->getName(), *this);
			break;
		case mg_rx_stream_type_t::mg_rx_simple_audio_e:
		case mg_rx_stream_type_t::mg_rx_simple_video_e:
			rx_stream = NEW mg_rx_common_stream_t(m_log, id, type, m_termination->getName(), *this);
			break;
		case mg_rx_stream_type_t::mg_rx_fax_t30_e:
			rx_stream = NEW mg_rx_fax_t30_stream_t(m_log, id, m_termination->getName(), *this);
			break;
		case mg_rx_stream_type_t::mg_rx_fax_t38_e:
			rx_stream = NEW mg_rx_fax_t38_stream_t(m_log, id, m_termination->getName(), *this);
			break;
		case mg_rx_stream_type_t::mg_rx_fax_proxy_e:
			rx_stream = NEW mg_rx_fax_proxy_stream_t(m_log, id, m_termination->getName(), *this);
			break;
		case mg_rx_stream_type_t::mg_rx_dummy_e:
			rx_stream = NEW mg_rx_dummy_stream_t(m_log, id, m_termination->getName(), *this);
			break;
		case mg_rx_stream_type_t::mg_rx_device_audio_e:
			rx_stream = NEW mg_rx_device_stream_t(m_log, id, m_termination->getName(), *this);
			break;
		default:
			ERROR_FMT("wrong type: %s(%d)", typeName, type);
			return false;
		}
	
		PRINT_FMT("rx stream created: %s", rx_stream->getName());
		
		m_rx_streams.add(rx_stream);

		rx_stream->lock();

		if ((type == mg_rx_stream_type_t::mg_rx_simple_audio_e) ||
			(type == mg_rx_stream_type_t::mg_rx_simple_video_e))
		{
			mg_rx_common_stream_t* common_stream = (mg_rx_common_stream_t*)rx_stream;
			common_stream->reset_stream_from_channel_event();
			common_stream->set_channel_event(nullptr);
		}
		else if (type == mg_rx_stream_type_t::mg_rx_webrtc_video_e)
		{
			mg_rx_webrtc_video_stream_t* webrtc_stream = (mg_rx_webrtc_video_stream_t*)rx_stream;
			webrtc_stream->reset_stream_from_channel_event();
			webrtc_stream->set_channel_event(nullptr);
		}

		update_rx_LocalControl_Mode(rx_stream, m_local_control.stream_mode);

		RETURN_BOOL(true);
	}
	//------------------------------------------------------------------------------
	void MediaSession::destroy_rx_streams()
	{
		ENTER();

		for (int i = 0; i < m_rx_streams.getCount(); i ++)
		{
			mg_rx_stream_t* rx_stream = m_rx_streams.getAt(i);
			if (rx_stream == nullptr)
			{
				continue;
			}

			rx_stream->lock();

			clean(rx_stream);

			unsubscribe_novoice_manager(rx_stream);
		
			rtl::String name(rx_stream->getName());
			DELETEO(rx_stream);
			rx_stream = nullptr;

			PRINT_FMT("rx_stream '%s' deleted", (const char*)name);
		}
		m_rx_streams.clear();

		LEAVE();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool MediaSession::create_tx_stream(mg_tx_stream_type_t type)
	{
		const char* typeName = mg_tx_stream_type_toString(type);

		ENTER_FMT("%s(%d)", typeName, type);

		uint32_t count = 0;
		for (int i = 0; i < m_tx_streams.getCount(); i++)
		{
			if (m_tx_streams[i]->getType() == type)
			{
				count++;
			}
		}

		uint16_t id = count * 10 + m_id;
		mg_tx_stream_t* tx_stream = nullptr;
		
		switch (type)
		{
		case mg_tx_stream_type_t::mg_tx_ivr_e:
			tx_stream = NEW mg_tx_ivr_stream_t(m_log, id, m_id, m_termination->getName());
			break;
		case mg_tx_stream_type_t::mg_tx_webrtc_video_e:
			tx_stream = NEW mg_tx_webrtc_video_stream_t(m_log, id, m_id, m_termination->getName());
			break;
		case mg_tx_stream_type_t::mg_tx_simple_audio_e:
		case mg_tx_stream_type_t::mg_tx_simple_video_e:
			tx_stream = NEW mg_tx_common_stream_t(m_log, id, type, m_id, m_termination->getName());
			break;
		case mg_tx_stream_type_t::mg_tx_fax_proxy_e:
			tx_stream = NEW mg_tx_fax_proxy_stream_t(m_log, id, m_id, m_termination->getName());
			break;
		case mg_tx_stream_type_t::mg_tx_fax_t30_e:
			tx_stream = NEW mg_tx_fax_t30_stream_t(m_log, id, m_id, m_termination->getName());
			break;
		case mg_tx_stream_type_t::mg_tx_fax_t38_e:
			tx_stream = NEW mg_tx_fax_t38_stream_t(m_log, id, m_id, m_termination->getName());
			break;
		case mg_tx_stream_type_t::mg_tx_dummy_e:
			tx_stream = NEW mg_tx_dummy_stream_t(m_log, id, m_id, m_termination->getName());
			break;
		case mg_tx_stream_type_t::mg_tx_device_audio_e:
			tx_stream = NEW mg_tx_device_stream_t(m_log, id, m_id, m_termination->getName());
			break;
		default:
			ERROR_FMT("wrong type: %s(%d)", typeName, type);
			return false;
		}

		PRINT_FMT("tx stream created: '%s'", tx_stream->getName());
		
		m_tx_streams.add(tx_stream);
	
		tx_stream->lock();
		update_tx_LocalControl_Mode(tx_stream, m_local_control.stream_mode);
		// потому что рано подписываться к таймеру пока не настроен удаленый стрим
		subscribe_novoice_manager();

		RETURN_BOOL(true);
	}
	//------------------------------------------------------------------------------
	void MediaSession::destroy_tx_streams()
	{
		ENTER();

		for (int i = 0; i < m_tx_streams.getCount(); i++)
		{
			mg_tx_stream_t* tx_stream = m_tx_streams[i];
			rtl::String name = tx_stream->getName();
			tx_stream->lock();
			DELETEO(tx_stream);
			tx_stream = nullptr;

			PRINT_FMT("m_tx_stream '%s' deleted.", (const char*)name);
		}
		m_tx_streams.clear();

		// если удаленный стрим удаляется то и подписка к таймеру ненужна уже
		unsubscribe_novoice_manager();

		LEAVE();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void MediaSession::clear()
	{
		for (int i = 0; i < m_rx_streams.getCount(); i ++)
		{
			clean(m_rx_streams[i]);
		}
	}
	//------------------------------------------------------------------------------
	void MediaSession::clean(mg_rx_stream_t* rx_stream)
	{
		if (rx_stream->getType() == mg_rx_stream_type_t::mg_rx_simple_audio_e)
		{
			mg_rx_common_stream_t* common_stream = (mg_rx_common_stream_t*)rx_stream;
			common_stream->reset_stream_from_channel_event();
		}
		else if (rx_stream->getType() == mg_rx_stream_type_t::mg_rx_simple_video_e)
		{
			mg_rx_common_stream_t* common_stream = (mg_rx_common_stream_t*)rx_stream;
			common_stream->reset_stream_from_channel_event();
		}
		else if (rx_stream->getType() == mg_rx_stream_type_t::mg_rx_webrtc_video_e)
		{
			mg_rx_webrtc_video_stream_t* webrtc_stream = (mg_rx_webrtc_video_stream_t*)rx_stream;
			webrtc_stream->reset_stream_from_channel_event();
		}
		else if (rx_stream->getType() == mg_rx_stream_type_t::mg_rx_fax_proxy_e)
		{
			mg_rx_fax_proxy_stream_t* fax_proxy_stream = (mg_rx_fax_proxy_stream_t*)rx_stream;
			fax_proxy_stream->reset_stream_from_channel_event();
		}

		PRINT("rx_stream (%s) cleaned");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool MediaSession::mg_stream_set_LocalControl_Mode(h248::TerminationStreamMode mode)
	{
		m_local_control_old.stream_mode = m_local_control.stream_mode;
		m_local_control.stream_mode = mode;

		ENTER_FMT("%d -> %d", m_local_control_old.stream_mode, m_local_control.stream_mode);

		for (int i = 0; i < m_rx_streams.getCount(); i++)
		{
			mg_rx_stream_t* rx_stream = m_rx_streams[i];

			if (!rx_stream->lock())
			{
				return false;
			}

			update_rx_LocalControl_Mode(rx_stream, mode);
		}

		for (int i = 0; i < m_tx_streams.getCount(); i++)
		{
			mg_tx_stream_t* tx_stream = m_tx_streams.getAt(i);

			if (!tx_stream->lock())
			{
				return false;
			}

			update_tx_LocalControl_Mode(tx_stream, mode);
		}
	
		return true;
	}
	//------------------------------------------------------------------------------
	bool MediaSession::mg_stream_set_LocalControl_ReserveValue(bool value)
	{
		ENTER();

		m_local_control_old.reserve_value = m_local_control.reserve_value;

		m_local_control.reserve_value = value;

		PRINT_FMT("%s -> %s",
			(m_local_control_old.reserve_value) ? "True" : "False",
			(m_local_control.reserve_value) ? "True" : "False"
		);

		for (int i = 0; i < m_rx_streams.getCount(); i++)
		{
			mg_rx_stream_t* rx_stream = m_rx_streams.getAt(i);
			if (rx_stream == nullptr)
			{
				continue;
			}
			rx_stream->mg_stream_set_LocalControl_ReserveValue(value);
		}
	
		for (int i = 0; i < m_tx_streams.getCount(); i++)
		{
			mg_tx_stream_t* tx_stream = m_tx_streams.getAt(i);
			if (tx_stream == nullptr)
			{
				continue;
			}
			tx_stream->mg_stream_set_LocalControl_ReserveValue(value);
		}

		RETURN_BOOL(true);
	}
	//------------------------------------------------------------------------------
	bool MediaSession::mg_stream_set_LocalControl_ReserveGroup(bool value)
	{
		ENTER();

		m_local_control_old.reserve_group = m_local_control.reserve_group;

		m_local_control.reserve_group = value;

		PRINT_FMT("%s -> %s", STR_BOOL(m_local_control_old.reserve_group), STR_BOOL(m_local_control.reserve_group));

		for (int i = 0; i < m_rx_streams.getCount(); i++)
		{
			mg_rx_stream_t* rx_stream = m_rx_streams.getAt(i);
			if (rx_stream == nullptr)
			{
				continue;
			}
			rx_stream->mg_stream_set_LocalControl_ReserveGroup(value);
		}

		for (int i = 0; i < m_tx_streams.getCount(); i++)
		{
			mg_tx_stream_t* tx_stream = m_tx_streams.getAt(i);
			if (tx_stream == nullptr)
			{
				continue;
			}
			tx_stream->mg_stream_set_LocalControl_ReserveGroup(value);
		}

		RETURN_BOOL(true);
	}
	//------------------------------------------------------------------------------
	bool MediaSession::mg_stream_set_LocalControl_Property(const h248::Field* LC_property)
	{
		ENTER();
	
		for (int i = 0; i < m_rx_streams.getCount(); i++)
		{
			mg_rx_stream_t* rx_stream = m_rx_streams.getAt(i);
			if (rx_stream == nullptr)
			{
				continue;
			}
			if (!rx_stream->lock())
			{
				return false;
			}
			rx_stream->mg_stream_set_LocalControl_Property(LC_property);
			rx_stream->unlock();
		}

		for (int i = 0; i < m_tx_streams.getCount(); i++)
		{
			mg_tx_stream_t* tx_stream = m_tx_streams.getAt(i);
			if (tx_stream == nullptr)
			{
				continue;
			}
			if (!tx_stream->lock())
			{
				return false;
			}
			tx_stream->mg_stream_set_LocalControl_Property(LC_property);
			tx_stream->unlock();
		}

		RETURN_BOOL(true);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool MediaSession::lock()
	{
		stop_rtcp_monitors();

		for (int i = 0; i < m_rx_streams.getCount(); i++)
		{
			mg_rx_stream_t* rx_stream = m_rx_streams.getAt(i);
			if (rx_stream == nullptr)
			{
				continue;
			}
			if (!rx_stream->lock())
			{
				WARNING("rx stream locking failed!");
				return false;
			}
		}

		for (int i = 0; i < m_tx_streams.getCount(); i++)
		{
			mg_tx_stream_t* tx_stream = m_tx_streams.getAt(i);
			if (tx_stream == nullptr)
			{
				continue;
			}
			if (!tx_stream->lock())
			{
				WARNING("tx stream locking failed.");
				return false;
			}
		}

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void MediaSession::unlock()
	{
		for (int i = 0; i < m_rx_streams.getCount(); i++)
		{
			mg_rx_stream_t* rx_stream = m_rx_streams.getAt(i);
			if (rx_stream == nullptr)
			{
				continue;
			}
			rx_stream->unlock();
		}

		for (int i = 0; i < m_tx_streams.getCount(); i++)
		{
			mg_tx_stream_t* tx_stream = m_tx_streams.getAt(i);
			if (tx_stream == nullptr)
			{
				continue;
			}
			tx_stream->unlock();
		}

		start_rtcp_monitors();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool MediaSession::isReady()
	{
		int rxc = m_rx_streams.getCount();
		int txc = m_tx_streams.getCount();

		PRINT_FMT("rx streams: %d, tx streams: %d", rxc, txc);

		if (rxc == 0 && txc == 0)
		{
			WARNING("No streams in session");
			return false;
		}

		if (m_tx_streams.getCount() == 0)
		{
			bool one_ivr = false;
			for (int i = 0; i < m_rx_streams.getCount(); i ++)
			{
				mg_rx_stream_t* rx_stream = m_rx_streams.getAt(i);
				if (rx_stream == nullptr)
				{
					continue;
				}
				if ((rx_stream->getType() == mg_rx_stream_type_t::mg_rx_ivr_e))
				{
					one_ivr = true;
				}
			}
			// если в списке нет ivr стримов и нет tx стримов то не готов.
			if (!one_ivr)
			{
				// по другому:
				// если нет стримов для отправки, то должен быть  хотя бы один ivr стрим для приема.
				// (ivr record)
				WARNING("no IVR tx stream in the session");
				return false;
			}
		}
		else if (m_rx_streams.getCount() == 0)
		{
			bool one_ivr = false;
			for (int i = 0; i < m_tx_streams.getCount(); i++)
			{
				mg_tx_stream_t* tx_stream = m_tx_streams.getAt(i);
				if (tx_stream == nullptr)
				{
					continue;
				}
				if ((tx_stream->getType() == mg_tx_stream_type_t::mg_tx_ivr_e))
				{
					one_ivr = true;
				}
			}
			if (!one_ivr)
			{
				// если нет стримов для приема, то должен быть хотя бы один ivr стрим для отправки.
				// (ivr player)
				WARNING("no IVR rx stream in the session");
				return false;
			}
		}

		// для всех простых стримов отправки должен быть указан адрес отправки
		for (int i = 0; i < m_tx_streams.getCount(); i++)
		{
			mg_tx_stream_t* tx_stream = m_tx_streams.getAt(i);
			if (tx_stream == nullptr)
			{
				continue;
			}

			if (tx_stream->getType() == mg_tx_stream_type_t::mg_tx_simple_audio_e)
			{
				mg_tx_common_stream_t* common_stream = (mg_tx_common_stream_t*)tx_stream;
				if (common_stream->get_remote_address()->get_ip_address()->empty())
				{
					WARNING_FMT("common stream '%s' audio without ip.", tx_stream->getName());
					return false;
				}
			}
			else if (tx_stream->getType() == mg_tx_stream_type_t::mg_tx_simple_video_e)
			{
				mg_tx_common_stream_t* common_stream = (mg_tx_common_stream_t*)tx_stream;
				if (common_stream->get_remote_address()->get_ip_address()->empty())
				{
					WARNING_FMT("common stream '%s' video without ip.", tx_stream->getName());
					return false;
				}
			}
			else if (tx_stream->getType() == mg_tx_stream_type_t::mg_tx_fax_proxy_e)
			{
				mg_tx_fax_proxy_stream_t* fax_proxy_stream = (mg_tx_fax_proxy_stream_t*)tx_stream;
				if (fax_proxy_stream->get_remote_address()->get_ip_address()->empty())
				{
					WARNING_FMT("fax stream '%s' without ip.", tx_stream->getName());
					return false;
				}
			}
		}

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void MediaSession::check_address_from(const socket_address* sockaddr)
	{
		if (!m_fist_packet_recive)
		{
			set_remote_address(sockaddr);
			m_fist_packet_recive = true;
		}
	}
	//------------------------------------------------------------------------------
	void MediaSession::set_remote_address(const socket_address* sockaddr)
	{
		for (int i = 0; i < m_tx_streams.getCount(); i++)
		{
			mg_tx_stream_t* tx_stream = m_tx_streams.getAt(i);
			if (tx_stream == nullptr)
			{
				continue;
			}

			if (tx_stream->getType() == mg_tx_stream_type_t::mg_tx_simple_audio_e)
			{
				mg_tx_common_stream_t* common_stream = (mg_tx_common_stream_t*)tx_stream;
				common_stream->set_remote_address(sockaddr);
			}
			else if (tx_stream->getType() == mg_tx_stream_type_t::mg_tx_simple_video_e)
			{
				mg_tx_common_stream_t* common_stream = (mg_tx_common_stream_t*)tx_stream;
				common_stream->set_remote_address(sockaddr);
			}
			else if (tx_stream->getType() == mg_tx_stream_type_t::mg_tx_webrtc_video_e)
			{
				mg_tx_webrtc_video_stream_t* webrtc_stream = (mg_tx_webrtc_video_stream_t*)tx_stream;
				webrtc_stream->set_remote_address(sockaddr);
				webrtc_stream->set_remote_rtcp_address(sockaddr);
			}
		}
	}
	//------------------------------------------------------------------------------
	void MediaSession::set_remote_rtcp_address(const socket_address* sockaddr)
	{
		for (int i = 0; i < m_tx_streams.getCount(); i++)
		{
			mg_tx_stream_t* tx_stream = m_tx_streams.getAt(i);
			if (tx_stream == nullptr)
			{
				continue;
			}

			if (tx_stream->getType() == mg_tx_stream_type_t::mg_tx_simple_audio_e)
			{
				mg_tx_common_stream_t* common_stream = (mg_tx_common_stream_t*)tx_stream;
				common_stream->set_remote_rtcp_address(sockaddr);
			}
			if (tx_stream->getType() == mg_tx_stream_type_t::mg_tx_simple_video_e)
			{
				mg_tx_common_stream_t* common_stream = (mg_tx_common_stream_t*)tx_stream;
				common_stream->set_remote_rtcp_address(sockaddr);
			}
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void MediaSession::update_rx_LocalControl_Mode(mg_rx_stream_t* rx_stream, h248::TerminationStreamMode mode)
	{
		if (mode == h248::TerminationStreamMode::SendOnly)
		{
			rx_stream->mg_stream_set_LocalControl_Mode(h248::TerminationStreamMode::RecvOnly);
		}
		else if (mode == h248::TerminationStreamMode::RecvOnly)
		{
			rx_stream->mg_stream_set_LocalControl_Mode(h248::TerminationStreamMode::Inactive);
		}
		else
		{
			rx_stream->mg_stream_set_LocalControl_Mode(mode);
		}
	}
	//------------------------------------------------------------------------------
	void MediaSession::update_tx_LocalControl_Mode(mg_tx_stream_t* tx_stream, h248::TerminationStreamMode mode)
	{
		if (mode == h248::TerminationStreamMode::SendOnly)
		{
			tx_stream->mg_stream_set_LocalControl_Mode(h248::TerminationStreamMode::Inactive);
		}
		else if (mode == h248::TerminationStreamMode::RecvOnly)
		{
			tx_stream->mg_stream_set_LocalControl_Mode(h248::TerminationStreamMode::SendOnly);
		}
		else
		{
			tx_stream->mg_stream_set_LocalControl_Mode(mode);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void MediaSession::subscribe_novoice_manager()
	{
		for (int i = 0; i < m_rx_streams.getCount(); i++)
		{
			mg_rx_stream_t* rx_stream = m_rx_streams.getAt(i);
			if (rx_stream == nullptr)
			{
				continue;
			}

			subscribe_novoice_manager(rx_stream);
		}
	}
	//------------------------------------------------------------------------------
	void MediaSession::subscribe_novoice_manager(mg_rx_stream_t* rx_stream)
	{
		if (g_mg_novoice_timer_manager == nullptr)
		{
			return;
		}

		if (rx_stream->getType() == mg_rx_stream_type_t::mg_rx_simple_audio_e)
		{
			mg_rx_common_stream_t* common_stream = (mg_rx_common_stream_t*)rx_stream;
			g_mg_novoice_timer_manager->add(common_stream);
		}
	}
	//------------------------------------------------------------------------------
	void MediaSession::unsubscribe_novoice_manager()
	{
		for (int i = 0; i < m_rx_streams.getCount(); i++)
		{
			mg_rx_stream_t* rx_stream = m_rx_streams.getAt(i);
			if (rx_stream == nullptr)
			{
				continue;
			}

			unsubscribe_novoice_manager(rx_stream);
		}
	}
	//------------------------------------------------------------------------------
	void MediaSession::unsubscribe_novoice_manager(mg_rx_stream_t* rx_stream)
	{
		if (g_mg_novoice_timer_manager == nullptr)
		{
			return;
		}

		if (rx_stream->getType() == mg_rx_stream_type_t::mg_rx_simple_audio_e)
		{
			mg_rx_common_stream_t* common_stream = (mg_rx_common_stream_t*)rx_stream;
			g_mg_novoice_timer_manager->remove(common_stream);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool MediaSession::start_rtcp_monitors()
	{
		for (int i = 0; i < m_rtcp_monitor_list.getCount(); i++)
		{
			rtcp_monitor_t* monitor = m_rtcp_monitor_list.getAt(i);
			if (monitor == nullptr)
			{
				continue;
			}
			monitor->start();
		}
		return true;
	}
	//------------------------------------------------------------------------------
	bool MediaSession::stop_rtcp_monitors()
	{
		for (int i = 0; i < m_rtcp_monitor_list.getCount(); i++)
		{
			rtcp_monitor_t* monitor = m_rtcp_monitor_list.getAt(i);
			if (monitor == nullptr)
			{
				continue;
			}
			monitor->stop();
		}
		return true;
	}
	//------------------------------------------------------------------------------
	bool MediaSession::create_rtcp_monitor(const char* addr, mg_rx_stream_t* rx_stream, mg_tx_stream_t* tx_stream, const char* cname_tx, bool webrtc)
	{
		uint32_t ssrc_rx = 0;
		if (rx_stream->getType() == mg_rx_stream_type_t::mg_rx_simple_audio_e)
		{
			mg_rx_common_stream_t* common_stream = (mg_rx_common_stream_t*)rx_stream;
			ssrc_rx = common_stream->get_ssrc_from_rtp();
		}
		else if (rx_stream->getType() == mg_rx_stream_type_t::mg_rx_simple_video_e)
		{
			mg_rx_common_stream_t* common_stream = (mg_rx_common_stream_t*)rx_stream;
			ssrc_rx = common_stream->get_ssrc_from_rtp();
		}
		else if (rx_stream->getType() == mg_rx_stream_type_t::mg_rx_webrtc_video_e)
		{
			mg_rx_webrtc_video_stream_t* webrtc_video_stream = (mg_rx_webrtc_video_stream_t*)rx_stream;
			ssrc_rx = webrtc_video_stream->get_ssrc_from_rtp();
		}

		uint32_t ssrc_tx = 0;
		if (tx_stream->getType() == mg_tx_stream_type_t::mg_tx_simple_audio_e)
		{
			mg_tx_common_stream_t* common_stream = (mg_tx_common_stream_t*)tx_stream;
			ssrc_tx = common_stream->get_ssrc_to_rtp();
		}
		else if (tx_stream->getType() == mg_tx_stream_type_t::mg_tx_simple_video_e)
		{
			mg_tx_common_stream_t* common_stream = (mg_tx_common_stream_t*)tx_stream;
			ssrc_tx = common_stream->get_ssrc_to_rtp();
		}
		else if (tx_stream->getType() == mg_tx_stream_type_t::mg_tx_webrtc_video_e)
		{
			mg_tx_webrtc_video_stream_t* webrtc_video_stream = (mg_tx_webrtc_video_stream_t*)tx_stream;
			ssrc_tx = webrtc_video_stream->get_ssrc_to_rtp();
		}

		rtcp_monitor_t* monitor = nullptr;
		for (int i = 0; i < m_rtcp_monitor_list.getCount(); i++)
		{
			monitor = m_rtcp_monitor_list.getAt(i);
			if (monitor == nullptr)
			{
				continue;
			}
			if (monitor->match(ssrc_tx, ssrc_rx, cname_tx))
			{
				break;
			}
			monitor = nullptr;
		}
		if (monitor == nullptr)
		{
			monitor = NEW rtcp_monitor_t(m_log, m_name, addr, webrtc);
			m_rtcp_monitor_list.add(monitor);
		}

		monitor->reset();
		monitor->setup(ssrc_tx, ssrc_rx, cname_tx);

		monitor->set_session_event_handler_rx(rx_stream);
		monitor->set_session_event_handler_tx(tx_stream);

		rx_stream->set_rtcp_monitor(monitor);
		tx_stream->set_rtcp_monitor(monitor);

		return true;
	}
	//------------------------------------------------------------------------------
	bool MediaSession::update_rtcp_monitor(uint32_t ssrc_rx_old, mg_rx_stream_t* rx_stream, mg_tx_stream_t* tx_stream)
	{
		rtcp_monitor_t* monitor = nullptr;
		for (int i = 0; i < m_rtcp_monitor_list.getCount(); i++)
		{
			monitor = m_rtcp_monitor_list.getAt(i);
			if (monitor == nullptr)
			{
				continue;
			}
			if (monitor->get_ssrc_opposite() == ssrc_rx_old)
			{
				break;
			}
			monitor = nullptr;
		}
		if (monitor == nullptr)
		{
			ERROR_FMT("monitor with ssrc:%10u not found ", ssrc_rx_old);
			return false;
		}

		uint32_t ssrc_rx = 0;
		if (rx_stream->getType() == mg_rx_stream_type_t::mg_rx_simple_audio_e)
		{
			mg_rx_common_stream_t* common_stream = (mg_rx_common_stream_t*)rx_stream;
			ssrc_rx = common_stream->get_ssrc_from_rtp();
		}
		else if (rx_stream->getType() == mg_rx_stream_type_t::mg_rx_simple_video_e)
		{
			mg_rx_common_stream_t* common_stream = (mg_rx_common_stream_t*)rx_stream;
			ssrc_rx = common_stream->get_ssrc_from_rtp();
		}
		else if (rx_stream->getType() == mg_rx_stream_type_t::mg_rx_webrtc_video_e)
		{
			mg_rx_webrtc_video_stream_t* webrtc_video_stream = (mg_rx_webrtc_video_stream_t*)rx_stream;
			ssrc_rx = webrtc_video_stream->get_ssrc_from_rtp();
		}

		uint32_t ssrc_tx = 0;
		if (tx_stream->getType() == mg_tx_stream_type_t::mg_tx_simple_audio_e)
		{
			mg_tx_common_stream_t* common_stream = (mg_tx_common_stream_t*)tx_stream;
			ssrc_tx = common_stream->get_ssrc_to_rtp();
		}
		else if (tx_stream->getType() == mg_tx_stream_type_t::mg_tx_simple_video_e)
		{
			mg_tx_common_stream_t* common_stream = (mg_tx_common_stream_t*)tx_stream;
			ssrc_tx = common_stream->get_ssrc_to_rtp();
		}
		else if (tx_stream->getType() == mg_tx_stream_type_t::mg_tx_webrtc_video_e)
		{
			mg_tx_webrtc_video_stream_t* webrtc_video_stream = (mg_tx_webrtc_video_stream_t*)tx_stream;
			ssrc_tx = webrtc_video_stream->get_ssrc_to_rtp();
		}

		rtl::String cname(monitor->get_cname());

		PRINT_FMT("old %10u -> %10u to new %10u -> %10u", monitor->get_ssrc(), ssrc_rx_old, ssrc_tx, ssrc_rx);

		monitor->reset();
		monitor->setup(ssrc_tx, ssrc_rx, cname);

		monitor->set_session_event_handler_rx(rx_stream);
		monitor->set_session_event_handler_tx(tx_stream);

		rx_stream->set_rtcp_monitor(monitor);
		tx_stream->set_rtcp_monitor(monitor);

		return true;
	}
	//------------------------------------------------------------------------------
	bool MediaSession::add_statistics_in_streams(const char* addr, mg_rx_stream_t* rx_stream, mg_tx_stream_t* tx_stream)
	{
		if (rx_stream == nullptr || tx_stream == nullptr)
		{
			return false;
		}
	
		rtl::String id_streams;
		id_streams << rx_stream->getId() << "_" << tx_stream->getId();

		for (int i = 0; i < m_rtp_stat_list.getCount(); i++)
		{
			rtp_statistics_t* statistics = m_rtp_stat_list.getAt(i);
			if (statistics == nullptr)
			{
				continue;
			}

			if (statistics->check_id((const char*)id_streams, id_streams.getLength()))
			{
				return true;
			}
		}

		rtp_statistics_t* new_statistics = NEW rtp_statistics_t((const char*)m_name, addr);
		new_statistics->set_id((const char*)id_streams, id_streams.getLength());

		stream_statistics_t* stat_rx = new_statistics->get_stream_statistics_rx();
		stream_statistics_t* stat_tx = new_statistics->get_stream_statistics_tx();

		rx_stream->add_statistics(stat_rx);
		tx_stream->add_statistics(stat_tx);

		m_rtp_stat_list.add(new_statistics);

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void MediaSession::raiseRecevierEvent(int eventId, int param)
	{
		for (int i = 0; i < m_tx_streams.getCount(); i++)
		{
			mg_tx_stream_t* tx_stream = m_tx_streams.getAt(i);
			if (tx_stream != nullptr)
			{
				tx_stream->onReceiverStarted();
			}
		}
	}
}
//------------------------------------------------------------------------------
