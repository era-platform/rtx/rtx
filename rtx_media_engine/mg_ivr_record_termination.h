﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "mg_termination.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	class IVRRecorderTermination : public mge::Termination
	{
	public:
		IVRRecorderTermination(rtl::Logger* log, const h248::TerminationID& termId, const char* parent_id);
		virtual ~IVRRecorderTermination() override;

		virtual bool mg_stream_start(uint32_t stream_id) override;
		virtual void mg_stream_stop(uint32_t stream_id) override;

		//------------------------------------------------------------------------------
		// методы контекста

		// конструктор и деструктор
		virtual bool mg_termination_initialize(h248::Field* reply) override;
		virtual bool mg_termination_destroy(h248::Field* reply) override;

		// настройки TerminationState
		virtual bool mg_termination_set_TerminationState_Property(const h248::Field* term_property, h248::Field* reply) override;

		// настройки Local и Remote
		virtual bool mg_termination_set_Local_Remote(uint16_t stream_id, const rtl::String& remote_sdp, const rtl::String& local_sdp, h248::Field* reply) override;
		virtual bool mg_termination_get_Local(uint16_t stream_id, rtl::String& local_sdp_answer, h248::Field* reply) override;

		//------------------------------------------------------------------------------
		const char* get_record_type();

	private:
		bool m_event_buffer_control;
		// RTX-97. raw or rtp.
		rtl::String m_record_type;
	};
}
//------------------------------------------------------------------------------
