/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_img_to_codec_tube.h"
#include "mg_media_element.h"
#include "rtp_packet.h"

#include "logdef_tube.h"

namespace mge
{

	//-----------------------------------------------
	//
	//-----------------------------------------------
	mg_img_to_codec_tube_t::mg_img_to_codec_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id) :
		Tube(HandlerType::VideoEncoder, id, log)
	{
		makeName(parent_id, "-imgc-tu");

		m_start_counter = 3;
		m_encoder = nullptr;
		m_payload_id = -1;
		m_last_sequence_number = 1;
		m_ssrc = rtl::DateTime::getTicks() + rand();

		rtl::res_counter_t::add_ref(g_mge_tube_counter);

		setLogTag("TUB-VCOD");
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	mg_img_to_codec_tube_t::~mg_img_to_codec_tube_t()
	{
		unsubscribe(this);

		if (m_encoder != nullptr)
		{
			rtx_codec__release_video_encoder(m_encoder);
			m_encoder = nullptr;
		}

		rtl::res_counter_t::release(g_mge_tube_counter);

#if defined TEST_VIDEO_CODEC
		m_out.close();
#endif
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_img_to_codec_tube_t::init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out)
	{
		if (payload_el_out == nullptr)
		{
			WARNING("payload element is null!");
		}

		m_payload_id = payload_el_out->get_payload_id();
		const char* in_enc = "null";
		int in_pid = 0;

		if (payload_el_in != nullptr)
		{
			in_enc = (const char*)payload_el_in->get_element_name();
			in_pid = payload_el_in->get_payload_id();
		}

		PRINT_FMT("in: %s %d   out: %s %d", in_enc, in_pid,
			(const char*)payload_el_out->get_element_name(), m_payload_id);

		media::PayloadFormat format;
		format.setEncoding(payload_el_out->get_element_name());
		format.setFrequency(payload_el_out->get_sample_rate());
		format.setChannelCount(payload_el_out->getChannelCount());
		format.setFmtp(payload_el_out->get_param_fmtp());
		format.setId((media::PayloadId)payload_el_out->get_payload_id());

		m_encoder = rtx_codec__create_video_encoder(format.getEncoding(), &format, rtx_video_encoder_cb_t, this);
		if (m_encoder == nullptr)
		{
			ERROR_MSG("failed to create video encoder.");
			return false;
		}

#if defined TEST_VIDEO_CODEC
		char path[260];
		int written = std_snprintf(path, 260, "%s%c%s_%u_out.raw", Log.get_folder(), FS_PATH_DELIMITER, (const char*)format.getEncoding(), m_id);
		m_out.create(path);
#endif

		RETURN_BOOL(true);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_img_to_codec_tube_t::is_suited(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out)
	{
		if (payload_el_out == nullptr)
		{
			return false;
		}
		return m_payload_id == payload_el_out->get_payload_id();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_img_to_codec_tube_t::send(const INextDataHandler* out_handler, const rtp_packet* packet)
	{
		// ....
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_img_to_codec_tube_t::send(const INextDataHandler* out_handler, const uint8_t* data, uint32_t len)
	{
		media::VideoImage* image = (media::VideoImage*)data;

		if (image == nullptr)
		{
			ERROR_MSG("nothing to send!");
			return;
		}

		if (m_encoder == nullptr)
		{
			ERROR_MSG("encoder is null!");
			DELETEO(image);
			return;
		}

		if (m_start_counter > 0)
		{
			m_start_counter--;
			m_encoder->reset();
		}

		m_encoder->encode(image);

		DELETEO(image);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_img_to_codec_tube_t::rtx_video_encoder_cb_t(const void* rtp_payload, int payload_length, uint32_t ts, bool marker, void* user)
	{
		mg_img_to_codec_tube_t* tube = (mg_img_to_codec_tube_t*)user;

		if (tube != nullptr)
		{
			tube->send_rtp(rtp_payload, payload_length, ts, marker);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_img_to_codec_tube_t::send_rtp(const void* rtp_payload, int payload_length, uint32_t ts, bool marker)
	{
		INextDataHandler* out = getNextHandler();

		if (out == nullptr)
		{
			ERROR_MSG("tube has no out handler!");
			return;
		}

		rtp_packet packet;

		packet.set_ssrc(m_ssrc);
		packet.set_payload_type((uint8_t)m_payload_id);
		packet.set_sequence_number(m_last_sequence_number++);
		packet.set_timestamp(ts);
		packet.set_marker(marker);
		packet.set_payload(rtp_payload, payload_length);

		out->send(this, &packet);

#if defined TEST_VIDEO_CODEC
		uint8_t buffer[2048];
		uint32_t sign = 0x80011008;
		uint32_t written = packet.write_packet(buffer, 2048);
		m_out.write(&sign, sizeof(uint32_t));
		m_out.write(&written, sizeof(uint32_t));
		m_out.write(buffer, written);
#endif
	}
}
//------------------------------------------------------------------------------
