﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_rx_stream.h"
#include "mg_session.h"

#include "logdef_stream.h"

namespace mge
{
	const char* mg_rx_stream_type_toString(mg_rx_stream_type_t type)
	{
		static const char* names[] = {
			"mg_rx_simple_audio_e",
			"mg_rx_device_audio_e",
			"mg_rx_dummy_e",
			"mg_rx_simple_video_e",
			"mg_rx_webrtc_video_e",
			"mg_rx_fax_t30_e",
			"mg_rx_fax_t38_e",
			"mg_rx_fax_proxy_e",
			"mg_rx_ivr_e"
		};

		return type < (sizeof(names) / sizeof(const char*)) ? names[type] : "unknown_type";
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_rx_stream_t::mg_rx_stream_t(rtl::Logger* log, uint32_t id, mg_rx_stream_type_t type, const char* term_id, MediaSession& session) :
		m_log(log), m_id(id), m_type(type), m_session(session), m_media_sdp(log, term_id, id)
	{
		m_main_lock = false;
		m_rx_lock.clear();

		m_local_control.stream_mode = h248::TerminationStreamMode::Inactive;
		m_local_control.reserve_group = false;
		m_local_control.reserve_value = false;
		m_rtcp_monitor = nullptr;
		m_stat = nullptr;

		strcpy(m_logTag, "RXS");

		rtl::res_counter_t::add_ref(g_mge_stream_counter);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_rx_stream_t::~mg_rx_stream_t()
	{
		m_local_control.stream_mode = h248::TerminationStreamMode::Inactive;

		m_stat = nullptr;

		if (m_rtcp_monitor != nullptr)
		{
			m_rtcp_monitor->set_session_event_handler_rx(nullptr);
			m_rtcp_monitor = nullptr;
		}

		for (int i = 0; i < m_tubes.getCount(); i++)
		{
			Tube* tube_at_list = m_tubes.getAt(i);
			if (tube_at_list == nullptr)
			{
				continue;
			}

			tube_at_list->unsubscribe(tube_at_list);
		}
		m_tubes.clear();

		rtl::res_counter_t::release(g_mge_stream_counter);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_rx_stream_t::mg_stream_set_LocalControl_Mode(h248::TerminationStreamMode mode)
	{
		ENTER_FMT("%s", TerminationStreamMode_toString(mode));

		if (!m_main_lock)
		{
			WARNING("The stream was not locked!");
			return false;
		}

		m_local_control.stream_mode = mode;
		return true;
	}
	//------------------------------------------------------------------------------
	bool mg_rx_stream_t::mg_stream_set_LocalControl_ReserveValue(bool value)
	{
		ENTER_FMT("%s", STR_BOOL(value));

		if (!m_main_lock)
		{
			WARNING("The stream was not locked!");
			return false;
		}

		m_local_control.reserve_value = value;
		return true;
	}
	//------------------------------------------------------------------------------
	bool mg_rx_stream_t::mg_stream_set_LocalControl_ReserveGroup(bool value)
	{
		ENTER_FMT("%s", STR_BOOL(value));

		if (!m_main_lock)
		{
			WARNING("The stream was not locked!");
			return false;
		}

		m_local_control.reserve_group = value;

		return true;
	}
	//------------------------------------------------------------------------------
	bool mg_rx_stream_t::mg_stream_set_LocalControl_Property(const h248::Field* LC_property)
	{
		ENTER_FMT("%s: %s", LC_property ? (const char*)LC_property->getName() : "(null)",
			LC_property ? (const char*)LC_property->getValue() : "(null)");

		if (!m_main_lock)
		{
			WARNING("The stream was not locked!");
			return false;
		}

		if (LC_property == nullptr)
		{
			WARNING("Invalid parameter LC_property!");
			return false;
		}

		ERROR_MSG("function not implemented!");
		return false;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_rx_stream_t::mg_termination__dtmf_event(char dtmf, int duration)
	{
		ENTER_FMT("dtmf:'%c',duration:%d", dtmf, duration);

		m_session.mg_stream__event(NEW h248::DTMF_EventParams(1234, dtmf, duration));
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_rx_stream_t::mg_termination__vad_event(bool vad, int volume)
	{
		ENTER_FMT("vad:%s, volume:%d", STR_BOOL(vad), volume);

		m_session.mg_stream__event(NEW h248::VAD_EventParams(1239, vad, volume));
	}
	//------------------------------------------------------------------------------
	void mg_rx_stream_t::mg_termination__no_voice_event(int duration)
	{
		ENTER_FMT("%d", duration);

		m_session.mg_stream__event(NEW h248::NoVoiceEventParams(duration));
	}
	//------------------------------------------------------------------------------
	void mg_rx_stream_t::mg_termination__voice_restored_event()
	{
		ENTER();

		m_session.mg_stream__event(NEW h248::VoiceRestoredEventParams());
	}
	//------------------------------------------------------------------------------
	int mg_rx_stream_t::rtcp_session_rx(const rtcp_packet_t* packet)
	{
		return 0;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_rx_stream_t::setMediaParams(mg_sdp_media_base_t& media)
	{
		m_media_sdp.clear();

		rtl::String sdp;
		media.write(sdp);
		m_media_sdp.read(sdp);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_rx_stream_t::lock()
	{
		if (isLocked())
			return true;

		uint32_t time_diff = 0;
		while (time_diff <= 3000)
		{
			if (checkAndLock())
			{
				rtl::Thread::sleep(5);
				time_diff += 5;
				continue;
			}
			m_main_lock = true;
			return true;
		}

		ERROR_MSG("stream not locked.");

		return false;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_rx_stream_t::unlock()
	{
		if (isLocked())
		{
			releaseLock();
			m_main_lock = false;
		}
	}
	//------------------------------------------------------------------------------
	// Adding tubes to a locked receiver.
	//------------------------------------------------------------------------------
	void mg_rx_stream_t::subscribe_tube(Tube* tube)
	{
		ENTER_FMT("%s", tube->getName());

		if (!m_main_lock)
		{
			WARNING("the stream was not locked!");
			return;
		}

		tube->set_free_in(false);

		m_tubes.add(tube);
	}
	//------------------------------------------------------------------------------
	// Removing tubes from a locked receiver.
	//------------------------------------------------------------------------------
	bool mg_rx_stream_t::unsubscribe_tube(uint32_t id_tube)
	{
		//ENTER_FMT("%d", id_tube);
		
		if (!m_main_lock)
		{
			WARNING("the stream was not locked!");
			return false;
		}

		bool found = false;
		for (int i = 0; i < m_tubes.getCount(); i++)
		{
			Tube* tube_at_list = m_tubes.getAt(i);
			if (tube_at_list == nullptr)
			{
				continue;
			}

			if (tube_at_list->getId() == id_tube)
			{
				tube_at_list->set_free_in(true);
				m_tubes.removeAt(i);
				found = true;
				break;
			}
		}

		return found;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_rx_stream_t::rtcp_analize(const rtcp_packet_t* packet)
	{
		if ((m_rtcp_monitor == nullptr) || (packet == nullptr))
		{
			return;
		}

		m_rtcp_monitor->rtcp_event_rx_ctrl(packet);
		return;
	}
	//------------------------------------------------------------------------------
	void mg_rx_stream_t::rtcp_analize(const rtp_packet* packet)
	{
		if ((m_rtcp_monitor == nullptr) || (packet == nullptr))
		{
			return;
		}

		m_rtcp_monitor->rtcp_event_rx_data(packet);
		return;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_rx_stream_t::stat_analize(const rtp_packet* packet)
	{
		if ((m_stat == nullptr) || (packet == nullptr))
		{
			return;
		}

		m_stat->packets++;
		m_stat->bytes += packet->get_full_length();
	}
	//------------------------------------------------------------------------------
	void mg_rx_stream_t::stat_out_of_order()
	{
		if (m_stat == nullptr)
		{
			return;
		}

		m_stat->out_of_order++;
	}
	//------------------------------------------------------------------------------
	void mg_rx_stream_t::stat_lost()
	{
		if (m_stat == nullptr)
		{
			return;
		}

		m_stat->lost++;
	}
	//------------------------------------------------------------------------------
	void mg_rx_stream_t::stat_invalid()
	{
		if (m_stat == nullptr)
		{
			return;
		}

		m_stat->invalid++;
	}
	//------------------------------------------------------------------------------
	void mg_rx_stream_t::stat_filtered()
	{
		if (m_stat == nullptr)
		{
			return;
		}

		m_stat->filtered++;
	}
}
//------------------------------------------------------------------------------
