﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_interface.h"
#include "mg_session.h"
#include "mg_media_element.h"

//-----------------------------------------------
//
//-----------------------------------------------
#define IS_ADDITIONAL_STREAM(x) (x >= 10)
#define STREAM_INDEX_BY_ID(x) (IS_ADDITIONAL_STREAM(x) ? (int(x/10)) : 0)

namespace mge
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	class Termination
	{
	public:
		Termination(rtl::Logger* log, const h248::TerminationID& termId, const char* parent_id);
		virtual ~Termination();

		uint32_t getId() const { return m_termId.getId(); }
		h248::TerminationType getType() const { return m_termId.getType(); }
		const h248::TerminationID& getTermId() { return m_termId; }

		const char* getName() const { return m_name; }

		uint32_t getVideoTermId() const { return m_vId; }
		const rtl::String& getVideoTitle() const { return m_vTitle; }

		MediaSession* findMediaSession(uint32_t streamId);

		virtual bool isReady(uint32_t streamId);
		virtual bool mg_stream_start(uint32_t streamId);
		virtual void mg_stream_stop(uint32_t streamId);

		bool mg_termination_lock();
		bool mg_termination_unlock();

		// Путь до файла записи.
		void set_rtp_record_file_path(const rtl::String& filepath) { m_rtp_record_file_path = filepath; }
		// калбеки для передачи сигналов наружу
		void mg_termination_set_event_handler(h248::ITerminationEventHandler* handler, uint32_t ctxId);
		void TerminationEvent_callback(h248::EventParams* params);

		static void enableVideoRecording(bool flag);
		static bool isVideoRecordingEnabled();

		// конструктор и деструктор
		virtual bool mg_termination_initialize(h248::Field* reply) = 0;
		virtual bool mg_termination_destroy(h248::Field* reply) = 0;

		// настройки TerminationState
		virtual bool mg_termination_set_TerminationState(h248::TerminationServiceState state, h248::Field* reply);
		virtual bool mg_termination_set_TerminationState_EventControl(bool eventBufferControl, h248::Field* reply);
		virtual bool mg_termination_set_TerminationState_Property(const h248::Field* property, h248::Field* reply);
		// настройки LocalControl Local Remote
		virtual bool mg_termination_set_LocalControl_Mode(uint16_t streamId, h248::TerminationStreamMode mode, h248::Field* reply);
		virtual bool mg_termination_set_LocalControl_ReserveValue(uint16_t streamId, bool value, h248::Field* reply);
		virtual bool mg_termination_set_LocalControl_ReserveGroup(uint16_t streamId, bool value, h248::Field* reply);
		virtual bool mg_termination_set_LocalControl_Property(uint16_t streamId, const h248::Field* LC_property, h248::Field* reply);
		// настройки Local и Remote
		virtual bool mg_termination_set_Local(uint16_t streamId, const rtl::String& local, rtl::String& answer, h248::Field* reply);
		virtual bool mg_termination_set_Remote(uint16_t streamId, const rtl::String& remote, h248::Field* reply);
		virtual bool mg_termination_set_Local_Remote(uint16_t streamId, const rtl::String& remote, const rtl::String& local, h248::Field* reply) = 0;
		virtual bool mg_termination_get_Local(uint16_t streamId, rtl::String& local, h248::Field* reply) = 0;
		// настройки событий
		virtual bool mg_termination_set_events(uint16_t streamId, const h248::Field* propertySet, h248::Field* reply);
		virtual bool mg_termination_set_signals(uint16_t streamId, const h248::Field* propertySet, h248::Field* reply);
		virtual bool mg_termination_set_property(const h248::Field* property, h248::Field* reply);

		void mg_termination_add_topology(const h248::TopologyTriple& topology);
		rtl::ArrayT<h248::TopologyTriple>& mg_termination_get_topology_list();
		//-----------------------------------------------

	protected:
		MediaSession* makeMediaSession(uint32_t streamId);
		void releaseMediaSession(const uint32_t streamId);
		void destroyMediaSession();

	protected:
		rtl::String m_rtp_record_file_path;

		// Флажок указывает что данный терминатор работает в режиме конференции.
		bool m_conference;
		uint32_t m_vId;
		rtl::String m_vTitle;
		char m_tag[9];

		h248::TerminationServiceState m_service_state;
		bool m_event_buffer_control;

	protected:
		rtl::Logger* m_log;

	private:
		rtl::String m_name;
		h248::TerminationID m_termId;

		bool m_lock;

		// Список топологий текущего терминэйшена.
		rtl::ArrayT<h248::TopologyTriple> m_topology_triple_list;

		// Хандлер на отправку сообщний наружу.
		uint32_t m_eventCtxId;
		h248::ITerminationEventHandler*	m_megaco_handler;

		// Список стримов для связки с тубами.
		MediaSessionList m_stream_list;

		// settings from config file
		static bool g_enableVideoRecording;
	};
	//-----------------------------------------------
	//
	//-----------------------------------------------
	typedef rtl::ArrayT<Termination*> TerminationList;
}
//-----------------------------------------------
