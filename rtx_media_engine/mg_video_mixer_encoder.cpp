/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_video_mixer_encoder.h"

namespace mge
{
#define LOG_PREFIX "vmix-enc"
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VideoMixerEncoder::VideoMixerEncoder(rtl::Logger* log, const media::PayloadFormat& vformat) : m_log(log)
	{
		m_lastSeqNumber = (uint16_t)(rand() ^ 0xFFFFCD5D);
		m_ssrc = (rand() ^ 0xE5D5D5D7);
		m_format = vformat;
		m_encoder = rtx_codec__create_video_encoder(m_format.getEncoding(), &m_format, videoEncoderCallback, this);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VideoMixerEncoder::~VideoMixerEncoder()
	{
		destroy();
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoMixerEncoder::destroy()
	{
		rtl::MutexLock lock(m_sync);

		if (m_encoder != nullptr)
		{
			rtx_codec__release_video_encoder(m_encoder);
			m_encoder = nullptr;
		}
		
		m_list.clear();
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoMixerEncoder::addMember(mg_video_conference_out_tube_t* handler)
	{
		if (handler == nullptr || m_encoder == nullptr)
			return;

		rtl::MutexLock lock(m_sync);
		for (int i = 0; i < m_list.getCount(); i++)
		{
			if (m_list[i] == handler)
			{
				PLOG_TUBE_WARNING("mix-enc", "addMember -- %s...already in list!", handler->getName());
				return;
			}
		}

		m_list.add(handler);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoMixerEncoder::removeMember(mg_video_conference_out_tube_t* handler)
	{
		if (handler == nullptr || m_encoder == nullptr)
			return;

		rtl::MutexLock lock(m_sync);
		m_list.remove(handler);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoMixerEncoder::updateImage(media::VideoImage* image)
	{
		if (m_encoder == nullptr)
			return;

		rtl::MutexLock lock(m_sync);
		m_encoder->encode(image);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoMixerEncoder::videoEncoderCallback(const void* rtpPayload, int payloadLength, uint32_t ts, bool marker, void* user)
	{
		VideoMixerEncoder* mixer = (VideoMixerEncoder*)user;

		if (mixer != nullptr)
		{
			mixer->sendPacket(rtpPayload, payloadLength, ts, marker);
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoMixerEncoder::sendPacket(const void* rtpPayload, int payloadLength, uint32_t ts, bool marker)
	{
		rtl::MutexLock lock(m_sync);

		rtp_packet packet;

		packet.set_ssrc(m_ssrc);
		packet.set_payload_type(m_format.getId_u8());
		packet.set_sequence_number(m_lastSeqNumber++);
		packet.set_timestamp(ts);
		packet.set_marker(marker);
		packet.set_payload(rtpPayload, payloadLength);

		int i = 0;

		for (; i < m_list.getCount(); i++)
		{
			mg_video_conference_out_tube_t* out = m_list[i];
			out->send(out, &packet);
		}

#if defined TEST_VIDEO_CODEC
		uint8_t buffer[2048];
		uint32_t sign = 0x80011008;
		uint32_t written = packet.write_packet(buffer, 2048);
		m_out.write(&sign, sizeof(uint32_t));
		m_out.write(&written, sizeof(uint32_t));
		m_out.write(buffer, written);
#endif
	}
}
//-----------------------------------------------
