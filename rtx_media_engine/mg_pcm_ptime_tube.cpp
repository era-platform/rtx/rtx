﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_pcm_ptime_tube.h"
#include "mg_media_element.h"
#include "rtp_packet.h"

#include "logdef_tube.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_pcm_ptime_tube_t::mg_pcm_ptime_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id) :
		Tube(HandlerType::AudioTimer, id, log)
	{
		makeName(parent_id, "-ptime-tu"); // PCM ptime

		m_pcm_buffer = nullptr;
		m_pcm_buffer_write = 0;
		m_seqno = 0;
		m_frame_size = (20 * 8000 / 1000) * sizeof(uint16_t);

		rtl::res_counter_t::add_ref(g_mge_tube_counter);

		setLogTag("TUB-TIME");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_pcm_ptime_tube_t::~mg_pcm_ptime_tube_t()
	{
		unsubscribe(this);

		if (m_pcm_buffer != nullptr)
		{
			DELETEAR(m_pcm_buffer);
			m_pcm_buffer = nullptr;
		}

		rtl::res_counter_t::release(g_mge_tube_counter);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_pcm_ptime_tube_t::init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out)
	{
		if (payload_el_out == nullptr)
		{
			ERROR_MSG("payload element is null!");
			return false;
		}
		m_payload_id = payload_el_out->get_payload_id();
		m_frame_size = uint16_t((payload_el_out->get_ptime() *  payload_el_out->get_sample_rate() / 1000) * sizeof(uint16_t));
		m_seqno = 0;
		if (m_pcm_buffer == nullptr)
		{
			m_pcm_buffer = NEW uint8_t[rtp_packet::PAYLOAD_BUFFER_SIZE]{ 0 };
			m_pcm_buffer_write = 0;
		}

		RETURN_BOOL(true);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_pcm_ptime_tube_t::is_suited(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out)
	{
		if (payload_el_out == nullptr)
		{
			return false;
		}
		return m_payload_id == payload_el_out->get_payload_id();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_pcm_ptime_tube_t::send(const INextDataHandler* out_handler, const rtp_packet* packet)
	{
		if (packet == nullptr)
		{
			ERROR_MSG("nothing to send");
			return;
		}

		if (m_seqno == 0)
			m_seqno = packet->get_sequence_number();

		uint32_t payload_length = packet->get_payload_length();
		const uint8_t* payload = packet->get_payload();
		if (payload_length == 0)
		{
			ERROR_MSG("payload length 0 ?");
			return;
		}

		uint32_t residue_length = send_parts_packet(packet);
		accumulation_data(payload + (payload_length - residue_length), residue_length);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_pcm_ptime_tube_t::accumulation_data(const uint8_t* payload, uint32_t payload_length)
	{
		if ((payload_length + m_pcm_buffer_write) >= rtp_packet::PAYLOAD_BUFFER_SIZE)
		{
			WARNING("pcm buffer max.");
			payload_length = rtp_packet::PAYLOAD_BUFFER_SIZE - m_pcm_buffer_write;
		}

		memcpy(m_pcm_buffer + m_pcm_buffer_write, payload, payload_length);
		m_pcm_buffer_write += payload_length;
	}
	//------------------------------------------------------------------------------
	// Возвращает размер неотправленных данных.
	//------------------------------------------------------------------------------
	uint32_t mg_pcm_ptime_tube_t::send_parts_packet(const rtp_packet* packet)
	{
		uint8_t count = send_from_buffer(packet);

		uint32_t residue = packet->get_payload_length();
		const uint8_t* payload = packet->get_payload();

		rtp_packet_priv_type type = analyze_type(residue);

		if ((residue + m_pcm_buffer_write) >= m_frame_size)
		{
			uint8_t buff[rtp_packet::PAYLOAD_BUFFER_SIZE] = { 0 };
			uint32_t write = 0;
			if (m_pcm_buffer_write > 0)
			{
				memcpy(buff, m_pcm_buffer, m_pcm_buffer_write);
				write = m_pcm_buffer_write;
				m_pcm_buffer_write = 0;
			}

			uint32_t send = 0;
			uint32_t diff = m_frame_size - write;
			if (diff > 0)
			{
				memcpy(buff + write, payload, diff);
				residue -= diff;
				send = diff;

				rtp_packet new_packet;
				copy_packet(&new_packet, packet, type, count);
				new_packet.set_payload(buff, m_frame_size);
				send_packet(&new_packet);

				count++;
			}

			while (residue >= m_frame_size)
			{
				rtp_packet new_packet;
				copy_packet(&new_packet, packet, type, count);

				uint32_t size = new_packet.set_payload(payload + send, m_frame_size);
				send += size;
				residue -= size;

				send_packet(&new_packet);

				count++;
			}
		}

		return residue;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	uint8_t mg_pcm_ptime_tube_t::send_from_buffer(const rtp_packet* packet)
	{
		uint8_t count = 0;
		uint32_t send = 0;
		while (m_pcm_buffer_write >= m_frame_size)
		{
			rtp_packet new_packet;
			copy_packet(&new_packet, packet, analyze_type(packet->get_payload_length()), count);

			uint32_t size = new_packet.set_payload(m_pcm_buffer + send, m_frame_size);
			send += size;
			m_pcm_buffer_write -= size;

			send_packet(&new_packet);
			count++;
		}

		if ((count > 0) && (m_pcm_buffer_write > 0))
		{
			memcpy(m_pcm_buffer, m_pcm_buffer + send, m_pcm_buffer_write);
		}

		return count;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_pcm_ptime_tube_t::send_packet(rtp_packet* packet)
	{
		packet->set_sequence_number(m_seqno++);
		INextDataHandler* out = getNextHandler();
		if (out != nullptr)
		{
			out->send(this, packet);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_pcm_ptime_tube_t::copy_packet(rtp_packet* new_packet, const rtp_packet* packet, rtp_packet_priv_type type, uint8_t count)
	{
		new_packet->copy_head_from(packet);
		new_packet->set_samples((m_frame_size / sizeof(uint16_t)));
		new_packet->set_priv_type(type);
		new_packet->set_priv_partial_count(count);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	rtp_packet_priv_type mg_pcm_ptime_tube_t::analyze_type(uint32_t payload_length)
	{
		rtp_packet_priv_type type = rtp_packet_priv_type::simple;
		if (payload_length > m_frame_size)
		{
			type = rtp_packet_priv_type::partial;
		}
		else if (payload_length < m_frame_size)
		{
			type = rtp_packet_priv_type::generate;
		}

		return type;
	}
}
//------------------------------------------------------------------------------
