/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_video_conference_ctrl.h"
#include "mg_video_conference_in_tube.h"
#include "mg_video_conference_out_tube.h"
//-----------------------------------------------
//
//-----------------------------------------------
#define LOG_PREFIX "VCONF"

namespace mge
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VideoConferenceController::VideoConferenceController(rtl::Logger* log, const char* parentId) :
		m_log(log),
		m_mixer(nullptr),
		m_selector(nullptr),
		m_recorder(nullptr)
	{
		m_name << parentId << "-vconf-mixer";

		rtl::res_counter_t::add_ref(g_mge_tube_counter);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VideoConferenceController::~VideoConferenceController()
	{
		destroy();

		rtl::res_counter_t::release(g_mge_tube_counter);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool VideoConferenceController::create(const char* schema, VideoSelector* selector)
	{
		m_mixer = NEW VideoMixer(m_log, m_name);

		m_selector = selector;

		if (m_selector != nullptr)
		{
			m_selector->setEventHandler(m_mixer);
		}

		m_mixer->start(schema);

		return true;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoConferenceController::destroy()
	{
		if (m_mixer != nullptr)
		{
			m_mixer->stop();
			DELETEO(m_mixer);
			m_mixer = nullptr;
		}

		if (m_recorder != nullptr)
		{
			m_recorder->unsubscribe(m_recorder);
			m_recorder = nullptr;
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoConferenceController::updateSchema(const char* jsonSchema)
	{
		if (m_mixer != nullptr)
			m_mixer->restart(jsonSchema);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool VideoConferenceController::setRecorderHandler(INextDataHandler* out_handler)
	{
		m_recorder = out_handler;
		return true;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool VideoConferenceController::addConfMember(Termination* term, Tube* inTube, Tube* outTube)
	{
		return m_mixer != nullptr ? m_mixer->addMember(term, inTube, outTube) : false;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoConferenceController::removeConfMember(uint32_t termId)
	{
		if (m_mixer != nullptr)
			m_mixer->removeMember(termId);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoConferenceController::attachConfMember(uint32_t termId)
	{
		if (m_mixer != nullptr)
			m_mixer->attachMember(termId);
	}
}
//-----------------------------------------------
