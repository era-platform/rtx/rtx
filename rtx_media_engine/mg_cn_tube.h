﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_tube.h"
#include "mmt_timer.h"

namespace mge
{
	//------------------------------------------------------------------------------
	// Труба комфортного шума. Генерирует CN.
	// На входе стрим. На выходе опозитный туб.
	//------------------------------------------------------------------------------
	class /*ME_DEBUG_API*/ mg_cn_tube_t : public media::Metronome, public Tube
	{
		mg_cn_tube_t(const mg_cn_tube_t&);
		mg_cn_tube_t& operator=(const mg_cn_tube_t&);

	public:
		mg_cn_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id);
		virtual	~mg_cn_tube_t();

		virtual bool init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out) override;
		virtual void send(const INextDataHandler* out_handler, const rtp_packet* packet) override;

		void set_timestamp_step(uint32_t timestamp_step);
		void set_sample_rate(uint32_t sample_rate_in);

	private:
		virtual	void metronomeTicks() override;

	private:
		uint32_t					m_timestamp_step;
		bool						m_recalc;
		rtp_packet					m_last_packet_cn;
		uint32_t					m_sample_rate_in;
		uint32_t					m_position;
	};
}
//------------------------------------------------------------------------------

