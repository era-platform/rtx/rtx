/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_transformer_tube.h"
#include "mg_media_element.h"
#include "rtp_packet.h"

#include "logdef_tube.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_transformer_tube_t::mg_transformer_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id) :
		Tube(HandlerType::ImageTransformer, id, log)
	{
		makeName(parent_id, "-res-tu"); // transformer Tube

		m_payload_id = -1;

		rtl::res_counter_t::add_ref(g_mge_tube_counter);

		setLogTag("TUB-TRAN");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_transformer_tube_t::~mg_transformer_tube_t()
	{
		unsubscribe(this);

		rtl::res_counter_t::release(g_mge_tube_counter);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_transformer_tube_t::init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out)
	{
		if (payload_el_in == nullptr)
		{
			ERROR_MSG("payload element in is null!");
			return false;
		}

		if (payload_el_out == nullptr)
		{
			ERROR_MSG("payload element out is null.");
			return false;
		}

		PRINT_FMT("in:%s, out:%s",
			(const char*)payload_el_in->get_element_name(),
			(const char*)payload_el_out->get_element_name());

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void mg_transformer_tube_t::send(const INextDataHandler* out_handler, const rtp_packet* packet)
	{
		return;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_transformer_tube_t::send(const INextDataHandler* out_handler, const uint8_t* data, uint32_t length)
	{
		ENTER();

		if (out_handler == nullptr || data == nullptr)
		{
			ERROR_MSG("nothing to send!");
			return;
		}

		media::VideoImage* image = (media::VideoImage*)data;

		// transform it...

		INextDataHandler* out = getNextHandler();
		if (out != nullptr)
		{
			// !! George !! ���������� �����
			out->send(this, (const uint8_t*)image, 0);
		}
	}
}
//------------------------------------------------------------------------------
