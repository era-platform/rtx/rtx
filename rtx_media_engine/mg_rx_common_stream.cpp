﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_thread.h"
#include "mg_rx_common_stream.h"
#include "mg_tube_manager.h"
#include "mg_cn_tube.h"
#include "mg_session.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "logdef_stream.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_rx_common_stream_t::mg_rx_common_stream_t(rtl::Logger* log, uint32_t id, mg_rx_stream_type_t type, const char* parent_id, MediaSession& session) :
		mg_rx_stream_t(log, id, type, parent_id, session)
	{
		m_rtp = nullptr;
		m_ssrc_from_rtp = 0;
		m_new_ssrc_from_rtp_counter = 0;
		m_seq_num_from_rtp = 0;
		m_seq_num_from_rtp_last = 0;
		m_timestamp_from_rtp = 0;
		m_timestamp_from_rtp_last = 0;
		m_timestamp_step = 20;

		rtl::String name;
		name << session.getName() << "-rxcs" << id;
		setName(name);
		setTag("RXS_RTP");

		m_recorder = nullptr;

		m_cn_packet = false;
		m_no_voice = false;

		m_vad = false;
		m_audio_analyzer = nullptr;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_rx_common_stream_t::~mg_rx_common_stream_t()
	{
		m_rtp = nullptr;

		m_ssrc_from_rtp = 0;
		m_new_ssrc_from_rtp_counter = 0;

		m_seq_num_from_rtp = 0;
		m_seq_num_from_rtp_last = 0;
		m_timestamp_from_rtp = 0;
		m_timestamp_from_rtp_last = 0;

		if (m_recorder != nullptr)
		{
			m_recorder->stop();
			m_recorder->close();
			DELETEO(m_recorder);
			m_recorder = nullptr;
		}

		if (m_audio_analyzer != nullptr)
		{
			DELETEO(m_audio_analyzer);
			m_audio_analyzer = nullptr;
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_rx_common_stream_t::set_channel_event(i_tx_channel_event_handler* rtp)
	{
		if (isLocked())
		{
			m_rtp = rtp;
		}
		else
		{
			WARNING("stream is in unlocked state! Handler not changed!");
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_rx_common_stream_t::reset_stream_from_channel_event()
	{
		if (!isLocked())
		{
			WARNING("stream is in unlocked state!");
			return;
		}

		if (m_rtp != nullptr)
		{
			m_rtp->dec_tx_subscriber(m_ssrc_from_rtp);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_rx_common_stream_t::rx_packet_stream(const rtp_channel_t* channel, const rtp_packet* packet, const socket_address* from)
	{
		if (checkAndLock())
		{
			stat_filtered();
			WARNING("stream is in locked state!");
			return false;
		}

		bool result = rx_packet_stream_lock(channel, packet, from);

		releaseLock();

		return result;
	}
	//------------------------------------------------------------------------------
	bool mg_rx_common_stream_t::rx_data_stream(const rtp_channel_t* channel, const uint8_t* data, uint32_t len, const socket_address* from)
	{
		if (checkAndLock())
		{
			WARNING("stream is in locked state!");
			return false;
		}

		bool result = rx_data_stream_lock(channel, data, len, from);

		releaseLock();
		
		return result;
	}
	//------------------------------------------------------------------------------
	bool mg_rx_common_stream_t::rx_ctrl_stream(const rtp_channel_t* channel, const rtcp_packet_t* packet, const socket_address* from)
	{
		if (checkAndLock())
		{
			WARNING("stream is in locked state!");
			return false;
		}

		bool result = rx_ctrl_stream_lock(channel, packet, from);

		releaseLock();
		return result;
	}
	//------------------------------------------------------------------------------
	bool mg_rx_common_stream_t::rx_packet_stream_lock(const rtp_channel_t* channel, const rtp_packet* packet, const socket_address* from)
	{
		bool result = false;

		if (mg_rx_stream_t::getState() == h248::TerminationStreamMode::Loopback)
		{
			PLOG_STREAM_WRITE(LOG_PREFIX, "ID(%s) : rx_data_stream_lock() : stream loopback.", getName());

			if (channel != nullptr)
			{
				rtp_channel_t* channel_loop = (rtp_channel_t*)channel;
				result = channel_loop->send_packet(packet, from);
			}

			stat_invalid();

			return result;
		}

		if (m_recorder != nullptr)
		{
			m_recorder->write_packet(packet);
		}

		rtp_packet* pckt = (rtp_packet*)packet;
		if (!check_ssrc(pckt))
		{
			stat_invalid();
			//ERROR_FMT("wrong ssrc(%u)", packet->get_ssrc());
			return result;
		}
		if (!check_sequence_number(pckt))
		{
			ERROR_FMT("wrong sequence number(%u)", packet->get_sequence_number());
			return result;
		}
		if (!check_timestamp(pckt))
		{
			stat_invalid();
			ERROR_FMT("wrong timestamp(%u)", packet->getTimestamp());
			return result;
		}

		uint16_t payload_id = (uint16_t)packet->get_payload_type();
		m_cn_packet = (payload_id == 13);

		if (m_no_voice)
		{
			mg_termination__voice_restored_event();
			m_no_voice = false;
			m_last_send_novoice = 0;
		}
		m_last_tick = rtl::DateTime::getTicks();

		if (!activity())
		{
			stat_invalid();
			return result;
		}

		mg_rx_stream_t::getSession().check_address_from(from);

		int tube_count = getTubeCount();
		for (int i = 0; i < tube_count; i++)
		{
			Tube* tube_at_list = getTube(i);
			if (tube_at_list == nullptr)
			{
				continue;
			}

			// передаем только тубу конкретного пайлоад айди.
			if (tube_at_list->is_suited(payload_id))
			{
				tube_at_list->send(tube_at_list, packet);
				result = true;
			}

			if (!m_cn_packet)
			{
				// для туба комфортного шума посылаем пакеты всегда...
				// что б туб понял что шум закончился.
				HandlerType type = tube_at_list->getType();
				if (type == HandlerType::ComfortNoise)
				{
					tube_at_list->send(tube_at_list, packet);
				}
			}
		}

		stat_analize(pckt);
		rtcp_analize(pckt);

		return result;
	}
	//------------------------------------------------------------------------------
	bool mg_rx_common_stream_t::rx_data_stream_lock(const rtp_channel_t* channel, const uint8_t* data, uint32_t len, const socket_address* from)
	{
		bool result = false;

		if (!activity())
		{
			return result;
		}

		int tube_count = getTubeCount();
		for (int i = 0; i < tube_count; i++)
		{
			Tube* tube_at_list = getTube(i);
			if (tube_at_list == nullptr)
			{
				continue;
			}

			if (tube_at_list->getType() == HandlerType::Simple)
			{
				tube_at_list->send(tube_at_list, data, len);
				result = true;
				break;
			}
		}

		return result;
	}
	//------------------------------------------------------------------------------
	bool mg_rx_common_stream_t::rx_ctrl_stream_lock(const rtp_channel_t* channel, const rtcp_packet_t* packet, const socket_address* from)
	{
		bool result = false;
		if (!activity())
		{
			return result;
		}

		rtcp_analize(packet);

		bool find = false;
		int tube_count = getTubeCount();
		for (int i = 0; i < tube_count; i++)
		{
			Tube* tube_at_list = getTube(i);
			if (tube_at_list == nullptr)
			{
				continue;
			}

			if (tube_at_list->getType() == HandlerType::Simple)
			{
				tube_at_list->send(tube_at_list, packet);
				find = true;
				result = true;
				break;
			}
		}

		if (!find)
		{
			if (tube_count != 0)
			{
				Tube* tube_at_list = getTube(0);
				if (tube_at_list == nullptr)
				{
					return false;
				}

				tube_at_list->send(tube_at_list, packet);
				result = true;
			}
		}

		return result;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_rx_common_stream_t::VadDetector__audioSignalChanged(uint32_t termId, bool VAD, int volume)
	{
		mg_termination__vad_event(VAD, volume);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	int mg_rx_common_stream_t::rtcp_session_rx(const rtcp_packet_t* packet)
	{
		bool send = false;
		int tube_count = getTubeCount();
		for (int i = 0; i < tube_count; i++)
		{
			Tube* tube_at_list = getTube(i);
			if (tube_at_list == nullptr)
			{
				continue;
			}

			if (tube_at_list->getType() == HandlerType::Simple)
			{
				tube_at_list->send(tube_at_list, packet);
				send = true;
				break;
			}
		}

		if ((!send) && (tube_count != 0))
		{
			Tube* tube_at_list = getTube(0);
			if (tube_at_list != nullptr)
			{
				tube_at_list->send(tube_at_list, packet);
				send = true;
			}
		}

		return 1;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_rx_common_stream_t::check_ssrc(rtp_packet* packet)
	{
		uint32_t ssrc = packet->get_ssrc();

		if (packet->get_marker())
		{
			m_ssrc_from_rtp = ssrc;
			//PLOG_STREAM_WRITE(LOG_PREFIX, "check_ssrc -- ID(%s): New SSRC (%p).", getName(), m_ssrc_from_rtp);
			m_seq_num_from_rtp_last = packet->get_sequence_number() - 1;
			return true;
		}

		if (m_ssrc_from_rtp != ssrc)
		{
			PRINT_FMT("Detected New SSRC (%08X). Stream SSRC is (%08X)", ssrc, m_ssrc_from_rtp);

			m_new_ssrc_from_rtp_counter++;
			if (m_new_ssrc_from_rtp_counter < 6)
			{
				return false;
			}

			m_ssrc_from_rtp = ssrc;
			m_new_ssrc_from_rtp_counter = 0;
			packet->set_marker(true);
			PRINT_FMT("New SSRC (%08X)", m_ssrc_from_rtp);
			m_seq_num_from_rtp_last = packet->get_sequence_number() - 1;
		}
		else
		{
			m_new_ssrc_from_rtp_counter = 0;
		}

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_rx_common_stream_t::createRecorder(const char* path)
	{
		ENTER();

		if (m_recorder != nullptr)
		{
			PRINT("already created.");
			return true;
		}

		mg_media_element_list_t* el_list = getMediaParams().get_media_elements();
		if (el_list == nullptr)
		{
			WARNING("not found actual format.");
			return false;
		}

		media::PayloadSet record_formats;
		for (int i = 0; i < el_list->getCount(); i++)
		{
			mg_media_element_t* element = el_list->getAt(i);
			if (element == nullptr)
			{
				continue;
			}

			rtl::String rtpmap = element->get_param_rtpmap();
			if (rtpmap.isEmpty())
			{
				record_formats.addFormat(element->get_element_name(), (media::PayloadId)element->get_payload_id(), element->get_sample_rate(), element->getChannelCount(), element->get_param_fmtp());
			}
			else
			{
				record_formats.addFormat(element->get_element_name(), (media::PayloadId)element->get_payload_id(), element->get_sample_rate(), rtpmap, element->get_param_fmtp());
			}
		}

		if (record_formats.getCount() == 0)
		{
			WARNING("record formats not found");
			return false;
		}

		if (path == nullptr)
		{
			WARNING("config has no record path.");
			record_formats.removeAll();
			return false;
		}

		m_recorder = NEW media::LazyMediaWriterRTP(m_log);
		if (m_recorder == nullptr)
		{
			WARNING("failed to create audio recorder.");
			record_formats.removeAll();
			return false;
		}

		if (!m_recorder->create(path, record_formats))
		{
			WARNING("create fail.");
			record_formats.removeAll();
			DELETEO(m_recorder);
			m_recorder = nullptr;
			return false;
		}

		if (!m_recorder->start())
		{
			WARNING("start fail");
			record_formats.removeAll();
			m_recorder->close();
			DELETEO(m_recorder);
			m_recorder = nullptr;
			return false;
		}

		record_formats.removeAll();

		RETURN_BOOL(true);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_rx_common_stream_t::check_sequence_number(rtp_packet* packet)
	{
		m_seq_num_from_rtp = packet->get_sequence_number();
		short difference = m_seq_num_from_rtp - m_seq_num_from_rtp_last;
		if (difference == 0)
		{
			stat_invalid();
			ERROR_FMT("Media packet dublicated : last (%u) received (%u).", m_seq_num_from_rtp_last, m_seq_num_from_rtp);
			return false;
		}
		else if (difference < 0)
		{
			stat_out_of_order();
			ERROR_FMT("Media packet is out of order : last (%u) received (%u).", m_seq_num_from_rtp_last, m_seq_num_from_rtp);
		}
		else if (difference > 0)
		{
			if (difference > 1)
			{
				stat_lost();
				WARNING_FMT("Media packets lost : last (%u) received (%u).", m_seq_num_from_rtp_last, m_seq_num_from_rtp);
			}
			m_seq_num_from_rtp_last = m_seq_num_from_rtp;
			m_seq_num_from_rtp_diff = difference;
		}

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_rx_common_stream_t::check_timestamp(rtp_packet* packet)
	{
		m_timestamp_from_rtp_last = m_timestamp_from_rtp;
		m_timestamp_from_rtp = packet->getTimestamp();
		if (m_timestamp_from_rtp < m_timestamp_from_rtp_last)
		{
			WARNING_FMT("Media packets lost : last ts (%u) received ts (%u).", m_timestamp_from_rtp_last, m_timestamp_from_rtp);
		}

		if (!m_cn_packet)
		{
			uint32_t samples = m_timestamp_from_rtp - m_timestamp_from_rtp_last;
			samples = samples * m_seq_num_from_rtp_diff;

			packet->set_samples(samples);
		}

		return true;
	}
	//------------------------------------------------------------------------------
	void mg_rx_common_stream_t::change_timestamp_step(uint32_t timestamp_step)
	{
		int tube_count = getTubeCount();
		for (int i = 0; i < tube_count; i++)
		{
			Tube* tube_at_list = getTube(i);
			if (tube_at_list == nullptr)
			{
				continue;
			}

			if (tube_at_list->getType() == HandlerType::ComfortNoise)
			{
				mg_cn_tube_t* cn_tube = (mg_cn_tube_t*)tube_at_list;
				cn_tube->set_timestamp_step(timestamp_step);
			}
		}

		m_timestamp_step = timestamp_step;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_rx_common_stream_t::activity()
	{
		return (mg_rx_stream_t::getState() != h248::TerminationStreamMode::SendOnly) &&
			(mg_rx_stream_t::getState() != h248::TerminationStreamMode::Inactive);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_rx_common_stream_t::setMediaParams(mg_sdp_media_base_t& media)
	{
		mg_rx_stream_t::setMediaParams(media);

		uint32_t step = media.get_ptime();
		if (m_timestamp_step != step)
		{
			change_timestamp_step(step);
		}
	}
	//------------------------------------------------------------------------------
	bool mg_rx_common_stream_t::vad_createAnalyzer()
	{
		if (m_audio_analyzer != nullptr)
		{
			DELETEO(m_audio_analyzer);
			m_audio_analyzer = nullptr;
		}

		m_audio_analyzer = NEW mge::AudioAnalyzer(m_log);

		return m_audio_analyzer->initialize(this, m_vad_params);
	}
	//------------------------------------------------------------------------------
	void mg_rx_common_stream_t::vad_setParams(const mge::AudioAnalyzerParams& params)
	{
		m_vad_params = params;

		if (m_audio_analyzer == nullptr)
		{
			if (m_vad)
			{
				m_audio_analyzer = NEW mge::AudioAnalyzer(m_log);
				m_audio_analyzer->initialize(this, m_vad_params);
			}
		}
		else
		{
			m_audio_analyzer->updateParams(m_vad_params);
		}
	}
}
//------------------------------------------------------------------------------
