﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "std_mutex.h"
#include "std_thread.h"
#include "std_event.h"

#include "rtp_port_manager.h"

#include "mg_termination.h"
#include "mg_direction.h"
#include "mg_processor.h"
#include "mg_audio_analyzer.h"
#include "mg_video_selector.h"

namespace mge
{
	//-----------------------------------------------
	// forward declaration for Dummy
	//-----------------------------------------------
	class DummyTermination;

	//-----------------------------------------------
	//
	//-----------------------------------------------
	enum class MediaContextMode
	{
		PeerToPeer,
		Conference
	};
	//-----------------------------------------------
	//
	//-----------------------------------------------
	struct mg_termination_ready_analyzer
	{
		uint32_t term_id;
		uint32_t stream_id;
		bool remote_sdp_exists;
		bool need_build_chain;
		bool already_added;
	};
	//-----------------------------------------------
	//
	//-----------------------------------------------
	class ME_DEBUG_API MediaContext : public h248::IMediaContext
	{
	public:
		MediaContext(rtl::Logger* log, uint32_t ctxId, uint32_t mediaGateId);
		virtual ~MediaContext();

		uint32_t getId() const { return m_id; }
		const char* getName() const { return m_name; }


		int getTerminationCount() { return m_termination_list.getCount(); }

		// IMediaContext interface

		virtual bool mg_context_initialize(h248::ITerminationEventHandler* handler, h248::Field* reply) override;

		virtual uint32_t mg_context_get_id() override;

		virtual uint32_t mg_context_add_termination(h248::TerminationID& termId, h248::Field* reply) override;
		virtual void mg_context_remove_termination(uint32_t termId) override;

		virtual bool mg_termination_lock(uint32_t termId) override;
		virtual bool mg_termination_unlock(uint32_t termId) override;

		// настройки TerminationState
		virtual bool mg_termination_set_TerminationState(uint32_t termId, h248::TerminationServiceState state, h248::Field* reply) override;
		virtual bool mg_termination_set_TerminationState_EventControl(uint32_t termId, bool eventBufferControl, h248::Field* reply) override;
		virtual bool mg_termination_set_TerminationState_Property(uint32_t termId, const h248::Field* property, h248::Field* reply) override;
		// настройки LocalControl Local Remote
		virtual bool mg_termination_set_LocalControl_Mode(uint32_t termId, uint16_t streamId, h248::TerminationStreamMode mode, h248::Field* reply) override;
		virtual bool mg_termination_set_LocalControl_ReserveValue(uint32_t termId, uint16_t streamId, bool value, h248::Field* reply) override;
		virtual bool mg_termination_set_LocalControl_ReserveGroup(uint32_t termId, uint16_t streamId, bool value, h248::Field* reply) override;
		virtual bool mg_termination_set_LocalControl_Property(uint32_t termId, uint16_t streamId, const h248::Field* property, h248::Field* reply) override;
		// настройки Local и Remote
		virtual bool mg_termination_set_Local(uint32_t termId, uint16_t streamId, const rtl::String& localTemplate, rtl::String& local, h248::Field* reply) override;
		virtual bool mg_termination_set_Remote(uint32_t termId, uint16_t streamId, const rtl::String& remote, h248::Field* reply) override;
		virtual bool mg_termination_set_Local_Remote(uint32_t termId, uint16_t streamId, const rtl::String& remote, const rtl::String& local, h248::Field* reply) override;
		virtual bool mg_termination_get_Local(uint32_t termId, uint16_t streamId, rtl::String& local, h248::Field* reply) override;
		// настройки событий
		virtual bool mg_termination_set_events(uint32_t termId, uint16_t streamId, const h248::Field* eventDescriptor, h248::Field* reply) override;
		virtual bool mg_termination_set_signals(uint32_t termId, uint16_t streamId, const h248::Field* signalsDescriptor, h248::Field* reply) override;
		virtual bool mg_termination_set_property(uint32_t termId, const h248::Field* property, h248::Field* reply) override;

		virtual bool mg_context_set_topology(const rtl::ArrayT<h248::TopologyTriple>& topologyTriple, h248::Field* reply) override;
		virtual bool mg_context_set_property(const h248::Field* property, h248::Field* reply) override;

		virtual bool mg_context_lock();
		virtual bool mg_context_unlock();

		/// внутренний интерфейс для классов MediaProcessor и Direction
		rtl::Logger* getLog() { return m_log; }
		MediaContextMode getMode() { return m_mode; }

		int getConf_frequency() { return m_conf_sample_rate; }
		int getConf_ptime() { return m_conf_ptime; }

		bool isVideoConference() const { return m_video_conf; }
		const rtl::String& getConf_videoSchema() { return m_conf_video_schema; }
		AudioAnalyzer* getConf_audioAnalyzer() { return m_conf_audio_analyzer; }
		VideoSelector* getConf_videoSelector() { return m_conf_video_selector; }

	private:
		/// работа с терминаторами
		Termination* findTermination(uint32_t id_termination);
		Termination* createTermination(h248::TerminationID& termId, h248::Field* reply);
		void destroyTermination(uint32_t id_termination);

		void destroyAllTerminations();
		bool checkDummy();
		int addSpecialTermination(Termination* termination);
		bool maybe_add_to_direction(Termination* termination);

		/// работа с медиа процессером
		MediaProcessor* createProcessor(uint32_t type);
		MediaProcessor* findProcessor(uint32_t type);
		void destroyProcessor(uint32_t id_processor);
		void destroyAllProcessors();

		/// вывод в лог информации по контексту.
		//void info();

		bool prepareRecordPath();
		void makeRecordPath(Termination* term);

		// в webrtc при обработке rtpc пакетов происходит замена ssrc.
		// При этом в ssrc потоке аудио может приходить ssrc видео потока.
		// По этому rtcp мониторы должны знать о всех ssrc учавствующих в сессии.
		void load_rtcp_ssrc_for_webrtc(Termination* t1, Termination* t2);

	private:
		rtl::Logger* m_log;

		bool m_lock;
		uint32_t m_id;
		rtl::String m_name;
		MediaContextMode m_mode; // режим работы контекста.
		bool m_video_conf;
		DummyTermination* m_dummy_termination;

		/// Режим конференции
		int m_conf_sample_rate;
		int m_conf_ptime;
		rtl::String m_conf_video_schema;
		AudioAnalyzer* m_conf_audio_analyzer;
		VideoSelector* m_conf_video_selector;

		/// Общие параметры контекста
		uint32_t m_termination_id_counter;
		uint32_t m_processor_id_counter;
		TerminationList m_termination_list;
		MediaProcessorList m_processor_list; // удаление корректно!
		h248::ITerminationEventHandler* m_megaco_handler;

		/// путь куда пушутся файлы. Он же флаг, если пустой то запись не производится.
		rtl::String m_record_path;
		//rtl::String m_record_path_conference;
		bool m_new_record_path;
		rtl::String m_timestamp_record;			// Для фрмирования имени файла записи.

		rtl::ArrayT<mg_termination_ready_analyzer> m_transaction_ready;
	};
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	typedef rtl::PonterArrayT<MediaContext> MediaContextList;
}
//------------------------------------------------------------------------------
