﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_splitter_tube.h"
#include "rtp_packet.h"
#include "mg_media_element.h"
#include "mg_rx_stream.h"

#include "logdef_tube.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_splitter_tube_t::mg_splitter_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id) :
		Tube(HandlerType::AudioSplitter, id, log)
	{
		makeName(parent_id, "-splitter-tu");

		m_out_2 = nullptr;

		rtl::res_counter_t::add_ref(g_mge_tube_counter);

		setLogTag("TUB-SPLT");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_splitter_tube_t::~mg_splitter_tube_t()
	{
		unsubscribe(this);

		rtl::res_counter_t::release(g_mge_tube_counter);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_splitter_tube_t::init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out)
	{
		if (payload_el_in == nullptr)
		{
			ERROR_MSG("payload element is null.");
			return false;
		}
		m_payload_id = payload_el_in->get_payload_id();

		PRINT_FMT("payload in: %s %d", payload_el_in->get_element_name(), m_payload_id);

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_splitter_tube_t::set_out_handler_2(INextDataHandler* out_handler)
	{
		ENTER_FMT("%p", out_handler);

		bool result = false;

		uint32_t count = 0;
		while (count < 5)
		{
			if (m_lock.test_and_set(std::memory_order_acquire))
			{
				rtl::Thread::sleep(2);
				count++;
				continue;
			}

			m_out_2 = out_handler;

			m_lock.clear(std::memory_order_release);

			result = true;
			break;
		}

		RETURN_BOOL(result);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_splitter_tube_t::send(const INextDataHandler* out_handler, const rtp_packet* packet)
	{
		if (packet == nullptr)
		{
			ERROR_MSG("nothing to send!");
			return;
		}

		INextDataHandler* out = getNextHandler();

		if (out != nullptr)
		{
			out->send(this, packet);
		}
		else
		{
			WARNING("nowhere to send!");
		}

		if (m_out_2 != nullptr)
		{
			m_out_2->send(this, packet);
		}

		return;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_splitter_tube_t::unsubscribe(const INextDataHandler* out_handler)
	{
		ENTER();

		bool result = Tube::unsubscribe(out_handler);

		if (result)
		{
			result = false;

			uint32_t count = 0;
			while (count < 100)
			{
				if (m_lock.test_and_set(std::memory_order_acquire))
				{
					rtl::Thread::sleep(2);
					count++;
					continue;
				}

				if (m_out_2 != nullptr)
				{
					m_out_2->unsubscribe(this);
					m_out_2 = nullptr;
				}

				m_lock.clear(std::memory_order_release);

				result = true;
				break;
			}
		}

		RETURN_BOOL(result);
	}
}
//------------------------------------------------------------------------------
