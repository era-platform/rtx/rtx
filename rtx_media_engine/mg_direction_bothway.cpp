﻿	/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_direction_bothway.h"
#include "mg_tube_manager.h"
#include "mg_rx_stream.h"
#include "mg_tx_stream.h"
#include "mg_ivr_record_termination.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define LOG_PREFIX m_tag // "MG-DIR2" // Media Gateway Direction
#define MG_DTMF_EVENT "mg-dtmf"
#define MG_PROCESSOR_AUDIO getProcessorType() == 1

#include "logdef.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	BothwayDirection::BothwayDirection(MediaContext* owner, uint32_t id, const char* parent_id) :
		Direction(owner, id, parent_id, DirectionType::bothway)
	{
		strcpy(m_tag, "DIR-B2B");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	BothwayDirection::~BothwayDirection()
	{
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool BothwayDirection::addTermination(Termination* term)
	{
		ENTER();

		if (term == nullptr)
		{
			ERROR_MSG("termination is null");
			return false;
		}

		int res;

		if ((res = checkTermination(term)) < 0)
		{
			ERROR_FMT("checkTermination return %d", res);
			return false;
		}

		int result_rx = buildRx();
		if (result_rx < 0)
		{
			//ERROR_FMT("build_rx failed - %d", result_rx);
			return false;
		}
		else if (result_rx == 0)
		{
			//RETURN_FMT(true, "term: %s", term->getName());
			return true;
		}

		if (!buildTx())
		{
			//ERROR_MSG("build_tx fail.");
			return false;
		}

		RETURN_FMT(true, "id term: %s", term->getName());
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	int BothwayDirection::checkTermination(Termination* term)
	{
		/*if (!term->isReady(getProcessorType()))
		{
			return -1;
		}*/

		uint32_t count = getTerminationCount();
		if (count > 2)
		{
			return -2;
		}

		if (count == 2)
		{
			if (containsTermination(term->getId()))
			{
				return 0;
			}
			return -3;
		}
		
		if (count == 1)
		{
			if (containsTermination(term->getId()))
			{
				WARNING_FMT("termination: %s already exist.", term->getName());
				return -4;
			}

			Direction::addTermination(term);
			return 1;
		}

		Direction::addTermination(term);
		return 1;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	int BothwayDirection::build(Termination* term_1, Termination* term_2)
	{
		MediaSession* stream_1 = term_1->findMediaSession(getProcessorType());
		if (stream_1 == nullptr)
		{
			ERROR_FMT("term_1(%s) stream is null.", term_1->getName());
			return -1;
		}

		MediaSession* stream_2 = term_2->findMediaSession(getProcessorType());
		if (stream_2 == nullptr)
		{
			ERROR_FMT("term_2(%s) stream is null.", term_2->getName());
			return -1;
		}

		PRINT_FMT("term_1(%s) term_2(%s)", (const char*)term_1->getName(), (const char*)term_2->getName());

		const rtl::ArrayT<mg_rx_stream_t*>& list_rx = stream_1->get_rx_streams();
		const rtl::ArrayT<mg_tx_stream_t*>& list_tx = stream_2->get_tx_streams();

		copyElementsToIvrRecord(term_1, term_2);

		int count_rx = list_rx.getCount();
		int count_tx = list_tx.getCount();
		int index_rx = (count_rx > count_tx) ? (count_rx - count_tx) : 0;
		// RTX-66 и RTX-82.
		// sipml при каждом холде добавляет стрим. По этому нужно брать последний.
		// Но при видео приходят два стрима. И если опозитный стрим простой RTP то там один поток. По этому замыкать нужно первый (второй сигнальный).
		if (getProcessorType() == 2)
		{
			// так же sipml для видео дублирует пару потоков
			index_rx = (index_rx > 0) ? index_rx - 1 : 0;
		}

		for (int i = 0; i < count_rx; i++, index_rx++)
		{
			mg_rx_stream_t* rx_stream = (count_rx <= index_rx) ? nullptr : list_rx[index_rx];
			if (rx_stream == nullptr)
			{
				continue;
			}

			mg_tx_stream_t* tx_stream = (count_tx <= i) ? nullptr : list_tx[i];
			if (tx_stream == nullptr)
			{
				continue;
			}

			mg_media_element_list_t list_1_out;
			mg_media_element_list_t list_2_out;

			mg_media_element_list_t fill_list_local_rx;
			mg_media_element_list_t fill_list_remote_rx;
			mg_media_element_list_t fill_list_local_tx;
			mg_media_element_list_t fill_list_remote_tx;

			fillElementListRx(term_1->getType(), rx_stream, tx_stream, &fill_list_local_rx, &fill_list_remote_rx);
			fillElementListTx(term_2->getType(), rx_stream, tx_stream, &fill_list_local_tx, &fill_list_remote_tx);

			if (fill_list_local_rx.getCount() == 0)
			{
				ERROR_FMT("term_1(%s) remote payloads is null.", term_1->getName());
				continue;
			}
			if (fill_list_remote_tx.getCount() == 0)
			{
				ERROR_FMT("term_2(%s) remote payloads is null.", term_2->getName());
				continue;
			}

			comparePayloads(&fill_list_local_rx, &list_1_out, &fill_list_remote_tx, &list_2_out);

			printPayloads(&list_1_out, &list_2_out);

			//---------------------------------------------------------------------

			if (!associateStreams(&list_1_out, rx_stream, &list_2_out, tx_stream))
			{
				ERROR_FMT("associate_streams(%s -> %s) fail.", rx_stream->getName(), tx_stream->getName());
				return -1;
			}
		}

		RETURN_FMT(1, "associate_streams(%s -> %s) ok.", stream_1->getName(), stream_2->getName());
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	int BothwayDirection::buildRx()
	{
		// если в списке есть ivr найдем его.
		Termination* term_1 = getTermination(0);
		if (term_1 == nullptr)
		{
			//WARNING("no required termination_1");
			return -1;
		}

		Termination* term_2 = getTermination(1);
		if (term_2 == nullptr)
		{
			//WARNING("no required termination_2");
			return -1;
		}

		cleanupTerminationTubes(term_1);
		cleanupTerminationTubes(term_2);

		PRINT_FMT("term_1(%s) term_2(%s)...", (const char*)term_1->getName(), (const char*)term_2->getName());

		if ((term_1->getType() == h248::TerminationType::IVR_Record))
		{
			if (buildTx() < 0)
			{
				return -1;
			}
			return 0;
		}
		else if ((term_2->getType() == h248::TerminationType::IVR_Record))
		{
			return build(term_1, term_2);
		}
		else
		{
			return build(term_1, term_2);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	int BothwayDirection::buildTx()
	{
		Termination* term_1 = getTermination(0);
		if (term_1 == nullptr)
		{
			return -1;
		}
		Termination* term_2 = getTermination(1);
		if (term_2 == nullptr)
		{
			return -1;
		}

		PRINT_FMT("term_1(%s) term_2(%s)...", (const char*)term_1->getName(), (const char*)term_2->getName());

		if ((term_1->getType() == h248::TerminationType::IVR_Record))
		{
			if (build(term_2, term_1))
			{
				term_1->mg_stream_start(getProcessorType());
				term_2->mg_stream_start(getProcessorType());
				return 1;
			}
		}
		else if ((term_2->getType() == h248::TerminationType::IVR_Record))
		{
			term_1->mg_stream_start(getProcessorType());
			term_2->mg_stream_start(getProcessorType());
			return 0;
		}
		else
		{
			if (build(term_2, term_1))
			{
				term_1->mg_stream_start(getProcessorType());
				term_2->mg_stream_start(getProcessorType());
				return 1;
			}
		}

		return -1;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void BothwayDirection::comparePayloads(const mg_media_element_list_t* list_1, mg_media_element_list_t* list_1_out,
		const mg_media_element_list_t* list_2, mg_media_element_list_t* list_2_out) const
	{
		ENTER();

		if ((list_1 == nullptr) || (list_1_out == nullptr) || (list_2 == nullptr) || (list_2_out == nullptr))
		{
			ERROR_MSG("wrong params");
			return;
		}

		bool use_dtmf = (Config.get_config_value(MG_DTMF_EVENT) != nullptr);

		rtl::String term_id_1 = list_1->get_termination_id();
		rtl::String term_id_2 = list_2->get_termination_id();

		bool ivr = (term_id_1.indexOf("IVR") != BAD_INDEX) || (term_id_2.indexOf("IVR") != BAD_INDEX);

		mg_media_element_list_t temp_list_1_out;

		// индекс комфортного шума
		int cn_index = -1;
		// если нет в списках то надо добавлять.
		// если это процессор видео то не надо добавлять.

		int index_priority = -1;
		for (int index = 0; index < list_1->getCount(); index++)
		{
			mg_media_element_t* element = list_1->getAt(index);
			if (element == nullptr)
			{
				continue;
			}

			if ((!element->isDtmf()) && (index_priority == -1))
			{
				index_priority = index;
			}
			else if (!use_dtmf)
			{
				continue;
			}

			if ((element->get_payload_id() == 13) || (element->get_element_name().indexOf("CN") != BAD_INDEX))
			{
				cn_index = index;
			}

			if (element->get_element_name().indexOf("IVR") != BAD_INDEX)
			{
				ivr = true;
			}

			list_1_out->create_with_copy_from(element, mg_media_element_list_t::POSITION_END);
			temp_list_1_out.create_with_copy_from(element, mg_media_element_list_t::POSITION_END);
			list_2_out->create(term_id_2, mg_media_element_t::NULL_ELEMENT, mg_media_element_list_t::POSITION_END);
		}
		list_1_out->set_priority_index(index_priority);
		temp_list_1_out.set_priority_index(index_priority);

		if (cn_index < 0)
		{
			if ((MG_PROCESSOR_AUDIO) && (!ivr)) // аудио
			{
				list_1_out->create(term_id_1, 13, mg_media_element_list_t::POSITION_END);
				temp_list_1_out.create(term_id_1, 13, mg_media_element_list_t::POSITION_END);
				list_2_out->create(term_id_2, mg_media_element_t::NULL_ELEMENT, mg_media_element_list_t::POSITION_END);
				cn_index = list_1_out->getCount() - 1;
			}
		}

		bool priority_check = false;
		for (int index_1 = 0; index_1 < list_2->getCount(); index_1++)
		{
			mg_media_element_t* element_1 = list_2->getAt(index_1);
			if (element_1 == nullptr)
			{
				continue;
			}

			if ((element_1->isDtmf()) && (!use_dtmf))
			{
				continue;
			}

			bool found = false;
			int index_2 = 0;
			for (; index_2 < temp_list_1_out.getCount(); index_2++)
			{
				mg_media_element_t* element_2 = temp_list_1_out.getAt(index_2);
				if (element_2 == nullptr)
				{
					list_1_out->removeAt(index_2);
					temp_list_1_out.removeAt(index_2);
					index_2--;
					continue;
				}

				if (element_1->compare_with(element_2))
				{
					list_2_out->removeAt(index_2);
					list_2_out->create_with_copy_from(element_1, index_2);
					if ((!priority_check) && (!element_1->isDtmf()))
					{
						list_2_out->set_priority_index(index_2);
						priority_check = true;
					}
					found = true;
					temp_list_1_out.removeAt(index_2);
					temp_list_1_out.create(term_id_1, mg_media_element_t::NULL_ELEMENT, /*MG_PROCESSOR_AUDIO, */index_2);
					break;
				}
			}

			if (!found)
			{
				list_2_out->create_with_copy_from(element_1, mg_media_element_list_t::POSITION_END);
				list_1_out->create(term_id_1, mg_media_element_t::NULL_ELEMENT, mg_media_element_list_t::POSITION_END);
				if (!priority_check)
				{
					uint32_t count = list_2_out->getCount();
					uint32_t last_index = count - 1;
					mg_media_element_t* last_element = list_2_out->getAt(last_index);
					if ((last_element != nullptr) && (!last_element->isDtmf()))
					{
						list_2_out->set_priority_index(last_index);
						priority_check = true;
					}
				}
			}
		}

		if ((MG_PROCESSOR_AUDIO) && (!ivr))
		{
			mg_media_element_t* element_cn = list_2_out->getAt(cn_index);
			if (element_cn == nullptr)
			{
				WARNING_FMT("element_cn not found by index:%d.", cn_index);
				return;
			}

			if (element_cn->isNull())
			{
				list_2_out->create(term_id_2, 13, mg_media_element_list_t::POSITION_END);
				list_1_out->create(term_id_1, mg_media_element_t::NULL_ELEMENT, mg_media_element_list_t::POSITION_END);
			}
		}

		uint32_t count_1 = list_1_out->getCount();
		uint32_t count_2 = list_2_out->getCount();

		if (count_1 != count_2)
		{
			WARNING("failed to add element!");

			if (count_1 > count_2)
			{
				uint32_t diff = count_1 - count_2;
				while (diff > 0)
				{
					list_2_out->create(term_id_2, mg_media_element_t::NULL_ELEMENT, mg_media_element_list_t::POSITION_END);
					diff--;
				}
			}
			else
			{
				uint32_t diff = count_2 - count_1;
				while (diff > 0)
				{
					list_1_out->create(term_id_1, mg_media_element_t::NULL_ELEMENT, mg_media_element_list_t::POSITION_END);
					diff--;
				}
			}
		}
		
		LEAVE();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void BothwayDirection::printPayloads(const mg_media_element_list_t* list_1_out, const mg_media_element_list_t* list_2_out)
	{
		if (list_1_out == nullptr)
		{
			ERROR_MSG("list_1_out is null in direction");
			return;
		}

		if (list_2_out == nullptr)
		{
			ERROR_MSG("list_2_out is null in direction");
			return;
		}

		ENTER();

		PLOG_WRITE(LOG_PREFIX, "-------------------------------------------------------------------------");
		PLOG_WRITE(LOG_PREFIX, "index| list_1_out  (RX)         | list_2_out  (TX)          | payloads");
		PLOG_WRITE(LOG_PREFIX, "-------------------------------------------------------------------------");

		uint32_t count_1 = list_1_out->getCount();
		uint32_t index = 0;
		while (index < count_1)
		{
			rtl::String index_str;
			index_str << index;
			while (index_str.getLength() < 5)
			{
				index_str << " ";
			}

			mg_media_element_t* element_1 = list_1_out->getAt(index);
			mg_media_element_t* element_2 = list_2_out->getAt(index);
			if ((element_1 == nullptr) || (element_2 == nullptr))
			{
				PLOG_WRITE(LOG_PREFIX, "%s|----------------------------  ERROR!!!", (const char*)index_str);
				index++;
				continue;
			}

			rtl::String el_str_1 = element_1->get_element_name();
			if (el_str_1.toLower().indexOf("null") == BAD_INDEX)
			{
				el_str_1 << "/" << element_1->get_sample_rate();
			}
			while (el_str_1.getLength() <= 24)
			{
				el_str_1 << " ";
			}

			rtl::String el_str_2 = element_2->get_element_name();
			if (el_str_2.toLower().indexOf("null") == BAD_INDEX)
			{
				el_str_2 << "/" << element_2->get_sample_rate();
			}
			while (el_str_2.getLength() <= 24)
			{
				el_str_2 << " ";
			}

			rtl::String pid1 = "";
			if (element_1->get_payload_id() == mg_media_element_t::NULL_ELEMENT)
			{
				pid1 = "   ";
			}
			else
			{
				if (element_1->get_payload_id() > 99)
				{
					pid1 << element_1->get_payload_id();
				}
				else if (element_1->get_payload_id() > 9)
				{
					pid1 << element_1->get_payload_id() << " ";
				}
				else
				{
					pid1 << element_1->get_payload_id() << "  ";
				}
			}
			rtl::String pid2 = "";
			if (element_2->get_payload_id() == mg_media_element_t::NULL_ELEMENT)
			{
				pid2 = "   ";
			}
			else
			{
				if (element_2->get_payload_id() > 99)
				{
					pid2 << element_2->get_payload_id();
				}
				else if (element_2->get_payload_id() > 9)
				{
					pid2 << element_2->get_payload_id() << " ";
				}
				else
				{
					pid2 << element_2->get_payload_id() << "  ";
				}
			}

			PLOG_WRITE(LOG_PREFIX, "%s| %s| %s | %s - %s",
				(const char*)index_str, (const char*)el_str_1, (const char*)el_str_2, (const char*)pid1, (const char*)pid2);
			index++;
		}
		PLOG_WRITE(LOG_PREFIX, "-------------------------------------------------------------------------");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool BothwayDirection::associateStreams(
		const mg_media_element_list_t* list_in, mg_rx_stream_t* mg_stream_in,
		const mg_media_element_list_t* list_out, mg_tx_stream_t* mg_stream_out)
	{
		if ((mg_stream_in == nullptr) || (mg_stream_out == nullptr) || (list_in == nullptr) || (list_out == nullptr))
		{
			ERROR_MSG("wrong params!");
			return false;
		}

		uint32_t count_1 = list_in->getCount();
		uint32_t count_2 = list_out->getCount();
		if (count_1 != count_2)
		{
			ERROR_MSG("no compliance out lists!");
			return false;
		}

		for (uint32_t index = 0; index < count_1; index++)
		{
			mg_media_element_t* element_in = list_in->getAt(index);
			mg_media_element_t* element_out = list_out->getAt(index);
			if ((element_in == nullptr) || (element_out == nullptr))
			{
				ERROR_MSG("elements is null!");
				continue;
			}

			if (element_in->isNull())
			{
				continue;
			}

			if (element_out->isNull())
			{
				// значит траскодинг
				element_out = list_out->get_priority();
				if (element_out == nullptr)
				{
					ERROR_MSG("element_out is null.");
					continue;
				}
			}

			if (!associateElements(element_in, mg_stream_in, element_out, mg_stream_out))
			{
				continue;
			}
		}

		return true;
	}
	//------------------------------------------------------------------------------
	bool BothwayDirection::associateElements(
		mg_media_element_t* element_in, mg_rx_stream_t* mg_stream_in,
		mg_media_element_t* element_out, mg_tx_stream_t* mg_stream_out)
	{
		ENTER_FMT("%s -> %s", (const char*)element_in->get_element_name(), (const char*)element_out->get_element_name());

		Tube* tube_in = nullptr;
		Tube* tube_out = nullptr;

		mg_splitter_params_t p;
		if (MediaSession::vad_isActive(mg_stream_in, &p))
		{
			element_in->setSplitterParams(&p);
		}

		if (!TubeManager::prepare_tubes_chain(this, element_in, element_out, &tube_in, &tube_out, true, true))
		{
			ERROR_MSG("prepare tubes chain fail!");
			return false;
		}

		if ((tube_in == nullptr) || (tube_out == nullptr))
		{
			ERROR_MSG("bad tubes!");
			return false;
		}

		if (!customizeStreamRx(mg_stream_in, tube_in))
		{
			ERROR_MSG("customize rx fail.");

			// достаточно вызвать очистку первого туба. остальные отцепятся по цепочке.
			// единственное то что сами объекты будут в списке тубов.
			// удаление прозойдет при полной очистки дирекшина.
			// пока так... 
			//TubeManager::clear_tube(this, tube_in);
			tube_in->release();
			return false;
		}

		if ((element_in->isDtmf()) && (!element_out->isDtmf()))
		{
			RETURN_FMT(false, "(%s -> null) ok", (const char*)element_in->get_element_name());
		}

		if (!customizeStreamTx(tube_out, mg_stream_out))
		{
			ERROR_MSG("customize tx fail!");

			// отписываем туб из входного стрима.
			mg_stream_in->unsubscribe_tube(tube_in->getId());

			// так как tx стрим наследутется от интерфейса туба,
			// то по цепочке тубов он сам отпишется.
			//TubeManager::clear_tube(this, tube_in);
			if (tube_in != nullptr)
			{
				HandlerType t = tube_in->getType();
				tube_in->unsubscribe(tube_in);
			}
			return false;
		}

		RETURN_FMT(true, "(%s -> %s) ok.",
			(const char*)element_in->get_element_name(),
			(const char*)element_out->get_element_name());
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void BothwayDirection::fillElementListRx(h248::TerminationType termination_type, mg_rx_stream_t* rx_stream, mg_tx_stream_t* tx_stream,
		mg_media_element_list_t* list_rx, mg_media_element_list_t* list_tx)
	{
		list_rx->clear();
		list_tx->clear();

		if ((termination_type == h248::TerminationType::IVR_Player) ||
			(termination_type == h248::TerminationType::IVR_Record))
		{
			if (rx_stream != nullptr)
			{
				list_rx->create_with_copy_from(rx_stream->getMediaParams().get_media_elements()->get_priority(), 0);
			}
			if (tx_stream != nullptr)
			{
				list_tx->create_with_copy_from(tx_stream->getMediaParams().get_media_elements()->get_priority(), 0);
			}
		}
		else
		{
			if (rx_stream != nullptr)
			{
				list_rx->copy_from(rx_stream->getMediaParams().get_media_elements());
			}
			if (tx_stream != nullptr)
			{
				list_tx->copy_from(tx_stream->getMediaParams().get_media_elements());
			}
		}
	}
	void BothwayDirection::fillElementListTx(h248::TerminationType termination_type, mg_rx_stream_t* rx_stream, mg_tx_stream_t* tx_stream,
		mg_media_element_list_t* list_rx, mg_media_element_list_t* list_tx)
	{
		list_rx->clear();
		list_tx->clear();

		if ((termination_type == h248::TerminationType::IVR_Record) ||
			(termination_type == h248::TerminationType::IVR_Player))
		{
			if (rx_stream != nullptr)
			{
				list_rx->create_with_copy_from(rx_stream->getMediaParams().get_media_elements()->get_priority(), 0);
			}
			if (tx_stream != nullptr)
			{
				list_tx->create_with_copy_from(tx_stream->getMediaParams().get_media_elements()->get_priority(), 0);
			}
		}
		else
		{
			if (rx_stream != nullptr)
			{
				list_rx->copy_from(rx_stream->getMediaParams().get_media_elements());
			}
			if (tx_stream != nullptr)
			{
				list_tx->copy_from(tx_stream->getMediaParams().get_media_elements());
			}
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void BothwayDirection::copyElementsToIvrRecord(Termination* term_1, Termination* term_2)
	{
		MediaSession* stream_1 = term_1->findMediaSession(getProcessorType());
		MediaSession* stream_2 = term_2->findMediaSession(getProcessorType());

		if (term_1->getType() == h248::TerminationType::IVR_Record)
		{
			IVRRecorderTermination* rec_t = (IVRRecorderTermination*)term_1;
			rtl::String t(rec_t->get_record_type());
			if (rtl::String::compare("rtp", t, false) == 0)
			{
				PRINT_FMT("copy sdp to term_1(%s) from term_2(%s)...",
					(const char*)term_1->getName(),
					(const char*)term_2->getName());
				
				stream_1->get_tx_stream_at(0)->setMediaParams(stream_2->get_rx_stream_at(0)->getMediaParams());
			}
		}
		else if (term_2->getType() == h248::TerminationType::IVR_Record)
		{
			IVRRecorderTermination* rec_t = (IVRRecorderTermination*)term_2;
			rtl::String t(rec_t->get_record_type());
			if (rtl::String::compare("rtp", t, false) == 0)
			{
				PRINT_FMT("copy sdp to term_2(%s) from term_1(%s)...",
					(const char*)term_2->getName(),
					(const char*)term_1->getName());

				stream_2->get_tx_stream_at(0)->setMediaParams(stream_1->get_rx_stream_at(0)->getMediaParams());
			}
		}

	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
