﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_ivr_record_termination.h"
#include "mg_rx_ivr_stream.h"
#include "mg_tx_ivr_stream.h"
#include "std_filesystem.h"
#include "std_sys_env.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define LOG_PREFIX m_tag //"MG-TREC" // Media Gateway IVR Record Termination

#include "logdef.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	IVRRecorderTermination::IVRRecorderTermination(rtl::Logger* log, const h248::TerminationID& termId, const char* parent_id) :
		Termination(log, termId, parent_id)
	{
		m_service_state = h248::TerminationServiceState::Test;
		m_event_buffer_control = false;
		m_record_type = "rtp";
		strcpy(m_tag, "TERM-REC");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	IVRRecorderTermination::~IVRRecorderTermination()
	{
		mg_termination_destroy(nullptr);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool IVRRecorderTermination::mg_termination_initialize(h248::Field* reply)
	{
		ENTER();

		MediaSession* mg_session = makeMediaSession(MG_STREAM_TYPE_AUDIO);
		if (mg_session == nullptr)
		{
			ERROR_MSG("create stream fail");
			reply->addErrorDescriptor(h248::ErrorCode::c510_Insufficient_resources, "Not enough memory to create a media session");
			// !!! @error code
			return false;
		}

		if (!mg_session->create_tx_stream(mg_tx_stream_type_t::mg_tx_ivr_e))
		{
			ERROR_MSG("create tx stream fail.");
			Termination::releaseMediaSession(MG_STREAM_TYPE_AUDIO);
			// !!! @error code
			reply->addErrorDescriptor(h248::ErrorCode::c510_Insufficient_resources, "Not enough memory to create a transmitter stream");
			return false;
		}

		mg_tx_stream_t* tx_stream = mg_session->get_tx_stream();

		// RTX-17
		mg_sdp_media_base_t& sdp = tx_stream->getMediaParams();
		mg_media_element_list_t* elements = sdp.get_media_elements();
		elements->clear();
		mg_media_element_t* el = elements->create(getName(), 0, 0);
		elements->set_priority_index(0);
		el->set_sample_rate(8000);
		el->set_element_name("pcmu");

		RETURN_BOOL(true);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool IVRRecorderTermination::mg_termination_destroy(h248::Field* reply)
	{
		ENTER();

		mg_stream_stop(MG_STREAM_TYPE_AUDIO);

		Termination::destroyMediaSession();

		RETURN_BOOL(true);
	}
	//------------------------------------------------------------------------------
	bool IVRRecorderTermination::mg_termination_set_TerminationState_Property(const h248::Field* property, h248::Field* reply)
	{
		const rtl::String& propName = property->getName();
		const rtl::String& propValue = property->getValue();

		ENTER_FMT("name:%s, value:%s", (const char*)propName, (const char*)propValue);

		MediaSession* mg_session = Termination::findMediaSession(MG_STREAM_TYPE_AUDIO);
		if (mg_session == nullptr)
		{
			ERROR_MSG("mg_session is null");
			reply->addErrorDescriptor(h248::ErrorCode::c500_Internal_software_failure_in_the_MG_or_MGC, "IVR recorder initialization failed");
			return false;
		}

		const rtl::ArrayT<mg_tx_stream_t*>& tx_streams = mg_session->get_tx_streams();
		if (tx_streams.getCount() == 0)
		{
			ERROR_MSG("ivr stream not found.");
			reply->addErrorDescriptor(h248::ErrorCode::c500_Internal_software_failure_in_the_MG_or_MGC, "IVR Player tx stream initialization failed");
			return false;
		}
		mg_tx_ivr_stream_t* ivr_stream = (mg_tx_ivr_stream_t*)tx_streams.getAt(0);
		if (ivr_stream == nullptr)
		{
			ERROR_MSG("ivr_stream is null.");
			reply->addErrorDescriptor(h248::ErrorCode::c500_Internal_software_failure_in_the_MG_or_MGC, "IVR Player tx stream initialization failed");
			return false;
		}

		rtl::String value = unescape_rfc3986(propValue);

		if (rtl::String::compare("file", propName, false) == 0)
		{
			rtl::String path = utf8_to_locale(value);

			path.replace('\\', FS_PATH_DELIMITER);
			path.replace('/', FS_PATH_DELIMITER);

			rtl::String full_path = get_local_path(path);

			if (full_path.isEmpty())
			{
				ERROR_FMT("prepare directory fail! (%s)", getName(), (const char*)full_path);
				reply->addErrorDescriptor(h248::ErrorCode::c500_Internal_software_failure_in_the_MG_or_MGC, "IVR Recorder file failed");
				return false;
			}

			int index_to_file = full_path.indexOf(".");
			if (index_to_file == BAD_INDEX)
			{
				ERROR_FMT("prepare directory fail! (%s)", getName(), (const char*)full_path);
				reply->addErrorDescriptor(h248::ErrorCode::c500_Internal_software_failure_in_the_MG_or_MGC, "IVR Recorder file failed");
				return false;
			}
			int index = index_to_file - 1;
			while (index > 0)
			{
				if (full_path[index] == FS_PATH_DELIMITER)
				{
					break;
				}
				index--;
			}
			index++;

			rtl::String path_to_file = full_path.substring(0, index);
			if (!fs_check_path_and_create(path_to_file))
			{
				ERROR_FMT("directory create fail! (%s)", (const char*)path_to_file);
				reply->addErrorDescriptor(h248::ErrorCode::c500_Internal_software_failure_in_the_MG_or_MGC, "IVR Recorder file failed");
				return false;
			}

			ivr_stream->set_record_path(full_path);
		}
		else if (rtl::String::compare("buffer_duration", propName, false) == 0)
		{
			uint32_t buffer_size = (uint32_t)strtoul(value, nullptr, 10);
			ivr_stream->set_buffer_size(buffer_size);
		}
		else if (rtl::String::compare("type", propName, false) == 0)
		{
			ivr_stream->set_type(value);

			m_record_type = value;

			if (rtl::String::compare("raw", m_record_type, false) == 0)
			{
				// RTX-97
				mg_sdp_media_base_t& sdp = ivr_stream->getMediaParams();
				mg_media_element_list_t* elements = sdp.get_media_elements();
				elements->clear();
				mg_media_element_t* el = elements->create(getName(), 11, 0);
				elements->set_priority_index(0);
				el->set_sample_rate(8000);
				el->set_element_name("L16");
			}
		}

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool IVRRecorderTermination::mg_termination_set_Local_Remote(uint16_t stream_id, const rtl::String& remote_sdp, const rtl::String& local_sdp, h248::Field* reply)
	{
		ENTER_FMT("%u", stream_id);

		MediaSession* mg_session = makeMediaSession(stream_id);
		if (mg_session == nullptr)
		{
			ERROR_MSG("create stream fail.");
			return false;
		}

		mg_sdp_session_t sdp(m_log);
		if (!sdp.read(local_sdp, false, mg_session->getId()))
		{
			ERROR_MSG("local sdp read fail.");
			return false;
		}

		const rtl::ArrayT<mg_tx_stream_t*>& tx_streams = mg_session->get_tx_streams();
		if (tx_streams.isEmpty())
		{
			ERROR_MSG("ivr tx stream list is empty.");
			return false;
		}
		mg_tx_stream_t* tx_stream = tx_streams.getFirst();
		if (tx_stream == nullptr)
		{
			ERROR_MSG("ivr tx stream not created.");
			return false;
		}

		mg_sdp_media_base_t* audio = sdp.get_media(mg_session->getId());
		if (audio == nullptr)
		{
			return false;
		}

		tx_stream->setMediaParams(*audio);

		RETURN_BOOL(true);
	}
	//------------------------------------------------------------------------------
	bool IVRRecorderTermination::mg_termination_get_Local(uint16_t stream_id, rtl::String& local_sdp_answer, h248::Field* reply)
	{
		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	const char* IVRRecorderTermination::get_record_type()
	{
		return m_record_type;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool IVRRecorderTermination::mg_stream_start(uint32_t stream_id)
	{
		ENTER();

		MediaSession* mg_session = Termination::findMediaSession(stream_id);
		if (mg_session == nullptr)
		{
			ERROR_MSG("mg_session is null.");
			return false;
		}

		const rtl::ArrayT<mg_tx_stream_t*>& tx_streams = mg_session->get_tx_streams();
		if (tx_streams.isEmpty())
		{
			ERROR_MSG("ivr stream list is empty.");
			return false;
		}
		mg_tx_stream_t* tx_stream = tx_streams.getFirst();
		if (tx_stream == nullptr)
		{
			ERROR_MSG("ivr stream not found.");
			return false;
		}

		mg_tx_ivr_stream_t* ivr_stream = (mg_tx_ivr_stream_t*)tx_stream;
		if (ivr_stream == nullptr)
		{
			ERROR_MSG("ivr_stream is null.");
			return false;
		}

		if (!ivr_stream->create_recorder())
		{
			ERROR_MSG("create recorder fail.");
			return false;
		}

		bool res = Termination::mg_termination_unlock();

		RETURN_BOOL(res);

		return res;
	}
	//------------------------------------------------------------------------------
	void IVRRecorderTermination::mg_stream_stop(uint32_t stream_id)
	{
		ENTER();

		Termination::mg_termination_lock();

		LEAVE();
	}
}
//------------------------------------------------------------------------------
