/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_video_mixer.h"
#include "mg_video_conference_in_tube.h"
#include "mg_video_conference_out_tube.h"
//-----------------------------------------------
//
//-----------------------------------------------
#define LOG_PREFIX "VMIX"
#define INCOMING_FRAME	1001
#define OUTGOING_FRAME  1002
//#define DEFAULT_FRAME_RATE 40 // 25 frames per second
#define DEFAULT_FRAME_RATE 66 // 15 frames per second

#include "logdef.h"

namespace mge
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VideoMixer::VideoMixer(rtl::Logger* log, const char* instname) 
		: m_timer(true, this, *log, "v-timer"),
		m_log(log), m_name(instname), m_started(false),
		m_lastDrawTS(0)
	{
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VideoMixer::~VideoMixer()
	{
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoMixer::start(const char* schema)
	{
		stop();

		if (m_frame.initialize(schema))
		{
			// starting timer etc
			m_timer.start(DEFAULT_FRAME_RATE);
			m_lastDrawTS = rtl::DateTime::getTicks();
			// starting encoder's message queue
			m_started = m_encoderQueue.start(rtl::ThreadPriority::Realtime);
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoMixer::restart(const char* schema)
	{
		if (m_frame.initialize(schema))
		{
			// starting timer etc
			if (!m_timer.isStarted())
			{
				m_timer.start(DEFAULT_FRAME_RATE);
				m_lastDrawTS = rtl::DateTime::getTicks();
			}

			// starting encoder's message queue
			m_started = m_encoderQueue.start(rtl::ThreadPriority::High);
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoMixer::stop()
	{
		m_started = false;

		m_timer.stop();

		m_encoderQueue.stop();

		{
			rtl::MutexLock lock(m_memberListSync);

			int count = m_memberList.getCount();

			for (int index = 0; index < count; index++)
			{
				VideoMixerMember* member = m_memberList[index];

				if (member->inputTube != nullptr)
				{
					member->inputTube->unsubscribe(member->inputTube);
				}

				if (member->outputTube != nullptr)
				{
					member->outputTube->unsubscribe(member->outputTube);
				}

				DELETEO(member);
			}

			m_memberList.clear();

			count = m_codecList.getCount();

			for (int index = 0; index < count; index++)
			{
				VideoMixerEncoder* enc = m_codecList[index];
				DELETEO(enc);
			}

			m_codecList.clear();
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool VideoMixer::addMember(Termination* term, Tube* in_tube, Tube* out_tube)
	{
		mg_video_conference_in_tube_t* c_in_tube = (mg_video_conference_in_tube_t*)in_tube;
		mg_video_conference_out_tube_t* c_out_tube = (mg_video_conference_out_tube_t*)out_tube;

		uint32_t termId = term->getId();
		uint32_t videoId = term->getVideoTermId();

		VideoMixerMember* member = findById(termId);

		if (member == nullptr)
		{
			member = NEW VideoMixerMember;
			rtl::MutexLock lock(m_memberListSync);
			m_memberList.add(member);
		}

		member->termId = termId;
		member->videoId = videoId;
		member->inputTube = c_in_tube;
		member->outputTube = c_out_tube;

		member->bindId = m_frame.bindVideoStream(termId, videoId);

		{
			rtl::MutexLock lock(m_memberListSync);

			attachToCodec(member);

			if (c_in_tube && member->bindId != media::VideoFrameStream::EmptyId)
			{
				c_in_tube->setConferenceBindId(this, member->bindId);
			}
		}

		return true;

	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoMixer::removeMember(uint32_t videoTermId)
	{
		ENTER_FMT("termId : %zu", videoTermId);

		rtl::MutexLock lock(m_memberListSync);

		for (int i = 0; i < m_memberList.getCount(); i++)
		{
			VideoMixerMember* member = m_memberList[i];
			if (member->termId == videoTermId)
			{
				PRINT("Start reassemble!");

				mg_video_conference_out_tube_t* c_out_tube = (mg_video_conference_out_tube_t*)member->outputTube;
				m_memberList.removeAt(i);
				detachFromEncoder(c_out_tube);
				m_frame.unbindVideoStream(videoTermId);
				break;
			}
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoMixer::attachMember(uint32_t videoTermId)
	{
		ENTER_FMT("termId : %zu", videoTermId);

		rtl::MutexLock lock(m_memberListSync);

		for (int i = 0; i < m_memberList.getCount(); i++)
		{
			if (m_memberList[i]->termId == videoTermId)
			{
				return;
			}
		}

		//addNewMember(videoTermId, nullptr, nullptr);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoMixer::attachToCodec(VideoMixerMember* member)
	{
		mg_video_conference_out_tube_t* outTube = (mg_video_conference_out_tube_t*)member->outputTube;

		if (outTube != nullptr)
		{

			for (int i = 0; i < m_codecList.getCount(); i++)
			{
				VideoMixerEncoder* enc = m_codecList[i];
				if (enc->getFormat() == outTube->getFormat())
				{
					enc->addMember(outTube);
					return;
				}
			}

			VideoMixerEncoder* enc = NEW VideoMixerEncoder(m_log, outTube->getFormat());
			m_codecList.add(enc);
			enc->addMember(outTube);
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoMixer::detachFromEncoder(mg_video_conference_out_tube_t* outTube)
	{
		if (outTube == nullptr)
		{
			for (int i = 0; i < m_codecList.getCount(); i++)
			{
				VideoMixerEncoder* enc = m_codecList[i];
				if (enc->getFormat() == outTube->getFormat())
				{
					enc->removeMember(outTube);
					if (enc->getMemberCount() == 0)
					{
						enc->destroy();
						m_codecList.removeAt(i);
						DELETEO(enc);
					}
					return;
				}
			}
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VideoMixerMember* VideoMixer::findById(uintptr_t termId)
	{
		for (int i = 0; i < m_memberList.getCount(); i++)
		{
			if (m_memberList[i]->termId == termId)
				return m_memberList[i];
		}

		return nullptr;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoMixer::videoSelector__priorityChanged(uint32_t termId, bool vadDetected)
	{
		VideoMixerMember* member = findById(termId);
		if (member)
		{
			m_frame.updateVideoStreamVAD(member->bindId, vadDetected);
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoMixer::timer_elapsed_event(rtl::Timer* sender, int tid)
	{
		uint32_t currentTS = rtl::DateTime::getTicks();

		rtl::MutexLock lock(m_memberListSync);

		const media::Bitmap* bmp = m_frame.getPicture();

		if (bmp != nullptr && bmp->getWidth() > 0)
		{
			PRINT("send outgoing picture");

			media::VideoImage* outgoingImage = NEW media::VideoImage();
			outgoingImage->importARGB(*bmp);

			m_encoderQueue.push(messageQueueHandler, this, 0, 0, outgoingImage);
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoMixer::messageQueueHandler(void* userData, uint16_t messageId, uintptr_t intParam, void* ptrParam)
	{
		VideoMixer* mixer = (VideoMixer*)userData;
		media::VideoImage* image = (media::VideoImage*)ptrParam;

		mixer->encodeQueue(image);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoMixer::encodeQueue(media::VideoImage* image)
	{
		// drawing image on outgoing canvas.
		rtl::MutexLock lock(m_memberListSync);

		for (int i = 0; i < m_codecList.getCount(); i++)
		{
			m_codecList[i]->updateImage(image);
		}

		DELETEO(image);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoMixer::updateVideoStream(uint64_t bindId, const media::VideoImage* image)
	{
		m_frame.updateVideoStream(bindId, image);
	}
}
