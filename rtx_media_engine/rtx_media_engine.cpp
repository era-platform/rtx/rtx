﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtx_media_engine.h"
#include "rtx_codec.h"
#include "mg_tube_manager.h"
#include "mg_session.h"
#include "mg_fax.h"
#include "mg_novoice_timer_manager.h"
#include "rtp_statistics.h"
#include "mg.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#if defined (DEBUG_MEMORY)
void* operator new (size_t size, char* file, int line)
{
	return std_mem_checker_malloc(size, file, line, rc_malloc_new_e);
}
void* operator new[](size_t size, char* file, int line)
{
	return std_mem_checker_malloc(size, file, line, rc_malloc_newar_e);
}
void operator delete (void* mem_ptr)
{
	std_mem_checker_free(mem_ptr, rc_malloc_delete_e);
}
void operator delete[](void* mem_ptr)
{
	std_mem_checker_free(mem_ptr, rc_malloc_deletear_e);
}
#endif
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_MGE_API uint32_t me_get_version()
{
	return (API_VERSION_MAJOR << 24) | (API_VERSION_MINOR << 16) | (API_VERSION_TEST << 8) | API_VERSION_BUILD;
}
//------------------------------------------------------------------------------
DECLARE rtl::Logger* g_mg_Log = nullptr;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
DECLARE int g_mge_context_counter = -1;
DECLARE int g_mge_termination_counter = -1;
DECLARE int g_mge_processor_counter = -1;
DECLARE int g_mge_direction_counter = -1;
DECLARE int g_mge_stream_counter = -1;
DECLARE int g_mge_tube_counter = -1;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_MGE_API h248::IMediaGate* create_media_gateway_module(const char* log_path)
{
	g_mge_context_counter = rtl::res_counter_t::add("mge_context");
	g_mge_termination_counter = rtl::res_counter_t::add("mge_termination");
	g_mge_processor_counter = rtl::res_counter_t::add("mge_processor");
	g_mge_direction_counter = rtl::res_counter_t::add("mge_direction");
	g_mge_stream_counter = rtl::res_counter_t::add("mge_stream");
	g_mge_tube_counter = rtl::res_counter_t::add("mge_tube");

	if (g_mg_Log == nullptr)
	{
		g_mg_Log = NEW rtl::Logger();
		g_mg_Log->create(log_path, "mg_module", LOG_APPEND);
	}

	if (mge::g_mg_tube_log == nullptr)
	{
		if (rtl::Logger::check_trace(TRF_FLAG6))
		{
			mge::g_mg_tube_log = NEW rtl::Logger();
			mge::g_mg_tube_log->create(log_path, "mg_tubes", LOG_APPEND);
		}
	}

	if (mge::g_mg_stream_log == nullptr)
	{
		if (rtl::Logger::check_trace(TRF_STREAM))
		{
			mge::g_mg_stream_log = NEW rtl::Logger();
			mge::g_mg_stream_log->create(log_path, "mg_streams", LOG_APPEND);
		}
	}

	if (g_mg_fax_log == nullptr)
	{
		if (rtl::Logger::check_trace(TRF_FAX))
		{
			g_mg_fax_log = NEW rtl::Logger();
			g_mg_fax_log->create(log_path, "mg_fax", LOG_APPEND);
		}
	}

	if (mge::g_mg_statistics_log == nullptr)
	{
		if (rtl::Logger::check_trace(TRF_STAT))
		{
			mge::g_mg_statistics_log = NEW rtl::Logger();
			mge::g_mg_statistics_log->create(log_path, "mg_rtp_stat", LOG_APPEND);
		}
	}
	
	if (mge::g_media_gateway == nullptr)
	{
		mge::g_media_gateway = NEW mge::media_gateway_t(g_mg_Log);
	}

	mge::TubeManager::setLogger(mge::g_mg_tube_log);
	if (mge::g_mg_tube_log != nullptr)
	{
		mge::g_mg_tube_log->log("MGTM", "tube manager created.");
	}

	if (mge::g_mg_novoice_timer_manager == nullptr)
	{
		mge::g_mg_novoice_timer_manager = NEW mge::mg_novoice_timer_manager_t();
	}

	int codec_lib_count = 0;
	const char* codec_lib_count_char = Config.get_config_value("codec-lib-count");
	if ((codec_lib_count_char != nullptr) && (codec_lib_count_char[0] != 0))
	{
		codec_lib_count = strtoul(codec_lib_count_char, nullptr, 10);
	}

	if (codec_lib_count > 0)
	{
		char cfg_path[128];
		for (int i = 1; i <= codec_lib_count; i++)
		{
			int len = std_snprintf(cfg_path, 128, "codec-lib-%u", i);
			cfg_path[len] = 0;
			const char* cfg_path_value = Config.get_config_value(cfg_path);
			if ((cfg_path_value != nullptr) && (cfg_path_value[0] != 0))
			{
				if (!rtx_codec__load_audio_codec(cfg_path_value))
				{
					g_mg_Log->log_error("MG", "rtx_codec__load_codecs FAIL.");
					DELETEO(mge::g_media_gateway);
					mge::g_media_gateway = nullptr;

					g_mg_Log->destroy();
					DELETEO(g_mg_Log);
					g_mg_Log = nullptr;

					break;
				}
			}
		}
	}
	else
	{
		const char* path = Config.get_config_value("work-folder");
		const char* codecs = Config.get_config_value("codecs");

		g_mg_Log->log("MG", "loading codecs with params: path:%s codecs:%s", path, codecs);

		if (!rtx_codec__load_all_codecs(path, codecs))
		{
			g_mg_Log->log_error("MG", "rtx_codec__load_all FAIL.");
			DELETEO(mge::g_media_gateway);
			mge::g_media_gateway = nullptr;

			g_mg_Log->destroy();
			DELETEO(g_mg_Log);
			g_mg_Log = nullptr;
		}
	}

	const char* vrec = Config.get_config_value("enable-video-recording");

	if (vrec != nullptr)
	{
		if (std_stricmp(vrec, "true") == 0 || vrec[0] == '1')
		{
			mge::Termination::enableVideoRecording(true);
		}
	}

	return mge::g_media_gateway;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void remove_media_gateway()
{
	if (mge::g_media_gateway != nullptr)
	{
		mge::g_media_gateway->MediaGate_destroy();
		DELETEO(mge::g_media_gateway);
		mge::g_media_gateway = nullptr;
	}

	if ((mge::g_mg_tube_log != nullptr) && (rtl::Logger::check_trace(TRF_FLAG6)))
	{
		mge::g_mg_tube_log->log("MGTM", "tube manager destroyed.");
	}

	if (mge::g_mg_tube_log != nullptr)
	{
		mge::g_mg_tube_log->destroy();
		DELETEO(mge::g_mg_tube_log);
		mge::g_mg_tube_log = nullptr;
	}

	if (mge::g_mg_stream_log != nullptr)
	{
		mge::g_mg_stream_log->destroy();
		DELETEO(mge::g_mg_stream_log);
		mge::g_mg_stream_log = nullptr;
	}

	if (mge::g_mg_statistics_log != nullptr)
	{
		mge::g_mg_statistics_log->destroy();
		DELETEO(mge::g_mg_statistics_log);
		mge::g_mg_statistics_log = nullptr;
	}

	if (g_mg_fax_log != nullptr)
	{
		g_mg_fax_log->destroy();
		DELETEO(g_mg_fax_log);
		g_mg_fax_log = nullptr;
	}

	if (g_mg_Log != nullptr)
	{
		g_mg_Log->destroy();
		DELETEO(g_mg_Log);
		g_mg_Log = nullptr;
	}

	if (mge::g_mg_novoice_timer_manager != nullptr)
	{
		DELETEO(mge::g_mg_novoice_timer_manager);
		mge::g_mg_novoice_timer_manager = nullptr;
	}

	rtx_codec__unload_codecs();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
inline const char* get_path_by_key(const char* key) { return Config.get_config_value(key); }
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool extract_path(const rtl::String& rel_path, rtl::String& key, rtl::String& path)
{
	int index;

	if (rel_path[0] != ':')
		return false;

	if ((index = rel_path.indexOfAny("/\\", 1)) == -1)
		return false;

	key = rel_path.substring(1, index - 1);
	path = rel_path.substring(index + 1);

	return !key.isEmpty() && !path.isEmpty();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtl::String get_local_path(const char* rel_path)
{
	rtl::String rpath = rel_path;

	rtl::String key;
	rtl::String path;

	if (!extract_path(rpath, key, path))
		return rtl::String::empty;

	rtl::String local_path = get_path_by_key(key);

	local_path << FS_PATH_DELIMITER << path;

#if defined (TARGET_OS_WINDOWS)
	rpath = local_path.replace('/', FS_PATH_DELIMITER);

	rpath.replace("\\\\", "\\");
#else
	rpath = local_path.replace('\\', FS_PATH_DELIMITER);

	rpath.replace("//", "/");
#endif

	return rpath;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
