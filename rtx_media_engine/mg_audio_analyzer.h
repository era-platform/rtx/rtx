/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_tube.h"
#include "mmt_vad.h"

#define TRACE_VAD 0

namespace mge
{
	//-----------------------------------------------
	// Audio Analyzers events handler interface
	//-----------------------------------------------
	class VadDetector
	{
	public:
		virtual ~VadDetector();
		virtual void VadDetector__audioSignalChanged(uint32_t termId, bool VAD, int level) = 0;
	};
	//-----------------------------------------------
	// audio analizer parameters
	//-----------------------------------------------
	struct AudioAnalyzerParams
	{
		int threshold;
		int vadDuration;
		int silenceDuration;

		AudioAnalyzerParams() : threshold(40), vadDuration(40), silenceDuration(500) { }

		void reset() { threshold = 40; vadDuration = 40; silenceDuration = 500; }
	};
	//-----------------------------------------------
	// Audio Analyzer -- container for VAD detecters (one for term)
	//-----------------------------------------------
	class AudioAnalyzer : public INextDataHandler
	{
	private:
		struct AudioDetector { uintptr_t termId; media::VAD_Detecter* detector; uint32_t lastTs; bool lastTsInited; };
		rtl::ArrayT<AudioDetector> m_detectorList;
		VadDetector* m_handler;
		rtl::Mutex m_sync;
		AudioAnalyzerParams m_params;
		uint32_t m_id;

		rtl::Logger* m_log;

		static volatile uint32_t s_idGen;

		#if TRACE_VAD
		static rtl::Logger s_log;
		static bool s_logInitialized;
		#endif

	private:
		AudioDetector* getDetector(uintptr_t termId);

	public:
		AudioAnalyzer(rtl::Logger* log);
		virtual ~AudioAnalyzer();

		bool initialize(VadDetector* eventHandler, const AudioAnalyzerParams& params);
		void cleanup();

		virtual void send(const INextDataHandler* sender, const rtp_packet* packet);
		virtual void send(const INextDataHandler* sender, const uint8_t* data, uint32_t len);
		virtual void send(const INextDataHandler* sender, const rtcp_packet_t* packet);
		virtual bool unsubscribe(const INextDataHandler* sender);

		void updateParams(const AudioAnalyzerParams& params);
	};
}
//-----------------------------------------------
