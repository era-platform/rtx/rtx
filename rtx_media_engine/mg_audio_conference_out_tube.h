﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_tube.h"
#include "mg_audio_conference_in_tube.h"
#include "mg_audio_mixer.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	class mg_audio_conference_out_tube_t;
	class mg_conference_t;
	//------------------------------------------------------------------------------
	// Элемент termination'а в коференции.
	//------------------------------------------------------------------------------
	class mg_conference_member_t
	{
		mg_audio_conference_in_tube_t* in_tube;
		mg_audio_conference_out_tube_t* out_tube;
		//mg_conference_t* conf;
		volatile bool active;
		volatile bool in_work;
		// счетчик не принятых данных из in туба.
		uint32_t recive_error_count;
		// счетчик не отправленных данных в out туб.
		uint32_t transmit_error_count;
		mg_conference_member_t* next;

	public:
		mg_conference_member_t();
		~mg_conference_member_t();

		mg_conference_t* getConf();
		mg_audio_conference_in_tube_t* getTubeIn() { return in_tube; }
		void setTubeIn(mg_audio_conference_in_tube_t* tube) { in_tube = tube; }
		mg_audio_conference_out_tube_t* getTubeOut() { return out_tube; }
		void setTubeOut(mg_audio_conference_out_tube_t* tube) { out_tube = tube; }
		const char* get_termination_name();

		void setWork(bool value = true) { in_work = value; }
		bool isActive() { return active; }
		bool inWork() { return in_work; }
		void activate() { active = true; }
		void deactivate() { active = false; }

		void detachTubeIn() { in_tube = nullptr; }

		void addReceiveError()
		{
			recive_error_count++;
			if (recive_error_count >= 20 && transmit_error_count >= 20)
			{
				active = false;
			}
		}
		void addTransmissionError() { transmit_error_count++; }
		void resetErrors() { recive_error_count = 0; transmit_error_count = 0; }

		mg_conference_member_t* getNext() { return next; }
		void setNext(mg_conference_member_t* n) { next = n; }
	};
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	class mg_conference_t : rtl::async_call_target_t
	{
	public:
		// Максимально возможное количество семплов.
		static const int MaxFrameSize = 30 * 32000 / 1000;

		mg_conference_t(rtl::Logger* log, mg_audio_conference_out_tube_t* owner);
		virtual	~mg_conference_t();

		void initialize(int samplesInFrame, bool hard);

		void update_frame_size(uint32_t mixer_frame_size);

		// Сюда приходят и микшированные данные и список всех участников конференции.
		// Второе чтобы вырезать голоса ненужные по топологии.
		bool send_mixing_data(AudioMixerMember* members, int members_count, const double* mixed_voice, int mixed_count);

		// Добавление учатсника конференции в черный список. (Которого не будет слышно в рамках текущего туба).
		void add_except_termination_id(const char* termination_id);
		void remove_except_termination_id(const char* termination_id);

		int readData(uint16_t* biffer, int bufferSize);

		const char* getName();
		const char* getLogTag();

	private:
		// Проверка в списке исключений.
		bool is_permissible_source(mg_conference_member_t* source);

		// Упаковка данных в пакет и отправка дальше.
		//void packetize();

		virtual const char* async_get_name();
		virtual void async_handle_call(uint16_t call_id, uintptr_t int_param, void* ptr_param);

		bool wait_mixer_tread();

	private:
		mg_audio_conference_out_tube_t* m_owner;
		rtl::Logger* m_log;
		// К какому терминатору подключен текущий туб.
		rtl::String m_termination_id;

		// Количество семплов с которым работает микшер.
		uint32_t m_mixer_samples_count;
		// Сюда микшируются данные с учетом списка исключений.
		double m_mixer_buffer[MaxFrameSize];
		// Сюда пишутся выровненые данные после микшера.
		uint16_t m_cvt_samples[MaxFrameSize];
		// Сюда уже пишутся данные на отправку в сеть.
		// Фрилок потому что может запускаться асинхронный вызов и 
		// данные будут обрабатыватся другим потоком.
		mg_free_lock_memory_queue_t m_freelock_buffer;

		// Количество семплов для исходящего траффика.
		uint32_t m_samples_count;

		// Флаг указывает что туб не легкий в плане кодировки.
		// И желательно запускать отдельный поток.
		bool m_hard;

		// Список участников коференции которых не должен слышать текущий.
		rtl::StringList m_except_list;
		// флаг указывает что поток микшера работает с черным списком.
		volatile bool m_is_permissible;
	};
	//------------------------------------------------------------------------------
	// Труба исходящих данных от микшера конференции.
	// Поток микшера приходит в туб, пишет микшированные данные в буфер.
	// Микширует данные еще раз (убирает голос текущего termination'a и 
	// голоса termination'ов ненужные по топологии).
	// Если следующий туб транскодига "тяжелый" то вызывает асинхронный вызов и уходит.
	// В обратном случае сам передает данные дальше.
	//------------------------------------------------------------------------------
	class mg_audio_conference_out_tube_t : public Tube
	{
	public:
		mg_audio_conference_out_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id);
		virtual	~mg_audio_conference_out_tube_t();

		virtual bool init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out) override;
		virtual bool is_suited(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out) override;

		void set_ssrc(uint32_t ssrc) { m_ssrc_to_rtp = ssrc; }
		// помечаем к на какой терминэйшн подписан туб.
		void set_termination_name(const char* termination_id) { m_termination_id = termination_id; }
		const char* get_termination_name() { return m_termination_id; }
		void update_frame_size(int frameSize) { m_conf.update_frame_size(frameSize); }
		void add_except_termination_id(const char* termination_id) { m_conf.add_except_termination_id(termination_id); }
		void remove_except_termination_id(const char* termination_id) { m_conf.remove_except_termination_id(termination_id); }
		mg_conference_t* get_conference() { return &m_conf; }

		void changeTopologyFrom(const char* termId, h248::TopologyDirection topology);
		void changeTopologyTo(const char* termId, h248::TopologyDirection topology);

	private:
		friend class mg_conference_t;
		// Упаковка данных в пакет и отправка дальше.
		void packetize();

	private:
		mg_conference_t m_conf;
		// К какому терминатору подключен текущий туб.
		rtl::String m_termination_id;

		//-----------------------------------------------------
		// Переменные для формирования ртп пакета.
		uint32_t m_ssrc_to_rtp;
		uint16_t m_seq_num_to_rtp;			// порядковый номер пакета.
		uint16_t m_seq_num_to_rtp_last;		// номер последнего отправленного пакета.
		uint32_t m_timestamp_to_rtp;		// таймштамп пакета.
		uint32_t m_timestamp_to_rtp_last;	// таймштамп последнего отправленного пакета.
	};
}
//------------------------------------------------------------------------------
