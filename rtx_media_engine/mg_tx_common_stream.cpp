﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_thread.h"
#include "mg_tx_common_stream.h"
#include "std_crypto.h"
#include "mg_session.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "logdef_stream.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_tx_common_stream_t::mg_tx_common_stream_t(rtl::Logger* log, uint32_t id, mg_tx_stream_type_t type, uint32_t parent_id, const char* term_id) :
		mg_tx_stream_t(log, id, type, term_id)
	{
		m_rtp = nullptr;
		m_ssrc_to_rtp = (rtl::DateTime::getTicks() + rand()) | 0x833000b0;
		m_seq_num_to_rtp = (uint16_t)(rtl::DateTime::getTicks() ^ 0x000000D5D);
		m_seq_num_to_rtp_last = 0;
		m_timestamp_to_rtp = 0;
		m_timestamp_to_rtp_last = 0;

		rtl::String name;
		name << term_id << "-s" << parent_id << "-txcs" << id;
		mg_tx_stream_t::setName(name);
		mg_tx_stream_t::setTag("TXS_RTP");

		m_first_send = true;
		m_new_connect = true;

		m_recalc_packet_to_rtp = true;
		m_last_packet_cn = false;

		m_last_tick = 0;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_tx_common_stream_t::~mg_tx_common_stream_t()
	{
		if (m_rtp != nullptr)
		{
			m_rtp->dec_tx_subscriber(0);
		}
		m_rtp = nullptr;

		m_seq_num_to_rtp_last = 0;
		m_timestamp_to_rtp_last = 0;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_tx_common_stream_t::mg_stream_set_LocalControl_Property(const h248::Field* LC_property)
	{
		ENTER();

		if (!isLocked())
		{
			WARNING("stream unlocked!");
			return false;
		}

		if (LC_property == nullptr)
		{
			WARNING("Wrong param LC_property!");
			return false;
		}

		rtl::String name(LC_property->getName());
		rtl::String value(LC_property->getValue());
		
		PRINT_FMT("name:%s value:%s", (const char*)name, (const char*)value);

		if (name.indexOf("RECALC_PACKET") != BAD_INDEX)
		{
			value.toUpper();
			bool recalc = value.indexOf("TRUE") != BAD_INDEX;
			change_flag_recalc_packet(recalc);
		}

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_tx_common_stream_t::send(const INextDataHandler* out_handler, const rtp_packet* packet)
	{

		if (checkAndLock())
		{
			stat_filtered();
			WARNING("stream locked!");
			return;
		}

		send_packet_to_rtp_lock(out_handler, packet);

		releaseLock();
	}
	//------------------------------------------------------------------------------
	void mg_tx_common_stream_t::send(const INextDataHandler* out_handler, const uint8_t* data, uint32_t len)
	{
		if (checkAndLock())
		{
			WARNING("stream locked!");
			return;
		}

		send_data_to_rtp_lock(out_handler, data, len);

		releaseLock();
	}
	//------------------------------------------------------------------------------
	void mg_tx_common_stream_t::send(const INextDataHandler* out_handler, const rtcp_packet_t* packet)
	{
		if (checkAndLock())
		{
			WARNING("stream locked!");
			return;
		}

		send_ctrl_to_rtp_lock(out_handler, packet);

		releaseLock();
	}
	//------------------------------------------------------------------------------
	void mg_tx_common_stream_t::send_packet_to_rtp_lock(const INextDataHandler* out_handler, const rtp_packet* packet)
	{
		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::Loopback)
		{
			stat_invalid();
			WARNING("stream mode is loopback!");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::Inactive)
		{
			stat_invalid();
			WARNING("stream mode is inactive!");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::RecvOnly)
		{
			stat_invalid();
			WARNING("stream mode is recvonly!");
			return;
		}

		if (packet == nullptr)
		{
			ERROR_MSG("packet is null!");
			return;
		}

		if (m_rtp == nullptr)
		{
			stat_invalid();
			ERROR_MSG("rtp handler is null!");
			return;
		}

		rtp_packet* pckt = (rtp_packet*)packet;
		pckt->set_ssrc(m_ssrc_to_rtp);

		if (!recalc_packet(pckt))
		{
			ERROR_MSG("packet recalculating failed!");
			return;
		}

		m_rtp->tx_packet_channel(pckt, &m_remote_address);

		m_last_tick = rtl::DateTime::getTicks();

		stat_analize(pckt);
		rtcp_analize(pckt);
	}
	//------------------------------------------------------------------------------
	void mg_tx_common_stream_t::send_data_to_rtp_lock(const INextDataHandler* out_handler, const uint8_t* data, uint32_t len)
	{
		if (data == nullptr)
		{
			ERROR_MSG("wrong parameers: data is null.");
			return;
		}


		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::Loopback)
		{
			WARNING("stream mode is loopback!");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::Inactive)
		{
			WARNING("stream mode is inactive.");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::RecvOnly)
		{
			WARNING("stream mode is recvonly!");
			return;
		}

		if (m_rtp == nullptr)
		{
			ERROR_MSG("rtp handler is null.");
			return;
		}

		m_rtp->tx_data_channel(data, len, &m_remote_address);
	}
	//------------------------------------------------------------------------------
	void mg_tx_common_stream_t::send_ctrl_to_rtp_lock(const INextDataHandler* out_handler, const rtcp_packet_t* packet)
	{
		if (packet == nullptr)
		{
			ERROR_MSG("wrong parameter: packet is null.");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::Loopback)
		{
			WARNING("stream mode is loopback!");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::Inactive)
		{
			WARNING("stream mode is inactive!");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::RecvOnly)
		{
			WARNING("stream mode is recvonly.");
			return;
		}

		if (m_rtp == nullptr)
		{
			ERROR_MSG("rtp handler is null.");
			return;
		}

		rtcp_packet_t new_packet;
		new_packet.copy(packet);

		if (rtcp_analize(&new_packet))
		{
			m_rtp->tx_ctrl_channel(&new_packet, &m_remote_rtcp_address);
		}
		else
		{
			m_rtp->tx_ctrl_channel(packet, &m_remote_rtcp_address);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	int mg_tx_common_stream_t::rtcp_session_tx(const rtcp_packet_t* packet)
	{
		if (m_rtp != nullptr)
		{
			m_rtp->tx_ctrl_channel(packet, &m_remote_rtcp_address);
		}

		return 1;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------	
	void mg_tx_common_stream_t::set_channel_event(i_tx_channel_event_handler* rtp)
	{
		if (!isLocked())
		{
			WARNING("stream unlocked!");
			return;
		}

		if (rtp == nullptr)
		{
			if (m_rtp != nullptr)
			{
				m_rtp->dec_tx_subscriber(m_ssrc_to_rtp);
			}
		}
		else
		{
			if (m_rtp == nullptr)
			{
				rtp->inc_tx_subscriber(m_ssrc_to_rtp);
			}
			else
			{
				if (m_rtp != rtp)
				{
					m_rtp->dec_tx_subscriber(m_ssrc_to_rtp);
					rtp->inc_tx_subscriber(m_ssrc_to_rtp);
				}
			}
		}
		m_rtp = rtp;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_tx_common_stream_t::reset_before_new_connect()
	{
		m_new_connect = true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_tx_common_stream_t::change_flag_recalc_packet(bool recalc_pckt)
	{
		if (isLocked())
		{
			m_recalc_packet_to_rtp = recalc_pckt;
		}
		else
		{
			WARNING("stream unlocked!");
		}
	}
	//------------------------------------------------------------------------------
	bool mg_tx_common_stream_t::recalc_packet(rtp_packet* packet)
	{
		if (!m_recalc_packet_to_rtp)
		{
			return true;
		}

		int count_packet = recalc_seq_num(packet);
		if (count_packet <= 0)
		{
			stat_invalid();
			// значит повторные пакеты пришли
			return false;
		}

		if (!recalc_timestamp(count_packet, packet))
		{
			stat_invalid();
			return false;
		}

		if (packet->get_marker())
		{
			packet->set_marker(false);
		}
		if (m_first_send)
		{
			packet->set_marker(true);
			m_first_send = false;
		}
		if (packet->get_payload_type() == 13)
		{
			m_last_packet_cn = true;
		}
		else if (m_last_packet_cn)
		{
			packet->set_marker(true);
			m_last_packet_cn = false;
		}

		return true;
	}
	//------------------------------------------------------------------------------
	int mg_tx_common_stream_t::recalc_seq_num(rtp_packet* packet)
	{
		int16_t diff = calc_diff(packet);
		if (diff > 0)
		{
			m_seq_num_to_rtp += diff;

			packet->set_sequence_number(m_seq_num_to_rtp);

			return diff;
		}

		return -1;
	}
	//------------------------------------------------------------------------------
	int	mg_tx_common_stream_t::calc_diff(rtp_packet* packet)
	{
		uint16_t seq = packet->get_sequence_number();

		if ((m_first_send) || (m_new_connect) || (packet->get_marker()))
		{
			m_new_connect = false;
			m_seq_num_to_rtp_last = seq;

			if (m_last_packet_cn)
			{
				uint64_t tick = rtl::DateTime::getTicks();
				uint64_t r = (tick - m_last_tick);
				if (r <= 10)
				{
					return -1;
				}
			}
			return 1;
		}

		rtp_packet_priv_type type = packet->get_priv_type();
		if (type == rtp_packet_priv_type::simple)
		{
			int diff = seq - m_seq_num_to_rtp_last;
			m_seq_num_to_rtp_last = seq;
			return diff;
		}
		else if (type == rtp_packet_priv_type::generate)
		{
			return 1;
		}
		else if (type == rtp_packet_priv_type::partial)
		{
			uint8_t i = packet->get_priv_partial_count();
			m_seq_num_to_rtp_last = seq + i;
			return i + 1;
		}

		return -1;
	}
	//------------------------------------------------------------------------------
	bool mg_tx_common_stream_t::recalc_timestamp(uint32_t count_packet, rtp_packet* packet)
	{
		//PLOG_STREAM_WRITE(LOG_PREFIX, "recalc_seq_num -- ID(%s): timestamp last:%u.", getName(), m_timestamp_to_rtp_last);

		uint8_t pt = packet->get_payload_type();

		if (m_first_send)
		{
			m_timestamp_to_rtp = rtl::DateTime::getTicks() & 0x005D5D5D;
			m_in_timestamp_last = packet->getTimestamp();
		}
		else if (packet->getTimestamp() == m_in_timestamp_last)
		{
			m_timestamp_to_rtp = m_timestamp_to_rtp_last;
		}
		else
		{
			uint32_t samples = (m_new_connect) ? m_timestamp_to_rtp_last : packet->get_samples();
			if (samples == 0)
			{
				samples = m_timestamp_to_rtp_last;
			}
			int16_t step = samples * count_packet;

			m_timestamp_to_rtp = m_timestamp_to_rtp_last + step;
			m_samples_to_rtp_last = samples;

			m_in_timestamp_last = packet->getTimestamp();
		}

		//PLOG_STREAM_WRITE(LOG_PREFIX, "recalc_seq_num -- ID(%s): set timestamp:%u ... %u", getName(), m_timestamp_to_rtp, pt);

		packet->set_timestamp(m_timestamp_to_rtp);
		m_timestamp_to_rtp_last = m_timestamp_to_rtp;

		return true;
	}
}
//------------------------------------------------------------------------------
