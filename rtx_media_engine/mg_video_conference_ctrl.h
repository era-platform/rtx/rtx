/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <atomic>
#include "mg_video_mixer.h"
#include "mg_video_selector.h"
#include "mg_termination.h"
//-----------------------------------------------
//
//-----------------------------------------------

namespace mge
{
	class VideoConferenceController
	{
	private:
		rtl::String m_name;
		rtl::Logger* m_log;

		VideoMixer* m_mixer;
		VideoSelector* m_selector;
		INextDataHandler* m_recorder;

	public:
		VideoConferenceController(rtl::Logger* log, const char* parentId);
		~VideoConferenceController();

		bool create(const char* schema, VideoSelector* selector);
		void destroy();
		void updateSchema(const char* jsonSchema);

		bool setRecorderHandler(INextDataHandler* outHandler);
		bool addConfMember(Termination* term, Tube* inTube, Tube* outTube);
		void removeConfMember(uint32_t termId);
		void attachConfMember(uint32_t termId);
	};
}
//-----------------------------------------------
