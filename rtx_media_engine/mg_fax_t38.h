﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "mg_fax.h"
//------------------------------------------------------------------------------
// константы T38
//------------------------------------------------------------------------------
enum mg_fax_t38_rate_management_t
{
	T38_TCF_NONE,
	T38_TCF_LOCAL,
	T38_TCF_TRANSFERRED,
};
//------------------------------------------------------------------------------
// параметры факса T38
//------------------------------------------------------------------------------
struct mg_fax_t38_params_t
{
	int						version;
	int						max_bit_rate;
	bool					fill_bit_removal;
	bool					transcoding_mmr;
	bool					transcoding_jbig;
	mg_fax_t38_rate_management_t rate_management;
	int						max_buffer;
	int						max_datagram;
	int						udp_ec; // transport error correction
	rtl::String				vendor_info;
	int						ecm_entries;
	int						ecm_span;
};
//--------------------------------------
//
//--------------------------------------
struct udptl_rx_frame_t
{
	int seq_no;
	const uint8_t* ifp;
	int ifp_length;
};
//--------------------------------------
//
//--------------------------------------
#define UDPTL_ECM_BUFFER_SIZE 16
#define UDPTL_FEC_ENTRIES 5
#define UDPTL_PACKET_SIZE 1400
//--------------------------------------
//
//--------------------------------------
struct udptl_rx_frame_fec_t
{
	int ifp_length;
	uint8_t ifp[UDPTL_PACKET_SIZE];
	int fec_length[UDPTL_FEC_ENTRIES];
	uint8_t fec[UDPTL_FEC_ENTRIES][UDPTL_PACKET_SIZE];
	int fec_span;
	int fec_entries;
};

//------------------------------------------------------------------------------
void mg_fax_t38_set_default_params(mg_fax_t38_params_t* params);
//------------------------------------------------------------------------------
// file_mask - Путь до файла. (все каталоги должны быть созданы, создается только сам фаил .tiff)
//------------------------------------------------------------------------------
bool mg_initialize_fax_receiver_t38(mg_fax_t* fax, const char* file_mask);
//------------------------------------------------------------------------------
// file_name - Путь до айла .tiff
// ecm - error correction mode
//------------------------------------------------------------------------------
bool mg_initialize_fax_transmitter_t38(mg_fax_t* fax, const char* file_name, bool ecm);
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class mg_fax_t38_t : public mg_fax_t
{
	mg_fax_t38_t(const mg_fax_t38_t&);
	mg_fax_t38_t& operator=(const mg_fax_t38_t&);

public:
	mg_fax_t38_t(rtl::Logger* log);
	~mg_fax_t38_t();

	virtual void destroy();

	void set_type(bool receiver);

	void phase_b_rx(t30_state_t *s, int result);
	void phase_d_rx(t30_state_t *s, int result);
	void phase_e_rx(t30_state_t *s, int completion_code);

	void phase_b_tx(t30_state_t *s, int result);
	void phase_d_tx(t30_state_t *s, int result);
	void phase_e_tx(t30_state_t *s, int completion_code);

	virtual uint32_t write_fax(const uint8_t* data, uint32_t length);
	virtual uint32_t read_fax(uint8_t* data, uint32_t length);

	void set_parameters(mg_fax_t38_params_t* params);
	void get_parameters(mg_fax_t38_params_t* params);

	t38_terminal_state_t* get_fax();
	void set_fax(t38_terminal_state_t* t38);

	//------------------------------------------------------------------------------
	// << mg_fax_transmitter_packet_handler_t38 >>
	int t38_packet_tx(t38_core_state_t *s, const uint8_t *buf, int len, int transmissions);
	//------------------------------------------------------------------------------
private:
	rtl::Logger* m_log;

	bool m_receiver;

	t38_terminal_state_t*	m_t38;
	mg_fax_t38_params_t		m_params;

	static const uint32_t	m_buff_size = 321;
	uint8_t					m_buff[m_buff_size];
	int						m_buff_write;
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
