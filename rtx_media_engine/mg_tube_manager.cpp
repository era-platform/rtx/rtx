﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_tube_manager.h"
#include "mg_media_element.h"
#include "mg_cn_tube.h"
#include "mg_simple_tube.h"
#include "mg_codec_to_pcm_tube.h"
#include "mg_pcm_to_codec_tube.h"
#include "mg_pcm_ptime_tube.h"
#include "mg_resampler_tube.h"
#include "mg_codec_to_img_tube.h"
#include "mg_img_to_codec_tube.h"
#include "mg_transformer_tube.h"
#include "mg_stereo_to_mono_tube.h"
#include "mg_mono_to_stereo_tube.h"
#include "mg_rfc2833_tube.h"
#include "mg_direction.h"
#include "mg_rx_stream.h"
#include "mg_audio_conference_in_tube.h"
#include "mg_audio_conference_out_tube.h"
#include "mg_video_conference_in_tube.h"
#include "mg_video_conference_out_tube.h"
#include "mg_conference_recorder_tube.h"
#include "mg_udptl_rx_tube.h"
#include "mg_udptl_tx_tube.h"
#include "mg_direction_bothway.h"
#include "mg_direction_audio_conference.h"
#include "mg_splitter_tube.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

#include "logdef_tube.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	struct mg_element_chain_tube
	{
		HandlerType h_type;
		mg_media_element_t* element_in;
		mg_media_element_t* element_out;
		Tube* tube;
	};
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	struct mg_elements_changes_info
	{
		bool stereo_to_mono = false;
		bool mono_to_stereo = false;
		bool resampling = false;
		bool transcoding = false;

		bool no_difference()
		{
			return !stereo_to_mono && !mono_to_stereo && !resampling && !transcoding;
		}
	};
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	rtl::Logger* TubeManager::m_log = nullptr;
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void TubeManager::setLogger(rtl::Logger* log)
	{
		m_log = log;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	Tube* TubeManager::createTube(HandlerType h_type, mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out, 
		const char* parent_id, uint32_t tube_number)
	{
		ENTER_FMT("type:%s, number:%d", HandlerType_toString(h_type), tube_number);

		if (parent_id == nullptr)
		{
			ERROR_MSG("parent id is null!");
			return nullptr;
		}

		PRINT_FMT("parent id: %s", parent_id);

		Tube* tube = nullptr;

		switch (h_type)
		{
		case HandlerType::ComfortNoise:
			tube = NEW mg_cn_tube_t(m_log, tube_number, parent_id);
			break;
		case HandlerType::Simple:
			tube = NEW mg_simple_tube_t(m_log, tube_number, parent_id);
			break;
		case HandlerType::AudioDecoder:
			tube = NEW mg_codec_to_pcm_tube_t(m_log, tube_number, parent_id);
			break;
		case HandlerType::AudioEncoder:
			tube = NEW mg_pcm_to_codec_tube_t(m_log, tube_number, parent_id);
			break;
		case HandlerType::AudioResampler:
			tube = NEW mg_resampler_tube_t(m_log, tube_number, parent_id);
			break;
		case HandlerType::VideoDecoder:
			tube = NEW mg_codec_to_img_tube_t(m_log, tube_number, parent_id);
			break;
		case HandlerType::VideoEncoder:
			tube = NEW mg_img_to_codec_tube_t(m_log, tube_number, parent_id);
			break;
		case HandlerType::ImageTransformer:
			tube = NEW mg_transformer_tube_t(m_log, tube_number, parent_id);
			break;
		case HandlerType::StereoResampler:
			tube = NEW mg_mono_to_stereo_tube_t(m_log, tube_number, parent_id);
			break;
		case HandlerType::MonoResampler:
			tube = NEW mg_stereo_to_mono_tube_t(m_log, tube_number, parent_id);
			break;
		case HandlerType::DTMF2833:
			tube = NEW mg_rfc2833_tube_t(m_log, tube_number, parent_id);
			break;
		case HandlerType::ConfAudioInput:
			tube = NEW mg_audio_conference_in_tube_t(m_log, tube_number, parent_id);
			break;
		case HandlerType::ConfAudioOutput:
			tube = NEW mg_audio_conference_out_tube_t(m_log, tube_number, parent_id);
			break;
		//case HandlerType::ConfAudioRecorder:
		//	tube = NEW mg_conference_record_tube_t(m_log, tube_number, parent_id);
		//	break;
		case HandlerType::UDPTL_Recevier:
			tube = NEW mg_udptl_rx_tube_t(m_log, tube_number, parent_id);
			break;
		case HandlerType::UDPTL_Transmitter:
			tube = NEW mg_udptl_tx_tube_t(m_log, tube_number, parent_id);
			break;
		case HandlerType::ConfVideoInput:
			tube = NEW mg_video_conference_in_tube_t(m_log, tube_number, parent_id);
			break;
		case HandlerType::ConfVideoOutput:
			tube = NEW mg_video_conference_out_tube_t(m_log, tube_number, parent_id);
			break;
		//case HandlerType::ConfVideoRecorder:
		//	tube = NEW mg_conference_record_tube_t(m_log, tube_number, parent_id);
		//	break;
		case HandlerType::AudioTimer:
			tube = NEW mg_pcm_ptime_tube_t(m_log, tube_number, parent_id);
			break;
		case HandlerType::AudioSplitter:
			tube = NEW mg_splitter_tube_t(m_log, tube_number, parent_id);
			break;
		default:
			ERROR_FMT("unknown type %d.", h_type);
			return nullptr;
		}

		if (!tube->init(payload_el_in, payload_el_out))
		{
			DELETEO(tube);
			ERROR_MSG("initialization failed.");
			return nullptr;
		}

		return tube;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool TubeManager::set_out_handler(Tube* tube, INextDataHandler* out_handler)
	{
		ENTER();

		if (tube == nullptr)
		{
			ERROR_MSG("tube is null!");
			return false;
		}

		if (out_handler == nullptr)
		{
			ERROR_MSG("out handler is null!");
			return false;
		}

		HandlerType type = tube->getType();

		ENTER_FMT("type: %s.", HandlerType_toString(type));

		if (!tube->setNextHandler(out_handler))
		{
			ERROR_MSG("setNextHandler() returns an error");
			return false;
		}

		RETURN_BOOL(true);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	//const char*	TubeManager::handler_type_to_string(HandlerType h_type)
	//{
	//	switch (h_type)
	//	{
	//	case HandlerType::Simple:
	//		return "simple";
	//	case HandlerType::AudioDecoder:
	//		return "codec to pcm";
	//	case HandlerType::AudioEncoder:
	//		return "pcm to codec";
	//	case HandlerType::ComfortNoise:
	//		return "comfort noise";
	//	case HandlerType::AudioResampler:
	//		return "resampler";
	//	case HandlerType::MonoResampler:
	//		return "stereo to mono";
	//	case HandlerType::StereoResampler:
	//		return "mono to stereo";
	//	case HandlerType::DTMF2833:
	//		return "rfc 2833";
	//	case HandlerType::ConfAudioInput:
	//		return "conf audio in";
	//	case HandlerType::ConfAudioOutput:
	//		return "conf audio out";
	//	//case ConfAudioRecorder:
	//	//	return "conf audio record";
	//	case HandlerType::UDPTL_Recevier:
	//		return "udptl rx";
	//	case HandlerType::UDPTL_Transmitter:
	//		return "udptl tx";
	//	case HandlerType::VideoEncoder:
	//		return "img to codec";
	//	case HandlerType::VideoDecoder:
	//		return "codec to img";
	//	case HandlerType::ImageTransformer:
	//		return "img transformer";
	//	case HandlerType::ConfVideoInput:
	//		return "conf video in";
	//	case HandlerType::ConfVideoOutput:
	//		return "conf video out";
	//	case HandlerType::ConfVideoRecorder:
	//		return "conf video record";
	//	case HandlerType::AudioTimer:
	//		return "pcm ptime";
	//	case HandlerType::AudioSplitter:
	//		return "splitter";

	//	default:
	//		return "unknown";
	//	}
	//}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool TubeManager::prepare_tubes_chain(mge::Direction* direction, mg_media_element_t* element_in, mg_media_element_t* element_out,
		Tube** tube_in, Tube** tube_out, bool clear_in, bool clear_out)
	{
		if (direction == nullptr)
		{
			ERROR_MSG("direction is null.");
			return false;
		}

		///temporary

		const char* direction_name = direction->getName();

		if (element_in == nullptr)
		{
			ERROR_FMT("dir %s : element_in is null", direction_name);
			return false;
		}

		if (element_out == nullptr)
		{
			ERROR_FMT("dir %s : element_out is null.", direction_name);
			return false;
		}

		if (direction->getProcessorType() == STREAM_AUDIO || direction->getProcessorType() == STREAM_IMAGE)
			return prepare_audio_tubes_chain(direction, element_in, element_out, tube_in, tube_out, clear_in, clear_out);

		if (direction->getProcessorType() == STREAM_VIDEO)
			return prepare_video_tubes_chain(direction, element_in, element_out, tube_in, tube_out, clear_in, clear_out);

		return false;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool TubeManager::prepare_audio_tubes_chain(mge::Direction* direction, mg_media_element_t* element_in, mg_media_element_t* element_out,
		Tube** tube_in, Tube** tube_out, bool clear_in, bool clear_out)
	{
		///temporary

		const char* direction_name = direction->getName();

		mg_elements_changes_info change_info;
		diff_elements(&change_info, element_in, element_out);
	
		//------------------------------------------------------------

		Tube* tube_in_param = *tube_in;
		Tube* tube_out_param = *tube_out;

		bool cn = (element_in->get_payload_id() == 13);

		rtl::ArrayT<mg_element_chain_tube*> chain;

		if (element_in->isDtmf())
		{
			mg_element_chain_tube* dtmf_element = NEW mg_element_chain_tube();
			dtmf_element->h_type = HandlerType::DTMF2833;
			dtmf_element->tube = nullptr;
			dtmf_element->element_in = element_in;
			dtmf_element->element_out = element_out;
			chain.add(dtmf_element);
		}
		else if (change_info.no_difference())
		{
			mg_element_chain_tube* chain_element = NEW mg_element_chain_tube();
			chain_element->h_type = HandlerType::Simple;
			chain_element->tube = nullptr;
			chain_element->element_in = element_in;
			chain_element->element_out = element_out;
			chain.add(chain_element);

			if (element_in->needSplitter())
			{
				mg_element_chain_tube* splitter_element = NEW mg_element_chain_tube();
				splitter_element->h_type = HandlerType::AudioSplitter;
				splitter_element->tube = nullptr;
				splitter_element->element_in = element_in;
				splitter_element->element_out = element_out;
				chain.add(splitter_element);
			}
		}
		else
		{
			HandlerType first_type = cn ? HandlerType::ComfortNoise : HandlerType::AudioDecoder;
			if (direction->getType() == mge::DirectionType::conference)
			{
				if (element_in->get_element_name().toLower().indexOf("conf_out") != BAD_INDEX)
				{
					first_type = HandlerType::ConfAudioOutput;
				}
			}

			int splitter_location = (first_type == HandlerType::AudioDecoder) 
				? calculate_splitter_location(element_in, element_out) 
				: (first_type == HandlerType::ComfortNoise)
					? 1
					:-1;

			if (element_in->get_element_name().toLower().indexOf("t38") != BAD_INDEX)
			{
				if (element_in->isHandmade())
				{
					mg_element_chain_tube* chain_element_first = NEW mg_element_chain_tube();
					chain_element_first->h_type = HandlerType::UDPTL_Transmitter;
					chain_element_first->tube = tube_in_param;
					chain_element_first->element_in = element_in;
					chain_element_first->element_out = element_out;
					chain.add(chain_element_first);
				}
			}
			else if ((element_in->get_element_name().toLower().indexOf("ivr") == BAD_INDEX) &&
				(element_in->get_element_name().toLower().indexOf("t30") == BAD_INDEX) &&
				(element_in->get_payload_id() != 11) &&
				(element_in->get_payload_id() != 10))
			{
				mg_element_chain_tube* chain_element_first = NEW mg_element_chain_tube();
				chain_element_first->h_type = first_type;
				chain_element_first->tube = tube_in_param;
				chain_element_first->element_in = element_in;
				chain_element_first->element_out = element_out;
				chain.add(chain_element_first);

				if (splitter_location == 1)
				{
					mg_element_chain_tube* splitter_element = NEW mg_element_chain_tube();
					splitter_element->h_type = HandlerType::AudioSplitter;
					splitter_element->tube = nullptr;
					splitter_element->element_in = element_in;
					splitter_element->element_out = element_out;
					chain.add(splitter_element);
				}
			}

			if (change_info.mono_to_stereo)
			{
				mg_element_chain_tube* chain_element = NEW mg_element_chain_tube();
				chain_element->h_type = HandlerType::StereoResampler;
				chain_element->tube = nullptr;
				chain_element->element_in = element_in;
				chain_element->element_out = element_out;
				chain.add(chain_element);
			}
			else if (change_info.stereo_to_mono)
			{
				mg_element_chain_tube* chain_element = NEW mg_element_chain_tube();
				chain_element->h_type = HandlerType::MonoResampler;
				chain_element->tube = nullptr;
				chain_element->element_in = element_in;
				chain_element->element_out = element_out;
				chain.add(chain_element);
			}
			if (splitter_location == 2)
			{
				mg_element_chain_tube* splitter_element = NEW mg_element_chain_tube();
				splitter_element->h_type = HandlerType::AudioSplitter;
				splitter_element->tube = nullptr;
				splitter_element->element_in = element_in;
				splitter_element->element_out = element_out;
				chain.add(splitter_element);
			}

			if (change_info.resampling)
			{
				mg_element_chain_tube* chain_element = NEW mg_element_chain_tube();
				chain_element->h_type = HandlerType::AudioResampler;
				chain_element->tube = nullptr;
				chain_element->element_in = element_in;
				chain_element->element_out = element_out;
				chain.add(chain_element);
			}
			if (splitter_location == 0)
			{
				mg_element_chain_tube* splitter_element = NEW mg_element_chain_tube();
				splitter_element->h_type = HandlerType::AudioSplitter;
				splitter_element->tube = nullptr;
				splitter_element->element_in = element_in;
				splitter_element->element_out = element_out;
				chain.add(splitter_element);
			}

			HandlerType last_type = HandlerType::AudioEncoder;
			if (direction->getType() == mge::DirectionType::conference)
			{
				if (element_out->get_element_name().toLower().indexOf("conf_in") != BAD_INDEX)
				{
					last_type = HandlerType::ConfAudioInput;
					if ((tube_out_param != nullptr) && (tube_out_param->getType() == HandlerType::ConfAudioInput))
					{
						mg_audio_conference_in_tube_t* conference_in_tube = (mg_audio_conference_in_tube_t*)tube_out_param;
						conference_in_tube->subscriber_add();
					}
				}
			}

			if (element_out->get_element_name().toLower().indexOf("t38") != BAD_INDEX)
			{
				if (element_out->isHandmade())
				{
					mg_element_chain_tube* chain_element_last = NEW mg_element_chain_tube();
					chain_element_last->h_type = HandlerType::UDPTL_Recevier;
					chain_element_last->tube = tube_out_param;
					chain_element_last->element_in = element_in;
					chain_element_last->element_out = element_out;
					chain.add(chain_element_last);
				}
			}
			else if ((element_out->get_element_name().toLower().indexOf("ivr") == BAD_INDEX) &&
				(element_out->get_element_name().toLower().indexOf("t30") == BAD_INDEX) &&
				(element_out->get_payload_id() != 11) &&
				(element_out->get_payload_id() != 10))
			{
				if ((last_type == HandlerType::AudioEncoder) && (!cn))
				{
					mg_element_chain_tube* chain_element_ptime = NEW mg_element_chain_tube();
					chain_element_ptime->h_type = HandlerType::AudioTimer;
					chain_element_ptime->tube = tube_out_param;
					chain_element_ptime->element_in = element_in;
					chain_element_ptime->element_out = element_out;
					chain.add(chain_element_ptime);
				}

				mg_element_chain_tube* chain_element_last = NEW mg_element_chain_tube();
				chain_element_last->h_type = last_type;
				chain_element_last->tube = tube_out_param;
				chain_element_last->element_in = element_in;
				chain_element_last->element_out = element_out;
				chain.add(chain_element_last);
			}
		}

		//------------------------------------------------------------

		if (!prepare_chain(direction, &chain))
		{
			ERROR_FMT("dir %s : prepare_chain fail", direction_name);

			clear_chain(direction, &chain, clear_out);
			if (clear_in)
			{
				*tube_in = nullptr;
			}
			if (clear_out)
			{
				*tube_out = nullptr;
			}
			
			return false;
		}
	
		uint32_t last_index = chain.getCount() - 1;
		mg_element_chain_tube* chain_element_first = chain.getAt(0);
		mg_element_chain_tube* chain_element_last = chain.getAt(last_index);

		if ((chain_element_first == nullptr) || (chain_element_last == nullptr))
		{
			ERROR_FMT("dir %s : terminal chains is null.", direction_name);
			clear_chain(direction, &chain, clear_out);
			if (clear_in)
			{
				*tube_in = nullptr;
			}
			if (clear_out)
			{
				*tube_out = nullptr;
			}
			return false;
		}
	
		// для конференции создается туб conf_in и на этот туб могут подписыватся несколько тубов.
		// по этому мы его не перезатираем и не пересоздаем.
		// в случае с dtmf tube_in и tube_out указывают на один экземпляр dtmf туба.
		// но это нарушает условие с тубом conf_in по этому следующая проверка.
		bool dtmf_conf_in = (element_out->get_element_name().toLower().indexOf("conf_in") != BAD_INDEX) && (chain_element_last->element_in->isDtmf());

		*tube_in = chain_element_first->tube;
		if (!dtmf_conf_in)
		{
			*tube_out = chain_element_last->tube;
		}
	
		clear_chain(&chain);
		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool TubeManager::prepare_video_tubes_chain(mge::Direction* direction, mg_media_element_t* element_in, mg_media_element_t* element_out,
		Tube** tube_in, Tube** tube_out, bool clear_in, bool clear_out)
	{
		const char* direction_name = direction->getName();

		bool transcoding = rtl::String::compare(element_in->get_element_name(), element_out->get_element_name(), true) != 0;

		PRINT_FMT("dir %s : %s -> %s",
			direction_name,
			element_in ? element_in->get_element_name() : "null",
			element_out ? element_out->get_element_name() : "null");

		//------------------------------------------------------------
		Tube* tube_in_param = *tube_in;
		Tube* tube_out_param = *tube_out;
		//------------------------------------------------------------

		rtl::ArrayT<mg_element_chain_tube*> chain;

		if (direction->getType() == mge::DirectionType::conference)
		{
			if (element_in->get_element_name() == "vconf_out")
			{
				mg_element_chain_tube* chain_element = NEW mg_element_chain_tube();
				chain_element->h_type = HandlerType::ConfVideoOutput;
				chain_element->tube = tube_in_param;
				chain_element->element_in = element_in;
				chain_element->element_out = element_out;
				chain.add(chain_element);
			}
			else if (element_out->get_element_name() == "vconf_in")
			{
				mg_element_chain_tube* chain_element = NEW mg_element_chain_tube();
				chain_element->h_type = HandlerType::VideoDecoder;
				chain_element->tube = nullptr;
				chain_element->element_in = element_in;
				chain_element->element_out = element_out;
				chain.add(chain_element);
				chain_element = NEW mg_element_chain_tube();
				chain_element->h_type = HandlerType::ConfVideoInput;
				chain_element->tube = tube_out_param;
				chain_element->element_in = element_out;
				chain_element->element_out = element_out;
				mg_video_conference_in_tube_t* conference_in_tube = (mg_video_conference_in_tube_t*)tube_out_param;
				conference_in_tube->subscriber_add();
				chain.add(chain_element);
			}
		}
		else if (!transcoding)
		{
			mg_element_chain_tube* chain_element = NEW mg_element_chain_tube();
			chain_element->h_type = HandlerType::Simple;
			chain_element->tube = nullptr;
			chain_element->element_in = element_in;
			chain_element->element_out = element_out;
			chain.add(chain_element);
		}
		else
		{
			HandlerType first_type = HandlerType::VideoDecoder;

			mg_element_chain_tube* chain_element_first = NEW mg_element_chain_tube();
			chain_element_first->h_type = first_type;
			chain_element_first->tube = tube_in_param;
			chain_element_first->element_in = element_in;
			chain_element_first->element_out = element_out;
			chain.add(chain_element_first);

			mg_element_chain_tube* chain_element = NEW mg_element_chain_tube();
			chain_element->h_type = HandlerType::ImageTransformer;
			chain_element->tube = nullptr;
			chain_element->element_in = element_in;
			chain_element->element_out = element_out;
			chain.add(chain_element);

			HandlerType last_type = HandlerType::VideoEncoder;

			mg_element_chain_tube* chain_element_last = NEW mg_element_chain_tube();
			chain_element_last->h_type = last_type;
			chain_element_last->tube = tube_out_param;
			chain_element_last->element_in = element_in;
			chain_element_last->element_out = element_out;
			chain.add(chain_element_last);
		}

		//------------------------------------------------------------

		if (!prepare_chain(direction, &chain))
		{
			ERROR_FMT("dir %s : prepare_chain fail.", direction_name);
			clear_chain(direction, &chain, clear_out);
			if (clear_in)
			{
				*tube_in = nullptr;
			}
			if (clear_out)
			{
				*tube_out = nullptr;
			}
			return false;
		}

		uint32_t last_index = chain.getCount() - 1;
		mg_element_chain_tube* chain_element_first = chain.getAt(0);
		mg_element_chain_tube* chain_element_last = chain.getAt(last_index);

		if ((chain_element_first == nullptr) || (chain_element_last == nullptr))
		{
			ERROR_FMT("dir %s : terminal chains is null", direction_name);
			clear_chain(direction, &chain, clear_out);
			if (clear_in)
			{
				*tube_in = nullptr;
			}
			if (clear_out)
			{
				*tube_out = nullptr;
			}
			return false;
		}

		// для конференции создается туб conf_in и на этот туб могут подписыватся несколько тубов.
		// по этому мы его не перезатираем и не пересоздаем.
		// в случае с dtmf tube_in и tube_out указывают на один экземпляр dtmf туба.
		// но это нарушает условие с тубом conf_in по этому следующая проверка.

		*tube_in = chain_element_first->tube;
		*tube_out = chain_element_last->tube;

		clear_chain(&chain);

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool TubeManager::prepare_chain(mge::Direction* direction, rtl::ArrayT<mg_element_chain_tube*>* chain)
	{
		if (chain == nullptr)
		{
			WARNING_FMT("dir %s : chain is null!", direction->getName());
			return false;
		}

		rtl::String type_for_log("stream in --> ");

		mg_element_chain_tube* chain_element_last = nullptr;
		for (int i = 0; i < chain->getCount(); i++)
		{
			mg_element_chain_tube* chain_element = chain->getAt(i);
			if (chain_element == nullptr)
			{
				PRINT_FMT("dir %s : chain element[%d] is null.", direction->getName(), i);
				return false;
			}

			if (chain_element->tube == nullptr)
			{
				if (!create_tube_in_direction(direction, chain_element))
				{
					WARNING_FMT("dir %s : create tube failed for element[%d].", direction->getName(), i);
					return false;
				}

				if (chain_element->h_type == HandlerType::AudioSplitter)
				{
					if (!prepare_splitter_tubes_chain(direction, chain_element->element_in, chain_element->element_out, chain_element->tube))
					{
						WARNING_FMT("dir %s : prepare splitter tubes chain failed for element[%d].", direction->getName(), i);
						return false;
					}
				}
			}

			if (chain_element_last != nullptr)
			{
				if (!set_out_handler(chain_element_last->tube, chain_element->tube))
				{
					WARNING_FMT("dir %s : failed set out handler for element[%d].", direction->getName(), i);
					return false;
				}
			
				chain_element->tube->set_free_in(false);
			}

			chain_element_last = chain_element;
			type_for_log << HandlerType_toString(chain_element->h_type);
			type_for_log << " --> ";
		}

		type_for_log << "stream out";

		RETURN_FMT(true, "dir %s : %s.", direction->getName(), (const char*)type_for_log);

	}
	//------------------------------------------------------------------------------
	bool TubeManager::create_tube_in_direction(mge::Direction* direction, mg_element_chain_tube* chain_element)
	{
		if (direction == nullptr)
		{
			WARNING_FMT("dir %s : direction is null.", direction->getName());
			return false;
		}

		if (chain_element == nullptr)
		{
			WARNING_FMT("dir %s : chain element is null.", direction->getName());
			return false;
		}

		HandlerType type = chain_element->h_type;

		Tube* tube = direction->createTube(type, chain_element->element_in, chain_element->element_out);
	
		if (tube == nullptr)
		{
			WARNING_FMT("dir %s : create_tube(%s, %s, %s) failed -> .", direction->getName(),
				HandlerType_toString(type),
				(const char*)chain_element->element_in->get_element_name(),
				(const char*)chain_element->element_out->get_element_name());

			return false;
		}

		chain_element->tube = tube;

		return true;
	}
	//------------------------------------------------------------------------------
	void TubeManager::clear_chain(mge::Direction* direction, rtl::ArrayT<mg_element_chain_tube*>* chain, bool clear_out)
	{
		if (direction == nullptr)
		{
			return;
		}

		if (chain == nullptr)
		{
			return;
		}

		for (int i = chain->getCount() - 1; i >= 0; i--)
		{
			mg_element_chain_tube* chain_element = chain->getAt(i);
			if (chain_element == nullptr)
			{
				continue;
			}

			bool clear = true;
			if (!clear_out)
			{
				if (chain_element->h_type == HandlerType::ConfAudioOutput ||
					chain_element->h_type == HandlerType::ConfVideoOutput)
				{
					clear = false;
				}
			}
			if (clear)
			{
				//clear_tube(direction, chain_element->tube);
				if (chain_element->tube)
				{
					chain_element->tube->release();
				}
				release_tube_in_direction(direction, chain_element);
			}
			chain_element->element_in = nullptr;
			chain_element->element_out = nullptr;

			DELETEO(chain_element);
			chain_element = nullptr;
			chain->removeAt(i);
		}
	}
	//------------------------------------------------------------------------------
	void TubeManager::clear_chain(rtl::ArrayT<mg_element_chain_tube*>* chain)
	{
		if (chain == nullptr)
		{
			return;
		}

		for (int i = 0; i < chain->getCount(); i++)
		{
			mg_element_chain_tube* chain_element = chain->getAt(i);
			if (chain_element == nullptr)
			{
				continue;
			}

			chain_element->element_in = nullptr;
			chain_element->element_out = nullptr;
			chain_element->tube = nullptr;

			DELETEO(chain_element);
			chain_element = nullptr;
			chain->removeAt(i);
			i--;
		}
	}
	//------------------------------------------------------------------------------
	void TubeManager::release_tube_in_direction(mge::Direction* direction, mg_element_chain_tube* chain_element)
	{
		if (direction == nullptr)
		{
			return;
		}

		if (chain_element == nullptr)
		{
			return;
		}

		Tube* tube = chain_element->tube;
		if (tube == nullptr)
		{
			return;
		}

		release_tube_in_direction(direction, tube);

		chain_element->tube = nullptr;
	}
	//------------------------------------------------------------------------------
	void TubeManager::release_tube_in_direction(mge::Direction* direction, Tube* tube)
	{
		if (direction == nullptr)
		{
			return;
		}

		if (tube == nullptr)
		{
			return;
		}

		if (direction->getType() == mge::DirectionType::bothway)
		{
			direction->releaseTube(tube->getId());
		}
		else if (direction->getType() == mge::DirectionType::conference)
		{
			if (tube->getType() == HandlerType::ConfAudioInput)
			{
				mg_audio_conference_in_tube_t* onference_in_tube = (mg_audio_conference_in_tube_t*)tube;
				if (onference_in_tube->get_subscribers_count() != 0)
				{
					return;
				}
				else
				{
					bool ok = false;
					uint16_t t = 5;
					while (t > 0)
					{
						if (onference_in_tube->can_delete())
						{
							ok = true;
							break;
						}
						else
						{
							t--;
							rtl::Thread::sleep(20);
						}
					}
					if (!ok)
					{
						return;
					}
				}
			}
			else if (tube->getType() == HandlerType::ConfVideoInput)
			{
				mg_video_conference_in_tube_t* conference_in_tube = (mg_video_conference_in_tube_t*)tube;
				if (conference_in_tube->get_subscribers_count() != 0)
				{
					return;
				}
				else
				{
					bool ok = false;
					uint16_t t = 5;
					while (t > 0)
					{
						if (conference_in_tube->can_delete())
						{
							ok = true;
							break;
						}
						else
						{
							t--;
							rtl::Thread::sleep(20);
						}
					}
					if (!ok)
					{
						return;
					}
				}
			}
		
			direction->releaseTube(tube->getId());
		}
	}
	//------------------------------------------------------------------------------
	//bool TubeManager::change_conference_topology(mge::Direction* direction, Tube* tube_from, Tube* tube_to, h248::TopologyDirection topology)
	//{
	//	if (direction == nullptr)
	//	{
	//		PLOG_TUBE_ERROR(LOG_PREFIX, "change_conference_topology -- direction is null.");
	//		return false;
	//	}
	//
	//	if (direction->getType() != mge::DirectionType::conference)
	//	{
	//		PLOG_TUBE_ERROR(LOG_PREFIX, "change_conference_topology -- wrong type direction.");
	//		return false;
	//	}
	//
	//	const char* direction_name = direction->getName();
	//
	//	if ((tube_from == nullptr) || (tube_to == nullptr))
	//	{
	//		PLOG_TUBE_ERROR(LOG_PREFIX, "change_conference_topology -- wrong param in direction %s.", direction_name);
	//		return false;
	//	}
	//	if ((tube_from->getType() != HandlerType::ConfAudioOutput) || (tube_to->getType() != HandlerType::ConfAudioOutput))
	//	{
	//		PLOG_TUBE_ERROR(LOG_PREFIX, "change_conference_topology -- wrong param in direction %s.", direction_name);
	//		return false;
	//	}
	//
	//	mg_audio_conference_out_tube_t* out_from_tube = (mg_audio_conference_out_tube_t*)tube_from;
	//	mg_audio_conference_out_tube_t* out_to_tube = (mg_audio_conference_out_tube_t*)tube_to;
	//
	//	if (change_conference_topology_from(direction, tube_from, out_to_tube->get_termination_name(), topology) &&
	//		change_conference_topology_to(direction, tube_to, out_from_tube->get_termination_name(), topology))
	//	{
	//		return true;
	//	}
	//
	//	PLOG_TUBE_ERROR(LOG_PREFIX, "change_conference_topology -- wrong topology in direction %s.", direction_name);
	//	return false;
	//}
	//------------------------------------------------------------------------------
	//bool TubeManager::change_conference_topology_from(mge::Direction* direction, Tube* tube_from, const char* id, h248::TopologyDirection topology)
	//{
	//	mg_audio_conference_out_tube_t* out_from_tube = (mg_audio_conference_out_tube_t*)tube_from;
	//	
	//	if (topology == h248::TopologyDirection::Bothway)
	//	{
	//		out_from_tube->remove_except_termination_id(id);
	//		return true;
	//	}
	//	else if (topology == h248::TopologyDirection::Isolate)
	//	{
	//		out_from_tube->add_except_termination_id(id);
	//		return true;
	//	}
	//	else if (topology == h248::TopologyDirection::Oneway)
	//	{
	//		out_from_tube->add_except_termination_id(id);
	//		return true;
	//	}
	//	return false;
	//}
	////------------------------------------------------------------------------------
	//bool TubeManager::change_conference_topology_to(mge::Direction* direction, Tube* tube_to, const char* id, h248::TopologyDirection topology)
	//{
	//	mg_audio_conference_out_tube_t* out_to_tube = (mg_audio_conference_out_tube_t*)tube_to;
	//
	//	if (topology == h248::TopologyDirection::Bothway)
	//	{
	//		out_to_tube->remove_except_termination_id(id);
	//		return true;
	//	}
	//	else if (topology == h248::TopologyDirection::Isolate)
	//	{
	//		out_to_tube->add_except_termination_id(id);
	//		return true;
	//	}
	//	else if (topology == h248::TopologyDirection::Oneway)
	//	{
	//		out_to_tube->remove_except_termination_id(id);
	//		return true;
	//	}
	//	return false;
	//}
	//------------------------------------------------------------------------------
	// -1 - не нужет splitter туб
	//  0 - добавляется в конец цепочки
	//  1 - добавляется после codec_to_pcm туба
	//  2 - добавляется после stereo_to_mono (mono_to_stereo) тубов
	//------------------------------------------------------------------------------
	int TubeManager::calculate_splitter_location(mg_media_element_t* element_in, mg_media_element_t* element_out)
	{
		if (!element_in->needSplitter())
		{
			return -1;
		}

		const mg_splitter_params_t* param = element_in->get_splitter_params();

		//------------------------------------------------------------

		uint32_t channel_count_in = element_in->getChannelCount();
		uint32_t channel_count_out = element_out->getChannelCount();

		if ((channel_count_out != channel_count_in) && (param->vad_channels != channel_count_out))
		{
			return 1;
		}

		//------------------------------------------------------------

		uint32_t sample_rate_in = element_in->get_sample_rate();
		uint32_t sample_rate_out = element_out->get_sample_rate();

		if ((sample_rate_out != sample_rate_in) && (param->vad_rate != sample_rate_out))
		{
			return 2;
		}

		return 0;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool TubeManager::prepare_splitter_tubes_chain(mge::Direction* direction, mg_media_element_t* element_in, mg_media_element_t* element_out, Tube* splitter_tube)
	{
		mg_elements_changes_info change_info;
		diff_elements(&change_info, element_in, element_out);

		int location = calculate_splitter_location(element_in, element_out);

		mg_splitter_params_t* param = element_in->get_splitter_params();

		mg_media_element_t el("", 8, nullptr);
		el.copy_from(element_out);
		el.set_sample_rate(param->vad_rate);

		rtl::ArrayT<mg_element_chain_tube*> chain;

		if (change_info.no_difference() && (element_in->get_payload_id() != 13))
		{
			mg_element_chain_tube* chain_element = NEW mg_element_chain_tube();
			chain_element->h_type = HandlerType::AudioDecoder;
			chain_element->tube = nullptr;
			chain_element->element_in = element_in;
			chain_element->element_out = element_out;
			chain.add(chain_element);

			//------------------------------------------------------------

			uint32_t channel_count_in = element_in->getChannelCount();

			if (channel_count_in > param->vad_channels)
			{
				mg_element_chain_tube* stereo_element = NEW mg_element_chain_tube();
				stereo_element->h_type = HandlerType::MonoResampler;
				stereo_element->tube = nullptr;
				stereo_element->element_in = element_in;
				stereo_element->element_out = element_out;
				chain.add(stereo_element);
			}
			else if (channel_count_in < param->vad_channels)
			{
				mg_element_chain_tube* mono_element = NEW mg_element_chain_tube();
				mono_element->h_type = HandlerType::StereoResampler;
				mono_element->tube = nullptr;
				mono_element->element_in = element_in;
				mono_element->element_out = element_out;
				chain.add(mono_element);
			}

			//------------------------------------------------------------

			if (param->vad_rate != element_in->get_sample_rate())
			{
				mg_element_chain_tube* resampler_element = NEW mg_element_chain_tube();
				resampler_element->h_type = HandlerType::AudioResampler;
				resampler_element->tube = nullptr;
				resampler_element->element_in = element_in;
				resampler_element->element_out = &el;
				chain.add(resampler_element);
			}
		}
		else
		{
			if (location == 2)
			{
				if (param->vad_rate != element_in->get_sample_rate())
				{
					mg_element_chain_tube* resampler_element = NEW mg_element_chain_tube();
					resampler_element->h_type = HandlerType::AudioResampler;
					resampler_element->tube = nullptr;
					resampler_element->element_in = element_in;
					resampler_element->element_out = &el;
					chain.add(resampler_element);
				}
			}
			else if (location == 1)
			{
				//------------------------------------------------------------

				uint32_t channel_count_in = element_in->getChannelCount();

				if (channel_count_in > param->vad_channels)
				{
					mg_element_chain_tube* stereo_element = NEW mg_element_chain_tube();
					stereo_element->h_type = HandlerType::MonoResampler;
					stereo_element->tube = nullptr;
					stereo_element->element_in = element_in;
					stereo_element->element_out = element_out;
					chain.add(stereo_element);
				}
				else if (channel_count_in < param->vad_channels)
				{
					mg_element_chain_tube* mono_element = NEW mg_element_chain_tube();
					mono_element->h_type = HandlerType::StereoResampler;
					mono_element->tube = nullptr;
					mono_element->element_in = element_in;
					mono_element->element_out = element_out;
					chain.add(mono_element);
				}

				//------------------------------------------------------------

				if (param->vad_rate != element_in->get_sample_rate())
				{
					mg_element_chain_tube* resampler_element = NEW mg_element_chain_tube();
					resampler_element->h_type = HandlerType::AudioResampler;
					resampler_element->tube = nullptr;
					resampler_element->element_in = element_in;
					resampler_element->element_out = &el;
					chain.add(resampler_element);
				}
			}
		}

		if (!prepare_splitter_chain(direction, &chain, param))
		{
			ERROR_FMT("dir %s : prepare_splitter_chain fail.", direction->getName());
			clear_chain(direction, &chain, true);
			return false;
		}

		uint32_t c = chain.getCount();
		uint32_t last_index = (c > 0) ? c -1 : 0;
		mg_element_chain_tube* chain_element_first = chain.getAt(0);
		mg_element_chain_tube* chain_element_last = chain.getAt(last_index);
		if ((chain_element_last == nullptr) && (c > 0))
		{
			ERROR_FMT("dir %s : terminal chains is null.", direction->getName());
			clear_chain(direction, &chain, true);
			return false;
		}

		Tube* in_tube = (c == 0) ? splitter_tube : chain_element_first->tube;
		if (!bind_splitter(splitter_tube, in_tube, (chain_element_last == nullptr) ? nullptr : chain_element_last->tube, param))
		{
			ERROR_FMT("dir %s : bind splitter.", direction->getName());
			clear_chain(direction, &chain, true);

			mg_splitter_tube_t* splitter = (mg_splitter_tube_t*)splitter_tube;
			splitter->set_out_handler_2(nullptr);

			return false;
		}

		clear_chain(&chain);
		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool TubeManager::prepare_splitter_chain(mge::Direction* direction, rtl::ArrayT<mg_element_chain_tube*>* chain, mg_splitter_params_t* param)
	{
		if (chain == nullptr)
		{
			ERROR_FMT("dir %s : chain is null.", direction->getName());
			return false;
		}

		rtl::String type_for_log;
		type_for_log << HandlerType_toString(HandlerType::AudioSplitter);
		type_for_log << " --> ";

		mg_element_chain_tube* chain_element_last = nullptr;
		for (int i = 0; i < chain->getCount(); i++)
		{
			mg_element_chain_tube* chain_element = chain->getAt(i);
			if (chain_element == nullptr)
			{
				WARNING_FMT("dir %s : chain element[%d] is null.", direction->getName(), i);
				return false;
			}

			if (chain_element->tube == nullptr)
			{
				if (!create_tube_in_direction(direction, chain_element))
				{
					WARNING_FMT("dir %s : create tube failed for element[%d].", direction->getName(), i);
					return false;
				}
			}

			if (chain_element_last != nullptr)
			{
				if (!set_out_handler(chain_element_last->tube, chain_element->tube))
				{
					WARNING_FMT("dir %s : failed set out handler for element[%d].", direction->getName(), i);
					return false;
				}

				chain_element->tube->set_free_in(false);
			}

			chain_element_last = chain_element;
			type_for_log << HandlerType_toString(chain_element->h_type);
			type_for_log << " --> ";
		}

		rtl::String end;
		if (param->vad)
		{
			end << "VAD";
		}
		type_for_log << end;
		PRINT_FMT("dir %s : %s.", direction->getName(), (const char*)type_for_log);

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool TubeManager::bind_splitter(Tube* splitter_tube, Tube* in_tube, Tube* out_tube, mg_splitter_params_t* param)
	{
		mg_splitter_tube_t* splitter = (mg_splitter_tube_t*)splitter_tube;
		if (splitter->getId() == in_tube->getId())
		{
			// если нет 2ой цепочки, просто замыкаем аут на аналайзер
			if (!splitter->set_out_handler_2((INextDataHandler*)param->event_handler))
			{
				ERROR_FMT("set_out_handler_2 fail. Splitter tube: %s.", splitter->getName());
				return false;
			}
		}
		else
		{
			// замыкаем на начало новой цепочки
			if (!splitter->set_out_handler_2(in_tube))
			{
				ERROR_FMT("set_out_handler_2 fail. Splitter tube: %s.", splitter->getName());
				return false;
			}

			if (out_tube == nullptr)
			{
				// сюда приходим если новая цепочка состоит из одного туба, его и замыкаем на аналайзер
				if (!set_out_handler(in_tube, (INextDataHandler*)param->event_handler))
				{
					ERROR_FMT("failed set out handler for tube: %s.", in_tube->getName());
					return false;
				}
			}
			else
			{
				// последний туб в новой цепочке замыкаем на аналайзер
				if (!set_out_handler(out_tube, (INextDataHandler*)param->event_handler))
				{
					ERROR_FMT("failed set out handler for tube: %s.", out_tube->getName());
					return false;
				}
			}
		}

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void TubeManager::diff_elements(mg_elements_changes_info* info, mg_media_element_t* element_in, mg_media_element_t* element_out)
	{
		//------------------------------------------------------------

		bool stereo_to_mono = false;
		bool mono_to_stereo = false;

		uint32_t channel_count_in = element_in->getChannelCount();
		uint32_t channel_count_out = element_out->getChannelCount();

		if (channel_count_in > channel_count_out)
		{
			stereo_to_mono = true;
		}
		else if (channel_count_out > channel_count_in)
		{
			mono_to_stereo = true;
		}

		//------------------------------------------------------------

		uint32_t sample_rate_in = element_in->get_sample_rate();
		uint32_t sample_rate_out = element_out->get_sample_rate();

		bool resampling = (sample_rate_in != sample_rate_out);

		//------------------------------------------------------------

		bool transcoding = !element_in->compare_with(element_out);

		if (!transcoding)
		{
			if (element_in->isHandmade() && !element_out->isHandmade())
			{
				transcoding = true;
			}
			else if (!element_in->isHandmade() && element_out->isHandmade())
			{
				transcoding = true;
			}
		}

		//------------------------------------------------------------

		bool cn = (element_in->get_payload_id() == 13);
		if ((element_in->get_payload_id() == 206) || (element_out->get_payload_id() == 206)) // dummy
		{
			stereo_to_mono = false;
			mono_to_stereo = false;
			resampling = false;
			transcoding = false;
		}

		info->stereo_to_mono = stereo_to_mono;
		info->mono_to_stereo = mono_to_stereo;
		info->resampling = resampling;
		info->transcoding = transcoding;
	}
}
//------------------------------------------------------------------------------
