﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_thread.h"
#include "mg_rx_ivr_stream.h"
#include "mg_tube_manager.h"
#include "mg_cn_tube.h"
#include "mg_session.h"

#include "logdef_stream.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_rx_ivr_stream_t::mg_rx_ivr_stream_t(rtl::Logger* log, uint32_t id, const char* parent_id, MediaSession& session) :
		mg_rx_stream_t(log, id, mg_rx_ivr_e, parent_id, session)
	{
		m_ssrc = (rtl::DateTime::getTicks() + rand()) | 0x833000b0;
		m_seq_num = (uint16_t)(rtl::DateTime::getTicks() ^ 0x000000D5D);;
		m_seq_num_last = 0;
		m_timestamp = rtl::DateTime::getTicks() & 0x005D5D5D;
		m_timestamp_last = 0;
		m_samples_last = 160;

		m_timestamp_step = 20;

		m_player = nullptr;

		rtl::String name;
		name << session.getName() << "-rxivrs" << id;
		setName(name);
		setTag("RXS_IVR");

		getMediaParams().get_media_elements()->clear();
		mg_create_media_element_ivr(parent_id, getMediaParams().get_media_elements());

		m_rx_lock.clear();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_rx_ivr_stream_t::~mg_rx_ivr_stream_t()
	{
		destroy_player();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_rx_ivr_stream_t::create_player()
	{
		uint32_t sample_rate = 8000;
		uint32_t channel_count = 1;
		mg_media_element_t* element = getMediaParams().get_media_elements()->get_priority();
		if (element != nullptr)
		{
			sample_rate = element->get_sample_rate();
			channel_count = element->getChannelCount();
		}

		if (m_player == nullptr)
		{
			m_player = NEW media::AudioPlayer(m_log);
			if (!m_player->init(m_timestamp_step, sample_rate, channel_count, this))
			{
				WARNING("player init fail.");
				DELETEO(m_player);
				m_player = nullptr;
				return false;
			}
			
			PRINT("player created");
			
			return true;
		}

		WARNING("reader aleady created.");

		return false;
	}
	//------------------------------------------------------------------------------
	void mg_rx_ivr_stream_t::destroy_player()
	{
		if (m_player != nullptr)
		{
			media::PlayerReport* report = m_player->stopPlay();
			//eventEndPlay(report);
			m_player->destroy();
			DELETEO(m_player);
			m_player = nullptr;
		}

		PRINT("player destroyed.");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_rx_ivr_stream_t::pause()
	{
		if (m_player != nullptr)
		{
			if (m_player->isMetronomeStarted())
			{
				if (!m_player->isMetronomePaused())
				{
					m_player->metronomePause();
				}
			}
		}
	}
	//------------------------------------------------------------------------------
	void mg_rx_ivr_stream_t::resume()
	{
		if (m_player != nullptr)
		{
			if (m_player->isMetronomeStarted())
			{
				if (m_player->isMetronomePaused())
				{
					m_player->metronomeResume();
				}
			}
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_rx_ivr_stream_t::lock()
	{
		if (isLocked())
		{
			return true;
		}

		if (!mg_rx_stream_t::lock())
		{
			return false;
		}

		if (m_player != nullptr)
		{
			m_player->metronomePause();
		}

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_rx_ivr_stream_t::unlock()
	{
		if (isLocked())
		{
			mg_rx_stream_t::unlock();

			if (m_player != nullptr)
			{
				if (m_player->isMetronomePaused())
				{
					m_player->metronomeResume();
				}
				else if (!m_player->startPlay())
				{
					WARNING("Player is not started!");
				}
			}
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_rx_ivr_stream_t::change_timestamp_step(uint32_t timestamp_step)
	{
		int count_tubes = getTubeCount();
		for (int i = 0; i < count_tubes; i++)
		{
			Tube* tube_at_list = getTube(i);
			if (tube_at_list == nullptr)
			{
				continue;
			}

			// TubeManager::set_timestamp_step(tube_at_list, timestamp_step);

			if (tube_at_list->getType() == HandlerType::ComfortNoise)
			{
				mg_cn_tube_t* cn_tube = (mg_cn_tube_t*)tube_at_list;
				cn_tube->set_timestamp_step(timestamp_step);
			}
		}

		m_timestamp_step = timestamp_step;

		mg_media_element_t* element = getMediaParams().get_media_elements()->get_priority();
		if (element != nullptr)
		{
			element->set_ptime(m_timestamp_step);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_rx_ivr_stream_t::eventDataReady(const char* data, int data_length)
	{
		if (data_length > (int)rtp_packet::PAYLOAD_BUFFER_SIZE)
		{
			ERROR_MSG("package size is too large.");
			return;
		}

		//---------------------------------------------------------------------
		// упаковываем в пакет

		if (data_length <= 0)
		{
			ERROR_MSG("package size is zero.");
			return;
		}

		uint32_t samples = data_length / sizeof(short);

		rtp_packet packet;
		packet.set_version(2);
		if ((m_seq_num_last == 0) || (m_timestamp_last == 0))
		{
			packet.set_marker(true);
		}

		packet.set_payload_type(11);
		packet.set_ssrc(m_ssrc);
		packet.set_samples(samples);

		m_seq_num_last = m_seq_num;
		m_seq_num++;
		packet.set_sequence_number(m_seq_num);

		m_timestamp_last = m_timestamp;
		m_timestamp += samples;
		packet.set_timestamp(m_timestamp);

		packet.set_payload(data, data_length);

		//---------------------------------------------------------------------

		//---------------------------------------------------------------------
		// рассылаем по тубам

		int count_tubes = getTubeCount();
		for (int i = 0; i < count_tubes; i++)
		{
			Tube* tube_at_list = getTube(i);
			if (tube_at_list == nullptr)
			{
				continue;
			}

			//PRINT_FMT("sending data %d bytes to %s", getName(), data_length, tube_at_list->getName());
			tube_at_list->send(tube_at_list, &packet);

			// RTX-20
			// Рассылаем только по одной цепочке, т.к. бывают моменты когда второй туб simpl CN.
			// На туб CN звук слать не нужно. (наверное есть вероятность что первый туб будет CN, но вроде как не должно быть)
			break;

		}

		//---------------------------------------------------------------------
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_rx_ivr_stream_t::eventEndPlay(media::PlayerReport* report)
	{
		if (report == nullptr)
		{
			WARNING("report is null.");
			return;
		}

		h248::IVR_EventParams* param = NEW h248::IVR_EventParams(report->stopTime, report->errorFiles);

		mg_rx_stream_t::getSession().mg_stream__event(param);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_rx_ivr_stream_t::setMediaParams(mg_sdp_media_base_t& media)
	{
		mg_rx_stream_t::setMediaParams(media);

		if (getMediaParams().get_media_elements()->getCount() == 0)
		{
			mg_create_media_element_ivr(getName(), getMediaParams().get_media_elements());
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_rx_ivr_stream_t::set_start_at(int start_at)
	{
		if (m_player == nullptr)
		{
			WARNING("player is null.");
			return;
		}
		return m_player->setStartAt(start_at);
	}
	//------------------------------------------------------------------------------
	void mg_rx_ivr_stream_t::set_stop_at(int stop_at)
	{
		if (m_player == nullptr)
		{
			WARNING("player is null.");
			return;
		}
		return m_player->setStopAt(stop_at);
	}
	//------------------------------------------------------------------------------
	void mg_rx_ivr_stream_t::set_loop(bool loop)
	{
		if (m_player == nullptr)
		{
			WARNING("player is null.");
			return;
		}
		return m_player->setLoop(loop);
	}
	//------------------------------------------------------------------------------
	void mg_rx_ivr_stream_t::set_random(bool random)
	{
		if (m_player == nullptr)
		{
			WARNING("player is null.");
			return;
		}
		return m_player->setRandom(random);
	}
	//------------------------------------------------------------------------------
	bool mg_rx_ivr_stream_t::add_file(const char* file_path)
	{
		if (m_player == nullptr)
		{
			WARNING("player is null.");
			return false;
		}

		return m_player->addFile(file_path);
	}
	//------------------------------------------------------------------------------
	bool mg_rx_ivr_stream_t::add_dir(const char* dir)
	{
		if (m_player == nullptr)
		{
			WARNING("player is null.");
			return false;
		}

		return m_player->addDir(dir);
	}
	//------------------------------------------------------------------------------
	void mg_rx_ivr_stream_t::set_volume(int volume)
	{
		if (m_player == nullptr)
		{
			WARNING("player is null.");
			return;
		}

		m_player->setVolume(volume);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	int mg_rx_ivr_stream_t::rtcp_session_rx(const rtcp_packet_t* packet)
	{
		return 0;
	}
}
//------------------------------------------------------------------------------
