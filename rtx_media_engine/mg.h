﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "std_mutex.h"
#include "rtp_port_manager.h"
#include "mg_context.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
extern rtl::Timer*  g_mg_no_voise_timer;

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	class media_gateway_t : public h248::IMediaGate
	{
	public:
		media_gateway_t(rtl::Logger* log);
		~media_gateway_t();

		uint32_t mg_get_id();

		// IMediaGate
		virtual bool MediaGate_initialize(h248::IMediaGateEventHandler* callback) override;
		virtual void MediaGate_destroy() override;

		virtual void MediaGate_setInterfaceName(const char* iface, const char* addressKey) override;
		virtual void MediaGate_removeInterfaceName(const char* iface) override;

		virtual bool MediaGate_addPortRangeRTP(const char* addressKey, uint16_t port_begin, uint16_t port_count) override;
		virtual void MediaGate_freePortRangeRTP(const char* addressKey) override;

		virtual h248::IMediaContext* MediaGate_createContext() override;
		virtual void MediaGate_destroyContext(h248::IMediaContext*) override;
		virtual void MediaGate_setDeviceIn(h248::IAudioDeviceInput* device_man) override;
		virtual void MediaGate_setDeviceOut(h248::IAudioDeviceOutput* callback) override;

		h248::IAudioDeviceInput* getDeviceIn();
		h248::IAudioDeviceOutput* getDeviceOut() const;

	private:
		MediaContext* create_context();
		void destroy_context(uint32_t context_id);
		void destroy_all_contexts();
		uint32_t generateContextId();

	private:
		rtl::Logger* m_log;
		rtl::MutexWatch m_mutex;

		uint32_t m_id;
		uint32_t m_context_id_counter;

		MediaContextList m_context_list;

		h248::IMediaGateEventHandler* m_megaco_callback;
		h248::IAudioDeviceInput* m_device_in_man;
		h248::IAudioDeviceOutput* m_device_out_man;
	};
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	extern media_gateway_t* g_media_gateway;
}
//------------------------------------------------------------------------------
