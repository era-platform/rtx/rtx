﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_sdp_base.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define LOG_PREFIX "SDP"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_sdp_session_t::mg_sdp_session_t(rtl::Logger* log) :
		m_log(log)
	{
		m_fingerprint = nullptr;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_sdp_session_t::~mg_sdp_session_t()
	{
		clear();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void  mg_sdp_session_t::set_termination_id(const char* term_id)
	{
		m_term_id = term_id;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_sdp_session_t::read(const rtl::String& spd, bool compare, uint32_t stream_id)
	{
		if (spd.isEmpty())
		{
			return false;
		}

		// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.read -- begin: \r\n%s.", m_term_id, spd);

		rtl::String spd_s(spd);
		if (spd_s.isEmpty())
		{
			// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.read -- spd_s - empty.",m_term_id);
			return false;
		}

		spd_s.trim(" \t");

		//char first_symbol = spd_s.front();
		// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.read -- first symbol: %c.", m_term_id, first_symbol);

		int index_b = 0;
		int index = spd_s.indexOf('\n');
		mg_sdp_media_base_t* current_media = nullptr;
		bool one = false;
		bool compare_media = compare;

		while (index != BAD_INDEX)
		{
			rtl::String item = spd_s.substring(index_b, index - index_b);

			// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.read -- getline: %s.", m_term_id, item);
			if (item.isEmpty())
			{
				index_b = index + 1;
				index = spd_s.indexOf('\n', index_b);
				continue;
			}

			item.trim(" \t\r");

			char first_symbol_line = item.front();
			// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.read -- first symbol line: %c.", m_term_id, first_symbol_line);

			if (first_symbol_line == 'm')
			{
				if (current_media == nullptr)
				{
					if (m_media_list.getCount() != 0)
					{
						current_media = get_media(stream_id);
					}
				}
				else
				{
					if ((item.indexOf(MG_STREAM_TYPE_AUDIO_STR) != BAD_INDEX) && (current_media->getType().indexOf(MG_STREAM_TYPE_AUDIO_STR) == BAD_INDEX))
					{
						insert_media_differences(current_media->get_differences());
						current_media = get_media(stream_id);
					}
					else if ((item.indexOf(MG_STREAM_TYPE_VIDEO_STR) != BAD_INDEX) && (current_media->getType().indexOf(MG_STREAM_TYPE_VIDEO_STR) == BAD_INDEX))
					{
						insert_media_differences(current_media->get_differences());
						current_media = get_media(stream_id);
					}
					else if ((item.indexOf(MG_STREAM_TYPE_IMAGE_STR) != BAD_INDEX) && (current_media->getType().indexOf(MG_STREAM_TYPE_IMAGE_STR) == BAD_INDEX))
					{
						insert_media_differences(current_media->get_differences());
						current_media = get_media(stream_id);
					}
				}

				if (current_media == nullptr)
				{
					current_media = NEW mg_sdp_media_base_t(m_log, m_term_id, stream_id);
					m_media_list.add(current_media);
					compare_media = false;
				}
				else
				{
					current_media->clear_differences_list();
				}
				// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.read -- new sdp media.", m_term_id);

				// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.read -- sdp media - add_media_line: \r\n%s.", m_term_id, item);
				if (current_media->add_media_line(item, compare_media))
				{
					one = true;
				}
				else
				{
					// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.read -- sdp media - add_media_line FAIL.", m_term_id);
				}
				// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.read -- push_back sdp media.", m_term_id);
			}
			else if (first_symbol_line == 'a')
			{
				if (current_media == nullptr)
				{
					if (item.indexOf("fingerprint") != BAD_INDEX)
					{
						if (read_line(item, compare_media))
						{
							one = true;
						}
					}

					// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.read -- current media NULL.", m_term_id);
					index_b = index + 1;
					index = spd_s.indexOf('\n', index_b);
					continue;
				}

				// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.read -- sdp media - add_media_line: \r\n%s.", m_term_id, item);
				if (current_media->add_media_line(item, compare_media))
				{
					one = true;
				}
				else
				{
					// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.read -- sdp media - add_media_line FAIL.", m_term_id);
				}
			}
			else
			{
				// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.read -- sdp media - read_line: \r\n%s.", m_term_id, item);
				if (read_line(item, compare_media))
				{
					one = true;
				}
				else
				{
					// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.read -- sdp media - read_line FAIL.", m_term_id);
				}
			}

			index_b = index + 1;
			index = spd_s.indexOf('\n', index_b);
			if (index == BAD_INDEX)
			{
				if (spd_s.getLength() > index_b)
				{
					index = spd_s.getLength();
				}
			}
		}

		if (current_media != nullptr)
		{
			insert_media_differences(current_media->get_differences());
		}

		current_media->filer_media_elements();

		return one;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_sdp_session_t::write_full(rtl::String& spd)
	{
		write_base(spd);

		for (int i = 0; i < m_media_list.getCount(); i++)
		{
			mg_sdp_media_base_t* media = m_media_list.getAt(i);
			if (media == nullptr)
			{
				continue;
			}

			spd << "\r\n";
			
			media->write(spd);
		}

		return true;
	}
	//------------------------------------------------------------------------------
	void mg_sdp_session_t::write_base(rtl::String& spd)
	{
		write_lines(spd);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_sdp_session_t::clear()
	{
		m_v_line.setEmpty();	// v = 0
		m_o_line.setEmpty();	// o = carol 28908764872 28908764872 IN IP4 100.3.6.6
		m_s_line.setEmpty();	// s = -
		m_t_line.setEmpty();	// t = 0 0
		m_c_line.setEmpty();	// c = IN IP4 192.0.2.4

		for (int i = 0; i < m_media_list.getCount(); i++)
		{
			mg_sdp_media_base_t* media = m_media_list.getAt(i);
			if (media == nullptr)
			{
				continue;
			}

			DELETEO(media);
			media = nullptr;
		}
		m_media_list.clear();

		if (m_fingerprint != nullptr)
		{
			DELETEO(m_fingerprint);
			m_fingerprint = nullptr;
		}

		m_address.parse("0.0.0.0");

		clear_bundle_param();

		clear_differences_list();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	const ip_address_t*	mg_sdp_session_t::get_address() const
	{
		return &m_address;
	}
	//------------------------------------------------------------------------------
	void mg_sdp_session_t::set_address(const ip_address_t* addr)
	{
		if (addr == nullptr)
		{
			return;
		}

		m_address.copy_from(addr);
		m_c_line.setEmpty();

		m_c_line.append("c=IN IP4 ");
		const char* ipaddr = m_address.to_string();
		m_c_line.append(ipaddr);
	}
	//------------------------------------------------------------------------------
	void mg_sdp_session_t::set_address(const char* addr)
	{
		if (addr == nullptr)
		{
			return;
		}

		m_address.parse(addr);

		m_c_line.setEmpty();
		m_c_line << "c=IN IP4 " << m_address.to_string();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_sdp_session_t::read_line(const rtl::String& string_line, bool compare)
	{
		if (string_line.isEmpty())
		{
			return false;
		}

		// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.read_line -- begin: \r\n%s.", m_term_id, string_line);

		string_line.trim(" \t");

		char first_symbol = string_line.front();
		// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.read_line -- first symbol: %c.", m_term_id, first_symbol);


		if (first_symbol == 'v')
		{
			return read_v_line(string_line, compare);
		}
		else if (first_symbol == 'o')
		{
			return read_o_line(string_line, compare);
		}
		else if (first_symbol == 's')
		{
			return read_s_line(string_line, compare);
		}
		else if (first_symbol == 't')
		{
			return read_t_line(string_line, compare);
		}
		else if (first_symbol == 'c')
		{
			return read_c_line(string_line, compare);
		}
		else if (first_symbol == 'a')
		{
			if (string_line.indexOf("extmap") != BAD_INDEX)
			{
				return true;
			}
			else if (string_line.indexOf("group") != BAD_INDEX)
			{
				if (compare)
				{
					if (rtl::String::compare(string_line, m_group_line, false) != 0)
					{
						mg_sdp_difference_t* diff = NEW mg_sdp_difference_t();
						diff->type = mg_sdp_difference_type::base;
						diff->line.append(string_line);
						diff->stream_id = 0;
						m_difference_list.add(diff);
					}
					return true;
				}

				m_group_line = string_line;
				return true;
			}
			else if (string_line.indexOf("msid") != BAD_INDEX)
			{
				if (compare)
				{
					if (rtl::String::compare(string_line, m_msid_line, false) != 0)
					{
						mg_sdp_difference_t* diff = NEW mg_sdp_difference_t();
						diff->type = mg_sdp_difference_type::base;
						diff->line.append(string_line);
						diff->stream_id = 0;
						m_difference_list.add(diff);
					}
					return true;
				}
				m_msid_line = string_line;
				return true;
			}
			else if (string_line.indexOf("fingerprint") != BAD_INDEX)
			{
				return read_fingerprint(string_line, false);
			}
		}

		// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.read_line -- FAIL.",m_term_id);
		return false;
	}
	//------------------------------------------------------------------------------
	void mg_sdp_session_t::write_lines(rtl::String& ss)
	{
		write_v_line(ss);
		write_o_line(ss);
		write_s_line(ss);
		write_t_line(ss);
		write_c_line(ss);

		if (!m_group_line.isEmpty())
		{
			m_group_line.trim();
			ss << "\r\n";
			ss << m_group_line;
		}

		if (!m_msid_line.isEmpty())
		{
			m_msid_line.trim();
			ss << "\r\n";
			ss << m_msid_line;
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_sdp_session_t::read_v_line(const rtl::String& string_line, bool compare)
	{
		if (m_v_line.isEmpty())
		{
			m_v_line = string_line;
		}

		if (compare)
		{
			if (rtl::String::compare(string_line, m_v_line, false) != 0)
			{
				mg_sdp_difference_t* diff = NEW mg_sdp_difference_t();
				diff->type = mg_sdp_difference_type::base;
				diff->line.append(string_line);
				diff->stream_id = 0;
				m_difference_list.add(diff);
			}
		}
		// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.read_v_line -- %s.", m_term_id, m_v_line);
		return true;
	}
	//------------------------------------------------------------------------------
	bool mg_sdp_session_t::read_o_line(const rtl::String& string_line, bool compare)
	{
		if (m_o_line.isEmpty())
		{
			m_o_line = string_line;
		}

		if (compare)
		{
			if (rtl::String::compare(string_line, m_o_line, false) != 0)
			{
				mg_sdp_difference_t* diff = NEW mg_sdp_difference_t();
				diff->type = mg_sdp_difference_type::base;
				diff->line.append(string_line);
				diff->stream_id = 0;
				m_difference_list.add(diff);
			}
		}

		// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.m_o_line -- %s.", m_term_id, m_o_line);
		return true;
	}
	//------------------------------------------------------------------------------
	bool mg_sdp_session_t::read_s_line(const rtl::String& string_line, bool compare)
	{
		if (m_s_line.isEmpty())
		{
			m_s_line = string_line;
		}

		if (compare)
		{
			if (rtl::String::compare(string_line, m_s_line, false) != 0)
			{
				mg_sdp_difference_t* diff = NEW mg_sdp_difference_t();
				diff->type = mg_sdp_difference_type::base;
				diff->line.append(string_line);
				diff->stream_id = 0;
				m_difference_list.add(diff);
			}
		}

		// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.m_s_line -- %s.", m_term_id, m_s_line);
		return true;
	}
	//------------------------------------------------------------------------------
	bool mg_sdp_session_t::read_t_line(const rtl::String& string_line, bool compare)
	{
		if (m_t_line.isEmpty())
		{
			m_t_line = string_line;
		}

		if (compare)
		{
			if (rtl::String::compare(string_line, m_t_line, false) != 0)
			{
				mg_sdp_difference_t* diff = NEW mg_sdp_difference_t();
				diff->type = mg_sdp_difference_type::base;
				diff->line.append(string_line);
				diff->stream_id = 0;
				m_difference_list.add(diff);
			}
		}
		// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.m_t_line -- %s.", m_term_id, m_t_line);
		return true;
	}
	//------------------------------------------------------------------------------
	bool mg_sdp_session_t::read_c_line(const rtl::String& string_line, bool compare)
	{
		if (!m_c_line.isEmpty())
		{
			return true;
		}

		if (compare)
		{
			if (rtl::String::compare(string_line, m_c_line, false) != 0)
			{
				mg_sdp_difference_t* diff = NEW mg_sdp_difference_t();
				diff->type = mg_sdp_difference_type::base;
				diff->line.append(string_line);
				diff->stream_id = 0;
				m_difference_list.add(diff);
			}

			return true;
		}

		m_c_line = string_line;
		// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.m_c_line -- %s.", m_term_id, m_c_line);


		int index_b = 0;
		int index = m_c_line.indexOf('=');

		if (index == BAD_INDEX)
			return false;

		rtl::String c = m_c_line.substring(0, index);

		// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.m_c_line -- parse: %s.", m_term_id, c);

		index_b = index + 1;
		index = m_c_line.indexOf(' ');

		if (index == BAD_INDEX)
			return false;

		rtl::String type = m_c_line.substring(index_b, index - index_b);
		// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.m_c_line -- parse: %s.", m_term_id, type);

		index_b = index + 1;
		index = m_c_line.indexOf(' ', index_b);

		if (index == BAD_INDEX)
			return false;

		rtl::String family = m_c_line.substring(index_b, index - index_b);
		// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.m_c_line -- parse: %s.", m_term_id, family);

		// ищем пробел после айпи
		int index_b_ip = index + 1;
		int index_ip = m_c_line.indexOf(' ', index_b_ip);
		rtl::String addr;
		if (index_ip == BAD_INDEX)
		{
			// значит после айпишника пробела нет.
			addr = m_c_line.substring(index_b_ip);
		}
		else
		{
			addr = m_c_line.substring(index_b_ip, index_ip - index_b_ip);
		}
		// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.m_c_line -- parse: %s.", m_term_id, addr);

		if ((addr.isEmpty()) || (addr.front() == '$'))
		{
			m_address.parse("0.0.0.0");
		}
		else
		{
			m_address.parse(addr);
		}

		// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.m_c_line -- ok.",m_term_id);
		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_sdp_session_t::write_v_line(rtl::String& ss)
	{
		ss << m_v_line;
		// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.m_v_line -- %s.", m_term_id, m_v_line);
	}
	//------------------------------------------------------------------------------
	void mg_sdp_session_t::write_o_line(rtl::String& ss)
	{
		if (!m_o_line.isEmpty())
		{
			ss << "\r\n";
			ss << m_o_line;
		}
		// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.m_o_line -- %s.", m_term_id, m_o_line);
	}
	//------------------------------------------------------------------------------
	void mg_sdp_session_t::write_s_line(rtl::String& ss)
	{
		if (!m_s_line.isEmpty())
		{
			ss << "\r\n";
			ss << m_s_line;
		}
		// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.m_s_line -- %s.", m_term_id, m_s_line);
	}
	//------------------------------------------------------------------------------
	void mg_sdp_session_t::write_t_line(rtl::String& ss)
	{
		if (!m_t_line.isEmpty())
		{
			ss << "\r\n";
			ss << m_t_line;
		}
		// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.m_t_line -- %s.", m_term_id, m_t_line);
	}
	//------------------------------------------------------------------------------
	void mg_sdp_session_t::write_c_line(rtl::String& ss)
	{
		if (!m_c_line.isEmpty())
		{
			ss << "\r\n";
			ss << m_c_line;
		}
		// //PLOG_SDP(LOG_PREFIX, "(%s) mg_sdp_session_t.m_c_line -- %s.", m_term_id, m_c_line);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_sdp_media_base_t* mg_sdp_session_t::get_media(int stream_id)
	{
		for (int i = 0; i < m_media_list.getCount(); i++)
		{
			mg_sdp_media_base_t* media = m_media_list.getAt(i);
			if (media == nullptr)
			{
				continue;
			}
			if (media->get_stream_id() == stream_id)
			{
				return media;
			}
		}

		return nullptr;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_sdp_session_t::is_empty()
	{
		return m_media_list.getCount() == 0;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_sdp_session_t::fill_bundle_param()
	{
		clear_bundle_param();

		for (int i = 0; i < m_media_list.getCount(); i++)
		{
			mg_sdp_media_base_t* media = m_media_list.getAt(i);
			if (media == nullptr)
			{
				continue;
			}

			if (i == 0)
			{
				m_group_line << "a=group:BUNDLE";
			}

			m_group_line << ' ' << media->get_mid_type();

			if (m_msid_line.isEmpty())
			{
				m_msid_line << "a=msid-semantic: WMS " << media->get_msid_semantic();
			}
		}

		return true;
	}
	//------------------------------------------------------------------------------
	void mg_sdp_session_t::clear_bundle_param()
	{
		m_group_line = rtl::String::empty;
		m_msid_line = rtl::String::empty;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_sdp_session_t::clear_differences_list()
	{
		for (int i = 0; i < m_difference_list.getCount(); i++)
		{
			mg_sdp_difference_t* diff = m_difference_list.getAt(i);
			if (diff == nullptr)
			{
				continue;
			}

			DELETEO(diff);
			diff = nullptr;
		}

		m_difference_list.clear();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_sdp_diff_list* mg_sdp_session_t::get_differences()
	{
		return &m_difference_list;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_sdp_session_t::apply_differences()
	{
		//mg_sdp_media_base_t* current_media = nullptr;

		for (int i = 0; i < m_difference_list.getCount(); i++)
		{
			mg_sdp_difference_t* diff = m_difference_list.getAt(i);
			if (diff == nullptr)
			{
				continue;
			}

			if (diff->type == mg_sdp_difference_type::base)
			{
				if (!read_line(diff->line, false))
				{
					return false;
				}
			}
		}

		for (int i = 0; i < m_media_list.getCount(); i++)
		{
			mg_sdp_media_base_t* media = m_media_list.getAt(i);
			if (media == nullptr)
			{
				continue;
			}

			if (!media->apply_differences())
			{
				return false;
			}
		}

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_sdp_session_t::insert_media_differences(const mg_sdp_diff_list* media_diff)
	{
		for (int i = 0; i < media_diff->getCount(); i++)
		{
			mg_sdp_difference_t* diff = media_diff->getAt(i);
			if (diff == nullptr)
			{
				continue;
			}
			mg_sdp_difference_t* new_diff = NEW mg_sdp_difference_t();
			new_diff->type = diff->type;
			new_diff->line.assign(diff->line);
			new_diff->stream_id = diff->stream_id;

			m_difference_list.add(new_diff);
		}

		return true;
	}
	//------------------------------------------------------------------------------
	// a=fingerprint:sha-256 8B:1A:7A:ED:F5:5C:D0:66:36:A6:3F:41:4C:F4:12:AF:A2:40:8B:FA:D9:69:F1:1B:0E:9F:DC:24:0B:74:F1:59
	//------------------------------------------------------------------------------
	bool mg_sdp_session_t::read_fingerprint(const rtl::String& media_line, bool compare)
	{
		if (m_fingerprint == nullptr)
		{
			m_fingerprint = NEW sdp_fingerprint_t();
		}

		m_fingerprint->parse(media_line);

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_sdp_session_t::set_fingerprint(sdp_fingerprint_t* fingerprint)
	{
		m_fingerprint = fingerprint;
	}
	//------------------------------------------------------------------------------
	sdp_fingerprint_t* mg_sdp_session_t::get_fingerprint()
	{
		return m_fingerprint;
	}
}
//------------------------------------------------------------------------------
