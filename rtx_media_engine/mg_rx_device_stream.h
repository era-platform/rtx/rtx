﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_rx_stream.h"

namespace mge
{
	//------------------------------------------------------------------------------
	// Однонаправленный поток из канала в тубы.
	//------------------------------------------------------------------------------
	class mg_rx_device_stream_t : public mg_rx_stream_t, public h248::IAudioDeviceInputCallback
	{
	public:
		mg_rx_device_stream_t(rtl::Logger* log, uint32_t id, const char* parent_id, MediaSession& session);
		virtual ~mg_rx_device_stream_t();

		virtual void deviceInput__dataReady(const uint8_t* data, uint32_t len, uint32_t ts);

	private:
		bool rx_data_stream_lock(const uint8_t* data, uint32_t len);
		bool activity_();

		virtual int rtcp_session_rx(const rtcp_packet_t* packet);

	private:
		//-----------------------------------------------------
		// переменные для работы с пакетами на отправку.

		uint32_t m_ssrc;
		uint16_t m_seq_num;				// порядковый номер пакета для отправки в ртп канал.
		uint16_t m_seq_num_last;		// номер последнего отправленного пакета в ртп канал.
		uint32_t m_timestamp;			// таймштамп пакета для отправки в ртп канал.
		uint32_t m_timestamp_last;		// таймштамп последнего отправленного пакета в ртп канал.
		uint32_t m_samples_last;		// последнее количество семплов которое было отправлено.
		uint32_t m_timestamp_step;
		std::atomic_flag m_rx_lock;
	};
}
//------------------------------------------------------------------------------
