﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_rx_stream.h"
#include "mg_fax.h"

namespace mge
{
	//------------------------------------------------------------------------------
	// 
	//------------------------------------------------------------------------------
	class mg_rx_fax_t38_stream_t : public mg_rx_stream_t, public mg_fax_handler
	{
	public:
		mg_rx_fax_t38_stream_t(rtl::Logger* log, uint32_t id, const char* parent_id, MediaSession& session);
		virtual ~mg_rx_fax_t38_stream_t();

	private:
		bool activity();

		//------------------------------------------------------------------------------
		// <<mg_fax_handler>>
		virtual void send(const char* data, int data_length);
		virtual void fax_end(bool error);
		//------------------------------------------------------------------------------

	private:
		std::atomic_flag m_rx_lock;
	};
}
//------------------------------------------------------------------------------
