﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_codec.h"
#include "std_logger.h"
#include "mmt_payload_format.h"
#include "mmt_payload_set.h"
#include <atomic>
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define TUBE_LOGGING  m_log	// && rtl::Logger::check_trace(TRF_FLAG8) потому что проверяется при создании файла лога,
							//если файла лога нет, то и писать некуда.
#define PLOG_TUBE_WRITE		if (TUBE_LOGGING)	m_log->log
#define PLOG_TUBE_WARNING	if (TUBE_LOGGING)	m_log->log_warning
#define PLOG_TUBE_ERROR		if (TUBE_LOGGING)	m_log->log_error

class rtcp_packet_t;


namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	class mg_media_element_t;
	class mg_media_element_list_t;

	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	enum class HandlerType
	{
		Simple = 0,
		AudioDecoder = 1,
		AudioEncoder = 2,
		ComfortNoise = 3,
		AudioResampler = 4,
		StereoResampler = 5,
		MonoResampler = 6,
		DTMF2833 = 7,
		ConfAudioInput = 8,
		ConfAudioOutput = 9,
		ConfAudioRecorder = 10,
		UDPTL_Recevier = 11,
		UDPTL_Transmitter = 12,
		VideoEncoder = 13,
		VideoDecoder = 14,
		ImageTransformer = 15,
		ConfVideoInput = 16,
		ConfVideoOutput = 17,
		ConfVideoRecorder = 18,
		AudioTimer = 19,
		AudioSplitter = 20
	};
	const char* HandlerType_toString(HandlerType type);

	class Tube;
	class TubeManager;
	class Direction;

	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	struct INextDataHandler
	{
		virtual void send(const INextDataHandler* out_handler, const rtp_packet* packet) = 0;
		virtual void send(const INextDataHandler* out_handler, const uint8_t* data, uint32_t len) = 0;
		virtual void send(const INextDataHandler* out_handler, const rtcp_packet_t* packet) = 0;

		virtual bool unsubscribe(const INextDataHandler* out_handler) = 0;
	};
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	class ME_DEBUG_API Tube : public INextDataHandler
	{
	public:
		Tube(HandlerType type, uint32_t id, rtl::Logger* log);
		virtual	~Tube();

		const uint32_t getId() const { return m_id; }
		const char* getName() const { return m_name; }
		HandlerType getType() { return m_type; }
		const char* getLogTag() { return m_logTag; }

		virtual bool init(mg_media_element_t* in, mg_media_element_t* out) = 0;
		virtual bool is_suited(uint16_t payload_id);
		virtual bool is_suited(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out);
		virtual bool setNextHandler(INextDataHandler* out_handler);

		virtual void send(const INextDataHandler* out_handler, const rtp_packet* packet);
		virtual void send(const INextDataHandler* out_handler, const uint8_t* data, uint32_t len);
		virtual void send(const INextDataHandler* out_handler, const rtcp_packet_t* packet);
		virtual bool unsubscribe(const INextDataHandler* out_handler);

		void release() { unsubscribe(this); }

		bool isNextHandler(const INextDataHandler* out_handler);

		INextDataHandler* getNextHandler();
		INextDataHandler* getNextHandler(bool* lock);

		virtual bool free();

		// Указать что на туб подписались.
		void set_free_in(bool in);

		uintptr_t get_tag() { return m_tag; }
		void set_tag(uintptr_t tag) { m_tag = tag; }

		void setLogTag(const char* tag) { strcpy(m_logTag, tag); }

	protected:
		std::atomic_flag m_lock;
		rtl::Logger* m_log;
		uint16_t m_payload_id;

		void makeName(const char* parentId, const char* tag) { m_name.setEmpty(); m_name << parentId << tag << m_id; }
	private:

		uint32_t m_id;
		rtl::String m_name;
		HandlerType m_type;
		char m_logTag[9];
		// флаг указывает что на текущий туб кто то подписан.
		bool m_in_free;
		bool m_out_free;

		INextDataHandler* m_nextHandler;
		uintptr_t m_tag;				// A tag is user data that can be attached to a tube object.
										// Сonference tubes use a tag to bind audio and video streams
	};
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	typedef rtl::PonterArrayT<Tube> TubeList;
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	extern rtl::Logger* g_mg_tube_log;
}
//------------------------------------------------------------------------------
