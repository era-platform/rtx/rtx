﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "std_logger.h"
#include "std_sys_env.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
#define DEFAULT_SSRC_CNAME_SIZE			24
#define DEFAULT_SSRC_MSLABEL_SIZE		36
#define DEFAULT_GUID_SIZE				64
//------------------------------------------------------------------------------
//a=ssrc:1061712719 cname:NCuce0OHjVRX4MAI
//a=ssrc:1061712719 msid:6n36IqVDOliFUE63N0W6CgBebhGRDxx8wWqS 281c0c2f-fd25-4b7e-a32c-de9d8dead668
//a=ssrc:1061712719 mslabel:6n36IqVDOliFUE63N0W6CgBebhGRDxx8wWqS
//a=ssrc:1061712719 label:281c0c2f-fd25-4b7e-a32c-de9d8dead668
//------------------------------------------------------------------------------
	class mg_sdp_ssrc_param_t
	{
		mg_sdp_ssrc_param_t(const mg_sdp_ssrc_param_t&);
		mg_sdp_ssrc_param_t&	operator=(const mg_sdp_ssrc_param_t&);

	public:
		mg_sdp_ssrc_param_t();
		~mg_sdp_ssrc_param_t();

		bool generate();
		void clear();
		bool copy_from(const mg_sdp_ssrc_param_t* param);

		bool add_ssrc_line(const rtl::String& media_line);
		bool write_ssrc_lines(rtl::String& ss);

		uint32_t get_ssrc();
		void set_ssrc(uint32_t ssrc);

		static uint32_t get_ssrc_in_line(const rtl::String& media_line);

		const char* get_cname();
		void set_cname(const char* cname);
		const char* get_mslabel();
		void set_mslabel(const char* mslabel);
		const char* get_label();
		void set_label(const char* label);

	private:
		uint32_t m_ssrc;
		rtl::String m_cname;
		rtl::String m_label;
		rtl::String m_mslabel;

		char m_work_buff[DEFAULT_SSRC_MSLABEL_SIZE + 1 + DEFAULT_GUID_SIZE];
	};
}
//------------------------------------------------------------------------------
