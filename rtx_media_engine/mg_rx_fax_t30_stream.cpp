﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_thread.h"
#include "mg_rx_fax_t30_stream.h"
#include "mg_session.h"

#include "logdef_stream.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_rx_fax_t30_stream_t::mg_rx_fax_t30_stream_t(rtl::Logger* log, uint32_t id, const char* parent_id, MediaSession& session) :
		mg_rx_stream_t(log, id, mg_rx_fax_t30_e, parent_id, session)
	{
		rtl::String name;
		name << session.getName() << "-rxft30s" << id;
		setName(name);
		setTag("RXS_FX30");

		m_ssrc_to_rtp = (rtl::DateTime::getTicks() + rand()) | 0x833000b0;
		m_seq_num_to_rtp = (uint16_t)(rtl::DateTime::getTicks() ^ 0x000000D5D);;
		m_seq_num_to_rtp_last = 0;
		m_timestamp_to_rtp = rtl::DateTime::getTicks() & 0x005D5D5D;
		m_timestamp_to_rtp_last = 0;

		m_rx_lock.clear();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_rx_fax_t30_stream_t::~mg_rx_fax_t30_stream_t()
	{
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_rx_fax_t30_stream_t::init(uint32_t payload_id, uint32_t sample_step)
	{
		m_payload_id = payload_id;
		m_samples_count = sample_step;

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_rx_fax_t30_stream_t::send(const char* data, int data_length)
	{
		if (data_length <= 0)
		{
			ERROR_MSG("zero length data!");
			return;
		}

		if (!activity())
		{
			return;
		}

		bool find = false;
		int count_tubes = getTubeCount();
		for (int i = 0; i < count_tubes; i++)
		{
			Tube* tube_at_list = getTube(i);
			if (tube_at_list == nullptr)
			{
				continue;
			}

			if ((tube_at_list->getType() == HandlerType::UDPTL_Recevier) ||
				(tube_at_list->getType() == HandlerType::UDPTL_Transmitter))
			{
				PRINT_FMT("sending %d bytes to tube '%s'.", data_length, tube_at_list->getName());
				tube_at_list->send(tube_at_list, (const uint8_t*)data, data_length);
				find = true;
				break;
			}
		}

		if (!find)
		{
			packetize((const uint8_t*)data, data_length);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_rx_fax_t30_stream_t::fax_end(bool error)
	{
		mg_rx_stream_t::getSession().mg_stream__event(NEW h248::FAX_EventParams(error));

		PRINT("event raised");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_rx_fax_t30_stream_t::activity()
	{
		if (mg_rx_stream_t::getState() == h248::TerminationStreamMode::SendOnly)
		{
			WARNING("stream is sendonly.");
			return false;
		}

		if (mg_rx_stream_t::getState() == h248::TerminationStreamMode::Inactive)
		{
			WARNING("stream is inactive.");
			return false;
		}
		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_rx_fax_t30_stream_t::packetize(const uint8_t* data, uint32_t len)
	{
		rtp_packet packet;
		packet.set_version(2);
		if ((m_seq_num_to_rtp_last == 0) || (m_timestamp_to_rtp_last == 0))
		{
			packet.set_marker(true);
		}

		packet.set_payload_type(uint8_t(m_payload_id));
		packet.set_ssrc(m_ssrc_to_rtp);
		packet.set_samples(m_samples_count);

		m_seq_num_to_rtp_last = m_seq_num_to_rtp;
		m_seq_num_to_rtp++;
		packet.set_sequence_number(m_seq_num_to_rtp);

		m_timestamp_to_rtp_last = m_timestamp_to_rtp;
		m_timestamp_to_rtp += m_samples_count;
		packet.set_timestamp(m_timestamp_to_rtp);

		packet.set_payload(data, len);

		Tube* firs_tube_at_list = getTube(0);
		if (firs_tube_at_list != nullptr)
		{
			//PRINT_FMT("sending %d bytes to tube '%s'", len, firs_tube_at_list->getName());
			firs_tube_at_list->send(firs_tube_at_list, &packet);
		}
	}
}
//------------------------------------------------------------------------------
