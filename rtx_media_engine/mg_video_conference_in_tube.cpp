/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_video_conference_in_tube.h"
#include "mg_media_element.h"

#include "logdef_tube.h"

namespace mge
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	mg_video_conference_in_tube_t::mg_video_conference_in_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id) :
		Tube(HandlerType::ConfVideoInput, id, log)
	{
		makeName(parent_id, "-conf-in-tu"); // Conference In Tube
		m_deleted_from_conference = true;
		m_subscriber_count = 0;

		m_videoMixer = nullptr;
		m_bindId = media::VideoFrameStream::EmptyId;

		rtl::res_counter_t::add_ref(g_mge_tube_counter);

		setLogTag("TUB-ICON");
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	mg_video_conference_in_tube_t::~mg_video_conference_in_tube_t()
	{
		unsubscribe(this);

		rtl::res_counter_t::release(g_mge_tube_counter);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void mg_video_conference_in_tube_t::send(const INextDataHandler* out_handler, const uint8_t* data, uint32_t length)
	{
		media::VideoImage* image = (media::VideoImage*)data;

		//PLOG_TUBE_WRITE(LOG_PREFIX, "push to conference (%d)-- ID(%s)...", image ? image->getImageSize() : 0, getName());

		if (m_videoMixer != nullptr && m_bindId != media::VideoFrameStream::EmptyId)
		{
			m_videoMixer->updateVideoStream(m_bindId, image);
		}

		DELETEO(image);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool mg_video_conference_in_tube_t::unsubscribe(const INextDataHandler* out_handler)
	{
		ENTER();

		bool result = Tube::unsubscribe(out_handler);

		if (m_subscriber_count > 0)
		{
			std_interlocked_dec(&m_subscriber_count);
		}

		PLOG_TUBE_WRITE(LOG_PREFIX, "unsubscribe -- ID(%s): %d end.", getName(), m_subscriber_count);
		return result;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool mg_video_conference_in_tube_t::free()
	{
		if (Tube::free())
		{
			return m_subscriber_count <= 0;
		}

		return false;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void mg_video_conference_in_tube_t::subscriber_add()
	{
		std_interlocked_inc(&m_subscriber_count);
		PRINT_FMT("count %d.", m_subscriber_count);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	uint64_t mg_video_conference_in_tube_t::get_subscribers_count()
	{
		return m_subscriber_count;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool mg_video_conference_in_tube_t::can_delete()
	{
		return m_deleted_from_conference;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void mg_video_conference_in_tube_t::add_to_conference()
	{
		m_deleted_from_conference = false;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void mg_video_conference_in_tube_t::deleted_from_conference()
	{
		m_deleted_from_conference = true;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool mg_video_conference_in_tube_t::init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out)
	{
		if (payload_el_in == nullptr)
		{
			ERROR_MSG("payload element is null.");
			return false;
		}
		
		m_payload_id = payload_el_in->get_payload_id();
		
		PRINT_FMT("payload in: %s %d", payload_el_in->get_element_name(), m_payload_id);

		m_termination_name = payload_el_in->get_termination_id();

		return true;
	}
}
//-----------------------------------------------
