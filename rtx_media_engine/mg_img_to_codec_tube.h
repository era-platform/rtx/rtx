/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_tube.h"

namespace mge
{

	//-----------------------------------------------
	// ����� �����������. 
	// �� ����� �����. �� ������ �����.
	// �� ����� pcm. �� ������ ������������ ������.
	//-----------------------------------------------
	class ME_DEBUG_API mg_img_to_codec_tube_t : public Tube
	{
		mg_img_to_codec_tube_t(const mg_img_to_codec_tube_t&);
		mg_img_to_codec_tube_t&	operator=(const mg_img_to_codec_tube_t&);

	public:
		mg_img_to_codec_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id);
		virtual	~mg_img_to_codec_tube_t();

		virtual bool init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out) override;
		virtual bool is_suited(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out) override;
		virtual void send(const INextDataHandler* out_handler, const rtp_packet* packet) override;
		virtual void send(const INextDataHandler* out_handler, const uint8_t* data, uint32_t len) override;

	private:
		int m_start_counter;
		uint16_t m_last_sequence_number;
		uint32_t m_ssrc;
		mg_video_encoder_t* m_encoder;
#if defined TEST_VIDEO_CODEC
		rtl::FileStream m_out;
#endif // TEST_VIDEO_CODEC

		void send_rtp(const void* rtp_payload, int payload_length, uint32_t ts, bool marker);
		static void rtx_video_encoder_cb_t(const void* rtp_payload, int payload_length, uint32_t ts, bool marker, void* user);
	};
}
//-----------------------------------------------
