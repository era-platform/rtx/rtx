﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "rtx_media_engine.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	struct mg_queue_block_t
	{
		bool ready;			// TRUE -- готов для записи, FALSE -- готов для чтения
		uint8_t buffer[80];
	};
	//------------------------------------------------------------------------------
	// Буфер (очередь) обмена без локов для двух потоков. (Один пишет другой читает).
	// Размер буферов для чтения и записи должны быть кратными 10 мс или 80 семплов (80/160 байт).
	// Поэтому блоки имеют размер 80 байт. Для PCM будут использоватся по 2 блока.
	//------------------------------------------------------------------------------
	class mg_free_lock_memory_queue_t
	{
		mg_free_lock_memory_queue_t(const mg_free_lock_memory_queue_t&);
		mg_free_lock_memory_queue_t& operator=(const mg_free_lock_memory_queue_t&);

	public:
		mg_free_lock_memory_queue_t();
		virtual ~mg_free_lock_memory_queue_t();

		void			create(int queue_length);
		void			reset();

		int				write(const uint8_t* data, int length);
		int				read(uint8_t* data, int size);

		// Количество записанных данных.
		int				get_available();
		int				get_free();

	private:
		mg_queue_block_t* get_writing_block();
		void			block_written();

		mg_queue_block_t* get_reading_block();
		void			block_read();

	private:

		mg_queue_block_t* m_queue;
		int				m_queue_size;
		int				m_write_idx;
		int				m_read_idx;

		volatile int32_t m_data_count;
	};
}
//------------------------------------------------------------------------------
