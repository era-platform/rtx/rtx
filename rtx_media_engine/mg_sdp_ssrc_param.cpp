﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_sdp_ssrc_param.h"
#include "mg_sdp_candidate.h"
#include "std_crypto.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_sdp_ssrc_param_t::mg_sdp_ssrc_param_t()
	{
		m_ssrc = 0;
		clear();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_sdp_ssrc_param_t::~mg_sdp_ssrc_param_t()
	{
		clear();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_sdp_ssrc_param_t::generate()
	{
		m_ssrc = (rtl::DateTime::getTicks() + rand()) | 0x833000b0;

		uint8_t tmp[DEFAULT_SSRC_CNAME_SIZE];
		crypto_get_random(tmp, DEFAULT_SSRC_CNAME_SIZE);
		Base64Encode(tmp, DEFAULT_SSRC_CNAME_SIZE, m_work_buff, MD5_HASH_HEX_SIZE);
		m_work_buff[16] = 0;
		m_cname.append(m_work_buff);

		guid_t gu = std_generate_guid();
		m_label.append(std_guid_to_string(gu, m_work_buff, sizeof(m_work_buff)));

		sdp_ice_candidate_t::generate_hex_text(m_work_buff, DEFAULT_SSRC_MSLABEL_SIZE);
		m_mslabel.append(m_work_buff);

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_sdp_ssrc_param_t::copy_from(const mg_sdp_ssrc_param_t* param)
	{
		if (param == nullptr)
		{
			return false;
		}

		m_ssrc = param->m_ssrc;
		m_cname = param->m_cname;
		m_label = param->m_label;
		m_mslabel = param->m_mslabel;

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_sdp_ssrc_param_t::clear()
	{
		m_cname = rtl::String::empty;
		m_label = rtl::String::empty;
		m_mslabel = rtl::String::empty;

		memset(m_work_buff, 0, DEFAULT_SSRC_MSLABEL_SIZE + 1 + DEFAULT_GUID_SIZE);
	}
	//------------------------------------------------------------------------------
	// a=ssrc:1061712719 cname:NCuce0OHjVRX4MAI
	// a=ssrc:1061712719 msid:6n36IqVDOliFUE63N0W6CgBebhGRDxx8wWqS 281c0c2f-fd25-4b7e-a32c-de9d8dead668
	// a=ssrc:1061712719 mslabel:6n36IqVDOliFUE63N0W6CgBebhGRDxx8wWqS
	// a=ssrc:1061712719 label:281c0c2f-fd25-4b7e-a32c-de9d8dead668
	//    or firefox
	// a=ssrc:1890385820 cname:{a858d452-49bc-454d-b8b9-9216952a7c24\}
	//------------------------------------------------------------------------------
	bool mg_sdp_ssrc_param_t::add_ssrc_line(const rtl::String& media_line)
	{
		if (media_line.isEmpty())
		{
			return false;
		}

		rtl::String ssrc_line(media_line);

		//--------------------a=ssrc---------------------
		int index_b = 0;
		int index = ssrc_line.indexOf(':');
		if (index == BAD_INDEX)
		{
			return false;
		}
		rtl::String a = ssrc_line.substring(0, index);

		//--------------------ssrc-----------------------
		index_b = index + 1;
		index = ssrc_line.indexOf(' ');
		if (index == BAD_INDEX)
		{
			return false;
		}
		rtl::String ssrc_str = ssrc_line.substring(index_b, index - index_b);
		m_ssrc = (uint32_t)strtoul(ssrc_str, nullptr, 10);

		//-----------------type-line---------------------
		index_b = index + 1;
		index = ssrc_line.indexOf(':', index_b);
		if (index == BAD_INDEX)
		{
			return false;
		}
		rtl::String type = ssrc_line.substring(index_b, index - index_b);
		index_b = index + 1;

		//-------------------cname-----------------------
		if (type.indexOf("cname") != BAD_INDEX)
		{
			m_cname = ssrc_line.substring(index_b);
		}
		//-------------------msid-----------------------
		else if (type.indexOf("msid") != BAD_INDEX)
		{
			index = ssrc_line.indexOf(' ', index_b + 1);
			if (index == BAD_INDEX)
			{
				return false;
			}
			m_mslabel = ssrc_line.substring(index_b, index - index_b);
			m_label = ssrc_line.substring(index + 1);
		}
		//-----------------mslabel----------------------
		else if (type.indexOf("mslabel") != BAD_INDEX)
		{
			m_mslabel = ssrc_line.substring(index_b);
		}
		//------------------label-----------------------
		else if (type.indexOf("label") != BAD_INDEX)
		{
			m_label = ssrc_line.substring(index_b);
		}

		return true;
	}
	//------------------------------------------------------------------------------
	uint32_t mg_sdp_ssrc_param_t::get_ssrc_in_line(const rtl::String& media_line)
	{
		if (media_line.isEmpty())
		{
			return 0;
		}

		rtl::String ssrc_line(media_line);

		//--------------------a=ssrc---------------------
		int index_b = 0;
		int index = ssrc_line.indexOf(':');
		if (index == BAD_INDEX)
		{
			return 0;
		}
		rtl::String a = ssrc_line.substring(0, index);

		//--------------------ssrc-----------------------
		index_b = index + 1;
		index = ssrc_line.indexOf(' ');
		if (index == BAD_INDEX)
		{
			return 0;
		}
		rtl::String ssrc_str = ssrc_line.substring(index_b, index - index_b);
		return (uint32_t)strtoul(ssrc_str, nullptr, 10);
	}
	//------------------------------------------------------------------------------
	// a=ssrc:1061712719 cname:NCuce0OHjVRX4MAI
	// a=ssrc:1061712719 msid:6n36IqVDOliFUE63N0W6CgBebhGRDxx8wWqS 281c0c2f-fd25-4b7e-a32c-de9d8dead668
	// a=ssrc:1061712719 mslabel:6n36IqVDOliFUE63N0W6CgBebhGRDxx8wWqS
	// a=ssrc:1061712719 label:281c0c2f-fd25-4b7e-a32c-de9d8dead668
	//------------------------------------------------------------------------------
	bool mg_sdp_ssrc_param_t::write_ssrc_lines(rtl::String& ss)
	{
		if ((m_cname.isEmpty()) || (m_label.isEmpty()) || (m_mslabel.isEmpty()))
		{
			return false;
		}

		ss << "\r\n";

		//--------------------------------------------------------------------------
		// a=ssrc:1061712719 cname:NCuce0OHjVRX4MAI
		ss << "a=ssrc:" << m_ssrc << " cname:" << m_cname << "\r\n";
		//--------------------------------------------------------------------------

		//--------------------------------------------------------------------------
		// a=ssrc:1061712719 msid:6n36IqVDOliFUE63N0W6CgBebhGRDxx8wWqS 281c0c2f-fd25-4b7e-a32c-de9d8dead668
		ss << "a=ssrc:" << m_ssrc << " msid:" << m_mslabel << ' ' << m_label << "\r\n";
		//--------------------------------------------------------------------------

		//--------------------------------------------------------------------------
		// a=ssrc:1061712719 mslabel:6n36IqVDOliFUE63N0W6CgBebhGRDxx8wWqS
		ss << "a=ssrc:" << m_ssrc << " mslabel:" << m_mslabel << "\r\n";
		//--------------------------------------------------------------------------

		//--------------------------------------------------------------------------
		// a=ssrc:1061712719 label:281c0c2f-fd25-4b7e-a32c-de9d8dead668
		ss << "a=ssrc:" << m_ssrc << " label:" << m_label;
		//--------------------------------------------------------------------------

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	uint32_t mg_sdp_ssrc_param_t::get_ssrc()
	{
		return m_ssrc;
	}
	//------------------------------------------------------------------------------
	void mg_sdp_ssrc_param_t::set_ssrc(uint32_t ssrc)
	{
		m_ssrc = ssrc;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	const char* mg_sdp_ssrc_param_t::get_mslabel()
	{
		return m_mslabel;
	}
	//------------------------------------------------------------------------------
	void mg_sdp_ssrc_param_t::set_mslabel(const char* mslabel)
	{
		m_mslabel = rtl::String::empty;
		m_mslabel.append(mslabel);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	const char* mg_sdp_ssrc_param_t::get_cname()
	{
		return m_cname;
	}
	//------------------------------------------------------------------------------
	void mg_sdp_ssrc_param_t::set_cname(const char* cname)
	{
		m_cname = rtl::String::empty;
		m_cname.append(cname);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	const char* mg_sdp_ssrc_param_t::get_label()
	{
		return m_label;
	}
	//------------------------------------------------------------------------------
	void mg_sdp_ssrc_param_t::set_label(const char* label)
	{
		m_label = rtl::String::empty;
		m_label.append(label);
	}
}
//------------------------------------------------------------------------------
