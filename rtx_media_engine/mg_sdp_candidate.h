/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "std_array_templates.h"
#include "net/ip_address.h"

namespace mge
{
	//--------------------------------------
	// �������������� component id
	//--------------------------------------
#define SDP_ICE_CANDIDATE_RTP		1
#define SDP_ICE_CANDIDATE_RTCP		2

#define SDP_ICE_UFRAG_SIZE			16
#define SDP_ICE_PWD_SIZE			24
//--------------------------------------
// ������
//   a=candidate:foundation SP component-id SP transport SP priority SP connection-address SP port SP cand-type [SP rel-addr] [SP rel-port]
//   a=candidate:1 1 UDP 2130706431 10.0.1.1 8998 typ host
//   a=candidate:2 1 UDP 1694498815 192.0.2.3 45664 typ srflx raddr 10.0.1.1 rport 8998
//--------------------------------------
	enum sdp_ice_candidate_type_t
	{
		sdp_ice_candidate_host_e,
		sdp_ice_candidate_srflx_e,
		sdp_ice_candidate_prflx_e,
		sdp_ice_candidate_relay_e,
		sdp_ice_candidate_unknown_e
	};
	const char* get_sdp_ice_candidate_type_name(sdp_ice_candidate_type_t type);
	sdp_ice_candidate_type_t get_sdp_ice_candidate_type(const char* line, int len = -1);
	//--------------------------------------
	//
	//--------------------------------------
	enum sdp_ice_transport_type_t
	{
		sdp_ice_transport_udp_e,
		sdp_ice_transport_tcp_e,
		sdp_ice_transport_tls_e,
		sdp_ice_transport_ws_e,
		sdp_ice_transport_wss_e,
		sdp_ice_transport_invalid_e,
	};
	const char* get_sdp_ice_transport_type_name(sdp_ice_transport_type_t type);
	sdp_ice_transport_type_t get_sdp_ice_transport_type(const char* line, int len = -1);
	//--------------------------------------
	//
	//--------------------------------------
	class sdp_ice_candidate_t
	{
		char* m_foundation;
		uint32_t m_component_id;
		sdp_ice_transport_type_t m_transport_type;
		uint32_t m_priority;
		sdp_ice_candidate_type_t m_cand_type;
		ip_address_t m_conn_address;
		uint16_t m_conn_port;

		struct sdp_ice_param_t
		{
			char* name;
			char* value;
		};

		rtl::ArrayT<sdp_ice_param_t> m_ice_params;

	public:
		sdp_ice_candidate_t();
		sdp_ice_candidate_t(const sdp_ice_candidate_t& candidate);
		~sdp_ice_candidate_t();

		bool parse(const char* sdp_line);
		int write_to(char* buffer, int size);

		sdp_ice_candidate_t& operator = (const sdp_ice_candidate_t& candidate);

		void set_foundation(const char* foundation);
		const char* generate_foundation();
		const char* get_foundation() const { return m_foundation; }

		void set_component_id(uint32_t comp_id) { m_component_id = comp_id; }
		uint32_t get_component_id() const { return m_component_id; }

		void set_transport(sdp_ice_transport_type_t transport) { m_transport_type = transport; }
		sdp_ice_transport_type_t get_transport() const { return m_transport_type; }

		void set_priority(uint32_t priority) { m_priority = priority; }
		uint32_t get_priority() const { return m_priority; }

		void set_type(sdp_ice_candidate_type_t type) { m_cand_type = type; }
		sdp_ice_candidate_type_t getType() const { return m_cand_type; }

		void set_conn_address(const ip_address_t* address, uint16_t port) { m_conn_address.copy_from(address); m_conn_port = port; }
		const ip_address_t* get_conn_address() const { return &m_conn_address; }
		uint16_t get_conn_port() const { return m_conn_port; }

		int get_ice_param_count() const { return m_ice_params.getCount(); }
		const sdp_ice_param_t& get_ice_param_at(int index) const { return m_ice_params.getAt(index); }
		void add_ice_param(const char* name, const char* value);

		/// static utilites
		static uint32_t generate_priority(sdp_ice_candidate_type_t type, uint16_t local_pref, bool is_rtp);
		static char* compute_foundation();
		static char* generate_ufrag();
		static char* generate_pwd();
		//--------------------------------------
		// Abramov 12.02.2015 for test vp8 ������� - �������
		static void generate_hex_text(char* tmp, uint32_t size);
		//--------------------------------------
	};
}
//--------------------------------------
