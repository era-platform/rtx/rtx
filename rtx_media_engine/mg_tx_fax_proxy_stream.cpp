﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_thread.h"
#include "mg_tx_fax_proxy_stream.h"
#include "mg_session.h"

#include "logdef_stream.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_tx_fax_proxy_stream_t::mg_tx_fax_proxy_stream_t(rtl::Logger* log, uint32_t id, uint32_t parent_id, const char* term_id) :
		mg_tx_stream_t(log, id, mg_tx_fax_proxy_e, term_id)
	{
		m_rtp = nullptr;

		rtl::String name;
		name << term_id << "-s" << parent_id << "-txfps" << id;
		mg_tx_stream_t::setName(name);
		mg_tx_stream_t::setTag("TXS_FXP");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_tx_fax_proxy_stream_t::~mg_tx_fax_proxy_stream_t()
	{
		if (m_rtp != nullptr)
		{
			m_rtp->dec_tx_subscriber(0);
		}
		m_rtp = nullptr;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_tx_fax_proxy_stream_t::send(const INextDataHandler* out_handler, const rtp_packet* packet)
	{
		if (checkAndLock())
		{
			WARNING("stream locked!");
			return;
		}

		send_packet_to_rtp_lock(out_handler, packet);

		releaseLock();
	}
	//------------------------------------------------------------------------------
	void mg_tx_fax_proxy_stream_t::send(const INextDataHandler* out_handler, const uint8_t* data, uint32_t len)
	{
		if (checkAndLock())
		{
			WARNING("stream locked!");
			return;
		}

		send_data_to_rtp_lock(out_handler, data, len);

		releaseLock();
	}
	//------------------------------------------------------------------------------
	void mg_tx_fax_proxy_stream_t::send(const INextDataHandler* out_handler, const rtcp_packet_t* packet)
	{
		if (checkAndLock())
		{
			WARNING("stream locked!");
			return;
		}

		send_ctrl_to_rtp_lock(out_handler, packet);

		releaseLock();
	}
	//------------------------------------------------------------------------------
	void mg_tx_fax_proxy_stream_t::send_packet_to_rtp_lock(const INextDataHandler* out_handler, const rtp_packet* packet)
	{
		if (packet == nullptr)
		{
			ERROR_MSG("packet is null!");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::Loopback)
		{
			WARNING("stream mode is loopback!");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::Inactive)
		{
			WARNING("stream mode is inactive!");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::RecvOnly)
		{
			WARNING("stream mode is recvonly!");
			return;
		}

		if (m_rtp == nullptr)
		{
			ERROR_MSG("rtp handler is null!");
			return;
		}

		m_rtp->tx_packet_channel(packet, &m_remote_address);
	}
	//------------------------------------------------------------------------------
	void mg_tx_fax_proxy_stream_t::send_data_to_rtp_lock(const INextDataHandler* out_handler, const uint8_t* data, uint32_t len)
	{
		if (data == nullptr)
		{
			ERROR_MSG("data is null!");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::Loopback)
		{
			WARNING("stream mode is loopback!");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::Inactive)
		{
			WARNING("stream mode is inactive!");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::RecvOnly)
		{
			WARNING("stream mode is recvonly!");
			return;
		}

		if (m_rtp == nullptr)
		{
			ERROR_MSG("rtp handler is null!");
			return;
		}

		m_rtp->tx_data_channel(data, len, &m_remote_address);
	}
	//------------------------------------------------------------------------------
	void mg_tx_fax_proxy_stream_t::send_ctrl_to_rtp_lock(const INextDataHandler* out_handler, const rtcp_packet_t* packet)
	{
		if (packet == nullptr)
		{
			ERROR_MSG("packet is null!");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::Loopback)
		{
			WARNING("stream mode is loopback!");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::Inactive)
		{
			WARNING("stream mode is inactive!");
			return;
		}

		if (mg_tx_stream_t::getState() == h248::TerminationStreamMode::RecvOnly)
		{
			WARNING("stream mode is recvonly!");
			return;
		}

		if (m_rtp == nullptr)
		{
			ERROR_MSG("rtp handler is null!");
			return;
		}

		//m_rtp->tx_ctrl_channel(this, packet, &m_remote_address);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------	
	void mg_tx_fax_proxy_stream_t::set_channel_event(i_tx_channel_event_handler* rtp)
	{
		if (!isLocked())
		{
			WARNING("stream unlocked!");
			return;
		}

		if (rtp == nullptr)
		{
			if (m_rtp != nullptr)
			{
				m_rtp->dec_tx_subscriber(0);
			}
		}
		else
		{
			if (m_rtp == nullptr)
			{
				rtp->inc_tx_subscriber(0);
			}
			else
			{
				if (m_rtp != rtp)
				{
					m_rtp->dec_tx_subscriber(0);
					rtp->inc_tx_subscriber(0);
				}
			}
		}
		m_rtp = rtp;
	}
}
//------------------------------------------------------------------------------
