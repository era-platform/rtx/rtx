﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "mmt_timer.h"
#include "mg_audio_mixer.h"
#include <atomic>

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	class mg_audio_conference_in_tube_t;
	class mg_audio_conference_out_tube_t;
	class mg_conference_member_t;
	struct INextDataHandler;
	class Tube;

	//------------------------------------------------------------------------------
	// Микшер конференции.
	// Обладает своим потоком. Который с определенной переодичностью пробегает по списку
	// conference_in_tube'ов, собирает накопившеся данные и микширут их.
	// Далее пробегает по списку conference_out_tube'ов и передает им микшированные данные.
	//------------------------------------------------------------------------------
	class AudioConferenceController : public media::Metronome
	{
	private:
		rtl::String m_name;
		rtl::Logger* m_log;
		mg_conference_member_t* m_member_first;
		mg_conference_member_t* m_member_last;
		rtl::ArrayT<mg_conference_member_t*> m_member_list;
		std::atomic_flag m_lock;
		uint32_t m_start_time;		// время начала отправки
		uint32_t m_tick_counter;		// счетчик тиков таймера
		uint32_t m_max_processing_time;
		volatile bool m_mixer_work;		// флаг показывает что пришел поток таймера и микширует данные.
		volatile uint32_t m_mixer_counter;	// счетчик определения прохождения потока микшера.
		AudioMixerMember* m_mixer;
		uint32_t m_audio_frame_size;
		int m_mixalgorithm;
		int m_ptime;
		uint32_t m_frequency;
		static const uint32_t m_sumvoice_buff_size = 30 * 32000 / 1000;
		double m_sumvoice_buff[m_sumvoice_buff_size];
		INextDataHandler* m_recorder;

	private:
		virtual	void metronomeTicks();

		const char* getName() { return m_name; }
		mg_conference_member_t* getFirst();
		void setFirst(mg_conference_member_t* member);
		void setNext(mg_conference_member_t* member);
		int contains(mg_audio_conference_in_tube_t* inTube, mg_audio_conference_out_tube_t* outTube, mg_conference_member_t** member);
		// Ждем пока флаг занятый другим потоком освободится и занимаем его.
		bool lock();
		// Освобождаем флаг.
		void unlock();
		// Собираем все звуки, микшируем и отправляем по каналам.
		void exchangeVoices();

	public:
		AudioConferenceController(rtl::Logger* log, const char* parent_id);
		~AudioConferenceController();

		void setRecorderHandler(INextDataHandler* out_handler) { m_recorder = out_handler; }
		// sample_rate: частота работы микшера.
		// ptime: период отправки пакетов.
		// mixing_algorithm: 0-просто сложение, 1-линейный модуль, 2-круг никитинский, 3-круг шамилича.
		bool init(uint32_t frequency, uint32_t ptime, uint32_t mixingAlgorithm);
		bool addMember(Tube* inTube, Tube* outTube, const char* termId);
	};
}
//-----------------------------------------------
