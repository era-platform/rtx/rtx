﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "std_logger.h"
#include "mg_sdp_candidate.h"
#include "rtx_media_engine.h"

class rtp_channel_t;

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	class mg_sdp_ice_param_t
	{
		mg_sdp_ice_param_t(const mg_sdp_ice_param_t&);
		mg_sdp_ice_param_t&	operator=(const mg_sdp_ice_param_t&);

	public:
		mg_sdp_ice_param_t();
		~mg_sdp_ice_param_t();

		bool generate(::rtp_channel_t* channel);
		bool copy_from(const mg_sdp_ice_param_t* param);
		void clear();

		bool add_ice_line(const rtl::String& media_line);
		bool write_ice_lines(rtl::String& ss);

		const char* get_ufrag();
		void set_ufrag(const char* ufrag);
		const char* get_pwd();
		void set_pwd(const char* pwd);

		bool compare_sdp_ice_line(const rtl::String& media_line);

		sdp_ice_candidate_t* get_top_candidate(::rtp_channel_t* channel);

	private:
		rtl::String m_ice_ufrag;
		rtl::String m_ice_pwd;

		rtl::ArrayT<sdp_ice_candidate_t*> m_candidate_list;
	};
}
//------------------------------------------------------------------------------
