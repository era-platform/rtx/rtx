﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtp_packet.h"
#include "rtcp_packet.h"
#include "rtcp_monitor.h"
#include "mg_tube.h"
#include "mg_sdp_base.h"
#include "rtp_statistics.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	enum mg_tx_stream_type_t
	{
		mg_tx_simple_audio_e,
		mg_tx_device_audio_e,
		mg_tx_dummy_e,
		mg_tx_simple_video_e,
		mg_tx_webrtc_video_e,
		mg_tx_fax_proxy_e,
		mg_tx_fax_t30_e,
		mg_tx_fax_t38_e,
		mg_tx_ivr_e,
	};
	const char* mg_tx_stream_type_toString(mg_tx_stream_type_t type);
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	class mg_tx_stream_t : public INextDataHandler, public rtcp_monitor_event_handler_tx_t
	{
	public:
		mg_tx_stream_t(rtl::Logger* log, uint32_t id, mg_tx_stream_type_t type, const char* term_id);
		virtual ~mg_tx_stream_t();

		virtual bool mg_stream_set_LocalControl_Mode(h248::TerminationStreamMode mode);
		virtual bool mg_stream_set_LocalControl_ReserveValue(bool value);
		virtual bool mg_stream_set_LocalControl_ReserveGroup(bool value);
		virtual bool mg_stream_set_LocalControl_Property(const h248::Field* LC_property);

		//------------------------------------------------------------------------------
		// << INextDataHandler >>
		virtual void send(const INextDataHandler* out_handler, const rtp_packet* packet) = 0;
		virtual void send(const INextDataHandler* out_handler, const uint8_t* data, uint32_t len) = 0;
		virtual void send(const INextDataHandler* out_handler, const rtcp_packet_t* packet) = 0;

		virtual bool unsubscribe(const INextDataHandler* out_handler);
		//------------------------------------------------------------------------------

		uint32_t getId() const { return m_id; }
		const char* getName() const { return m_name; }
		const char* getTag() const { return m_logTag; }
		mg_tx_stream_type_t getType() const { return m_type; }

		// пометить туб от которого зависит текущий стрим. (туб который ссылается на текущий стрим).
		void mark_tube(Tube* tube);
		// вызываем функцию туба что б он отписал текущий стрим.
		void disconnected_from_tubes();

		// для правильной очистки тубов нужно знать этот туб.
		void set_conference_out_tube(Tube* tube) { m_conference_out_tube = tube; }


		virtual bool lock();
		virtual void unlock();

		virtual void setMediaParams(mg_sdp_media_base_t& media);
		mg_sdp_media_base_t& getMediaParams() { return m_media_sdp; }

		// сбрасываем счетчики пересчета таймштампов при новом конекте.
		virtual void reset_before_new_connect();

		void set_rtcp_monitor(rtcp_monitor_t* monitor) { m_rtcp_monitor = monitor; }
		void add_statistics(stream_statistics_t* stat) { m_stat = stat; }

		// событие получения первого пакета от пира
		virtual void onReceiverStarted();

	private:
		//------------------------------------------------------------------------------
		// << rtcp_monitor_event_handler_tx_t >>
		virtual int rtcp_session_tx(const rtcp_packet_t* packet) override;
		//------------------------------------------------------------------------------

	protected:
		void setName(const char* name) { m_name = name; }
		void setTag(const char* tag) { strcpy(m_logTag, tag); }
		h248::TerminationStreamMode getState() { return m_local_control.stream_mode; }
		bool isLocked() { return m_main_lock; }
		bool checkAndLock() { return m_tx_lock.test_and_set(std::memory_order_acquire); }
		void releaseLock() { m_tx_lock.clear(std::memory_order_release); }

		bool rtcp_analize(rtcp_packet_t* packet);
		void rtcp_analize(const rtp_packet* packet);

		void stat_analize(const rtp_packet* packet);
		void stat_out_of_order();
		void stat_lost();
		void stat_invalid();
		void stat_filtered();


	protected:
		rtl::Logger* m_log;
	private:
		uint32_t m_id;
		rtl::String m_name;
		char m_logTag[9];
		mg_tx_stream_type_t	m_type;

		volatile bool m_main_lock;
		std::atomic_flag m_tx_lock;

		// структура контроля - описание как ведет себя стрим, в каком он состоянии.. и т.д.
		volatile h248::TerminationLocalControl m_local_control;
		// список тубов которые ссылаются на текущий стрим.
		// Нужен для того что бы информировать их что стрим удаляется.
		rtl::ArrayT<Tube*> m_rx_tubes;
		// Список медиа элементов с которым работает данный стрим.
		mg_sdp_media_base_t m_media_sdp;
		rtcp_monitor_t* m_rtcp_monitor;
		stream_statistics_t* m_stat;
		// если попадаем в конференцию, то нужно знать при очистке.
		Tube* m_conference_out_tube;
	};
}
//------------------------------------------------------------------------------
