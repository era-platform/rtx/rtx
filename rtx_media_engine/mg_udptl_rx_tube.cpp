﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_udptl_rx_tube.h"
#include "mg_media_element.h"

#include "logdef_tube.h"

//--------------------------------------
//
//--------------------------------------
#define UDPTL_BUF_MASK 0x0000000F

namespace mge
{
	//--------------------------------------
	//
	//--------------------------------------
	mg_udptl_rx_tube_t::mg_udptl_rx_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id) :
		Tube(HandlerType::UDPTL_Recevier, id, log)
	{
		makeName(parent_id, "-udptlrxtu"); // udptl rx Tube

		rtl::res_counter_t::add_ref(g_mge_tube_counter);

		setLogTag("TUB-RXFX");

		m_rx_seq_no = 0;
		m_rx_queue = nullptr;
		m_rx_queue_size = 0;
		m_rx_buffer = nullptr;

		m_rx_queue = NEW udptl_rx_frame_t[UDPTL_ECM_BUFFER_SIZE];
		memset(m_rx_queue, 0, sizeof(udptl_rx_frame_t) * UDPTL_ECM_BUFFER_SIZE);
		m_rx_buffer = NEW udptl_rx_frame_fec_t[UDPTL_ECM_BUFFER_SIZE];
		memset(m_rx_buffer, 0, sizeof(udptl_rx_frame_fec_t) * UDPTL_ECM_BUFFER_SIZE);

	}
	//--------------------------------------
	//
	//--------------------------------------
	mg_udptl_rx_tube_t::~mg_udptl_rx_tube_t()
	{
		unsubscribe(this);

		if (m_rx_queue != nullptr)
		{
			DELETEAR(m_rx_queue);
			m_rx_queue = nullptr;
			m_rx_queue = 0;
		}

		if (m_rx_buffer != nullptr)
		{
			DELETEAR(m_rx_buffer);
			m_rx_buffer = nullptr;
		}

		rtl::res_counter_t::release(g_mge_tube_counter);
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool mg_udptl_rx_tube_t::init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out)
	{
		if (payload_el_in == nullptr)
		{
			PLOG_TUBE_ERROR(LOG_PREFIX, "mg_udptl_rx_tube_t::init -- payload element is null.");
			return false;
		}
		m_payload_id = payload_el_in->get_payload_id();
		PLOG_TUBE_WRITE(LOG_PREFIX, "mg_udptl_rx_tube_t::init -- payload element: %d", m_payload_id);
		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void mg_udptl_rx_tube_t::send(const INextDataHandler* out_handler, const rtp_packet* packet)
	{
		return;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void mg_udptl_rx_tube_t::send(const INextDataHandler* out_handler, const uint8_t* data, uint32_t length)
	{
		if (!parse_rx_packet(data, length))
		{
			PLOG_TUBE_WARNING("UDPTLS", "stream %s : RAW PACKET HAS WRONG FORMAT", getName());
			return;
		}

		// передадим пакеты дальше на обработку
		for (int i = 0; i < m_rx_queue_size; i++)
		{
			udptl_rx_frame_t* frame = &m_rx_queue[i];
			if (frame->ifp_length < 0)
				break;

			PLOG_TUBE_WRITE("UDPTL", "pass ifp packet sn:%u l:%d", frame->seq_no, frame->ifp_length);
			INextDataHandler* out = getNextHandler();
			if (out != nullptr)
			{
				// !! George !! передается фрейм вместо ifp пакета для передачи порядкового номера
				out->send(this, (const uint8_t*)frame, frame->ifp_length);
			}
		}

		m_rx_seq_no = m_rx_queue[m_rx_queue_size - 1].seq_no;
	}
	//--------------------------------------
	// Private routines
	//--------------------------------------
	//
	//--------------------------------------
	bool mg_udptl_rx_tube_t::parse_rx_packet(const uint8_t* packet, int length)
	{
		int				index = 2;
		int				seq_no;
		const uint8_t*	ifp;
		int				ifp_length;

		//memset(m_rx_queue, 0, sizeof(m_rx_queue)); ??
		memset(m_rx_queue, 0, sizeof(udptl_rx_frame_t) * UDPTL_ECM_BUFFER_SIZE);
		m_rx_queue_size = 0;

		// Decode sequense number -> 2 bytes
		if (length < 3)
		{
			WARNING_FMT("Can't parse IFP packet length! index:%d + 1> length:%d", index, length);
			return false;
		}

		seq_no = ntohs(*(const uint16_t*)packet);

		// parse primary packet
		if (!parse_frame(packet, length, &index, &ifp, &ifp_length))
		{
			WARNING("Can't parse primary IFP packet!");
			return false;
		}
		
		PRINT_FMT("Primary ifp packet parsed seq_no:%d ifp:%p length:%d", seq_no, ifp, ifp_length);

		// check error recovery flag
		if (index + 1 > length)
		{
			WARNING_FMT("Can't parse Error recovery info! index:%d + 1 > length:%d", index, length);
			return false;
		}

		// t38UDPfec error correction mode
		if (packet[index++] & 0x80)
		{
			if (!parse_rx_packet_fec(seq_no, ifp, ifp_length, packet, length, index))
				return false;
		}
		else // t38UDPRedundancy error correction mode
		{
			if (!parse_rx_packet_red(seq_no, packet, length, index))
				return false;
		}

		if (seq_no >= m_rx_seq_no - 1)
		{
			PRINT_FMT("write top ifp packet to queue seq_no:%d ifp:%p length:%d", seq_no, ifp, ifp_length);

			m_rx_queue[m_rx_queue_size].seq_no = seq_no;
			m_rx_queue[m_rx_queue_size].ifp = ifp;
			m_rx_queue[m_rx_queue_size++].ifp_length = ifp_length;
		}
		else
		{
			WARNING_FMT("Primary ifp packet will not written to queue : seq_no:%d/%d ifp:%p length:%d", seq_no, m_rx_seq_no, ifp, ifp_length);
		}

		//m_rx_seq_no = seq_no + 1;

		return m_rx_queue_size > 0;
	}
	//--------------------------------------
	// 00 04 06 c0  01 80 00 00  ff 00 00
	//--------------------------------------
	bool mg_udptl_rx_tube_t::parse_rx_packet_red(int seq_no, const uint8_t* packet, int length, int index)
	{
		if (seq_no > m_rx_seq_no)
		{
			// We received a later packet than we expected, so we need to check if we can fill in
			// the gap from the secondary packets.
			int res;
			int ifp_count;
			int total_count = 0;
			const uint8_t *bufs[16];
			int lengths[16];

			memset(bufs, 0, sizeof bufs);
			memset(lengths, 0, sizeof lengths);

			do
			{
				if ((res = parse_frame_length(packet, length, &index, &ifp_count)) < 0)
				{
					WARNING_FMT("Can't parse prinary IFP packet length! res:%d", res);
					return false;
				}

				for (int i = 0; i < ifp_count; i++)
				{
					if (!parse_frame(packet, length, &index, &bufs[total_count + i], &lengths[total_count + i]))
					{
						WARNING_FMT("Can't parse IFP packet %d length!", i);
						return false;
					}
				}

				total_count += ifp_count;
			} while (res > 0);

			for (int i = total_count; i > 0; i--)
			{
				if (seq_no - i > m_rx_seq_no)
				{
					m_rx_queue[m_rx_queue_size].seq_no = seq_no - i;
					m_rx_queue[m_rx_queue_size].ifp_length = lengths[i - 1];
					m_rx_queue[m_rx_queue_size++].ifp = bufs[i - 1];
				}
			}
		}

		return true;
	}
	//--------------------------------------
	// FEC mode for error recovery
	//--------------------------------------
	bool mg_udptl_rx_tube_t::parse_rx_packet_fec(int seq_no, const uint8_t* ifp, int ifp_length, const uint8_t* packet, int length, int index)
	{
		int repaired[16];
		int span, entries;

		// Update any missed slots in the buffer
		for (; seq_no > m_rx_seq_no; m_rx_seq_no++)
		{
			udptl_rx_frame_fec_t* fec = &m_rx_buffer[m_rx_seq_no & UDPTL_BUF_MASK];
			fec->ifp_length = 0;
			fec->fec_length[0] = 0;
			fec->fec_span = 0;
			fec->fec_entries = 0;
		}

		memset(repaired, 0, sizeof repaired);

		int top = seq_no & UDPTL_BUF_MASK;

		// Save the New IFP packet
		memcpy(m_rx_buffer[top].ifp, ifp, m_rx_buffer[top].ifp_length = ifp_length);
		repaired[top] = 1;

		// Decode the FEC packets
		if (index + 2 > length)
		{
			WARNING_FMT("Can't parse FEC flag and key's count! index:%d + 1> length:%d", index, length);
			return false;
		}

		if (packet[index++] != 1)
		{
			WARNING("FEC flag is not 1!");
			return false;
		}

		m_rx_buffer[top].fec_span = span = packet[index++];

		if (index + 1 > length)
		{
			WARNING_FMT("Can't parse FEC entries's count! index:%d + 1> length:%d", index, length);
			return false;
		}

		m_rx_buffer[top].fec_entries = entries = packet[index++];

		// Decode the fec elements
		for (int i = 0; i < entries; i++)
		{
			const uint8_t* fec_data;
			int fec_length;

			if (!parse_frame(packet, length, &index, &fec_data, &fec_length))
			{
				WARNING("Can't parse FEC key!");
				return false;
			}

			if (fec_length > UDPTL_PACKET_SIZE)
			{
				WARNING_FMT("FEC key(%d bytes) is longer than 1400!", fec_length);
				return false;
			}

			memcpy(m_rx_buffer[top].fec[i], fec_data, m_rx_buffer[top].fec_length[i] = fec_length);
		}

		// See if we can reconstruct anything which is missing
		// TODO: this does not comprehensively hunt back and repair everything that is possible
		for (int line = top; line != ((top - (16 - span * entries)) & UDPTL_BUF_MASK); line = (line - 1) & UDPTL_BUF_MASK)
		{
			udptl_rx_frame_fec_t* rx_fec = &m_rx_buffer[line];

			if (rx_fec->fec_length[0] <= 0)
				continue;

			for (int entry = 0; entry < entries; entry++)
			{
				int limit = (line + entry) & UDPTL_BUF_MASK;
				int which, k;
				for (which = -1, k = (limit - rx_fec->fec_span * rx_fec->fec_entries) & UDPTL_BUF_MASK; k != limit; k = (k + rx_fec->fec_entries) & UDPTL_BUF_MASK)
				{
					if (m_rx_buffer[k].ifp_length <= 0)
						which = (which == -1) ? k : -2;
				}

				if (which >= 0)
				{
					// Repairable
					for (int j = 0; j < rx_fec->fec_length[entry]; j++)
					{
						m_rx_buffer[which].ifp[j] = rx_fec->fec[entry][j];
						for (k = (limit - rx_fec->fec_span * rx_fec->fec_entries) & UDPTL_BUF_MASK; k != limit; k = (k + rx_fec->fec_entries) & UDPTL_BUF_MASK)
							m_rx_buffer[which].ifp[j] ^= (m_rx_buffer[k].ifp_length > j) ? m_rx_buffer[k].ifp[j] : 0;
					}
					m_rx_buffer[which].ifp_length = rx_fec->fec_length[entry];
				}
			}
		}

		// Now play any New packets forwards in time
		for (int line = (top + 1) & UDPTL_BUF_MASK, j = seq_no - UDPTL_BUF_MASK; line != top; line = (line + 1) & UDPTL_BUF_MASK, j++)
		{
			if (m_rx_buffer[line].ifp_length > 0)
			{
				m_rx_queue[m_rx_queue_size].seq_no = j;
				m_rx_queue[m_rx_queue_size].ifp_length = m_rx_buffer[line].ifp_length;
				m_rx_queue[m_rx_queue_size++].ifp = m_rx_buffer[line].ifp;
			}
		}

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int mg_udptl_rx_tube_t::parse_frame_length(const uint8_t* packet, int length, int *index, int *ret_value)
	{
		// выход за пределы буфера
		if (*index >= length)
		{
			WARNING_FMT("parse frame length failed : index:%d >= length:%d ", *index, length);
			return -1;
		}

		// проверим длину сообщения
		if (packet[*index] < 0x80)
		{
			// длина пакета меньше 128 -> длина в одном байте
			*ret_value = (uint32_t)packet[*index];
			(*index)++;

			PRINT_FMT("parse frame length return:%d ", *ret_value);
			return 0; // нормальный блок
		}

		// проверим на фрагментацию
		if ((packet[*index] & 0x40) == 0)
		{
			// длина пакета меньше 16КБ -> длина в двух байтах
			if (*index >= length - 1)
				return -1;

			*ret_value = (packet[*index] & 0x3F) << 8;
			(*index)++;
			*ret_value |= packet[*index];
			(*index)++;
			return 0; // нормальный блок
		}

		// фрагмент длиной больше 16 КБ -> длина кол-во 16 КБ блоков
		*ret_value = (packet[*index] & 0x3F) << 14;
		(*index)++;
		return 1; // фрагмент
	}
	//--------------------------------------
	// 00 04 06 c0  01 80 00 00  ff 00 00
	//--------------------------------------
	bool mg_udptl_rx_tube_t::parse_frame(const uint8_t *packet, int length, int *index, const uint8_t **ifp, int *ifp_length)
	{
		int octet_count = 0;
		int length_type;

		*ifp_length = 0;

		for (int octet_index = 0;; octet_index += octet_count)
		{
			octet_count = 0;

			if ((length_type = parse_frame_length(packet, length, index, &octet_count)) < 0)
			{
				return false;
			}

			if (octet_count > 0)
			{
				if (*index + octet_count > length)
				{
					return false;
				}

				*ifp_length += octet_count;
				*ifp = &packet[*index];
				*index += octet_count;
			}

			if (length_type == 0)
				break;
		}

		RETURN_FMT(true, "parse frame: %d ", *ifp_length);
	}
}
//--------------------------------------
