/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_video_conference_out_tube.h"
#include "mg_media_element.h"

#include "logdef_tube.h"

namespace mge
{
	//-----------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_video_conference_out_tube_t::mg_video_conference_out_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id) :
		Tube(HandlerType::ConfVideoOutput, id, log)
	{
		makeName(parent_id, "-conf-out-tu"); // Conference Out Tube

		rtl::res_counter_t::add_ref(g_mge_tube_counter);

		setLogTag("TUB-OCON");
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	mg_video_conference_out_tube_t::~mg_video_conference_out_tube_t()
	{
		unsubscribe(this);

		m_except_list.clear();

		rtl::res_counter_t::release(g_mge_tube_counter);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool mg_video_conference_out_tube_t::is_suited(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out)
	{
		if (payload_el_out == nullptr)
		{
			return false;
		}

		return m_payload.getId_u8() == payload_el_out->get_payload_id();
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool mg_video_conference_out_tube_t::init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out)
	{
		if (payload_el_out == nullptr)
		{
			PLOG_TUBE_ERROR(LOG_PREFIX, "mg_video_conference_out_tube_t::init -- payload element is null.");
			return false;
		}

		m_payload.setEncoding(payload_el_out->get_element_name());
		m_payload.setId((media::PayloadId)payload_el_out->get_payload_id());
		m_payload.setChannelCount(payload_el_out->getChannelCount());
		m_payload.setFrequency(payload_el_out->get_sample_rate());
		m_payload.setFmtp(payload_el_out->get_param_fmtp());

		PLOG_TUBE_WRITE(LOG_PREFIX, "mg_video_conference_out_tube_t::init -- payload element: %d", m_payload.getId());

		return true;
	}
	//-----------------------------------------------
	// �������� ��������� � ������ ����������
	//-----------------------------------------------
	bool mg_video_conference_out_tube_t::is_permissible_source(const char* source)
	{
		if (source == nullptr || source[0] == 0)
		{
			return false;
		}

		rtl::MutexLock lock(m_except_sync);

		return m_except_list.getCount() == 0 ? true : m_except_list.contains(source);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void mg_video_conference_out_tube_t::add_except_termination_id(const char* term_id)
	{
		rtl::MutexLock lock(m_except_sync);

		if (!m_except_list.contains(term_id))
		{
			ERROR_MSG("already exist! (member not added)");
			return;
		}

		m_except_list.add(term_id);

		PRINT_FMT("term-id %s added", term_id);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void mg_video_conference_out_tube_t::remove_except_termination_id(const char* term_id)
	{
		rtl::MutexLock lock(m_except_sync);

		int index = m_except_list.indexOf(term_id);

		if (index >= 0)
		{
			m_except_list.removeAt(index);
			PRINT_FMT("term-id %s removed.", term_id);
			return;
		}

		ERROR_FMT("term-id %s not found.", term_id);
	}
	void mg_video_conference_out_tube_t::changeTopologyFrom(const char* id, h248::TopologyDirection topology)
	{
		switch (topology)
		{
		case h248::TopologyDirection::Bothway:
			remove_except_termination_id(id);
			break;
		case h248::TopologyDirection::Isolate:
			add_except_termination_id(id);
			break;
		case h248::TopologyDirection::Oneway:
			add_except_termination_id(id);
			break;
		}
	}
	void mg_video_conference_out_tube_t::changeTopologyTo(const char* id, h248::TopologyDirection topology)
	{
		switch (topology)
		{
		case h248::TopologyDirection::Bothway:
			remove_except_termination_id(id);
			break;
		case h248::TopologyDirection::Isolate:
			add_except_termination_id(id);
			break;
		case h248::TopologyDirection::Oneway:
			remove_except_termination_id(id);
			break;
		}
	}
}
//-----------------------------------------------
