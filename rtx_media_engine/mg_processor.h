﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//-----------------------------------------------
//
//-----------------------------------------------
#include "mg_direction.h"

namespace mge
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	class MediaContext;

	//-----------------------------------------------
	//
	//-----------------------------------------------
	class MediaProcessor
	{
	public:
		MediaProcessor(MediaContext* owner, uint32_t id, const char* parentId);
		~MediaProcessor();

		uint32_t getId() { return m_id; }
		uint32_t getType() { return m_type; }
		void setType(uint32_t type) { m_type = type; }
		const char* getName() const { return m_name; }
		void setRecordPath(const char* path) { m_pathRecord = path; }
		void setExtraEngine(void* engine) { m_extraEngine = engine; }

		uint32_t getTerminationCount();
		bool addTermination(Termination* mg_term, DirectionType direction_type);
		void removeTermination(uint32_t termId);
		void updateTopology(const rtl::ArrayT<h248::TopologyTriple>& triple_list);
		void updateConfSchema(const char* jsonSchema);
		void resetConference();

	private:
		Direction* createDirection(DirectionType direction_type);
		Direction* findDirection(DirectionType direction_type);
		void releaseDirection(uint32_t direction_id);
		void clearAllDirections();
		bool addTerminationToDirection(Termination* mg_term, Direction* mg_direction);

	private:
		uint32_t m_id;
		uint32_t m_type;
		rtl::String m_name;
		uint32_t m_direction_id_counter;

		MediaContext* m_owner;
		rtl::Logger* m_log;

		bool m_conference;	// флаг созданной конференции.
		DirectionList m_directionList;

		void* m_extraEngine;
		rtl::String m_pathRecord;
	};
	//-----------------------------------------------
	//
	//-----------------------------------------------
	typedef rtl::ArrayT<MediaProcessor*> MediaProcessorList;
}
//-----------------------------------------------
