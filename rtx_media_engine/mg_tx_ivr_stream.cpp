﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_thread.h"
#include "mg_tx_ivr_stream.h"
#include "mg_session.h"

#include "logdef_stream.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_tx_ivr_stream_t::mg_tx_ivr_stream_t(rtl::Logger* log, uint32_t id, uint32_t parent_id, const char* term_id) :
		mg_tx_stream_t(log, id, mg_tx_ivr_e, term_id)
	{
		rtl::String name;
		name << term_id << "-s" << parent_id << "-txivrs" << id;
		mg_tx_stream_t::setName(name);
		mg_tx_stream_t::setTag("TXS_IVR");

		m_rec_path = rtl::String::empty;
		m_type = "rtp";
		m_buffer_size_ms = 0;
		m_recorder_rtp = nullptr;
		m_recorder_raw = nullptr;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_tx_ivr_stream_t::~mg_tx_ivr_stream_t()
	{
		if (m_recorder_rtp != nullptr)
		{
			m_recorder_rtp->stop();
			m_recorder_rtp->close();
			DELETEO(m_recorder_rtp);
			m_recorder_rtp = nullptr;
		}

		if (m_recorder_raw != nullptr)
		{
			m_recorder_raw->stop();
			m_recorder_raw->close();
			DELETEO(m_recorder_raw);
			m_recorder_raw = nullptr;
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_tx_ivr_stream_t::mg_stream_set_LocalControl_Property(const h248::Field* LC_property)
	{
		ENTER();

		if (!isLocked())
		{
			WARNING("stream unlocked!");
			return false;
		}

		if (LC_property == nullptr)
		{
			WARNING("wrong param LC_property!");
			return false;
		}

		rtl::String name(LC_property->getName());
		rtl::String value(LC_property->getValue());
		
		PRINT_FMT("name:%s value:%s", (const char*)name, (const char*)value);

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_tx_ivr_stream_t::send(const INextDataHandler* out_handler, const rtp_packet* packet)
	{
		if (checkAndLock())
		{
			WARNING("stream locked!");
			return;
		}

		send_to_rtp_lock(out_handler, packet);

		releaseLock();
	}
	//------------------------------------------------------------------------------
	void mg_tx_ivr_stream_t::send(const INextDataHandler* out_handler, const uint8_t* data, uint32_t len)
	{
	}
	//------------------------------------------------------------------------------
	void mg_tx_ivr_stream_t::send(const INextDataHandler* out_handler, const rtcp_packet_t* packet)
	{
	}
	//------------------------------------------------------------------------------
	void mg_tx_ivr_stream_t::send_to_rtp_lock(const INextDataHandler* out_handler, const rtp_packet* packet)
	{
		if (packet == nullptr)
		{
			ERROR_MSG("packet is null!");
			return;
		}

		if (m_recorder_rtp != nullptr)
		{
			m_recorder_rtp->write_packet(packet);
		}
		if (m_recorder_raw != nullptr)
		{
			m_recorder_raw->write_packet(packet);
		}
	}
	//------------------------------------------------------------------------------
	bool mg_tx_ivr_stream_t::create_recorder()
	{
		if ((m_recorder_rtp != nullptr) || (m_recorder_raw != nullptr))
		{
			return true;
		}

		ENTER();

		if (m_rec_path.isEmpty())
		{
			PRINT("writing to file is disabled");
			return false;
		}

		mg_media_element_list_t* el_list = mg_tx_stream_t::getMediaParams().get_media_elements();
		if (el_list == nullptr)
		{
			ERROR_MSG("no parameters to write to file!");
			return false;
		}

		media::PayloadSet record_formats;
		for (int i = 0; i < el_list->getCount(); i++)
		{
			mg_media_element_t* element = el_list->getAt(i);
			if (element == nullptr)
			{
				continue;
			}

			rtl::String rtpmap = element->get_param_rtpmap();
			if (rtpmap.isEmpty())
			{
				PRINT_FMT("format:%s pid:%u:", (const char*)element->get_element_name(), element->get_payload_id());
				record_formats.addFormat(element->get_element_name(), (media::PayloadId)element->get_payload_id(), element->get_sample_rate(), element->getChannelCount(), element->get_param_fmtp());
			}
			else
			{
				PRINT_FMT("format:%s pid:%u:", (const char*)element->get_element_name(), element->get_payload_id());
				record_formats.addFormat(element->get_element_name(), (media::PayloadId)element->get_payload_id(), element->get_sample_rate(), rtpmap, element->get_param_fmtp());
			}
		}

		if (record_formats.getCount() == 0)
		{
			ERROR_MSG("file recording format not specified");
			return false;
		}

		if (rtl::String::compare("raw", m_type, false) == 0)
		{
			m_recorder_raw = NEW media::BufferedMediaWriterRaw(m_log);
			if (m_recorder_raw == nullptr)
			{
				ERROR_MSG("failed to create audio recorder.");
				record_formats.removeAll();
				return false;
			}

			PRINT_FMT("recording file path: %s", (const char*)m_rec_path);
			if (!m_recorder_raw->create(m_rec_path, m_buffer_size_ms))
			{
				WARNING("create fail!");
				record_formats.removeAll();
				return false;
			}

			if (!m_recorder_raw->start())
			{
				WARNING("start failed!");
				record_formats.removeAll();
				return false;
			}
		}
		else
		{
			m_recorder_rtp = NEW media::BufferedMediaWriterRTP(m_log);
			if (m_recorder_rtp == nullptr)
			{
				ERROR_MSG("failed to create audio recorder!");
				record_formats.removeAll();
				return false;
			}

			PRINT_FMT("recording file path: %s", (const char*)m_rec_path);
			if (!m_recorder_rtp->create(m_rec_path, m_buffer_size_ms, record_formats))
			{
				WARNING("create failed!");
				record_formats.removeAll();
				return false;
			}

			if (!m_recorder_rtp->start())
			{
				WARNING("start failed!");
				record_formats.removeAll();
				return false;
			}
		}

		record_formats.removeAll();

		RETURN_BOOL(true);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_tx_ivr_stream_t::setMediaParams(mg_sdp_media_base_t& media)
	{
		mg_tx_stream_t::setMediaParams(media);

		if (getMediaParams().get_media_elements()->getCount() == 0)
		{
			mg_create_media_element_ivr(getName(), getMediaParams().get_media_elements());
		}

		if (m_recorder_rtp != nullptr)
		{
			mg_media_element_list_t* el_list = media.get_media_elements();

			media::PayloadSet record_formats;
			for (int i = 0; i < el_list->getCount(); i++)
			{
				mg_media_element_t* element = el_list->getAt(i);
				if (element == nullptr)
				{
					continue;
				}

				rtl::String rtpmap = element->get_param_rtpmap();
				if (rtpmap.isEmpty())
				{
					PRINT_FMT("format:%s pid:%u:", (const char*)element->get_element_name(), element->get_payload_id());
					record_formats.addFormat(element->get_element_name(), (media::PayloadId)element->get_payload_id(), element->get_sample_rate(), element->getChannelCount(), element->get_param_fmtp());
				}
				else
				{
					PRINT_FMT("format:%s pid:%u:", (const char*)element->get_element_name(), element->get_payload_id());
					record_formats.addFormat(element->get_element_name(), (media::PayloadId)element->get_payload_id(), element->get_sample_rate(), rtpmap, element->get_param_fmtp());
				}
			}

			m_recorder_rtp->update_format(record_formats);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	int mg_tx_ivr_stream_t::rtcp_session_tx(const rtcp_packet_t* packet)
	{
		return 0;
	}
}
//------------------------------------------------------------------------------
