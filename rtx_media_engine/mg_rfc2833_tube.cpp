﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_rfc2833_tube.h"
#include "mg_media_element.h"
#include "mg_rx_stream.h"
#include "rtp_packet.h"

#include "logdef_tube.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	static const char g_rfc2833_table_events[] = "0123456789*#ABCD!";
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_rfc2833_tube_t::mg_rfc2833_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id) :
		Tube(HandlerType::DTMF2833, id, log)
	{
		m_rx_stream = nullptr;

		makeName(parent_id, "-rfc2833-tu"); // rfc 2833 (dtmf)

		m_recv_complete = true;
		m_recv_tone = 0;
		m_recv_timestamp = 0;
		m_recv_last_packet_time = 0;

		rtl::res_counter_t::add_ref(g_mge_tube_counter);

		setLogTag("TUB-DTMF");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_rfc2833_tube_t::~mg_rfc2833_tube_t()
	{
		unsubscribe(this);

		m_rx_stream = nullptr;

		rtl::res_counter_t::release(g_mge_tube_counter);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_rfc2833_tube_t::init(mg_media_element_t* payload_el_in, mg_media_element_t* payload_el_out)
	{
		if (payload_el_in == nullptr)
		{
			ERROR_MSG("payload element is null!");
			return false;
		}

		m_payload_id = payload_el_in->get_payload_id();
		PRINT_FMT("payload id: %d", (const char*)payload_el_in->get_element_name(), m_payload_id);

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_rfc2833_tube_t::set_stream(mg_rx_stream_t* rx_stream)
	{
		m_rx_stream = rx_stream;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_rfc2833_tube_t::send(const INextDataHandler* out_handler, const rtp_packet* packet)
	{
		if (packet == nullptr)
		{
			ERROR_MSG("nothing to send.");
			return;
		}

		if (m_rx_stream == nullptr)
		{
			ERROR_MSG("rx stream is null.");
			return;
		}

		// игнорируем звуковые пакеты если тон не принимается
		if ((uint16_t)packet->get_payload_type() != m_payload_id)
		{
			if (m_recv_complete)
			{
				return;
			}

			// проверка таймаута
			// таймштамп текущего времени
			uint32_t current = rtl::DateTime::getTicks();
			if (current - m_recv_last_packet_time > 150)
			{
				PRINT_FMT("timeout %d", current - m_recv_last_packet_time);
				raise_dtmf_received_event(m_recv_tone, m_recv_duration);
			}
			return;
		}

		// слишком маленький пакет
		if (packet->get_payload_length() < 4)
		{
			WARNING("ignoring packet, too small.");
			return;
		}

		// таймштамп текущего времени
		uint32_t current = rtl::DateTime::getTicks();
		const uint8_t * payload = packet->get_payload();
		uint8_t end_flag = payload[1] & 0x80;

		if (payload[0] >= sizeof(g_rfc2833_table_events) - 1)
		{
			WARNING("ignoring packet, unsupported event.");
			return;
		}

		char rcvTone = g_rfc2833_table_events[payload[0]];
		short rcvDuration = (payload[2] << 8) + payload[3];
		uint32_t timestamp = packet->getTimestamp();

		PRINT_FMT("pt:%d tone:%c duration:%u ts:%08X flags:%02X",
			packet->get_payload_type(), rcvTone, rcvDuration, timestamp, end_flag);

		// начало нового тона
		if (m_recv_complete)
		{
			if (m_recv_timestamp == timestamp)
			{
				//MLOG_STREAM_CALL(L"rfc2833", L"receiver: dublicated packet");
				return;
			}

			PRINT("new tone started");

			m_recv_complete = false;
			m_recv_last_packet_time = current;
			m_recv_timestamp = timestamp;
			m_recv_duration = rcvDuration;
			m_recv_tone = rcvTone;
		}

		m_recv_last_packet_time = current;// rtl::DateTime::getTicks();

		// новый с недоконченным тоном!
		if (m_recv_timestamp != timestamp)
		{
			PRINT("new tone started over not not completed tone");
			// сообщим о завершении тона
			raise_dtmf_received_event(m_recv_tone, m_recv_duration ? m_recv_duration : 40);

			// начало нового тона
			PRINT_FMT("start tone=%c", m_recv_tone);
			m_recv_tone = rcvTone;
			m_recv_duration = rcvDuration;
			m_recv_timestamp = timestamp;
		}
		else
		{
			PRINT("continues tone");

			m_recv_tone = rcvTone;
			m_recv_duration = rcvDuration;
		}

		// если флаг завершения то возбудим событие
		if ((payload[1] & 0x80) != 0)
		{
			PRINT("end tone flag");
			raise_dtmf_received_event(m_recv_tone, m_recv_duration ? m_recv_duration : 40);
		}

		INextDataHandler* out = getNextHandler();
		if (out != nullptr)
		{
			out->send(this, packet);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_rfc2833_tube_t::unsubscribe(const INextDataHandler* out_handler)
	{
		ENTER();

		bool result = Tube::unsubscribe(out_handler);

		m_rx_stream = nullptr;

		RETURN_BOOL(result);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_rfc2833_tube_t::raise_dtmf_received_event(char tone, uint32_t duration)
	{
		ENTER_FMT("tone=%c duration=%u.", tone, duration);

		m_rx_stream->mg_termination__dtmf_event(tone, duration);
		m_recv_complete = true;
		m_recv_last_packet_time = 0;
	}
}
//------------------------------------------------------------------------------
