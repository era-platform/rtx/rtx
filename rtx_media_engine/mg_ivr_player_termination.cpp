﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_ivr_player_termination.h"
#include "mg_rx_ivr_stream.h"
#include "mg_tx_ivr_stream.h"
#include "std_filesystem.h"
#include "std_sys_env.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define LOG_PREFIX m_tag //"MG-TPLY" // Media Gateway IVR Player Termination

#include "logdef.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	IVRPlayerTermination::IVRPlayerTermination(rtl::Logger* log, const h248::TerminationID& termId, const char* parent_id) :
		Termination(log, termId, parent_id)
	{
		m_service_state = h248::TerminationServiceState::Test;
		m_event_buffer_control = false;
		strcpy(m_tag, "TERM-PLY");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	IVRPlayerTermination::~IVRPlayerTermination()
	{
		mg_termination_destroy(nullptr);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool IVRPlayerTermination::mg_termination_initialize(h248::Field* reply)
	{
		ENTER();

		MediaSession* mg_session = makeMediaSession(MG_STREAM_TYPE_AUDIO);
		if (mg_session == nullptr)
		{
			ERROR_MSG("create stream fail.");
			// !!! @error code
			reply->addErrorDescriptor(h248::ErrorCode::c510_Insufficient_resources, "Not enough memory to create a media session");
			return false;
		}

		if (!mg_session->create_rx_stream(mg_rx_stream_type_t::mg_rx_ivr_e))
		{
			ERROR_MSG("create rx stream fail.");
			mge::Termination::releaseMediaSession(MG_STREAM_TYPE_AUDIO);
			// !!! @error code
			reply->addErrorDescriptor(h248::ErrorCode::c510_Insufficient_resources, "Not enough memory to create a receiver stream");
			return false;
		}

		mg_rx_ivr_stream_t* ivr_stream = (mg_rx_ivr_stream_t*)mg_session->get_rx_stream_first();

		if (!ivr_stream->create_player())
		{
			ERROR_MSG("create player in stream fail.");
			mge::Termination::releaseMediaSession(MG_STREAM_TYPE_AUDIO);
			reply->addErrorDescriptor(h248::ErrorCode::c510_Insufficient_resources, "Not enough memory to create a audio player");
			// !!! @error code
			return false;
		}

		if (!mg_session->create_tx_stream(mg_tx_stream_type_t::mg_tx_ivr_e))
		{
			ERROR_MSG("create tx stream fail.");
			mge::Termination::releaseMediaSession(MG_STREAM_TYPE_AUDIO);
			reply->addErrorDescriptor(h248::ErrorCode::c510_Insufficient_resources, "Not enough memory to create a transmitter stream");
			// !!! @error code
			return false;
		}
		mg_tx_stream_t* tx_stream = mg_session->get_tx_stream();

		mg_sdp_media_base_t& sdp = tx_stream->getMediaParams();
		mg_media_element_list_t* elements = sdp.get_media_elements();
		elements->clear();
		mg_media_element_t* el = elements->create(getName(), 101, 0);
		elements->set_priority_index(0);
		el->set_element_name("telephone-event");

		RETURN_BOOL(true);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool IVRPlayerTermination::mg_termination_destroy(h248::Field* reply)
	{
		mg_stream_stop(MG_STREAM_TYPE_AUDIO);

		mge::Termination::destroyMediaSession();

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	// reply->addErrorDescriptor(h248::ErrorCode::c449_Unsupported_or_unknown_parameter_or_property_value, "TerminationState Property has invalid value");
	bool IVRPlayerTermination::mg_termination_set_TerminationState_Property(const h248::Field* property, h248::Field* reply)
	{
		const rtl::String& propName = property->getName();
		const rtl::String& propValue = property->getValue();

		ENTER_FMT("name:%s, value:%s", (const char*)propName, (const char*)propValue);

		MediaSession* mg_session = mge::Termination::findMediaSession(MG_STREAM_TYPE_AUDIO);
		if (mg_session == nullptr)
		{
			ERROR_MSG("mg_session is null.");
			reply->addErrorDescriptor(h248::ErrorCode::c500_Internal_software_failure_in_the_MG_or_MGC, "IVR Player initialization failed");
			return false;
		}

		const rtl::ArrayT<mg_rx_stream_t*>& rx_streams = mg_session->get_rx_streams();
		if (rx_streams.getCount() == 0)
		{
			ERROR_MSG("ivr stream not found.");
			reply->addErrorDescriptor(h248::ErrorCode::c500_Internal_software_failure_in_the_MG_or_MGC, "IVR Player rx stream initialization failed");
			return false;
		}

		mg_rx_ivr_stream_t* ivr_stream = (mg_rx_ivr_stream_t*)rx_streams.getAt(0);
		if (ivr_stream == nullptr)
		{
			ERROR_MSG("ivr stream is null.");
			reply->addErrorDescriptor(h248::ErrorCode::c500_Internal_software_failure_in_the_MG_or_MGC, "IVR Player rx stream initialization failed");
			return false;
		}

		rtl::String value = unescape_rfc3986(propValue);

		if (rtl::String::compare("file", propName, false) == 0)
		{
			rtl::String path = utf8_to_locale(value);

			path.replace('\\', FS_PATH_DELIMITER);
			path.replace('/', FS_PATH_DELIMITER);

			rtl::String full_path = get_local_path(path);// (m_media_path);

			PRINT_FMT("start play file %s", (const char*)full_path);

			if (!ivr_stream->add_file(full_path))
			{
				ERROR_MSG("add file fail.");
				reply->addErrorDescriptor(h248::ErrorCode::c500_Internal_software_failure_in_the_MG_or_MGC, "IVR Player file failed");
				return false;
			}
		}
		else if (rtl::String::compare("files", propName, false) == 0)
		{
			rtl::String path = utf8_to_locale(value);

			path.trim("\"");
			path.replace('\\', FS_PATH_DELIMITER);
			path.replace('/', FS_PATH_DELIMITER);

			rtl::StringList file_list;
			path.split(file_list, ';');

			for (int i = 0; i < file_list.getCount(); i++)
			{
				rtl::String file_at_list = file_list.getAt(i);
				rtl::String full_path = get_local_path(file_at_list);

				PRINT_FMT("start files [%i] %s.", i, (const char*)full_path);

				if (!ivr_stream->add_file(full_path))
				{
					ERROR_MSG("add file fail.");
					continue;
				}
			}
		}
		else if (rtl::String::compare("loop", propName, false) == 0)
		{
			ivr_stream->set_loop(rtl::String::compare("1", value, false) == 0);
		}
		else if (rtl::String::compare("random", propName, false) == 0)
		{
			ivr_stream->set_random(rtl::String::compare("1", value, false) == 0);
		}
		else if (rtl::String::compare("dir", propName, false) == 0)
		{
			rtl::String dir = utf8_to_locale(value);

			dir.replace('\\', FS_PATH_DELIMITER);
			dir.replace('/', FS_PATH_DELIMITER);

			rtl::String full_dir = get_local_path(dir);

			full_dir.replace('\\', FS_PATH_DELIMITER);
			full_dir.replace('/', FS_PATH_DELIMITER);

			if (full_dir[full_dir.getLength() - 1] == '/')
			{
				full_dir.remove(full_dir.getLength() - 1);
			}

			Log.log("IVR", "start play directory %s.", (const char*)full_dir);

			PRINT_FMT("start play directory %s.", (const char*)full_dir);

			if (!ivr_stream->add_dir(full_dir))
			{
				ERROR_MSG("add_dir fail.");
				reply->addErrorDescriptor(h248::ErrorCode::c500_Internal_software_failure_in_the_MG_or_MGC, "IVR Player file failed");
				return false;
			}
		}
		else if (rtl::String::compare("startat", propName, false) == 0)
		{
			ivr_stream->set_start_at((uint32_t)strtoul(value, nullptr, 10));
		}
		else if (rtl::String::compare("stopat", propName, false) == 0)
		{
			ivr_stream->set_stop_at((uint32_t)strtoul(value, nullptr, 10));
		}
		else if (rtl::String::compare("volume", propName, false) == 0)
		{
			ivr_stream->set_volume((uint32_t)strtoul(value, nullptr, 10));
		}
		else if (rtl::String::compare("mode", propName, false) == 0)
		{
			if (value.indexOf("stop") != BAD_INDEX)
			{
				ivr_stream->destroy_player();
			}
			else if (value.indexOf("pause") != BAD_INDEX)
			{
				ivr_stream->pause();
			}
			else if (value.indexOf("play") != BAD_INDEX)
			{
				ivr_stream->resume();
			}
		}

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool IVRPlayerTermination::mg_termination_set_Local_Remote(uint16_t stream_id, const rtl::String& remote_sdp, const rtl::String& local_sdp_offer, h248::Field* reply)
	{
		return true;
	}
	//------------------------------------------------------------------------------
	bool IVRPlayerTermination::mg_termination_get_Local(uint16_t stream_id, rtl::String& local_sdp_answer, h248::Field* reply)
	{
		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool IVRPlayerTermination::mg_stream_start(uint32_t id_stream)
	{
		return mge::Termination::mg_termination_unlock();
	}
	//------------------------------------------------------------------------------
	void IVRPlayerTermination::mg_stream_stop(uint32_t id_stream)
	{
		mge::Termination::mg_termination_lock();
	}
}
//------------------------------------------------------------------------------
