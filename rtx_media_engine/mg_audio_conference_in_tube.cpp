﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_audio_conference_in_tube.h"
#include "mg_media_element.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//#define LOG_PREFIX "ITUB-CNF"

//#define __OBJ_NAME__ getName()

#include "logdef_tube.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_audio_conference_in_tube_t::mg_audio_conference_in_tube_t(rtl::Logger* log, uint32_t id, const char* parent_id) :
		Tube(HandlerType::ConfAudioInput, id, log)
	{
		makeName(parent_id, "-conf-in-tu"); // Conference In Tube

		m_mixer_samples_count = 160;

		m_deleted_from_conference = true;

		m_subscriber_count = 0;

		rtl::res_counter_t::add_ref(g_mge_tube_counter);

		setLogTag("TUB-ICON");
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	mg_audio_conference_in_tube_t::~mg_audio_conference_in_tube_t()
	{
		unsubscribe(this);

		m_freelock_buffer.reset();

		m_mixer_samples_count = 0;

		rtl::res_counter_t::release(g_mge_tube_counter);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_audio_conference_in_tube_t::send(const INextDataHandler* out_handler, const rtp_packet* packet)
	{
		if (packet == nullptr)
		{
			ERROR_MSG("nothing to send.");
			return;
		}

		int size = packet->get_payload_length();
		if (m_freelock_buffer.write(packet->get_payload(), size) != size)
		{
			ERROR_FMT("packet:%d write :%d in buffer fail.", packet->get_sequence_number(), size);
		}

		INextDataHandler* out = getNextHandler();
		if (out != nullptr)
		{
			out->send(this, packet);
		}
	}
	//------------------------------------------------------------------------------
	void mg_audio_conference_in_tube_t::send(const INextDataHandler* out_handler, const uint8_t* data, uint32_t len)
	{
		if ((data == nullptr) || (len == 0))
		{
			ERROR_MSG("nothing to send.");
			return;
		}

		if (m_freelock_buffer.write(data, len) != (int)len)
		{
			ERROR_FMT("data write :%d in buffer fail.", len);
		}

		INextDataHandler* out = getNextHandler();
		if (out != nullptr)
		{
			out->send(this, data, len);
		}
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_audio_conference_in_tube_t::set_termination_name(const char* termination_id)
	{
		m_termination_id = termination_id;
	}
	//------------------------------------------------------------------------------
	const char* mg_audio_conference_in_tube_t::get_termination_name()
	{
		return m_termination_id;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_audio_conference_in_tube_t::unsubscribe(const INextDataHandler* out_handler)
	{
		ENTER();

		bool result = Tube::unsubscribe(out_handler);

		if (m_subscriber_count > 0)
		{
			std_interlocked_dec(&m_subscriber_count);
		}

		RETURN_FMT(result, "remain: %d", m_subscriber_count);
		return result;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_audio_conference_in_tube_t::free()
	{
		if (Tube::free())
		{
			return m_subscriber_count <= 0;
		}

		return false;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	const uint16_t*	mg_audio_conference_in_tube_t::get_media()
	{
		int size = m_mixer_samples_count * sizeof(uint16_t);

		// проверим сколько данных записано.
		int size_write = m_freelock_buffer.get_available();
		if ((size_write < size) || (size == 0))
		{
			//PLOG_TUBE_WARNING(LOG_PREFIX, "get_media -- ID(%s): insufficient data. (need: %d, available: %d)", getName(), size, size_write);
			return nullptr;
		}

		if (m_freelock_buffer.read((uint8_t*)m_mixer_samples, size) != size)
		{
			ERROR_FMT("buffer reading error!", getName());
			return nullptr;
		}

		//	PLOG_TUBE_WRITE(LOG_PREFIX, "get_media -- ID(%s): get %d data.", getName(), size);

		return m_mixer_samples;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_audio_conference_in_tube_t::subscriber_add()
	{
		std_interlocked_inc(&m_subscriber_count);
		PRINT_FMT("count: %d", m_subscriber_count);
	}
	//------------------------------------------------------------------------------
	uint64_t mg_audio_conference_in_tube_t::get_subscribers_count()
	{
		return m_subscriber_count;
	}
	//------------------------------------------------------------------------------
	bool mg_audio_conference_in_tube_t::can_delete()
	{
		return m_deleted_from_conference;
	}
	//------------------------------------------------------------------------------
	void mg_audio_conference_in_tube_t::add_to_conference()
	{
		m_deleted_from_conference = false;
	}
	//------------------------------------------------------------------------------
	void mg_audio_conference_in_tube_t::deleted_from_conference()
	{
		m_deleted_from_conference = true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool mg_audio_conference_in_tube_t::init(mg_media_element_t* payload_el_in, mg_media_element_t*)
	{
		if (payload_el_in == nullptr)
		{
			ERROR_MSG("payload element is null!");
			return false;
		}
		m_payload_id = payload_el_in->get_payload_id();
		PRINT_FMT("payload in: %s %d", payload_el_in->get_element_name(), m_payload_id);

		uint32_t ptime = payload_el_in->get_ptime();
		uint32_t sample_rate = payload_el_in->get_sample_rate();
		uint32_t samples_count = ptime * sample_rate / 1000;

		m_freelock_buffer.reset();
		// навсякий случай берем 6 блоков
		m_freelock_buffer.create(samples_count * 6 * sizeof(short));
		memset(m_mixer_samples, 0, m_mixer_samples_count_max * sizeof(uint16_t));
		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void mg_audio_conference_in_tube_t::update_frame_size(uint32_t mixer_frame_size)
	{
		m_mixer_samples_count = mixer_frame_size;
	}
}
//------------------------------------------------------------------------------
