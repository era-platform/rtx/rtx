﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "rtp_port_manager.h"

#include "mg_termination.h"
#include "mg_session.h"
#include "net/ip_address.h"

namespace mge
{
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	class WebRTCTermination : public Termination
	{
	public:
		WebRTCTermination(rtl::Logger* log, const h248::TerminationID& termId, const char* parent_id);
		virtual ~WebRTCTermination() override;

		virtual bool isReady(uint32_t stream_id) override;

		virtual bool mg_stream_start(uint32_t stream_id) override;
		virtual void mg_stream_stop(uint32_t stream_id) override;

		//------------------------------------------------------------------------------
		// методы контекста

		// конструктор и деструктор
		virtual bool mg_termination_initialize(h248::Field* reply) override;
		virtual bool mg_termination_destroy(h248::Field* reply) override;

		virtual bool mg_termination_set_LocalControl_Mode(uint16_t stream_id, h248::TerminationStreamMode mode, h248::Field* reply) override;

		// настройки TerminationState
		virtual bool mg_termination_set_TerminationState_Property(const h248::Field* term_property, h248::Field* reply) override;
		// настройки Local и Remote
		virtual bool mg_termination_set_Local_Remote(uint16_t stream_id, const rtl::String& remote_sdp, const rtl::String& local_sdp_offer, h248::Field* reply) override;
		virtual bool mg_termination_get_Local(uint16_t stream_id, rtl::String& local_sdp_answer, h248::Field* reply) override;

		void update_rtcp_monitors();
		//------------------------------------------------------------------------------
	private:
		bool prepare_audio(rtp_channel_t* channel, uint16_t stream_id, const rtl::String& remote_sdp, const rtl::String& local_sdp);
		bool prepare_video(uint16_t stream_id, const rtl::String& remote_sdp, const rtl::String& local_sdp);
		//---------------------------------------------------------------------------------------------------------------
		bool prepare_offer_audio(rtp_channel_t* channel, MediaSession* audio_stream, uint16_t stream_id, const rtl::String& local_sdp);
		bool apply_answer_audio(rtp_channel_t* channel, MediaSession* audio_stream, uint16_t stream_id, const rtl::String& remote_sdp);
		bool prepare_answer_audio(rtp_channel_t* channel, MediaSession* audio_stream, uint16_t stream_id, const rtl::String& local_sdp);
		bool apply_offer_audio(rtp_channel_t* channel, MediaSession* audio_stream, uint16_t stream_id, const rtl::String& remote_sdp);
		//---------------------------------------------------------------------------------------------------------------
		bool prepare_offer_webrtc_video(MediaSession* video_stream, uint16_t stream_id, const rtl::String& local_sdp, bool first);
		bool apply_answer_webrtc_video(MediaSession* video_stream, uint16_t stream_id, const rtl::String& remote_sdp, bool first);
		bool prepare_answer_webrtc_video(MediaSession* video_stream, uint16_t stream_id, const rtl::String& local_sdp, bool first);
		bool apply_offer_webrtc_video(MediaSession* video_stream, uint16_t stream_id, const rtl::String& remote_sdp, bool first);
		//---------------------------------------------------------------------------------------------------------------

		mg_rx_stream_t*	find_rx_stream_by_type(mg_rx_stream_type_t type_rx, MediaSession* mg_stream, int index);
		mg_tx_stream_t*	find_tx_stream_by_type(mg_tx_stream_type_t type_tx, MediaSession* mg_stream, int index);

		rtp_channel_t*	create_channel(uint32_t stream_id);
		rtp_channel_t*	get_channel(uint32_t stream_id);
		void release_channel(rtp_channel_t* channel);
		void clear_cannels();

		void stop_all();

		bool bind_ssrc(mg_sdp_ssrc_param_t* ssrc_param, rtp_channel_t* channel, mg_rx_stream_t* rx_stream);
		bool subscribe_ssrc(uint32_t ssrc, rtp_channel_t* channel, mg_rx_stream_t* rx_stream);
		// analyze the address using ice
		bool analyze_remote_addr_ice_1(mg_sdp_media_base_t* audio_r, rtp_channel_t* channel, socket_address* sadd, socket_address* saddr_rtcp);

		void prepare_ssrc_local_1(uint16_t stream_id, uint32_t ssrc, mg_sdp_media_base_t* media);
		bool prepare_ice_local_1(uint16_t stream_id, rtp_channel_t* channel, mg_sdp_media_base_t* media);

		rtl::String find_diff(mg_sdp_diff_list* diff_list, mg_sdp_difference_type type);

	private:
		rtl::String m_adress_key;
		bool m_event_buffer_control;
		rtp_channel_list m_channel_list;
		mg_sdp_session_t m_sdp_local;
		mg_sdp_session_t m_sdp_remote;
	};
}
//------------------------------------------------------------------------------
