/* RTX H.248 Media Gate Engine Test Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "test_rtx_megaco.h"

namespace rtx_megaco
{
	static void test_mid(const char* value, const char* sign, h248::MessageWriterParams* params)
	{
		h248::MID mid;
		rtl::MemoryStream stream;

		mid.parse(value);
		mid.writeTo(stream, params);
		ASSERT_STRCASEEQ(sign, (const char*)stream.getBuffer());
	}

	const char* mid1 = "[192.168.0.12]:1234";
	const char* mid2 = "<sub.domain.com>:1234";
	const char* mid3 = "megaco_device@media_gate.ru";
	const char* mid4 = "MTP { 1234ABCD }";


	TEST(test_megaco_parser, mid_ipv4)
	{
		PRINTF("Test H.248 message MID ipv4 test...\n");
		h248::MessageWriterParams params = {true, false, 0, 0};
		test_mid(mid1, "[192.168.0.12]:1234\r\n", &params);
	}

	TEST(test_megaco_parser, mid_dom)
	{
		PRINTF("Test H.248 message MID domain test...\n");

		test_mid(mid2, "<sub.domain.com>:1234 ", nullptr);
	}

	TEST(test_megaco_parser, mid_url)
	{
		PRINTF("Test H.248 message MID url test...\n");
		h248::MessageWriterParams params = { true, false, 0, 0 };
		test_mid(mid3, "megaco_device@media_gate.ru\r\n", &params);
	}

	TEST(test_megaco_parser, mid_mtp)
	{
		PRINTF("Test H.248 message MID mtp test...\n");

		test_mid(mid4, "MTP {1234ABCD} ", nullptr);
	}
}
//-----------------------------------------------
