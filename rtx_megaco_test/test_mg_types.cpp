/* RTX H.248 Media Gate Engine Test Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_sys_env.h"
#include "test_rtx_megaco.h"

 //-----------------------------------------------
//
//-----------------------------------------------
namespace rtx_megaco
{
	static void test_term_id(const char* id, const char* sample)
	{
		h248::TerminationID term_id;
		char buffer[128];

		if (term_id.parse(id))
		{
			const char* str = term_id.toString(buffer, 128);
			PRINTF("compare %s --> %s == %s\n", id, str, sample);
			ASSERT_STRCASEEQ(str, sample);
		}
		else
		{
			ERRORF("%s parser error\n\n", id);
			ASSERT_TRUE(false);
		}


	}

	static const char* print_hex(char* buf, int bsize, uint8_t* data, int length)
	{
		char* p = buf;
		int w = 0;
		for (int i = 0; i < length; i++)
		{
			w += std_snprintf(buf+w, bsize-w, "%02X", data[i]);
		}
		buf[w] = 0;
		return buf;
	}

	static void test_guid()
	{
		guid_t guid1 = std_generate_guid();
		char buff1[128];
		const char* guid_str1 = std_guid_to_string(guid1, buff1, 128);
		PRINTF("--- generated uuid {%s}\n", guid_str1);

		guid_t guid2;
		std_parse_guid(guid2, guid_str1, -1);

		char buff2[128];
		const char* guid_str2 = std_guid_to_string(guid2, buff2, 128);
		PRINTF("--- parsed uuid {%s}\n", guid_str2);
		bool result = std_is_guid_equal(guid1, guid2);
		PRINTF("--- comparing result %s\n", result?"true":"false");
		ASSERT_TRUE(result);

	}

	const char* test_ids[4][2] = { { "tgw/ivr/$", "tgw/ivrp/$" }, { "*", "*/*/*" }, { "*/rtp/*", "*/rtp/*" }, { "prefix2/rtp/$", "prefix2/rtp/$" } };

	TEST(test_parser, termid_gen)
	{
		for (int i = 0; i < 4; i++)
			test_term_id(test_ids[i][0], test_ids[i][1]);
	}

	TEST(test_parser, uuid)
	{
		test_guid();
	}
}
//-----------------------------------------------
