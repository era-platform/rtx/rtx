/* RTX H.248 Media Gate Engine Test Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "test_rtx_megaco.h"

 //-----------------------------------------------
//
//-----------------------------------------------
namespace rtx_megaco
{
	static char* make_level(char* buffer, int level)
	{
		static char max_level[] = "-max-level-";
		if (level > 50)
			return max_level;
		if (level < 0)
		{
			buffer[0] = 0;
		}
		else
		{
			memset(buffer, ' ', level * 2);
			buffer[level * 2] = 0;
		}

		return buffer;
	}

	void test_megaco_parser(const char* message)
	{
		h248::Parser parser;

		parser.startReading(message);
		bool eof = false;

		const char* name;
		const char* value;

		int level_shift = 0;
		char level[64];

		while (!eof)
		{
			h248::ParserState prev = parser.getState();
			h248::ParserState res = parser.readNext();

			switch (res)
			{
			case h248::ParserState::Auth:
				printf("%s = %s\n", parser.getTokenName(), parser.getTokenValue());
				break;
			case h248::ParserState::Startline:
				printf("%s %s\n", parser.getTokenName(), parser.getTokenValue());
				break;
			case h248::ParserState::Token:
				value = parser.getTokenValue();
				name = parser.getTokenName();

				if (value[0] == 0)
				{
					printf("%s%s", make_level(level, level_shift), name);
				}
				else
				{
					printf("%s%s = %s", make_level(level, level_shift), name, value);
				}

				if (std_stricmp(name, "Local") == 0 || std_stricmp(name, "Remote") == 0)
				{
					if ((res = parser.readTokenGroup()) == h248::ParserState::EndGroup)
					{
						printf(" {\n%s\n", parser.getTokenValue());
						printf("%s}", make_level(level, --level_shift));
					}
					else if (res == h248::ParserState::Error)
					{
						ERRORF("; error\n");
						eof = true;
					}
				}
				else if (parser.hasGroup())
				{
					printf(" {\n");
					++level_shift;
				}
				else
				{
					printf("\n");
				}
				break;
			case h248::ParserState::EndGroup:
				printf("\n%s}\n", make_level(level, --level_shift));
				break;
			case h248::ParserState::EndOfFile:
				printf("\n; eof\n");
				eof = true;
				break;
			case h248::ParserState::Error:
			case h248::ParserState::False:
				ERRORF("\n; error\n");
				eof = true;
				break;
			}
		}

		printf("; end\n");
	}

	const char* message[] = {
"MEGACO/1 [124.124.124.222]\n\
Transaction = 9998 {\n\
\tContext = - {\n\
\t\tServiceChange = root {\n\
\t\t\tServices {\n\
\t\t\t\tMethod = Restart,\n\
\t\t\t\tServiceChangeAddress = 55555,\n\
\t\t\t\tProfile = resgw/1,\n\
\t\t\t\tReason = \"901 mg col boot\"\n\
\t\t\t}\n\
\t\t}\n\
\t}\n\
}",
"MEGACO/1 [123.123.123.4]:55555\n\
\tTransaction = 9999 {\n\
\t\tContext = -{\n\
\t\t\tModify = 11111111/00000000/11111111 {\n\
\t\t\t\tMedia{\n\
\t\t\t\t\tStream = 1 {\n\
\t\t\t\t\t\tLocalControl{\n\
\t\t\t\t\t\t\tMode = SendReceive,\n\
\t\t\t\t\t\t\ttdmc/gain = 2,\n\
\t\t\t\t\t\t\ttdmc/ec = g165\n\
\t\t\t\t\t\t},\n\
\t\t\t\t\t\tLocal{\n\
v=0\n\
c=IN IP4 $\n\
m=audio $ RTP / AVP 0\n\
a=fmtp:PCMU VAD = X - NNVAD\n\
\n\
\t\t\t\t\t\t}\n\
\t\t\t\t\t}\n\
\t\t\t\t},\n\
\t\t\t\tEvents = 2222 {\n\
\t\t\t\tal/of\n\
\t\t\t}\n\
\t\t}\n\
\t}\n\
}",
"MEGACO/3 [124.124.124.222]:55555\n\
Transaction = 9898 {\n\
Context = 1 {\n\
Priority = 15,\n\
Emergency,\n\
Topology {\n\
11111111/00000000/00000000,\n\
11111111/00000000/00001111,\n\
Oneway,\n\
11111111/00001111/00000000,\n\
11111111/00001111/00001111,\n\
Bothway\n\
},\n\
ContextAttr {\n\
nt/jit =  [\n\
40,\n\
50,\n\
50\n\
]\n\
},\n\
Notify = 11111111/00000000/00000000 {\n\
ObservedEvents = 2222 {\n\
19990729T22000000:al/of\n\
}\n\
},\n\
Notify = 11111111/00000000/00000000 {\n\
ObservedEvents = 2222 {\n\
19990729T22000111:al/on\n\
}\n\
}\n\
}\n\
}",
"MEGACO/3 [125.125.125.111]:55555\n\
Transaction = 10001 {\n\
Context = - {\n\
Modify = 11111111/00000000/00000000 {\n\
Events = 2223 {\n\
al/on {\n\
strict=state\n\
},\n\
al/on {\n\
Stream = 7801,\n\
strict=state,\n\
KeepActive,\n\
DigitMap = dialplan00,\n\
ImmediateNotify\n\
}\n\
},\n\
Signals {\n\
cg/rt\n\
},\n\
DigitMap = dialplan00 {\n\
(0s| 00s|[1-7]xlxx|8lxxxxxxx|#xxxxxxx|*xx|9l1xxxxxxxxxx|9l011x.s)\n\
}\n\
}\n\
}\n\
}",
"MEGACO/1 [123.123.123.4]:55555\n\
Transaction = 10001 {\n\
Context = - {\n\
Modify = 11111111/00000000/00000000 {\n\
Events = 2223 {\n\
al/on,\n\
dd/ce {\n\
DigitMap = dialplan00\n\
}\n\
},\n\
Signals Signals{\n\
cg/rt\n\
},\n\
DigitMap = dialplan00 {\n\
T:1,\n\
S:23,\n\
L:99,\n\
(0s| 00s|[1-7]xlxx|8lxxxxxxx|#xxxxxxx|*xx|9l1xxxxxxxxxx|9l011x.s)\n\
}\n\
}\n\
}\n\
}",
"MEGACO/1 [125.125.125.111]:55555\n\
Transaction = 50008 {\n\
Context = 5000 {\n\
Notify = 11111111/11111111/00000000 {\n\
ObservedEvents = 1235 {\n\
19990729T24020002:al/on\n\
}\n\
}\n\
}\n\
}Transaction = 50009 {\n\
Context = 5001 {\n\
Notify = 11111111/11111111/11111111 {\n\
ObservedEvents = 1235 {\n\
19990729T24020002:al/on\n\
}\n\
}\n\
}\n\
}"
	};

	TEST(test_parser, message_hr)
	{
		PRINTF("Test H.248 parser (human readable)...\n");

		int count = sizeof(message) / sizeof(const char*);
		for (int i = 0; i < count; i++)
			test_megaco_parser(message[i]);
	}
}
//-----------------------------------------------
