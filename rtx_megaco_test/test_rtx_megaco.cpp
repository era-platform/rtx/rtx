/* RTX H.248 Media Gate Engine Test Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "test_rtx_megaco.h"
//--------------------------------------
//
//--------------------------------------
#if defined (DEBUG_MEMORY)
void* operator new (size_t size, char* file, int line)
{
	return std_mem_checker_malloc(size, file, line, rc_malloc_new_e);
}
void* operator new[](size_t size, char* file, int line)
{
	return std_mem_checker_malloc(size, file, line, rc_malloc_newar_e);
}
void operator delete (void* mem_ptr)
{
	std_mem_checker_free(mem_ptr, rc_malloc_delete_e);
}
void operator delete[](void* mem_ptr)
{
	std_mem_checker_free(mem_ptr, rc_malloc_deletear_e);
}
#endif
//-----------------------------------------------
//
//-----------------------------------------------
int main(int argc, char* argv[])
{
	printf(" - RTX TEST H.248 run_tests\n");
	rtl::String toutput, vec_path;
	printf(" - RTX TEST H.248 argc: %d\n", argc);
	for (int i = 0; i < argc; i++)
	{
		printf(" - RTX TEST H.248 check arg[%d]=%s\n", i, argv[i]);
		rtl::String str(argv[i]);
		if (str.indexOf("toutput=") != BAD_INDEX)
		{
			toutput.append(argv[i] + 8);
		}
		else if (str.indexOf("vecpath=") != BAD_INDEX)
		{
			vec_path = argv[i] + 8;
		}
	}

	printf(" - RTX TEST H.248 check vecpath\n");
	if (!vec_path.isEmpty())
	{
		Config.set_config_value("vecpath", vec_path);
	}
	printf(" - RTX TEST H.248 vecpath = %s\n", (const char*)vec_path);

	printf(" - RTX TEST H.248 check toutput\n");
	if (!toutput.isEmpty())
	{
		rtl::String path("xml:");
		path << toutput << FS_PATH_DELIMITER << "rtx_megaco_test_result.xml";
		::testing::GTEST_FLAG(output) = (const char*)path;
	}

	printf(" - RTX TEST H.248 InitGoogleTest\n");
	::testing::InitGoogleTest(&argc, argv);
	::testing::InitGoogleMock(&argc, argv);
	int Res = RUN_ALL_TESTS();

	printf(" - RTX TEST H.248 end(%d)\n", Res);

	return Res;
}
//-----------------------------------------------
