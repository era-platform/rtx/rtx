/* RTX H.248 Media Gate. G.722 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "G722_codec.h"

extern int g_rtx_g722_decoder_counter = -1;
extern int g_rtx_g722_encoder_counter = -1;

G722_encoder_t::~G722_encoder_t()
{
	destroy();
}

bool G722_encoder_t::initialize(const media::PayloadFormat& format)
{
	if (m_ctx != nullptr)
		destroy();

	m_ctx = g722_encode_init(nullptr, 64000, G722_PACKED); // G722_SAMPLE_RATE_8000 | 

	return true;
}

void G722_encoder_t::destroy()
{
	if (m_ctx != nullptr)
	{
		g722_encode_free(m_ctx);
		m_ctx = nullptr;
	}
}

const char* G722_encoder_t::getEncoding()
{
	return "g722";
}

uint32_t G722_encoder_t::encode(const uint8_t* buff_from, uint32_t size_from, uint8_t* buff_to, uint32_t size_to)
{
	return g722_encode(m_ctx, (uint8_t*)buff_to, (const short*)buff_from, size_from/sizeof(short));
}

uint32_t G722_encoder_t::packetize(rtp_packet* packet, const uint8_t* buff, uint32_t buff_size)
{
	if (packet == nullptr)
	{
		return 0;
	}

	if (buff == nullptr)
	{
		return 0;
	}

	if (buff_size > rtp_packet::PAYLOAD_BUFFER_SIZE)
	{
		return 0;
	}

	if (packet->set_payload(buff, buff_size) == 0)
	{
		return 0;
	}

	packet->set_payload_type(9);

	packet->set_samples(160);

	return buff_size;
};

// �������� ������ ������ � ������� �������� �������.
uint32_t G722_encoder_t::get_frame_size()
{
	return 640;
}

G722_decoder_t::G722_decoder_t() : m_ctx(nullptr)
{
}

G722_decoder_t::~G722_decoder_t()
{
	destroy();
}

bool G722_decoder_t::initialize(const media::PayloadFormat& format)
{
	if (m_ctx != nullptr)
		destroy();

	m_ctx = g722_decode_init(nullptr, 64000, G722_PACKED); // G722_SAMPLE_RATE_8000

	return true;
}

void G722_decoder_t::destroy()
{
	if (m_ctx != nullptr)
	{
		g722_decode_free(m_ctx);
		m_ctx = nullptr;
	}
}

const char* G722_decoder_t::getEncoding()
{
	return "g722";
}

uint32_t G722_decoder_t::depacketize(const rtp_packet* packet, uint8_t* buff, uint32_t buff_size)
{
	if (packet == nullptr)
	{
		return 0;
	}

	if (buff == nullptr)
	{
		return 0;
	}

	if (buff_size < packet->get_payload_length())
	{
		return 0;
	}

	uint32_t payload_length = packet->get_payload_length();

	memcpy(buff, packet->get_payload(), payload_length);

	return payload_length;
}

uint32_t G722_decoder_t::decode(const uint8_t* buff_from, uint32_t size_from, uint8_t* buff_to, uint32_t size_to)
{
	//short rbuff[640];
	//short* samples = (short*)buff_to;
	//int used = 0;

	int res = g722_decode(m_ctx, (short*)buff_to, buff_from, size_from);

	return res * sizeof(short);
}

// �������� ������ ������ � ������� �������� �������.
uint32_t G722_decoder_t::get_frame_size_pcm()
{
	return 640;
}
// �������� ������ ������ G722
uint32_t G722_decoder_t::get_frame_size_cod()
{
	return 64;
}
