/* RTX H.248 Media Gate. G.722 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtx_G722_module.h"
#include "std_logger.h"

#include "G722_codec.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#if defined (DEBUG_MEMORY)
void* operator new (size_t size, char* file, int line)
{
	return std_mem_checker_malloc(size, file, line, rc_malloc_new_e);
}
void* operator new[](size_t size, char* file, int line)
{
	return std_mem_checker_malloc(size, file, line, rc_malloc_newar_e);
}
void operator delete (void* mem_ptr)
{
	std_mem_checker_free(mem_ptr, rc_malloc_delete_e);
}
void operator delete[](void* mem_ptr)
{
	std_mem_checker_free(mem_ptr, rc_malloc_deletear_e);
}
#endif
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_G722_MODULE_API bool initlib()
{
	g_rtx_g722_decoder_counter = rtl::res_counter_t::add("mge_g722_decoder");
	g_rtx_g722_encoder_counter = rtl::res_counter_t::add("mge_g722_encoder");

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_G722_MODULE_API void freelib()
{
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_G722_MODULE_API const char* get_codec_list()
{
	return "G722";
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_G722_MODULE_API bool get_available_audio_payloads(media::PayloadSet& set)
{
	set.addStandardFormat(media::PayloadId::G722);

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_G722_MODULE_API mg_audio_decoder_t* create_audio_decoder(const media::PayloadFormat& format)
{
	rtl::String codec_name(format.getEncoding());
	if (codec_name.isEmpty())
	{
		return nullptr;
	}
	codec_name.trim();
	codec_name.toLower();

	if (rtl::String::compare("g722", codec_name, false) == 0)
	{
		G722_decoder_t* decoder = NEW G722_decoder_t();
		if (decoder->initialize(format))
		{
			return decoder;
		}

		DELETEO(decoder);
	}

	return nullptr;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_G722_MODULE_API void release_audio_decoder(mg_audio_decoder_t* decoder)
{
	if (decoder == nullptr)
	{
		return;
	}

	rtl::String type(decoder->getEncoding());
	type.toLower();

	if (rtl::String::compare("g722", type, false) == 0)
	{
		G722_decoder_t* g711a_decoder = (G722_decoder_t*)decoder;
		if (g711a_decoder != nullptr)
		{
			DELETEO(g711a_decoder);
			g711a_decoder = nullptr;
		}
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_G722_MODULE_API mg_audio_encoder_t* create_audio_encoder(const media::PayloadFormat& format)
{
	rtl::String codec_name(format.getEncoding());
	if (codec_name.isEmpty())
	{
		return nullptr;
	}

	codec_name.trim();
	codec_name.toLower();

	if (rtl::String::compare("g722", codec_name, false) == 0)
	{
		G722_encoder_t* encoder = NEW G722_encoder_t();
		if (encoder->initialize(format))
		{
			return encoder;
		}

		DELETEO(encoder);
	}

	return nullptr;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_G722_MODULE_API void release_audio_encoder(mg_audio_encoder_t* encoder)
{
	if (encoder == nullptr)
	{
		return;
	}

	rtl::String type(encoder->getEncoding());
	type.toLower();

	if (rtl::String::compare("g722", type, false) == 0)
	{
		G722_encoder_t* g711a_encoder = (G722_encoder_t*)encoder;
		if (g711a_encoder != nullptr)
		{
			DELETEO(g711a_encoder);
			g711a_encoder = nullptr;
		}
	}
}
//------------------------------------------------------------------------------
