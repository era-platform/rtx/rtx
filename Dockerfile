FROM ubuntu:14.04

RUN apt-get update
RUN apt-get install -y wget 
RUN apt-get install -y apt-transport-https 
RUN apt-get install -y software-properties-common

RUN add-apt-repository ppa:rayanayar/openmcu-ru-dev

RUN mkdir -p /var/rtx_mg3

RUN apt-get update && apt-get install -y git curl build-essential unzip libyuv-dev libmp3lame-dev libgtest-dev libtiff5-dev libjpeg-dev zlib1g-dev libopus-dev libssl-dev cmake google-mock 
RUN apt-get install -y nasm
RUN apt-get install -y pkg-config

RUN mkdir -p /build
RUN cd /build && git clone -b release/3.4 https://github.com/FFmpeg/FFmpeg.git
RUN cd /build/FFmpeg && ./configure --enable-shared && make && make install

VOLUME /var/rtx_mg3