﻿/* RTX H.248 Media Gate
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mge_manager.h"

namespace mg3
{

	//--------------------------------------
	//
	//--------------------------------------
	rtl::MutexWatch Gate::m_sync("mg-gate-man");
	rtl::ArrayT<VirtualGate*> Gate::m_gate_list;
	rtl::Module Gate::m_module;
	h248::IMediaGate* Gate::m_mg_module;
	Gate::mg_module_callback_interface_imp_t Gate::m_eventHandler;
	//--------------------------------------
	//
	//--------------------------------------
	void Gate::mg_module_callback_interface_imp_t::mge__event_raised()
	{
		// ...
	}
	void Gate::mg_module_callback_interface_imp_t::mge__error_raised()
	{
		// ...
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Gate::create(const char* path, const char* logpath)
	{
		// load module and bind it with interface ptr
		// самое сложное!!!
		// как под линуксом там?
		// 

		LOG_CALL("mge-man", "create...");

		rtl::Module module;

		LOG_CALL("mge-man", "load spandsp...");

		//if (!module.load("/home/george/Develop/rtx_mg3/X64/Release/libspandsp.so"))
		//{
		//	LOG_CALL("mge-man", "spandsp not loaded!");
		//}
		//else
		//{
		//	void* fun_ptr = module.bind_symbol("fax_init");

		//	LOG_CALL("mge-man", "spandsp::fax_init function %s", fun_ptr == nullptr ? "not found" : "found");
		//}

		LOG_CALL("mge-man", "load media engine module %s...", path);

		if (!m_module.load(path))
		{
			LOG_ERROR("mge-man", "create : failed : media engine module not loaded");
			return false;
		}

		h248::pfn_createMediaGate module_init = (h248::pfn_createMediaGate)m_module.bindSymbol("create_media_gateway_module");

		if (module_init == nullptr)
		{
			LOG_ERROR("mge-man", "create : failed : module has no entry point 'create_media_gateway_module'");
			return false;
		}

		m_mg_module = module_init(logpath);

		if (m_mg_module == nullptr)
		{
			LOG_ERROR("mge-man", "create : failed : module_init() return error");
			return false;
		}

		bool result = m_mg_module->MediaGate_initialize(&m_eventHandler);

		LOG_CALL("mge-man", "create : %s", result ? "done" : "failed : MediaGate_initialize() return error");

		return result;
	}
	//--------------------------------------
	// delete all virtual mg and unload module
	//--------------------------------------
	void Gate::destroy()
	{
		LOG_CALL("mge-man", "destroy...");

		//rtl::MutexLock lock(m_sync);
		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		LOG_CALL("mge-man", "destroy : free %d virtual gates...", m_gate_list.getCount());

		for (int i = 0; i < m_gate_list.getCount(); i++)
		{
			VirtualGate* gate = m_gate_list[i];

			DELETEO(gate);
		}

		m_gate_list.clear();

		LOG_CALL("mge-man", "destroy : unload module...");

		if (m_mg_module != nullptr)
		{
			m_mg_module->MediaGate_destroy();
			m_mg_module = nullptr;
		}

		h248::pfn_removeMediaGate module_free = (h248::pfn_removeMediaGate)m_module.bindSymbol("remove_media_gateway");

		if (module_free == nullptr)
		{
			LOG_CALL("mge-man", "destroy : failed : module has no entry point 'remove_media_gateway'");
		}
		else
		{
			module_free();
		}

		m_module.unload();

		LOG_CALL("mge-man", "destroy : done");
	}
	//--------------------------------------
	//
	//--------------------------------------
	VirtualGate* Gate::create_virtual_gate(IMG3_EventHandler* handler)
	{
		LOG_CALL("mge-man", "create virtual gate...");

		VirtualGate* gate = NEW VirtualGate(handler);

		//rtl::MutexLock lock(m_sync);
		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		m_gate_list.add(gate);

		LOG_CALL("mge-man", "create virtual gate : done");

		return gate;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Gate::destroy_virtual_gate(VirtualGate* media_gate)
	{
		LOG_CALL("mge-man", "destroy virtual gate...");

		//rtl::MutexLock lock(m_sync);
		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		int index = m_gate_list.indexOf(media_gate);

		if (index != BAD_INDEX)
		{
			m_gate_list.removeAt(index);

			DELETEO(media_gate);

			LOG_CALL("mge-man", "destroy virtual gate : done");
		}
		else
		{
			LOG_WARN("mge-man", "destroy virtual gate : gate not found");
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Gate::set_interface_prefix(const char* ip_address, const char* prefix)
	{
		//rtl::MutexLock lock(m_sync);
		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		if (m_mg_module != nullptr)
		{
			LOG_CALL("mge-man", "set interface prefix %s -> %s", ip_address, prefix);

			m_mg_module->MediaGate_setInterfaceName(ip_address, prefix);
		}
		else
		{
			LOG_WARN("mge-man", "set interface prefix : failed : module not loaded");
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Gate::remove_interface_prefix(const char* prefix)
	{
		//rtl::MutexLock lock(m_sync);
		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		if (m_mg_module != nullptr)
		{
			m_mg_module->MediaGate_removeInterfaceName(prefix);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Gate::add_rtp_interface(const char* prefix, uint16_t port_begin, uint16_t port_count)
	{
		//rtl::MutexLock lock(m_sync);
		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		if (m_mg_module != nullptr)
		{
			LOG_CALL("mge-man", "add interface rtp port range to %s -> %u:%u", prefix, port_begin, port_count);

			return m_mg_module->MediaGate_addPortRangeRTP(prefix, port_begin, port_count);
		}

		LOG_WARN("mge-man", "add interface rtp port range : failed : module not loaded");
		return false;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Gate::free_rtp_interface(const char* iface)
	{
		//rtl::MutexLock lock(m_sync);
		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		if (m_mg_module != nullptr)
		{
			m_mg_module->MediaGate_freePortRangeRTP(iface);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	h248::IMediaContext* Gate::create_context()
	{
		//rtl::MutexLock lock(m_sync);
		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		if (m_mg_module != nullptr)
		{
			LOG_CALL("mge-man", "create context...");

			h248::IMediaContext* context = m_mg_module->MediaGate_createContext();

			LOG_CALL("mge-man", "create context : %s : %p", context != nullptr ? "done" : "failed", context);

			return context;
		}
		else
		{
			LOG_WARN("mge-man", "create context : failed : module not loaded");
		}
		return nullptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Gate::destroy_context(h248::IMediaContext* context)
	{
		LOG_CALL("mge-man", "destroy context %p", context);

		//rtl::MutexLock lock(m_sync);
		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		if (m_mg_module != nullptr)
		{
			m_mg_module->MediaGate_destroyContext(context);
		}
		else
		{
			LOG_CALL("mge-man", "destroy context : failed : module not loaded");
		}
	}
}
//--------------------------------------

