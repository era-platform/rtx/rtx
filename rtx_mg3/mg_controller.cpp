﻿/* RTX H.248 Media Gate
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_service.h"
#include "mg_controller.h"
#include "mg_service_change.h"

namespace mg3
{
	//--------------------------------------
	//
	//--------------------------------------
#define ASYNC_RESTART_EVENT				1001
#define ASYNC_PINGPONG_EVENT			1002
#define ASYNC_CONNECT_TIMEOUT_EVENT		1003
//--------------------------------------
#define RESTART_COUNT_KEY "restart-count-to-mgc"
//--------------------------------------
//
//--------------------------------------
	volatile uint32_t MGC_Proxy::s_idGen = 0;
	//--------------------------------------
	//
	//--------------------------------------
	MGC_Proxy::MGC_Proxy() : m_mgcVersion(3), m_state(State_New), m_serviceSync("mgc-proxy")
	{
		m_id = std_interlocked_inc(&s_idGen);
		memset(&m_route, 0, sizeof m_route);
		m_requestTranID.id = 0;
		m_pingTranID.id = 0;
		m_restartRaised = false;
		m_currentRouteIndex = 0;
		m_stoping = false;
		m_stoped = false;
	}
	//--------------------------------------
	//
	//--------------------------------------
	MGC_Proxy::~MGC_Proxy()
	{
		destroy();
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool MGC_Proxy::initialize(const rtl::ArrayT<h248::EndPoint>& mgc_route_list)
	{
		if (m_state != State_New || mgc_route_list.getCount() == 0)
			return false;

		rtl::MutexWatchLock lock(m_serviceSync, __FUNCTION__);

		m_altRouteList.assign(mgc_route_list);

		h248::EndPoint mgc_point = m_altRouteList[0];

		m_route.type = mgc_point.type;
		m_route.remoteAddress = mgc_point.address;
		m_route.remotePort = mgc_point.port;
		m_route.localAddress.s_addr = 0;
		m_route.localPort = 0;

		m_dispatcher.initialize(m_id, this, m_mgcVersion);
		m_dispatcher.setControlRoute(m_route);

		// создаем виртуальный медиа шлюз
		m_gate = Gate::create_virtual_gate(this);

		if (m_gate == nullptr)
		{
			m_dispatcher.destroy();
			m_state = State_Failed;

			LOG_WARN("mgc-prx", "cid:%u : initializing failed : media gate error!", m_id);

			return false;
		}

		m_gate->start();

		m_state = State_Ready;

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void MGC_Proxy::destroy()
	{
		if (m_state == State_Terminated)
			return;

		rtl::MutexWatchLock lock(m_serviceSync, __FUNCTION__);

		LOG_CALL("mgc-prx", "cid:%u : destroying...", m_id);

		rtl::Timer::stopTimer(pingpongTimerProc, this, 0);

		m_state = State_Terminated;

		m_dispatcher.destroy();

		if (m_gate != nullptr)
		{
			m_gate->stop();

			Gate::destroy_virtual_gate(m_gate);

			m_gate = nullptr;
		}

		h248::TransportManager::removeRoute(m_route, true);

		LOG_CALL("mgc-prx", "cid:%u : destroyed", m_id);
	}
	//--------------------------------------
	// стартовая функция: отправка сообщения на MGC
	//--------------------------------------
	bool MGC_Proxy::start()
	{
		if (m_state != State_Ready)
		{
			LOG_WARN("mgc-prx", "cid:%u : starting failed : invalid state %s...", m_id, State_toString(m_state));
			return false;
		}

		raiseRestartEvent(h248::Token::Restart, h248::Reason::c901_Cold_boot);

		return true;
	}
	//--------------------------------------
	// стартовая функция: отправка сообщения на MGC
	// состояния: ready, disconnected, failed?
	//--------------------------------------
	bool MGC_Proxy::restart(h248::Token method, h248::Reason code, const char* reason)
	{
		LOG_CALL("mgc-prx", "cid:%u : restarting : method %s reason %u %s", m_id, Token_toStringLong(method), code, Reason_toString(code));

		if (m_state != State_Ready && m_state != State_Disconnected && m_state != State_Failed)
		{
			LOG_WARN("mgc-prx", "cid:%u : restarting failed : invalid state %s...", m_id, State_toString(m_state));
			return false;
		}

		h248::TransportManager::removeRoute(m_route, true);

		if (m_currentRouteIndex >= m_altRouteList.getCount())
		{
			m_currentRouteIndex = 0;
		}

		h248::EndPoint point = m_altRouteList[m_currentRouteIndex];

		m_route.type = point.type;
		m_route.remoteAddress = point.address;
		m_route.remotePort = point.port;
		m_route.localAddress.s_addr = 0;
		m_route.localPort = 0;

		// стартуем таймер подключения (мах 1 сек)

		rtl::Timer::setTimer(connectTimer, this, 1, Service::getConnectTimeout(), false);

		if (!h248::TransportManager::createRoute(m_route, Service::getConnectTimeout()))
		{
			LOG_WARN("mgc-prx", "cid:%u : restarting failed : connection returns net error!", m_id);
			rtl::Timer::stopTimer(connectTimer, this, 1);
			m_currentRouteIndex++;
			return false;
		}

		LOG_CALL("mgc-prx", "cid:%u : restarting ok : connected. Sending register...", m_id);

		rtl::Timer::stopTimer(connectTimer, this, 1);

		m_dispatcher.stop();
		m_dispatcher.setControlRoute(m_route);
		m_pingTranID.id = 0;
		// отправляем начальные дейстрвия
		//rtl::MutexLock lock(m_serviceSync);
		rtl::MutexWatchLock lock(m_serviceSync, __FUNCTION__);

		LOG_CALL("mgc-prx", "cid:%u : restarting with method %s code %u reason %s...", m_id, Token_toStringLong(method), code, reason);

		if (!sendServiceChange(method, code, reason))
		{
			m_state = State_Disconnected;
			m_currentRouteIndex++;
			LOG_CALL("mgc-prx", "cid:%u : restarting failed : send servie message failed", m_id);

			return false;
		}

		m_state = State_Connecting;
		LOG_CALL("mgc-prx", "cid:%u : restart ok", m_id);
		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool MGC_Proxy::stop()
	{
		LOG_CALL("mgc-prx", "cid:%u : stopping...", m_id);

		if (m_stoped)
		{
			LOG_CALL("mgc-prx", "cid:%u : already stoped", m_id);
			return true;
		}

		m_state = State_Disconnecting;

		rtl::Timer::stopTimer(pingpongTimerProc, this, 0);

		m_dispatcher.stop();

		bool result = sendServiceChange(h248::Token::Forced, h248::Reason::c905_Termination_taken_out_of_service, nullptr);

		m_stoping = true;

		while (!m_stoped)
		{
			LOG_CALL("mgc-prx", "cid:%u : wait stoped", m_id);
			rtl::Thread::sleep(100);
		}

		LOG_CALL("mgc-prx", "cid:%u : stopped (%s)", m_id, STR_BOOL(result));

		return result;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool MGC_Proxy::sendServiceChange(h248::Token method, h248::Reason code, const char* reason)
	{
		LOG_CALL("mgc-prx", "cid:%u : send ServiceChange %s %u %s...", m_id, Token_toStringLong(method), code, reason);

		h248::Field request(h248::Token::Transaction);

		h248::Field* context = request.addField(h248::Token::Context, "-");

		h248::Field* serviceChanged = context->addField(h248::Token::ServiceChange, "root");

		h248::Field* services = serviceChanged->addField(h248::Token::Services);

		services->addField(h248::Token::Method, Token_toString(method));
		services->addField(h248::Token::Version, m_mgcVersion);

		rtl::String reason_text;

		const char* text_ptr = reason != nullptr && reason[0] != 0 ? reason : Reason_toString(code);

		if (reason_text != nullptr)
		{
			reason_text << '\"' << (uint32_t)code << ' ' << text_ptr << '\"';
		}
		else
		{
			reason_text << '\"' << (uint32_t)code << '\"';
		}

		services->addField(h248::Token::Reason, reason_text);
		services->addField(h248::Token::Profile, "resgw/1");

		m_state = State_Connecting;

		LOG_CALL("mgc-prx", "cid:%u : send service change 1", m_id);

		m_dispatcher.generateTranId(m_requestTranID);

		bool result = m_dispatcher.sendRequest(&request, this, m_requestTranID);

		LOG_CALL("mgc-prx", "cid:%u : ServiceChange %s", m_id, result ? "sent" : "failed");

		return result;
	}
	//--------------------------------------
	// пинг
	//--------------------------------------
	void MGC_Proxy::sendPing()
	{
		if (m_pingTranID.id != 0)
		{
			// ответ отсутствует!
			LOG_CALL("mgc-prx", "cid:%u : previous ping not replied", m_id);
			//h248::TransportManager::removeRoute(m_route, true);
			raiseRestartEvent(h248::Token::Failover, h248::Reason::c907_Transmission_failure);
			return;
		}

		h248::Field request(h248::Token::Transaction);

		h248::Field* context = request.addField(h248::Token::Context, "-");

		h248::Field* serviceChanged = context->addField(h248::Token::ServiceChange, "root");

		h248::Field* services = serviceChanged->addField(h248::Token::Services);

		services->addField(h248::Token::Method, Token_toString(h248::Token::Restart));
		services->addField(h248::Token::Reason, "\"900 Service Restored\"");
		services->addField(h248::Token::Version, m_mgcVersion);

		LOG_CALL("mgc-prx", "cid:%u : send ping", m_id);

		m_dispatcher.generateTranId(m_pingTranID);

		if (!m_dispatcher.sendRequest(&request, this, m_pingTranID))
		{
			raiseRestartEvent(h248::Token::Failover, h248::Reason::c907_Transmission_failure);
			m_pingTranID.id = 0;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void MGC_Proxy::raiseRestartEvent(h248::Token method, h248::Reason reason)
	{
		bool raise_event = false;

		LOG_CALL("mgc-prx", "cid:%u : raise restart! method:%s reason:%s", m_id, Token_toStringLong(method), Reason_toString(reason));

		{
			rtl::MutexWatchLock lock(m_serviceSync, __FUNCTION__);

			if (!m_restartRaised)
			{
				m_restartRaised = raise_event = true;
			}
		}

		if (raise_event)
		{
			LOG_CALL("mgc-prx", "cid:%u : restarting", m_id);
			MegacoAsyncCall(asyncCallback, this, ASYNC_RESTART_EVENT, (int)method, (void*)reason);
		}
	}
	//--------------------------------------
	// асинхронный рестарт после разрыва соединения с MGC
	//--------------------------------------
	void MGC_Proxy::asyncRestart(h248::Token method, h248::Reason reason)
	{
		// если порвалась сеть то пытаемся сами переподключится
		m_state = method == h248::Token::Failover ? State_Failed : State_Disconnected;

		if (m_stoping)
		{
			m_stoped = true;
			LOG_CALL("mgc-prx", "cid:%u : stoping signal...", m_id);
			return;
		}

		int attempts_count = 0;
		int max_attempts_count = m_altRouteList.getCount();
		int sleep_time = Service::getConnectFailTimeout();

		LOG_CALL("mgc-prx", "cid:%u : async restart : max attempts count:%d, route index:%d (sleep-timeout:%d)", m_id, max_attempts_count, m_currentRouteIndex, sleep_time);

		int restart_count = 0;

		uint32_t ts = rtl::DateTime::getTicks();

		while (!restart(method, reason, nullptr))
		{
			if (m_stoping)
			{
				m_stoped = true;
				LOG_CALL("mgc-prx", "cid:%u : stoping signal...", m_id);
				return;
			}

			attempts_count++;

			if (attempts_count >= max_attempts_count)
			{
				attempts_count = 0;

				uint32_t curr_ts = rtl::DateTime::getTicks();
				int diff = curr_ts - ts;

				if (diff < sleep_time)
				{
					LOG_CALL("mgc-prx", "cid:%u : async restart : wait for %d ms...", m_id, sleep_time - diff);
					rtl::Thread::sleep(sleep_time - diff);
				}
				else
				{
					LOG_WARN("mgc-prx", "cid:%u : async restart : no timeout! (%d >= %d)", m_id, diff, sleep_time);
				}

				ts = rtl::DateTime::getTicks();
			}
		}

		LOG_CALL("mgc-prx", "cid:%u : async restart : done...", m_id);

		m_restartRaised = false;
	}
	//--------------------------------------
	// входящая транзакция
	//--------------------------------------
	void MGC_Proxy::IRequestReceiver_requestReceived(h248::TransactionID tid, const h248::Field* request) // , h248::Field* reply
	{
		// 

		// проверка акций на сервисные сообщения
		for (int i = 0; i < request->getFieldCount(); i++)
		{
			const h248::Field* action = request->getFieldAt(i);

			if (action->getType() != h248::Token::Context)
			{
				// ошибка!
			}

			const char* contextId = action->getValue();

			// NULL context -> сервисное сообщение
			if (contextId[0] == '-' && contextId[1] == 0)
			{
				LOG_EVENT("mgc-prx", "cid:%u : service action request received", m_id);

				h248::Field reply(h248::Token::Reply, request->getValue());
				h248::Field* action_reply = reply.addField(h248::Token::Context, action->getValue());

				serviceAction(action, action_reply);

				m_dispatcher.sendReply(&reply, tid);
			}
			else
			{
				LOG_EVENT("mgc-prx", "cid:%u : request received : media gate action", m_id);

				m_gate->doAction(request, tid);
			}
		}
	}
	//--------------------------------------
	// 
	//--------------------------------------
	void MGC_Proxy::IRequestReceiver_transportFailure(int net_error, const h248::Route& route)
	{
		// если инициатива дисконнекта наша то сообщаем сервису
		if (m_state == State_Terminated)
		{
			LOG_CALL("mgc-prx", "cid:%u : transport failure : state terminated!", m_id);
			rtl::Timer::stopTimer(pingpongTimerProc, this, 0);
			Service::proxyFailed(this);

			return;
		}

		raiseRestartEvent(h248::Token::Failover, h248::Reason::c907_Transmission_failure);
		m_pingTranID.id = 0;
	}
	//--------------------------------------
	// 
	//--------------------------------------
	void MGC_Proxy::IRequestReceiver_transportDisconnected(const h248::Route& route)
	{
		// разрыв соединения по инициативе MGC переход на следующий или останов сервиса
		if (m_state == State_Terminated)
		{
			LOG_CALL("mgc-prx", "cid:%u : transport disconnected : state terminated!", m_id);
			rtl::Timer::stopTimer(pingpongTimerProc, this, 0);
			Service::proxyDisconnected(this);

			return;
		}

		// если порвалась сеть то пытаемся сами переподключится

		raiseRestartEvent(h248::Token::Disconnected, h248::Reason::c907_Transmission_failure);
		m_pingTranID.id = 0;
	}
	//--------------------------------------
	// 
	//--------------------------------------
	bool MGC_Proxy::RequestSender_responseReceived(h248::TransactionID tid, const h248::Field* reply)
	{
		LOG_EVENT("mgc-prx", "cid:%u : response received", m_id);

		// ответ может быть только на регистрацию/разрегистрацию
		bool result = false;

		if (reply == nullptr)
		{
			// pending
			return true;
		}

		rtl::MutexWatchLock lock(m_serviceSync, __FUNCTION__);

		if (tid == m_requestTranID)
		{
			result = serviceChangeResponseReceived(tid, reply);
		}
		else if (tid == m_pingTranID)
		{
			m_pingTranID.id = 0;
			result = true;
		}
		else
		{
			// пришел ответ на событие от MGE
			for (int i = 0; i < reply->getFieldCount(); i++)
			{
				const h248::Field* action = reply->getFieldAt(i);

				if (action->getType() != h248::Token::Context)
				{
					return false;
				}

				const char* context_iod_str = action->getValue();

				//
			}
			// понг
			// нужно обновлять время жизни, правда непонятно зачем. TCP сам чует разрывы
			result = true;
		}

		return result;
	}
	//--------------------------------------
	// 
	//--------------------------------------
	void MGC_Proxy::RequestSender_errorReceived(h248::TransactionID tid, const h248::Field* error)
	{
		LOG_EVENT("mgc-prx", "cid:%u : error received : service action", m_id);

		raiseRestartEvent(h248::Token::Restart, h248::Reason::c907_Transmission_failure);
		m_pingTranID.id = 0;
	}
	//--------------------------------------
	// 
	//--------------------------------------
	void MGC_Proxy::RequestSender_responseTimeout(h248::TransactionID tid)
	{
		LOG_EVENT("mgc-prx", "cid:%u : response timeout", m_id);

		raiseRestartEvent(h248::Token::Restart, h248::Reason::c907_Transmission_failure);
		m_pingTranID.id = 0;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void MGC_Proxy::mge__notify_raised(h248::Field* notify)
	{
		h248::Field request(h248::Token::Transaction);

		request.addField(notify, true);

		LOG_CALL("mgc-prx", "cid:%u : send notify", m_id);

		h248::TransactionID tid;
		m_dispatcher.generateTranId(tid);
		m_dispatcher.sendRequest(&request, this, tid);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void MGC_Proxy::mge__send_reply(h248::Field* reply, h248::TransactionID tid)
	{
		LOG_CALL("mgc-prx", "cid:%u : send reply", m_id);
		m_dispatcher.sendReply(reply, tid);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void MGC_Proxy::mge__service_change_raised(h248::Field* service_changed)
	{
		h248::Field request(h248::Token::Transaction);

		request.addField(service_changed, true);

		LOG_CALL("mgc-prx", "cid:%u : send service change 2", m_id);
		h248::TransactionID tid;
		m_dispatcher.generateTranId(tid);
		m_dispatcher.sendRequest(&request, this, tid);
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool MGC_Proxy::serviceChangeResponseReceived(h248::TransactionID tid, const h248::Field* reply)
	{
		LOG_EVENT("mgc-prx", "cid:%u : registration response received", m_id);

		if (reply == nullptr)
		{
			LOG_CALL("mgc-prx", "cid:%u : registration pending received", m_id);
			return true;
		}

		const h248::Field* services = reply->findField("Context\\ServiceChange\\Services");

		if (services == nullptr)
		{
			// что-то пошло не так!
			LOG_CALL("mgc-prx", "cid:%u : registration failed : message has no Services ", m_id);
			m_dispatcher.sendError(tid, h248::ErrorCode::c472_Required_information_missing, nullptr);
			m_state = State_Failed;
			//Service::MGC_proxy_failed(this);
			raiseRestartEvent(h248::Token::Failover, h248::Reason::c907_Transmission_failure);
			return false;
		}

		h248::ServiceChangeDescriptor sc;

		if (!sc.read(*services) || sc.get_method() == h248::Token::_Error)
		{
			// кривой ответ!
			LOG_CALL("mgc-prx", "cid:%u : registration failed : message has invalid Services ", m_id);

			m_dispatcher.sendError(tid, h248::ErrorCode::c400_Syntax_error, nullptr);
			m_state = State_Failed;
			raiseRestartEvent(h248::Token::Failover, h248::Reason::c907_Transmission_failure);
			return false;
		}

		// начинаем разбор!
		if (sc.get_mid_type() == h248::Token::ServiceChangeAddress)
		{
			// заменяем адрес для доставки новых запросов на MGC
			LOG_CALL("mgc-prx", "cid:%u : registration : redirection to other port -- ignore", m_id);
		}
		else if (sc.get_mid_type() == h248::Token::MgcIdToTry)
		{
			// запуск нового проху на другой адрес
			Service::proxyRedirect(this, sc.get_method(), sc.get_reason_code(), sc.get_reasdon_text(), sc.get_mid());

			LOG_CALL("mgc-prx", "cid:%u : registration : redirection ", m_id);

			return true;
		}

		int new_version = sc.get_version();

		if (new_version >= 0)
		{
			if (new_version >= 1 && new_version <= 3) // new_version != m_mgcVersion
			{
				m_mgcVersion = new_version;
				m_dispatcher.setMgcVersion(m_mgcVersion);
			}
			else
			{
				// уведомление об ошибке!

				LOG_CALL("mgc-prx", "cid:%u : registration : version mismatch ", m_id);

				m_dispatcher.sendError(tid, h248::ErrorCode::c406_Version_not_supported, nullptr);
				m_state = State_Failed;
				//Service::MGC_proxy_failed(this);
				raiseRestartEvent(h248::Token::Failover, h248::Reason::c907_Transmission_failure);

				return false;
			}
		}

		m_state = State_Connected;



		rtl::Timer::setTimer(pingpongTimerProc, this, 0, Service::getPingPeriod(), true);

		LOG_EVENT("mgc-prx", "cid:%u : registration : done", m_id);

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void MGC_Proxy::serviceAction(const h248::Field* message, h248::Field* reply)
	{
		for (int i = 0; i < message->getFieldCount(); i++)
		{
			const h248::Field* command = message->getFieldAt(i);// (Token::AuditCapability);

			switch (command->getType())
			{
			case h248::Token::AuditCapability:
				processAuditCapability(command, reply);
				break;
			case h248::Token::ServiceChange:
				processServiceChange(command, reply);
				break;
			default:
				processDefault(command, reply);
			}
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void MGC_Proxy::processServiceChange(const h248::Field* service_change, h248::Field* reply)
	{
		if (service_change != nullptr)
		{
			const char* root = service_change->getValue();

			if (root != nullptr && std_stricmp(root, "root") == 0)
			{
				//...
				return;
			}
		}

		// отвечаем отказом
		reply->addErrorDescriptor(h248::ErrorCode::c501_Not_implemented);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void MGC_Proxy::processAuditCapability(const h248::Field* acaps, h248::Field* reply)
	{
		if (acaps != nullptr)
		{
			const char* root = acaps->getValue();

			if (root != nullptr && std_stricmp(root, "root") == 0)
			{
				reply->addField(h248::Token::AuditCapability, "root");
				return;
			}
		}

		// отвечаем отказом
		reply->addErrorDescriptor(h248::ErrorCode::c501_Not_implemented);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void MGC_Proxy::processDefault(const h248::Field* request, h248::Field* reply)
	{
		// ...
	}
	//--------------------------------------
	//
	//--------------------------------------
	void MGC_Proxy::pingpongTimerProc(rtl::Timer* sender, uint32_t elapsed, void* user_data, int tid)
	{
		h248::MegacoAsyncCall(asyncCallback, (void*)user_data, ASYNC_PINGPONG_EVENT, 0, nullptr);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void MGC_Proxy::connectTimer(rtl::Timer* sender, uint32_t elapsed, void* user_data, int tid)
	{
		h248::MegacoAsyncCall(asyncCallback, (void*)user_data, ASYNC_CONNECT_TIMEOUT_EVENT, 0, nullptr);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void MGC_Proxy::asyncCallback(void* userdata, uint16_t call_id, uintptr_t int_param, void* ptr_param)
	{
		MGC_Proxy* proxy = (MGC_Proxy*)userdata;

		if (proxy == nullptr)
			return;

		switch (call_id)
		{
		case ASYNC_RESTART_EVENT:
		{
			LOG_CALL("mgc-prx", "cid:%u : async restart event!", proxy->getId());
			uintptr_t iii = (uintptr_t)ptr_param;
			rtl::Timer::stopTimer(pingpongTimerProc, proxy, 0);

			proxy->asyncRestart((h248::Token)int_param, (h248::Reason)iii);
		}
		break;
		case ASYNC_PINGPONG_EVENT:
			proxy->sendPing();
			break;
		case ASYNC_CONNECT_TIMEOUT_EVENT:
			LOG_CALL("mgc-prx", "cid:%u : connecting timeout event!", proxy->getId());
			h248::TransportManager::removeRoute(proxy->m_route, false);
			break;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	const char* MGC_Proxy::State_toString(State state)
	{
		static const char* names[] = {
			"New",
			"Ready",
			"Connecting",
			"Connected",
			"Disconnecting",
			"Disconnected",
			"Terminated",
			"Failed",
		};

		return state >= State_New && state <= State_Failed ? names[state] : "proxy_state_unknown_e";
	}
}
//--------------------------------------
