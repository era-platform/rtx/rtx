﻿/* RTX H.248 Media Gate
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mge_termination.h"

//--------------------------------------
//
//--------------------------------------
#define DEFAULT_STREAM_ID 1

namespace mg3
{
	//--------------------------------------
	//
	//--------------------------------------
	Termination::Termination(h248::IMediaContext* context, h248::TerminationID& termId) :
		m_context(context),
		m_id(termId)
	{
		rtl::res_counter_t::add_ref(g_termination_counter);
	}
	//--------------------------------------
	//
	//--------------------------------------
	Termination::~Termination()
	{
		rtl::res_counter_t::release(g_termination_counter);
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Termination::initialize(h248::Field* reply)
	{
		if (m_id.getId() != MG_TERM_INVALID)
		{
			// already initialized!
			LOG_WARN("term", "id:%u : initialize : already initialized!", m_id.getId());
			return true;
		}

			uint32_t term = (uint32_t)m_context->mg_context_add_termination(m_id, reply);

		if (term == MG_TERM_ERR_CTX)
		{
			LOG_CALL("term", "id:%u : initialize : failed : MG Engine termination return MG_TERM_ERR_CTX (unlocked context)", m_id.getId());
		}
		else if (term == MG_TERM_ERR_FAILED)
		{
			LOG_CALL("term", "id:%u : initialize : failed : MG Engine termination return MG_TERM_ERR_FAILED (create fail)", m_id.getId());
		}
		else if (term == MG_TERM_ERR_RES)
		{
			LOG_CALL("term", "id:%u : initialize : failed : MG Engine termination return MG_TERM_ERR_RES (not enough resources)", m_id.getId());
		}
		else if (term == MG_TERM_INVALID)
		{
			LOG_CALL("term", "id:%u : initialize : failed : MG Engine termination return MG_TERM_INVALID (unknown reaction)", m_id.getId());
		}
		else
		{
			m_id.setId(term);
			LOG_CALL("term", "id:%u : initialize : done : binded MG Engine termination", m_id.getId());
		}

		return term != MG_TERM_INVALID;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Termination::destroy()
	{
		LOG_CALL("term", "id:%u : destroy...", m_id.getId());

		if (m_id.getId() != MG_TERM_INVALID)
		{
			m_context->mg_context_remove_termination(m_id.getId());
			m_id.setId(MG_TERM_INVALID);
		}

		LOG_CALL("term", "id:%u : destroy : done", m_id.getId());
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Termination::setup(const h248::Field* command, h248::Field* command_reply)
	{
		if (m_id.getId() == MG_TERM_INVALID)
		{
			LOG_CALL("term", "id:%u : setup : failed : has no binded MG Engine termination", m_id.getId());
			command_reply->addErrorDescriptor(h248::ErrorCode::c430_Unknown_TerminationID);
			return false;
		}

		bool result = true;

		int param_count = command->getFieldCount();

		for (int i = 0; i < param_count; i++)
		{
			const h248::Field* param = command->getFieldAt(i);

			switch (param->getType())
			{
			case h248::Token::Media:
			{
				h248::Field* media_reply = command_reply->addField(param->getType());
				result = set_media_descriptor(param, media_reply);
				if (media_reply->getFieldCount() == 0)
				{
					command_reply->removeField(media_reply);
				}
			}
			break;
			case h248::Token::Events:
				result = set_events_descriptor(param, command_reply);
				break;
			case h248::Token::EventBuffer:
				result = set_event_buffer_descriptor(param, command_reply);
				break;
			case h248::Token::Signals:
				result = set_signals_descriptor(param, command_reply);
				break;
			case h248::Token::DigitMap:
				result = set_digitmap_descriptor(param, command_reply);
				break;
			case h248::Token::Audit:
				result = set_audit_descriptor(param, command_reply);
				break;
			case h248::Token::Statistics:
				result = set_statistics_descriptor(param, command_reply);
				break;
			default:
				// Token::Mux, Token::Modem
				break;
			}

			if (!result)
			{
				break;
			}
		}

		LOG_CALL("term", "id:%u : setup : %s", m_id.getId(), result ? "done" : "failed");

		return result;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Termination::audit_value(const h248::Field* audit, h248::Field* response)
	{
		// cover
		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Termination::audit_capability(const h248::Field* audit, h248::Field* response)
	{
		// cover
		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Termination::set_media_descriptor(const h248::Field* descriptor, h248::Field* reply)
	{
		LOG_CALL("term", "id:%u : set media descriptor", m_id.getId());

		bool result = true;

		rtl::String local_sdp;
		rtl::String remote_sdp;

		int paramCount = descriptor->getFieldCount();

		for (int i = 0; i < paramCount; i++)
		{
			const h248::Field* param = descriptor->getFieldAt(i);

			switch (param->getType())
			{
			case h248::Token::TerminationState:
				result = set_termination_state_descriptor(param, reply);
				break;
			case h248::Token::Stream:
			{
				h248::Field* stream_reply = reply->addField(h248::Token::Stream, param->getValue());
				result = set_stream_descriptor(param, stream_reply);
				if (stream_reply->getFieldCount() == 0)
				{
					reply->removeField(stream_reply);
				}
			}
			break;
			case h248::Token::LocalControl:
				result = set_local_control_descriptor(0, param, reply);
				break;
			case h248::Token::Local:
				local_sdp = param->getRawData();
				break;
			case h248::Token::Remote:
				remote_sdp = param->getRawData();
				break;
			default:
				break;
			}

			if (!result)
			{
				break;
			}
		}

		if (!result)
		{
			LOG_CALL("term", "id:%u : set media descriptor : failed : MG Engine set params return error", m_id.getId());
			return false;
		}

		result = true;

		if ((!local_sdp.isEmpty()) || (!remote_sdp.isEmpty()))
		{
			result = false;
			if (m_context->mg_termination_set_Local_Remote(m_id.getId(), DEFAULT_STREAM_ID, remote_sdp, local_sdp, reply))
			{
				m_context->mg_termination_get_Local(m_id.getId(), DEFAULT_STREAM_ID, local_sdp, reply);
				if (!local_sdp.isEmpty())
				{
					h248::Field* answer = reply->addField(h248::Token::Local);
					answer->setRawData(local_sdp);
					result = true;
				}
			}
		}

		if (!result)
		{
			LOG_CALL("term", "id:%u : set Local descriptor : failed : MG Engine set_local return error", m_id.getId());
			reply->addErrorDescriptor(h248::ErrorCode::c441_Missing_remote_or_local_descriptor);
			return false;
		}

		LOG_CALL("term", "id:%u : set media descriptor : done", m_id.getId());

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Termination::set_events_descriptor(const h248::Field* descriptor, h248::Field* reply)
	{
		LOG_CALL("term", "id:%u : set Events descriptor", m_id.getId());

		int param_count = descriptor->getFieldCount();
		int reqId = strtoul(descriptor->getValue(), nullptr, 10);

		for (int i = 0; i < param_count; i++)
		{
			const h248::Field* evt = descriptor->getFieldAt(i);
			const char* evtName = evt->getName();

			LOG_CALL("term", "id:%u : set Events descriptor : %s", m_id.getId(), evtName);

			m_context->mg_termination_set_events(m_id.getId(), reqId, evt, reply);
		}

		LOG_CALL("term", "id:%u : set events descriptor : done", m_id.getId());

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Termination::set_event_buffer_descriptor(const h248::Field* descriptor, h248::Field* response)
	{
		return false;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Termination::set_signals_descriptor(const h248::Field* descriptor, h248::Field* response)
	{
		return false;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Termination::set_digitmap_descriptor(const h248::Field* descriptor, h248::Field* response)
	{
		return false;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Termination::set_audit_descriptor(const h248::Field* descriptor, h248::Field* response)
	{
		return false;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Termination::set_statistics_descriptor(const h248::Field* descriptor, h248::Field* response)
	{
		return false;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Termination::set_termination_state_descriptor(const h248::Field* ts_descriptor, h248::Field* reply)
	{
		h248::TerminationServiceState service_states = h248::TerminationServiceState::InService;
		bool event_buffer_control = false; // OFF

		for (int i = 0; i < ts_descriptor->getFieldCount(); i++)
		{
			const h248::Field* prop = ts_descriptor->getFieldAt(i);

			if (prop == nullptr)
			{
				reply->addErrorDescriptor(h248::ErrorCode::c449_Unsupported_or_unknown_parameter_or_property_value);
				return false;
			}

			const char* value = nullptr;

			switch (prop->getType())
			{
			case h248::Token::ServiceStates:
				//--------------
				// ServiceStates
				value = prop->getValue();

				if (value != nullptr)
				{
					service_states = parse__mg_termination_service_state(value);
					if (!m_context->mg_termination_set_TerminationState(m_id.getId(), service_states, reply))
					{
						// called function sets error code 
						return false;
					}
				}
				break;
			case h248::Token::Buffer:
				//-------------------
				// EventBufferControl

				if (value = prop->getValue())
				{
					if (std_stricmp(value, "LockStep") == 0) // true - LockStep
						m_context->mg_termination_set_TerminationState_EventControl(m_id.getId(), true, reply);
					else if (std_stricmp(value, "OFF") == 0) // false - OFF
						m_context->mg_termination_set_TerminationState_EventControl(m_id.getId(), false, reply);
					else
					{
						reply->addErrorDescriptor(h248::ErrorCode::c446_Unsupported_or_unknown_parameter, "TerminationState::EventControl has invalid value");
						return false;
					}
				}
				else
				{
					reply->addErrorDescriptor(h248::ErrorCode::c449_Unsupported_or_unknown_parameter_or_property_value, "TerminationState EventControl has invalid value");
					return false;
				}
				break;
			case h248::Token::Mode:
				//-----------
				// properties
				if (!m_context->mg_termination_set_TerminationState_Property(m_id.getId(), prop, reply))
				{
					return false;
				}
				break;
			case h248::Token::_Custom:
				//-----------
				// properties
				if (!m_context->mg_termination_set_TerminationState_Property(m_id.getId(), prop, reply))
				{
					return false;
				}

				break;
			default:
				// unknow properties must be ignored 
				break;
			}
		}

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Termination::set_stream_descriptor(const h248::Field* descriptor, h248::Field* reply)
	{
		const char* stream_id_ptr = descriptor->getValue();

		rtl::String local_sdp;
		rtl::String remote_sdp;

		if (stream_id_ptr == nullptr || stream_id_ptr[0] == 0)
		{
			// TODO: set normal error code
			LOG_CALL("term", "id:%u : set stream descriptor : failed : invalid StreamID", m_id.getId());
			reply->addErrorDescriptor(h248::ErrorCode::c449_Unsupported_or_unknown_parameter_or_property_value, "Invalid Stream ID");
			return false;
		}

		uint16_t stream_id = (uint16_t)strtoul(stream_id_ptr, nullptr, 10);

		LOG_CALL("term", "id:%u : set stream descriptor : ID:%u", m_id.getId(), stream_id);

		int param_count = descriptor->getFieldCount();

		for (int i = 0; i < param_count; i++)
		{
			const h248::Field* param = descriptor->getFieldAt(i);

			switch (param->getType())
			{
			case h248::Token::LocalControl:
				if (!set_local_control_descriptor(stream_id, param, reply))
					return false;
			case h248::Token::Local:
				local_sdp = param->getRawData();
				break;
			case h248::Token::Remote:
				remote_sdp = param->getRawData();
				break;
			default:
				break;
			}
		}

		if ((!local_sdp.isEmpty()) || (!remote_sdp.isEmpty()))
		{
			if (m_context->mg_termination_set_Local_Remote(m_id.getId(), stream_id, remote_sdp, local_sdp, reply))
			{
				if (m_context->mg_termination_get_Local(m_id.getId(), stream_id, local_sdp, reply))
				{
					if (!local_sdp.isEmpty())
					{
						// All OK
						h248::Field* answer = reply->addField(h248::Token::Local);
						answer->setRawData(local_sdp);
						LOG_CALL("term", "id:%u : set stream descriptor : done", m_id.getId());
						return true;
					}
				}
			}
		}

		LOG_CALL("term", "id:%u : set Local descriptor : failed : MG Engine set_local return error", m_id.getId());

		return false;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Termination::set_local_control_descriptor(uint16_t stream_id, const h248::Field* descriptor, h248::Field* reply)
	{
		bool result = false;

		for (int i = 0; i < descriptor->getFieldCount(); i++)
		{
			result = false;
			const h248::Field* prop = descriptor->getFieldAt(i);

			if (prop == nullptr) // ???
			{
				reply->addErrorDescriptor(h248::ErrorCode::c449_Unsupported_or_unknown_parameter_or_property_value);
				return false;
			}

			switch (prop->getType())
			{
			case h248::Token::Mode:
			{
				h248::TerminationStreamMode stream_mode = parse__mg_termination_stream_mode(prop->getValue());

				if (stream_mode == h248::TerminationStreamMode::Error)
				{
					LOG_CALL("term", "id:%u : set LocalControl descriptor : failed : invalid Mode value '%s'", m_id.getId(), (const char*)prop->getValue());
					reply->addErrorDescriptor(h248::ErrorCode::c449_Unsupported_or_unknown_parameter_or_property_value, "LocalControl Mode has invalid value");
					result = false;
				}
				else
				{
					result = m_context->mg_termination_set_LocalControl_Mode(m_id.getId(), stream_id, stream_mode, reply);
				}
				break;
			}
			case h248::Token::ReservedGroup:
			{
				const char* value = prop->getValue();

				if (value == nullptr)
				{
					// parser error!
					LOG_CALL("term", "id:%u : set LocalControl descriptor : failed : empty ReserveGroup value", m_id.getId());
					reply->addErrorDescriptor(h248::ErrorCode::c449_Unsupported_or_unknown_parameter_or_property_value, "LocalControl ReserveGroup has invalid value");
					result = false;
				}
				else
				{
					if (std_stricmp(value, "true") == 0)
					{
						result = m_context->mg_termination_set_LocalControl_ReserveGroup(m_id.getId(), stream_id, true, reply);
					}
					else if (std_stricmp(value, "false") == 0)
					{
						result = m_context->mg_termination_set_LocalControl_ReserveGroup(m_id.getId(), stream_id, false, reply);
					}
					else
					{
						// parser error!
						LOG_CALL("term", "id:%u : set LocalControl descriptor : failed : invalid ReserveGroup value '%s'", m_id.getId(), (const char*)prop->getValue());
						reply->addErrorDescriptor(h248::ErrorCode::c449_Unsupported_or_unknown_parameter_or_property_value, "LocalControl ReserveGroup has invalid value");
						result = false;
					}
				}
				break;
			}
			case h248::Token::ReservedValue:
			{
				const char* value = prop->getValue();

				if (value == nullptr)
				{
					// parser error!
					LOG_CALL("term", "id:%u : set LocalControl descriptor : failed : empty ReserveValue value", m_id.getId());
					reply->addErrorDescriptor(h248::ErrorCode::c449_Unsupported_or_unknown_parameter_or_property_value, "LocalControl ReserveValue has invalid value");
					result = false;
				}
				else
				{
					if (std_stricmp(value, "true") == 0)
					{
						result = m_context->mg_termination_set_LocalControl_ReserveValue(m_id.getId(), stream_id, true, reply);
					}
					else if (std_stricmp(value, "false") == 0)
					{
						result = m_context->mg_termination_set_LocalControl_ReserveValue(m_id.getId(), stream_id, false, reply);
					}
					else
					{
						// parser error!
						LOG_CALL("term", "id:%u : set LocalControl descriptor : failed : invalid ReserveValue value '%s'", m_id.getId(), (const char*)prop->getValue());
						reply->addErrorDescriptor(h248::ErrorCode::c449_Unsupported_or_unknown_parameter_or_property_value, "LocalControl ReserveValue has invalid value");
						result = false;
					}
				}
				break;
			}
			default:
				result = m_context->mg_termination_set_LocalControl_Property(m_id.getId(), stream_id, prop, reply);
				break;
			}
		}

		LOG_CALL("term", "id:%u : set LocalControl descriptor : done", m_id.getId());

		return result;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Termination::set_local_sdp_descriptor(uint16_t stream_id, const h248::Field* descriptor, h248::Field* reply)
	{
		const char* sdp_offer = descriptor->getRawData();
		rtl::String sdp_answer;

		if (m_context->mg_termination_set_Local(m_id.getId(), stream_id, sdp_offer, sdp_answer, reply))
		{
			if (!sdp_answer.isEmpty())
			{
				h248::Field* answer = reply->addField(h248::Token::Local);
				answer->setRawData(sdp_answer);

				LOG_CALL("term", "id:%u : set Local descriptor : done", m_id.getId());

				return true;
			}

			// unbelievable state!
			reply->addErrorDescriptor(h248::ErrorCode::c500_Internal_software_failure_in_the_MG_or_MGC);
		}

		LOG_CALL("term", "id:%u : set Local descriptor : failed : MG Engine set_local return error", m_id.getId());
		
		return false;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Termination::set_remote_sdp_descriptor(uint16_t stream_id, const h248::Field* descriptor, h248::Field* reply)
	{
		const char* remote_sdp = descriptor->getRawData();

		m_context->mg_termination_set_Remote(m_id.getId(), stream_id, remote_sdp, reply);

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	h248::TerminationServiceState parse__mg_termination_service_state(const char* name)
	{
		h248::Token token = h248::Token_parse(name);
		if (token == h248::Token::_Custom)
			token = h248::Token::_Error;

		return (h248::TerminationServiceState)token;
	}
	//--------------------------------------
	//
	//--------------------------------------
	h248::TerminationStreamMode parse__mg_termination_stream_mode(const char* name)
	{
		h248::Token token = h248::Token_parse(name);
		if (token == h248::Token::_Custom)
			token = h248::Token::_Error;

		return (h248::TerminationStreamMode)token;
	}
}
//--------------------------------------
