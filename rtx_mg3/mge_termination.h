﻿/* RTX H.248 Media Gate
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_interface.h"

namespace mg3
{

	//--------------------------------------
	//
	//--------------------------------------
	h248::TerminationServiceState parse__mg_termination_service_state(const char*);
	h248::TerminationStreamMode parse__mg_termination_stream_mode(const char*);
	//--------------------------------------
	//
	//--------------------------------------
	class Context;
	//--------------------------------------
	//
	//--------------------------------------
	class Termination
	{
		h248::IMediaContext* m_context;
		h248::TerminationID m_id;

	public:
		Termination(h248::IMediaContext* context, h248::TerminationID& term_id);
		~Termination();

		const h248::TerminationID& getTermId() const { return m_id; }
		uint32_t getId() const { return m_id.getId(); }

		bool initialize(h248::Field* response);
		void destroy();

		bool setup(const h248::Field* command, h248::Field* response);

		bool audit_value(const h248::Field* audit, h248::Field* response);
		bool audit_capability(const h248::Field* audit, h248::Field* response);

	private:
		// termination 
		bool set_media_descriptor(const h248::Field* descriptor, h248::Field* response);
		bool set_events_descriptor(const h248::Field* descriptor, h248::Field* response);
		bool set_event_buffer_descriptor(const h248::Field* descriptor, h248::Field* response);
		bool set_signals_descriptor(const h248::Field* descriptor, h248::Field* response);
		bool set_digitmap_descriptor(const h248::Field* descriptor, h248::Field* response);
		bool set_audit_descriptor(const h248::Field* descriptor, h248::Field* response);
		bool set_statistics_descriptor(const h248::Field* descriptor, h248::Field* response);

		// media 
		bool set_termination_state_descriptor(const h248::Field* descriptor, h248::Field* reply);
		bool set_stream_descriptor(const h248::Field* descriptor, h248::Field* reply);
		// if stream setup made without stream token stram_id will be 0
		bool set_local_control_descriptor(uint16_t stream_id, const h248::Field* descriptor, h248::Field* reply);
		bool set_local_sdp_descriptor(uint16_t stream_id, const h248::Field* descriptor, h248::Field* reply);
		bool set_remote_sdp_descriptor(uint16_t stream_id, const h248::Field* descriptor, h248::Field* reply);
	};
}
//--------------------------------------
