﻿/* RTX H.248 Media Gate
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_service.h"
#include "mmt_file_wave.h"
//--------------------------------------
//
//--------------------------------------
int get_command(const char* text);
void process_start_command(char* params);
void process_stop_command(char* params);
void process_restart_command(char* params);
void process_set_command(char* params);
void process_get_command(char* params);
void process_print_command(char* params);
void process_save_command(char* params);
void process_help_command(char* params);
//--------------------------------------
//
//--------------------------------------
void run_command_processor()
{
	char buffer[1024];
	int command;

	do
	{

		memset(buffer, 0, sizeof(buffer));
		printf("=> ");
		const char* ptr = fgets(buffer, 1024, stdin);

		if (ptr == nullptr)
			break;

		rtl::strtrim(buffer, " \n\r\n");

		if (buffer[0] != 0)
		{
			char* params = NULL;
			char* sep = strchr(buffer, ' ');

			if (sep != NULL)
			{
				params = sep + 1;
				*sep = 0;
			}

			command = get_command(buffer);

			switch (command)
			{
			case MG3_SHELL_CMD_EXIT:
				break;
			case MG3_SHELL_CMD_START:
				process_start_command(params);
				break;
			case MG3_SHELL_CMD_STOP:
				process_stop_command(params);
				break;
			case MG3_SHELL_CMD_RESTART:
				process_restart_command(params);
				break;
			case MG3_SHELL_CMD_SET:
				process_set_command(params);
				break;
			case MG3_SHELL_CMD_GET:
				process_get_command(params);
				break;
			case MG3_SHELL_CMD_PRINT:
				process_print_command(params);
				break;
			case MG3_SHELL_CMD_SAVE:
				process_save_command(params);
				break;
			case MG3_SHELL_CMD_HELP:
				process_help_command(params);
				break;
			case MG3_SHELL_CMD_ERROR:
				break;
			}
		}
	}
	while (command != MG3_SHELL_CMD_EXIT);
}
//--------------------------------------
//
//--------------------------------------
int get_command(const char* text)
{
	int command = MG3_SHELL_CMD_ERROR;

	if (strcmp(text, "exit") == 0)
	{
		command = MG3_SHELL_CMD_EXIT;
	}
	else if (strcmp(text, "start") == 0)
	{
		command = MG3_SHELL_CMD_START;
	}
	else if (strcmp(text, "stop") == 0)
	{
		command = MG3_SHELL_CMD_STOP;
	}
	else if (strcmp(text, "restart") == 0)
	{
		command = MG3_SHELL_CMD_RESTART;
	}
	else if (strcmp(text, "set") == 0)
	{
		command = MG3_SHELL_CMD_SET;
	}
	else if (strcmp(text, "get") == 0)
	{
		command = MG3_SHELL_CMD_GET;
	}
	else if (strcmp(text, "print") == 0)
	{
		command = MG3_SHELL_CMD_PRINT;
	}
	else if (strcmp(text, "save") == 0)
	{
		command = MG3_SHELL_CMD_SAVE;
	}
	else if (strcmp(text, "test") == 0)
	{
		command = MG3_SHELL_CMD_TEST;
	}
	else if (strcmp(text, "help") == 0)
	{
		command = MG3_SHELL_CMD_HELP;
	}

	return command;
}
//--------------------------------------
//
//--------------------------------------
void process_start_command(char* params)
{
	printf("\nstarting '%s'...\n", params);

	if (mg3::Service::start())
	{
		printf("started\n");

		mg3::Service::run();
	}
	else
	{
		printf("not started\n");
	}
}
//--------------------------------------
//
//--------------------------------------
void process_stop_command(char* params)
{
	printf("stopping '%s'...\n", params);

	mg3::Service::stop();

	printf("stopped\n");
}
//--------------------------------------
//
//--------------------------------------
void process_restart_command(char* params)
{
	printf("restarting '%s'...\n", params);

	mg3::Service::restart();

	printf("restarted\n");

}
//--------------------------------------
//
//--------------------------------------
void process_set_command(char* params)
{
	printf("set '%s'...\n", params);

	char* key = params;
	char* value = strchr(params, ' ');
	if (value != nullptr)
	{
		*value++ = 0;
	}

	Config.set_config_value(key, value);
	
	printf("value set\n");
}
//--------------------------------------
//
//--------------------------------------
void process_get_command(char* params)
{
	printf("get '%s'...\n", params);

	const char* value = Config.get_config_value(params);
	
	printf("value of '%s' is '%s'\n", params, value);
}
//--------------------------------------
//
//--------------------------------------
void process_print_command(char* params)
{
	// печать всех значений в конфиге
	Config.print(stdout);
}
//--------------------------------------
//
//--------------------------------------
void process_save_command(char* params)
{
}
//--------------------------------------
//
//--------------------------------------
void process_help_command(char* params)
{
}
void wave_file_test()
{
	media::FileReaderWAV in;
	media::FileWriterWAV out(&Log);
	
	const char* wav_in = nullptr;
	const char* wav_out = nullptr;

	if (!in.open(wav_in))
		return;

	if (!out.create(wav_out, in.getFormat()))
	{
		in.close();
		return;
	}

	int read = 0;
	uint8_t buffer[64 * 1024];

	while ((read = in.readAudioData(buffer, 64 * 1024)) > 0)
	{
		out.write(buffer, read);
	}

	in.close();
	out.close();

}
//--------------------------------------
