﻿/* RTX H.248 Media Gate
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "std_module.h"
#include "mg_interface.h"
#include "mge_virtual_gate.h"

namespace mg3
{

	//--------------------------------------
	// представитель физического медиа шлюза
	//--------------------------------------
	class Gate
	{
		class mg_module_callback_interface_imp_t : public h248::IMediaGateEventHandler
		{
		public:
			virtual void mge__event_raised();
			virtual void mge__error_raised();
		};

		static mg_module_callback_interface_imp_t m_eventHandler;

		static rtl::MutexWatch m_sync;
		static rtl::ArrayT<VirtualGate*> m_gate_list;

		static rtl::Module m_module;
		static h248::IMediaGate* m_mg_module;

	public:
		static bool create(const char* modulepath, const char* logpath);
		static void destroy();

		static VirtualGate* create_virtual_gate(IMG3_EventHandler* handler);
		static void destroy_virtual_gate(VirtualGate*);

		// internal interface
		// для вызовы функций физического медиа шлюза
		static void set_interface_prefix(const char* ip_address, const char* prefix);
		static void remove_interface_prefix(const char* ip_address);

		static bool add_rtp_interface(const char* iface, uint16_t port_begin, uint16_t port_count);
		static void free_rtp_interface(const char* iface);

		static h248::IMediaContext* create_context();
		static void destroy_context(h248::IMediaContext* context);
	};
}
//--------------------------------------
