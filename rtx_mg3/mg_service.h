﻿/* RTX H.248 Media Gate
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_transport_manager.h"
#include "mg_transaction_manager.h"
#include "mge_manager.h"
#include "mg_controller.h"

namespace mg3
{
	//--------------------------------------
	// получатель событий из сети
	//--------------------------------------
	class ServiceDispatcher : public h248::ITransportUser
	{
	public:
		virtual void TransportUser_disconnected(const h248::Route& sender);
		virtual void TransportUser_packetArrival(const h248::Route& route, h248::Message* message);
		virtual void TransportUser_netFailure(const h248::Route& route, int netError);
		virtual void TransportUser_parserError(const h248::Route& route);
	};
	//--------------------------------------
	// Главный диспечер событий транспорта и контоллера
	// транспорт работает сам по себе и события поступают от него через интерфейс событий
	// контроллеры вызывают диспетчер напрямую
	//--------------------------------------
	class Service
	{
		static bool m_started;										// признак старта сервиса
		static rtl::MutexWatch m_MGC_routeSync;						// синхронизация для доступа к списку адресов
		static rtl::ArrayT<h248::EndPoint> m_MGC_routeList;				// список адресов MGC
		static MGC_Proxy* m_MGC_mainProxy;							// указатель на прокси

		static ServiceDispatcher m_transportDispatcher;				// обработчик сообщений сети
		static rtl::Event m_raiseEvent;									// событие останова сервиса -- для функции main()

		// configuration parameters
		static int m_connectionTimeout;
		static int m_connectionFailTimeout;
		static int m_pingReplyTimeout;
		static int m_pingInterval;

	private:
		static bool startMgcProxy();
		static void stopMgcProxy();

		static bool isKnownMgc(const h248::EndPoint& mgcAddress);
		static bool isActiveMgc(const h248::EndPoint& mgcAddress);

		static void initializeOutputMode();
		static void intializeLifespanTimes();

		static bool loadMgcList();
		static bool initializeMediaEngineModule();

		static bool initializeTransportManager();
		static bool initializeParameters();

		static void asyncCallHandler(void* userData, uint16_t callId, uintptr_t intParam, void* ptrParam);

		static void asyncProxyStarted(MGC_Proxy* mgcProxy);
		static void asyncProxyStopped(MGC_Proxy* mgcProxy, h248::Token method, h248::Reason code, const char* reason);
		static void asyncProxyRedirect(MGC_Proxy* mgcProxy, h248::Token method, h248::Reason code, const char* reason, const h248::MID& mid);
		static void asyncProxyDisconnected(MGC_Proxy* mgcProxy);
		static void asyncProxyFailed(MGC_Proxy* mgcProxy);

	public:
		// управление физическим шлюзом
		static bool start();
		static bool stop();
		static bool restart();
		static bool run();

		static bool isStarted() { return m_started; }

		// настройки физического медиа шлюза
		static bool addPortRangeRTP(const char* iface, uint16_t range_start, uint16_t range_size);
		static bool removePortRangeRTP(const char* iface);

		// события от контроллера
		//static void MGC_proxy_started(MGC_Proxy* mgcProxy);
		static void proxyStopped(MGC_Proxy* mgcProxy, h248::Token method, h248::Reason code, const char* reason);
		static void proxyRedirect(MGC_Proxy* mgcProxy, h248::Token method, h248::Reason code, const char* reason, const h248::MID& mid);
		static void proxyDisconnected(MGC_Proxy* mgcProxy);
		static void proxyFailed(MGC_Proxy* mgcProxy);

		// события от менеджера сети
		static void TransportUser_disconnected(const h248::Route& route);
		static void TransportUser_packetArrival(const h248::Route& route, h248::Message* message);
		static void TransportUser_netFailure(const h248::Route& route, int netError);
		static void TransportUser_parserError(const h248::Route& route);

		inline static int getConnectTimeout() { return m_connectionTimeout; }
		inline static int getConnectFailTimeout() { return m_connectionFailTimeout; }
		inline static int getReplyTimeout() { return m_pingReplyTimeout; }
		inline static int getPingPeriod() { return m_pingInterval; }
	};
}
//--------------------------------------
