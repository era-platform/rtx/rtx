﻿/* RTX H.248 Media Gate
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

//--------------------------------------
// configuration files
//--------------------------------------
#include "std_basedef.h"
#include "std_typedef.h"
#include "std_funcdef.h"

#if defined (TARGET_OS_WINDOWS)
//--------------------------------------
// ��� ������������� � VS2005, VS2008, VS2010
//--------------------------------------
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#define _CRT_NON_CONFORMING_SWPRINTFS
//#define _USE_32BIT_TIME_T
#define __STDC_LIMIT_MACROS
#define _CRT_RAND_S
#pragma setlocale(".ACP")
#endif
//--------------------------------------
//
//--------------------------------------
#include "std_mem_checker.h"
//--------------------------------------
//
//--------------------------------------
#if defined (DEBUG_MEMORY)
#pragma warning(disable : 4291)

void* operator new(size_t size, char* file, int line);
void* operator new[](size_t size, char* file, int line);
void operator delete (void* mem_ptr);
void operator delete[](void* mem_ptr);

#define NEW new (__FILE__, __LINE__)
#define DELETEO(ptr) delete (ptr)
#define DELETEAR(ptr) delete[] (ptr)
#define MALLOC(size) std_mem_checker_malloc(size, __FILE__, __LINE__, rc_malloc_malloc_e)
#define FREE(ptr) std_mem_checker_free(ptr, rc_malloc_free_e)
#define REALLOC(ptr, size) std_mem_checker_realloc(ptr, size, __FILE__, __LINE__)
#else
#define NEW new
#define DELETEO(ptr) delete ptr
#define DELETEAR(ptr) delete[] ptr
#define MALLOC(size) malloc(size)
#define FREE(ptr) free(ptr)
#define REALLOC(ptr, size) realloc(ptr, size)
#endif
//--------------------------------------
// Standard Header Files:
//--------------------------------------
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <assert.h>
#include <inttypes.h>
#include <limits.h>
#include <string.h>
#include <locale.h>

#if defined (TARGET_OS_WINDOWS)
//--------------------------------------
// Exclude rarely-used stuff from Windows headers
//--------------------------------------
#define WIN32_LEAN_AND_MEAN

#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0600
#endif

#ifndef _WIN32_IE
#define _WIN32_IE 0x0600
#endif

#include <process.h>
//--------------------------------------
// Windows Header Files:
//--------------------------------------
#include <winsock2.h>
#include <ws2tcpip.h>
#include <ws2ipdef.h>
#include <iphlpapi.h>
#include <windows.h>

#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)

#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#if defined(TARGET_OS_LINUX)
#include <sys/epoll.h>
#elif defined(TARGET_OS_FREEBSD)
#include <sys/event.h>
#endif
#include <ctype.h>
#include <signal.h>
#endif
//--------------------------------------
//
//--------------------------------------
#include "rtx_stdlib.h"
#include "rtx_netlib.h"
#include "rtx_megaco.h"
#include "rtx_media_tools.h"
//--------------------------------------
//
//--------------------------------------
#define MG3_SHELL_CMD_EXIT 		0
#define MG3_SHELL_CMD_START 	1
#define MG3_SHELL_CMD_STOP 		2
#define MG3_SHELL_CMD_RESTART 	3
#define MG3_SHELL_CMD_SET 		4
#define MG3_SHELL_CMD_GET 		5
//#define MG3_SHELL_CMD_ADD 		6
#define MG3_SHELL_CMD_PRINT 	7
#define MG3_SHELL_CMD_SAVE 		8
#define MG3_SHELL_CMD_TEST 		9
#define MG3_SHELL_CMD_HELP 		10
#define MG3_SHELL_CMD_ERROR 	-1
//--------------------------------------
//
//--------------------------------------
extern char g_log_path_buffer[MAX_PATH];
//--------------------------------------
//
//--------------------------------------
void run_command_processor();
//--------------------------------------
// ключи конфигурации относящиеся к MEGACO
//--------------------------------------
#define CONF_MGC_LIST						"megaco-control-list"

#define CONF_MG_MEDIA_GATE_LIB				"megaco-media-gate-lib"
#define CONF_MG_TERM_PREFIX_COUNT			"mg-term-prefix-count"
#define CONF_MG_TERM_PREFIX_MASK			"mg-term-prefix-%u"
#define CONF_MG_TERM_RTP_RANGE_COUNT		"mg-term-rtp-range-count"
#define CONF_MG_TERM_RTP_RANGE_MASK			"mg-term-rtp-range-%u"
#define CONF_MG_SERV_MID_TYPE				"megaco-mid-type"
#define CONF_MG_SERV_MID_DOMAIN				"megaco-mid-domain"
#define CONF_MG_LAZY_RECORD_TIME			"mg-lazy-record-time"

#define CONF_MG_CONNECTION_TIMEOUT			"mg-connection-timeout"
#define CONF_MG_CONNECTING_FAULT_TIMEOUT	"mg-connecting-fault-timeout"
#define CONF_MG_PING_REPLY_TIMEOUT			"mg-ping-reply-timeout"
#define CONF_MG_PING_INTERVAL				"mg-ping-interval"

//--------------------------------------
// счетчики для объектов
//--------------------------------------
extern int g_context_counter;
extern int g_termination_counter;
//--------------------------------------



