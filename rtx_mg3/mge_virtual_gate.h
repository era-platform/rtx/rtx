﻿/* RTX H.248 Media Gate
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_interface.h"

namespace mg3
{

	//--------------------------------------
	//
	//--------------------------------------
	class Gate;
	class Context;
	//--------------------------------------
	//
	//--------------------------------------
	struct IMG3_EventHandler
	{
		virtual void mge__notify_raised(h248::Field* notify) = 0;
		virtual void mge__service_change_raised(h248::Field* service_changed) = 0;
		virtual void mge__send_reply(h248::Field* reply, h248::TransactionID tid) = 0;
	};
	//--------------------------------------
	// виртуальный медиа шлюз (связан с определенным контроллером)
	//--------------------------------------
	class VirtualGate : public h248::ITerminationEventHandler
	{
		// хранилище контекстов
		rtl::MutexWatch m_sync;
		volatile bool m_started;
		typedef Context* ContextPtr;
		rtl::SortedArrayT<ContextPtr, uint32_t> m_ctxList;
		// бек интерфейс к контроллеру
		IMG3_EventHandler* m_eventHandler;

		char m_vid[16];
		volatile static uint32_t s_idGen;

	public:
		VirtualGate(IMG3_EventHandler* handler);
		~VirtualGate();

		bool start();
		void stop();

		void doAction(const h248::Field* actionRequest, h248::TransactionID tranId);
		void raiseNotifyEvent(h248::Field* notify);
		void raiseServiceChangeEvent(h248::Field* serviceChange);

	private:
		static int compareObj(const ContextPtr& l, const ContextPtr& r);
		static int compareKey(const uint32_t& l, const ContextPtr& r);

		virtual void TerminationEvent_callback(h248::EventParams* eventData) override;

		Context* createContext();
		Context* findContext(uint32_t ctxId);

		static void doActionCallback(void* userData, uint16_t callId, uintptr_t intParam, void* ptrParam);

		void doActionAsync(const h248::Field* actionRequest, h248::Field* actionReply);
		bool doActionForAll(const h248::Field* actionRequest, h248::Field* actionReply);
		void removeContext(Context* context);
		void checkContextLifetime();

		static void timerProc(rtl::Timer* sender, uint32_t elapsed, void* userData, int timerId);
		static void asyncCallback(void* userData, uint16_t callId, uintptr_t intParam, void* ptrParam);
	};
}
//--------------------------------------
