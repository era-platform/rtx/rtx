﻿/* RTX H.248 Media Gate
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mge_context.h"
#include "mge_manager.h"

namespace mg3
{

	//--------------------------------------
	//
	//--------------------------------------
#define MGE_CONTEXT_HOUR (1000 * 60 * 60) // 1 час в миллисекундах
#define MGE_CONTEXT_DEFAULT_LIFESPAN (MGE_CONTEXT_HOUR * 10) // по умолчанию
	int Context::s_lifespan = MGE_CONTEXT_DEFAULT_LIFESPAN;
	//--------------------------------------
	//
	//--------------------------------------
	//volatile uint32_t Context::m_termIdGen = 0;
	//--------------------------------------
	//
	//--------------------------------------
	Context::Context(VirtualGate& owner, h248::IMediaContext* mg_context) :
		m_accessCaptured(false),
		m_refCount(1),
		m_contextID(MG_CONTEXT_NULL),
		m_readyToDelete(false),
		m_newContext(true),
		m_timestamp(rtl::DateTime::getTicks()),
		m_owner(owner),
		m_mediaContext(mg_context),
		m_accessSync("mg-ctx")
	{
		rtl::res_counter_t::add_ref(g_context_counter);

		if (mg_context != nullptr)
		{
			m_contextID = mg_context->mg_context_get_id();
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	Context::~Context()
	{
		LOG_CALL("ctx", "id:%u : destructor", m_contextID);

		rtl::res_counter_t::release(g_context_counter);
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Context::initialize(h248::Field* reply)
	{
		return m_mediaContext->mg_context_initialize(&m_owner, reply);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Context::destroy()
	{
		// по идее уже должен быть взведен!
		LOG_CALL("ctx", "id:%u : destroy : term count %d...", m_contextID, m_termimationList.getCount());

		// в момент вызова больше не должно приходить вызовов
		// что нужно:

		// 1. проверить лист ожидания и освободить его 
		{
			rtl::MutexWatchLock lock(m_accessSync, __FUNCTION__);

			// оповестим всех
			for (int i = 0; i < m_waitList.getCount(); i++)
			{
				m_waitList[i]->signal();
			}

			m_waitList.clear();
		}

		LOG_CALL("ctx", "id:%u : destroy : wait entered threads", m_contextID);

		// ждем сброса флага m_accessCaptured
		while (m_accessCaptured && m_waitList.getCount() > 0)
		{
			rtl::Thread::sleep(0);
		}

		// все потоки ушли

		LOG_CALL("ctx", "id:%u : destroy : all threads left", m_contextID);

		// 2. удалить все терминейшн оболочки если они еще остались!

		LOG_CALL("ctx", "id:%u : destroy : terms count:%d", m_contextID, m_termimationList.getCount());

		int term_count = m_termimationList.getCount();

		if (term_count > 0)
		{
			m_mediaContext->mg_context_lock();

			for (int i = 0; i < term_count; i++)
			{
				Termination* term = m_termimationList[i];

				LOG_CALL("ctx", "id:%u : destroy : term-id %d", m_contextID, term->getTermId().getId());

				term->destroy();

				DELETEO(term);
			}
			m_termimationList.clear();

			m_mediaContext->mg_context_unlock();
		}

		LOG_CALL("ctx", "id:%u : destroy : unbind MG Engine context %p", m_contextID, m_mediaContext);

		Gate::destroy_context(m_mediaContext);

		m_mediaContext = nullptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Context::checkLifetime(uint32_t current_time)
	{
		int diff = current_time - m_timestamp;
		return diff < s_lifespan;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Context::doCommands(const h248::Field* action_request, h248::Field* action_reply)
	{
		LOG_CALL("ctx", "id:%u : do command : wait access", m_contextID);

		waitAccess();

		LOG_CALL("ctx", "id:%u : do command : access granted", m_contextID);

		if (m_readyToDelete)
		{
			LOG_CALL("ctx", "id:%u : do command : context removed!", m_contextID);
			releaseAccess();
			action_reply->addErrorDescriptor(h248::ErrorCode::c411_The_transaction_refers_to_an_unknown_ContextID);
			return false;
		}

		m_timestamp = rtl::DateTime::getTicks();

		LOG_CALL("ctx", "id:%u : do command : Lock MG Engine context", m_contextID);

		m_mediaContext->mg_context_lock();

		LOG_CALL("ctx", "id:%u : do command : MG Engine context locked", m_contextID);

		int action_count = action_request->getFieldCount();
		bool result = true; // пустая команда
		//h248::Field* errorDescriptor = nullptr;

		for (int i = 0; i < action_count; i++)
		{
			// команды
			const h248::Field* command = action_request->getFieldAt(i);

			h248::Token cmd_type = command->getType();

			LOG_CALL("ctx", "id:%u : do command %s...", m_contextID, Token_toStringLong(cmd_type));

			switch (cmd_type)
			{
			case h248::Token::Add:
				result = cmdAdd(command, action_reply);
				break;
			case h248::Token::Modify:
				result = cmdModify(command, action_reply);
				break;
			case h248::Token::Subtract:
				result = cmdSubtract(command, action_reply);
				break;
			case h248::Token::Move:
				result = cmdMove(command, action_reply);
				break;
			case h248::Token::AuditValue:
				result = cmdAuditValue(command, action_reply);
				break;
			case h248::Token::AuditCapability:
				result = cmdAuditCapability(command, action_reply);
				break;
			case h248::Token::Topology:
				result = propTopology(command, action_reply);
				break;
			case h248::Token::Priority:
				result = propPriority(command, action_reply);
				break;
			case h248::Token::Emergency:
				result = propEmergency(command, action_reply);
				break;
			case h248::Token::EmergencyOff:
				result = propEmergencyOff(command, action_reply);
				break;
			case h248::Token::IEPSCall:
				result = propIEPSCall(command, action_reply);
				break;
			case h248::Token::ContextAttr:
				result = propContextAttribute(command, action_reply);
				break;

				// не должны приходить сюда!
			case h248::Token::Notify:
			case h248::Token::ServiceChange:
			default:
				result = false;
				action_reply->addErrorDescriptor(h248::ErrorCode::c421_Unknown_action_or_illegal_combination_of_actions);
				break;
			}

			LOG_CALL("ctx", "id:%u : command %s : %s", m_contextID, Token_toStringLong(cmd_type), result ? "done" : "failed");

			if (!result)
				break;
		}

		LOG_CALL("ctx", "id:%u : do command : Unlock MG Engine context", m_contextID);

		m_mediaContext->mg_context_unlock();

		LOG_CALL("ctx", "id:%u : do command : MG Engine context unlocked", m_contextID);

		LOG_CALL("ctx", "id:%u : do command : release access...", m_contextID);

		releaseAccess();

		LOG_CALL("ctx", "id:%u : do command : access released", m_contextID);

		return result;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Context::addRef(Context* context)
	{
		std_interlocked_inc(&context->m_refCount);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Context::release(Context* context)
	{
		uint32_t ref_count = std_interlocked_dec(&context->m_refCount);

		if (ref_count == 0)
		{
			DELETEO(context);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Context::setLifespan(uint32_t time_in_hours)
	{
		s_lifespan = time_in_hours * MGE_CONTEXT_HOUR;

		if (s_lifespan < MGE_CONTEXT_HOUR)
		{
			s_lifespan = MGE_CONTEXT_DEFAULT_LIFESPAN;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Context::cmdAdd(const h248::Field* command_request, h248::Field* action_reply)
	{
		const char* term_ptr = command_request->getValue();

		LOG_CALL("ctx", "id:%u : Add %s", m_contextID, term_ptr);

		if (term_ptr == nullptr || term_ptr[0] == 0)
		{
			LOG_CALL("ctx", "id:%u : Add : failed : empty termination ID!", m_contextID);
			action_reply->addErrorDescriptor(h248::ErrorCode::c410_Incorrect_identifier, "Incorrect TerminationID");
			return false;
		}

		if (*term_ptr == '[')
		{
			// termination id list!
			LOG_CALL("ctx", "id:%u : Add : failed : terminationID list '%s' not allowed here!", m_contextID, term_ptr);
			action_reply->addErrorDescriptor(h248::ErrorCode::c421_Unknown_action_or_illegal_combination_of_actions);
			return false;
		}

		h248::TerminationID term_id;

		if (!term_id.parse(term_ptr))
		{
			LOG_CALL("ctx", "id:%u : Add : failed : '%s' is invalid termination ID!", m_contextID, term_ptr);
			action_reply->addErrorDescriptor(h248::ErrorCode::c410_Incorrect_identifier, "Incorrect TerminationID");
			return false;
		}

		if (term_id.isRoot())
		{
			LOG_CALL("ctx", "id:%u : Add : failed : ROOT termination ID not allowed here!", m_contextID);
			action_reply->addErrorDescriptor(h248::ErrorCode::c410_Incorrect_identifier, "Incorrect TerminationID ROOT");
			return false;
		}

		if (term_id.isAll())
		{
			LOG_CALL("ctx", "id:%u : Add : failed : ALL termination ID not allowed here!", m_contextID);
			action_reply->addErrorDescriptor(h248::ErrorCode::c410_Incorrect_identifier, "Incorrect TerminationID ALL");
			return false;
		}

		if (term_id.isInterfaceChoose())
		{
			term_id.setInterface(nullptr);
		}

		if (term_id.isTypeChoose())
		{
			term_id.setType(h248::TerminationType::RTP);
		}

		if (findTermination(term_id) != nullptr)
		{
			LOG_CALL("ctx", "id:%u : Add : failed : termination already in context!", m_contextID);
			action_reply->addErrorDescriptor(h248::ErrorCode::c433_TerminationID_is_already_in_a_context);
			return false;
		}

		Termination* term = addNewTermination(term_id, action_reply);

		if (term == nullptr)
		{
			LOG_CALL("ctx", "id:%u : Add : failed : addNewTermination return null!", m_contextID);
			action_reply->addErrorDescriptor(h248::ErrorCode::c500_Internal_software_failure_in_the_MG_or_MGC);
			return false;
		}

		uint32_t termId = term->getTermId().getId();
		h248::Field* command_reply = action_reply->addField(h248::Token::Add, command_request->getValue());
		char buffer[128];
		command_reply->setValue(term_id.toString(buffer, 128));

		LOG_CALL("ctx", "id:%u : Add : setup termination...", m_contextID);

		m_mediaContext->mg_termination_lock(termId);
		bool result = term->setup(command_request, command_reply);
		m_mediaContext->mg_termination_unlock(termId);
		if (!result)
		{
			removeTermination(term);
			LOG_CALL("ctx", "id:%u : Add : setup failed", m_contextID);
			return false;
		}

		m_newContext = false;

		LOG_CALL("ctx", "id:%u : Add : done", m_contextID);

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Context::cmdModify(const h248::Field* command_request, h248::Field* action_reply)
	{
		const char* term_ptr = command_request->getValue();

		LOG_CALL("ctx", "id:%u : Modify %s", m_contextID, term_ptr);

		if (term_ptr == nullptr || term_ptr[0] == 0)
		{
			LOG_CALL("ctx", "id:%u : Modify : failed : empty termination ID!", m_contextID);
			action_reply->addErrorDescriptor(h248::ErrorCode::c410_Incorrect_identifier, "Incorrect TerminationID");
			return false;
		}

		if (*term_ptr == '[')
		{
			// termination id list!
			LOG_CALL("ctx", "id:%u : Modify : failed : terminationID list '%s' not allowed here!", m_contextID, term_ptr);
			action_reply->addErrorDescriptor(h248::ErrorCode::c421_Unknown_action_or_illegal_combination_of_actions);
			return false;
		}

		h248::TerminationID term_id;

		if (!term_id.parse(term_ptr))
		{
			LOG_CALL("ctx", "id:%u : Modify : failed : '%s' is invalid termination ID!", m_contextID, term_ptr);
			action_reply->addErrorDescriptor(h248::ErrorCode::c410_Incorrect_identifier, "Incorrect TerminationID");
			return false;
		}

		if (term_id.isRoot())
		{
			LOG_CALL("ctx", "id:%u : Modify : failed : ROOT termination ID not allowed here!", m_contextID);
			action_reply->addErrorDescriptor(h248::ErrorCode::c410_Incorrect_identifier, "Incorrect TerminationID ROOT");
			return false;
		}

		if (term_id.isAll())
		{
			LOG_CALL("ctx", "id:%u : Modify : failed : ALL termination ID not allowed here!", m_contextID);
			action_reply->addErrorDescriptor(h248::ErrorCode::c410_Incorrect_identifier, "Incorrect TerminationID ALL");
			return false;
		}

		if (term_id.isInterfaceChoose())
		{
			LOG_CALL("ctx", "id:%u : Modify : failed : CHOOSE termination ID not allowed here!", m_contextID);
			action_reply->addErrorDescriptor(h248::ErrorCode::c410_Incorrect_identifier, "Incorrect TerminationID CHOOSE");
			return false;
		}

		if (term_id.isTypeChoose())
		{
			LOG_CALL("ctx", "id:%u : Modify : failed : CHOOSE termination ID not allowed here!", m_contextID);
			action_reply->addErrorDescriptor(h248::ErrorCode::c410_Incorrect_identifier, "Incorrect TerminationID CHOOSE");
			return false;
		}

		Termination* term = findTermination(term_id);

		if (term == nullptr)
		{
			LOG_CALL("ctx", "id:%u : Modify : failed : termination ID not found!", m_contextID);
			action_reply->addErrorDescriptor(h248::ErrorCode::c430_Unknown_TerminationID);
			return false;
		}

		LOG_CALL("ctx", "id:%u : Modify : setup termination...", m_contextID);

		h248::Field* command_reply = action_reply->addField(h248::Token::Modify, command_request->getValue());
		m_mediaContext->mg_termination_lock(term->getId());
		bool result = term->setup(command_request, command_reply);
		m_mediaContext->mg_termination_unlock(term->getId());
		if (!result)
		{
			removeTermination(term);
			LOG_CALL("ctx", "id:%u : Modify : setup failed", m_contextID);
			return false;
		}

		LOG_CALL("ctx", "id:%u : Modify : done", m_contextID);

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Context::cmdSubtract(const h248::Field* command_request, h248::Field* action_reply)
	{
		const char* term_ptr = command_request->getValue();

		LOG_CALL("ctx", "id:%u : Subtract %s", m_contextID, term_ptr);

		if (term_ptr == nullptr || term_ptr[0] == 0)
		{
			LOG_CALL("ctx", "id:%u : Subtract : failed : empty termination ID!", m_contextID);
			action_reply->addErrorDescriptor(h248::ErrorCode::c410_Incorrect_identifier, "Incorrect TerminationID");
			return false;
		}

		if (*term_ptr == '[')
		{
			// termination id list!
			LOG_CALL("ctx", "id:%u : Subtract : failed : terminationID list '%s' not allowed here!", m_contextID, term_ptr);
			action_reply->addErrorDescriptor(h248::ErrorCode::c421_Unknown_action_or_illegal_combination_of_actions);
			return false;
		}

		h248::TerminationID term_id;

		if (!term_id.parse(term_ptr))
		{
			LOG_CALL("ctx", "id:%u : Subtract : failed : '%s' is invalid termination ID!", m_contextID, term_ptr);
			action_reply->addErrorDescriptor(h248::ErrorCode::c410_Incorrect_identifier, "Incorrect TerminationID");
			return false;
		}

		if (term_id.isRoot())
		{
			LOG_CALL("ctx", "id:%u : Subtract : failed : ROOT termination ID not allowed here!", m_contextID);
			action_reply->addErrorDescriptor(h248::ErrorCode::c410_Incorrect_identifier, "Incorrect TerminationID ROOT");
			return false;
		}

		if (term_id.isInterfaceChoose() || term_id.isTypeChoose() || term_id.isIdChoose())
		{
			LOG_CALL("ctx", "id:%u : Subtract : failed : CHOOSE termination ID not allowed here!", m_contextID);
			action_reply->addErrorDescriptor(h248::ErrorCode::c410_Incorrect_identifier, "Incorrect TerminationID ALL");
			return false;
		}

		if (term_id.isAll())
		{
			LOG_CALL("ctx", "id:%u : Subtract : remove ALL terminations", m_contextID);

			char buffer[128];
			for (int i = m_termimationList.getCount() - 1; i >= 0; i--)
			{

				Termination* term = m_termimationList[i];
				m_mediaContext->mg_termination_lock(term->getId());
				action_reply->addField(h248::Token::Subtract, term->getTermId().toString(buffer, 128));
				term->destroy();
				m_termimationList.removeAt(i);
				DELETEO(term);
			}

			LOG_CALL("ctx", "id:%u : Subtract : set readyToDelete", m_contextID);

			m_readyToDelete = true;// destroy();
		}
		else
		{
			LOG_CALL("ctx", "id:%u : Subtract : remove termination %s", m_contextID, term_ptr);

			int index;
			Termination* term = findTermination(term_id, &index);

			if (term == nullptr)
			{
				LOG_CALL("ctx", "id:%u : Subtract : failed : termination ID not found!", m_contextID);
				action_reply->addErrorDescriptor(h248::ErrorCode::c435_Termination_ID_is_not_in_specified_context);
				return false;
			}
			m_mediaContext->mg_termination_lock(term->getId());

			char buffer[128];
			h248::Field* command_reply = action_reply->addField(h248::Token::Subtract, term_id.toString(buffer, 128));

			term->destroy();
			m_termimationList.removeAt(index);
			DELETEO(term);

			if (m_termimationList.getCount() == 0)
			{
				LOG_CALL("ctx", "id:%u : Subtract : set readyToDelete", m_contextID);
				m_readyToDelete = true;
			}
		}

		LOG_CALL("ctx", "id:%u : Subtract : done", m_contextID);

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Context::cmdMove(const h248::Field* ctx_command, h248::Field* response)
	{
		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Context::cmdAuditValue(const h248::Field* ctx_command, h248::Field* response)
	{
		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Context::cmdAuditCapability(const h248::Field* ctx_command, h248::Field* response)
	{
		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Context::propTopology(const h248::Field* ctx_command, h248::Field* response)
	{
		rtl::ArrayT<h248::TopologyTriple> triple_list;

		h248::TopologyTriple triple;
		int state = 0;

		for (int i = 0; i < ctx_command->getFieldCount(); i++)
		{
			const h248::Field* node = ctx_command->getFieldAt(i);
			h248::TerminationID term_id;
			switch (state)
			{
			case 0: // первый элемент в трипле
				triple.term_from = getTerminationID(node->getName());
				if (triple.term_from != 0)
					state++;
				else
					state = -1;
				break;
			case 1: // второй элемент в трипле
				triple.term_to = getTerminationID(node->getName());
				if (triple.term_from != 0)
					state++;
				else
					state = -1;
				break;
			case 2: // тип соединения в трипле
				triple.direction = h248::TopologyDirection_parse(node->getName());
				if (triple.direction != h248::TopologyDirection::Error)
				{
					triple_list.add(triple);
					state = 0;
				}
				break;
			default: // ошибка чтения
				return false;
			}
		}

		if (triple_list.getCount() > 0)
		{
			m_mediaContext->mg_context_set_topology(triple_list, response);
		}

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Context::propPriority(const h248::Field* ctx_property, h248::Field* response)
	{
		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Context::propEmergency(const h248::Field* ctx_property, h248::Field* response)
	{
		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Context::propEmergencyOff(const h248::Field* ctx_property, h248::Field* response)
	{
		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Context::propIEPSCall(const h248::Field* ctx_property, h248::Field* response)
	{
		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Context::propContextAttribute(const h248::Field* ctx_property, h248::Field* response)
	{
		if (m_mediaContext == nullptr)
		{
			return false;
		}

		for (int i = 0; i < ctx_property->getFieldCount(); i++)
		{
			const h248::Field* prop = ctx_property->getFieldAt(i);
			if (!m_mediaContext->mg_context_set_property(prop, response))
			{
				return false;
			}
		}

		return true;
	}
	//--------------------------------------
	// добавляем и инициализируем!
	//--------------------------------------
	Termination* Context::addNewTermination(h248::TerminationID& termId, h248::Field* reply)
	{
		// получим следующий идентификатор
		termId.setId(0);// generateTermId());

		// передается не полный ид терма!

		Termination* term = NEW Termination(m_mediaContext, termId);

		if (!term->initialize(reply))
		{
			LOG_CALL("ctx", "id:%u : add termination : failed : MG Engine termination initialize() return error", m_contextID);
			term->destroy();
			DELETEO(term);
			return nullptr;
		}

		// нужно обязательно получить полный ид терма!

		termId = term->getTermId();

		m_termimationList.add(term);
		char buff[64];
		LOG_CALL("ctx", "id:%u : add termination : done : termId %s", m_contextID, termId.toString(buff, 64));

		return term;
	}
	//--------------------------------------
	// только удаляем из списка!
	//--------------------------------------
	bool Context::removeTermination(Termination* term)
	{
		for (int i = 0; i < m_termimationList.getCount(); i++)
		{
			Termination* it = m_termimationList[i];
			if (it == term)
			{
				m_termimationList.removeAt(i);
				m_mediaContext->mg_termination_lock(term->getId());
				term->destroy();
				DELETEO(term);

				LOG_CALL("ctx", "id:%u : remove termination : done", m_contextID);

				return true;
			}
		}

		LOG_CALL("ctx", "id:%u : remove termination : failed : termination not found", m_contextID);

		return false;
	}
	//--------------------------------------
	// ищем по точному соответствию
	//--------------------------------------
	Termination* Context::findTermination(const h248::TerminationID& term_info, int* index)
	{
		for (int i = 0; i < m_termimationList.getCount(); i++)
		{
			Termination* it = m_termimationList[i];
			if (it->getTermId() == term_info)
			{
				if (index != nullptr)
				{
					*index = i;
				}

				return it;
			}
		}

		return nullptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	uint32_t Context::getTerminationID(const char* term)
	{
		h248::TerminationID term_id;

		if (!term_id.parse(term))
		{
			return 0;
		}

		Termination* term_ptr = findTermination(term_id);

		return term_ptr != nullptr ? term_ptr->getId() : 0;
	}
	//--------------------------------------
	// ожидание последовательного доступа к контексту
	//--------------------------------------
	void Context::waitAccess()
	{
		rtl::Event* wait_event = nullptr;
		{
			//rtl::MutexLock lock(m_accessSync);
			rtl::MutexWatchLock lock(m_accessSync, __FUNCTION__);

			if (m_accessCaptured)
			{
				// добавляем поток в очередь ожидания
				wait_event = NEW rtl::Event;
				wait_event->create(true, false);
				m_waitList.add(wait_event);
			}
		}

		if (wait_event != nullptr)
		{
			// ждем освобождения
			wait_event->wait(INFINITE);

			// нужно блочить удаление евента!
			// дестрой всех обходит и ему нужен эксклюзивный доступ к списку ожидания?
			// удаляем только евент
			wait_event->close();
			DELETEO(wait_event);

			// дождались
			// флаг занятости уже взведен!
		}
		else
		{
			// захват ресурса
			m_accessCaptured = true;
		}
	}
	//--------------------------------------
	// освобождение доступа и передача контекста следующему потоку
	//--------------------------------------
	void Context::releaseAccess()
	{
		//rtl::MutexLock lock(m_accessSync);
		rtl::MutexWatchLock lock(m_accessSync, __FUNCTION__);

		if (m_waitList.getCount() > 0)
		{
			// нас ждут
			rtl::Event* wait_event = m_waitList[0];
			m_waitList.removeAt(0);
			// разбудим следующий поток
			wait_event->signal();

			// флаг занятости не сбрасываем!
		}
		else
		{
			// просто сообщаем что ресурс свободный
			m_accessCaptured = false;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	//uint32_t Context::generateTermId()
	//{
	//	uint32_t id = MG_TERM_ROOT;

	//	while (id == MG_TERM_ROOT || id == MG_TERM_CHOOSE || id == MG_TERM_ALL)
	//	{
	//		id = std_interlocked_inc(&m_termIdGen);
	//	}

	//	return id;
	//}
}
//--------------------------------------
