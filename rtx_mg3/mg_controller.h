﻿/* RTX H.248 Media Gate
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_transaction_dispatcher.h"
#include "mge_manager.h"


namespace mg3
{
	//--------------------------------------
	// представитель контроллера для управления связанным с ним виртуальным медиа шлюзом
	//--------------------------------------
	class MGC_Proxy : h248::IRequestSender, h248::IRequestReceiver, IMG3_EventHandler
	{
		//--------------------------------------
		//
		//--------------------------------------
		enum State
		{
			State_New,					// -> ready | failed
			State_Ready,				// -> conneting | failed
			State_Connecting,			// -> connected | disconnected | failed
			State_Connected,			// -> disconnecting | failed
			State_Disconnecting,		// -> disconnected | failed 
			State_Disconnected,			// -> terminated
			State_Terminated,			// .
			State_Failed,				// -> terminated
		};

		// common controler variables
		uint32_t m_id;										// иденитификатор для логирования
		static volatile uint32_t s_idGen;					// генератор идентификаторов
		rtl::ArrayT<h248::EndPoint> m_altRouteList;				// список альтернативных MGC
		int m_currentRouteIndex;							// указатель на текущий маршрут
		h248::Route m_route;								// текущий путь к физическому конторллеру
		uint32_t m_mgcVersion;								// оговоренная версия протокола
		rtl::MutexWatch m_serviceSync;
		volatile State m_state;								// состояние контроллера
		volatile bool m_restartRaised;
		h248::TransactionID m_requestTranID;				// номер транзакции отправленного сервисного сообщения
		h248::TransactionID m_pingTranID;					// номер транзакции отправленного пинга
		h248::TransactionDispatcher m_dispatcher;			// диспетчер транзакций данного контроллера
		VirtualGate* m_gate;								// интерфейс прикрепленного виртуального шлюза

		volatile bool m_stoping;
		volatile bool m_stoped;

	public:
		MGC_Proxy();
		~MGC_Proxy();

		const h248::Route& getRoute() const { return m_route; }
		uint32_t getId() const { return m_id; }
		State getState() const { return m_state; }
		h248::TransactionDispatcher& dispatch() { return m_dispatcher; }

		bool initialize(const rtl::ArrayT<h248::EndPoint>& mgcRoutelist);
		void destroy();

		bool start();
		bool stop();


	private:
		bool restart(h248::Token method, h248::Reason code, const char* text);
		// первичная обработка запросов
		virtual void IRequestReceiver_requestReceived(h248::TransactionID tid, const h248::Field* request); // , h248::Field* reply
		virtual void IRequestReceiver_transportFailure(int net_error, const h248::Route& route);
		virtual void IRequestReceiver_transportDisconnected(const h248::Route& route);


		// обработка сервисных ответов на запросы от MG к MGC
		virtual bool RequestSender_responseReceived(h248::TransactionID tid, const h248::Field* reply);
		virtual void RequestSender_errorReceived(h248::TransactionID tid, const h248::Field* error);
		virtual void RequestSender_responseTimeout(h248::TransactionID tid);

		// IMG3_EventHandler
		virtual void mge__notify_raised(h248::Field* notify);
		virtual void mge__send_reply(h248::Field* reply, h248::TransactionID tid);
		virtual void mge__service_change_raised(h248::Field* service_changed);


		// обработка регистрации и сервисных сообщений
		bool sendServiceChange(h248::Token method, h248::Reason code, const char* text);
		void sendPing();

		bool serviceChangeResponseReceived(h248::TransactionID tid, const h248::Field* reply);
		void serviceAction(const h248::Field* request, h248::Field* reply);
		
		void processServiceChange(const h248::Field* request, h248::Field* reply);
		void processAuditCapability(const h248::Field* request, h248::Field* reply);
		void processDefault(const h248::Field* request, h248::Field* reply);

		void raiseRestartEvent(h248::Token method, h248::Reason reason);
		void asyncRestart(h248::Token method, h248::Reason reason);

		const char* State_toString(State state);

		static void pingpongTimerProc(rtl::Timer* sender, uint32_t elapsed, void* userData, int tid);
		static void connectTimer(rtl::Timer* sender, uint32_t elapsed, void* userData, int tid);
		static void asyncCallback(void* userdata, uint16_t callId, uintptr_t intParam, void* ptrParam);
	};
}
//--------------------------------------
