﻿/* RTX H.248 Media Gate
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_interface.h"
#include "mge_termination.h"

namespace mg3
{

	//--------------------------------------
	//
	//--------------------------------------
#define MGE_CONTEXT_ASYNC__DTMF_EVENT			1001
#define MGE_CONTEXT_ASYNC__NO_VOICE_EVENT		1002
#define MGE_CONTEXT_ASYNC__VOICE_RESTORED_EVENT	1003
#define MGE_CONTEXT_ASYNC__NET_ERROR			1004
#define MGE_CONTEXT_ASYNC__IVR_END_PLAY			1005
#define MGE_CONTEXT_ASYNC__FAX_END				1006
#define MGE_CONTEXT_ASYNC__VAD_EVENT			1007

	//--------------------------------------
	//
	//--------------------------------------
	class VirtualGate;
	//--------------------------------------
	//
	//--------------------------------------
	class Context
	{
		rtl::MutexWatch m_accessSync;
		volatile bool m_accessCaptured;
		rtl::ArrayT<rtl::Event*> m_waitList;
		volatile uint32_t m_refCount;

		uint32_t m_contextID;
		volatile bool m_readyToDelete;
		volatile bool m_newContext;
		volatile uint32_t m_timestamp;
		static int s_lifespan;

		VirtualGate& m_owner;

		h248::IMediaContext* m_mediaContext;

		rtl::ArrayT<Termination*> m_termimationList;

		// по хорошему нужно проверять на существование старого цикла по этому номеру!
		//static volatile uint32_t m_termIdGen;

	public:
		Context(VirtualGate& owner, h248::IMediaContext* mg_context);
		~Context();

		bool initialize(h248::Field* reply);
		void destroy();

		uint32_t getId() { return m_contextID; }
		h248::IMediaContext* getMediaContext() { return m_mediaContext; }

		// единственная команда
		bool doCommands(const h248::Field* actionRequest, h248::Field* actionReply);
		bool readyToDelete() { return m_readyToDelete; }
		bool isNewContext() { return m_newContext; }
		bool checkLifetime(uint32_t currentTime);

		static void addRef(Context* context);
		static void release(Context* context);
		static void setLifespan(uint32_t hours);

	private:
		bool cmdAdd(const h248::Field* commandRequest, h248::Field* commandReply);
		bool cmdModify(const h248::Field* commandRequest, h248::Field* commandReply);
		bool cmdSubtract(const h248::Field* commandRequest, h248::Field* commandReply);
		bool cmdMove(const h248::Field* commandRequest, h248::Field* commandReply);
		bool cmdAuditValue(const h248::Field* commandRequest, h248::Field* commandReply);
		bool cmdAuditCapability(const h248::Field* commandRequest, h248::Field* commandReply);
		bool propTopology(const h248::Field* commandRequest, h248::Field* commandReply);
		bool propPriority(const h248::Field* commandRequest, h248::Field* commandReply);
		bool propEmergency(const h248::Field* commandRequest, h248::Field* commandReply);
		bool propEmergencyOff(const h248::Field* commandRequest, h248::Field* commandReply);
		bool propIEPSCall(const h248::Field* commandRequest, h248::Field* commandReply);
		bool propContextAttribute(const h248::Field* commandRequest, h248::Field* commandReply);

		//bool parse_termination(const char* term_id, mge_term_id_t& term_info);
		Termination* addNewTermination(h248::TerminationID& termInfo, h248::Field* reply);
		bool removeTermination(Termination* term);
		Termination* findTermination(const h248::TerminationID& termInfo, int* index = nullptr);
		uint32_t getTerminationID(const char* termId);

		void waitAccess();
		void releaseAccess();

		//static uint32_t generateTermId();
	};
}
//--------------------------------------
