﻿/* RTX H.248 Media Gate
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_date_time.h"
#include "net/ip_address4.h"
#include "std_filesystem.h"
#include "std_sys_env.h"
#include "std_config.h"
#include "mg_service.h"
#include "std_lock_watcher.h"
#include "mmt_lazy_writer.h"
// #include <atomic>
//--------------------------------------
//
//--------------------------------------
#if defined (DEBUG_MEMORY)
void* operator new (size_t size, char* file, int line)
{
	return std_mem_checker_malloc(size, file, line, rc_malloc_new_e);
}
void* operator new[](size_t size, char* file, int line)
{
	return std_mem_checker_malloc(size, file, line, rc_malloc_newar_e);
}
void operator delete (void* mem_ptr)
{
	std_mem_checker_free(mem_ptr, rc_malloc_delete_e);
}
void operator delete[](void* mem_ptr)
{
	std_mem_checker_free(mem_ptr, rc_malloc_deletear_e);
}
#endif

//--------------------------------------
//
//--------------------------------------
int g_context_counter = -1;
int g_termination_counter = -1;
//--------------------------------------
//
//--------------------------------------
int initialize_config(const char* path);
void resolve_configuration();
int initialize_loggers();
void close_loggers();
void initialize_watcher();
void release_watcher();
static void timer_event_callback(rtl::Timer* sender, uint32_t elapsed, void* user_data, int tid);
static void async_call_stat(void* userData, uint16_t callId, uintptr_t intParam, void* ptrParam);

extern void wave_file_test();
//-----------------------------------------------
// statistics values
//-----------------------------------------------
#define CONF_STAT_FREQUENCY "stat-frequency"
#define STAT_DEFAULT_FREQUENCY 5000			// 5 sec
#define STAT_MIN_FREQUENCY 1000				// 1 sec
#define STAT_MAX_FREQUENCY (1000 * 3600)	// 1 hour
//--------------------------------------
//
//--------------------------------------
rtl::Logger Stat;
//--------------------------------------
//
//--------------------------------------
char g_log_path_buffer[MAX_PATH] = { 0 };
//--------------------------------------
//
//--------------------------------------
void process_test_command(char* params);
void process_start_command(char* params);
void authorization_test();
//--------------------------------------
//
//--------------------------------------
int main(int argc, char* argv[])
{
	rtl::DateTime dt;
	char tmp[256];
	printf("RTX MEGACO(H.248) MediaGate service %d.%d.%d %d:%d:%d\n", dt.getYear(), dt.getMonth(), dt.getDay(), dt.getHour(), dt.getMinute(), dt.setSecond());

#if defined (TARGET_OS_WINDOWS)
	setlocale(LC_ALL, ".ACP");
#else
	setlocale(LC_ALL, "");
#endif

	EXCEPTION_ENTER;

	try
	{
		const char* cfg_path = nullptr;

		if (argc > 1)
		{
			cfg_path = argv[1];
		}

		//Err.create("c:/temp", "err", LOG_APPEND);

		int res = initialize_config(cfg_path);

		printf("config %s!\n", res ? "loaded" : "not loaded");

		initialize_watcher();

		initialize_loggers();

		LOG_CALL("main", "starting on config %s...", cfg_path);

		LOG_CALL("main", "starting log with trace flags: %s", rtl::Logger::trace_to_string(tmp, 256, false));

		LOG_CALL("main", "SYNC       -> %s", Config.get_config_value("SYNC")); 
		LOG_CALL("main", "RECORD      -> %s", Config.get_config_value("RECORD"));
		LOG_CALL("main", "SITESHARE   -> %s", Config.get_config_value("SITESHARE"));
		LOG_CALL("main", "GLOBALSHARE -> %s", Config.get_config_value("GLOBALSHARE"));

		const char* cfg_value = Config.get_config_value(CONF_ASYNC_THREAD_INITIAL);
		uint32_t async_initial = cfg_value != nullptr ? strtoul(cfg_value, nullptr, 10) : ASYNC_THREADS;
		cfg_value = Config.get_config_value(CONF_ASYNC_THREAD_MAX);
		uint32_t async_max = cfg_value != nullptr ? strtoul(cfg_value, nullptr, 10) : 0;
		LOG_CALL("main", "init async call manager %d/%d...", async_initial, async_max);
		rtl::async_call_manager_t::createAsyncCall(async_initial, async_max);

		cfg_value = Config.get_config_value(CONF_MG_LAZY_RECORD_TIME);
		media::LazyMediaWriter::set_record_max_time(cfg_value == nullptr ? 5000 : strtoul(cfg_value, nullptr, 10));

		rtl::Timer::initializeGlobalTimer(false);

		// добавим счетчики
		g_context_counter = rtl::res_counter_t::add("MG_CTX");
		g_termination_counter = rtl::res_counter_t::add("MG_TERM");
		megaco_register_counters();

		// do not forgot to comment tests
		// wave_file_test();

		process_start_command(nullptr);

		printf("RTX MEGACO(H.248) stopping services\r\n");

		mg3::Service::stop();

		rtl::async_call_manager_t::destroyAsyncCall();
		LOG_CALL("main", "destroy async call manager");

		printf("RTX MEGACO(H.248) exit\r\n");

		close_loggers();

		release_watcher();

		Config.close();
	}
	catch (rtl::Exception& ex)
	{
		LOG_ERROR("main", "Exception raised %s", ex.get_message());
	}

	EXCEPTION_LEAVE;

	return 0;
}
//--------------------------------------
//
//--------------------------------------
int initialize_config(const char* path)
{
	rtl::String cfgpath = path;

	printf("config path arg...%s\n", path);
	//Err.log("cfg", "config path arg...%s", path);
	if (path == nullptr || path[0] == 0)
	{
		char app_path[MAX_PATH];
		std_get_application_startup_path(app_path, MAX_PATH);
		cfgpath = app_path;

		cfgpath += FS_PATH_DELIMITER;
	}

	if (!cfgpath.isEmpty())
	{
		if (cfgpath[cfgpath.getLength()-1] == FS_PATH_DELIMITER)
		{
			cfgpath += "rtx_mg3.cfg";
		}
	}
	else
	{
		cfgpath = "rtx_mg3.cfg";
	}

	printf("config path...%s\n", (const char*)cfgpath);
	//Err.log("cfg", "config path...%s", (const char*)cfgpath);

	bool result = Config.read(cfgpath);

	if (result)
	{
		resolve_configuration();
	}

	return result;
}
//--------------------------------------
//
//--------------------------------------
void resolve_configuration()
{
	int path_count = 0;
	char key[128];
	int len;

	const char* div;
	const char* value = Config.get_config_value("mg-path-count", 0);

	//Err.log("cfg", "mg-path-count...%s", value);

	if (value == nullptr || value[0] == 0)
		return;

	if ((path_count = strtoul(value, nullptr, 10)) == 0)
		return;

	for (int i = 1; i <= path_count; i++)
	{
		if ((len = std_snprintf(key, 128, "mg-path-%u", i)) <= 0)
			return;

		key[len] = 0;

		//Err.log("cfg", "mg-path-%i...%s", i, key);
		
		if ((value = Config.get_config_value(key)) == nullptr || value[0] != ':')
			return;

		//Err.log("cfg", "mg-path-%i...%s", i, value);

		// path with key -> ":MEDIAKEY" + "c:/dir1/dir2"

		if ((div = strchr(value + 1, '/')) == nullptr)
			return;

		int diff = PTR_DIFF(div, value);

		if (diff >= 128)
			return;

		strncpy(key, value+1, diff-1);
		key[diff-1] = 0;

		const char* path = value + diff + 1;

		//Err.log("main", "mg-path-%i...set PATH key %s to %s", i, key, path);

		Config.set_config_value(key, path);
	}
}
//--------------------------------------
//
//--------------------------------------
void timer_event_callback(rtl::Timer* sender, uint32_t elapsed, void* user_data, int tid)
{
	rtl::async_call_manager_t::callAsync(async_call_stat, nullptr, 0, 0, nullptr);
}
//--------------------------------------
//
//--------------------------------------
void async_call_stat(void* userData, uint16_t callId, uintptr_t intParam, void* ptrParam)
{
	char buffer[2048];
	int len = rtl::res_counter_t::get_statistics(buffer, 2048);

	buffer[len] = 0;

	Stat.log("stat", "%s", buffer);
}
//--------------------------------------
//
//--------------------------------------
int initialize_loggers()
{
	const char* cfg_value = Config.get_config_value(CONF_LOG_ROOT_PATH);

	if (cfg_value == nullptr || cfg_value[0] == 0)
	{
		std_get_application_startup_path(g_log_path_buffer, MAX_PATH);
	}
	else
	{
		strncpy(g_log_path_buffer, cfg_value, MAX_PATH);
		g_log_path_buffer[MAX_PATH-1] = 0;
	}

	int len = std_strlen(g_log_path_buffer);

	if (g_log_path_buffer[len - 1] == FS_PATH_DELIMITER)
	{
		g_log_path_buffer[len - 1] = 0;
	}

	cfg_value = Config.get_config_value(CONF_LOG_MAX_SIZE);
	uint64_t max_size = cfg_value != nullptr ? strtoull(cfg_value, nullptr, 10) : 10;
	cfg_value = Config.get_config_value(CONF_LOG_PART_SIZE);
	uint64_t part_size = cfg_value != nullptr ? strtoull(cfg_value, nullptr, 10) : 10;
	cfg_value = Config.get_config_value(CONF_LOG_PART_COUNT);
	uint64_t part_count = cfg_value != nullptr ? strtoull(cfg_value, nullptr, 10) : 10;

	rtl::Logger::set_log_sizes(max_size, part_size, part_count);

	cfg_value = Config.get_config_value(CONF_LOG_TRACE);

	rtl::Logger::set_trace_flags(cfg_value);

	Log.create(g_log_path_buffer, "mg3", LOG_APPEND);
	h248::LogProto.create(g_log_path_buffer, "proto", LOG_APPEND);
	Stat.create(g_log_path_buffer, "stat", LOG_APPEND);

	// запуст сбора статистики
	if (rtl::Logger::check_trace(TRF_STAT))
	{
		cfg_value = Config.get_config_value(CONF_STAT_FREQUENCY);
		int freq;
		
		if (cfg_value)
		{
			freq = atoi(cfg_value);
			
			if (freq < STAT_MIN_FREQUENCY)
				freq = STAT_MIN_FREQUENCY;
			else if (freq > STAT_MAX_FREQUENCY)
				freq = STAT_MAX_FREQUENCY;
		}
		else
		{
			freq = STAT_DEFAULT_FREQUENCY;
		}

		rtl::Timer::setTimer(timer_event_callback, nullptr, 0, freq, true); 
	}

	return 0;
}
//--------------------------------------
//
//--------------------------------------
void close_loggers()
{
	if (rtl::Logger::check_trace(TRF_STAT))
	{
		rtl::Timer::stopTimer(timer_event_callback, nullptr, 0);
	}

	Log.destroy();
	Err.destroy();
	h248::LogProto.destroy();
	Stat.destroy();
}
//--------------------------------------
//
//--------------------------------------
void initialize_watcher()
{
	const char* cfg_value = Config.get_config_value(CONF_WATCHER_ENABLED);
	
	if (cfg_value != nullptr && std_stricmp(cfg_value, "true") == 0)
	{
		cfg_value = Config.get_config_value(CONF_WATCHER_PERIOD);
		int  watcherPeriod = 30000;
		if (cfg_value != nullptr)
			watcherPeriod = strtoul(cfg_value, nullptr, 10) * 1000;

		LOG_CALL("main", "Mutex watcher start with period %u ms", watcherPeriod);
		rtl::MutexWatch::setup_watcher(true, watcherPeriod);
	}
}
void release_watcher()
{
	rtl::MutexWatch::setup_watcher(false, 0);

	rtl::Watch_Destroy();
}
//--------------------------------------
