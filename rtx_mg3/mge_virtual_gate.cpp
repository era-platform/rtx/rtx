﻿/* RTX H.248 Media Gate
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mge_manager.h"
#include "mge_context.h"

namespace mg3
{

	//--------------------------------------
	//
	//--------------------------------------
	volatile uint32_t VirtualGate::s_idGen = 0;
	//--------------------------------------
	//
	//--------------------------------------
	VirtualGate::VirtualGate(IMG3_EventHandler* handler) :
		m_ctxList(compareObj, compareKey),
		m_eventHandler(handler),
		m_sync("mg-virt-gate")
	{
		uint32_t id = std_interlocked_inc(&s_idGen);

		std_snprintf(m_vid, 16, "vid(%u)", id);

	}
	//--------------------------------------
	//
	//--------------------------------------
	VirtualGate::~VirtualGate()
	{
		// do noting!
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool VirtualGate::start()
	{
		LOG_CALL("mge-v", "%s : start...", m_vid);

		{
			//rtl::MutexLock lock(m_sync);
			rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

			m_started = true;
		}

		rtl::Timer::setTimer(timerProc, this, 0, 60000, true);

		LOG_CALL("mge-v", "%s : started", m_vid);

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void VirtualGate::stop()
	{
		LOG_CALL("mge-v", "%s : stop...", m_vid);

		m_started = false;

		rtl::Timer::stopTimer(timerProc, this, 0);

		// уничтожаем все контексты!
		//rtl::MutexLock lock(m_sync);
		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		LOG_CALL("mge-v", "%s : stop : release %d contexts", m_vid, m_ctxList.getCount());

		for (int i = 0; i < m_ctxList.getCount(); i++)
		{
			Context* context = m_ctxList[i];
			context->destroy();
			DELETEO(context);
		}

		m_ctxList.clear();

		LOG_CALL("mge-v", "%s : stopped", m_vid);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void VirtualGate::doAction(const h248::Field* request, h248::TransactionID tid)
	{
		if (!m_started)
		{
			h248::Field reply(h248::Token::Reply, request->getValue());
			reply.addErrorDescriptor(h248::ErrorCode::c502_Not_ready);
			m_eventHandler->mge__send_reply(&reply, tid);
			return;
		}

		h248::Field* copy = NEW h248::Field(*request);

		rtl::async_call_manager_t::callAsync(doActionCallback, this, 1001, tid.id, copy);
		//do_action_callback(this, 1001, tid.id, copy);
	}
	void VirtualGate::doActionCallback(void* user, uint16_t callId, uintptr_t intParam, void* ptrParam)
	{
		uint32_t start = rtl::DateTime::getTicks();

		LOG_CALL("mge-v", "do action async... ");

		VirtualGate* gate = (VirtualGate*)user;
		h248::Field* request = (h248::Field*)ptrParam;
		h248::TransactionID tid;
		tid.id = intParam;

		h248::Field* action_request = request->getField(h248::Token::Context);

		h248::Field reply(h248::Token::Reply, request->getValue());
		h248::Field* action_reply = reply.addField(h248::Token::Context, action_request->getValue());

		gate->doActionAsync(action_request, action_reply);
		gate->m_eventHandler->mge__send_reply(&reply, tid);

		DELETEO(request);

		LOG_CALL("mge-v", "action work time %d... ", rtl::DateTime::getTicks() - start);
	}
	//----------------------------------------------
	//
	//----------------------------------------------
	void VirtualGate::doActionAsync(const h248::Field* action_request, h248::Field* action_reply)
	{
		if (!m_started)
		{
			action_reply->addErrorDescriptor(h248::ErrorCode::c502_Not_ready);
			return;
		}

		const char* context_ptr = action_request->getValue();
		uint32_t context_id = 0;
		Context* context = nullptr;

		LOG_CALL("mge-v", "%s : do_action for Context = %s...", m_vid, (const char*)action_request->getValue());

		if (context_ptr == nullptr || context_ptr[0] == 0)
		{
			// TODO: error response return
			action_reply->addErrorDescriptor(h248::ErrorCode::c411_The_transaction_refers_to_an_unknown_ContextID);
			LOG_WARN("mge-v", "%s : do_action : failed : empty ContextID", m_vid);
			return;
		}

		if (context_ptr[0] == '-' && context_ptr[1] == 0) // NULL -> для всех контекстов -- недолжно доходить сюда
		{
			// TODO: работа для всех контекстов!
			LOG_WARN("mge-v", "%s : do_action : failed : invalid 'NULL' ContextID", m_vid);
			action_reply->addErrorDescriptor(h248::ErrorCode::c411_The_transaction_refers_to_an_unknown_ContextID, "The transaction refers to an NULL ContextID");
			return;
		}
		else if (context_ptr[0] == '*' && context_ptr[1] == 0) // ALL -> любой контекст
		{
			// TODO: мы должны найти контекст содержащий указанные тернимейшены и продолжить работу

			doActionForAll(action_request, action_reply);
			return;

			/*LOG_WARN("mge-v", "%s : do_action : failed : invalid 'ALL' ContextID", m_vid);
			action_reply->addErrorDescriptor(411, "The transaction refers to an unknown ContextID");
			return false;*/
		}
		else if (context_ptr[0] == '$' && context_ptr[1] == 0) // CHOOSE -> новый контекст
		{
			if ((context = createContext()) == nullptr)
			{
				LOG_WARN("mge-v", "%s : do_action : failed : create_context() return null", m_vid);
				action_reply->addErrorDescriptor(h248::ErrorCode::c412_No_ContextIDs_available);
				return;
			}
			
			if (!context->initialize(action_reply))
			{
				LOG_WARN("mge-v", "%s : do_action : the context initialize function returns an error", m_vid);
				return;
			}

			action_reply->setValue(context->getId());

			LOG_CALL("mge-v", "%s : do_action : new Context made ContextID %u", m_vid, context->getId());
		}
		else
		{
			// существующий контекст
			context_id = strtoul(context_ptr, nullptr, 10);

			if (context_id == 0)
			{
				// TODO: return parser error!
				// если хотели NULL то пусть передают '-' а не '0'
				LOG_WARN("mge-v", "%s : do_action : failed : invalid '0' ContextID", m_vid);
				action_reply->addErrorDescriptor(h248::ErrorCode::c422_Syntax_error_in_action, "Syntax error in action request: '0' is not proper id, use '-'");
				return;
			}

			if (context_id == 0xFFFFFFFF)
			{
				// TODO: return parser error!
				// тоже самое -- если хотели CHOOSE то пусть передают '$' а не '-1'
				LOG_WARN("mge-v", "%s : do_action : failed : invalid '-1' ContextID", m_vid);
				action_reply->addErrorDescriptor(h248::ErrorCode::c422_Syntax_error_in_action, "Syntax error in action request: '-1' is not proper id, use CHOOSE '$'");
				return;
			}

			if (context_id == 0xFFFFFFFE)
			{
				// TODO: return parser error!
				// тоже самое -- если хотели ALL то пусть передают '*' а не '-2'
				LOG_WARN("mge-v", "%s : do_action : failed : invalid '-2' ContextID", m_vid);
				action_reply->addErrorDescriptor(h248::ErrorCode::c422_Syntax_error_in_action, "Syntax error in action request: '-2' is not proper id, use ALL '*'");
				return;
			}

			if ((context = findContext(context_id)) == nullptr)
			{
				LOG_WARN("mge-v", "%s : do_action : failed : unknown ContextID", m_vid);
				action_reply->addErrorDescriptor(h248::ErrorCode::c411_The_transaction_refers_to_an_unknown_ContextID);
				return;
			}
		}

		LOG_CALL("mge-v", "%s : do_action : context %u found : do commands...", m_vid, context->getId());


		bool result = context->doCommands(action_request, action_reply);

		LOG_CALL("mge-v", "%s : do_command result %s (rtd:%s,inc:%s): done", m_vid,
			STR_BOOL(result),
			STR_BOOL(context->readyToDelete()),
			STR_BOOL(context->isNewContext()));

		
		if (context->readyToDelete() || context->isNewContext())
		{
			//LOG_CALL("mge-v", "%s : do_action : new context or is ready to delete", m_vid);
			removeContext(context);
		}

		Context::release(context);

		LOG_CALL("mge-v", "%s : do_action : done", m_vid);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void VirtualGate::raiseNotifyEvent(h248::Field* notify)
	{
		m_eventHandler->mge__notify_raised(notify);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void VirtualGate::raiseServiceChangeEvent(h248::Field* service_changed)
	{
		m_eventHandler->mge__service_change_raised(service_changed);
	}
	//--------------------------------------
	//
	//--------------------------------------
	int VirtualGate::compareObj(const ContextPtr& l, const ContextPtr& r)
	{
		uint32_t l_key = l->getId();
		uint32_t r_key = r->getId();

		return l_key - r_key;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int VirtualGate::compareKey(const uint32_t& l, const ContextPtr& r)
	{
		uint32_t r_key = r->getId();

		return l - r_key;
	}
	//--------------------------------------
	// event callback
	//--------------------------------------
	void VirtualGate::TerminationEvent_callback(h248::EventParams* eventData)
	{
		h248::Field* action = eventData->makeEvent();

		if (eventData->getType() == h248::EventParams::Notify)
			raiseNotifyEvent(action);
		else if (eventData->getType() == h248::EventParams::ServiceChange)
			raiseServiceChangeEvent(action);

		DELETEO(eventData);
	}
	//--------------------------------------
	// user interface
	//--------------------------------------
	Context* VirtualGate::createContext()
	{
		if (!m_started)
			return nullptr;

		h248::IMediaContext* context_ptr = Gate::create_context();

		if (context_ptr == nullptr)
		{
			LOG_WARN("mge-v", "%s : create context : failed : MG Engine returns null", m_vid);
			return nullptr;
		}

		Context* context = NEW Context(*this, context_ptr);

		{
			rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

			Context::addRef(context);
			m_ctxList.add(context);
		}

		LOG_CALL("mge-v", "%s : create context : done : new id %u binded to mge_context %p", m_vid, context->getId(), context_ptr);

		return context;
	}
	//--------------------------------------
	//
	//--------------------------------------
	Context* VirtualGate::findContext(uint32_t context_id)
	{
		//rtl::MutexLock lock(m_sync);
		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		ContextPtr* ptr = m_ctxList.find(context_id);

		Context* context = nullptr;

		if (ptr != nullptr && *ptr != nullptr)
		{
			context = *ptr;
			Context::addRef(context);
		}

		return context;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool VirtualGate::doActionForAll(const h248::Field* action_request, h248::Field* action_reply)
	{
		LOG_CALL("mge-v", "%s : do_action for ALL Contexts...", m_vid);

		int action_count = action_request->getFieldCount();
		bool result = true; // пустая команда

		for (int i = 0; i < action_count; i++)
		{
			// команды
			const h248::Field* command = action_request->getFieldAt(i);

			const char* value = command->getValue();

			if (command->getType() == h248::Token::Subtract &&  value != nullptr && value[0] == '*')
			{
				LOG_CALL("mge-v", "%s : reset gate", m_vid);

				action_reply->addField(h248::Token::Subtract, "*");

				stop();

				start();

				return true;
			}
		}

		LOG_CALL("mge-v", "%s : unknown request", m_vid);

		return false;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void VirtualGate::removeContext(Context* context)
	{
		{
			//rtl::MutexLock lock(m_sync);
			rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

			m_ctxList.remove(context);
		}

		LOG_CALL("mge-v", "%s : remove context %u...", m_vid, context->getId());

		// с этого момента ни один запрос не сможет найти этот контекст
		// а те кто зашел в лист ожидания после пробуждения сразу выходят
		// destroy() ждет их 

		context->destroy();

		Context::release(context);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void VirtualGate::checkContextLifetime()
	{
		/*
			нужно проверить все подвисшие контексты
			подвисшим контекс считается если статус:
				1) connected	: время жизни больше чем МАКСИМАЛЬНОЕ ВРЕМЯ ЖИЗНИ;
				2) idle, free	: отсутствуют транки в течении МАКСИМАЛЬНОЕ ВРЕМЯ НЕ В РАБОТЕ
				2) setup		: транки не скоммутированны в течении МАКСИМАЛЬНОЕ ВРЕМЯ НЕ В КОММУТАЦИИ
		*/

		if (!m_started)
			return;

		// копируем массив контекстов
		rtl::ArrayT<ContextPtr> ctx_list;

		{
			//rtl::MutexLock lock(m_sync);
			rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

			for (int i = 0; i < m_ctxList.getCount(); i++)
			{
				Context* ctx = m_ctxList.getAt(i);

				Context::addRef(ctx);

				ctx_list.add(ctx);
			}
		}

		uint32_t current_time = rtl::DateTime::getTicks();

		for (int i = 0; i < ctx_list.getCount(); i++)
		{
			Context* ctx = ctx_list.getAt(i);

			// проверка
			if (!ctx->checkLifetime(current_time))
			{
				removeContext(ctx);
			}

			// освобождаем контекс
			Context::release(ctx);
		}

		ctx_list.clear();
	}
	//--------------------------------------
	//
	//--------------------------------------
	void VirtualGate::timerProc(rtl::Timer* sender, uint32_t elapsed, void* user_data, int tid)
	{
		VirtualGate* gate = (VirtualGate*)user_data;

		if (gate->m_started)
		{
			rtl::async_call_manager_t::callAsync(asyncCallback, gate, 0, 0, nullptr);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void VirtualGate::asyncCallback(void* userdata, uint16_t call_id, uintptr_t int_param, void* ptr_param)
	{
		VirtualGate* gate = (VirtualGate*)userdata;

		gate->checkContextLifetime();
	}
}
//--------------------------------------
