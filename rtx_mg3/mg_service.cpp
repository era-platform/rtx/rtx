﻿/* RTX H.248 Media Gate
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_service.h"
#include "mg_transport_manager.h"
#include "mg_transaction_manager.h"
#include "mg_transaction_dispatcher.h"
#include "mge_context.h"
#include "rtx_codec.h"

namespace mg3
{
	//--------------------------------------
	//
	//--------------------------------------
#define ASYNC_MGC_DISCONNECTED	1001
#define ASYNC_MGC_FAILED		1002
#define ASYNC_MGC_STOPPED		1003
#define ASYNC_MGC_STARTED		1004
#define ASYNC_MGC_REDIRECT		1005
#define ASYNC_MGC_START			1006
//--------------------------------------
#define TIME_RUN_KEY "time-run-mg"
//--------------------------------------
//
//--------------------------------------
	struct mgc_async_data_t
	{
		MGC_Proxy* mgc_proxy;
		h248::Token method;
		h248::Reason code;
		char* reason;
		h248::MID* mid;
	};
	//--------------------------------------
	//
	//--------------------------------------
	rtl::MutexWatch Service::m_MGC_routeSync("mg-service");
	rtl::ArrayT<h248::EndPoint> Service::m_MGC_routeList;
	bool Service::m_started = false;
	// менеджер связки медийного шлюза
	MGC_Proxy* Service::m_MGC_mainProxy = nullptr;
	ServiceDispatcher Service::m_transportDispatcher;
	rtl::Event Service::m_raiseEvent;
	int Service::m_connectionTimeout = 500;
	int Service::m_connectionFailTimeout = 1000;
	int Service::m_pingReplyTimeout = 500;
	int Service::m_pingInterval = 1000;
	//--------------------------------------
	//
	//--------------------------------------
	static bool parseStartParams(const char* params, h248::EndPoint& point);
	//--------------------------------------
	//
	//--------------------------------------
	bool Service::startMgcProxy()
	{
		{
			//rtl::MutexLock lock(m_MGC_routeSync);
			rtl::MutexWatchLock lock(m_MGC_routeSync, __FUNCTION__);

			if (m_MGC_mainProxy != nullptr)
			{
				LOG_WARN("svc", "MGC proxy already started!");
				return false;
			}
		}

		LOG_CALL("svc", "start MGC proxy...");

		MGC_Proxy* proxy = NEW MGC_Proxy;

		if (!proxy->initialize(m_MGC_routeList))
		{
			DELETEO(proxy);
			LOG_WARN("svc", "start MGC proxy : failed : initialization failed!");
			return false;
		}

		if (proxy->start())
		{
			LOG_CALL("svc", "start MGC proxy : started...");

			{
				//rtl::MutexLock lock(m_MGC_routeSync);
				rtl::MutexWatchLock lock(m_MGC_routeSync, __FUNCTION__);

				m_MGC_mainProxy = proxy;
			}

			return true;
		}

		proxy->destroy();
		DELETEO(proxy);

		LOG_WARN("svc", "start MGC proxy : failed : start failed!");

		return false;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Service::stopMgcProxy()
	{
		LOG_CALL("svc", "stop mgc_proxy...");

		MGC_Proxy* proxy = nullptr;

		{
			//rtl::MutexLock lock(m_MGC_routeSync);
			rtl::MutexWatchLock lock(m_MGC_routeSync, __FUNCTION__);

			proxy = m_MGC_mainProxy;

			m_MGC_mainProxy = nullptr;
		}

		if (proxy != nullptr)
		{
			proxy->stop();
			proxy->destroy();
			DELETEO(proxy);
		}

		LOG_CALL("svc", "stop mgc_proxy : done");
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Service::isKnownMgc(const h248::EndPoint& mgc_address)
	{
		//rtl::MutexLock lock(m_MGC_routeSync);
		rtl::MutexWatchLock lock(m_MGC_routeSync, __FUNCTION__);

		for (int i = 0; i < m_MGC_routeList.getCount(); i++)
		{
			if (m_MGC_routeList[i].address == mgc_address.address)
				return true;
		}

		return false;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Service::isActiveMgc(const h248::EndPoint& mgc_address)
	{
		//rtl::MutexLock lock(m_MGC_routeSync);
		rtl::MutexWatchLock lock(m_MGC_routeSync, __FUNCTION__);

		return (m_MGC_mainProxy != nullptr && mgc_address.address == m_MGC_mainProxy->getRoute().remoteAddress);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Service::initializeOutputMode()
	{
		h248::MessageWriterParams write_params = { false, false, 0, 0 };

		megaco_get_output_mode(&write_params);

		const char* cfg_value = Config.get_config_value(CONF_MEGACO_MESSAGE_PRETTY);
		if (cfg_value != nullptr && cfg_value[0] != 0)
		{
			write_params.human_readable = std_strnicmp(cfg_value, "true", 4) == 0;

			h248::setTokenNameMode(write_params.human_readable);
		}

		cfg_value = Config.get_config_value(CONF_MEGACO_MESSAGE_PRETTY_INDENT);
		if (cfg_value != nullptr && cfg_value[0] != 0)
		{
			write_params.spaces_count = strtoul(cfg_value, nullptr, 10);
		}

		cfg_value = Config.get_config_value(CONF_MEGACO_MESSAGE_PRETTY_INDENT_TAB);
		if (cfg_value != nullptr && cfg_value[0] != 0)
		{
			write_params.use_tab = std_strnicmp(cfg_value, "true", 4) == 0;
		}

		megaco_set_output_mode(&write_params);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Service::intializeLifespanTimes()
	{
		const char* cfg_value = Config.get_config_value(CONF_MEGACO_CONTEXT_LIFESPAN);
		if (cfg_value != nullptr && cfg_value[0] != 0)
		{
			Context::setLifespan(strtoul(cfg_value, nullptr, 10));
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Service::initializeParameters()
	{
		const char* cfg_value = Config.get_config_value(CONF_MG_CONNECTION_TIMEOUT);
		if (cfg_value != nullptr && cfg_value[0] != 0)
		{
			m_connectionTimeout = strtoul(cfg_value, nullptr, 10);

			// от 0,5 до 3 секунд
			if (m_connectionTimeout < 500)
				m_connectionTimeout = 500;
			else if (m_connectionTimeout > 3000)
				m_connectionTimeout = 3000;
		}

		cfg_value = Config.get_config_value(CONF_MG_CONNECTING_FAULT_TIMEOUT);
		if (cfg_value != nullptr && cfg_value[0] != 0)
		{
			m_connectionFailTimeout = strtoul(cfg_value, nullptr, 10);

			// от нуля до 5 сек
			if (m_connectionFailTimeout < 0 || m_connectionFailTimeout > 5000)
				m_connectionFailTimeout = 5000;
		}

		cfg_value = Config.get_config_value(CONF_MG_PING_REPLY_TIMEOUT);
		if (cfg_value != nullptr && cfg_value[0] != 0)
		{
			m_pingReplyTimeout = strtoul(cfg_value, nullptr, 10);

			// от 0.1 сек до 5 сек
			if (m_pingReplyTimeout < 100)
				m_pingReplyTimeout = 100;
			else if (m_pingReplyTimeout > 5000)
				m_pingReplyTimeout = 5000;
		}

		cfg_value = Config.get_config_value(CONF_MG_PING_INTERVAL);
		if (cfg_value != nullptr && cfg_value[0] != 0)
		{
			m_pingInterval = strtoul(cfg_value, nullptr, 10);

			// от 0,5 сек до 10 мин
			if (m_pingInterval < 500)
				m_pingInterval = 500;
			else if (m_pingInterval > 10000 * 60)
				m_pingInterval = 10000 * 60;
		}

		LOG_CALL("mg3",
			"\n\tconnect_timeout       : %d\n\
\tconnect_fail_timeout : %d\n\
\tping_reply_timeout   : %d\n\
\tping_interval        : %d\n", m_connectionTimeout, m_connectionFailTimeout, m_pingReplyTimeout, m_pingInterval);

		printf("\n\tconnect_timeout       : %d\n\
\tconnect_fail_timeout : %d\n\
\tping_reply_timeout   : %d\n\
\tping_interval        : %d\n", m_connectionTimeout, m_connectionFailTimeout, m_pingReplyTimeout, m_pingInterval);

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Service::loadMgcList()
	{
		// initialize list of known controllers
		const char* cfg_value = Config.get_config_value(CONF_MGC_LIST);

		if (cfg_value == nullptr || cfg_value[0] == 0)
			return false;

		//rtl::MutexLock lock(m_MGC_routeSync);
		rtl::MutexWatchLock lock(m_MGC_routeSync, __FUNCTION__);

		LOG_CALL("svc", "starting megaco service : read known mgc -> %s", cfg_value);

		char buffer[32];
		const char* token = cfg_value;
		const char* delim;
		h248::EndPoint mgc_point;

		while ((delim = strchr(token, ',')) != nullptr)
		{
			int to_copy = MIN(31, PTR_DIFF(delim, token));
			strncpy(buffer, token, to_copy);
			buffer[to_copy] = 0;

			if (parseStartParams(buffer, mgc_point))
			{
				m_MGC_routeList.add(mgc_point);
			}

			token = delim + 1;
		}

		strncpy(buffer, token, 31);
		buffer[31] = 0;

		if (parseStartParams(buffer, mgc_point))
		{
			m_MGC_routeList.add(mgc_point);
		}

		LOG_CALL("svc", "starting megaco service : %d mgc added ", m_MGC_routeList.getCount());

		if (m_MGC_routeList.getCount() == 0)
		{
			LOG_ERROR("svc", "no valid MGC address in the list!");
			return false;
		}

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Service::initializeMediaEngineModule()
	{
		const char* cfg_value = Config.get_config_value(CONF_MG_MEDIA_GATE_LIB);

		if (cfg_value == nullptr || cfg_value[0] == 0)
		{
			LOG_CALL("svc", "starting megaco service : failed : MediaGate module path not defined!");
			return false;
		}

		if (!Gate::create(cfg_value, g_log_path_buffer))
		{
			LOG_CALL("svc", "starting megaco service : failed : MediaGate module '%s' not loaded!", cfg_value);
			return false;
		}

		int prefix_count = 10;

		cfg_value = Config.get_config_value(CONF_MG_TERM_PREFIX_COUNT);

		if (cfg_value != nullptr && cfg_value[0] != 0)
		{
			prefix_count = strtoul(cfg_value, nullptr, 10);
		}

		// выставляем интерфейсы
		char cfg_name[128];

		for (int i = 1; i <= prefix_count; i++)
		{
			int len = std_snprintf(cfg_name, 128, CONF_MG_TERM_PREFIX_MASK, i);
			cfg_name[len] = 0;
			cfg_value = Config.get_config_value(cfg_name);

			if (cfg_value != nullptr && cfg_value[0] != 0)
			{
				// формат: prefix ' ' ipv4_address | ipv6_address
				char prefix[128];
				char ip_address[128];

				const char* cp = strpbrk(cfg_value, " \t");

				if (cp == nullptr)
					continue;

				int to_copy = MIN(PTR_DIFF(cp, cfg_value), 127);
				strncpy(prefix, cfg_value, to_copy);
				prefix[to_copy] = 0;
				rtl::strtrim(prefix, " \t");

				to_copy = MIN(std_strlen(cp + 1), 127);

				strncpy(ip_address, cp + 1, to_copy);
				ip_address[to_copy] = 0;
				rtl::strtrim(ip_address, " \t");

				LOG_CALL("svc", "set interface prefix to %s -> %s", ip_address, prefix);

				Gate::set_interface_prefix(ip_address, prefix);
			}
		}

		int port_count = 10;

		// проверим наборы портов
		cfg_value = Config.get_config_value(CONF_MG_TERM_RTP_RANGE_COUNT);

		if (cfg_value != nullptr && cfg_value[0] != 0)
		{
			port_count = strtoul(cfg_value, nullptr, 10);
		}

		for (int i = 1; i <= port_count; i++)
		{
			int len = std_snprintf(cfg_name, 128, CONF_MG_TERM_RTP_RANGE_MASK, i);
			cfg_name[len] = 0;
			cfg_value = Config.get_config_value(cfg_name);

			if (cfg_value != nullptr && cfg_value[0] != 0)
			{
				// формат: ipv4_address | ipv6_address ' ' port_begin ':' port_end
				char prefix[128];
				uint16_t port_begin;
				uint16_t port_count;

				const char* cp = strpbrk(cfg_value, " \t");

				if (cp == nullptr)
					continue;

				int to_copy = MIN(PTR_DIFF(cp, cfg_value), 127);
				strncpy(prefix, cfg_value, to_copy);
				prefix[to_copy] = 0;
				rtl::strtrim(prefix, " \t");

				cp = rtl::skip_SEP(cp);

				port_begin = (uint16_t)strtoul(cp, nullptr, 10);

				cp = strchr(cp, ':');

				if (cp == nullptr)
					continue;

				cp = rtl::skip_SEP(cp + 1);

				port_count = (uint16_t)strtoul(cp, nullptr, 10);

				if (prefix[0] == 0 || port_begin == 0 || port_count == 0)
					continue;

				LOG_CALL("svc", "set rtp port range %s -> %u:%u", prefix, port_begin, port_count);

				Gate::add_rtp_interface(prefix, port_begin, port_count);
			}
		}

		rtl::String list = rtx_codec__get_codec_list();

		printf("default codec list: %s\r\n", (const char*)list);
		LOG_CALL("svc", "default codec list: %s\r\n", (const char*)list);

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Service::initializeTransportManager()
	{
		// добавление слушателей по умолчанию
		h248::TransportManager::create(&m_transportDispatcher);

		uint16_t megaco_default_port = 0;// MEGACO_DEFAULT_TEXT_PORT;
		in_addr megaco_default_interface = { 0 };

		const char* cfg_value = Config.get_config_value(CONF_MEGACO_DEF_TEXT_PORT);
		if (cfg_value != nullptr && cfg_value[0] != 0)
		{
			megaco_default_port = (uint16_t)strtoul(cfg_value, nullptr, 10);
			/*if (megaco_default_port == 0)
			{
				megaco_default_port = MEGACO_DEFAULT_TEXT_PORT;
			}*/
		}

		cfg_value = Config.get_config_value(CONF_MEGACO_DEF_TEXT_INTERFACE);
		if (cfg_value != nullptr && cfg_value[0] != 0)
		{
			megaco_default_interface.s_addr = inet_addr(cfg_value);

			if (megaco_default_interface.s_addr == INADDR_NONE)
			{
				megaco_default_interface.s_addr = INADDR_ANY;
			}
		}

		LOG_CALL("svc", "starting megaco service : start default transport listeners on %s:%u...", inet_ntoa(megaco_default_interface), megaco_default_port);

		if (megaco_default_port != 0)
		{
			h248::EndPoint listen_point = { h248::TransportType::TCP, megaco_default_interface, megaco_default_port };
			h248::TransportManager::addListener(listen_point);
			listen_point.type = h248::TransportType::UDP;
			h248::TransportManager::addListener(listen_point);
		}

		// читаем свойства mid для исходящих пакетов
		LOG_CALL("svc", "starting megaco service : start transaction manager...");
		cfg_value = Config.get_config_value(CONF_MG_SERV_MID_TYPE);

		h248::mg_out_mid_type_t mid_type = h248::mg_out_mid_ipv4_e;

		const char* mid_domain = nullptr;

		LOG_CALL("svc", "starting megaco service : start transaction manager step 1");

		if (cfg_value != nullptr && cfg_value[0] != 0)
		{
			mid_type = h248::parse_mg_out_mid_type(cfg_value);

			LOG_CALL("svc", "starting megaco service : start transaction manager step 2");
			if (mid_type == h248::mg_out_mid_domain_e)
			{
				mid_domain = Config.get_config_value(CONF_MG_SERV_MID_DOMAIN);

				LOG_CALL("svc", "starting megaco service : start transaction manager step 3");
				if (mid_domain == nullptr || mid_domain[0] == 0)
				{
					mid_type = h248::mg_out_mid_ipv4_e;
				}
			}
			else if (mid_type == h248::mg_out_mid_unknown_e)
			{
				mid_type = h248::mg_out_mid_ipv4_e;
			}

			LOG_CALL("svc", "starting megaco service : start transaction manager step 4");
		}

		LOG_CALL("svc", "starting megaco service : start transaction manager step 5");

		return h248::TransactionManager::create(mid_type, mid_domain);
	}
	//--------------------------------------
	// async call handler
	//--------------------------------------
	void Service::asyncCallHandler(void* userdata, uint16_t call_id, uintptr_t int_param, void* ptr_param)
	{
		switch (call_id)
		{
		case ASYNC_MGC_STARTED:
			asyncProxyStarted((MGC_Proxy*)ptr_param);
			break;
		case ASYNC_MGC_STOPPED:
		{
			mgc_async_data_t* data = (mgc_async_data_t*)ptr_param;

			asyncProxyStopped(data->mgc_proxy, data->method, data->code, data->reason);

			FREE(data->reason);
			DELETEO(data);
		}
		break;
		case ASYNC_MGC_REDIRECT:
		{
			mgc_async_data_t* data = (mgc_async_data_t*)ptr_param;

			asyncProxyRedirect(data->mgc_proxy, data->method, data->code, data->reason, *data->mid);

			FREE(data->reason);
			DELETEO(data->mid);
			DELETEO(data);
		}
		case ASYNC_MGC_FAILED:
			asyncProxyFailed((MGC_Proxy*)ptr_param);
			break;
		case ASYNC_MGC_DISCONNECTED:
			asyncProxyDisconnected((MGC_Proxy*)ptr_param);
			break;
		}
	}
	//--------------------------------------
	// async call handler
	//--------------------------------------
	void Service::asyncProxyStarted(MGC_Proxy* mgc_proxy)
	{
		LOG_EVENT("svc", "MGC(%u) proxy STARTED", mgc_proxy->getId());
		// выставляем 
		//rtl::MutexLock lock(m_MGC_routeSync);
		//m_MGC_mainProxy = mgc_proxy;
	}
	//--------------------------------------
	// async call handler
	//--------------------------------------
	void Service::asyncProxyStopped(MGC_Proxy* mgc_proxy, h248::Token method, h248::Reason code, const char* reason)
	{
		// ...
		LOG_EVENT("svc", "MGC(%u) proxy STOPPED", mgc_proxy->getId());

		m_raiseEvent.signal();
	}
	//--------------------------------------
	// async call handler
	//--------------------------------------
	void Service::asyncProxyRedirect(MGC_Proxy* mgc_proxy, h248::Token method, h248::Reason code, const char* reason, const h248::MID& mid)
	{
		LOG_EVENT("svc", "MGC(%u) proxy REDIRECT", mgc_proxy->getId());

		m_raiseEvent.signal();
	}
	//--------------------------------------
	// async call handler
	//--------------------------------------
	void Service::asyncProxyDisconnected(MGC_Proxy* mgc_proxy)
	{
		LOG_EVENT("svc", "MGC(%u) proxy DISCONNECTED", mgc_proxy->getId());

		m_raiseEvent.signal();
	}
	//--------------------------------------
	// async call handler
	//--------------------------------------
	void Service::asyncProxyFailed(MGC_Proxy* mgc_proxy)
	{
		LOG_EVENT("svc", "MGC(%u) proxy FAILED", mgc_proxy->getId());

		m_raiseEvent.signal();
	}
	//--------------------------------------
	// start service and sending first ServiceChanged request to primary mgc
	//--------------------------------------
	bool Service::start()
	{
		try
		{
			LOG_CALL("svc", "starting megaco service...");

			{
				//rtl::MutexLock lock(m_MGC_routeSync);
				rtl::MutexWatchLock lock(m_MGC_routeSync, __FUNCTION__);

				if (m_started)
				{
					LOG_CALL("svc", "megaco service already started");
					return true;
				}

				m_started = true;
			}

			m_raiseEvent.create(true, false);

			initializeOutputMode();

			if (!initializeMediaEngineModule())
			{
				return false;
			}

			if (!initializeTransportManager())
			{
				return false;
			}

			initializeParameters();

			// initialize list of known controllers
			if (!loadMgcList())
			{
				return false;
			}

			intializeLifespanTimes();

			// load and start mg controllers
			// стартуем
			LOG_CALL("svc", "starting megaco service : start  mgc proxy...");

			if (!startMgcProxy())
			{
				return false;
			}

			LOG_CALL("svc", "megaco service started");
		}
		catch (rtl::Exception& ex)
		{
			printf("starting megaco service : failed : exception catched %s\n", ex.get_message());
			return false;
		}

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Service::stop()
	{
		LOG_CALL("svc", "megaco service stopping...");

		{
			//rtl::MutexLock lock(m_MGC_routeSync);
			rtl::MutexWatchLock lock(m_MGC_routeSync, __FUNCTION__);

			if (!m_started)
			{
				LOG_CALL("svc", "megaco service already stoped");
				return true;
			}

			m_started = false;
		}

		// stopping

		LOG_CALL("svc", "megaco service stopping : stopping transport manager");
		h248::TransportManager::stop();

		LOG_CALL("svc", "megaco service stopping : stoping transaction managment");
		h248::TransactionManager::stop();

		//// destroying

		LOG_CALL("svc", "megaco service stopping : destroy transport manager");
		h248::TransportManager::destroy();

		LOG_CALL("svc", "megaco service stopping : destroy transaction managment");
		h248::TransactionManager::destroy();

		LOG_CALL("svc", "megaco service stopping : stop & destroy mg mgc_proxy proxies");
		stopMgcProxy();

		LOG_CALL("svc", "megaco service stopping : unload MediaGate module");
		Gate::destroy();

		LOG_CALL("svc", "megaco service stopped");

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Service::restart()
	{
		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Service::run()
	{
		const char* time_run_char = Config.get_config_value(TIME_RUN_KEY);
		if ((time_run_char != nullptr) && (time_run_char[0] != 0))
		{
			int time_run = strtoul(time_run_char, nullptr, 10);
			if (time_run > 0)
			{
				return m_raiseEvent.wait(time_run * 60 * 1000);
			}
		}

		return m_raiseEvent.wait(INFINITE);
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Service::addPortRangeRTP(const char* iface, uint16_t range_start, uint16_t range_size)
	{
		return Gate::add_rtp_interface(iface, range_start, range_size);
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Service::removePortRangeRTP(const char* iface)
	{
		Gate::free_rtp_interface(iface);
		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Service::proxyStopped(MGC_Proxy* mgc_proxy, h248::Token method, h248::Reason code, const char* reason)
	{
		// пока не ясно. по идее останов сервиса без обхода списка
		mgc_async_data_t* data = NEW mgc_async_data_t;

		data->mgc_proxy = mgc_proxy;
		data->method = method;
		data->code = code;
		data->reason = rtl::strdup(reason);
		data->mid = nullptr;

		h248::MegacoAsyncCall(asyncCallHandler, nullptr, ASYNC_MGC_STOPPED, 0, data);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Service::proxyRedirect(MGC_Proxy* mgc_proxy, h248::Token method, h248::Reason code, const char* reason, const h248::MID& mid)
	{
		mgc_async_data_t* data = NEW mgc_async_data_t;

		data->mgc_proxy = mgc_proxy;
		data->method = method;
		data->code = code;
		data->reason = rtl::strdup(reason);
		data->mid = NEW h248::MID(mid);

		h248::MegacoAsyncCall(asyncCallHandler, nullptr, ASYNC_MGC_REDIRECT, 0, data);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Service::proxyDisconnected(MGC_Proxy* mgc_proxy)
	{
		// подключение к следующему MGC
		// и если нет то останов сервиса
		h248::MegacoAsyncCall(asyncCallHandler, nullptr, ASYNC_MGC_DISCONNECTED, 0, mgc_proxy);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Service::proxyFailed(MGC_Proxy* mgc_proxy)
	{
		// подключение к следующему MGC
		// и если нет то продолжаем подключатся до победного конца
		MegacoAsyncCall(asyncCallHandler, nullptr, ASYNC_MGC_FAILED, 0, mgc_proxy);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Service::TransportUser_disconnected(const h248::Route& route)
	{
		LOG_EVENT("svc", "transport disconnected : search related mgc...");
		// проверка и отмена транзакций пришедших с этого транспорта
		h248::EndPoint mgc_address = { route.type, route.remoteAddress, route.remotePort };

		if (isActiveMgc(mgc_address))
		{
			LOG_EVENT("svc", "transport disconnected : mgc found");
			m_MGC_mainProxy->dispatch().handle_Disconnected(route);
		}
		else
		{
			LOG_EVENT("svc", "transport disconnected : related mgc not found");
		}
	}
	//--------------------------------------
	// полученно сообщение
	//--------------------------------------
	void Service::TransportUser_packetArrival(const h248::Route& route, h248::Message* message)
	{
		LOG_EVENT("svc", "transport packet arrival...");

		h248::EndPoint mgc_address = { route.type, route.remoteAddress, route.remotePort };

		if (isActiveMgc(mgc_address))
		{
			LOG_EVENT("svc", "transport packet arrival : related mgc found -- pass to it...");

			m_MGC_mainProxy->dispatch().handle_PacketArrival(message, route);

			return;
		}

		LOG_EVENT("svc", "transport packet arrival : from unknown host -- drop...");
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Service::TransportUser_netFailure(const h248::Route& route, int net_error)
	{
		LOG_EVENT("svc", "transport net failure %d", net_error);

		// проверка и отмена транзакций пришедших с этого транспорта
		h248::EndPoint mgc_address = { route.type, route.remoteAddress, route.remotePort };

		if (isActiveMgc(mgc_address))
		{
			m_MGC_mainProxy->dispatch().handle_NetFailureEvent(net_error, route);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Service::TransportUser_parserError(const h248::Route& route)
	{
		LOG_EVENT("svc", "transport parser error...");

		// возможно нужно просто пропускать сломанный пакет?
		// m_transaction_manager.check_failed_transport(sender->getRoute());
		// проверка и отмена транзакций пришедших с этого транспорта
		h248::EndPoint mgc_address = { route.type, route.remoteAddress, route.remotePort };

		if (isActiveMgc(mgc_address))
		{
			m_MGC_mainProxy->dispatch().handle_ParserErrorEvent(route);
		}
	}
	//--------------------------------------
	// params format: [tcp/def:udp] address[:port(def:2944)]
	//--------------------------------------
	static bool parseStartParams(const char* params, h248::EndPoint& point)
	{
		memset(&point, 0, sizeof(point));

		if (params == nullptr || params[0] == 0)
		{
			return false;
		}

		// значения по умолчанию
		point.type = h248::TransportType::UDP;		// UDP
		point.port = MEGACO_DEFAULT_TEXT_PORT;	// 2944
		point.address.s_addr = INADDR_NONE;		// 255.255.255.255

		const char* cp = params, *next;

		// skip leading spaces
		while (*cp != 0 && (*cp == ' ' || *cp == '\t')) cp++;

		if ((next = strchr(cp, ' ')) != nullptr)
		{
			// check for transport type
			if (std_strnicmp(cp, "tcp", 3) == 0)
			{
				point.type = h248::TransportType::TCP;
				cp += 3;
			}
			else if (std_strnicmp(cp, "udp", 3) == 0)
			{
				point.type = h248::TransportType::UDP;
				cp += 3;
			}
			else
			{
				return false;
			}
		}

		// skip leading spaces
		while (*cp != 0 && (*cp == ' ' || *cp == '\t')) cp++;

		// parse address and port
		const char* sport = strpbrk(cp, ":,");

		if (sport != nullptr && *sport == ':')
		{
			char buffer[32];
			strncpy(buffer, cp, sport - cp);
			buffer[sport - cp] = 0;

			if (inet_pton(AF_INET, buffer, &point.address) != 1)
			{
				return false;
			}

			point.port = (uint16_t)strtoul(sport + 1, nullptr, 10);
		}
		else
		{
			point.port = MEGACO_DEFAULT_TEXT_PORT;
			if (inet_pton(AF_INET, cp, &point.address) != 1)
			{
				return false;
			}
		}

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void ServiceDispatcher::TransportUser_disconnected(const h248::Route& route)
	{
		Service::TransportUser_disconnected(route);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void ServiceDispatcher::TransportUser_packetArrival(const h248::Route& route, h248::Message* message)
	{
		Service::TransportUser_packetArrival(route, message);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void ServiceDispatcher::TransportUser_netFailure(const h248::Route& route, int net_error)
	{
		Service::TransportUser_netFailure(route, net_error);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void ServiceDispatcher::TransportUser_parserError(const h248::Route& route)
	{
		Service::TransportUser_parserError(route);
	}
}
//--------------------------------------
