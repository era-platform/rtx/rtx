﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

//--------------------------------------
// Traversal Using Relays around NAT (TURN) messages.
//--------------------------------------

//--------------------------------------
// TURN channel data message as per draft-ietf-behave-turn-16 subclause 11.4.
//--------------------------------------
/*	draft-ietf-behave-turn-16 11.4.  The ChannelData Message
	0                   1                   2                   3
	0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	|         Channel Number        |            Length             |
	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	|                                                               |
	/                       Application Data                        /
	/                                                               /
	|                                                               |
	|                               +-------------------------------+
	|                               |
	+-------------------------------+
*/
class net_turn_channel_data_t
{
	uint16_t m_number;
	uint16_t m_length;
	void* m_data;

public:
	net_turn_channel_data_t() : m_number(0), m_length(0), m_data(NULL) { }
	net_turn_channel_data_t(uint16_t number, uint16_t length, const void* data);
	~net_turn_channel_data_t();
	
	uint16_t get_number() const { return m_number; }
	uint16_t getLength() const { return m_length; }
	const void* get_data() const { return m_data; }

	int serialize(rtl::MemoryStream& stream);
};
//--------------------------------------
//
//--------------------------------------
//int net_turn_channel_data_serialize(const net_turn_channel_data_t *message, uint8_t* buffer, int size);
//--------------------------------------
//
//--------------------------------------
//net_turn_channel_data_t* net_turn_channel_data_create(uint16_t number, uint16_t length, const void* data);
//net_turn_channel_data_t* net_turn_channel_data_create_null();
//void net_turn_channel_data_destroy(net_turn_channel_data_t* message);
//--------------------------------------
