﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "net/tcp_header.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
tcp_header::tcp_header()
{
	reset();
}
tcp_header::~tcp_header()
{
}
//------------------------------------------------------------------------------
// распаковка tcp из буфера.
//------------------------------------------------------------------------------
bool					tcp_header::read(const void* data, uint32_t length)
{
	if ((data == NULL) || (length == 0))
	{
		return false;
	}

	if (length < FIXED_TCP_HEADER_LENGTH)
	{
		return false;
	}
	
	return true;
}
//------------------------------------------------------------------------------
// упаковка tcp в буфер.
//------------------------------------------------------------------------------
uint32_t				tcp_header::write(void* data, uint32_t size) const
{
	if ((data == NULL) || (size == 0))
	{
		return 0;
	}

	if (size < FIXED_TCP_HEADER_LENGTH)
	{
		return 0;
	}

	//uint8_t* pdata = (uint8_t*)data;

	
	return 0;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void					tcp_header::reset()
{
	
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
