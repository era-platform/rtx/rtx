﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "net_nat_ctx.h"
#include "net_turn.h"
#include "net_stun_message.h"
#include "net_turn_message.h"
//--------------------------------------
//  TURN(draft-ietf-behave-turn-16) implementation.
//--------------------------------------
net_turn_channel_binding_t* net_turn_channel_binding_create(const net_turn_allocation_t *allocation)
{
	net_turn_channel_binding_t *channel_binding = NEW net_turn_channel_binding_t;

	static net_turn_channel_binding_id_t __allocation_unique_id = 0x4000; /* 0x4000 through 0x7FFF */		
		
	channel_binding->id = __allocation_unique_id++;
	channel_binding->allocation = allocation;
	channel_binding->timeout = NET_TURN_CHANBIND_TIMEOUT_DEFAULT; /* 10 minutes as per draft-ietf-behave-turn-16 subclause 11 */

	if (__allocation_unique_id >= 0x7FFF)
	{
		__allocation_unique_id = 0x4000;
	}

	return channel_binding;
}
//--------------------------------------
//
//--------------------------------------
net_turn_permission_t* net_turn_permission_create(uint32_t timeout)
{
	net_turn_permission_t *permission = NEW net_turn_permission_t;

	permission->timeout = timeout;

	return permission;
}
//--------------------------------------
//
//--------------------------------------
net_turn_allocation_t::net_turn_allocation_t(rtl::Logger* log) : m_log(log)
{
	static net_turn_allocation_id_t __allocation_unique_id = 0;

	m_id = ++__allocation_unique_id;

	m_context = nullptr;
	m_localFD = nullptr;

	m_username = nullptr;
	m_password = nullptr;

	m_server_address.parse("");
	m_server_port = 0;
	
	m_timeout = 600;

	m_relay_address = nullptr;
	memset(&m_maddr, 0, sizeof m_maddr);
	memset(&m_xmaddr, 0, sizeof m_xmaddr);
	

	/// the authentication information
	m_realm = nullptr;
	m_nonce = nullptr;
	m_software = nullptr;
}
//--------------------------------------
//
//--------------------------------------
net_turn_allocation_t::~net_turn_allocation_t()
{ 
	FREE(m_relay_address);
	FREE(m_username);
	FREE(m_password);
	FREE(m_realm);
	FREE(m_nonce);
	FREE(m_software);
}
//--------------------------------------
//
//--------------------------------------
void net_turn_allocation_t::intitalize(const net_nat_context_t* context, net_socket* fd, const ip_address_t* server_address, uint16_t server_port, const char* username, const char* password)
{
	static net_turn_allocation_id_t __allocation_unique_id = 0;

	m_id = ++__allocation_unique_id;

	m_context = context;
	m_localFD = fd;

	m_username = rtl::strdup(username);
	m_password = rtl::strdup(password);

	m_server_address.copy_from(server_address);
	m_server_port = server_port; //tnet_sockaddr_init(server_address, server_port, allocation->socket_type, &allocation->server);
	
	m_timeout = 600;

	m_relay_address = nullptr;
	memset(&m_maddr, 0, sizeof m_maddr);
	memset(&m_xmaddr, 0, sizeof m_xmaddr);
	

	/// the authentication information
	m_realm = nullptr;
	m_nonce = nullptr;
	m_software = nullptr;
}
//--------------------------------------
// - IMPORTANT: 16.  Detailed Example
// - It is suggested that the client refresh the allocation roughly 1 minute before it expires.
// - If the client wishes to immediately delete an existing allocation, it includes a LIFETIME attribute with a value of 0.
//--------------------------------------
typedef net_stun_request_t* (*net_turn_create_request_func)(const net_nat_context_t* context, net_turn_allocation_t* allocation, va_list *app);
//--------------------------------------
//
//--------------------------------------
net_stun_request_t* net_turn_allocation_t::create_request(net_stun_message_type_t type) const
{
	net_stun_request_t *request = NEW net_stun_message_t(type, m_context->get_username(), m_context->get_password());

	if (request)
	{
		request->set_flags(m_context->get_fingerprint(), m_context->get_integrity(), m_context->get_dontfrag(), false);
		request->set_realm(m_realm);
		request->set_nonce(m_nonce);

		/* Create random transaction id */
		request->generate_transaction_id();

		/* Add software attribute */
		if (m_software != nullptr && m_software[0] != 0)
		{
			request->add_attribute_data(net_stun_software, m_software, int(strlen(m_software)));
		}
	}

	return request;
}
//--------------------------------------
//
//--------------------------------------
net_stun_request_t* net_turn_allocation_t::create_request_allocate() const
{
	net_stun_request_t* request =  create_request(net_stun_allocate_request);

	if(request)
	{
		/* Add Requested transport. */
		request->add_attribute_int8(net_stun_requested_transport, IPPROTO_UDP);
		/* Add lifetime */
		request->add_attribute_int32(net_stun_lifetime, m_timeout);
		/* Add Event Port */
		request->add_attribute_int8(net_stun_even_port, m_context->get_evenport() ? 1 : 0);
	}

	return request;
}
//--------------------------------------
//
//--------------------------------------
net_stun_request_t* net_turn_allocation_t::create_request_refresh() const
{
	net_stun_request_t *request = create_request_allocate();
	
	if (request)
	{
		request->set_type(net_stun_refresh_request);
	}

	return request;
}
//--------------------------------------
//
//--------------------------------------
net_stun_request_t* net_turn_allocation_t::create_request_unallocate() const
{
	net_stun_request_t *request = create_request_refresh();
	
	if (request)
	{
		request->set_lifetime(0);
	}

	return request;
}
//--------------------------------------
//
//--------------------------------------
net_stun_request_t* net_turn_allocation_t::create_request_channel_bind(const net_turn_channel_binding_t* channel_binding) const
{
	net_stun_request_t* request = create_request(net_stun_channelbind_request);
	
	if (request)
	{
		net_turn_channel_binding_id_t number = htons(channel_binding->id);
		uint32_t lifetime = htonl(channel_binding->timeout);

		/* XOR-PEER */
		request->add_attribute_address(net_stun_xor_peer_address, channel_binding->xpeer.port, channel_binding->xpeer.ipv4);

		/* CHANNEL-NUMBER */
		request->add_attribute_int16(net_stun_channel_number, number);

		/* LIFETIME */
		request->set_lifetime(lifetime);
	}

	return request;
}
////--------------------------------------
////
////--------------------------------------
//net_stun_request_t* net_turn_create_request_channel_refresh(const net_nat_context_t* context, net_turn_allocation_t* allocation, va_list *app)
//{
//	return net_turn_create_request_channel_bind(context, allocation, app);
//}
//--------------------------------------
//
//--------------------------------------
net_stun_request_t* net_turn_allocation_t::create_request_sendindication(const net_stun_address_t* xpeer, const void* data, int size) const
{
	net_stun_request_t* request =  create_request(net_stun_send_indication);

	if (request)
	{
		/*
			draft-ietf-behave-turn-16 - 10.1.  Forming a Send Indication

			When forming a Send indication, the client MUST include a XOR-PEER-
			ADDRESS attribute and a DATA attribute.  The XOR-PEER-ADDRESS
			attribute contains the transport address of the peer to which the
			data is to be sent, and the DATA attribute contains the actual
			application data to be sent to the peer.
		*/

		/* XOR-PEER-ADDRESS */
		request->add_attribute_address(net_stun_xor_peer_address, xpeer->port, xpeer->ipv4);

		/* DATA */
		request->add_attribute_data(net_stun_data, data, size);
	}

	return request;
}
//--------------------------------------
//
//--------------------------------------
net_stun_request_t* net_turn_allocation_t::create_request_permission(const ip_address_t* ipv4, uint32_t timeout) const
{
	net_stun_request_t* request = create_request(net_stun_createpermission_request);

	if (request)
	{
		//--const char* ipaddress = va_arg(*app, const char *);

		/* XOR-PEER-ADDRESS */
		in_addr addr;
		addr.s_addr = 0x4783A179;
		
		request->add_attribute_address(net_stun_xor_peer_address, 0, addr);
	}

	return request;
}
//--------------------------------------
//
//--------------------------------------
int net_turn_allocation_t::send_request(net_stun_message_t* request)
{
	int ret = -1;

	if (request)
	{
		//if (m_socket_type) // used only over UDP
		//{
			net_stun_response_t *response = net_stun_send_unreliably(m_localFD, 500, 7, request, &m_server_address, m_server_port);

			if (response)
			{
				if (NET_STUN_RESPONSE_IS_ERROR(response->getType()))
				{
					short code = response->get_errorcode();
					const char* realm = response->get_realm();
					const char* nonce = response->get_nonce();

					if (code == 401 && realm && nonce)
					{
						if (m_nonce == nullptr || m_nonce[0] == 0)
						{
							/* First time we get a nonce */
							set_nonce(nonce);
							set_realm(realm);
							
							/* Delete the message and response before retrying*/
							DELETEO(response);
							DELETEO(request);

							// Send again using new transaction identifier
							request = update_request(request);
							return send_request(request);
						}
						else
						{
							ret = -3;
						}
					}
					else
					{
						PLOG_NET_ERROR("net-turn", "Server error code: %d", code);
						ret = -2;
					}
				}
				else /* Any (allocate, permission, channel binding ...) success response */
				{
					response->set_type(net_stun_allocate_success_response);
					if (response->getType() != 0) /* Allocate success response */
					{
						/* LifeTime */
						{
							int32_t lifetime = response->get_lifetime();
							if (lifetime >= 0)
							{
								m_timeout = lifetime;
							}
						}
						/* STUN mapped or xmapped address */
						{
							const net_stun_attribute_t& xmaddr = response->get_attribute(net_stun_xor_mapped_address);
							if (xmaddr.type == net_stun_xor_mapped_address)
							{
								set_xmapped_address(xmaddr.address);
							}
							else 
							{
								const net_stun_attribute_t& maddr = response->get_attribute(net_stun_mapped_address);
								if (maddr.type == net_stun_mapped_address)
								{
									set_mapped_address(maddr.address);
								}
							}
						}
					}

					/* Set ret to zero (success) */
					ret = 0;
				}
			}
			else
			{
				ret = -4;
			}

			DELETEO(response);
		//}
	}
	
	DELETEO(request);

	return ret;
}
//--------------------------------------
//
//--------------------------------------
bool net_turn_allocation_t::send_allocate()
{
	net_stun_request_t* request = create_request_allocate();

	if (!send_request(request))
	{
		PLOG_NET_ERROR("net-turn", "TURN allocation failed with error.");
		return false;
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool net_turn_allocation_t::send_refresh()
{
	int ret;
	net_stun_request_t* request = create_request_refresh();
	ret = send_request(request);
	if (ret)
	{
		PLOG_NET_ERROR("net-turn", "TURN allocation refresh failed with error code:%d.", ret);
		return false;
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool net_turn_allocation_t::send_unallocate()
{
	int ret;
	net_stun_request_t* request = create_request_unallocate();
	ret = send_request(request);
	if (ret)
	{
		PLOG_NET_ERROR("net-turn", "TURN unallocation failed with error code:%d.", ret);
		return false;
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
net_turn_channel_binding_id_t net_turn_allocation_t::channel_bind(const ip_address_t* peer_address, uint16_t peer_port)
{
	net_turn_channel_binding_id_t id = NET_TURN_INVALID_CHANNEL_BINDING_ID;
	net_turn_channel_binding_t *channel_binding = nullptr;

	int ret;
		
	channel_binding = net_turn_channel_binding_create(this);
		
	if (channel_binding != nullptr)
	{
		uint32_t _sin_addr = 0;

		channel_binding->xpeer.family = stun_ipv4;
		channel_binding->xpeer.port = (htons(peer_port) ^ htons(0x2112));
				
		const in_addr* addr_v4 = (peer_address->get_family() == e_ip_address_family::E_IP_ADDRESS_FAMILY_IPV4) ? (const in_addr*)peer_address->get_native() : nullptr;
		if (addr_v4 != nullptr)
		{
			_sin_addr = htonl(addr_v4->s_addr) ^ htonl(NET_STUN_MAGIC_COOKIE);
		}
		channel_binding->xpeer.ipv4.s_addr = _sin_addr;
	}
	else
	{
		return id;
	}

	net_stun_request_t* request = create_request_channel_bind(channel_binding); 
	if ((ret = send_request(request)))
	{
		PLOG_NET_ERROR("net-turn", "TURN (CHANNEL-BIND) failed with error code:%d.", ret);
		DELETEO(channel_binding);
		channel_binding = nullptr;
		return id;
	}
	else
	{
		id = channel_binding->id;
		//tsk_list_push_back_data(allocation->channel_bindings, (void**)&channel_binding);
		m_channel_bindings.add(channel_binding);
	}

	return id;
}
//--------------------------------------
//
//--------------------------------------
bool net_turn_allocation_t::channel_refresh(const net_turn_channel_binding_t* channel_bind)
{
	if (channel_bind)
	{
		int ret;
		net_stun_request_t* request = create_request_channel_bind(channel_bind);
		if ((ret = send_request(request)))
		{
			PLOG_NET_ERROR("net-turn", "TURN channel-binding refresh failed with error code:%d.", ret);
			return false;
		}
		else
			return true;
	}
	return false;
}
//--------------------------------------
//
//--------------------------------------
int net_turn_allocation_t::channel_senddata(const net_turn_channel_binding_t* channel_bind, const void* data, int size, int indication)
{
	net_turn_channel_data_t *channel_data = 0;
	rtl::MemoryStream stream;
	int ret = -1;

	if (channel_bind)
	{
		if (indication)
		{	/* SEND INDICATION */
			net_stun_request_t* request = create_request_sendindication(&channel_bind->xpeer, data, size);
			if ((ret = send_request(request)))
			{
				PLOG_NET_ERROR("net-turn", "TURN channel send indication failed with error code:%d.", ret);
				return -1;
			}
			else
				return 0;
		}
		else
		{	/* CHANNEL DATA */
			if (!(channel_data = NEW net_turn_channel_data_t(channel_bind->id, size, data)))
			{
				PLOG_NET_ERROR("net-turn", "Failed to create TURN CHANNEL-DATA message.");
				goto bail;
			}

			ret = channel_data->serialize(stream);
			if (ret < 4)
			{
				PLOG_NET_ERROR("net-turn", "Failed to serialize TURN CHANNEL-DATA.");
				goto bail;
			}

			net_socket* sock = channel_bind->allocation->get_socket();
			socket_address saddr(channel_bind->allocation->get_server_address(), channel_bind->allocation->get_server_port());
			if (sock->send_to(stream.getBuffer(), uint32_t(stream.getLength()), &saddr) <= 0)
			{
				PLOG_NET_ERROR("net-turn", "Failed to send TURN messsage.");
				ret = -2;
				goto bail;
			}
			else
			{
				ret = 0;
			}
		}
	}

bail:
	DELETEO(channel_data);

	return ret;
}
//--------------------------------------
//
//--------------------------------------
int net_turn_allocation_t::add_permission(const char* ipaddress, uint32_t timeout)
{
	int ret;

	ip_address_t ipaddr(ipaddress);
	//in_addr ipaddr = net_t::get_host_by_name(ipaddress);
	
	net_stun_request_t* request = create_request_permission(&ipaddr, timeout);
	if ((ret = send_request(request)))
	{
		PLOG_NET_ERROR("net-turn", "TURN (ADD) permission failed with error code:%d.", ret);
		return -1;
	}

	return 0;
}
//--------------------------------------
//
//--------------------------------------
void net_turn_allocation_t::remove_permission(net_turn_permission_t* permission)
{

}
//--------------------------------------
//
//--------------------------------------
void net_turn_allocation_t::add_binding(net_turn_channel_binding_t* binding)
{
	m_channel_bindings.add(binding);
}
//--------------------------------------
//
//--------------------------------------
void net_turn_allocation_t::remove_binding(net_turn_channel_binding_t* binding)
{
	m_channel_bindings.remove(binding);
}
//--------------------------------------
//
//--------------------------------------
const net_turn_channel_binding_t* net_turn_allocation_t::find_binding(net_turn_channel_binding_id_t id) const
{
	for (int i = 0; i < m_channel_bindings.getCount(); i++)
	{
		if (m_channel_bindings[i]->id == id)
			return m_channel_bindings[i];
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
net_stun_request_t* net_turn_allocation_t::update_request(net_stun_request_t* original_request) const
{
	net_stun_request_t* new_request = nullptr;

	net_stun_message_type_t type = original_request->getType();
	switch (type)
	{
	case net_stun_allocate_request:
		new_request = create_request_allocate();
		break;
	case net_stun_refresh_request:
		new_request = create_request_refresh();
		new_request->set_lifetime(original_request->get_lifetime());
		break;
	case net_stun_channelbind_request:
		{
			const net_stun_attribute_t& attr = original_request->get_attribute(net_stun_channel_number);
			const net_turn_channel_binding_t* binding = find_binding(attr.value16[0]);
			new_request = create_request_channel_bind(binding);
		}
		break;
	case net_stun_send_indication:
		{
			const net_stun_attribute_t& xpeer = original_request->get_attribute(net_stun_xor_peer_address);
			const net_stun_attribute_t& data = original_request->get_attribute(net_stun_data);
			new_request = create_request_sendindication(&xpeer.address, data.data_ptr, data.length);
		}
		break;
	case net_stun_createpermission_request:
		{
			ip_address_t any;
			new_request = create_request_permission(&any, 0);
		}
		break;
	}

	DELETEO(original_request);

	return new_request;
}
//--------------------------------------
