﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "net_address.h"
//--------------------------------------
// ansi to in_addr xxx.xxx.xxx.xxx
//--------------------------------------
RTX_NET_API in_addr net_parse(const char* addr_str)
{
	const char* cp = addr_str;
	char ch;

	in_addr result = { INADDR_ANY };

	int digits[4] = { -1, -1, -1, -1 };
	int count = 0;
	int d_count = 0;

	while ((ch = *cp++) != 0)
	{
		if (ch == '.')
		{
			if (digits[d_count] > 255)
			{
				result.s_addr = INADDR_NONE;
				return result;
			}

			d_count++;
			count = 0;

			if (d_count > 3)
			{
				result.s_addr = INADDR_NONE;
				return result;
			}
		}
		else if (ch >= '0' && ch <= '9')
		{
			if (digits[d_count] == -1)
				digits[d_count] = 0;

			digits[d_count] *= 10;
			digits[d_count] += (ch - '0');

			if (++count > 3 || digits[d_count] > 255)
			{
				result.s_addr = INADDR_NONE;
				return result;
			}
		}
		else if (d_count == 3 && digits[d_count] != -1)
		{
			break;
		}
		else
		{
			result.s_addr = INADDR_NONE;
			return result;
		}
	}

	uint8_t* bytes = (uint8_t*)&result.s_addr;
	for (int i = 0; i< 4; i++)
	{
		if (digits[i] == -1)
		{
			result.s_addr = INADDR_NONE;
			return result;
		}
		bytes[i] = (uint8_t)digits[i];
	}

	return result;
}
//--------------------------------------
// ansi to in_addr and port xxx.xxx.xxx.xxx:xxxx
//--------------------------------------
RTX_NET_API bool net_parse(const char* addr_str, in_addr& addr, uint16_t& port)
{
	if (addr_str == nullptr || addr_str[0] == 0)
		return false;

	const char* cp = addr_str;
	char ch;
	int digits[4] = { -1, -1, -1, -1 };
	int count = 0;
	int d_count = 0;

	while ((ch = *cp++) != 0)
	{
		if (ch == '.')
		{
			if (digits[d_count] > 255)
			{
				addr.s_addr = INADDR_NONE;
				return false;
			}

			d_count++;
			count = 0;

			if (d_count > 3)
			{
				addr.s_addr = INADDR_NONE;
				return false;
			}
		}
		else if (ch >= '0' && ch <= '9')
		{
			if (digits[d_count] == -1)
				digits[d_count] = 0;

			digits[d_count] *= 10;
			digits[d_count] += (ch - '0');

			if (++count > 3 || digits[d_count] > 255)
			{
				addr.s_addr = INADDR_NONE;
				return false;
			}
		}
		else if (ch == ':')
		{
			port = (unsigned short)strtoul(cp, NULL, 10);
			break;
		}
		else if (d_count == 3 && digits[d_count] != -1)
		{
			break;
		}
		else
		{
			addr.s_addr = INADDR_NONE;
			return false;
		}
	}

	uint8_t* bytes = (uint8_t*)&addr.s_addr;
	for (int i = 0; i< 4; i++)
	{
		if (digits[i] == -1)
		{
			addr.s_addr = INADDR_NONE;
			return false;
		}
		bytes[i] = (uint8_t)digits[i];
	}

	return true;
}
//--------------------------------------
// unicode to in_addr and port xxx.xxx.xxx.xxx:xxxx
//--------------------------------------
RTX_NET_API const char* net_to_string(char* buffer, int size, in_addr addr)
{
	uint8_t* bytes = (uint8_t*)&addr;
	int len = std_snprintf(buffer, size, "%u.%u.%u.%u", bytes[0], bytes[1], bytes[2], bytes[3]);
	buffer[len] = 0;
	return buffer;
}
//--------------------------------------
// unicode to in_addr and port xxx.xxx.xxx.xxx:xxxx
//--------------------------------------
RTX_NET_API const char* net_to_string(char* buffer, int size, in_addr addr, uint16_t port)
{
	uint8_t* bytes = (uint8_t*)&addr;
	int len = std_snprintf(buffer, size, "%u.%u.%u.%u:%u", bytes[0], bytes[1], bytes[2], bytes[3], port);
	buffer[len] = 0;
	return buffer;
}
//--------------------------------------
