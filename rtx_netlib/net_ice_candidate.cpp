﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "net_ice_candidate.h"
#include "net_ice_utils.h"
#include "ctype.h"
//--------------------------------------
//
//--------------------------------------
static const char* get_transport_name(net_ice_transport_type_t transport_e);
static net_ice_transport_type_t get_transport_type(const char* transport_str, int len = -1);
static const char* get_candtype_name(net_ice_cand_type_t candtype_e);
static net_ice_cand_type_t get_candtype(const char* candtype_str, int len);
//--------------------------------------
//
//--------------------------------------
net_ice_candidate_t::net_ice_candidate_t(rtl::Logger* log) : m_log(log)
{
	m_type = net_ice_cand_type_host;
	memset(m_foundation, 0, sizeof m_foundation);
	m_comp_id = 0;
	m_transport = net_ice_transport_udp;
	m_priority = 0;
	m_conn_address.parse("");
	m_conn_port = 0;
	m_is_ice_jingle = false;
	m_is_rtp = false;
	m_is_video = false;
	m_local_pref = 0;
	m_ufrag = nullptr;
	m_pwd = nullptr;
	m_socket = nullptr;

	memset(&m_stun, 0, sizeof m_stun);
}
//--------------------------------------
//
//--------------------------------------
net_ice_candidate_t::net_ice_candidate_t(const net_ice_candidate_t& candidate) : m_log(candidate.m_log)
{
	m_type = candidate.m_type;
	memcpy(m_foundation, candidate.m_foundation, 33);
	m_comp_id = candidate.m_comp_id;						// 1*5DIGIT
	m_transport = candidate.m_transport;
	m_priority = candidate.m_priority;					// 1*10DIGIST [1 - (2**31 - 1)]
	m_conn_address.copy_from(&candidate.m_conn_address);
	m_conn_port = candidate.m_conn_port;
	
	for (int i = 0; i < m_extension_att_list.getCount(); i++)
	{
		const ice_param_t& param = candidate.m_extension_att_list[i];
		add_ice_param(param.name, param.value);
	}

	m_is_ice_jingle = candidate.m_is_ice_jingle;
	m_is_rtp = candidate.m_is_rtp;
	m_is_video = candidate.m_is_video;
	m_local_pref = candidate.m_local_pref;
	
	m_ufrag = rtl::strdup(candidate.m_ufrag);
	m_pwd = rtl::strdup(candidate.m_pwd);

	m_socket = candidate.m_socket;

	m_stun.nonce = rtl::strdup(candidate.m_stun.nonce);
	m_stun.realm = rtl::strdup(candidate.m_stun.realm);
	memcpy(m_stun.transac_id, candidate.m_stun.transac_id, NET_STUN_TRANSACID_SIZE);
	m_stun.srflx_addr.copy_from(&candidate.m_stun.srflx_addr);
	m_stun.srflx_port = candidate.m_stun.srflx_port;
}
//--------------------------------------
//
//--------------------------------------
net_ice_candidate_t::~net_ice_candidate_t()
{
	for (int i = 0; i < m_extension_att_list.getCount(); i++)
	{
		ice_param_t& param = m_extension_att_list[i];

		FREE(param.name);
		FREE(param.value);
	}

	m_extension_att_list.clear();
	
	m_socket = nullptr;

	FREE(m_stun.nonce);
	FREE(m_stun.realm);

	FREE(m_ufrag);
	FREE(m_pwd);
}
//--------------------------------------
//
//--------------------------------------
void net_ice_candidate_t::initialize(net_ice_cand_type_t type_e, net_socket* socket, bool is_ice_jingle, bool is_rtp, bool is_video, const char* ufrag, const char* pwd, const char *foundation)
{
	m_transport = net_ice_transport_udp;//socket->type;
	m_type = type_e;
	m_socket = socket;//tsk_object_ref(socket);
	m_local_pref = 0xFFFF;
	m_is_ice_jingle = is_ice_jingle;
	m_is_rtp = is_rtp;
	m_is_video = is_video;
	m_comp_id = is_rtp ? NET_ICE_CANDIDATE_COMPID_RTP : NET_ICE_CANDIDATE_COMPID_RTCP;
	
	if (foundation)
	{
		memcpy(m_foundation, foundation, MIN(strlen(foundation), NET_ICE_CANDIDATE_FOUND_SIZE_PREF));
	}
	else
	{
		net_ice_utils_compute_foundation((char*)m_foundation, MIN(sizeof(m_foundation), NET_ICE_CANDIDATE_FOUND_SIZE_PREF));
	}
	
	m_priority = net_ice_utils_get_priority(m_type, m_local_pref, m_is_rtp);
	
	if (m_socket)
	{
		//sockaddr_in l_addr;
		//int l_addr_len = sizeof l_addr;
		const socket_address* local_addr = m_socket->get_local_address();
		m_conn_address.copy_from(local_addr->get_ip_address());
		m_conn_port = ntohs(local_addr->get_port());
	}
	
	set_credential(ufrag, pwd);
}
//--------------------------------------
//
//--------------------------------------
bool net_ice_candidate_t::have_param(const char* name) const
{
	for (int i = 0; i < m_extension_att_list.getCount(); i++)
	{
		const ice_param_t& param = m_extension_att_list[i];

		if (std_stricmp(param.name, name) == 0)
		{
			return true;
		}
	}

	return false;
}
//--------------------------------------
//
//--------------------------------------
void net_ice_candidate_t::int_add_ice_param(char* name, char* value)
{
	ice_param_t param = { name, value };
	m_extension_att_list.add(param);
}
//--------------------------------------
//
//--------------------------------------
net_stun_message_t* net_ice_candidate_t::create_bind_request(const char* username, const char* password)
{
	net_stun_message_t *request = nullptr;
	
	/* Set the request type (RFC 5389 defines only one type) */
	request = NEW net_stun_message_t(net_stun_binding_request, username, password);

	request->set_realm(m_stun.realm);
	request->set_nonce(m_stun.nonce);

	/* Add software attribute */
	request->add_attribute_data(net_stun_software, NET_ICE_SOFTWARE, 8);
	
	return request;
}
//--------------------------------------
//
//--------------------------------------
void net_ice_candidate_t::set_credential(const char* ufrag, const char* pwd)
{
	rtl::strupdate(m_ufrag, ufrag);
	rtl::strupdate(m_pwd, pwd);
}
//--------------------------------------
//
//--------------------------------------
void net_ice_candidate_t::set_rflx_addr(const ip_address_t* address, uint16_t port)
{
	if ((address == nullptr) || (address->empty()) || (!port))
	{
		//MLOG_ERROR("ICE_CAND", "Invalid argument");
		return;
	}
	
	m_stun.srflx_addr.copy_from(address);
	m_stun.srflx_port = port;
}
//--------------------------------------
//
//--------------------------------------
const char* net_ice_candidate_t::get_att_value(const char* att_name) const
{
	if (!att_name)
	{
		//MLOG_ERROR("ICE_CAND", "Invalid parameter");
		return nullptr;
	}

	for (int i = 0; i < m_extension_att_list.getCount(); i++)
	{
		const ice_param_t& param = m_extension_att_list[i];

		if (std_stricmp(param.name, att_name) == 0)
		{
			return param.value;
		}
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
int net_ice_candidate_t::set_local_pref(uint16_t local_pref)
{
	m_local_pref = local_pref;
	m_priority = net_ice_utils_get_priority(m_type, m_local_pref, m_is_rtp);
	
	return 0;
}
//--------------------------------------
//
//--------------------------------------
int net_ice_candidate_t::encode(char* buffer, int size) const
{
	char* initial_ptr = buffer;
	const char* _transport_str;
	char __str[16]; // always allocated: bad idea :(

	_transport_str = get_transport_name(m_transport);

	//MLOG_CALL("ICE-CAND", "encode : is_ice_jingle = %s", m_is_ice_jingle ? "true" : "false");

	if (m_is_ice_jingle)
	{
		int i, s = int(strlen(_transport_str));
		memset(__str, 0, sizeof(__str));
		for (i = 0; i < s && i < int(sizeof(__str)/sizeof(__str[0])); ++i)
		{
			__str[i] = tolower(_transport_str[i]);
		}
		_transport_str = &__str[0];
	}

	//_tnet_ice_candidate_tostring(
	//	m_foundation,
	//	m_comp_id,
	//	_transport_str,
	//	m_priority,
	//	(tsk_strnullORempty(m_connection_addr) && m_socket) ? m_socket->ip : m_connection_addr,
	//	(m_port <= 0 && m_socket) ? m_socket->port : m_port,
	//	m_cand_type_str ? m_cand_type_str : _tnet_ice_candidate_get_candtype_str(m_type_e),
	//	m_extension_att_list,
	//	&m_tostring);

	const ip_address_t* addr = (m_type == net_ice_cand_type_host) ? &m_conn_address : &m_stun.srflx_addr;
	uint16_t port = m_type == net_ice_cand_type_host ? m_conn_port : m_stun.srflx_port;

	int len = std_snprintf(buffer, size, "%s %d %s %d %s %d typ %s",
		m_foundation, m_comp_id, _transport_str, m_priority, addr->to_string(), port, get_candtype_name(m_type));
	
	buffer += len;
	size -= len;

	for (int i = 0; i < m_extension_att_list.getCount(); i++)
	{
		const ice_param_t& param = m_extension_att_list.getAt(i);
		len = std_snprintf(buffer, size, " %s %s", param.name, param.value);

		buffer += len;
		size -= size;

		//MLOG_CALL("ICE-CAND", "encode : extension[%d] = %s %s", i, param.name, param.value);
	}

	// WebRTC (Chrome) specific
	if (m_is_ice_jingle)
	{
		//if (!have_param("name"))
		//{
		//	const char* name = m_is_rtp ? (m_is_video ? "video_rtp" : "rtp") : (m_is_video ? "video_rtcp" : "rtcp");
		//	len = std_snprintf(buffer, size, " name %s", name);
		//	buffer += len;
		//	size -= len;

		//	MLOG_CALL("ICE-CAND", "encode : name = %s", name);
		//}
		
		//if (!have_param("username"))
		//{
		//	len = std_snprintf(buffer, size, " username %s", m_ufrag);
		//	buffer += len;
		//	size -= len;

		//	MLOG_CALL("ICE-CAND", "encode : username = %s", m_ufrag);
		//}
		//
		//if (!have_param("password"))
		//{
		//	len = std_snprintf(buffer, size, " password %s", m_pwd);
		//	buffer += len;
		//	size -= len;

		//	MLOG_CALL("ICE-CAND", "encode : password = %s", m_pwd);
		//}
		//
		//if (!have_param("network_name"))
		//{
		//	len = std_snprintf(buffer, size, " network_name {9EBBE687-CCE6-42D3-87F5-B57BB30DEE23}");
		//	buffer += len;
		//	size -= len;

		//	MLOG_CALL("ICE-CAND", "encode : network_name = network_name {9EBBE687-CCE6-42D3-87F5-B57BB30DEE23}");
		//}
		
		if (!have_param("generation"))
		{
			len = std_snprintf(buffer, size, " generation 0");
			buffer += len;
			size -= len;

			//MLOG_CALL("ICE-CAND", "encode : generation = 0");
		}
	}
	
	return int(buffer - initial_ptr);
}
//--------------------------------------
//
//--------------------------------------
int net_ice_candidate_t::send_stun_bind_request(const ip_address_t* address, uint16_t port, const char* username, const char* password)
{
	net_stun_message_t *request = nullptr; 
	rtl::MemoryStream stream;
	size_t sendBytes;

	//MLOG_CALL("ICE-CAND", "send STUN:BIND request to %s:%u usr:%s pwd:%s", inet_ntoa(address), port, username, password);

	if ((address == nullptr) || (address->empty()) || (m_socket->get_native_handle() == INVALID_SOCKET))
	{
		//MLOG_ERROR("ICE_CAND", "Invalid parameter");
		return -1;
	}
	socket_address saddr(address, port);

	request = create_bind_request(username, password);
	
	if (request == nullptr)
	{
		//MLOG_ERROR("ICE_CAND", "Failed to create STUN request");
		return 0;
	}

	request->encode(stream);

	request->log(m_log);

	if (stream.getLength() < 4)
	{
		//MLOG_ERROR("ICE_CAND", "Failed to serialize STUN request");
		DELETEO(request);
		return 0;
	}

	//sockaddr_in addr;
	//int addr_len = sizeof addr;
	//m_socket->get_interface((sockaddr*)&addr, &addr_len);

	const uint8_t* data = stream.getBuffer();
	int data_len = int(stream.getLength());

	sendBytes = m_socket->send_to(data, data_len, &saddr);
	
	if (sendBytes == stream.getLength())
	{
		memcpy(m_stun.transac_id, request->get_transaction_id(), NET_STUN_TRANSACID_SIZE);
		//MLOG_CALL("ICE-CAND", "request sent");
	}
	else
	{
		//MLOG_ERROR("ICE_CAND", "Only %d bytes sent", sendBytes);
	}

	DELETEO(request);

	return 0;
}
//--------------------------------------
//
//--------------------------------------
int net_ice_candidate_t::process_stun_response(const net_stun_response_t* response, net_socket_t* fd)
{
	int ret = 0;

	if (!response)
	{
		//MLOG_ERROR("ICE_CAND", "Inavlid parameter");
		return -1;
	}
	
	if (memcmp(response->get_transaction_id(), m_stun.transac_id, NET_STUN_TRANSACID_SIZE) != 0)
	{
		//MLOG_ERROR("ICE_CAND", "Transaction id mismatch");
		return -2;
	}

	if (NET_STUN_RESPONSE_IS_ERROR(response->getType()))
	{
		//MLOG_CALL("ICE-CAND", "Error responce received!");
		short code = response->get_errorcode();
		const char* realm = response->get_realm();
		const char* nonce = response->get_nonce();

		if (code == 401 && realm && nonce)
		{
			if (!m_stun.nonce)
			{
				/* First time we get a nonce */
				rtl::strupdate(m_stun.nonce, nonce);
				rtl::strupdate(m_stun.realm, realm);
				return 0;
			}
			else
			{
				//MLOG_ERROR("ICE_CAND", "Authentication failed");
				return -3;
			}
		}
		else
		{
			//MLOG_ERROR("ICE_CAND", "STUN error: %hi", code);
			return -4;
		}
	}
	else if (NET_STUN_RESPONSE_IS_SUCCESS(response->getType()))
	{
		//MLOG_CALL("ICE-CAND", "Success responce received!");

		const net_stun_attribute_t& xmaddr = response->get_attribute(net_stun_xor_mapped_address);

		if (xmaddr.type == net_stun_xor_mapped_address)
		{
			m_stun.srflx_addr.set_native(&xmaddr.address.ipv4, sizeof(xmaddr.address.ipv4));
			m_stun.srflx_port = xmaddr.address.port;
		}
		else
		{
			const net_stun_attribute_t& maddr = response->get_attribute(net_stun_mapped_address);
			if (maddr.type == net_stun_mapped_address)
			{
				m_stun.srflx_addr.set_native(&maddr.address.ipv4, sizeof(maddr.address.ipv4));
				m_stun.srflx_port = maddr.address.port;
			}
			else
			{
				//MLOG_CALL("ICE-CAND", "MAPPED-ADDRESS atribute not found!");
			}
		}

		//MLOG_CALL("ICE-CAND", "srflx = %s:%u", inet_ntoa(m_stun.srflx_addr), m_stun.srflx_port);
	}

	return ret;
}
//--------------------------------------
// "1 1 udp 1 192.168.196.1 57806 typ host name video_rtcp network_name {0C0137CC-DB78-46B6-9B6C-7E097FFA79FE} username StFEVThMK2DHThkv password qkhKUDr4WqKRwZTo generation 0"
// foundation SP component-id SP transport SP priority SP connection-address SP port SP cand-type [SP rel-addr] [SP rel-port]
//--------------------------------------
net_ice_candidate_t* net_ice_candidate_t::parse(const char* str, rtl::Logger* log)
{
	if (str == nullptr || str[0] == 0)
	{
		//LOG_ERROR("ICE_CAND", "Invalid parameter");
		return nullptr;
	}	

	const char *cp;
	net_ice_candidate_t* candidate = NEW net_ice_candidate_t(log);

	// foundation
	if ((cp = strchr(str, ' ')) == nullptr)
		return nullptr;
	candidate->set_foundation(str, int(cp - str));

	// component id
	str = cp+1;
	if ((cp = strchr(str, ' ')) == nullptr)
		return nullptr;
	candidate->set_component_id(atoi(str));

	// transport
	str = cp+1;
	if ((cp = strchr(str, ' ')) == nullptr)
		return nullptr;
	candidate->set_transport(get_transport_type(str, int(cp - str)));

	// priority
	str = cp+1;
	if ((cp = strchr(str, ' ')) == nullptr)
		return nullptr;
	candidate->set_priority(atoi(str));
	
	// conn-address
	str = cp+1;
	if ((cp = strchr(str, ' ')) == nullptr)
		return nullptr;
	ip_address_t ipaddr(str);
	candidate->set_connection_address(&ipaddr);

	// conn-port
	str = cp+1;
	if ((cp = strchr(str, ' ')) == nullptr)
		return nullptr;
	candidate->set_connection_port((uint16_t)strtoul(str, nullptr, 10));

	// cand type
	str = cp+1;
	if ((cp = strchr(str, ' ')) == nullptr)
		return nullptr;

	if (std_strnicmp(str, "typ", 3) != 0)
		return nullptr;
	str = cp+1;
	cp = strchr(str, ' ');

	candidate->set_cand_type(get_candtype(str, (cp != nullptr) ? int(cp-str) : -1));

	if (cp == nullptr)
		return candidate;

	// read optional extended parameters
	str = cp+1;
	while ((cp = strchr(str, ' ')) != nullptr)
	{
		// сразу извлекаем имя параметра
		char* name = rtl::strdup(str, int(cp - str));
		char* value = nullptr;
		// теперь извлекаем значение параметра
		str = cp+1;
		cp = strchr(str, ' ');
		if (cp == nullptr)
		{
			// последний параметр
			value = rtl::strdup(str);
			candidate->int_add_ice_param(name, value);
			break;
		}

		value = rtl::strdup(str, int(cp - str));
		str = cp+1;
	}

	return candidate;
}
//--------------------------------------
//
//--------------------------------------
net_ice_candidate_t* net_ice_candidate_find_by_fd(net_ice_candidate_list_t& candidates, net_socket* fd)
{
	for (int i = 0; i < candidates.getCount(); i++)
	{
		net_ice_candidate_t* candidate = candidates[i];
		net_socket* sock = candidate->get_socket();
		if (sock != nullptr && sock == fd)
		{
			return candidate;
		}
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
static const char* get_transport_name(net_ice_transport_type_t transport_e)
{
	static const char* names[] = { "UDP", "TCP", "TLS", "WS", "WSS" };
	return transport_e >= net_ice_transport_udp && transport_e <= net_ice_transport_wss ? names[transport_e] : "UNKNOWN";
}
//--------------------------------------
//
//--------------------------------------
static net_ice_transport_type_t get_transport_type(const char* transport_str, int len)
{
	if (len == 2 && std_strnicmp(transport_str, "WS", 2) == 0)
		return net_ice_transport_ws;

	if (len != 3)
		return net_ice_transport_invalid;

	if (std_strnicmp(transport_str, "UDP", 3) == 0)
		return net_ice_transport_udp;

	if (std_strnicmp(transport_str, "TCP", 3) == 0)
		return net_ice_transport_tcp;

	if (std_strnicmp(transport_str, "TLS", 3) == 0)
		return net_ice_transport_tls;

	if (std_strnicmp(transport_str, "WSS", 3) == 0)
		return net_ice_transport_wss;

	return net_ice_transport_invalid;
}
//--------------------------------------
//
//--------------------------------------
static const char* get_candtype_name(net_ice_cand_type_t candtype_e)
{
	static const char* names[] = {"unknown", "host", "srflx", "prflx", "relay" };
	return candtype_e > net_ice_cand_type_unknown && candtype_e <= net_ice_cand_type_relay ? names[candtype_e] : names[0];
}
//--------------------------------------
//
//--------------------------------------
static net_ice_cand_type_t get_candtype(const char* candtype_str, int len)
{
	if (len == -1)
		len = int(strlen(candtype_str));

	if (len == 4 && std_strnicmp(NET_ICE_CANDIDATE_TYPE_HOST, candtype_str, 4) == 0)
	{
		return net_ice_cand_type_host;
	}
	
	if (len != 5)
	{
		return net_ice_cand_type_unknown;
	}

	if (std_strnicmp(NET_ICE_CANDIDATE_TYPE_SRFLX, candtype_str, 5) == 0)
	{
		return net_ice_cand_type_srflx;
	}
	
	if (std_strnicmp(NET_ICE_CANDIDATE_TYPE_PRFLX, candtype_str, 5) == 0)
	{
		return net_ice_cand_type_prflx;
	}
	
	if(std_strnicmp(NET_ICE_CANDIDATE_TYPE_RELAY, candtype_str, 5) == 0)
	{
		return net_ice_cand_type_relay;
	}

	return net_ice_cand_type_unknown;
}
//--------------------------------------
