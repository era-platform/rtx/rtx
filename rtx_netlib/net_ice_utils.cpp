﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "net_ice_utils.h"
#include "net_ice_candidate.h"
//--------------------------------------
//
//--------------------------------------
static const char ice_chars[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'k', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 
								 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'K', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
								 '0','1', '2', '3', '4', '5', '6', '7', '8', '9'}; // /!\do not add '/' and '+' because of WebRTC password

static const int ice_chars_count = sizeof(ice_chars);

uint32_t net_ice_utils_get_priority(net_ice_cand_type_t type, uint16_t local_pref, bool is_rtp)
{
	uint32_t pref;
	
	switch(type)
	{
		case net_ice_cand_type_host: pref = NET_ICE_CANDIDATE_PREF_HOST; break;
		case net_ice_cand_type_srflx: pref = NET_ICE_CANDIDATE_PREF_SRFLX; break;
		case net_ice_cand_type_prflx: pref = NET_ICE_CANDIDATE_PREF_PRFLX; break;
		case net_ice_cand_type_relay: default: pref = NET_ICE_CANDIDATE_PREF_RELAY; break;
	}
	
	return (pref << 24) +
		(local_pref << 8) +
		((256 - (is_rtp ? NET_ICE_CANDIDATE_COMPID_RTP : NET_ICE_CANDIDATE_COMPID_RTCP)) << 0);
}


int net_ice_utils_compute_foundation(char* foundation, int size)
{
	
	int i;

	if (!foundation || !size)
	{
		LOG_ERROR("ice", "Invalid argument");
		return -1;
	}

	for (i = 0; i < size; ++i)
	{
		foundation[i] = ice_chars[(rand() ^ rand()) % ice_chars_count];
	}

	return 0;
}

//int net_ice_utils_create_sockets(/*tnet_socket_type_t*/int socket_type, const char* local_ip, /*tnet_socket_t**/SOCKET* socket_rtp, /*tnet_socket_t**/SOCKET* socket_rtcp)
//{
//	bool look4_rtp = (socket_rtp != nullptr);
//	bool look4_rtcp = (socket_rtcp != nullptr);
//	uint8_t retry_count = 4;
//	static const uint64_t port_range_start = 1024;
//	static const uint64_t port_range_stop = 65535;
//	static uint64_t counter = 0;
//
//	/* Creates local rtp and rtcp sockets */
//	while (retry_count--)
//	{
//		/* random number in the range [start - stop] */
//		uint16_t local_port = (uint16_t)((((tsk_time_epoch() + rand() ) ^ ++counter) % (port_range_stop - port_range_start)) + port_range_start);
//		local_port = (local_port & 0xFFFE); /* turn to even number */
//		
//		/* beacuse failure will cause errors in the log, print a message to alert that there is
//		* nothing to worry about */
//		LOG_CALL(L"ice", L"RTP/RTCP manager[Begin]: Trying to bind to random ports");
//		
//		if (look4_rtp)
//		{
//			if (!(*socket_rtp = tnet_socket_create(local_ip, local_port, socket_type)))
//			{
//				LOG_CALL(L"ice", L"Failed to bind to %d", local_port);
//				continue;
//			}
//		}
//
//		if (look4_rtcp)
//		{
//			if (!(*socket_rtcp = tnet_socket_create(local_ip, (local_port + 1), socket_type)))
//			{
//				LOG_CALL(L"ice", L"Failed to bind to %d", (local_port + 1));
//				if (look4_rtp)
//				{
//					//DELETEO((*socket_rtp));
//				}
//				continue;
//			}
//		}
//
//		LOG_CALL(L"ice", L"RTP/RTCP manager[End]: Trying to bind to random ports");
//		return 0;
//	}
//
//	LOG_ERROR(L"ice", L"Failed to bind sockets");
//	return -1;
//}

int net_ice_utils_set_ufrag(char** ufrag)
{
	if (ufrag)
	{
		char tmp[16]; int i;
		for (i = 0; i < int(sizeof(tmp)/sizeof(tmp[0])) - 1; ++i)
		{
			tmp[i] = ice_chars[(rand() ^ rand()) % ice_chars_count];
		}

		tmp[i] = '\0';
		rtl::strupdate(*ufrag, tmp);
		return 0;
	}
	else
	{
		LOG_NET_ERROR("ice", "Invalid parameter");
		return -1;
	}
}

int net_ice_utils_set_pwd(char** pwd)
{
	if (pwd)
	{
		char tmp[22]; int i;
		for (i = 0; i < int(sizeof(tmp)/sizeof(tmp[0])) - 1; ++i)
		{
			tmp[i] = ice_chars[(rand() ^ rand()) % ice_chars_count];
		}
		tmp[i] = '\0';
		rtl::strupdate(*pwd, tmp);
		return 0;
	}
	else
	{
		LOG_ERROR("ice", "Invalid parameter");
		return -1;
	}
}

void hmac_google(const uint8_t* key, int key_len, const uint8_t* input, int in_len, uint8_t* output, int out_len)
{
	sha1_context_t hmac;

	size_t block_len = 64;
	// Copy the key to a block-sized buffer to simplify padding.
	// If the key is longer than a block, hash it and use the result instead.
	uint8_t new_key[64];
	
	memcpy(new_key, key, key_len);
	memset(new_key + key_len, 0, block_len - key_len);

	// Set up the padding from the key, salting appropriately for each padding.
	uint8_t o_pad[64];
	uint8_t i_pad[64];
	
	for (size_t i = 0; i < block_len; ++i)
	{
		o_pad[i] = 0x5c ^ new_key[i];
		i_pad[i] = 0x36 ^ new_key[i];
	}
	
	// Inner hash; hash the inner padding, and then the input buffer.
	uint8_t inner[20];

	hmac.input(i_pad, int(block_len));
	hmac.input(input, in_len);
	hmac.result(inner);
	hmac.reset();
	// Outer hash; hash the outer padding, and then the result of the inner hash.
	hmac.input(o_pad, int(block_len));
	hmac.input(inner, 20);
	
	hmac.result(output);
}
//--------------------------------------
