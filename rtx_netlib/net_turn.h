﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "net_stun_attribute.h"

 //--------------------------------------
 // Traversal Using Relays around NAT (TURN) implementation as per 'draft-ietf-behave-turn-16', 'draft-ietf-behave-turn-tcp-05'
 // and 'draft-ietf-behave-turn-ipv6'.
 //--------------------------------------


typedef net_stun_attribute_t net_turn_attribute_t;
//--------------------------------------
// TNET_TURN_PERMISSION_TIMEOUT_DEFAULT
// TNET_TURN_CHANBIND_TIMEOUT_DEFAULT
//--------------------------------------
#define NET_TURN_PERMISSION_TIMEOUT_DEFAULT			300 /* draft-ietf-behave-turn-16 subclause 8 */
#define NET_TURN_CHANBIND_TIMEOUT_DEFAULT			600 /* draft-ietf-behave-turn-16 subclause 11 */
//--------------------------------------
// TNET_TURN_INVALID_ALLOCATION_ID.
// TNET_TURN_IS_VALID_ALLOCATION_ID.
//--------------------------------------
#define NET_TURN_INVALID_ALLOCATION_ID				0
#define NET_TURN_IS_VALID_ALLOCATION_ID(id)			(id != TNET_TURN_INVALID_ALLOCATION_ID)
//--------------------------------------
//
//--------------------------------------
typedef uint64_t net_turn_allocation_id_t;
//--------------------------------------
// TNET_TURN_INVALID_CHANNEL_BINDING_ID.
// TNET_TURN_IS_VALID_CHANNEL_BINDING_ID.
//--------------------------------------
#define NET_TURN_INVALID_CHANNEL_BINDING_ID			0x00
#define NET_TURN_IS_VALID_CHANNEL_BINDING_ID(id)	( (0x4000 <= id) && (id <= 0x7FFF) ) /* see draft-ietf-behave-turn-16 subcaluse 11. */
//--------------------------------------
//
//--------------------------------------
typedef uint16_t net_turn_channel_binding_id_t;
class net_turn_allocation_t;
//--------------------------------------
//
//--------------------------------------
struct net_turn_permission_t
{
	net_turn_attribute_t xpeer;
	uint32_t timeout;			// Timeout value in seconds. Default is 300 sec (5 minutes).
};
//--------------------------------------
//
//--------------------------------------
typedef rtl::ArrayT<net_turn_permission_t> net_turn_permission_list_t; /**< List of @ref tnet_turn_permission_t elements. */
//--------------------------------------
//
//--------------------------------------
struct net_turn_channel_binding_t
{
	net_turn_channel_binding_id_t id;
	const net_turn_allocation_t* allocation;
	net_stun_address_t xpeer;
	uint32_t timeout; /**< Timeout value in seconds. Default is 600s(10 minutes). */
};
//--------------------------------------
//
//--------------------------------------
typedef rtl::ArrayT<net_turn_channel_binding_t*> net_turn_channel_binding_list_t; /**< List of @ref tnet_turn_channel_binding_t elements. */
//--------------------------------------
//
//--------------------------------------
class net_nat_context_t;
//--------------------------------------
//
//--------------------------------------
class net_turn_allocation_t
{
	rtl::Logger* m_log;

	const net_nat_context_t* m_context;
	net_turn_allocation_id_t m_id;		// Unique id
	char* m_relay_address;				// the relayed transport address
	
	net_stun_address_t m_maddr;			// Server reflexive address of the local socket(STUN1 as per RFC 3489).
	net_stun_address_t m_xmaddr;		// XORed server reflexive address (STUN2 as per RFC 5389).
	
	net_socket* m_localFD;						// 5-tuple
	ip_address_t m_server_address;
	uint16_t m_server_port;

	/// the authentication information
	char* m_username;
	char* m_password;
	char* m_realm;
	char* m_nonce;
	
	uint32_t m_timeout;					// Timeout value in seconds. Default is 600s(10 minutes)
	
	char* m_software;

	net_turn_channel_binding_list_t m_channel_bindings;	// A list of channel to peer bindings
	net_turn_permission_list_t m_permissions;			// A list of permissions

public:
	net_turn_allocation_t(rtl::Logger* log);
	~net_turn_allocation_t();

	void intitalize(const net_nat_context_t* context, net_socket* fd, const ip_address_t* server_address, uint16_t server_port, const char* username, const char* password);

	net_turn_allocation_id_t get_id() const { return m_id; }
	const char* get_relay_address() const { return m_relay_address; }
	
	net_socket* get_socket() const { return m_localFD; }
	const ip_address_t* get_server_address() const { return &m_server_address; }
	uint16_t get_server_port() const { return m_server_port; }

	const net_stun_address_t& get_xmapped_address() const { return m_xmaddr; }
	void set_xmapped_address(const net_stun_address_t& xmaddr) { m_xmaddr = xmaddr; }
	const net_stun_address_t& get_mapped_address() const { return m_maddr; }
	void set_mapped_address(const net_stun_address_t& maddr) { m_maddr = maddr; }

	/// the authentication information
	const char* get_user_name() const { return m_username; }
	void set_username(const char* username) { rtl::strupdate(m_username, username); }
	const char* get_password() const { return m_password; }
	void set_password(const char* password) { rtl::strupdate(m_password, password); }
	const char* get_realm() const { return m_realm; }
	void set_realm(const char* realm) { rtl::strupdate(m_realm, realm); }
	const char* get_nonce() const { return m_nonce; }
	void set_nonce(const char* nonce) { rtl::strupdate(m_nonce, nonce); }
	uint32_t get_timeout() const { return m_timeout; }
	void set_timeout(int32_t timeout) { m_timeout = timeout; }
	const char* get_software() const { return m_software; }
	void set_software(const char* software) { rtl::strupdate(m_software, software); }

	void add_permission(net_turn_permission_t* permission);
	void remove_permission(net_turn_permission_t* permission);

	void add_binding(net_turn_channel_binding_t* binding);
	void remove_binding(net_turn_channel_binding_t* binding);
	const net_turn_channel_binding_t* find_binding(net_turn_channel_binding_id_t id) const;

	bool send_allocate();
	bool send_refresh();
	bool send_unallocate();

	net_turn_channel_binding_id_t channel_bind(const ip_address_t* peer_address, uint16_t peer_port);
	bool channel_refresh(const net_turn_channel_binding_t* channel_bind);
	int channel_senddata(const net_turn_channel_binding_t * channel_bind, const void* data, int size, int indication);
	
	int add_permission(const char* ipaddress, uint32_t timeout);

private:
	net_stun_request_t* create_request(net_stun_message_type_t type) const;
	net_stun_request_t* create_request_allocate() const;
	net_stun_request_t* create_request_refresh() const;
	net_stun_request_t* create_request_unallocate() const;
	net_stun_request_t* create_request_channel_bind(const net_turn_channel_binding_t* channel_binding) const;
	net_stun_request_t* create_request_sendindication(const net_stun_address_t* xpeer, const void* data, int size) const;
	net_stun_request_t* create_request_permission(const ip_address_t* ipv4, uint32_t timeout) const;

	net_stun_request_t* update_request(net_stun_request_t* original) const;

	int send_request(net_stun_message_t* request);
};
//--------------------------------------
// List of net_turn_allocation_t elements
//--------------------------------------
typedef rtl::ArrayT<net_turn_allocation_t*> net_turn_allocation_list_t;
//--------------------------------------
