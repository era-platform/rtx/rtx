﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "std_memory_stream.h"
#include "net/socket_address.h"
//--------------------------------------
// STUN IP family as per RFC 5389 subclause 15.1.
//--------------------------------------
enum net_stun_addr_family_t
{
	stun_ipv4 = 0x01,
	stun_ipv6 = 0x02
};
//--------------------------------------
// STUN attribute types as per RFC 5389 subclause 18.2.
// RFC 5389 - Comprehension-required range (0x0000-0x7FFF)
//--------------------------------------
enum net_stun_attribute_type_t
{
	/// rfc5389
	net_stun_reserved				= 0x0000,	// (Reserved)
    net_stun_mapped_address			= 0x0001,	// #page-32
    net_stun_response_address		= 0x0002,	// (Reserved; was RESPONSE-ADDRESS)
    net_stun_change_address			= 0x0003,	// (Reserved; was CHANGE-ADDRESS)
    net_stun_source_address			= 0x0004,	// (Reserved; was SOURCE-ADDRESS)
    net_stun_changed_address		= 0x0005,	// (Reserved; was CHANGED-ADDRESS)
	net_stun_username				= 0x0006,
	net_stun_password				= 0x0007,	// (Reserved; was PASSWORD)
	net_stun_message_integrity		= 0x0008,
	net_stun_error_code				= 0x0009,
	net_stun_unknown_attributes		= 0x000A,
	net_stun_reflected_from			= 0x000B,	// (Reserved; was REFLECTED-FROM)
	net_stun_realm					= 0x0014,
	net_stun_nonce					= 0x0015,
	net_stun_xor_mapped_address		= 0x0020,

	/// RFC 5389 - Comprehension-optional range (0x8000-0xFFFF)
	net_stun_software = 0x8022,
	net_stun_alternate_server		= 0x8023,
	net_stun_fingerprint			= 0x8028,

	/// draft-ietf-behave-turn-16
	net_stun_channel_number			= 0x000C,	// CHANNEL-NUMBER
	net_stun_lifetime				= 0x000D,	// LIFETIME
	net_stun_reserved2				= 0x0010,	// Reserved (was BANDWIDTH)
	net_stun_xor_peer_address		= 0x0012,	// XOR-PEER-ADDRESS
	net_stun_data					= 0x0013,	// DATA
	net_stun_xor_relayed_address	= 0x0016,	// XOR-RELAYED-ADDRESS
	net_stun_even_port				= 0x0018,	// EVEN-PORT
	net_stun_requested_transport	= 0x0019,	// REQUESTED-TRANSPORT
	net_stun_dont_fragment			= 0x001A,	// DONT-FRAGMENT
	net_stun_reserved3				= 0x0021,	// Reserved (was TIMER-VAL)
	net_stun_reservation_token		= 0x0022,	// RESERVATION-TOKEN

	/// RFC 5245  21.2. STUN Attributes
	net_stun_ice_priority			= 0x0024,
	net_stun_ice_use_candidate		= 0x0025,
	net_stun_ice_controlled			= 0x8029,
	net_stun_ice_controlling		= 0x802A,
};
//--------------------------------------
//	*RFC 5389 - 15.6.  ERROR-CODE 
//--------------------------------------
struct net_stun_errorcode_t
{
	uint8_t err_class;
	uint8_t number;
	char* reason_phrase;
};
//--------------------------------------
// RFC 5389 - 15.1.  MAPPED-ADDRESS
// RFC 5389 - 15.2.  XOR-MAPPED-ADDRESS
// RFC 5389 - 15.11. ALTERNATE-SERVER
// RFC 5389 - 15.4.  MESSAGE-INTEGRITY.
//--------------------------------------
struct net_stun_address_t
{
	net_stun_addr_family_t family;
	uint16_t port;
	union
	{
		in_addr ipv4;
		in6_addr ipv6;
	};
};
//--------------------------------------
// RFC 5389 - 15.    STUN Attributes
// RFC 5245 - 19.1.  USE-CANDIDATE (flag with no body)
//--------------------------------------
struct net_stun_attribute_t
{
	uint16_t type;
	uint16_t length;
	union
	{
		net_stun_errorcode_t	error;
		net_stun_address_t		address;
		uint8_t*				data_ptr;
		uint8_t					value8[8];
		uint16_t				value16[4];
		uint32_t				value32[2];
		uint64_t				value64;
	};
};
//--------------------------------------
// attribute reader
//--------------------------------------
void net_stun_cleanup_attribute(net_stun_attribute_t& attribute);
int net_stun_read_attribute(net_stun_attribute_t& attribute, const uint8_t* stream, int size);
//--------------------------------------
//
//--------------------------------------
int net_stun_write_attribute(const net_stun_attribute_t& attribute, uint8_t* buffer, int size);
int net_stun_write_attribute(const net_stun_attribute_t& attribute, rtl::MemoryStream& stream);
int net_stun_write_attribute(const net_stun_attribute_t& attribute, char* buffer, int size);

int net_stun_write_attribute_data(net_stun_attribute_type_t type, const void* data, int size, rtl::MemoryStream& stream);
int net_stun_write_attribute_string(net_stun_attribute_type_t type, const char* string, rtl::MemoryStream& stream);
int net_stun_write_attribute_address(net_stun_attribute_type_t type, const net_stun_address_t& address, rtl::MemoryStream& stream);
int net_stun_write_attribute_xaddress(net_stun_attribute_type_t type, const net_stun_address_t& address, rtl::MemoryStream& stream);
int net_stun_write_attribute_int32(net_stun_attribute_type_t type, uint32_t value, rtl::MemoryStream& stream);
//--------------------------------------
//
//--------------------------------------
int net_stun_address_to_string(const net_stun_address_t* address, char** out_ip);
//--------------------------------------
// TNET_STUN_ATTRIBUTE_H
//--------------------------------------
