﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "net_turn_message.h"
//--------------------------------------
//
//--------------------------------------
net_turn_channel_data_t::net_turn_channel_data_t(uint16_t number, uint16_t length, const void* data)
{
	m_number = number;
	m_length = length;
	
	if (data != NULL && length > 0)
	{
		m_data = MALLOC(length);
		memcpy(m_data, data, length);
	}
}
//--------------------------------------
//
//--------------------------------------
net_turn_channel_data_t::~net_turn_channel_data_t()
{
	if (m_data != NULL)
	{
		FREE(m_data);
		m_data = NULL;
	}
}
//--------------------------------------
//
//--------------------------------------
int net_turn_channel_data_t::serialize(rtl::MemoryStream& stream)
{
	/*	draft-ietf-behave-turn-16 11.4.  The ChannelData Message
		0                   1                   2                   3
		0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		|         Channel Number        |            Length             |
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		|                                                               |
		/                       Application Data                        /
		/                                                               /
		|                                                               |
		|                               +-------------------------------+
		|                               |
		+-------------------------------+
	*/

	// Channel Number
	uint16_t number = htons(m_number);
	stream.write(&number, sizeof(uint16_t));
	
	uint16_t length_tons = htons(m_length);
	stream.write(&length_tons, sizeof(uint16_t));

	// Application Data
	stream.write(m_data, m_length);

	/*	=== Padding:
		Over stream transports, the ChannelData message MUST be padded to a
		multiple of four bytes in order to ensure the alignment of subsequent
		messages.  The padding is not reflected in the length field of the
		ChannelData message, so the actual size of a ChannelData message
		(including padding) is (4 + Length) rounded up to the nearest
		multiple of 4.  Over UDP, the padding is not required but MAY be included.
	*/

	int length = m_length + 4;
	int pad = m_length & 3;

	if (pad > 0)
	{
		static uint32_t zeros = 0x00000000;
		stream.write(&zeros, 4 - pad);
		length += 4 - pad;
	}

	return length;
}
//--------------------------------------
