﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "net/socket_address.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
						socket_address::socket_address()
{
}
						socket_address::socket_address(e_ip_address_family family) 
							: m_ip_address(family)
{
}
						socket_address::socket_address(const socket_address* saddr)
{
	copy_from(saddr);
}
						socket_address::socket_address(const void* native, uint32_t size)
{
	set_native(native, size);
}
						socket_address::socket_address(const ip_address_t* ipaddr, uint16_t port)
{
	set_ip_address_with_port(ipaddr, port);
}
						socket_address::~socket_address()
{
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void					socket_address::copy_from(const socket_address* saddr)
{
	if (saddr != NULL)
	{
		m_ip_address.copy_from(&saddr->m_ip_address);

		if (m_ip_address.get_family() == E_IP_ADDRESS_FAMILY_IPV4)
			m_saddr4.copy_from(&saddr->m_saddr4);
		else
			m_saddr6.copy_from(&saddr->m_saddr6);
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void					socket_address::set_native(const void* native, uint32_t size)
{
	if ((native != NULL) && (size != 0))
	{
		if (size == socket_address4::get_native_len())
		{
			m_saddr4.set_native(native);

			const ip_address4* ip4 = m_saddr4.get_ip_address();
			m_ip_address.set_native(ip4->get_native(), ip4->get_native_len());
		}
		else
		{
			m_saddr6.set_native(native);

			const ip_address6* ip6 = m_saddr6.get_ip_address();
			m_ip_address.set_native(ip6->get_native(), ip6->get_native_len());
		}
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void					socket_address::set_ip_address_with_port(const ip_address_t* ipaddr, uint16_t port)
{
	if (ipaddr != NULL)
	{
		m_ip_address.copy_from(ipaddr);

		if (m_ip_address.get_family() == E_IP_ADDRESS_FAMILY_IPV4)
			m_saddr4.set_ip_address_with_port(&m_ip_address.m_ip4, port);
		else
			m_saddr6.set_ip_address_with_port(&m_ip_address.m_ip6, port);
	}
}
void					socket_address::set_ip_address(const ip_address_t* ipaddr)
{
	if (ipaddr != NULL)
	{
		m_ip_address.copy_from(ipaddr);

		if (m_ip_address.get_family() == E_IP_ADDRESS_FAMILY_IPV4)
			m_saddr4.set_ip_address(&m_ip_address.m_ip4);
		else
			m_saddr6.set_ip_address(&m_ip_address.m_ip6);
	}
}
void					socket_address::set_port(uint16_t port)
{
	if (m_ip_address.get_family() == E_IP_ADDRESS_FAMILY_IPV4)
		m_saddr4.set_port(port);
	else
		m_saddr6.set_port(port);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const void*				socket_address::get_native() const
{
	if (m_ip_address.get_family() == E_IP_ADDRESS_FAMILY_IPV4)
		return m_saddr4.get_native();
	else
		return m_saddr6.get_native();
}
uint32_t				socket_address::get_native_len() const
{
	if (m_ip_address.get_family() == E_IP_ADDRESS_FAMILY_IPV4)
		return m_saddr4.get_native_len();
	else
		return m_saddr6.get_native_len();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const ip_address_t*		socket_address::get_ip_address() const
{
	return &m_ip_address;
}
uint16_t				socket_address::get_port() const
{
	if (m_ip_address.get_family() == E_IP_ADDRESS_FAMILY_IPV4)
		return m_saddr4.get_port();
	else
		return m_saddr6.get_port();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const char*				socket_address::to_string() const
{
	if (m_ip_address.get_family() == E_IP_ADDRESS_FAMILY_IPV4)
		return m_saddr4.to_string();
	else
		return m_saddr6.to_string();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool					socket_address::equals(const socket_address* saddr)
{
	if (saddr == NULL)
		return false;

	if (m_ip_address.get_family() != saddr->m_ip_address.get_family())
		return false;

	if (m_ip_address.get_family() == E_IP_ADDRESS_FAMILY_IPV4)
		return m_saddr4.equals(&saddr->m_saddr4);
	else
		return m_saddr6.equals(&saddr->m_saddr6);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
