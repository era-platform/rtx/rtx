﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "std_basedef.h"
#include "std_typedef.h"
#include "std_funcdef.h"

#if defined (TARGET_OS_WINDOWS)
//--------------------------------------
// для совместимости с  VS2005, VS2008, VS2010
//--------------------------------------
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#define _CRT_NON_CONFORMING_SWPRINTFS
//#define _USE_32BIT_TIME_T
#define __STDC_LIMIT_MACROS
#define _CRT_RAND_S
#pragma setlocale("ru-RU")
#endif
//--------------------------------------
// Standard Header Files:
//--------------------------------------
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <assert.h>
#include <inttypes.h>
#include <limits.h>
#include <wchar.h>
#include <string.h>
//--------------------------------------
//
//--------------------------------------
#include "std_mem_checker.h"
//--------------------------------------
//
//--------------------------------------
#if defined (DEBUG_MEMORY)
#pragma warning(disable : 4291)

void* operator new(size_t size, char* file, int line);
void* operator new[](size_t size, char* file, int line);
void operator delete (void* mem_ptr);
void operator delete[](void* mem_ptr);

#define NEW new (__FILE__, __LINE__)
#define DELETEO(ptr) delete (ptr)
#define DELETEAR(ptr) delete[] (ptr)
#define MALLOC(size) std_mem_checker_malloc(size, __FILE__, __LINE__, rc_malloc_malloc_e)
#define FREE(ptr) std_mem_checker_free(ptr, rc_malloc_free_e)
#define REALLOC(ptr, size) std_mem_checker_realloc(ptr, size, __FILE__, __LINE__)
#else
#define NEW new
#define DELETEO(ptr) delete ptr
#define DELETEAR(ptr) delete[] ptr
#define MALLOC(size) malloc(size)
#define FREE(ptr) free(ptr)
#define REALLOC(ptr, size) realloc(ptr, size)
#endif

#if defined (TARGET_OS_WINDOWS)
#include <process.h>
//--------------------------------------
// Exclude rarely-used stuff from Windows headers
//--------------------------------------
#define WIN32_LEAN_AND_MEAN

#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0600
#endif

#ifndef _WIN32_IE
#define _WIN32_IE 0x0600
#endif


//--------------------------------------
// Windows Header Files:
//--------------------------------------
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <ws2ipdef.h>
#include <iphlpapi.h>
#include <in6addr.h>

#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)

#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#if defined(TARGET_OS_LINUX)
#include <sys/epoll.h>
#elif defined(TARGET_OS_FREEBSD)
#include <sys/event.h>
#endif
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

#include <ifaddrs.h>
#if defined(TARGET_OS_LINUX)
#include <linux/if_link.h>
#endif
#endif
//--------------------------------------
//
//--------------------------------------
#include "rtx_stdlib.h"
//--------------------------------------
//
//--------------------------------------
#include "rtx_netlib.h"
//--------------------------------------
