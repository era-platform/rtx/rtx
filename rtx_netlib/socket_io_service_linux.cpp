﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "include/socket_io_service_linux.h"

#include "std_sys_env.h"
#include "std_thread.h"
#include "std_mutex.h"
//#include "net/rtp_packet.h"

#include <sys/epoll.h>
#include <unistd.h>
#include <errno.h>

#define EPOLLING_TIMEOUT    100

// структура для хранения контекста, в котором работает каждый поток.
struct netio_thread_context
{
	socket_io_service_impl*		service;
	int							netio_fd;
	volatile int				stopping; // флаг-команда потоку остановиться
    volatile uint32_t           looper; // монотонно-увеличивающийся счетчик циклов потока
    volatile uint32_t           socket_count; // кол-во сокетов, обрабатываемых этим потоком
	rtl::Thread					netio_thread;
};

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
socket_io_service_impl::socket_io_service_impl(rtl::Logger* log) : m_mutex("sock-io-svc")
{
	m_log = log;

}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
socket_io_service_impl::~socket_io_service_impl()
{
	destroy();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool socket_io_service_impl::create(uint32_t threads)
{
	if (threads == 0)
	{
		threads = std_get_processors_count();
		PLOG_NET_WARNING("io-serv","socket_io_service.create -- threads count not specified, will use value: %u", threads);
	}

	//rtl::MutexLock lock(*m_mutex);
	rtl::MutexWatchLock lock(m_mutex, __FUNCTION__);

	if (m_contexts.getCount() > 0)
	{
		PLOG_NET_ERROR("io-serv","socket_io_service.create -- already created");
		return false;
	}

	bool created = true;

	// сначала пытаемся создать все объекты epoll
	rtl::ArrayT<int> fds;
	for (uint32_t i = 0; i < threads; ++i)
	{
		int fd = epoll_create(1000000);
		if (fd < 0)
		{
			int err = errno;
			PLOG_NET_ERROR("io-serv","socket_io_service.create -- epoll_create() failed. error = %d.", err);
			created = false;
			break;
		}
        fds.add(fd);
	}

	if (created)
	{
		// объекты созданы успешно, запускаем для каждого отдельный поток.
		for (uint32_t i = 0; i < threads; ++i)
		{
			netio_thread_context* ctx = NEW netio_thread_context();
			m_contexts.add(ctx);

			ctx->service = this;
			ctx->netio_fd = fds.getAt(i);
			ctx->stopping = 0;
            ctx->looper = 0;
            ctx->socket_count = 0;
			ctx->netio_thread.start(thread_start, ctx);
		}
	}
	else
	{
		for (int i = 0; i < fds.getCount(); ++i)
		{
			int fd = fds.getAt(i);
			int res = ::close(fd);
			if (res != 0)
			{
				int err = errno;
				PLOG_NET_ERROR("io-serv","socket_io_service.create -- close() failed. error = %d.", err);
			}
		}
	}

	return created;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void socket_io_service_impl::destroy()
{
    //rtl::MutexLock lock(*m_mutex);
	rtl::MutexWatchLock lock(m_mutex, __FUNCTION__);

	if (m_contexts.getCount() == 0)
		return;

	// выставляем каждому потоку флажок
	for (int i = 0; i < m_contexts.getCount(); ++i)
	{
		netio_thread_context* ctx = m_contexts.getAt(i);
		ctx->stopping = 1;
	}

	// дожидаемся завершения потоков
	for (int i = 0; i < m_contexts.getCount(); ++i)
	{
		netio_thread_context* ctx = m_contexts.getAt(i);
		ctx->netio_thread.wait();
	}

	// закрываем epoll-объекты и удаляем все
	for (int i = 0; i < m_contexts.getCount(); ++i)
	{
		netio_thread_context* ctx = m_contexts.getAt(i);

		int res = ::close(ctx->netio_fd);
        if (res != 0)
        {
            int err = errno;
           PLOG_NET_WARNING("io-serv","socket_io_service.destroy -- close() failed. error = %d.", err);
        }

		DELETEO(ctx);
	}

	m_contexts.clear();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool socket_io_service_impl::add(net_socket* sock, i_socket_event_handler* user)
{
	if (sock == NULL)
	{
		PLOG_NET_ERROR("io-serv","socket_io_service.add -- invalid socket");
		return false;
	}

	if (user == NULL)
	{
		PLOG_NET_ERROR("io-serv","socket_io_service.add -- invalid user");
		return false;
	}

	netio_thread_context* ctx = nullptr;

	{
		//rtl::MutexLock lock(*m_mutex);
		rtl::MutexWatchLock lock(m_mutex, __FUNCTION__);


		if (m_contexts.getCount() == 0)
		{
			PLOG_NET_ERROR("io-serv","socket_io_service.add -- wasn't created");
			return false;
		}

		PLOG_NET_WRITE("io-ser", "socket_io_service.add -- adding socket %d", sock->get_native_handle());

		ctx = get_least_busy_ctx(); // наименее загруженный поток
	}

    sock->m_iosvctag_user = user;
    sock->m_iosvctag_netioctx = ctx;

	epoll_event epollev = {0};
    epollev.events |= EPOLLIN;
    epollev.data.ptr = sock;

	int res = epoll_ctl(ctx->netio_fd, EPOLL_CTL_ADD, sock->get_native_handle(), &epollev);
    if (res < 0)
    {
        int err = errno;
        PLOG_NET_ERROR("io-serv","socket_io_service.add -- epoll_ctl() failed. error = %d.", err);
        sock->m_iosvctag_user = NULL;
        sock->m_iosvctag_netioctx = NULL;
        return false;
    }

    ctx->socket_count++;

    PLOG_NET_WRITE("io-serv", "socket_io_service.add -- ok");

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void socket_io_service_impl::remove(net_socket* sock)
{
	if (sock == NULL)
	{
		PLOG_NET_ERROR("io-serv","socket_io_service.remove -- invalid socket");
		return;
	}

	if (sock->m_iosvctag_netioctx == NULL)
	{
		PLOG_NET_ERROR("io-serv","socket_io_service.remove -- invalid io_svc_context");
		return;
	}

    {
    	//rtl::MutexLock lock(*m_mutex);
    	rtl::MutexWatchLock lock(m_mutex, __FUNCTION__);


		if (m_contexts.getCount() == 0)
		{
			PLOG_NET_ERROR("io-serv","socket_io_service.remove -- wasn't created");
			return;
		}
    }

    PLOG_NET_WRITE("io-serv", "socket_io_service.remove -- removing socket %d", sock->get_native_handle());

	netio_thread_context* ctx = sock->m_iosvctag_netioctx;

    // нужно гарантировать, что после завершения этого метода
    // fd сокета больше не будет использоваться.
    // для это делаем следующее:
    // - удаляем fd сокета из объекта epoll.
    // - показываем потоку, что мы удаляем сокет.
    // - дожидаемся, когда поток будет вне функции epoll_wait() и увидит наш сигнал.

    epoll_event epollev = {0};

    int res = epoll_ctl(ctx->netio_fd, EPOLL_CTL_DEL, sock->get_native_handle(), &epollev);
    if (res == 0)
    {
        // FIXME
        // это меганеправильно, но так проще всего.
        //sock->close();

        // ждем пока поток перезайдет в epoll_wait().
        // если значение ctx->counter изменилось, значит поток по-любому был вне epoll_wait().

        uint32_t oldlooper = ctx->looper;
        uint64_t start = rtl::DateTime::getTicks64();

        while (ctx->looper == oldlooper)
        {
            rtl::Thread::sleep(2);

            uint64_t diff = rtl::DateTime::getTicks64() - start;
            if (diff >= 3000)
            {
                PLOG_NET_ERROR("io-serv","socket_io_service.remove -- got a timeout");
                break;
            }
        }

        PLOG_NET_WRITE("io-serv", "socket_io_service.add -- ok");
    }
    else
    {
        int err = errno;
        PLOG_NET_ERROR("io-serv","socket_io_service.remove -- epoll_ctl() failed. error = %d.", err);
    }

    ctx->socket_count--;

	sock->m_iosvctag_netioctx = NULL;
    sock->m_iosvctag_user = NULL;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool socket_io_service_impl::send_data_to(net_socket* sock, const uint8_t* data, uint32_t len, const socket_address* to)
{
	if (sock == NULL)
	{
		PLOG_NET_ERROR("io-serv", "socket_io_service.send_data_to -- invalid socket");
		return false;
	}

	if (data == NULL)
	{
		PLOG_NET_ERROR("io-serv", "socket_io_service.send_data_to -- invalid data");
		return false;
	}

	if (len == 0)
	{
		PLOG_NET_ERROR("io-serv", "socket_io_service.send_data_to -- invalid length");
		return false;
	}

	if (to == NULL)
	{
		PLOG_NET_ERROR("io-serv", "socket_io_service.send_data_to -- invalid address");
		return false;
	}

    uint32_t sent = sock->send_to(data, len, to);
    if (sent != len)
        return false;

    return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
netio_thread_context* socket_io_service_impl::get_least_busy_ctx()
{
    netio_thread_context* freectx = m_contexts.getAt(0);

	for (int i = 1; i < m_contexts.getCount(); ++i)
	{
		netio_thread_context* ctx = m_contexts.getAt(i);
		if (ctx->socket_count < freectx->socket_count)
			freectx = ctx;
	}

    return freectx;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void socket_io_service_impl::thread_start(rtl::Thread* thread, void* param)
{
	netio_thread_context* ctx = (netio_thread_context*)param;
	ctx->service->main_loop(ctx);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void socket_io_service_impl::main_loop(netio_thread_context* ctx)
{
	//PLOG_NET_WRITE("io-serv", "socket_io_service.main_loop -- enter");

	int epollfd = ctx->netio_fd;
	epoll_event outevent;

	bool stop = false;
	while (!stop)
	{
        ctx->looper++;

		int res = epoll_wait(epollfd, &outevent, 1, EPOLLING_TIMEOUT);

        if (ctx->stopping != 0)
        {
            PLOG_NET_WRITE("io-serv", "socket_io_service.main_loop -- stop flag detected.");
            break;
        }

        if (res > 0)
        {
            //PLOG_WRITE("io-serv", "socket_io_service.main_loop -- epoll_wait() returned %d.", res);
            on_epoll_event(&outevent);
        }
        else if (res < 0)
        {
            int err = errno;

            if (err == EINTR)
            {
               PLOG_NET_WARNING("io-serv","socket_io_service.main_loop -- epoll_wait() failed. error = EINTR.", err);
                continue;
            }

           PLOG_NET_WARNING("io-serv","socket_io_service.main_loop -- epoll_wait() failed. error = %d.", err);
        }
	}

	PLOG_NET_WRITE("io-serv", "socket_io_service.main_loop -- exit");
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void socket_io_service_impl::on_epoll_event(epoll_event* epollev)
{
    if (epollev->events & EPOLLIN)
    {
        uint8_t buffer[2048];
        socket_address saddr;

        net_socket* sock = (net_socket*)epollev->data.ptr;
        uint32_t count = sock->recv_from(buffer, sizeof(buffer), &saddr);
        if (count == 0)
        {
            PLOG_NET_WARNING("io-serv", "socket_io_service.on_epoll_event -- (socket %d) net_socket.recv_from() returns 0!",
                sock->get_native_handle());
            return;
        }

        //PLOG_NET_WRITE("io-serv", "socket_io_service.on_epoll_event -- (socket %d) received %u bytes",
        //    sock->get_native_handle(), count);

        i_socket_event_handler* user = sock->m_iosvctag_user;
        if (user != NULL)
            user->socket_data_received(sock, buffer, count, &saddr);
        else
            PLOG_NET_WARNING("io-serv", "socket_io_service.on_epoll_event -- (socket %d) user is NULL", sock->get_native_handle());
    }
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
