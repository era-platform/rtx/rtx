﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "net_stun_attribute.h"
#include "std_crypto.h"
#include "std_array_templates.h"
#include "std_string.h"
//--------------------------------------
// STUN2 magic cookie value in network byte order as per RFC 5389 subclause 6.
//--------------------------------------
#define NET_STUN_MAGIC_COOKIE			0x2112A442
//--------------------------------------
// STUN2 header size as per RFC 5389 subclause 6.
//--------------------------------------
#define NET_STUN_HEADER_SIZE			20
#define NET_STUN_ATTR_HEADER_SIZE		4
//--------------------------------------
// STUN message trasactionn ID size (96 bits = 12bytes).
//--------------------------------------
#define NET_STUN_TRANSACID_SIZE			12

//--------------------------------------
// parser mask's
//--------------------------------------
#define NET_STUN_CLASS_REQUEST_MASK		0x0000
#define NET_STUN_CLASS_INDICATION_MASK	0x0010
#define NET_STUN_CLASS_SUCCESS_MASK		0x0100
#define NET_STUN_CLASS_ERROR_MASK		0x0110
//--------------------------------------
// Checks whether the STUN message is a request or not.
//--------------------------------------
#define NET_STUN_MESSAGE_IS_REQUEST(t)						((t & 0x0110) == NET_STUN_CLASS_REQUEST_MASK)
#define NET_STUN_MESSAGE_IS_RESPONSE(t)						(NET_STUN_RESPONSE_IS_SUCCESS(t) || NET_STUN_RESPONSE_IS_ERROR(t))
//--------------------------------------
// Checks whether the STUN message is an indicaton message or not.
//--------------------------------------
#define NET_STUN_MESSAGE_IS_INDICATION(t)					((t & 0x0110) == NET_STUN_CLASS_INDICATION_MASK)
//--------------------------------------
// Checks whether the STUN message is a success response or not.
//--------------------------------------
#define NET_STUN_RESPONSE_IS_SUCCESS(t)						((t & 0x0110) == NET_STUN_CLASS_SUCCESS_MASK)
//--------------------------------------
// Checks whether the STUN message is an error response or not.
//--------------------------------------
#define NET_STUN_RESPONSE_IS_ERROR(t)						((t & 0x0110) == NET_STUN_CLASS_ERROR_MASK)
//--------------------------------------
// Checks if the pointer to the buffer hold a STUN header by checking that it starts with 0b00 and contain the magic cookie.
//			As per RFC 5389 subclause 19: Explicitly point out that the most significant 2 bits of STUN are
//			0b00, allowing easy differentiation with RTP packets when used with ICE.
//			As per RFC 5389 subclause 6: The magic cookie field MUST contain the fixed value 0x2112A442 in
//			network byte order.
//--------------------------------------
#define NET_IS_STUN2_MSG(msg, size)	(((msg)) && ((size) >= NET_STUN_HEADER_SIZE) && (((msg)[0] & 0xc0) == 0x00) && ( *(uint32_t*)(msg+4) ==  0x42A41221))
//--------------------------------------
// List of all supported STUN classes as per RFC 5389 subcaluse 6.
//--------------------------------------
enum net_stun_class_type_t
{
	net_stun_class_request				= 0x00,		// Request class: 0b00
	net_stun_class_indication			= 0x01,		// Indication class: 0b01
	net_stun_class_success_response		= 0x02,		// Success response class: 0b10
	net_stun_class_error_response		= 0x03,		// Error/failure response class: 0b11
};
//--------------------------------------
// List of all supported STUN methods. 
// RFC 5389 only define one method(Bining). All other methods have been defined
// by TURN (draft-ietf-behave-turn-16 and draft-ietf-behave-turn-tcp-05).		
//--------------------------------------
enum net_stun_method_type_t
{
	net_stun_method_binding				= 0x0001,	// RFC 5389 - Binding method: 0b000000000001
	net_stun_method_allocate			= 0x0003,	// Allocate          (only request/response semantics defined)
	net_stun_method_refresh				= 0x0004,	// Refresh           (only request/response semantics defined)
	net_stun_method_send				= 0x0006,	// Send              (only indication semantics defined)
	net_stun_method_data				= 0x0007,	// Data              (only indication semantics defined)
	net_stun_method_createpermission	= 0x0008,	// CreatePermission  (only request/response semantics defined
	net_stun_method_channelbind			= 0x0009,	// ChannelBind       (only request/response semantics defined)
};
//--------------------------------------
// List of all supported STUN types.
//--------------------------------------
/*	RFC 5389 - 6.  STUN Message Structure
		
	The message type defines the message class (request, success
	response, failure response, or indication) and the message method
	(the primary function) of the STUN message.  Although there are four
	message classes, there are only two types of transactions in STUN:
	request/response transactions (which consist of a request message and
	a response message) and indication transactions (which consist of a
	single indication message).  Response classes are split into error
	and success responses to aid in quickly processing the STUN message.

	The message type field is decomposed further into the following
	structure:

	0                 1
    2  3  4 5 6 7 8 9 0 1 2 3 4 5
    +--+--+-+-+-+-+-+-+-+-+-+-+-+-+
    |M |M |M|M|M|C|M|M|M|C|M|M|M|M|
    |11|10|9|8|7|1|6|5|4|0|3|2|1|0|
    +--+--+-+-+-+-+-+-+-+-+-+-+-+-+
*/
enum net_stun_message_type_t
{
	net_stun_binding_request					= (net_stun_method_binding | NET_STUN_CLASS_REQUEST_MASK),
	net_stun_binding_indication					= (net_stun_method_binding | NET_STUN_CLASS_INDICATION_MASK),
	net_stun_binding_success_response			= (net_stun_method_binding | NET_STUN_CLASS_SUCCESS_MASK),
	net_stun_binding_error_response				= (net_stun_method_binding | NET_STUN_CLASS_ERROR_MASK),

	net_stun_allocate_request					= (net_stun_method_allocate | NET_STUN_CLASS_REQUEST_MASK),
	net_stun_allocate_indication				= (net_stun_method_allocate | NET_STUN_CLASS_INDICATION_MASK),
	net_stun_allocate_success_response			= (net_stun_method_allocate | NET_STUN_CLASS_SUCCESS_MASK),
	net_stun_allocate_error_response			= (net_stun_method_allocate | NET_STUN_CLASS_ERROR_MASK),

	net_stun_refresh_request					= (net_stun_method_refresh | NET_STUN_CLASS_REQUEST_MASK),
	net_stun_refresh_indication					= (net_stun_method_refresh | NET_STUN_CLASS_INDICATION_MASK),
	net_stun_refresh_success_response			= (net_stun_method_refresh | NET_STUN_CLASS_SUCCESS_MASK),
	net_stun_refresh_error_response				= (net_stun_method_refresh | NET_STUN_CLASS_ERROR_MASK),

	net_stun_send_indication					= (net_stun_method_send | NET_STUN_CLASS_INDICATION_MASK),

	net_stun_data_indication					= (net_stun_method_data | NET_STUN_CLASS_INDICATION_MASK),

	net_stun_createpermission_request			= (net_stun_method_createpermission | NET_STUN_CLASS_REQUEST_MASK),
	net_stun_createpermission_indication		= (net_stun_method_createpermission | NET_STUN_CLASS_INDICATION_MASK),
	net_stun_createpermission_success_response	= (net_stun_method_createpermission | NET_STUN_CLASS_SUCCESS_MASK),
	net_stun_createpermission_error_response	= (net_stun_method_createpermission | NET_STUN_CLASS_ERROR_MASK),

	net_stun_channelbind_request				= (net_stun_method_channelbind | NET_STUN_CLASS_REQUEST_MASK),
	net_stun_channelbind_indication				= (net_stun_method_channelbind | NET_STUN_CLASS_INDICATION_MASK),
	net_stun_channelbind_success_response		= (net_stun_method_channelbind | NET_STUN_CLASS_SUCCESS_MASK),
	net_stun_channelbind_error_response			= (net_stun_method_channelbind | NET_STUN_CLASS_ERROR_MASK),
};
extern RTX_NET_API const char* get_net_stun_msg_name(net_stun_message_type_t type);
//--------------------------------------
// STUN Message structure as per RFC 5389 subclause 6.
//--------------------------------------
class RTX_NET_API net_stun_message_t
{
	/// common fields
	net_stun_message_type_t m_type;
	uint16_t m_length;
	uint32_t m_cookie;
	uint8_t m_transaction_id[NET_STUN_TRANSACID_SIZE];

	/// flags
	bool m_fingerprint;
	uint32_t m_fingerprint_value;
	bool m_integrity;
	bool m_dontfrag;
	bool m_nointegrity;

	/// from attributes
	char* m_username;
	char* m_password;
	char* m_realm;
	char* m_nonce;
	uint8_t m_msg_int_hmac[SHA1_HASH_SIZE];

	rtl::ArrayT<net_stun_attribute_t> m_attributes; /**< List of all attributes associated to this message */

	int write_message_header(uint8_t* buffer, int size) const;
	int write_attributes(uint8_t* buffer, int size) const;
	int write_message_header(rtl::MemoryStream& stream) const;
	int write_attributes(rtl::MemoryStream& stream) const;

	bool calculate_message_integrity(const uint8_t* msg, int length, uint8_t digest[SHA1_HASH_SIZE]) const;

	net_stun_attribute_t& find_attribute(net_stun_attribute_type_t type);
	const net_stun_attribute_t& find_attribute(net_stun_attribute_type_t type) const;

public:
	net_stun_message_t(net_stun_message_type_t type, const char* username, const char* password);
	net_stun_message_t();
	~net_stun_message_t();

	void set_type(net_stun_message_type_t type) { m_type = type; }
	net_stun_message_type_t getType() const { return m_type; }

	bool is_request() const { return NET_STUN_MESSAGE_IS_REQUEST(m_type); }
	bool is_response() const { return NET_STUN_MESSAGE_IS_RESPONSE(m_type); }

	void set_username(const char* username) { rtl::strupdate(m_username, username); }
	const char* get_username() const { return m_username; }
	void set_password(const char* password) { rtl::strupdate(m_password, password); }
	const char* get_password() const { return m_password; }
	
	void set_flags(bool fingerprint, bool integrity, bool dontfrag, bool nointegrity);
	
	const uint8_t* generate_transaction_id();
	const uint8_t* get_transaction_id() const { return m_transaction_id; }
	void set_transaction_id(const uint8_t* id) { memcpy(m_transaction_id, id, NET_STUN_TRANSACID_SIZE); }

	short get_errorcode() const;

	const char* get_realm() const;// { return m_realm; }
	void set_realm(const char* realm) { rtl::strupdate(m_realm, realm); }
	const char* get_nonce() const;// { return m_nonce; }
	void set_nonce(const char* nonce) { rtl::strupdate(m_nonce, nonce); }
	
	int32_t get_lifetime() const;
	void set_lifetime(int32_t lifetime);

	bool get_fingerprint() const { return m_fingerprint; }
	void set_fingerprint(bool fp) { m_fingerprint = fp; }

	bool get_nointegrity() const { return m_nointegrity; }
	void set_nointegrity(bool flag) { m_nointegrity = flag; }

	int getLength() const { return m_length; }

	bool has_attribute(net_stun_attribute_type_t type) const;
	
	//bool add_attribute(net_stun_attribute_t* attribute);
	bool add_attribute_address(net_stun_attribute_type_t type, uint16_t port, in_addr ipv4);
	bool add_attribute_address(net_stun_attribute_type_t type, uint16_t port, in6_addr ipv6);
	bool add_attribute_error_code(net_stun_attribute_type_t type, uint8_t err_class, uint8_t err_code, const char* reason_phrase);
	bool add_attribute_data(net_stun_attribute_type_t type, const void* data_ptr, int size);
	bool add_attribute_int8(net_stun_attribute_type_t type, uint8_t value);
	bool add_attribute_int16(net_stun_attribute_type_t type, uint16_t value);
	bool add_attribute_int32(net_stun_attribute_type_t type, uint32_t value);
	bool add_attribute_int64(net_stun_attribute_type_t type, uint64_t value);

	bool set_attribute_address(net_stun_attribute_type_t type, uint16_t port, in_addr ipv4);
	bool set_attribute_address(net_stun_attribute_type_t type, uint16_t port, in6_addr ipv6);
	bool set_attribute_data(net_stun_attribute_type_t type, const void* data_ptr, int size);
	bool set_attribute_int8(net_stun_attribute_type_t type, uint8_t value);
	bool set_attribute_int16(net_stun_attribute_type_t type, uint16_t value);
	bool set_attribute_int32(net_stun_attribute_type_t type, uint32_t value);
	bool set_attribute_int64(net_stun_attribute_type_t type, uint64_t value);
	
	void remove_attribute(net_stun_attribute_type_t type);
	const net_stun_attribute_t& get_attribute(net_stun_attribute_type_t type) const;
	int get_attributes_count() const { return m_attributes.getCount(); }
	const net_stun_attribute_t& get_attribute_at(int index) const { return m_attributes.getAt(index); }

	int encode(uint8_t* buffer, int size);
	int encode(rtl::MemoryStream& stream);
	bool decode(const uint8_t *data, int size);

	void log(rtl::Logger* log);
	static void log(rtl::Logger* log, const uint8_t* packet, int length);
};
//--------------------------------------
//
//--------------------------------------
bool transaction_is_equals(const uint8_t* id1, const uint8_t* id2);
//--------------------------------------
//
//--------------------------------------
typedef net_stun_message_t net_stun_response_t;
typedef net_stun_message_t net_stun_request_t;
//--------------------------------------
/* TNET_STUN_MESSAGE_H */
