﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_sys_env.h"
#include "net_io_service.h"
#include "net/socket_address.h"
//--------------------------------------
//
//--------------------------------------
#if defined (TARGET_OS_WINDOWS)
//--------------------------------------
//
//--------------------------------------
#define REMOVE_TIMEOUT			1000
#define GET_IO_STATUS_TIMEOUT	500
#define MAX_ERROR_COUNT_WHILE_TRYING_TO_START_RECV 20  //максимальное кол-во ошибок (подряд) при вызове WSARecv, на которое мы не обращаем внимания.
//--------------------------------------
//
//--------------------------------------
struct iocp_user_key
{
	iocp_user_key()
	{
		socket = NULL;
		user = NULL;

		recv_wsa_buf.buf = (char*)recv_buffer;
		recv_wsa_buf.len = sizeof(recv_buffer);
		memset(&recv_overlapped, 0, sizeof(recv_overlapped));
		recv_bytes_count = 0;
		recv_flags = 0;
		memset(recv_saddr, 0, sizeof(recv_saddr));
		recv_saddr_len = sizeof(recv_saddr);
		m_stopping = false;
		m_stopped = false;

		send_wsa_buf.buf = (char*)send_buffer;
		send_wsa_buf.len = sizeof(send_buffer);
		memset(&send_overlapped, 0, sizeof(send_overlapped));
		send_bytes_count = 0;
	}

	net_socket_t* socket;
	net_io_event_handler_t*	user;
	volatile bool m_stopping;
	volatile bool m_stopped;

	uint8_t recv_buffer[net_socket_t::PAYLOAD_BUFFER_SIZE];
	WSABUF recv_wsa_buf;
	WSAOVERLAPPED recv_overlapped;
	DWORD recv_bytes_count;
	DWORD recv_flags;
	uint8_t recv_saddr[socket_address::MAX_NATIVE_SOCKADDR_SIZE];
	int recv_saddr_len;

	uint8_t send_buffer[net_socket_t::PAYLOAD_BUFFER_SIZE];
	WSABUF send_wsa_buf;
	WSAOVERLAPPED send_overlapped;
	DWORD send_bytes_count;
};
//--------------------------------------
//
//--------------------------------------
#elif defined (TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
//--------------------------------------
//
//--------------------------------------
#if defined(TARGET_OS_LINUX)
#define EPOLLING_TIMEOUT    100
#elif defined(TARGET_OS_FREEBSD)
#define KEVENT_TIMEOUT      {0, 100 * 1000 * 1000}
#endif

//--------------------------------------
// структура для хранения контекста, в котором работает каждый поток.
//--------------------------------------
struct netio_thread_context
{
	net_io_service_t*		    service;
	int							netio_fd;
	volatile int				stopping;		// флаг-команда потоку остановиться
	volatile uint32_t           looper;			// монотонно-увеличивающийся счетчик циклов потока
	volatile uint32_t           socket_count;	// кол-во сокетов, обрабатываемых этим потоком
	rtl::Thread					netio_thread;
};
//--------------------------------------
//
//--------------------------------------
#endif
//--------------------------------------
//
//--------------------------------------
net_io_service_t::net_io_service_t(rtl::Logger* log) : m_sync("net-io-svc")
{
	m_log = log;
#if defined (TARGET_OS_WINDOWS)
	m_iocp_handle = NULL;
	m_stopping = false;
#endif
}
//--------------------------------------
//
//--------------------------------------
net_io_service_t::~net_io_service_t()
{
	destroy();
}
//--------------------------------------
//
//--------------------------------------
bool net_io_service_t::create(uint32_t threads)
{
	if (threads == 0)
	{
		threads = std_get_processors_count();
		PLOG_NET_WARNING("io-serv", "socket_io_service.create -- threads count not specified, will use value: %u", threads);
	}

	//rtl::MutexLock lock(m_sync);
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

#if defined (TARGET_OS_WINDOWS)

	if (m_iocp_handle != NULL)
	{
		PLOG_NET_ERROR("io-serv", "socket_io_service.create -- iocp already created");
		return false;
	}

	m_stopping = false;

	m_iocp_handle = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, NULL, DWORD(threads));
	if (m_iocp_handle == NULL)
	{
		DWORD err = GetLastError();
		PLOG_NET_ERROR("io-serv", "socket_io_service.create -- CreateIoCompletionPort() failed. error = %u", err);
		return false;
	}

	for (uint32_t i = 0; i < threads; ++i)
	{
		rtl::Thread* t = NEW rtl::Thread();
		t->start(thread_start, this);
		m_thread_list.add(t);
	}

	return true;

#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)

	if (m_context_list.getCount() > 0)
	{
		PLOG_NET_ERROR("io-serv", "socket_io_service.create -- already created");
		return false;
	}

	bool created = true;

	// сначала пытаемся создать все объекты epoll/kqueue
	rtl::ArrayT<int> fds;
	for (uint32_t i = 0; i < threads; ++i)
	{
#if defined(TARGET_OS_LINUX)
		int fd = epoll_create(1000000);
#elif defined(TARGET_OS_FREEBSD)
		int fd = kqueue();
#endif
		if (fd < 0)
		{
			int err = errno;
#if defined(TARGET_OS_LINUX)
			PLOG_NET_ERROR("io-serv", "socket_io_service.create -- epoll_create() failed. error = %d.", err);
#elif defined(TARGET_OS_FREEBSD)
			PLOG_NET_ERROR("io-serv", "socket_io_service.create -- kqueue() failed. error = %d.", err);
#endif
			created = false;
			break;
		}
		fds.add(fd);
	}

	if (created)
	{
		// объекты созданы успешно, запускаем для каждого отдельный поток.
		for (uint32_t i = 0; i < threads; ++i)
		{
			netio_thread_context* ctx = NEW netio_thread_context();
			m_context_list.add(ctx);

			ctx->service = this;
			ctx->netio_fd = fds.getAt(i);
			ctx->stopping = 0;
			ctx->looper = 0;
			ctx->socket_count = 0;
			ctx->netio_thread.start(thread_start, ctx);
		}
	}
	else
	{
		for (int i = 0; i < fds.getCount(); ++i)
		{
			int fd = fds.getAt(i);
			int res = ::close(fd);
			if (res != 0)
			{
				int err = errno;
				PLOG_NET_ERROR("io-serv", "socket_io_service.create -- close() failed. error = %d.", err);
			}
		}
	}

	return created;
#endif
}
//--------------------------------------
//
//--------------------------------------
void net_io_service_t::destroy()
{
	//rtl::MutexLock lock(m_sync);
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);
#if defined (TARGET_OS_WINDOWS)

	m_stopping = true;

	if (m_iocp_handle != NULL)
	{
		if (!CloseHandle(m_iocp_handle))
		{
			DWORD err = GetLastError();
			PLOG_NET_ERROR("io-serv", "socket_io_service.destroy -- CloseHandle() failed. error = %u", err);
		}

		m_iocp_handle = NULL;
	}

	if (m_thread_list.getCount() > 0)
	{
		for (int i = 0; i < m_thread_list.getCount(); ++i)
		{
			rtl::Thread* t = m_thread_list.getAt(i);
			t->wait(INFINITE);
			DELETEO(t);
		}
		m_thread_list.clear();
	}
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)

	if (m_context_list.getCount() == 0)
		return;

	// выставляем каждому потоку флажок
	for (int i = 0; i < m_context_list.getCount(); ++i)
	{
		netio_thread_context* ctx = m_context_list.getAt(i);
		ctx->stopping = 1;
	}

	// дожидаемся завершения потоков
	for (int i = 0; i < m_context_list.getCount(); ++i)
	{
		netio_thread_context* ctx = m_context_list.getAt(i);
		ctx->netio_thread.wait();
	}

	// закрываем epoll/kqueue-объекты и удаляем все
	for (int i = 0; i < m_context_list.getCount(); ++i)
	{
		netio_thread_context* ctx = m_context_list.getAt(i);

		int res = ::close(ctx->netio_fd);
		if (res != 0)
		{
			int err = errno;
			PLOG_WARNING("io-serv", "socket_io_service.destroy -- close() failed. error = %d.", err);
		}

		DELETEO(ctx);
	}

	m_context_list.clear();
#endif
}
//--------------------------------------
//
//--------------------------------------
bool net_io_service_t::add(net_socket_t* sock, net_io_event_handler_t* handler)
{
	if (sock == NULL)
	{
		PLOG_NET_ERROR("io-serv", "socket_io_service.add -- invalid socket");
		return false;
	}

	if (handler == NULL)
	{
		PLOG_NET_ERROR("io-serv", "socket_io_service.add -- invalid user");
		return false;
	}

	//rtl::MutexLock lock(m_sync);
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

#if defined (TARGET_OS_WINDOWS)
	if (m_iocp_handle == NULL)
	{
		PLOG_NET_ERROR("io-serv", "socket_io_service.add -- iocp isn't created");
		return false;
	}

	PLOG_NET_WRITE("io-serv", "socket_io_service.add -- socket %u", sock->get_handle());

	iocp_user_key* key = NEW iocp_user_key();
	key->socket = sock;
	key->user = handler;

	HANDLE h = CreateIoCompletionPort((HANDLE)sock->get_handle(), m_iocp_handle, (ULONG_PTR)key, 0);
	if (h == NULL)
	{
		DWORD err = GetLastError();
		PLOG_NET_ERROR("io-srv", "socket_io_service.add -- CreateIoCompletionPort() failed. error = %u", err);
		DELETEO(key);
		return false;
	}

	sock->set_tag(key);

	if (h != m_iocp_handle)
	{
		PLOG_WARNING("io-serv", "socket_io_service.add -- CreateIoCompletionPort() returned unknown handle");
	}

	// запускаем асинхронный прием
	start_async_recv(key);

	return true;

#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)

	if (m_context_list.getCount() == 0)
	{
		PLOG_NET_ERROR("io-serv", "socket_io_service.add -- isn't created");
		return false;
	}

	PLOG_NET_WRITE("io-serv", "socket_io_service.add -- adding socket %d", sock->get_handle());

	netio_thread_context* ctx = get_least_busy_ctx(); // наименее загруженный поток

	sock->m_iosvctag_user = handler;
	sock->m_iosvctag_netioctx = ctx;

#if defined(TARGET_OS_LINUX)
	epoll_event epollev = { 0 };
	epollev.events |= EPOLLIN;
	epollev.data.ptr = sock;

	int res = epoll_ctl(ctx->netio_fd, EPOLL_CTL_ADD, sock->get_handle(), &epollev);
#elif defined(TARGET_OS_FREEBSD)
	struct kevent kev;
    EV_SET(&kev, sock->get_handle(), EVFILT_READ, EV_ADD /* | EV_CLEAR ? */, 0, 0, (void*) sock);

	int res = kevent(ctx->netio_fd, &kev, 1, NULL, 0, NULL);
#endif
	if (res < 0)
	{
		int err = errno;
#if defined(TARGET_OS_LINUX)
		PLOG_NET_ERROR("io-serv", "socket_io_service.add -- epoll_ctl() failed. error = %d.", err);
#elif defined(TARGET_OS_FREEBSD)
		PLOG_NET_ERROR("io-serv", "socket_io_service.add -- kevent() failed. error = %d.", err);
#endif
		sock->m_iosvctag_user = NULL;
		sock->m_iosvctag_netioctx = NULL;
		return false;
	}

	ctx->socket_count++;

	PLOG_NET_WRITE("io-serv", "socket_io_service.add -- ok");

	return true;
#endif
}
void net_io_service_t::remove(net_socket_t* sock)
{
	if (sock == NULL)
	{
		PLOG_NET_ERROR("io-srv", "socket_io_service.remove -- invalid socket");
		return;
	}

#if defined (TARGET_OS_WINDOWS)
	if (sock->get_tag() == NULL)
	{
		PLOG_NET_ERROR("io-srv", "socket_io_service.remove -- invalid io_service_tag");
		return;
	}

	// ?? rtl::MutexLock lock(m_sync);

	PLOG_NET_WRITE("io-serv", "socket_io_service.remove -- socket %u", sock->get_handle());

	iocp_user_key* key = (iocp_user_key*)sock->get_tag();
	key->m_stopping = true;

	// FIXME
	// это меганеправильно, но так проще всего.
	// поток сразу вывалится из GetQueuedCompletionStatus() с ошибкой.
	sock->close();

	uint64_t start = rtl::DateTime::getTicks64();

	// ждем, когда поток ответит
	while (!key->m_stopped)
	{
		rtl::Thread::sleep(20);

		uint64_t elapsed = rtl::DateTime::getTicks64() - start;
		if (elapsed >= REMOVE_TIMEOUT)
		{
			PLOG_NET_ERROR("io-serv", "socket_io_service.remove -- timeout %u", REMOVE_TIMEOUT);
			break;
		}
	}

	DELETEO(key);
	sock->set_tag(NULL);

#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	if (sock->m_iosvctag_netioctx == NULL)
	{
		PLOG_NET_ERROR("io-serv", "socket_io_service.remove -- invalid io_svc_context");
		return;
	}

	//rtl::MutexLock lock(m_sync);
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (m_context_list.getCount() == 0)
	{
		PLOG_NET_ERROR("io-serv", "socket_io_service.remove -- isn't created");
		return;
	}

	PLOG_NET_WRITE("io-serv", "socket_io_service.remove -- removing socket %d", sock->get_handle());

	netio_thread_context* ctx = sock->m_iosvctag_netioctx;

	// нужно гарантировать, что после завершения этого метода
	// fd сокета больше не будет использоваться.
	// для это делаем следующее:
	// - удаляем fd сокета из объекта epoll/kqueue.
	// - показываем потоку, что мы удаляем сокет.
	// - дожидаемся, когда поток будет вне функции epoll_wait() и увидит наш сигнал.

#if defined(TARGET_OS_LINUX)
	epoll_event epollev = { 0 };

	int res = epoll_ctl(ctx->netio_fd, EPOLL_CTL_DEL, sock->get_handle(), &epollev);
#elif defined(TARGET_OS_FREEBSD)
	struct kevent kev;
    EV_SET(&kev, sock->get_handle(), EVFILT_READ, EV_DELETE, 0, 0, (void*) sock);

	int res = kevent(ctx->netio_fd, &kev, 1, NULL, 0, NULL);
#endif

	if (res == 0)
	{
		// ждем пока поток перезайдет в epoll_wait().
		// если значение ctx->counter изменилось, значит поток по-любому был вне epoll_wait().

		uint32_t oldlooper = ctx->looper;
		uint64_t start = rtl::DateTime::getTicks64();

		while (ctx->looper == oldlooper)
		{
			rtl::Thread::sleep(10);

			int64_t diff = rtl::DateTime::getTicks64() - start;
			if (diff >= 3000)
			{
				PLOG_NET_ERROR("io-serv", "socket_io_service.remove -- timeout");
				break;
			}
		}

		if (ctx->looper != oldlooper)
		{
            PLOG_NET_WRITE("io-serv", "socket_io_service.add -- ok");
        }
	}
	else
	{
		int err = errno;
#if defined(TARGET_OS_LINUX)
		PLOG_NET_ERROR("io-serv", "socket_io_service.remove -- epoll_ctl() failed. error = %d.", err);
#elif defined(TARGET_OS_FREEBSD)
		PLOG_NET_ERROR("io-serv", "socket_io_service.remove -- kevent() failed. error = %d.", err);
#endif
	}

	ctx->socket_count--;

	sock->m_iosvctag_netioctx = NULL;
	sock->m_iosvctag_user = NULL;
#endif
}
bool net_io_service_t::send_data_to(net_socket_t* sock, const uint8_t* data, uint32_t length, const sockaddr* to, int sockaddr_len)
{
#if defined (TARGET_OS_WINDOWS)
	if (sock == NULL)
	{
		PLOG_NET_ERROR("io-serv", "socket_io_service.send_data_to -- invalid socket");
		return false;
	}

	if (data == NULL)
	{
		PLOG_NET_ERROR("io-serv", "socket_io_service.send_data_to -- invalid data");
		return false;
	}

	if (length == 0)
	{
		PLOG_NET_ERROR("io-serv", "socket_io_service.send_data_to -- invalid length");
		return false;
	}

	if (to == NULL)
	{
		PLOG_NET_ERROR("io-serv", "socket_io_service.send_data_to -- invalid address");
		return false;
	}

	if (sock->get_tag() == NULL)
	{
		PLOG_NET_ERROR("io-serv", "socket_io_service.send_data_to -- (socket %u) invalid user_tag", sock->get_handle());
		return false;
	}

	iocp_user_key* key = (iocp_user_key*)sock->get_tag();

	if (key->m_stopped)
	{
		PLOG_NET_WARNING("io-serv", "socket_io_service.send_data_to -- (socket %u) stopped-flag is enabled.", key->socket->get_handle());
		return false;
	}

	//memcpy(key->send_buffer, data, len);
	key->send_wsa_buf.buf = (char*)data;
	key->send_wsa_buf.len = length;

#if defined(USE_ASYNC_SOCKET_SEND)
	OVERLAPPED* poverlapped = &key->send_overlapped;
#else
	OVERLAPPED* poverlapped = NULL;
#endif

	PLOG_NET_WRITE("io-serv", "socket_io_service.send_data_to -- (socket %u) sending %u bytes.", key->socket->get_handle(), length);

	int res = WSASendTo(sock->get_handle(),
		&key->send_wsa_buf,
		1,
		&key->send_bytes_count,
		0,
		to,
		sockaddr_len,
		poverlapped,
		NULL);

	if (res == 0)
		return true;

	int err = WSAGetLastError();

#if defined(USE_ASYNC_SOCKET_SEND)
	if (err == WSA_IO_PENDING)
		return true;
#endif

	if (err == WSAENOTSOCK)
	{
		PLOG_NET_WRITE("io-serv", "socket_io_service.send_data_to -- (socket %u) WSASendTo() failed. error = WSAENOTSOCK. setting stopped-flag.",
			key->socket->get_handle());
		key->m_stopped = true;
	}
	else
	{
		PLOG_NET_WARNING("io-serv", "socket_io_service.send_data_to -- (socket %u) WSASendTo() failed. error = %d",
			key->socket->get_handle(), err);
	}

	return false;

#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)

	if (sock == NULL)
	{
		PLOG_NET_ERROR("io-serv", "socket_io_service.send_data_to -- invalid socket");
		return false;
	}

	if (data == NULL)
	{
		PLOG_NET_ERROR("io-serv", "socket_io_service.send_data_to -- invalid data");
		return false;
	}

	if (length == 0)
	{
		PLOG_NET_ERROR("io-serv", "socket_io_service.send_data_to -- invalid length");
		return false;
	}

	if (to == NULL)
	{
		PLOG_NET_ERROR("io-serv", "socket_io_service.send_data_to -- invalid address");
		return false;
	}

	uint32_t sent = sock->send_to(data, length, 0, to, sockaddr_len);
	if (sent != length)
		return false;

	return true;

#endif
}

#if defined (TARGET_OS_WINDOWS)
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void net_io_service_t::thread_start(rtl::Thread* thread, void* param)
{
	net_io_service_t* pthis = (net_io_service_t*)param;

	pthis->main_loop();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void net_io_service_t::main_loop()
{
	PLOG_WRITE("io-srv", "socket_io_service.main_loop -- enter.");

	DWORD bytes = 0;
	OVERLAPPED* poverlapped = NULL;
	iocp_user_key* pkey = NULL;
	socket_address saddr;

	bool stop = false;
	while (!stop)
	{
		if (m_stopping)
		{
			PLOG_WRITE("io-srv", "socket_io_service.main_loop -- main stopping flag enabled. exit thread.");
			break;
		}

		bytes = 0;
		poverlapped = NULL;
		pkey = NULL;

		BOOL res = GetQueuedCompletionStatus(m_iocp_handle, &bytes, (PULONG_PTR)&pkey, &poverlapped, GET_IO_STATUS_TIMEOUT);
		if (res)
		{
			if (pkey == NULL)
			{
				PLOG_NET_WARNING("io-srv", "socket_io_service.main_loop -- key is NULL.");
				continue;
			}

			if (poverlapped == NULL)
			{
				PLOG_NET_WARNING("io-srv", "socket_io_service.main_loop -- overlapped is NULL.");
				continue;
			}

			if (bytes == 0)
			{
				PLOG_NET_WARNING("io-srv", "socket_io_service.main_loop -- (socket %u) no data transferred.",
					pkey->socket->get_handle());
			}

			if (poverlapped == &pkey->recv_overlapped)
			{
				saddr.set_native(pkey->recv_saddr, pkey->recv_saddr_len);

				PLOG_NET_WRITE("io-srv", "socket_io_service.main_loop -- (socket %u) %u bytes received from %s.", pkey->socket->get_handle(), bytes, saddr.to_string());

				pkey->user->net_io_data_received(pkey->socket, pkey->recv_buffer, bytes, (const sockaddr*)saddr.get_native(), saddr.get_native_len());

				start_async_recv(pkey);
			}
#if defined(USE_ASYNC_SOCKET_SEND)
			else if (poverlapped == &pkey->send_overlapped)
			{
				// ничего не делаем

				int stop = 0;
			}
#endif
			else
			{
				PLOG_NET_WARNING("io-srv", "socket_io_service.main_loop -- unknown overlapped ptr.");
			}
		}
		else
		{
			DWORD gle = GetLastError();
			switch (gle)
			{
			case ERROR_OPERATION_ABORTED:
				if (pkey != NULL)
				{
					PLOG_NET_WRITE("io-srv", "socket_io_service.main_loop -- (socket %u) GetQueuedCompletionStatus() failed. error = ERROR_OPERATION_ABORTED. setting stopped-flag.",
						pkey->socket->get_handle());
					pkey->m_stopped = true;
				}
				break;

			case ERROR_ABANDONED_WAIT_0:
				PLOG_WRITE("io-srv", "socket_io_service.main_loop -- GetQueuedCompletionStatus() failed. error = ERROR_ABANDONED_WAIT_0. exit thread.");
				stop = true;
				break;

			case ERROR_INVALID_HANDLE:
				PLOG_WRITE("io-srv", "socket_io_service.main_loop -- GetQueuedCompletionStatus() failed. error = ERROR_INVALID_HANDLE. exit thread.");
				stop = true;
				break;

			case WAIT_TIMEOUT:
				break;

			default:
				if (pkey != NULL)
				{
					PLOG_NET_WARNING("io-srv", "socket_io_service.main_loop -- (socket %u) GetQueuedCompletionStatus() failed. error = %u.", pkey->socket->get_handle(), gle);
					start_async_recv(pkey);
				}
				else
				{
					PLOG_NET_WARNING("io-srv", "socket_io_service.main_loop -- GetQueuedCompletionStatus() failed. error = %u.", gle);
				}
				break;
			}
		}
	}

	PLOG_WRITE("io-srv", "socket_io_service.main_loop -- exit.");
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void net_io_service_t::start_async_recv(iocp_user_key* key)
{
	if (key->m_stopped)
	{
		PLOG_NET_WARNING("io-srv", "socket_io_service.start_async_recv -- (socket %u) stopped-flag is enabled.", key->socket->get_handle());
		return;
	}

	PLOG_NET_WRITE("io-srv", "socket_io_service.start_async_recv -- socket %u.", key->socket->get_handle());

	uint32_t errors = 0;

	for (;;)
	{
		int res = WSARecvFrom(key->socket->get_handle(),
			&key->recv_wsa_buf,
			1,
			&key->recv_bytes_count,
			&key->recv_flags,
			(sockaddr*)&key->recv_saddr,
			&key->recv_saddr_len,
			&key->recv_overlapped,
			NULL);

		if (res == 0)
		{
			return;
		}

		int err = WSAGetLastError();
		if (err == WSA_IO_PENDING)
		{
			return;
		}

		if (err == WSAENOTSOCK)
		{
			PLOG_NET_WRITE("io-srv", "socket_io_service.start_async_recv -- (socket %u) WSARecvFrom() failed. error = WSAENOTSOCK. setting stopped-flag.",
				key->socket->get_handle());
			key->m_stopped = true;
			return;
		}

		if (++errors < MAX_ERROR_COUNT_WHILE_TRYING_TO_START_RECV)
		{
			PLOG_NET_WARNING("io-srv", "socket_io_service.start_async_recv -- (socket %u) WSARecvFrom() failed. error = %d",
				key->socket->get_handle(), err);
		}
		else
		{
			PLOG_NET_ERROR("io-srv", "socket_io_service.start_async_recv -- (socket %u) WSARecvFrom() failed. error = %d. max error count exceeded.",
				key->socket->get_handle(), err);
		}
	}
}

#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
netio_thread_context* net_io_service_t::get_least_busy_ctx()
{
	netio_thread_context* freectx = m_context_list.getAt(0);

	for (int i = 1; i < m_context_list.getCount(); ++i)
	{
		netio_thread_context* ctx = m_context_list.getAt(i);
		if (ctx->socket_count < freectx->socket_count)
			freectx = ctx;
	}

	return freectx;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void net_io_service_t::thread_start(rtl::Thread* thread, void* param)
{
	netio_thread_context* ctx = (netio_thread_context*)param;
	ctx->service->main_loop(ctx);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void net_io_service_t::main_loop(netio_thread_context* ctx)
{
	PLOG_WRITE("io-serv", "socket_io_service.main_loop -- enter");

#if defined(TARGET_OS_LINUX)
	int epollfd = ctx->netio_fd;
	epoll_event outevent;
#elif defined(TARGET_OS_FREEBSD)
	int kqfd = ctx->netio_fd;
	struct kevent outevent;
    struct timespec kevent_timeout = KEVENT_TIMEOUT;
#endif

	bool stop = false;
	while (!stop)
	{
		ctx->looper++;

#if defined(TARGET_OS_LINUX)
		int res = epoll_wait(epollfd, &outevent, 1, EPOLLING_TIMEOUT);
#elif defined(TARGET_OS_FREEBSD)
		int res = kevent(kqfd, NULL, 0, &outevent, 1, &kevent_timeout);
#endif

		if (ctx->stopping != 0)
		{
			PLOG_WRITE("io-serv", "socket_io_service.main_loop -- stop flag detected.");
			break;
		}

		if (res > 0)
		{
#if defined(TARGET_OS_LINUX)
			PLOG_WRITE("io-serv", "socket_io_service.main_loop -- epoll_wait() returned %d.", res);
			on_epoll_event(&outevent);
#elif defined(TARGET_OS_FREEBSD)
			PLOG_WRITE("io-serv", "socket_io_service.main_loop -- kevent() returned %d.", res);
			on_kevent(&outevent);
#endif
		}
		else if (res < 0)
		{
			int err = errno;

			if (err == EINTR)
			{
#if defined(TARGET_OS_LINUX)
				PLOG_WARNING("io-serv", "socket_io_service.main_loop -- epoll_wait() failed. error = EINTR.", err);
#elif defined(TARGET_OS_FREEBSD)
				PLOG_WARNING("io-serv", "socket_io_service.main_loop -- kevent() failed. error = EINTR.", err);
#endif
				continue;
			}

#if defined(TARGET_OS_LINUX)
			PLOG_WARNING("io-serv", "socket_io_service.main_loop -- epoll_wait() failed. error = %d.", err);
#elif defined(TARGET_OS_FREEBSD)
			PLOG_WARNING("io-serv", "socket_io_service.main_loop -- kevent() failed. error = %d.", err);
#endif
		}
	}

	PLOG_WRITE("io-serv", "socket_io_service.main_loop -- exit");
}
#if defined(TARGET_OS_LINUX)
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void net_io_service_t::on_epoll_event(epoll_event* epollev)
{
	PLOG_WRITE("io-serv", "socket_io_service.on_epoll_event -- %08X", epollev->events);

	//  if (epollev->events & EPOLLIN)
	//        PLOG_WRITE("io-serv", "socket_io_service.on_epoll_event -- EPOLLIN");
	//  if (epollev->events & EPOLLOUT)
	//        PLOG_WRITE("io-serv", "socket_io_service.on_epoll_event -- EPOLLOUT");
	//  if (epollev->events & EPOLLERR)
	//        PLOG_WRITE("io-serv", "socket_io_service.on_epoll_event -- EPOLLERR.");
	//	if (epollev->events & EPOLLRDHUP)
	//        PLOG_WRITE("io-serv", "socket_io_service.on_epoll_event -- EPOLLRDHUP");
	//	if (epollev->events & EPOLLPRI)
	//        PLOG_WRITE("io-serv", "socket_io_service.on_epoll_event -- EPOLLPRI");
	//	if (epollev->events & EPOLLHUP)
	//        PLOG_WRITE("io-serv", "socket_io_service.on_epoll_event -- EPOLLHUP");
	//	if (epollev->events & EPOLLET)
	//        PLOG_WRITE("io-serv", "socket_io_service.on_epoll_event -- EPOLLET");
	//	if (epollev->events & EPOLLRDNORM)
	//        PLOG_WRITE("io-serv", "socket_io_service.on_epoll_event -- EPOLLRDNORM");
	//	if (epollev->events & EPOLLRDBAND)
	//        PLOG_WRITE("io-serv", "socket_io_service.on_epoll_event -- EPOLLRDBAND");
	//	if (epollev->events & EPOLLWRNORM)
	//        PLOG_WRITE("io-serv", "socket_io_service.on_epoll_event -- EPOLLWRNORM");
	//	if (epollev->events & EPOLLWRBAND)
	//        PLOG_WRITE("io-serv", "socket_io_service.on_epoll_event -- EPOLLWRBAND");
	//	if (epollev->events & EPOLLONESHOT)
	//        PLOG_WRITE("io-serv", "socket_io_service.on_epoll_event -- EPOLLONESHOT");
	//	if (epollev->events & EPOLLMSG)
	//        PLOG_WRITE("io-serv", "socket_io_service.on_epoll_event -- EPOLLMSG");

	if (epollev->events & EPOLLIN)
	{
		uint8_t buffer[2048];
		sockaddr_in saddr;
        uint32_t sockaddr_len = 0;
		net_socket_t* sock = (net_socket_t*)epollev->data.ptr;
		uint32_t count = sock->receive_from(buffer, sizeof(buffer), 0, (sockaddr*)&saddr, &sockaddr_len);
		if (count == 0)
		{
			PLOG_NET_WARNING("io-serv", "socket_io_service.on_epoll_event -- (socket %d) net_socket.recv_from() failed.",
				sock->get_handle());
			return;
		}

		PLOG_NET_WRITE("io-serv", "socket_io_service.on_epoll_event -- (socket %d) received %u bytes",
			sock->get_handle(), count);

		net_io_event_handler_t* user = sock->m_iosvctag_user;
		if (user != NULL)
			user->net_io_data_received(sock, buffer, count, (sockaddr*)&saddr, sockaddr_len);
		else
			PLOG_NET_WARNING("io-serv", "socket_io_service.on_epoll_event -- (socket %d) user is NULL", sock->get_handle());
	}
}
#elif defined(TARGET_OS_FREEBSD)
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void net_io_service_t::on_kevent(struct kevent* kev)
{
	PLOG_WRITE("io-serv", "socket_io_service.on_kevent -- %08X", kev->filter);

	if (kev->filter == EVFILT_READ)
	{
		uint8_t buffer[2048];
		sockaddr_in saddr;
        uint32_t sockaddr_len = 0;
		net_socket_t* sock = (net_socket_t*)kev->udata;
		uint32_t count = sock->receive_from(buffer, sizeof(buffer), 0, (sockaddr*)&saddr, &sockaddr_len);
		if (count == 0)
		{
			PLOG_NET_WARNING("io-serv", "socket_io_service.on_kevent -- (socket %d) net_socket.recv_from() failed.",
				sock->get_handle());
			return;
		}

		PLOG_NET_WRITE("io-serv", "socket_io_service.on_kevent -- (socket %d) received %u bytes",
			sock->get_handle(), count);

		net_io_event_handler_t* user = sock->m_iosvctag_user;
		if (user != NULL)
			user->net_io_data_received(sock, buffer, count, (sockaddr*)&saddr, sockaddr_len);
		else
			PLOG_NET_WARNING("io-serv", "socket_io_service.on_kevent -- (socket %d) user is NULL", sock->get_handle());
	}
}
#endif
#endif
//--------------------------------------
