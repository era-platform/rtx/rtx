﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//--------------------------------------
//
//--------------------------------------
enum net_ice_cand_type_t
{
	net_ice_cand_type_unknown,
	net_ice_cand_type_host,
	net_ice_cand_type_srflx,
	net_ice_cand_type_prflx,
	net_ice_cand_type_relay
};
//enum tnet_socket_type_e socket_type;
//struct tnet_socket_s;
//--------------------------------------
//
//--------------------------------------
uint32_t net_ice_utils_get_priority(net_ice_cand_type_t type, uint16_t local_pref, bool is_rtp);
int net_ice_utils_compute_foundation(char* foundation, int size);
//int net_ice_utils_create_sockets(/*enum tnet_socket_type_e*/ int socket_type, const char* local_ip, /*struct tnet_socket_s**/SOCKET* socket_rtp, /*struct tnet_socket_s**/SOCKET* socket_rtcp);
int net_ice_utils_set_ufrag(char** ufrag);
int net_ice_utils_set_pwd(char** pwd);
void hmac_google(const uint8_t* key, int key_len, const uint8_t* input, int in_len, uint8_t* output, int out_len);
//--------------------------------------

