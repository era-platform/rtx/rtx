﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "net/ip_packet.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
ip_packet::ip_packet()
{
	reset();
}
ip_packet::~ip_packet()
{
}
//------------------------------------------------------------------------------
// распаковка ip-пакета из буфера.
//------------------------------------------------------------------------------
bool					ip_packet::read_packet(const void* data, uint32_t length)
{
	if (length < FIXED_IP_HEADER_LENGTH)
	{
		return false;
	}

	if (length > FIXED_IP_HEADER_LENGTH + BUFFER_SIZE)
	{
		return false;
	}

	reset();

	const uint8_t* pdata = (const uint8_t*) data;

	uint32_t size = 0;

	//------------------------------

	uint8_t chek1 = *(pdata);
	uint8_t chek2 = 0;
	memcpy(&chek2, &chek1, sizeof(uint8_t));

	m_hl = (chek1 & 0x0F) << 2;
	m_v = (chek2 & 0xF0) >> 4;

	size += sizeof(uint8_t);

	//------------------------------

	m_tos = *(pdata + size);
	size += sizeof(uint8_t);

	//------------------------------

	m_len = *(uint16_t*)(pdata + size);
	m_len = htons(m_len);
	size += sizeof(uint16_t);

	//------------------------------

	m_id = *(uint16_t*)(pdata + size);
	m_id = htons(m_id);
	size += sizeof(uint16_t);

	//------------------------------

	uint8_t chek3 = *(pdata + size);
	m_flags = (chek3 & 0xE0) >> 5;

	uint16_t chek4 = *(uint16_t*)(pdata + size);
	m_off = (chek4 & 0xE000) >> 3;

	size += sizeof(uint16_t);

	//------------------------------

	m_ttl = *(pdata + size);
	size += sizeof(uint8_t);

	//------------------------------

	m_p = *(pdata + size);
	size += sizeof(uint8_t);

	//------------------------------

	m_sum = *(uint16_t*)(pdata + size);
	m_sum = htons(m_sum);
	size += sizeof(uint16_t);

	//------------------------------

	uint32_t chek5 = *(uint32_t*)(pdata + size);
	m_src.set_native(&chek5, sizeof(uint32_t)); // 32
	size += sizeof(uint32_t);

	//------------------------------

	uint32_t chek6 = *(uint32_t*)(pdata + size);
	m_dst.set_native(&chek6, sizeof(uint32_t)); // 32
	size += sizeof(uint32_t);

	//------------------------------

	m_buff_length = length - size;
	memcpy(m_buff, pdata + size, m_buff_length);

	return true;
}
//------------------------------------------------------------------------------
// упаковка ip-пакета в буфер.
//------------------------------------------------------------------------------
uint32_t				ip_packet::write_packet(void* data, uint32_t size) const
{
	if ((data == NULL) || (size == 0))
	{
		return 0;
	}

	if (size < FIXED_IP_HEADER_LENGTH)
	{
		return 0;
	}

	uint8_t* pdata = (uint8_t*)data;

	//-------------------------

	uint32_t lenght = 0;

	//------------------------------

	uint8_t chek1 = (m_v << 4) | (m_hl >> 2);
	memcpy(pdata, &chek1, 1);

	lenght += 1;

	//------------------------------

	memcpy(pdata + lenght, &m_tos, sizeof(m_tos));
	lenght += sizeof(m_tos);

	//------------------------------

	uint16_t chek2 = ntohs(m_len);
	memcpy(pdata + lenght, &chek2, sizeof(m_len));
	lenght += sizeof(m_len);

	//------------------------------

	uint16_t chek3 = ntohs(m_id);
	memcpy(pdata + lenght, &chek3, sizeof(m_id));
	lenght += sizeof(m_id);

	//------------------------------

	uint16_t chek4 = (m_flags << 13) | m_off;
	chek4 = ntohs(chek4);
	memcpy(pdata + lenght, &chek4, sizeof(chek4));
	lenght += sizeof(chek4);

	//------------------------------

	memcpy(pdata + lenght, &m_ttl, sizeof(m_ttl));
	lenght += sizeof(m_ttl);

	//------------------------------

	memcpy(pdata + lenght, &m_p, sizeof(m_p));
	lenght += sizeof(m_p);

	//------------------------------

	uint16_t chek5 = ntohs(m_sum);
	memcpy(pdata + lenght, &chek5, sizeof(m_sum));
	lenght += sizeof(m_sum);

	//------------------------------

	memcpy(pdata + lenght, m_src.get_native(), m_src.get_native_len()); // 4
	lenght += m_src.get_native_len();

	//------------------------------

	memcpy(pdata + lenght, m_dst.get_native(), m_dst.get_native_len()); // 4
	lenght += m_dst.get_native_len();

	//------------------------------

	if (size > lenght)
	{
		uint32_t size_write = size - lenght;
		if (m_buff_length > size_write)
		{
			memcpy(pdata + lenght, m_buff, size_write);
			lenght += size_write;
		}
		else
		{
			memcpy(pdata + lenght, m_buff, m_buff_length);
			lenght += m_buff_length;
		}
	}

	return lenght;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void					ip_packet::reset()
{
	m_buff_length = 0;

	m_hl = 0;
	m_v = 0;
	m_tos = 0;
	m_len = FIXED_IP_HEADER_LENGTH;
	m_id = 0;
	m_flags = 0;
	m_off = 0;
	m_ttl = 0;
	m_p = 0;
	m_sum = 0;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const uint8_t*			ip_packet::get_buffer() const
{
	return m_buff;
}
uint32_t				ip_packet::get_buffer_length() const
{
	return m_buff_length;
}
uint32_t				ip_packet::set_buffer(const void* data, uint32_t length)
{
	if ((data == NULL) || (length == 0))
	{
		return 0;
	}

	if (length > BUFFER_SIZE)
	{
		length = BUFFER_SIZE;
	}

	memcpy(m_buff, data, length);
	m_buff_length = length;

	m_len = uint16_t(FIXED_IP_HEADER_LENGTH + m_buff_length);

	return m_buff_length;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint16_t				ip_packet::in_cksum(uint16_t* addr, int len)
{
	int nleft = len;
	int sum = 0;
	unsigned short *w = addr;
	uint16_t answer = 0;

	while (nleft > 1)
	{
		sum += *w++;
		nleft -= 2;
	}

	if (nleft == 1)
	{
		*(unsigned char *)(&answer) = *(unsigned char *)w;
		sum += answer;
	}

	sum = (sum >> 16) + (sum & 0xFFFF);
	sum += (sum >> 16);
	answer = uint16_t(~sum);
	return (answer);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint8_t					ip_packet::get_header_length() const
{
	return m_hl;
}
void					ip_packet::set_header_length(uint8_t value)
{
	m_hl = value;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint8_t					ip_packet::get_ip_version() const
{
	return m_v;
}
void					ip_packet::set_ip_version(uint8_t value)
{
	m_v = value;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint8_t					ip_packet::get_type_of_service() const
{
	return m_tos;
}
void					ip_packet::set_type_of_service(uint8_t value)
{
	m_tos = value;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//uint16_t				ip_packet::get_total_length() const
//{
//	return m_len;
//}
//void					ip_packet::set_total_length(uint16_t value)
//{
//	m_len = value;
//}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint16_t				ip_packet::get_identification() const
{
	return m_id;
}
void					ip_packet::set_identification(uint16_t value)
{
	m_id = value;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint8_t					ip_packet::get_flags() const
{
	return m_flags;
}
void					ip_packet::set_flags(uint8_t value)
{
	m_flags = value;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint16_t				ip_packet::get_fragment_offset() const
{
	return m_off;
}
void					ip_packet::set_fragment_offset(uint16_t value)
{
	m_off = value;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint8_t					ip_packet::get_time_to_live() const
{
	return m_ttl;
}
void					ip_packet::set_time_to_live(uint8_t value)
{
	m_ttl = value;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint8_t					ip_packet::get_protocol() const
{
	return m_p;
}
void					ip_packet::set_protocol(uint8_t value)
{
	m_p = value;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint16_t				ip_packet::get_checksum() const
{
	return m_sum;
}
void					ip_packet::set_checksum(uint16_t value)
{
	m_sum = value;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const ip_address_t*		ip_packet::get_source_address() const
{
	return &m_src;
}
void					ip_packet::set_source_address(const ip_address_t* value)
{
	m_src.copy_from(value);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const ip_address_t*		ip_packet::get_destination_address() const
{
	return &m_dst;
}
void					ip_packet::set_destination_address(const ip_address_t* value)
{
	m_dst.copy_from(value);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
