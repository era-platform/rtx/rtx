﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "net/ip_address.h"
#include "net/ip_address4.h"
#include "net/ip_address6.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void ip_address_t::copy_from(const ip_address_t* addr)
{
	if (addr != nullptr)
	{
		m_family = addr->m_family;

		if (m_family == E_IP_ADDRESS_FAMILY_IPV4)
			m_ip4.copy_from(&addr->m_ip4);
		else
			m_ip6.copy_from(&addr->m_ip6);
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void ip_address_t::set_native(const void* native, uint32_t size)
{
	if ((native != nullptr) && (size != 0))
	{
		if (size == ip_address4::get_native_len())
		{
			m_family = E_IP_ADDRESS_FAMILY_IPV4;
			m_ip4.set_native(native);
		}
		else
		{
			m_family = E_IP_ADDRESS_FAMILY_IPV6;
			m_ip6.set_native(native);
		}
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void ip_address_t::parse(const char* str)
{
	if (str != nullptr)
	{
		// если это строка IP4
        const char* ptr = strchr(str, '.');
		if (ptr != nullptr)
		{
			m_ip4.parse(str);
			m_family = E_IP_ADDRESS_FAMILY_IPV4;
			return;
		}

		// если это строка IP6
        ptr = strchr(str, ':');
		if (ptr != nullptr)
		{
			m_ip6.parse(str);
			m_family = E_IP_ADDRESS_FAMILY_IPV6;
			return;
		}

		// если дошли до сюда, значит в строке лежит какое-то говно.
		// чтобы объект вел себя ожидаемым образом, нужно сбросить все значения на дефолтные.

		m_family = E_IP_ADDRESS_FAMILY_IPV4;
		m_ip4.parse(str);
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const char* ip_address_t::to_string() const
{
	if (m_family == E_IP_ADDRESS_FAMILY_IPV4)
		return m_ip4.to_string();
	else
		return m_ip6.to_string();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool ip_address_t::equals(const ip_address_t* ip) const
{
	if (ip == nullptr)
		return false;

	if (m_family != ip->m_family)
		return false;

	if (m_family == E_IP_ADDRESS_FAMILY_IPV4)
		return m_ip4.equals(&ip->m_ip4);

	return m_ip6.equals(&ip->m_ip6);;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const bool ip_address_t::empty() const
{
	if (m_family == E_IP_ADDRESS_FAMILY_IPV4)
	{
		return m_ip4.empty();
	}
	else
	{
		return m_ip6.empty();
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const bool ip_address_t::fill_suitable_local_mask(char* str, uint32_t len) const
{
	if (m_family == E_IP_ADDRESS_FAMILY_IPV4)
	{
		return m_ip4.fill_suitable_local_mask(str, len);
	}
	else
	{
		return m_ip6.fill_suitable_local_mask(str, len);
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
