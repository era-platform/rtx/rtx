﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "net/socket_address4.h"
#include "net/ip_address4.h"
//#include <sstream>

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
					socket_address4::socket_address4()
{
	first_init();
}
					socket_address4::socket_address4(const socket_address4* saddr)
{
	first_init();
	copy_from(saddr);
}
					socket_address4::socket_address4(const void* native)
{
	first_init();

	set_native(native);
}
					socket_address4::socket_address4(const ip_address4* ipaddr, uint16_t port)
{
	first_init();

	set_ip_address_with_port(ipaddr, port);
}
					socket_address4::~socket_address4()
{
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void				socket_address4::copy_from(const socket_address4* saddr)
{
	if (saddr != NULL)
	{
		m_ip_address.copy_from(&saddr->m_ip_address);
		m_port = saddr->m_port;
		memcpy(&m_native, &saddr->m_native, sizeof(m_native));
		memcpy(m_text, saddr->m_text, sizeof(m_text));
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void				socket_address4::set_native(const void* native)
{
	if (native != NULL)
	{
		memcpy(&m_native, native, sizeof(m_native));
		extract_native();
		update_text();
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void				socket_address4::set_ip_address_with_port(const ip_address4* ipaddr, uint16_t port)
{
	if (ipaddr != NULL)
	{
		m_ip_address.copy_from(ipaddr);
		m_port = port;

		fill_native();

		update_text();
	}
}
void				socket_address4::set_ip_address(const ip_address4* ipaddr)
{
	if (ipaddr != NULL)
	{
		m_ip_address.copy_from(ipaddr);

		fill_native();

		update_text();
	}
}
void				socket_address4::set_port(uint16_t port)
{
	m_port = port;

	fill_native();

	update_text();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const void*			socket_address4::get_native() const
{
	return &m_native;
}
uint32_t			socket_address4::get_native_len()
{
	return sizeof(sockaddr_in);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const ip_address4*	socket_address4::get_ip_address() const
{
	return &m_ip_address;
}
uint16_t			socket_address4::get_port() const
{
	return m_port;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const char*			socket_address4::to_string() const
{
	return m_text;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void				socket_address4::first_init()
{
	m_port = 0;
	fill_native();
	update_text();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void				socket_address4::fill_native()
{
	m_native.sin_family = AF_INET;

	const in_addr* paddr = (const in_addr*) m_ip_address.get_native();
	m_native.sin_addr = *paddr;

	m_native.sin_port = htons(m_port);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void				socket_address4::extract_native()
{
	m_port = ntohs(m_native.sin_port);

	m_ip_address.set_native(&m_native.sin_addr);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void				socket_address4::update_text()
{
	std_snprintf(m_text, ARRAY_LEN(m_text), "%s:%u", m_ip_address.to_string(), m_port);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool				socket_address4::equals(const socket_address4* saddr)
{
	if (saddr == NULL)
		return false;

	if (!m_ip_address.equals(&saddr->m_ip_address))
		return false;

	if (m_port != saddr->m_port)
		return false;

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
