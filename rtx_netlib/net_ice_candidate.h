﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "net_stun_message.h"
#include "net/ip_address.h"
#include "net/net_socket.h"
#include "net_ice_utils.h"
//--------------------------------------
//
//--------------------------------------
#define NET_ICE_CANDIDATE_TRANSPORT_UDP		"UDP"
#define NET_ICE_CANDIDATE_TRANSPORT_TCP		"TCP"
#define NET_ICE_CANDIDATE_TRANSPORT_TLS		"TLS"
#define NET_ICE_CANDIDATE_TRANSPORT_SCTP	"SCTP"
#define NET_ICE_CANDIDATE_TRANSPORT_WS		"WS"
#define NET_ICE_CANDIDATE_TRANSPORT_WSS		"WSS"

#define NET_ICE_CANDIDATE_TYPE_HOST			"host"
#define NET_ICE_CANDIDATE_TYPE_SRFLX		"srflx"
#define NET_ICE_CANDIDATE_TYPE_PRFLX		"prflx"
#define NET_ICE_CANDIDATE_TYPE_RELAY		"relay"

#define NET_ICE_CANDIDATE_PREF_HOST			126
#define NET_ICE_CANDIDATE_PREF_SRFLX		100
#define NET_ICE_CANDIDATE_PREF_PRFLX		110
#define NET_ICE_CANDIDATE_PREF_RELAY		0

#define NET_ICE_CANDIDATE_COMPID_RTP		1
#define NET_ICE_CANDIDATE_COMPID_RTCP		2

#define NET_ICE_CANDIDATE_FOUND_SIZE_PREF	8

#define NET_ICE_SOFTWARE "IM-client/OMA1.0 doubango/v0.0.0"
//--------------------------------------
//
//--------------------------------------
enum net_ice_transport_type_t
{
	net_ice_transport_udp,
	net_ice_transport_tcp,
	net_ice_transport_tls,
	net_ice_transport_ws,
	net_ice_transport_wss,
	net_ice_transport_invalid = -1
};
//--------------------------------------
//
//--------------------------------------
class net_ice_candidate_t
{
	rtl::Logger* m_log;
	net_ice_cand_type_t m_type;
	uint8_t m_foundation[33];				// 1*32ice-char
	uint32_t m_comp_id;						// 1*5DIGIT
	net_ice_transport_type_t m_transport;
	uint32_t m_priority;					// 1*10DIGIST [1 - (2**31 - 1)]
	ip_address_t m_conn_address;
	uint16_t m_conn_port;
	
	struct ice_param_t
	{
		char* name;
		char* value;
	};
	rtl::ArrayT<ice_param_t> m_extension_att_list;

	bool m_is_ice_jingle;
	bool m_is_rtp;
	bool m_is_video;
	uint16_t m_local_pref;					// [0 - 65535]
	
	char* m_ufrag;
	char* m_pwd;

	net_socket* m_socket;

	struct ice_stun_params_t
	{
		char* nonce;
		char* realm;
		uint8_t transac_id[NET_STUN_TRANSACID_SIZE];
		ip_address_t srflx_addr;
		uint16_t srflx_port;
	};
	
	ice_stun_params_t m_stun;

	rtl::MemoryStream m_buffer;

public:
	RTX_NET_API net_ice_candidate_t(rtl::Logger* log);
	RTX_NET_API net_ice_candidate_t(const net_ice_candidate_t& candidate);
	RTX_NET_API ~net_ice_candidate_t();

	void initialize(net_ice_cand_type_t type_e, net_socket* socket, bool is_ice_jingle, bool is_rtp, bool is_video, const char* ufrag, const char* pwd, const char *foundation);

	net_ice_cand_type_t get_cand_type() const { return m_type; }
	void set_cand_type(net_ice_cand_type_t type) { m_type = type; }
	
	void set_credential(const char* ufrag, const char* pwd);
	const char* get_ufrag() const {	return m_ufrag ? m_ufrag : get_att_value("username"); }
	const char* get_pwd() const { return m_pwd ? m_pwd : get_att_value("password"); }

	void set_rflx_addr(const ip_address_t* address, uint16_t port);
	const ip_address_t* get_srflx_address() const { return &m_stun.srflx_addr; }
	uint16_t get_srflx_port() const { return m_stun.srflx_port; }
	
	const char* get_att_value(const char* att_name) const;
	int set_local_pref(uint16_t local_pref);

	int send_stun_bind_request(const ip_address_t* address, uint16_t port, const char* username, const char* password);
	int process_stun_response(const net_stun_response_t* response, net_socket_t* fd);


	const char* get_foundation() const { return (const char*)m_foundation; }
	void set_foundation(const void* foundation, int len) { int l = MIN(len,32); memcpy(m_foundation, foundation, l); m_foundation[l] = 0; }

	uint32_t get_component_id() const { return m_comp_id; }
	void set_component_id(uint32_t id) { m_comp_id = id; }
	
	void set_transport(net_ice_transport_type_t ttype) { m_transport = ttype; }
	net_ice_transport_type_t get_transport() const { return m_transport; }

	void add_ice_param(const char* name, const char* value) { int_add_ice_param(rtl::strdup(name), rtl::strdup(value)); }

	void set_priority(uint32_t priority) { m_priority = priority; }
	uint32_t get_priority() const { return m_priority; }
	const ip_address_t* get_connection_address() const { return &m_conn_address; }
	void set_connection_address(const ip_address_t* address) { m_conn_address.copy_from(address); }
	uint16_t get_connection_port() const { return m_conn_port; }
	void set_connection_port(uint16_t port) { m_conn_port = port; }

	uint16_t get_local_pref() const { return m_local_pref; }
	bool is_rtp() const { return m_is_rtp; }
	bool is_ice_jingle() const { return m_is_ice_jingle; }

	net_socket* get_socket() const { return m_socket; }
	
	int encode(char* buffer, int size) const;
	static net_ice_candidate_t* parse(const char* str, rtl::Logger* log);
	
private:
	bool have_param(const char* name) const;
	net_stun_message_t* create_bind_request(const char* username, const char* password);
	void int_add_ice_param(char* name, char* value);
};
//--------------------------------------
//
//--------------------------------------
typedef rtl::ArrayT<net_ice_candidate_t*> net_ice_candidate_list_t;
//--------------------------------------
//
//--------------------------------------
net_ice_candidate_t* net_ice_candidate_find_by_fd(net_ice_candidate_list_t& candidates, net_socket* fd);
//--------------------------------------
/* TNET_ICE_CANDIDATE_H */
//--------------------------------------
