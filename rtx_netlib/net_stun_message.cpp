﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "net_stun_message.h"
#include "net_ice_utils.h"
#include "std_crypto.h"
//#include "srtp_hmac.h"

//--------------------------------------
//
//--------------------------------------
net_stun_message_t::net_stun_message_t()
{
	m_type = (net_stun_message_type_t)0;
	m_length = 0;
	m_cookie = 0;
	memset(m_transaction_id, 0, sizeof m_transaction_id);

	m_fingerprint = 0;
	m_integrity = 0;
	m_dontfrag = 0;
	m_nointegrity = 0;

	m_username = nullptr;
	m_password = nullptr;
	m_realm = nullptr;
	m_nonce = nullptr;

	m_fingerprint_value = 0;
	memset(m_msg_int_hmac, 0, sizeof m_msg_int_hmac);
}
//--------------------------------------
//
//--------------------------------------
net_stun_message_t::net_stun_message_t(net_stun_message_type_t type, const char* username, const char* password)
{
	m_type = (net_stun_message_type_t)type;
	m_length = 0;
	m_cookie = NET_STUN_MAGIC_COOKIE;
	crypto_get_random(m_transaction_id, 12);

	m_fingerprint = 1;
	m_integrity = 0;
	m_dontfrag = 0;
	m_nointegrity = 0;

	m_username = rtl::strdup(username);
	m_password = rtl::strdup(password);
	m_realm = nullptr;
	m_nonce = nullptr;
}
//--------------------------------------
//
//--------------------------------------
net_stun_message_t::~net_stun_message_t()
{
	FREE(m_username);
	FREE(m_password);
	FREE(m_realm);
	FREE(m_nonce);

	for (int i = 0; i < m_attributes.getCount(); i++)
	{
		net_stun_cleanup_attribute(m_attributes.getAt(i));
	}
	m_attributes.clear();
}
//--------------------------------------
//
//--------------------------------------
net_stun_attribute_t& net_stun_message_t::find_attribute(net_stun_attribute_type_t type)
{
	for (int i = 0; i < m_attributes.getCount(); i++)
	{
		net_stun_attribute_t& attr = m_attributes[i];
		if (attr.type == type)
			return attr;
	}

	return m_attributes.getEmpty();
}
//--------------------------------------
//
//--------------------------------------
const net_stun_attribute_t& net_stun_message_t::find_attribute(net_stun_attribute_type_t type) const
{
	for (int i = 0; i < m_attributes.getCount(); i++)
	{
		const net_stun_attribute_t& attr = m_attributes[i];
		if (attr.type == type)
			return attr;
	}

	return m_attributes.getEmpty();
}
//--------------------------------------
//
//--------------------------------------
void net_stun_message_t::set_flags(bool fingerprint, bool integrity, bool dontfrag, bool nointegrity)
{
	m_fingerprint = fingerprint;
	m_integrity = integrity;
	m_dontfrag = dontfrag;
	m_nointegrity = nointegrity;
}
//--------------------------------------
//
//--------------------------------------
const uint8_t* net_stun_message_t::generate_transaction_id()
{
	crypto_get_random(m_transaction_id, NET_STUN_TRANSACID_SIZE);
	return m_transaction_id;
}
//--------------------------------------
//
//--------------------------------------
bool net_stun_message_t::has_attribute(net_stun_attribute_type_t type) const
{
	for (int i = 0; i < m_attributes.getCount(); i++)
	{
		if (m_attributes[i].type == type)
		{
			return true;
		}
	}

	return false;
}
//--------------------------------------
//
//--------------------------------------
bool net_stun_message_t::add_attribute_address(net_stun_attribute_type_t type, uint16_t port, in_addr ipv4)
{
	net_stun_attribute_t attr;
	memset(&attr, 0, sizeof attr);

	attr.type = type;
	attr.length = 8;
	attr.address.family = stun_ipv4;
	attr.address.port = port;
	attr.address.ipv4 = ipv4;
	
	m_attributes.add(attr);

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool net_stun_message_t::add_attribute_address(net_stun_attribute_type_t type, uint16_t port, in6_addr ipv6)
{
	net_stun_attribute_t attr;
	memset(&attr, 0, sizeof attr);

	attr.type = type;
	attr.length = 20;
	attr.address.family = stun_ipv6;
	attr.address.port = port;
	attr.address.ipv6 = ipv6;

	m_attributes.add(attr);

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool net_stun_message_t::add_attribute_error_code(net_stun_attribute_type_t type, uint8_t err_class, uint8_t err_code, const char* reason_phrase)
{
	net_stun_attribute_t attr;
	memset(&attr, 0, sizeof attr);

	attr.type = type;
	attr.length = uint16_t(2 + strlen(reason_phrase));
	attr.error.err_class = err_class;
	attr.error.number = err_code;
	attr.error.reason_phrase = NEW char[attr.length];
	strcpy(attr.error.reason_phrase, reason_phrase);

	m_attributes.add(attr);

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool net_stun_message_t::add_attribute_data(net_stun_attribute_type_t type, const void* data_ptr, int size)
{
	net_stun_attribute_t attr;
	memset(&attr, 0, sizeof attr);

	attr.type = type;
	
	if (size > 0)
	{
		attr.length = size;
		attr.data_ptr = NEW uint8_t[size];
		memcpy(attr.data_ptr, data_ptr, size);
	}

	m_attributes.add(attr);

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool net_stun_message_t::add_attribute_int8(net_stun_attribute_type_t type, uint8_t value)
{
	net_stun_attribute_t attr;
	memset(&attr, 0, sizeof attr);

	attr.type = type;
	attr.length = 1;
	attr.value8[0] = value;

	m_attributes.add(attr);

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool net_stun_message_t::add_attribute_int16(net_stun_attribute_type_t type, uint16_t value)
{
	net_stun_attribute_t attr;
	memset(&attr, 0, sizeof attr);

	attr.type = type;
	attr.length = 2;
	attr.value16[0] = value;

	m_attributes.add(attr);

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool net_stun_message_t::add_attribute_int32(net_stun_attribute_type_t type, uint32_t value)
{
	net_stun_attribute_t attr;
	memset(&attr, 0, sizeof attr);

	attr.type = type;
	attr.length = 4;
	attr.value32[0] = value;

	m_attributes.add(attr);

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool net_stun_message_t::add_attribute_int64(net_stun_attribute_type_t type, uint64_t value)
{
	net_stun_attribute_t attr;
	memset(&attr, 0, sizeof attr);

	attr.type = type;
	attr.length = 8;
	attr.value64 = value;

	m_attributes.add(attr);

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool net_stun_message_t::set_attribute_address(net_stun_attribute_type_t type, uint16_t port, in_addr ipv4)
{
	net_stun_attribute_t& attr = find_attribute(type);
	
	if (attr.type == net_stun_reserved)
		return false;

	attr.address.port = port;
	attr.address.ipv4 = ipv4;

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool net_stun_message_t::set_attribute_address(net_stun_attribute_type_t type, uint16_t port, in6_addr ipv6)
{
	net_stun_attribute_t& attr = find_attribute(type);
	
	if (attr.type == net_stun_reserved)
		return false;

	attr.address.port = port;
	attr.address.ipv6 = ipv6;

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool net_stun_message_t::set_attribute_data(net_stun_attribute_type_t type, const void* data_ptr, int size)
{
	net_stun_attribute_t& attr = find_attribute(type);
	
	if (attr.type == net_stun_reserved)
		return false;

	if (attr.data_ptr != nullptr)
	{
		DELETEAR(attr.data_ptr);
		attr.data_ptr = nullptr;
		attr.length = 0;
	}

	if (data_ptr != nullptr && size > 0)
	{
		attr.data_ptr = NEW uint8_t[size+1];
		memcpy(attr.data_ptr, data_ptr, size);
		attr.data_ptr[size] = 0;
		attr.length = size;
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool net_stun_message_t::set_attribute_int8(net_stun_attribute_type_t type, uint8_t value)
{
	net_stun_attribute_t& attr = find_attribute(type);
	
	if (attr.type == net_stun_reserved)
		return false;

	attr.value8[0] = value;

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool net_stun_message_t::set_attribute_int16(net_stun_attribute_type_t type, uint16_t value)
{
	net_stun_attribute_t& attr = find_attribute(type);
	
	if (attr.type == net_stun_reserved)
		return false;

	attr.value16[0] = value;

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool net_stun_message_t::set_attribute_int32(net_stun_attribute_type_t type, uint32_t value)
{
	net_stun_attribute_t& attr = find_attribute(type);
	
	if (attr.type == net_stun_reserved)
		return false;

	attr.value32[0] = value;

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool net_stun_message_t::set_attribute_int64(net_stun_attribute_type_t type, uint64_t value)
{
	net_stun_attribute_t& attr = find_attribute(type);
	
	if (attr.type == net_stun_reserved)
		return false;

	attr.value64 = value;

	return true;
}
//--------------------------------------
//
//--------------------------------------
void net_stun_message_t::remove_attribute(net_stun_attribute_type_t type)
{
	for (int i = 0; i < m_attributes.getCount(); i++)
	{
		if (m_attributes[i].type == type)
		{
			net_stun_cleanup_attribute(m_attributes[i]);
			m_attributes.removeAt(i);
			return;
		}
	}
}
//--------------------------------------
//
//--------------------------------------
const net_stun_attribute_t& net_stun_message_t::get_attribute(net_stun_attribute_type_t type) const
{
	static net_stun_attribute_t empty = { 0 };

	for (int i = 0; i < m_attributes.getCount(); i++)
	{
		if (m_attributes[i].type == type)
			return m_attributes[i];
	}

	return empty;
}
//--------------------------------------
//
//--------------------------------------
short net_stun_message_t::get_errorcode() const
{
	const net_stun_attribute_t& error = get_attribute(net_stun_error_code);
	if (error.type == net_stun_error_code)
	{
		return ((error.error.err_class * 100) + error.error.number);
	}

	return -1;
}
//--------------------------------------
//
//--------------------------------------
const char* net_stun_message_t::get_realm() const
{
	const net_stun_attribute_t& attr = get_attribute(net_stun_realm);
	if (attr.type == net_stun_realm)
	{
		return (const char*)attr.data_ptr;
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
const char* net_stun_message_t::get_nonce() const
{
	const net_stun_attribute_t& attr = get_attribute(net_stun_nonce);
	if (attr.type == net_stun_nonce)
	{
		return (const char*)attr.data_ptr;
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
int32_t net_stun_message_t::get_lifetime() const
{
	const net_stun_attribute_t& attr = get_attribute(net_stun_lifetime);
	if (attr.type == net_stun_lifetime)
	{
		return attr.value32[0];
	}

	return 0;
}
//--------------------------------------
//
//--------------------------------------
void net_stun_message_t::set_lifetime(int32_t lifetime)
{
	net_stun_attribute_t& attr = find_attribute(net_stun_lifetime);
	
	if (attr.type == net_stun_lifetime)
	{
		attr.value32[0] = lifetime;
	}
	else
	{
		add_attribute_int32(net_stun_lifetime, lifetime);
	}
}
//--------------------------------------
//
//--------------------------------------
int net_stun_message_t::write_message_header(uint8_t* buffer, int size) const
{
	// STUN Message Type 
	*(uint16_t*)buffer = htons(m_type);
	buffer += 2;
	size -= 2;
	
	// Message Length ==> Will be updated after attributes have been added. */
	*(uint16_t*)buffer = 0;
	buffer += 2;
	size -= 2;

	// Magic Cookie
	*(uint32_t*)buffer = htonl(m_cookie);
	buffer += 4;
	size -= 4;

	// Transaction ID (96 bits==>16bytes)
	memcpy(buffer, m_transaction_id, NET_STUN_TRANSACID_SIZE);
	buffer += NET_STUN_TRANSACID_SIZE;
	size -= NET_STUN_TRANSACID_SIZE;

	return 20;
}
//--------------------------------------
// Attributes
//--------------------------------------
int net_stun_message_t::write_attributes(uint8_t* buffer, int size) const
{
	int len;
	uint8_t* initial_ptr = buffer;

	for (int i = 0; i < m_attributes.getCount(); i++)
	{
		const net_stun_attribute_t& attr = m_attributes[i];
		if ((len = net_stun_write_attribute(attr, buffer, size)) > 0)
		{
			buffer += len;
			size -= len;
		}
		else
		{
			return 0;
		}
	}

	return int(buffer - initial_ptr);
}
//--------------------------------------
//
//--------------------------------------
int net_stun_message_t::write_message_header(rtl::MemoryStream& stream) const
{
	int start_length = int(stream.getLength());
	// STUN Message Type 
	int16_t type = htons(m_type);
	stream.write(&type, sizeof(int16_t));
	// Message Length ==> Will be updated after attributes have been added. */
	int16_t length = 0;
	stream.write(&length, sizeof(int16_t));
	// Magic Cookie
	int32_t magic_cookie = htonl(m_cookie);
	stream.write(&magic_cookie, sizeof(int32_t));
	// Transaction ID (96 bits==>16bytes)
	stream.write(m_transaction_id, NET_STUN_TRANSACID_SIZE);

	return int(stream.getLength() - start_length);
}
//--------------------------------------
// Attributes
//--------------------------------------
int net_stun_message_t::write_attributes(rtl::MemoryStream& stream)const 
{
	int start_length = int(stream.getLength());

	for (int i = 0; i < m_attributes.getCount(); i++)
	{
		const net_stun_attribute_t& attr = m_attributes[i];
		net_stun_write_attribute(attr, stream);
	}

	return int(stream.getLength() - start_length);
}
//--------------------------------------
//
//--------------------------------------
bool net_stun_message_t::calculate_message_integrity(const uint8_t* msg, int length, uint8_t digest[SHA1_HASH_SIZE]) const
{
	if (m_username && m_realm && m_password)
	{
		// long term
		char keystr[4096];
		md5_context_t md5;
		uint8_t md5_digest[MD5_HASH_SIZE];
		int len = std_snprintf(keystr, 4096, "%s:%s:%s", m_username, m_realm, m_password);
		keystr[len] = 0;
		md5.update(keystr, len);
		md5.final(md5_digest);

		hmac_google((const uint8_t*)keystr, len, msg, length, digest, SHA1_HASH_SIZE);
	}
	else
	{
		//char* bin_dump = NEW char[length*3];
		//char* cp = bin_dump;

		//for (int i = 0; i < length; i++)
		//{
		//	if (i >= 16 && (i % 16) == 0)
		//	{
		//		*cp++ = '\r';
		//		*cp++ = '\n';
		//	}

		//	uint8_t t1 = (msg[i] >> 4) & 0xF;
		//	uint8_t t2 = msg[i] & 0x0F;
		//	*cp++ = t1 <= 9 ? t1 + 0x30 : (t1 - 10) + 'A';
		//	*cp++ = t2 <= 9 ? t2 + 0x30 : (t2 - 10) + 'A';
		//}

		//*cp = 0;

		//Log.Info("STUN", "STUN message dump for check M-I size %d bytes\r\n%s", length, bin_dump);

		//DELETEAR(bin_dump);

		// short term
		int pass_len = m_password != nullptr ? int(strlen(m_password)) : 0;
		/*hmac_ctx_t hmac(pass_len, SHA1_HASH_SIZE);
		hmac.create((const uint8_t*)m_password, pass_len);
		hmac.compute(msg, length, digest, SHA1_HASH_SIZE);*/

		hmac_google((const uint8_t*)m_password, pass_len, msg, length, digest, SHA1_HASH_SIZE);

		//char c_hmac_str[41];

		//LOG_CALL("STUN", "password to calculate hmac %s hmac %s", m_password, hmac_to_string(digest, c_hmac_str, 41));
	}

	return true;
}
//--------------------------------------
// Serializes a STUN message as binary data.
//--------------------------------------
int net_stun_message_t::encode(uint8_t* buffer, int size) 
{
	uint8_t* initial_ptr = buffer;
	int len;
	uint32_t compute_integrity = m_integrity;

	if (size < 20 || (len = write_message_header(buffer, size)) == 0)
		return 0;

	buffer += len;
	size -= len;

	if ((len = write_attributes(buffer, size)) == 0)
		return 0;

	buffer += len;
	size -= len;

	/* AUTHENTICATION */
	if (m_realm && m_nonce)
	{
		// long term
		net_stun_attribute_t attr = { 0 };
		attr.type = net_stun_realm;
		attr.length = uint16_t(strlen(m_realm));
		attr.data_ptr = (uint8_t*)m_realm;
		
		if ((len = net_stun_write_attribute(attr, buffer, size)) == 0)
			return 0;

		buffer += len;
		size -= len;

		attr.type = net_stun_nonce;
		attr.length = uint16_t(strlen(m_nonce));
		attr.data_ptr = (uint8_t*)m_nonce;
		
		if ((len = net_stun_write_attribute(attr, buffer, size)) == 0)
			return 0;

		buffer += len;
		size -= len;
		
		compute_integrity = !m_nointegrity;
	}
	else if (m_password)
	{
		// short term
		compute_integrity = !m_nointegrity;
	}

	if (compute_integrity && m_username)
	{
		// long term
		net_stun_attribute_t attr = { 0 };
		attr.type = net_stun_username;
		attr.length = uint16_t(strlen(m_username));
		attr.data_ptr = (uint8_t*)m_username;
		
		if ((len = net_stun_write_attribute(attr, buffer, size)) == 0)
			return 0;

		buffer += len;
		size -= len;
	}

	// Message Length: The message length MUST contain the size, in bytes, of the message not including the 20-byte STUN header.

	// compute length for 'MESSAGE-INTEGRITY'
	// will be computed again to store the correct value
	uint16_t length = uint16_t((buffer - initial_ptr) - NET_STUN_HEADER_SIZE);

	if (compute_integrity)
		length += (NET_STUN_ATTR_HEADER_SIZE + SHA1_HASH_SIZE /* INTEGRITY VALUE*/);

	*(((uint16_t*)initial_ptr)+1) = htons(length);

	/* MESSAGE-INTEGRITY */
	if (compute_integrity)
	{
		/* RFC 5389 - 15.4.  MESSAGE-INTEGRITY
		   The MESSAGE-INTEGRITY attribute contains an HMAC-SHA1 [RFC2104] of the STUN message.

		   For long-term credentials ==> key = MD5(username ":" realm ":" SASLprep(password))
		   For short-term credentials ==> key = SASLprep(password)
		*/
		
		//uint8_t hmac[SHA1_HASH_SIZE];

		calculate_message_integrity(initial_ptr, int(buffer - initial_ptr), m_msg_int_hmac);

		// long term
		net_stun_attribute_t attr = { 0 };
		attr.type = net_stun_message_integrity;
		attr.length = SHA1_HASH_SIZE;
		attr.data_ptr = m_msg_int_hmac;

		if ((len = net_stun_write_attribute(attr, buffer, size)) == 0)
			return 0;

		buffer += len;
		size -= len;

		/*if (!add_attribute_data(net_stun_message_integrity, m_msg_int_hmac, SHA1_HASH_SIZE))
		{
			return 0;
		}

		buffer += SHA1_HASH_SIZE;
		size -= SHA1_HASH_SIZE;*/
	}

	/* FINGERPRINT */
	if (m_fingerprint)
	{
		//JINGLE_ICE
		/*	RFC 5389 - 15.5.  FINGERPRINT
			The FINGERPRINT attribute MAY be present in all STUN messages.  The
			value of the attribute is computed as the CRC-32 of the STUN message
			up to (but excluding) the FINGERPRINT attribute itself, XOR'ed with
			the 32-bit value 0x5354554e
		*/
		m_length = uint16_t(((buffer - initial_ptr) - NET_STUN_HEADER_SIZE)+8);
		*(((uint16_t*)initial_ptr)+1)  = htons(m_length);

		int len = int(buffer - initial_ptr);
		m_fingerprint_value = calculate_crc32(0, initial_ptr, len);
		m_fingerprint_value ^= 0x5354554e;
		net_stun_attribute_t attr = { 0 };
		attr.type = net_stun_fingerprint;
		attr.length = 4;
		attr.value32[0] = m_fingerprint_value;

		if ((len = net_stun_write_attribute(attr, buffer, size)) == 0)
			return 0;

		buffer += len;
		size -= len;

		//if (!add_attribute_int32(net_stun_fingerprint, m_fingerprint_value))
		//{
		//	return 0;
		//}

		//buffer += 4;
		//size -= 4;

//		len = buffer - initial_ptr;
//		Log.Binary("STUN-CRC", initial_ptr, len, "STUN MSG %d bytes FINGERPRINT %08x xored %08x", len, m_fingerprint_value ^ 0x5354554e, m_fingerprint_value);
	}
	else
	{
		// LENGTH
		m_length = uint16_t(((buffer - initial_ptr) - NET_STUN_HEADER_SIZE));
		*(((uint16_t*)initial_ptr)+1)  = htons(m_length);
	}

	return int(buffer - initial_ptr);
}
//--------------------------------------
// Serializes a STUN message as binary data.
//--------------------------------------
int net_stun_message_t::encode(rtl::MemoryStream& stream) 
{
	uint32_t compute_integrity = m_integrity;

	if (write_message_header(stream) == 0)
		return 0;

	if (write_attributes(stream) == 0)
		return 0;

	/* AUTHENTICATION */
	if (m_realm && m_nonce)
	{
		// long term
		net_stun_write_attribute_string(net_stun_realm, m_realm, stream);
		net_stun_write_attribute_string(net_stun_nonce, m_nonce, stream);
		
		compute_integrity = !m_nointegrity;
	}
	else if (m_password)
	{
		// short term
		compute_integrity = !m_nointegrity;
	}

	if (compute_integrity && m_username)
	{
		// long term
		net_stun_write_attribute_string(net_stun_username, m_username, stream);
	}

	// Message Length: The message length MUST contain the size, in bytes, of the message not including the 20-byte STUN header.

	// compute length for 'MESSAGE-INTEGRITY'
	// will be computed again to store the correct value
	uint16_t length = uint16_t(stream.getLength() - NET_STUN_HEADER_SIZE);

	if (compute_integrity)
		length += (2/* Type */ + 2 /* Length */+ SHA1_HASH_SIZE /* INTEGRITY VALUE*/);
	
	//stream.write_int16(2, htons(length));
	stream.setPosition(2);
	uint16_t length16 = htons(length);
	stream.write(&length16, sizeof(uint16_t));
	stream.seekEnd(0);

	/* MESSAGE-INTEGRITY */
	if (compute_integrity)
	{
		/* RFC 5389 - 15.4.  MESSAGE-INTEGRITY
		   The MESSAGE-INTEGRITY attribute contains an HMAC-SHA1 [RFC2104] of the STUN message.

		   For long-term credentials ==> key = MD5(username ":" realm ":" SASLprep(password))
		   For short-term credentials ==> key = SASLprep(password)
		*/
		
		//uint8_t hmac[SHA1_HASH_SIZE];

		calculate_message_integrity(stream.getBuffer(), int(stream.getLength()), m_msg_int_hmac);
		// long term
		net_stun_write_attribute_data(net_stun_message_integrity, m_msg_int_hmac, SHA1_HASH_SIZE, stream);
	}

	/* FINGERPRINT */
	if (m_fingerprint)
	{
		//JINGLE_ICE
		/*	RFC 5389 - 15.5.  FINGERPRINT
			The FINGERPRINT attribute MAY be present in all STUN messages.  The
			value of the attribute is computed as the CRC-32 of the STUN message
			up to (but excluding) the FINGERPRINT attribute itself, XOR'ed with
			the 32-bit value 0x5354554e
		*/
		// LENGTH
		m_length = uint16_t((stream.getLength() - NET_STUN_HEADER_SIZE) + 8);
		//stream.write_int16(2, htons(m_length));
		stream.setPosition(2);
		length16 = htons(m_length);
		stream.write(&length16, sizeof(uint16_t));
		stream.seekEnd(0);

		m_fingerprint_value = calculate_crc32(0/*TSK_PPPINITFCS32*/, (const uint8_t*)stream.getBuffer(), int(stream.getLength()));
		m_fingerprint_value ^= 0x5354554e;
		net_stun_write_attribute_int32(net_stun_fingerprint, m_fingerprint_value, stream);
	}
	else
	{
		// LENGTH
		m_length = uint16_t(stream.getLength() - NET_STUN_HEADER_SIZE);
		//stream.write_int16(2, htons(m_length));
		stream.setPosition(2);
		length16 = htons(m_length);
		stream.write(&length16, sizeof(uint16_t));
		stream.seekEnd(0);
	}

	return int(stream.getLength());
}
//--------------------------------------
// decode STUN message
//--------------------------------------
bool net_stun_message_t::decode(const uint8_t *data, int size)
{
	const uint8_t* data_end;

	//if (!NET_IS_STUN2_MSG(data, size))
	//{
	//	return false;
	//}

	//(((msg)) && ((size) >= NET_STUN_HEADER_SIZE) && (((msg)[0] & 0xc0) == 0x00) && ( *(uint32_t*)(msg+4) ==  0x42A41221))

	if (data == nullptr || size < NET_STUN_HEADER_SIZE)
	{
		return false;
	}
	
	if ((data[0] & 0xc0) != 0x00)
	{
		return false;
	}

	if (*(uint32_t*)(data+4) !=  0x42A41221)
	{
		return false; 
	}

	data_end = data + size;

	// Message Type
	m_type = (net_stun_message_type_t)ntohs(*(const uint16_t*)data);
	data += 2;

	// Message Length 
	m_length = ntohs(*(const uint16_t*)data);
	data += 2;

	// Check message validity
	if ((m_length + NET_STUN_HEADER_SIZE) != size)
	{
		return false;
	}

	// Magic Cookie ==> already set by the constructor and checked by TNET_IS_STUN2 
	data += 4;

	// Transaction ID
	memcpy(m_transaction_id, data, NET_STUN_TRANSACID_SIZE);
	data += NET_STUN_TRANSACID_SIZE;

	// Parse attributes

	size -= NET_STUN_HEADER_SIZE;
	
	int read_size = 0;

	while (data < data_end)
	{
		net_stun_attribute_t attribute = { 0 };
		int len = net_stun_read_attribute(attribute, data, size - read_size);
		
		if (len > 0)
		{
			int pad = len & 3;
			if (pad)
			{
				len += 4 - pad;
			}

			data += len;
			read_size += len;
			m_attributes.add(attribute);
		}
		else
		{
			return false;
		}
	}

	return true;
}

const char* get_net_stun_msg_name(net_stun_message_type_t type)
{
	const char* name = "unknown";

	switch (type)
	{
		case net_stun_binding_request:						name = "binding_request";					break;
		case net_stun_binding_indication:					name = "binding_indication";				break;
		case net_stun_binding_success_response:				name = "binding_success_response";			break;
		case net_stun_binding_error_response:				name = "binding_error_response";			break;

		case net_stun_allocate_request:						name = "allocate_request";					break;
		case net_stun_allocate_indication:					name = "allocate_indication";				break;
		case net_stun_allocate_success_response:			name = "allocate_success_response";			break;
		case net_stun_allocate_error_response:				name = "allocate_error_response";			break;

		case net_stun_refresh_request:						name = "refresh_request";					break;
		case net_stun_refresh_indication:					name = "refresh_indication";				break;
		case net_stun_refresh_success_response:				name = "refresh_success_response";			break;
		case net_stun_refresh_error_response:				name = "refresh_error_response";			break;

		case net_stun_send_indication:						name = "send_indication";					break;

		case net_stun_data_indication:						name = "data_indication";					break;

		case net_stun_createpermission_request:				name = "createpermission_request";			break;
		case net_stun_createpermission_indication:			name = "createpermission_indication";		break;
		case net_stun_createpermission_success_response:	name = "createpermission_success_response";	break;
		case net_stun_createpermission_error_response:		name = "createpermission_error_response";	break;

		case net_stun_channelbind_request:					name = "channelbind_request";				break;
		case net_stun_channelbind_indication:				name = "channelbind_indication";			break;
		case net_stun_channelbind_success_response:			name = "channelbind_success_response";		break;
		case net_stun_channelbind_error_response:			name = "channelbind_error_response";		break;
	};

	return name;
}
//--------------------------------------
//
//--------------------------------------
void net_stun_message_t::log(rtl::Logger* log)
{
	if (!rtl::Logger::check_trace(TRF_CALL))
		return;
	
	char msg_buffer[2048];
	char* buffer = msg_buffer;
	int size = 2048;

	int len = std_snprintf(buffer, size, "NET STUN MESAGE:\n");
	buffer += len;
	size -= len;

	len = std_snprintf(buffer, size,
"\t\tmsg-type:       %s\n\
\t\tmsg-len:        %d\n\
\t\tmsg-magic:      %08X\n\
\t\tmsg-trn-id:     %08X%08X%08X\n",
		get_net_stun_msg_name(m_type),
		m_length,
		m_cookie,
		*(uint32_t*)&m_transaction_id[0], *(uint32_t*)&m_transaction_id[4], *(uint32_t*)&m_transaction_id[8]
	);
	
	buffer += len;
	size -= len;

	// print attributes
	for (int i = 0; i < m_attributes.getCount(); i++)
	{
		const net_stun_attribute_t& attr = m_attributes[i];

		len = net_stun_write_attribute(attr, buffer, size);
		buffer += len;
		size -= len;
	}

	uint32_t compute_integrity = m_integrity;

	if (m_realm && m_nonce)
	{
		// long term
		len = std_snprintf(buffer, size, "\t\t\tattr-realm      %s\n\t\t\tattr-nonce      %s\n", m_realm, m_nonce);

		buffer += len;
		size -= len;

		compute_integrity = !m_nointegrity;
	}
	else if (m_password)
	{
		// short term
		compute_integrity = !m_nointegrity;
	}

	if (compute_integrity && m_username)
	{
		// long term
		len = std_snprintf(buffer, size, "\t\t\tattr-username   %s\n", m_username);
		buffer += len;
		size -= len;
	}

	if (compute_integrity)
	{
		uint32_t* msg_int = (uint32_t*)m_msg_int_hmac;
		//len = std_snprintf(buffer, size, "\t\t\tattr-msg-int    %p%p%p%p%p\n", msg_int[0], msg_int[1], msg_int[2], msg_int[3], msg_int[4]);
		len = std_snprintf(buffer, size, "\t\t\tattr-msg-int    %d%d%d%d%d\n", msg_int[0], msg_int[1], msg_int[2], msg_int[3], msg_int[4]);
		buffer += len;
		size -= len;
	}

	if (m_fingerprint && m_fingerprint_value != 0)
	{
		len = std_snprintf(buffer, size, "\t\t\tattr-fingerprint   %u\n", m_fingerprint_value);
		buffer += len;
		size -= len;
	}

	//log->log("stun-msg", "%s", msg_buffer);
}
//--------------------------------------
//
//--------------------------------------
void net_stun_message_t::log(rtl::Logger* log, const uint8_t* packet, int length)
{
	const uint8_t* data_end;

	if (!rtl::Logger::check_trace(TRF_RTP))
		return;
	
	char msg_buffer[2048];
	char* buffer = msg_buffer;
	int size = 2048;
	int len;

	uint16_t msg_type = ntohs(*(const uint16_t*)packet);
	uint16_t msg_len = ntohs(*(const uint16_t*)(packet+2));
	const uint32_t* msg_cookie = (const uint32_t*)(packet+4);

	// Check message validity
	if ((msg_len + NET_STUN_HEADER_SIZE) != length)
	{
		return;
	}

	len = std_snprintf(buffer, size,
"\t\tmsg-type:       %s\n\
\t\tmsg-len:        %u\n\
\t\tmsg-magic:      %08X\n\
\t\tmsg-trn-id:     %08X%08X%08X\n",
		get_net_stun_msg_name((net_stun_message_type_t)msg_type),
		msg_len,
		ntohl(msg_cookie[0]),
		ntohl(msg_cookie[1]), ntohl(msg_cookie[2]), ntohl(msg_cookie[3])
	);
	
	buffer += len;
	size -= len;

	// Parse attributes

	int read_size = 0;
	data_end = packet + length;
	packet += 20;

	while (packet < data_end)
	{
		net_stun_attribute_t attribute = { 0 };
		int len = net_stun_read_attribute(attribute, packet, size - read_size);
		
		if (len > 0)
		{
			int pad = len & 3;
			if (pad)
			{
				len += 4 - pad;
			}

			packet += len;
			read_size += len;

			len = net_stun_write_attribute(attribute, buffer, size);
			buffer += len;
			size -= len;
		}
		else
		{
			break;
		}
	}

	// печать
	//log->log("stun-msg", "STUN MESSAGE DUMP len(%d)\r\n%S", length, msg_buffer);
}
//--------------------------------------
