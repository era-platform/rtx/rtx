﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_sys_env.h"
#include "std_thread.h"
#include "std_mutex.h"

#include "include/socket_io_service_win32.h"


#define REMOVE_TIMEOUT			1000
#define GET_IO_STATUS_TIMEOUT	500
#define MAX_ERROR_COUNT_WHILE_TRYING_TO_START_RECV 20  //максимальное кол-во ошибок (подряд) при вызове WSARecv, на которое мы не обращаем внимания.

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
struct iocp_user_key
{
	iocp_user_key()
	{
		socket = NULL;
		user = NULL;

		recv_wsa_buf.buf = (char*)recv_buffer;
		recv_wsa_buf.len = sizeof(recv_buffer);
		memset(&recv_overlapped, 0, sizeof(recv_overlapped));
		recv_bytes_count = 0;
		recv_flags = 0;
		memset(recv_saddr, 0, sizeof(recv_saddr));
		recv_saddr_len = sizeof(recv_saddr);
		m_stopping = false;
		m_stopped = false;

		send_wsa_buf.buf = (char*)send_buffer;
		send_wsa_buf.len = sizeof(send_buffer);
		memset(&send_overlapped, 0, sizeof(send_overlapped));
		send_bytes_count = 0;
	}

	net_socket* socket;
	i_socket_event_handler*	user;
	volatile bool m_stopping;
	volatile bool m_stopped;

	uint8_t recv_buffer[net_socket::PAYLOAD_BUFFER_SIZE];
	WSABUF recv_wsa_buf;
	WSAOVERLAPPED recv_overlapped;
	DWORD recv_bytes_count;
	DWORD recv_flags;
	uint8_t recv_saddr[socket_address::MAX_NATIVE_SOCKADDR_SIZE];
	int recv_saddr_len;

	uint8_t send_buffer[net_socket::PAYLOAD_BUFFER_SIZE];
	WSABUF send_wsa_buf;
	WSAOVERLAPPED send_overlapped;
	DWORD send_bytes_count;
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
socket_io_service_impl::socket_io_service_impl(rtl::Logger* log) : m_mutex("sock-io-svc")
{
	m_log = log;
	
	m_iocp_handle = NULL;
	m_stopping = false;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
socket_io_service_impl::~socket_io_service_impl()
{
	destroy();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool socket_io_service_impl::create(uint32_t threads)
{
	if (threads == 0)
	{
		threads = std_get_processors_count();
		PLOG_NET_WARNING("io-srv", "socket_io_service.create -- threads count not specified, will use value: %u", threads);
	}

	//rtl::MutexLock lock(*m_mutex);
	rtl::MutexWatchLock lock(m_mutex, __FUNCTION__);

	if (m_iocp_handle != NULL)
	{
		PLOG_ERROR("io-srv", "socket_io_service.create -- iocp already created");
		return false;
	}

	m_stopping = false;

	m_iocp_handle = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, NULL, DWORD(threads));
	if (m_iocp_handle == NULL)
	{
		DWORD err = GetLastError();
		PLOG_ERROR("io-srv", "socket_io_service.create -- CreateIoCompletionPort() failed. error = %u", err);
		return false;
	}

	for (uint32_t i = 0; i < threads; ++i)
	{
		rtl::Thread* t = NEW rtl::Thread();
		t->start(thread_start, this);
		m_threads.add(t);
	}

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void socket_io_service_impl::destroy()
{
	//rtl::MutexLock lock(*m_mutex);
	rtl::MutexWatchLock lock(m_mutex, __FUNCTION__);

	m_stopping = true;

	if (m_iocp_handle != NULL)
	{
		if (!CloseHandle(m_iocp_handle))
		{
			DWORD err = GetLastError();
			PLOG_ERROR("io-srv", "socket_io_service.destroy -- CloseHandle() failed. error = %u", err);
		}

		m_iocp_handle = NULL;
	}

	if (m_threads.getCount() > 0)
	{
		for (int i = 0; i < m_threads.getCount(); ++i)
		{
			rtl::Thread* t = m_threads.getAt(i);
			t->wait(INFINITE);
			DELETEO(t);
		}
		m_threads.clear();
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool socket_io_service_impl::add(net_socket* sock, i_socket_event_handler* user)
{
	if (sock == NULL)
	{
		PLOG_ERROR("io-srv", "socket_io_service.add -- invalid socket");
		return false;
	}

	if (user == NULL)
	{
		PLOG_ERROR("io-srv", "socket_io_service.add -- invalid user");
		return false;
	}

	//rtl::MutexLock lock(*m_mutex);
	rtl::MutexWatchLock lock(m_mutex, __FUNCTION__);

	if (m_iocp_handle == NULL)
	{
		PLOG_ERROR("io-srv", "socket_io_service.add -- iocp isn't created");
		return false;
	}

	PLOG_NET_WRITE("io-srv", "socket_io_service.add -- socket %u", sock->get_native_handle());

	iocp_user_key* key = NEW iocp_user_key();
	key->socket = sock;
	key->user = user;

	HANDLE h = CreateIoCompletionPort((HANDLE)sock->get_native_handle(), m_iocp_handle, (ULONG_PTR)key, 0);
	if (h == NULL)
	{
		DWORD err = GetLastError();
		PLOG_ERROR("io-srv", "socket_io_service.add -- CreateIoCompletionPort(%u) failed. error = %u", sock->get_local_address()->get_port(), err);
		DELETEO(key);
		return false;
	}
	else
	{
		PLOG_WRITE("io-srv", "socket_io_service.add -- CreateIoCompletionPort(%u) Ok.", sock->get_local_address()->get_port());
	}

	sock->m_user_tag = key;

	if (h != m_iocp_handle)
	{
		PLOG_WARNING("io-srv", "socket_io_service.add -- CreateIoCompletionPort() returned unknown handle");
	}

	// запускаем асинхронный прием
	start_async_recv(key);

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void socket_io_service_impl::remove(net_socket* sock)
{
	if (sock == NULL)
	{
		PLOG_ERROR("io-srv", "socket_io_service.remove -- invalid socket");
		return;
	}

	if (sock->m_user_tag == NULL)
	{
		PLOG_ERROR("io-srv", "socket_io_service.remove -- invalid io_service_tag");
		return;
	}

	//rtl::MutexLock lock(*m_mutex);
	rtl::MutexWatchLock lock(m_mutex, __FUNCTION__);

	PLOG_NET_WRITE("io-srv", "socket_io_service.remove -- socket %u", sock->get_native_handle());

	iocp_user_key* key = (iocp_user_key*) sock->m_user_tag;
	key->m_stopping = true;

	// FIXME
	// это меганеправильно, но так проще всего.
	// поток сразу вывалится из GetQueuedCompletionStatus() с ошибкой.
	sock->close();

	uint64_t start = rtl::DateTime::getTicks64();

	// ждем, когда поток ответит
	while (!key->m_stopped)
	{
		rtl::Thread::sleep(2);

		uint64_t elapsed = rtl::DateTime::getTicks64() - start;
		if (elapsed >= REMOVE_TIMEOUT)
		{
			PLOG_ERROR("io-srv", "socket_io_service.remove -- timeout %u", REMOVE_TIMEOUT);
			break;
		}
	}

	DELETEO(key);
	sock->m_user_tag = NULL;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool socket_io_service_impl::send_data_to(net_socket* sock, const uint8_t* data, uint32_t len, const socket_address* to)
{
	if (sock == NULL)
	{
		PLOG_NET_ERROR("io-srv", "socket_io_service.send_data_to -- invalid socket");
		return false;
	}

	if (data == NULL)
	{
		PLOG_NET_ERROR("io-srv", "socket_io_service.send_data_to -- invalid data");
		return false;
	}

	if (len == 0)
	{
		PLOG_NET_ERROR("io-srv", "socket_io_service.send_data_to -- invalid length");
		return false;
	}

	if (to == NULL)
	{
		PLOG_NET_ERROR("io-srv", "socket_io_service.send_data_to -- invalid address");
		return false;
	}

	if (sock->m_user_tag == NULL)
	{
		PLOG_NET_ERROR("io-srv", "socket_io_service.send_data_to -- (socket %u) invalid user_tag", sock->get_native_handle());
		return false;
	}

	iocp_user_key* key = (iocp_user_key*)sock->m_user_tag;

	if (key->m_stopped)
	{
		PLOG_NET_WARNING("io-srv", "socket_io_service.send_data_to -- (socket %u) stopped-flag is enabled.", key->socket->get_native_handle());
		return false;
	}

	//memcpy(key->send_buffer, data, len);
	key->send_wsa_buf.buf = (char*)data;
	key->send_wsa_buf.len = len;

#if defined(USE_ASYNC_SOCKET_SEND)
	OVERLAPPED* poverlapped = &key->send_overlapped;
#else
	OVERLAPPED* poverlapped = NULL;
#endif

	//PLOG_NET_WRITE("io-srv", "socket_io_service.send_data_to -- (socket %u) sending %u bytes to %s.", key->socket->get_native_handle(), len, to->to_string());

	int res = WSASendTo(sock->get_native_handle(),
		&key->send_wsa_buf,
		1,
		&key->send_bytes_count,
		0,
		(const sockaddr*)to->get_native(),
		int(to->get_native_len()),
		poverlapped,
		NULL);

	if (res == 0)
		return true;

	int err = WSAGetLastError();

#if defined(USE_ASYNC_SOCKET_SEND)
	if (err == WSA_IO_PENDING)
		return true;
#endif

	if (err == WSAENOTSOCK)
	{
		PLOG_NET_WRITE("io-srv", "socket_io_service.send_data_to -- (socket %u) WSASendTo() failed. error = WSAENOTSOCK. setting stopped-flag.",
			key->socket->get_native_handle());
		key->m_stopped = true;
	}
	else
	{
		PLOG_NET_WARNING("io-srv", "socket_io_service.send_data_to -- (socket %u) WSASendTo() failed. error = %d",
			key->socket->get_native_handle(), err);
	}

	return false;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void socket_io_service_impl::thread_start(rtl::Thread* thread, void* param)
{
	thread->set_priority(rtl::ThreadPriority::High);

	socket_io_service_impl* pthis = (socket_io_service_impl*)param;
	pthis->main_loop();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void socket_io_service_impl::main_loop()
{
	PLOG_WRITE("io-srv", "socket_io_service.main_loop -- enter.");

	DWORD bytes = 0;
	OVERLAPPED* poverlapped = NULL;
	iocp_user_key* pkey = NULL;
	socket_address saddr;

	
		bool stop = false;
		while (!stop)
		{
			if (m_stopping)
			{
				PLOG_WRITE("io-srv", "socket_io_service.main_loop -- main stopping flag enabled. exit thread.");
				break;
			}

			bytes = 0;
			poverlapped = NULL;
			pkey = NULL;

			BOOL res = GetQueuedCompletionStatus(m_iocp_handle, &bytes, (PULONG_PTR)&pkey, &poverlapped, GET_IO_STATUS_TIMEOUT);
			if (res)
			{
				if (pkey == NULL)
				{
					PLOG_NET_WARNING("io-srv", "socket_io_service.main_loop -- key is NULL.");
					continue;
				}

				if (poverlapped == NULL)
				{
					PLOG_NET_WARNING("io-srv", "socket_io_service.main_loop -- overlapped is NULL.");
					continue;
				}

				/*if (bytes == 0)
				{
					PLOG_NET_WARNING("io-srv", "socket_io_service.main_loop -- (socket %u) no data transferred.",
						pkey->socket->get_native_handle());
				}*/

				if (poverlapped == &pkey->recv_overlapped)
				{
					saddr.set_native(pkey->recv_saddr, pkey->recv_saddr_len);

					//PLOG_NET_WRITE("io-srv", "socket_io_service.main_loop -- (socket %u) %u bytes received from %s.", pkey->socket->get_native_handle(), bytes, saddr.to_string());

					//DWORD gle = GetLastError();

					if (bytes > 0)
					{
						pkey->user->socket_data_received(pkey->socket, pkey->recv_buffer, bytes, &saddr);
					}

					start_async_recv(pkey);
				}
#if defined(USE_ASYNC_SOCKET_SEND)
				else if (poverlapped == &pkey->send_overlapped)
				{
					// ничего не делаем

					int stop = 0;
				}
#endif
				else
				{
					PLOG_NET_WARNING("io-srv", "socket_io_service.main_loop -- unknown overlapped ptr.");
				}
			}
			else
			{
				DWORD gle = GetLastError();
				switch (gle)
				{
				case ERROR_OPERATION_ABORTED:
					if (pkey != NULL)
					{
						/*PLOG_NET_WRITE("io-srv", "socket_io_service.main_loop -- (socket %u) GetQueuedCompletionStatus() failed. error = ERROR_OPERATION_ABORTED. setting stopped-flag.",
							pkey->socket->get_native_handle());*/
						pkey->m_stopped = true;
					}
					break;

				case ERROR_ABANDONED_WAIT_0:
					PLOG_WRITE("io-srv", "socket_io_service.main_loop -- GetQueuedCompletionStatus() failed. error = ERROR_ABANDONED_WAIT_0. exit thread.");
					stop = true;
					break;

				case ERROR_INVALID_HANDLE:
					PLOG_WRITE("io-srv", "socket_io_service.main_loop -- GetQueuedCompletionStatus() failed. error = ERROR_INVALID_HANDLE. exit thread.");
					stop = true;
					break;

				case WAIT_TIMEOUT:
					break;

				default:
					if (pkey != NULL)
					{
						PLOG_NET_WARNING("io-srv", "socket_io_service.main_loop -- (socket %u) GetQueuedCompletionStatus() failed. error = %u.", pkey->socket->get_native_handle(), gle);
						start_async_recv(pkey);
					}
					else
					{
						PLOG_NET_WARNING("io-srv", "socket_io_service.main_loop -- GetQueuedCompletionStatus() failed. error = %u.", gle);
					}
					break;
				}
			}
		}
	

	PLOG_WRITE("io-srv", "socket_io_service.main_loop -- exit.");
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void				socket_io_service_impl::start_async_recv(iocp_user_key* key)
{
	if (key->m_stopped)
	{
		PLOG_NET_WARNING("io-srv", "socket_io_service.start_async_recv -- (socket %u) stopped-flag is enabled.", key->socket->get_native_handle());
		return;
	}

	//PLOG_NET_WRITE("io-srv", "socket_io_service.start_async_recv -- socket %u.", key->socket->get_native_handle());

	uint32_t errors = 0;

	for (;;)
	{
		int res = WSARecvFrom(key->socket->get_native_handle(),
			&key->recv_wsa_buf,
			1,
			&key->recv_bytes_count,
			&key->recv_flags,
			(sockaddr*)&key->recv_saddr,
			&key->recv_saddr_len,
			&key->recv_overlapped,
			NULL);

		if (res == 0)
		{
			return;
		}

		int err = WSAGetLastError();
		if (err == WSA_IO_PENDING)
		{
			return;
		}

		if (err == WSAENOTSOCK)
		{
			PLOG_NET_WRITE("io-srv", "socket_io_service.start_async_recv -- (socket %u) WSARecvFrom() failed. error = WSAENOTSOCK. setting stopped-flag.",
				key->socket->get_native_handle());
			key->m_stopped = true;
			return;
		}

		if (++errors < MAX_ERROR_COUNT_WHILE_TRYING_TO_START_RECV)
		{
			PLOG_NET_WARNING("io-srv", "socket_io_service.start_async_recv -- (socket %u) WSARecvFrom() failed. error = %d",
				key->socket->get_native_handle(), err);
		}
		else
		{
			PLOG_NET_ERROR("io-srv", "socket_io_service.start_async_recv -- (socket %u) WSARecvFrom() failed. error = %d. max error count exceeded.",
				key->socket->get_native_handle(), err);
		}
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
