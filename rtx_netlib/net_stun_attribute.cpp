﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "stdafx.h"
#include "net_stun_attribute.h"
//--------------------------------------
//
//--------------------------------------
static const char* get_net_stun_attr_name(net_stun_attribute_type_t type);
//--------------------------------------
// ERROR-CODE parser
//--------------------------------------
static bool net_stun_read_error_attribute(net_stun_attribute_t& attribute, const uint8_t* stream, int size)
{
	attribute.error.err_class = stream[2] & 0x0F;
	attribute.error.number = stream[3];
	int phrase_len = attribute.length - 4;
	attribute.error.reason_phrase = NEW char [phrase_len + 1];
	memcpy(attribute.error.reason_phrase, stream + 4, phrase_len);
	attribute.error.reason_phrase[phrase_len] = 0;

	return true;
}
//--------------------------------------
//
//--------------------------------------
static bool net_stun_read_address_attribute(net_stun_attribute_t& attribute, const uint8_t* stream, int size)
{
	attribute.address.family = (net_stun_addr_family_t)*(stream + 1);
	attribute.address.port = ntohs(*(uint16_t*)(stream + 2));

	if (attribute.address.family == stun_ipv4)
	{
		if (attribute.length != 8)
			return false;

		attribute.address.ipv4.s_addr = *(const uint32_t*)(stream + 4);
	}
	else if (attribute.address.family == stun_ipv6)
	{
		if (attribute.length != 20)
			return false;

		memcpy(&attribute.address, stream + 4, 16);
	}
	else
	{
		LOG_NET_ERROR("net-stun", "UNKNOWN FAMILY [%u].", attribute.address.family);
		return false;
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
static bool net_stun_read_xaddress_attribute(net_stun_attribute_t& attribute, const uint8_t* stream, int size)
{
	attribute.address.family = (net_stun_addr_family_t)*(stream + 1);
	attribute.address.port = ntohs(*(uint16_t*)(stream + 2)) ^ 0x2112;

	if (attribute.address.family == stun_ipv4)
	{
		if (attribute.length != 8)
			return false;

		uint32_t addr_value = ntohl(*(const uint32_t*)(stream + 4)) ^ 0x2112A442;
		attribute.address.ipv4.s_addr = htonl(addr_value);
	}
	else if (attribute.address.family == stun_ipv6)
	{
		if (attribute.length != 20)
			return false;

		memcpy(&attribute.address, stream + 4, 16);

		// делаем колюч их кука и ид транзакции
		// !! пока нет доступа !!
	}
	else
	{
		LOG_NET_ERROR("net-stun", "UNKNOWN FAMILY [%u].", attribute.address.family);
		return false;
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
static bool net_stun_read_string_attribute(net_stun_attribute_t& attribute, const uint8_t* stream, int size)
{
	if (attribute.length > 0)
	{
		attribute.data_ptr = NEW uint8_t[attribute.length + 1];
		memcpy(attribute.data_ptr, stream, attribute.length);
		attribute.data_ptr[attribute.length] = 0;
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
static bool net_stun_read_int32_attribute(net_stun_attribute_t& attribute, const uint8_t* stream, int size)
{
	if (attribute.length != 4)
		return false;

	attribute.value32[0] = ntohl(*(uint32_t*)stream);
	return true;
}
//--------------------------------------
//
//--------------------------------------
static bool net_stun_read_int64_attribute(net_stun_attribute_t& attribute, const uint8_t* stream, int size)
{
	if (attribute.length != 8)
		return false;

	attribute.value64 = (uint64_t(ntohl(*(uint32_t*)stream)) >> 32) | uint64_t(ntohl(*(uint32_t*)(stream + 4)));
	return true;
}
//--------------------------------------
//
//--------------------------------------
static bool net_stun_read_int8_attribute(net_stun_attribute_t& attribute, const uint8_t* stream, int size)
{
	if (attribute.length != 1)
		return false;

	attribute.value8[0] = *stream;
	return true;
}
//--------------------------------------
// main reader with switcher
//--------------------------------------
int net_stun_read_attribute(net_stun_attribute_t& attribute, const uint8_t* stream, int size)
{
	// все атрибуты имеют заголовок в 4 байта
	if (stream == nullptr || size < 4)
		return 0;

	bool result = false;

	attribute.type = ntohs(*(const uint16_t*)stream);
	attribute.length = ntohs(*(const uint16_t*)(stream+2));

	// если атрибут лежит в стриме не полностью!
	if (attribute.length + 4 > size)
		return 0;

	// возможно это флаг
	if (attribute.length == 0)
		return 4;

	switch (attribute.type)
	{
		/// address
	case net_stun_mapped_address:
	case net_stun_response_address:
	case net_stun_change_address:
	case net_stun_source_address:
	case net_stun_changed_address:
	case net_stun_reflected_from:
	case net_stun_alternate_server:
		result = net_stun_read_address_attribute(attribute, stream + 4, size - 4);
		break;
		
		/// xored address
	case net_stun_xor_mapped_address:
	case net_stun_xor_peer_address:
	case net_stun_xor_relayed_address:
		result = net_stun_read_xaddress_attribute(attribute, stream + 4, size - 4);
		break;

		/// error-code
	case net_stun_error_code:
		result = net_stun_read_error_attribute(attribute, stream + 4, size - 4);
		break;
		/// uint8_t
	case net_stun_even_port:
		result = net_stun_read_int8_attribute(attribute, stream + 4, size - 4);
		break;

		/// uint32_t
	case net_stun_fingerprint:
	case net_stun_ice_priority:
	case net_stun_channel_number:
	case net_stun_lifetime:
	case net_stun_requested_transport:
		result = net_stun_read_int32_attribute(attribute, stream + 4, size - 4);
		break;

		/// uint64_t
	case net_stun_ice_controlled:
	case net_stun_ice_controlling:
	case net_stun_reservation_token:
		result = net_stun_read_int64_attribute(attribute, stream + 4, size - 4);
		break;

		/// data block
	/*case net_stun_username:
	case net_stun_password:
	case net_stun_message_integrity:
	case net_stun_unknown_attributes:
	case net_stun_realm:
	case net_stun_nonce:
	case net_stun_software:
	case net_stun_ice_use_candidate:
	case net_stun_dont_fragment:
	case net_stun_data:*/
	default:
		result = net_stun_read_string_attribute(attribute, stream + 4, size - 4);
		break;
	}

	return result ? attribute.length + 4 : 0;
}
//--------------------------------------
//
//--------------------------------------
static int net_stun_write_address_attribute(const net_stun_attribute_t& attribute, uint8_t* buffer, int size)
{
	int len = 4;
	buffer[1] = attribute.address.family;
	*(uint16_t*)(buffer+2) = htons(attribute.address.port);
	
	if (attribute.address.family == stun_ipv4)
	{
		*(uint32_t*)(buffer+2) = attribute.address.ipv4.s_addr;
		len += 4;
	}
	else
	{
		memcpy(buffer + 4, &attribute.address.ipv6, 16);
		len += 16;
	}

	return len;
}
//--------------------------------------
//
//--------------------------------------
static int net_stun_write_address_attribute(const net_stun_attribute_t& attribute, rtl::MemoryStream& stream)
{
	int len = 4;
	uint8_t first = 0;
	stream.write(&first, sizeof(uint8_t));

	uint8_t family = attribute.address.family;
	stream.write(&family, sizeof(uint8_t));

	uint16_t port = htons(attribute.address.port);
	stream.write(&port, sizeof(uint16_t));
	
	if (attribute.address.family == stun_ipv4)
	{
		uint32_t addr = htonl(attribute.address.ipv4.s_addr);
		stream.write(&addr, sizeof(uint32_t));
		len += 4;
	}
	else
	{
		stream.write(&attribute.address.ipv6, 16);
		len += 16;
	}

	return len;
}
//--------------------------------------
//
//--------------------------------------
static int net_stun_write_xaddress_attribute(const net_stun_attribute_t& attribute, uint8_t* buffer, int size)
{
	int len = 4;
	buffer[0] = 0;
	buffer[1] = attribute.address.family;
	uint16_t xport = attribute.address.port ^ 0x2112;
	buffer[2] = uint8_t(xport >> 8);
	buffer[3] = uint8_t(xport & 0xFF);

	if (attribute.address.family == stun_ipv4)
	{
		uint32_t addr = ntohl(attribute.address.ipv4.s_addr) ^ 0x2112A442;
		//*(uint32_t*)(buffer+2) = htonl(addr);
		buffer[4] = uint8_t(addr >> 24);
		buffer[5] = uint8_t((addr >> 16) & 0xFF);
		buffer[6] = uint8_t((addr >> 8) & 0xFF);
		buffer[7] = uint8_t(addr & 0xFF);
		len += 4;
	}
	else
	{
		// !not implemented!
		memcpy(buffer + 4, &attribute.address.ipv6, 16);
		len += 16;
	}

	return len;
}
//--------------------------------------
//
//--------------------------------------
static int net_stun_write_xaddress_attribute(const net_stun_attribute_t& attribute, rtl::MemoryStream& stream)
{
	int len = 4;
	uint8_t first = 0;
	stream.write(&first, sizeof(uint8_t));

	uint8_t family = attribute.address.family;
	stream.write(&family, sizeof(uint8_t));

	uint16_t port = htons(attribute.address.port ^ 0x2112);
	stream.write(&port, sizeof(uint16_t));

	if (attribute.address.family == stun_ipv4)
	{
		uint32_t addr = ntohl(attribute.address.ipv4.s_addr) ^ 0x2112A442;
		uint32_t addr_ip4 = htons(addr);
		stream.write(&addr_ip4, sizeof(uint32_t));

		len += 4;
	}
	else
	{
		// !not implemented!
		stream.write(&attribute.address.ipv6, 16);
		len += 16;
	}

	return len;
}
//--------------------------------------
//
//--------------------------------------
static int net_stun_write_error_attribute(const net_stun_attribute_t& attribute, uint8_t* buffer, int size)
{
	int len = attribute.length;
	*(uint16_t*)buffer = 0;
	buffer[2] = attribute.error.err_class & 0x0F;
	buffer[3] = attribute.error.number;
	memcpy(buffer + 4, attribute.error.reason_phrase, attribute.length - 4);

	// padding to 32 bits
	int pad = attribute.length & 0x03;
	
	if (pad)
	{
		memset(buffer + attribute.length, 0, 4-pad);
		len += 4-pad;
	}

	return len;
}
//--------------------------------------
//
//--------------------------------------
static int net_stun_write_error_attribute(const net_stun_attribute_t& attribute, rtl::MemoryStream& stream)
{
	int len = attribute.length;
	uint16_t first = 0;
	stream.write(&first, sizeof(uint16_t));
	
	uint8_t err_class = attribute.error.err_class & 0x0F;
	stream.write(&err_class, sizeof(uint8_t));

	uint8_t number = attribute.error.number;
	stream.write(&number, sizeof(uint8_t));

	stream.write(attribute.error.reason_phrase, attribute.length - 4);

	// padding to 32 bits
	int pad = attribute.length & 0x03;
	
	if (pad)
	{
		int z = 0;
		stream.write(&z, 4 - pad);
		len += 4-pad;
	}

	return len;
}
//--------------------------------------
//
//--------------------------------------
static int net_stun_write_int8_attribute(const net_stun_attribute_t& attribute, uint8_t* buffer, int size)
{
	*buffer = attribute.value8[0];
	return 4;
}
//--------------------------------------
//
//--------------------------------------
static int net_stun_write_int32_attribute(const net_stun_attribute_t& attribute, uint8_t* buffer, int size)
{
	*(uint32_t*)buffer = htonl(attribute.value32[0]);
	return 4;
}
//--------------------------------------
//
//--------------------------------------
static int net_stun_write_int64_attribute(const net_stun_attribute_t& attribute, uint8_t* buffer, int size)
{
	*(uint32_t*)buffer = htonl(uint32_t(attribute.value64 >> 32));
	*(uint32_t*)(buffer+4) = htonl((uint32_t)attribute.value64);
	
	return 8;
}
//--------------------------------------
//
//--------------------------------------
static int net_stun_write_string_attribute(const net_stun_attribute_t& attribute, uint8_t* buffer, int size)
{
	int len = attribute.length;
	if (attribute.data_ptr != nullptr && attribute.length > 0)
		memcpy(buffer, attribute.data_ptr, attribute.length);

	// padding to 32 bits
	int pad = attribute.length & 0x03;
	
	if (pad)
	{
		memset(buffer + attribute.length, 0, 4-pad);
		len += 4 - pad;
	}

	return len;
}
//--------------------------------------
//
//--------------------------------------
static int net_stun_write_string_attribute(const net_stun_attribute_t& attribute, rtl::MemoryStream& stream)
{
	int len = attribute.length;
	if (attribute.data_ptr != nullptr && attribute.length > 0)
	{
		stream.write(attribute.data_ptr, attribute.length);
	}

	// padding to 32 bits
	int pad = attribute.length & 0x03;
	
	if (pad)
	{
		int z = 0;
		stream.write(&z, 4 - pad);
		len += 4 - pad;
	}

	return len;
}
//--------------------------------------
//
//--------------------------------------
int net_stun_write_attribute(const net_stun_attribute_t& attribute, uint8_t* buffer, int size)
{
	if (attribute.length + 4 > size)
		return 0;

	// записываем тип и длину
	*(uint16_t*)buffer = htons(attribute.type);
	*(uint16_t*)(buffer+2) = htons(attribute.length);
	
	int written = 4;

	if (attribute.length == 0)
		return written; // may be flag

	switch (attribute.type)
	{
		/// address
	case net_stun_mapped_address:
	case net_stun_response_address:
	case net_stun_change_address:
	case net_stun_source_address:
	case net_stun_changed_address:
	case net_stun_reflected_from:
	case net_stun_alternate_server:
		written += net_stun_write_address_attribute(attribute, buffer + 4, size - 4);
		break;
		/// xored address
	case net_stun_xor_mapped_address:
	case net_stun_xor_peer_address:
	case net_stun_xor_relayed_address:
		written += net_stun_write_xaddress_attribute(attribute, buffer + 4, size - 4);
		break;

		/// error-code
	case net_stun_error_code:
		written += net_stun_write_error_attribute(attribute, buffer + 4, size - 4);
		break;

		/// uint8_t
	case net_stun_even_port:
		written += net_stun_write_int8_attribute(attribute, buffer + 4, size - 4);
		break;

		/// uint32_t
	case net_stun_fingerprint:
	case net_stun_ice_priority:
	case net_stun_requested_transport:
		written += net_stun_write_int32_attribute(attribute, buffer + 4, size - 4);
		break;

		/// uint64_t
	case net_stun_ice_controlled:
	case net_stun_ice_controlling:
	case net_stun_reservation_token:
		written += net_stun_write_int64_attribute(attribute, buffer + 4, size - 4);
		break;

		/// data block
	/*case net_stun_username:
	case net_stun_password:
	case net_stun_message_integrity:
	case net_stun_unknown_attributes:
	case net_stun_realm:
	case net_stun_nonce:
	case net_stun_software:
	case net_stun_ice_use_candidate:
	...
	*/
	default:
		written += net_stun_write_string_attribute(attribute, buffer + 4, size - 4);
		break;
	}

	return written;
}
//--------------------------------------
//
//--------------------------------------
int net_stun_write_attribute(const net_stun_attribute_t& attribute, rtl::MemoryStream& stream)
{
	int initial_size = int(stream.getLength());

	// записываем тип и длину
	uint16_t type = htons(attribute.type);
	stream.write(&type, sizeof(uint16_t));

	uint16_t length = htons(attribute.length);
	stream.write(&length, sizeof(uint16_t));
	
	if (attribute.length == 0)
		return 4; // may be a flag

	uint8_t value_0 = 0;

	switch (attribute.type)
	{
		/// address
	case net_stun_mapped_address:
	case net_stun_response_address:
	case net_stun_change_address:
	case net_stun_source_address:
	case net_stun_changed_address:
	case net_stun_reflected_from:
	case net_stun_alternate_server:
		net_stun_write_address_attribute(attribute, stream);
		break;
		/// xored address
	case net_stun_xor_mapped_address:
	case net_stun_xor_peer_address:
	case net_stun_xor_relayed_address:
		net_stun_write_xaddress_attribute(attribute, stream);
		break;

		/// error-code
	case net_stun_error_code:
		net_stun_write_error_attribute(attribute, stream);
		break;

		/// uint8_t
	case net_stun_even_port:
		stream.write(&attribute.value8[0], sizeof(uint8_t));
		stream.write(&value_0, sizeof(uint8_t));
		stream.write(&value_0, sizeof(uint8_t));
		stream.write(&value_0, sizeof(uint8_t));
		break;

		/// uint32_t
	case net_stun_fingerprint:
	case net_stun_ice_priority:
	case net_stun_requested_transport:
		stream.write(&attribute.value32[0], sizeof(uint32_t));
		break;

		/// uint64_t
	case net_stun_ice_controlled:
	case net_stun_ice_controlling:
	case net_stun_reservation_token:
		stream.write(&attribute.value64, sizeof(uint64_t));
		break;

		/// data block
	/*case net_stun_username:
	case net_stun_password:
	case net_stun_message_integrity:
	case net_stun_unknown_attributes:
	case net_stun_realm:
	case net_stun_nonce:
	case net_stun_software:
	case net_stun_ice_use_candidate:
	...
	*/
	default:
		net_stun_write_string_attribute(attribute, stream);
		break;
	}

	return initial_size;
}
//--------------------------------------
//
//--------------------------------------
int net_stun_write_attribute_data(net_stun_attribute_type_t type, const void* data, int size, rtl::MemoryStream& stream)
{
	int initial_size = int(stream.getLength());
	
	uint16_t type_16 = htons(type);
	stream.write(&type_16, sizeof(uint16_t));
	if (data != nullptr && size > 0)
	{
		uint16_t size_16 = htons(size);
		stream.write(&size_16, sizeof(uint16_t));
		stream.write(data, size);
	}

	return int(stream.getLength() - initial_size);
}
//--------------------------------------
//
//--------------------------------------
int net_stun_write_attribute_string(net_stun_attribute_type_t type, const char* string, rtl::MemoryStream& stream)
{
	int initial_size = int(stream.getLength());
	int str_length = (string != nullptr && string[0] != 0) ? int(strlen(string)) : 0;
	
	uint16_t type_16 = htons(type);
	stream.write(&type_16, sizeof(uint16_t));
	if (str_length > 0)
	{
		uint16_t length = htons(str_length);
		stream.write(&length, sizeof(uint16_t));
		stream.write(string, str_length);
	}

	return int(stream.getLength() - initial_size);
}
//--------------------------------------
//
//--------------------------------------
int net_stun_write_attribute_int32(net_stun_attribute_type_t type, uint32_t value, rtl::MemoryStream& stream)
{
	uint16_t type_16 = htons(type);
	stream.write(&type_16, sizeof(uint16_t));

	uint16_t next = 0x0400;
	stream.write(&next, sizeof(uint16_t));

	uint32_t value_32 = htonl(value);
	stream.write(&value_32, sizeof(uint32_t));

	return 8;
}
//--------------------------------------
//
//--------------------------------------
void net_stun_cleanup_attribute(net_stun_attribute_t& attribute)
{
	switch (attribute.type)
	{
	case net_stun_error_code:
		delete[] attribute.error.reason_phrase;
		attribute.error.reason_phrase = nullptr;
		break;
	case net_stun_username:
	case net_stun_password:
	case net_stun_message_integrity:
	case net_stun_unknown_attributes:
	case net_stun_realm:
	case net_stun_nonce:
	case net_stun_software:
	case net_stun_ice_use_candidate:
		delete[] attribute.data_ptr;
		attribute.data_ptr = nullptr;
		break;
	}
}
//--------------------------------------
// Formats binary IP address as string.
//--------------------------------------
int net_stun_address_to_string(const net_stun_address_t* address, char** out_ip)
{
	if (address == nullptr || out_ip == nullptr)
		return -1;

	if (address->family == stun_ipv6)
	{
		*out_ip = NEW char[64];
		ip_address6 addr6(&address->ipv6);
		std_snprintf(*out_ip, 40, "%s", addr6.to_string());
	}
	else if (address->family == stun_ipv4)
	{
		*out_ip = NEW char[64];
		uint8_t* in_ip = (uint8_t*)&address;
		std_snprintf(*out_ip, 16, "%u.%u.%u.%u", in_ip[0], in_ip[1], in_ip[2], in_ip[3]);
		
		return 0;
	}
	else
	{
		LOG_NET_ERROR("net-stun", "Unsupported address family: %u.", address->family);
	}

	return -1;
}
//--------------------------------------
//
//--------------------------------------
int net_stun_write_attribute(const net_stun_attribute_t& attribute, char* buffer, int size)
{
	if (attribute.length + 4 > size)
		return 0;

	char* initial_ptr = buffer;

	int len = std_snprintf(buffer, size, "\t\t\tattr-type       %s\n\t\t\tattr-len        %d\n", get_net_stun_attr_name((net_stun_attribute_type_t)attribute.type), attribute.length);
	// записываем тип и длину

	buffer += len;
	size -= len;

	if (attribute.length == 0)
		return len; // may be flag

	switch (attribute.type)
	{
		/// address
	case net_stun_mapped_address:
	case net_stun_response_address:
	case net_stun_change_address:
	case net_stun_source_address:
	case net_stun_changed_address:
	case net_stun_reflected_from:
	case net_stun_alternate_server:
		/// xored address
	case net_stun_xor_mapped_address:
	case net_stun_xor_peer_address:
	case net_stun_xor_relayed_address:
		len =std_snprintf(buffer, size, "\t\t\tattr-addr-fam   %d\n\t\t\tattr-addr-port  %u\n\t\t\tattr-addr-ip    %s\n", attribute.address.family, attribute.address.port, inet_ntoa(attribute.address.ipv4));
		buffer += len;
		size -= len;
		break;

		/// error-code
	case net_stun_error_code:
		len =std_snprintf(buffer, size, "\t\t\tattr-err-class  %d\n\t\t\tattr-err-num    %u\n\t\t\tattr-err-phrase %s\n", attribute.error.err_class, attribute.error.number, attribute.error.reason_phrase);
		buffer += len;
		size -= len;
		break;
		/// uint8_t
	case net_stun_even_port:
		len =std_snprintf(buffer, size, "\t\t\tattr-even-port  %02X\n", attribute.value8[0]);
		buffer += len;
		size -= len;
		break;

		/// uint32_t
	case net_stun_fingerprint:
	case net_stun_ice_priority:
	case net_stun_requested_transport:
		len =std_snprintf(buffer, size, "\t\t\tattr-value      %08X\n", htonl(attribute.value32[0]));
		buffer += len;
		size -= len;
		break;

		/// uint64_t
	case net_stun_ice_controlled:
	case net_stun_ice_controlling:
	case net_stun_reservation_token:
		len =std_snprintf(buffer, size, "\t\t\tattr-value      %08X%08X\n", htonl(attribute.value32[1]), htonl(attribute.value32[0]));
		buffer += len;
		size -= len;
		break;


		/// data block
	/*case net_stun_username:
	case net_stun_password:
	case net_stun_message_integrity:
	case net_stun_unknown_attributes:
	case net_stun_realm:
	case net_stun_nonce:
	case net_stun_software:
	case net_stun_ice_use_candidate:
	...
	*/
	default:
		len =std_snprintf(buffer, size, "\t\t\tattr-value:     ");
		buffer += len;
		size -= len;

		for (int i = 0; i < attribute.length && size > 2; i++)
		{
			uint8_t ht = attribute.data_ptr[i] >> 4;
			uint8_t lt = attribute.data_ptr[i] & 0x0F;

			*buffer++ = ht <= 9 ? ht + '0' : ht - 10 + 'A';
			*buffer++ = lt <= 9 ? lt + '0' : lt - 10  + 'A';
			size -= 2;
		}

		/*if (size > 2)
		{
			*buffer++ = ' ';
			*buffer++ = '\'';
			size -= 2;
		}


		if (size > attribute.length)
		{
			memcpy(buffer, attribute.data_ptr, attribute.length);
			buffer += attribute.length;
			size -= attribute.length;
		}*/

		if (size > 2)
		{
			//*buffer++ = '\'';
			*buffer++ = '\r';
			*buffer++ = '\n';
			size -= 2;
		}

		*buffer = 0;

		break;
	}

	return int(buffer - initial_ptr);
}
//--------------------------------------
//
//--------------------------------------
static const char* get_net_stun_attr_name(net_stun_attribute_type_t type)
{
	const char* name = "unknown";
	switch (type)
	{
	case net_stun_reserved:				name = "RESERVED";				break;
	case net_stun_mapped_address:		name = "MAPPED-ADDRESS";		break;
	case net_stun_response_address:		name = "RESPONSE-ADDRESS";		break;
	case net_stun_change_address:		name = "CHANGE-ADDRESS";		break;
	case net_stun_source_address:		name = "SOURCE-ADDRESS";		break;
	case net_stun_changed_address:		name = "CHANGED-ADDRESS";		break;
	case net_stun_username:				name = "USERNAME";				break;
	case net_stun_password:				name = "PASSWORD";				break;
	case net_stun_message_integrity:	name = "MESSAGE-INTEGRITY";		break;
	case net_stun_error_code:			name = "ERROR-CODE";			break;
	case net_stun_unknown_attributes:	name = "UNKNOWN-ATTRIBUTES";	break;
	case net_stun_reflected_from:		name = "REFLECTED-FROM";		break;
	case net_stun_realm:				name = "REALM";					break;
	case net_stun_nonce:				name = "NONCE";					break;
	case net_stun_xor_mapped_address:	name = "XOR-MAPPED-ADDRESS";	break;
	case net_stun_software:				name = "SOFTWARE";				break;
	case net_stun_alternate_server:		name = "ALTERNATE-SERVER";		break;
	case net_stun_fingerprint:			name = "FINGERPRINT";			break;
	case net_stun_channel_number:		name = "CHANNEL-NUMBER";		break;
	case net_stun_lifetime:				name = "LIFETIME";				break;
	case net_stun_reserved2:			name = "RESERVED2";				break;
	case net_stun_xor_peer_address:		name = "XOR-PEER-ADDRESS";		break;
	case net_stun_data:					name = "DATA";					break;
	case net_stun_xor_relayed_address:	name = "XOR-RELAY-ADDRESS";		break;
	case net_stun_even_port:			name = "EVEN-PORT";				break;
	case net_stun_requested_transport:	name = "REQUESTED-TRANSPORT";	break;
	case net_stun_dont_fragment:		name = "DONT-FRAGMENT";			break;
	case net_stun_reserved3:			name = "RESERVED3";				break;
	case net_stun_reservation_token:	name = "RESERVATION-TOKEN";		break;
	case net_stun_ice_priority:			name = "PRIORITY";				break;
	case net_stun_ice_use_candidate:	name = "USE-CANDIDATE";			break;
	case net_stun_ice_controlled:		name = "ICE-CONTROLLED";		break;
	case net_stun_ice_controlling:		name = "ICE-CONTROLLING";		break;
	};

	return name;
}
//--------------------------------------
