﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "stdafx.h"

#include "net/ip_address.h"
#include "net/ip_address6.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
ip_address6::ip_address6()
{
	first_init();
}
ip_address6::ip_address6(const ip_address6* ipaddr)
{
	first_init();
	copy_from(ipaddr);
}
ip_address6::ip_address6(const void* native)
{
	first_init();
	set_native(native);
}
ip_address6::ip_address6(const char* str)
{
	first_init();
	parse(str);
}
ip_address6::~ip_address6()
{
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void					ip_address6::first_init()
{
	m_native = in6addr_any;
	strncpy(m_text, "::", ARRAY_LEN(m_text));
	m_text[ARRAY_LEN(m_text)-1] = 0;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void					ip_address6::copy_from(const ip_address6* ipaddr)
{
	if (ipaddr != NULL)
	{
		memcpy(&m_native, &ipaddr->m_native, sizeof(m_native));
		memcpy(m_text, &ipaddr->m_text, sizeof(m_text));
	}
}
void					ip_address6::set_native(const void* native)
{
	if (native != NULL)
	{
		memcpy(&m_native, native, sizeof(m_native));
		update_text();
	}
}
void					ip_address6::parse(const char* str)
{
#if defined(TARGET_OS_WINDOWS)

	if (str != NULL)
	{
		bool success = false;

		addrinfo* result = NULL;

		addrinfo hints = {0};
		hints.ai_flags = AI_NUMERICHOST;

		if (getaddrinfo(str, NULL, &hints, &result) == 0)
		{
			if (result->ai_family == AF_INET6)
			{
				const sockaddr_in6* saddr6 = (const sockaddr_in6*)result->ai_addr;
				memcpy(&m_native, &saddr6->sin6_addr, sizeof(m_native));
				success = true;
			}

			freeaddrinfo(result);
		}

		if (!success)
			m_native= in6addr_any;

		update_text();
	}

#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)

	if (str != NULL)
	{
		if (inet_pton(AF_INET6, str, &m_native) != 1)
			m_native = in6addr_any;
			
		update_text();
	}

#else

#error not implemented!

#endif
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const void*				ip_address6::get_native() const
{
	return &m_native;
}
uint32_t				ip_address6::get_native_len()
{
	return sizeof(in6_addr);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const char*				ip_address6::to_string() const
{
	return m_text;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void					ip_address6::update_text()
{
#if defined(TARGET_OS_WINDOWS)

	if (is_any())
	{
		std_snprintf(m_text, INET6_ADDRSTRLEN, "::");
	}
	else if (is_loopback())
	{
		std_snprintf(m_text, INET6_ADDRSTRLEN, "::1");
	}
	else if (is_ipv4_compatible() || is_ipv4_mapped())
	{
		uint32_t pos = 0;

		const uint16_t* words = (const uint16_t*) &m_native;
		if (words[5] == 0)
			pos += std_snprintf(m_text + pos, INET6_ADDRSTRLEN - pos, "::");
		else
			pos += std_snprintf(m_text + pos, INET6_ADDRSTRLEN - pos, "::ffff:");

		const uint8_t* bytes = (const uint8_t*) &m_native;
		pos += std_snprintf(m_text + pos, INET6_ADDRSTRLEN - pos, "%d.", bytes[12]);
		pos += std_snprintf(m_text + pos, INET6_ADDRSTRLEN - pos, "%d.", bytes[13]);
		pos += std_snprintf(m_text + pos, INET6_ADDRSTRLEN - pos, "%d.", bytes[14]);
		pos += std_snprintf(m_text + pos, INET6_ADDRSTRLEN - pos, "%d", bytes[15]);
	}
	else
	{
		uint32_t pos = 0;
		const uint16_t* words = (const uint16_t*) &m_native;

		bool zeroSequence = false;

		for (int i = 0; i < 8; ++i)
		{
			if (!zeroSequence && words[i] == 0)
			{
				int zi = i;
				while (zi < 8 && words[zi] == 0)
					++zi;
				if (zi > i + 1)
				{
					i = zi;
					pos += std_snprintf(m_text + pos, INET6_ADDRSTRLEN - pos, ":");
					zeroSequence = true;
				}
			}

			if (i > 0)
				pos += std_snprintf(m_text + pos, INET6_ADDRSTRLEN - pos, ":");

			if (i < 8)
				pos += std_snprintf(m_text + pos, INET6_ADDRSTRLEN - pos, "%x", ntohs(words[i]));
		}
	}

	m_text[INET6_ADDRSTRLEN] = 0;

#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)

	if (inet_ntop(AF_INET6, &m_native, m_text, INET6_ADDRSTRLEN) == NULL)
	{
		m_text[0] = ':';
		m_text[1] = ':';
		m_text[2] = 0;
	}
	m_text[INET6_ADDRSTRLEN] = 0;

#else

#error not implemented!

#endif
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool					ip_address6::equals(const ip_address6* ip) const
{
	if (ip == NULL)
		return false;

	bool result = (strcmp(m_text, ip->m_text) == 0);
	return result;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const bool ip_address6::empty() const
{
#if defined(TARGET_OS_WINDOWS)

	if (is_any() || is_loopback())
	{
		return true;
	}
	
	return false;

#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	char buffer[INET6_ADDRSTRLEN+1];
	if (inet_ntop(AF_INET6, &m_native, buffer, INET6_ADDRSTRLEN) == NULL)
	{
		return true;
	}

	return false;
#else

#error not implemented!

#endif
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const bool ip_address6::fill_suitable_local_mask(char* str, uint32_t len) const
{
	return false;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#if defined(TARGET_OS_WINDOWS)
bool					ip_address6::is_any() const
{
	const uint16_t* words = (const uint16_t*)&m_native;
	return ((words[0] == 0) && (words[1] == 0) && (words[2] == 0) && (words[3] == 0) && (words[4] == 0) && (words[5] == 0) && (words[6] == 0) && (words[7] == 0));
}
bool					ip_address6::is_loopback() const
{
	const uint16_t* words = (const uint16_t*)&m_native;
	return ((words[0] == 0) && (words[1] == 0) && (words[2] == 0) && (words[3] == 0) && (words[4] == 0) && (words[5] == 0) && (words[6] == 0) && (ntohs(words[7]) == 0x0001));
}
bool					ip_address6::is_ipv4_compatible() const
{
	const uint16_t* words = (const uint16_t*)&m_native;
	return ((words[0] == 0) && (words[1] == 0) && (words[2] == 0) && (words[3] == 0) && (words[4] == 0) && (words[5] == 0));
}
bool					ip_address6::is_ipv4_mapped() const
{
	const uint16_t* words = (const uint16_t*)&m_native;
	return ((words[0] == 0) && (words[1] == 0) && (words[2] == 0) && (words[3] == 0) && (words[4] == 0) && (ntohs(words[5]) == 0xFFFF));
}
#endif
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
