﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "net_sock.h"
//--------------------------------------
//
//--------------------------------------
static const char* get_socket_state(net_socket_state_t state)
{
	static const char* names[] = { "SD_RECEIVE", "SD_SEND", "SD_BOTH", "INVALID" };

	return state >= 0 && state <= net_shutdown_closed ? names[state] : "ERROR";
}
//--------------------------------------
//
//--------------------------------------
RTX_NET_API bool net_socket_t::open(int af, int type, int protocol)
{
	close();
#if defined (TARGET_OS_WINDOWS)	
	m_handle = ::WSASocket(af, type, protocol, NULL, 0, WSA_FLAG_OVERLAPPED);
#elif defined (TARGET_OS_LINUX)	or defined(TARGET_OS_FREEBSD)
	m_handle = ::socket(af, type, protocol);
#endif

	return m_handle != INVALID_SOCKET;
}
//--------------------------------------
//
//--------------------------------------
RTX_NET_API bool net_socket_t::close()
{
	if (m_handle != INVALID_SOCKET)
	{
		bool result = ::closesocket(m_handle) == 0;
		m_handle = INVALID_SOCKET;
		m_shutdown = net_shutdown_closed;
		return result;
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
RTX_NET_API int net_socket_t::send_to(const void* packet, int size, int flags, const sockaddr* addr, int length)
{
	return ::sendto(m_handle, (const char*)packet, size, flags, addr, length);
}
//--------------------------------------
//
//--------------------------------------
RTX_NET_API int net_socket_t::send_to(const void* packet, int size, in_addr addr, uint16_t port)
{
	sockaddr_in r_addr = { 0 };

	r_addr.sin_family = AF_INET;
	r_addr.sin_addr = addr;
	r_addr.sin_port = htons(port);

	int r_addr_len = sizeof(r_addr);

	return ::sendto(m_handle, (const char*)packet, size, 0, (const sockaddr*)&r_addr, r_addr_len);
}
//--------------------------------------
//
//--------------------------------------
RTX_NET_API int net_socket_t::receive_select(void* buffer, int size, uint32_t timeout)
{
	int read = 0;
	int result = 0;

	if (m_handle != INVALID_SOCKET)
	{
		do
		{
			timeval tval;
			tval.tv_sec = 0;
			tval.tv_usec = timeout * 1000;

			fd_set reads;
			FD_ZERO(&reads);
			FD_SET(m_handle, &reads);

			if ((result = ::select(1, &reads, nullptr, nullptr, timeout >= 0? &tval : nullptr)) > 0)
			{
				if ((result = ::recv(m_handle, (char*)buffer + read, size - read, 0)) == SOCKET_ERROR)
				{
					LOG_NET_ERROR("tcp", "Socket::Receive recv() returns error %d.", std_get_error);
					return 0;
				}
				else if (result == 0)
				{
					LOG_NET("tcp", "Socket::Receive recv() returns 0. connectin shutdowned!");
					return 0;
				}

				// продолжаем чтение!
				read += result;
			}
			else if (result < 0)
			{
				// таймаут или ошибка чтения!
				LOG_NET_ERROR("tcp", "Socket::Receive select() returns error %d.", std_get_error);
				return 0;
			}
			else
			{
				LOG_NET("tcp", "Socket::Receive shutdown flag is set %s.", get_socket_state(m_shutdown));
				return 0;
			}
			/*else if (m_shutdown != net_socket_state_t::net_shutdown_closed)
			{
				LOG_NET("tcp", "Socket::Receive shutdown flag is set %s.",
					(m_shutdown == net_shutdown_both ? "SD_BOTH" :
					(m_shutdown == net_shutdown_recv ? "SD_RECEIVE" :
					(m_shutdown == net_shutdown_send ? "SD_SEND" : "UNKNOWN"))));

				return 0;
			}*/

			// продолжаем чтение!
		}
		while (read < size);
	}

	// количество удачно прочитанных байт
	return read;
}
//--------------------------------------
//
//--------------------------------------
#if defined (TARGET_OS_WINDOWS)
RTX_NET_API int net_socket_t::select(rtl::ArrayT<net_socket_t*>& readfds, uint32_t timeout)
{
	timeval tv;
	fd_set rfd;
	int i;

	memset(&rfd, 0, sizeof rfd);

	int readfds_count = readfds.getCount();

	if (readfds_count == 0)
		return 0;

	for (i = 0; i < readfds.getCount(); i++)
		FD_SET(readfds[i]->get_handle(), &rfd);

	tv.tv_sec = timeout / 1000;
	tv.tv_usec = (timeout % 1000) * 1000;

	int result = ::select(0, &rfd, nullptr, nullptr, timeout == 0 ? nullptr : &tv);

	if (result > 0)
	{
		for (i = readfds_count-1; i >= 0; i--)
		{
			if (!FD_ISSET(readfds[i]->get_handle(), &rfd))
			{
				readfds.removeAt(i);
			}
		}
	}
	else
	{
		readfds.clear();
	}

	return result;
}
//--------------------------------------
//
//--------------------------------------
RTX_NET_API int net_socket_t::select(rtl::ArrayT<net_socket_t*>* readfds, rtl::ArrayT<net_socket_t*>* writefds, rtl::ArrayT<net_socket_t*>* exceptfds, uint32_t timeout)
{
	timeval tv;
	fd_set rfd;
	fd_set wfd;
	fd_set efd;

	memset(&rfd, 0, sizeof rfd);
	memset(&wfd, 0, sizeof wfd);
	memset(&efd, 0, sizeof efd);

	int i;
	int readfds_count = 0;
	int writefds_count = 0;
	int exceptfds_count = 0;

	if (readfds != nullptr)
	{
		readfds_count = readfds->getCount();
		for (i = 0; i < readfds_count; i++)
			FD_SET(readfds->getAt(i)->get_handle(), &rfd);
	}

	if (writefds != nullptr)
	{
		writefds_count = writefds->getCount();
		for (i = 0; i < writefds_count; i++)
			FD_SET(writefds->getAt(i)->get_handle(), &wfd);
	}

	if (exceptfds != nullptr)
	{
		exceptfds_count = exceptfds->getCount();
		for (i = 0; i < exceptfds_count; i++)
			FD_SET(exceptfds->getAt(i)->get_handle(), &efd);
	}

	tv.tv_sec = timeout / 1000;
	tv.tv_usec = (timeout % 1000) * 1000;

	int result = ::select(
		0,
		readfds_count > 0 ? &rfd : nullptr,
		writefds_count > 0 ? &wfd : nullptr,
		exceptfds_count > 0 ? &efd : nullptr,
		timeout == 0 ? nullptr : &tv
	);

	if (result >= 0)
	{
		if (readfds != nullptr)
		{
			for (i = readfds_count-1; i >= 0; i--)
			{
				if (!FD_ISSET(readfds->getAt(i)->get_handle(), &rfd))
				{
					readfds->removeAt(i);
				}
			}
		}

		if (writefds != nullptr)
		{
			for (i = writefds_count-1; i >= 0; i--)
			{
				if (!FD_ISSET(writefds->getAt(i)->get_handle(), &wfd))
				{
					writefds->removeAt(i);
				}
			}
		}

		if (exceptfds != nullptr)
		{
			for (i = exceptfds_count-1; i >= 0; i--)
			{

				if (FD_ISSET(exceptfds->getAt(i)->get_handle(), &efd))
				{
					exceptfds->removeAt(i);
				}
			}
		}
	}

	return result;
}
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
#define MAX_SELECT_COUNT 64
//--------------------------------------
//
//--------------------------------------
RTX_NET_API int net_socket_t::select(rtl::ArrayT<net_socket_t*>& readfds, uint32_t timeout)
{
	return net_socket_t::select(&readfds, nullptr, nullptr, timeout);
}
//--------------------------------------
//
//--------------------------------------
RTX_NET_API int net_socket_t::select(rtl::ArrayT<net_socket_t*>* readfds, rtl::ArrayT<net_socket_t*>* writefds, rtl::ArrayT<net_socket_t*>* exceptfds, uint32_t timeout)
{
	//---------------------------------------------------------
	// если нет параметров, то отрабатываем просто как sleep(timeout)
	//---------------------------------------------------------

	if ((readfds == NULL) && (writefds == NULL) && (exceptfds == NULL))
	{
		rtl::Thread::sleep(timeout);
		return true;
	}

	//---------------------------------------------------------
	// убедимся, что нам не передали слшиком много сокетов.
	//---------------------------------------------------------

	if ((readfds != NULL) && (readfds->getCount() > MAX_SELECT_COUNT))
		return false;

	if ((writefds != NULL) && (writefds->getCount() > MAX_SELECT_COUNT))
		return false;

#if defined(TARGET_OS_LINUX)
	if ((exceptfds != NULL) && (exceptfds->getCount() > MAX_SELECT_COUNT))
		return false;
#elif defined(TARGET_OS_FREEBSD)
	if ((exceptfds != NULL) && (exceptfds->getCount() > 0))
    {
		LOG_NET_ERROR("net-sock", "net_socket_t::select -- impossible to receive \"error\" events.");
		return false;
    }
#endif

	//---------------------------------------------------------
	// заполняем массивчик для создания объекта epoll/kqueue
	//---------------------------------------------------------

#if defined(TARGET_OS_LINUX)
	epoll_event events[3 * MAX_SELECT_COUNT];
#elif defined(TARGET_OS_FREEBSD)
	struct kevent events[2 * MAX_SELECT_COUNT];
#endif
    memset(events, 0, sizeof(events));
	uint32_t evcount = 0;

	/*LOG_NET("net-sock", "net_socket_t::select: events:%p count:%u sockets:%p sockets_count:%u sockets[0]:%" PRIX64 " %u",
			events, evcount,
			readfds, readfds != nullptr ? readfds->getCount() : 0,
			readfds != nullptr ? (uintptr_t)readfds->getAt(0) : 0xFFFFFFFFFFFFFFFF, EPOLLIN);*/

	if ((readfds != NULL) && readfds->getCount() > 0)
#if defined(TARGET_OS_LINUX)
		evcount = add_events(events, evcount, readfds, EPOLLIN);
#elif defined(TARGET_OS_FREEBSD)
		evcount = add_events(events, evcount, readfds, EVFILT_READ);
#endif

	if ((writefds != NULL) && writefds->getCount() > 0)
#if defined(TARGET_OS_LINUX)
		evcount = add_events(events, evcount, writefds, EPOLLOUT);
#elif defined(TARGET_OS_FREEBSD)
		evcount = add_events(events, evcount, writefds, EVFILT_WRITE);
#endif

#if defined(TARGET_OS_LINUX)
	if ((exceptfds != NULL) && exceptfds->getCount() > 0)
		evcount = add_events(events, evcount, exceptfds, EPOLLERR);
#endif

	//---------------------------------------------------------
	// создаем объект epoll/kqueue
	//---------------------------------------------------------

#if defined(TARGET_OS_LINUX)
	int epollfd = epoll_create(evcount);
	if (epollfd < 0)
#elif defined(TARGET_OS_FREEBSD)
	int kqfd = kqueue();
	if (kqfd < 0)
#endif
	{
		int err = errno;
#if defined(TARGET_OS_LINUX)
		LOG_NET_ERROR("net-sock", "net_socket_t::select -- epoll_create() failed. error = %d.", err);
#elif defined(TARGET_OS_FREEBSD)
		LOG_NET_ERROR("net-sock", "net_socket_t::select -- kqueue() failed. error = %d.", err);
#endif
		return 0;
	}

#if defined(TARGET_OS_LINUX)
	for (uint32_t i = 0; i < evcount; ++i)
	{
		epoll_event* ev = events + i;
		net_socket_t* sock = (net_socket_t*) ev->data.ptr;
		SOCKET sockfd = sock->m_handle;
		if (sockfd != INVALID_SOCKET)
		{
			int error = epoll_ctl(epollfd, EPOLL_CTL_ADD, sockfd, ev);
			if (error < 0)
			{
				int err = errno;
				LOG_NET_ERROR("net-sock", "net_socket_t::select -- epoll_ctl() failed. error = %d.", err);
				int res = ::close(epollfd);
				if (res < 0)
				{
					err = errno;
					LOG_NET_WARN("net-sock", "net_socket_t::select -- close() failed. error = %d.", err);
				}
				return 0;
			}
		}
	}
#elif defined(TARGET_OS_FREEBSD)
    int error = kevent(kqfd, events, evcount, NULL, 0, NULL);
    if (error < 0)
    {
        int err = errno;
        LOG_NET_ERROR("net-sock", "net_socket_t::select -- kevent() failed. error = %d.", err);
        int res = ::close(kqfd);
        if (res < 0)
        {
            err = errno;
            LOG_NET_WARN("net-sock", "net_socket_t::select -- close() failed. error = %d.", err);
        }
        return 0;
    }
#endif

	//---------------------------------------------------------
	// основная функция epoll_wait()/kevent() с ожиданием событий
	//---------------------------------------------------------

#if defined(TARGET_OS_LINUX)
	epoll_event outevents[3 * MAX_SELECT_COUNT];
#elif defined(TARGET_OS_FREEBSD)
	struct kevent outevents[2 * MAX_SELECT_COUNT];
#endif
    memset(outevents, 0, sizeof(outevents));

    int rc = 0;
	uint32_t remaining = timeout;
#if defined(TARGET_OS_FREEBSD)
    struct timespec remaining_ts = {remaining / 1000, (remaining % 1000) * 1000 * 1000};
#endif

	while (true)
	{
		uint64_t start = rtl::DateTime::getTicks64(); // XXX better take 'start' out of the loop and assign it ONCE!

#if defined(TARGET_OS_LINUX)
		rc = epoll_wait(epollfd, outevents, evcount, int(remaining));
#elif defined(TARGET_OS_FREEBSD)
        rc = kevent(kqfd, NULL, 0, outevents, sizeof(outevents), &remaining_ts);
#endif

		if (rc >= 0)
			break;

		// жопа какая-то
		if (errno != EINTR)
		{
			int err = errno;
#if defined(TARGET_OS_LINUX)
			LOG_NET_WARN("net-sock", "net_socket_t::select -- epoll_wait() failed. error = %d.", err);
#elif defined(TARGET_OS_FREEBSD)
			LOG_NET_WARN("net-sock", "net_socket_t::select -- kevent() failed. error = %d.", err);
#endif
			break;
		}

#if defined(TARGET_OS_LINUX)
		LOG_NET_ERROR("net-sock", "net_socket_t::select -- epoll_wait() failed. error = EINTR.");
#elif defined(TARGET_OS_FREEBSD)
		LOG_NET_ERROR("net-sock", "net_socket_t::select -- kevent() failed. error = EINTR.");
#endif

		uint64_t elapsed = rtl::DateTime::getTicks64() - start;
		if (elapsed >= remaining)
			break;

		remaining -= elapsed;
#if defined(TARGET_OS_FREEBSD)
        remaining_ts = {remaining / 1000, (remaining % 1000) * 1000 * 1000};
#endif
	}

	//---------------------------------------------------------
	// уничтожаем объект epoll/kqueue
	//---------------------------------------------------------

#if defined(TARGET_OS_LINUX)
	int res = ::close(epollfd);
#elif defined(TARGET_OS_FREEBSD)
	int res = ::close(kqfd);
#endif
	if (res < 0)
	{
		int err = errno;
		LOG_NET_WARN("net-sock", "net_socket_t::select -- close() failed. error = %d.", err);
	}

	if (rc < 0)
		return 0;

    //---------------------------------------------------------
	// подготавливаем списки сокетов для возврата
	//---------------------------------------------------------

	if (readfds != NULL)
		readfds->clear();

	if (writefds != NULL)
		writefds->clear();

	if (exceptfds != NULL)
		exceptfds->clear();

	int result = 0;

	for (int i = 0; i < rc; ++i)
	{
#if defined(TARGET_OS_LINUX)
		epoll_event* ev = outevents + i;
#elif defined(TARGET_OS_FREEBSD)
		struct kevent* ev = outevents + i;
#endif

#if defined(TARGET_OS_LINUX)
		if ((readfds != NULL) && (ev->events & EPOLLIN))
#elif defined(TARGET_OS_FREEBSD)
		if ((readfds != NULL) && (ev->filter == EVFILT_READ))
#endif
		{
#if defined(TARGET_OS_LINUX)
			net_socket_t* sock = (net_socket_t*)ev->data.ptr;
#elif defined(TARGET_OS_FREEBSD)
			net_socket_t* sock = (net_socket_t*)ev->udata;
#endif
			readfds->add(sock);
			result++;
		}

#if defined(TARGET_OS_LINUX)
		if ((writefds != NULL) && (ev->events & EPOLLOUT))
#elif defined(TARGET_OS_FREEBSD)
		if ((writefds != NULL) && (ev->filter == EVFILT_WRITE))
#endif
		{
#if defined(TARGET_OS_LINUX)
			net_socket_t* sock = (net_socket_t*)ev->data.ptr;
#elif defined(TARGET_OS_FREEBSD)
			net_socket_t* sock = (net_socket_t*)ev->udata;
#endif
			writefds->add(sock);
			result++;
		}

#if defined(TARGET_OS_LINUX)
		if ((exceptfds != NULL) && (ev->events & EPOLLERR))
		{
			net_socket_t* sock = (net_socket_t*)ev->data.ptr;
			exceptfds->add(sock);
			result++;
		}
#endif
	}

	return result;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#if defined(TARGET_OS_LINUX)
uint32_t net_socket_t::add_events(epoll_event* events, uint32_t count, rtl::ArrayT<net_socket_t*>* sockets, int flag)
#elif defined(TARGET_OS_FREEBSD)
uint32_t net_socket_t::add_events(struct kevent* events, uint32_t count, rtl::ArrayT<net_socket_t*>* sockets, short filter)
#endif
{
	//LOG_NET("net-sock", "add events : events:%p count:%u sockets:%p sockets_count:%u sockets[0]:%" PRIX64 " %u", events, count, sockets, sockets != nullptr ? sockets->getCount() : 0, sockets != nullptr ? (uintptr_t)sockets->getAt(0) : 0xFFFFFFFFFFFFFFFF, flag);

#if defined(TARGET_OS_LINUX)
	epoll_event* evlast = events + count;
#elif defined(TARGET_OS_FREEBSD)
	struct kevent* evlast = events + count;
#endif

	for (int i = 0; i < sockets->getCount(); i++)
	{
		const net_socket_t* sockcur = sockets->getAt(i);

		//LOG_NET("net-sock", "add events : sock[%d] = %p", i, sockcur);

		if (sockcur->m_handle != INVALID_SOCKET)
		{
#if defined(TARGET_OS_LINUX)
			epoll_event* evcur = events;

			for (; evcur != evlast; ++evcur)
			{
				const net_socket_t* sockadded = (const net_socket_t*) evcur->data.ptr;
				if (sockadded->m_handle == sockcur->m_handle)
				{
					//LOG_NET("net-sock", "add events : sock found %u", sockcur->m_handle);
					break;
				}
			}

			if (evcur == evlast)
			{
				evcur->data.ptr = (void*) sockcur;
				++evlast;
			}

			evcur->events |= flag;
#elif defined(TARGET_OS_FREEBSD)
            EV_SET(evlast, sockcur->m_handle, filter, EV_ADD /* | EV_CLEAR ? */, 0, 0, (void*) sockcur);
            ++evlast;
#endif
		}
	}

	count = evlast - events;

	return count;
}
#endif
//--------------------------------------
