﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "net_nat_ctx.h"

//--------------------------------------
// NAT Traversal helper functions using STUN, TURN and ICE.
//--------------------------------------
//--------------------------------------
// Creates new NAT context.
//--------------------------------------
net_nat_context_t::net_nat_context_t(rtl::Logger* log) : m_log(log)
{
	m_username = nullptr;
	m_password = nullptr;

	m_server_port = NET_NAT_TCP_UDP_DEFAULT_PORT;
		
	//	7.2.1.  Sending over UDP : In fixed-line access links, a value of 500 ms is RECOMMENDED.
	m_RTO = NET_NAT_DEFAULT_RTO;
	//	7.2.1.  Sending over UDP : Rc SHOULD be configurable and SHOULD have a default of 7.
	m_Rc = NET_NAT_DEFAULT_RC;
		
	m_software = nullptr;

	//m_enable_evenport = 1;
	//m_enable_fingerprint = 1;
	m_enable_integrity = 0;
	m_enable_dontfrag = 1;//TNET_SOCKET_TYPE_IS_DGRAM(m_socket_type) ? 1 : 0;
}
//--------------------------------------
//
//--------------------------------------
net_nat_context_t::~net_nat_context_t()
{ 
	FREE(m_username);
	FREE(m_password);
	FREE(m_software);

	for (int i = 0; i < m_allocations.getCount(); i++)
	{
		net_turn_allocation_t* allocation = m_allocations[i];
		DELETEO(allocation);
	}

	for (int i = 0; i < m_stun_bindings.getCount(); i++)
	{
		net_stun_binding_t* binding = m_stun_bindings[i];
		DELETEO(binding);
	}
}
//--------------------------------------
//
//--------------------------------------
void net_nat_context_t::initialize(const char* username, const char* password)
{
	m_username = rtl::strdup(username);
	m_password = rtl::strdup(password);

	m_server_port = NET_NAT_TCP_UDP_DEFAULT_PORT;
		
	//	7.2.1.  Sending over UDP : In fixed-line access links, a value of 500 ms is RECOMMENDED.
	m_RTO = NET_NAT_DEFAULT_RTO;
	//	7.2.1.  Sending over UDP : Rc SHOULD be configurable and SHOULD have a default of 7.
	m_Rc = NET_NAT_DEFAULT_RC;
		
	m_software = rtl::strdup("IM-client/OMA1.0 doubango/v0.0.0");

	//m_enable_evenport = 1;
	//m_enable_fingerprint = 1;
	m_enable_integrity = 0;
	m_enable_dontfrag = 1;//TNET_SOCKET_TYPE_IS_DGRAM(m_socket_type) ? 1 : 0;
}
//--------------------------------------
//
//--------------------------------------
void net_nat_context_t::set_server_address(const ip_address_t* server_address)
{
	m_server_address.copy_from(server_address);
}
//--------------------------------------
//
//--------------------------------------
void net_nat_context_t::set_server(const ip_address_t* server_address, uint16_t server_port)
{
	m_server_address.copy_from(server_address);
	m_server_port = server_port;
}
//--------------------------------------
// Creates and sends a STUN2 binding request to the STUN/TURN server in order to get the server reflexive
//			address associated to this file descriptor (or socket). The caller should call @ref tnet_nat_stun_unbind to destroy the binding.
//--------------------------------------
net_stun_binding_id_t net_nat_context_t::stun_bind(net_socket* localFD)
{
	//return net_stun_bind(this, localFD);
	net_stun_binding_id_t id = NET_STUN_INVALID_BINDING_ID;

	net_stun_binding_t *binding = nullptr;

	if (localFD->get_native_handle() != INVALID_SOCKET)
	{
		binding = NEW net_stun_binding_t(m_log);
		binding->initialize(localFD, &m_server_address, m_server_port, m_username, m_password);

		if (binding->send_bind(this))
		{
			DELETEO(binding);
		}
		else
		{
			id = binding->get_id();
			m_stun_bindings.add(binding);
		}
	}

	return id;
}
//--------------------------------------
// Creates and sends a STUN2 binding request to the STUN/TURN server in order to get the server reflexive
//			address associated to this file descriptor (or socket). The caller should call @ref tnet_nat_stun_unbind to destroy the binding.
//--------------------------------------
//net_stun_binding_id_t net_nat_context_t::stun_unbind(net_stun_binding_id_t id)
//{
//	return net_stun_bind(this, localFD);
//}
//--------------------------------------
// Gets the server reflexive address associated to this STUN2 binding.
//--------------------------------------
int net_nat_context_t::stun_get_reflexive_address(net_stun_binding_id_t id, char** ipaddress, uint16_t *port) const
{
	net_stun_binding_t *binding = find_stun_binding(id);
	/*STUN2: XOR-MAPPED-ADDRESS */
	if (binding->has_xmaddr())
	{
		int ret = 0;
		if (ipaddress)
		{
			ret = net_stun_address_to_string(&binding->get_xmaddr(), ipaddress);
		}
		
		if (port)
		{
			*port = binding->get_xmaddr().port;
		}
		
		return ret;
	}

	/*STUN1: MAPPED-ADDRESS*/
	if (binding->has_maddr())
	{
		int ret = 0;
		if (ipaddress)
		{
			ret = net_stun_address_to_string(&binding->get_maddr(), ipaddress);
		}
		
		if (port)
		{
			*port = binding->get_maddr().port;
		}
		
		return ret;
	}

	return -1;
}
//--------------------------------------
// Creates TURN allocation as per draft-ietf-behave-turn-16 subclause 6. This function  will also 
//			send an allocation request to the server (subclause 6.1).
//--------------------------------------
net_turn_allocation_id_t net_nat_context_t::turn_allocate(net_socket* localFD)
{
	net_turn_allocation_id_t id = NET_TURN_INVALID_ALLOCATION_ID;

	int ret;
	net_turn_allocation_t* allocation = NEW net_turn_allocation_t(m_log);
	allocation->intitalize(this, localFD, &m_server_address, m_server_port, m_username, m_password);
		
	allocation->set_software(m_software);
		
	if ((ret = allocation->send_allocate()))
	{
		PLOG_NET_ERROR("net-stun", "TURN allocation failed with error");
		DELETEO(allocation);
	}
	else
	{
		id = allocation->get_id();
		m_allocations.add(allocation);
	}

	return id;
}
//--------------------------------------
// Gets the STUN server-refelexive IP address and port associated to this TURN allocation.
//--------------------------------------
int net_nat_context_t::turn_get_reflexive_address(net_turn_allocation_id_t id, char** ipaddress, uint16_t *port) const
{
	net_turn_allocation_t *allocation = find_turn_allocation(id);
	
	/*STUN2: XOR-MAPPED-ADDRESS */
	if (allocation->get_xmapped_address().family != -1)
	{
		int ret = net_stun_address_to_string(&allocation->get_xmapped_address(), ipaddress);
		*port = allocation->get_xmapped_address().port;
		return ret;
	}

	/*STUN1: MAPPED-ADDRESS*/
	if (allocation->get_mapped_address().family != -1)
	{
		int ret = net_stun_address_to_string(&allocation->get_mapped_address(), ipaddress);
		*port = allocation->get_mapped_address().port;
		return ret;
	}

	return -1;
}
//--------------------------------------
// Refresh a TURN allocation previously created using @ref tnet_nat_turn_allocate.
//--------------------------------------
int net_nat_context_t::turn_allocation_refresh(net_turn_allocation_id_t id) const
{
	net_turn_allocation_t *allocation = find_turn_allocation(id);
	return allocation->send_refresh();
}
//--------------------------------------
// Unallocate/remove a TURN allocation from the server.
int net_nat_context_t::turn_unallocate(net_turn_allocation_id_t id)
{
	net_turn_allocation_t *allocation = find_turn_allocation(id);
	int ret = allocation->send_unallocate();
	//tsk_list_remove_item_by_data(nat_context->allocations, allocation);
	m_allocations.remove(allocation);
	return ret;
}
//--------------------------------------
// Creates TURN channel binding as per draft-ietf-behave-turn-16 sublause 11 and send it to the
//			server as per subclause 11.1.
//--------------------------------------
net_turn_channel_binding_id_t net_nat_context_t::turn_channel_bind(net_turn_allocation_id_t id, const ip_address_t* address, uint16_t port) const
{
	net_turn_allocation_t *allocation = find_turn_allocation(id);
	return allocation->channel_bind(address, port);
}
//--------------------------------------
//
//--------------------------------------
int net_nat_context_t::turn_channel_refresh(net_turn_channel_binding_id_t id)
{
	for (int i = 0; i < m_allocations.getCount(); i++)
	{
		net_turn_allocation_t* allocation = m_allocations[i]; //(((tnet_turn_allocation_t *)curr->data)->channel_bindings, __pred_find_turn_channel_binding, &id);
		const net_turn_channel_binding_t* binding = allocation->find_binding(id);

		if (binding != nullptr)
		{
			return allocation->channel_refresh(binding);
		}
	}

	return -1;
}
//--------------------------------------
//
//--------------------------------------
int net_nat_context_t::turn_channel_send(net_turn_channel_binding_id_t id, const void* data, int size, int indication) const
{
	if (!data || !size)
		return -1;

	for (int i = 0; i < m_allocations.getCount(); i++)
	{
		net_turn_allocation_t* allocation = m_allocations[i]; //(((tnet_turn_allocation_t *)curr->data)->channel_bindings, __pred_find_turn_channel_binding, &id);
		const net_turn_channel_binding_t* binding = allocation->find_binding(id);

		if (binding != nullptr)
		{
			return allocation->channel_senddata(binding, data, size, indication);
		}
	}

	return -1;
}
//--------------------------------------
//
//--------------------------------------
int net_nat_context_t::turn_add_permission(net_turn_allocation_id_t id, const char* ipaddress, uint32_t timeout) const
{
	net_turn_allocation_t *allocation = find_turn_allocation(id);
	
	if (allocation)
	{
		return allocation->add_permission(ipaddress, timeout);
	}

	return -1;
}
//--------------------------------------
//
//--------------------------------------
net_stun_binding_t* net_nat_context_t::find_stun_binding(net_stun_binding_id_t id) const
{
	for (int i = 0; i < m_stun_bindings.getCount(); i++)
	{
		if (m_stun_bindings[i]->get_id() == id)
			return m_stun_bindings[i];
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
net_turn_allocation_t* net_nat_context_t::find_turn_allocation(net_turn_allocation_id_t id) const
{
	for (int i = 0; i < m_allocations.getCount(); i++)
	{
		if (m_allocations[i]->get_id() == id)
			return m_allocations[i];
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
net_stun_request_t* net_nat_context_t::create_request(net_turn_allocation_t* allocation, net_stun_message_type_t type)
{
	net_stun_request_t *request = NEW net_stun_message_t(type, m_username, m_password);

	request->set_flags(m_enable_fingerprint, m_enable_integrity, m_enable_dontfrag, false);
	
	request->set_realm(allocation->get_realm());
	request->set_nonce(allocation->get_nonce());

	// Create random transaction id
	request->generate_transaction_id();

	// Add software attribute
	if (allocation->get_software())
	{
		request->add_attribute_data(net_stun_software, allocation->get_software(), int(strlen(allocation->get_software())));
	}

	return request;
}
//--------------------------------------
