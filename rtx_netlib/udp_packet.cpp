﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "net/udp_packet.h"
#include "net/ip_packet.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
udp_packet::udp_packet()
{
	reset();
}
udp_packet::~udp_packet()
{

}
//------------------------------------------------------------------------------
// распаковка udp из буфера.
//------------------------------------------------------------------------------
bool					udp_packet::read(const void* data, uint32_t length)
{
	if (length < FIXED_UDP_HEADER_LENGTH)
	{
		return false;
	}
	if (length > FIXED_UDP_HEADER_LENGTH + BUFFER_SIZE)
	{
		return false;
	}

	reset();

	const uint8_t* pdata = (const uint8_t*) data;

	uint32_t size = 0;

	//------------------------------

	m_sport = *(uint16_t*)(pdata + size);
	size += sizeof(uint16_t);
	m_sport = ntohs(m_sport);

	//------------------------------

	m_dport = *(uint16_t*)(pdata + size);
	size += sizeof(uint16_t);
	m_dport = ntohs(m_dport);

	//------------------------------

	m_ulen = *(uint16_t*)(pdata + size);
	size += sizeof(uint16_t);
	m_ulen = ntohs(m_ulen);

	//------------------------------

	m_sum = *(uint16_t*)(pdata + size);
	size += sizeof(uint16_t);
	m_sum = ntohs(m_sum);

	//------------------------------

	m_buff_length = length - size;
	memcpy(m_buff, pdata + size, m_buff_length);

	return true;
}
//------------------------------------------------------------------------------
// упаковка udp в буфер.
//------------------------------------------------------------------------------
uint32_t				udp_packet::write(void* data, uint32_t size) const
{
	if ((data == NULL) || (size == 0))
	{
		return 0;
	}

	if (size < FIXED_UDP_HEADER_LENGTH)
	{
		return 0;
	}

	uint16_t* pdata = (uint16_t*)data;

	uint16_t chek = 0;

	uint32_t length = 0;

	//------------------------------
	
	chek = htons(m_sport);
	memcpy(pdata, &chek, sizeof(uint16_t));
	length += sizeof(uint16_t);

	//------------------------------

	chek = htons(m_dport);
	memcpy(pdata + 1, &chek, sizeof(uint16_t));
	length += sizeof(uint16_t);

	//------------------------------
	
	chek = htons(m_ulen);
	memcpy(pdata + 2, &chek, sizeof(uint16_t));
	length += sizeof(uint16_t);

	//------------------------------

	chek = htons(m_sum);
	memcpy(pdata + 3, &chek, sizeof(uint16_t));
	length += sizeof(uint16_t);

	//------------------------------

	if (size - length < m_buff_length)
	{
		memcpy(pdata + 4, m_buff, size - length);
		length = size;
	}
	else
	{
		memcpy(pdata + 4, m_buff, m_buff_length);
		length += m_buff_length;
	}

	return length;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void					udp_packet::reset()
{
	m_sport = 0;
	m_dport = 0;
	m_ulen = FIXED_UDP_HEADER_LENGTH;
	m_sum = 0;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint16_t				udp_packet::get_source_port() const
{
	return m_sport;
}
void					udp_packet::set_source_port(uint16_t value)
{
	m_sport = value;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint16_t				udp_packet::get_destination_port() const
{
	return m_dport;
}
void					udp_packet::set_destination_port(uint16_t value)
{
	m_dport = value;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint16_t				udp_packet::get_udp_length() const
{
	return m_ulen;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint16_t				udp_packet::in_cksum_udp(int src, int dst)
{
	m_sum = 0;

	uint8_t buf[20] = {0};

	uint32_t size = 0;
	memcpy(buf, &src, sizeof(src));
	size += sizeof(src);
	memcpy(buf + size, &dst, sizeof(dst));
	size += sizeof(dst);

	memset(buf + size, 0, sizeof(uint8_t));
	size += sizeof(uint8_t); //pad = 0

	uint8_t proto = 17; //IPPROTO_UDP
	memcpy(buf + size, &proto, sizeof(uint8_t));
	size += sizeof(uint8_t);

	uint16_t len = htons(FIXED_UDP_HEADER_LENGTH);
	memcpy(buf + size, &len, sizeof(uint16_t));
	size += sizeof(uint16_t);

	write(buf + size, 8);

	m_sum = ip_packet::in_cksum((uint16_t*)buf, size + FIXED_UDP_HEADER_LENGTH); // 12 + 8

	return m_sum;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const uint8_t*			udp_packet::get_buffer() const
{
	return m_buff;
}
uint32_t				udp_packet::get_buffer_length() const
{
	return m_buff_length;
}
uint32_t				udp_packet::set_buffer(const void* data, uint32_t length)
{
	if ((data == NULL) || (length == 0))
	{
		return 0;
	}

	if (length > BUFFER_SIZE)
	{
		length = BUFFER_SIZE;
	}

	memcpy(m_buff, data, length);
	m_buff_length = length;

	m_ulen = uint16_t(FIXED_UDP_HEADER_LENGTH + m_buff_length);

	return m_buff_length;
}
//------------------------------------------------------------------------------
