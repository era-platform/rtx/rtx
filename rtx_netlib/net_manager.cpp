﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "net_manager.h"
//--------------------------------------
//
//--------------------------------------
//class net_monitor_thread_t : rtl::IThreadUser
//{
//	rtl::Thread m_thread;
//	HANDLE m_net_notify_handle;
//	OVERLAPPED m_net_event;
//	volatile bool m_stop_flag;
//
//public:
//	net_monitor_thread_t();
//	~net_monitor_thread_t();
//
//	void start();
//	void stop();
//
//private:
//	/// rtl::IThreadUser
//	virtual void thread_run(rtl::Thread* thread);
//
//	void event_raised();
//
//	void raise_network_failed();
//	void raise_addresses_added(const rtl::ArrayT<in_addr>& list);
//	void raise_addresses_removed(const rtl::ArrayT<in_addr>& list);
//};
//--------------------------------------
//
//--------------------------------------
static rtl::Mutex g_net_sync;
static volatile bool g_net_started = false;
static rtl::ArrayT<in_addr> g_net_addr_table;
//static rtl::ArrayT<net_monitor_t*> g_net_monitor_list;
//static net_monitor_thread_t g_net_monitor;
//--------------------------------------
//
//--------------------------------------
RTX_NET_API bool net_t::start()
{
#if defined (TARGET_OS_WINDOWS)
	if (!g_net_started)
	{
		WSAData d;
		int result = WSAStartup(MAKEWORD(2, 0), &d);
		g_net_started = result == 0;

		if (result != 0)
		{
			LOG_NET_ERROR("net-mon", "net start : WSAStartup() failed"); // std_get_error,
			return false;
		}

		// получим интерфейсную таблицу
		//get_interface_count();
		// запуск потока обновления таблыцы
		//g_net_monitor.start();
	}
	return g_net_started;
#else
	return true;
#endif

}
//--------------------------------------
//
//--------------------------------------
RTX_NET_API void net_t::stop()
{
#if defined (TARGET_OS_WINDOWS)
	if (g_net_started)
	{
		WSACleanup();

		//g_net_monitor.stop();
	}
#endif
}
//--------------------------------------
// получение локального интерфеса и адреса домена для указанного домена или адреса
//--------------------------------------
//static bool net_manager_get_best_iface_index(in_addr dst, uint32_t& index)
//{
//	IPAddr dst_addr = dst.s_addr;
//	return ::GetBestInterface(dst_addr, (DWORD*)&index) == NO_ERROR;
//}
//--------------------------------------
//
//--------------------------------------
RTX_NET_API bool net_t::get_best_interface(in_addr remote_ip, in_addr& local_ip)
{
	uint16_t destPort = 80;

	SOCKET sock = ::socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (INVALID_SOCKET == sock)
	{
		return false;
	}

	sockaddr_in RecvAddr = { 0 };

	RecvAddr.sin_family = AF_INET;
	RecvAddr.sin_port = htons(destPort);
	RecvAddr.sin_addr = remote_ip;

	bool result = false;

	if (::connect(sock, (sockaddr*)&RecvAddr, sizeof(RecvAddr)) == 0)
	{
		sockaddr_in saddr;
		socklen_t slen = sizeof(saddr);

		if (getsockname(sock, (sockaddr*)&saddr, &slen) == 0)
		{
			local_ip = saddr.sin_addr;
			result = true;
		}
	}

	closesocket(sock);

	return result;
}
//--------------------------------------
//
//--------------------------------------
RTX_NET_API bool net_t::get_host_by_name(const char* domain, in_addr& domain_ip, in_addr& iface)
{
	iface.s_addr = domain_ip.s_addr = INADDR_NONE;

	if (domain == nullptr || domain[0] == 0)
	{
		//LOG_NET_ERROR(L"net-mon", L"get_host_by_name(%s, ip, iface) failed : bad parameter : empty string", domain);
		return false;
	}


	if (strcmp(domain, "(null)") == 0)
	{
		//LOG_NET_ERROR(L"net-mon", L"get_host_by_name(%s, ip, iface) failed : bad parameter : (null)", domain);

		return false;
	}

	if (strstr(domain, ".invalid") != nullptr)
	{
		//LOG_NET_ERROR(L"net-mon", L"get_host_by_name(%s, ip, iface) failed : bad parameter : domain is '.invalid'", domain);

		return false;
	}

	int call_res;
	bool res = false;
//	bool localAddr = false;

	iface.s_addr = htonl(INADDR_LOOPBACK);

	if (std_stricmp(domain, "localhost") == 0)
	{
		domain_ip.s_addr = htonl(INADDR_LOOPBACK);
		return true;
	}

	// попробуем парчить как йпи
	domain_ip.s_addr = inet_addr(domain);

	if (domain_ip.s_addr == INADDR_NONE || domain_ip.s_addr == INADDR_ANY)
	{
		addrinfo		hints;
		addrinfo*		result = nullptr;
		addrinfo*		ptr = nullptr;
		sockaddr_in*	addr_ipv4;

		memset(&hints, 0, sizeof hints);
		hints.ai_family = AF_INET;
		hints.ai_socktype = SOCK_DGRAM;
		hints.ai_protocol = IPPROTO_UDP;

		if ((call_res = getaddrinfo(domain, "0", &hints, &result)) != 0)
		{
			LOG_NET_ERROR("net-mon", "get_host_by_name(%s) failed : getaddrinfo() failed with error: %d", domain, call_res);
			return false;
		}

		for (ptr = result; ptr != nullptr ;ptr = ptr->ai_next)
		{
			if (ptr->ai_family == AF_INET && ptr->ai_socktype == SOCK_DGRAM && ptr->ai_protocol == IPPROTO_UDP)
			{
				addr_ipv4 = (sockaddr_in*)ptr->ai_addr;

				if (!ip_is_valid(addr_ipv4->sin_addr))
				{
					continue;
				}

				if (ip_is_loopback(addr_ipv4->sin_addr))
				{
					iface = addr_ipv4->sin_addr;
//					localAddr = true;
					res = true;
					continue;
				}

				if (get_best_interface(addr_ipv4->sin_addr, iface))
				{
					domain_ip = addr_ipv4->sin_addr;
					res = true;
					break;
				}
			}
		}

		if (result != nullptr)
			freeaddrinfo(result);
	}
	else
	{
		if (get_best_interface(domain_ip, iface))
		{
			res = true;
		}
	}

	if (ip_is_loopback(iface) && !ip_is_loopback(domain_ip))
	{
		iface = domain_ip;
	}

	return res;
}
//--------------------------------------
//
//--------------------------------------
RTX_NET_API in_addr net_t::get_host_by_name(const char* host)
{
	in_addr result;

	result.s_addr = INADDR_NONE;

	if (host == nullptr || host[0] == 0)
	{
		//LOG_NET_ERROR(L"net-mon", L"get_host_by_name(%s) failed : bad parameter : empty string", host);
		return result;
	}


	if (strcmp(host, "(null)") == 0)
	{
		//LOG_NET_ERROR(L"net-mon", L"get_host_by_name(%s) failed : bad parameter : (null)", host);

		return result;
	}

	if (strstr(host, ".invalid") != nullptr)
	{
		//LOG_NET_ERROR(L"net-mon", L"get_host_by_name(%s) failed : bad parameter : host is '.invalid'", host);

		return result;
	}

	result = net_parse(host);

	if (result.s_addr == INADDR_NONE || result.s_addr == INADDR_ANY)
	{
		hostent* h = gethostbyname(host);

		if (h != nullptr && h->h_addrtype == AF_INET)
		{
			result.s_addr = *(uint32_t*)h->h_addr_list[0];
		}
		else
		{
			LOG_NET_ERROR("net-mon", "get_host_by_name(%s) failed : gethostbyname() failed with error: %d", host, std_get_error);
		}
	}

	return result;
}
//--------------------------------------
//
//--------------------------------------
RTX_NET_API int net_t::get_host_by_name(const char* host, rtl::ArrayT<in_addr>& ip_table)
{
	if (host == nullptr || host[0] == 0)
	{
		//LOG_NET_ERROR(L"net-mon", L"get_host_by_name(%s, table) failed : bad parameter : empty string", host);
		return 0;
	}


	if (strcmp(host, "(null)") == 0)
	{
		//LOG_NET_ERROR(L"net-mon", L"get_host_by_name(%s, table) failed : bad parameter : (null)", host);

		return 0;
	}

	if (strstr(host, ".invalid") != nullptr)
	{
		//LOG_NET_ERROR(L"net-mon", L"get_host_by_name(%s, table) failed : bad parameter : host is '.invalid'", host);

		return 0;
	}

	ip_table.clear();

	hostent* h = gethostbyname(host);
	in_addr addr;

	if (h != nullptr)
	{
		if (h->h_addrtype == AF_INET)
		{
			int i = 0;
			while (h->h_addr_list[i] != nullptr)
			{
				addr.s_addr = *(uint32_t*)h->h_addr_list[i++];
				ip_table.add(addr);
			}
		}
	}
	else
	{
		LOG_NET_ERROR("net-mon", "get_host_by_name(%s) failed : gethostbyname() failed with error: %d", host, std_get_error);
	}

	return ip_table.getCount();
}
//--------------------------------------
// функция считывает создает/обновляет таблицу интерфейсов!
//--------------------------------------
RTX_NET_API int net_t::get_interface_count()
{
#if defined (TARGET_OS_WINDOWS) 
	int iface_count = 0;

	bool res = false;
	MIB_IPADDRTABLE* info;
	unsigned long outBufLen;
	uint32_t retVal;
	int tableIndex = -1;

	info = (MIB_IPADDRTABLE*)MALLOC(sizeof MIB_IPADDRTABLE);
	outBufLen = sizeof(MIB_IPADDRTABLE);

	if (GetIpAddrTable(info, &outBufLen, 0) == ERROR_INSUFFICIENT_BUFFER)
	{
		FREE(info);
		info = (MIB_IPADDRTABLE*)MALLOC(outBufLen);
	}

	if ((retVal = GetIpAddrTable(info, &outBufLen, 0)) != NO_ERROR)
	{
		LOG_NET_ERROR("net-mon", "GetInterfaceInfo failed with error: %d", retVal);
		FREE(info);
		return 0;
	}

	rtl::MutexLock lock(g_net_sync);

	g_net_addr_table.clear();

	for (int i = 0; i < (int)info->dwNumEntries; i++)
	{
		in_addr addr;
		addr.s_addr = info->table[i].dwAddr;
		g_net_addr_table.add(addr);
	}

	iface_count = g_net_addr_table.getCount();

	FREE(info);

	return iface_count;
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	struct ifaddrs *ifaddr, *ifa;
	//int family, s, n;
	//char host[NI_MAXHOST];

	if (getifaddrs(&ifaddr) == -1)
	{
		LOG_NET_ERROR("net-mon", "getifaddrs failed with error: %d", errno);
		return 0;
	}

	/* Walk through linked list, maintaining head pointer so we
	can free list later */
	int n;
	for (ifa = ifaddr, n = 0; ifa != NULL; ifa = ifa->ifa_next, n++)
	{
		if (ifa->ifa_addr == NULL)
			continue;

		if (ifa->ifa_addr->sa_family == AF_INET)
		{
			sockaddr_in* addr_in = (sockaddr_in*)ifa->ifa_addr;
			g_net_addr_table.add(addr_in->sin_addr);
		}
	}
	
	freeifaddrs(ifaddr);

	return 0;
#endif
}
//--------------------------------------
// функция НЕ не обновляет таблицу интерфейсов!
//--------------------------------------
RTX_NET_API bool net_t::get_interface_at(int index, in_addr& iface)
{
	rtl::MutexLock lock(g_net_sync);

	if (index >= 0 && index < g_net_addr_table.getCount())
	{
		iface = g_net_addr_table[index];
		return true;
	}

	return false;
}
//--------------------------------------
//
//--------------------------------------
RTX_NET_API bool net_t::get_interface_table(rtl::ArrayT<in_addr>& iface_table)
{
	iface_table.clear();

#if defined (TARGET_OS_WINDOWS)
	MIB_IPADDRTABLE* info;
	unsigned long outBufLen;
	uint32_t retVal;
	int tableIndex = -1;

	info = (MIB_IPADDRTABLE*)MALLOC(sizeof MIB_IPADDRTABLE);
	outBufLen = sizeof(MIB_IPADDRTABLE);

	if (GetIpAddrTable(info, &outBufLen, 0) == ERROR_INSUFFICIENT_BUFFER)
	{
		FREE(info);
		info = (MIB_IPADDRTABLE*)MALLOC(outBufLen);
	}

	if ((retVal = GetIpAddrTable(info, &outBufLen, 0)) != NO_ERROR)
	{
		LOG_NET_ERROR("net-mon", "GetInterfaceInfo failed with error: %d", retVal);
		FREE(info);
		return false;
	}

	//LOG_NET("NET-MAN", "iftable size:%d", info->dwNumEntries);

	for (int i = 0; i < (int)info->dwNumEntries; i++)
	{
		in_addr addr;
		addr.s_addr = info->table[i].dwAddr;
		if (addr.s_addr != INADDR_NONE && addr.s_addr != INADDR_ANY)
		{
			//LOG_NET("NET-MAN", "adding if:%s", inet_ntoa(addr));
			iface_table.add(addr);
		}
	}

	FREE(info);
#else
	struct ifaddrs *ifaddr, *ifa;
	//int family, s, n;
	//char host[NI_MAXHOST];

	if (getifaddrs(&ifaddr) == -1)
	{
		LOG_NET_ERROR("net-mon", "getifaddrs failed with error: %d", errno);
		return false;
	}

	/* Walk through linked list, maintaining head pointer so we
	can free list later */
	int n;
	for (ifa = ifaddr, n = 0; ifa != NULL; ifa = ifa->ifa_next, n++)
	{
		if (ifa->ifa_addr == NULL)
			continue;

		if (ifa->ifa_addr->sa_family == AF_INET)
		{
			sockaddr_in* addr_in = (sockaddr_in*)ifa->ifa_addr;
			iface_table.add(addr_in->sin_addr);
		}
	}

	freeifaddrs(ifaddr);
#endif
	return iface_table.getCount() > 0;
}
//--------------------------------------
//
//--------------------------------------
//RTX_NET_API bool net_t::get_gateway(in_addr& gateway)
//{
//	/* Declare and initialize variables */
//
//	// It is possible for an adapter to have multiple
//	// IPv4 addresses, gateways, and secondary WINS servers
//	// assigned to the adapter.
//	//
//	// Note that this sample code only prints out the
//	// first entry for the IP address/mask, and gateway, and
//	// the primary and secondary WINS server for each adapter.
//
//    PIP_ADAPTER_INFO adapterInfo;
//    PIP_ADAPTER_INFO adapter = nullptr;
//    uint32_t retVal;
//	gateway.s_addr = INADDR_NONE;
//
//    unsigned long outBufLen = sizeof (IP_ADAPTER_INFO);
//    adapterInfo = (IP_ADAPTER_INFO *)MALLOC(sizeof (IP_ADAPTER_INFO));
//    if (adapterInfo == nullptr)
//	{
//        LOG_NET_ERROR("net-mon", "Error allocating memory needed to call GetAdaptersinfo");
//        return FALSE;
//    }
//
//	// Make an initial call to GetAdaptersInfo to get
//	// the necessary size into the ulOutBufLen variable
//    if (GetAdaptersInfo(adapterInfo, &outBufLen) == ERROR_BUFFER_OVERFLOW)
//	{
//        FREE(adapterInfo);
//        adapterInfo = (IP_ADAPTER_INFO *) MALLOC(outBufLen);
//        if (adapterInfo == nullptr)
//		{
//            LOG_NET_ERROR("net-mon", "Error allocating memory needed to call GetAdaptersinfo");
//            return FALSE;
//        }
//    }
//
//    if ((retVal = GetAdaptersInfo(adapterInfo, &outBufLen)) == NO_ERROR)
//	{
//        adapter = adapterInfo;
//        while (adapter)
//		{
//			gateway = net_parse(adapter->GatewayList.IpAddress.String);
//            adapter = adapter->Next;
//			if (gateway.s_addr != INADDR_NONE && gateway.s_addr != INADDR_ANY)
//				break;
//        }
//    }
//	else
//	{
//		LOG_NET_ERROR("net-mon", "GetAdaptersInfo return error %u", retVal);
//	}
//
//    FREE(adapterInfo);
//
//    return gateway.s_addr != INADDR_NONE && gateway.s_addr != INADDR_ANY;
//}
//--------------------------------------
//
//--------------------------------------
//RTX_NET_API bool net_t::get_default_interface(in_addr& iface)
//{
//	in_addr gateway;
//
//	if (!get_gateway(gateway))
//	{
//		LOG_NET_WARNING("net-mon", "Unable to retrieve gateway address");
//
//		for (int i = 0; i < g_net_addr_table.getCount(); i++)
//		{
//			in_addr addr = g_net_addr_table[i];
//
//			if (ip_is_valid(addr) && !ip_is_loopback(addr))
//			{
//				iface = addr;
//				uint8_t* b = (uint8_t*)&addr;
//				LOG_NET("net-mon", "Default address is %d.%d.%d.%d", b[0], b[1], b[2], b[3]);
//				return true;
//			}
//		}
//
//		return false;
//	}
//
//	return get_best_interface(gateway, iface);
//}
//--------------------------------------
// проверяем в списке адресов локального компа
//--------------------------------------
RTX_NET_API bool net_t::ip_is_localhost(in_addr addr)
{
	if (addr.s_addr == INADDR_ANY)
		return true;

	// lookup the host address using inet_addr, assuming it is a "." address
	if (addr.s_addr == htonl(INADDR_LOOPBACK))
		return true;

	char tmp[256];

	::gethostname(tmp, 256);
	tmp[255] = 0;

	struct hostent* host_info = ::gethostbyname(tmp);

	if (host_info == nullptr)
		return false;

	for (int i = 0; host_info->h_addr_list[i] != nullptr; i++)
	{
		if (addr.s_addr == *(u_long*)host_info->h_addr_list[i])
			return true;
	}

	return false;
}
//--------------------------------------
//
//--------------------------------------
in_addr net_t::select_address_by_mask(const rtl::ArrayT<in_addr>& list, in_addr mask)
{
	// ищем по локальным подсетям
	mask.s_addr &= 0xFFFFFF00;

	for (int i = 0; i < list.getCount(); i++)
	{
		in_addr candidate = list[i];
		candidate.s_addr = 0xFFFFFF00;
		if (candidate.s_addr == mask.s_addr)
		{
			// нашли!
			return list[i];
		}
	}

	// если не нашли то скорее всего ситуация такая: две точки на одной машине с несколькими интерфейсами!
	for (int i = 0; i < list.getCount(); i++)
	{
		in_addr candidate = list[i];
		if (net_t::ip_is_localhost(candidate))
		{
			// нашли первый попавшийся!
			return candidate;
		}
	}

	// нет подходящего
	in_addr addr_none;
	addr_none.s_addr = INADDR_NONE;
	return addr_none;
}
//--------------------------------------
//
//--------------------------------------
//bool net_t::add_transport_monitor(net_monitor_t* monitor)
//{
//	if (monitor == nullptr)
//	{
//		return false;
//	}
//
//	synchro_t<rtl::Mutex> lock(g_net_sync);
//
//	for (int i = 0; i < g_net_monitor_list.getCount(); i++)
//	{
//		if (g_net_monitor_list[i] == monitor)
//			return true;
//	}
//
//	g_net_monitor_list.add(monitor);
//
//	return true;
//}
//--------------------------------------
//
//--------------------------------------
//bool net_t::remove_transport_monitor(net_monitor_t* monitor)
//{
//	if (monitor == nullptr)
//	{
//		return false;
//	}
//
//	synchro_t<rtl::Mutex> lock(g_net_sync);
//
//	for (int i = 0; i < g_net_monitor_list.getCount(); i++)
//	{
//		if (g_net_monitor_list[i] == monitor)
//		{
//			g_net_monitor_list.removeAt(i);
//			return true;
//		}
//	}
//
//	return false;
//}
//--------------------------------------
//
//--------------------------------------
//net_monitor_thread_t::net_monitor_thread_t() : m_net_notify_handle(nullptr), m_stop_flag(true)
//{
//	memset(&m_net_event, 0, sizeof OVERLAPPED);
//}
////--------------------------------------
////
////--------------------------------------
//net_monitor_thread_t::~net_monitor_thread_t()
//{
//	stop();
//}
////--------------------------------------
////
////--------------------------------------
//void net_monitor_thread_t::start()
//{
//	memset(&m_net_event, 0, sizeof m_net_event);
//
//	LOG_NET("net-mon", "net monitor start...");
//
//	m_thread.start(this);
//
//	LOG_NET("net-mon", "net monitor started");
//}
////--------------------------------------
////
////--------------------------------------
//void net_monitor_thread_t::stop()
//{
//	if (m_net_event.hEvent != WSA_INVALID_EVENT)
//	{
//		LOG_NET("net-mon", "net monitor stopping...");
//
//		m_stop_flag = true;
//
//		if (!CancelIPChangeNotify(&m_net_event))
//		{
//			LOG_NET_ERROR("net-mon", "net monitor stop failed"); // std_get_error,
//		}
//
//		//WSASetEvent(m_net_event.hEvent);
//
//		m_thread.wait();
//
//		WSACloseEvent(m_net_event.hEvent);
//
//		m_net_event.hEvent = WSA_INVALID_EVENT;
//		m_net_notify_handle = nullptr;
//
//		LOG_NET("net-mon", "net monitor stopped");
//	}
//}
////--------------------------------------
////
////--------------------------------------
//void net_monitor_thread_t::thread_run(rtl::Thread* thread)
//{
//	m_stop_flag = false;
//
//	m_net_event.hEvent = WSACreateEvent();
//
//	if (m_net_event.hEvent == WSA_INVALID_EVENT)
//	{
//		LOG_NET_ERROR("net-mon", "net monitor start failed"); // , std_get_error
//		return;
//	}
//
//	do
//	{
//		if (NotifyAddrChange(&m_net_notify_handle, &m_net_event) != ERROR_IO_PENDING)
//		{
//			LOG_NET_ERROR("net-mon", "net monitor start failed"); // , std_get_error
//			break;
//		}
//
//		LOG_NET("net-mon", "net monitor run : wait event...");
//		unsigned long result = WaitForSingleObject(m_net_event.hEvent, INFINITE);
//
//		if (m_stop_flag)
//			break;
//
//		if (result == WAIT_OBJECT_0)
//		{
//			event_raised();
//		}
//		else if (result == WAIT_FAILED)
//		{
//			LOG_NET_ERROR("net-mon", "net monitor run : wait failed"); // , std_get_error
//			break;
//		}
//	}
//	while (!m_stop_flag);
//
//	LOG_NET("net-mon", "net monitor exit");
//}
////--------------------------------------
////
////--------------------------------------
//void net_monitor_thread_t::event_raised()
//{
//	rtl::ArrayT<in_addr> iftable, changes_list;
//
//	LOG_NET("net-mon", "net monitor run : call monitors...");
//
//	synchro_t<rtl::Mutex> lock(g_net_sync);
//
//	// таблица отсутствует - упала вся сеть
//	LOG_NET("net-mon", "net monitor event : check table...");
//	if (!net_t::get_interface_table(iftable) || iftable.getCount() == 0)
//	{
//		LOG_NET("net-mon", "net monitor event : if table is empty!");
//
//		g_net_addr_table.clear();
//		raise_network_failed();
//
//		return;
//	}
//
//	char tmp[512] = {0};
//	int written = 0;
//
//	int if_count = g_net_addr_table.getCount();
//	for (int ii = 0; ii < if_count; ii++)
//	{
//		written += _snprintf(tmp + written, 512 - written, "    if entry[%d] : %s\r\n", ii, inet_ntoa(g_net_addr_table[ii]));
//	}
//
//	LOG_NET("net-mon", "net monitor event : existing table :\r\n%s", tmp);
//
//	written = 0;
//	tmp[0] = 0;
//	if_count = iftable.getCount();
//
//	for (int ii = 0; ii < if_count; ii++)
//	{
//		written += _snprintf(tmp + written, 512 - written, "    if entry[%d] : %s\r\n", ii, inet_ntoa(iftable[ii]));
//	}
//
//	LOG_NET("net-mon", "net monitor event : new table :\r\n%s", tmp);
//
//	// в таблице чтото измениенилось -- сравним
//	// сначала найдем удаленные
//	LOG_NET("net-mon", "net monitor event : check removed...");
//	for (int i = g_net_addr_table.getCount() - 1; i >= 0; i--)
//	{
//		bool found = false;
//		in_addr iface = g_net_addr_table[i];
//
//		for (int j = 0; j < iftable.getCount(); j++)
//		{
//			char laddr[64], raddr[64];
//			strcpy(laddr, inet_ntoa(iface));
//			strcpy(raddr, inet_ntoa(iftable[j]));
//
//			LOG_NET("net-mon", "net monitor event : compare %s && %s", laddr, raddr);
//
//			if (iface.s_addr == iftable[j].s_addr)
//			{
//				LOG_NET("net-mon", "net monitor event : found!");
//				found = true;
//				break;
//			}
//		}
//
//		if (!found)
//		{
//			LOG_NET("net-mon", "net monitor event : removing iface %s!", inet_ntoa(iface));
//			g_net_addr_table.removeAt(i);
//			changes_list.add(iface);
//		}
//	}
//
//	if (changes_list.getCount() > 0)
//	{
//		raise_addresses_removed(changes_list);
//	}
//
//	changes_list.clear();
//
//	//теперь добавим новые
//	LOG_NET("net-mon", "net monitor event : check new...");
//	for (int i = 0; i < iftable.getCount(); i++)
//	{
//		bool found = false;
//		in_addr iface = iftable[i];
//
//		for (int j = 0; j < g_net_addr_table.getCount(); j++)
//		{
//			char laddr[64], raddr[64];
//			strcpy(laddr, inet_ntoa(iface));
//			strcpy(raddr, inet_ntoa(g_net_addr_table[j]));
//
//			LOG_NET("net-mon", "net monitor event : compare %s && %s", laddr, raddr);
//
//			if (iface.s_addr == g_net_addr_table[j].s_addr)
//			{
//				LOG_NET("net-mon", "net monitor event : found!");
//				found = true;
//				break;
//			}
//		}
//
//		if (!found)
//		{
//			LOG_NET("net-mon", "net monitor event : adding iface %s!", inet_ntoa(iface));
//			g_net_addr_table.add(iface);
//			changes_list.add(iface);
//		}
//	}
//
//	if (changes_list.getCount() > 0)
//	{
//		raise_addresses_added(changes_list);
//	}
//
//	LOG_NET("net-mon", "net monitor event : hanled");
//}
////--------------------------------------
////
////--------------------------------------
//void net_monitor_thread_t::raise_network_failed()
//{
//	LOG_NET("net-mon", "net monitor event : network is not avaliable");
//
//	for (int i = 0; i < g_net_monitor_list.getCount(); i++)
//	{
//		g_net_monitor_list[i]->net_transport_network_failed();
//	}
//}
////--------------------------------------
////
////--------------------------------------
//void net_monitor_thread_t::raise_addresses_added(const rtl::ArrayT<in_addr>& list)
//{
//	LOG_NET("net-mon", "net monitor event : new interface attached");
//
//	for (int k = 0; k < g_net_monitor_list.getCount(); k++)
//	{
//		g_net_monitor_list[k]->net_transport_addresses_added(list);
//	}
//}
////--------------------------------------
////
////--------------------------------------
//void net_monitor_thread_t::raise_addresses_removed(const rtl::ArrayT<in_addr>& list)
//{
//	LOG_NET("net-mon", "net monitor event : some interface detached");
//
//	for (int k = 0; k < g_net_monitor_list.getCount(); k++)
//	{
//		g_net_monitor_list[k]->net_transport_addresses_removed(list);
//	}
//}
//--------------------------------------
