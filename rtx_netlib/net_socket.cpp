﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "net/net_socket.h"
#include "std_thread.h"

#if defined(TARGET_OS_WINDOWS)

    #include <WinSock2.h>
	#include <ws2ipdef.h>

	struct wsa_initializer
	{
		wsa_initializer()
		{
			WSADATA WsaData;
			WSAStartup(0x0202, &WsaData);
		}
	} g_wsa_initializer;

#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
    #include <sys/types.h>
    #include <sys/socket.h>
	#include <unistd.h>
	#include <sys/ioctl.h>
#if defined(TARGET_OS_LINUX)
#include <sys/epoll.h>
#elif defined(TARGET_OS_FREEBSD)
#include <sys/event.h>
#endif
    #include <netinet/in.h>
    #include <errno.h>

    #if !defined(INVALID_SOCKET)
        #define INVALID_SOCKET (-1)
    #endif

    #if !defined(SOCKET_ERROR)
        #define SOCKET_ERROR (-1)
    #endif

    #if !defined(SOMAXCONN)
        #define SOMAXCONN       0x7fffffff
    #endif

    #if !defined(closesocket)
        #define closesocket ::close
    #endif

	#if !defined(ioctlsocket)
		#define ioctlsocket	ioctl
	#endif

#endif

#define DEFAULT_BACKLOG SOMAXCONN

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
i_socket_event_handler::i_socket_event_handler()
{
}
i_socket_event_handler::~i_socket_event_handler()
{
}
//------------------------------------------------------------------------------
// net_socket Constructor
//------------------------------------------------------------------------------
						net_socket::net_socket(rtl::Logger* log)
{
	m_log = log;
	m_socket = INVALID_SOCKET;
	m_backlog = DEFAULT_BACKLOG;

	m_event_handler = NULL;
    m_user_tag = NULL;

#if defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)

    m_iosvctag_user = NULL;
    m_iosvctag_netioctx  = NULL;

#endif
}
//------------------------------------------------------------------------------
// net_socket Destructor
//------------------------------------------------------------------------------
						net_socket::~net_socket()
{
	close();
}
//------------------------------------------------------------------------------
// Get native handle
//------------------------------------------------------------------------------
SOCKET					net_socket::get_native_handle() const
{
	return m_socket;
}
//------------------------------------------------------------------------------
// Get local address
//------------------------------------------------------------------------------
const socket_address*	net_socket::get_local_address() const
{
	return &m_local_address;
}
const socket_address*	net_socket::get_remote_address() const
{
	return &m_remote_address;
}
//------------------------------------------------------------------------------
// Get back log
//------------------------------------------------------------------------------
int						net_socket::get_back_log() const
{
	return m_backlog;
}
void					net_socket::set_back_log(int value)
{
	m_backlog = value;
}
//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------
bool					net_socket::create(e_ip_address_family family, e_ip_protocol_type protocol)
{
	int af = 0;
	int type = 0;
	int proto = 0;

	if (family == E_IP_ADDRESS_FAMILY_IPV4)
	{
		af = PF_INET;
	}
	else if (family == E_IP_ADDRESS_FAMILY_IPV6)
	{
		af = PF_INET6;
	}

	if (protocol == e_ip_protocol_type::E_IP_PROTOCOL_TCP)
	{
		type = SOCK_STREAM;
		proto = IPPROTO_TCP;
	}
	else if (protocol == e_ip_protocol_type::E_IP_PROTOCOL_UDP)
	{
		type = SOCK_DGRAM;
		proto = IPPROTO_UDP;
	}
	else if (protocol == e_ip_protocol_type::E_IP_PROTOCOL_RAW_UDP)
	{
        type = SOCK_RAW;

#if defined(TARGET_OS_WINDOWS)
        proto = IPPROTO_IP;
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
        proto = IPPROTO_UDP;
#endif
	}

#if defined(TARGET_OS_WINDOWS)
	m_socket = WSASocket(af, type, proto, NULL, NULL, WSA_FLAG_OVERLAPPED);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	m_socket = ::socket(af, type, proto);
#endif

	if (m_socket == INVALID_SOCKET)
	{
#if defined(TARGET_OS_WINDOWS)
		int err = WSAGetLastError();
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		int err = errno;
#endif

		PLOG_NET_ERROR("net-sock", "net_socket.create -- socket() failed. error = %d.", err);

		return false;
	}

	return true;
}
//------------------------------------------------------------------------------
// Close
//------------------------------------------------------------------------------
void					net_socket::close()
{
	if (m_socket != INVALID_SOCKET)
	{
		int res = ::closesocket(m_socket);

		if (res != 0)
		{
#if defined(TARGET_OS_WINDOWS)
			int err = WSAGetLastError();
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
			int err = errno;
#endif
			PLOG_NET_ERROR("net-sock", "net_socket.close -- closesocket() failed. error = %d", err);
		}

		m_socket = INVALID_SOCKET;
	}
}
//------------------------------------------------------------------------------
// Shutdown
//------------------------------------------------------------------------------
void					net_socket::shutdown(e_socket_shutdown_type shutdown_type)
{
	if (m_socket != INVALID_SOCKET)
	{
		int how = 0;

#if defined(TARGET_OS_WINDOWS)

		switch (shutdown_type)
		{
		case e_socket_shutdown_type::E_SOCKET_SHUTDOWN_TYPE_RECEIVE:
			how = SD_RECEIVE;
			break;
		case e_socket_shutdown_type::E_SOCKET_SHUTDOWN_TYPE_SEND:
			how = SD_SEND;
			break;
		case e_socket_shutdown_type::E_SOCKET_SHUTDOWN_TYPE_BOTH:
			how = SD_BOTH;
			break;
		}

#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)

        switch (shutdown_type)
		{
		case e_socket_shutdown_type::E_SOCKET_SHUTDOWN_TYPE_RECEIVE:
			how = SHUT_RD;
			break;
		case e_socket_shutdown_type::E_SOCKET_SHUTDOWN_TYPE_SEND:
			how = SHUT_WR;
			break;
		case e_socket_shutdown_type::E_SOCKET_SHUTDOWN_TYPE_BOTH:
			how = SHUT_RDWR;
			break;
		}

#endif

		int res = ::shutdown(m_socket, how);

		if (res != 0)
		{
#if defined(TARGET_OS_WINDOWS)
			int err = WSAGetLastError();
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
			int err = errno;
#endif
			PLOG_NET_ERROR("net-sock", "net_socket.shutdown -- shutdown() failed. error = %d", err);
		}
	}
}
//------------------------------------------------------------------------------
// Bind
//------------------------------------------------------------------------------
bool					net_socket::bind(const socket_address* local_addr)
{
	if (local_addr == NULL)
	{
		PLOG_NET_ERROR("net-sock", "net_socket.bind -- invalid address");
		return false;
	}

	if (m_socket == INVALID_SOCKET)
	{
		PLOG_NET_ERROR("net-sock", "net_socket.bind -- socket isn't created");
		return false;
	}

	socklen_t slen = socklen_t(local_addr->get_native_len());

	int result = ::bind(m_socket, (const sockaddr*) local_addr->get_native(), slen);
	if (result != 0)
	{
#if defined(TARGET_OS_WINDOWS)
		int err = WSAGetLastError();
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		int err = errno;
#endif
		PLOG_NET_ERROR("net-sock", "net_socket.bind -- bind() failed. error = %d", err);
		return false;
	}

	m_local_address.copy_from(local_addr);

	if (local_addr->get_port() == 0)
	{
		if (local_addr->get_ip_address()->get_family() == e_ip_address_family::E_IP_ADDRESS_FAMILY_IPV4)
		{
			sockaddr_in saddr;
			socklen_t len = sizeof(saddr);
			int result = getsockname(m_socket, (sockaddr*)&saddr, &len);
			if (result == 0)
			{
				m_local_address.set_native(&saddr, len);
			}
		}
		else
		{
			sockaddr_in6 saddr6;
			socklen_t len = sizeof(saddr6);
			int result = getsockname(m_socket, (sockaddr*)&saddr6, &len);
			if (result == 0)
			{
				m_local_address.set_native(&saddr6, len);
			}
		}
	}

	return true;
}
//------------------------------------------------------------------------------
// Listen
//------------------------------------------------------------------------------
/*bool					net_socket::listen()
{
	if (m_socket == INVALID_SOCKET)
	{
		PLOG_NET_ERROR("net-sock", "net_socket.listen -- socket isn't created");
		return false;
	}

	int result = ::listen(m_socket, m_backlog);
	if (result != 0)
	{
#if defined(TARGET_OS_WINDOWS)
		int err = WSAGetLastError();
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		int err = errno;
#endif
		PLOG_NET_ERROR("net-sock", "net_socket.listen -- listen() failed. error = %d", err);
		return false;
	}

	return true;
}*/
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
/*bool					net_socket::connect(const socket_address* remote_addr)
{
	if (remote_addr == NULL)
	{
		PLOG_NET_ERROR("net-sock", "net_socket.connect -- invalid address");
		return false;
	}

	if (m_socket == INVALID_SOCKET)
	{
		PLOG_NET_ERROR("net-sock", "net_socket.connect -- socket isn't created");
		return false;
	}

	socklen_t slen = socklen_t(remote_addr->get_native_len());

	int res = ::connect(m_socket, (const sockaddr*) remote_addr->get_native(), slen);
	if (res != 0)
	{
#if defined(TARGET_OS_WINDOWS)
		int err = WSAGetLastError();
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		int err = errno;
#endif
		PLOG_NET_ERROR("net-sock", "net_socket.connect -- connect() failed. error = %d", err);
		return false;
	}

	m_remote_address.copy_from(remote_addr);

	uint8_t local[socket_address::MAX_NATIVE_SOCKADDR_SIZE];
	socklen_t locallen = sizeof(local);

	res = getsockname(m_socket, (sockaddr*)local, &locallen);
	if (res != 0)
	{
#if defined(TARGET_OS_WINDOWS)
		int err = WSAGetLastError();
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		int err = errno;
#endif
		PLOG_NET_ERROR("net-sock", "net_socket.connect -- getsockname() failed. error = %d", err);
		return false;
	}

	m_local_address.set_native(local, uint32_t(locallen));

	return true;
}*/
//------------------------------------------------------------------------------
// Accept
//------------------------------------------------------------------------------
net_socket*				net_socket::accept(rtl::Logger* log)
{
	if (m_socket == INVALID_SOCKET)
	{
		PLOG_NET_ERROR("net-sock", "net_socket.accept -- socket isn't created");
		return NULL;
	}

	//--------------------------------------------------------------------------
	// акцептим, обновляем удаленный адрес
	//--------------------------------------------------------------------------

	uint8_t remote[socket_address::MAX_NATIVE_SOCKADDR_SIZE];
	socklen_t remotelen = sizeof(remote);

	SOCKET s = ::accept(m_socket, (sockaddr*) remote, &remotelen);

#if defined(TARGET_OS_WINDOWS)
	if (s == INVALID_SOCKET)
	{
		int err = WSAGetLastError();
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	if (s < 0)
	{
		int err = errno;
#endif
		PLOG_NET_ERROR("net-sock", "net_socket.accept -- accept() failed. error = %d", err);
		return NULL;
	}

	//--------------------------------------------------------------------------
	// обновляем локальный адрес
	//--------------------------------------------------------------------------

	uint8_t local[socket_address::MAX_NATIVE_SOCKADDR_SIZE];
	socklen_t locallen = sizeof(local);

	int res = getsockname(s, (sockaddr*)local, &locallen);
	if (res != 0)
	{
#if defined(TARGET_OS_WINDOWS)
		int err = WSAGetLastError();
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		int err = errno;
#endif
		PLOG_NET_ERROR("net-sock", "net_socket.accept -- getsockname() failed. error = %d", err);
		return NULL;
	}

	net_socket* newsock = NEW net_socket(log);
	newsock->m_socket = s;
	newsock->m_local_address.set_native(local, uint32_t(locallen));
	newsock->m_remote_address.set_native(remote, uint32_t(remotelen));

	return newsock;
}
//------------------------------------------------------------------------------
// Send (tcp)
//------------------------------------------------------------------------------
uint32_t				net_socket::send(const void* data, uint32_t len) const
{
	if (data == NULL)
	{
		PLOG_NET_ERROR("net-sock", "net_socket.send -- invalid data");
		return 0;
	}

	if (len == 0)
	{
		PLOG_NET_ERROR("net-sock", "net_socket.send -- invalid length");
		return 0;
	}

	if (m_socket == INVALID_SOCKET)
	{
		PLOG_NET_ERROR("net-sock", "net_socket.send -- socket isn't created");
		return 0;
	}

#if defined(TARGET_OS_WINDOWS)
	int res = ::send(m_socket, (const char*) data, int(len), 0);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	int res = ::send(m_socket, (const char*) data, size_t(len), 0);
#endif

	if (res < 0)
	{
#if defined(TARGET_OS_WINDOWS)
		int err = WSAGetLastError();
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		int err = errno;
#endif
		PLOG_NET_ERROR("net-sock", "net_socket.send -- send() failed. error = %d", err);
		return 0;
	}

	return uint32_t(res);
}
uint32_t				net_socket::recv(void* buffer, uint32_t size)
{
	if (buffer == NULL)
	{
		PLOG_NET_ERROR("net-sock", "net_socket.recv -- invalid buffer");
		return 0;
	}

	if (size == 0)
	{
		PLOG_NET_ERROR("net-sock", "net_socket.recv -- invalid size");
		return 0;
	}

	if (m_socket == INVALID_SOCKET)
	{
		PLOG_NET_ERROR("net-sock", "net_socket.recv -- socket isn't created");
		return 0;
	}

#if defined(TARGET_OS_WINDOWS)
	int res = ::recv(m_socket, (char*) buffer, int(size), 0);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	int res = ::recv(m_socket, (char*) buffer, size_t(size), 0);
#endif

	if (res < 0)
	{
#if defined(TARGET_OS_WINDOWS)
		int err = WSAGetLastError();
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		int err = errno;
#endif
		PLOG_NET_ERROR("net-sock", "net_socket.recv -- recv() failed. error = %d", err);
		return 0;
	}

	return uint32_t(res);
}
//------------------------------------------------------------------------------
// SendTo (udp)
//------------------------------------------------------------------------------
uint32_t				net_socket::send_to(const void* data, uint32_t len, const socket_address* remote_addr)  const
{
	if (data == NULL)
	{
		PLOG_NET_ERROR("net-sock", "net_socket.send_to -- invalid data");
		return 0;
	}

	if (len == 0)
	{
		PLOG_NET_ERROR("net-sock", "net_socket.send_to -- invalid length");
		return 0;
	}

	if (remote_addr == NULL)
	{
		PLOG_NET_ERROR("net-sock", "net_socket.send_to -- invalid address");
		return 0;
	}

	if (m_socket == INVALID_SOCKET)
	{
		PLOG_NET_ERROR("net-sock", "net_socket.send_to -- socket isn't created");
		return 0;
	}

	const sockaddr* to = (const sockaddr*) remote_addr->get_native();
	socklen_t tolen = int(remote_addr->get_native_len());

#if defined(TARGET_OS_WINDOWS)
	int	res = ::sendto(m_socket, (const char*) data, int(len), 0, to, tolen);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	int	res = ::sendto(m_socket, (const char*) data, size_t(len), 0, to, tolen);
#endif

	if (res < 0)
	{
#if defined(TARGET_OS_WINDOWS)
		int err = WSAGetLastError();
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		int err = errno;
#endif
		PLOG_NET_ERROR("net-sock", "net_socket.send_to -- sendto() failed. error = %d", err);
		return 0;
	}

	return uint32_t(res);
}
uint32_t				net_socket::recv_from(void* buffer, uint32_t size, socket_address* remote_addr)
{
	if (buffer == NULL)
	{
		PLOG_NET_ERROR("net-sock", "net_socket.recv_from -- invalid buffer");
		return 0;
	}

	if (size == 0)
	{
		PLOG_NET_ERROR("net-sock", "net_socket.recv_from -- invalid size");
		return 0;
	}

	if (remote_addr == NULL)
	{
		PLOG_NET_ERROR("net-sock", "net_socket.recv_from -- invalid address");
		return 0;
	}

	if (m_socket == INVALID_SOCKET)
	{
		PLOG_NET_ERROR("net-sock", "net_socket.recv_from -- socket isn't created");
		return 0;
	}

	uint8_t from[socket_address::MAX_NATIVE_SOCKADDR_SIZE];
	socklen_t fromlen = sizeof(from);

#if defined(TARGET_OS_WINDOWS)
	int res = ::recvfrom(m_socket, (char*) buffer, int(size), 0, (sockaddr*)from, &fromlen);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	int res = ::recvfrom(m_socket, (char*) buffer, size_t(size), 0, (sockaddr*)from, &fromlen);
#endif

	if (res < 0)
	{
#if defined(TARGET_OS_WINDOWS)
		int err = WSAGetLastError();
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		int err = errno;
#endif
		PLOG_NET_ERROR("net-sock", "net_socket.recv_from -- recvfrom() failed. error = %d", err);
		return 0;
	}

	remote_addr->set_native(from, uint32_t(fromlen));

	return uint32_t(res);
}
//------------------------------------------------------------------------------
// Get available bytes count
//------------------------------------------------------------------------------
uint32_t				net_socket::get_available_bytes_count()
{
	uint32_t size = 0;

	int res = ::ioctlsocket(m_socket, FIONREAD, (u_long*)&size);
	if (res != 0)
	{
#if defined(TARGET_OS_WINDOWS)
		int err = WSAGetLastError();
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		int err = errno;
#endif
		PLOG_NET_ERROR("net-sock", "net_socket.get_available_bytes_count -- ioctlsocket() failed. error = %d", err);
		return 0;
	}

	return size;
}
//------------------------------------------------------------------------------
// Select
//------------------------------------------------------------------------------
bool					net_socket::select(socket_vector_t* read, socket_vector_t* write, socket_vector_t* error, uint32_t timeout, rtl::Logger* log)
{
#if defined(TARGET_OS_WINDOWS)
	return select_win32(read, write, error, timeout, log);
#elif defined(TARGET_OS_LINUX)
	return epoll_linux(read, write, error, timeout, log);
#elif defined(TARGET_OS_FREEBSD)
	return kqueue_freebsd(read, write, error, timeout, log);
#endif
}
//------------------------------------------------------------------------------
// Указываем что сами будем формировать ip пакет.
//------------------------------------------------------------------------------
bool					net_socket::set_raw()
{
	if ((m_socket == 0) || (m_socket == INVALID_SOCKET))
	{
		return false;
	}

	uint32_t use_own_header = 1;
	uint32_t use_own_header_len = sizeof(use_own_header);

    // говорим о том что мы вручную будем формировать заголовки пакетов

	int per = setsockopt(m_socket, IPPROTO_IP, IP_HDRINCL, (char*)&use_own_header, use_own_header_len);
	if (per == SOCKET_ERROR)
	{
#if defined(TARGET_OS_WINDOWS)
		int err = WSAGetLastError();
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
        int err = errno;
#endif

		PLOG_NET_ERROR("net-sock", "net_socket.set_raw -- setsockopt() failed. error = %d.", err);

		return false;
	}

	return true;
}
//------------------------------------------------------------------------------
// Реализвция Select для разных ОС.
//------------------------------------------------------------------------------
#if defined(TARGET_OS_WINDOWS)
//------------------------------------------------------------------------------
// Реализвция Select для Windows.
//------------------------------------------------------------------------------
bool					net_socket::select_win32(socket_vector_t* read, socket_vector_t* write, socket_vector_t* error, uint32_t timeout, rtl::Logger* m_log)
{
	//---------------------------------------------------------
	// если нет параметров, то отрабатываем просто как sleep(timeout)
	//---------------------------------------------------------

	if ((read == NULL) && (write == NULL) && (error == NULL))
	{
		rtl::Thread::sleep(timeout);
		return true;
	}

	//---------------------------------------------------------
	// убедимся, что нам не передали слшиком много сокетов.
	//---------------------------------------------------------

	if ((read != NULL) && (read->getCount() > MAX_SELECT_COUNT))
		return false;

	if ((write != NULL) && (write->getCount() > MAX_SELECT_COUNT))
		return false;

	if ((error != NULL) && (error->getCount() > MAX_SELECT_COUNT))
		return false;

	//---------------------------------------------------------
	// заполняем fd_set'ы
	//---------------------------------------------------------

	fd_set read_fds;
	fd_set write_fds;
	fd_set error_fds;

	int nfds = 0;

	if (read != NULL)
	{
		int res = fill_fd_set(read, &read_fds);
		if (res > nfds)
			nfds = res;
	}

	if (write != NULL)
	{
		int res = fill_fd_set(write, &write_fds);
		if (res > nfds)
			nfds = res;
	}

	if (error != NULL)
	{
		int res = fill_fd_set(error, &error_fds);
		if (res > nfds)
			nfds = res;
	}

	//---------------------------------------------------------
	// основная функция select()
	//---------------------------------------------------------

	::timeval tv = {0};
	tv.tv_sec = timeout / 1000;
	tv.tv_usec = 1000 * (timeout % 1000);

	if (nfds != 0)
		nfds++;

    int result = ::select(nfds,
		(read != NULL) ? &read_fds : NULL,
		(write != NULL) ? &write_fds : NULL,
		(error != NULL) ? &error_fds : NULL,
		(timeout != 0xFFFFFFFF) ? &tv : NULL);

	if (result < 0)
	{
		int err = WSAGetLastError();
		PLOG_NET_ERROR("net-sock", "net_socket.select_win32 -- select() failed. error = %d", err);

		if (read != NULL)
			read->clear();

		if (write != NULL)
			write->clear();

		if (error != NULL)
			error->clear();

		return false;
    }

	//---------------------------------------------------------
	// готовим выходные списки
	//---------------------------------------------------------

	if (read != NULL)
		fill_vector(&read_fds, read);

	if (write != NULL)
		fill_vector(&write_fds, write);

	if (error != NULL)
		fill_vector(&error_fds, error);

	return true;
}
int						net_socket::fill_fd_set(const socket_vector_t* vector, void* set)
{
	fd_set* fdset = (fd_set*) set;

	FD_ZERO(fdset);

	int max = 0;

	for (int i = 0; i < vector->getCount(); ++i)
	{
		const net_socket* sock = vector->getAt(i);

#if defined(TARGET_OS_WINDOWS) // лишнее условие. вся секция обёрнута в такое же.
#pragma warning(push)
#pragma warning(disable:4127)
#endif

		FD_SET(sock->m_socket, fdset);

#if defined(TARGET_OS_WINDOWS) // лишнее условие. вся секция обёрнута в такое же.
#pragma warning(pop)
#endif

		if (int(sock->m_socket) > max)
		{
			max = int(sock->m_socket);
		}
	}

	return max;
}
void					net_socket::fill_vector(const void* set, socket_vector_t* vector)
{
	fd_set* fdset = (fd_set*) set;

	for (int i = vector->getCount() - 1; i >= 0; i--)
	{
		net_socket* sock = vector->getAt(i);

		if (!FD_ISSET(sock->m_socket, fdset))
			vector->removeAt(i);
	}


	//socket_vector_t output(MAX_SELECT_COUNT);
	////output.reserve(MAX_SELECT_COUNT);

	//for (uint32_t i = 0; i < vector->getCount(); i++)
	//{
	//	net_socket* sock = vector->getAt(i);

	//	if (FD_ISSET(sock->m_socket, fdset))
	//		output.add(sock);
	//}

	//std::swap(output, *vector);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#elif defined(TARGET_OS_LINUX)
//------------------------------------------------------------------------------
// Реализвция Select для Linux.
//------------------------------------------------------------------------------
bool					net_socket::epoll_linux(socket_vector_t* read, socket_vector_t* write, socket_vector_t* error, uint32_t timeout, rtl::Logger* m_log)
{
	//---------------------------------------------------------
	// если нет параметров, то отрабатываем просто как sleep(timeout)
	//---------------------------------------------------------

	if ((read == NULL) && (write == NULL) && (error == NULL))
	{
		rtl::Thread::sleep(timeout);
		return true;
	}

	//---------------------------------------------------------
	// убедимся, что нам не передали слшиком много сокетов.
	//---------------------------------------------------------

	if ((read != NULL) && (read->getCount() > MAX_SELECT_COUNT))
		return false;

	if ((write != NULL) && (write->getCount() > MAX_SELECT_COUNT))
		return false;

	if ((error != NULL) && (error->getCount() > MAX_SELECT_COUNT))
		return false;

	//---------------------------------------------------------
	// заполняем массивчик для создания объекта epoll
	//---------------------------------------------------------

	epoll_event events[3 * MAX_SELECT_COUNT];
    memset(events, 0, sizeof(events));
	uint32_t evcount = 0;

	if ((read != NULL) && read->getCount() > 0)
		evcount = add_events(events, evcount, read, EPOLLIN);

	if ((write != NULL) && write->getCount() > 0)
		evcount = add_events(events, evcount, write, EPOLLOUT);

	if ((error != NULL) && error->getCount() > 0)
		evcount = add_events(events, evcount, error, EPOLLERR);

	//---------------------------------------------------------
	// создаем объект epoll
	//---------------------------------------------------------

	int epollfd = epoll_create(evcount);
	if (epollfd < 0)
	{
		int err = errno;
		PLOG_NET_ERROR("net-sock", "net_socket.epoll_linux -- epoll_create() failed. error = %d.", err);
		return false;
	}

	for (uint32_t i = 0; i < evcount; ++i)
	{
		epoll_event* ev = events + i;
		net_socket* sock = (net_socket*) ev->data.ptr;
		SOCKET sockfd = sock->m_socket;
		if (sockfd != INVALID_SOCKET)
		{
			int error = epoll_ctl(epollfd, EPOLL_CTL_ADD, sockfd, ev);
			if (error < 0)
			{
				int err = errno;
				PLOG_NET_ERROR("net-sock", "net_socket.epoll_linux -- epoll_ctl() failed. error = %d.", err);
				int res = ::close(epollfd);
				if (res < 0)
				{
					err = errno;
					PLOG_NET_WARNING("net-sock", "net_socket.epoll_linux -- close() failed. error = %d.", err);
				}
				return false;
			}
		}
	}

	//---------------------------------------------------------
	// основная функция epoll_wait()
	//---------------------------------------------------------

	epoll_event outevents[3 * MAX_SELECT_COUNT];
    memset(outevents, 0, sizeof(outevents));

    int rc = 0;
	uint32_t remaining = timeout;

	while (true)
	{
		uint64_t start = rtl::DateTime::getTicks64();

		rc = epoll_wait(epollfd, outevents, evcount, int(remaining));

		if (rc >= 0)
			break;

		// жопа какая-то
		if (errno != EINTR)
		{
			int err = errno;
			PLOG_NET_WARNING("net-sock", "net_socket.epoll_linux -- epoll_wait() failed. error = %d.", err);
			break;
		}

		PLOG_NET_WRITE("net-sock", "net_socket.epoll_linux -- epoll_wait() failed. error = EINTR.");

		uint64_t elapsed = rtl::DateTime::getTicks64() - start;
		if (elapsed >= remaining)
			break;

		remaining -= elapsed;
	}

	//---------------------------------------------------------
	// уничтожаем объект epoll
	//---------------------------------------------------------

	int res = ::close(epollfd);
	if (res < 0)
	{
		int err = errno;
		PLOG_NET_WARNING("net-sock", "net_socket.epoll_linux -- close() failed. error = %d.", err);
	}

	if (rc < 0)
		return false;

    //---------------------------------------------------------
	// подготавливаем списки сокетов для возврата
	//---------------------------------------------------------

	if (read != NULL)
		read->clear();

	if (write != NULL)
		write->clear();

	if (error != NULL)
		error->clear();

	for (int i = 0; i < rc; ++i)
	{
		epoll_event* ev = outevents + i;

		if ((read != NULL) && (ev->events & EPOLLIN))
		{
			net_socket* sock = (net_socket*)ev->data.ptr;
			read->add(sock);
		}

		if ((write != NULL) && (ev->events & EPOLLOUT))
		{
			net_socket* sock = (net_socket*)ev->data.ptr;
			write->add(sock);
		}

		if ((error != NULL) && (ev->events & EPOLLERR))
		{
			net_socket* sock = (net_socket*)ev->data.ptr;
			error->add(sock);
		}
	}

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t net_socket::add_events(epoll_event* events, uint32_t count, socket_vector_t* sockets, int flag)
{
	epoll_event* evlast = events + count;

	for (int i = 0; i < sockets->getCount(); i++)
	{
		net_socket* sockcur = sockets->getAt(i);

		if (sockcur->m_socket != INVALID_SOCKET)
		{
			epoll_event* evcur = events;

			for (; evcur != evlast; ++evcur)
			{
				const net_socket* sockadded = (const net_socket*) evcur->data.ptr;
				if (sockadded->m_socket == sockcur->m_socket)
					break;
			}

			if (evcur == evlast)
			{
				evcur->data.ptr = (void*) sockcur;
				++evlast;
			}

			evcur->events |= flag;
		}
	}

	count = evlast - events;
	return count;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#elif defined(TARGET_OS_FREEBSD)
//------------------------------------------------------------------------------
// Реализвция Select для FreeBSD.
//------------------------------------------------------------------------------
bool					net_socket::kqueue_freebsd(socket_vector_t* read, socket_vector_t* write, socket_vector_t* error, uint32_t timeout, rtl::Logger* m_log)
{
	//---------------------------------------------------------
	// если нет параметров, то отрабатываем просто как sleep(timeout)
	//---------------------------------------------------------

	if ((read == NULL) && (write == NULL) && (error == NULL))
	{
		rtl::Thread::sleep(timeout);
		return true;
	}

	//---------------------------------------------------------
	// убедимся, что нам не передали слшиком много сокетов.
	//---------------------------------------------------------

	if ((read != NULL) && (read->getCount() > MAX_SELECT_COUNT))
		return false;

	if ((write != NULL) && (write->getCount() > MAX_SELECT_COUNT))
		return false;

	if ((error != NULL) && (error->getCount() > 0))
    {
		PLOG_NET_ERROR("net-sock", "net_socket.kqueue_freebsd -- impossible to receive \"error\" events.");
		return false;
    }

	//---------------------------------------------------------
	// заполняем массивчик для создания объекта kqueue
	//---------------------------------------------------------

	struct kevent events[2 * MAX_SELECT_COUNT];
    memset(events, 0, sizeof(events));
	uint32_t evcount = 0;

	if ((read != NULL) && read->getCount() > 0)
		evcount = add_events(events, evcount, read, EVFILT_READ);

	if ((write != NULL) && write->getCount() > 0)
		evcount = add_events(events, evcount, write, EVFILT_WRITE);

	//---------------------------------------------------------
	// создаем объект kqueue и добавляем фильтры событий
	//---------------------------------------------------------

	int kqfd = kqueue();
	if (kqfd < 0)
	{
		int err = errno;
		PLOG_NET_ERROR("net-sock", "net_socket.kqueue_freebsd -- kqueue() failed. error = %d.", err);
		return false;
	}

    int kerror = kevent(kqfd, events, evcount, NULL, 0, NULL);
    if (kerror < 0)
    {
        int err = errno;
        PLOG_NET_ERROR("net-sock", "net_socket.kqueue_freebsd -- kevent() failed. error = %d.", err);
        int res = ::close(kqfd);
        if (res < 0)
        {
            err = errno;
            PLOG_NET_WARNING("net-sock", "net_socket.kqueue_freebsd -- close() failed. error = %d.", err);
        }
        return false;
    }

	//---------------------------------------------------------
	// основная функция kevent() с ожиданием событий
	//---------------------------------------------------------

	struct kevent outevents[2 * MAX_SELECT_COUNT];
    memset(outevents, 0, sizeof(outevents));

    int rc = 0;
	uint32_t remaining = timeout;
    struct timespec remaining_ts = {remaining / 1000, (remaining % 1000) * 1000 * 1000};

	while (true)
	{
		uint64_t start = rtl::DateTime::getTicks64(); // XXX better take 'start' out of the loop and assign it ONCE!

        rc = kevent(kqfd, NULL, 0, outevents, sizeof(outevents), &remaining_ts);

		if (rc >= 0)
			break;

		// жопа какая-то
		if (errno != EINTR)
		{
			int err = errno;
			PLOG_NET_WARNING("net-sock", "net_socket.kqueue_freebsd -- kevent() failed. error = %d.", err);
			break;
		}

		PLOG_NET_WRITE("net-sock", "net_socket.kqueue_freebsd -- kevent() failed. error = EINTR.");

		uint64_t elapsed = rtl::DateTime::getTicks64() - start;
		if (elapsed >= remaining)
			break;

		remaining -= elapsed;
        remaining_ts = {remaining / 1000, (remaining % 1000) * 1000 * 1000};
	}

	//---------------------------------------------------------
	// уничтожаем объект kqueue
	//---------------------------------------------------------

	int res = ::close(kqfd);
	if (res < 0)
	{
		int err = errno;
		PLOG_NET_WARNING("net-sock", "net_socket.kqueue_freebsd -- close() failed. error = %d.", err);
	}

	if (rc < 0)
		return false;

    //---------------------------------------------------------
	// подготавливаем списки сокетов для возврата
	//---------------------------------------------------------

	if (read != NULL)
		read->clear();

	if (write != NULL)
		write->clear();

	for (int i = 0; i < rc; ++i)
	{
		struct kevent* ev = outevents + i;

		if ((read != NULL) && (ev->filter == EVFILT_READ))
		{
			net_socket* sock = (net_socket*)ev->udata;
			read->add(sock);
		}

		if ((write != NULL) && (ev->filter == EVFILT_WRITE))
		{
			net_socket* sock = (net_socket*)ev->udata;
			write->add(sock);
		}
	}

	return true;
}
//------------------------------------------------------------------------------
// Add events
//------------------------------------------------------------------------------
uint32_t net_socket::add_events(struct kevent* events, uint32_t count, socket_vector_t* sockets, short filter)
{
	struct kevent* evlast = events + count;

	for (int i = 0; i < sockets->getCount(); i++)
	{
		net_socket* sockcur = sockets->getAt(i);

		if (sockcur->m_socket != INVALID_SOCKET)
		{
            EV_SET(evlast, sockcur->m_socket, filter, EV_ADD /* | EV_CLEAR ? */, 0, 0, (void*) sockcur);
            ++evlast;
		}
	}

	count = evlast - events;
	return count;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#endif
