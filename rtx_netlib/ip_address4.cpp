﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "net/ip_address4.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void ip_address4::first_init()
{
	m_native.s_addr = INADDR_ANY; strcpy(m_text, "0.0.0.0");
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void ip_address4::copy_from(const ip_address4* ipaddr)
{
	if (ipaddr != NULL)
	{
		m_native = ipaddr->m_native;
		strcpy(m_text, ipaddr->m_text);
	}
	else
	{
		m_native.s_addr = INADDR_ANY;
		strcpy(m_text, "0.0.0.0");
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void ip_address4::set_native(const void* native)
{
	if (native != NULL)
	{
		memcpy(&m_native, native, sizeof(m_native));
		update_text();
	}
	else
	{
		m_native.s_addr = INADDR_ANY;
		strcpy(m_text, "0.0.0.0");
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void ip_address4::parse(const char* str)
{
	if (str != NULL)
	{
		m_native.s_addr = inet_addr(str);
		if (m_native.s_addr == INADDR_NONE)
		{
			if (strcmp(str, "255.255.255.255") != 0)
				m_native.s_addr = INADDR_ANY;
		}

		update_text();
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const char* ip_address4::to_string() const
{
	return m_text;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void ip_address4::update_text()
{
	m_text[0] = 0;

	const char* str = inet_ntoa(m_native);
	if (str != NULL)
	{
		strncpy(m_text, str, ARRAY_LEN(m_text));
		m_text[ARRAY_LEN(m_text)-1] = 0;
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool ip_address4::equals(const ip_address4* ip) const
{
	if (ip == NULL)
		return false;

	bool result = (m_native.s_addr == ip->m_native.s_addr);
	return result;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const bool ip_address4::empty() const
{
	if ((m_native.s_addr == INADDR_ANY) || (m_native.s_addr == INADDR_NONE))
	{
		return true;
	}

	return false;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const bool ip_address4::fill_suitable_local_mask(char* str, uint32_t len) const
{
	bool is_local = false;
	rtl::String mask(str, len);
	uint8_t* bytes = (uint8_t*)&m_native;
	if ((bytes[0] == 0) ||
		(bytes[0] == 10) ||
		(bytes[0] == 127))
	{
		
		mask << bytes[0] << ".*.*.*";
		is_local = true;
	}
	else if (((bytes[0] == 169) && (bytes[1] == 254)) ||
		((bytes[0] == 172) && (bytes[1] > 15) && (bytes[1] < 32)) ||
		((bytes[0] == 192) && (bytes[1] == 168)))
	{
		mask << bytes[0] << "." << bytes[1] << ".*.*";
		is_local = true;
	}

	strncpy(str, mask, len);

	return is_local;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
