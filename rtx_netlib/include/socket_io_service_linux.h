﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "net/net_socket.h"
#include "net/socket_io_service.h"

//class rtl::Thread;
//class rtl::Mutex;
struct epoll_event;
struct netio_thread_context;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class socket_io_service_impl
{
	socket_io_service_impl(const socket_io_service_impl&);
	socket_io_service_impl& operator = (const socket_io_service_impl&);

public:
	socket_io_service_impl(rtl::Logger* log);
	virtual ~socket_io_service_impl();

	bool create(uint32_t threads);
	void destroy();

	bool add(net_socket* sock, i_socket_event_handler* user);
	void remove(net_socket* sock);

	bool send_data_to(net_socket* sock, const uint8_t* data, uint32_t len, const socket_address* to);

private:
    netio_thread_context* get_least_busy_ctx();

	static void thread_start(rtl::Thread* thread, void* param);
	void main_loop(netio_thread_context* ctx);

	void on_epoll_event(epoll_event* epollev);

private:
	rtl::Logger* m_log;
	rtl::MutexWatch m_mutex;
	//std::vector<netio_thread_context*>	m_contexts;
	rtl::ArrayT<netio_thread_context*>	m_contexts;
};
//------------------------------------------------------------------------------
