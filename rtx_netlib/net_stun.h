﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "net_stun_message.h"
#include "net/net_socket.h"
//--------------------------------------
// tnet_stun_group
//--------------------------------------
typedef uint64_t net_stun_binding_id_t;
//--------------------------------------
// invalid binding id.
//--------------------------------------
#define NET_STUN_INVALID_BINDING_ID				0
//--------------------------------------
// Checks the validity of the STUN @a id.
//--------------------------------------
#define NET_STUN_IS_VALID_BINDING_ID(id)			(id != NET_STUN_INVALID_BINDING_ID)
//--------------------------------------
// Default port for both TCP and UDP protos as per RFC 5389 subclause 9.
//--------------------------------------
#define NET_STUN_TCP_UDP_DEFAULT_PORT 3478
//--------------------------------------
// Default port for TLS protocol as per RFC 5389 subclause 9.
//--------------------------------------
#define NET_STUN_TLS_DEFAULT_PORT 5349
//--------------------------------------
// STUN2 binding context.
//--------------------------------------
class net_nat_context_t;

class net_stun_binding_t
{
	rtl::Logger* m_log;

	net_stun_binding_id_t m_id;			// A unique id to identify this binding. 
	char* m_username;					// The username to authenticate to the STUN server. 
	char* m_password;					// The password to authenticate to the STUN server. 
	char* m_realm;						// The realm.
	char* m_nonce;						// The nonce.
	char* m_software;					// The client name.
	
	net_socket* m_localFD;			// Local file descriptor for which to get server reflexive address.
	
	ip_address_t m_server_address;			// The address of the STUN server.
	uint16_t m_server_port;
	
	net_stun_address_t m_maddr;			// Server reflexive address of the local socket(STUN1 as per RFC 3489).
	net_stun_address_t m_xmaddr;		// XORed server reflexive address (STUN2 as per RFC 5389).

public:
	net_stun_binding_t(rtl::Logger* log);
	~net_stun_binding_t();

	void initialize(net_socket* fd, const ip_address_t* server_address, uint16_t server_port, const char* username, const char* password);

	net_stun_binding_id_t get_id() const { return m_id; }
	
	const char* get_username() const { return m_username; }
	void set_username(const char* username) { rtl::strupdate(m_username, username); }
	const char* get_password() const { return m_password; }
	void set_password(const char* pswd) { rtl::strupdate(m_password, pswd); }
	const char* get_nonce() const { return m_nonce; }
	void set_nonce(const char* nonce) { rtl::strupdate(m_nonce, nonce); }
	const char* get_realm() const { return m_realm; }
	void set_realm(const char* realm) { rtl::strupdate(m_realm, realm); }
	const char* get_software() const { return m_software; }
	void set_software(const char* software) { rtl::strupdate(m_software, software); }

	const ip_address_t* get_server_address() const { return &m_server_address; }
	uint16_t get_server_port() const { return ntohs(m_server_port); }

	bool has_xmaddr() const { return m_xmaddr.family != -1; }
	const net_stun_address_t& get_xmaddr() const { return m_xmaddr; }
	void set_xmaddr(const net_stun_address_t& xmaddr) { m_xmaddr = xmaddr; }
	
	bool has_maddr() const { return m_maddr.family != -1; }
	const net_stun_address_t& get_maddr() const { return m_maddr; }
	void set_maddr(const net_stun_address_t& maddr) { m_maddr = maddr; }

	int send_bind(const net_nat_context_t* context);

private:
	net_stun_message_t* create_request();
};
//--------------------------------------
// List of @ref tnet_stun_binding_t elements.
//--------------------------------------
typedef rtl::ArrayT<net_stun_binding_t*> net_stun_binding_list_t;

net_stun_response_t* net_stun_send_unreliably(net_socket* localFD, uint16_t RTO, uint16_t Rc, net_stun_message_t* message, const ip_address_t* server, uint16_t port);
//--------------------------------------
/* TNET_STUN_H */

