﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "net_stun.h"
#include "net_turn.h"

//--------------------------------------
// Estimate of the round-trip time (RTT) in millisecond.
//--------------------------------------
#define NET_NAT_DEFAULT_RTO			500
//--------------------------------------
// Number of retransmission for UDP retransmission in millisecond.
//	7.2.1.  Sending over UDP
//	Rc SHOULD be configurable and SHOULD have a default of 7.
//--------------------------------------
#define NET_NAT_DEFAULT_RC				/*7*/3/* 7 is too hight */
//--------------------------------------
//
//--------------------------------------
#define NET_NAT_TCP_UDP_DEFAULT_PORT	3478
//--------------------------------------
// NAT context.
//--------------------------------------
class net_nat_context_t
{
protected:
	rtl::Logger* m_log;

	char* m_username;				// The username to use to authenticate against the TURN/STUN server. */
	char* m_password;				// The password to use to authenticate against the TURN/STUN server. */
	char* m_software;				// The turn/stun client name. */

	ip_address_t m_server_address;		// TURN/STUN server address (could be FQDN or IP) */
	uint16_t m_server_port;			// TURN/STUN server port. */

	uint16_t m_RTO;					// Estimate of the round-trip time (RTT) in millisecond. */
	uint16_t m_Rc;					// Number of retransmissions for UDP in millisecond. */

	bool m_enable_dontfrag;
	bool m_enable_integrity;
	bool m_enable_evenport;
	bool m_enable_fingerprint;		// Indicates whether to add the 'fingerprint' attribute in all outgoing stun/turn requests. */
	bool m_use_dnsquery;			// Indicates whether to use DNS SRV query to find the stun/turn ip address. */

	net_turn_allocation_list_t m_allocations;		// List of all allocations associated to this context. */
	net_stun_binding_list_t m_stun_bindings;		// List of all STUN2 bindings associated to this context. */

public:
	net_nat_context_t(rtl::Logger* log);
	~net_nat_context_t();

	void initialize(const char* username, const char* password);

	const char* get_username() const { return m_username; }
	const char* get_password() const { return m_password; }
	const char* get_software() const { return m_software; }
	
	bool get_fingerprint() const { return m_enable_fingerprint; }
	bool get_integrity() const { return m_enable_integrity; }
	bool get_dontfrag() const { return m_enable_dontfrag; }
	bool get_evenport() const { return m_enable_evenport; }

	const ip_address_t* get_server_address() const { return &m_server_address; }
	uint16_t get_server_port() const { return m_server_port; }
	void set_server_address(const ip_address_t* server_address);
	void set_server(const ip_address_t* server_address,  uint16_t server_port);

	uint16_t get_RTO() const { return m_RTO; }
	uint16_t get_Rc() const { return m_Rc; }

	net_stun_binding_id_t stun_bind(net_socket* localFD);
	int stun_unbind(net_stun_binding_id_t id);
	int stun_get_reflexive_address(net_stun_binding_id_t id, char** ipaddress, uint16_t *port) const;

	net_turn_allocation_id_t turn_allocate(net_socket* localFD);
	int turn_unallocate(net_turn_allocation_id_t id);

	int turn_get_reflexive_address(net_turn_allocation_id_t id, char** ipaddress, uint16_t *port) const;
	int turn_allocation_refresh(net_turn_allocation_id_t id) const;
	
	net_turn_channel_binding_id_t turn_channel_bind(net_turn_allocation_id_t id, const ip_address_t* address, uint16_t port) const;
	int turn_channel_refresh(net_turn_channel_binding_id_t id);
	int turn_channel_send(net_turn_channel_binding_id_t id, const void* data, int size, int indication) const;
	int turn_add_permission(net_turn_allocation_id_t id, const char* ipaddress, uint32_t timeout) const;

private:
	net_stun_binding_t* find_stun_binding(net_stun_binding_id_t id) const;
	net_turn_allocation_t* find_turn_allocation(net_turn_allocation_id_t id) const;

	net_stun_request_t* create_request(net_turn_allocation_t* allocation, net_stun_message_type_t type);
};
//--------------------------------------
// Handle to the NAT context(@ref tnet_nat_context_t).
//--------------------------------------
typedef void net_nat_context_handle_t;
//--------------------------------------
