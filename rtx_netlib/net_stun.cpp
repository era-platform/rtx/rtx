﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "net_nat_ctx.h"
#include "net_stun.h"

//--------------------------------------
// Session Traversal Utilities for NAT (STUN) implementation as per RFC 5389 and RFC 3489(Obsolete).
//--------------------------------------
net_stun_binding_t::net_stun_binding_t(rtl::Logger* log) : m_log(log)
{
	static net_stun_binding_id_t __binding_unique_id = 0;

	m_id = ++__binding_unique_id;

	m_localFD = nullptr;

	m_username = nullptr;
	m_password = nullptr;
		
	m_server_address.parse("");
	m_server_port = 0;

	m_software = nullptr;
}
//--------------------------------------
//
//--------------------------------------
net_stun_binding_t::~net_stun_binding_t()
{
	DELETEAR(m_username);
	DELETEAR(m_password);
	DELETEAR(m_realm);
	DELETEAR(m_nonce);
	DELETEAR(m_software);
}
//--------------------------------------
//
//--------------------------------------
void net_stun_binding_t::initialize(net_socket* fd, const ip_address_t* server_address, uint16_t server_port, const char* username, const char* password)
{
	m_localFD = fd;

	m_username = rtl::strdup(username);
	m_password = rtl::strdup(password);
		
	m_server_address.copy_from(server_address);
	m_server_port = server_port;

	m_software = rtl::strdup("IM-client/OMA1.0 doubango/v0.0.0");
}
//--------------------------------------
// Create generic STUN2 request with all mandatory headers and attributes. 
// @param [in,out]	binding	The binding object from which to create the request. 
// @retval	STUN2 request if succeed and nullptr otherwise. 
//--------------------------------------
net_stun_message_t* net_stun_binding_t::create_request()
{
	/* Set the request type (RFC 5389 defines only one type) */
	net_stun_message_t *message = NEW net_stun_message_t(net_stun_binding_request, m_username, m_password);

	message->set_realm(m_realm);
	message->set_nonce(m_nonce);

	message->generate_transaction_id();

	// Add software attribute
	if (m_software)
	{
		message->add_attribute_data(net_stun_software, m_software, int(strlen(m_software)));
	}
	
	return message;
}
//--------------------------------------
// Internal function to send a STUN2 binding request over the network.
//--------------------------------------
int net_stun_binding_t::send_bind(const net_nat_context_t* context)
{
	int ret = -1;
	net_stun_response_t *response = 0;
	net_stun_request_t *request = 0;

	//goto stun_phase0;

	/*	RFC 5389 - 10.2.1.1.  First Request
		If the client has not completed a successful request/response
		transaction with the server (as identified by hostname, if the DNS
		procedures of Section 9 are used, else IP address if not), it SHOULD
		omit the USERNAME, MESSAGE-INTEGRITY, REALM, and NONCE attributes.
		In other words, the very first request is sent as if there were no
		authentication or message integrity applied.
	*/
//stun_phase0:
	{
		if (!(request = create_request()))
		{
			return -1;
		}		

		response = net_stun_send_unreliably(m_localFD, context->get_RTO(), context->get_Rc(), request, &m_server_address, m_server_port);

		if (response)
		{
			if (NET_STUN_RESPONSE_IS_ERROR(response->getType()))
			{
				short code = response->get_errorcode();
				const char* realm = response->get_realm();
				const char* nonce = response->get_nonce();

				if (code == 401 && realm && nonce)
				{
					if (!m_nonce)
					{
						/* First time we get a nonce */
						set_nonce(nonce);
						set_realm(realm);

						/* Delete the message and response before retrying*/
						DELETEO(response);
						DELETEO(request);

						// Send again using new transaction identifier
						return send_bind(context);
					}
					else
					{
						return -3;
					}
				}
				else
				{
					return -2;
				}
			}
			else
			{
				const net_stun_attribute_t& xmaddr = response->get_attribute(net_stun_xor_mapped_address);
				if (xmaddr.type == net_stun_xor_mapped_address)
				{
					ret = 0;
					m_xmaddr = xmaddr.address;
				}
				else
				{
					const net_stun_attribute_t& maddr = response->get_attribute(net_stun_mapped_address);
					if (maddr.type == net_stun_mapped_address)
					{
						ret = 0;
						m_maddr = maddr.address;
					}
				}
			}
		}
	} 

	/* END OF stun_phase0 */
	DELETEO(response);
	DELETEO(request);

	return ret;
}
//--------------------------------------
//
//--------------------------------------
//int tnet_stun_send_reliably(const tnet_stun_message_t* message)
//{
//	return -1;
//}
//--------------------------------------
// Internal function to send a STUN message using unrealiable protocol such as UDP.
//--------------------------------------
net_stun_response_t* net_stun_send_unreliably(net_socket* localFD, uint16_t RTO, uint16_t Rc, net_stun_message_t* message, const ip_address_t* address, uint16_t port)
{
	/*	RFC 5389 - 7.2.1.  Sending over UDP
		STUN indications are not retransmitted; thus, indication transactions over UDP 
		are not reliable.
	*/

	int ret = -1;
	uint16_t i, rto = RTO;
	struct timeval tv;
	fd_set set;
	rtl::MemoryStream stream;
	
	ret = message->encode(stream);
	if (ret < 4)
		return nullptr;
	
	tv.tv_sec = 0;
	tv.tv_usec = 0;

	/*	RFC 5389 - 7.2.1.  Sending over UDP
		A client SHOULD retransmit a STUN request message starting with an
		interval of RTO ("Retransmission TimeOut"), doubling after each
		retransmission.

		e.g. 0 ms, 500 ms, 1500 ms, 3500 ms, 7500ms, 15500 ms, and 31500 ms
	*/

	for (i = 0; i < Rc; i++)
	{
		tv.tv_sec += rto/1000;
		tv.tv_usec += (rto% 1000) * 1000;
		
		FD_ZERO(&set);
		FD_SET(localFD->get_native_handle(), &set);

		socket_address saddr(address, port);
		ret = localFD->send_to(stream.getBuffer(), int(stream.getLength()), &saddr);

		if ((ret = select(0, &set, nullptr, nullptr, &tv)) < 0)
		{
			return nullptr;
		}
		else if (ret > 0 && FD_ISSET(localFD->get_native_handle(), &set))
		{
			/* there is data to read */
			uint32_t len = 0;
			void* data = 0;
		
			LOG_NET("net-stun", "STUN request got response");

			/* Check how how many bytes are pending */
			//if ((ret = localFD->io_control(FIONREAD, &len)) < 0)
			//{
			//	return nullptr;
			//}
			len = localFD->get_available_bytes_count();
			if (len==0)
			{
				LOG_NET("net-stun", "tnet_ioctlt() returent zero bytes");
				continue;
			}

			/* Receive pending data */
			data = MALLOC(len);
			//sockaddr_in server;
			socket_address server;
			memset(&server, 0, sizeof server);
			ret = localFD->recv_from((char*)data, len, &server);
			if (ret == 0)
			{
				FREE(data);
				LOG_NET_ERROR("net-stun", "Recving STUN dgrams failed.");
				return nullptr;
			}

			/* Parse the incoming response. */
			net_stun_response_t* response = NEW net_stun_message_t();
			response->decode((const uint8_t*)data, ret);
			FREE(data);

			if (response)
			{
				if (memcmp(message->get_transaction_id(), response->get_transaction_id(), NET_STUN_TRANSACID_SIZE) != 0)
				{
					/* Not same transaction id */
					DELETEO(response);
					continue;
				}
			}
			
			return response;
		}
		else if(ret == 0)
		{
			/* timeout */
			LOG_NET_WARN("net-stun", "STUN request timedout at %d", i);
			rto *= 2;
		}
	}

	return nullptr;
}
//--------------------------------------

