﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "dtls_define.h"
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
dtls_connection_role_t dtls_connection_role_parse(const char* val)
{
	if (std_stricmp(val, "active") == 0)
	{
		return dtls_connection_role_active_e;
	}

	if (std_stricmp(val, "passive") == 0)
	{
		return dtls_connection_role_passive_e;
	}
	if (std_stricmp(val, "actpass") == 0)
	{
		return dtls_connection_role_actpass_e;
	}

	return dtls_connection_role_invalid_e;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
const char*	dtls_connection_role_get_name(dtls_connection_role_t val)
{
	const char* names[] = { "active", "passive", "actpass" };
	return val >= dtls_connection_role_active_e && val <= dtls_connection_role_actpass_e ? names[val] : "invalid";
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
