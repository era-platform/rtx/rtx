﻿/* RTX H.248 Media Gate Network Library
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "net/socket_io_service.h"

#if defined(TARGET_OS_WINDOWS)
	#include "include/socket_io_service_win32.h"
#elif defined(TARGET_OS_LINUX)
	#include "include/socket_io_service_linux.h"
#elif defined(TARGET_OS_FREEBSD)
	#include "include/socket_io_service_freebsd.h"
#endif

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
socket_io_service_t::socket_io_service_t(rtl::Logger* log)
{
	m_implementation = NEW socket_io_service_impl(log);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
socket_io_service_t::~socket_io_service_t()
{
	destroy();

	DELETEO(m_implementation);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool				socket_io_service_t::create(uint32_t threads)
{
	return m_implementation->create(threads);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void				socket_io_service_t::destroy()
{
	m_implementation->destroy();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool				socket_io_service_t::add(net_socket* sock, i_socket_event_handler* user)
{
	return m_implementation->add(sock, user);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void				socket_io_service_t::remove(net_socket* sock)
{
	m_implementation->remove(sock);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool				socket_io_service_t::send_data_to(net_socket* sock, const uint8_t* data, uint32_t len, const socket_address* to)
{
	return m_implementation->send_data_to(sock, data, len, to);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
