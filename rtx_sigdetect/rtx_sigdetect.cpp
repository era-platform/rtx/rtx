/* RTX H.248 Media Gate Signal detecter (Tools)
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_file_wave.h"
#include "spandsp.h"
//--------------------------------------
//
//--------------------------------------
#if defined (DEBUG_MEMORY)
void* operator new (size_t size, char* file, int line)
{
	return std_mem_checker_malloc(size, file, line, rc_malloc_new_e);
}
void* operator new[](size_t size, char* file, int line)
{
	return std_mem_checker_malloc(size, file, line, rc_malloc_newar_e);
}
void operator delete (void* mem_ptr)
{
	std_mem_checker_free(mem_ptr, rc_malloc_delete_e);
}
void operator delete[](void* mem_ptr)
{
	std_mem_checker_free(mem_ptr, rc_malloc_deletear_e);
}
#endif

#define DTMF_INFO_MAX	100

struct DTMF_INFO
{
	char signal;
	int start;
	int duration;
};

DTMF_INFO dtmf_collector[DTMF_INFO_MAX];
int dtmf_collected = 0;

void add_dtmf(char symbol, int start, int duration)
{
	if (dtmf_collected < DTMF_INFO_MAX)
	{
		dtmf_collector[dtmf_collected].signal = symbol;
		dtmf_collector[dtmf_collected].start = start;
		dtmf_collector[dtmf_collected].duration = duration;

		dtmf_collected++;
	}
}
rtl::String print_results();
//--------------------------------------
// ����������� ���� ������
//--------------------------------------
static void span_message(int level, const char *msg)
{
	if (msg == nullptr)
		return;

	if (level == SPAN_LOG_ERROR)
		printf("Error: spandsp engine : fax error(%d): %s\n", level, msg);
	else if (level == SPAN_LOG_WARNING)
		printf("Warning: spandsp engine : fax warning(%d): %s\n", level, msg);
	else
		printf("Out: info(%d): %s\n", level, msg);
}
//--------------------------------------
// ����������� ������ ���� ������
//--------------------------------------
static void span_error(const char *msg)
{
	if (msg == nullptr)
		return;

	printf("SE: %s", msg);
}

//--------------------------------------
//
//--------------------------------------
int main(int argc, char* argv[])
{
	if (argc < 2)
	{
		fprintf(stdout, "Usage: rtx_sigdetect wavefilename\n");
		return 0;
	}

	rtl::String filename = argv[1];

	media::FileReaderWAV wave;
	dtmf_rx_state_t* dtmf_state;

	if (!wave.open(filename)) // "c:/test/sig.wav"s
	{
		fprintf(stderr, "ERROR: File not found\n");
		return 1;
	}

	span_set_message_handler(span_message);
	span_set_error_handler(span_error);

	//printf("Start detecting...\n");

	dtmf_state = dtmf_rx_init(nullptr, nullptr, nullptr);
	int count = 0;
	int total = 0;
	int packets = 0;
	short samples[160];
	int size;

	while ((size = wave.readAudioData(samples, 320)) > 0)
	{
		count = size / 2;
		//printf("wave file read %d samples\n", count);
		dtmf_rx(dtmf_state, samples, count);
		char tone[2] = { 0, 0 };

		dtmf_rx_get(dtmf_state, tone, 1);

		if (tone[0] != 0 && tone[0] != 'x')
		{
			//printf("detecting dtmf %c, starts %dms\n", (wchar_t)tone[0], total * 8);
			add_dtmf(tone[0], total*8, 250);
		}

		total += count;
		packets++;
	}

	dtmf_rx_free(dtmf_state);

	wave.close();

	//printf("Detecting stop...%d packets %d samples processed\n", packets, total);
	
	rtl::String stream = print_results();

	fprintf(stdout, "%s", (const char*)stream);

	int idx = filename.lastIndexOf(FS_PATH_DELIMITER);

	if (idx != BAD_INDEX)
	{
		filename.remove(idx + 1, BAD_INDEX);
		filename += "out.json";
	}
	else 
	{
		idx = filename.lastIndexOf('/');
		if (idx != BAD_INDEX)
		{
			filename.remove(idx + 1, BAD_INDEX);
			filename += "out.json";
		}
		else
		{
			filename = "out.json";
		}
	}

	rtl::FileStream file;

	if (file.createNew(filename))
	{
		file.write((const char*)stream, stream.getLength());

		file.close();
	}
	else
	{
		fprintf(stdout, "Error: %s not created! %d", (const char*)filename, errno);
	}

	return 0;
}

rtl::String print_results()
{
	rtl::String stream = "{\"dtmf\":[";

	for (int i = 0; i < dtmf_collected; i++)
	{
		DTMF_INFO& dtmf = dtmf_collector[i];

		stream << "{\"symbol\":\"" << dtmf.signal << "\",";
		stream << "\"start\":" << dtmf.start << ',';
		stream << "\"duration\":" << dtmf.duration << '}';
		
		if (i < dtmf_collected - 1)
			stream << ',';
	}

	stream += "]}";

	return stream;
}
//--------------------------------------
