﻿/* RTX H.248 Media Gate Engine Test Module 
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "test_rtx_media_engine.h"
#include "net/net_socket.h"

namespace rtx_media_engine
{
	class rtx_stream_mode_bothway_test_suite_t : public testing::Test
	{
	public:
		rtx_stream_mode_bothway_test_suite_t();
		virtual ~rtx_stream_mode_bothway_test_suite_t();
		virtual void SetUp();
		virtual void TearDown();
		void send_packet_in();
		void send_packet_out();
		bool receive_packet_out();
		bool receive_packet_in();
	};
	//--------------------------------------------------------------------------
	class rtx_stream_mode_conference_test_suite_t : public testing::Test
	{
	public:
		rtx_stream_mode_conference_test_suite_t();
		virtual ~rtx_stream_mode_conference_test_suite_t();
		virtual void SetUp();
		virtual void TearDown();
		void send_packet_in();
		void send_packet_out();
		bool receive_packet_out();
		bool receive_packet_in();
	};
}
//------------------------------------------------------------------------------
