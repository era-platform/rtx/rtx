/* RTX H.248 Media Gate Engine Test Module 
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "test_audio_transcode.h"
#include "test_utils.h"
#include "../rtx_media_engine/mg_codec_to_pcm_tube.h"
#include "../rtx_media_engine/mg_pcm_to_codec_tube.h"
#include "../rtx_media_engine/mg_resampler_tube.h"
#include "../rtx_media_engine/mg_media_element.h"
#include "rtp_packet.h"
//-----------------------------------------------
//
//-----------------------------------------------
namespace rtx_media_engine
{
	class audio_term_handler : public mge::INextDataHandler
	{
		const uint8_t* m_ref_vec;
		int m_ref_vec_size;
		int m_ref_vec_offset;
		rtl::FileStream writer;

	public:
		audio_term_handler(const char* fileout, const uint8_t* ref_vec, int size)
		{
			m_ref_vec = ref_vec;
			m_ref_vec_size = size;
			m_ref_vec_offset = 0;
			if (fileout != nullptr)
			{
				rtl::String fpath = get_path_to_tests_data() + FS_PATH_DELIMITER + fileout;
				writer.createNew(fpath);
			}
		}
		~audio_term_handler()
		{
			writer.close();
		}

		virtual void send(const mge::INextDataHandler* out_handler, const rtp_packet* packet);
		virtual void send(const mge::INextDataHandler* out_handler, const uint8_t* data, uint32_t len);
		virtual void send(const mge::INextDataHandler* out_handler, const rtcp_packet_t* packet);
		virtual bool unsubscribe(const mge::INextDataHandler* out_handler);
	};

	void audio_term_handler::send(const mge::INextDataHandler* out_handler, const rtp_packet* packet)
	{
		const uint8_t *pdata = packet->get_payload();
		int psize = packet->get_payload_length();

		const uint8_t *rdata = m_ref_vec + m_ref_vec_offset;

		int res = memcmp(pdata, rdata, psize);

		if (writer.isOpened())
		{
			writer.write(pdata, psize);
		}

		ASSERT_EQ(res, 0);

		m_ref_vec_offset += psize;

		ASSERT_LE(m_ref_vec_offset, m_ref_vec_size);
	}
	void audio_term_handler::send(const mge::INextDataHandler* out_handler, const uint8_t* data, uint32_t len)
	{
	}
	void audio_term_handler::send(const mge::INextDataHandler* out_handler, const rtcp_packet_t* packet)
	{
	}
	bool audio_term_handler::unsubscribe(const mge::INextDataHandler* out_handler)
	{
		return false;
	}

	void audio_transcode_test(media::PayloadFormat* src_fmt, media::PayloadFormat* dst_fmt)
	{
		if (src_fmt->isEqual(*dst_fmt))
			return;

		/*
		
		
		
		
		*/

		// 
		uint8_t* src_data = nullptr;
		int src_data_len = 0;
		char vector_name[128];

		std_snprintf(vector_name, 128, "%s_%d_%d.src", (const char*)src_fmt->getEncoding(), src_fmt->getFrequency(), src_fmt->getChannelCount());

		if (!load_test_vector(vector_name, &src_data, &src_data_len))
		{
			ERRORF("audio_encode_test....invalid path to test input vector '%s'\n", vector_name);
			return;
		}

		uint8_t* dst_data = nullptr;
		int dst_data_len = 0;

		std_snprintf(vector_name, 128, "%s_%d_%d.dst", (const char*)dst_fmt->getEncoding(), dst_fmt->getFrequency(), dst_fmt->getChannelCount());

		if (!load_test_vector(vector_name, &dst_data, &dst_data_len))
		{
			ERRORF("audio_encode_test....invalid path to test output vector '%s'\n", vector_name);
			return;
		}

		mge::mg_media_element_t el_in("at-0-src", src_fmt->getId_u8(), nullptr);
		mge::mg_media_element_t el_out("at-0-dst", dst_fmt->getId_u8(), nullptr);

		el_in.set_element_name(src_fmt->getEncoding());
		el_in.set_sample_rate(src_fmt->getFrequency());
		el_in.set_sample_rate(src_fmt->getFrequency());

		el_out.set_element_name(dst_fmt->getEncoding());
		el_out.set_sample_rate(dst_fmt->getFrequency());
		el_out.set_sample_rate(dst_fmt->getFrequency());

		mge::mg_codec_to_pcm_tube_t* src_tube = NEW mge::mg_codec_to_pcm_tube_t(&Log, 1, "at-0");
		mge::mg_pcm_to_codec_tube_t* dst_tube = NEW mge::mg_pcm_to_codec_tube_t(&Log, 2, "at-0");
		mge::mg_resampler_tube_t* resampler = nullptr;

		//std_snprintf(vector_name, 128, "%s_%d_%d.dst_out", (const char*)dst_fmt->getEncoding(), dst_fmt->getFrequency(), dst_fmt->getChannelCount());
		//vector_name
		audio_term_handler term(nullptr, dst_data, dst_data_len);

		src_tube->init(&el_in, &el_out);
		dst_tube->init(&el_in, &el_out);

		if (src_fmt->getFrequency() != dst_fmt->getFrequency())
		{
			resampler = NEW mge::mg_resampler_tube_t(&Log, 3, "at-0");
			resampler->init(&el_in, &el_out);
			src_tube->setNextHandler(resampler);
			resampler->setNextHandler(dst_tube);

			PRINTF("Make resampler!\n");
		}
		else
		{
			src_tube->setNextHandler(dst_tube);
		}

		dst_tube->setNextHandler(&term);

		int frame_size = 160;
		int src_data_offset = 0;
		uint32_t ssrc = 0x1234abcd;
		uint16_t seq = 1234;
		uint32_t ts = 0x1000000;
		uint32_t ts_step = (src_fmt->getFrequency() / 1000) * 20;

		while (src_data_offset + frame_size <= src_data_len)
		{
			rtp_packet packet;
			packet.set_payload_type(src_fmt->getId_u8());
			packet.set_payload(src_data + src_data_offset, frame_size);
			packet.set_ssrc(ssrc);
			packet.set_sequence_number(seq++);
			packet.set_timestamp(ts); ts += ts_step;
			
			src_tube->send(nullptr, &packet);
			
			src_data_offset += frame_size;
		}
	}

	/*TEST(test_audio_transcode, g711a_g729)
	{
		PRINTF("test PCMA ==> G729...\n");
		PayloadFormat fmt1 = { "PCMA", 8, 8000, 1 };
		PayloadFormat fmt2 = { "G729", 18, 8000, 1 };
		
		audio_transcode_test(&fmt1, &fmt2);
	}*/
	TEST(test_audio_transcode, g711a_g722)
	{
		PRINTF("test PCMA ==> G722...\n");
		media::PayloadFormat fmt1 = { "PCMA", media::PayloadId::PCMA, 8000, 1 };
		media::PayloadFormat fmt3 = { "G722", media::PayloadId::G722, 16000, 1 };

		audio_transcode_test(&fmt1, &fmt3);
	}

}
