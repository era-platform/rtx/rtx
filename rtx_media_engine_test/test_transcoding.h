﻿/* RTX H.248 Media Gate Engine Test Module 
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "test_rtx_media_engine.h"

 //------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
namespace rtx_media_engine
{
	struct test_transcoding_param_t
	{
		uint8_t payload_id_in = UINT8_MAX;
		uint8_t payload_id_out = UINT8_MAX;

		char rtpmap_in[64];
		char rtpmap_out[64];

		static const int buffer_size = 512;
		uint8_t payload_in[buffer_size];
		int payload_in_size = 0;
		uint8_t payload_out[buffer_size];
		int payload_out_size = 0;
	};
	//--------------------------------------------------------------------------
	typedef void fill_transcoding_test_param_fun(test_transcoding_param_t*);
	//--------------------------------------------------------------------------
	class test_transcoding_suite_t : public ::testing::TestWithParam<fill_transcoding_test_param_fun*>
	{
	public:
		test_transcoding_suite_t();
		virtual ~test_transcoding_suite_t();
		virtual void SetUp();
		virtual void TearDown();
	protected:
		test_transcoding_param_t m_transcoding_param;
	};
	//------------------------------------------------------------------------------
	void fill_data_PCMA_to_PCMU(test_transcoding_param_t* param);
	void fill_data_PCMU_to_PCMA(test_transcoding_param_t* param);
	//------------------------------------------------------------------------------
	INSTANTIATE_TEST_CASE_P(transcoding, test_transcoding_suite_t,
		::testing::Values(
			&fill_data_PCMA_to_PCMU,
			&fill_data_PCMU_to_PCMA
	));
}
//------------------------------------------------------------------------------
