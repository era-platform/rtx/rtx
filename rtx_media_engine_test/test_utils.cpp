﻿/* RTX H.248 Media Gate Engine Test Module 
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "test_utils.h"
#include "rtp_packet.h"
#include "std_sys_env.h"
#include "std_file_stream.h"
#include "mock_callbacks.h"

 //------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
namespace rtx_media_engine
{
	rtl::String get_path_to_tests_data()
	{
		const char* vec_path = Config.get_config_value("vecpath");

		if (vec_path == nullptr || vec_path[0] == 0)
		{
			char buffer[256];
			
			return std_get_application_startup_path(buffer, 256) ? rtl::String(buffer) : rtl::String::empty;
		}

		return rtl::String(vec_path);
	}
	//--------------------------------------------------------------------------
	int read_data_from_file(const char* path, uint8_t* buff, int buff_size)
	{
		rtl::FileStream file;
		if (!file.open(path))
		{
			return 0;
		}
		int read = int(file.read(buff, buff_size));
		file.close();
		return read;
	}
	bool load_test_vector(const char* name, uint8_t** dataptr, int* datasize)
	{
		rtl::String path = get_path_to_tests_data();
		path << FS_PATH_DELIMITER << name;
		PRINTF("vector filepath is %s\n", (const char*)path);
		rtl::FileStream file;
		if (!file.open(path))
		{
			ERRORF("invalid filepath %s errno(%d)\n", (const char*)path, errno);
			return false;
		}

		*datasize = (int)file.getLength();
		*dataptr = (uint8_t*)MALLOC(*datasize);
		int read = (int)file.read(*dataptr, *datasize);
		file.close();
		if (read != *datasize)
		{
			ERRORF("invalid file read %d != %d\n", read, *datasize);
			FREE(*dataptr);
			*datasize = 0;
		}
		PRINTF("vector size %d\n", *datasize);
		return *datasize != 0;
	}
	//--------------------------------------------------------------------------
	bool open_tests_sockets(const rtl::ArrayT<net_socket*>* list, ip_address_t* ipaddr)
	{
		if (list == nullptr)
		{
			return false;
		}
		uint16_t port = 8181;
		for (int i = 0; i < list->getCount(); i++)
		{
			net_socket* socket = list->getAt(i);
			if (socket == nullptr)
			{
				return false;
			}
			if (!socket->create(E_IP_ADDRESS_FAMILY_IPV4, e_ip_protocol_type::E_IP_PROTOCOL_UDP))
			{
				return false;
			}

			bool bind = false;
			for (; port < 8281; port++)
			{
				socket_address sockaddr(ipaddr, port);
				if (socket->bind(&sockaddr))
				{
					port++;
					bind = true;
					break;
				}
			}
			if (!bind)
			{
				socket->close();
				return false;
			}
		}

		return true;
	}
	//--------------------------------------------------------------------------
	void close_tests_sockets(const rtl::ArrayT<net_socket*>* list)
	{
		if (list == nullptr)
		{
			return;
		}
		for (int i = 0; i < list->getCount(); i++)
		{
			net_socket* socket = list->getAt(i);
			if (socket == nullptr)
			{
				return;
			}
			socket->close();
		}

		return;
	}
}
//------------------------------------------------------------------------------
