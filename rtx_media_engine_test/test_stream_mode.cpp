﻿/* RTX H.248 Media Gate Engine Test Module 
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "test_stream_mode.h"
#include "rtp_packet.h"
#include "test_utils.h"
#include "mock_callbacks.h"

 //------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
namespace rtx_media_engine
{
	//--------------------------------------------------------------------------
	//
	//--------------------------------------------------------------------------
	rtx_stream_mode_bothway_test_suite_t::rtx_stream_mode_bothway_test_suite_t()
	{
		
	}
	//--------------------------------------------------------------------------
	rtx_stream_mode_bothway_test_suite_t::~rtx_stream_mode_bothway_test_suite_t()
	{
		
	}
	//--------------------------------------------------------------------------
	void rtx_stream_mode_bothway_test_suite_t::SetUp()
	{
		ASSERT_TRUE(nullptr != g_environment);
		rtx_test_context_bothway_enviroment_info* bothway = g_environment->get_context_bothway();
		ASSERT_TRUE(nullptr != bothway);
		ASSERT_TRUE(nullptr != bothway->context);
		ASSERT_TRUE(nullptr != bothway->termination_info_1.socket);
		ASSERT_TRUE(nullptr != bothway->termination_info_2.socket);

		clear_receiver(bothway->termination_info_1.socket);
		clear_receiver(bothway->termination_info_2.socket);
	}
	//--------------------------------------------------------------------------
	void rtx_stream_mode_bothway_test_suite_t::TearDown()
	{
		rtx_test_context_bothway_enviroment_info* bothway = g_environment->get_context_bothway();

		clear_receiver(bothway->termination_info_1.socket);
		clear_receiver(bothway->termination_info_2.socket);

		ASSERT_TRUE(bothway->context->mg_context_lock());

		h248::Field reply;
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_1.termination_id, 1, h248::TerminationStreamMode::SendRecv, &reply));
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_2.termination_id, 1, h248::TerminationStreamMode::SendRecv, &reply));

		ASSERT_TRUE(bothway->context->mg_context_unlock());
	}
	//--------------------------------------------------------------------------
	void rtx_stream_mode_bothway_test_suite_t::send_packet_in()
	{
		rtx_test_context_bothway_enviroment_info* bothway = g_environment->get_context_bothway();
		ASSERT_NE(0, bothway->termination_info_1.port);

		send_packet(bothway->termination_info_1.socket, bothway->termination_info_1.port, bothway->termination_info_1.seq, bothway->termination_info_1.timestamp);
		bothway->termination_info_1.seq++;
		bothway->termination_info_1.timestamp += 160;
	}
	//--------------------------------------------------------------------------
	void rtx_stream_mode_bothway_test_suite_t::send_packet_out()
	{
		rtx_test_context_bothway_enviroment_info* bothway = g_environment->get_context_bothway();
		ASSERT_NE(0, bothway->termination_info_2.port);

		send_packet(bothway->termination_info_2.socket, bothway->termination_info_2.port, bothway->termination_info_2.seq, bothway->termination_info_2.timestamp);
		bothway->termination_info_2.seq++;
		bothway->termination_info_2.timestamp += 160;
	}
	//--------------------------------------------------------------------------
	bool rtx_stream_mode_bothway_test_suite_t::receive_packet_out()
	{
		rtx_test_context_bothway_enviroment_info* bothway = g_environment->get_context_bothway();
		if (bothway == nullptr || bothway->termination_info_2.socket == nullptr)
		{
			return false;
		}

		return receive_packet(bothway->termination_info_2.socket);
	}
	//--------------------------------------------------------------------------
	bool rtx_stream_mode_bothway_test_suite_t::receive_packet_in()
	{
		rtx_test_context_bothway_enviroment_info* bothway = g_environment->get_context_bothway();
		if (bothway == nullptr || bothway->termination_info_1.socket == nullptr)
		{
			return false;
		}

		return receive_packet(bothway->termination_info_1.socket);
	}
	//--------------------------------------------------------------------------
	//
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_bothway_test_suite_t, stream_mode_sendreceive_receiveonly_test)
	{
		rtx_test_context_bothway_enviroment_info* bothway = g_environment->get_context_bothway();
		ASSERT_TRUE(bothway->context->mg_context_lock());

		h248::Field reply;
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_1.termination_id, 1, h248::TerminationStreamMode::SendRecv, &reply));
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_2.termination_id, 1, h248::TerminationStreamMode::RecvOnly, &reply));

		ASSERT_TRUE(bothway->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_TRUE(receive_packet_out());
		ASSERT_FALSE(receive_packet_in());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_bothway_test_suite_t, stream_mode_sendreceive_sendonly_test)
	{
		rtx_test_context_bothway_enviroment_info* bothway = g_environment->get_context_bothway();
		ASSERT_TRUE(bothway->context->mg_context_lock());
		h248::Field reply;
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_1.termination_id, 1, h248::TerminationStreamMode::SendRecv, &reply));
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_2.termination_id, 1, h248::TerminationStreamMode::SendOnly, &reply));

		ASSERT_TRUE(bothway->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_FALSE(receive_packet_out());
		ASSERT_TRUE(receive_packet_in());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_bothway_test_suite_t, stream_mode_sendreceive_inactive_test)
	{
		rtx_test_context_bothway_enviroment_info* bothway = g_environment->get_context_bothway();
		ASSERT_TRUE(bothway->context->mg_context_lock());

		h248::Field reply;

		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_1.termination_id, 1, h248::TerminationStreamMode::SendRecv, &reply));
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_2.termination_id, 1, h248::TerminationStreamMode::Inactive, &reply));

		ASSERT_TRUE(bothway->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_FALSE(receive_packet_out());
		ASSERT_FALSE(receive_packet_in());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_bothway_test_suite_t, stream_mode_receiveonly_receiveonly_test)
	{
		rtx_test_context_bothway_enviroment_info* bothway = g_environment->get_context_bothway();
		ASSERT_TRUE(bothway->context->mg_context_lock());
		h248::Field reply;
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_1.termination_id, 1, h248::TerminationStreamMode::RecvOnly, &reply));
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_2.termination_id, 1, h248::TerminationStreamMode::RecvOnly, &reply));

		ASSERT_TRUE(bothway->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_FALSE(receive_packet_out());
		ASSERT_FALSE(receive_packet_in());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_bothway_test_suite_t, stream_mode_receiveonly_sendreceive_test)
	{
		rtx_test_context_bothway_enviroment_info* bothway = g_environment->get_context_bothway();
		ASSERT_TRUE(bothway->context->mg_context_lock());

		h248::Field reply;
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_1.termination_id, 1, h248::TerminationStreamMode::RecvOnly, &reply));
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_2.termination_id, 1, h248::TerminationStreamMode::SendRecv, &reply));

		ASSERT_TRUE(bothway->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_FALSE(receive_packet_out());
		ASSERT_TRUE(receive_packet_in());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_bothway_test_suite_t, stream_mode_receiveonly_sendonly_test)
	{
		rtx_test_context_bothway_enviroment_info* bothway = g_environment->get_context_bothway();
		ASSERT_TRUE(bothway->context->mg_context_lock());

		h248::Field reply;
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_1.termination_id, 1, h248::TerminationStreamMode::RecvOnly, &reply));
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_2.termination_id, 1, h248::TerminationStreamMode::SendOnly, &reply));

		ASSERT_TRUE(bothway->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_FALSE(receive_packet_out());
		ASSERT_TRUE(receive_packet_in());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_bothway_test_suite_t, stream_mode_receiveonly_inactive_test)
	{
		rtx_test_context_bothway_enviroment_info* bothway = g_environment->get_context_bothway();
		ASSERT_TRUE(bothway->context->mg_context_lock());

		h248::Field reply;
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_1.termination_id, 1, h248::TerminationStreamMode::RecvOnly, &reply));
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_2.termination_id, 1, h248::TerminationStreamMode::Inactive, &reply));

		ASSERT_TRUE(bothway->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_FALSE(receive_packet_out());
		ASSERT_FALSE(receive_packet_in());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_bothway_test_suite_t, stream_mode_sendonly_sendonly_test)
	{
		rtx_test_context_bothway_enviroment_info* bothway = g_environment->get_context_bothway();
		ASSERT_TRUE(bothway->context->mg_context_lock());

		h248::Field reply;
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_1.termination_id, 1, h248::TerminationStreamMode::SendOnly, &reply));
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_2.termination_id, 1, h248::TerminationStreamMode::SendOnly, &reply));

		ASSERT_TRUE(bothway->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_FALSE(receive_packet_out());
		ASSERT_FALSE(receive_packet_in());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_bothway_test_suite_t, stream_mode_sendonly_sendreceive_test)
	{
		rtx_test_context_bothway_enviroment_info* bothway = g_environment->get_context_bothway();
		ASSERT_TRUE(bothway->context->mg_context_lock());

		h248::Field reply;
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_1.termination_id, 1, h248::TerminationStreamMode::SendOnly, &reply));
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_2.termination_id, 1, h248::TerminationStreamMode::SendRecv, &reply));

		ASSERT_TRUE(bothway->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_TRUE(receive_packet_out());
		ASSERT_FALSE(receive_packet_in());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_bothway_test_suite_t, stream_mode_sendonly_receiveonly_test)
	{
		rtx_test_context_bothway_enviroment_info* bothway = g_environment->get_context_bothway();
		ASSERT_TRUE(bothway->context->mg_context_lock());

		h248::Field reply;
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_1.termination_id, 1, h248::TerminationStreamMode::SendOnly, &reply));
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_2.termination_id, 1, h248::TerminationStreamMode::RecvOnly, &reply));

		ASSERT_TRUE(bothway->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_TRUE(receive_packet_out());
		ASSERT_FALSE(receive_packet_in());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_bothway_test_suite_t, stream_mode_sendonly_inactive_test)
	{
		rtx_test_context_bothway_enviroment_info* bothway = g_environment->get_context_bothway();
		ASSERT_TRUE(bothway->context->mg_context_lock());

		h248::Field reply;
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_1.termination_id, 1, h248::TerminationStreamMode::SendOnly, &reply));
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_2.termination_id, 1, h248::TerminationStreamMode::Inactive, &reply));

		ASSERT_TRUE(bothway->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_FALSE(receive_packet_out());
		ASSERT_FALSE(receive_packet_in());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_bothway_test_suite_t, stream_mode_inactive_inactive_test)
	{
		rtx_test_context_bothway_enviroment_info* bothway = g_environment->get_context_bothway();
		ASSERT_TRUE(bothway->context->mg_context_lock());

		h248::Field reply;
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_1.termination_id, 1, h248::TerminationStreamMode::Inactive, &reply));
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_2.termination_id, 1, h248::TerminationStreamMode::Inactive, &reply));

		ASSERT_TRUE(bothway->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_FALSE(receive_packet_out());
		ASSERT_FALSE(receive_packet_in());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_bothway_test_suite_t, stream_mode_inactive_receiveonly_test)
	{
		rtx_test_context_bothway_enviroment_info* bothway = g_environment->get_context_bothway();
		ASSERT_TRUE(bothway->context->mg_context_lock());

		h248::Field reply;
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_1.termination_id, 1, h248::TerminationStreamMode::Inactive, &reply));
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_2.termination_id, 1, h248::TerminationStreamMode::RecvOnly, &reply));

		ASSERT_TRUE(bothway->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_FALSE(receive_packet_out());
		ASSERT_FALSE(receive_packet_in());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_bothway_test_suite_t, stream_mode_inactive_sendonly_test)
	{
		rtx_test_context_bothway_enviroment_info* bothway = g_environment->get_context_bothway();
		ASSERT_TRUE(bothway->context->mg_context_lock());

		h248::Field reply;
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_1.termination_id, 1, h248::TerminationStreamMode::Inactive, &reply));
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_2.termination_id, 1, h248::TerminationStreamMode::SendOnly, &reply));

		ASSERT_TRUE(bothway->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_FALSE(receive_packet_out());
		ASSERT_FALSE(receive_packet_in());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_bothway_test_suite_t, stream_mode_inactive_sendreceive_test)
	{
		rtx_test_context_bothway_enviroment_info* bothway = g_environment->get_context_bothway();
		ASSERT_TRUE(bothway->context->mg_context_lock());

		h248::Field reply;
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_1.termination_id, 1, h248::TerminationStreamMode::Inactive, &reply));
		ASSERT_TRUE(bothway->context->mg_termination_set_LocalControl_Mode(bothway->termination_info_2.termination_id, 1, h248::TerminationStreamMode::SendRecv, &reply));

		ASSERT_TRUE(bothway->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_FALSE(receive_packet_out());
		ASSERT_FALSE(receive_packet_in());
	}
	//--------------------------------------------------------------------------
	//
	//--------------------------------------------------------------------------
	rtx_stream_mode_conference_test_suite_t::rtx_stream_mode_conference_test_suite_t()
	{

	}
	//--------------------------------------------------------------------------
	rtx_stream_mode_conference_test_suite_t::~rtx_stream_mode_conference_test_suite_t()
	{
		
	}
	//--------------------------------------------------------------------------
	void rtx_stream_mode_conference_test_suite_t::SetUp()
	{
		ASSERT_TRUE(nullptr != g_environment);
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		ASSERT_TRUE(nullptr != conference);
		ASSERT_TRUE(nullptr != conference->context);
		ASSERT_TRUE(nullptr != conference->termination_info_1.socket);
		ASSERT_TRUE(nullptr != conference->termination_info_2.socket);
		ASSERT_TRUE(nullptr != conference->termination_info_3.socket);

		clear_receiver(conference->termination_info_1.socket);
		clear_receiver(conference->termination_info_2.socket);
		clear_receiver(conference->termination_info_3.socket);
	}
	//--------------------------------------------------------------------------
	void rtx_stream_mode_conference_test_suite_t::TearDown()
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();

		clear_receiver(conference->termination_info_1.socket);
		clear_receiver(conference->termination_info_2.socket);
		clear_receiver(conference->termination_info_3.socket);

		ASSERT_TRUE(conference->context->mg_context_lock());

		h248::Field reply;
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_1.termination_id, 1, h248::TerminationStreamMode::SendRecv, &reply));
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_2.termination_id, 1, h248::TerminationStreamMode::SendRecv, &reply));

		ASSERT_TRUE(conference->context->mg_context_unlock());
	}
	//--------------------------------------------------------------------------
	void rtx_stream_mode_conference_test_suite_t::send_packet_in()
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		ASSERT_TRUE(nullptr != conference);
		ASSERT_TRUE(nullptr != conference->termination_info_1.socket);
		ASSERT_NE(0, conference->termination_info_1.port);

		send_packet(conference->termination_info_1.socket, conference->termination_info_1.port, conference->termination_info_1.seq, conference->termination_info_1.timestamp);
		conference->termination_info_1.seq++;
		conference->termination_info_1.timestamp += 160;
	}
	//--------------------------------------------------------------------------
	void rtx_stream_mode_conference_test_suite_t::send_packet_out()
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		ASSERT_TRUE(nullptr != conference);
		ASSERT_TRUE(nullptr != conference->termination_info_2.socket);
		ASSERT_NE(0, conference->termination_info_2.port);

		send_packet(conference->termination_info_2.socket, conference->termination_info_2.port, conference->termination_info_2.seq, conference->termination_info_2.timestamp);
		conference->termination_info_2.seq++;
		conference->termination_info_2.timestamp += 160;
	}
	//--------------------------------------------------------------------------
	bool rtx_stream_mode_conference_test_suite_t::receive_packet_out()
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		if (conference == nullptr || conference->termination_info_2.socket == nullptr)
		{
			return false;
		}

		return receive_packet(conference->termination_info_2.socket);
	}
	//--------------------------------------------------------------------------
	bool rtx_stream_mode_conference_test_suite_t::receive_packet_in()
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		if (conference == nullptr || conference->termination_info_1.socket == nullptr)
		{
			return false;
		}

		return receive_packet(conference->termination_info_1.socket);
	}
	//--------------------------------------------------------------------------
	//
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_conference_test_suite_t, stream_mode_sendreceive_receiveonly_test)
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		ASSERT_TRUE(conference->context->mg_context_lock());

		h248::Field reply;
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_1.termination_id, 1, h248::TerminationStreamMode::SendRecv, &reply));
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_2.termination_id, 1, h248::TerminationStreamMode::RecvOnly, &reply));

		ASSERT_TRUE(conference->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_TRUE(receive_packet_out());
		ASSERT_FALSE(receive_packet_in());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_conference_test_suite_t, stream_mode_sendreceive_sendonly_test)
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		ASSERT_TRUE(conference->context->mg_context_lock());

		h248::Field reply;
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_1.termination_id, 1, h248::TerminationStreamMode::SendRecv, &reply));
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_2.termination_id, 1, h248::TerminationStreamMode::SendOnly, &reply));

		ASSERT_TRUE(conference->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_FALSE(receive_packet_out());
		ASSERT_TRUE(receive_packet_in());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_conference_test_suite_t, stream_mode_sendreceive_inactive_test)
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		ASSERT_TRUE(conference->context->mg_context_lock());

		h248::Field reply;
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_1.termination_id, 1, h248::TerminationStreamMode::SendRecv, &reply));
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_2.termination_id, 1, h248::TerminationStreamMode::Inactive, &reply));

		ASSERT_TRUE(conference->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_FALSE(receive_packet_out());
		ASSERT_FALSE(receive_packet_in());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_conference_test_suite_t, stream_mode_receiveonly_receiveonly_test)
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		ASSERT_TRUE(conference->context->mg_context_lock());

		h248::Field reply;
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_1.termination_id, 1, h248::TerminationStreamMode::RecvOnly, &reply));
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_2.termination_id, 1, h248::TerminationStreamMode::RecvOnly, &reply));

		ASSERT_TRUE(conference->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_FALSE(receive_packet_out());
		ASSERT_FALSE(receive_packet_in());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_conference_test_suite_t, stream_mode_receiveonly_sendreceive_test)
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		ASSERT_TRUE(conference->context->mg_context_lock());

		h248::Field reply;
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_1.termination_id, 1, h248::TerminationStreamMode::RecvOnly, &reply));
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_2.termination_id, 1, h248::TerminationStreamMode::SendRecv, &reply));

		ASSERT_TRUE(conference->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_FALSE(receive_packet_out());
		ASSERT_TRUE(receive_packet_in());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_conference_test_suite_t, stream_mode_receiveonly_sendonly_test)
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		ASSERT_TRUE(conference->context->mg_context_lock());

		h248::Field reply;
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_1.termination_id, 1, h248::TerminationStreamMode::RecvOnly, &reply));
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_2.termination_id, 1, h248::TerminationStreamMode::SendOnly, &reply));

		ASSERT_TRUE(conference->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_FALSE(receive_packet_out());
		ASSERT_TRUE(receive_packet_in());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_conference_test_suite_t, stream_mode_receiveonly_inactive_test)
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		ASSERT_TRUE(conference->context->mg_context_lock());

		h248::Field reply;
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_1.termination_id, 1, h248::TerminationStreamMode::RecvOnly, &reply));
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_2.termination_id, 1, h248::TerminationStreamMode::Inactive, &reply));

		ASSERT_TRUE(conference->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_FALSE(receive_packet_out());
		ASSERT_FALSE(receive_packet_in());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_conference_test_suite_t, stream_mode_sendonly_sendonly_test)
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		ASSERT_TRUE(conference->context->mg_context_lock());

		h248::Field reply;
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_1.termination_id, 1, h248::TerminationStreamMode::SendOnly, &reply));
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_2.termination_id, 1, h248::TerminationStreamMode::SendOnly, &reply));

		ASSERT_TRUE(conference->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_FALSE(receive_packet_out());
		ASSERT_FALSE(receive_packet_in());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_conference_test_suite_t, stream_mode_sendonly_sendreceive_test)
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		ASSERT_TRUE(conference->context->mg_context_lock());

		h248::Field reply;
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_1.termination_id, 1, h248::TerminationStreamMode::SendOnly, &reply));
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_2.termination_id, 1, h248::TerminationStreamMode::SendRecv, &reply));

		ASSERT_TRUE(conference->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_TRUE(receive_packet_out());
		ASSERT_FALSE(receive_packet_in());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_conference_test_suite_t, stream_mode_sendonly_receiveonly_test)
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		ASSERT_TRUE(conference->context->mg_context_lock());

		h248::Field reply;
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_1.termination_id, 1, h248::TerminationStreamMode::SendOnly, &reply));
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_2.termination_id, 1, h248::TerminationStreamMode::RecvOnly, &reply));

		ASSERT_TRUE(conference->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_TRUE(receive_packet_out());
		ASSERT_FALSE(receive_packet_in());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_conference_test_suite_t, stream_mode_sendonly_inactive_test)
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		ASSERT_TRUE(conference->context->mg_context_lock());

		h248::Field reply;
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_1.termination_id, 1, h248::TerminationStreamMode::SendOnly, &reply));
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_2.termination_id, 1, h248::TerminationStreamMode::Inactive, &reply));

		ASSERT_TRUE(conference->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_FALSE(receive_packet_out());
		ASSERT_FALSE(receive_packet_in());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_conference_test_suite_t, stream_mode_inactive_inactive_test)
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		ASSERT_TRUE(conference->context->mg_context_lock());

		h248::Field reply;
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_1.termination_id, 1, h248::TerminationStreamMode::Inactive, &reply));
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_2.termination_id, 1, h248::TerminationStreamMode::Inactive, &reply));

		ASSERT_TRUE(conference->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_FALSE(receive_packet_out());
		ASSERT_FALSE(receive_packet_in());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_conference_test_suite_t, stream_mode_inactive_receiveonly_test)
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		ASSERT_TRUE(conference->context->mg_context_lock());

		h248::Field reply;
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_1.termination_id, 1, h248::TerminationStreamMode::Inactive, &reply));
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_2.termination_id, 1, h248::TerminationStreamMode::RecvOnly, &reply));

		ASSERT_TRUE(conference->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_FALSE(receive_packet_out());
		ASSERT_FALSE(receive_packet_in());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_conference_test_suite_t, stream_mode_inactive_sendonly_test)
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		ASSERT_TRUE(conference->context->mg_context_lock());

		h248::Field reply;
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_1.termination_id, 1, h248::TerminationStreamMode::Inactive, &reply));
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_2.termination_id, 1, h248::TerminationStreamMode::SendOnly, &reply));

		ASSERT_TRUE(conference->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_FALSE(receive_packet_out());
		ASSERT_FALSE(receive_packet_in());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_stream_mode_conference_test_suite_t, stream_mode_inactive_sendreceive_test)
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		ASSERT_TRUE(conference->context->mg_context_lock());

		h248::Field reply;
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_1.termination_id, 1, h248::TerminationStreamMode::Inactive, &reply));
		ASSERT_TRUE(conference->context->mg_termination_set_LocalControl_Mode(conference->termination_info_2.termination_id, 1, h248::TerminationStreamMode::SendRecv, &reply));

		ASSERT_TRUE(conference->context->mg_context_unlock());

		send_packet_in();
		send_packet_out();

		ASSERT_FALSE(receive_packet_out());
		ASSERT_FALSE(receive_packet_in());
	}
}
//------------------------------------------------------------------------------
