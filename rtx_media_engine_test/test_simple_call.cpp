﻿/* RTX H.248 Media Gate Engine Test Module 
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "test_simple_call.h"
#include "net/net_socket.h"
#include "rtp_packet.h"
#include "mock_callbacks.h"
#include "test_utils.h"

#include "gtest/gtest.h"
#include "gmock/gmock.h"

namespace rtx_media_engine
{
	//--------------------------------------------------------------------------
	TEST(rtx_media_engine_test, try_make_simple_call)
	{
		ASSERT_TRUE(nullptr == g_environment) << "Simple call must be started before creating environment.";

		//------------------------------------------------------------------------------
		// prepare network environment
		net_socket socket_sender(nullptr);
		net_socket socket_receiver(nullptr);
		rtl::ArrayT<net_socket*> list;
		list.add(&socket_sender);
		list.add(&socket_receiver);

		ip_address_t ipaddr(g_test_interface);

		ASSERT_TRUE(open_tests_sockets(&list, &ipaddr));

		EXPECT_NE(0, socket_sender.get_local_address()->get_port());
		EXPECT_NE(0, socket_receiver.get_local_address()->get_port());

		//------------------------------------------------------------------------------
		// prepare context

		//rtl::Logger::set_trace_flags("CALL EVENTS ERROR WARNING PROTO TRANS SESSION MEDIA-FLOW NET RTP RTP-FLOW FLAG8 FLAG7 STAT");
		mock_module_callback_interface_t module_callback;
		h248::IMediaGate* module = create_media_gateway_module(g_test_log_path);
		ASSERT_TRUE(nullptr != module);

		EXPECT_CALL(module_callback, mge__event_raised()).Times(0);
		EXPECT_CALL(module_callback, mge__error_raised()).Times(0);

		EXPECT_TRUE(module->MediaGate_initialize(&module_callback));

		module->MediaGate_setInterfaceName(g_test_interface, "test");

		ASSERT_TRUE(module->MediaGate_addPortRangeRTP("test", 10000, 10000));

		h248::IMediaContext* context = module->MediaGate_createContext();
		ASSERT_TRUE(nullptr != context);

		mock_termination_event_handler_t termination_callback;
		EXPECT_CALL(termination_callback, TerminationEvent_callback(::testing::_)).Times(0);
		
		h248::Field reply;
		ASSERT_TRUE(context->mg_context_initialize(&termination_callback, &reply));

		ASSERT_TRUE(context->mg_context_lock()) << "first lock";

		h248::TerminationID termId(h248::TerminationType::RTP, "test", 1);
		uint32_t termination_id_1 = context->mg_context_add_termination(termId, &reply);
		ASSERT_EQ(1, termination_id_1);
		
		ASSERT_TRUE(context->mg_termination_set_LocalControl_Mode(termination_id_1, 1, h248::TerminationStreamMode::SendRecv, &reply));

		rtl::String sdp_local_1;
		sdp_local_1 << "v=0" << "\r\n";
		sdp_local_1 << "c=IN IP4 $" << "\r\n";
		sdp_local_1 << "m=audio $ RTP/AVP 0" << "\r\n";
		sdp_local_1 << "a=sendrecv" << "\r\n";

		ASSERT_TRUE(context->mg_termination_set_Local_Remote(termination_id_1, 1, rtl::String::empty, sdp_local_1, &reply));

		ASSERT_TRUE(context->mg_termination_get_Local(termination_id_1, 1, sdp_local_1, &reply));

		termId.setId(2);
		uint32_t termination_id_2 = context->mg_context_add_termination(termId, &reply);
		ASSERT_EQ(2, termination_id_2);

		ASSERT_TRUE(context->mg_termination_set_LocalControl_Mode(termination_id_2, 1, h248::TerminationStreamMode::SendRecv, &reply));

		rtl::String sdp_local_2;
		sdp_local_2 << "v=0" << "\r\n";
		sdp_local_2 << "c=IN IP4 $" << "\r\n";
		sdp_local_2 << "m=audio $ RTP/AVP 0" << "\r\n";
		sdp_local_2 << "a=sendrecv" << "\r\n";

		rtl::String sdp_remote_2;
		sdp_remote_2 << "v=0" << "\r\n";
		sdp_remote_2 << "c=IN IP4 " << g_test_interface << "\r\n";
		sdp_remote_2 << "m=audio " << socket_sender.get_local_address()->get_port() << " RTP/AVP 0" << "\r\n";
		sdp_remote_2 << "a=sendrecv" << "\r\n";

		ASSERT_TRUE(context->mg_termination_set_Local_Remote(termination_id_2, 1, sdp_remote_2, sdp_local_2, &reply));

		ASSERT_TRUE(context->mg_termination_get_Local(termination_id_2, 1, sdp_local_2, &reply));

		rtl::String sdp_remote_1;
		sdp_remote_1 << "v=0" << "\r\n";
		sdp_remote_1 << "c=IN IP4 " << g_test_interface << "\r\n";
		sdp_remote_1 << "m=audio " << socket_receiver.get_local_address()->get_port() << " RTP/AVP 0" << "\r\n";
		sdp_remote_1 << "a=sendrecv" << "\r\n";

		ASSERT_TRUE(context->mg_termination_set_Local_Remote(termination_id_1, 1, sdp_remote_1, rtl::String::empty, &reply));

		ASSERT_TRUE(context->mg_context_unlock()) << "first unlock";

		//------------------------------------------------------------------------------
		// send test packet

		int index_A = sdp_local_2.indexOf("m=audio ") + 8;
		int index_B = sdp_local_2.indexOf(" ", index_A);
		rtl::String port_str = sdp_local_2.substring(index_A, index_B - index_A);
		uint16_t port_send_to = (uint16_t)strtoul(port_str, nullptr, 10);
		socket_address sockaddr_send_to(&ipaddr, port_send_to);

		uint8_t buff[5] = { "TEST" };
		rtp_packet packet_send;
		packet_send.set_payload(buff, 5);
		packet_send.set_sequence_number(1);

		uint8_t buff_packet_send[1024] = { 0 };
		uint32_t buff_packet_write = packet_send.write_packet(buff_packet_send, 1024);

		EXPECT_EQ(buff_packet_write, socket_sender.send_to(buff_packet_send, buff_packet_write, &sockaddr_send_to));

		//------------------------------------------------------------------------------
		// receive test packet

		socket_vector_t read;
		read.add(&socket_receiver);
		rtp_packet packet_receive;

		int count = 10;
		while (count > 0)
		{
			EXPECT_TRUE(net_socket::select(&read, NULL, NULL, 20, NULL));

			if (read.getCount() == 0)
			{
				rtl::Thread::sleep(100);
				read.add(&socket_receiver);
				count--;
			}
			else
			{
				socket_address sockaddr_from_to;
				uint8_t buff_packet_receive[1024] = { 0 };
				uint32_t read_size = read.getAt(0)->recv_from(buff_packet_receive, 1024, &sockaddr_from_to);
				EXPECT_NE(0, read_size);
				EXPECT_TRUE(packet_receive.read_packet(buff_packet_receive, read_size));
				break;
			}
		}

		EXPECT_NE(0, packet_receive.get_payload_length());
		EXPECT_EQ(0, memcmp(packet_receive.get_payload(), buff, packet_receive.get_payload_length()));

		ASSERT_TRUE(context->mg_context_lock()) << "second lock";

		context->mg_context_remove_termination(termination_id_1);
		context->mg_context_remove_termination(termination_id_2);

		ASSERT_TRUE(context->mg_context_unlock()) << "second unlock";

		close_tests_sockets(&list);

		module->MediaGate_destroyContext(context);
		remove_media_gateway();
	}
	//--------------------------------------------------------------------------
}
//------------------------------------------------------------------------------
