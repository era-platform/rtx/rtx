/* RTX H.248 Media Gate Engine Test Module 
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "test_utils.h"
#include "../rtx_media_engine/mg_codec_to_img_tube.h"
#include "../rtx_media_engine/mg_img_to_codec_tube.h"
#include "../rtx_media_engine/mg_transformer_tube.h"
#include "../rtx_media_engine/mg_media_element.h"
#include "rtp_packet.h"

 //-----------------------------------------------
//
//-----------------------------------------------
namespace rtx_media_engine
{
	class video_term_handler : public mge::INextDataHandler
	{
		const uint8_t* m_ref_vec;
		int m_ref_vec_size;
		int m_ref_vec_offset;
		rtl::FileStream writer;

	public:
		video_term_handler(const char* fileout, const uint8_t* ref_vec, int size)
		{
			m_ref_vec = ref_vec;
			m_ref_vec_size = size;
			m_ref_vec_offset = 0;
			if (fileout != nullptr)
			{
				rtl::String fpath = get_path_to_tests_data() + FS_PATH_DELIMITER + fileout;
				writer.createNew(fpath);
			}
		}
		~video_term_handler()
		{
			writer.close();
		}

		virtual void send(const mge::INextDataHandler* out_handler, const rtp_packet* packet);
		virtual void send(const mge::INextDataHandler* out_handler, const uint8_t* data, uint32_t len);
		virtual void send(const mge::INextDataHandler* out_handler, const rtcp_packet_t* packet);
		virtual bool unsubscribe(const mge::INextDataHandler* out_handler);
	};

	void video_term_handler::send(const mge::INextDataHandler* out_handler, const rtp_packet* packet)
	{
		const uint8_t *pdata = packet->get_payload();
		int psize = packet->get_payload_length();

		if (writer.isOpened())
		{
			writer.write(pdata, psize);
		}

		const uint8_t *rdata = m_ref_vec + m_ref_vec_offset;

		if (rdata != nullptr)
		{
			int res = memcmp(pdata, rdata, psize);
			ASSERT_EQ(res, 0);
			m_ref_vec_offset += psize;
			ASSERT_LE(m_ref_vec_offset, m_ref_vec_size);
		}
	}
	void video_term_handler::send(const mge::INextDataHandler* out_handler, const uint8_t* data, uint32_t len)
	{
	}
	void video_term_handler::send(const mge::INextDataHandler* out_handler, const rtcp_packet_t* packet)
	{
	}
	bool video_term_handler::unsubscribe(const mge::INextDataHandler* out_handler)
	{
		return false;
	}

	static void video_transcode_test(media::PayloadFormat* src_fmt, media::PayloadFormat* dst_fmt)
	{
		if (src_fmt->isEqual(*dst_fmt))
			return;

		/*
		
		
		
		
		*/

		// 
		uint8_t* src_data = nullptr;
		int src_data_len = 0;
		char vector_name[128];

		std_snprintf(vector_name, 128, "%s_source.vrtp", (const char*)src_fmt->getEncoding());

		if (!load_test_vector(vector_name, &src_data, &src_data_len))
		{
			ERRORF("video_encode_test....invalid input vector '%s'\n", vector_name);
			return;
		}

		uint32_t sign = *(uint32_t*)src_data;

		if (sign != 0x80011008)
		{
			ERRORF("video_encode_test....invalid input vector sign 0x%X\n", sign);
			FREE(src_data);
			return;
		}

		uint8_t* dst_data = nullptr;
		int dst_data_len = 0;

		std_snprintf(vector_name, 128, "%s_sample.raw", (const char*)dst_fmt->getEncoding());

		if (!load_test_vector(vector_name, &dst_data, &dst_data_len))
		{
			PRINTF("video_encode_test....sample vector '%s' not found -- generating\n", vector_name);
		}

		mge::mg_media_element_t el_in("at-0-src", src_fmt->getId_u8(), nullptr);
		mge::mg_media_element_t el_out("at-0-dst", dst_fmt->getId_u8(), nullptr);

		el_in.set_element_name(src_fmt->getEncoding());
		el_in.set_sample_rate(src_fmt->getFrequency());
		el_in.set_sample_rate(src_fmt->getFrequency());

		el_out.set_element_name(dst_fmt->getEncoding());
		el_out.set_sample_rate(dst_fmt->getFrequency());
		el_out.set_sample_rate(dst_fmt->getFrequency());

		mge::mg_codec_to_img_tube_t* src_tube = NEW mge::mg_codec_to_img_tube_t(&Log, 1, "at-0");
		mge::mg_img_to_codec_tube_t* dst_tube = NEW mge::mg_img_to_codec_tube_t(&Log, 2, "at-0");
		mge::mg_transformer_tube_t* resampler = nullptr;

		std_snprintf(vector_name, 128, "%s_res.raw", (const char*)dst_fmt->getEncoding());

		video_term_handler term(vector_name, dst_data, dst_data_len);

		src_tube->init(&el_in, &el_out);
		dst_tube->init(&el_in, &el_out);

		src_tube->setNextHandler(dst_tube);
		dst_tube->setNextHandler(&term);

		int frame_size = *(uint32_t*)(src_data + 4);
		int src_data_offset = 8;

		while (src_data_offset + frame_size <= src_data_len)
		{
			rtp_packet packet;
			packet.read_packet(src_data + src_data_offset, frame_size);
			src_tube->send(nullptr, &packet);

			if (src_data_offset + frame_size < src_data_len)
			{
				src_data_offset += frame_size;
				uint32_t sign = *(uint32_t*)(src_data + src_data_offset);

				if (sign != 0x80011008)
				{
					ERRORF("video_encode_test....invalid input vector sign 0x%X : offset 0x%X frame 0x%X\n", sign, src_data_offset, frame_size);
					FREE(src_data);
					return;
				}
				src_data_offset += 4;
				frame_size = *(uint32_t*)(src_data + src_data_offset);
				src_data_offset += 4;
			}
			else
			{
				break;
			}
		}

		FREE(src_data);
		FREE(dst_data);
	}

	/*TEST(test_vide_transcode, H263_H264)
	{
		PRINTF("test H263 ==> H264...\n");
		PayloadFormat fmt1 = { "H263", 34, 90000, nullptr };
		PayloadFormat fmt2 = { "H264", 96, 90000, nullptr};

		video_transcode_test(&fmt1, &fmt2);
	}
	TEST(test_audio_transcode, H264_H263)
	{
		PRINTF("test H264 ==> H263...\n");
		PayloadFormat fmt1 = { "H263", 34, 90000, nullptr };
		PayloadFormat fmt2 = { "H264", 96, 90000, nullptr };

		video_transcode_test(&fmt2, &fmt1);
	}*/

}
