﻿/* RTX H.248 Media Gate Engine Test Module 
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "test_rtx_media_engine.h"

 //------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
extern char* g_test_interface = nullptr;
extern char* g_test_log_path = nullptr;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "gtest/gtest.h"
#include "gmock/gmock.h"
namespace rtx_media_engine
{
	void clear_termination_info(rtx_test_termination_enviroment_info* info)
	{
		if (info == nullptr)
		{
			return;
		}

		if (info->socket != nullptr)
		{
			rtl::ArrayT<net_socket*> list;
			list.add(info->socket);
			close_tests_sockets(&list);
			DELETEO(info->socket);
			info->socket = nullptr;
		}
		
		info->port = 0;
		info->seq = 1;
		info->termination_id = 0;
		info->context_index = -1;
		info->timestamp = 10000000;
	}
	//--------------------------------------------------------------------------
	void send_packet(net_socket* socket, uint16_t port, uint16_t s, uint32_t t)
	{
		ip_address_t ipaddr(g_test_interface);
		socket_address sockaddr_send_to(&ipaddr, port);

		uint8_t buff[160] = { "TESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTES" };

		rtp_packet packet_send;
		packet_send.set_payload(buff, 160);
		packet_send.set_sequence_number(s);
		packet_send.set_payload_type(0);
		packet_send.set_timestamp(t);

		uint8_t buff_packet_send[1024] = { 0 };
		uint32_t buff_packet_write = packet_send.write_packet(buff_packet_send, 1024);

		EXPECT_EQ(buff_packet_write, socket->send_to(buff_packet_send, buff_packet_write, &sockaddr_send_to));
	}
	//--------------------------------------------------------------------------
	bool receive_packet(net_socket* socket)
	{
		socket_vector_t read;
		read.add(socket);

		int count = 5;
		while (count > 0)
		{
			EXPECT_TRUE(net_socket::select(&read, NULL, NULL, 20, NULL));

			if (read.getCount() == 0)
			{
				rtl::Thread::sleep(50);
				read.add(socket);
				count--;
			}
			else
			{
				socket_address sockaddr_from_to;
				uint8_t buff_packet_receive[1024] = { 0 };
				uint32_t read_size = read.getAt(0)->recv_from(buff_packet_receive, 1024, &sockaddr_from_to);
				return read_size != 0;
			}
		}

		return false;
	}
	//--------------------------------------------------------------------------
	void clear_receiver(net_socket* socket)
	{
		socket_vector_t read;
		read.add(socket);

		int count = 5;
		while (count > 0)
		{
			EXPECT_TRUE(net_socket::select(&read, NULL, NULL, 20, NULL));
			if (read.getCount() == 0)
			{
				rtl::Thread::sleep(50);
				read.add(socket);
				count--;
			}
			else
			{
				socket_address sockaddr_from_to;
				uint8_t buff_packet_receive[1024] = { 0 };
				read.getAt(0)->recv_from(buff_packet_receive, 1024, &sockaddr_from_to);
			}
		}
	}
	//--------------------------------------------------------------------------
	//
	//--------------------------------------------------------------------------
	rtx_test_environment::rtx_test_environment()
	{
		m_module = nullptr;
		m_module_callback = nullptr;

		m_bothway.context = nullptr;
		m_bothway.termination_callback = nullptr;
		m_bothway.termination_info_1.socket = nullptr;
		m_bothway.termination_info_2.socket = nullptr;
		clear_termination_info(&m_bothway.termination_info_1);
		clear_termination_info(&m_bothway.termination_info_2);

		m_conference.context = nullptr;
		m_conference.termination_callback = nullptr;
		m_conference.termination_info_1.socket = nullptr;
		m_conference.termination_info_2.socket = nullptr;
		m_conference.termination_info_3.socket = nullptr;
		clear_termination_info(&m_conference.termination_info_1);
		clear_termination_info(&m_conference.termination_info_2);
		clear_termination_info(&m_conference.termination_info_3);
	}
	//--------------------------------------------------------------------------
	rtx_test_environment::~rtx_test_environment()
	{
	}
	//--------------------------------------------------------------------------
	void rtx_test_environment::SetUp()
	{
		m_bothway.termination_info_1.socket = NEW net_socket(nullptr);
		m_bothway.termination_info_2.socket = NEW net_socket(nullptr);
		m_conference.termination_info_1.socket = NEW net_socket(nullptr);
		m_conference.termination_info_2.socket = NEW net_socket(nullptr);
		m_conference.termination_info_3.socket = NEW net_socket(nullptr);
		
		rtl::ArrayT<net_socket*> list;
		list.add(m_bothway.termination_info_1.socket);
		list.add(m_bothway.termination_info_2.socket);
		list.add(m_conference.termination_info_1.socket);
		list.add(m_conference.termination_info_2.socket);
		list.add(m_conference.termination_info_3.socket);

		ip_address_t ipaddr(g_test_interface);

		EXPECT_TRUE(open_tests_sockets(&list, &ipaddr));

		EXPECT_NE(0, m_bothway.termination_info_1.socket->get_local_address()->get_port());
		EXPECT_NE(0, m_bothway.termination_info_2.socket->get_local_address()->get_port());
		EXPECT_NE(0, m_conference.termination_info_1.socket->get_local_address()->get_port());
		EXPECT_NE(0, m_conference.termination_info_2.socket->get_local_address()->get_port());
		EXPECT_NE(0, m_conference.termination_info_3.socket->get_local_address()->get_port());

		//rtl::Logger::set_trace_flags("CALL EVENTS ERROR WARNING PROTO TRANS SESSION MEDIA-FLOW NET RTP RTP-FLOW FLAG8 FLAG7 STAT");

		Config.set_config_value("mg-dtmf", "1");

		rtl::async_call_manager_t::createAsyncCall(2, 16);

		//----------------------------------------------------------------------
		m_module = create_media_gateway_module(g_test_log_path);
		if (m_module_callback == nullptr)
		{
			m_module_callback = NEW mock_module_callback_interface_t();
		}
		EXPECT_CALL(*m_module_callback, mge__event_raised()).Times(0);
		EXPECT_CALL(*m_module_callback, mge__error_raised()).Times(0);

		EXPECT_TRUE(m_module->MediaGate_initialize(m_module_callback));
		m_module->MediaGate_setInterfaceName(g_test_interface, "test");
		m_module->MediaGate_addPortRangeRTP("test", 10000, 10000);

		//----------------------------------------------------------------------
		m_bothway.context = m_module->MediaGate_createContext();
		ASSERT_TRUE(nullptr != m_bothway.context);
		if (m_bothway.termination_callback == nullptr)
		{
			m_bothway.termination_callback = NEW mock_termination_event_handler_t();
		}
		EXPECT_CALL(*m_bothway.termination_callback, TerminationEvent_callback(::testing::_)).Times(::testing::AnyNumber());

		h248::Field reply;
		ASSERT_TRUE(m_bothway.context->mg_context_initialize(m_bothway.termination_callback, &reply));
		ASSERT_TRUE(m_bothway.context->mg_context_lock());
		h248::TerminationID termId(h248::TerminationType::RTP, "test", 1);
		m_bothway.termination_info_1.termination_id = m_bothway.context->mg_context_add_termination(termId, &reply);
		ASSERT_EQ(1, m_bothway.termination_info_1.termination_id);

		rtl::String sdp_local_1;
		sdp_local_1 << "v=0" << "\r\n";
		sdp_local_1 << "c=IN IP4 $" << "\r\n";
		sdp_local_1 << "m=audio $ RTP/AVP 0 101" << "\r\n";
		sdp_local_1 << "a=rtpmap:101 telephone-event/8000" << "\r\n";
		sdp_local_1 << "a=sendrecv" << "\r\n";

		ASSERT_TRUE(m_bothway.context->mg_termination_set_Local_Remote(m_bothway.termination_info_1.termination_id, 1, rtl::String::empty, sdp_local_1, &reply));
		ASSERT_TRUE(m_bothway.context->mg_termination_get_Local(m_bothway.termination_info_1.termination_id, 1, sdp_local_1, &reply));

		termId.setId(2);

		m_bothway.termination_info_2.termination_id = m_bothway.context->mg_context_add_termination(termId, &reply);
		ASSERT_EQ(2, m_bothway.termination_info_2.termination_id);

		rtl::String sdp_local_2;
		sdp_local_2 << "v=0" << "\r\n";
		sdp_local_2 << "c=IN IP4 $" << "\r\n";
		sdp_local_2 << "m=audio $ RTP/AVP 0 101" << "\r\n";
		sdp_local_2 << "a=rtpmap:101 telephone-event/8000" << "\r\n";
		sdp_local_2 << "a=sendrecv" << "\r\n";

		rtl::String sdp_remote_2;
		sdp_remote_2 << "v=0" << "\r\n";
		sdp_remote_2 << "c=IN IP4 " << g_test_interface << "\r\n";
		sdp_remote_2 << "m=audio " << m_bothway.termination_info_2.socket->get_local_address()->get_port() << " RTP/AVP 0 101" << "\r\n";
		sdp_remote_2 << "a=rtpmap:101 telephone-event/8000" << "\r\n";
		sdp_remote_2 << "a=sendrecv" << "\r\n";

		ASSERT_TRUE(m_bothway.context->mg_termination_set_Local_Remote(m_bothway.termination_info_2.termination_id, 1, sdp_remote_2, sdp_local_2, &reply));

		ASSERT_TRUE(m_bothway.context->mg_termination_get_Local(m_bothway.termination_info_2.termination_id, 1, sdp_local_2, &reply));

		int index_A = sdp_local_1.indexOf("m=audio ") + 8;
		int index_B = sdp_local_1.indexOf(" ", index_A);
		rtl::String port_str = sdp_local_1.substring(index_A, index_B - index_A);
		m_bothway.termination_info_1.port = (uint16_t)strtoul(port_str, nullptr, 10);

		index_A = sdp_local_2.indexOf("m=audio ") + 8;
		index_B = sdp_local_2.indexOf(" ", index_A);
		port_str = sdp_local_2.substring(index_A, index_B - index_A);
		m_bothway.termination_info_2.port = (uint16_t)strtoul(port_str, nullptr, 10);

		rtl::String sdp_remote_1;
		sdp_remote_1 << "v=0" << "\r\n";
		sdp_remote_1 << "c=IN IP4 " << g_test_interface << "\r\n";
		sdp_remote_1 << "m=audio " << m_bothway.termination_info_1.socket->get_local_address()->get_port() << " RTP/AVP 0 101" << "\r\n";
		sdp_remote_1 << "a=rtpmap:101 telephone-event/8000" << "\r\n";
		sdp_remote_1 << "a=sendrecv" << "\r\n";

		ASSERT_TRUE(m_bothway.context->mg_termination_set_Local_Remote(m_bothway.termination_info_1.termination_id, 1, sdp_remote_1, rtl::String::empty, &reply));

		m_bothway.context->mg_termination_set_LocalControl_Mode(m_bothway.termination_info_1.termination_id, 1, h248::TerminationStreamMode::SendRecv, &reply);
		m_bothway.context->mg_termination_set_LocalControl_Mode(m_bothway.termination_info_2.termination_id, 1, h248::TerminationStreamMode::SendRecv, &reply);

		ASSERT_TRUE(m_bothway.context->mg_context_unlock());

		//----------------------------------------------------------------------
		m_conference.context = m_module->MediaGate_createContext();
		ASSERT_TRUE(nullptr != m_conference.context);
		if (m_conference.termination_callback == nullptr)
		{
			m_conference.termination_callback = NEW mock_termination_event_handler_t();
		}
		EXPECT_CALL(*m_conference.termination_callback, TerminationEvent_callback(::testing::_)).Times(::testing::AnyNumber());

		ASSERT_TRUE(m_conference.context->mg_context_initialize(m_conference.termination_callback, &reply));
		ASSERT_TRUE(m_conference.context->mg_context_lock());

		termId.setId(1);
		m_conference.termination_info_1.termination_id = m_conference.context->mg_context_add_termination(termId, &reply);
		ASSERT_EQ(1, m_conference.termination_info_1.termination_id);

		rtl::String sdp_conf_local_1;
		sdp_conf_local_1 << "v=0" << "\r\n";
		sdp_conf_local_1 << "c=IN IP4 $" << "\r\n";
		sdp_conf_local_1 << "m=audio $ RTP/AVP 0 101" << "\r\n";
		sdp_conf_local_1 << "a=rtpmap:101 telephone-event/8000" << "\r\n";
		sdp_conf_local_1 << "a=sendrecv" << "\r\n";

		ASSERT_TRUE(m_conference.context->mg_termination_set_Local_Remote(m_conference.termination_info_1.termination_id, 1, rtl::String::empty , sdp_conf_local_1, &reply));
		ASSERT_TRUE(m_conference.context->mg_termination_get_Local(m_conference.termination_info_1.termination_id, 1, sdp_conf_local_1, &reply));

		termId.setId(2);
		m_conference.termination_info_2.termination_id = m_conference.context->mg_context_add_termination(termId, &reply);
		ASSERT_EQ(2, m_conference.termination_info_2.termination_id);
		
		rtl::String sdp_conf_local_2;
		sdp_conf_local_2 << "v=0" << "\r\n";
		sdp_conf_local_2 << "c=IN IP4 $" << "\r\n";
		sdp_conf_local_2 << "m=audio $ RTP/AVP 0 101" << "\r\n";
		sdp_conf_local_2 << "a=rtpmap:101 telephone-event/8000" << "\r\n";
		sdp_conf_local_2 << "a=sendrecv" << "\r\n";

		rtl::String sdp_cong_remote_2;
		sdp_cong_remote_2 << "v=0" << "\r\n";
		sdp_cong_remote_2 << "c=IN IP4 " << g_test_interface << "\r\n";
		sdp_cong_remote_2 << "m=audio " << m_conference.termination_info_2.socket->get_local_address()->get_port() << " RTP/AVP 0 101" << "\r\n";
		sdp_cong_remote_2 << "a=rtpmap:101 telephone-event/8000" << "\r\n";
		sdp_cong_remote_2 << "a=sendrecv" << "\r\n";

		ASSERT_TRUE(m_conference.context->mg_termination_set_Local_Remote(m_conference.termination_info_2.termination_id, 1, sdp_cong_remote_2, sdp_conf_local_2, &reply));
		ASSERT_TRUE(m_conference.context->mg_termination_get_Local(m_conference.termination_info_2.termination_id, 1, sdp_conf_local_2, &reply));
		
		index_A = sdp_conf_local_1.indexOf("m=audio ") + 8;
		index_B = sdp_conf_local_1.indexOf(" ", index_A);
		port_str = sdp_conf_local_1.substring(index_A, index_B - index_A);
		m_conference.termination_info_1.port = (uint16_t)strtoul(port_str, nullptr, 10);

		index_A = sdp_conf_local_2.indexOf("m=audio ") + 8;
		index_B = sdp_conf_local_2.indexOf(" ", index_A);
		port_str = sdp_conf_local_2.substring(index_A, index_B - index_A);
		m_conference.termination_info_2.port = (uint16_t)strtoul(port_str, nullptr, 10);

		rtl::String sdp_conf_remote_1;
		sdp_conf_remote_1 << "v=0" << "\r\n";
		sdp_conf_remote_1 << "c=IN IP4 " << g_test_interface << "\r\n";
		sdp_conf_remote_1 << "m=audio " << m_conference.termination_info_1.socket->get_local_address()->get_port() << " RTP/AVP 0 101" << "\r\n";
		sdp_conf_remote_1 << "a=rtpmap:101 telephone-event/8000" << "\r\n";
		sdp_conf_remote_1 << "a=sendrecv" << "\r\n";

		ASSERT_TRUE(m_conference.context->mg_termination_set_Local_Remote(m_conference.termination_info_1.termination_id, 1, sdp_conf_remote_1, rtl::String::empty, &reply));
		rtl::String sdp_conf_local_3;
		sdp_conf_local_3 << "v=0" << "\r\n";
		sdp_conf_local_3 << "c=IN IP4 $" << "\r\n";
		sdp_conf_local_3 << "m=audio $ RTP/AVP 0 101" << "\r\n";
		sdp_conf_local_3 << "a=rtpmap:101 telephone-event/8000" << "\r\n";
		sdp_conf_local_3 << "a=sendrecv" << "\r\n";

		rtl::String sdp_conf_remote_3;
		sdp_conf_remote_3 << "v=0" << "\r\n";
		sdp_conf_remote_3 << "c=IN IP4 " << g_test_interface << "\r\n";
		sdp_conf_remote_3 << "m=audio " << m_conference.termination_info_3.socket->get_local_address()->get_port() << " RTP/AVP 0 101" << "\r\n";
		sdp_conf_remote_3 << "a=rtpmap:101 telephone-event/8000" << "\r\n";
		sdp_conf_remote_3 << "a=sendrecv" << "\r\n";

		termId.setId(3);
		m_conference.termination_info_3.termination_id = m_conference.context->mg_context_add_termination(termId, &reply);
		ASSERT_EQ(3, m_conference.termination_info_3.termination_id);
		ASSERT_TRUE(m_conference.context->mg_termination_set_Local_Remote(m_conference.termination_info_3.termination_id, 1, sdp_conf_remote_3, sdp_conf_local_3, &reply));
		ASSERT_TRUE(m_conference.context->mg_termination_get_Local(m_conference.termination_info_3.termination_id, 1, sdp_conf_local_3, &reply));
		index_A = sdp_conf_local_3.indexOf("m=audio ") + 8;
		index_B = sdp_conf_local_3.indexOf(" ", index_A);
		port_str = sdp_conf_local_3.substring(index_A, index_B - index_A);
		m_conference.termination_info_3.port = (uint16_t)strtoul(port_str, nullptr, 10);
		ASSERT_TRUE(m_conference.context->mg_termination_set_LocalControl_Mode(m_conference.termination_info_1.termination_id, 1, h248::TerminationStreamMode::SendRecv, &reply));
		ASSERT_TRUE(m_conference.context->mg_termination_set_LocalControl_Mode(m_conference.termination_info_2.termination_id, 1, h248::TerminationStreamMode::SendRecv, &reply));
		ASSERT_TRUE(m_conference.context->mg_termination_set_LocalControl_Mode(m_conference.termination_info_3.termination_id, 1, h248::TerminationStreamMode::SendRecv, &reply));
		ASSERT_TRUE(m_conference.context->mg_context_unlock());
	}
	//--------------------------------------------------------------------------
	void rtx_test_environment::TearDown()
	{
		if (m_bothway.context == nullptr)
		{
			clear_termination_info(&m_bothway.termination_info_1);
			clear_termination_info(&m_bothway.termination_info_2);
		}
		else
		{
			ASSERT_TRUE(m_bothway.context->mg_context_lock()) << "TearDown lock bothway";
			m_bothway.context->mg_context_remove_termination(m_bothway.termination_info_1.termination_id);
			m_bothway.context->mg_context_remove_termination(m_bothway.termination_info_2.termination_id);
			ASSERT_TRUE(m_bothway.context->mg_context_unlock()) << "TearDown unlock bothway";
			if (m_module != nullptr)
			{
				m_module->MediaGate_destroyContext(m_bothway.context);
			}
			m_bothway.context = nullptr;
		}
		if (m_bothway.termination_callback != nullptr)
		{
			EXPECT_TRUE(::testing::Mock::VerifyAndClearExpectations(m_bothway.termination_callback));
			DELETEO(m_bothway.termination_callback);
			m_bothway.termination_callback = nullptr;
		}

		if (m_conference.context == nullptr)
		{
			clear_termination_info(&m_conference.termination_info_1);
			clear_termination_info(&m_conference.termination_info_2);
			clear_termination_info(&m_conference.termination_info_3);
		}
		else
		{
			ASSERT_TRUE(m_conference.context->mg_context_lock()) << "TearDown lock conference";
			m_conference.context->mg_context_remove_termination(m_conference.termination_info_1.termination_id);
			m_conference.context->mg_context_remove_termination(m_conference.termination_info_2.termination_id);
			m_conference.context->mg_context_remove_termination(m_conference.termination_info_3.termination_id);
			ASSERT_TRUE(m_conference.context->mg_context_unlock()) << "TearDown unlock conference";
			if (m_module != nullptr)
			{
				m_module->MediaGate_destroyContext(m_conference.context);
			}
			m_conference.context = nullptr;
		}
		if (m_conference.termination_callback != nullptr)
		{
			EXPECT_TRUE(::testing::Mock::VerifyAndClearExpectations(m_conference.termination_callback));
			DELETEO(m_conference.termination_callback);
			m_conference.termination_callback = nullptr;
		}

		if (m_module != nullptr)
		{
			remove_media_gateway();
			m_module = nullptr;
		}
		if (m_module_callback != nullptr)
		{
			EXPECT_TRUE(::testing::Mock::VerifyAndClearExpectations(m_module_callback));
			DELETEO(m_module_callback);
			m_module_callback = nullptr;
		}
	}
	//--------------------------------------------------------------------------
	h248::IMediaGate* rtx_test_environment::get_module()
	{
		return m_module;
	}
	mock_module_callback_interface_t* rtx_test_environment::get_module_callback()
	{
		return m_module_callback;
	}
	//--------------------------------------------------------------------------
	rtx_test_context_bothway_enviroment_info* rtx_test_environment::get_context_bothway()
	{
		return &m_bothway;
	}
	//--------------------------------------------------------------------------
	rtx_test_context_conference_enviroment_info* rtx_test_environment::get_context_conference()
	{
		return &m_conference;
	}
	//--------------------------------------------------------------------------
	//
	//--------------------------------------------------------------------------
	extern rtx_test_environment* g_environment = nullptr;
}
//------------------------------------------------------------------------------
int main(int argc, char* argv[])
{
	rtl::String toutput, vec_path;
	bool add_filters = true;
	for (int i = 0; i < argc; i++)
	{
		rtl::String str(argv[i]);
		if (str.indexOf("tiface=") != BAD_INDEX)
		{
			g_test_interface = argv[i] + 7;
		}
		else if (str.indexOf("tlog=") != BAD_INDEX)
		{
			g_test_log_path = argv[i] + 5;
		}
		else if (str.indexOf("toutput=") != BAD_INDEX)
		{
			toutput.append(argv[i] + 8);
		}
		else if (str.indexOf("--gtest_filter") != BAD_INDEX)
		{
			add_filters = false;
		}
		else if (str.indexOf("vecpath=") != BAD_INDEX)
		{
			vec_path = argv[i] + 8;
		}
	}

	if (!vec_path.isEmpty())
	{
		Config.set_config_value("vecpath", vec_path);
	}

	if (!toutput.isEmpty())
	{
		rtl::String path("xml:");
		path << toutput << FS_PATH_DELIMITER << "rtx_media_engine_test_result.xml";
		::testing::GTEST_FLAG(output) = (const char*)path;
	}

	::testing::InitGoogleTest(&argc, argv);
	::testing::InitGoogleMock(&argc, argv);
	
	if (add_filters)
	{
		::testing::GTEST_FLAG(filter) = "mg_context_test_t.*";
		int res = RUN_ALL_TESTS();
		if (res != 0)
		{
			return res;
		}
		::testing::GTEST_FLAG(filter) = "rtx_media_engine_test.try_make_simple_call";
		res = RUN_ALL_TESTS();
		if (res != 0)
		{
			return res;
		}
		::testing::GTEST_FLAG(filter) = "-rtx_media_engine_test.try_make_simple_call:-test_tools*.*:mg_context_test_t.*";
	}

	rtx_codec__load_all_codecs(nullptr, nullptr);

	rtx_media_engine::g_environment = (::rtx_media_engine::rtx_test_environment*)::testing::AddGlobalTestEnvironment(new ::rtx_media_engine::rtx_test_environment);
	return RUN_ALL_TESTS();
}
//------------------------------------------------------------------------------
