﻿/* RTX H.248 Media Gate Engine Test Module 
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mock_callbacks.h"
#include "net/net_socket.h"
#include "test_utils.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
extern char* g_test_interface;
extern char* g_test_log_path;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "gtest/gtest.h"
#include "gmock/gmock.h"

#define PRINTF(fmt, ...) printf("[          ] " #fmt, ##__VA_ARGS__ )
#define ERRORF(fmt, ...) printf("[ Error    ] " #fmt, ##__VA_ARGS__ )
//------------------------------------------------------------------------------
namespace rtx_media_engine
{
	struct rtx_test_termination_enviroment_info
	{
		net_socket* socket;
		uint32_t termination_id;
		uint16_t port;
		uint16_t seq;
		uint32_t timestamp;
		int context_index;
	};
	//--------------------------------------------------------------------------
	//
	//--------------------------------------------------------------------------
	struct rtx_test_context_bothway_enviroment_info
	{
		h248::IMediaContext* context;
		mock_termination_event_handler_t* termination_callback;

		rtx_test_termination_enviroment_info termination_info_1;
		rtx_test_termination_enviroment_info termination_info_2;
	};
	//--------------------------------------------------------------------------
	//
	//--------------------------------------------------------------------------
	struct rtx_test_context_conference_enviroment_info
	{
		h248::IMediaContext* context;
		mock_termination_event_handler_t* termination_callback;

		rtx_test_termination_enviroment_info termination_info_1;
		rtx_test_termination_enviroment_info termination_info_2;
		rtx_test_termination_enviroment_info termination_info_3;
	};
	//--------------------------------------------------------------------------
	void send_packet(net_socket* socket, uint16_t port, uint16_t s, uint32_t t);
	//--------------------------------------------------------------------------
	bool receive_packet(net_socket* socket);
	//--------------------------------------------------------------------------
	void clear_receiver(net_socket* socket);
	//--------------------------------------------------------------------------
	//
	//--------------------------------------------------------------------------
	class rtx_test_environment : public ::testing::Environment
	{
	public:
		rtx_test_environment();
		//----------------------------------------------------------------------
		~rtx_test_environment() override;
		//----------------------------------------------------------------------
		void SetUp() override;
		//----------------------------------------------------------------------
		void TearDown() override;
		//----------------------------------------------------------------------
		h248::IMediaGate* get_module();
		mock_module_callback_interface_t* get_module_callback();
		//----------------------------------------------------------------------
		rtx_test_context_bothway_enviroment_info* get_context_bothway();
		//----------------------------------------------------------------------
		rtx_test_context_conference_enviroment_info* get_context_conference();
		//----------------------------------------------------------------------

	private:
		h248::IMediaGate* m_module;
		mock_module_callback_interface_t* m_module_callback;
		rtx_test_context_bothway_enviroment_info m_bothway;
		rtx_test_context_conference_enviroment_info m_conference;
	};
	//--------------------------------------------------------------------------
	//
	//--------------------------------------------------------------------------
	extern rtx_test_environment* g_environment;
}
//------------------------------------------------------------------------------
extern "C"
{
	RTX_MGE_API int run_tests(int argc, char* argv[]);
}
//------------------------------------------------------------------------------
