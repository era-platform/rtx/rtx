﻿/* RTX H.248 Media Gate Engine Test Module 
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtp_packet.h"
#include "test_conference.h"
#include "mg_message_node.h"
#include "std_sys_env.h"

#include "gtest/gtest.h"
#include "gmock/gmock.h"

namespace rtx_media_engine
{
	rtx_conference_test_suite_t::rtx_conference_test_suite_t()
	{
		PRINTF("ctor\n");
		m_context = nullptr;
	}
	//--------------------------------------------------------------------------
	rtx_conference_test_suite_t::~rtx_conference_test_suite_t()
	{
		PRINTF("dtor\n");
	}
	//--------------------------------------------------------------------------
	void rtx_conference_test_suite_t::SetUp()
	{
		PRINTF("SetUp\n");
		if (m_context == nullptr)
		{
			ASSERT_TRUE(nullptr != g_environment);
			h248::IMediaGate* module = g_environment->get_module();
			ASSERT_TRUE(nullptr != module);

			m_context = module->MediaGate_createContext();
			ASSERT_TRUE(nullptr != m_context);

			EXPECT_CALL(m_termination_callback, TerminationEvent_callback(::testing::_)).Times(0);

			h248::Field reply;

			ASSERT_TRUE(m_context->mg_context_initialize(&m_termination_callback, &reply));
		}
		PRINTF("SetUp done\n");
	}
	//--------------------------------------------------------------------------
	void rtx_conference_test_suite_t::TearDown()
	{
		PRINTF("TearDown\n");
		if (m_context != nullptr)
		{
			h248::IMediaGate* module = g_environment->get_module();
			if (module == nullptr)
			{
				m_context = nullptr;
				return;
			}
			module->MediaGate_destroyContext(m_context);
			m_context = nullptr;
		}
		PRINTF("TearDown done\n");
	}
	//--------------------------------------------------------------------------
	//
	//--------------------------------------------------------------------------
	TEST_F(rtx_conference_test_suite_t, try_transform_to_conference)
	{
		PRINTF("step 0\n");
		//----------------------------------------------------------------------
		// prepare network environment
		net_socket socket_sender(nullptr);
		net_socket socket_receiver_1(nullptr);
		net_socket socket_receiver_2(nullptr);
		rtl::ArrayT<net_socket*> list;
		list.add(&socket_sender);
		list.add(&socket_receiver_1);
		list.add(&socket_receiver_2);

		ip_address_t ipaddr(g_test_interface);

		ASSERT_TRUE(open_tests_sockets(&list, &ipaddr));

		EXPECT_NE(0, socket_sender.get_local_address()->get_port());
		EXPECT_NE(0, socket_receiver_1.get_local_address()->get_port());
		EXPECT_NE(0, socket_receiver_2.get_local_address()->get_port());

		//----------------------------------------------------------------------
		// prepare context

		PRINTF("step 1\n");

		ASSERT_TRUE(m_context->mg_context_lock()) << "first lock";

		rtl::String sdp_local_1;
		sdp_local_1 << "v=0" << "\r\n";
		sdp_local_1 << "c=IN IP4 $" << "\r\n";
		sdp_local_1 << "m=audio $ RTP/AVP 0" << "\r\n";
		sdp_local_1 << "a=sendrecv" << "\r\n";

		rtl::String sdp_remote_1;
		sdp_remote_1 << "v=0" << "\r\n";
		sdp_remote_1 << "c=IN IP4 " << g_test_interface << "\r\n";
		sdp_remote_1 << "m=audio " << socket_sender.get_local_address()->get_port() << " RTP/AVP 0" << "\r\n";
		sdp_remote_1 << "a=sendrecv" << "\r\n";


		rtl::String sdp_local_2;
		sdp_local_2 << "v=0" << "\r\n";
		sdp_local_2 << "c=IN IP4 $" << "\r\n";
		sdp_local_2 << "m=audio $ RTP/AVP 0" << "\r\n";
		sdp_local_2 << "a=sendrecv" << "\r\n";

		rtl::String sdp_remote_2;
		sdp_remote_2 << "v=0" << "\r\n";
		sdp_remote_2 << "c=IN IP4 " << g_test_interface << "\r\n";
		sdp_remote_2 << "m=audio " << socket_receiver_1.get_local_address()->get_port() << " RTP/AVP 0" << "\r\n";
		sdp_remote_2 << "a=sendrecv" << "\r\n";

		h248::Field reply;
		h248::TerminationID termId(h248::TerminationType::RTP, "test", 1);
		uint32_t termination_id_1 = m_context->mg_context_add_termination(termId, &reply);
		ASSERT_EQ(1, termination_id_1);

		ASSERT_TRUE(m_context->mg_termination_set_Local_Remote(termination_id_1, 1, rtl::String::empty, sdp_local_1, &reply));

		ASSERT_TRUE(m_context->mg_termination_get_Local(termination_id_1, 1, sdp_local_1, &reply));

		termId.setId(2);
		uint32_t termination_id_2 = m_context->mg_context_add_termination(termId, &reply);
		ASSERT_EQ(2, termination_id_2);

		ASSERT_TRUE(m_context->mg_termination_set_Local_Remote(termination_id_2, 1, sdp_remote_2, sdp_local_2, &reply));

		ASSERT_TRUE(m_context->mg_termination_get_Local(termination_id_2, 1, sdp_local_2, &reply));

		ASSERT_TRUE(m_context->mg_termination_set_Local_Remote(termination_id_1, 1, sdp_remote_1, rtl::String::empty, &reply));

		ASSERT_TRUE(m_context->mg_termination_set_LocalControl_Mode(termination_id_1, 1, h248::TerminationStreamMode::SendRecv, &reply));
		ASSERT_TRUE(m_context->mg_termination_set_LocalControl_Mode(termination_id_2, 1, h248::TerminationStreamMode::SendRecv, &reply));

		ASSERT_TRUE(m_context->mg_context_unlock()) << "first unlock";

		PRINTF("step 2\n");

		//----------------------------------------------------------------------
		// send test packet

		int index_A = sdp_local_1.indexOf("m=audio ") + 8;
		int index_B = sdp_local_1.indexOf(" ", index_A);
		rtl::String port_str = sdp_local_1.substring(index_A, index_B - index_A);
		uint16_t port_send_to = (uint16_t)strtoul(port_str, nullptr, 10);
		socket_address sockaddr_send_to(&ipaddr, port_send_to);

		uint8_t buff[160] = { "TESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTES" };
		rtp_packet packet_send;
		packet_send.set_payload(buff, 160);
		packet_send.set_sequence_number(1);

		uint8_t buff_packet_send[1024] = { 0 };
		uint32_t buff_packet_write = packet_send.write_packet(buff_packet_send, 1024);

		EXPECT_EQ(buff_packet_write, socket_sender.send_to(buff_packet_send, buff_packet_write, &sockaddr_send_to));

		//----------------------------------------------------------------------
		// receive test packet

		socket_vector_t read;
		read.add(&socket_receiver_1);
		rtp_packet packet_receive_1;

		PRINTF("step 3\n");

		int count = 10;
		while (count > 0)
		{
			EXPECT_TRUE(net_socket::select(&read, NULL, NULL, 20, NULL));

			if (read.getCount() == 0)
			{
				rtl::Thread::sleep(50);
				read.add(&socket_receiver_1);
				count--;
			}
			else
			{
				socket_address sockaddr_from_to;
				uint8_t buff_packet_receive[1024] = { 0 };
				uint32_t read_size = read.getAt(0)->recv_from(buff_packet_receive, 1024, &sockaddr_from_to);
				EXPECT_NE(0, read_size);
				EXPECT_TRUE(packet_receive_1.read_packet(buff_packet_receive, read_size));
				break;
			}
		}

		EXPECT_NE(0, packet_receive_1.get_payload_length());

		//----------------------------------------------------------------------
		// add 3 termination

		PRINTF("step 4\n");

		ASSERT_TRUE(m_context->mg_context_lock()) << "second lock";

		rtl::String sdp_local_3;
		sdp_local_3 << "v=0" << "\r\n";
		sdp_local_3 << "c=IN IP4 $" << "\r\n";
		sdp_local_3 << "m=audio $ RTP/AVP 0" << "\r\n";
		sdp_local_3 << "a=sendrecv" << "\r\n";

		rtl::String sdp_remote_3;
		sdp_remote_3 << "v=0" << "\r\n";
		sdp_remote_3 << "c=IN IP4 " << g_test_interface << "\r\n";
		sdp_remote_3 << "m=audio " << socket_receiver_2.get_local_address()->get_port() << " RTP/AVP 0" << "\r\n";
		sdp_remote_3 << "a=sendrecv" << "\r\n";

		termId.setId(3);
		uint32_t termination_id_3 = m_context->mg_context_add_termination(termId, &reply);
		ASSERT_EQ(3, termination_id_3);

		ASSERT_TRUE(m_context->mg_termination_set_Local_Remote(termination_id_3, 1, sdp_remote_3, sdp_local_3, &reply));

		ASSERT_TRUE(m_context->mg_termination_get_Local(termination_id_3, 1, sdp_local_3, &reply));


		ASSERT_TRUE(m_context->mg_termination_set_LocalControl_Mode(termination_id_3, 1, h248::TerminationStreamMode::SendRecv, &reply));

		ASSERT_TRUE(m_context->mg_context_unlock()) << "second unlock";

		//----------------------------------------------------------------------
		// send test packet

		packet_send.set_sequence_number(2);

		memset(buff_packet_send, 0, 1024);
		buff_packet_write = packet_send.write_packet(buff_packet_send, 1024);

		EXPECT_EQ(buff_packet_write, socket_sender.send_to(buff_packet_send, buff_packet_write, &sockaddr_send_to));

		//----------------------------------------------------------------------
		// receive test packet

		read.clear();
		read.add(&socket_receiver_1);
		memset(&packet_receive_1, 0, sizeof(rtp_packet));

		PRINTF("step 5\n");

		count = 10;
		while (count > 0)
		{
			EXPECT_TRUE(net_socket::select(&read, NULL, NULL, 20, NULL));

			if (read.getCount() == 0)
			{
				rtl::Thread::sleep(50);
				read.add(&socket_receiver_1);
				count--;
			}
			else
			{
				socket_address sockaddr_from_to;
				uint8_t buff_packet_receive[1024] = { 0 };
				uint32_t read_size = read.getAt(0)->recv_from(buff_packet_receive, 1024, &sockaddr_from_to);
				EXPECT_NE(0, read_size);
				EXPECT_TRUE(packet_receive_1.read_packet(buff_packet_receive, read_size));
				break;
			}
		}

		read.clear();
		read.add(&socket_receiver_2);
		rtp_packet packet_receive_2;

		PRINTF("step 6\n");

		count = 10;
		while (count > 0)
		{
			EXPECT_TRUE(net_socket::select(&read, NULL, NULL, 20, NULL));

			if (read.getCount() == 0)
			{
				rtl::Thread::sleep(50);
				read.add(&socket_receiver_2);
				count--;
			}
			else
			{
				socket_address sockaddr_from_to;
				uint8_t buff_packet_receive[1024] = { 0 };
				uint32_t read_size = read.getAt(0)->recv_from(buff_packet_receive, 1024, &sockaddr_from_to);
				EXPECT_NE(0, read_size);
				EXPECT_TRUE(packet_receive_2.read_packet(buff_packet_receive, read_size));
				break;
			}
		}

		read.clear();
		read.add(&socket_sender);

		count = 5;
		while (count > 0)
		{
			EXPECT_TRUE(net_socket::select(&read, NULL, NULL, 20, NULL));

			if (read.getCount() == 0)
			{
				rtl::Thread::sleep(50);
				read.add(&socket_sender);
				count--;
			}
			else
			{
				socket_address sockaddr_from_to;
				uint8_t buff_packet_receive[1024] = { 0 };
				uint32_t read_size = read.getAt(0)->recv_from(buff_packet_receive, 1024, &sockaddr_from_to);
				EXPECT_EQ(0, read_size);
				break;
			}
		}

		EXPECT_NE(0, packet_receive_1.get_payload_length());
		EXPECT_NE(0, packet_receive_2.get_payload_length());

		ASSERT_TRUE(m_context->mg_context_lock()) << "thrid lock";

		m_context->mg_context_remove_termination(termination_id_1);
		m_context->mg_context_remove_termination(termination_id_2);
		m_context->mg_context_remove_termination(termination_id_3);

		close_tests_sockets(&list);

		PRINTF("step 7\n");

		ASSERT_TRUE(m_context->mg_context_unlock()) << "thrid unlock";
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_conference_test_suite_t, try_transform_to_simple)
	{
		//----------------------------------------------------------------------
		// prepare network environment
		net_socket socket_sender(nullptr);
		net_socket socket_receiver_1(nullptr);
		net_socket socket_receiver_2(nullptr);
		rtl::ArrayT<net_socket*> list;
		list.add(&socket_sender);
		list.add(&socket_receiver_1);
		list.add(&socket_receiver_2);

		ip_address_t ipaddr(g_test_interface);

		ASSERT_TRUE(open_tests_sockets(&list, &ipaddr));

		EXPECT_NE(0, socket_sender.get_local_address()->get_port());
		EXPECT_NE(0, socket_receiver_1.get_local_address()->get_port());
		EXPECT_NE(0, socket_receiver_2.get_local_address()->get_port());

		//----------------------------------------------------------------------
		// prepare context

		ASSERT_TRUE(m_context->mg_context_lock()) << "first lock";

		rtl::String sdp_local_1;
		sdp_local_1 << "v=0" << "\r\n";
		sdp_local_1 << "c=IN IP4 $" << "\r\n";
		sdp_local_1 << "m=audio $ RTP/AVP 0" << "\r\n";
		sdp_local_1 << "a=sendrecv" << "\r\n";

		rtl::String sdp_remote_1;
		sdp_remote_1 << "v=0" << "\r\n";
		sdp_remote_1 << "c=IN IP4 " << g_test_interface << "\r\n";
		sdp_remote_1 << "m=audio " << socket_sender.get_local_address()->get_port() << " RTP/AVP 0" << "\r\n";
		sdp_remote_1 << "a=sendrecv" << "\r\n";


		rtl::String sdp_local_2;
		sdp_local_2 << "v=0" << "\r\n";
		sdp_local_2 << "c=IN IP4 $" << "\r\n";
		sdp_local_2 << "m=audio $ RTP/AVP 0" << "\r\n";
		sdp_local_2 << "a=sendrecv" << "\r\n";

		rtl::String sdp_remote_2;
		sdp_remote_2 << "v=0" << "\r\n";
		sdp_remote_2 << "c=IN IP4 " << g_test_interface << "\r\n";
		sdp_remote_2 << "m=audio " << socket_receiver_1.get_local_address()->get_port() << " RTP/AVP 0" << "\r\n";
		sdp_remote_2 << "a=sendrecv" << "\r\n";

		h248::Field reply;
		h248::TerminationID termId(h248::TerminationType::RTP, "test", 1);
		uint32_t termination_id_1 = m_context->mg_context_add_termination(termId, &reply);
		ASSERT_EQ(1, termination_id_1);

		ASSERT_TRUE(m_context->mg_termination_set_Local_Remote(termination_id_1, 1, rtl::String::empty, sdp_local_1, &reply));

		ASSERT_TRUE(m_context->mg_termination_get_Local(termination_id_1, 1, sdp_local_1, &reply));

		termId.setId(2);
		uint32_t termination_id_2 = m_context->mg_context_add_termination(termId, &reply);
		ASSERT_EQ(2, termination_id_2);

		ASSERT_TRUE(m_context->mg_termination_set_Local_Remote(termination_id_2, 1, sdp_remote_2, sdp_local_2, &reply));

		ASSERT_TRUE(m_context->mg_termination_get_Local(termination_id_2, 1, sdp_local_2, &reply));

		ASSERT_TRUE(m_context->mg_termination_set_Local_Remote(termination_id_1, 1, sdp_remote_1, rtl::String::empty, &reply));

		ASSERT_TRUE(m_context->mg_termination_set_LocalControl_Mode(termination_id_1, 1, h248::TerminationStreamMode::SendRecv, &reply));
		ASSERT_TRUE(m_context->mg_termination_set_LocalControl_Mode(termination_id_2, 1, h248::TerminationStreamMode::SendRecv, &reply));

		rtl::String sdp_local_3;
		sdp_local_3 << "v=0" << "\r\n";
		sdp_local_3 << "c=IN IP4 $" << "\r\n";
		sdp_local_3 << "m=audio $ RTP/AVP 0" << "\r\n";
		sdp_local_3 << "a=sendrecv" << "\r\n";

		rtl::String sdp_remote_3;
		sdp_remote_3 << "v=0" << "\r\n";
		sdp_remote_3 << "c=IN IP4 " << g_test_interface << "\r\n";
		sdp_remote_3 << "m=audio " << socket_receiver_2.get_local_address()->get_port() << " RTP/AVP 0" << "\r\n";
		sdp_remote_3 << "a=sendrecv" << "\r\n";

		termId.setId(3);
		uint32_t termination_id_3 = m_context->mg_context_add_termination(termId, &reply);
		ASSERT_EQ(3, termination_id_3);

		ASSERT_TRUE(m_context->mg_termination_set_Local_Remote(termination_id_3, 1, sdp_remote_3, sdp_local_3, &reply));

		ASSERT_TRUE(m_context->mg_termination_get_Local(termination_id_3, 1, sdp_local_3, &reply));


		ASSERT_TRUE(m_context->mg_termination_set_LocalControl_Mode(termination_id_3, 1, h248::TerminationStreamMode::SendRecv, &reply));

		ASSERT_TRUE(m_context->mg_context_unlock()) << "first unlock";

		//----------------------------------------------------------------------
		// send test packet

		int index_A = sdp_local_1.indexOf("m=audio ") + 8;
		int index_B = sdp_local_1.indexOf(" ", index_A);
		rtl::String port_str = sdp_local_1.substring(index_A, index_B - index_A);
		uint16_t port_send_to = (uint16_t)strtoul(port_str, nullptr, 10);
		socket_address sockaddr_send_to(&ipaddr, port_send_to);

		uint8_t buff[160] = { "TESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTES" };
		rtp_packet packet_send;
		packet_send.set_payload(buff, 160);
		packet_send.set_sequence_number(1);

		uint8_t buff_packet_send[1024] = { 0 };
		uint32_t buff_packet_write = packet_send.write_packet(buff_packet_send, 1024);

		EXPECT_EQ(buff_packet_write, socket_sender.send_to(buff_packet_send, buff_packet_write, &sockaddr_send_to));

		//----------------------------------------------------------------------
		// receive test packet

		socket_vector_t read;
		read.add(&socket_receiver_1);
		rtp_packet packet_receive_1;

		int count = 10;
		while (count > 0)
		{
			EXPECT_TRUE(net_socket::select(&read, NULL, NULL, 20, NULL));

			if (read.getCount() == 0)
			{
				rtl::Thread::sleep(50);
				read.add(&socket_receiver_1);
				count--;
			}
			else
			{
				socket_address sockaddr_from_to;
				uint8_t buff_packet_receive[1024] = { 0 };
				uint32_t read_size = read.getAt(0)->recv_from(buff_packet_receive, 1024, &sockaddr_from_to);
				EXPECT_NE(0, read_size);
				EXPECT_TRUE(packet_receive_1.read_packet(buff_packet_receive, read_size));
				break;
			}
		}

		read.clear();
		read.add(&socket_receiver_2);
		rtp_packet packet_receive_2;

		count = 10;
		while (count > 0)
		{
			EXPECT_TRUE(net_socket::select(&read, NULL, NULL, 20, NULL));

			if (read.getCount() == 0)
			{
				rtl::Thread::sleep(50);
				read.add(&socket_receiver_2);
				count--;
			}
			else
			{
				socket_address sockaddr_from_to;
				uint8_t buff_packet_receive[1024] = { 0 };
				uint32_t read_size = read.getAt(0)->recv_from(buff_packet_receive, 1024, &sockaddr_from_to);
				EXPECT_NE(0, read_size);
				EXPECT_TRUE(packet_receive_2.read_packet(buff_packet_receive, read_size));
				break;
			}
		}

		read.clear();
		read.add(&socket_sender);

		count = 5;
		while (count > 0)
		{
			EXPECT_TRUE(net_socket::select(&read, NULL, NULL, 20, NULL));

			if (read.getCount() == 0)
			{
				rtl::Thread::sleep(50);
				read.add(&socket_sender);
				count--;
			}
			else
			{
				socket_address sockaddr_from_to;
				uint8_t buff_packet_receive[1024] = { 0 };
				uint32_t read_size = read.getAt(0)->recv_from(buff_packet_receive, 1024, &sockaddr_from_to);
				EXPECT_EQ(0, read_size);
				break;
			}
		}

		EXPECT_NE(0, packet_receive_1.get_payload_length());
		EXPECT_NE(0, packet_receive_2.get_payload_length());

		//----------------------------------------------------------------------
		// remove termination

		ASSERT_TRUE(m_context->mg_context_lock()) << "second lock";

		m_context->mg_context_remove_termination(termination_id_3);

		ASSERT_TRUE(m_context->mg_context_unlock()) << "second unlock";

		//----------------------------------------------------------------------
		// send test packet

		packet_send.set_sequence_number(2);

		memset(buff_packet_send, 0, 1024);
		buff_packet_write = packet_send.write_packet(buff_packet_send, 1024);

		EXPECT_EQ(buff_packet_write, socket_sender.send_to(buff_packet_send, buff_packet_write, &sockaddr_send_to));

		//----------------------------------------------------------------------
		// receive test packet

		read.clear();
		read.add(&socket_receiver_1);
		
		memset(&packet_receive_1, 0, sizeof(rtp_packet));
		memset(&packet_receive_2, 0, sizeof(rtp_packet));

		count = 10;
		while (count > 0)
		{
			EXPECT_TRUE(net_socket::select(&read, NULL, NULL, 20, NULL));

			if (read.getCount() == 0)
			{
				rtl::Thread::sleep(50);
				read.add(&socket_receiver_1);
				count--;
			}
			else
			{
				socket_address sockaddr_from_to;
				uint8_t buff_packet_receive[1024] = { 0 };
				uint32_t read_size = read.getAt(0)->recv_from(buff_packet_receive, 1024, &sockaddr_from_to);
				EXPECT_NE(0, read_size);
				EXPECT_TRUE(packet_receive_1.read_packet(buff_packet_receive, read_size));
				break;
			}
		}

		EXPECT_NE(0, packet_receive_1.get_payload_length());

		read.clear();
		read.add(&socket_receiver_2);
		count = 5;
		while (count > 0)
		{
			EXPECT_TRUE(net_socket::select(&read, NULL, NULL, 20, NULL));

			if (read.getCount() == 0)
			{
				rtl::Thread::sleep(50);
				read.add(&socket_receiver_2);
				count--;
			}
			else
			{
				socket_address sockaddr_from_to;
				uint8_t buff_packet_receive[1024] = { 0 };
				uint32_t read_size = read.getAt(0)->recv_from(buff_packet_receive, 1024, &sockaddr_from_to);
				EXPECT_EQ(0, read_size);
				break;
			}
		}

		read.clear();
		read.add(&socket_sender);

		count = 5;
		while (count > 0)
		{
			EXPECT_TRUE(net_socket::select(&read, NULL, NULL, 20, NULL));

			if (read.getCount() == 0)
			{
				rtl::Thread::sleep(50);
				read.add(&socket_sender);
				count--;
			}
			else
			{
				socket_address sockaddr_from_to;
				uint8_t buff_packet_receive[1024] = { 0 };
				uint32_t read_size = read.getAt(0)->recv_from(buff_packet_receive, 1024, &sockaddr_from_to);
				EXPECT_EQ(0, read_size);
				break;
			}
		}


		ASSERT_TRUE(m_context->mg_context_lock()) << "thrid lock";

		m_context->mg_context_remove_termination(termination_id_1);
		m_context->mg_context_remove_termination(termination_id_2);
		m_context->mg_context_remove_termination(termination_id_3);

		close_tests_sockets(&list);

		ASSERT_TRUE(m_context->mg_context_unlock()) << "thrid unlock";
	}
	////--------------------------------------------------------------------------
	//TEST_F(rtx_conference_test_suite_t, try_conference_record)
	//{
	//	//----------------------------------------------------------------------
	//	// prepare network environment
	//	net_socket socket_sender(nullptr);
	//	rtl::ArrayT<net_socket*> list;
	//	list.add(&socket_sender);
	//	
	//	ip_address_t ipaddr(g_test_interface);

	//	ASSERT_TRUE(open_tests_sockets(&list, &ipaddr));

	//	EXPECT_NE(0, socket_sender.get_local_address()->get_port());
	//	
	//	//----------------------------------------------------------------------
	//	// prepare context

	//	ASSERT_TRUE(m_context->mg_context_lock()) << "first lock";

	//	rtl::String sdp_local_1;
	//	sdp_local_1 << "v=0" << "\r\n";
	//	sdp_local_1 << "c=IN IP4 $" << "\r\n";
	//	sdp_local_1 << "m=audio $ RTP/AVP 0" << "\r\n";
	//	sdp_local_1 << "a=sendrecv" << "\r\n";

	//	rtl::String sdp_remote_1;
	//	sdp_remote_1 << "v=0" << "\r\n";
	//	sdp_remote_1 << "c=IN IP4 " << g_test_interface << "\r\n";
	//	sdp_remote_1 << "m=audio " << socket_sender.get_local_address()->get_port() << " RTP/AVP 0" << "\r\n";
	//	sdp_remote_1 << "a=sendrecv" << "\r\n";


	//	rtl::String sdp_local_2;
	//	sdp_local_2 << "v=0" << "\r\n";
	//	sdp_local_2 << "c=IN IP4 $" << "\r\n";
	//	sdp_local_2 << "m=audio $ RTP/AVP 0" << "\r\n";
	//	sdp_local_2 << "a=sendrecv" << "\r\n";

	//	rtl::String sdp_remote_2;
	//	sdp_remote_2 << "v=0" << "\r\n";
	//	sdp_remote_2 << "c=IN IP4 " << g_test_interface << "\r\n";
	//	sdp_remote_2 << "m=audio 9990 RTP/AVP 0" << "\r\n";
	//	sdp_remote_2 << "a=sendrecv" << "\r\n";

	//	rtl::String sdp_local_3;
	//	sdp_local_3 << "v=0" << "\r\n";
	//	sdp_local_3 << "c=IN IP4 $" << "\r\n";
	//	sdp_local_3 << "m=audio $ RTP/AVP 0" << "\r\n";
	//	sdp_local_3 << "a=sendrecv" << "\r\n";

	//	rtl::String sdp_remote_3;
	//	sdp_remote_3 << "v=0" << "\r\n";
	//	sdp_remote_3 << "c=IN IP4 " << g_test_interface << "\r\n";
	//	sdp_remote_3 << "m=audio 9991 RTP/AVP 0" << "\r\n";
	//	sdp_remote_3 << "a=sendrecv" << "\r\n";

	//	Config.set_config_value("RECORD", g_test_log_path);

	//	rtl::String record_path(g_test_log_path);
	//	record_path << FS_PATH_DELIMITER << "RECORD";
	//	rtl::String record_mask(record_path);
	//	record_mask << FS_PATH_DELIMITER << "*";
	//	rtl::ArrayT<fs_entry_info_t> files;
	//	fs_directory_get_entries(record_mask, files);
	//	for (int i = 0; i < files.getCount(); i++)
	//	{
	//		fs_entry_info_t file = files.getAt(i);
	//		if (!file.fs_directory)
	//		{
	//			rtl::String filepath(record_path);
	//			filepath << FS_PATH_DELIMITER << file.fs_filename;
	//			fs_delete_file(filepath);
	//		}
	//	}
	//	fs_directory_delete(record_path);
	//	
	//	h248::Field node_rec("recpath", "%3aRECORD%2fRECORD");
	//	h248::Field node_time_rec("timestamp", "1510573560124");
	//	h248::Field node_conf("mode", "conf");
	//	
	//	ASSERT_TRUE(m_context->mg_context_set_property(&node_time_rec));
	//	ASSERT_TRUE(m_context->mg_context_set_property(&node_rec));
	//	ASSERT_TRUE(m_context->mg_context_set_property(&node_conf));
	//	
	//	uint32_t termination_id_1 = m_context->mg_context_add_termination(h248::TerminationType::RTP, "test", "test/term/1");
	//	ASSERT_EQ(1, termination_id_1);
	//	ASSERT_TRUE(m_context->mg_termination_set_Local_Remote(termination_id_1, 1, rtl::String::empty, sdp_local_1));
	//	ASSERT_TRUE(m_context->mg_termination_get_Local(termination_id_1, 1, sdp_local_1));

	//	uint32_t termination_id_2 = m_context->mg_context_add_termination(h248::TerminationType::RTP, "test", "test/term/2");
	//	ASSERT_EQ(2, termination_id_2);
	//	ASSERT_TRUE(m_context->mg_termination_set_Local_Remote(termination_id_2, 1, sdp_remote_2, sdp_local_2));
	//	ASSERT_TRUE(m_context->mg_termination_get_Local(termination_id_2, 1, sdp_local_2));

	//	ASSERT_TRUE(m_context->mg_termination_set_Local_Remote(termination_id_1, 1, sdp_remote_1, rtl::String::empty));

	//	uint32_t termination_id_3 = m_context->mg_context_add_termination(h248::TerminationType::RTP, "test", "test/term/3");
	//	ASSERT_EQ(3, termination_id_3);
	//	ASSERT_TRUE(m_context->mg_termination_set_Local_Remote(termination_id_3, 1, sdp_remote_3, sdp_local_3));
	//	ASSERT_TRUE(m_context->mg_termination_get_Local(termination_id_3, 1, sdp_local_3));

	//	ASSERT_TRUE(m_context->mg_termination_set_LocalControl_Mode(termination_id_1, 1, h248::TerminationStreamMode::SendRecv));
	//	ASSERT_TRUE(m_context->mg_termination_set_LocalControl_Mode(termination_id_2, 1, h248::TerminationStreamMode::SendRecv));
	//	ASSERT_TRUE(m_context->mg_termination_set_LocalControl_Mode(termination_id_3, 1, h248::TerminationStreamMode::SendRecv));

	//	ASSERT_TRUE(m_context->mg_context_unlock()) << "first unlock";

	//	//----------------------------------------------------------------------
	//	// send test packet

	//	int index_A = sdp_local_1.indexOf("m=audio ") + 8;
	//	int index_B = sdp_local_1.indexOf(" ", index_A);
	//	rtl::String port_str = sdp_local_1.substring(index_A, index_B - index_A);
	//	uint16_t port_send_to = (uint16_t)strtoul(port_str, nullptr, 10);
	//	socket_address sockaddr_send_to(&ipaddr, port_send_to);

	//	uint8_t buff[160] = { "***************************************************************************************************************************************************************" };
	//	rtp_packet packet_send;
	//	packet_send.set_payload(buff, 160);
	//	packet_send.set_sequence_number(10);

	//	uint8_t buff_packet_send[1024] = { 0 };
	//	uint32_t buff_packet_write = packet_send.write_packet(buff_packet_send, 1024);

	//	EXPECT_EQ(buff_packet_write, socket_sender.send_to(buff_packet_send, buff_packet_write, &sockaddr_send_to));

	//	//----------------------------------------------------------------------
	//	// check record file

	//	rtl::Thread::sleep(200);

	//	ASSERT_TRUE(m_context->mg_context_lock()) << "second lock";

	//	m_context->mg_context_remove_termination(termination_id_1);
	//	m_context->mg_context_remove_termination(termination_id_2);
	//	m_context->mg_context_remove_termination(termination_id_3);

	//	ASSERT_TRUE(m_context->mg_context_unlock()) << "second unlock";

	//	files.clear();
	//	fs_directory_get_entries(record_mask, files);
	//	int count_record_file = 0;
	//	for (int i = 0; i < files.getCount(); i++)
	//	{
	//		fs_entry_info_t file = files.getAt(i);
	//		if (!file.fs_directory)
	//		{
	//			rtl::String filepath(record_path);
	//			filepath << FS_PATH_DELIMITER << file.fs_filename;
	//			if (file.fs_filesize != 0)
	//			{
	//				count_record_file++;
	//			}
	//			fs_delete_file(filepath);
	//		}
	//	}
	//	fs_directory_delete(record_path);

	//	EXPECT_EQ(1, count_record_file);

	//	close_tests_sockets(&list);
	//}
	//--------------------------------------------------------------------------
	//TEST_F(rtx_conference_test_suite_t, try_player_to_conference)
	//{
	//	//----------------------------------------------------------------------
	//	// prepare network environment
	//	net_socket socket_receiver_1(nullptr);
	//	net_socket socket_receiver_2(nullptr);
	//	rtl::ArrayT<net_socket*> list;
	//	list.add(&socket_receiver_1);
	//	list.add(&socket_receiver_2);

	//	ip_address_t ipaddr(g_test_interface);

	//	ASSERT_TRUE(open_tests_sockets(&list, &ipaddr));

	//	EXPECT_NE(0, socket_receiver_1.get_local_address()->get_port());
	//	EXPECT_NE(0, socket_receiver_2.get_local_address()->get_port());

	//	//----------------------------------------------------------------------
	//	// prepare context

	//	EXPECT_CALL(m_termination_callback, TerminationEvent_callback(::testing::_)).Times(0);

	//	ASSERT_TRUE(m_context->mg_context_lock()) << "first lock";

	//	rtl::String sdp_local_1;
	//	sdp_local_1 << "v=0" << "\r\n";
	//	sdp_local_1 << "c=IN IP4 $" << "\r\n";
	//	sdp_local_1 << "m=audio $ RTP/AVP 0" << "\r\n";
	//	sdp_local_1 << "a=sendrecv" << "\r\n";

	//	rtl::String sdp_remote_1;
	//	sdp_remote_1 << "v=0" << "\r\n";
	//	sdp_remote_1 << "c=IN IP4 " << g_test_interface << "\r\n";
	//	sdp_remote_1 << "m=audio " << socket_receiver_1.get_local_address()->get_port() << " RTP/AVP 0" << "\r\n";
	//	sdp_remote_1 << "a=sendrecv" << "\r\n";


	//	rtl::String sdp_local_2;
	//	sdp_local_2 << "v=0" << "\r\n";
	//	sdp_local_2 << "c=IN IP4 $" << "\r\n";
	//	sdp_local_2 << "m=audio $ RTP/AVP 0" << "\r\n";
	//	sdp_local_2 << "a=sendrecv" << "\r\n";

	//	rtl::String sdp_remote_2;
	//	sdp_remote_2 << "v=0" << "\r\n";
	//	sdp_remote_2 << "c=IN IP4 " << g_test_interface << "\r\n";
	//	sdp_remote_2 << "m=audio " << socket_receiver_2.get_local_address()->get_port() << " RTP/AVP 0" << "\r\n";
	//	sdp_remote_2 << "a=sendrecv" << "\r\n";

	//
	//	uint32_t termination_id_1 = m_context->mg_context_add_termination(h248::TerminationType::RTP, "test", "test/term/1");
	//	ASSERT_EQ(1, termination_id_1);
	//	ASSERT_TRUE(m_context->mg_termination_set_Local_Remote(termination_id_1, 1, rtl::String::empty, sdp_local_1));
	//	ASSERT_TRUE(m_context->mg_termination_get_Local(termination_id_1, 1, sdp_local_1));

	//	uint32_t termination_id_2 = m_context->mg_context_add_termination(h248::TerminationType::RTP, "test", "test/term/2");
	//	ASSERT_EQ(2, termination_id_2);
	//	ASSERT_TRUE(m_context->mg_termination_set_Local_Remote(termination_id_2, 1, sdp_remote_2, sdp_local_2));
	//	ASSERT_TRUE(m_context->mg_termination_get_Local(termination_id_2, 1, sdp_local_2));

	//	ASSERT_TRUE(m_context->mg_termination_set_Local_Remote(termination_id_1, 1, sdp_remote_1, rtl::String::empty));

	//	uint32_t termination_id_3 = m_context->mg_context_add_termination(h248::TerminationType::IVR_Player, "test", "test/term/3_player");
	//	ASSERT_EQ(3, termination_id_3);

	//	rtl::String path_to_media = get_path_to_tests_data();

	//	Config.set_config_value("MEDIA", path_to_media);
	//	rtl::String path_to_file(path_to_media);
	//	path_to_file << FS_PATH_DELIMITER << "Media" << FS_PATH_DELIMITER << "try_player_to_conference.wav";
	//	PRINTF("!!!! try_player_to_conference.wav !!!! path is %s\n",(const char*)path_to_file);
	//	ASSERT_TRUE(fs_is_file_exist(path_to_file)) << " : " << (const char*)path_to_file;

	//	rtl::String file("%3aMEDIA%2f");
	//	file << "Media" << FS_PATH_DELIMITER << "try_player_to_conference.wav";
	//	h248::Field node_file("file", file);
	//	ASSERT_TRUE(m_context->mg_termination_set_TerminationState_Property(termination_id_3, &node_file));

	//	ASSERT_TRUE(m_context->mg_termination_set_LocalControl_Mode(termination_id_1, 1, h248::TerminationStreamMode::SendRecv));
	//	ASSERT_TRUE(m_context->mg_termination_set_LocalControl_Mode(termination_id_2, 1, h248::TerminationStreamMode::SendRecv));
	//	ASSERT_TRUE(m_context->mg_termination_set_LocalControl_Mode(termination_id_3, 1, h248::TerminationStreamMode::SendRecv));

	//	ASSERT_TRUE(m_context->mg_context_unlock()) << "first unlock";

	//	//----------------------------------------------------------------------
	//	// receive test packet

	//	socket_vector_t read;
	//	read.add(&socket_receiver_1);
	//	rtp_packet packet_receive_1;

	//	int count = 10;
	//	while (count > 0)
	//	{
	//		EXPECT_TRUE(net_socket::select(&read, NULL, NULL, 20, NULL));

	//		if (read.getCount() == 0)
	//		{
	//			rtl::Thread::sleep(50);
	//			read.add(&socket_receiver_1);
	//			count--;
	//		}
	//		else
	//		{
	//			socket_address sockaddr_from_to;
	//			uint8_t buff_packet_receive[1024] = { 0 };
	//			uint32_t read_size = read.getAt(0)->recv_from(buff_packet_receive, 1024, &sockaddr_from_to);
	//			EXPECT_NE(0, read_size);
	//			EXPECT_TRUE(packet_receive_1.read_packet(buff_packet_receive, read_size));
	//			break;
	//		}
	//	}

	//	read.clear();
	//	read.add(&socket_receiver_2);
	//	rtp_packet packet_receive_2;

	//	count = 10;
	//	while (count > 0)
	//	{
	//		EXPECT_TRUE(net_socket::select(&read, NULL, NULL, 20, NULL));

	//		if (read.getCount() == 0)
	//		{
	//			rtl::Thread::sleep(50);
	//			read.add(&socket_receiver_2);
	//			count--;
	//		}
	//		else
	//		{
	//			socket_address sockaddr_from_to;
	//			uint8_t buff_packet_receive[1024] = { 0 };
	//			uint32_t read_size = read.getAt(0)->recv_from(buff_packet_receive, 1024, &sockaddr_from_to);
	//			EXPECT_NE(0, read_size);
	//			EXPECT_TRUE(packet_receive_2.read_packet(buff_packet_receive, read_size));
	//			break;
	//		}
	//	}

	//	EXPECT_NE(0, packet_receive_1.get_payload_length());
	//	EXPECT_NE(0, packet_receive_2.get_payload_length());

	//	ASSERT_TRUE(m_context->mg_context_lock()) << "second lock";

	//	m_context->mg_context_remove_termination(termination_id_1);
	//	m_context->mg_context_remove_termination(termination_id_2);
	//	m_context->mg_context_remove_termination(termination_id_3);

	//	ASSERT_TRUE(m_context->mg_context_unlock()) << "second unlock";

	//	close_tests_sockets(&list);
	//}
	//--------------------------------------------------------------------------
}
//------------------------------------------------------------------------------
