﻿/* RTX H.248 Media Gate Engine Test Module 
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "stdafx.h"

#include "mg_interface.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "gmock/gmock.h"
namespace rtx_media_engine
{
	struct mock_module_callback_interface_t : public h248::IMediaGateEventHandler
	{
	public:
		MOCK_METHOD0(mge__event_raised, void());
		MOCK_METHOD0(mge__error_raised, void());
	};
	//------------------------------------------------------------------------------
	struct mock_termination_event_handler_t : public h248::ITerminationEventHandler
	{
	public:
		MOCK_METHOD1(TerminationEvent_callback, void(h248::EventParams* eventData));
	};
	//------------------------------------------------------------------------------
}
//------------------------------------------------------------------------------
