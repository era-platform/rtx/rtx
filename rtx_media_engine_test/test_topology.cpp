﻿/* RTX H.248 Media Gate Engine Test Module 
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtp_packet.h"
#include "test_topology.h"

 //------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
namespace rtx_media_engine
{
	void send_packet_conference_to_A()
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		ASSERT_NE(0, conference->termination_info_1.port);

		send_packet(conference->termination_info_1.socket, conference->termination_info_1.port, conference->termination_info_1.seq, conference->termination_info_1.timestamp);
		conference->termination_info_1.seq++;
		conference->termination_info_1.timestamp += 160;
	}
	//--------------------------------------------------------------------------
	void send_packet_conference_to_B()
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		ASSERT_NE(0, conference->termination_info_2.port);

		send_packet(conference->termination_info_2.socket, conference->termination_info_2.port, conference->termination_info_2.seq, conference->termination_info_2.timestamp);
		conference->termination_info_2.seq++;
		conference->termination_info_2.timestamp += 160;
	}
	//--------------------------------------------------------------------------
	void send_packet_conference_to_C()
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		ASSERT_NE(0, conference->termination_info_3.port);

		send_packet(conference->termination_info_3.socket, conference->termination_info_3.port, conference->termination_info_3.seq, conference->termination_info_3.timestamp);
		conference->termination_info_3.seq++;
		conference->termination_info_3.timestamp += 160;
	}
	//--------------------------------------------------------------------------
	bool receive_packet_conference_on_A()
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		if (conference == nullptr || conference->termination_info_1.socket == nullptr)
		{
			return false;
		}

		return receive_packet(conference->termination_info_1.socket);
	}
	//--------------------------------------------------------------------------
	bool receive_packet_conference_on_B()
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		if (conference == nullptr || conference->termination_info_2.socket == nullptr)
		{
			return false;
		}

		return receive_packet(conference->termination_info_2.socket);
	}
	//--------------------------------------------------------------------------
	bool receive_packet_conference_on_C()
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		if (conference == nullptr || conference->termination_info_3.socket == nullptr)
		{
			return false;
		}

		return receive_packet(conference->termination_info_3.socket);
	}
	//--------------------------------------------------------------------------
	//
	//--------------------------------------------------------------------------
	rtx_topology_test_suite_t::rtx_topology_test_suite_t()
	{

	}
	//--------------------------------------------------------------------------
	rtx_topology_test_suite_t::~rtx_topology_test_suite_t()
	{
		
	}
	//--------------------------------------------------------------------------
	void rtx_topology_test_suite_t::SetUp()
	{
		ASSERT_TRUE(nullptr != g_environment);
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		ASSERT_TRUE(nullptr != conference);
		ASSERT_TRUE(nullptr != conference->context);
		ASSERT_TRUE(nullptr != conference->termination_info_1.socket);
		ASSERT_TRUE(nullptr != conference->termination_info_2.socket);
		ASSERT_TRUE(nullptr != conference->termination_info_3.socket);
	}
	//--------------------------------------------------------------------------
	void rtx_topology_test_suite_t::TearDown()
	{
		
	}
	//--------------------------------------------------------------------------
	//
	//--------------------------------------------------------------------------
	TEST_F(rtx_topology_test_suite_t, conference_ABC_isolate_C)
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		ASSERT_TRUE(conference->context->mg_context_lock());

		h248::TopologyTriple one;
		one.term_from = conference->termination_info_1.termination_id;
		one.term_to = conference->termination_info_3.termination_id;
		one.direction = h248::TopologyDirection::Isolate;

		h248::TopologyTriple two;
		two.term_from = conference->termination_info_2.termination_id;
		two.term_to = conference->termination_info_3.termination_id;
		two.direction = h248::TopologyDirection::Isolate;

		h248::TopologyTriple three;
		three.term_from = conference->termination_info_1.termination_id;
		three.term_to = conference->termination_info_2.termination_id;
		three.direction = h248::TopologyDirection::Bothway;

		rtl::ArrayT<h248::TopologyTriple> topology_tr;
		topology_tr.add(one);
		topology_tr.add(two);
		topology_tr.add(three);

		h248::Field reply;
		conference->context->mg_context_set_topology(topology_tr, &reply);

		ASSERT_TRUE(conference->context->mg_context_unlock());

		send_packet_conference_to_A();
		ASSERT_TRUE(receive_packet_conference_on_B());
		ASSERT_FALSE(receive_packet_conference_on_C());

		send_packet_conference_to_B();
		ASSERT_TRUE(receive_packet_conference_on_A());
		ASSERT_FALSE(receive_packet_conference_on_C());

		send_packet_conference_to_C();
		ASSERT_FALSE(receive_packet_conference_on_A());
		ASSERT_FALSE(receive_packet_conference_on_B());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_topology_test_suite_t, conference_ABC_A_oneway_B_isolate_C)
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		ASSERT_TRUE(conference->context->mg_context_lock());

		h248::TopologyTriple one;
		one.term_from = conference->termination_info_1.termination_id;
		one.term_to = conference->termination_info_3.termination_id;
		one.direction = h248::TopologyDirection::Isolate;

		h248::TopologyTriple two;
		two.term_from = conference->termination_info_2.termination_id;
		two.term_to = conference->termination_info_3.termination_id;
		two.direction = h248::TopologyDirection::Isolate;

		h248::TopologyTriple three;
		three.term_from = conference->termination_info_1.termination_id;
		three.term_to = conference->termination_info_2.termination_id;
		three.direction = h248::TopologyDirection::Oneway;

		rtl::ArrayT<h248::TopologyTriple> topology_tr;
		topology_tr.add(one);
		topology_tr.add(two);
		topology_tr.add(three);

		h248::Field reply;
		conference->context->mg_context_set_topology(topology_tr, &reply);

		ASSERT_TRUE(conference->context->mg_context_unlock());

		send_packet_conference_to_A();
		ASSERT_TRUE(receive_packet_conference_on_B());
		ASSERT_FALSE(receive_packet_conference_on_C());

		send_packet_conference_to_B();
		ASSERT_FALSE(receive_packet_conference_on_A());
		ASSERT_FALSE(receive_packet_conference_on_C());

		send_packet_conference_to_C();
		ASSERT_FALSE(receive_packet_conference_on_A());
		ASSERT_FALSE(receive_packet_conference_on_B());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_topology_test_suite_t, conference_ABC_A_oneway_B)
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		ASSERT_TRUE(conference->context->mg_context_lock());

		h248::TopologyTriple one;
		one.term_from = conference->termination_info_1.termination_id;
		one.term_to = conference->termination_info_3.termination_id;
		one.direction = h248::TopologyDirection::Bothway;

		h248::TopologyTriple two;
		two.term_from = conference->termination_info_2.termination_id;
		two.term_to = conference->termination_info_3.termination_id;
		two.direction = h248::TopologyDirection::Bothway;

		h248::TopologyTriple three;
		three.term_from = conference->termination_info_1.termination_id;
		three.term_to = conference->termination_info_2.termination_id;
		three.direction = h248::TopologyDirection::Oneway;

		rtl::ArrayT<h248::TopologyTriple> topology_tr;
		topology_tr.add(one);
		topology_tr.add(two);
		topology_tr.add(three);

		h248::Field reply;
		conference->context->mg_context_set_topology(topology_tr, &reply);

		ASSERT_TRUE(conference->context->mg_context_unlock());

		send_packet_conference_to_A();
		ASSERT_TRUE(receive_packet_conference_on_B());
		ASSERT_TRUE(receive_packet_conference_on_C());

		send_packet_conference_to_B();
		ASSERT_FALSE(receive_packet_conference_on_A());
		ASSERT_TRUE(receive_packet_conference_on_C());

		send_packet_conference_to_C();
		ASSERT_TRUE(receive_packet_conference_on_A());
		ASSERT_TRUE(receive_packet_conference_on_B());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_topology_test_suite_t, conference_ABC_A_oneway_B_oneway_C_oneway_A)
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		ASSERT_TRUE(conference->context->mg_context_lock());

		h248::TopologyTriple one;
		one.term_from = conference->termination_info_1.termination_id;
		one.term_to = conference->termination_info_2.termination_id;
		one.direction = h248::TopologyDirection::Oneway;

		h248::TopologyTriple two;
		two.term_from = conference->termination_info_2.termination_id;
		two.term_to = conference->termination_info_3.termination_id;
		two.direction = h248::TopologyDirection::Oneway;

		h248::TopologyTriple three;
		three.term_from = conference->termination_info_3.termination_id;
		three.term_to = conference->termination_info_1.termination_id;
		three.direction = h248::TopologyDirection::Oneway;

		rtl::ArrayT<h248::TopologyTriple> topology_tr;
		topology_tr.add(one);
		topology_tr.add(two);
		topology_tr.add(three);

		h248::Field reply;
		conference->context->mg_context_set_topology(topology_tr, &reply);

		ASSERT_TRUE(conference->context->mg_context_unlock());

		send_packet_conference_to_A();
		ASSERT_TRUE(receive_packet_conference_on_B());
		ASSERT_FALSE(receive_packet_conference_on_C());

		send_packet_conference_to_B();
		ASSERT_FALSE(receive_packet_conference_on_A());
		ASSERT_TRUE(receive_packet_conference_on_C());

		send_packet_conference_to_C();
		ASSERT_TRUE(receive_packet_conference_on_A());
		ASSERT_FALSE(receive_packet_conference_on_B());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_topology_test_suite_t, conference_ABC_A_oneway_B_oneway_C)
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		ASSERT_TRUE(conference->context->mg_context_lock());

		h248::TopologyTriple one;
		one.term_from = conference->termination_info_1.termination_id;
		one.term_to = conference->termination_info_2.termination_id;
		one.direction = h248::TopologyDirection::Oneway;

		h248::TopologyTriple two;
		two.term_from = conference->termination_info_2.termination_id;
		two.term_to = conference->termination_info_3.termination_id;
		two.direction = h248::TopologyDirection::Oneway;

		h248::TopologyTriple three;
		three.term_from = conference->termination_info_3.termination_id;
		three.term_to = conference->termination_info_1.termination_id;
		three.direction = h248::TopologyDirection::Bothway;

		rtl::ArrayT<h248::TopologyTriple> topology_tr;
		topology_tr.add(one);
		topology_tr.add(two);
		topology_tr.add(three);

		h248::Field reply;
		conference->context->mg_context_set_topology(topology_tr, &reply);

		ASSERT_TRUE(conference->context->mg_context_unlock());

		send_packet_conference_to_A();
		ASSERT_TRUE(receive_packet_conference_on_B());
		ASSERT_TRUE(receive_packet_conference_on_C());

		send_packet_conference_to_B();
		ASSERT_FALSE(receive_packet_conference_on_A());
		ASSERT_TRUE(receive_packet_conference_on_C());

		send_packet_conference_to_C();
		ASSERT_TRUE(receive_packet_conference_on_A());
		ASSERT_FALSE(receive_packet_conference_on_B());
	}
	//--------------------------------------------------------------------------
	TEST_F(rtx_topology_test_suite_t, conference_ABC_A_isolate_B_oneway_C_bothway_A)
	{
		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		ASSERT_TRUE(conference->context->mg_context_lock());

		h248::TopologyTriple one;
		one.term_from = conference->termination_info_1.termination_id;
		one.term_to = conference->termination_info_2.termination_id;
		one.direction = h248::TopologyDirection::Isolate;

		h248::TopologyTriple two;
		two.term_from = conference->termination_info_2.termination_id;
		two.term_to = conference->termination_info_3.termination_id;
		two.direction = h248::TopologyDirection::Oneway;

		h248::TopologyTriple three;
		three.term_from = conference->termination_info_3.termination_id;
		three.term_to = conference->termination_info_1.termination_id;
		three.direction = h248::TopologyDirection::Bothway;

		rtl::ArrayT<h248::TopologyTriple> topology_tr;
		topology_tr.add(one);
		topology_tr.add(two);
		topology_tr.add(three);

		h248::Field reply;
		conference->context->mg_context_set_topology(topology_tr, &reply);

		ASSERT_TRUE(conference->context->mg_context_unlock());

		send_packet_conference_to_A();
		ASSERT_FALSE(receive_packet_conference_on_B());
		ASSERT_TRUE(receive_packet_conference_on_C());

		send_packet_conference_to_B();
		ASSERT_FALSE(receive_packet_conference_on_A());
		ASSERT_TRUE(receive_packet_conference_on_C());

		send_packet_conference_to_C();
		ASSERT_TRUE(receive_packet_conference_on_A());
		ASSERT_FALSE(receive_packet_conference_on_B());
	}
	//--------------------------------------------------------------------------
}
//------------------------------------------------------------------------------
