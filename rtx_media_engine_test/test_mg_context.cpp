﻿/* RTX H.248 Media Gate Engine Test Module 
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "test_mg_context.h"

 //------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
namespace rtx_media_engine
{
	void mg_context_test_t::SetUp()
	{
		if (m_context != nullptr)
		{
			DELETEO(m_context);
			m_context = nullptr;
		}
		rtp_port_manager_t::initialize(nullptr);
		m_context = NEW mge::MediaContext(nullptr, 1, 1);
	}
	//--------------------------------------------------------------------------
	//
	//--------------------------------------------------------------------------
	void mg_context_test_t::TearDown()
	{
		if (m_context != nullptr)
		{
			DELETEO(m_context);
			m_context = nullptr;
		}

		rtp_port_manager_t::destroy();
	}
	//--------------------------------------------------------------------------
	//
	//--------------------------------------------------------------------------
	TEST_F(mg_context_test_t, check_create_context)
	{
		ASSERT_TRUE(m_context != nullptr);
	}
	//--------------------------------------------------------------------------
	TEST_F(mg_context_test_t, check_context_init)
	{
		ASSERT_TRUE(m_context != nullptr);
		h248::Field reply;
		ASSERT_TRUE(m_context->mg_context_initialize(nullptr, &reply));
	}
}
//------------------------------------------------------------------------------
