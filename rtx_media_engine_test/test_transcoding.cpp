﻿/* RTX H.248 Media Gate Engine Test Module 
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "test_transcoding.h"
#include "net/net_socket.h"
#include "rtp_packet.h"
#include "test_utils.h"
#include "mock_callbacks.h"

 //------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
namespace rtx_media_engine
{
	test_transcoding_suite_t::test_transcoding_suite_t(){}
	//--------------------------------------------------------------------------
	test_transcoding_suite_t::~test_transcoding_suite_t(){}
	//--------------------------------------------------------------------------
	void test_transcoding_suite_t::SetUp()
	{
		memset(&m_transcoding_param, 0, sizeof(test_transcoding_param_t));
		(*GetParam())(&m_transcoding_param);
		rtl::String str;
		str << m_transcoding_param.payload_id_in;
		if (m_transcoding_param.rtpmap_out[0] != 0)
		{
			str << "(" << m_transcoding_param.rtpmap_in << ")";
		}
		str << " -> " << m_transcoding_param.payload_id_out;
		if (m_transcoding_param.rtpmap_out[0] != 0)
		{
			str << "(" << m_transcoding_param.rtpmap_out << ")";
		}
		str << "\n";
		PRINTF(str);
	}
	//--------------------------------------------------------------------------
	void test_transcoding_suite_t::TearDown(){}
	//--------------------------------------------------------------------------
	//
	//--------------------------------------------------------------------------
	void fill_data_PCMA_to_PCMU(test_transcoding_param_t* param)
	{
		if (param == nullptr)
		{
			return;
		}

		rtl::String file_encoding_pcma = get_path_to_tests_data();
		file_encoding_pcma << FS_PATH_DELIMITER << "pcma_to_pcmu.in";
		rtl::String file_decoding_pcmu = get_path_to_tests_data();
		file_decoding_pcmu << FS_PATH_DELIMITER << "pcma_to_pcmu.out";

		param->payload_in_size = read_data_from_file(file_encoding_pcma, param->payload_in, param->buffer_size);
		param->payload_out_size = read_data_from_file(file_decoding_pcmu, param->payload_out, param->buffer_size);

		param->payload_id_in = 8;
		param->payload_id_out = 0;
	}
	void fill_data_PCMU_to_PCMA(test_transcoding_param_t* param)
	{
		if (param == nullptr)
		{
			return;
		}

		rtl::String file_encoding_pcmu = get_path_to_tests_data();
		file_encoding_pcmu << FS_PATH_DELIMITER << "pcmu_to_pcma.in";
		rtl::String file_decoding_pcma = get_path_to_tests_data();
		file_decoding_pcma << FS_PATH_DELIMITER << "pcmu_to_pcma.out";

		param->payload_in_size = read_data_from_file(file_encoding_pcmu, param->payload_in, param->buffer_size);
		param->payload_out_size = read_data_from_file(file_decoding_pcma, param->payload_out, param->buffer_size);

		param->payload_id_in = 0;
		param->payload_id_out = 8;
	};
	//--------------------------------------------------------------------------
	//
	//--------------------------------------------------------------------------
	TEST_P(test_transcoding_suite_t, try_make_transcoding_call)
	{
		//----------------------------------------------------------------------
		// prepare network environment
		net_socket socket_sender(nullptr);
		net_socket socket_receiver(nullptr);
		rtl::ArrayT<net_socket*> list;
		list.add(&socket_sender);
		list.add(&socket_receiver);

		ip_address_t ipaddr(g_test_interface);

		EXPECT_TRUE(open_tests_sockets(&list, &ipaddr));

		EXPECT_NE(0, socket_sender.get_local_address()->get_port());
		EXPECT_NE(0, socket_receiver.get_local_address()->get_port());

		//----------------------------------------------------------------------
		// prepare context

		h248::IMediaGate* module = nullptr;
		mock_module_callback_interface_t module_callback;
		if (g_environment == nullptr)
		{
			module = create_media_gateway_module(g_test_log_path);
			ASSERT_TRUE(nullptr != module);

			EXPECT_CALL(module_callback, mge__event_raised()).Times(0);
			EXPECT_CALL(module_callback, mge__error_raised()).Times(0);

			EXPECT_TRUE(module->MediaGate_initialize(&module_callback));

			module->MediaGate_setInterfaceName(g_test_interface, "test");

			ASSERT_TRUE(module->MediaGate_addPortRangeRTP("test", 10000, 10000));
		}
		else
		{
			module = g_environment->get_module();
			ASSERT_TRUE(nullptr != module);
		}

		h248::IMediaContext* context = module->MediaGate_createContext();
		ASSERT_TRUE(nullptr != context);

		mock_termination_event_handler_t termination_callback;
		EXPECT_CALL(termination_callback, TerminationEvent_callback(::testing::_)).Times(0);

		h248::Field reply;
		ASSERT_TRUE(context->mg_context_initialize(&termination_callback, &reply));

		ASSERT_TRUE(context->mg_context_lock()) << "first lock";

		h248::TerminationID termId(h248::TerminationType::RTP, "test", 1);
		uint32_t termination_id_1 = context->mg_context_add_termination(termId, &reply);
		ASSERT_EQ(1, termination_id_1);

		rtl::String sdp_local_1;
		sdp_local_1 << "v=0" << "\r\n";
		sdp_local_1 << "c=IN IP4 $" << "\r\n";
		sdp_local_1 << "m=audio $ RTP/AVP " << m_transcoding_param.payload_id_out << "\r\n";
		if (m_transcoding_param.rtpmap_out && m_transcoding_param.rtpmap_out[0] != 0)
		{
			sdp_local_1 << m_transcoding_param.rtpmap_out << "\r\n";
		}
		sdp_local_1 << "a=sendrecv" << "\r\n";

		ASSERT_TRUE(context->mg_termination_set_Local_Remote(termination_id_1, 1, rtl::String::empty, sdp_local_1, &reply));

		ASSERT_TRUE(context->mg_termination_get_Local(termination_id_1, 1, sdp_local_1, &reply));

		termId.setId(2);
		uint32_t termination_id_2 = context->mg_context_add_termination(termId, &reply);
		ASSERT_EQ(2, termination_id_2);

		rtl::String sdp_local_2;
		sdp_local_2 << "v=0" << "\r\n";
		sdp_local_2 << "c=IN IP4 $" << "\r\n";
		sdp_local_2 << "m=audio $ RTP/AVP " << m_transcoding_param.payload_id_in << "\r\n";
		if (m_transcoding_param.rtpmap_in && m_transcoding_param.rtpmap_in[0] != 0)
		{
			sdp_local_2 << m_transcoding_param.rtpmap_in << "\r\n";
		}
		sdp_local_2 << "a=sendrecv" << "\r\n";

		rtl::String sdp_remote_2;
		sdp_remote_2 << "v=0" << "\r\n";
		sdp_remote_2 << "c=IN IP4 " << g_test_interface << "\r\n";
		sdp_remote_2 << "m=audio " << socket_sender.get_local_address()->get_port() << " RTP/AVP " << m_transcoding_param.payload_id_in << "\r\n";
		if (m_transcoding_param.rtpmap_in && m_transcoding_param.rtpmap_in[0] != 0)
		{
			sdp_remote_2 << m_transcoding_param.rtpmap_in << "\r\n";
		}
		sdp_remote_2 << "a=sendrecv" << "\r\n";

		ASSERT_TRUE(context->mg_termination_set_Local_Remote(termination_id_2, 1, sdp_remote_2, sdp_local_2, &reply));

		ASSERT_TRUE(context->mg_termination_get_Local(termination_id_2, 1, sdp_local_2, &reply));

		rtl::String sdp_remote_1;
		sdp_remote_1 << "v=0" << "\r\n";
		sdp_remote_1 << "c=IN IP4 " << g_test_interface << "\r\n";
		sdp_remote_1 << "m=audio " << socket_receiver.get_local_address()->get_port() << " RTP/AVP " << m_transcoding_param.payload_id_out << "\r\n";
		if (m_transcoding_param.rtpmap_out && m_transcoding_param.rtpmap_out[0] != 0)
		{
			sdp_remote_1 << m_transcoding_param.rtpmap_out << "\r\n";
		}
		sdp_remote_1 << "a=sendrecv" << "\r\n";

		ASSERT_TRUE(context->mg_termination_set_Local_Remote(termination_id_1, 1, sdp_remote_1, rtl::String::empty, &reply));

		ASSERT_TRUE(context->mg_termination_set_LocalControl_Mode(termination_id_1, 1, h248::TerminationStreamMode::SendRecv, &reply));
		ASSERT_TRUE(context->mg_termination_set_LocalControl_Mode(termination_id_2, 1, h248::TerminationStreamMode::SendRecv, &reply));

		ASSERT_TRUE(context->mg_context_unlock()) << "first unlock";

		//----------------------------------------------------------------------
		// send test packet

		int index_A = sdp_local_2.indexOf("m=audio ") + 8;
		int index_B = sdp_local_2.indexOf(" ", index_A);
		rtl::String port_str = sdp_local_2.substring(index_A, index_B - index_A);
		uint16_t port_send_to = (uint16_t)strtoul(port_str, nullptr, 10);
		socket_address sockaddr_send_to(&ipaddr, port_send_to);

		rtp_packet packet_send;
		packet_send.set_payload(m_transcoding_param.payload_in, m_transcoding_param.payload_in_size);
		packet_send.set_payload_type(m_transcoding_param.payload_id_in);
		packet_send.set_sequence_number(1);

		uint8_t buff_packet_send[1024] = { 0 };
		uint32_t buff_packet_write = packet_send.write_packet(buff_packet_send, 1024);

		EXPECT_EQ(buff_packet_write, socket_sender.send_to(buff_packet_send, buff_packet_write, &sockaddr_send_to));

		//----------------------------------------------------------------------
		// receive test packet

		socket_vector_t read;
		read.add(&socket_receiver);
		rtp_packet packet_receive;

		int count = 10;
		while (count > 0)
		{
			EXPECT_TRUE(net_socket::select(&read, NULL, NULL, 20, NULL));

			if (read.getCount() == 0)
			{
				rtl::Thread::sleep(100);
				read.add(&socket_receiver);
				count--;
			}
			else
			{
				socket_address sockaddr_from_to;
				uint8_t buff_packet_receive[1024] = { 0 };
				uint32_t read_size = read.getAt(0)->recv_from(buff_packet_receive, 1024, &sockaddr_from_to);
				EXPECT_NE(0, read_size);
				EXPECT_TRUE(packet_receive.read_packet(buff_packet_receive, read_size));
				break;
			}
		}

		EXPECT_NE(0, packet_receive.get_payload_length());
		EXPECT_EQ(0, memcmp(packet_receive.get_payload(), m_transcoding_param.payload_out, packet_receive.get_payload_length()))
			<< " " << m_transcoding_param.payload_id_in << " -> " << m_transcoding_param.payload_id_out;
		
		ASSERT_TRUE(context->mg_context_lock()) << "second lock";

		context->mg_context_remove_termination(termination_id_1);
		context->mg_context_remove_termination(termination_id_2);

		ASSERT_TRUE(context->mg_context_unlock()) << "second unlock";

		close_tests_sockets(&list);

		module->MediaGate_destroyContext(context);
	}
	//--------------------------------------------------------------------------
	//
	//--------------------------------------------------------------------------
}
//------------------------------------------------------------------------------
