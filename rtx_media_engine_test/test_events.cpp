﻿/* RTX H.248 Media Gate Engine Test Module 
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "test_events.h"
#include "rtp_packet.h"
#include "test_utils.h"

#include "gtest/gtest.h"
#include "gmock/gmock.h"

namespace rtx_media_engine
{
	rtx_events_test_suite_t::rtx_events_test_suite_t()
	{
		
	}
	//--------------------------------------------------------------------------
	rtx_events_test_suite_t::~rtx_events_test_suite_t()
	{
		
	}
	//--------------------------------------------------------------------------
	void rtx_events_test_suite_t::SetUp()
	{
		ASSERT_TRUE(nullptr != g_environment);
		h248::IMediaGate* module = g_environment->get_module();
		ASSERT_TRUE(nullptr != module);
		m_ipaddr.parse(g_test_interface);
		rtx_test_context_bothway_enviroment_info* bothway = g_environment->get_context_bothway();
		ASSERT_TRUE(nullptr != bothway);
		ASSERT_TRUE(nullptr != bothway->termination_callback);
	}
	//--------------------------------------------------------------------------
	void rtx_events_test_suite_t::TearDown()
	{
		
	}
	//--------------------------------------------------------------------------
	void rtx_events_test_suite_t::send_packets(int id)
	{
		rtx_test_context_bothway_enviroment_info* bothway = g_environment->get_context_bothway();
		ASSERT_TRUE(nullptr != bothway);
		ASSERT_NE(0, bothway->termination_info_1.port);
		ASSERT_TRUE(nullptr != bothway->termination_info_1.socket);

		socket_address sockaddr_send_to(&m_ipaddr, bothway->termination_info_1.port);

		int count = 0;
		while (count < 10)
		{
			uint8_t buff[4];
			buff[0] = id;
			if (count < 3)
			{
				buff[1] = 10;
				buff[2] = 0;
				buff[3] = 160;
			}
			else if (count == 3)
			{
				buff[1] = 10;
				buff[2] = 1;
				buff[3] = 64;
			}
			else if (count == 4)
			{
				buff[1] = 10;
				buff[2] = 1;
				buff[3] = 224;
			}
			else if (count == 5)
			{
				buff[1] = 10;
				buff[2] = 2;
				buff[3] = 128;
			}
			else if (count == 6)
			{
				buff[1] = 10;
				buff[2] = 3;
				buff[3] = 32;
			}
			else if (count == 7)
			{
				buff[1] = 10;
				buff[2] = 3;
				buff[3] = 192;
			}
			else if (count == 8)
			{
				buff[1] = 10;
				buff[2] = 4;
				buff[3] = 96;
			}
			else if (count > 8)
			{
				buff[1] = 138;
				buff[2] = 5;
				buff[3] = 0;
			}

			rtp_packet packet_send;
			packet_send.set_payload(buff, 4);
			packet_send.set_sequence_number(bothway->termination_info_1.seq);
			packet_send.set_payload_type(101);
			packet_send.set_timestamp(bothway->termination_info_1.timestamp);

			uint8_t buff_packet_send[1024] = { 0 };
			uint32_t buff_packet_write = packet_send.write_packet(buff_packet_send, 1024);

			EXPECT_EQ(buff_packet_write, bothway->termination_info_1.socket->send_to(buff_packet_send, buff_packet_write, &sockaddr_send_to));

			rtl::Thread::sleep(20);
			bothway->termination_info_1.seq++;
			count++;
		}

		bothway->termination_info_1.timestamp += 160;
	}

	MATCHER_P(dtmfAreEqual, rp, "")
	{
		h248::DTMF_EventParams* a1 = (h248::DTMF_EventParams*)arg;
		h248::DTMF_EventParams* a2 = (h248::DTMF_EventParams*)rp;
		return (a1->getDTMF() == a2->getDTMF());
	}

	void rtx_events_test_suite_t::test_dtmf(char dtmf, int i)
	{
		rtx_test_context_bothway_enviroment_info* bothway = g_environment->get_context_bothway();
		h248::DTMF_EventParams* param = NEW h248::DTMF_EventParams(1234+i, dtmf, 120);
		EXPECT_CALL(*bothway->termination_callback, TerminationEvent_callback(dtmfAreEqual(param))).Times(1);
		send_packets(i);
		rtl::Thread::sleep(100);
		DELETEO(param);
	}

	//--------------------------------------------------------------------------
	// 
	//--------------------------------------------------------------------------
	TEST_F(rtx_events_test_suite_t, try_bothway_dtmf_2833_event_1) { test_dtmf('1', 1); }
	//--------------------------------------------------------------------------
	TEST_F(rtx_events_test_suite_t, try_bothway_dtmf_2833_event_2) { test_dtmf('2', 2); }
	//--------------------------------------------------------------------------
	TEST_F(rtx_events_test_suite_t, try_bothway_dtmf_2833_event_3) { test_dtmf('3', 3); }
	//--------------------------------------------------------------------------
	TEST_F(rtx_events_test_suite_t, try_bothway_dtmf_2833_event_4) { test_dtmf('4', 4); }
	//--------------------------------------------------------------------------
	TEST_F(rtx_events_test_suite_t, try_bothway_dtmf_2833_event_5) { test_dtmf('5', 5); }
	//--------------------------------------------------------------------------
	TEST_F(rtx_events_test_suite_t, try_bothway_dtmf_2833_event_6) { test_dtmf('6', 6); }
	//--------------------------------------------------------------------------
	TEST_F(rtx_events_test_suite_t, try_bothway_dtmf_2833_event_7) { test_dtmf('7', 7); }
	//--------------------------------------------------------------------------
	TEST_F(rtx_events_test_suite_t, try_bothway_dtmf_2833_event_8) { test_dtmf('8', 8); }
	//--------------------------------------------------------------------------
	TEST_F(rtx_events_test_suite_t, try_bothway_dtmf_2833_event_9) { test_dtmf('9', 9); }
	//--------------------------------------------------------------------------
	TEST_F(rtx_events_test_suite_t, try_bothway_dtmf_2833_event_0) { test_dtmf('0', 0); }
	//--------------------------------------------------------------------------
	TEST_F(rtx_events_test_suite_t, try_bothway_dtmf_2833_event_star) { test_dtmf('*', 10); }
	//--------------------------------------------------------------------------
	TEST_F(rtx_events_test_suite_t, try_bothway_dtmf_2833_event_hash) { test_dtmf('#', 11); }
	//--------------------------------------------------------------------------
	TEST_F(rtx_events_test_suite_t, try_bothway_dtmf_2833_event_A) { test_dtmf('A', 12); }
	//--------------------------------------------------------------------------
	TEST_F(rtx_events_test_suite_t, try_bothway_dtmf_2833_event_B) { test_dtmf('B', 13); }
	//--------------------------------------------------------------------------
	TEST_F(rtx_events_test_suite_t, try_bothway_dtmf_2833_event_C) { test_dtmf('C', 14); }
	//--------------------------------------------------------------------------
	TEST_F(rtx_events_test_suite_t, try_bothway_dtmf_2833_event_D) { test_dtmf('D', 15); }
	//--------------------------------------------------------------------------
	TEST_F(rtx_events_test_suite_t, try_bothway_dtmf_2833_event_flash) { test_dtmf('!', 16); }
	//--------------------------------------------------------------------------
	TEST_F(rtx_events_test_suite_t, try_no_voice_event)
	{
		ASSERT_TRUE(nullptr != g_environment);
		h248::IMediaGate* module = g_environment->get_module();
		ASSERT_TRUE(nullptr != module);

		rtx_test_context_bothway_enviroment_info* bothway = g_environment->get_context_bothway();
		EXPECT_CALL(*bothway->termination_callback, TerminationEvent_callback(::testing::_)).Times(::testing::AnyNumber());

		rtx_test_context_conference_enviroment_info* conference = g_environment->get_context_conference();
		EXPECT_CALL(*conference->termination_callback, TerminationEvent_callback(::testing::_)).Times(::testing::AnyNumber());

		//------------------------------------------------------------------------------
		// prepare network environment
		net_socket socket_sender(nullptr);
		net_socket socket_receiver(nullptr);
		rtl::ArrayT<net_socket*> list;
		list.add(&socket_sender);
		list.add(&socket_receiver);

		ip_address_t ipaddr(g_test_interface);

		ASSERT_TRUE(open_tests_sockets(&list, &ipaddr));

		EXPECT_NE(0, socket_sender.get_local_address()->get_port());
		EXPECT_NE(0, socket_receiver.get_local_address()->get_port());

		//------------------------------------------------------------------------------
		// prepare context
		h248::IMediaContext* context = module->MediaGate_createContext();
		ASSERT_TRUE(nullptr != context);

		mock_termination_event_handler_t termination_callback;
		EXPECT_CALL(termination_callback, TerminationEvent_callback(::testing::_)).Times(3);
		
		h248::Field reply;

		ASSERT_TRUE(context->mg_context_initialize(&termination_callback, &reply));

		ASSERT_TRUE(context->mg_context_lock()) << "first lock";

		h248::TerminationID termId(h248::TerminationType::RTP, "test", 1);
		uint32_t termination_id_1 = context->mg_context_add_termination(termId, &reply);
		ASSERT_EQ(1, termination_id_1);

		rtl::String sdp_local_1;
		sdp_local_1 << "v=0" << "\r\n";
		sdp_local_1 << "c=IN IP4 $" << "\r\n";
		sdp_local_1 << "m=audio $ RTP/AVP 0 101" << "\r\n";
		sdp_local_1 << "a=rtpmap:101 telephone-event/8000" << "\r\n";
		sdp_local_1 << "a=sendrecv" << "\r\n";

		ASSERT_TRUE(context->mg_termination_set_Local_Remote(termination_id_1, 1, rtl::String::empty, sdp_local_1, &reply));

		ASSERT_TRUE(context->mg_termination_get_Local(termination_id_1, 1, sdp_local_1, &reply));

		termId.setId(2);
		uint32_t termination_id_2 = context->mg_context_add_termination(termId, &reply);
		ASSERT_EQ(2, termination_id_2);

		rtl::String sdp_local_2;
		sdp_local_2 << "v=0" << "\r\n";
		sdp_local_2 << "c=IN IP4 $" << "\r\n";
		sdp_local_2 << "m=audio $ RTP/AVP 0 101" << "\r\n";
		sdp_local_2 << "a=rtpmap:101 telephone-event/8000" << "\r\n";
		sdp_local_2 << "a=sendrecv" << "\r\n";

		rtl::String sdp_remote_2;
		sdp_remote_2 << "v=0" << "\r\n";
		sdp_remote_2 << "c=IN IP4 " << g_test_interface << "\r\n";
		sdp_remote_2 << "m=audio " << socket_receiver.get_local_address()->get_port() << " RTP/AVP 0 101" << "\r\n";
		sdp_remote_2 << "a=rtpmap:101 telephone-event/8000" << "\r\n";
		sdp_remote_2 << "a=sendrecv" << "\r\n";

		ASSERT_TRUE(context->mg_termination_set_Local_Remote(termination_id_2, 1, sdp_remote_2, sdp_local_2, &reply));

		ASSERT_TRUE(context->mg_termination_get_Local(termination_id_2, 1, sdp_local_2, &reply));

		rtl::String sdp_remote_1;
		sdp_remote_1 << "v=0" << "\r\n";
		sdp_remote_1 << "c=IN IP4 " << g_test_interface << "\r\n";
		sdp_remote_1 << "m=audio " << socket_sender.get_local_address()->get_port() << " RTP/AVP 0 101" << "\r\n";
		sdp_remote_1 << "a=rtpmap:101 telephone-event/8000" << "\r\n";
		sdp_remote_1 << "a=sendrecv" << "\r\n";

		ASSERT_TRUE(context->mg_termination_set_Local_Remote(termination_id_1, 1, sdp_remote_1, rtl::String::empty, &reply));

		ASSERT_TRUE(context->mg_termination_set_LocalControl_Mode(termination_id_1, 1, h248::TerminationStreamMode::SendRecv, &reply));
		ASSERT_TRUE(context->mg_termination_set_LocalControl_Mode(termination_id_2, 1, h248::TerminationStreamMode::SendRecv, &reply));

		ASSERT_TRUE(context->mg_context_unlock()) << "first unlock";

		//------------------------------------------------------------------------------
		// send test packet
		int index_A = sdp_local_2.indexOf("m=audio ") + 8;
		int index_B = sdp_local_2.indexOf(" ", index_A);
		rtl::String port_str = sdp_local_2.substring(index_A, index_B - index_A);
		uint16_t port_send_to = (uint16_t)strtoul(port_str, nullptr, 10);
		socket_address sockaddr_send_to(&ipaddr, port_send_to);

		int count = 0;
		uint32_t t = 11111100;
		while (count < 500)
		{
			uint8_t buff[5] = { "TEST" };

			rtp_packet packet_send;
			packet_send.set_payload(buff, 4);
			packet_send.set_sequence_number(count + 1);
			packet_send.set_payload_type(0);
			t += 160;
			packet_send.set_timestamp(t);

			uint8_t buff_packet_send[1024] = { 0 };
			uint32_t buff_packet_write = packet_send.write_packet(buff_packet_send, 1024);

			EXPECT_EQ(buff_packet_write, socket_sender.send_to(buff_packet_send, buff_packet_write, &sockaddr_send_to));

			rtl::Thread::sleep(20);

			count++;
		}

		rtl::Thread::sleep(2500);

		t += (160 * 60);
		while (count < 1100)
		{
			uint8_t buff[5] = { "TEST" };

			rtp_packet packet_send;
			packet_send.set_payload(buff, 4);
			packet_send.set_sequence_number(count + 1);
			packet_send.set_payload_type(0);
			t += 160;
			packet_send.set_timestamp(t);

			uint8_t buff_packet_send[1024] = { 0 };
			uint32_t buff_packet_write = packet_send.write_packet(buff_packet_send, 1024);

			EXPECT_EQ(buff_packet_write, socket_sender.send_to(buff_packet_send, buff_packet_write, &sockaddr_send_to));

			rtl::Thread::sleep(20);

			count++;
		}

		rtl::Thread::sleep(2500);

		ASSERT_TRUE(context->mg_context_lock()) << "second lock";

		context->mg_context_remove_termination(termination_id_1);
		context->mg_context_remove_termination(termination_id_2);

		ASSERT_TRUE(context->mg_context_unlock()) << "second unlock";

		close_tests_sockets(&list);

		module->MediaGate_destroyContext(context);
	}
}
//------------------------------------------------------------------------------
