﻿/* RTX H.248 Media Gate Engine Test Module 
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "../rtx_rtplib/rtp_port_manager.h"

namespace rtx_media_engine
{
	class mock_rtp_port_manager_t
	{
	public:
		mock_rtp_port_manager_t(rtl::Logger* log) { rtp_port_manager_t::initialize(log); }
		virtual ~mock_rtp_port_manager_t() {}

		MOCK_METHOD2(get_channel, rtp_channel_t*(const char* address_key, rtp_channel_type_t type));
		MOCK_METHOD1(release_channel, void(rtp_channel_t* channel));
		MOCK_METHOD1(start_channel_listen, bool(rtp_channel_t* channel));
		MOCK_METHOD1(stop_channel_listen, void(rtp_channel_t* channel));
		MOCK_METHOD2(compare_channels, bool(rtp_channel_t* channel_1, rtp_channel_t* channel_2));
	};
}
//------------------------------------------------------------------------------
