/* RTX H.248 Media Gate. H.263 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_video_encoder_h263.h"
#include "mg_video_decoder_h263.h"
//-----------------------------------------------
//
//-----------------------------------------------
#define TDAV_H263_GOP_SIZE_IN_SECONDS		25
#define RTP_PAYLOAD_SIZE				   750
#define H263P_HEADER_SIZE					 2
#define H263_HEADER_MODE_A_SIZE				 4
#define H263_HEADER_MODE_B_SIZE				 8
#define H263_HEADER_MODE_C_SIZE				12

#define CODEC_FLAG_H263P_UMV 1
#define CODEC_FLAG_H263P_SLICE_STRUCT 1
#define CODEC_FLAG_H263P_AIV 1

#define H263_DEF_CIF_W	352
#define H263_DEF_CIF_H	288
//-----------------------------------------------
// defined in decoder.cpp
//-----------------------------------------------
extern void avcodec_get_frame_defaults(AVFrame *frame);

static volatile uint32_t s_id_gen = 1;
//-----------------------------------------------
//
//-----------------------------------------------
mg_video_encoder_h263_t::mg_video_encoder_h263_t(rtl::Logger& log, rtx_video_encoder_cb_t cb, void* usr, int type, AVCodecID codecId) : m_log(log)
{
	m_id = std_interlocked_inc(&s_id_gen);

	m_type = type;
	m_initialized = false;
	m_rtp = { nullptr, 0 };

	if (!(m_codec = avcodec_find_encoder(AV_CODEC_ID_H263))) // codecId
	{
		MLOG_ERROR(LOG_PREFIX, "id:% 4u : ctor : failed to find encoder %u", m_id, codecId);
		return;
	}

	LOG_PREFIX = codecId == AV_CODEC_ID_H263 ? "H263-E" : "H263P-E";

	m_context = nullptr;
	m_picture = nullptr;
	m_buffer = nullptr;
	m_buffer_size = 0;
	m_force_idr = false;
	m_quality = 5;
	m_max_bw_kpbs = INT32_MAX;
	m_width = H263_DEF_CIF_W;
	m_height = H263_DEF_CIF_H;
	m_fps = 15;
	m_last_clock = rtl::DateTime::getTicks();
	m_last_ts = (rand() ^ 0x1B5D) & 0x000FFFFF;
	m_max_br = 0;
	m_max_mbps = 0;
	m_chroma = media::VideoChroma::YUV420p;
	m_flip = false;
	m_callback = cb;
	m_user = usr;
}
//-----------------------------------------------
//
//-----------------------------------------------
mg_video_encoder_h263_t::~mg_video_encoder_h263_t()
{
	m_codec = nullptr;

	// FFMpeg resources are destroyed by close()
	FREE(m_rtp.ptr);
	m_rtp.size = 0;
}
//-----------------------------------------------
//
//-----------------------------------------------
bool mg_video_encoder_h263_t::initialize(const media::PayloadFormat* fmt)
{
	MLOG_CALL(LOG_PREFIX, "id:%04u : initialize with '%s'", m_id, (const char*)fmt->getFmtp());

	return initialize_codec();
}

void mg_video_encoder_h263_t::set_dimention(int width, int height)
{
	m_scale = false;

	switch (width + height)
	{
	case 224:	//  128 x   96 ok
	case 320:	//  176 x  144 ok
	case 640:	//  352 x  288 ok
	case 1280:	//  704 x  576 ok
	case 2560:	// 1408 x 1152 ok
		m_width = width;
		m_height = height;
		break;
	default:	// transformation
		{
			/*mmt_video_size_t cif = { 0, 0 };
			//mmt_get_nearest_cif({ width, height }, cif);*/
			m_scale = true;
			m_width = H263_DEF_CIF_W;
			m_height = H263_DEF_CIF_H;
		}
	}
}

bool mg_video_encoder_h263_t::initialize_codec()
{
	MLOG_CALL(LOG_PREFIX, "id:%04u : initialize codec", m_id);

	m_context = avcodec_alloc_context3(m_codec);
	avcodec_get_context_defaults3(m_context, m_codec);

	m_context->pix_fmt = AV_PIX_FMT_YUV420P;
	m_context->time_base.num = 1;
	m_context->time_base.den = m_fps;
	m_context->width = m_width;
	m_context->height = m_height;

	m_context->qmin = 10;
	m_context->qmax = 51;
	m_context->mb_decision = FF_MB_DECISION_RD;
	
	int max_bw_kpbs = VIDEO_CLAMP(0,
		media::getVideoBandwidthKbps2(m_width, m_height, m_fps),
		m_max_bw_kpbs);
	
	m_context->bit_rate = (max_bw_kpbs * 4 * 1024);// bps

	m_context->rtp_payload_size = RTP_PAYLOAD_SIZE;
	m_context->opaque = nullptr;
	m_context->gop_size = (m_fps * TDAV_H263_GOP_SIZE_IN_SECONDS);
	m_context->flags |= CODEC_FLAG_QSCALE;
	m_context->global_quality = FF_QP2LAMBDA * m_quality;
	m_context->max_b_frames = 0;

	// Picture (YUV 420)
	m_picture = av_frame_alloc();
	avcodec_get_frame_defaults(m_picture);

	//size = avpicture_get_size(AV_PIX_FMT_YUV420P, m_context->width, m_context->height);
	//m_buffer = (uint8_t*)malloc(size);

	// RTP Callback
	switch (m_type)
	{
	case 0: {
		// H263 - 1996
		break;
	}
	case 1: {
		// H263 - 1998
#if defined(CODEC_FLAG_H263P_UMV)
		m_context->flags |= CODEC_FLAG_H263P_UMV;		// Annex D+
#endif
		m_context->flags |= CODEC_FLAG_AC_PRED;			// Annex I and T
		m_context->flags |= CODEC_FLAG_LOOP_FILTER;		// Annex J
#if defined(CODEC_FLAG_H263P_SLICE_STRUCT)
		m_context->flags |= CODEC_FLAG_H263P_SLICE_STRUCT;	// Annex K
#endif
#if defined(CODEC_FLAG_H263P_AIV)
		m_context->flags |= CODEC_FLAG_H263P_AIV;			// Annex S
#endif
		break;
	}
	case 2: {
		// H263 - 2000
#if defined(CODEC_FLAG_H263P_UMV)
		m_context->flags |= CODEC_FLAG_H263P_UMV;		// Annex D+
#endif
		m_context->flags |= CODEC_FLAG_AC_PRED;			// Annex I and T
		m_context->flags |= CODEC_FLAG_LOOP_FILTER;		// Annex J
#if defined(CODEC_FLAG_H263P_SLICE_STRUCT)
		m_context->flags |= CODEC_FLAG_H263P_SLICE_STRUCT;	// Annex K
#endif
#if defined(CODEC_FLAG_H263P_AIV)
		m_context->flags |= CODEC_FLAG_H263P_AIV;			// Annex S
#endif
		break;
	}
	}
	// Open encoder
	int ret = avcodec_open2(m_context, m_codec, nullptr);

	if (ret < 0)
	{
		char tmp[256] = { 0 };
		av_make_error_string(tmp, 256, ret);
		MLOG_ERROR(LOG_PREFIX, "id:% 4u : initialize_codec : failed to open H.263 codec -- error:%d '%s'", m_id, ret, tmp);
		return false;
	}

	MLOG_CALL(LOG_PREFIX, "id:% 4u : initialize_codec : bitrate:%d bps", m_id, m_context->bit_rate);

	return ret >= 0;
}
//-----------------------------------------------
//
//-----------------------------------------------
void mg_video_encoder_h263_t::destroy()
{
	if (m_context)
	{
		avcodec_close(m_context);
		av_free(m_context);
		m_context = nullptr;
	}

	if (m_picture)
	{
		av_free(m_picture);
		m_picture = nullptr;
	}

	if (m_buffer)
	{
		FREE(m_buffer);
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
const char*	mg_video_encoder_h263_t::getEncoding()
{
	return m_type == 0 ? "H263" : m_type == 1 ? "H263-1998" : "H263-2000";
}
//-----------------------------------------------
//
//-----------------------------------------------
bool mg_video_encoder_h263_t::init(rtx_video_encoder_cb_t cb, void* user)
{
	m_callback = cb;
	m_user = user;
	return true;
}
//-----------------------------------------------
//
//-----------------------------------------------
void mg_video_encoder_h263_t::reset()
{
	//
}
//-----------------------------------------------
//
//-----------------------------------------------
bool mg_video_encoder_h263_t::encode(const media::VideoImage* image)
{
	int ret = 1;
	int size;

	if (!m_initialized)
	{
		set_dimention(image->getWidth(), image->getHeight());
		initialize_codec();
		m_initialized = true;
	}

	MLOG_CALL(LOG_PREFIX, "id:% 4u : encode : image %dx%d size:%d -> %dx%d", m_id, image->getWidth(), image->getHeight(), image->getImageSize(), m_width, m_height);

	media::VideoImage w_image;
	const media::VideoImage* p_image;

	if (m_scale)
	{
		image->transformTo(w_image, m_width, m_height, media::Transform::Stretch);
		p_image = &w_image;
	}
	else
	{
		p_image = image;
	}

	// wrap yuv420 buffer
	size = avpicture_fill((AVPicture *)m_picture, p_image->getYData(), AV_PIX_FMT_YUV420P, m_context->width, m_context->height);
	
	if (size <= 0) // != image->getImageSize()
	{
		/* guard */
		char t[256];
		av_strerror(size, t, 256);
		MLOG_ERROR(LOG_PREFIX, "id:% 4u : encode : preparing av picture failed with error (%d) '%s'", m_id, size, t);
		return false;
	}

	m_picture->pict_type = m_force_idr ? AV_PICTURE_TYPE_I : AV_PICTURE_TYPE_NONE;
	m_picture->pts = AV_NOPTS_VALUE;
	m_picture->quality = m_context->global_quality;
	m_picture->width = m_context->width;
	m_picture->height = m_context->height;
	m_picture->format = m_context->pix_fmt;

	AVPacket pkt;

	av_init_packet(&pkt);

	check_and_resize_buffer(size);

	pkt.data = m_buffer;
	pkt.size = size;

	int got_picture;
	ret = avcodec_encode_video2(m_context, &pkt, m_picture, &got_picture);

	if (ret >= 0)
	{
		if (m_type == 0)
		{
			packetize_rtp2190(pkt.data, pkt.size);
			//packetize_rtp2190(image->get_image_data(), image->getImageSize());
		}
		else
		{
			packetize_rtp4629(pkt.data, pkt.size);
			//packetize_rtp4629(image->get_image_data(), image->getImageSize());
		}
	}
	else
	{
		char t[256];
		av_strerror(ret, t, 256);
		MLOG_ERROR(LOG_PREFIX, "id:% 4u : encode : ffmpeg encode failed with error (%d) '%s'", m_id, ret, t);
		return false;
	}

	m_force_idr = false;

	return true;
}
//-----------------------------------------------
//
//-----------------------------------------------
void mg_video_encoder_h263_t::check_and_resize_buffer(int size)
{
	if (m_buffer_size < size)
	{
		m_buffer = (uint8_t*)realloc(m_buffer, size);
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
void mg_video_encoder_h263_t::packetize_rtp2190(const uint8_t* pdata, int size)
{
	int32_t last_index = 0;

	if (size >= RTP_PAYLOAD_SIZE)
	{

		uint32_t current = rtl::DateTime::getTicks();
		m_last_ts += (current - m_last_clock) * 90;
		m_last_clock = current;

		for (int i = 4; i < (size - 4); i++)
		{
			if (pdata[i] == 0x00 && pdata[i + 1] == 0x00 && pdata[i + 2] != 0)
			{
				/* PSC or (GBSC) found */
				if ((i - last_index) >= RTP_PAYLOAD_SIZE || true/* FIXME */)
				{
					send_rtp_h263(pdata + last_index, (i - last_index), m_last_ts, (last_index == size));
					last_index = i;
				}
			}
		}
	}

	if (last_index < size)
	{
		send_rtp_h263(pdata + last_index, (size - last_index), m_last_ts, true);
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
const uint8_t *ff_h263_find_resync_marker_reverse(const uint8_t *start, const uint8_t *end)
{
	const uint8_t *p = end - 1;
	start += 1; /* Make sure we never return the original start. */
	for (; p > start; p -= 2)
	{
		if (!*p)
		{
			if (!p[1] && p[2]) return p;
			else if (!p[-1] && p[1]) return p - 1;
		}
	}

	return end;
}
//-----------------------------------------------
//
//-----------------------------------------------
void mg_video_encoder_h263_t::packetize_rtp4629(const uint8_t* pdata, int size)
{
	uint32_t current = rtl::DateTime::getTicks();
	int ms_diff = current - m_last_clock;

	m_last_ts += ms_diff * 90;
	m_last_clock = current;

	uint8_t priv_data[2048];
	int len, max_packet_size;
	uint8_t *q;

	max_packet_size = RTP_PAYLOAD_SIZE;

	while (size > 0)
	{
		q = priv_data;
		if (size >= 2 && (pdata[0] == 0) && (pdata[1] == 0))
		{
			*q++ = 0x04;
			pdata += 2;
			size -= 2;
		}
		else
		{
			*q++ = 0;
		}

		*q++ = 0;

		len = FFMIN(max_packet_size - 2, size);

		/* Look for a better place to split the frame into packets. */
		if (len < size)
		{
			const uint8_t *end = ff_h263_find_resync_marker_reverse(pdata, pdata + len);
			len = (int)(end - pdata);
		}

		memcpy(q, pdata, len);
		q += len;

		send_rtp_h263p(priv_data, (int)(q - priv_data), m_last_ts, len == size);

		pdata += len;
		size -= len;
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
void mg_video_encoder_h263_t::send_rtp_h263(const uint8_t* data, int size, uint32_t ts, bool marker)
{
	const uint8_t* pdata = (uint8_t*)data;

	if (m_rtp.size < (size + H263_HEADER_MODE_A_SIZE))
	{
		m_rtp.ptr = (uint8_t*)REALLOC(m_rtp.ptr, (size + H263_HEADER_MODE_A_SIZE));
		m_rtp.size = (size + H263_HEADER_MODE_A_SIZE);
	}
	memcpy((m_rtp.ptr + H263_HEADER_MODE_A_SIZE), data, size);

	/* http://eu.sabotage.org/www/ITU/H/H0263e.pdf section 5.1
	* 5.1.1 Picture Start Code (PSC) (22 bits) - PSC is a word of 22 bits. Its value is 0000 0000 0000 0000 1 00000.

	*
	* 5.1.1 Picture Start Code (PSC) (22 bits)
	* 5.1.2 Temporal Reference (TR) (8 bits)
	* 5.1.3 Type Information (PTYPE) (Variable Length)
	*	� Bit 1: Always "1", in order to avoid start code emulation.
	*	� Bit 2: Always "0", for distinction with Recommendation H.261.

	*	� Bit 3: Split screen indicator, "0" off, "1" on.
	*	� Bit 4: Document camera indicator, "0" off, "1" on.
	*	� Bit 5: Full Picture Freeze Release, "0" off, "1" on.
	*	� Bits 6-8: Source Format, "000" forbidden, "001" sub-QCIF, "010" QCIF, "011" CIF,
	"100" 4CIF, "101" 16CIF, "110" reserved, "111" extended PTYPE.
	If bits 6-8 are not equal to "111", which indicates an extended PTYPE (PLUSPTYPE), the following
	five bits are also present in PTYPE:
	� Bit 9: Picture Coding Type, "0" INTRA (I-picture), "1" INTER (P-picture).
	� Bit 10: Optional Unrestricted Motion Vector mode (see Annex D), "0" off, "1" on.
	� Bit 11: Optional Syntax-based Arithmetic Coding mode (see Annex E), "0" off, "1" on.
	� Bit 12: Optional Advanced Prediction mode (see Annex F), "0" off, "1" on.
	� Bit 13: Optional PB-frames mode (see Annex G), "0" normal I- or P-picture, "1" PB-frame.
	*/
	if (pdata[0] == 0x00 && pdata[1] == 0x00 && (pdata[2] & 0xfc) == 0x80) { /* PSC */
		/* RFC 2190 -5.1 Mode A
		0                   1                   2                   3
		0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		|F|P|SBIT |EBIT | SRC |I|U|S|A|R      |DBQ| TRB |    TR         |
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

		SRC : 3 bits
		Source format, bit 6,7 and 8 in PTYPE defined by H.263 [4], specifies
		the resolution of the current picture.

		I:  1 bit.
		Picture coding type, bit 9 in PTYPE defined by H.263[4], "0" is
		intra-coded, "1" is inter-coded.
		*/

		// PDATA[4] ======> Bits 3-10 of PTYPE
		uint32_t rtp_hdr = 0;
		uint8_t format, pict_type;

		// Source Format = 4,5,6
		format = (pdata[4] & 0x3C) >> 2;
		// Picture Coding Type = 7
		pict_type = (pdata[4] & 0x02) >> 1;
		// RTP mode A header
		((uint8_t*)&rtp_hdr)[1] = (format << 5) | (pict_type << 4);
		memcpy(m_rtp.ptr, &rtp_hdr, sizeof(rtp_hdr));
	}

	// Send data over the network
	if (m_callback != nullptr)
	{
		//MLOG_CALL(LOG_PREFIX, "packetize packet for %d bytes ts:% 10u m:%s", size + H263_HEADER_MODE_A_SIZE, ts, STR_BOOL(marker));
		m_callback(m_rtp.ptr, size + H263_HEADER_MODE_A_SIZE, ts, marker, m_user);
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
void mg_video_encoder_h263_t::send_rtp_h263p(const uint8_t* data, int size, /*bool frag, */uint32_t ts, bool marker)
{
	uint8_t* pdata = (uint8_t*)data;

	// Send data over the network
	if (m_callback)
	{
		//MLOG_CALL(LOG_PREFIX, "packetize packet for %d bytes ts:% 10u m:%s", size + H263P_HEADER_SIZE, ts, STR_BOOL(marker));

		m_callback(data, size, ts, marker, m_user);
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
mg_video_encoder_h263p_t::mg_video_encoder_h263p_t(rtl::Logger& log, rtx_video_encoder_cb_t cb, void* usr, int type, AVCodecID avCodec) :
	mg_video_encoder_h263_t(log, cb, usr, type, avCodec)
{
}
//-----------------------------------------------
//
//-----------------------------------------------
mg_video_encoder_h263p_t::~mg_video_encoder_h263p_t()
{
}
//-----------------------------------------------
//
//-----------------------------------------------
mg_video_encoder_h263pp_t::mg_video_encoder_h263pp_t(rtl::Logger& log, rtx_video_encoder_cb_t cb, void* usr) :
	mg_video_encoder_h263p_t(log, cb, usr, 2, AV_CODEC_ID_H263P)
{
}
//-----------------------------------------------
//
//-----------------------------------------------
mg_video_encoder_h263pp_t::~mg_video_encoder_h263pp_t()
{
}
//-----------------------------------------------
