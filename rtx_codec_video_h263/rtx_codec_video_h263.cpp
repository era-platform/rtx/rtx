/* RTX H.248 Media Gate. H.263 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtx_codec_video_h263.h"
#include "mg_video_decoder_h263.h"
#include "mg_video_encoder_h263.h"

#ifdef __cplusplus
extern "C" {
#endif
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavfilter/avfiltergraph.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavutil/opt.h>
#include <libavutil/pixdesc.h>

#include <libavutil/imgutils.h>
#include <libavutil/parseutils.h>
#include <libswscale/swscale.h>

#ifdef __cplusplus
}
#endif

//-----------------------------------------------
//
//-----------------------------------------------
//rtl::Logger h263_log;

//-----------------------------------------------
//
//-----------------------------------------------
RTX_MODULE_API bool initlib()
{
//	h263_log.create(Log.get_folder(), "h263", LOG_APPEND);

	av_register_all();
	avfilter_register_all();

	return true;
}
//-----------------------------------------------
//
//-----------------------------------------------
RTX_MODULE_API void freelib()
{
//	h263_log.destroy();
}
//-----------------------------------------------
//
//-----------------------------------------------
RTX_MODULE_API const char* get_codec_list()
{
	return "H263 H263-1998 H263-2000";
}
//-----------------------------------------------
//
//-----------------------------------------------
RTX_MODULE_API bool get_video_codec_info(const media::PayloadFormat* format, struct rtx_picture_info_t* info)
{
	return false;
}
//-----------------------------------------------
//
//-----------------------------------------------
RTX_MODULE_API mg_video_decoder_t* create_video_decoder(const media::PayloadFormat* format)
{
	mg_video_decoder_h263_t* dec = nullptr;

	if (rtl::String::compare(format->getEncoding(), "H263", true) == 0)
	{
		dec = NEW mg_video_decoder_h263_t(Log);
	}
	else if (rtl::String::compare(format->getEncoding(), "H263-1998", true) == 0)
	{
		dec = NEW mg_video_decoder_h263p_t(Log);
	}
	else if (rtl::String::compare(format->getEncoding(), "H263-2000", true) == 0)
	{
		dec = NEW mg_video_decoder_h263pp_t(Log);
	}

	if (!dec->initialize(format))
	{
		DELETEO(dec);
		return nullptr;
	}

	return dec;
}
//-----------------------------------------------
//
//-----------------------------------------------
RTX_MODULE_API void release_video_decoder(mg_video_decoder_t* decoder)
{
	DELETEO(decoder);
}
//-----------------------------------------------
//
//-----------------------------------------------
RTX_MODULE_API mg_video_encoder_t* create_video_encoder(const media::PayloadFormat* format, rtx_video_encoder_cb_t cb, void* usr)
{
	mg_video_encoder_h263_t* enc = nullptr;

	if (rtl::String::compare(format->getEncoding(), "H263", true) == 0)
	{
		enc = NEW mg_video_encoder_h263_t(Log, cb, usr);
	}
	else if (rtl::String::compare(format->getEncoding(), "H263-1998", true) == 0)
	{
		enc = NEW mg_video_encoder_h263p_t(Log, cb, usr);
	}
	else if (rtl::String::compare(format->getEncoding(), "H263-2000", true) == 0)
	{
		enc = NEW mg_video_encoder_h263pp_t(Log, cb, usr);
	}

	if (!enc->initialize(format))
	{
		DELETEO(enc);
		return nullptr;
	}

	return enc;
}
//-----------------------------------------------
//
//-----------------------------------------------
RTX_MODULE_API void release_video_encoder(mg_video_encoder_t* encoder)
{
	DELETEO(encoder);
}
