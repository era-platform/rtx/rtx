/* RTX H.248 Media Gate. H.263 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavutil/mem.h>
}

//-----------------------------------------------
//
//-----------------------------------------------
class mg_video_encoder_h263_t :	public mg_video_encoder_t
{
private:
	rtl::Logger& m_log;
	uint32_t m_id;
	const char* LOG_PREFIX;

	int m_type;
	bool m_scale;
	bool m_initialized;

	struct {
		uint8_t* ptr;
		size_t size;
	} m_rtp;

	// Encoder
	AVCodec* m_codec;
	AVCodecContext* m_context;
	AVFrame* m_picture;
	
	uint8_t* m_buffer;
	int m_buffer_size;

	bool m_force_idr;
	int32_t m_quality; // [1-31]
	int32_t m_max_bw_kpbs;

	// tmedia_codec_video_s
	uint32_t m_width;
	uint32_t m_height;
	uint32_t m_fps;
	uint32_t m_last_clock;
	uint32_t m_last_ts;
	uint32_t m_max_br;
	uint32_t m_max_mbps;
	media::VideoChroma m_chroma;
	bool m_flip;
	
	rtx_video_encoder_cb_t m_callback;
	void* m_user;

public:
	mg_video_encoder_h263_t(rtl::Logger& log, rtx_video_encoder_cb_t cb, void* usr, int type = 0, AVCodecID codecId = AV_CODEC_ID_H263);
	virtual ~mg_video_encoder_h263_t();

	bool initialize(const media::PayloadFormat* fmt);
	void destroy();

	virtual const char*	getEncoding();
	virtual bool init(rtx_video_encoder_cb_t cb, void* user);
	virtual void reset();
	virtual bool encode(const media::VideoImage* picture);

protected:
	void set_dimention(int width, int height);
	bool initialize_codec();
	void packetize_rtp2190(const uint8_t* picture, int size);
	void packetize_rtp4629(const uint8_t* picture, int size);

	void send_rtp_h263(const uint8_t* pdata, int size, uint32_t ts, bool last);
	void send_rtp_h263p(const uint8_t* pdata, int size, uint32_t ts, bool last);

	void check_and_resize_buffer(int size);
};

class mg_video_encoder_h263p_t : public mg_video_encoder_h263_t
{
public:
	mg_video_encoder_h263p_t(rtl::Logger& log, rtx_video_encoder_cb_t cb, void* usr, int type = 1, AVCodecID avCodec = AV_CODEC_ID_H263P);
	virtual ~mg_video_encoder_h263p_t();
};

class mg_video_encoder_h263pp_t : public mg_video_encoder_h263p_t
{
public:
	mg_video_encoder_h263pp_t(rtl::Logger& log, rtx_video_encoder_cb_t cb, void* usr);
	virtual ~mg_video_encoder_h263pp_t();
};
