/* RTX H.248 Media Gate. H.263 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavutil/mem.h>
}

//#define VIDEO_TEST

//-----------------------------------------------
//
//-----------------------------------------------
class mg_video_decoder_h263_t : public mg_video_decoder_t
{
protected:
	uint32_t m_id;

	rtl::Logger& m_log;
	// h263
	int m_type;	// 0 - H263-1996, 1 - H263-1998, 2 - H263-2000
	void* m_rtp;

	AVCodec* m_codec;
	AVCodecContext* m_context;
	AVFrame* m_picture;

	uint8_t* m_accumulator;
	uint8_t m_ebit;
	size_t m_accumulator_pos;
	uint16_t m_last_seq;

	uint8_t* m_pict_buffer;
	size_t m_pict_buffer_size;

	//! preferred video size
	media::VideoStandard m_pref_size;

#if defined(VIDEO_TEST)
	FILE* m_outfile;
#endif

	const char* LOG_PREFIX;

public:
	mg_video_decoder_h263_t(rtl::Logger& log, int type = 0, AVCodecID avCodec = AV_CODEC_ID_H263);
	virtual ~mg_video_decoder_h263_t();

	bool initialize(const media::PayloadFormat* fmt);
	void destroy();

	virtual const char*	getEncoding();
	virtual media::VideoImage* decode(const rtp_packet* packet);

private:
	void setFmtp(const char* fmt);
};

class mg_video_decoder_h263p_t : public mg_video_decoder_h263_t
{
public:
	mg_video_decoder_h263p_t(rtl::Logger& log, int type = 1, AVCodecID avCodec = AV_CODEC_ID_H263P);
	virtual ~mg_video_decoder_h263p_t();

	virtual const char*	getEncoding();
	virtual media::VideoImage* decode(const rtp_packet* packet);
};

class mg_video_decoder_h263pp_t : public mg_video_decoder_h263p_t
{
public:
	mg_video_decoder_h263pp_t(rtl::Logger& log);
	virtual ~mg_video_decoder_h263pp_t();

	virtual const char*	getEncoding();
};
