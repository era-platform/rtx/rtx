/* RTX H.248 Media Gate. H.263 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavutil/mem.h>
}
#include "mg_video_decoder_h263.h"
//-----------------------------------------------
//
//-----------------------------------------------
#define TDAV_H263_GOP_SIZE_IN_SECONDS	  25
#define RTP_PAYLOAD_SIZE				 750
#define H263P_HEADER_SIZE				   2
#define H263_HEADER_MODE_A_SIZE			   4
#define H263_HEADER_MODE_B_SIZE			   8
#define H263_HEADER_MODE_C_SIZE			  12
#define SQCIF_WIDTH						 128
#define SQCIF_HEIGHT					  96
#define qCIF_WIDTH						 176
#define qCIF_HEIGHT						 144
#define CIF_WIDTH						 352
#define CIF_HEIGHT						 288

//-----------------------------------------------
//
//-----------------------------------------------
void avcodec_get_frame_defaults(AVFrame *frame);

static volatile uint32_t s_id_gen = 1;
//-----------------------------------------------
//
//-----------------------------------------------
mg_video_decoder_h263_t::mg_video_decoder_h263_t(rtl::Logger& log, int type, AVCodecID codecid) : m_log(log)
{
	m_id = std_interlocked_inc(&s_id_gen);

	m_type = type;
	m_rtp = nullptr;

	LOG_PREFIX = codecid == AV_CODEC_ID_H263 ? "H263-D" : "H263P-D";

	if (!(m_codec = avcodec_find_decoder(AV_CODEC_ID_H263))) // codecid
	{
		MLOG_ERROR(LOG_PREFIX, "id:%04u : ctor : failed to find decoder %u", m_id, codecid); // AV_CODEC_ID_H263
	}

	m_context = nullptr;
	m_picture = nullptr;
	m_accumulator = nullptr;
	m_ebit = 0;
	m_accumulator_pos = 0;
	//m_last_seq = 0;
	m_pict_buffer = nullptr;
	m_pict_buffer_size = 0;
	m_pref_size = media::VideoStandard::CIF;

#if defined (VIDEO_TEST)
	m_outfile = nullptr;
#endif
}
//-----------------------------------------------
//
//-----------------------------------------------
mg_video_decoder_h263_t::~mg_video_decoder_h263_t()
{
	m_codec = nullptr;
	// FFMpeg resources are destroyed by close()
	if (m_rtp != nullptr)
	{
		FREE(m_rtp);
		m_rtp = nullptr;
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
bool mg_video_decoder_h263_t::initialize(const media::PayloadFormat* fmt)
{
	int ret, size;

	MLOG_CALL(LOG_PREFIX, "id:%04u : initialize with '%s'", m_id, (const char*)fmt->getFmtp());

	setFmtp(fmt->getFmtp());

	if (m_context != nullptr)
	{
		MLOG_ERROR(LOG_PREFIX, "id:%04u : initialize : decoder already opened", m_id);
		return false;
	}

	// open

	m_context = avcodec_alloc_context3(m_codec);

	avcodec_get_context_defaults3(m_context, nullptr);

	m_context->pix_fmt = AV_PIX_FMT_YUV420P;
	m_context->width = CIF_WIDTH;		// CIF
	m_context->height = CIF_HEIGHT;	// CIF

	// Picture (YUV 420)
	m_picture = av_frame_alloc();

	avcodec_get_frame_defaults(m_picture);
	size = avpicture_get_size(AV_PIX_FMT_YUV420P, m_context->width, m_context->height);
	m_accumulator = (uint8_t*)MALLOC(size + FF_INPUT_BUFFER_PADDING_SIZE);

	// Open decoder
	if ((ret = avcodec_open2(m_context, m_codec, nullptr)) < 0)
	{
		char tmp[256] = { 0 };
		av_make_error_string(tmp, 256, ret);
		MLOG_ERROR(LOG_PREFIX, "id:%04u : initialize : failed to open H.263 codec -- error:(%u) '%s'", m_id, ret, tmp);
		return false;
	}

	//m_last_seq = 0;

#if defined(VIDEO_TEST)

	static int nnn = 1;
	char fname[128];
	std_snprintf(fname, 128, "c:\\temp\\outfile_h263_%d.raw", nnn++);
	m_outfile = fopen(fname, "wb");

	bool b = (m_outfile != nullptr);

	MLOG_CALL(LOG_PREFIX, "open video raw file %s (fmt %s pt:%u)", STR_BOOL(b), fmt->getEncoding(), fmt->get_id());

#endif

	MLOG_CALL(LOG_PREFIX, "id:%04u : initialize : done", m_id);

	return true;
}
//-----------------------------------------------
//
//-----------------------------------------------
void mg_video_decoder_h263_t::destroy()
{
	MLOG_CALL(LOG_PREFIX, "id:%04u : destroy...", m_id);

	if (m_context)
	{
		avcodec_close(m_context);
		av_free(m_context);
		m_context = nullptr;
	}
	
	if (m_picture)
	{
		av_free(m_picture);
		m_picture = nullptr;
	}
	
	if (m_accumulator != nullptr)
	{
		FREE(m_accumulator);
		m_accumulator_pos = 0;
	}

#if defined(VIDEO_TEST)
	fclose(m_outfile);
	m_outfile = nullptr;
#endif

	MLOG_CALL(LOG_PREFIX, "id:%04u : destroy : done", m_id);
}
//-----------------------------------------------
//
//-----------------------------------------------
const char*	mg_video_decoder_h263_t::getEncoding()
{
	return "H263";
}
//-----------------------------------------------
//
//-----------------------------------------------
media::VideoImage* mg_video_decoder_h263_t::decode(const rtp_packet* packet)
{
	const uint8_t* in_data = packet->get_payload();
	int in_size = packet->get_payload_length();
	media::VideoImage* image = nullptr;

	uint8_t F, P, sbit, ebit;
	const uint8_t* pdata = in_data;
	const uint8_t* pay_ptr;
	size_t pay_size;
	size_t hdr_size;
	size_t xsize, retsize = 0;
	int got_picture_ptr;
	int ret;

	//tdav_codec_h263_t* h263 = (tdav_codec_h263_t*)self;
	uint16_t rtp_seq = packet->get_sequence_number();
	uint32_t rtp_ts = packet->getTimestamp();
	uint32_t rtp_ssrc = packet->get_ssrc();
	bool rtp_marker = packet->get_marker();

	bool is_idr = false;

	MLOG_CALL(LOG_PREFIX, "id:% 4u : decode : ssrc:%08x seq:% 6d m:% 5s ts:% 10u", m_id, rtp_ssrc, rtp_seq, STR_BOOL(rtp_marker), rtp_ts);

	/*	RFC 2190
	get F and P bits, used to determine the header Mode (A, B or C)
	F: 1 bit
	The flag bit indicates the mode of the payload header. F=0, mode A;
	F=1, mode B or mode C depending on P bit defined below.
	P: 1 bit
	Optional PB-frames mode as defined by the H.263 [4]. "0" implies
	normal I or P frame, "1" PB-frames. When F=1, P also indicates modes:
	mode B if P=0, mode C if P=1.

	I:  1 bit.
	Picture coding type, bit 9 in PTYPE defined by H.263[4], "0" is
	intra-coded, "1" is inter-coded.
	*/
	F = *pdata >> 7;
	P = (*pdata >> 6) & 0x01;

	/* SBIT and EBIT */
	sbit = (*pdata >> 3) & 0x0F;
	ebit = (*pdata & 0x07);

	if (F == 0) {
		/*	MODE A
		0                   1                   2                   3
		0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		|F|P|SBIT |EBIT | SRC |I|U|S|A|R      |DBQ| TRB |    TR         |
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		*/
		hdr_size = H263_HEADER_MODE_A_SIZE;
		is_idr = (in_size >= 2) && !(pdata[1] & 0x10) /* I==1 */;
	}
	else if (P == 0) { // F=1 and P=0
		/* MODE B
		0                   1                   2                   3
		0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		|F|P|SBIT |EBIT | SRC | QUANT   |  GOBN   |   MBA           |R  |
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		|I|U|S|A| HMV1        | VMV1        | HMV2        | VMV2        |
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		*/
		hdr_size = H263_HEADER_MODE_B_SIZE;
		is_idr = (in_size >= 5) && !(pdata[4] & 0x80) /* I==1 */;
	}
	else { // F=1 and P=1
		/* MODE C
		0                   1                   2                   3
		0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		|F|P|SBIT |EBIT | SRC | QUANT   |  GOBN   |   MBA           |R  |
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		|I|U|S|A| HMV1        | VMV1        | HMV2        | VMV2        |
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		| RR                                  |DBQ| TRB |    TR         |
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		*/
		hdr_size = H263_HEADER_MODE_C_SIZE;
		is_idr = (in_size >= 5) && !(pdata[4] & 0x80) /* I==1 */;
	}

	/* Check size */
	if (in_size < hdr_size)
	{
		MLOG_WARN(LOG_PREFIX, "id:% 4u : decode : packet too short (%d bytes)", m_id, in_size);
		return 0;
	}

	pay_ptr = (pdata + hdr_size);
	pay_size = (in_size - hdr_size);

	xsize = avpicture_get_size(m_context->pix_fmt, m_context->width, m_context->height);

	//MLOG_CALL(LOG_PREFIX, " Picture width %d height %d --> xsize = %I64u", m_context->width, m_context->height, xsize);

	/* Packet lost? */
	//if (m_last_seq != (rtp_seq - 1) && m_last_seq)
	//{
	//	if (m_last_seq == rtp_seq)
	//	{
	//		// Could happen on some stupid emulators
	//		MLOG_WARN(LOG_PREFIX, "id% 4u : decode : packet duplicated, seq_num=%d", m_id, rtp_seq);
	//		return 0;
	//	}

	//	MLOG_WARN(LOG_PREFIX, "id% 4u : decode : packet loss, seq numbers : last:%d, curr:%d, loss %d", m_id, rtp_ssrc, m_last_seq, rtp_seq, rtp_seq - m_last_seq);
	//}
	
	//m_last_seq = rtp_seq;

	if ((int)(m_accumulator_pos + pay_size) <= xsize)
	{
		if ((m_ebit + sbit) == 8)
		{
			/* Perfect one Byte to clean up */
			if (m_accumulator_pos)
			{
				((uint8_t*)m_accumulator)[m_accumulator_pos - 1] =
					(((uint8_t*)m_accumulator)[m_accumulator_pos - 1] & (0xFF << m_ebit)) |
					(*pay_ptr & (0xFF >> sbit));
			}
			pay_ptr++, pay_size--;
		}
		m_ebit = ebit;

		memcpy(&((uint8_t*)m_accumulator)[m_accumulator_pos], pay_ptr, pay_size);
		m_accumulator_pos += pay_size;
	}
	else
	{
		MLOG_WARN(LOG_PREFIX, "id:% 4u : decode : buffer overflow!", m_id);
		m_accumulator_pos = 0;
		return nullptr;
	}

	if (rtp_marker)
	{
		AVPacket packet;
		/* allocate destination buffer */
		av_init_packet(&packet);
		
		packet.size = (int)m_accumulator_pos;
		packet.data = m_accumulator;
		ret = avcodec_decode_video2(m_context, m_picture, &got_picture_ptr, &packet);

		if (ret < 0)
		{
			char t[256];
			av_strerror(ret, t, 256);
			MLOG_ERROR(LOG_PREFIX, "id:% 4u : decode : failed to decode the buffer with error code (%d) '%s'", m_id, ret, t);
		}
		else if (got_picture_ptr) 
		{
			MLOG_CALL(LOG_PREFIX, "id:% 4u : decode : picture decoded to %dx%d (%d)", m_id, m_context->width, m_context->height, ret);

			retsize = xsize;
			// Is it IDR frame?

			image = NEW media::VideoImage(m_context->width, m_context->height);
			
			/* copy picture into a linear buffer */
			if ((ret = avpicture_layout((AVPicture *)m_picture, m_context->pix_fmt, (int)m_context->width, (int)m_context->height,
				image->getImageBuffer(), (int)retsize)) < 0)
			{
				char t[256];
				av_strerror(ret, t, 256);
				MLOG_WARN(LOG_PREFIX, "id:% 4u : decode : failed to layout picture with error (%d) '%s'", m_id, ret, t);
			}
			//else
			//{
			//	image->update_buffer_size(ret);
			//}
		}

		#if defined(VIDEO_TEST)
			fwrite(image->get_image_buffer(), retsize, 1, m_outfile);
		#endif
		
		/* in all cases: reset accumulator */
		m_accumulator_pos = 0;
	}

	MLOG_CALL(LOG_PREFIX, "id:% 4u : decode : %s", m_id, retsize > 0 ? "image ready" : "packet stored");

	return image; // retsize > 0;
}
//-----------------------------------------------
//
//-----------------------------------------------
void mg_video_decoder_h263_t::setFmtp(const char* fmtp)
{
	int fps;
	media::VideoSize vsize = media::parseVideoFMTP(fmtp, m_pref_size, &fps);

	if (vsize.width == 0)
	{
		MLOG_ERROR(LOG_PREFIX, "id% 4u : setFmtp : failed to match fmtp=%s", m_id, fmtp);
		return;
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
void avcodec_get_frame_defaults(AVFrame *frame)
{
#if LIBAVCODEC_VERSION_MAJOR >= 55
	// extended_data should explicitly be freed when needed, this code is unsafe currently
	// also this is not compatible to the <55 ABI/API
	if (frame->extended_data != frame->data && 0)
		av_freep(&frame->extended_data);
#endif

	memset(frame, 0, sizeof(AVFrame));
	av_frame_unref(frame);
}
//-----------------------------------------------
//
//-----------------------------------------------
mg_video_decoder_h263p_t::mg_video_decoder_h263p_t(rtl::Logger& log, int type, AVCodecID avCodec) : mg_video_decoder_h263_t(log, type, avCodec)
{
}
//-----------------------------------------------
//
//-----------------------------------------------
mg_video_decoder_h263p_t::~mg_video_decoder_h263p_t()
{
}
//-----------------------------------------------
//
//-----------------------------------------------
const char*	mg_video_decoder_h263p_t::getEncoding()
{
	return "H2673-1998";
}
//-----------------------------------------------
//
//-----------------------------------------------
media::VideoImage* mg_video_decoder_h263p_t::decode(const rtp_packet* packet)
{
	const uint8_t* in_data = packet->get_payload();
	size_t in_size = packet->get_payload_length();
	media::VideoImage* image = nullptr;

	uint8_t P, V, PLEN, PEBIT;
	uint8_t* pdata = (uint8_t*)in_data;
	int hdr_size = H263P_HEADER_SIZE;

	MLOG_CALL(LOG_PREFIX, "id:% 4u : decode : ssrc:%08x seq:% 6d m:% 5s ts:% 10u",
		m_id, packet->get_ssrc(), packet->get_sequence_number(),
		STR_BOOL(packet->get_marker()), packet->getTimestamp());

	/*
	rfc4629 - 5.1.  General H.263+ Payload Header

	0                   1
	0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5
	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	|   RR    |P|V|   PLEN    |PEBIT|
	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	*/
	P = (pdata[0] & 0x04) >> 2;
	V = (pdata[0] & 0x02) >> 1;
	PLEN = (((pdata[0] & 0x01) << 5) | pdata[1] >> 3);
	PEBIT = pdata[1] & 0x07;

	if (V)
	{
		/*
		Indicates the presence of an 8-bit field containing information
		for Video Redundancy Coding (VRC), which follows immediately after
		the initial 16 bits of the payload header, if present.  For syntax
		and semantics of that 8-bit VRC field, see Section 5.2.
		*/
	}
	
	if (PLEN)
	{
		/*
		Length, in bytes, of the extra picture header.  If no extra
		picture header is attached, PLEN is 0.  If PLEN>0, the extra
		picture header is attached immediately following the rest of the
		payload header.  Note that the length reflects the omission of the
		first two bytes of the picture start code (PSC).  See Section 6.1.
		*/
		hdr_size += PLEN;
		if (PEBIT)
		{
			/*
			Indicates the number of bits that shall be ignored in the last
			byte of the picture header.  If PLEN is not zero, the ignored bits
			shall be the least significant bits of the byte.  If PLEN is zero,
			then PEBIT shall also be zero.
			*/
			MLOG_WARN(LOG_PREFIX, "id:% 4u : decode : PEBIT ignored", m_id);
		}
	}

	if (P)
	{
		/* MUST be done after PLEN and PEBIT */
		/*
		Indicates the picture start or a picture segment (GOB/Slice) start
		or a video sequence end (EOS or EOSBS).  Two bytes of zero bits
		then have to be prefixed to the payload of such a packet to
		compose a complete picture/GOB/slice/EOS/EOSBS start code.  This
		bit allows the omission of the two first bytes of the start codes,
		thus improving the compression ratio.
		*/
		hdr_size -= 2;
		pdata[hdr_size] = 0x00, pdata[hdr_size + 1] = 0x00;
	}

	size_t p_size = avpicture_get_size(m_context->pix_fmt, m_context->width, m_context->height);
	size_t pay_size = (in_size - hdr_size);

	if ((int)(m_accumulator_pos + pay_size) <= p_size)
	{
		/* PEBIT is ignored */
		memcpy(&m_accumulator[m_accumulator_pos], (pdata + hdr_size), pay_size);
		m_accumulator_pos += pay_size;
	}
	else
	{
		MLOG_WARN(LOG_PREFIX, "id:% 4u : decode : buffer overflow", m_id);
		m_accumulator_pos = 0;
		return nullptr; // false;
	}

	if (packet->get_marker())
	{
		AVPacket avpacket;

		/* decode the picture */
		av_init_packet(&avpacket);
		avpacket.size = (int)m_accumulator_pos;
		avpacket.data = m_accumulator;

		int got_picture_ptr;
		int ret = avcodec_decode_video2(m_context, m_picture, &got_picture_ptr, &avpacket);

		if (ret < 0)
		{
			char t[256];
			av_strerror(ret, t, 256);
			MLOG_WARN(LOG_PREFIX, "id:% 4u : decode : failed to decode the buffer (%d) %s", m_id, ret, t);
		}
		else if (got_picture_ptr)
		{
			image = NEW media::VideoImage(m_context->width, m_context->height);

			/* copy picture into a linear buffer */
			if ((ret = avpicture_layout((AVPicture *)m_picture, m_context->pix_fmt, (int)m_context->width, (int)m_context->height,
				image->getImageBuffer(), (int)p_size)) < 0)
			{
				char t[256];
				av_strerror(ret, t, 256);
				MLOG_WARN(LOG_PREFIX, "id:% 4u : decode : failed to layout picture with error (%d) '%s'", m_id, ret, t);
			}
			else
			{
				p_size = ret;
				MLOG_CALL(LOG_PREFIX, "id:% 4u : decode : image ready %dx%d size %d", m_id, image->getWidth(), image->getHeight(), image->getImageSize());
			}
#if defined(VIDEO_TEST)
			fwrite(image->get_image_buffer(), ret, 1, m_outfile);
#endif
			/* in all cases: reset accumulator */
		}
		
		m_accumulator_pos = 0;

		return ret < 0 ? nullptr : image; // p_size > 0;
	}

	MLOG_CALL(LOG_PREFIX, "id:% 4u : decode : %s", m_id, p_size > 0 ? "image ready" : "packet stored");

	return nullptr; // false;
}
//-----------------------------------------------
//
//-----------------------------------------------
mg_video_decoder_h263pp_t::mg_video_decoder_h263pp_t(rtl::Logger& log) : mg_video_decoder_h263p_t(log, 2, AV_CODEC_ID_H263P)
{
}
//-----------------------------------------------
//
//-----------------------------------------------
mg_video_decoder_h263pp_t::~mg_video_decoder_h263pp_t()
{
}
//-----------------------------------------------
//
//-----------------------------------------------
const char*	mg_video_decoder_h263pp_t::getEncoding()
{
	return "H2673-2000";
}
//-----------------------------------------------
