/* RTX H.248 Media Gate. H.264 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavutil/mem.h>
#include <x264.h>
}

extern int g_rtx_encoder_counter;

//-----------------------------------------------
//
//-----------------------------------------------
class mg_video_encoder_h264_v2_t : public mg_video_encoder_t
{
	uint32_t m_id;

	x264_t* m_x264;

	x264_param_t* m_x264_param;

	x264_picture_t* m_x264_picture;

	video_settings m_settings;

	bool m_enc_initialized;

	rtl::Logger& m_log;

	AVFrame* m_frame;

	t_video_nal_array m_nal_list;

	bool m_has_delayed;//���������� ���� �� ������ ����������� �������� ������

	rtx_video_encoder_cb_t m_callback;
	void* m_callback_user;

	// packetization

	struct rtp_pack
	{
		char*		data;
		unsigned	size;

		bool		marker;
		unsigned	timestamp;
	};

	typedef rtl::ArrayT<rtp_pack*> rtp_pack_array;
	rtp_pack_array m_packet_pool;
	rtp_pack_array m_current_packets;

	unsigned m_payload_length;

	char* m_buffer;
	unsigned m_buffer_pos;
	unsigned m_buffer_size;

	unsigned m_framerate;
	unsigned m_timestamp;

public:
	mg_video_encoder_h264_v2_t(rtl::Logger& log, rtx_video_encoder_cb_t cb, void* usr);
	virtual ~mg_video_encoder_h264_v2_t();

	bool initialize(const media::PayloadFormat* fmt);
	void destroy();

	virtual const char*	getEncoding();
	virtual bool init(rtx_video_encoder_cb_t cb, void* user);
	virtual void reset();
	virtual bool encode(const media::VideoImage* picture);

private:
	bool update(video_settings& settings);
	bool encode(bool null);

	bool init_x264_params();
	void free_x264_params();

	bool init_x264_picture();
	void free_x264_picture();

	bool init_x264_encoder();
	void free_x264_encoder();

	bool init_av_frame();
	void free_av_frame();

	bool init_rgb_yuv_converter();
	void free_rgb_yuv_converter();

	bool prepare_rgb_frame(const char* data, unsigned len);
	bool prepare_x264_picture();

	void clear_nal_pool();

	// packetization
	bool packetize_frame(const t_video_nal_array& nal_queue);
	void packetize_nal(const uint8_t* nal, unsigned size);

	void clear_current_packets();
	void clear_packet_pool();

	void add_single(const uint8_t* data, unsigned size);
	void add_fua(const uint8_t* data, unsigned size);

	void check_buffer_space(unsigned need_space);

	rtp_pack* get_free_packet();
};

