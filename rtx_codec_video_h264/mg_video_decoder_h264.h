/* RTX H.248 Media Gate. H.264 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavutil/mem.h>
}

#include "h264_common.h"

extern int g_rtx_decoder_counter;

#define VIDEO_TEST
//-----------------------------------------------
//
//-----------------------------------------------
class mg_video_decoder_h264_t : public mg_video_decoder_t, public h264_common_t
{
	rtl::Logger& m_log;

	AVCodec* m_codec;
	AVCodecContext* m_context;
	AVFrame* m_picture;

	void* m_accumulator;
	size_t m_accumulator_pos;
	size_t m_accumulator_size;
	uint16_t m_last_seq;

#if defined(VIDEO_TEST)
	FILE* m_outfile;
#endif


public:
	mg_video_decoder_h264_t(rtl::Logger& log);
	virtual ~mg_video_decoder_h264_t();

	bool initialize(const media::PayloadFormat* fmt);
	void destroy();

	virtual const char*	getEncoding();
	virtual media::VideoImage* decode(const rtp_packet* rtp_pack);

private:
	int init(profile_idc_t profile);
};
//-----------------------------------------------
