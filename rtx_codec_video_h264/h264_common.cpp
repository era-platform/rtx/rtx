/* RTX H.248 Media Gate. H.264 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "stdafx.h"

#include "h264_common.h"
//-----------------------------------------------
// Table A-1 � Level limits
//-----------------------------------------------
extern const int32_t MaxMBPS[H264_LEVEL_MAX_COUNT] = { 1485, 1485, 3000, 6000, 11880, 11880, 19800, 20250, 40500, 108000, 216000, 245760, 245760, 522240, 589824, 983040 };
extern const int32_t MaxFS[H264_LEVEL_MAX_COUNT] = { 99, 99, 396, 396, 396, 396, 792, 1620, 1620, 3600, 5120, 8192, 8192, 8704, 22080, 36864 };
extern const int32_t MaxDpbMbs[H264_LEVEL_MAX_COUNT] = { 396, 396, 900, 2376, 2376, 2376, 4752, 8100, 8100, 18000, 20480, 32768, 32768, 34816, 110400, 184320 };
extern const int32_t MaxBR[H264_LEVEL_MAX_COUNT] = { 64, 128, 192, 384, 768, 2000, 4000, 4000, 10000, 14000, 20000, 20000, 50000, 50000, 135000, 240000 };
extern const int32_t MaxCPB[H264_LEVEL_MAX_COUNT] = { 175, 350, 500, 1000, 2000, 2000, 4000, 4000, 10000, 14000, 20000, 25000, 62500, 62500, 135000, 240000 };
//-----------------------------------------------
// From "enum level_idc_e" to zero-based index
// level_idc = (level.prefix * 10) + level.suffix. For example: 3.1 = 31. Exception for 1b = 9.
// usage: int32_t maxDpbMbs = MaxDpbMbs[HL_CODEC_264_LEVEL_TO_ZERO_BASED_INDEX[level_idc]];
//-----------------------------------------------
extern const int32_t H264_LEVEL_TO_ZERO_BASED_INDEX[255] = {
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 2, 3, 4, 4, 4, 4, 4,
	4, 4, 5, 6, 7, 7, 7, 7, 7, 7, 7, 7, 8, 9, 10, 10, 10, 10, 10,
	10, 10, 10, 11, 12, 13, 13, 13, 13, 13, 13, 13, 13, 14, 15, 15,
	15, 15, 15, 15, 15, 15, 15, 15, 16, 16, 16, 16, 16, 16, 16, 16, 16,
	16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
	16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
	16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
	16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
	16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
	16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
	16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
	16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
	16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
	16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
	16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
	16, 16, 16, 16, 16, 16, 16, 16, 16
};
//-----------------------------------------------
//
//-----------------------------------------------
extern const h264_common_level_size_xt h264_common_level_sizes[] =
{
	{ level_idc_1_0, 128, 96, 99 },
#if 0
	{ level_idc_1_b, 128, 96, 99 },
#endif
	{ level_idc_1_1, 176, 144, 396 },
	{ level_idc_1_2, 320, 240, 396 },
	{ level_idc_1_3, 352, 288, 396 },
	{ level_idc_2_0, 352, 288, 396 },
	{ level_idc_2_1, 480, 352, 792 }, // Swap(352x480)
	{ level_idc_2_2, 720, 480, 1620 },
	{ level_idc_3_0, 720, 576, 1620 },
	{ level_idc_3_1, 1280, 720, 3600 },
	{ level_idc_3_2, 1280, 720, 5120 },
	{ level_idc_4_0, 2048, 1024, 8192 },
	{ level_idc_4_1, 2048, 1024, 8192 },
	{ level_idc_4_2, 2048, 1080, 8704 },
	{ level_idc_5_0, 2560, 1920, 22080 },
	{ level_idc_5_1, 3840, 2160, 32400 },
	{ level_idc_5_2, 4096, 2048, 32768 }
};
//-----------------------------------------------
//
//-----------------------------------------------
int h264_common_size_from_level(level_idc_t level, uint32_t *width, uint32_t *height)
{
	for (size_t i = 0; i < sizeof(h264_common_level_sizes) / sizeof(h264_common_level_sizes[0]); ++i)
	{
		if (h264_common_level_sizes[i].level == level)
		{
			*width = h264_common_level_sizes[i].width;
			*height = h264_common_level_sizes[i].height;
		
			return 0;
		}
	}

	return -1;
}
//-----------------------------------------------
//
//-----------------------------------------------
int h264_common_size_from_fs(uint32_t maxFS, uint32_t *width, uint32_t *height)
{
	size_t i;
	int ret = -1;
	for (i = 0; i < sizeof(h264_common_level_sizes) / sizeof(h264_common_level_sizes[0]); ++i)
	{
		if (h264_common_level_sizes[i].maxFS <= maxFS)
		{
			*width = h264_common_level_sizes[i].width;
			*height = h264_common_level_sizes[i].height;
			ret = 0;
		}
		else
		{
			break;
		}
	}
	return ret;
}
//-----------------------------------------------
//
//-----------------------------------------------
int h264_common_level_from_size(uint32_t width, uint32_t height, level_idc_t *level)
{
	size_t i;
	uint32_t maxFS = (((width + 15) >> 4) * ((height + 15) >> 4));
	static const size_t __tdav_codec_h264_common_level_sizes_count =
		sizeof(h264_common_level_sizes) / sizeof(h264_common_level_sizes[0]);

	for (i = 0; i < __tdav_codec_h264_common_level_sizes_count; ++i)
	{
		/*h264_common_level_sizes[i].maxFS*/
		if (((h264_common_level_sizes[i].width * h264_common_level_sizes[i].height) >> 8) >= maxFS)
		{
			*level = h264_common_level_sizes[i].level;
			return 0;
		}
	}
	
	//TSK_DEBUG_WARN("Failed to find default level for size=(%ux%u)", width, height);
	*level = h264_common_level_sizes[__tdav_codec_h264_common_level_sizes_count - 1].level;
	return 0;
}
//-----------------------------------------------
//
//-----------------------------------------------
bool h264_common_get_profile_and_level(const char* value, profile_idc_t *profile, level_idc_t *level)
{
	*profile = profile_idc_none;
	*level = level_idc_none;

	profile_idc_t p_idc;
	level_idc_t l_idc;

	h264_parse_profile(value, &p_idc, nullptr, &l_idc);

	switch (p_idc)
	{
	case profile_idc_baseline:
	case profile_idc_main:
		*profile = p_idc;
		*level = l_idc;
		return true;
	case profile_idc_extended:
	case profile_idc_high:
	default:
		/* Not supported */
		break;
	}

	return false;
}
//-----------------------------------------------
//
//-----------------------------------------------
h264_common_t::h264_common_t(rtx_video_encoder_cb_t cb, void* usr)
{

	m_width = 640;// width;
	m_height = 480;// height;

	m_chroma = media::VideoChroma::YUV420p;
	m_flip = false;

	//common
	m_profile = profile_idc_main;
	m_profile_iop = 0;
	m_level = level_idc_t::level_idc_2_2;
	m_maxFS = 0;

	m_pack_mode_remote = packetization_mode_t::Unknown_Mode; // remote packetization mode
	m_pack_mode_local = packetization_mode_t::Unknown_Mode; // local packetization mode

	m_enc_callback = cb;
	m_user = usr;

	m_rtp = { nullptr, 0 };


	level_idc_t level;
	// because at this step 'tmedia_codec_init()' is not called yet and we need default size to compute the H.264 level
	if (m_width == 0) // xxx || TMEDIA_CODEC_VIDEO(h264)->in.width == 0
	{
		
		media::VideoStandard pref_size = media::VideoStandard::CIF; // tmedia_defaults_get_pref_video_size();
		media::VideoSize vsize = media::videoStandardDimention(pref_size);
	}

	m_last_clock = rtl::DateTime::getTicks();
	m_last_ts = (rand() ^ 0x1B5D) & 0x000FFFFF;

	m_maxFS = (((m_width + 15) >> 4) * ((m_height + 15) >> 4));
	if ((h264_common_level_from_size(m_width, m_height, &level)) == 0)
	{
		m_maxFS = MIN((int32_t)m_maxFS, MaxFS[H264_LEVEL_TO_ZERO_BASED_INDEX[level]]);
		m_level = level;
	}
	m_profile_iop = 0x80;
	m_pack_mode_local = H264_PACKETIZATION_MODE;
	m_pack_mode_remote = Unknown_Mode;

	m_enc_callback = nullptr;
	m_user = nullptr;
	m_rtp = { nullptr, 0 };
}
//-----------------------------------------------
//
//-----------------------------------------------
h264_common_t::~h264_common_t()
{
	//TSK_DEBUG_INFO("tdav_codec_h264_common_deinit");

	//tmedia_codec_video_deinit(TMEDIA_CODEC_VIDEO(h264));
	FREE(m_rtp.ptr);
	m_rtp.size = 0;
}
//-----------------------------------------------
//
//-----------------------------------------------
bool h264_common_t::set_profile_level_id(const char* value)
{
	bool ret = false;
	profile_idc_t profile;
	level_idc_t level;

	/* Check whether the profile match (If the profile is missing, then we consider that it's ok) */
	if (h264_common_get_profile_and_level(value, &profile, &level) != 0)
	{
		//TSK_DEBUG_ERROR("Not valid profile-level: %s", att_value);
		return false;
	}

	if (m_profile != profile)
	{
		return false;
	}
	
	if (m_level != level)
	{
		// change the output size only when the remote party request lower level. If it request higher (or same) level then, we send our preferred size.
		if (m_level > level)
		{
			uint32_t width, height;
			m_level = MIN(m_level, level);
			if (h264_common_size_from_level(m_level, &width, &height) != 0)
			{
				return false;
			}
			// Do not change our size if it match the requested level
			if (width < m_width || height < m_height)
			{
				// Set "out" size. We must not send more than "MaxFS".
				// Default "out" is equal to the preferred sized and initialized in init().
				// "TANDBERG/4120 (X7.2.2)" will terminate the call if frame size > maxFS
				m_width = MIN(m_width, width);
				m_height = MIN(m_height, height);
				ret = true;
			}
			// Set default "in". Will be updated after receiving the first frame.
			m_width = width;
			m_height = height;
		}
	}

	return ret;
}
//-----------------------------------------------
//
//-----------------------------------------------
bool h264_common_t::sdp_att_match(const char* att_name, const char* att_value)
{
	bool ret = true;
	bool outsize_changed = false;

	//TSK_DEBUG_INFO("[H.264] Trying to match [%s:%s]", att_name, att_value);

	if (std_stricmp(att_name, "fmtp") == 0)
	{
		rtl::str_scanner_t sn_ctx = { 0 };

		if (rtl::str_scanner_init(&sn_ctx, att_value))
		{
			do
			{
				/* e.g. profile-level-id=42e00a; packetization-mode=1; max-br=452; max-mbps=11880 */

				if (std_stricmp(sn_ctx.name, "profile-level-id") == 0)
				{
					outsize_changed = set_profile_level_id(sn_ctx.value);
				}
				else if (std_stricmp(sn_ctx.name, "max-br") == 0)
				{
					m_max_br = 1000 * strtoul(sn_ctx.value, nullptr, 10);
				}
				else if (std_stricmp(sn_ctx.name, "max-fs") == 0)
				{
					uint32_t width_max, height_max, maxFS, currFS;
					uint32_t val_int = strtoul(sn_ctx.value, nullptr, 10);

					currFS = (m_width * m_height) >> 8;
					maxFS = MIN(m_maxFS/*preferred*/, (uint32_t)val_int/*proposed*/); // make sure we'll never send more than we advertised
					if (currFS > maxFS)
					{
						// do not use default sizes when we already respect the MaxFS
						if (h264_common_size_from_fs(maxFS, &width_max, &height_max) == 0)
						{
							m_width = width_max;
							m_height = height_max;
							outsize_changed = true;
						}
					}
				}
				else if (std_stricmp(sn_ctx.name, "max-mbps") == 0)
				{
					// should compare "max-mbps"?
					uint32_t val_int = strtoul(sn_ctx.value, nullptr, 10);
					m_max_mbps = val_int * 1000;
				}
				else if (std_stricmp(sn_ctx.name, "packetization-mode") == 0)
				{
					uint32_t val_int = strtoul(sn_ctx.value, nullptr, 10);
					if ((packetization_mode_t)val_int == Single_NAL_Unit_Mode || (packetization_mode_t)val_int == Non_Interleaved_Mode)
					{
						m_pack_mode_remote = (packetization_mode_t)val_int;
						m_pack_mode_local = MAX(m_pack_mode_local, m_pack_mode_remote);
					}
					else
					{
						//TSK_DEBUG_INFO("packetization-mode not matching");
						ret = false;
						break;
					}
				}
			} while (str_scanner_next(&sn_ctx));
		}
	}

	// clamp the output size to the defined max range
	if (outsize_changed && false) // tmedia_defaults_get_adapt_video_size_range_enabled()
	{
		/*if (tmedia_codec_video_clamp_out_size_to_range_max(h264) != 0)
		{
			ret = false;
		}*/
	}

	return ret;
}
//-----------------------------------------------
//
//-----------------------------------------------
rtl::String h264_common_t::sdp_att_get(const char* att_name) const
{
	if (!att_name)
	{
		//TSK_DEBUG_ERROR("Invalid parameter");
		return rtl::String::empty;
	}

	if (std_stricmp(att_name, "fmtp") == 0)
	{
		char fmtp[256];
#if 1
		// Required by "TANDBERG/4120 (X7.2.2)" and CISCO TelePresence
		int written = std_snprintf(fmtp, 255, "profile-level-id=%x;max-mbps=%d;max-fs=%d",
			((m_profile << 16) | (m_profile_iop << 8) | (m_level & 0xff)),
			MaxMBPS[H264_LEVEL_TO_ZERO_BASED_INDEX[m_level]],
			m_maxFS
			);
		// Do not restrict packetisation-mode until we knwon what the remote party supports
		if (m_pack_mode_remote != Unknown_Mode) {
			std_snprintf(fmtp+written, 265-written, ";packetization-mode=%d", m_pack_mode_local);
		}
#else
		tsk_sprintf(&fmtp, "profile-level-id=%x; packetization-mode=%d", ((m_profile << 16) | (m_profile_iop << 8) | (m_level & 0xff)), m_pack_mode);
#endif
		return rtl::String(fmtp);
	}
	
	return rtl::String::empty;
}
//-----------------------------------------------
