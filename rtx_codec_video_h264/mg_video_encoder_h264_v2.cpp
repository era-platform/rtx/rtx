/* RTX H.248 Media Gate. H.264 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_video_encoder_h264_v2.h"

#define MLOG_PREFIX "X264-E"

#define FFMPEG_PIXELFORMAT_ARGB		AV_PIX_FMT_BGR32	//	PIX_FMT_RGB32
#define FFMPEG_PIXELFORMAT_RGB		AV_PIX_FMT_BGR24	// PIX_FMT_RGB24
#define FFMPEG_PIXELFORMAT_YUV		AV_PIX_FMT_YUV420P

#define H264_CLOCK_RATE				90000
#define DEFAULT_NAL_BUFFER_SIZE		(32 * 1024)

#define NAL_TYPE_UNDEFINED			0
#define NAL_TYPE_NAL_FROM			1
#define NAL_TYPE_NAL_TO				23
#define NAL_TYPE_STAPA				24
#define NAL_TYPE_STAPB				25
#define NAL_TYPE_MTAP16				26
#define NAL_TYPE_MTAP24				27
#define NAL_TYPE_FUA				28
#define NAL_TYPE_FUB				29

#define MLOG_PREFIX_P				"H264>RTP"
#define MLOG_PREFIX_D				"RTP>H264"

static volatile uint32_t s_id_gen = 1;

//extern rtl::Logger h264_log;
static void x264_log_callback(void *p_unused, int i_level, const char *psz_fmt, va_list arg);

mg_video_encoder_h264_v2_t::mg_video_encoder_h264_v2_t(rtl::Logger& log, rtx_video_encoder_cb_t cb, void* usr) : m_log(log),
	m_x264(NULL), m_x264_param(NULL), m_x264_picture(NULL),
	m_frame(NULL), m_has_delayed(false),
	m_payload_length(0), m_timestamp(0), m_framerate(0),
	m_buffer_size(0), m_buffer_pos(0), m_buffer(NULL),
	m_enc_initialized(false)
{
	memset(&m_settings, 0, sizeof(video_settings));

	m_callback = cb;
	m_callback_user = usr;

	m_id = std_interlocked_inc(&s_id_gen);

	rtl::res_counter_t::add_ref(g_rtx_encoder_counter);
}

mg_video_encoder_h264_v2_t::~mg_video_encoder_h264_v2_t()
{
	destroy();

	rtl::res_counter_t::release(g_rtx_encoder_counter);
}

bool mg_video_encoder_h264_v2_t::initialize(const media::PayloadFormat* fmt)
{
	MLOG_CALL(MLOG_PREFIX, "id:% 4u : initializing...", m_id);

	destroy();

	m_settings.width = 640;
	m_settings.height = 480;
	m_settings.framerate = 25;
	m_settings.bitrate = 2048 * 1024;

	m_framerate = m_settings.framerate;
	m_payload_length = 1388;// rtp_payload_length;

	//��������� ��� 5 ��� �������
	m_buffer_size = 5 * m_payload_length;
	m_buffer = new char[m_buffer_size];

	MLOG_CALL(MLOG_PREFIX, "id:% 4u : initialized", m_id);

	return true;
}

void mg_video_encoder_h264_v2_t::destroy()
{
	MLOG_CALL(MLOG_PREFIX, "id:% 4u : disposing...", m_id);

	{
		free_x264_encoder();
		free_x264_params();
		free_x264_picture();
		free_rgb_yuv_converter();
		free_av_frame();

#ifdef X264MLOG_ENABLED
		g_x264log.destroy();
#endif
	}

	memset(&m_settings, 0, sizeof(m_settings));

	clear_nal_pool();

	m_has_delayed = false;

	clear_current_packets();

	clear_packet_pool();

	if (m_buffer != NULL)
	{
		delete[] m_buffer;
		m_buffer = NULL;
	}
	m_buffer_size = 0;
	m_buffer_pos = 0;

	m_payload_length = 0;
	m_framerate = 0;
	m_timestamp = 0;


	MLOG_CALL(MLOG_PREFIX, "id:% 4u : disposed", m_id);
}

const char*	mg_video_encoder_h264_v2_t::getEncoding()
{
	return "H264";
}

bool mg_video_encoder_h264_v2_t::init(rtx_video_encoder_cb_t cb, void* user)
{
	m_callback = cb;
	m_callback_user = user;

	return true;
}

void mg_video_encoder_h264_v2_t::reset()
{
}

bool mg_video_encoder_h264_v2_t::encode(const media::VideoImage* picture)
{
	MLOG_CALL(MLOG_PREFIX, "id:% 4u : encode : picture %dx%d len %d bytes (%dx%d)", m_id, picture->getWidth(), picture->getHeight(), picture->getImageSize(),
		m_settings.width, m_settings.height);

	bool reset = !m_enc_initialized || m_settings.width != picture->getWidth() || m_settings.height != picture->getHeight();

	if (reset)
	{
		m_enc_initialized = true;
		video_settings settings = { 0 };
		settings.width = picture->getWidth();
		settings.height = picture->getHeight();
		settings.framerate = m_framerate;
		settings.bitrate = m_settings.bitrate;

		if (!update(settings))
		{
			MLOG_WARN(MLOG_PREFIX, "id:% 4u : encode : error while initializing encoder!", m_id);
			return false;
		}
	}
	//else
	//{
	//	m_settings.width = picture->getWidth();
	//	m_settings.height = picture->getHeight();

	//	MLOG_CALL(MLOG_PREFIX, "id:% 4u : encode : update picture size to %dx%d", m_id, picture->getWidth(), picture->getHeight());
	//}

	clear_nal_pool();

	AVPixelFormat pixfmt = AVPixelFormat::AV_PIX_FMT_YUV420P;

	avpicture_fill((AVPicture*)m_frame, picture->getYData(), (AVPixelFormat)pixfmt, m_settings.width, m_settings.height);

	if (!prepare_x264_picture())
	{
		MLOG_ERROR(MLOG_PREFIX, "id:% 4u : encode : prepare_x264_picture() failed", m_id);
		return false;
	}

	if (!encode(false))
	{
		MLOG_WARN(MLOG_PREFIX, "id:% 4u : encode : error while encoding!", m_id);
		return false;
	}

	if (!packetize_frame(m_nal_list))
	{
		MLOG_WARN(MLOG_PREFIX, "id:% 4u : encode : error while packetizing!", m_id);
		return false;
	}

	const rtp_pack_array& array = m_current_packets;

	for (int i = 0; i < array.getCount(); ++i)
	{
		rtp_pack* rtp = array[i];
		m_callback(rtp->data, rtp->size, rtp->timestamp, rtp->marker, m_callback_user);
	}

	return true;
}

bool mg_video_encoder_h264_v2_t::update(video_settings& settings)
{
	m_settings = settings;

	if (!init_x264_picture())
		return false;

	if (!init_x264_params())
		return false;


	m_x264_param->pf_log = x264_log_callback;

	if (!init_x264_encoder())
		return false;

	if (!init_av_frame())
		return false;

	return true;
}

bool mg_video_encoder_h264_v2_t::encode(bool null)
{
	x264_picture_t pic_out;
	x264_nal_t* nal_ptr = NULL;
	int nal_count = 0;

	int res = x264_encoder_encode(m_x264, &nal_ptr, &nal_count, null ? NULL : m_x264_picture, &pic_out);
	if (res < 0)
	{
		MLOG_ERROR(MLOG_PREFIX, "id:% 4u : encode(null) : failed, x264_encoder_encode() return %d", m_id, res);
		return false;
	}

	MLOG_CALL(MLOG_PREFIX, "id:% 4u : encode(null) : res: %d, nal_count: %d", m_id, res, nal_count);

	for (int i = 0; i < nal_count; ++i)
	{
		x264_nal_t* x264_nal = nal_ptr + i;

		//t_video_nal_data nal = ;

		m_nal_list.add({ x264_nal->p_payload, (uint32_t)(x264_nal->i_payload) });
	}

	m_has_delayed = true;

	return true;
}
bool mg_video_encoder_h264_v2_t::init_x264_params()
{
	free_x264_params();

	m_x264_param = NEW x264_param_t();

	x264_param_default(m_x264_param);

	int error = x264_param_default_preset(m_x264_param, "ultrafast", "zerolatency");
	//int error = x264_param_default_preset(m_x264_param, "medium", "film");

	//����������
	m_x264_param->i_width = m_settings.width;
	m_x264_param->i_height = m_settings.height;
	m_x264_param->vui.i_sar_width = 0;
	m_x264_param->vui.i_sar_height = 0;

	//�������
	m_x264_param->rc.i_rc_method = X264_RC_ABR;
	m_x264_param->rc.i_bitrate = m_settings.bitrate / 1024;
	m_x264_param->rc.f_rate_tolerance = 0.05f;
	m_x264_param->rc.f_qcompress = 0;

	//fps
	m_x264_param->i_fps_num = m_settings.framerate;
	m_x264_param->i_fps_den = 1;

	error = x264_param_apply_profile(m_x264_param, "high"); // baseline

	// We make use of the baseline profile, that means:
	// no B-Frames (too much latency in interactive video)
	// CBR (we want to get the max. quality making use of all the bitrate that is available)
	// baseline profile begin
	m_x264_param->b_cabac = 0;  // Only >= MAIN LEVEL
	m_x264_param->i_bframe = 0; // Only >= MAIN LEVEL

	//1 - ���� �����, 0 - ���������������� �� ��� ����
	m_x264_param->i_threads = 1;

	//�����
	m_x264_param->i_level_idc = 30;

	//
	m_x264_param->rc.i_qp_min = 10;
	m_x264_param->rc.i_qp_max = 51;
	m_x264_param->rc.i_qp_step = 4;

	// i-frame period.
	//
	// i-frame - �������� ����, ������� ������������ ���������, �.�. ��� ���������� ��� ��� ���������� ����.
	// ��������� ����� ����������� ������������ ����, �.�. ���������� � ���������� ��� ������� ������������ i-�����.
	// ���� i-���� �����-�� ������� ������� �� ������, �� ��� ������������� ��������� ����� ����� ���������� �����.
	// ��� ����� ������������ �� ��������� ���������� i-�����.
	// �������, ��������� � ��� �������� ����������, �� ������� i-���� ������ �������, �����:
	// 1) ����� ������� �� ��������.
	// 2) ���������� ������ ���������� ������������� � ������ ����������� (�.�. ����� �� ����������� ����� i-�����)
	m_x264_param->i_keyint_min = 0;
	m_x264_param->i_keyint_max = m_settings.framerate;

	return true;
}
void mg_video_encoder_h264_v2_t::free_x264_params()
{
	if (m_x264_param == NULL)
		return;

	DELETEO(m_x264_param);
	m_x264_param = NULL;
}
bool mg_video_encoder_h264_v2_t::init_x264_picture()
{
	free_x264_picture();

	m_x264_picture = NEW x264_picture_t();

	return true;
}
void mg_video_encoder_h264_v2_t::free_x264_picture()
{
	if (m_x264_picture == NULL)
		return;

	DELETEO(m_x264_picture);
	m_x264_picture = NULL;
}
bool mg_video_encoder_h264_v2_t::init_x264_encoder()
{
	free_x264_encoder();

	m_x264 = x264_encoder_open(m_x264_param);
	if (m_x264 == NULL)
	{
		MLOG_ERROR(MLOG_PREFIX, "id:% 4u : init_x264_encoder -- failed, x264_encoder_open() return NULL", m_id);
		return false;
	}

	MLOG_CALL(MLOG_PREFIX, "id:% 4u : init_x264_encoder -- %p", m_id, m_x264);

	return true;
}
void mg_video_encoder_h264_v2_t::free_x264_encoder()
{
	if (m_x264 == NULL)
		return;

	MLOG_CALL(MLOG_PREFIX, "id:% 4u : free_x264_encoder -- %p", m_id, m_x264);

	x264_encoder_close(m_x264);
	m_x264 = NULL;
}
bool mg_video_encoder_h264_v2_t::init_av_frame()
{
	free_av_frame();

	m_frame = av_frame_alloc();
	if (m_frame == NULL)
	{
		MLOG_ERROR(MLOG_PREFIX, "id:% 4u : init_av_frame -- failed, avcodec_alloc_frame() return NULL", m_id);
		return false;
	}

	return true;
}
void mg_video_encoder_h264_v2_t::free_av_frame()
{
	if (m_frame == NULL)
		return;

	av_free(m_frame);
	m_frame = NULL;
}
bool mg_video_encoder_h264_v2_t::init_rgb_yuv_converter()
{
	//if (m_rgb_to_yuv != NULL)
	//	return true;

	//m_rgb_to_yuv = new rgb_yuv_converter(m_log);

	//if (!m_rgb_to_yuv->init_rgb_to_yuv(m_settings->width, m_settings->height))
	//	return false;

	return true;
}
void mg_video_encoder_h264_v2_t::free_rgb_yuv_converter()
{
	//if (m_rgb_to_yuv == NULL)
	//	return;

	//m_rgb_to_yuv->dispose();
	//delete m_rgb_to_yuv;
	//m_rgb_to_yuv = NULL;
}

bool mg_video_encoder_h264_v2_t::prepare_rgb_frame(const char* data, unsigned len)
{
	int pixels = m_settings.width * m_settings.height;
	int bytes_per_pixel = len / pixels;

	if ((bytes_per_pixel != 3) && (bytes_per_pixel != 4))
	{
		MLOG_ERROR(MLOG_PREFIX, "id:% 4u : prepare_rgb_frame -- failed, invalid image! size: %d, width: %d, height: %d, pixel: %d", m_id,
			len, m_settings.width, m_settings.height, bytes_per_pixel);
		return false;
	}

	int pixfmt = (bytes_per_pixel == 4) ? FFMPEG_PIXELFORMAT_ARGB : FFMPEG_PIXELFORMAT_RGB;

	avpicture_fill((AVPicture*)m_frame, (uint8_t*)data, (AVPixelFormat)pixfmt, m_settings.width, m_settings.height);

	return true;
}
bool mg_video_encoder_h264_v2_t::prepare_x264_picture()
{
	x264_picture_init(m_x264_picture);

	//������� ��������� �������� ������ (���������� � ffmpeg)
	m_x264_picture->img.i_csp = X264_CSP_I420;
	m_x264_picture->img.i_plane = 3;
	m_x264_picture->i_type = X264_TYPE_AUTO;

	const AVFrame* avframe = m_frame;

	for (int i = 0; i < 3; ++i)
	{
		m_x264_picture->img.plane[i] = avframe->data[i];
		m_x264_picture->img.i_stride[i] = avframe->linesize[i];
	}

	return true;
}
//t_video_nal_data& mg_video_encoder_h264_v2_t::get_free_nal()
//{
//	if (m_nal_pool.getCount() == 0)
//	{
//		t_video_nal_data nal = { 0 };
//		m_nal_pool.add(nal);
//	}
//
//	t_video_nal_data& nal = m_nal_pool.getAt(m_nal_pool.getCount()-1);
//	m_nal_pool.removeAt(m_nal_pool.getCount() - 1);
//	m_current_nals.add(nal);
//
//	return nal;
//}
//void mg_video_encoder_h264_v2_t::clear_current_nals()
//{
//	//for (unsigned i = 0; i < m_current_nals.getCount(); ++i)
//	//{
//	//	t_video_nal_data& nal = m_current_nals[i];
//	//	memset(nal.ptr, 0, sizeof(t_video_nal_data));
//	//	m_nal_pool.push_back(nal);
//	//}
//	m_current_nals.clear();
//}
void mg_video_encoder_h264_v2_t::clear_nal_pool()
{
	//for (unsigned i = 0; i < m_nal_pool.getCount(); ++i)
	//{
	//	t_video_nal_data& nal = m_nal_pool[i];
	//	DELETEO(nal.ptr);
	//}
	m_nal_list.clear();
}

bool mg_video_encoder_h264_v2_t::packetize_frame(const t_video_nal_array& nal_queue)
{
	clear_current_packets();

	m_buffer_pos = 0;

	if (nal_queue.getCount() == 0)
		return false;

	MLOG_CALL(MLOG_PREFIX_P, "id:% 4u : packetize_frame --> nals count: %d, current timestamp: %u", m_id,
		nal_queue.getCount(), m_timestamp);

	for (int i = 0; i < nal_queue.getCount(); ++i)
	{
		const t_video_nal_data& nal = nal_queue[i];
		packetize_nal(nal.ptr, nal.len);
	}

	if (!m_current_packets.isEmpty())
	{
		rtp_pack* packet = m_current_packets.getLast();
		packet->marker = true;
	}

	MLOG_CALL(MLOG_PREFIX_P, "id:% 4u : packetize_frame <-- packets count: %d", m_id, m_current_packets.getCount());

	uint32_t ts = H264_CLOCK_RATE / m_framerate;
	m_timestamp += ts;

	return true;
}
void mg_video_encoder_h264_v2_t::packetize_nal(const uint8_t* nal, unsigned size)
{
	//���������� ������ � 0x000001 ��� 0x00000001
	if ((nal[0] == 0x00) && (nal[1] == 0x00))
	{
		if ((nal[2] == 0x00) && (nal[3] == 0x01))
		{
			nal += 4;
			size -= 4;
		}
		else if (nal[2] == 0x01)
		{
			nal += 3;
			size -= 3;
		}
	}

	if (size <= m_payload_length)
	{
		add_single(nal, size);
	}
	else
	{
		//check_buffer_size(size);
		add_fua(nal, size);
	}
}
void mg_video_encoder_h264_v2_t::clear_current_packets()
{
	for (int i = 0; i < m_current_packets.getCount(); ++i)
	{
		rtp_pack* packet = m_current_packets[i];
		m_packet_pool.add(packet);
	}

	m_current_packets.clear();
}
void mg_video_encoder_h264_v2_t::clear_packet_pool()
{
	for (int i = 0; i < m_packet_pool.getCount(); ++i)
	{
		rtp_pack* packet = m_packet_pool[i];
		DELETEO(packet);
	}

	m_packet_pool.clear();
}
void mg_video_encoder_h264_v2_t::add_single(const uint8_t* data, unsigned size)
{
	MLOG_MEDIA_FLOW(MLOG_PREFIX_P, "id:% 4u : add_single -- data: %p, size: %d", m_id, data, size);

	check_buffer_space(size);

	//������ ��������� ��������, �� ��� ����� ��������� ���-�����
	memcpy(m_buffer + m_buffer_pos, data, size);

	rtp_pack* packet = get_free_packet();

	packet->data = m_buffer + m_buffer_pos;
	packet->marker = false;
	packet->size = size;
	packet->timestamp = m_timestamp;

	m_buffer_pos += size;
}
void mg_video_encoder_h264_v2_t::add_fua(const uint8_t* data, unsigned size)
{
	/*
	�� ��������� ���� ����� ������ ���� (��������� ����),
	�� ���� ����� ������������ �����. ���� ������ ���� �����
	�� ������������ ��� ���������� ����.

	������ ���-����� ���������� �� 2 ������ ��������� + ����������
	����� ��������� ����. ��� 2 ����� ��������� ����������� ��
	����������� ������.
	*/

	//���������� ������ ��� (forbidden bit)
	char flag = data[0] & 0x80;
	//���������� ������-������ ���� (NRI)
	char nri = data[0] & 0x60;
	//���������� ��������� 5 ����� - ��� ����
	char type = data[0] & 0x1F;

	++data;
	--size;

	//���������� ������ ������
	unsigned count = size / (m_payload_length - 2);//�� ������ ����� ��� 2 ����� ����������
	if (size % (m_payload_length - 2) != 0)
		++count;
	check_buffer_space(count * m_payload_length);

	MLOG_MEDIA_FLOW(MLOG_PREFIX_P, "id:% 4u : add_fua -- data: %p, size: %d, packets: %d", m_id, data - 1, size + 1, count);

	bool first = true;//������ �����?
	bool last = false;//��������� �����?

	while (size != 0)
	{
		rtp_pack* packet = get_free_packet();
		packet->data = m_buffer + m_buffer_pos;

		//��������� ��� ������, 2 �����
		packet->data[0] = flag;
		packet->data[0] |= nri;
		packet->data[0] |= NAL_TYPE_FUA;//��������� ��� ��� FU-A
		packet->data[1] |= 0;
		packet->data[1] = type;

		//������� ���� �� ���������� � ��� �����?
		if (size + 2 > m_payload_length)
		{
			packet->size = m_payload_length;
		}
		else
		{
			packet->size = size + 2;
			last = true;
		}

		if (first)
		{
			packet->data[1] |= 0x80;//������ ��� ���������� ��� ��� ������ �����
			first = false;
		}

		if (last)
			packet->data[1] |= 0x40;//������ ��� ���������� ��� ��� ��������� �����

		memcpy(packet->data + 2, data, packet->size - 2);
		m_buffer_pos += packet->size;

		packet->marker = false;
		packet->timestamp = m_timestamp;

		size -= (packet->size - 2);
		data += (packet->size - 2);
	}
}
void mg_video_encoder_h264_v2_t::check_buffer_space(unsigned need_space)
{
	unsigned free = m_buffer_size - m_buffer_pos;
	if (free < need_space)
	{
		m_buffer_size = (unsigned)((m_buffer_pos + need_space) * 1.5);
		char* new_buffer = new char[m_buffer_size];
		memcpy(new_buffer, m_buffer, m_buffer_pos);
		delete[] m_buffer;

		//���������� ��������� ��� ��������� �������
		for (int i = 0; i < m_current_packets.getCount(); ++i)
		{
			rtp_pack* packet = m_current_packets[i];
			int offset = (int)(packet->data - m_buffer);
			packet->data = new_buffer + offset;
		}

		m_buffer = new_buffer;
	}
}
mg_video_encoder_h264_v2_t::rtp_pack* mg_video_encoder_h264_v2_t::get_free_packet()
{
	//���� ��� ������, ������� 1 �����
	if (m_packet_pool.isEmpty())
	{
		rtp_pack* packet = NEW rtp_pack();
		memset(packet, 0, sizeof(rtp_pack));
		m_packet_pool.add(packet);
	}

	//�� ���� ����� ���������� � �������� �������
	rtp_pack* packet = m_packet_pool.getLast();
	m_packet_pool.removeLast();
	m_current_packets.add(packet);

	return packet;
}
//-----------------------------------------------
//
//-----------------------------------------------

static void x264_log_callback(void *p_unused, int i_level, const char *psz_fmt, va_list arg)
{
	char *psz_prefix;
	switch (i_level)
	{
	case X264_LOG_ERROR:
		psz_prefix = "error";
		break;
	case X264_LOG_WARNING:
		psz_prefix = "warning";
		break;
	case X264_LOG_INFO:
		psz_prefix = "info";
		break;
	case X264_LOG_DEBUG:
		psz_prefix = "debug";
		break;
	default:
		psz_prefix = "unknown";
		break;
	}

	char msg[2048] = { 0 };
	int len = std_snprintf(msg, sizeof(msg), "x264 [%s]: %s", psz_prefix, psz_fmt);

	//h264_log.log_format("X264", msg, arg);
}
//-----------------------------------------------
