/* RTX H.248 Media Gate. H.264 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "h264_common.h"

/*
*	ITU H.264 - http://www.itu.int/rec/T-REC-H.264-200903-S/en
*/

extern const uint8_t H264_START_CODE_PREFIX[4] = { 0x00, 0x00, 0x00, 0x01 };

#define H264_NAL_UNIT_TYPE_HEADER_SIZE		1
#define H264_F_UNIT_TYPE_HEADER_SIZE		1
#define H264_FUA_HEADER_SIZE				2
#define H264_FUB_HEADER_SIZE				4
#define H264_NAL_AGG_MAX_SIZE			65535

static int h264_get_fua_pay(const uint8_t* in_data, size_t in_size, const void** out_data, size_t *out_size, bool* append_scp, bool* end_of_unit);
static int h264_get_nalunit_pay(const uint8_t* in_data, size_t in_size, const void** out_data, size_t *out_size);

// profile_level_id MUST be a "null-terminated" string
int h264_parse_profile(const char* profile_level_id, profile_idc_t *p_idc, profile_iop_t *p_iop, level_idc_t *l_idc)
{
	uint32_t value;

	if (std_strlen(profile_level_id) != 6)
	{
		//TSK_DEBUG_ERROR("I say [%s] is an invalid profile-level-id", profile_level_id);
		return -1;
	}

	value = strtol(profile_level_id, nullptr, 16);

	/* profile-idc */
	if (p_idc) {
		switch ((value >> 16))
		{
		case profile_idc_baseline:
			*p_idc = profile_idc_baseline;
			break;
		case profile_idc_extended:
			*p_idc = profile_idc_extended;
			break;
		case profile_idc_main:
			*p_idc = profile_idc_main;
			break;
		case profile_idc_high:
			*p_idc = profile_idc_high;
			break;
		default:
			*p_idc = profile_idc_none;
			break;
		}
	}

	/* profile-iop */
	if (p_iop) {
		p_iop->constraint_set0_flag = ((value >> 8) & 0x80) >> 7;
		p_iop->constraint_set1_flag = ((value >> 8) & 0x40) >> 6;
		p_iop->constraint_set2_flag = ((value >> 8) & 0x20) >> 5;
		p_iop->reserved_zero_5bits = ((value >> 8) & 0x1F);
	}

	/* level-idc */
	if (l_idc) {
		switch ((value & 0xFF)) {
		case level_idc_1_0:
			*l_idc = level_idc_1_0;
			break;
		case level_idc_1_b:
			*l_idc = level_idc_1_b;
			break;
		case level_idc_1_1:
			*l_idc = level_idc_1_1;
			break;
		case level_idc_1_2:
			*l_idc = level_idc_1_2;
			break;
		case level_idc_1_3:
			*l_idc = level_idc_1_3;
			break;
		case level_idc_2_0:
			*l_idc = level_idc_2_0;
			break;
		case level_idc_2_1:
			*l_idc = level_idc_2_1;
			break;
		case level_idc_2_2:
			*l_idc = level_idc_2_2;
			break;
		case level_idc_3_0:
			*l_idc = level_idc_3_0;
			break;
		case level_idc_3_1:
			*l_idc = level_idc_3_1;
			break;
		case level_idc_3_2:
			*l_idc = level_idc_3_2;
			break;
		case level_idc_4_0:
			*l_idc = level_idc_4_0;
			break;
		case level_idc_4_1:
			*l_idc = level_idc_4_1;
			break;
		case level_idc_4_2:
			*l_idc = level_idc_4_2;
			break;
		case level_idc_5_0:
			*l_idc = level_idc_5_0;
			break;
		case level_idc_5_1:
			*l_idc = level_idc_5_1;
			break;
		case level_idc_5_2:
			*l_idc = level_idc_5_2;
			break;
		default:
			*l_idc = level_idc_none;
			break;
		}
	}

	return 0;
}

int h264_get_pay(const void* in_data, size_t in_size, const void** out_data, size_t *out_size, bool* append_scp, bool* end_of_unit)
{
	const uint8_t* pdata = (const uint8_t*)in_data;
	
	if (!in_data || !in_size || !out_data || !out_size || !append_scp || !end_of_unit)
	{
		//TSK_DEBUG_ERROR("Invalid parameter");
		return -1;
	}

	*out_data = nullptr;
	*out_size = 0;

	/* 5.3. NAL Unit Octet Usage
	+---------------+
	|0|1|2|3|4|5|6|7|
	+-+-+-+-+-+-+-+-+
	|F|NRI|  Type   |
	+---------------+
	*/

	uint8_t nal_type = pdata[0] & 0x1F;

	switch (nal_type)
	{
	case undefined_0:
	case undefined_30:
	case undefined_31:
	case stap_a:
	case stap_b:
	case mtap16:
	case mtap24:
	case fu_b:
		break;
	case fu_a:
		return h264_get_fua_pay(pdata, in_size, out_data, out_size, append_scp, end_of_unit);
	default: /* NAL unit (1-23) */
		*append_scp = true; //(nal_type != 7 && nal_type != 8); // SPS or PPS
		*end_of_unit = true;
		return h264_get_nalunit_pay(pdata, in_size, out_data, out_size);
	}

	//TSK_DEBUG_WARN("%d not supported as valid NAL Unit type", (*pdata & 0x1F));
	return -1;
}

static int h264_get_fua_pay(const uint8_t* in_data, size_t in_size, const void** out_data, size_t *out_size, bool* append_scp, bool* end_of_unit)
{
	if (in_size <= H264_FUA_HEADER_SIZE)
	{
		//TSK_DEBUG_ERROR("Too short");
		return -1;
	}
	
	/* RFC 3984 - 5.8. Fragmentation Units (FUs)

	0                   1                   2                   3
	0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	| FU indicator  |   FU header   |                               |
	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+                               |
	|                                                               |
	|                         FU payload                            |
	|                                                               |
	|                               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	|                               :...OPTIONAL RTP padding        |
	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

	The FU indicator octet has the following format:

	+---------------+
	|0|1|2|3|4|5|6|7|
	+-+-+-+-+-+-+-+-+
	|F|NRI|  Type   |
	+---------------+

	The FU header has the following format:

	+---------------+
	|0|1|2|3|4|5|6|7|
	+-+-+-+-+-+-+-+-+
	|S|E|R|  Type   |
	+---------------+
	*/

	if (((in_data[1] & 0x80) /*S*/))
	{
		/* discard "FU indicator" */
		*out_data = (in_data + H264_NAL_UNIT_TYPE_HEADER_SIZE);
		*out_size = (in_size - H264_NAL_UNIT_TYPE_HEADER_SIZE);

		// Do need to append Start Code Prefix ?
		/* S: 1 bit
		When set to one, the Start bit indicates the start of a fragmented
		NAL unit.  When the following FU payload is not the start of a
		fragmented NAL unit payload, the Start bit is set to zero.*/
		*append_scp = true;

		// F, NRI and Type
		*((uint8_t*)*out_data) = (in_data[0] & 0xe0) /* F,NRI from "FU indicator"*/ | (in_data[1] & 0x1f) /* type from "FU header" */;
	}
	else
	{
		*append_scp = false;
		*out_data = (in_data + H264_FUA_HEADER_SIZE);
		*out_size = (in_size - H264_FUA_HEADER_SIZE);
	}
	/*
	E:     1 bit
	When set to one, the End bit indicates the end of a fragmented
	NAL unit, i.e., the last byte of the payload is also the last
	byte of the fragmented NAL unit.  When the following FU
	payload is not the last fragment of a fragmented NAL unit, the
	End bit is set to zero.
	*/
	*end_of_unit = (((in_data[1] & 0x40) /*E*/)) ? true : false;

	return 0;
}

static int h264_get_nalunit_pay(const uint8_t* in_data, size_t in_size, const void** out_data, size_t *out_size)
{

	/*	5.6. Single NAL Unit Packet

	0                   1                   2                   3
	0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	|F|NRI|  type   |                                               |
	+-+-+-+-+-+-+-+-+                                               |
	|                                                               |
	|               Bytes 2..n of a Single NAL unit                 |
	|                                                               |
	|                               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	|                               :...OPTIONAL RTP padding        |
	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	*/

	*out_data = in_data;
	*out_size = in_size;

	return 0;
}

static const uint8_t* search_nal_start(const uint8_t* stream, size_t length, int& sign_len)
{
	const uint8_t* cp = stream, *ptr = cp;
	sign_len = 0;

	if (*cp == 0 && *(cp + 1) == 0)
	{
		if (*(cp + 2) == 1)
		{
			cp += 3, length -= 3;
			sign_len = 3;
		}
		else if (*(cp + 2) == 0 && *(cp + 3) == 1)
		{
			cp += 4, length -= 4;
			sign_len = 4;
		}
	}

	while ((cp = (const uint8_t*)memchr(cp, 0, length)) != nullptr)
	{
		// check next nal start sign
		int scanned = (int)(cp - ptr);
		ptr = cp;
		length -= scanned;

		if (length < 3)
			break;

		if (*cp == 0 && *(cp + 1) == 0)
		{
			if (*(cp + 2) == 1)
			{
				return cp;
			}
			else if (length > 3 && *(cp + 2) == 0 && *(cp + 3) == 1)
			{
				return cp;
			}

			cp = cp + 2;
			length -= 2;

		}
		else
		{
			cp++;
			length--;
		}
	}

	return nullptr;
}

void h264_common_t::h264_rtp_encap(const uint8_t* pdata, size_t size)
{
	const int size_of_scp = sizeof(H264_START_CODE_PREFIX); /* we know it's equal to 4 .. */

	if (!pdata || size < size_of_scp)
	{
		return;
	}

	uint32_t current = rtl::DateTime::getTicks();
	int ms_diff = current - m_last_clock;
	m_last_ts += ms_diff * 90;
	m_last_clock = current;

	const uint8_t* nal = pdata;
	const uint8_t* prev = pdata;
	int sign_len = 0;

	while ((nal = search_nal_start(nal, size, sign_len)) != nullptr)
	{
		int nal_size = (int)(nal - prev);

		h264_rtp_callback(prev + sign_len, nal_size - sign_len, m_last_ts, false);
		
		size -= nal_size;
		prev = nal;
	}

	h264_rtp_callback(prev + sign_len, size - sign_len, m_last_ts, true);
}

void h264_common_t::h264_rtp_callback(const void *data, size_t size, uint32_t ts, bool marker)
{
	uint8_t* pdata = (uint8_t*)data;

	if (m_pack_mode_local == Single_NAL_Unit_Mode || size < H264_RTP_PAYLOAD_SIZE)
	{
		if (m_pack_mode_local == Single_NAL_Unit_Mode && size > H264_RTP_PAYLOAD_SIZE)
		{
			LOG_WARN("H264-RTP", "pack_mode=Single_NAL_Unit_Mode but size(%d) > H264_RTP_PAYLOAD_SIZE(%d). Did you forget to set \"avctx->rtp_payload_size\"?", size, H264_RTP_PAYLOAD_SIZE);
		}
		// Can be packet in a Single Nal Unit
		// Send data over the network
		if (m_enc_callback)
		{
			m_enc_callback(pdata, (int)size, ts, marker, m_user);
		}
	}
	else if (size > H264_NAL_UNIT_TYPE_HEADER_SIZE)
	{
		/* Should be Fragmented as FUA */
		uint8_t fua_hdr[H264_FUA_HEADER_SIZE];		/* "FU indicator" and "FU header" - 2bytes */
		fua_hdr[0] = pdata[0] & 0x60;				/* NRI */
		fua_hdr[0] |= fu_a;
		fua_hdr[1] = 0x80;							/* S=1,E=0,R=0 */
		fua_hdr[1] |= pdata[0] & 0x1f;				/* type */
		
		// discard header
		pdata += H264_NAL_UNIT_TYPE_HEADER_SIZE;
		size -= H264_NAL_UNIT_TYPE_HEADER_SIZE;

		while (size)
		{
			size_t packet_size = MIN(H264_RTP_PAYLOAD_SIZE - H264_FUA_HEADER_SIZE, size);

			if (m_rtp.size < (packet_size + H264_FUA_HEADER_SIZE))
			{
				m_rtp.ptr = (uint8_t*)REALLOC(m_rtp.ptr, (packet_size + H264_FUA_HEADER_SIZE));
				m_rtp.size = (packet_size + H264_FUA_HEADER_SIZE);
			}

			// set E bit
			if ((size - packet_size) == 0)
			{
				// Last packet
				fua_hdr[1] |= 0x40;
			}
			
			// copy FUA header
			memcpy(m_rtp.ptr, fua_hdr, H264_FUA_HEADER_SIZE);
			// reset "S" bit
			fua_hdr[1] &= 0x7F;
			// copy data
			memcpy((m_rtp.ptr + H264_FUA_HEADER_SIZE), pdata, packet_size);
			pdata += packet_size;
			size -= packet_size;

			// send data
			if (m_enc_callback)
			{
			/*	TMEDIA_CODEC_VIDEO(self)->out.result.buffer.ptr = self->rtp.ptr;
				TMEDIA_CODEC_VIDEO(self)->out.result.buffer.size = (packet_size + H264_FUA_HEADER_SIZE);
				TMEDIA_CODEC_VIDEO(self)->out.result.duration = (uint32_t)((1. / (double)TMEDIA_CODEC_VIDEO(self)->out.fps) * TMEDIA_CODEC(self)->plugin->rate);
				TMEDIA_CODEC_VIDEO(self)->out.result.last_chunck = (size == 0);
				TMEDIA_CODEC_VIDEO(self)->out.callback(&TMEDIA_CODEC_VIDEO(self)->out.result);*/
				m_enc_callback(m_rtp.ptr, (uint32_t)(packet_size + H264_FUA_HEADER_SIZE), ts, marker ? size == 0 : false, m_user);
			}
		}
	}
}
