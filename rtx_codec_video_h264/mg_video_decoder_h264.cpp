/* RTX H.248 Media Gate. H.264 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_video_decoder_h264.h"

#define LOG_PREFIX "H264-D"

//-----------------------------------------------
//
//-----------------------------------------------
void avcodec_get_frame_defaults(AVFrame *frame);
//-----------------------------------------------
//
//-----------------------------------------------
mg_video_decoder_h264_t::mg_video_decoder_h264_t(rtl::Logger& log) :
	h264_common_t(nullptr, nullptr), // decoder has no callback!
	m_log(log),
	m_codec(nullptr), m_context(nullptr), m_picture(nullptr),
	m_accumulator(nullptr), m_accumulator_pos(0), m_accumulator_size(0),
	m_last_seq(0)
{
	init(profile_idc_main);

#if defined (VIDEO_TEST)
	m_outfile = nullptr;
#endif

	rtl::res_counter_t::add_ref(g_rtx_decoder_counter);
}
//-----------------------------------------------
//
//-----------------------------------------------
mg_video_decoder_h264_t::~mg_video_decoder_h264_t()
{
#if defined (VIDEO_TEST)
	if (m_outfile != nullptr)
	{
		fclose(m_outfile);
		m_outfile = nullptr;
	}
#endif

	rtl::res_counter_t::release(g_rtx_decoder_counter);
}
//-----------------------------------------------
//
//-----------------------------------------------
bool mg_video_decoder_h264_t::initialize(const media::PayloadFormat* fmt)
{
	sdp_att_match("fmtp", fmt->getFmtp());

	init(m_profile);

	m_context = avcodec_alloc_context3(m_codec);
	avcodec_get_context_defaults3(m_context, m_codec);

	m_context->pix_fmt = AV_PIX_FMT_YUV420P;
	m_context->flags2 |= CODEC_FLAG2_FAST;
	m_context->width = m_width;
	m_context->height = m_height;

	// Picture (YUV 420)
	if (!(m_picture = av_frame_alloc()))
	{
		MLOG_ERROR(LOG_PREFIX, "Failed to create decoder picture");
		return true;
	}
	
	avcodec_get_frame_defaults(m_picture);

	// Open decoder
	if (avcodec_open2(m_context, m_codec, nullptr) < 0)
	{
		MLOG_ERROR(LOG_PREFIX, "Failed to open [%s] codec", (const char*)fmt->getEncoding());
		return false;
	}

	m_last_seq = 0;

#if defined(VIDEO_TEST)

	static int nnn = 1;
	char fname[128];
	std_snprintf(fname, 128, "c:\\temp\\outfile_h264_%d.raw", nnn++);
	m_outfile = fopen(fname, "wb");

	bool b = (m_outfile != nullptr);
#endif
	return true;
}
//-----------------------------------------------
//
//-----------------------------------------------
void mg_video_decoder_h264_t::destroy()
{
	if (m_context) {
		avcodec_close(m_context);
		av_free(m_context);
		m_context = nullptr;
	}
	if (m_picture)
	{
		av_free(m_picture);
		m_picture = nullptr;
	}

	FREE(m_accumulator);
	m_accumulator_pos = 0;

#if defined(VIDEO_TEST)
	if (m_outfile != nullptr)
	{
		fclose(m_outfile);
		m_outfile = nullptr;
	}
#endif

}
//-----------------------------------------------
//
//-----------------------------------------------
const char*	mg_video_decoder_h264_t::getEncoding()
{
	return "H264";
}
//-----------------------------------------------
//
//-----------------------------------------------
media::VideoImage* mg_video_decoder_h264_t::decode(const rtp_packet* rtp_pack)
{
	media::VideoImage* image = nullptr;
	const uint8_t* pay_ptr = nullptr;
	size_t pay_size = 0;
	int ret;
	bool sps_or_pps, append_scp, end_of_unit;
	size_t retsize = 0, size_to_copy = 0;
	static const size_t xmax_size = (3840 * 2160 * 3) >> 3; // >>3 instead of >>1 (not an error)
	static size_t start_code_prefix_size = sizeof(H264_START_CODE_PREFIX);
	int got_picture_ptr = 0;

	const uint8_t* in_data = rtp_pack->get_payload();
	size_t in_size = rtp_pack->get_payload_length();

	if (m_context == nullptr)
	{
		MLOG_ERROR(LOG_PREFIX, "Invalid parameter");
		return nullptr;// false;
	}

	/* Packet lost? */
	if ((m_last_seq + 1) != rtp_pack->get_sequence_number() && m_last_seq != 0)
	{
		MLOG_CALL("H264-DEC", "[H.264] Packet loss, seq_num=%d", (m_last_seq + 1));
	}
	
	m_last_seq = rtp_pack->get_sequence_number();


	/* 5.3. NAL Unit Octet Usage
	+---------------+
	|0|1|2|3|4|5|6|7|
	+-+-+-+-+-+-+-+-+
	|F|NRI|  Type   |
	+---------------+
	*/
	if (*((uint8_t*)in_data) & 0x80)
	{
		MLOG_WARN(LOG_PREFIX, "F=1");
		/* reset accumulator */
		m_accumulator_pos = 0;
		return nullptr;// true;
	}

	/* get payload */
	if ((ret = h264_get_pay(in_data, in_size, (const void**)&pay_ptr, &pay_size, &append_scp, &end_of_unit)) || !pay_ptr || !pay_size)
	{
		MLOG_ERROR(LOG_PREFIX, "Depayloader failed to get H.264 content");
		return nullptr;// false;
	}
	//append_scp = tsk_true;
	size_to_copy = pay_size + (append_scp ? start_code_prefix_size : 0);
	// whether it's SPS or PPS (append_scp is false for subsequent FUA chuncks)
	sps_or_pps = append_scp && pay_ptr && ((pay_ptr[0] & 0x1F) == 7 || (pay_ptr[0] & 0x1F) == 8);

	// start-accumulator
	if (!m_accumulator)
	{
		if (size_to_copy > xmax_size)
		{
			MLOG_ERROR(LOG_PREFIX, "%u too big to contain valid encoded data. xmax_size=%u", size_to_copy, xmax_size);
			return nullptr;// false;
		}

		if (!(m_accumulator = (uint8_t*)MALLOC(size_to_copy)))
		{
			MLOG_ERROR(LOG_PREFIX, "Failed to allocated new buffer");
			return nullptr;// false;
		}
		m_accumulator_size = size_to_copy;
	}
	
	if ((m_accumulator_pos + size_to_copy) >= xmax_size)
	{
		MLOG_ERROR(LOG_PREFIX, "BufferOverflow");
		m_accumulator_pos = 0;
		return nullptr;// false;
	}

	if ((m_accumulator_pos + size_to_copy) > m_accumulator_size)
	{
		if (!(m_accumulator = (uint8_t*)REALLOC(m_accumulator, (m_accumulator_pos + size_to_copy))))
		{
			MLOG_ERROR(LOG_PREFIX, "Failed to reallocated new buffer");
			m_accumulator_pos = 0;
			m_accumulator_size = 0;
			return nullptr;// false;
		}

		m_accumulator_size = (m_accumulator_pos + size_to_copy);
	}

	if (append_scp)
	{
		memcpy(&((uint8_t*)m_accumulator)[m_accumulator_pos], H264_START_CODE_PREFIX, start_code_prefix_size);
		m_accumulator_pos += start_code_prefix_size;
	}
	memcpy(&((uint8_t*)m_accumulator)[m_accumulator_pos], pay_ptr, pay_size);
	m_accumulator_pos += pay_size;
	// end-accumulator

	if (sps_or_pps)
	{
		// http://libav-users.943685.n4.nabble.com/Decode-H264-streams-how-to-fill-AVCodecContext-from-SPS-PPS-td2484472.html
		// SPS and PPS should be bundled with IDR
		MLOG_CALL(LOG_PREFIX, "Receiving SPS or PPS ...to be tied to an IDR");
	}
	else if (rtp_pack->get_marker())
	{
		// !m_passthrough

		AVPacket packet;

		/* decode the picture */
		av_init_packet(&packet);
		packet.dts = packet.pts = AV_NOPTS_VALUE;
		packet.size = (int)m_accumulator_pos;
		packet.data = (uint8_t*)m_accumulator;

		static int s_frame_no = 1;

		ret = avcodec_decode_video2(m_context, m_picture, &got_picture_ptr, &packet);

		if (ret <0)
		{
			char t[256];
			av_make_error_string(t, 256, ret);
			MLOG_CALL(LOG_PREFIX, "Failed to decode the buffer with error (%d) : %s -> size=%u, append=%s", ret, t, m_accumulator_pos, append_scp ? "yes" : "no");
		}
		else if (got_picture_ptr)
		{
			size_t xsize;

			/* IDR ? */
			if (((pay_ptr[0] & 0x1F) == 0x05))
			{
				MLOG_CALL(LOG_PREFIX, "Decoded H.264 IDR");
			}
			/* fill out */
			xsize = avpicture_get_size(m_context->pix_fmt, m_context->width, m_context->height);
			image = NEW media::VideoImage(m_context->width, m_context->height );
			retsize = xsize;
			m_width = m_context->width;
			m_height = m_context->height;
			avpicture_layout((AVPicture *)m_picture, m_context->pix_fmt, (int)m_context->width, (int)m_context->height, image->getImageBuffer(), (int)retsize);

#if defined(VIDEO_TEST)
			fwrite(image->getImageBuffer(), retsize, 1, m_outfile);
#endif
		}

		m_accumulator_pos = 0;
	}

	return image; // retsize > 0;
}
//-----------------------------------------------
//
//-----------------------------------------------
int mg_video_decoder_h264_t::init(profile_idc_t profile)
{
	int ret = 0;
	level_idc_t level;

	if ((ret = h264_common_level_from_size(m_width, m_height, &level)))
	{
		MLOG_ERROR(LOG_PREFIX, "Failed to find level for size=[%u, %u]", m_width, m_height);
		return ret;
	}

	m_pack_mode_local = H264_PACKETIZATION_MODE;
	m_profile = profile;
	m_level = level;
	m_max_mbps = H264_MAX_MBPS * 1000;
	m_max_br = H264_MAX_BR * 1000;

	if (!(m_codec = avcodec_find_decoder(AV_CODEC_ID_H264)))
	{
		MLOG_ERROR(LOG_PREFIX, "Failed to find H.264 decoder");
		ret = -3;
	}

	/* allocations MUST be done by open() */
	return ret;
}
//-----------------------------------------------
//
//-----------------------------------------------
void avcodec_get_frame_defaults(AVFrame *frame)
{
#if LIBAVCODEC_VERSION_MAJOR >= 55
	// extended_data should explicitly be freed when needed, this code is unsafe currently
	// also this is not compatible to the <55 ABI/API
	if (frame->extended_data != frame->data && 0)
		av_freep(&frame->extended_data);
#endif

	memset(frame, 0, sizeof(AVFrame));
	av_frame_unref(frame);
}
//-----------------------------------------------
