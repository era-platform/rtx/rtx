/* RTX H.248 Media Gate. H.264 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "h264_rtp.h"
//-----------------------------------------------
//
//-----------------------------------------------
#if !defined(H264_MAX_BR)
#	define H264_MAX_BR					452
#endif
#if !defined(H264_MAX_MBPS)
#	define H264_MAX_MBPS				11880
#endif

#if !defined(H264_PACKETIZATION_MODE)
#	if METROPOLIS || 1
#		define H264_PACKETIZATION_MODE	Single_NAL_Unit_Mode
#	else
#		define H264_PACKETIZATION_MODE	Non_Interleaved_Mode
#endif
#endif

#if !defined(H264_FS_MAX_COUNT)
#	define H264_FS_MAX_COUNT			16 // max number of DPBFS in the list
#endif
#if !defined(H264_LEVEL_MAX_COUNT)
#	define H264_LEVEL_MAX_COUNT			16 // Table A-1 � Level limits: "1", "1b"... "5.1"
#endif

#if !defined(TDAV_H264_GOP_SIZE_IN_SECONDS)
#   define TDAV_H264_GOP_SIZE_IN_SECONDS		25
#endif

//-----------------------------------------------
//
//-----------------------------------------------
// Table A-1 � Level limits
extern const int32_t MaxMBPS[H264_LEVEL_MAX_COUNT];
extern const int32_t MaxFS[H264_LEVEL_MAX_COUNT];
extern const int32_t MaxDpbMbs[H264_LEVEL_MAX_COUNT];
extern const int32_t MaxBR[H264_LEVEL_MAX_COUNT];
extern const int32_t MaxCPB[H264_LEVEL_MAX_COUNT];

// From "enum level_idc_e" to zero-based index
// level_idc = (level.prefix * 10) + level.suffix. For example: 3.1 = 31. Exception for 1b = 9.
// usage: int32_t maxDpbMbs = MaxDpbMbs[HL_CODEC_264_LEVEL_TO_ZERO_BASED_INDEX[level_idc]];
extern const int32_t H264_LEVEL_TO_ZERO_BASED_INDEX[255];
//-----------------------------------------------
//
//-----------------------------------------------
// Because of FD, declare it here
enum packetization_mode_t
{
	Unknown_Mode = -1,
	Single_NAL_Unit_Mode = 0,		/* Single NAL mode (Only nals from 1-23 are allowed) */
	Non_Interleaved_Mode = 1,		/* Non-interleaved Mode: 1-23, 24 (STAP-A), 28 (FU-A) are allowed */
	Interleaved_Mode = 2			/* 25 (STAP-B), 26 (MTAP16), 27 (MTAP24), 28 (FU-A), and 29 (FU-B) are allowed.*/
};

struct h264_common_level_size_xt
{
	level_idc_t level;
	uint32_t width;
	uint32_t height;
	uint32_t maxFS; // From "Table A-1 � Level limits"
};

extern const h264_common_level_size_xt h264_common_level_sizes[];
//-----------------------------------------------
//
//-----------------------------------------------
class h264_common_t
{
protected:
	// base
	uint32_t m_width;
	uint32_t m_height;
	uint32_t m_fps;
	uint32_t m_last_ts;
	uint32_t m_last_clock;
	uint32_t m_max_br;
	uint32_t m_max_mbps;
	media::VideoChroma m_chroma;
	bool m_flip;

	//common
	profile_idc_t m_profile;
	uint8_t m_profile_iop;
	level_idc_t m_level;
	uint32_t m_maxFS;

	packetization_mode_t m_pack_mode_remote; // remote packetization mode
	packetization_mode_t m_pack_mode_local; // local packetization mode

	rtx_video_encoder_cb_t m_enc_callback;
	void* m_user;

	struct
	{
		uint8_t* ptr;
		size_t size;
	} m_rtp;

public:
	h264_common_t(rtx_video_encoder_cb_t cb, void* usr);
	~h264_common_t();

	bool sdp_att_match(const char* att_name, const char* att_value);
	rtl::String sdp_att_get(const char* att_name) const;

protected:
	void h264_rtp_encap(const uint8_t* pdata, size_t size);
	void h264_rtp_callback(const void *data, size_t size, uint32_t ts, bool marker);

private:
	bool set_profile_level_id(const char* value);
};
//-----------------------------------------------
//
//-----------------------------------------------
int h264_common_size_from_level(level_idc_t level, uint32_t *width, uint32_t *height);
int h264_common_size_from_fs(uint32_t maxFS, uint32_t *width, uint32_t *height);
int h264_common_level_from_size(uint32_t width, uint32_t height, level_idc_t *level);
bool h264_common_get_profile_and_level(const char* value, profile_idc_t *profile, level_idc_t *level);
