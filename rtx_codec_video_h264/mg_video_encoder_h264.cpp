/* RTX H.248 Media Gate. H.264 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_video_encoder_h264.h"

extern "C" {
#include <libavcodec/avcodec.h>
#if (LIBAVUTIL_VERSION_INT >= AV_VERSION_INT(51, 35, 0))
#include <libavutil/opt.h>
#endif
}

mg_video_encoder_h264_t::mg_video_encoder_h264_t(rtl::Logger& log, rtx_video_encoder_cb_t cb, void* usr) :
	h264_common_t(cb, usr),
	m_log(log),
	m_codec(nullptr), m_context(nullptr), m_picture(nullptr),
	m_buffer(nullptr), m_frame_count(0), 
	m_force_idr(false), m_quality(1),
	m_max_bw_kpbs(0), m_passthrough(false)
{
	rtl::res_counter_t::add_ref(g_rtx_encoder_counter);
}

mg_video_encoder_h264_t::~mg_video_encoder_h264_t()
{
	rtl::res_counter_t::release(g_rtx_encoder_counter);
}

extern void avcodec_get_frame_defaults(AVFrame *frame);

bool mg_video_encoder_h264_t::initialize(const media::PayloadFormat* fmt)
{
	sdp_att_match("fmtp", fmt->getFmtp());

	init(m_profile);

	int ret;
	size_t size;

	if (m_context) {
		//TSK_DEBUG_ERROR("Encoder already opened");
		return false;
	}

	if ((m_context = avcodec_alloc_context3(m_codec)))
	{
		avcodec_get_context_defaults3(m_context, m_codec);
	}

	if (!m_context)
	{
		//TSK_DEBUG_ERROR("Failed to allocate context");
		return false;
	}

	m_context->pix_fmt = AV_PIX_FMT_YUV420P;
	m_fps = 15;
	m_context->time_base.num = 1;
	m_context->time_base.den = m_fps;
	m_context->width = m_width;
	m_context->height = m_height;
	//m_context->sample_aspect_ratio = {1,0};
	m_max_bw_kpbs = VIDEO_CLAMP(
		0,
		media::getVideoBandwidthKbps2(m_width, m_height, m_fps),
		INT32_MAX
		);
	m_context->bit_rate = 768 * 1024;//(m_max_bw_kpbs * 1024);// bps

	m_context->rc_min_rate = (m_context->bit_rate >> 3);
	m_context->rc_max_rate = m_context->bit_rate;

	m_context->global_quality = FF_QP2LAMBDA * m_quality;

	m_context->me_method = ME_UMH;
	m_context->me_range = 16;
	m_context->qmin = 10;
	m_context->qmax = 51;

	/* video time_base can be set to whatever is handy and supported by encoder */
	m_context->flags |= CODEC_FLAG_LOW_DELAY;
	
	if (m_context->profile == FF_PROFILE_H264_BASELINE)
	{
		m_context->max_b_frames = 0;
	}

	switch (m_profile)
	{
	case profile_idc_baseline:
	default:
		m_context->profile = FF_PROFILE_H264_BASELINE;
		m_context->level = m_level;
		break;
	case profile_idc_main:
		m_context->profile = FF_PROFILE_H264_MAIN;
		m_context->level = m_level;
		break;
	}

	/* Comment from libavcodec/libx264.c:
	* Allow x264 to be instructed through AVCodecContext about the maximum
	* size of the RTP payload. For example, this enables the production of
	* payload suitable for the H.264 RTP packetization-mode 0 i.e. single
	* NAL unit per RTP packet.
	*/
	m_context->rtp_payload_size = H264_RTP_PAYLOAD_SIZE;
	m_context->opaque = nullptr;
	m_context->gop_size = (m_fps * TDAV_H264_GOP_SIZE_IN_SECONDS);


	if ((ret = av_opt_set_int(m_context->priv_data, "slice-max-size", H264_RTP_PAYLOAD_SIZE, 0))) {
		//TSK_DEBUG_ERROR("Failed to set x264 slice-max-size to %d", H264_RTP_PAYLOAD_SIZE);
	}
	if ((ret = av_opt_set(m_context->priv_data, "profile", (m_context->profile == FF_PROFILE_H264_BASELINE ? "baseline" : "main"), 0))) {
		//TSK_DEBUG_ERROR("Failed to set x264 profile");
	}
	if ((ret = av_opt_set(m_context->priv_data, "preset", "veryfast", 0))) {
		//TSK_DEBUG_ERROR("Failed to set x264 preset to veryfast");
	}
	if ((ret = av_opt_set_int(m_context->priv_data, "rc-lookahead", 0, 0)) && (ret = av_opt_set_int(m_context->priv_data, "rc_lookahead", 0, 0))) {
		//TSK_DEBUG_ERROR("Failed to set x264 rc_lookahead=0");
	}
	if ((ret = av_opt_set(m_context->priv_data, "tune", "animation+zerolatency", 0))) {
		//TSK_DEBUG_ERROR("Failed to set x264 tune to zerolatency");
	}

	// Picture (YUV 420)
	if (!(m_picture = av_frame_alloc()))
	{
		//TSK_DEBUG_ERROR("Failed to create encoder picture");
		return false;
	}

	avcodec_get_frame_defaults(m_picture);

	size = avpicture_get_size(AV_PIX_FMT_YUV420P, m_context->width, m_context->height);
	if (!(m_buffer = MALLOC(size)))
	{
		//TSK_DEBUG_ERROR("Failed to allocate encoder buffer");
		return false;
	}

	// Open encoder
	if ((ret = avcodec_open2(m_context, m_codec, nullptr)) < 0)
	{
		char tmp[256] = { 0 };
		av_make_error_string(tmp, 256, ret);
		MLOG_ERROR("H264-ENC", "Failed to open H.264 codec -- error:%d '%s'", ret, tmp);
		return false;
	}

	m_frame_count = 0;

	//TSK_DEBUG_INFO("[H.264] bitrate=%d bps", m_context->bit_rate);

	return ret >= 0;
}

void mg_video_encoder_h264_t::destroy()
{
	if (m_context)
	{
		avcodec_close(m_context);
		av_free(m_context);
		m_context = nullptr;
	}
	if (m_picture)
	{
		av_free(m_picture);
		m_picture = nullptr;
	}

	if (m_buffer)
	{
		FREE(m_buffer);
	}
	m_frame_count = 0;
}

const char*	mg_video_encoder_h264_t::getEncoding()
{
	return "H264";
}

bool mg_video_encoder_h264_t::init(rtx_video_encoder_cb_t cb, void* user)
{
	m_enc_callback = cb;
	m_user = user;
	return true;
}

void mg_video_encoder_h264_t::reset()
{
}

bool mg_video_encoder_h264_t::encode(const media::VideoImage* picture)
{
	int ret = 0;

	int size;
	bool send_idr, send_hdr;
	const uint8_t* in_data = picture->getYData();
	size_t in_size = picture->getImageSize();

	size = avpicture_fill((AVPicture *)m_picture, (uint8_t*)in_data, AV_PIX_FMT_YUV420P, m_context->width, m_context->height);
		
	if (size != in_size)
	{
		/* guard */
		//TSK_DEBUG_ERROR("Invalid size: %u<>%u", size, in_size);
		return false;
	}

	// send IDR for:
	//	- the first frame
	//  - remote peer requested an IDR
	//	- every second within the first 4seconds
	send_idr = (m_frame_count++ == 0 || m_force_idr
		//|| ( (m_frame_count < (int)TMEDIA_CODEC_VIDEO(h264)->out.fps * 4) && ((m_frame_count % TMEDIA_CODEC_VIDEO(h264)->out.fps)==0) )
			);

	// send SPS and PPS headers for:
	//  - IDR frames (not required but it's the easiest way to deal with pkt loss)
	//  - every 5 seconds after the first 4seconds
	send_hdr = (
		send_idr
		//|| ( (m_frame_count % (TMEDIA_CODEC_VIDEO(h264)->out.fps * 5))==0 )
		);

	if (send_hdr)
	{
		h264_rtp_encap(m_context->extradata, m_context->extradata_size);
	}

	// Encode data
	m_picture->pict_type = send_idr ? AV_PICTURE_TYPE_I : AV_PICTURE_TYPE_NONE;

	m_picture->key_frame = send_idr ? 1 : 0;
	m_picture->pts = AV_NOPTS_VALUE;
	m_picture->quality = m_context->global_quality;

	m_picture->width = m_context->width;
	m_picture->height = m_context->height;
	m_picture->format = m_context->pix_fmt;

	// m_picture->pts = m_frame_count; MUST NOT
	AVPacket avp;
	
	av_init_packet(&avp);

	avp.data = (uint8_t*)m_buffer;
	avp.size = size;
	int got_picture = 0; // ?
	
	ret = avcodec_encode_video2(m_context, &avp, m_picture, &got_picture);
	
	if (ret > 0)
	{
		h264_rtp_encap((const uint8_t*)m_buffer, ret);
	}

	m_force_idr = false;

	return true;
}

void mg_video_encoder_h264_t::init(profile_idc_t profile)
{
	int ret = 0;
	level_idc_t level;

	if ((ret = h264_common_level_from_size(m_width, m_height, &level)))
	{
		//TSK_DEBUG_ERROR("Failed to find level for size=[%u, %u]", m_width, m_height);
		return;
	}

	m_max_bw_kpbs = INT32_MAX;
	m_pack_mode_local = H264_PACKETIZATION_MODE;
	m_profile = profile;
	m_level = level;
	m_max_mbps = H264_MAX_MBPS * 1000;
	m_max_br = H264_MAX_BR * 1000;


	if (!(m_codec = avcodec_find_encoder(AV_CODEC_ID_H264)))
	{
		//TSK_DEBUG_ERROR("Failed to find H.264 encoder");
		ret = -2;
	}

	m_quality = 1;

	/* allocations MUST be done by open() */
	return;
}
