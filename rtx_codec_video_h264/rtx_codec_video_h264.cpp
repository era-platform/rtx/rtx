/* RTX H.248 Media Gate. H.264 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtx_codec_video_h264.h"
#include "mg_video_decoder_h264.h"
#include "mg_video_decoder_h264_v2.h"
#include "mg_video_encoder_h264.h"
#include "mg_video_encoder_h264_v2.h"

#ifdef __cplusplus
extern "C" {
#endif
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavfilter/avfiltergraph.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavutil/opt.h>
#include <libavutil/pixdesc.h>

#include <libavutil/imgutils.h>
#include <libavutil/parseutils.h>
#include <libswscale/swscale.h>

#ifdef __cplusplus
}
#endif

extern int g_rtx_decoder_counter = -1;
extern int g_rtx_encoder_counter = -1;

#define USE_V2_DEC	1
#define USE_V2_ENC	1
//-----------------------------------------------
//
//-----------------------------------------------
//rtl::Logger h264_log;
static void __h264_log_callback(void *avcl, int level, const char *fmt, va_list vl)
{
	//char tfmt[1024];
	//std_snprintf(tfmt, 1024, "ffmpeg(lvl:%d) --> %s", level, fmt);
	//if (level == 16 || level == 32)
	//	h264_log.log_error_formatted("ffmpeg", "error --- > ", tfmt, vl);
	//else
	//	h264_log.log_format("ffmpeg", tfmt, vl);
}
//-----------------------------------------------
//
//-----------------------------------------------
bool initlib()
{
	g_rtx_decoder_counter = rtl::res_counter_t::add("mge_h264_decoder");
	g_rtx_encoder_counter = rtl::res_counter_t::add("mge_h264_encoder");

	//h264_log.create(Log.get_folder(), "h264", LOG_APPEND);
	//av_log_set_callback(__h264_log_callback);
	//av_log_set_level(AV_LOG_TRACE);

	av_register_all();
	avfilter_register_all();

	return true;
}
//-----------------------------------------------
//
//-----------------------------------------------
void freelib()
{
	//h264_log.destroy();
}
//-----------------------------------------------
//
//-----------------------------------------------
const char* get_codec_list()
{
	return "H264";
}
//-----------------------------------------------
//
//-----------------------------------------------
//bool get_video_codec_info(const PayloadFormat* format, mmt_image_t* info)
//{
//	return false;
//}
//-----------------------------------------------
//
//-----------------------------------------------
mg_video_decoder_t* create_video_decoder(const media::PayloadFormat* format)
{
#if defined (USE_V2_DEC)
	mg_video_decoder_h264_v2_t* dec = NEW mg_video_decoder_h264_v2_t(Log);
#else
	mg_video_decoder_h264_t* dec = NEW mg_video_decoder_h264_t(Log);
#endif

	// resolution? bitrate?

	if (dec->initialize(format))
	{
		return dec;
	}

	DELETEO(dec);
	return nullptr;
}
//-----------------------------------------------
//
//-----------------------------------------------
void release_video_decoder(mg_video_decoder_t* decoder)
{
#if defined (USE_V2_DEC)
	mg_video_decoder_h264_v2_t* dec = (mg_video_decoder_h264_v2_t*)decoder;
#else
	mg_video_decoder_h264_t* dec = (mg_video_decoder_h264_t*)decoder;
#endif

	DELETEO(dec);
}
//-----------------------------------------------
//
//-----------------------------------------------
mg_video_encoder_t* create_video_encoder(const media::PayloadFormat* format, rtx_video_encoder_cb_t cb, void* usr)
{
#if defined (USE_V2_ENC)
	mg_video_encoder_h264_v2_t* enc = NEW mg_video_encoder_h264_v2_t(Log, cb, usr);
#else
	mg_video_encoder_h264_t* enc = NEW mg_video_encoder_h264_t(Log, cb, usr);
#endif

	if (enc->initialize(format))
	{
		return enc;
	}

	DELETEO(enc);
	return nullptr;
}
//-----------------------------------------------
//
//-----------------------------------------------
void release_video_encoder(mg_video_encoder_t* encoder)
{
#if defined (USE_V2_ENC)
	mg_video_encoder_h264_v2_t* enc = (mg_video_encoder_h264_v2_t*)encoder;
#else
	mg_video_encoder_h264_t* enc = (mg_video_encoder_h264_t*)encoder;
#endif

	DELETEO(enc);
}
//-----------------------------------------------
//
//-----------------------------------------------
static uint32_t encoding_to_id(const char* encoding)
{
	if (std_stricmp(encoding, "h264") == 0)
	{
		return AV_CODEC_ID_H264;
	}

	return AV_CODEC_ID_NONE;
}
//-----------------------------------------------
