/* RTX H.248 Media Gate. H.264 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_crypto.h"

#include "mg_video_decoder_h264_v2.h"

#define LOG_PREFIX "H264-D"
#define H264_CLOCK_RATE				90000
#define DEFAULT_NAL_BUFFER_SIZE		32*1024
#define NAL_TYPE_UNDEFINED		0
#define NAL_TYPE_NAL_FROM		1
#define NAL_TYPE_NAL_TO			23
#define NAL_TYPE_STAPA			24
#define NAL_TYPE_STAPB			25
#define NAL_TYPE_MTAP16			26
#define NAL_TYPE_MTAP24			27
#define NAL_TYPE_FUA			28
#define NAL_TYPE_FUB			29

static volatile uint32_t g_gen_id = 1;
//-----------------------------------------------
//
//-----------------------------------------------
void avcodec_get_frame_defaults(AVFrame *frame);
//-----------------------------------------------
//
//-----------------------------------------------
mg_video_decoder_h264_v2_t::mg_video_decoder_h264_v2_t(rtl::Logger& log) :
	m_log(log),
	m_av_codec(nullptr),
	m_av_codec_context(nullptr),
	m_av_frame(nullptr),
	m_av_packet(nullptr)
{
#if defined (VIDEO_TEST)
	m_outfile = nullptr;
#endif

	m_id = std_interlocked_inc(&g_gen_id);

#if defined (VIDEO_RTP_TEST)
	m_rtpfile = nullptr;
#endif

	rtl::res_counter_t::add_ref(g_rtx_decoder_counter);
}

mg_video_decoder_h264_v2_t::~mg_video_decoder_h264_v2_t()
{
	destroy();

#if defined (VIDEO_TEST)
	if (m_outfile != nullptr)
	{
		fclose(m_outfile);
		m_outfile = nullptr;
	}
#endif

#if defined (VIDEO_RTP_TEST)
	if (m_rtpfile != nullptr)
	{
		fclose(m_rtpfile);
		m_rtpfile = nullptr;
	}
#endif

	rtl::res_counter_t::release(g_rtx_decoder_counter);
}

bool mg_video_decoder_h264_v2_t::initialize(const media::PayloadFormat* fmt)
{
	m_parser = NEW h264_parser(m_log, m_id);
	m_depacketizer = NEW h264_rtp_depacketizer(m_log, m_id);

	init();
	m_parser->init();
	m_depacketizer->init();
	setFmtp(fmt->getFmtp());

#if defined(VIDEO_TEST)

	static int nnn = 1;
	char fname[128];
	nnn++;
	std_snprintf(fname, 128, "c:\\temp\\outfile_h264_v2%d.raw", nnn);
	m_outfile = fopen(fname, "wb");

	//bool b = (m_outfile != nullptr);
	//MLOG_CALL("VP8-DEC", "open video raw file %s", STR_BOOL(b));
#endif

#if defined (VIDEO_RTP_TEST)
	static int nnnn = 1;
	char fnamer[128];
	nnnn++;
	std_snprintf(fnamer, 128, "c:\\temp\\outfile_h264_v2%d.rtp", nnn);
	m_rtpfile = fopen(fnamer, "wb");
#endif

	return true;
}

void mg_video_decoder_h264_v2_t::destroy()
{
	if (m_parser != nullptr)
	{
		DELETEO(m_parser);
		m_parser = nullptr;
	}

	if (m_depacketizer != nullptr)
	{
		DELETEO(m_depacketizer);
		m_depacketizer = nullptr;
	}

	dispose();

#if defined(VIDEO_TEST)
	if (m_outfile != nullptr)
	{
		fclose(m_outfile);
		m_outfile = nullptr;
	}
#endif

#if defined (VIDEO_RTP_TEST)
	if (m_rtpfile != nullptr)
	{
		fclose(m_rtpfile);
		m_rtpfile = nullptr;
	}
#endif

}

const char*	mg_video_decoder_h264_v2_t::getEncoding()
{
	return "H264";
}

media::VideoImage* mg_video_decoder_h264_v2_t::decode(const rtp_packet* packet)
{
	MLOG_CALL(LOG_PREFIX, "id:% 4u : decode : RTP SSRC %08X : seq:%05u len:%05u m:%02u ts:%08x", m_id, packet->get_ssrc(), packet->get_sequence_number(),
		packet->get_payload_length(), packet->get_marker() ? 1 : 0, packet->getTimestamp());

#if defined(VIDEO_RTP_TEST)
	fwrite("RTPV", 4, 1, m_rtpfile);
	uint32_t len = packet->get_full_length();
	fwrite(&len, 4, 1, m_rtpfile);
	uint8_t buffer[2048];
	len = packet->write_packet(buffer, 2048);
	fwrite(buffer, len, 1, m_rtpfile);
#endif

	media::VideoImage* image = nullptr;

	if (m_depacketizer->depacketize(packet)) // ->get_payload(), packet->get_payload_length(), packet->get_marker(), packet->getTimestamp()
	{
		const t_video_nal_array& array = m_depacketizer->current_nals();

		for (int i = 0; i < array.getCount(); ++i)
		{
			const t_video_nal_data& nal = array[i];

			unsigned processed = 0;

			while (processed < nal.len)
			{
				processed += m_parser->parse(nal.ptr + processed, nal.len - processed);
				
				if (m_parser->parsed_frame() == nullptr)
					continue;

				bool got_picture = false;

				if (!decode_frame(m_parser->parsed_frame(), m_parser->parsed_frame_size(), got_picture, image))
				{
					MLOG_WARN(LOG_PREFIX, "id:% 4u : decode : decoder error!", m_id);
					continue; // break;
				}

				if (!got_picture)
					continue; // break;
			}
		}
	}
	else
	{
		MLOG_WARN(LOG_PREFIX, "id:% 4u : decode : depacketizer error while depacketizing!", m_id);
	}

	bool result = image != nullptr;

	if (result)
	{
		MLOG_CALL(LOG_PREFIX, "id:% 4u : decode : done : picture dim %dx%d size %d bytes", m_id, image->getWidth(), image->getHeight(), image->getImageSize());
	}
	else
	{
		MLOG_CALL(LOG_PREFIX, "id:% 4u : decode : done : picture not ready", m_id);
	}

	return image;// result;
}

bool mg_video_decoder_h264_v2_t::init()
{
	MLOG_CALL(LOG_PREFIX, "id:% 4u : initialize...", m_id);

	dispose();

	if (!init_av_codec_context())
		return false;

	if (!init_av_codec())
		return false;

	if (!init_av_frame())
		return false;

	if (!init_av_packet())
		return false;

	MLOG_CALL(LOG_PREFIX, "id:% 4u : initialized", m_id);

	return true;
}

void mg_video_decoder_h264_v2_t::dispose()
{
	MLOG_CALL(LOG_PREFIX, "id:% 4u : disposing...", m_id);

	free_av_codec();

	free_av_codec_context();

	free_av_frame();

	free_av_packet();

	MLOG_CALL(LOG_PREFIX, "id:% 4u : disposed", m_id);
}

void mg_video_decoder_h264_v2_t::setFmtp(const char* fmtp_c_str)
{
	if (!fmtp_c_str || fmtp_c_str[0] == 0)
		return;

	MLOG_CALL(LOG_PREFIX, "id:% 4u : setFmtp -- '%s'", m_id, fmtp_c_str);

	//���� "sprop-parameter-sets="
	using namespace std;
	rtl::String fmtp = fmtp_c_str;
	rtl::String sprop("sprop-parameter-sets=");
	int pos = fmtp.indexOf(sprop);
	if (pos == BAD_INDEX)
		return;

	//��� ��� �� - ������
	fmtp.remove(0, pos + sprop.getLength());

	//��� ��� ����� - ������
	pos = fmtp.indexOf(";");
	if (pos == BAD_INDEX)
		pos = fmtp.indexOf("\r");
	if (pos == BAD_INDEX)
		pos = fmtp.indexOf("\n");
	if (pos != BAD_INDEX)
		fmtp.remove(pos);

	if (fmtp.isEmpty())
		return;

	//��������� ������ �� �������� ','
	rtl::StringList vec;
	rtl::String separator = ",";
	pos = fmtp.indexOf(separator);
	while (pos != BAD_INDEX)
	{
		vec.add(fmtp.substring(0, pos));
		fmtp.remove(0, pos + separator.getLength());
		pos = fmtp.indexOf(separator);
	}

	//������ ������ ���������� �� base64 � nal � �������� � �������
	for (int i = 0; i < vec.getCount(); ++i)
	{
		const rtl::String& str = vec[i];

		int len = Base64_CalcDecodedSize((int)str.getLength());
		
		if (len <= 0)
			continue;

		uint8_t* ptr = NEW uint8_t[len];

		Base64Decode(str, str.getLength(), ptr, len);

		//decode_frame(ptr, len, got_picture, nullptr);
		m_av_packet->data = (uint8_t*)ptr;
		m_av_packet->size = len;
		int got_picture = 0;
		int used = avcodec_decode_video2(m_av_codec_context, m_av_frame, &got_picture, m_av_packet);

		DELETEAR(ptr);
	}
}

bool mg_video_decoder_h264_v2_t::decode_frame(const uint8_t* data, unsigned len, bool& picture_decoded, media::VideoImage*& image)
{
	picture_decoded = false;
	m_av_packet->data = (uint8_t*)data;
	m_av_packet->size = len;

	int got_picture = 0;

	MLOG_CALL(LOG_PREFIX, "id:% 4u : decode : data: %p, size: %d", m_id, data, len);

	int used = avcodec_decode_video2(m_av_codec_context, m_av_frame, &got_picture, m_av_packet);
	if (used < 0)
	{
		char t[256];
		av_strerror(used, t, 256);
		MLOG_ERROR(LOG_PREFIX, "id:% 4u : decode : failed, avcodec_decode_video2() return (%d) : %s", m_id, used, t);
		return false;
	}

	MLOG_CALL(LOG_PREFIX, "id:% 4u : decode : used: %d, got_picture: %d, %dx%d -> %dx%d", m_id, used, got_picture, m_av_frame->width, m_av_frame->height,
		m_av_codec_context->width, m_av_codec_context->height);

	if (got_picture != 0)
	{
		if (image == nullptr)
		{
			int xsize = avpicture_get_size(m_av_codec_context->pix_fmt, m_av_codec_context->width, m_av_codec_context->height);
			image = NEW media::VideoImage(m_av_codec_context->width, m_av_codec_context->height);
			int retsize = avpicture_layout((AVPicture *)m_av_frame, m_av_codec_context->pix_fmt,
				m_av_codec_context->width, m_av_codec_context->height,
				image->getImageBuffer(), (int)xsize);

//			image->update_buffer_size(retsize);

#if defined(VIDEO_TEST)
			fwrite(image->get_image_buffer(), retsize, 1, m_outfile);
#endif
			picture_decoded = true;

			MLOG_CALL(LOG_PREFIX, "id:% 4u : decode : done: %dx%d", m_id, image->getWidth(), image->getHeight());
		}
	}

	return image != nullptr;
}

//----------------------------------------
//
//----------------------------------------
bool mg_video_decoder_h264_v2_t::init_av_codec_context()
{
	m_av_codec_context = avcodec_alloc_context3(nullptr);
	if (m_av_codec_context == NULL)
	{
		MLOG_ERROR(LOG_PREFIX, "id:% 4u : init_av_codec_context : failed, avcodec_alloc_context() return NULL", m_id);
		return false;
	}

	return true;
}
//----------------------------------------
//
//----------------------------------------
void mg_video_decoder_h264_v2_t::free_av_codec_context()
{
	if (m_av_codec_context == NULL)
		return;

	av_free(m_av_codec_context);
	m_av_codec_context = NULL;
}
//----------------------------------------
//
//----------------------------------------
bool mg_video_decoder_h264_v2_t::init_av_codec()
{
	m_av_codec = avcodec_find_decoder(AV_CODEC_ID_H264);
	if (m_av_codec == NULL)
	{
		MLOG_ERROR(LOG_PREFIX, "id:% 4u : init_av_codec : failed, avcodec_find_decoder() return NULL", m_id);
		return false;
	}

	int error = avcodec_open2(m_av_codec_context, m_av_codec, nullptr);
	if (error != 0)
	{
		MLOG_ERROR(LOG_PREFIX, "id:% 4u : init_av_codec : failed, avcodec_open() return %d", m_id, error);
		return false;
	}

	return true;
}
//----------------------------------------
//
//----------------------------------------
void mg_video_decoder_h264_v2_t::free_av_codec()
{
	if (m_av_codec == NULL)
		return;

	avcodec_close(m_av_codec_context);

	//������ �����������, ����������� ������
	m_av_codec = NULL;
}
//----------------------------------------
//
//----------------------------------------
bool mg_video_decoder_h264_v2_t::init_av_frame()
{
	m_av_frame = av_frame_alloc();
	if (m_av_frame == NULL)
	{
		MLOG_ERROR(LOG_PREFIX, "id:% 4u : init_av_frame : failed, avcodec_alloc_frame() return NULL", m_id);
		return false;
	}

	return true;
}
//----------------------------------------
//
//----------------------------------------
void mg_video_decoder_h264_v2_t::free_av_frame()
{
	if (m_av_frame == NULL)
		return;

	av_free(m_av_frame);
	m_av_frame = NULL;
}
//----------------------------------------
//
//----------------------------------------
bool mg_video_decoder_h264_v2_t::init_av_packet()
{
	unsigned size = sizeof(AVPacket);

	//����� � ������� ��� �������� � �������
	m_av_packet = (AVPacket*)av_malloc(size);
	if (m_av_packet == NULL)
	{
		MLOG_ERROR(LOG_PREFIX, "id:% 4u : init_av_packet -- failed, av_malloc(%d) return NULL", m_id, size);
		return false;
	}

	av_init_packet(m_av_packet);

	return true;
}
//----------------------------------------
//
//----------------------------------------
void mg_video_decoder_h264_v2_t::free_av_packet()
{
	if (m_av_packet == NULL)
		return;

	av_free_packet(m_av_packet);
	av_free(m_av_packet);
	
	m_av_packet = NULL;
}
//---------------------------------------------
//
//---------------------------------------------
h264_parser::h264_parser(rtl::Logger& log, uint32_t id) : m_id(id),
	m_log(log), m_av_parser_context(NULL), m_av_codec(NULL),
	m_av_codec_context(NULL), m_parsed_frame(NULL), m_parsed_size(0)
{
}
//---------------------------------------------
//
//---------------------------------------------
h264_parser::~h264_parser()
{
	dispose();
}
//---------------------------------------------
//
//---------------------------------------------
bool h264_parser::init()
{
	MLOG_CALL(LOG_PREFIX, "id:% 4u : parser initializing...", m_id);

	dispose();

	if (!init_av_codec_context())
		return false;

	if (!init_av_codec())
		return false;

	if (!init_parser_context())
		return false;

	MLOG_CALL(LOG_PREFIX, "id:% 4u : parser initialized", m_id);

	return true;
}
//---------------------------------------------
//
//---------------------------------------------
void h264_parser::dispose()
{
	MLOG_CALL(LOG_PREFIX, "id:% 4u : parser disposing...", m_id);

	free_av_codec();

	free_av_codec_context();

	free_parser_context();

	m_parsed_frame = NULL;
	m_parsed_size = 0;

	MLOG_CALL(LOG_PREFIX, "id:% 4u : parser disposed", m_id);
}
//---------------------------------------------
//
//---------------------------------------------
bool h264_parser::init_av_codec_context()
{
	m_av_codec_context = avcodec_alloc_context3(nullptr);
	if (m_av_codec_context == NULL)
	{
		MLOG_ERROR(LOG_PREFIX, "id:% 4u : init_av_codec_context : failed, avcodec_alloc_context() return NULL", m_id);
		return false;
	}

	return true;
}
//---------------------------------------------
//
//---------------------------------------------
void h264_parser::free_av_codec_context()
{
	if (m_av_codec_context == NULL)
		return;

	av_free(m_av_codec_context);
	m_av_codec_context = NULL;
}
//---------------------------------------------
//
//---------------------------------------------
bool h264_parser::init_av_codec()
{
	m_av_codec = avcodec_find_decoder(AV_CODEC_ID_H264);
	if (m_av_codec == NULL)
	{
		MLOG_ERROR(LOG_PREFIX, "id:% 4u : init_av_codec : failed, avcodec_find_decoder() return NULL", m_id);
		return false;
	}

	int error = avcodec_open2(m_av_codec_context, m_av_codec, nullptr);
	if (error != 0)
	{
		MLOG_ERROR(LOG_PREFIX, "id:% 4u : init_av_codec : failed, avcodec_open() return %d", m_id, error);
		return false;
	}

	return true;
}
//---------------------------------------------
//
//---------------------------------------------
void h264_parser::free_av_codec()
{
	if (m_av_codec == NULL)
		return;

	avcodec_close(m_av_codec_context);

	//������ �����������, ����������� ������
	m_av_codec = NULL;
}
//---------------------------------------------
//
//---------------------------------------------
bool h264_parser::init_parser_context()
{
	m_av_parser_context = av_parser_init(AV_CODEC_ID_H264);
	if (m_av_parser_context == NULL)
	{
		MLOG_ERROR(LOG_PREFIX, "id:% 4u : init_parser_context : failed, av_parser_init() return NULL", m_id);
		return false;
	}

	return true;
}
//---------------------------------------------
//
//---------------------------------------------
void h264_parser::free_parser_context()
{
	if (m_av_parser_context == NULL)
		return;

	av_parser_close(m_av_parser_context);
	m_av_parser_context = NULL;
}
//---------------------------------------------
//
//---------------------------------------------
unsigned h264_parser::parse(const uint8_t* data, unsigned len)
{
	m_parsed_frame = NULL;
	m_parsed_size = 0;

	if ((data == NULL) || (len == 0))
		return 0;

	MLOG_CALL(LOG_PREFIX, "id:% 4u : parse : parsing data: %p, size: %d", m_id, data, len);

	uint8_t* buffer = NULL;
	int buffer_size = 0;

	int used = av_parser_parse2(m_av_parser_context, m_av_codec_context,
		&buffer, &buffer_size, data, (int)len, 0, 0, 0);

	MLOG_CALL(LOG_PREFIX, "id:% 4u : parse : used: %d, buffer: %p, buffer_size: %d", m_id, used, buffer, buffer_size);

	//����� �����?
	if ((buffer != NULL) && (buffer_size != 0))
	{
		m_parsed_frame = buffer;
		m_parsed_size = (unsigned)buffer_size;
	}

	return (unsigned)used;
}
//------------------------------------------
//
//------------------------------------------
h264_rtp_depacketizer::h264_rtp_depacketizer(rtl::Logger& log, uint32_t id) : m_id(id),
	m_log(log), m_current_is_fua(false),
	m_nal_buffer(NULL), m_nal_buf_size(0), m_nal_buf_pos(0)
{
	m_ts = 0;
	m_seq = 0;
	m_new_session = true;
	m_ready_to_next = true;
}
//------------------------------------------
//
//------------------------------------------
h264_rtp_depacketizer::~h264_rtp_depacketizer(void)
{
	dispose();
}
//------------------------------------------
//
//------------------------------------------
bool h264_rtp_depacketizer::init()
{
	MLOG_CALL(LOG_PREFIX, "id:% 4u : depacketizer initializing...", m_id);

	dispose();

	m_nal_buf_size = DEFAULT_NAL_BUFFER_SIZE;
	m_nal_buffer = NEW uint8_t[m_nal_buf_size];

	MLOG_CALL(LOG_PREFIX, "id:% 4u : depacketizer initialized", m_id);

	return true;
}
//------------------------------------------
//
//------------------------------------------
void h264_rtp_depacketizer::dispose()
{
	MLOG_CALL(LOG_PREFIX, "id:% 4u : depacketizer disposing...", m_id);

	clear_nal_pool();

	if (m_nal_buffer != NULL)
	{
		DELETEAR(m_nal_buffer);
		m_nal_buffer = NULL;
	}
	m_nal_buf_size = 0;
	m_nal_buf_pos = 0;

	m_current_is_fua = false;

	MLOG_CALL(LOG_PREFIX, "id:% 4u : depacketizer disposed", m_id);
}
//------------------------------------------
//
//------------------------------------------
bool h264_rtp_depacketizer::depacketize(const rtp_packet* packet) // const uint8_t* data, unsigned length, bool marker, unsigned timestamp
{
	bool marker = packet->get_marker();
	unsigned timestamp = packet->getTimestamp();
	uint16_t cseq = packet->get_sequence_number();

	if (!m_new_session && cseq != m_seq + 1)
	{
		// drop frame assembly -- prevous packets lost
		MLOG_ERROR(LOG_PREFIX, "id:% 4u : depacketize --> drop frame assembly -- new:%s cseq:% 5u last_cseq:% 5u previous packets lost", m_id, STR_BOOL(m_new_session), cseq, m_seq);
		reset();
		return false;
	}

	if (!m_ready_to_next && m_ts != timestamp)
	{
		// drop frame assembly -- timestamp changed without marked packet
		MLOG_WARN(LOG_PREFIX, "id:% 4u : depacketize --> drop frame assembly -- ready:%s m:%s ts diff % 10d ... timestamp changed without marked packet", m_id,
			STR_BOOL(m_ready_to_next), STR_BOOL(marker), int(timestamp - m_ts));
		reset();
		//return false;
	}

	const uint8_t* data = packet->get_payload();
	unsigned length = packet->get_payload_length();
	
	if (data == nullptr || length == 0)
	{
		// ???
		MLOG_ERROR(LOG_PREFIX, "id:% 4u : depacketize --> drop frame assembly -- empty packet!!!", m_id);
		reset();
		return false;
	}

	m_new_session = true;
	m_ts = timestamp;
	m_seq++;
	m_ready_to_next = marker;

	bool res = false;
	uint8_t type = data[0] & 0x1F;

	if ((type >= NAL_TYPE_NAL_FROM) && (type <= NAL_TYPE_NAL_TO))
	{
		add_single(data, length);
		res = true;
	}
	else if (type == NAL_TYPE_FUA)
	{
		add_fua(data, length);
		res = true;
	}
	else if (type == NAL_TYPE_STAPA)
	{
		add_stapa(data, length);
		res = true;
	}
	else
	{
		MLOG_ERROR(LOG_PREFIX, "id:% 4u : depacketize -- failed, unsupported nal type: %u", m_id, type);
		res = false;
	}

	MLOG_CALL(LOG_PREFIX, "id:% 4u : depacketize <-- nal_count: %d", m_id, m_nal_list.getCount());

	return res;
}
//------------------------------------------
//
//------------------------------------------
void h264_rtp_depacketizer::clear_nal_pool()
{
	m_nal_list.clear();
}
//------------------------------------------
//
//------------------------------------------
void h264_rtp_depacketizer::check_buffer_space(unsigned need_space)
{
	//��������� ������ ������, ���� ����� �������� ������
	//� �������� ���� ������

	unsigned free = m_nal_buf_size - m_nal_buf_pos;
	if (free < need_space)
	{
		m_nal_buf_size = (unsigned)((m_nal_buf_pos + need_space) * 1.5);
		uint8_t* new_buffer = NEW uint8_t[m_nal_buf_size];
		if (m_nal_buf_pos != 0)
			memcpy(new_buffer, m_nal_buffer, m_nal_buf_pos);
		delete[] m_nal_buffer;
		m_nal_buffer = new_buffer;
	}
}
//------------------------------------------
//
//------------------------------------------
void h264_rtp_depacketizer::reset()
{
	clear_nal_pool();

	m_current_is_fua = false;
	m_nal_buf_pos = 0;
}
//------------------------------------------
//
//------------------------------------------
unsigned h264_rtp_depacketizer::stapa_compute_buffer_size(const uint8_t* data, unsigned len, unsigned& nal_count)
{
	unsigned res = 0;
	nal_count = 0;

	while (len != 0)
	{
		unsigned short unit_len = data[0];
		unit_len += (data[1] << 8);

		data += 2;
		len -= 2;

		//�� ������ ��������� �������� unit_len
		if (unit_len > len)
		{
			nal_count = 0;
			MLOG_WARN(LOG_PREFIX, "id:% 4u : add_stapa -- invalid nal unit length", m_id);
			return 0;
		}

		res += 4 + unit_len;//4 ����� ��������� ������������������
		data += unit_len;
		len -= unit_len;
		++nal_count;
	}

	return res;
}
//------------------------------------------
//
//------------------------------------------
void h264_rtp_depacketizer::add_single(const uint8_t* data, unsigned len)
{
	MLOG_CALL(LOG_PREFIX, "id:% 4u : add_single -- data: %p, size: %d", m_id, data, len);

	//����� ����� ��������� ������� �� ����

	//clear_nal_pool();

	//m_current_is_fua = false;
	//m_nal_buf_pos = 0;

	reset();

	check_buffer_space(len + 4);

	m_nal_buffer[0] = 0x00;
	m_nal_buffer[1] = 0x00;
	m_nal_buffer[2] = 0x00;
	m_nal_buffer[3] = 0x01;
	m_nal_buf_pos += 4;

	memcpy(m_nal_buffer + m_nal_buf_pos, data, len);
	m_nal_buf_pos += len;

	m_nal_list.add({ m_nal_buffer, m_nal_buf_pos });
}
//------------------------------------------
//
//------------------------------------------
void h264_rtp_depacketizer::add_fua(const uint8_t* data, unsigned len)
{
	//FU-������ - ��� ���� ���. �������� �� ��������� rtp �������

	//FU ������ ����� ���� ��� ������ � ��������� ������ 2 ����� ����������
	if (len <= 2)
		return;

	bool is_first = (data[1] & 0x80) != 0; //1 ��� ����������, ��� ��� ������ FU-�����
	bool is_last = (data[1] & 0x40) != 0;	//2 ��� ����������, ��� ��� ��������� FU-�����

	MLOG_CALL(LOG_PREFIX, "id:% 4u : add_fua -- data: %p, size: %d, first: %d, last: %d",
		m_id, data, len, is_first ? 1 : 0, is_last ? 1 : 0);

	if (is_first &&  is_last)
		return;

	if (is_first)
	{
		//clear_nal_pool();
		reset();

		//��������� ������������������ ��� ����
		m_nal_buffer[0] = 0x00;
		m_nal_buffer[1] = 0x00;
		m_nal_buffer[2] = 0x00;
		m_nal_buffer[3] = 0x01;

		//��������� ����� �� ��������� FU-������ � ��������� ��������� ����
		char flag = data[0] & 0x80;
		char nri = data[0] & 0x60;
		char type = data[1] & 0x1F;
		m_nal_buffer[4] = 0;
		m_nal_buffer[4] |= flag;
		m_nal_buffer[4] |= nri;
		m_nal_buffer[4] |= type;

		m_nal_buf_pos = 5;
		m_current_is_fua = true;
	}

	if (!m_current_is_fua)
		return;

	//2 ����� �� ����� - ��� ���������
	data += 2;
	len -= 2;

	check_buffer_space(len);

	memcpy(m_nal_buffer + m_nal_buf_pos, data, len);
	m_nal_buf_pos += len;

	if (is_last)
	{
		m_current_is_fua = false;
		m_nal_list.add({ m_nal_buffer, m_nal_buf_pos });
	}
}
//------------------------------------------
//
//------------------------------------------
void h264_rtp_depacketizer::add_stapa(const uint8_t* data, unsigned len)
{
	//clear_nal_pool();

	//m_current_is_fua = false;

	reset();

	//������ ���� ��� ������� 3 ����� ���������
	if (len < 3)
		return;

	//������ ���� ����������, �� �� ���������
	++data;
	--len;

	//��������� ������ ������
	m_nal_buf_pos = 0;
	unsigned nal_count = 0;
	unsigned need_len = stapa_compute_buffer_size(data, len, nal_count);
	if (need_len == 0)
		return;
	check_buffer_space(need_len);

	MLOG_CALL(LOG_PREFIX, "id:% 4u : add_stapa -- data: %p, size: %d, nal_count: %d", m_id,
		data - 1, len + 1, nal_count);

	while (len != 0)
	{
		t_video_nal_data nal = { m_nal_buffer + m_nal_buf_pos, 4 };

		m_nal_buffer[m_nal_buf_pos] = 0x00;
		m_nal_buffer[m_nal_buf_pos + 1] = 0x00;
		m_nal_buffer[m_nal_buf_pos + 2] = 0x00;
		m_nal_buffer[m_nal_buf_pos + 3] = 0x01;
		m_nal_buf_pos += 4;

		m_nal_list.add(nal);

		unsigned short unit_len = data[0];
		unit_len += (data[1] << 8);
		data += 2;
		len -= 2;

		memcpy(m_nal_buffer + m_nal_buf_pos, data, unit_len);
		m_nal_buf_pos += unit_len;

		data += unit_len;
		len -= unit_len;
		m_nal_list.getLast().len += unit_len;
	}
}
//---------------------------------------------
