/* RTX H.248 Media Gate. H.264 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mmt_video_image.h"
//-----------------------------------------------
//
//-----------------------------------------------
#define H264_RTP_PAYLOAD_SIZE	1300
#define H264_START_CODE_PREFIX_SIZE	4

extern const uint8_t H264_START_CODE_PREFIX[4];
//-----------------------------------------------
//
//-----------------------------------------------
enum profile_idc_t
{
	profile_idc_none = 0,

	profile_idc_baseline = 66,
	profile_idc_extended = 88,
	profile_idc_main = 77,
	profile_idc_high = 100
};

struct profile_iop_t
{
	uint32_t constraint_set0_flag : 1;
	uint32_t constraint_set1_flag : 1;
	uint32_t constraint_set2_flag : 1;
	uint32_t reserved_zero_5bits : 5;
};

enum level_idc_t
{
	level_idc_none = 0,

	level_idc_1_0 = 10,
	level_idc_1_b = 14,
	level_idc_1_1 = 11,
	level_idc_1_2 = 12,
	level_idc_1_3 = 13,
	level_idc_2_0 = 20,
	level_idc_2_1 = 21,
	level_idc_2_2 = 22,
	level_idc_3_0 = 30,
	level_idc_3_1 = 31,
	level_idc_3_2 = 32,
	level_idc_4_0 = 40,
	level_idc_4_1 = 41,
	level_idc_4_2 = 42,
	level_idc_5_0 = 50,
	level_idc_5_1 = 51,
	level_idc_5_2 = 52,
};


/* 5.2. Common Structure of the RTP Payload Format
Type   Packet    Type name                        Section
---------------------------------------------------------
0      undefined                                    -
1-23   NAL unit  Single NAL unit packet per H.264   5.6
24     STAP-A    Single-time aggregation packet     5.7.1
25     STAP-B    Single-time aggregation packet     5.7.1
26     MTAP16    Multi-time aggregation packet      5.7.2
27     MTAP24    Multi-time aggregation packet      5.7.2
28     FU-A      Fragmentation unit                 5.8
29     FU-B      Fragmentation unit                 5.8
30-31  undefined                                    -
*/
enum nal_unit_type_t
{
	undefined_0 = 0,
	nal_unit,
	stap_a = 24,
	stap_b = 25,
	mtap16 = 26,
	mtap24 = 27,
	fu_a = 28,
	fu_b = 29,
	undefined_30 = 30,
	undefined_31 = 31
};
//-----------------------------------------------
//
//-----------------------------------------------
int h264_parse_profile(const char* profile_level_id, profile_idc_t *p_idc, profile_iop_t *p_iop, level_idc_t *l_idc);
int h264_get_pay(const void* in_data, size_t in_size, const void** out_data, size_t *out_size, bool* append_scp, bool* end_of_unit);
//-----------------------------------------------
