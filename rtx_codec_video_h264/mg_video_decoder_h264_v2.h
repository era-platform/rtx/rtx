/* RTX H.248 Media Gate. H.264 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavutil/mem.h>
}

#include "h264_common.h"

//#define VIDEO_TEST
//#define VIDEO_RTP_TEST

extern int g_rtx_decoder_counter;

class h264_parser;
class h264_rtp_depacketizer;
//-----------------------------------------------
//
//-----------------------------------------------
class mg_video_decoder_h264_v2_t : public mg_video_decoder_t//, public h264_common_t
{
public:
	mg_video_decoder_h264_v2_t(rtl::Logger& log);
	virtual ~mg_video_decoder_h264_v2_t();

	bool initialize(const media::PayloadFormat* fmt);
	void destroy();

	virtual const char*	getEncoding();
	virtual media::VideoImage* decode(const rtp_packet* rtp_pack);

private:
	bool init();
	void dispose();
	void setFmtp(const char* fmtp);
	bool decode_frame(const uint8_t* data, unsigned len, bool& picture_decoded, media::VideoImage*& image);

	bool init_av_codec_context();
	void free_av_codec_context();

	bool init_av_codec();
	void free_av_codec();

	bool init_av_frame();
	void free_av_frame();

	bool init_av_packet();
	void free_av_packet();

	uint32_t get_id() const { return m_id; }

private:

	uint32_t m_id;

	AVCodec* m_av_codec;
	AVCodecContext* m_av_codec_context;
	AVFrame* m_av_frame;
	AVPacket* m_av_packet;

	h264_parser* m_parser;
	h264_rtp_depacketizer* m_depacketizer;

	rtl::Logger& m_log;

#if defined(VIDEO_TEST)
	FILE* m_outfile;
#endif
#if defined(VIDEO_RTP_TEST)
	FILE* m_rtpfile;
#endif
};

//---------------------------------------------
//
//---------------------------------------------
class h264_parser
{
public:
	h264_parser(rtl::Logger& log, uint32_t id);
	~h264_parser();

	bool init();
	void dispose();
	unsigned parse(const uint8_t* data, unsigned len);
	const uint8_t* parsed_frame() { return m_parsed_frame; }
	unsigned parsed_frame_size() { return m_parsed_size; }

private:
	bool init_av_codec();
	void free_av_codec();

	bool init_av_codec_context();
	void free_av_codec_context();

	bool init_parser_context();
	void free_parser_context();

private:
	uint32_t m_id;
	AVCodec* m_av_codec;
	AVCodecContext* m_av_codec_context;
	AVCodecParserContext* m_av_parser_context;

	const uint8_t* m_parsed_frame;
	unsigned m_parsed_size;

	rtl::Logger& m_log;
};
//------------------------------------------
//
//------------------------------------------
class h264_rtp_depacketizer
{
public:
	h264_rtp_depacketizer(rtl::Logger& log, uint32_t id);
	~h264_rtp_depacketizer();

	bool init();
	void dispose();

	bool depacketize(const rtp_packet* packet); // const uint8_t* data, unsigned length, bool marker, unsigned timestamp
	const t_video_nal_array& current_nals() { return m_nal_list; }

private:
	void clear_nal_pool();
	void add_single(const uint8_t* data, unsigned len);
	void add_fua(const uint8_t* data, unsigned len);
	void add_stapa(const uint8_t* data, unsigned len);
	void check_buffer_space(unsigned need_space);
	unsigned stapa_compute_buffer_size(const uint8_t* data, unsigned len, unsigned& nal_count);
	void reset();

private:
	t_video_nal_array m_nal_list;
	uint8_t* m_nal_buffer;
	unsigned m_nal_buf_size;
	unsigned m_nal_buf_pos;
	bool m_current_is_fua;

	bool m_new_session;		// for initial sequence number checking
	bool m_ready_to_next;	// ts cjecking after receiving marker 
	uint32_t m_ts;			// last ts
	uint16_t m_seq;			// last seq number
	uint32_t m_id;
	rtl::Logger& m_log;
};
//-----------------------------------------------
