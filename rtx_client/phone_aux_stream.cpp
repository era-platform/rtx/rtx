﻿/* RTX H.248 Media Gate. SIP Softphone
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "phone_aux_stream.h"
//--------------------------------------
// конструктор
//--------------------------------------
phone_aux_stream_t::phone_aux_stream_t() : m_aux_started(false),
	m_out(nullptr), m_out_header_count(3), m_out_header_time(20), m_out_jitter_depth(6), m_out_jitter(Log),
	m_in(nullptr), m_in_header_count(2), 
	m_rec_out(nullptr), m_rec_in(nullptr), m_rec_out_enable(false), m_rec_in_enable(false)
{
}
//--------------------------------------
// деструктор
//--------------------------------------
phone_aux_stream_t::~phone_aux_stream_t()
{
	destroy();
}
//--------------------------------------
// создание и пуск устройства 
//--------------------------------------
bool phone_aux_stream_t::create(int clockrate, int channels, int headerCount, int headerTime, int jitterDepth)
{
	rtl::MutexLock lock(m_sync);

	m_clockrate = clockrate;
	m_channels = channels;

	LOG_CALL("aux", "setup hc:%d jd:%d...", headerCount, jitterDepth);

	m_out_header_time = headerTime;
	m_out_header_count = m_in_header_count = headerCount;
	m_out_jitter_depth = jitterDepth;
	m_out_queue.create((m_out_jitter_depth + 2) * m_out_header_time * 16);
	m_out_jitter.setPacketDepth(m_out_jitter_depth);

	if (m_in == nullptr)
	{
		m_in = aux_device_t::create_input(this);
	}

	if (m_out == nullptr)
	{
		m_out = aux_device_t::create_output(this);
	}

	LOG_CALL("aux", "setup done");

	return true;
}
//--------------------------------------
// разрушение объекта
//--------------------------------------
void phone_aux_stream_t::destroy()
{
	rtl::MutexLock lock(m_sync);

	if (m_in == nullptr && m_out == nullptr)
		return;

	LOG_CALL("aux", "destroy...");

	stop_device();

	if (m_in != nullptr)
	{
		aux_device_t::free_input(m_in);
		m_in = nullptr;
	}

	if (m_out != nullptr)
	{
		aux_device_t::free_output(m_out);
		m_out = nullptr;
	}

	LOG_CALL("aux", "destroyed");
}
//--------------------------------------
// выбор устройств
//--------------------------------------
bool phone_aux_stream_t::set_devices(const char* deviceOut, const char* deviceIn)
{
	LOG_CALL("aux", "select devices out:%s in:%s...", deviceOut, deviceIn);

	bool replaceIn = !((m_in_device[0] == 0 && (deviceIn == nullptr || deviceIn[0] == 0)) || std_stricmp(deviceIn, m_in_device) == 0);
	bool replaceOut = !((m_out_device[0] == 0 && (deviceOut == nullptr || deviceOut[0] == 0)) || std_stricmp(deviceOut, m_out_device) == 0);

	if (!replaceIn && !replaceOut)
		return true;

	bool restart = false;

	// media_stream::m_stream_started
	if (m_aux_started)
	{
		restart = true;
		stop_device();
	}

	{
		rtl::MutexLock lock(m_sync);

		// удалим старые устройства

		if (m_in != nullptr)
		{
			aux_device_t::free_input(m_in);
		}

		if (m_out != nullptr)
		{
			aux_device_t::free_output(m_out);
		}

		// создадим новые
		if (deviceOut == nullptr)
		{
			m_out_device.setEmpty();
		}
		else
		{
			m_out_device = deviceOut;
		}

		if (deviceIn == nullptr)
		{
			m_in_device.setEmpty();
		}
		else
		{
			m_in_device = deviceIn;
		}

		m_in = aux_device_t::create_input(this);
		m_out = aux_device_t::create_output(this);
	}

	if (restart)
		start_device();

	LOG_CALL("aux", "devices changed for out:%s, in:%s", STR_BOOL(replaceOut), STR_BOOL(replaceIn));

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool phone_aux_stream_t::set_output(const char* deviceOut)
{
	bool replaceOut = !((m_out_device[0] == 0 && (deviceOut == nullptr || deviceOut[0] == 0)) || std_stricmp(deviceOut, m_out_device) == 0);

	if (replaceOut)
	{
		LOG_CALL("aux", "change out device to %s (started:'%s')", deviceOut, STR_BOOL(m_aux_started));

		bool restart = false;

		if (m_aux_started)
		{
			restart = true;
			stop_device();
		}

		{
			rtl::MutexLock lock(m_sync);

			// удалим старые устройства
			aux_device_t::free_output(m_out);
			// создадим новые
			m_out_device = deviceOut;
			m_out = aux_device_t::create_output(this);
		}

		if (restart)
			start_device();
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool phone_aux_stream_t::set_input(const char* deviceIn)
{
	bool replaceIn = !((m_in_device[0] == 0 && (deviceIn == nullptr || deviceIn[0] == 0)) || std_stricmp(deviceIn, m_in_device) == 0);

	if (replaceIn)
	{
		LOG_CALL("aux", "Change in device to %s (started:'%s')", deviceIn, STR_BOOL(m_aux_started));

		bool restart = false;
		// media_stream::m_stream_started
		if (m_aux_started)
		{
			restart = true;
			stop_device();
		}

		{
			rtl::MutexLock lock(m_sync);

			// удалим старые устройства
			aux_device_t::free_input(m_in);

			// создадим новые
			m_in_device = deviceIn;
			m_in = aux_device_t::create_input(this);
		}

		if (restart)
			start_device();
	}

	return true;
}
void phone_aux_stream_t::set_input_data_handler(aux_device_in_handler_t* handler)
{
	LOG_CALL("aux", "Setting handler...");
	m_in_handler = handler;
}
//--------------------------------------
//
//--------------------------------------
const char* phone_aux_stream_t::get_output()
{
	if (m_out_device.isEmpty())
	{
		if (MM_AudioGetOutputCount() > 0)
		{
			char buffer[128];
			MM_AudioGetOutputName(0, buffer, 128);
			m_out_device = buffer;
		}
	}

	return m_out_device;
}
//--------------------------------------
//
//--------------------------------------
const char* phone_aux_stream_t::get_input()
{
	if (m_in_device.isEmpty())
	{
		if (MM_AudioGetInputCount() > 0)
		{
			char buffer[128];
			MM_AudioGetInputName(0, buffer, 128);
			m_in_device = buffer;
		}
	}

	return m_out_device;
}
//--------------------------------------
// присвоить путь к папке для записи аудио файлов
//--------------------------------------
void phone_aux_stream_t::record_set_path(const char* recFolder)
{
	m_rec_folder = recFolder;
}
//--------------------------------------
// включить/выключить запись в файл исходящего потока
//--------------------------------------
void phone_aux_stream_t::record_input(bool enable)
{
	m_rec_in_enable = enable;
}
//--------------------------------------
// включить/выключить запись в файл входящего потока
//--------------------------------------
void phone_aux_stream_t::record_output(bool enable)
{
	m_rec_out_enable = enable;
}
//--------------------------------------
//
//--------------------------------------
void phone_aux_stream_t::add_output_data(const short* samples, int length, uint16_t seq_no)
{
	m_out_queue.write((const uint8_t*)samples, length * 2);
	//LOG_CALL("aux", "pass packet to waveout. size:%d bytes queue:%d bytes", length, m_out_queue.get_available());
	record_output_write(samples, length);

	if (!m_out_started)
	{
		// проверим размер накопительного буфера и стартанем если больше чем 2 блока
		// m_out_header_count
		if (m_out_queue.getAvailable() > m_out->aux_get_block_size() * (m_out_header_count + 1))
		{
			m_out->aux_start_device();
			m_out_started = true;
		}
	}
}
//--------------------------------------
// использование aux_out_wave_t
//--------------------------------------
//void phone_aux_stream_t::send_packet_to_device(rtp_packet* packet)
//{
//	const short* samples = nullptr;
//	int count = 0;
//
//	if (m_out_jitter.get_packet_depth() == 0)
//	{
//		samples = packet->get_paysamples();
//		count = packet->get_samples();
//
//		m_out_queue.write((const uint8_t*)samples, count * 2);
//
//		MLOG_STREAM_FLOW(L"aux", L"stream %S : write packet to waveout. size:%d bytes queue:%d bytes", m_name, count * 2, m_out_queue.get_available());
//
//		record_output_write(samples, count);
//
//		if (!m_out_started)
//		{
//			// проверим размер накопительного буфера и стартанем если больше чем 2 блока
//			if (m_out_queue.get_available() > m_out->aux_get_block_size() * (m_out_header_count + 1))
//			{
//				MLOG_STREAM_FLOW(L"aux", L"stream %S : starting out device", m_name);
//				m_out->aux_start_device();
//				m_out_started = true;
//			}
//		}
//		return;
//	}
//
//	// джиттер включен!
//
//	MLOG_STREAM_FLOW(L"aux", L"stream %S : push packet to jitter", m_name);
//
//	//packet->add_ref();
//
//	rtp_jitter_state_t state = m_out_jitter.push(packet);
//
//	switch (state)
//	{
//	case rtp_jitter_pass_e:
//		samples = packet->get_samples();
//		count = packet->get_samples_count();
//		m_out_queue.write((const uint8_t*)samples, count * 2);
//		MLOG_STREAM_FLOW(L"aux", L"stream %S : pass packet to waveout. size:%d bytes queue:%d bytes", m_name, count * 2, m_out_queue.get_available());
//		record_output_write(samples, count);
//
//		break;
//	case rtp_jitter_drop_e:
//		m_tx_stat.lost++;
//		record_output_write(nullptr, 0);
//		MLOG_STREAM_FLOW(L"aux", L"stream %S : drop packet", m_name);
//		//packet->Release();
//		break;
//	case rtp_jitter_captured_e:
//		// пакет в джиттере
//		packet->add_ref();
//		MLOG_STREAM_FLOW(L"aux", L"stream %S : packet stored in jitter", m_name);
//		break;
//	case rtp_jitter_ready_e:
//		packet->add_ref();
//		packet = m_out_jitter.get_first();
//
//		while (packet != nullptr)
//		{
//			samples = packet->get_samples();
//			count = packet->get_samples_count();
//			m_out_queue.write((const uint8_t*)samples, count * 2);
//
//			MLOG_STREAM_FLOW(L"aux", L"stream %S : write packet to waveout from jitter. size:%d bytes queue:%d bytes", m_name, count * 2, m_out_queue.get_available());
//
//			record_output_write(samples, count);
//
//			packet->release();
//
//			packet = m_out_jitter.get_next();
//		}
//
//		break;
//	}
//
//	if (!m_out_started)
//	{
//		// проверим размер накопительного буфера и стартанем если больше чем 2 блока
//		// m_out_header_count
//		if (m_out_queue.get_available() > m_out->aux_get_block_size() * (m_out_header_count + 1))
//		{
//			m_out->aux_start_device();
//			m_out_started = true;
//		}
//	}
//}
//--------------------------------------
// старт ввода вывода
//--------------------------------------
bool phone_aux_stream_t::start_device()
{
	bool res_in = false, res_out = false;

	if (m_in == nullptr || m_out == nullptr)
	{
		LOG_WARN("aux", "start device failed : in(%p) out(%p)", m_in, m_out);
		return false;
	}

	LOG_CALL("aux", "starting...");

	m_out->aux_setup_device(m_clockrate, m_channels, m_out_header_time, m_out_header_count);
	m_out_queue.create((m_out_jitter_depth + 2) * m_out_header_time * 16);
	m_out_jitter.setPacketDepth(m_out_jitter_depth);
	res_out = m_out->aux_open_device(m_out_device) && m_out->aux_start_device();

	m_in->aux_setup_device(m_clockrate, m_channels, 20, m_in_header_count);
	res_in = m_in->aux_open_device(m_in_device) && m_in->aux_start_device();

	// media_stream_aux::m_stream_started
	m_aux_started = true;

	LOG_CALL("aux", "started:'%s'...", STR_BOOL(m_aux_started));

	if (res_out && res_in)
	{
		record_output_start();
		record_input_start();

		LOG_CALL("aux", "started");
	}
	else
	{
		LOG_WARN("aux", "start failed : in(%s) out(%s)", STR_BOOL(res_in), STR_BOOL(res_out));
	}

	return res_in && res_out;
}
//--------------------------------------
// стоп ввода вывода
//--------------------------------------
void phone_aux_stream_t::stop_device()
{
	if (m_in != nullptr && m_out != nullptr)
	{
		LOG_CALL("aux", "stopping...");

		record_output_stop();
		record_input_stop();

		m_in->aux_close_device();
		m_out->aux_close_device();

		//Если не закрываем устройства, то выставляем флаг, 
		//чтобы обмен звуком с устройством пока не осуществлялся
		//и ждем выхода потоков из колбэков AudioDevice_XXXX()
		// media_stream_aux::m_stream_started ?
		
		m_aux_started = false;

		int att = 200;
		while ((m_out_thread_enter || m_in_thread_enter) && (att-- > 0))
			rtl::Thread::sleep(5);

		m_out_started = false;
		m_out_jitter.reset();

		LOG_CALL("aux", "stopped (stopped:'%s')", STR_BOOL(m_aux_started));
	}
}
//--------------------------------------
//
//--------------------------------------
void phone_aux_stream_t::mute(bool mute)
{
	LOG_CALL("aux", "set in device to %s state!", mute ? "Mute" : "Off");

	m_in->aux_set_paused(mute != false);
}
//--------------------------------------
//	Звук пришел с WAVEIN
//--------------------------------------
void phone_aux_stream_t::aux_in_data_ready(const short* mediaData, int size)
{
	if (!m_aux_started || m_in == nullptr)
	{
		LOG_WARN("aux", "aux_in_data_ready() : device not started!" );
		return;
	}

	m_in_thread_enter = true;

	if (m_in_handler != nullptr)
	{
		m_in_handler->aux_in_data_ready(mediaData, size);
	}
	else
	{
		LOG_WARN("aux-in", "handler is nullptr!");
	}

	record_input_write(mediaData, size);

	m_in_thread_enter = false;
}
//--------------------------------------
//	WAVEOUT пришел забрать звук
//--------------------------------------
int phone_aux_stream_t::aux_out_get_data(short* mediaData, int size)
{
	if (!m_aux_started)
	{
		LOG_WARN("aux", "aux_out_get_data() : device not started!" );
		return 0;
	}
	
	m_out_thread_enter = true;
	
	int length = m_out_queue.read((uint8_t*)mediaData, size * 2) / 2;

	m_out_thread_enter = false;

	/*if (length == 0)
	{
		LOG_WARN("aux", "aux_out_get_data() : audio buffer empty!" );
	}*/

	return length;
}
//--------------------------------------
//
//--------------------------------------
void phone_aux_stream_t::record_output_start()
{
	if (m_rec_out_enable)
	{
		char fileName[MAX_PATH];
		strcpy(fileName, m_rec_folder);
		int len = (int)strlen(fileName);
		if (fileName[len - 1] != L'\\')
		{
			fileName[len++] = L'\\';
			fileName[len] = 0;
		}
		rtl::DateTime st = rtl::DateTime::now();
		std_snprintf(fileName + len, MAX_PATH - len, "rec_rtp_%d%d%d_%d_%d_%d.wav", st.getYear(), st.getMonth(), st.getDay(), st.getHour(), st.getMinute(), st.setSecond());

		media::WAVFormat format = { media::WAVFormatTagPCM, 1, 8000, 16000, 2, 16, 0 };
		m_rec_out.create(fileName, &format);

		LOG_CALL("aux", "recording audio rtp stream to file started '%s'", fileName);
	}
}
//--------------------------------------
//
//--------------------------------------
void phone_aux_stream_t::record_output_stop()
{
	if (m_rec_out_enable)
	{
		m_rec_out.close();
		LOG_CALL("aux", "Audio rtp stream recording stopped");
	}
}
//--------------------------------------
//
//--------------------------------------
void phone_aux_stream_t::record_output_write(const short* samples, int count)
{
	if (m_rec_out_enable)
	{
		m_rec_out.write(samples, count * sizeof(short));
	}
}
//--------------------------------------
//
//--------------------------------------
void phone_aux_stream_t::record_input_start()
{
	if (m_rec_in_enable)
	{
		char fileName[MAX_PATH];
		strcpy(fileName, m_rec_folder);
		int len = (int)strlen(fileName);
		if (fileName[len - 1] != L'\\')
		{
			fileName[len++] = L'\\';
			fileName[len] = 0;
		}
		rtl::DateTime st = rtl::DateTime::now();

		std_snprintf(fileName + len, MAX_PATH - len, "rec_aux_%d%d%d_%d_%d_%d.wav", st.getYear(), st.getMonth(), st.getDay(),
			st.getHour(), st.getMinute(), st.setSecond());

		media::WAVFormat format = { media::WAVFormatTagPCM, 1, 8000, 16000, 2, 16, 0 };
		m_rec_in.create(fileName, &format);

		LOG_CALL("aux", "recording audio microphone stream to file started '%s'", fileName);
	}
}
//--------------------------------------
//
//--------------------------------------
void phone_aux_stream_t::record_input_stop()
{
	if (m_rec_in_enable)
	{
		m_rec_in.close();
		LOG_CALL("aux", "Audio microphone stream recording stopped");
	}
}
//--------------------------------------
//
//--------------------------------------
void phone_aux_stream_t::record_input_write(const short* samples, int count)
{
	if (m_rec_in_enable)
	{
		m_rec_in.write(samples, count * sizeof(short));
	}
}
//--------------------------------------
