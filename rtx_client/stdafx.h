/* RTX H.248 Media Gate. SIP Softphone
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//--------------------------------------
// configuration files
//--------------------------------------
#include "std_basedef.h"
#include "std_typedef.h"
#include "std_funcdef.h"
//--------------------------------------
// ��� ������������� � VS2005, VS2008, VS2010
//--------------------------------------
#if defined (TARGET_OS_WINDOWS)
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#define _CRT_NON_CONFORMING_SWPRINTFS
//#define _USE_32BIT_TIME_T
#define __STDC_LIMIT_MACROS
#define _CRT_RAND_S

#pragma setlocale("ru-RU")
#endif

#include "std_mem_checker.h"
//--------------------------------------
//
//--------------------------------------
#if defined (DEBUG_MEMORY)
#pragma warning(disable : 4291)

void* operator new(size_t size, char* file, int line);
void* operator new[](size_t size, char* file, int line);
void operator delete (void* mem_ptr);
void operator delete[](void* mem_ptr);

#define NEW new (__FILE__, __LINE__)
#define DELETEO(ptr) delete (ptr)
#define DELETEAR(ptr) delete[] (ptr)
#define MALLOC(size) std_mem_checker_malloc(size, __FILE__, __LINE__, rc_malloc_malloc_e)
#define MALLOCZ(size) std_mallocz(size)
#define FREE(ptr) std_mem_checker_free(ptr, rc_malloc_free_e)
#define FREEP(ptr) std_freep(ptr)
#define REALLOC(ptr, size) std_mem_checker_realloc(ptr, size, __FILE__, __LINE__)
#else
#define NEW new
#define DELETEO(ptr) delete ptr
#define DELETEAR(ptr) delete[] ptr
#define MALLOC(size) malloc(size)
#define MALLOCZ(size) std_mallocz(size)
#define FREE(ptr) free(ptr)
#define FREEP(ptr) std_freep(ptr)
#define REALLOC(ptr, size) realloc(ptr, size)
#endif
//--------------------------------------
// Standard stdlib header files:
//--------------------------------------
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <assert.h>
#include <inttypes.h>
#include <limits.h>
#include <string.h>
#include <time.h>
#include <locale.h>
#include <stdarg.h>
#include <errno.h>
#include <math.h>

#if defined (TARGET_OS_WINDOWS)
//--------------------------------------
// Windows specefic stdlib header files:
//--------------------------------------
#include <process.h>
//--------------------------------------
// Exclude rarely-used stuff from Windows headers
//--------------------------------------
#define WIN32_LEAN_AND_MEAN
#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0600
#endif
#ifndef _WIN32_IE
#define _WIN32_IE 0x0600
#endif
//--------------------------------------
// Windows Header Files:
//--------------------------------------
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <ws2ipdef.h>
#include <iphlpapi.h>
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/time.h>
#include <dlfcn.h>
#include <sys/ioctl.h>
#if defined(TARGET_OS_LINUX)
#include <sys/epoll.h>
#elif defined(TARGET_OS_FREEBSD)
#include <sys/event.h>
#endif
#endif
//--------------------------------------
// used modules includes
//--------------------------------------
#include "rtx_stdlib.h"
#include "rtx_netlib.h"
#include "rtx_client.h"
#include "rtx_aux.h"
#include "rtx_media_tools.h"
#include "mmt_payload_set.h"
#include "rtx_megaco.h"
#include "rtx_media_engine.h"
#include "mg_interface.h"

using namespace media;
//--------------------------------------
// project includes
//--------------------------------------
#include "sip_user_agent.h"
#include "sip_stack.h"
#include "sip_client.h"
//--------------------------------------
//
//--------------------------------------
extern char g_startup_path[];
extern char g_log_root_path[];
extern char g_rec_path[];
extern bool g_in_device;
extern uint32_t g_wave_header_time;
extern uint32_t g_wave_header_count;
extern uint32_t g_jitter_depth;
extern sip_client_t* g_sip_client;
//--------------------------------------
//
//--------------------------------------
bool read_reg_profile_string(const char* key, char* buffer, int size);
bool write_reg_profile_string(const char* key, const char* value);
int	read_reg_profile_int(const char* key, int defaultValue);
bool write_reg_profile_int(const char* key, int value);
//--------------------------------------
//
//--------------------------------------
bool read_config_string(const char* key, char* buffer, int size);
bool read_config_integer(const char* key, uint32_t& value);
bool read_config_boolean(const char* key, bool& value);

const char* get_sip_call_disconnect_reason_text(enum DisconnectReason reason);
//--------------------------------------
