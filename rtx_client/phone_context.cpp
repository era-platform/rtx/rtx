/* RTX H.248 Media Gate. SIP Softphone
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "phone_media_engine.h"
#include "phone_context.h"
//--------------------------------------
//
//--------------------------------------
uint32_t g_context_id_gen = 0;

#define LOGTAG "ctx"

static uint32_t generate_context_id()
{
	uint32_t id = MG_CONTEXT_NULL;

	while (id == MG_CONTEXT_NULL || id == MG_CONTEXT_CHOOSE || id == MG_CONTEXT_ALL)
	{
		id = std_interlocked_inc(&g_context_id_gen);
	}

	return id;
}
//-----------------------------------------------
//
//-----------------------------------------------
bool phone_context_t::initialize()
{
	return true;
}
//-----------------------------------------------
//
//-----------------------------------------------
void phone_context_t::release()
{
}
//-----------------------------------------------
//
//-----------------------------------------------
phone_context_t::phone_context_t() :
	m_ctx(nullptr), m_recording(false)
{

}
//-----------------------------------------------
//
//-----------------------------------------------
phone_context_t::~phone_context_t()
{
}
//--------------------------------------
//
//--------------------------------------
bool phone_context_t::start()
{
	LOG_CALL(LOGTAG, "start phone context...");

	rtl::MutexLock lock(m_sync);

	m_ctx = rtx_phone::create_context();
	m_ctx->mg_context_initialize(this, nullptr);

	if (!m_ctx->mg_context_lock())
	{
		LOG_WARN(LOGTAG, "start phone context : context lock failed!");
		rtx_phone::destroy_context(m_ctx);
		m_ctx = nullptr;
		return false;
	}

	bool result = start_dev_term();

	m_ctx->mg_context_unlock();

	if (!result)
	{
		rtx_phone::destroy_context(m_ctx);
		m_ctx = nullptr;
		return false;
	}

	LOG_CALL(LOGTAG, "start phone context : done");

	return true;
}
//--------------------------------------
//
//--------------------------------------
void phone_context_t::stop()
{
	LOG_CALL(LOGTAG, "stop phone context...");

	rtl::MutexLock lock(m_sync);

	if (m_ctx != nullptr)
	{
		LOG_CALL(LOGTAG, "stop phone context : unbinding all streams and destroy context...");

		if (!m_ctx->mg_context_lock())
		{
			LOG_WARN(LOGTAG, "stop phone context : context lock failed!");
			return;
		}

		// 1.1 stop and unbind all rtp-streams
		for (int i = 0; i < m_rtp_term_list.getCount(); i++)
		{
			free_rtp_term(m_rtp_term_list[i]);
		}
		
		m_rtp_term_list.clear();

		// 1.2 stop and destroy aux-stream
		stop_dev_term();

		// 2. destroy conf session
		rtx_phone::destroy_context(m_ctx);
		
		m_ctx = nullptr;
		m_recording = false;
	}

	LOG_CALL(LOGTAG, "stop phone context : done");
}
//-----------------------------------------------
//
//-----------------------------------------------
phone_rtp_term_t* phone_context_t::make_rtp_term(in_addr iface)
{
	LOG_CALL(LOGTAG, "make rtp term '%s'...", inet_ntoa(iface));

	rtl::MutexLock lock(m_sync);

	if (m_ctx == nullptr && !start())
		return nullptr;

	if (!m_ctx->mg_context_lock())
	{
		LOG_WARN(LOGTAG, "make rtp term : context lock failed!");
		return nullptr;
	}

	// ������� ��������� �������������
	phone_rtp_term_t* term = NEW phone_rtp_term_t(m_ctx);

	if (term->setup(iface))
	{
		m_rtp_term_list.add(term);

		LOG_CALL(LOGTAG, "make rtp term : term id(%u) made", term->get_id());
	}
	else
	{
		LOG_CALL(LOGTAG, "make rtp term : mg engine termination return error");
		term->destroy();
		DELETEO(term);
	}

	m_ctx->mg_context_unlock();

	LOG_CALL(LOGTAG, "make rtp term : %s", term != nullptr ? "done" : "failed");

	return term;
}
//--------------------------------------
//
//--------------------------------------
void phone_context_t::free_rtp_term(phone_rtp_term_t* term)
{
	LOG_CALL(LOGTAG, "free rtp term (%u)...", term->get_id());

	rtl::MutexLock lock(m_sync);

	int index;

	if (m_ctx->mg_context_lock() && rtp_term_contained(term, index))
	{
		LOG_CALL(LOGTAG, "free rtp term : unbind from context...");

		m_rtp_term_list.removeAt(index);
		term->destroy();
		DELETEO(term);
	}

	if (m_rtp_term_list.getCount() == 0 && !m_recording)
	{
		stop();
	}

	LOG_CALL(LOGTAG, "free rtp term : done");
}
//--------------------------------------
//
//--------------------------------------
void phone_context_t::mute_rtp_term(phone_rtp_term_t* term, bool mute)
{
	LOG_CALL(LOGTAG, "mute rtp term (%u) mute state to %s", term->get_id(), STR_BOOL(mute));

	rtl::MutexLock lock(m_sync);

	int index;

	if (rtp_term_contained(term, index))
	{
		rtl::ArrayT<h248::TopologyTriple> topology;

		// setup
		h248::TopologyTriple triple;
		triple.term_from = term->get_id();
		triple.term_to = MG_TERM_ALL;
		triple.direction = mute ? h248::TopologyDirection::Oneway : h248::TopologyDirection::Bothway;

		topology.add(triple);

		m_ctx->mg_context_set_topology(topology, nullptr);

		LOG_CALL(LOGTAG, "mute rtp term : done");
	}
	else
	{
		LOG_WARN(LOGTAG, "mute rtp term : unknown term ptr!");
	}
}
//--------------------------------------
//
//--------------------------------------
void phone_context_t::start_recording(const char* file_path)
{
	LOG_CALL(LOGTAG, "start recording to '%s'...not implemented!", file_path);

	/*rtl::MutexLock lock(m_sync);

	if (m_ctx == nullptr)
	{
		if (!(m_recording = start()))
		{
			return;
		}
	}

	if (!m_ctx->start_recording(file_path))
	{
		LOG_WARN(LOGTAG, "conf_session_start_rec() failed.");
	}
	else
	{
		LOG_CALL(LOGTAG, "recording started");
	}*/
}
//--------------------------------------
//
//--------------------------------------
void phone_context_t::stop_recording()
{
	LOG_CALL(LOGTAG, "stop recording...not implemented");

	//rtl::MutexLock lock(m_sync);

	//if (m_ctx != nullptr)
	//{
	//	m_recording = false;
	//	m_ctx->stop_recording();

	//	if (m_rtp_term_list.getCount() == 0)
	//	{
	//		stop();
	//	}
	//}

	//LOG_CALL(LOGTAG, "recording stopped");
}
//-----------------------------------------------
//
//-----------------------------------------------
bool phone_context_t::start_dev_term()
{

	LOG_CALL(LOGTAG, "start device term...");

	m_dev_term.create(m_ctx);

	m_dev_term.start_device();

	LOG_CALL(LOGTAG, "start device term : done");

	return true;
}
//-----------------------------------------------
//
//-----------------------------------------------
void phone_context_t::stop_dev_term()
{
	LOG_CALL(LOGTAG, "stop device term...");

	m_dev_term.stop_device();

	m_dev_term.destroy();

	LOG_CALL(LOGTAG, "stop device term : done");
}
//-----------------------------------------------
//
//-----------------------------------------------
bool phone_context_t::rtp_term_contained(phone_rtp_term_t* term, int& index)
{
	for (int i = 0; i < m_rtp_term_list.getCount(); i++)
	{
		if (m_rtp_term_list.getAt(i) == term)
		{
			index = i;
			return true;
		}
	}

	return false;
}
//--------------------------------------
//
//--------------------------------------
void phone_context_t::TerminationEvent_callback(h248::EventParams* params)
{
}
//--------------------------------------
