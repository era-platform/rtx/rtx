﻿/* RTX H.248 Media Gate. SIP Softphone
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sdp.h"
#include "sdp_session.h"
#include "std_crypto.h"
//--------------------------------------
//
//--------------------------------------
/*time_t timecvt(const wchar_t* time_string)
{
	wchar_t* term;

	time_t res = (time_t)wcstoul(time_string, &term, 0);

	// проверим приставки
	if (*term == L'd')
		return res *= 86400;
	// указанны часы
	if (*term == L'h')
		return res *= 3600;
	// указанны минуты
	if (*term == L'm')
		return res *= 60;

	return res;
}
//--------------------------------------
//
//--------------------------------------
const wchar_t* time2str(wchar_t* buffer, int size, int time_val)
{
	int len;
	
	if (time_val % 86400 == 0)
		len = _snwprintf(buffer, size, L"%dd", time_val / 86400);
	else if (time_val % 3600 == 0)
		len = _snwprintf(buffer, size, L"%dh", time_val / 3600);
	else if (time_val % 60 == 0)
		len = _snwprintf(buffer, size, L"%dm", time_val / 60);

	return buffer;
}*/
//--------------------------------------
//
//--------------------------------------
void sdp_session_t::parse_owner(const char* str)
{
	// o=<username> <sess-id> <sess-version> <nettype> <addrtype> <unicast-address>
	
	const char* mstr = str, *cp;
	char nettype[8];
	int len;

	// <username>
	if ((cp = strchr(mstr, ' ')) == nullptr)
		return;

	len = MIN(PTR_DIFF(cp, mstr), sizeof(m_o_user_name)-1);
	strncpy(m_o_user_name, mstr, len);
	m_o_user_name[len] = 0;
	mstr = cp+1;

	// <sess-id>
	if ((cp = strchr(mstr, ' ')) == nullptr)
		return;
	m_o_session_id = strtoull(mstr, nullptr, 10);
	mstr = cp+1;

	// <sess-version>
	if ((cp = strchr(mstr, ' ')) == nullptr)
		return;
	m_o_version = strtoull(mstr, nullptr, 10);
	mstr = cp+1;

	// <nettype>
	if ((cp = strchr(mstr, ' ')) == nullptr)
		return;

	len = MIN(PTR_DIFF(cp, mstr), sizeof(nettype)-1);
	strncpy(nettype, mstr, len);
	nettype[len] = 0;
	mstr = cp+1;

	if (std_stricmp(nettype, "IN") != 0)
		return;

	// <addrtype>
	if ((cp = strchr(mstr, ' ')) == nullptr)
		return;

	len = MIN(PTR_DIFF(cp, mstr), sizeof(nettype)-1);
	strncpy(nettype, mstr, len);
	nettype[len] = 0;
	mstr = cp+1;

	if (std_stricmp(nettype, "IP4") != 0)
		return;

	// <unicast-address>
	if ((cp = strchr(mstr, ' ')) == nullptr)
	{
		len = MIN((int)strlen(mstr), sizeof(m_o_address)-1); // TEST ERROR
	}
	else
		len = MIN(PTR_DIFF(cp, mstr), sizeof(m_o_address)-1);

	strncpy(m_o_address, mstr, len);
	m_o_address[len] = 0;
	mstr = cp+1;
}
//--------------------------------------
//
//--------------------------------------
void sdp_session_t::parse_connect_address(const char* str)
{
	// c=<nettype> <addrtype> <connection-address>
	
	const char* mstr = str, *cp;
	char net[32];
	int len;

	if ((cp = strchr(mstr, ' ')) == nullptr)
		return;

	len = MIN(PTR_DIFF(cp, mstr), sizeof(net)-1);
	strncpy(net, mstr, len);
	net[len] = 0;
	mstr = cp+1;
	if (std_stricmp(net, "IN") != 0)
		return;

	if ((cp = strchr(mstr, ' ')) == nullptr)
		return;

	len = MIN(PTR_DIFF(cp, mstr), sizeof(net)-1);
	strncpy(net, mstr, len);
	net[len] = 0;
	mstr = cp+1;
	if (std_stricmp(net, "IP4") != 0)
		return;

	cp = strchr(mstr, '/');

	if (cp == nullptr)
		len = (int)MIN(strlen(mstr), sizeof(net)-1);
	else
		len = MIN(PTR_DIFF(cp, mstr), sizeof(net)-1);

	strncpy(net, mstr, len);
	net[len] = 0;
	m_c_address.s_addr = inet_addr(net);
}
//--------------------------------------
//
//--------------------------------------
sdp_session_t::sdp_session_t(rtl::Logger& log) : m_log(log)
{
	m_v = 0;
	memset(m_s_name, 0, sizeof m_s_name);

	strcpy(m_s_name, SDP_DEFAULT_SESSION_NAME);

	memset(m_o_user_name, 0, sizeof m_o_user_name);
	m_o_user_name[0] = '-';
	m_o_session_id = 0;
	m_o_version = 0;
	memset(m_o_address, 0, sizeof m_o_address);
	m_c_address.s_addr = INADDR_ANY;
	memset(m_b_modifier, 0, sizeof m_b_modifier);
	m_b_value = 0;
	m_direction = sdp_direction_sendrecv_e;
	m_dynamic_payload_base = (int)media::PayloadId::Dynamic;
}
//--------------------------------------
//
//--------------------------------------
sdp_session_t::sdp_session_t(rtl::Logger& log, const char* sdp) : m_log(log)
{
	m_v = 0;
	memset(m_s_name, 0, sizeof m_s_name);
	memset(m_o_user_name, 0, sizeof m_o_user_name);
	m_o_session_id = 0;
	m_o_version = 0;
	memset(m_o_address, 0, sizeof m_o_address);
	m_c_address.s_addr = INADDR_ANY;
	memset(m_b_modifier, 0, sizeof m_b_modifier);
	m_b_value = 0;
	m_direction = sdp_direction_sendrecv_e;
	m_dynamic_payload_base = (int)media::PayloadId::Dynamic;

	decode(sdp);
}
//--------------------------------------
//
//--------------------------------------
sdp_session_t::~sdp_session_t()
{
	for (int i = 0; i < m_media_list.getCount(); i++)
	{
		DELETEO(m_media_list[i]);
	}

	m_media_list.clear();
}
//--------------------------------------
//
//--------------------------------------
bool sdp_session_t::decode(const char* sdp)
{
	const char* mstr = sdp, *cp, *value;
	char tmp[256], name;
	sdp_media_description_t* current_media = nullptr;
	bool media_present = false;
	int len;

	cleanup();

	while (mstr && *mstr != 0)
	{
		// выделяем строку
		cp = strpbrk(mstr, "\r\n");
		if (cp == nullptr)
		{
			strncpy(tmp, mstr, sizeof(tmp)-1);
			tmp[sizeof(tmp)-1] = 0;
			mstr = nullptr;
		}
		else
		{
			len = MIN(PTR_DIFF(cp, mstr), sizeof(tmp)-1);
			strncpy(tmp, mstr, len);
			tmp[len] = 0;
			while (*cp == '\r' || *cp == '\n') cp++;
			mstr = cp;
		}

		if (tmp[1] != '=')
		{
			while (*cp != 0 && (*cp == '\r' || *cp == '\n')) cp++;
			mstr = cp;
			continue;
		}
		name = tmp[0];
		value = tmp+2; // f=v

		// media name and transport address (mandatory)
		if (name == 'm')
		{
			media_present = true;
			current_media = NEW sdp_media_description_t(this);
			if (current_media->decode(value))
			{
				m_media_list.add(current_media);
			}
			else
			{
				DELETEO(current_media);
				current_media = nullptr;
			}
		}
		else if (current_media == nullptr && !media_present)
		{
			switch (name)
			{
			case 'v' : // protocol version (mandatory)
				m_v = atoi(value);
				break;
			case 'o' : // owner/creator and session identifier (mandatory)
				parse_owner(value);
				break;
			case 's' : // session name (mandatory)
				len = (int)strlen(value);
				len = MIN(len, sizeof(m_s_name)-1);
				strncpy(m_s_name, value, len);
				m_s_name[len] = 0;
				break;
			case 'c' : // connection information - not required if included in all media
				parse_connect_address(value);
				break;
			case 't' : // time the session is active (mandatory)
			case 'i' : // session information
			case 'u' : // URI of description
			case 'e' : // email address
			case 'p' : // phone number
				break;
			case 'b' : // bandwidth information
				cp = strchr(value, ':');
				if (cp != nullptr)
				{
					int len = MIN(PTR_DIFF(cp, value), 31);
					strncpy(m_b_modifier, value, len);
					m_b_modifier[len] = 0;
					m_b_value = atoi(cp+1);
				}
				break;
			case 'z' : // time zone adjustments
			case 'k' : // encryption key
			case 'r' : // zero or more repeat times
				break;
			case 'a' : // zero or more session attribute lines
				decode_attribute(value);
				break;
			}
		}
		else if (current_media != nullptr)
		{
			current_media->decode_line(name, value);
		}
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
void sdp_session_t::decode_attribute(const char* attr)
{
	if (std_stricmp(attr, "sendonly") == 0)
		set_direction(sdp_direction_sendonly_e);
	else if (std_stricmp(attr, "recvonly") == 0)
		set_direction (sdp_direction_recvonly_e);
	else if (std_stricmp(attr, "sendrecv") == 0)
		set_direction (sdp_direction_sendrecv_e);
	else if (std_stricmp(attr, "inactive") == 0)
		set_direction (sdp_direction_inactive_e);
	// else do nothing becouse we are do not want any flagged attributes
}
//--------------------------------------
//
//v=0
//o=- 2863982976 106964 IN IP4 192.168.10.10
//s=oktell-phone
//c=IN IP4 192.168.10.10
//t=0 0
//m=audio 20000 RTP/AVP 0 101
//a=rtpmap:0 PCMU/8000
//a=rtpmap:101 telephone-event/8000
//a=fmtp:101 0-15
//a=ptime:20
//--------------------------------------
rtl::String sdp_session_t::encode()
{
	rtl::String stream;
	// encode mandatory session information
	stream << "v=" << m_v << "\r\n";
	stream << "o=" << m_o_user_name << ' ' << m_o_session_id << ' ' << m_o_version << " IN IP4 " << m_o_address << "\r\n";
	stream << "s=" << m_s_name << "\r\n";

	if (net_t::ip_is_valid(m_c_address))
	{
		stream << "c=IN IP4 " << inet_ntoa(m_c_address) << "\r\n";
	}

	stream << "t=0 0\r\n";

	if (m_b_modifier[0] != 0 && m_b_value != 0)
	{
		stream << "b=" << m_b_modifier << ':' << m_b_value << "\r\n";
	}

	// encode media session information
	for (int i = 0; i < m_media_list.getCount(); i++)
	{
		m_media_list[i]->encode(stream);
	}

	return stream;
}
//--------------------------------------
//
//--------------------------------------
void sdp_session_t::cleanup()
{
	m_v = 0;
	memset(m_s_name, 0, sizeof m_s_name);
	strcpy(m_s_name, SDP_DEFAULT_SESSION_NAME);
	memset(m_o_user_name, 0, sizeof m_o_user_name);
	m_o_user_name[0] = '-';
	m_o_session_id = 0;
	m_o_version = 0;
	memset(m_o_address, 0, sizeof m_o_address);
	m_c_address.s_addr = INADDR_ANY;
	memset(m_b_modifier, 0, sizeof m_b_modifier);
	m_b_value = 0;
	m_direction = sdp_direction_sendrecv_e;

	for (int i = 0; i < m_media_list.getCount(); i++)
	{
		DELETEO(m_media_list[i]);
	}

	m_media_list.clear();

	m_dynamic_payload_base = (int)media::PayloadId::Dynamic;
}
//--------------------------------------
//
//--------------------------------------
void sdp_session_t::remove_media_description(sdp_media_description_type_t mediaType)
{
	for (int i = 0; i < m_media_list.getCount(); i++)
	{
		if (m_media_list[i]->get_media_type() == mediaType)
		{
			DELETEO(m_media_list[i]);
			m_media_list.removeAt(i);
			break;
		}
	}
}
//--------------------------------------
//
//--------------------------------------
void sdp_session_t::set_origin(const char* userName, uint64_t sessionId, uint64_t sessionVer, const char* address, const char* netType, const char* addrType)
{
	if (userName != nullptr && userName[0] != 0)
	{
		strncpy(m_o_user_name, userName, sizeof(m_o_user_name)-1);
		m_o_user_name[sizeof(m_o_user_name)-1] = 0;
	}
	else
	{
		m_o_user_name[0] = '-'; 
		m_o_user_name[1] = 0;
	}

	m_o_session_id = sessionId;
	m_o_version = sessionVer;

	if (address != nullptr && address[0] != 0)
	{
		strncpy(m_o_address, address, sizeof(m_o_address)-1);
		m_o_address[sizeof(m_o_address)-1] = 0;
	}
	else
	{
		m_o_address[0] = '0';
		m_o_address[1] = 0;
	}

	if (netType == nullptr || netType[0] == 0)
	{
		strcpy(m_o_net, "IN");
	}
	else
	{
		strncpy(m_o_net, netType, sizeof(m_o_net)-1);
		m_o_net[sizeof(m_o_net)-1] = 0;
	}

	if (addrType == nullptr || addrType[0] == 0)
	{
		strcpy(m_o_ip, "IP4");
	}
	else
	{
		strncpy(m_o_ip, addrType, sizeof(m_o_ip)-1);
		m_o_ip[sizeof(m_o_ip)-1] = 0;
	}
}
//--------------------------------------
//
//--------------------------------------
sdp_media_description_t* sdp_session_t::get_media_description(sdp_media_description_type_t rtpMediaType)
{
	for (int i = 0; i < m_media_list.getCount(); i++)
	{
		if (m_media_list[i]->get_media_type() == rtpMediaType)
			return m_media_list[i];
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
sdp_media_description_t* sdp_session_t::get_last_media_description()
{
	int count = m_media_list.getCount();
	return count > 0 ? m_media_list[count-1] : nullptr;
}
//--------------------------------------
//
//--------------------------------------
void sdp_session_t::add_media_description(sdp_media_description_t* md)
{
	m_media_list.add(md);
}
//--------------------------------------
//
//--------------------------------------
sdp_direction_type_t sdp_session_t::get_direction(sdp_media_description_type_t mediaType)
{
	for (int i = 0; i < m_media_list.getCount(); i++)
	{
		if (m_media_list[i]->get_media_type() == mediaType)
			return m_media_list[i]->get_direction();
	}

	return sdp_direction_sendrecv_e;
}
//--------------------------------------
//
//--------------------------------------
void sdp_session_t::set_connection(in_addr address, const char* netType, const char* addrType)
{
	m_c_address = address;

	if (netType == nullptr || netType[0] == 0)
	{
		strcpy(m_c_net, "IN");
	}
	else
	{
		strncpy(m_c_net, netType, sizeof(m_c_net)-1);
		m_c_net[sizeof(m_c_net)-1] = 0;
	}

	if (addrType == nullptr || addrType[0] == 0)
	{
		strcpy(m_c_ip, "IP4");
	}
	else
	{
		strncpy(m_c_ip, addrType, sizeof(m_c_ip)-1);
		m_c_ip[sizeof(m_c_ip)-1] = 0;
	}
}
//--------------------------------------
//
//--------------------------------------
const char* sdp_get_direction_name(sdp_direction_type_t dir)
{
	switch (dir)
	{
	case sdp_direction_inactive_e: return "inactive";
	case sdp_direction_recvonly_e: return "recvonly";
	case sdp_direction_sendonly_e: return "sendonly";
	case sdp_direction_sendrecv_e: return "sendrecv";
	}
	
	return "undefined";
}
//--------------------------------------
