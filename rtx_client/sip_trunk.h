/* RTX H.248 Media Gate. SIP Softphone
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sdp.h"
#include "phone_context.h"


//--------------------------------------
//
//--------------------------------------
class sip_account_t;
//--------------------------------------
// ������� IP_DISCONNECT_CALL
//--------------------------------------
#define SIP_TRUNK_DISCONNECTED 1003
struct sip_disconnect_info_t
{
	sip_call_session_t* session;
	sip_message_t* message;
};
//--------------------------------------
// 
//--------------------------------------
enum sip_trunk_state_t
{
	sip_state_new_e = 0,			// ��������� ���������
	sip_state_calling_e,			// ����� (���������/��������)
	sip_state_ringing_e,			// ������
	sip_state_progress_e,			// ���������
	sip_state_connecting_e,			// ����� ����������� (200 OK ���������/������)
	sip_state_connected_e,			// ���������� ������������ (������/��������� ACK)
	sip_state_disconnected_e,		// ���������� ���������� -- ��� ������������� ������ � �����!
};
extern const char* get_sip_trunk_state_name(sip_trunk_state_t state);
//--------------------------------------
//
//--------------------------------------
class sip_trunk_t : public call_session_event_handler_t, rtl::async_call_target_t
{
	rtl::MutexWatch m_sync;
	sip_account_t& m_account;
	sip_profile_t m_profile;

	sip_trunk_state_t m_call_state;						// ��������� ������
	bool m_incoming_call;								// true - incoming, false - outgoing
	bool m_hold_state;									// ������� ��������� ������
	bool m_mute_state;									// ������� ����� �� �������� �����

	sip_message_t m_incoming_invite;

	DisconnectReason m_call_end_reason;					// ������� ������� ����������
	uint16_t m_reject_code;
	rtl::String m_reject_reason_phrase;

	sip_call_session_t* m_call_session;						// ������
	phone_rtp_term_t* m_media_session;


public:
	sip_trunk_t(sip_account_t& account);
	~sip_trunk_t();

	void destroy();

	sip_account_t& get_account() { return m_account; }

	bool process_incoming_call(sip_call_session_t& session, const sip_message_t& invite);
	bool call_to(const char* number, const char* caller_id, const char* caller_name);
	bool call_to_target(const sip_value_uri_t& referTo, const sip_value_uri_t* referredBy);
	void disconnect();
	bool answer(bool ringing);
	void reject(uint16_t code, const char* reason);
	rtl::String get_sip_header(const char* sip_header_name, int index);
	void hold(bool state);
	void mute(bool state);
	void refer_to(const sip_value_uri_t& referTo);
	void send_dtmf(char tone);

	void start_playing(const char* file_path, bool in_cycle);
	void stop_playing();

	void start_recording(const char* file_path);
	void stop_recording();

	const sip_profile_t& get_profile() { return m_profile; }
	sip_trunk_state_t get_call_state() { return m_call_state; }
	void get_remote_uri(sip_uri_t& uri) const;
	void get_local_uri(sip_uri_t& uri) const;
	sip_call_session_t* get_session() { return m_call_session; }
	bool check_dialog(const rtl::String& callid, const rtl::String& totag, const rtl::String& fromtag);

private:
	void process_call_disconnected_event(bool local_end);
	void process_call_rejected_event(uint16_t status_code, const char* status_reason);

	/// ��������� SDP �������
	bool create_media_session();
	void destroy_media_session();

	/// async_call_target_t interface
	virtual const char* async_get_name();
	virtual void async_handle_call(uint16_t call_id, uintptr_t int_param, void* ptr_param);

	/// asynchronius functions
	void async_session_call_disconnected(sip_disconnect_info_t* info);

	/// call_session_event_handler_t
	virtual void call_session__recv_1xx(sip_call_session_t& session, const sip_message_t& message);
	virtual void call_session__call_established(sip_call_session_t& session, const sip_message_t& message);
	virtual void call_session__call_disconnected(sip_call_session_t& session, const sip_message_t& message);

	virtual void call_session__ack_sent(sip_call_session_t& session);
	virtual void call_session__recv_ack(sip_call_session_t& session, const sip_message_t& msg);

	virtual reinvite_response_t call_session__recv_reinvite(sip_call_session_t& session, const sip_message_t& offer);
	virtual void call_session__call_reestablished(sip_call_session_t& session, const sip_message_t& message);

	virtual bool call_session__apply_sdp_offer(sip_call_session_t& session, const sip_message_t& offer);
	virtual bool call_session__prepare_sdp_answer(sip_call_session_t& session, sip_message_t& answer);
	virtual bool call_session__prepare_sdp_offer(sip_call_session_t& session, sip_message_t& offer);
	virtual bool call_session__apply_sdp_answer(sip_call_session_t& session, const sip_message_t& answer);

	virtual bool call_session__apply_sdp_reoffer(sip_call_session_t& session, const sip_message_t& offer);
	virtual bool call_session__prepare_sdp_reanswer(sip_call_session_t& session, sip_message_t& answer);
	virtual bool call_session__prepare_sdp_reoffer(sip_call_session_t& session, sip_message_t& offer, bool hold);
	virtual bool call_session__apply_sdp_reanswer(sip_call_session_t& session, const sip_message_t& anser);

	virtual bool call_session__recv_ni_request(sip_call_session_t& session, const sip_message_t& request, sip_message_t& response);
	virtual void call_session__recv_ni_response(sip_call_session_t& session, const sip_message_t& response);

	virtual void call_session__failure(sip_call_session_t& session);

	/// specefic requests handler
	void process_info_request(const sip_message_t& info, sip_message_t& response);
	void process_refer_request(const sip_message_t& refer, sip_message_t& response);
	void process_refer_response(const sip_message_t& response);
	void process_notify_request(const sip_message_t& notify, sip_message_t& response);		
	//...
};
//--------------------------------------

