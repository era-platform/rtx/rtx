/* RTX H.248 Media Gate. SIP Softphone
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "std_basedef.h"
#include "std_typedef.h"
#include "std_funcdef.h"

//-----------------------------------------------
//
//-----------------------------------------------
#if defined TARGET_OS_WINDOWS
	#if defined RTX_CLIENT_EXPORTS
		#define SIPC_API __declspec(dllexport)
	#else
		#define SIPC_API __declspec(dllimport)
	#endif
	#define SIPC_CALL __stdcall
#else
	#define SIPC_API
	#define SIPC_CALL
#endif
//-----------------------------------------------
// ��������� ��� �������� ������ ������� �����
// tx - ���������
// rx - ���������
//-----------------------------------------------
enum DisconnectReason
{
	DisconnectByeTX,		// TX ���������
	DisconnectByeRX,		// RX ��������
	DisconnectCancelTX,
	DisconnectCancelRX,
	DisconnectRejectTX,
	DisconnectRejectRX,
	DisconnectTimeout,		// ������ ��� ��������� �������
	DisconnectUnknown,		// ��������� �������� ������
};
//-----------------------------------------------
// ��������� �������� ��� ����������
//-----------------------------------------------
enum SIPTransportType
{
	SIPTransportUDP = 0,
	SIPTransportTCP = 1,
	SIPTransportTLS = 2,
	SIPTransportWS = 3,
	SIPTransportWSS = 4,
};
//-----------------------------------------------
//
//-----------------------------------------------
enum ReferNotifyType
{
	ReferNotify_Income = 0,
	ReferNotify_Answer = 1,
	ReferNotify_Notify = 3,
};
//-----------------------------------------------
// ��������� ������. ������ ������������ ������. ����� ������ ��������
//-----------------------------------------------
#pragma pack(push, 1)
struct PayloadFormatRecord
{
	char encoding[64];
	int id;
	int clockrate;
	int channels;
};
#pragma pack(pop)
//-----------------------------------------------
// ��������� SIP �������
//-----------------------------------------------
#define STDKEY_WORK_FOLDER				"workFolder"
#define STDKEY_LOG_FOLDER				"logFolder"
#define STDKEY_TRACE_LOGS				"traceLogFlags"
#define STDKEY_RECORD_FOLDER			"recordFolder"
#define STDKEY_SIP_LISTENER_INTERFACE	"sipListenerInterface"
#define STDKEY_SIP_LISTENER_PORT		"sipListenerPort"

#define STDKEY_JITTER_DEPTH				"jitterDepth"
#define STDKEY_WAVE_BUFFER_COUNT		"waveBufferCount"
#define STDKEY_WAVE_BUFFER_TIME			"waveBufferTime"

#define STDKEY_SIP_UA_NAME				"sipUserAgentName"
//-----------------------------------------------
// ��������� ������� ��������� ������
//-----------------------------------------------
/// <summary>
/// ������� �������� ����������� ��������
/// </summary>
/// <param name="account_id">���������� ������������� ��������</param>
typedef void (SIPC_CALL *SIP_AccountRegisteredEventHandler)(uintptr_t account_id);
/// <summary>
/// ������� ��� �������������� ��� � ������� ��� �����������
/// </summary>
/// <param name="account_id">���������� ������������� ��������</param>
/// <param name="reason">true - ��������������, ���������� ������� �����������</param>
typedef void (SIPC_CALL *SIP_AccountUnregisteredEventHandler)(uintptr_t account_id, bool by_user);
/// <summary>
/// ������� ��������� ������ �� ��������
/// </summary>
/// <param name="account_id">���������� ������������� ��������</param>
/// <param name="trunk_id">���������� ������������� ������</param>
/// <param name="caller_id">����� ����������� ��������</param>
/// <param name="caller_name">��� ����������� ��������</param>
/// <returns>true - process call, false - reject call</returns>
typedef void (SIPC_CALL *SIP_AccountIncomingCallEventHandler)(uintptr_t account_id, uintptr_t trunk_id, const char* called_id, const char* called_name, const char* caller_id, const char* caller_name);
/// <summary>
/// ������������� ���������� c������ � ��������
/// </summary>
/// <param name="account_id">���������� ������������� ��������</param>
/// <param name="event_name">��� �������</param>
/// <param name="event_parameter">��������� ������������� ����������</param>
typedef void(SIPC_CALL *SIP_AccountNamedEventHandler)(uintptr_t account_id, const char* event_name, const char* event_parameter);
/// <summary>
/// ������� ���������������� ������ (1xx)
/// </summary>
/// <param name="trunk_id">���������� ������������� ������</param>
/// <param name="with_media">true - ����� ������ � ����������� ����� � ����� ������ ������ ������� (Progress),
/// false - ����������� ��� ������ ����� �� �������� (Ringing)</param>
typedef void (SIPC_CALL *SIP_CallRingingEventHandler)(uintptr_t trunk_id, uint16_t code, const char* reason, bool with_media);
/// <summary>
/// ������� �������������� �������������� ������ (2xx)
/// </summary>
/// <param name="trunk_id">���������� ������������� ������</param>
typedef void (SIPC_CALL *SIP_CallAnsweredEventHandler)(uintptr_t trunk_id);
/// <summary>
/// ������� �������������� �������������� ������ 3xx - 6xx
/// </summary>
/// <param name="trunk_id">���������� ������������� ������</param>
typedef void (SIPC_CALL *SIP_CallRejectedEventHandler)(uintptr_t trunk_id, uint16_t code, const char* reason);
/// <summary>
/// ������� ��������� ������������ �����
/// </summary>
/// <param name="trunk_id">���������� ������������� ������</param>
typedef void (SIPC_CALL *SIP_CallConnectedEventHandler)(uintptr_t trunk_id);
/// <summary>
/// ������� ��������� �����.
/// �������� ��� ��� ������, ������ ��� � ��� ������ �����
/// </summary>
/// <param name="trunk_id">���������� ������������� ������</param>
typedef void (SIPC_CALL *SIP_CallDisconnectedEventHandler)(uintptr_t trunk_id, enum DisconnectReason reason);
/// <summary>
/// ����� ��������� NOTIFY � ���� �������� ������
/// </summary>
/// <param name="trunk_id">����� ����������� REFER</param>
/// <param name="sipfrag">���������� � ��������� ������ � ����� ��������� ������ ������ �� REFER-target</param>
typedef void (SIPC_CALL *SIP_CallReferNotifyEventHandler)(uintptr_t trunk_id, ReferNotifyType type, const char* message);
/// <summary>
/// ����� ��������� NOTIFY � ���� �������� ������
/// </summary>
/// <param name="trunk_id">����� ����������� REFER</param>
/// <param name="sipfrag">���������� � ��������� ������ � ����� ��������� ������ ������ �� REFER-target</param>
typedef void (SIPC_CALL *SIP_CallReplacedByReferEventHandler)(uintptr_t trunk_old, uintptr_t trunk_new, const char* called_id, const char* called_name, const char* caller_id, const char* caller_name);
/// <summary>
/// ��������������� ����� �����������. ������� ������������ ������ ���� ��������������� ����� ������������ �� ������� ����� ������.
/// �� ���� ��������� ������� ������� �� ������������
/// </summary>
/// <param name="trunk_id">���������� ������������� ������</param>
typedef void (SIPC_CALL *SIP_CallPlayStopped)(uintptr_t trunk_id);
/// <summary>
/// ������ �������� �� ������ �����
/// </summary>
/// <param name="value_key">���� ��� ������</param>
/// <param name="value_buffer">������ ��� ��������</param>
/// <param name="buffer_size">������ ������</param>
/// <returns>�������� �� ����� ��������</returns>
typedef bool (SIPC_CALL *SIP_ReadConfigValueEventHandler)(const char* value_key, char* value_buffer, int buffer_size);
//--------------------------------------
// ��������� ��� �������� ���������� �� ������� ��������� ������
// ��������� ������������
//--------------------------------------
struct sip_client_callback_interface_t
{
    SIP_AccountRegisteredEventHandler AccountRegistered;
    SIP_AccountUnregisteredEventHandler AccountUnregistered;
    SIP_AccountIncomingCallEventHandler AccountIncomingCall;
	SIP_AccountNamedEventHandler AccountNamedEvent;
    SIP_CallRingingEventHandler CallRinging;
    SIP_CallAnsweredEventHandler CallAnswered;
    SIP_CallRejectedEventHandler CallRejected;
    SIP_CallConnectedEventHandler CallConnected;
    SIP_CallDisconnectedEventHandler CallDisconnected;
	SIP_CallReferNotifyEventHandler CallReferNotify;
	SIP_CallReplacedByReferEventHandler CallReplacedByRefer;
	SIP_CallPlayStopped CallPlayStopped;
	SIP_ReadConfigValueEventHandler ReadConfigValue;
};
//--------------------------------------
// ��������� � ��� �������
//--------------------------------------
extern "C"
{
/// <summary>
/// ������������� oktell.SIPClientSvc.dll. ���������� ����������� � ������ SIP �������
/// </summary>
/// <param name="callback_info">������� ����������� ������� �� ����������</param>
/// <returns>True</returns>
SIPC_API bool SIPC_CALL SIP_InitLib(sip_client_callback_interface_t& callback_info);
/// <summary>
/// ������� ������� HAL.StopService()
/// </summary>
SIPC_API void SIPC_CALL SIP_FreeLib();
/// <summary>
/// ���������� �������� (������� ������ ���� username@domain)
/// </summary>
/// <param name="username">��� �������� �� ������� �����������</param>
/// <param name="domain">��� ������� �����������</param>
/// <returns>���������� ������������� ��������</returns>
/// <remarks>����� �������� ������� �� �������� ��������� � ��������� ���������:
/// ����������� ��������������,
/// ������ ����� ������������ �� ����� ������ �.� ������ �� ���������
/// ������ ��������������� 3600 ������,
/// ����������� ������ 711u/711a/GSM</remarks>
SIPC_API uintptr_t SIPC_CALL SIP_AccountCreate(const char* username, const char* domain);
/// <summary>
/// �������� �������� � �������������� ���� ������� ��� ���������������
/// </summary>
/// <param name="account_id">���������� ������������� ��������</param>
SIPC_API void SIPC_CALL SIP_AccountDestroy(uintptr_t account_id);
//-----
/// <summary>
/// ��������� ���������� ����� ������ ����� ����������� ������� ����������� � ��������� ������
/// ������� ��� UDP ��� ��� ��� TCP ���� ����������� ��������
/// </summary>
/// <param name="account_id">���������� ������������� ��������</param>
/// <param name="port">��������� ����</param>
/// <returns></returns>
SIPC_API void SIPC_CALL SIP_AccountSetLocalPort(uintptr_t account_id, uint16_t port);
/// <summary>
/// ��������� ������ ������ ������� ���� ����� ����������� ������� ����������� � ��������� ������
/// </summary>
/// <param name="account_id">���������� ������������� ��������</param>
/// <param name="address">IPv4 ����� ��� DNS ��� ������ �������</param>
/// <param name="port">���� ������ �������</param>
/// <returns></returns>
SIPC_API void SIPC_CALL SIP_AccountSetProxy(uintptr_t account_id, const char* address, uint16_t port, enum SIPTransportType type);
/// <summary>
/// �������������� ��������� �������� ������� Keep-Alive
/// </summary>
/// <param name="account_id">���������� ������������� ��������</param>
/// <param name="use_keepalive">���������/���������� �������������� ��������</param>
/// <param name="interval">�������� �������� � �������� (���:5, ����:60)</param>
SIPC_API void SIPC_CALL SIP_AccountSetKeepAlive(uintptr_t account_id, int interval);
/// <summary>
/// ��������� ���������� ��������������
/// </summary>
/// <param name="account_id">���������� ������������� ��������</param>
/// <param name="auth_id">�������������</param>
/// <param name="password">������</param>
/// <returns></returns>
SIPC_API void SIPC_CALL SIP_AccountSetAuthentication(uintptr_t account_id, const char* auth_id, const char* password);
/// <summary>
/// ��������� ������������� �����
/// </summary>
/// <param name="account_id">���������� ������������� ��������</param>
/// <param name="displayName">������������ ���</param>
SIPC_API void SIPC_CALL SIP_AccountSetDisplayName(uintptr_t account_id, const char* displayName);
/// <summary>
/// ����������� ������� ����� ��� ������ �����������
/// </summary>
/// <param name="account_id">���������� ������������� ��������</param>
/// <param name="expires">����� ����� � ��������</param>
SIPC_API void SIPC_CALL SIP_AccountSetExpires(uintptr_t account_id, int expires);
/// <summary>
/// ��������� ���������� ������ ��� ���������� ��������
/// </summary>
/// <param name="account_id">���������� ������������� ��������</param>
/// <param name="payload_set">��������� �� ������ ��������������� �������</param>
/// <param name="count">����������</param>
SIPC_API void SIPC_CALL SIP_AccountSetAudioCodecs(uintptr_t account_id, const PayloadFormatRecord* payloadList, int count);
/// <summary>
/// ����������� � ������ ������ ��������
/// </summary>
/// <param name="account_id">���������� ������������� ��������</param>
SIPC_API void SIPC_CALL SIP_AccountRegister(uintptr_t account_id);
/// <summary>
/// ������ ����������� � ������� ������ ��������, ��� ������ ���������
/// </summary>
/// <param name="account_id">���������� ������������� ��������</param>
SIPC_API void SIPC_CALL SIP_AccountUnregister(uintptr_t account_id);
/// <summary>
/// �������� ���������� ��������� MESSAGE
/// </summary>
/// <param name="account_id">���������� ������������� ��������</param>
/// <param name="to_username">������������� ���������� ���������</param>
/// <param name="to_displayname">��� ���������� ���������</param>
/// <param name="content_type">��� ��������</param>
/// <param name="message_text">���������</param>
SIPC_API void SIPC_CALL SIP_AccountSendTextMessage(uintptr_t account_id, const char* to_username, const char* to_displayname, const char* content_type, const char* message_text);
/// <summary>
/// �������� ������ ������ ��� ���������� ������
/// </summary>
/// <param name="account_id">���������� ������������� ��������</param>
/// <returns>���������� ������������� ������</returns>
SIPC_API uintptr_t SIPC_CALL SIP_AccountCreateCall(uintptr_t account_id);
/// <summary>
/// �������� ������
/// </summary>
/// <param name="trunk_id">���������� ������������� ������</param>
SIPC_API void SIPC_CALL SIP_AccountDestroyCall(uintptr_t trunk_id);
/// <summary>
/// ������� ���������� ��������
/// </summary>
/// <param name="trunk_id">���������� ������������� ������</param>
/// <param name="number">����� ����������� ��������. ������������� � �������� username � To:</param>
/// <param name="caller_id">UserName � URI ��� ���� From: ����� �����������, ���� ������ ������ ��� ���� �� ����������� ��� ��������:</param>
/// <param name="caller_name">DisplayName ��� ���� From: ��� �����������, ���� ������ ������ ��� ���� �� ������ �� ����������</param>
/// <returns></returns>
SIPC_API bool SIPC_CALL SIP_CallTo(uintptr_t trunk_id, const char* number, const char* caller_id, const char* caller_name);
/// <summary>
/// ����� ������
/// </summary>
/// <param name="trunk_id">���������� ������������� ������</param>
SIPC_API void SIPC_CALL SIP_CallDisconnect(uintptr_t trunk_id);
/// <summary>
/// ����� �� �������� �����
/// </summary>
/// <param name="trunk_id">���������� ������������� ������</param>
/// <param name="ringing">false - 200 Ok, true - 180 Ringing</param>
/// <returns></returns>
SIPC_API bool SIPC_CALL SIP_CallAnswer(uintptr_t trunk_id, bool ringing);
/// <summary>
/// ����� ��������� ������. ��� ��������� ������� ��� ������������� ���������� �������������� ��� SIP_DisconnectCall
/// </summary>
/// <param name="trunk_id">���������� ������������� ������</param>
/// <param name="code">��� ������ 3xx - 6xx</param>
/// <param name="reason">������� �����. ���� ������ null ��
/// � ����� ��������� ����������� ������� ��� ���������� ����</param>
SIPC_API void SIPC_CALL SIP_CallReject(uintptr_t trunk_id, uint16_t code, const char* reason);
/// <summary>
///  �������� ������� REFER �� ������
/// </summary>
/// <param name="trunk_id">����� �����������</param>
/// <param name="number">����� ��� ��������</param>
/// <param name="replace_to">����� ���������� (��� �������� �������) - ��� ������������ ��������� Replaces=call-id;to-tag;from-tag</param>
SIPC_API void SIPC_CALL SIP_ReferTo(uintptr_t trunk_id, const char* number, uintptr_t replace_to);
/// <summary>
/// ��������� �������� ���� SIP ���������
/// </summary>
/// <param name="trunk_id">���������� ������������� ������</param>
/// <param name="sip_header_name">��� ����</param>
/// <param name="value_index">������ �������� ����</param>
/// <returns>�������� ���� ��� ������ ������</returns>
SIPC_API bool SIPC_CALL SIP_CallGetHeader(uintptr_t trunk_id, const char* sip_header_name, int index, char* buffer, int size);
/// <summary>
/// ���������/������������� ������ (����������� �� �����������)
/// </summary>
/// <param name="trunk_id">���������� ������������� ������</param>
/// <param name="state">������������ ���������</param>
SIPC_API void SIPC_CALL SIP_CallHold(uintptr_t trunk_id, bool state);
/// <summary>
/// ���������/���������� �� ��������� (���������� ������ �����������)
/// </summary>
/// <param name="trunk_id">���������� ������������� ������</param>
/// <param name="state">��������� ���������</param>
SIPC_API void SIPC_CALL SIP_CallMute(uintptr_t trunk_id, bool state);
/// <summary>
/// ������� DTMF ������
/// </summary>
/// <param name="trunk_id">���������� ������������� ������</param>
/// <param name="tone">������ 0-9 A-D</param>
SIPC_API void SIPC_CALL SIP_CallSentDtmf(uintptr_t trunk_id, char tone);
 /// <summary>
/// ������ ��������������� ����� ������, ������ � ������ Hold (sendonly)
/// </summary>
/// <param name="trunk_id">���������� ������������� ������</param>
/// <param name="file_path">���� � ����� �����</param>
/// <param name="in_cycle">������������� ���� � �����</param>
SIPC_API void SIPC_CALL SIP_CallStartPlaying(uintptr_t trunk_id, const char* file_path, bool in_cycle);
/// <summary>
/// ���������� ��������������� ������
/// </summary>
/// <param name="trunk_id">���������� ������������� ������</param>
SIPC_API void SIPC_CALL SIP_CallStopPlaying(uintptr_t trunk_id);
/// <summary>
/// ������ ������ ���������
/// </summary>
/// <param name="trunk_id">���������� ������������� ������</param>
/// <param name="file_path">���� � ����� �����</param>
SIPC_API void SIPC_CALL SIP_CallStartRecording(uintptr_t trunk_id, const char* file_path);
/// <summary>
/// ���������� ������ ���������
/// </summary>
/// <param name="trunk_id">���������� ������������� ������</param>
SIPC_API void SIPC_CALL SIP_CallStopRecording(uintptr_t trunk_id);
/// <summary>
/// �������� ������ �����
/// </summary>
/// <param name="file_path">���� � �����</param>
SIPC_API void SIPC_CALL SIP_StartRecording(const char* file_path);
/// <summary>
/// ������������� ������ �����
/// </summary>
SIPC_API void SIPC_CALL SIP_StopRecording();
//----- ������� ���������� ����� ������������
/// <summary>
/// ����������� ��������� ��������������� � ������ � ����� ��� ���������� ��� ������ � ����� ������������
/// </summary>
/// <param name="device_out">��� ���������� ���������������</param>
/// <param name="device_in">��� ���������� ������</param>
/// <param name="direct_sound">true - ������������ ���������� DirectSound
/// false - ������������ ���������� wave</param>
SIPC_API void SIPC_CALL SIP_AudioSetDevices(const char* device_out, const char* device_in);
/// <summary>
/// ��������� ����� ���������� ��������� ��� ��������������� 
/// </summary>
/// <param name="buffer">�����</param>
/// <param name="size">������ ������</param>
SIPC_API void SIPC_CALL SIP_AudioGetOutput(char* buffer, int size);
/// <summary>
/// ������������ ���������� ��� ���������������
/// </summary>
/// <param name="deviceOutputName">��� ����������</param>
SIPC_API void SIPC_CALL SIP_AudioSetOutput(const char* device_out);
/// <summary>
/// ��������� ����� ���������� ������ ��� ��������������� 
/// </summary>
/// <param name="buffer">�����</param>
/// <param name="size">������ ������</param>
SIPC_API void SIPC_CALL SIP_AudioGetInput(char* buffer, int size);
/// <summary>
/// ������������ ���������� ��� ������
/// </summary>
/// <param name="deviceInputName">��� ����������</param>
SIPC_API void SIPC_CALL SIP_AudioSetInput(const char* device_in);
/// <summary>
/// ���������� ��������� �� ���� �������
/// </summary>
/// <param name="mute">����������/�����������</param>
SIPC_API void SIPC_CALL SIP_AudioMuteInput(bool mute);
/// <summary>
/// ��������� ���������� ������������� ������� � �������
/// </summary>
/// <returns>���������� �������</returns>
SIPC_API int SIPC_CALL SIP_AudioGetCodecCount();
/// <summary>
/// �������� ���������� � ������ �� �������
/// </summary>
/// <param name="index">������ � ������������</param>
/// <param name="payload_id">������������� ������</param>
/// <param name="payload_name">��� ������. ����� �� ����� 32 ��������</param>
/// <param name="payload_description">�������� ������. ����� �� ����� 64 ��������</param>
/// <returns>��������� ����������</returns>
SIPC_API bool SIPC_CALL SIP_AudioGetCodecInfo(int index, PayloadFormatRecord* payloadFormat);
}
//--------------------------------------
