/* RTX H.248 Media Gate. SIP Softphone
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "sip_call_manager.h"
#include "sip_account.h"
#include "sip_trunk.h"
//--------------------------------------
//
//--------------------------------------
sip_call_manager_t::sip_call_manager_t() : sip_call_session_manager_t(*g_sip_client, 2)
{
}
//--------------------------------------
//
//--------------------------------------
sip_call_manager_t::~sip_call_manager_t()
{
}
//--------------------------------------
//
//--------------------------------------
call_session_event_handler_t* sip_call_manager_t::process_incoming_connection(const sip_message_t& invite, sip_call_session_t& session)
{
	sip_account_t* account = g_sip_client->find_account(invite);

	if (account == nullptr)
	{
		LOG_CALL("call-man", "incoming call rejected : account not found!");
		return nullptr;
	}
	
	LOG_CALL("call-man", "process incoming call...");

	call_session_event_handler_t* trunk = account->process_incoming_call(invite, session);

	session.SendTrying();

	return trunk;
}
//--------------------------------------
//
//--------------------------------------
bool sip_call_manager_t::process_incoming_im_message(const sip_message_t& message)
{
	sip_account_t* account = g_sip_client->find_account(message);

	if (account == nullptr)
	{
		LOG_CALL("call-man", "incoming im message rejected : account not found!");
		return nullptr;
	}

	LOG_CALL("call-man", "process incoming im message...");

	return account->process_incoming_im_message(message);
}
//--------------------------------------
