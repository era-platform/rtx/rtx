﻿/* RTX H.248 Media Gate. SIP Softphone
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sip_call_manager.h"
#include "sip_reg_manager.h"
//-----------------------------------------------
//
//-----------------------------------------------
class sip_account_t;
class sip_trunk_t;
//-----------------------------------------------
//
//-----------------------------------------------
class sip_client_t : public sip_user_agent_t
{
	rtl::MutexWatch m_sync;
	bool m_initialized;
	sip_call_manager_t* m_call_manager;
	sip_reg_manager_t* m_reg_manager;
	rtl::ArrayT<sip_account_t*> m_account_list;
	rtl::ArrayT<sip_trunk_t*> m_trunk_list;
	uint16_t m_default_listener_port;
	/// phone state

public:
	sip_client_t();
	~sip_client_t();

	bool initialize();
	void terminate();

	uintptr_t create_account(const char* username, const char* domain);
	void destroy_account(uintptr_t account_id);
	
	void set_local_port(uintptr_t account_id, uint16_t port);
	uint16_t get_local_port(uintptr_t account_id);
	void set_proxy_address(uintptr_t account_id, const char* address, uint16_t port, sip_transport_type_t type);
	void set_keepalive(uintptr_t account_id, int keepalive_interval);
	uint32_t get_keepalive(uintptr_t account_id);
	void set_authentication(uintptr_t account_id, const char* auth_id, const char* auth_password);
	void set_displayname(uintptr_t account_id, const char* displayname);
	void set_expires(uintptr_t account_id, int expires);
	uint32_t get_expires(uintptr_t account_id);

	void start_registration(uintptr_t account_id);
	void start_unregistration(uintptr_t account_id);
	void set_payload_set(uintptr_t account_id, const media::PayloadSet& payloads);
	void send_text_message(uintptr_t account_id, const char* to_username, const char* to_displayname, const char* content_type, const char* message_text);

	uintptr_t create_trunk(uintptr_t account_id);
	void destroy_trunk(uintptr_t trunk_id);
	
	void add_trunk_ptr(sip_trunk_t* trunk);
	void remove_trunk_ptr(sip_trunk_t** trunk_list, int count = 1);

	bool trunk_call_to(uintptr_t trunk_id, const char* number, const char* caller_id, const char* caller_name);
	void trunk_disconnect(uintptr_t trunk_id);
	bool trunk_answer(uintptr_t trunk_id, bool ringing);
	void trunk_reject(uintptr_t trunk_id, uint16_t code, const char* reason);
	void trunk_refer_to(uintptr_t trunk_id, const char* number, uintptr_t replace_to);
	bool get_call_header(uintptr_t trunk_id, const char* sip_header_name, int index, rtl::String& value);
	void trunk_hold(uintptr_t trunk_id, bool state);
	//void trunk_unhold(uintptr_t trunk_id);
	void trunk_mute(uintptr_t trunk_id, bool state);
	//void trunk_unmute(uintptr_t trunk_id);
	void trunk_send_dtmf(uintptr_t trunk_id, char tone);
	void trunk_start_playing(uintptr_t trunk_id, const char* file_path, bool in_cycle);
	void trunk_stop_playing(uintptr_t trunk_id);
	void trunk_start_recording(uintptr_t trunk_id, const char* file_path);
	void trunk_stop_recording(uintptr_t trunk_id);

	void set_devices(const char* device_out, const char* device_in);
	void get_output(char* buffer, int size);
	void set_output(const char* device_out);
	void get_input(char* buffer, int size);
	void set_input(const char* device_in);
	void mute_all(bool mute);

	void start_recording(const char* file_path);
	void stop_recording();

	/// sip_call_manager_t interface
	sip_account_t* find_account(const sip_message_t& invite);

	/// sip_user_agent_t virtual func override
	virtual void process_sip_stack_event(sip_stack_event_t * sip_event);

	sip_call_manager_t* get_call_manager() { return m_call_manager; }
	sip_reg_manager_t* get_reg_manager() { return m_reg_manager; }
	
	bool add_transport(sip_transport_type_t type, in_addr if_address, uint16_t if_port);
	uint16_t get_default_listener_port() { return m_default_listener_port; }

	/// account events
	void raise_account_registered(sip_account_t* account);
	void raise_account_unregistered(sip_account_t* account, bool by_user);
	void raise_account_named_event(sip_account_t* account, const char* event_name, const char* event_parameter);
	void raise_account_incoming_call(sip_account_t* account, sip_trunk_t* trunk, const char* called_id, const char* called_name, const char* caller_id, const char* caller_name/*, const char* invite*/);
	/// trunk events
	void raise_call_ringing(sip_trunk_t* trunk, uint16_t code, const char* reason, bool with_media);
	void raise_call_answered(sip_trunk_t* trunk/*, const char* sip_message*/);
	void raise_call_rejected(sip_trunk_t* trunk, uint16_t code, const char* reason);
	void raise_call_connected(sip_trunk_t* trunk);
	void raise_call_disconnected(sip_trunk_t* trunk, DisconnectReason reason);
	void raise_call_refer_notified(sip_trunk_t* trunk, ReferNotifyType nType, const char* message);
	void raise_call_replaced_by_refer(sip_trunk_t* trunk_old, sip_trunk_t* trunk_new,
		const rtl::String& caller_id, const rtl::String& caller_name, const rtl::String& callee_id, const rtl::String& callee_name);
	void raise_call_play_stopped(sip_trunk_t* trunk_id);

private:
	void check_accounts(const sip_transport_point_t& point);
	sip_account_t* get_account(uintptr_t account_id);
	sip_trunk_t* get_trunk(uintptr_t trunk_id);
};
//-----------------------------------------------
