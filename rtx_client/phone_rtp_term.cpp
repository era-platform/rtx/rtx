/* RTX H.248 Media Gate. SIP Softphone
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "phone_media_engine.h"
#include "phone_rtp_term.h"

//-----------------------------------------------
//
//-----------------------------------------------
char phone_rtp_term_t::s_ua_name[64] = "rtx-phone";

#define LOGTAG "rtp-term"
//-----------------------------------------------
//
//-----------------------------------------------
phone_rtp_term_t::phone_rtp_term_t(h248::IMediaContext* context) :
	m_context(context),
	m_term_id(0), m_play_id(0),
	m_remote_sdp(Log),
	m_local_sdp(Log),
	m_state(State::idle),
	m_direction(Direction::none)
{
	m_audio_session_id = rtl::DateTime::getNTP() & 0x000000000FDF5FDF;
	m_audio_session_ver = rtl::DateTime::getTicks64() & 0x0000000000FDF5F0;
}
//-----------------------------------------------
//
//-----------------------------------------------
phone_rtp_term_t::~phone_rtp_term_t()
{
	destroy();
}
//-----------------------------------------------
//
//-----------------------------------------------
bool phone_rtp_term_t::setup(in_addr iface)
{
	LOG_CALL(LOGTAG, "id:new : setup : iface %s...", inet_ntoa(iface));

	if (m_term_id != MG_TERM_INVALID)
	{
		LOG_WARN(LOGTAG, "id:%u : setup : already called! %s", m_term_id, inet_ntoa(iface));
		return false;
	}

	m_iface = iface;

	const char* key = rtx_phone::get_interface_key(iface);

	h248::TerminationID termId(h248::TerminationType::RTP, key, MG_TERM_CHOOSE);

	m_term_id = m_context->mg_context_add_termination(termId, nullptr);

	LOG_CALL(LOGTAG, "id:%u : setup : done", m_term_id);

	return m_term_id != 0;
}
//-----------------------------------------------
//
//-----------------------------------------------
void phone_rtp_term_t::destroy()
{
	LOG_CALL("term", "id:%u : destroy...", m_term_id);

	if (m_play_id != MG_TERM_INVALID)
		stop_player();
	

	if (m_term_id != MG_TERM_INVALID)
	{
		m_context->mg_context_remove_termination(m_term_id);
		m_term_id = MG_TERM_INVALID;
	}

	LOG_CALL("term", "id:%u : destroy : done", m_term_id);
}
//-----------------------------------------------
//
//-----------------------------------------------
void phone_rtp_term_t::set_allowed_codecs(const PayloadSet& allowed)
{
	m_audio_codecs = allowed;
}
//-----------------------------------------------
//
//-----------------------------------------------
bool phone_rtp_term_t::set_remote_sdp(const char* remote_sdp)
{
	if (m_state == State::idle)
	{
		// incoming call
		if (m_context->mg_context_lock() && m_context->mg_termination_lock(m_term_id))
		{
			rtl::String std_template = make_sdp_template(remote_sdp);
			m_context->mg_termination_set_LocalControl_Mode(m_term_id, 1, h248::TerminationStreamMode::SendRecv, nullptr);
			m_context->mg_termination_set_Local_Remote(m_term_id, 1, remote_sdp, std_template, nullptr);

			m_context->mg_termination_unlock(m_term_id);
			m_context->mg_context_unlock();

			m_state = State::offered;

			return true;
		}
	}
	else if (m_state == State::offered)
	{
		// received answer for outgoing call
		if (m_context->mg_context_lock() && m_context->mg_termination_lock(m_term_id))
		{
			rtl::String local_sdp = m_local_sdp.encode();
			m_context->mg_termination_set_LocalControl_Mode(m_term_id, 1, h248::TerminationStreamMode::SendRecv, nullptr);
			m_context->mg_termination_set_Local_Remote(m_term_id, 1, remote_sdp, local_sdp, nullptr);
			m_context->mg_termination_unlock(m_term_id);
			m_context->mg_context_unlock();

			m_state = State::answered;

			return true;
		}
	}

	return false;
}
//-----------------------------------------------
//
//-----------------------------------------------
bool phone_rtp_term_t::get_local_sdp(rtl::String& sdp_out)
{
	if (m_state == State::idle)
	{
		// outgoing call
		if (m_context->mg_context_lock() && m_context->mg_termination_lock(m_term_id))
		{
			rtl::String std_template = make_sdp_template();
			m_context->mg_termination_set_LocalControl_Mode(m_term_id, 1, h248::TerminationStreamMode::SendRecv, nullptr);
			m_context->mg_termination_set_Local_Remote(m_term_id, 1, rtl::String::empty, std_template, nullptr);
			m_context->mg_termination_get_Local(m_term_id, 1, sdp_out, nullptr);
			
			m_context->mg_termination_unlock(m_term_id);
			m_context->mg_context_unlock();

			m_local_sdp.decode(sdp_out);
			sdp_out = m_local_sdp.encode();
			
			m_state = State::offered;

			return true;
		}
	}
	else if (m_state == State::offered)
	{
		// answer for incoming call
		if (m_context->mg_context_lock() && m_context->mg_termination_lock(m_term_id))
		{
			m_context->mg_termination_get_Local(m_term_id, 1, sdp_out, nullptr);

			m_context->mg_termination_unlock(m_term_id);
			m_context->mg_context_unlock();

			m_local_sdp.decode(sdp_out);
			sdp_out = m_local_sdp.encode();
			
			m_state = State::answered;
			return true;
		}
	}

	// error call
	return false;
}
//-----------------------------------------------
//
//-----------------------------------------------
bool phone_rtp_term_t::reset_remote_sdp(const char* remote_sdp)
{
	if (m_state == State::answered)
	{
		// incoming reinvite
		if (m_context->mg_context_lock() && m_context->mg_termination_lock(m_term_id))
		{
			rtl::String std_template = make_sdp_template(remote_sdp);
			m_context->mg_termination_set_LocalControl_Mode(m_term_id, 1, h248::TerminationStreamMode::SendRecv, nullptr);
			m_context->mg_termination_set_Local_Remote(m_term_id, 1, remote_sdp, std_template, nullptr);

			m_context->mg_termination_unlock(m_term_id);
			m_context->mg_context_unlock();

			m_state = State::offered;

			return true;
		}
	}
	else if (m_state == State::re_offered)
	{
		// answer for outgoing reinvite
		// received answer for outgoing call
		if (m_context->mg_context_lock() && m_context->mg_termination_lock(m_term_id))
		{
			rtl::String local_sdp = m_local_sdp.encode();
			m_context->mg_termination_set_LocalControl_Mode(m_term_id, 1, h248::TerminationStreamMode::SendRecv, nullptr);
			m_context->mg_termination_set_Local_Remote(m_term_id, 1, remote_sdp, local_sdp, nullptr);
			m_context->mg_termination_unlock(m_term_id);
			m_context->mg_context_unlock();

			m_state = State::answered;

			return true;
		}
	}
	else
	{
		// error call
	}

	return false;
}
//-----------------------------------------------
//
//-----------------------------------------------
bool phone_rtp_term_t::reget_local_sdp(bool hold, rtl::String& local_sdp)
{
	if (m_state == State::answered)
	{
		// outgoing reinvite
		// outgoing call
		if (m_context->mg_context_lock() && m_context->mg_termination_lock(m_term_id))
		{
			m_local_sdp.get_media_description(sdp_media_audio_e)->set_direction(hold ? sdp_direction_sendonly_e : sdp_direction_sendrecv_e);
			rtl::String std_template = m_local_sdp.encode();
			m_context->mg_termination_set_LocalControl_Mode(m_term_id, 1, hold ? h248::TerminationStreamMode::Inactive : h248::TerminationStreamMode::SendRecv, nullptr);
			m_context->mg_termination_set_Local_Remote(m_term_id, 1, rtl::String::empty, std_template, nullptr);
			m_context->mg_termination_get_Local(m_term_id, 1, local_sdp, nullptr);

			m_context->mg_termination_unlock(m_term_id);
			m_context->mg_context_unlock();

			m_local_sdp.decode(local_sdp);
			local_sdp = m_local_sdp.encode();

			m_state = State::re_offered;

			return true;
		}

	}
	else if (m_state == State::re_offered)
	{
		// answer for incoming reinvite
		if (m_context->mg_context_lock() && m_context->mg_termination_lock(m_term_id))
		{
			m_context->mg_termination_get_Local(m_term_id, 1, local_sdp, nullptr);

			m_context->mg_termination_unlock(m_term_id);
			m_context->mg_context_unlock();

			m_local_sdp.decode(local_sdp);
			local_sdp = m_local_sdp.encode();

			m_state = State::answered;
			return true;
		}
	}
	else
	{
		// error call
	}

	return false;
}
//-----------------------------------------------
//
//-----------------------------------------------
bool phone_rtp_term_t::start_tone_player(int signal)
{
	bool res = false;
	if (m_state == State::answered)
	{
	}

	return res;
}
//-----------------------------------------------
//
//----------------------------------------------- // phone_signeal_t
bool phone_rtp_term_t::start_player(const char* path, bool cycled)
{
	bool res = false;
	if (m_state == State::answered)
	{
	}
	return res;
}
//-----------------------------------------------
//
//-----------------------------------------------
void phone_rtp_term_t::stop_player()
{
	if (m_state == State::answered)
	{
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
bool phone_rtp_term_t::start_recording(const char* path)
{
	bool res = false;
	if (m_state == State::answered)
	{
	}
	return res;
}
//-----------------------------------------------
//
//-----------------------------------------------
void phone_rtp_term_t::stop_recording()
{
	if (m_state == State::answered)
	{
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
void phone_rtp_term_t::mute(bool state)
{
	if (m_state == State::answered)
	{
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
void phone_rtp_term_t::send_dtmf(char tone, int duration)
{
	if (m_state == State::answered)
	{
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
void phone_rtp_term_t::set_ua_name(const char* ua_name)
{
	STR_COPY(s_ua_name, ua_name);
}
//-----------------------------------------------
//
//v=0
//c=IN IP4 $
//m=audio $ RTP/AVP 0 8 101 13 18
//a=rtpmap:0 PCMU/8000
//a=rtpmap:8 PCMA/8000
//a=rtpmap:101 telephone-event/8000
//a=fmtp:101 0-15
//a=ptime:20
//a=rtpmap:13 CN/8000
//a=rtpmap:18 G729/8000
//a=sendrecv
//-----------------------------------------------
//v=0
//c=IN IP4 192.168.0.10
//m=audio 10000 RTP/AVP 0 8 101 13 18
//a=rtcp:10001 IN IP4 192.168.0.10
//a=rtpmap:0 PCMU/8000
//a=rtpmap:8 PCMA/8000
//a=rtpmap:101 telephone-event/8000
//a=fmtp:101 0-15
//a=rtpmap:13 CN/8000
//a=rtpmap:18 G729/8000
//a=sendrecv
//a=ptime:20
//-----------------------------------------------
rtl::String phone_rtp_term_t::make_sdp_template()
{
	m_local_sdp.cleanup();
	m_local_sdp.set_connection(m_iface);
	m_local_sdp.set_direction(sdp_direction_sendrecv_e);
	m_local_sdp.set_origin("-", m_audio_session_id, m_audio_session_ver++, inet_ntoa(m_iface));
	m_local_sdp.set_session_name("RTX test phone");
	m_local_sdp.set_direction(sdp_direction_sendrecv_e);
	sdp_media_description_t* audio = NEW sdp_media_description_t(&m_local_sdp, sdp_media_audio_e, "audio");

	load_audio_codecs();

	audio->set_media_formats(m_audio_codecs);
	audio->set_port_choose();
	audio->set_direction(sdp_direction_sendrecv_e);

	m_local_sdp.add_media_description(audio);
	
	return m_local_sdp.encode();
}
//-----------------------------------------------
//
//-----------------------------------------------
void phone_rtp_term_t::update_local_sdp(const rtl::String& sdp)
{
	//...
	m_local_sdp.decode(sdp); // ?
}
//-----------------------------------------------
//
//-----------------------------------------------
void phone_rtp_term_t::load_audio_codecs()
{
	//...
	if (m_audio_codecs.getCount() == 0)
	{
		rtx_codec__get_available_audio_codecs(m_audio_codecs);
	}
}
rtl::String phone_rtp_term_t::make_sdp_template(const char* remote_sdp)
{
	m_remote_sdp.decode(remote_sdp);

	m_local_sdp.cleanup();
	m_local_sdp.set_connection(m_iface);
	m_local_sdp.set_direction(sdp_direction_sendrecv_e);
	m_local_sdp.set_origin("-", m_audio_session_id, m_audio_session_ver++, inet_ntoa(m_iface));
	m_local_sdp.set_session_name("RTX test phone");
	m_local_sdp.set_direction(sdp_direction_sendrecv_e);

	sdp_media_description_t* r_audio = m_remote_sdp.get_media_description(sdp_media_audio_e);

	PayloadSet result;
	PayloadSet::intersect(r_audio->get_media_formats(), m_audio_codecs, result);

	sdp_media_description_t* l_audio = NEW sdp_media_description_t(&m_local_sdp, sdp_media_audio_e, "audio");

	load_audio_codecs();

	l_audio->add_media_format(result.getTopFormat());
	l_audio->set_port_choose();
	l_audio->set_direction(sdp_direction_sendrecv_e);

	m_local_sdp.add_media_description(l_audio);

	return m_local_sdp.encode();
}
//-----------------------------------------------

