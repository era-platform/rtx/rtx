﻿/* RTX H.248 Media Gate. SIP Softphone
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mmt_media_buffers.h"
#include "mmt_file_wave.h"

#include "audio_device.h"
//-----------------------------------------------
//
//-----------------------------------------------
class phone_aux_stream_t : public aux_device_in_handler_t, public aux_device_out_handler_t
{
	rtl::Mutex m_sync;
	volatile bool m_aux_started;		// признак работы aux устройства

	// pcm format
	int m_clockrate;
	int m_channels;

	/// waveout settings
	rtl::String m_out_device;
	aux_device_out_t* m_out;
	int m_out_header_count;
	int m_out_header_time;
	media::AudioQueue m_out_queue;
	media::JitterQueue m_out_jitter;
	int m_out_jitter_depth;
	int m_out_started;
	volatile bool m_out_thread_enter;	// поток вывода в обработке

	/// wave in settings
	rtl::String m_in_device;
	aux_device_in_t* m_in;
	int m_in_header_count;
	aux_device_in_handler_t* m_in_handler;
	volatile bool m_in_thread_enter;	// поток ввода в обработке

	/// media recording for debugging
	media::FileWriterWAV m_rec_out;
	bool m_rec_out_enable;
	media::FileWriterWAV m_rec_in;
	bool m_rec_in_enable;
	rtl::String m_rec_folder;

public:
	phone_aux_stream_t();
	virtual ~phone_aux_stream_t();

	bool create(int clockrate, int channels, int headerCount, int headerTime, int jitterDepth);
	void destroy();

	bool start_device();
	void stop_device();

	bool isStarted() { return m_aux_started; }

	void mute(bool mute = true);
	// data exchange api
	void add_output_data(const short* pcm, int size, uint16_t seq_no);

	bool set_devices(const char* output, const char* input);
	bool set_output(const char* output);
	bool set_input(const char* input);
	void set_input_data_handler(aux_device_in_handler_t* handler);

	const char* get_output();// { return m_out_device; }
	const char* get_input();// { return m_in_device; }

	void record_set_path(const char* recFolder);
	void record_input(bool enable = true);
	void record_output(bool enable = true);

protected:
	// <aux_device_in_handler_t>
	virtual void aux_in_data_ready(const short* mediaData, int size);
	// <aux_device_out_handler_t>
	virtual int aux_out_get_data(short* mediaData, int size);

	void record_output_start();
	void record_output_stop();
	void record_output_write(const short* samples, int count);

	void record_input_start();
	void record_input_stop();
	void record_input_write(const short* samples, int count);
};
//--------------------------------------
