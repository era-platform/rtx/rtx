/* RTX H.248 Media Gate. SIP Softphone
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "phone_dev_term.h"
#include "phone_media_engine.h"
//-----------------------------------------------
//
//-----------------------------------------------
#define LOGTAG "dev-term"
//-----------------------------------------------
//
//-----------------------------------------------
phone_device_term_t::phone_device_term_t() :
	m_context(nullptr),
	m_term_id(MG_TERM_INVALID),
	m_dev_in__callback(nullptr)
{
}
//-----------------------------------------------
//
//-----------------------------------------------
phone_device_term_t::~phone_device_term_t()
{
}
//-----------------------------------------------
//
//-----------------------------------------------
bool phone_device_term_t::create(h248::IMediaContext* context)
{
	LOG_CALL(LOGTAG, "id:new : create...");

	// device with name 'aux'
	m_context = context;

	h248::TerminationID termId(h248::TerminationType::Device, "aux", MG_TERM_CHOOSE);
	m_term_id = m_context->mg_context_add_termination(termId, nullptr);
	
	m_aux_stream.create(8000, 1, 4, 200, 16);
	m_aux_stream.set_input_data_handler(this);

	rtx_phone::set_device_in(this);
	rtx_phone::set_device_out(this);

	LOG_CALL(LOGTAG, "id:%u : create : done", m_term_id);

	return m_term_id != MG_TERM_INVALID;
}
//-----------------------------------------------
//
//-----------------------------------------------
void phone_device_term_t::destroy()
{
	LOG_CALL(LOGTAG, "id:%u : destroy...", m_term_id);

	m_aux_stream.stop_device();
	m_aux_stream.destroy();

	if (m_term_id != MG_TERM_INVALID && m_context->mg_termination_lock(m_term_id))
	{
		m_context->mg_context_remove_termination(m_term_id);
		m_term_id = MG_TERM_INVALID;
		m_context = nullptr;
	}
	else
	{
		LOG_WARN(LOGTAG, "id:%u : destroy : failed", m_term_id);
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
bool phone_device_term_t::start_device()
{
	LOG_CALL(LOGTAG, "id:%u : start_device...");

	bool result;

	if (m_context->mg_termination_lock(m_term_id))
	{
		rtl::String remote_sdp =
			"v=0\r\n\
			c=IN IP4 127.0.0.1\r\n\
			m=audio 0 AUX/DEV 11\r\n\
			a=rtpmap:11 L16/8000\r\n\
			a=ptime:20\r\n\
			a=sendrecv";

		rtl::String local_sdp =
			"v=0\r\n\
			c=IN IP4 127.0.0.1\r\n\
			m=audio 0 AUX/DEV 11\r\n\
			a=rtpmap:11 L16/8000\r\n\
			a=ptime:20\r\n\
			a=sendrecv";

		m_context->mg_termination_set_Local_Remote(m_term_id, 1, remote_sdp, local_sdp, nullptr);
		m_context->mg_termination_set_LocalControl_Mode(m_term_id, 1, h248::TerminationStreamMode::SendRecv, nullptr);
		m_context->mg_termination_unlock(m_term_id);

		result = m_aux_stream.start_device();
	}

	LOG_CALL(LOGTAG, "id:%u : start_device : %s", m_term_id, result ? "done" : "failed");

	return result;
}
//-----------------------------------------------
//
//-----------------------------------------------
void phone_device_term_t::stop_device()
{
	LOG_CALL(LOGTAG, "id:%u : stop_device...", m_term_id);
	m_aux_stream.stop_device();
	LOG_CALL(LOGTAG, "id:%u : stop_device : done", m_term_id);
}
//-----------------------------------------------
//
//-----------------------------------------------
uintptr_t phone_device_term_t::deviceInput__addHandler(h248::IAudioDeviceInputCallback* cb, uint32_t freq, uint32_t channels)
{
	m_dev_in__callback = cb;

	// freq and channels parametrs ignored -> 8000 kz and mono (1 channel)
	LOG_CALL(LOGTAG, "id:%u : deviceInput__addHandler (freq:%u ch:%u)...", m_term_id, freq, channels);

	return 0;
}
//-----------------------------------------------
//
//-----------------------------------------------
void phone_device_term_t::deviceInput__removeHandler(uintptr_t device)
{
	// ignore
	LOG_CALL(LOGTAG, "id:%u : deviceInput__removeHandler %p...", m_term_id, device);
}
//-----------------------------------------------
//
//-----------------------------------------------
uintptr_t phone_device_term_t::deviceOutput__addHandler(uint32_t freq, uint32_t channels)
{
	// ignore
	LOG_CALL(LOGTAG, "id:%u : deviceOutput__addHandler (freq:%u ch:%u)...", m_term_id, freq, channels);
	return 0;
}
//-----------------------------------------------
//
//-----------------------------------------------
void phone_device_term_t::deviceOutput__removeHandler(uintptr_t device)
{
	// ignore
	LOG_CALL(LOGTAG, "id:%u : deviceOutput__removeHandler %p...", m_term_id, device);
}
//-----------------------------------------------
//
//-----------------------------------------------
void phone_device_term_t::deviceOutput__playAudio(uintptr_t device, const uint8_t* data, uint32_t len, uint16_t seq_no)
{
	//LOG_CALL(LOGTAG, "id:%u : deviceOutput__playAudio %p %p %u %u...", m_term_id, device, data, len, seq_no);

	m_aux_stream.add_output_data((const short*)data, len / 2, seq_no);
}
//-----------------------------------------------
//
//-----------------------------------------------
void phone_device_term_t::aux_in_data_ready(const short* mediaData, int size)
{
	uint32_t ts = 0;

	//LOG_CALL(LOGTAG, "id:%u : aux_in_data_ready %u samples...", m_term_id, size);

	if (m_dev_in__callback != nullptr)
	{
		m_dev_in__callback->deviceInput__dataReady((const uint8_t*)mediaData, size * 2, ts);
	}
	else
	{
		LOG_WARN(LOGTAG, "id:%u : aux_in_data_ready handler is nullptr...", m_term_id);
	}
}
//-----------------------------------------------
