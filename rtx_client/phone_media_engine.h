/* RTX H.248 Media Gate. SIP Softphone
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once


#include "phone_context.h"

//--------------------------------------
//
//--------------------------------------
class rtx_phone : public h248::IMediaGateEventHandler
{
	static rtl::Mutex m_sync;
	static h248::IMediaGate* m_engine;
	static rtx_phone s_phone;
	struct IFACE_NAME { in_addr address; char name[16]; };
	static rtl::ArrayT<IFACE_NAME> s_ifaces;

public:
	static void initialize();
	static void release();

	// простая блокировка
	static h248::IMediaContext* create_context();
	static void destroy_context(h248::IMediaContext*);

	// RTX-48
	static void set_device_in(h248::IAudioDeviceInput* callback);
	static void set_device_out(h248::IAudioDeviceOutput* callback);

	static const char* get_interface_key(in_addr addr);

	static phone_context_t context;

private:
	rtx_phone();
	~rtx_phone();

	/// IMediaGateEventHandler
	virtual void mge__event_raised();
	virtual void mge__error_raised();
	
	static void initialize_inerfaces();
};
//--------------------------------------
