/* RTX H.248 Media Gate. SIP Softphone
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
//--------------------------------------
// глобальные переменные
//--------------------------------------
extern char g_startup_path[MAX_PATH] = {0};
extern char g_log_root_path[MAX_PATH] = {0};
extern char g_rec_path[MAX_PATH] = { 0 };
extern bool g_in_device = true;
extern uint32_t g_wave_header_time = 20;
extern uint32_t g_wave_header_count = 3;
extern uint32_t g_jitter_depth = 6;
//extern bool g_stop_thread_after_device = false;

#if defined TARGET_OS_WINDOWS
//--------------------------------------
// dllmain.cpp : Defines the entry point for the DLL application.
//--------------------------------------
BOOL APIENTRY DllMain(HANDLE module, unsigned long reason, void* reserved)
{
	switch (reason)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}
#endif

#if defined (DEBUG_MEMORY)
	void* operator new (size_t size, char* file, int line)
	{
		return RC_Malloc(size, file, line, rc_malloc_new_e);
	}
	void* operator new[] (size_t size, char* file, int line)
	{
		return RC_Malloc(size, file, line, rc_malloc_newar_e);
	}
	void operator delete (void* mem_ptr)
	{
		RC_Free(mem_ptr, rc_malloc_delete_e);
	}
	void operator delete[] (void* mem_ptr)
	{
		RC_Free(mem_ptr, rc_malloc_deletear_e);
	}
#endif
//--------------------------------------
