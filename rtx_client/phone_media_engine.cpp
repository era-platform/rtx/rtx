/* RTX H.248 Media Gate. SIP Softphone
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtx_media_engine.h"
#include "phone_media_engine.h"
//-----------------------------------------------
//
//-----------------------------------------------
rtl::Mutex rtx_phone::m_sync;
h248::IMediaGate* rtx_phone::m_engine = nullptr;
rtx_phone rtx_phone::s_phone;
phone_context_t rtx_phone::context;
rtl::ArrayT<rtx_phone::IFACE_NAME> rtx_phone::s_ifaces;
//-----------------------------------------------
//
//-----------------------------------------------
void rtx_phone::initialize()
{
	rtl::MutexLock lock(m_sync);

	Config.set_config_value("workFolder", g_startup_path);

	m_engine = create_media_gateway_module(g_log_root_path);
	
	
	if (m_engine != nullptr)
	{
		m_engine->MediaGate_initialize(&s_phone);

		context.initialize();

		// ����� � �������� ������� ���� �����������
		initialize_inerfaces();
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
void rtx_phone::release()
{
	rtl::MutexLock lock(m_sync);

	if (m_engine != nullptr)
	{
		context.release();
		
		m_engine->MediaGate_destroy();

		remove_media_gateway();
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
h248::IMediaContext* rtx_phone::create_context()
{
	h248::IMediaContext* ctx = nullptr;

	rtl::MutexLock lock(m_sync);

	if (m_engine != nullptr)
	{
		ctx = m_engine->MediaGate_createContext();
	}

	return ctx;
}
//-----------------------------------------------
//
//-----------------------------------------------
void rtx_phone::destroy_context(h248::IMediaContext* ctx)
{
	if (ctx == nullptr)
		return;

	rtl::MutexLock lock(m_sync);

	if (m_engine != nullptr)
	{
		m_engine->MediaGate_destroyContext(ctx);
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
void rtx_phone::set_device_in(h248::IAudioDeviceInput* callback)
{
	if (callback == nullptr)
		return;

	LOG_CALL("phone-me", "set_device_in ...");

	rtl::MutexLock lock(m_sync);

	if (m_engine != nullptr)
	{
		m_engine->MediaGate_setDeviceIn(callback);
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
void rtx_phone::set_device_out(h248::IAudioDeviceOutput* callback)
{
	if (callback == nullptr)
		return;

	LOG_CALL("phone-me", "set_device_out ...");

	rtl::MutexLock lock(m_sync);

	if (m_engine != nullptr)
	{
		m_engine->MediaGate_setDeviceOut(callback);
	}
}
//-----------------------------------------------
// nothing to do
//-----------------------------------------------
rtx_phone::rtx_phone()
{
}
//-----------------------------------------------
//
//-----------------------------------------------
rtx_phone::~rtx_phone()
{
}
//-----------------------------------------------
//
//-----------------------------------------------
void rtx_phone::mge__event_raised()
{
}
//-----------------------------------------------
//
//-----------------------------------------------
void rtx_phone::mge__error_raised()
{
}
//-----------------------------------------------
//
//-----------------------------------------------
void rtx_phone::initialize_inerfaces()
{
	rtl::ArrayT<in_addr> addresses;
	
	net_t::get_interface_table(addresses);
	
	for (int i = 0; i < addresses.getCount(); i++)
	{
		in_addr addr = addresses[i];
		
		IFACE_NAME ifname = { addr, { 't', 'g', 'w', (char)i + '0', '\0' } };
		s_ifaces.add(ifname);

		m_engine->MediaGate_setInterfaceName(inet_ntoa(addr), ifname.name);

		m_engine->MediaGate_addPortRangeRTP(ifname.name, 40000, 10000);
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
const char* rtx_phone::get_interface_key(in_addr addr)
{
	for (int i = 0; i < s_ifaces.getCount(); i++)
	{
		if (s_ifaces[i].address.s_addr == addr.s_addr)
		{
			return s_ifaces[i].name;
		}
	}

	return nullptr;
}
//-----------------------------------------------
