/* RTX H.248 Media Gate. SIP Softphone
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_client.h"
#include "sip_account.h"
#include "sip_trunk.h"
//--------------------------------------
//
//--------------------------------------
sip_account_t::sip_account_t(const char* username, const char* domain) :
	m_sync("sip-account"), m_session(nullptr), m_reg_state(reg_state_t::unregistered),
	m_local_port(g_sip_client->get_default_listener_port())
{
	int l = std_snprintf(m_aor, 63, "%s@%s", username, domain);
	m_aor[l] = 0;

	LOG_CALL("account", "%s : new account created with URI <sip:%s@%s>", m_aor, username, domain);

	m_profile.set_username(username);
	m_profile.set_domain(domain);
	m_profile.set_expires(3600);
	m_profile.set_keep_alive_interval(0);

	m_audio_payload_set.addStandardFormat(PayloadId::PCMA);
	m_audio_payload_set.addStandardFormat(PayloadId::PCMU);

	m_refer_trunk = nullptr;

}
//--------------------------------------
//
//--------------------------------------
sip_account_t::~sip_account_t()
{
	destroy();
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::destroy()
{
	rtl::Timer::stopTimer(this, SIP_REREGISTER_TIMER_ID);

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (m_session != nullptr)
	{
		//m_session->set_event_handler(nullptr);
		m_session->send_unregister();
		m_session = nullptr;
	}


	if (m_trunk_list.getCount() > 0)
	{
		g_sip_client->remove_trunk_ptr(m_trunk_list.getBase(), m_trunk_list.getCount());

		for (int i = 0; i < m_trunk_list.getCount(); i++)
		{
			sip_trunk_t* trunk = m_trunk_list[i];
			if (trunk->get_call_state() != sip_state_new_e)
			{
				trunk->disconnect();
			}
			DELETEO(trunk);
		}

		m_trunk_list.clear();
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::set_local_port(uint16_t port)
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	LOG_CALL("account", "%s : set local port to %u", m_aor, port);

	m_profile.set_interface_port(m_local_port = port);
	m_profile.set_listener_port(m_local_port);

	g_sip_client->add_transport(sip_transport_udp_e, m_profile.get_interface(), m_local_port);

	if (m_reg_state == reg_state_t::registered)
	{
		LOG_CALL("account", "%s : set_local_port -> start_registration()", m_aor);
		start_registration();
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::set_proxy_address(const char* address, uint16_t port, sip_transport_type_t type)
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	LOG_CALL("account", "%s : set proxy address to %s:%u %s...", m_aor, address, port, TransportType_toString(type));

	m_profile.set_proxy(address);
	// ����� ����������� ����� ������
	in_addr iface = { INADDR_ANY };
	in_addr proxy_address = net_t::get_host_by_name(m_profile.get_proxy());

	if (proxy_address.s_addr == INADDR_NONE)
		proxy_address.s_addr = INADDR_ANY;

	if (proxy_address.s_addr == INADDR_ANY)
	{
		net_t::get_best_interface(m_profile.get_proxy(), proxy_address, iface);

		m_profile.set_interface(iface);
		m_profile.set_interface_port(m_local_port);

		m_profile.set_listener(iface);
		m_profile.set_listener_port(m_local_port);
	}

	m_profile.set_proxy_address(proxy_address);
	m_profile.set_proxy_port(port);
	m_profile.set_transport(type);

	// ��������� ������������� ���������
	short ll = (short)proxy_address.s_addr;
	short rl = (short)iface.s_addr;

	if (ll != rl || m_profile.get_keep_alive_interval() == 0)
	{
		// ��������
		m_profile.set_keep_alive_interval(10);
	}

	if (m_reg_state == reg_state_t::registered)
	{
		LOG_CALL("account", "%s : set_proxy_address() -> start_registration()", m_aor);
		start_registration();
	}

	char net_buf[64];
	LOG_CALL("account", "%s : set proxy done : selected iface %s:%u...", m_aor, net_to_string(net_buf, 64, m_profile.get_interface()), m_profile.get_interface_port());
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::set_keepalive(int keepalive_interval)
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	LOG_CALL("account", "%s : set keepalive %d", m_aor, keepalive_interval);

	m_profile.set_keep_alive_interval(keepalive_interval);

	// ��������
	if (keepalive_interval > 0)
	{
		if (m_session != nullptr)
		{
			m_session->set_nat_keep_alive(keepalive_interval);
		}
	}
	else
	{
		in_addr proxy_address = m_profile.get_proxy_address();
		in_addr iface = m_profile.get_interface();

		// ��������� ������������� ���������
		short ll = (short)proxy_address.s_addr;
		short rl = (short)iface.s_addr;
		if (ll != rl)
		{
			// ��������
			m_profile.set_keep_alive_interval(10);

			if (m_session != nullptr)
			{
				m_session->set_nat_keep_alive(10);
			}
		}
		else
		{
			// ���������
			m_profile.set_keep_alive_interval(keepalive_interval);

			if (m_session != nullptr)
			{
				m_session->set_nat_keep_alive(keepalive_interval);
			}
		}
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::set_authentication(const char* auth_id, const char* auth_password)
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	LOG_CALL("account", "%s : set authentication to '%s'", m_aor, auth_id);

	m_profile.set_auth_id(auth_id);
	m_profile.set_auth_pwd(auth_password);

	if (m_reg_state == reg_state_t::registered)
	{
		LOG_CALL("account", "%s : set_authentication -> start_registration()", m_aor);

		start_registration();
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::set_displayname(const char* displayname)
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	LOG_CALL("account", "%s : set displayname to '%s'", m_aor, displayname);

	m_profile.set_display_name(displayname);

	if (m_reg_state == reg_state_t::registered)
	{
		LOG_CALL("account", "%s : set_displayname -> start_registration()", m_aor);

		start_registration();
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::set_expires(int expires)
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (expires != m_profile.get_expires())
	{
		LOG_CALL("account", "%s : set new expires value to %d seconds", m_aor, expires);
		
		m_profile.set_expires(expires);

		if (m_reg_state == reg_state_t::registered)
		{
			LOG_CALL("account", "%s : set_expires -> start_registration()", m_aor);

			start_registration();
		}
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::set_audio_set(const PayloadSet& payloads)
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	m_audio_payload_set = payloads;
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::get_audio_set(PayloadSet& payloads)
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	payloads = m_audio_payload_set;
}
//--------------------------------------
//
//--------------------------------------
//void sip_account_t::set_video_payload_set(const char* payload_set[], int count)
//{
//	char fmt_name[32];
//
//	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);
//
//	char payloads[256];
//	int len = 0;
//
//	for (int i = 0; i < count; i++)
//	{
//		len += std_snprintf(payloads + len, 256 - len, L"%s ", payload_set[i]);
//	}
//
//	payloads[len] = 0;
//
//	LOG_CALL("account", "%s : set video payload set to %s", m_username, payloads);
//
//	m_video_payload_set.remove_all();
//
//	for (int i = 0; i < count; i++)
//	{
//		hal_wcstombcs(fmt_name, 32, payload_set[i], -1);
//		const PayloadFormat& format = PayloadSet::video_set().get_format(fmt_name);
//
//		m_video_payload_set.add_format(format);
//	}
//}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::start_registration()
{
	raise_event(SIP_ASYNC_SEND_REGISTER_EVENT);
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::start_unregistration()
{
	raise_event(SIP_ASYNC_SEND_UNREGISTER_EVENT);
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::send_text_message(const char* to_username, const char* to_displayname, const char* content_type, const char* message_text)
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	LOG_CALL("stream", "%s : send MESSAGE request to %s...", m_aor, to_username);

	/// Create the start line
	sip_uri_t requestURI;
	sip_message_t request;

	/// Create the Request uri and To uri
	/// To and from for registrations are identical

	const char* to_username_a;
	const char* to_displayname_a;
	const char* content_type_a;

	if (to_username == nullptr || to_username[0] == 0)
	{
		to_username_a = m_profile.get_username();
	}
	else
	{
		to_username_a = to_username;
	}

	if (to_displayname == nullptr || to_displayname[0] == 0)
	{
		to_displayname_a = m_profile.get_display_name();
	}
	else
	{
		to_displayname_a = to_displayname;
	}

	requestURI.set_user(to_username);
	requestURI.set_host(m_profile.get_domain());

	request.set_request_line(sip_MESSAGE_e, requestURI);

	/// Create the via
	sip_value_via_t* via = request.make_Via_value();

	via->set_address(inet_ntoa(m_profile.get_listener()));
	via->set_port(m_profile.get_listener_port());
	via->set_protocol(m_profile.get_transport());
	via->set_branch(sip_utils::GenBranchParameter());

	/// Create the from uri
	rtl::String domain = m_profile.get_domain();
	sip_value_user_t* from = request.make_From_value();
	sip_uri_t fromURI;

	fromURI.set_user(m_profile.get_username());
	fromURI.set_host(domain);

	from->set_display_name(m_profile.get_display_name());
	from->set_uri(fromURI);

	/// always add a NEW tag
	rtl::String tag = sip_utils::GenTagParameter();
	from->set_tag(tag);
	
	sip_value_uri_t* to = request.make_To_value();
	sip_uri_t toURI;
	to->set_display_name(to_displayname_a);
	to->set_uri(requestURI);

	/// Create the CSeq
	request.CSeq(1, "MESSAGE");

	/// set the call-id
	request.CallId(sip_utils::GenCallId(inet_ntoa(m_profile.get_interface())));

	request.make_Max_Forwards_value()->set_number(SIP_DEFAULT_MAX_FORWARDS);


	/// Set the content 
	if (content_type == nullptr || content_type[0] == 0)
	{
		content_type_a = "text/plain";
	}
	else
	{
		content_type_a = content_type;
	}

	request.make_Content_Type()->assign(content_type_a);

	request.set_body(message_text);

	sip_transport_route_t route = {
		m_profile.get_transport(),
		m_profile.get_interface(), m_profile.get_interface_port(),
		m_profile.get_proxy_address(), m_profile.get_proxy_port() };

	request.set_route(route);

	g_sip_client->get_sip_stack().send_message_to_transport(request);
}
//--------------------------------------
//
//--------------------------------------
bool sip_account_t::check_request(in_addr remoteAddress, uint16_t remotePort, const char* username, const char* domain)
{
	if (m_profile.get_proxy_address().s_addr == remoteAddress.s_addr && m_profile.get_proxy_port() == remotePort && rtl::strcmp(m_profile.get_username(), username) == 0)
		return true;

	return false;
}
//--------------------------------------
//
//--------------------------------------
call_session_event_handler_t* sip_account_t::process_incoming_call(const sip_message_t& invite, sip_call_session_t& call_session)
{
	const sip_value_uri_t* from = invite.get_From_value();
	const sip_value_uri_t* to = invite.get_To_value();

	const rtl::String& caller_name = from->get_display_name();
	const rtl::String& caller_id = from->get_uri().get_user();
	const rtl::String& called_name = to->get_display_name();
	const rtl::String& called_id = to->get_uri().get_user();

	LOG_CALL("stream", "%s : process incoming call from %s(%s) to %s(%s)...", m_aor, (const char*)caller_id, (const char*)caller_name, (const char*)called_id, (const char*)called_name);

	sip_trunk_t* trunk = create_trunk();

	if (!trunk->process_incoming_call(call_session, invite))
	{
		remove_trunk(trunk);
		return nullptr;
	}

	g_sip_client->add_trunk_ptr(trunk);

	// refer processing
	const sip_value_t* replaces = invite.get_Replaces_value();
	const sip_value_uri_t* referedBy = invite.get_Referred_By_value();

	sip_trunk_t* trunk_old = find_invite_replaces(referedBy, replaces);

	if (trunk_old != nullptr)
	{
		LOG_CALL("stream", "%s : process incoming call with replace", m_aor);

		trunk->answer(false);

		trunk_old->disconnect();

		g_sip_client->raise_call_replaced_by_refer(trunk_old, trunk, caller_id, caller_name, called_id, called_name);
	}
	else
	{
		LOG_CALL("stream", "%s : process incoming call normal", m_aor);

		g_sip_client->raise_account_incoming_call(this, trunk, caller_id, caller_name, called_id, called_name/*, invite_str*/);
	}



	LOG_CALL("stream", "%s : process incoming call done", m_aor);
	
	return trunk;
}
//--------------------------------------
//
//--------------------------------------
bool sip_account_t::process_incoming_im_message(const sip_message_t& message)
{
	const sip_value_uri_t* from = message.get_From_value();
	const sip_value_uri_t* to = message.get_To_value();

	const rtl::String& caller_name = from->get_display_name();
	const rtl::String& caller_id = from->get_uri().get_user();
	const rtl::String& called_name = to->get_display_name();
	const rtl::String& called_id = to->get_uri().get_user();

	LOG_CALL("stream", "%s : process incoming im message from %s(%s) to %s(%s)...", m_aor, (const char*)caller_id, (const char*)caller_name, (const char*)called_id, (const char*)called_name);

	rtl::String text = message.to_string();

	if (!text.isEmpty())
	{
		g_sip_client->raise_account_named_event(this, "streamtextmessage", text);
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
sip_trunk_t* sip_account_t::create_trunk()
{
	LOG_CALL("stream", "%s : create new trunk", m_aor);

	sip_trunk_t* trunk = nullptr;

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (m_reg_state == reg_state_t::registered)
	{
		trunk = NEW sip_trunk_t(*this);
		m_trunk_list.add(trunk);
	}

	return trunk;
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::remove_trunk(sip_trunk_t* trunk)
{
	LOG_CALL("stream", "%s : remove trunk", m_aor);

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	for (int i = 0; i < m_trunk_list.getCount(); i++)
	{
		if (trunk == m_trunk_list[i])
		{
			m_trunk_list.removeAt(i);
			trunk->disconnect();
			DELETEO(trunk);
			break;
		}
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::refer_to(sip_trunk_t* referrer, const char* number, sip_trunk_t* target)
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (m_refer_trunk != nullptr)
	{
		LOG_WARN("account", "%s : refer to error : already in refer state", m_aor);
		return;
	}

	m_refer_trunk = referrer;

	m_refer_to.cleanup();

	sip_uri_t& uri = m_refer_to.get_uri();

	uri.set_scheme("sip");
	uri.set_user(number);
	uri.set_host(m_profile.get_domain());

	rtl::String uristr = uri.to_string();

	LOG_CALL("trunk", "id:%p : refer to '%s'", this, (const char*)uristr);

	if (target != nullptr)
	{
		target->get_remote_uri(uri);
		rtl::String replaces = target->get_session()->get_call_id();
		replaces += ";to-tag=";
		replaces += target->get_session()->GetRemoteTag();
		replaces += ";from-tag=";
		replaces += target->get_session()->GetLocalTag();

		//rtl::String rep_enc = escape_rfc3986(replaces);

		uri.get_headers().add("Replaces", replaces);
	}

	referrer->refer_to(m_refer_to);
}
//--------------------------------------
// ��� ��������� �������� ����������� � ������ �� �����
//--------------------------------------
void sip_account_t::timer_elapsed_event(rtl::Timer* sender, int tid)
{
	if (tid == SIP_REREGISTER_TIMER_ID)
		raise_event(SIP_ASYNC_SEND_REGISTER_EVENT);
	else if (tid == SIP_REFER_TIMER_ID)
		raise_event(SIP_ASYNC_TRUNK_REFER_ANSWERED, sip_408_RequestTimeout);
}
//--------------------------------------
//
//--------------------------------------
const char* sip_account_t::async_get_name()
{
	return m_aor;
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::async_handle_call(uint16_t call_id, uintptr_t int_param, void* ptr_param)
{
	switch (call_id)
	{
	case SIP_ASYNC_SEND_REGISTER_EVENT:
		event_send_register();
		break;
	case SIP_ASYNC_SEND_UNREGISTER_EVENT:
		event_send_unregister();
		break;
	case SIP_ASYNC_BACK_REGISTERED_EVENT:
		event_async_registered((sip_transport_route_t*)ptr_param);
		break;
	case SIP_ASYNC_BACK_UNREGISTERED_EVENT:
		event_async_unregistered((sip_reg_session_client_t*)ptr_param);
		break;
	case SIP_ASYNC_BACK_TIMEOUT_EVENT:
		event_async_timeout((sip_reg_session_client_t*)ptr_param);
		break;
	case SIP_ASYNC_BACK_TRANSP_FAILURE_EVENT:
		event_async_transport_failure((sip_reg_session_client_t*)ptr_param);
		break;
	case SIP_ASYNC_BACK_DESTROYED_EVENT:
		event_async_session_destroyed((sip_reg_session_client_t*)ptr_param);
		break;
	case SIP_ASYNC_TRUNK_REFER_ANSWERED:
		event_async_refer_answered((int)int_param);
		break;
	case SIP_ASYNC_TRUNK_REFER_NOTIFIED:
		event_async_refer_notified((int)int_param);
		break;
	case SIP_ASYNC_TRUNK_REFER_INCOME:
		event_async_refer_refer_income();
		break;
	default:
		LOG_CALL("account", "%s : async...unknown call %d %d %p", m_aor, call_id, int_param, ptr_param);
	}
	
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::raise_event(uint16_t call_id, uintptr_t int_param, void* ptr_param)
{
	rtl::async_call_manager_t::callAsync(this, call_id, int_param, ptr_param);
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::event_async_registered(sip_transport_route_t* route)
{
	LOG_EVENT("account", "%s : REGISTRATION ACCEPTED EVENT RAISED", m_aor);

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	m_reg_state = reg_state_t::registered;

	m_profile.set_interface(route->if_address);
	m_profile.set_interface_port(route->if_port);

	m_profile.set_listener(route->if_address);
	m_profile.set_listener_port(route->if_port);

	g_sip_client->raise_account_registered(this);

	DELETEO(route);
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::event_async_unregistered(sip_reg_session_client_t* session)
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (session != m_session && m_session != nullptr)
	{
		LOG_EVENT("account", "%s : UNREGISTRATION EVENT RAISED", m_aor);
		m_reg_state = reg_state_t::unregistered;
		g_sip_client->raise_account_unregistered(this, false);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::event_async_timeout(sip_reg_session_client_t* session)
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (session == m_session && m_session != nullptr)
	{
		LOG_EVENT("account", "%s : registration timeout", m_aor);
		m_reg_state = reg_state_t::unregistered;
		g_sip_client->raise_account_unregistered(this, false);
		rtl::Timer::setTimer(this, SIP_REREGISTER_TIMER_ID, SIP_REREGISTER_TIME, false);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::event_async_session_destroyed(sip_reg_session_client_t* session)
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (session == m_session && m_session != nullptr)
	{
		LOG_CALL("account", "%s : registeration session destroyed!", m_aor);
		m_session = nullptr;
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::event_async_transport_failure(sip_reg_session_client_t* session)
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (m_session != nullptr && session == m_session)
	{
		LOG_CALL("account", "%s : registration transport failure %d", m_aor, sip_503_ServiceUnavailable);
		m_reg_state = reg_state_t::unregistered;
		g_sip_client->raise_account_unregistered(this, false);
		rtl::Timer::setTimer(this, SIP_REREGISTER_TIMER_ID, SIP_REREGISTER_TIME, false);
	}
	else
	{ 
		LOG_WARN("account", "%s : registration transport failure for unknown session %p", m_aor, session);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::event_async_refer_answered(int code)
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);
	g_sip_client->raise_call_refer_notified(m_refer_trunk, ReferNotify_Answer, m_refer_message);
	
	if (m_refer_code >= sip_300_MultipleChoices)
	{
		m_refer_trunk = nullptr;
		m_refer_to.cleanup();
		m_referred_by.cleanup();
	}

	m_refer_code = sip_000;
	m_refer_message.setEmpty();
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::event_async_refer_notified(int code)
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);
	g_sip_client->raise_call_refer_notified(m_refer_trunk, ReferNotify_Notify, m_refer_message);

	if (m_refer_code < sip_200_OK)
	{
		m_refer_code = sip_000;
		m_refer_message.setEmpty();
	}
	else if (m_refer_code == sip_200_OK)
	{
		LOG_CALL("account", "............disconnect.............");
		// terminated
		m_refer_trunk->disconnect();
		m_refer_trunk = nullptr;
		m_refer_to.cleanup();
		m_referred_by.cleanup();
	}
	else
	{
		m_refer_trunk->hold(false);
		m_refer_trunk = nullptr;
		m_refer_to.cleanup();
		m_referred_by.cleanup();
	}

	m_refer_code = sip_000;
	m_refer_message.setEmpty();
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::event_async_refer_refer_income()
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);
	g_sip_client->raise_call_refer_notified(m_refer_trunk, ReferNotify_Income, m_refer_message);
	
	// making new trunk and call to it!
	if ((m_target_trunk = create_trunk()) == nullptr)
	{
		LOG_ERROR("account", "event_async_refer_refer_income() -> @#*#&^#%@&@*");
		return;
	}

	g_sip_client->add_trunk_ptr(m_target_trunk);

	bool is_rby_empty = m_referred_by.get_uri().get_host().isEmpty();

	m_target_trunk->call_to_target(m_refer_to, is_rby_empty ? nullptr : &m_referred_by);

	m_refer_message.setEmpty();
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::event_send_register()
{
	try
	{
		rtl::Timer::stopTimer(this, SIP_REREGISTER_TIMER_ID);

		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		if (m_reg_state == reg_state_t::registering)
		{
			LOG_WARN("account", "ALREADY REGISTERING!");
			return;
		}

		m_reg_state = reg_state_t::registering;

		LOG_CALL("account", "%s : send register", m_aor);

		const char* proxy = m_profile.get_proxy();

		if (proxy == nullptr || proxy[0] == 0)
		{
			uint16_t port = 5060;
			const rtl::String& domain = m_profile.get_domain();
			rtl::String proxy_str = domain;

			int index = domain.indexOf(L':');

			if (index != -1)
			{
				port = (uint16_t)strtoul(domain.substring(index + 1), nullptr, 10);
				proxy_str = domain.substring(0, index);
			}

			set_proxy_address(proxy_str, port, sip_transport_udp_e);
		}

		LOG_CALL("account", "%s : start registration on %s over %s:%u %s experes after %d seconds", m_aor, (const char*)m_profile.get_domain(), (const char*)m_profile.get_proxy(), m_profile.get_proxy_port(), TransportType_toString(m_profile.get_transport()), m_profile.get_expires());

		sip_reg_session_client_t* regSession = nullptr;

		if (m_session != nullptr)
		{
			/// destroy the old session if it exists
			m_session->destroy_session();
			m_session = nullptr;
		}

		rtl::String callId = sip_utils::GenCallId(inet_ntoa(m_profile.get_interface()));
		in_addr iface = { INADDR_ANY };

		m_profile.set_interface(iface, m_local_port);
		m_profile.set_listener(iface, m_local_port);

		m_session = (sip_reg_session_client_t*)g_sip_client->get_reg_manager()->create_client_session(m_profile, callId);

		if (m_session != nullptr)
		{
			m_session->send_register(this);
		}
		else
		{
			LOG_ERROR("account", "%s : creating registartion session failed", m_aor);
		}
	}
	catch (rtl::Exception& ex)
	{
		LOG_ERROR("account", "%s : start registration failed with exception : %s", m_aor, ex.get_message());
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::event_send_unregister()
{
	LOG_CALL("stream", "%s : remove registration", m_aor);

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (m_session != nullptr && m_reg_state == reg_state_t::registered)
	{
		m_reg_state = reg_state_t::unregistering;

		m_session->send_unregister();
	}
	else
	{
		LOG_WARN("stream", "%s : session already NOT registered!", m_aor);
	}
}
//--------------------------------------
//
//--------------------------------------
sip_trunk_t* sip_account_t::find_invite_replaces(const sip_value_uri_t* referredBy, const sip_value_t* replaces)
{
	LOG_CALL("stream", "%s : find_invite_replaces(referredBy:%p, Replaces:%p)", m_aor, referredBy, replaces);

	// if has no replaces do nothing
	if (replaces == nullptr)
	{
		LOG_CALL("stream", "%s : find_invite_replaces() => no Replaces", m_aor);
		return nullptr;
	}

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	rtl::String call_id = replaces->value();
	rtl::String to_tag = replaces->param_get("to-tag");
	rtl::String from_tag = replaces->param_get("from-tag");

	LOG_CALL("stream", "%s : find_invite_replaces('%s' | '%s' | '%s')", m_aor, (const char*)call_id, (const char*)to_tag, (const char*)from_tag);

	for (int i = 0; i < m_trunk_list.getCount(); i++)
	{
		sip_trunk_t* trunk = m_trunk_list.getAt(i);

		if (trunk->check_dialog(call_id, to_tag, from_tag))
		{
			return trunk;
		}
	}

	LOG_CALL("stream", "%s : find_invite_replaces() => Dialog not found", m_aor);

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
const char* get_sip_dtmf_type_name(sip_dtmf_type_t type)
{
	static const char* names[] = { "rfc2833", "sip-info", "inband" };

	return type >= 0 && type < (sizeof(names) / sizeof(const char*)) ? names[type] : "undefined";
}
//--------------------------------------
