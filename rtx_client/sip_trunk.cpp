/* RTX H.248 Media Gate. SIP Softphone
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_account.h"
#include "sip_trunk.h"
#include "phone_media_engine.h"
//--------------------------------------
//
//--------------------------------------
const char* get_sip_trunk_state_name(sip_trunk_state_t state)
{
	static const char* names[] =
	{
		"new",
		"call",
		"ring",
		"progress",
		"connected",
		"in_call",	
		"holding",
		"holded",
		"disconnected",
	};

	return state >= sip_state_new_e && state <= sip_state_disconnected_e ? names[state] : "unknown";
}
//--------------------------------------
//
//--------------------------------------
sip_trunk_t::sip_trunk_t(sip_account_t& account) :
	m_sync("sip-trunk"),
	m_account(account),
	m_call_state(sip_state_new_e),
	m_hold_state(false),
	m_mute_state(false),
	m_call_end_reason(DisconnectUnknown),
	m_call_session(nullptr),
	m_media_session(nullptr)
{
	m_reject_code = sip_480_TemporarilyUnavailable;
	m_reject_reason_phrase = rtl::String::empty;
}
//--------------------------------------
//
//--------------------------------------
sip_trunk_t::~sip_trunk_t()
{
	destroy();
}
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::destroy()
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (m_call_state != sip_state_new_e)
	{
		disconnect();
	}
}
//--------------------------------------
//
//--------------------------------------
bool sip_trunk_t::process_incoming_call(sip_call_session_t& session, const sip_message_t& invite)
{
	LOG_EVENT("trunk", "id:%p : process incoming call event call-id:%s", this, (const char*)invite.CallId());

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	m_call_state = sip_state_calling_e;
	m_call_session = &session;
	m_incoming_invite = invite;
	m_profile = m_account.get_profile();
	session.set_profile(m_profile);

	LOG_CALL("trunk", "id:%p : process incoming call : done", this);

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool sip_trunk_t::call_to(const char* number, const char* caller_id, const char* caller_name)
{
	LOG_CALL("trunk", "id:%p : call to '%s' as '%s' (%s)", this, number, caller_id, caller_name);

	if (m_call_state != sip_state_new_e)
		return false;

	if (number == nullptr || number[0] == 0)
		return false;

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	m_profile = m_account.get_profile();

	m_call_end_reason = DisconnectUnknown;

	m_profile.set_to_user(number);
	m_profile.set_request_user(number);

	if (caller_id != nullptr && caller_id[0] != 0)
	{
		m_profile.set_display_name(caller_id);
	}

	m_call_state = sip_state_calling_e;

	rtl::String callId = sip_utils::GenCallId(inet_ntoa(m_profile.get_interface()));

	m_call_session = (sip_call_session_t*)g_sip_client->get_call_manager()->create_client_session(m_profile, callId);

	if (m_call_session == nullptr)
	{
		LOG_WARN("trunk", "id:%p : call to : failed -> null session", this);
		return false;
	}

	if (!m_call_session->make_call(this))
	{
		LOG_WARN("trunk", "id:%p : call to : failed", this);
		m_call_session->StartAutoDestructTimer(32);
		m_call_session = nullptr;

		return false;
	}

	LOG_CALL("trunk", "id:%p : call to : done", this);

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool sip_trunk_t::call_to_target(const sip_value_uri_t& referTo, const sip_value_uri_t* referredBy)
{
	LOG_CALL("trunk", "id:%p : call to target '%s' referred by '%s'", this, (const char*)referTo.to_string(),
		referredBy ? (const char*)referredBy->to_string() : "(null)");

	if (m_call_state != sip_state_new_e)
		return false;

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	m_profile = m_account.get_profile();
	m_call_end_reason = DisconnectUnknown;
	m_call_state = sip_state_calling_e;

	rtl::String callId = sip_utils::GenCallId(inet_ntoa(m_profile.get_interface()));

	m_call_session = (sip_call_session_t*)g_sip_client->get_call_manager()->create_client_session(m_profile, callId);

	if (m_call_session == nullptr)
	{
		LOG_WARN("trunk", "id:%p : call to : failed -> null session", this);
		return false;
	}

	if (!m_call_session->make_call_to_target(this, referTo, referredBy))
	{
		LOG_WARN("trunk", "id:%p : call to : failed", this);
		m_call_session->StartAutoDestructTimer(32);
		m_call_session = nullptr;

		return false;
	}

	LOG_CALL("trunk", "id:%p : call to : done", this);

	return true;
}
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::disconnect()
{
	LOG_CALL("trunk", "id:%p : disconnect call : state %s...", this, get_sip_trunk_state_name(m_call_state));

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__); // ���������� ��� ������ ��������� ������

	if (m_call_state != sip_state_new_e)
	{
		process_call_disconnected_event(true);
	}
	else
	{
		g_sip_client->raise_call_disconnected(this, DisconnectReason::DisconnectCancelTX);
	}

	LOG_CALL("trunk", "id:%p : disconnect call : done", this);
}
//--------------------------------------
//
//--------------------------------------
bool sip_trunk_t::answer(bool ringing)
{
	LOG_CALL("trunk", "id:%p : answer call %s in state %s...", this, ringing ? L"ring" : L"connect", get_sip_trunk_state_name(m_call_state));

	bool result = false;

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if ((m_call_state >= sip_state_calling_e && m_call_state < sip_state_connecting_e) && m_call_session != nullptr)
	{
		m_call_state = ringing ? sip_state_ringing_e :sip_state_connecting_e;
		result = m_call_session->AnswerCall(ringing ? sip_call_session_t::AnswerCallDeferred : sip_call_session_t::AnswerCallNow) != FALSE;

		LOG_CALL("trunk", "id:%p : call answered", this);
	}
	else
	{
		LOG_ERROR("trunk", "id:%p : answer call failed : invalid state %s", this, get_sip_trunk_state_name(m_call_state));
	}

	return result;
}
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::reject(uint16_t code, const char* reason)
{
	LOG_CALL("trunk", "id:%p : reject call code:%u reason:%s in state %s...", this, code, reason, get_sip_trunk_state_name(m_call_state));

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	m_reject_code = code;
	m_reject_reason_phrase = reason;

	disconnect();

	LOG_CALL("trunk", "id:%p : call rejected", this);
}
//--------------------------------------
//
//--------------------------------------
rtl::String sip_trunk_t::get_sip_header(const char* sip_header_name, int index)
{
	LOG_CALL("trunk", "id:%p : get call header name:%s index:%d in state %s...", this, sip_header_name, index, get_sip_trunk_state_name(m_call_state));
	rtl::String result;

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	rtl::String name = sip_header_name;
	sip_header_type_t htype = sip_parse_header_type(name);
	const sip_header_t* hdr = (htype == sip_custom_header_e) ?
		m_incoming_invite.get_custom_header(name) :
		m_incoming_invite.get_std_header(htype);

	if (hdr != nullptr && index < hdr->get_value_count())
	{
		result = hdr->get_value_at(index)->to_string();
	}

	LOG_CALL("trunk", "id:%p : get call header value '%s'", this, !result.isEmpty() ? (const char*)result : "empty");

	return result;
}
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::hold(bool state)
{
	LOG_CALL("trunk", "id:%p : hold call (%s): holded:%s", this, STR_BOOL(state), STR_BOOL(m_hold_state));

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (m_hold_state == state)
	{
		LOG_WARN("trunk", "id:%p : hold ignored : call already %s!", this, m_hold_state ? "holded" : "free");
		return;
	}

	if (m_call_state != sip_state_connected_e)
	{
		LOG_WARN("trunk", "id:%p : hold ignored : invalid state %s", this, get_sip_trunk_state_name(m_call_state));
		return;
	}
	
	if (m_call_session == nullptr)
	{
		LOG_WARN("trunk", "id:%p : hold ignored : session is null", this);
		return;
	}

	// state(true) + mute(true) -> do nothing
	// state(false) + mute(true) -> do nothing

	// state(true) + mute(false) -> mute(true)
	// state(false) + mute(false) -> mute(false)
	if (!m_mute_state)
		m_media_session->mute(state);

	if (m_call_session->SendHold(m_hold_state = state))
	{
		LOG_CALL("trunk", "id:%p : hold call : done", this);
	}
	else
	{
		LOG_WARN("trunk", "id:%p : hold failed : sip_call_session_t::SendHold(true) return error", this);
	}
}
//--------------------------------------
//
//--------------------------------------
//void sip_trunk_t::unhold()
//{
//	LOG_CALL("trunk", "id:%p : unhold call : holded:%s", this, STR_BOOL(m_hold_state));
//
//	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);
//
//	if (!m_hold_state)
//	{
//		LOG_WARN("trunk", "id:%p : unhold ignored : call already unholded!", this);
//		return;
//	}
//
//	if (m_call_state != sip_state_connected_e)
//	{
//		LOG_WARN("trunk", "id:%p : unhold ignored : invalid state %s", this, get_sip_trunk_state_name(m_call_state));
//		return;
//	}
//	
//	if (m_call_session == nullptr)
//	{
//		LOG_WARN("trunk", "id:%p : unhold ignored : session is null", this);
//		return;
//	}
//
//	m_media_session->mute(false);
//
//	if (m_call_session->SendHold(m_hold_state = false))
//	{
//		LOG_CALL("trunk", "id:%p : unhold call : done", this);
//	}
//	else
//	{
//		LOG_WARN("trunk", "id:%p : unhold failed : sip_call_session_t::SendHold(false) return error", this);
//	}
//}
//--------------------------------------
//
//--------------------------------------
static void encode_uri(rtl::String& value)
{
	int start = 0, index;

	while ((index = index = value.indexOfAny("@;=", start)) != BAD_INDEX)
	{
		char ch = value[index];
		const char* str = "";

		switch (ch)
		{
		case '@': str = "%40"; break;
		case ';': str = "%3B"; break;
		case '=': str = "%3D"; break;
		}

		value.remove(index, 1);
		value.insert(index, str);
		start = index + 3;
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::refer_to(const sip_value_uri_t& referTo)
{
	LOG_CALL("trunk", "id:%p : refer...", this);

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (m_call_state != sip_state_connected_e)
	{
		LOG_WARN("trunk", "id:%p : refer to ignored : invalid state %s", this, get_sip_trunk_state_name(m_call_state));
		return;
	}
	
	if (m_call_session == nullptr)
	{
		LOG_WARN("trunk", "id:%p : refer to ignored : session is null", this);
		return;
	}

	if (m_call_session->SendRefer(referTo))
	{
		LOG_CALL("trunk", "id:%p : refer to : done", this);
	}
	else
	{
		LOG_WARN("trunk", "id:%p : refer to failed : sip_call_session_t::SendReferTo() return error", this);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::mute(bool state)
{
	LOG_CALL("trunk", "id:%p : mute call (%s): muted:%s", this, STR_BOOL(state), STR_BOOL(m_mute_state));

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (m_mute_state == state)
	{
		LOG_WARN("trunk", "id:%p : mute ignored : call already is %s", this, m_mute_state ? "muted" : "free");
		return;
	}

	if (m_call_state != sip_state_connected_e)
	{
		LOG_WARN("trunk", "id:%p : mute ignored : invalid state %s", this, get_sip_trunk_state_name(m_call_state));
		return;
	}

	if (m_call_session == nullptr || m_media_session == nullptr)
	{
		LOG_WARN("trunk", "id:%p : mute ignored : sessions is null", this);
		return;
	}

	m_media_session->mute(m_mute_state = state);

	LOG_CALL("trunk", "id:%p : mute call : done", this);
}
//--------------------------------------
//
//--------------------------------------
//void sip_trunk_t::unmute()
//{
//	LOG_CALL("trunk", "id:%p : unmute call : muted:%s", this, STR_BOOL(m_mute_state));
//
//	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);
//
//	if (!m_mute_state)
//	{
//		LOG_WARN("trunk", "id:%p : unmute ignored : call already unmuted", this);
//		return;
//	}
//
//	if (m_call_state != sip_state_connected_e)
//	{
//		LOG_WARN("trunk", "id:%p : unmute ignored : invalid state %s", this, get_sip_trunk_state_name(m_call_state));
//		return;
//	}
//
//	if (m_call_session == nullptr || m_media_session == nullptr)
//	{
//		LOG_WARN("trunk", "id:%p : unmute ignored : sessions is null", this);
//		return;
//	}
//
//	m_media_session->mute(m_mute_state = false);
//
//	LOG_CALL("trunk", "id:%p : unmute call : done", this);
//}
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::send_dtmf(char tone)
{
	sip_dtmf_type_t dtmfType = m_account.get_dtmf_type();
	int duration = 180;

	LOG_CALL("trunk", "id:%p : send DTMF tone %c over %s...", this, tone, get_sip_dtmf_type_name(dtmfType));

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	switch (dtmfType)
	{
	case sip_dtmf_inband_e:
		//if (m_dtmf_out != nullptr)
		//{
		//	m_dtmf_out->SendTone(tone, duration);
		//}
		//break;
	case sip_dtmf_rfc2833_e:
		if (m_media_session != nullptr)
		{
			m_media_session->send_dtmf(tone, duration);
		}
		else
		{
			LOG_WARN("trunk", "id:%p : send dtmf ignored : media session is null", this);
		}
		break;
	case sip_dtmf_info_e:
		if (m_call_session != nullptr)
		{
			m_call_session->SendDTMF(tone, duration);
		}
		else
		{
			LOG_WARN("trunk", "id:%p : send dtmf ignored : sip session is null", this);
		}
		break;
	}

	LOG_CALL("trunk", "id:%p : send DTMF done", this); 
}
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::start_playing(const char* file_path, bool in_cycle)
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (m_hold_state)
	{
		LOG_CALL("trunk", "id:%p : start playing...");

		if (m_media_session != nullptr && m_media_session->start_player(file_path, in_cycle))
		{
			LOG_CALL("trunk", "id:%p : start playing : done");
		}
		else
		{
			LOG_WARN("trunk", "id:%p : start play failed : player or session error (%p)", m_media_session);
		}
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::stop_playing()
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (m_media_session != nullptr)
	{
		m_media_session->stop_player();
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::start_recording(const char* file_path)
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	LOG_CALL("trunk", "id:%p : start recording...");

	if (m_media_session != nullptr && m_media_session->start_recording(file_path))
	{
		LOG_CALL("trunk", "id:%p : start recording done");
	}
	else
	{
		LOG_WARN("trunk", "id:%p : start recording failed : recorder or session error (%p)", m_media_session);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::stop_recording()
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (m_media_session != nullptr)
	{
		m_media_session->stop_recording();
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::get_remote_uri(sip_uri_t& uri) const
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (m_call_session != nullptr)
	{
		uri = m_call_session->GetRemoteURI();
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::get_local_uri(sip_uri_t& uri) const
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (m_call_session != nullptr)
	{
		uri = m_call_session->GetLocalURI();
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
bool sip_trunk_t::check_dialog(const rtl::String& callid, const rtl::String& totag, const rtl::String& fromtag)
{
	if (m_call_session == nullptr)
		return false;

	if (callid != m_call_session->get_call_id())
		return false;

	if (totag != m_call_session->GetLocalTag())
		return false;

	if (fromtag != m_call_session->GetRemoteTag())
		return false;

	return true;
}
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::process_call_disconnected_event(bool local_end)
{
	LOG_EVENT("trunk", "id:%p : process call disconnect event...", this);

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (m_call_state != sip_state_new_e)
	{
		m_call_state = sip_state_new_e;

			
		if (m_call_session != nullptr)
		{
			if (m_call_session->GetState() < sip_call_session_t::StateConnected)
			{
				// Not yet connected
				if (m_call_session->getType() == sip_session_client_e)      // Local initiated the call
				{
					LOG_CALL("trunk", "id:%p : process call disconnect event : sending CANCEL", this);
					m_call_end_reason = DisconnectCancelTX;
					
				}
				else if (m_call_session->getType() == sip_session_server_e) // Remote initiated the call
				{
					LOG_CALL("trunk", "id:%p : process call disconnect event : sending REJECT", this);
					m_call_end_reason = DisconnectRejectTX;
				}
			}
			else 
			{
				if (local_end)
				{
					LOG_CALL("trunk", "id:%p : process call disconnect event : sending BYE...", this);
					m_call_end_reason = DisconnectByeTX;
				}
			}

			m_call_session->DisconnectCall(false, m_reject_code, m_reject_reason_phrase);
		}
		else
		{
			LOG_WARN("trunk", "id:%p : process call disconnect event : failed : call-session is null!", this);
		}

		if (m_call_session != nullptr)
		{
			m_call_session->unbind_trunk_handler();
			m_call_session->StartAutoDestructTimer(32);
			m_call_session = nullptr;
		}

		g_sip_client->raise_call_disconnected(this, m_call_end_reason);
	}

	if (m_media_session != nullptr)
	{
		rtx_phone::context.free_rtp_term(m_media_session);
		m_media_session = nullptr;
	}

	if (m_call_session != nullptr)
	{
		m_call_session->unbind_trunk_handler();
		m_call_session->StartAutoDestructTimer(32);
		m_call_session = nullptr;
	}

	m_reject_code = sip_480_TemporarilyUnavailable;
	m_reject_reason_phrase = rtl::String::empty;

	LOG_CALL("trunk", "id:%p : process call disconnect event : done", this);
}
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::process_call_rejected_event(uint16_t status_code, const char* status_reason)
{
	LOG_EVENT("trunk", "id:%p : process outgoing call rejected event : code:%u reason:'%s'", this, status_code, status_reason);

	if (m_call_state != sip_state_new_e)
	{
		g_sip_client->raise_call_rejected(this, status_code, status_reason);
	}
}
//--------------------------------------
//
//--------------------------------------
const char* sip_trunk_t::async_get_name()
{
	return  "sip-trunk";
}
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::async_handle_call(uint16_t call_id, uintptr_t int_param, void* ptr_param)
{
	switch (call_id)
	{
	case SIP_TRUNK_DISCONNECTED: // ACMD_DISCONNECT
	{
		sip_disconnect_info_t* info = (sip_disconnect_info_t*)ptr_param;
		async_session_call_disconnected(info);
		if (info->message)
			DELETEO(info->message);
		DELETEO(info);
		break;
	}
	default: // SIP_TRUNK_RX_REINVITE
		break;
	}
}
//-----------------------------------------------
