/* RTX H.248 Media Gate. SIP Softphone
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

//--------------------------------------
//
//--------------------------------------
class sip_trunk_t;
//--------------------------------------
//
//--------------------------------------
enum sip_dtmf_type_t
{
	sip_dtmf_rfc2833_e,	// 0
	sip_dtmf_info_e,	// 1
	sip_dtmf_inband_e,	// 2
};
const char* get_sip_dtmf_type_name(sip_dtmf_type_t type);
//--------------------------------------
// ������ ��������� ������� �����������
// ����� ������� ����������� ������ ����� 3 ������ 
//--------------------------------------
#define SIP_REREGISTER_TIMER_ID 1001
#define SIP_REREGISTER_TIME (60000) // 1 min
#define SIP_REFER_TIMER_ID 1002
#define SIP_REFER_TIMEOUT (1000) // 1 sec
//-----
#define SIP_ASYNC_SEND_REGISTER_EVENT		101
#define SIP_ASYNC_SEND_UNREGISTER_EVENT		102
//-----
#define SIP_ASYNC_BACK_REGISTERED_EVENT		201
#define SIP_ASYNC_BACK_UNREGISTERED_EVENT	202
#define SIP_ASYNC_BACK_TIMEOUT_EVENT		203
#define SIP_ASYNC_BACK_DESTROYED_EVENT		204
#define SIP_ASYNC_BACK_TRANSP_FAILURE_EVENT	205

#define SIP_ASYNC_TRUNK_REFER_ANSWERED		301
#define SIP_ASYNC_TRUNK_REFER_NOTIFIED		302
#define SIP_ASYNC_TRUNK_REFER_INCOME		303
//--------------------------------------
//
//--------------------------------------
enum class reg_state_t
{
	registered,
	unregistered,
	registering,
	unregistering
};
enum class refer_state_t
{
	prepared,	// terminated -> prepared -> sent! -> answered! -> notifying! -> terminated -> 
	sent,
	answered,
	notifying,
	terminated
};
//--------------------------------------
//
//--------------------------------------
class sip_account_t : public sip_reg_session_event_handler_t, rtl::ITimerEventHandler, rtl::async_call_target_t
{
	rtl::MutexWatch m_sync;
	sip_profile_t m_profile;
	sip_reg_session_client_t* m_session;
	reg_state_t m_reg_state;
	rtl::ArrayT<sip_trunk_t*> m_trunk_list;
	PayloadSet m_audio_payload_set;
	uint16_t m_local_port;
	char m_aor[64];

	/// refer and avtive trunk states
	sip_trunk_t* m_refer_trunk; // for referrer and referree;
	sip_trunk_t* m_target_trunk; // for refering target
	sip_value_uri_t m_refer_to;
	sip_value_uri_t m_referred_by;
	sip_status_t m_refer_code;
	rtl::String m_refer_message;

public:
	sip_account_t(const char* username, const char* domain);
	~sip_account_t();

	void destroy();

	const rtl::String& get_username() { return m_profile.get_username(); }
	const rtl::String& get_domain() { return m_profile.get_domain(); }
	uint16_t get_local_port() { return m_local_port; }
	const rtl::String& get_proxy_address() { return m_profile.get_proxy(); }
	uint16_t get_proxy_port() { return m_profile.get_proxy_port(); }
	const rtl::String& get_auth_id() { return m_profile.get_auth_id(); }
	const rtl::String& get_auth_pwd() { return m_profile.get_auth_pwd(); }
	int get_expires() { return m_profile.get_expires(); }
	int get_keepalive() { return m_profile.get_keep_alive_interval(); }
	void get_audio_set(PayloadSet& payloads);

	void set_local_port(uint16_t port);
	void set_proxy_address(const char* address, uint16_t port, sip_transport_type_t type);
	void set_authentication(const char* auth_id, const char* auth_pwd);
	void set_displayname(const char* displayname);
	void set_expires(int expires);
	void set_keepalive(int interval);
	void set_audio_set(const PayloadSet& payloads);

	sip_profile_t& get_profile() { return m_profile; }
	reg_state_t get_reg_state() { return m_reg_state; }

	void start_registration();
	void start_unregistration();
	void send_text_message(const char* to_username, const char* to_displayname, const char* content_type, const char* message_text);

	bool check_request(in_addr remoteAddress, uint16_t remotePort, const char* username, const char* domain);
	call_session_event_handler_t* process_incoming_call(const sip_message_t& invite, sip_call_session_t& call_session);
	bool process_incoming_im_message(const sip_message_t& message);

	sip_trunk_t* create_trunk();
	void remove_trunk(sip_trunk_t* trunk);

	void refer_to(sip_trunk_t* referrer, const char* number, sip_trunk_t* target);

	sip_dtmf_type_t get_dtmf_type() { return sip_dtmf_rfc2833_e; }
	bool get_remote_address_flag() { return false; }

	bool process_refer_request(sip_trunk_t* trunk, const sip_value_uri_t* referTo, const sip_value_uri_t* referredBy);
	void process_refer_response(sip_trunk_t* trunk, sip_status_t code, const rtl::String& reason);
	bool process_notify_request(sip_trunk_t* trunk, const rtl::String& state, const rtl::String& state_value, const rtl::String& sipfrag);

private:
	virtual void sip_reg_session__accepted(sip_reg_session_client_t& session, const sip_message_t& request);
	virtual void sip_reg_session__rejected(sip_reg_session_client_t& session, const sip_message_t& request);
	virtual void sip_reg_session__timeout(sip_reg_session_client_t& session);
	virtual void sip_reg_session__options_request(sip_reg_session_client_t& session, const sip_message_t& request, sip_message_t& response);
	virtual void sip_reg_session__options_response(sip_reg_session_client_t& session, const sip_message_t& response);
	virtual void sip_reg_session__destroying(sip_reg_session_client_t& session);
	virtual void sip_reg_session__transport_failure(sip_reg_session_client_t& session);

	virtual void timer_elapsed_event(rtl::Timer* sender, int tid);

	virtual const char* async_get_name();
	virtual void async_handle_call(uint16_t call_id, uintptr_t int_param, void* ptr_param);

	void raise_event(uint16_t call_id, uintptr_t int_param = 0, void* ptr_param = nullptr);

	void event_async_registered(sip_transport_route_t* route);
	void event_async_unregistered(sip_reg_session_client_t* sesion);
	void event_async_timeout(sip_reg_session_client_t* sesion);
	void event_async_session_destroyed(sip_reg_session_client_t* sesion);
	void event_async_transport_failure(sip_reg_session_client_t* sesion);

	void event_async_refer_answered(int code);
	void event_async_refer_notified(int code);
	void event_async_refer_refer_income();

	void event_send_register();
	void event_send_unregister();

	sip_trunk_t* find_invite_replaces(const sip_value_uri_t* referredBy, const sip_value_t* replaces);
};
//--------------------------------------
