﻿/* RTX H.248 Media Gate. SIP Softphone
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtx_client.h"
#include "rtx_aux.h"
#include "sip_client.h"
#include "sip_account.h"
#include "sip_trunk.h"
#include "std_lock_watcher.h"
#include "std_sys_env.h"
#include "phone_media_engine.h"
#include <ctype.h>
//#include "tone_generator.h"
//-----------------------------------------------
//
//-----------------------------------------------
static SIP_AccountRegisteredEventHandler pfn_AccountRegistered;
static SIP_AccountUnregisteredEventHandler pfn_AccountUnregistered;
static SIP_AccountIncomingCallEventHandler pfn_AccountIncomingCall;
static SIP_AccountNamedEventHandler pfn_AccountNamedEvent;
static SIP_CallRingingEventHandler pfn_CallRinging;
static SIP_CallAnsweredEventHandler pfn_CallAnswered;
static SIP_CallRejectedEventHandler pfn_CallRejected;
static SIP_CallConnectedEventHandler pfn_CallConnected;
static SIP_CallDisconnectedEventHandler pfn_CallDisconnected;
static SIP_CallReferNotifyEventHandler pfn_CallReferNotify;
static SIP_CallReplacedByReferEventHandler pfn_CallReplacedByRefer;
static SIP_CallPlayStopped pfn_CallPlayStopped;
static SIP_ReadConfigValueEventHandler pfn_ReadConfigValue;
//static SIP_AccountIncomingCallExEventHandler pfn_AccountIncomingCallEx;
//static SIP_CallAnsweredExEventHandler pfn_CallAnsweredEx;
//-----------------------------------------------
// global rtx_client object
//-----------------------------------------------
sip_client_t* g_sip_client = nullptr;
//-----------------------------------------------
// static routines
//-----------------------------------------------
static PayloadSet g_codec_set;
static int find_codec(const PayloadFormatRecord& pf);
static void load_codec_set();
static void make_payload_set(PayloadSet& set, const PayloadFormatRecord* pf, int count);
static sip_transport_type_t SIPTransportType__convert(SIPTransportType type);
//-----------------------------------------------
// Макросы проверки и ловли исключений
//-----------------------------------------------
#define CHECK_ENTRY_R(res) \
if (g_sip_client == nullptr) \
{ \
	LOG_ERROR("rtx", "%s - service not started", __FUNCTION__); \
	return res; \
}
#define CHECK_ENTRY \
if (g_sip_client == nullptr) \
{ \
	LOG_ERROR("rtx", "%s - service not started", __FUNCTION__); \
	return; \
}
#define CHECK_ACCOUNT(accid) \
if (accid == 0) \
{ \
	LOG_CALL("rtx", "%s - account id is null!", __FUNCTION__); \
	return; \
}
#define CHECK_ACCOUNT_R(accid, res) \
if (accid == 0) \
{ \
	LOG_CALL("rtx", "%s - account id is null!", __FUNCTION__); \
	return res; \
}
#define CHECK_CALL(callid) \
if (callid == 0) \
{ \
	LOG_CALL("rtx", "%s - call id is null!", __FUNCTION__); \
	return; \
}
#define CHECK_CALL_R(callid, res) \
if (callid == 0) \
{ \
	LOG_CALL("rtx", "%s - call id is null!", __FUNCTION__); \
	return res; \
}
#define START_EXCEPTION \
EXCEPTION_ENTER; \
try {
#define END_EXCEPTION } \
catch(rtl::Exception& ex) \
{\
	LOG_ERROR("rtx", "%s failed with exception : %s", __FUNCTION__, ex.get_message()); \
} \
EXCEPTION_LEAVE;

//-----------------------------------------------
// инициализация oktell.SIPClientSvc.dll
//-----------------------------------------------
SIPC_API bool SIPC_CALL SIP_InitLib(sip_client_callback_interface_t& callback_info)
{
	if (g_sip_client != nullptr)
		return false;

	bool result = false;

	char tmp[MAX_PATH];;//, *rp;
	char logFlags[MAX_PATH];

	EXCEPTION_ENTER;

	try
	{
		// сохраним колбаки
		pfn_AccountRegistered = callback_info.AccountRegistered;
		pfn_AccountUnregistered = callback_info.AccountUnregistered;
		pfn_AccountIncomingCall = callback_info.AccountIncomingCall;
		pfn_AccountNamedEvent = callback_info.AccountNamedEvent;

		pfn_CallRinging = callback_info.CallRinging;
		pfn_CallAnswered = callback_info.CallAnswered;
		pfn_CallRejected = callback_info.CallRejected;
		pfn_CallConnected = callback_info.CallConnected;
		pfn_CallDisconnected = callback_info.CallDisconnected;
		pfn_CallReferNotify = callback_info.CallReferNotify;
		pfn_CallReplacedByRefer = callback_info.CallReplacedByRefer;
		pfn_CallPlayStopped = callback_info.CallPlayStopped;
		pfn_ReadConfigValue = callback_info.ReadConfigValue;

		// 10.07.2008 Peter 
		// Определим каталог начальный для размещения лог журналов.
		// Опрашиваем конфиг, если там нет - берем StartupPath (к екзешнику)
		g_startup_path [ 0 ] = 0;

		if (!read_config_string(STDKEY_WORK_FOLDER, g_startup_path, MAX_PATH))
		{	
			std_get_application_startup_path(g_startup_path, MAX_PATH);
			g_startup_path[MAX_PATH-1] = 0;
		}

		size_t len = strlen(g_startup_path);
		if (g_startup_path[len-1] == FS_PATH_DELIMITER)
		{
			g_startup_path[len-1] = 0;
		}

		if (read_config_string(STDKEY_TRACE_LOGS, logFlags, MAX_PATH))
		{
			rtl::Logger::set_trace_flags(logFlags);
		}

		// сначала определим пути к логам
		if (!read_config_string(STDKEY_LOG_FOLDER, tmp, MAX_PATH))
		{
			strcpy(tmp, "Log");
		}
		
		// скопируем в глобальную переменную
		if (tmp[1] == ':' || tmp[0] == '/')
			strcpy(g_log_root_path, tmp);
		else
			std_snprintf(g_log_root_path, MAX_PATH, "%s%c%s", g_startup_path, FS_PATH_DELIMITER, tmp);

		printf("Log root path: '%s'\n", g_log_root_path);

		// если небыло каталога для логов то создадим его
		fs_directory_create(g_log_root_path);
	
		// 10.07.2008 Peter 
		// Определим каталог начальный где лежат модули.
		// Опрашиваем конфиг, если там нет - берем путь к текущему модулю (а не к запустившему проект)

		Err.create(g_log_root_path, "err", LOG_APPEND | LOG_LOCAL);

		Log.create(g_log_root_path, "rtx", LOG_APPEND);
		
		rtl::Logger::trace_to_string(tmp, MAX_PATH, false); // rtl::Logger::get_trace_flags()
		
		LOG_CALL("rtx", "Starting SIP Client ver %d.%d.%d.%d ... trace %s", API_VERSION_MAJOR, API_VERSION_MINOR, API_VERSION_TEST, API_VERSION_BUILD, tmp);
		LOG_CALL("rtx", "Log path %s + %s", Log.get_folder(), Log.getName());

		SLog.create(g_log_root_path, "ua", LOG_APPEND);
		if (rtl::Logger::check_trace(TRF_CALL))
			SLog.log("UA", "*** version %d.%d.%d.%d ***", API_VERSION_MAJOR, API_VERSION_MINOR, API_VERSION_TEST, API_VERSION_BUILD);

		TransLog.create(g_log_root_path, "trn", LOG_APPEND);
		if (rtl::Logger::check_trace(TRF_CALL))
			TransLog.log("TRN", "*** version %d.%d.%d.%d ***", API_VERSION_MAJOR, API_VERSION_MINOR, API_VERSION_TEST, API_VERSION_BUILD);

		//LOG_CALL("rtx", "initialize dead-lock watcher...");
		bool watcherEnable = true;//read_reg_profile_int("LOCK_WATCHER_ENABLE", TRUE) != FALSE;
		int  watcherPeriod = 60000; // read_reg_profile_int("LOCK_TIMER_PERIOD", 60000);

		LOG_CALL("rtx", "enable watcher %s period %u ms", STR_BOOL(watcherEnable), watcherPeriod);
		rtl::MutexWatch::setup_watcher(watcherEnable, watcherPeriod);
		LOG_CALL("rtx", "initialize dead-lock watcher...done");

		// инициализация подсистем
		LOG_CALL("rtx", "initialize asynchronius call manager...");
		rtl::async_call_manager_t::createAsyncCall(2, 16);
		LOG_CALL("rtx", "initialize asynchronius call manager...done");

		LOG_CALL("rtx", "initialize timers...");
		rtl::Timer::initializeGlobalTimer(true);
		LOG_CALL("rtx", "initialize timers...done");
		
		// а есть ли аудио устройства для воип телефонов?
		LOG_CALL("rtx", "check audio devices...");
		//uint32_t wave_out_count = MM_AudioGetOutputCount();
		//uint32_t wave_in_count = MM_AudioGetInputCount();
		
		//if (wave_out_count == 0 || wave_in_count == 0)
		//{
		//	LOG_ERROR("rtx", "check audio devices failed out:%d || in:%d not present", wave_out_count, wave_in_count);
		//	res = RES_DEVICE_NOT_FOUND;
		//	goto theEnd;
		//}
		LOG_CALL("rtx", "check audio devices...done");

		LOG_CALL("rtx", "initialize audio devices...");

		g_wave_header_count = 3;
		read_config_integer(STDKEY_WAVE_BUFFER_COUNT, g_wave_header_count);

		if (g_wave_header_count < 2 || g_wave_header_count > 4)
			g_wave_header_count = 3;

		g_wave_header_time = 40;
		read_config_integer(STDKEY_WAVE_BUFFER_TIME, g_wave_header_time);
		if (g_wave_header_time < 20 || g_wave_header_time > 40)
			g_wave_header_time = 40;

		g_jitter_depth = 6;
		read_config_integer(STDKEY_JITTER_DEPTH, g_jitter_depth);
		//Peter - с нулем звука нет, одни сехи...
		if (g_jitter_depth != 0 && (g_jitter_depth < 4 || g_jitter_depth > 10))
			g_jitter_depth = 6;

		LOG_CALL("rtx", "initialize audio devices..done : JITTER-DEPTH %d bytes, WAVE-COUNT %d blocks, WAVE-TIME %d", g_jitter_depth, g_wave_header_count, g_wave_header_time);

		// инициируем запись если разрешено!
		// если пусто то не пишем!
		// если есть путь то пишем в rec_path/yearmonthday/time/rec_account_ts.rtp
		read_config_string(STDKEY_RECORD_FOLDER, g_rec_path, MAX_PATH);

//		ToneGen_Initialize();
		// создадим сетевого клиента
		
		LOG_CALL("rtx", "initialize SIP Client...");

		rtx_phone::initialize();

		g_sip_client = NEW sip_client_t;

		g_sip_client->initialize();

		LOG_CALL("rtx", "initialize SIP client...done");

		bool r = selftest_sipdef();

		LOG_CALL("rtx", "SIP Client started (r:%u)", r);

		result = true;
	}
	catch (rtl::Exception& ex)
	{
		LOG_ERROR("rtx", "SIP Client starting failed with exception %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	EXCEPTION_LEAVE;

	return result;
}
//-----------------------------------------------
// инициализация oktell.SIPClientSvc.dll
//-----------------------------------------------
//SIPC_API bool SIPC_CALL SIP_InitLibEx(sip_client_callback_interface_t& callback_info, sip_client_callback_interface_ex_t& callback_info_ex)
//{
//	if (!SIP_InitLib(callback_info))
//	{
//		return false;
//	}
//
//	pfn_AccountIncomingCallEx = callback_info_ex.AccountIncomingCall;
//	pfn_CallAnsweredEx = callback_info_ex.CallAnsweredEx;
//
//	return true;
//}
//-----------------------------------------------
// Синоним функции HAL.StopService()
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_FreeLib()
{
	if (g_sip_client == nullptr)
		return;

	// проверим состояние и параметры
	LOG_CALL("rtx", "Stopping SIP Client...");

	EXCEPTION_ENTER;

	try
	{
#ifdef DEBUG_MEMORY
#define MEGABYTE (1024 * 1024)
#define KILOBYTE (1024)
	uint64_t usage = RC_MemoryUsage();
	uint64_t mbytes = usage / MEGABYTE;
	uint64_t mmean = usage % MEGABYTE;
	uint64_t kbytes = mmean / KILOBYTE;
	uint64_t bytes = mmean % KILOBYTE;
	
	LOG_CALL("rtx", ".....MEMORY USAGE %I64d MB %I64d KB %I64d bytes.....", mbytes, kbytes, bytes);
#endif
		LOG_CALL("rtx", "destroy device manager...");

		LOG_CALL("rtx", "destroy SIP Client...");

		g_sip_client->terminate();
		DELETEO(g_sip_client);
		g_sip_client = nullptr;

		LOG_CALL("rtx", "destroy SIP Client...done");

		LOG_CALL("rtx", "destroy aqsynchronius call manager...");
		rtl::async_call_manager_t::destroyAsyncCall();
		LOG_CALL("rtx", "destroy aqsynchronius call manager...done");

		//ToneGen_Destroy();

		rtl::Watch_Stop();
	}
	catch (rtl::Exception& ex)
	{
		LOG_ERROR("rtx", "SIP Client stopping failed with exception %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}
	
	LOG_CALL("rtx", "SIP Client stopped");

	Log.destroy();

	SLog.destroy();

	TransLog.destroy();

	EXCEPTION_LEAVE;
}
//-----------------------------------------------
// Добавление акаунта (учетной записи вида username@domain)
//-----------------------------------------------
SIPC_API uintptr_t SIPC_CALL SIP_AccountCreate(const char* username, const char* domain)
{
	CHECK_ENTRY_R(0);

	uintptr_t account_id = 0;

	START_EXCEPTION;

	account_id = g_sip_client->create_account(username, domain);

	END_EXCEPTION;

	return account_id;
}
//-----------------------------------------------
// Удаление акаунта и разрегистрации если акаунт был зарегистрирован
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_AccountDestroy(uintptr_t account_id)
{
	CHECK_ENTRY;

	CHECK_ACCOUNT(account_id);

	START_EXCEPTION;

	g_sip_client->destroy_account(account_id);

	END_EXCEPTION;
}
//-----------------------------------------------
//
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_AccountSetLocalPort(uintptr_t account_id, uint16_t port)
{
	CHECK_ENTRY;

	CHECK_ACCOUNT(account_id);

	START_EXCEPTION;

	g_sip_client->set_local_port(account_id, port);
	
	END_EXCEPTION;
}
//-----------------------------------------------
// Установка адреса прокси сервера куда будут отправлятся запросы регистрации и инициации звонка
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_AccountSetProxy(uintptr_t account_id, const char* address, uint16_t port, SIPTransportType type)
{
	CHECK_ENTRY;

	CHECK_ACCOUNT(account_id);

	START_EXCEPTION;

	g_sip_client->set_proxy_address(account_id, address, port, SIPTransportType__convert(type));
	
	END_EXCEPTION;
}
//-----------------------------------------------
// Принудительное включение отправки пакетов Keep-Alive
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_AccountSetKeepAlive(uintptr_t account_id, int keepalive_interval)
{
	CHECK_ENTRY;

	CHECK_ACCOUNT(account_id);

	START_EXCEPTION;

	g_sip_client->set_keepalive(account_id, keepalive_interval);
	
	END_EXCEPTION;
}
//-----------------------------------------------
// Установка параметров аутентификации
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_AccountSetAuthentication(uintptr_t account_id, const char* auth_id, const char* password)
{
	CHECK_ENTRY;

	CHECK_ACCOUNT(account_id);

	START_EXCEPTION;

	g_sip_client->set_authentication(account_id, auth_id, password);

	END_EXCEPTION;
}
//-----------------------------------------------
// Установка отображаемого имени
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_AccountSetDisplayName(uintptr_t account_id, const char* displayName)
{
	CHECK_ENTRY;

	CHECK_ACCOUNT(account_id);

	START_EXCEPTION;

	g_sip_client->set_displayname(account_id, displayName);

	END_EXCEPTION;
}
//-----------------------------------------------
// Установить время жизни регистрации
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_AccountSetExpires(uintptr_t account_id, int expires)
{
	CHECK_ENTRY;

	CHECK_ACCOUNT(account_id);

	START_EXCEPTION;

	g_sip_client->set_expires(account_id, expires);

	END_EXCEPTION;
}
//-----------------------------------------------
//
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_AccountSetAudioCodecs(uintptr_t account_id, const PayloadFormatRecord* payloadList, int count)
{
	CHECK_ENTRY;

	CHECK_ACCOUNT(account_id);

	START_EXCEPTION;

	PayloadSet payloads;
	make_payload_set(payloads, payloadList, count);
	g_sip_client->set_payload_set(account_id, payloads);

	END_EXCEPTION;
}
//-----------------------------------------------
// Регистрация и начало работы акаунта
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_AccountRegister(uintptr_t account_id)
{
	CHECK_ENTRY;

	CHECK_ACCOUNT(account_id);

	START_EXCEPTION;

	g_sip_client->start_registration(account_id);

	END_EXCEPTION;
}
//-----------------------------------------------
// Отмена регистрации и останов работы акаунта, все транки удаляются
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_AccountUnregister(uintptr_t account_id)
{
	CHECK_ENTRY;

	CHECK_ACCOUNT(account_id);

	START_EXCEPTION;

	g_sip_client->start_unregistration(account_id);
	
	END_EXCEPTION;
}
//-----------------------------------------------
// отправка сообщения кому-либо
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_AccountSendTextMessage(uintptr_t account_id, const char* to_username, const char* to_displayname, const char* content_type, const char* message_text)
{
	CHECK_ENTRY;

	CHECK_ACCOUNT(account_id);

	START_EXCEPTION;
		
	g_sip_client->send_text_message(account_id, to_username, to_displayname, content_type, message_text);
		
	END_EXCEPTION;
}
//-----------------------------------------------
// Создание Нового транка для ИСХОДЯЩЕГО звонка
//-----------------------------------------------
SIPC_API uintptr_t SIPC_CALL SIP_AccountCreateCall(uintptr_t account_id)
{
	CHECK_ENTRY_R(0);

	CHECK_ACCOUNT_R(account_id, 0);

	uintptr_t trunk_id = 0;

	START_EXCEPTION;

	trunk_id = g_sip_client != nullptr && account_id != 0 ? g_sip_client->create_trunk(account_id) : 0;
	
	END_EXCEPTION;

	return trunk_id;
}
//-----------------------------------------------
// удаление транка
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_AccountDestroyCall(uintptr_t trunk_id)
{
	CHECK_ENTRY;

	CHECK_CALL(trunk_id);

	START_EXCEPTION;

	g_sip_client->destroy_trunk(trunk_id);
		
	END_EXCEPTION;
}
//-----------------------------------------------
// звонить указонному абоненту
//-----------------------------------------------
SIPC_API bool SIPC_CALL SIP_CallTo(uintptr_t trunk_id, const char* number, const char* caller_id, const char* caller_name)
{
	CHECK_ENTRY_R(false);

	CHECK_CALL_R(trunk_id, false);

	bool result = false;

	START_EXCEPTION;

	result = g_sip_client->trunk_call_to(trunk_id, number, caller_id, caller_name);

	END_EXCEPTION;

	return result;
}
//-----------------------------------------------
// обрыв звонка
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_CallDisconnect(uintptr_t trunk_id)
{
	CHECK_ENTRY;

	CHECK_CALL(trunk_id);

	START_EXCEPTION;

	g_sip_client->trunk_disconnect(trunk_id);

	END_EXCEPTION;
}
//-----------------------------------------------
// ответ на входящий вызов
//-----------------------------------------------
SIPC_API bool SIPC_CALL SIP_CallAnswer(uintptr_t trunk_id, bool ringing)
{
	CHECK_ENTRY_R(false);

	CHECK_CALL_R(trunk_id, false);

	bool result = false;

	START_EXCEPTION;

	result = g_sip_client->trunk_answer(trunk_id, ringing);

	END_EXCEPTION;

	return result;
}
//-----------------------------------------------
// отмена входящего вызова
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_CallReject(uintptr_t trunk_id, uint16_t code, const char* reason)
{
	CHECK_ENTRY;

	CHECK_CALL(trunk_id);

	START_EXCEPTION;

	g_sip_client->trunk_reject(trunk_id, code, reason);

	END_EXCEPTION;
}
//-----------------------------------------------
//
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_ReferTo(uintptr_t trunk_id, const char* number, uintptr_t replace_to)
{
	CHECK_ENTRY;

	CHECK_CALL(trunk_id);

	START_EXCEPTION;

	g_sip_client->trunk_refer_to(trunk_id, number, replace_to);

	END_EXCEPTION;
}
//-----------------------------------------------
// Получить поле заголовока
//-----------------------------------------------
SIPC_API bool SIPC_CALL SIP_CallGetHeader(uintptr_t trunk_id, const char* sip_header_name, int index, char* buffer, int size)
{
	CHECK_ENTRY_R(false);

	CHECK_CALL_R(trunk_id, false);

	bool result = false;

	START_EXCEPTION;
	
	rtl::String value;
	result = g_sip_client->get_call_header(trunk_id, sip_header_name, index, value);

	if (!value.isEmpty())
	{
		strncpy(buffer, value, size);
		buffer[size-1] = 0;
	}
	else
	{
		buffer[0] = 0;
	}

	END_EXCEPTION;

	return result;
}
//-----------------------------------------------
// удержание звонка
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_CallHold(uintptr_t trunk_id, bool state)
{
	CHECK_ENTRY;

	CHECK_CALL(trunk_id);

	START_EXCEPTION;

	g_sip_client->trunk_hold(trunk_id, state);

	END_EXCEPTION;
}
//-----------------------------------------------
// удержание звонка
//-----------------------------------------------
//SIPC_API void SIPC_CALL SIP_CallUnhold(uintptr_t trunk_id)
//{
//	CHECK_ENTRY;
//
//	CHECK_CALL(trunk_id);
//
//	START_EXCEPTION;
//		
//
//	g_sip_client->trunk_unhold(trunk_id);
//
//	END_EXCEPTION;
//}
//-----------------------------------------------
// Отключение микрофона
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_CallMute(uintptr_t trunk_id, bool state)
{
	CHECK_ENTRY;

	CHECK_CALL(trunk_id);

	START_EXCEPTION;
			
	g_sip_client->trunk_mute(trunk_id, state);

	END_EXCEPTION;
}
//-----------------------------------------------
// Отключение микрофона
//-----------------------------------------------
//SIPC_API void SIPC_CALL SIP_CallUnmute(uintptr_t trunk_id)
//{
//	CHECK_ENTRY;
//
//	CHECK_CALL(trunk_id);
//
//	START_EXCEPTION;
//	
//	g_sip_client->trunk_unmute(trunk_id);
//
//	END_EXCEPTION;
//}
//-----------------------------------------------
// послать DTMF сигнал
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_CallSentDtmf(uintptr_t trunk_id, char tone)
{
	CHECK_ENTRY;

	CHECK_CALL(trunk_id);

	START_EXCEPTION;

	g_sip_client->trunk_send_dtmf(trunk_id, tone);
	
	END_EXCEPTION;
}
//-----------------------------------------------
// 
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_CallStartPlaying(uintptr_t trunk_id, const char* file_path, bool in_cycle)
{
	CHECK_ENTRY;

	CHECK_CALL(trunk_id);

	START_EXCEPTION;

	g_sip_client->trunk_start_playing(trunk_id, file_path, in_cycle);

	END_EXCEPTION;
}
//-----------------------------------------------
// 
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_CallStopPlaying(uintptr_t trunk_id)
{
	CHECK_ENTRY;

	CHECK_CALL(trunk_id);

	START_EXCEPTION;

	g_sip_client->trunk_stop_playing(trunk_id);
	
	END_EXCEPTION;
}
//-----------------------------------------------
// 
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_CallStartRecording(uintptr_t trunk_id, const char* file_path)
{
	CHECK_ENTRY;

	CHECK_CALL(trunk_id);

	START_EXCEPTION;

	g_sip_client->trunk_start_recording(trunk_id, file_path);
	
	END_EXCEPTION;
}
//-----------------------------------------------
// 
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_CallStopRecording(uintptr_t trunk_id)
{
	CHECK_ENTRY;

	CHECK_CALL(trunk_id);

	START_EXCEPTION;

	g_sip_client->trunk_stop_recording(trunk_id);

	END_EXCEPTION;
}
//-----------------------------------------------
// 
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_StartRecording(const char* file_path)
{
	CHECK_ENTRY;

	if ((file_path == 0) || (file_path[0] == L'\0'))
	{
		LOG_CALL("rtx", "SIP_StartRecording -- start recording failed - invalid file path!");
		return;
	}

	START_EXCEPTION;

	g_sip_client->start_recording(file_path);

	END_EXCEPTION;
}
//-----------------------------------------------
// 
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_StopRecording()
{
	CHECK_ENTRY;

	START_EXCEPTION;

	g_sip_client->stop_recording();

	END_EXCEPTION;
}
//-----------------------------------------------
// Выставление устройств воспроизведения и записи а также тип библиотеки для работы с аудио устройствами
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_AudioSetDevices(const char* device_out, const char* device_in)
{
	CHECK_ENTRY;

	START_EXCEPTION;

	g_sip_client->set_devices(device_out, device_in);
	
	END_EXCEPTION;
}
//-----------------------------------------------
// получение имени устройства выбранное для воспроизведения 
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_AudioGetOutput(char* buffer, int size)
{
	CHECK_ENTRY;

	if (buffer == nullptr || size <= 0)
	{
		LOG_CALL("rtx", "%s failed - invalid buffer %p or size %d!", __FUNCTION__, buffer, size);
		return;
	}

	START_EXCEPTION;

	g_sip_client->get_output(buffer, size);

	END_EXCEPTION;
}
//-----------------------------------------------
// установление устройства для воспроизведения
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_AudioSetOutput(const char* device_out)
{
	CHECK_ENTRY;

	if (device_out == nullptr || device_out[0] == 0)
	{
		LOG_CALL("rtx", "%s failed - invalid device_out!", __FUNCTION__);
		return;
	}

	START_EXCEPTION;
	
	g_sip_client->set_output(device_out);

	END_EXCEPTION;
}
//-----------------------------------------------
// получение имени устройства записи для воспроизведения 
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_AudioGetInput(char* buffer, int size)
{
	CHECK_ENTRY;

	if (buffer == nullptr || size <= 0)
	{
		LOG_CALL("rtx", "%s failed - invalid buffer %p or size %d!", __FUNCTION__, buffer, size);
		return;
	}

	START_EXCEPTION;

	g_sip_client->get_input(buffer, size);
	
	END_EXCEPTION;
}
//-----------------------------------------------
// установление устройства для записи
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_AudioSetInput(const char* device_in)
{
	CHECK_ENTRY;

	START_EXCEPTION;

	g_sip_client->set_input(device_in);

	END_EXCEPTION;
}
//-----------------------------------------------
// отключение микрофона от всех транков
//-----------------------------------------------
SIPC_API void SIPC_CALL SIP_AudioMuteInput(bool mute)
{
	CHECK_ENTRY;

	START_EXCEPTION;

	g_sip_client->mute_all(mute);
	
	END_EXCEPTION;
}
//-----------------------------------------------
//
//-----------------------------------------------
SIPC_API int SIPC_CALL SIP_AudioGetCodecCount()
{
	CHECK_ENTRY_R(0);

	int count = 0;

	START_EXCEPTION;

	load_codec_set();
	
	count = g_codec_set.getCount();

	END_EXCEPTION;

	return count;
}
//-----------------------------------------------
//
//-----------------------------------------------
SIPC_API bool SIPC_CALL SIP_AudioGetCodecInfo(int index, PayloadFormatRecord* format)
{
	CHECK_ENTRY_R(false);

	if (format == nullptr)
	{
		LOG_CALL("rtx", "%s failed - invalid format ptr!", __FUNCTION__);
		return false;
	}

	bool result = false;

	START_EXCEPTION;

	load_codec_set();

	if (index >= 0 && index < g_codec_set.getCount())
	{
		const PayloadFormat& fmt = g_codec_set.getAt(index);

		strncpy(format->encoding, fmt.getEncoding(), 63);
		format->encoding[63] = 0;
		format->id = fmt.getId_u8();
		format->clockrate = fmt.getFrequency();
		format->channels = fmt.getChannelCount();

		result = true;
	}

	END_EXCEPTION;

	return result;
}
//--------------------------------------
//
//--------------------------------------
void raise_account_registered(uintptr_t account_id)
{
	LOG_CALL("rtx", "raise account event REGISTERED for id:%p...", account_id);

	if (pfn_AccountRegistered != nullptr)
	{
		pfn_AccountRegistered(account_id);
		
		LOG_CALL("rtx", "event handled");
	}
	else
	{
		LOG_ERROR("rtx", "event no raised - callback function is null!");
	}
}
//--------------------------------------
//
//--------------------------------------
void raise_account_unregistered(uintptr_t account_id, bool by_user)
{
	LOG_CALL("rtx", "raise account event UNREGISTERED for id:%p by-from:%s...", account_id, STR_BOOL(by_user));

	if (pfn_AccountUnregistered != nullptr)
	{
		pfn_AccountUnregistered(account_id, by_user);

		LOG_CALL("rtx", "event handled");
	}
	else
	{
		LOG_ERROR("rtx", "event no raised - callback function is null!");
	}
}
//--------------------------------------
//
//--------------------------------------
void raise_account_incoming_call(uintptr_t account_id, uintptr_t trunk_id, const char* called_id, const char* called_name, const char* caller_id, const char* caller_name/*, const char* invite*/)
{
	LOG_CALL("rtx", "raise account event INCOMING_CALL for id:%p trunk-id:%p called-id:%s called-name:%s caller-id:%s caller-name:%s...", account_id, trunk_id, called_id, called_name, caller_id, caller_name);

	/*if ((invite != nullptr) && (pfn_AccountIncomingCallEx != nullptr))
	{
		pfn_AccountIncomingCallEx(account_id, trunk_id, invite);

		LOG_CALL("rtx", "event handled (ex)");
	}
	else*/
	if (pfn_AccountIncomingCall != nullptr)
	{
		pfn_AccountIncomingCall(account_id, trunk_id, called_id, called_name, caller_id, caller_name);

		LOG_CALL("rtx", "event handled");
	}
	else
	{
		LOG_ERROR("rtx", "event no raised - callback function is null!");
	}
}
//--------------------------------------
//
//--------------------------------------
void raise_account_named_event(uintptr_t account_id, const char* event_name, const char* event_parameter)
{
	LOG_CALL("rtx", "raise named event %s for account id:%p...", event_name, account_id);

	if (pfn_AccountNamedEvent != nullptr)
	{
		pfn_AccountNamedEvent(account_id, event_name, event_parameter);

		LOG_CALL("rtx", "event handled");
	}
	else
	{
		LOG_ERROR("rtx", "event no raised - callback function is null!");
	}
}
//--------------------------------------
//
//--------------------------------------
void raise_call_ringing(uintptr_t trunk_id, uint16_t code, const char* reason, bool with_media)
{
	LOG_CALL("rtx", "raise call event RINGING for id:%p code:%u reason:%s with-media:%s...", trunk_id, code, reason, STR_BOOL(with_media));

	if (pfn_CallRinging != nullptr)
	{
		pfn_CallRinging(trunk_id, code, reason, with_media);

		LOG_CALL("rtx", "event handled");
	}
	else
	{
		LOG_ERROR("rtx", "event no raised - callback function is null!");
	}
}
//--------------------------------------
//
//--------------------------------------
void raise_call_answered(uintptr_t trunk_id/*, const char* sip_message*/)
{
	LOG_CALL("rtx", "raise call event ANSWERED for id:%p...", trunk_id);

	/*if ((sip_message != nullptr) && (pfn_CallAnsweredEx != nullptr))
	{
		pfn_CallAnsweredEx(trunk_id, sip_message);

		LOG_CALL("rtx", "event (ex) handled");
	}
	else*/
	if (pfn_CallAnswered != nullptr)
	{
		pfn_CallAnswered(trunk_id);
	
		LOG_CALL("rtx", "event handled");
	}
	else
	{
		LOG_ERROR("rtx", "event no raised - callback function is null!");
	}
}
//--------------------------------------
//
//--------------------------------------
void raise_call_rejected(uintptr_t trunk_id, uint16_t code, const char* reason)
{
	LOG_CALL("rtx", "raise call event REJECTED for id:%p code:%u reason:%s...", trunk_id, code, reason);

	if (pfn_CallRejected != nullptr)
	{
		pfn_CallRejected(trunk_id, code, reason);
	
		LOG_CALL("rtx", "event handled");
	}
	else
	{
		LOG_ERROR("rtx", "event no raised - callback function is null!");
	}
}
//--------------------------------------
//
//--------------------------------------
void raise_call_connected(uintptr_t trunk_id)
{
	LOG_CALL("rtx", "raise call event CONNECTED for id:%p...", trunk_id);

	if (pfn_CallConnected != nullptr)
	{
		pfn_CallConnected(trunk_id);
	
		LOG_CALL("rtx", "event handled");
	}
	else
	{
		LOG_ERROR("rtx", "event no raised - callback function is null!");
	}
}
//--------------------------------------
//
//--------------------------------------
void raise_call_disconnected(uintptr_t trunk_id, DisconnectReason reason)
{
	LOG_CALL("rtx", "raise call event DISCONNECTED for id:%p reason:%s...", trunk_id, get_sip_call_disconnect_reason_text(reason));

	if (pfn_CallDisconnected != nullptr)
	{
		pfn_CallDisconnected(trunk_id, reason);

		LOG_CALL("rtx", "event handled");
	}
	else
	{
		LOG_ERROR("rtx", "event no raised - callback function is null!");
	}
}
//--------------------------------------
//
//--------------------------------------
void raise_call_refer_notify(uintptr_t trunk_id, ReferNotifyType ntype, const char* message)
{
	static const char* ntype_names[] = { "ReferIncome", "ReferAnswer", "ReferNotify" };
	const char* nname = ntype >= 0 && ntype < sizeof(ntype_names) / sizeof(const char*) ? ntype_names[ntype] : "unknown";
	LOG_CALL("rtx", "raise call event NOTIFY for id:%p type:%s message:%s...", trunk_id, nname, message);

	if (pfn_CallReferNotify != nullptr)
	{
		pfn_CallReferNotify(trunk_id, ntype, message);

		LOG_CALL("rtx", "event handled");
	}
	else
	{
		LOG_ERROR("rtx", "event no raised - callback function is null!");
	}
}
//--------------------------------------
//
//--------------------------------------
void raise_call_replaced_by_refer(uintptr_t trunk_old, uintptr_t trunk_new, const char* called_id, const char* called_name, const char* caller_id, const char* caller_name)
{
	LOG_CALL("rtx", "raise call replaced by refer for old:%p new:%p called-id:%s called-name:%s caller-id:%s caller-name:%s...", trunk_old, trunk_new, called_id, called_name, caller_id, caller_name);

	if (pfn_CallReplacedByRefer != nullptr)
	{
		pfn_CallReplacedByRefer(trunk_old, trunk_new, called_id, called_name, caller_id, caller_name);

		LOG_CALL("rtx", "event handled");
	}
	else
	{
		LOG_ERROR("rtx", "event no raised - callback function is null!");
	}
}
//--------------------------------------
//
//--------------------------------------
void raise_call_play_stopped(uintptr_t trunk_id)
{
	LOG_CALL("rtx", "raise call event PLAY STOPPED for id:%p...", trunk_id);

	if (pfn_CallPlayStopped != nullptr)
	{
		pfn_CallPlayStopped(trunk_id);

		LOG_CALL("rtx", "event handled");
	}
	else
	{
		LOG_ERROR("rtx", "event no raised - callback function is null!");
	}
}
//--------------------------------------
//
//--------------------------------------
bool read_config_string(const char* key, char* buffer, int size)
{
	if (pfn_ReadConfigValue != nullptr)
	{
		memset(buffer, 0, size);
		bool res = pfn_ReadConfigValue(key, buffer, size);
		fprintf(stdout, "read_config_string(%s) -> '%s'\n\n", key, res ? buffer : "---not found---");
		return res;
	}

	return false;
}
//--------------------------------------
//
//--------------------------------------
bool read_config_integer(const char* key, uint32_t& value)
{
	char tmp[64];

	if (read_config_string(key, tmp, 64))
	{
		if (isdigit(tmp[0]))
		{
			value = strtoul(tmp, nullptr, 10);
			return true;
		}
	}

	return false;
}
//--------------------------------------
//
//--------------------------------------
bool read_config_boolean(const char* key, bool& value)
{
	char tmp[64];
	bool result = false;

	if (read_config_string(key, tmp, 64))
	{
		if (std_stricmp(tmp, "true") == 0 || strcmp(tmp, "1") == 0)
		{
			value = true;
			result = true;
		}
		else if (std_stricmp(tmp, "false") == 0 || strcmp(tmp, "0") == 0)
		{
			value = false;
			result = true;
		}
	}

	return result;
}
//--------------------------------------
//
//--------------------------------------
const char* get_sip_call_disconnect_reason_text(DisconnectReason reason)
{
	static const char* names[] =
	{
		"DisconnectByeTX",	"DisconnectByeRX",
		"DisconnectCancelTX", "DisconnectCancelRX",
		"DisconnectRejectTX",	"DisconnectRejectRX",
		"DisconnectTimeout",	"DisconnectUnknown",
	};

	if (reason < DisconnectByeTX  || reason > DisconnectUnknown)
		reason = DisconnectUnknown;

	return names[reason];
}
//-----------------------------------------------
//
//-----------------------------------------------
static int find_codec(const PayloadFormatRecord& pf)
{
	load_codec_set();

	for (int i = 0; i < g_codec_set.getCount(); i++)
	{
		const PayloadFormat& p = g_codec_set.getAt(i);
		if (rtl::String::compare(p.getEncoding(), pf.encoding, true) == 0 && p.getFrequency() == pf.clockrate && p.getChannelCount() == pf.channels)
		{
			return i;
		}
	}

	return -1;
}
//-----------------------------------------------
//
//-----------------------------------------------
static void load_codec_set()
{
	static bool initialized = false;

	if (!initialized)
	{
		rtx_codec__get_available_audio_codecs(g_codec_set);
		initialized = true;
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
static void make_payload_set(PayloadSet& set, const PayloadFormatRecord* pfl, int count)
{
	if (rtl::Logger::check_trace(TRF_CALL))
	{
		char payloads[256];
		int len = 0;

		for (int i = 0; i < count; i++)
		{
			len += std_snprintf(payloads + len, 256 - len, "%s ", pfl[i].encoding);
		}

		payloads[len] = 0;

		Log.log("rtx", "set payload set -- %s", payloads);
	}

	for (int i = 0; i < count; i++)
	{
		const PayloadFormatRecord& pf = pfl[i];
		int index = find_codec(pf);
		if (index != -1)
		{
			set.addFormat(g_codec_set.getAt(index));
		}
		else
		{
			// ????
			set.addFormat(pf.encoding, (media::PayloadId)pf.id, pf.clockrate, pf.channels);
		}
	}

}
//-----------------------------------------------
//enum sip_transport_type_t
//{
//sip_transport_udp_e = 0,			// стандартный канал на udp
//sip_transport_tcp_e = 1,			// стандартный канал на tcp
//sip_transport_tls_e = 2,			// шифрованный канал на tcp
//sip_transport_ws_e = 3,				// стандартный канал на WebSocket
//sip_transport_wss_e = 4,			// шифрованный WebSocket
// //sip_transport_sctp_e,			// стандартный канал на sctp
//};
//-----------------------------------------------
static sip_transport_type_t SIPTransportType__convert(SIPTransportType type)
{
	return (sip_transport_type_t)type;
}
//-----------------------------------------------
