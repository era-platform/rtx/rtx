/* RTX H.248 Media Gate. SIP Softphone
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sdp_media.h"
//-----------------------------------------------
//
//-----------------------------------------------
#define SDP_DEFAULT_SESSION_NAME	"softphone"
//-----------------------------------------------
//
//-----------------------------------------------
class sdp_session_t
{
	rtl::Logger& m_log;

	int m_v;								// v=0
	char m_o_user_name[64];					// o=<username>
	uint64_t m_o_session_id;				//   <sess-id>
	uint64_t m_o_version;					//   <sess-version>
	char m_o_net[8];						//   <nettype> default IN
	char m_o_ip[8];							//   <addrtype> default IP4
	char m_o_address[64];					//   <unicast-address> ip or dns domain
	char m_s_name[64];						// s=<session name> default '-'
	char m_c_net[8];						// c=<nettype>
	char m_c_ip[8];							//   <addrtype>
	in_addr m_c_address;					//   <connection-address>
	char m_b_modifier[32];					// b=<bwtype>:<bandwidth>
	int m_b_value;
	sdp_direction_type_t m_direction;		//  a=recvonly, sendonly, sendrecv, inactive
	int m_dynamic_payload_base;
	sdp_media_list_t m_media_list;

private:
	void parse_owner(const char* str);
	void parse_connect_address(const char* str);
	void decode_attribute(const char* attr);
	void decode_group_attribute(const char* attr);

public:
	sdp_session_t(rtl::Logger& log);
	sdp_session_t(rtl::Logger& log, const char* sdp);
	~sdp_session_t();

	bool decode(const char* sdp);
	rtl::String encode();

	void cleanup();

	rtl::Logger& get_log() { return m_log; }
	bool is_empty() { return m_media_list.getCount() == 0; }
	void remove_media_description(sdp_media_description_type_t type);

	void set_origin(const char* userName, uint64_t sessionId, uint64_t sessionVer, const char* address, const char* netType = nullptr, const char* addrType = nullptr);
	const char* get_origin_name() const { return m_o_user_name; }
	uint64_t get_origin_id() { return m_o_session_id; }
	uint64_t get_origin_version() { return m_o_version; }
	const char* get_origin_net_type() { return m_o_net; }
	const char* get_origin_address_type() { return m_o_ip; }
	const char* get_origin_address() { return m_o_address; }

	void set_session_name(const char* name) { STR_COPY(m_s_name, name); }
	const char* get_session_name() { return m_s_name; }

	void set_connection(in_addr address, const char* netType = nullptr, const char* addrType = nullptr);
	const char* get_connection_net_type() { return m_c_net; }
	const char* get_connection_address_type() { return m_c_ip; }
	in_addr get_connection_address() { return m_c_address; }

	const char* get_bandwidth_modifier() { return m_b_modifier; }
	void set_bandwidth_modifier(const char* modifier) { strncpy(m_b_modifier, modifier, 31); m_b_modifier[31] = 0; }

	int get_bandwidth_value() { return m_b_value; }
	void set_bandwidth_value(int value) { m_b_value = value; }

	sdp_media_list_t& get_media_list() { return m_media_list; }
	sdp_media_description_t* get_media_description(sdp_media_description_type_t rtpMediaType);
	sdp_media_description_t* get_last_media_description();
	int get_next_dynamic_payload() { return m_dynamic_payload_base++; }

	void add_media_description(sdp_media_description_t* mediaDescription);
	
	void set_direction(sdp_direction_type_t direction) { m_direction = direction; }
	sdp_direction_type_t get_direction() { return m_direction; }
	sdp_direction_type_t get_direction(sdp_media_description_type_t mediaType);
};
//-----------------------------------------------
