/* RTX H.248 Media Gate. SIP Softphone
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "sdp.h"
//-----------------------------------------------
//
//-----------------------------------------------
class phone_rtp_term_t
{
	h248::IMediaContext* m_context;

	uint32_t m_term_id;
	uint32_t m_play_id;

	PayloadSet m_audio_codecs;
	
	uint64_t m_audio_session_id;
	uint64_t m_audio_session_ver;
	
	in_addr m_iface;

	sdp_session_t m_remote_sdp;
	sdp_session_t m_local_sdp;

	static char s_ua_name[64];

	enum class State { idle, offered, re_offered, answered, destroyed } m_state;
	enum class Direction { incoming, outgoing, none } m_direction;

public:
	phone_rtp_term_t(h248::IMediaContext* context);
	~phone_rtp_term_t();

	bool setup(in_addr iface);
	void destroy();

	uint32_t get_id() { return m_term_id; }

	void set_allowed_codecs(const PayloadSet& allowed);

	bool set_remote_sdp(const char* remote_sdp);
	bool get_local_sdp(rtl::String& local_sdp);
	bool reset_remote_sdp(const char* remote_sdp);
	bool reget_local_sdp(bool hold, rtl::String& local_sdp);
	
	bool start_tone_player(int signal); // phone_signal_t
	bool start_player(const char* path, bool cycled);
	void stop_player();
	bool start_recording(const char* path);
	void stop_recording();
	void mute(bool state);
	void send_dtmf(char tone, int duration);

	static void set_ua_name(const char* ua_name);

private:
	rtl::String make_sdp_template();
	rtl::String make_sdp_template(const char* remote_sdp);
	void update_local_sdp(const rtl::String& sdp);
	void load_audio_codecs();
};
//-----------------------------------------------
