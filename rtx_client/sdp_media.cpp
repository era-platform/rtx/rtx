﻿/* RTX H.248 Media Gate. SIP Softphone
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sdp_session.h"
#include "sdp_media.h"
//-----------------------------------------------
//
//-----------------------------------------------
sdp_media_description_t::sdp_media_description_t(const sdp_media_description_t& description) :
	m_log(description.m_log)
{
	m_session = description.m_session;
	m_media_type = description.m_media_type;			// type of media
	strncpy(m_media, description.m_media, 16);			// name of media

	m_port_count = description.m_port_count;			// port count in sequense
	m_port = description.m_port;						// port begins sequense
	m_address = description.m_address;					// address
	strncpy(m_transport, description.m_transport, 32);	// transport/profile

	m_direction = description.m_direction;				// flow direction
	m_formats = description.m_formats;					// described formats
	m_packet_time = description.m_packet_time;			// ptime attribute, in milliseconds
}
//--------------------------------------
//
//--------------------------------------
sdp_media_description_t::sdp_media_description_t(sdp_session_t* session, sdp_media_description_type_t mediaType, const char* mediaName) : m_log(session->get_log())
{
	m_session = session;
	m_media_type = mediaType;
	m_port_count = 0;
	m_port = 0;
	
	if (mediaName == nullptr)
	{
		switch (m_media_type)
		{
		case sdp_media_audio_e: mediaName = "audio"; break;
		case sdp_media_video_e: mediaName = "video"; break;
		}
	}

	if (mediaName != nullptr)
	{
		strncpy(m_media, mediaName, sizeof(m_media)-1);
		m_media[sizeof(m_media)-1] = 0;
	}
	else
	{
		memset(m_media, 0, sizeof(m_media));
	}

	strcpy(m_transport, SDP_MEDIA_PROFILE_RTP);
	m_address.s_addr = INADDR_ANY;
	m_direction = sdp_direction_undefined_e;
	m_packet_time = 0;
}
//--------------------------------------
//
//--------------------------------------
sdp_media_description_t::~sdp_media_description_t()
{
}
//--------------------------------------
//
//--------------------------------------
void sdp_media_description_t::decode_transport(const char* transport)
{
	// c=<nettype> <addrtype> <connection-address>
	
	const char* mstr = transport, *cp;
	char net[32];
	int len;

	if ((cp = strchr(mstr, ' ')) == nullptr)
		return;

	len = MIN(PTR_DIFF(cp, mstr), sizeof(net)-1);
	strncpy(net, mstr, len);
	net[len] = 0;
	mstr = cp+1;
	if (std_stricmp(net, "IN") != 0)
		return;

	if ((cp = strchr(mstr, ' ')) == nullptr)
		return;

	len = MIN(PTR_DIFF(cp, mstr), sizeof(net)-1);
	strncpy(net, mstr, len);
	net[len] = 0;
	mstr = cp+1;
	if (std_stricmp(net, "IP4") != 0)
		return;

	cp = strchr(mstr, '/');

	if (cp == nullptr)
		len = (int)MIN(strlen(mstr), sizeof(net)-1);
	else
		len = MIN(PTR_DIFF(cp, mstr), sizeof(net)-1);
	
	strncpy(net, mstr, len);
	net[len] = 0;
	m_address.s_addr = inet_addr(net);
}
//--------------------------------------
//
//--------------------------------------
void sdp_media_description_t::decode_attribute(const char* attr)
{
	// get the attribute type
	const char* cp, *mstr = attr;
	char attr_name[32];
	int len;

	// атрибут без параметров
	if ((cp = strchr(mstr, ':')) == nullptr)
	{
		if (std_stricmp(mstr, "sendonly") == 0)
			m_direction = sdp_direction_sendonly_e;
		else if (std_stricmp(mstr, "recvonly") == 0)
			m_direction = sdp_direction_recvonly_e;
		else if (std_stricmp(mstr, "sendrecv") == 0)
			m_direction = sdp_direction_sendrecv_e;
		else if (std_stricmp(mstr, "inactive") == 0)
			m_direction = sdp_direction_inactive_e;
		return;
	}

	len = MIN(PTR_DIFF(cp, mstr), sizeof(attr_name)-1);
	strncpy(attr_name, mstr, len);
	attr_name[len] = 0;

	mstr = cp+1;

	if (std_stricmp(attr_name, "ptime") == 0)
	{
		m_packet_time = atoi(mstr) ;
	}
	else
	{
		// extract the RTP payload type
		cp = strchr(mstr, ' ');
		if (cp == nullptr)
			return;

		media::PayloadId payload = (media::PayloadId)atoi(mstr);
		mstr = cp+1;
	
		// find the format that matches the payload type
		if (!m_formats.hasFormat(payload))
			return;

		PayloadFormat& fmt = m_formats.getFormat(payload);

		if (std_stricmp(attr_name, "rtpmap") == 0)
		{
			// handle rtpmap attribute
			//a=rtpmap:<payload type> <encoding name>/<clock rate> [/<encoding parameters>]
			if ((cp = strchr(mstr, '/')) == nullptr)
				return;

			// encoding name
			fmt.setEncoding(mstr, PTR_DIFF(cp, mstr));

			if (std_stricmp(fmt.getEncoding(), TELEPHONE_EVENT) == 0)
				fmt.setDtmfFlag(true);
		
			if ((cp = strchr(mstr, '/')) == nullptr)
				return;

			mstr = cp+1;

			// clock rate
			//if (m_media_type == sdp_media_audio)
				fmt.setFrequency(atoi(mstr));

			if ((cp = strchr(mstr, '/')) == nullptr)
				return;

			mstr = cp+1;

			// encoding parameters : audio -> channels, video -> drop
			if (m_media_type == sdp_media_audio_e)
			{
				fmt.setChannelCount(atoi(mstr));
				mstr = cp+1;
			}
			else
			{
				fmt.setParams(mstr, PTR_DIFF(cp, mstr));
			}

			return;
		}
		else if (std_stricmp(attr_name, "fmtp") == 0)
		{
			// handle fmtp attributes
			int len;
			if ((cp = strpbrk(mstr, "\r\n")) == nullptr)
				len = (int)strlen(mstr);
			else
				len = PTR_DIFF(cp, mstr);

			fmt.setFmtp(mstr, len);
		}
	}
	// all other attriutes ignored
	return;
}
//--------------------------------------
//
//--------------------------------------
void sdp_media_description_t::encode_media(rtl::String& stream)
{
	// output media header
	stream << "m=" << m_media;
	
	if (m_port == 0xFFFFFFFF)
		stream << " $ ";
	else
		stream << ' ' << m_port << ' ';

	stream << m_transport;

	for (int i = 0; i < m_formats.getCount(); i++)
	{
		stream << ' ' << m_formats.getAt(i).getId_u8();
	}

	stream << "\r\n";
}
//--------------------------------------
//
//--------------------------------------
void sdp_media_description_t::encode_rtp_formats(rtl::String& stream)
{
	// output attributes for each payload type
	for (int i = 0; i < m_formats.getCount(); i++)
	{
		const PayloadFormat& fmt = m_formats.getAt(i);

		stream << "a=rtpmap:" << fmt.getId_u8() << ' ' << fmt.getEncoding() << '/' << fmt.getFrequency();

		if (fmt.getChannelCount() > 1 && m_media_type == sdp_media_audio_e)
		{
			 stream << '/' << fmt.getChannelCount();
		}
		else
		{
			const rtl::String& ptr = fmt.getParams();
			if (!ptr.isEmpty())
			{
				stream << '/' << ptr;
			}
		}

		stream << "\r\n";

		const rtl::String& ptr = fmt.getFmtp();
		
		if (!ptr.isEmpty())
		{
			stream << "a=fmtp:" << fmt.getId_u8() << ' ' << ptr << "\r\n";
		}	
	}
}
//--------------------------------------
//
//--------------------------------------
void sdp_media_description_t::encode(rtl::String& stream)
{
	if (m_formats.getCount() == 0)
		return;

	encode_media(stream);

	if (m_address.s_addr == INADDR_ANY)
	{
		m_address = m_session->get_connection_address();
	}

	if (std_stricmp(m_transport, SDP_MEDIA_PROFILE_RTP) == 0)
	{
		encode_rtp_formats(stream);
	}

	stream << "a=ptime:20\r\n";

	// media format direction
	if (m_direction != sdp_direction_undefined_e) //  && m_direction != sdp_direction_sendrecv_e
	{
		stream << "a=" << sdp_get_direction_name(m_direction) << "\r\n";
	}
}
//--------------------------------------
//
//--------------------------------------
bool sdp_media_description_t::decode(const char* str)
{
	// parsing
	const char* mstr = str, *cp, *cp2;
	int len;

	// считаем тип медийных данных
	if ((cp = strchr(mstr, ' ')) == nullptr)
		return false;

	len = MIN(PTR_DIFF(cp, str), sizeof(m_media)-1);
	strncpy(m_media, str, len);
	m_media[len] = 0;

	//mstr = cp + 1;
	
	if (std_stricmp(m_media, "video") == 0)
		m_media_type = sdp_media_video_e;
	else if (std_stricmp(m_media, "audio") == 0)
		m_media_type = sdp_media_audio_e;
	else
	{
		m_media_type = sdp_media_unknown_e;
		return false;
	}

	mstr = cp + 1;

	// считаем порт/ы приема/передачи медиа данных
	if ((cp = strchr(mstr, ' ')) != nullptr)
	{
		m_port = (uint16_t)strtoul(mstr, nullptr, 10);
		// проверим колво портов
		cp = strchr(mstr, ' ');
		cp2 = strchr(mstr, '/');

		if (cp2 != nullptr && cp2 < cp)
			m_port_count = (uint16_t)strtoul(cp2+1, nullptr, 10);
		else
			m_port_count = 1;

		// 2014.04.28 George : если порт 0 то нужно оставлять для передачи инфы в пир (видео)
		//if (m_port == 0)
		//	return false;
	}
	else
		return false;

	mstr = cp+1;

	// считаем тип транспорта
	cp = strchr(mstr, ' ');

	if (cp == nullptr)
		return false;

	len = MIN(PTR_DIFF(cp, mstr), sizeof(m_transport)-1);
	strncpy(m_transport, mstr, len);
	m_transport[len] = 0;

	mstr = cp+1;

	// считаем набор форматов
	while (cp = strchr(mstr, ' '))
	{
		char enc[16];
		media::PayloadId payload = (media::PayloadId)atoi(mstr);
		if (PayloadSet::isStandardId(payload))
		{
			if (m_media_type == sdp_media_audio_e)
				m_formats.addStandardFormat(payload);
			else if (m_media_type == sdp_media_video_e)
				m_formats.addStandardFormat(payload);
		}
		else
		{
			std_snprintf(enc, 16, "%d", payload);
			m_formats.addFormat(enc, payload, 8000, 0, 0);
		}

		mstr = cp+1;
	}

	if (mstr[0] != 0)
	{
		media::PayloadId payload = (media::PayloadId)atoi(mstr);
		char enc[16];
		if (PayloadSet::isStandardId(payload))
		{
			if (m_media_type == sdp_media_audio_e)
				m_formats.addStandardFormat(payload);
			else if (m_media_type == sdp_media_video_e)
				m_formats.addStandardFormat(payload);
		}
		else
		{
			std_snprintf(enc, 16, "%d", payload);
			m_formats.addFormat(enc, payload, 8000, 0, 0);
		}
		mstr = cp+1;
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool sdp_media_description_t::decode_line(char name, const char* value)
{
	switch (name)
	{
	case 'i' : // media title
	case 'b' : // bandwidth information
	case 'k' : // encryption key
		break;
	case 'c' : // connection information - optional if included at session-level
		decode_transport(value);
		break;
	case 'a' : // zero or more media attribute lines
		decode_attribute(value);
		break;
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
in_addr sdp_media_description_t::get_address() const
{
	in_addr conn_addr = { 0 };

	conn_addr = m_address.s_addr == INADDR_ANY && m_session != nullptr ? m_session->get_connection_address() : m_address;

	// если неудача то запишем адрес из сессии

	if (conn_addr.s_addr == INADDR_ANY)
	{
		conn_addr = m_session->get_connection_address();
	}

	return conn_addr;
}
//--------------------------------------
//
//--------------------------------------
in_addr sdp_media_description_t::get_address(in_addr mask) const
{
	in_addr conn_addr = { 0 };

	conn_addr = m_address.s_addr == INADDR_ANY && m_session != nullptr ? m_session->get_connection_address() : m_address;

	// если неудача то запишем адрес из сессии
	if (conn_addr.s_addr == INADDR_ANY)
	{
		//LOG_CALL(L"sdp", L"get address use session default 2");
		conn_addr = m_session->get_connection_address();
	}

	return conn_addr;
}
//--------------------------------------
//
//--------------------------------------
void sdp_media_description_t::set_media_formats(const PayloadSet& formats)
{
	m_formats.removeAll();

	for (int i = 0; i < formats.getCount(); i++)
	{
		m_formats.addFormat(formats.getAt(i));
		PayloadFormat& fmt = m_formats.getAt(i);

		if (fmt.getId() == PayloadId::Dynamic)
			fmt.setId((PayloadId)m_session->get_next_dynamic_payload());
	}
}
//--------------------------------------
//
//--------------------------------------
int sdp_media_description_t::add_media_format(const PayloadFormat& mediaFormat)
{
	int payloadId = mediaFormat.getId_u8();

	if (m_formats.addFormat(mediaFormat))
	{
		PayloadFormat& fmt = m_formats.getAt(m_formats.getCount()-1);

		if (fmt.getId() == PayloadId::Dynamic)
		{
			fmt.setId((PayloadId)(payloadId = m_session->get_next_dynamic_payload()));
		}
	}

	return payloadId;
}
//--------------------------------------
//
//--------------------------------------
void sdp_media_description_t::remove_media_formats()
{
	m_formats.removeAll();
}
//--------------------------------------
//
//--------------------------------------
sdp_direction_type_t sdp_media_description_t::get_direction() const
{
	return (m_direction == sdp_direction_undefined_e) ?
		(m_session != nullptr) ? m_session->get_direction() : sdp_direction_sendrecv_e :
		m_direction;
}
//--------------------------------------
