/* RTX H.248 Media Gate. SIP Softphone
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "phone_aux_stream.h"
//-----------------------------------------------
//
//-----------------------------------------------
class phone_device_term_t : public h248::IAudioDeviceInput, public h248::IAudioDeviceOutput, public aux_device_in_handler_t
{
	h248::IMediaContext* m_context;
	uint32_t m_term_id;

	h248::IAudioDeviceInputCallback* m_dev_in__callback;
	phone_aux_stream_t m_aux_stream;

public:
	phone_device_term_t();
	~phone_device_term_t();

	bool create(h248::IMediaContext* context);
	void destroy();

	bool start_device();
	void stop_device();

	bool isStarted() { return m_aux_stream.isStarted(); }

	bool set_devices(const char* output, const char* input) { return m_aux_stream.set_devices(output, input); }
	void set_output(const char* output) { m_aux_stream.set_output(output); }
	void set_input(const char* input) { m_aux_stream.set_input(input); }

	const char* get_output() { return m_aux_stream.get_output(); }
	const char* get_input() { return m_aux_stream.get_input(); }

	void mute(bool value) { m_aux_stream.mute(value); }

private:

	// <IAudioDeviceInput>
	virtual uintptr_t deviceInput__addHandler(h248::IAudioDeviceInputCallback* cb, uint32_t freq, uint32_t channels);
	virtual void deviceInput__removeHandler(uintptr_t device);

	// <IAudioDeviceOutput>
	virtual uintptr_t deviceOutput__addHandler(uint32_t freq, uint32_t channels);
	virtual void deviceOutput__removeHandler(uintptr_t device);
	virtual void deviceOutput__playAudio(uintptr_t device, const uint8_t* data, uint32_t len, uint16_t seq_no);

	// <aux_device_in_handler_t>
	virtual void aux_in_data_ready(const short* mediaData, int size);
};
