/* RTX H.248 Media Gate. SIP Softphone
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "phone_rtp_term.h"
#include "phone_dev_term.h"
//-----------------------------------------------
//
//-----------------------------------------------
class phone_context_t : public h248::ITerminationEventHandler
{
	rtl::Mutex m_sync;
	h248::IMediaContext* m_ctx;
	rtl::ArrayT<phone_rtp_term_t*> m_rtp_term_list;
	phone_device_term_t m_dev_term;
	bool m_recording;

public:
	// public interface
	bool initialize();
	void release();

	void set_devices(const char* output, const char* input) { m_dev_term.set_devices(output, input); }
	void set_output(const char* output) { m_dev_term.set_output(output); }
	void set_input(const char* input) { m_dev_term.set_input(input); }
	void mute(bool value) { m_dev_term.mute(value); }

	const char* get_output() { return m_dev_term.get_output(); }
	const char* get_input() { return m_dev_term.get_input(); }

	phone_rtp_term_t* make_rtp_term(in_addr iface);
	void free_rtp_term(phone_rtp_term_t* term);
	void mute_rtp_term(phone_rtp_term_t* term, bool mute);

	void start_recording(const char* filepath);
	void stop_recording();

private:
	friend class rtx_phone;

	phone_context_t();
	~phone_context_t();

	/// internal routines
	bool start();
	void stop();

	bool start_dev_term();
	void stop_dev_term();

	phone_rtp_term_t* create_rtp_term(in_addr iface);
	void destroy_rtp_term(phone_rtp_term_t* term);
	bool rtp_term_contained(phone_rtp_term_t* term, int& index);

	/// ITerminationEventHandler
	virtual void TerminationEvent_callback(h248::EventParams* params) override;
};
//-----------------------------------------------
