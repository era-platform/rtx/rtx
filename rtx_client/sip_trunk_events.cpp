/* RTX H.248 Media Gate. SIP Softphone
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_account.h"
#include "sip_trunk.h"
#include "phone_media_engine.h"
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::call_session__recv_1xx(sip_call_session_t& session, const sip_message_t& message)
{
	LOG_EVENT("trunk", "id:%p : session event : trunk-ringing", this);

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (session.getType() == sip_session_client_e)
	{
		if (m_call_state == sip_state_calling_e || m_call_state == sip_state_progress_e || m_call_state == sip_state_ringing_e)
		{
			sip_status_t code = message.get_response_code();

			m_call_state = code == sip_180_Ringing ? sip_state_ringing_e : sip_state_progress_e;
			g_sip_client->raise_call_ringing(this, message.get_response_code(), message.get_response_reason(), message.has_sdp());
		}
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::call_session__call_established(sip_call_session_t& session, const sip_message_t& message)
{
	LOG_EVENT("trunk", "id:%p : session event : 200 Ok sent/received (trunk-connected)", this);

	//rtl::String msg_str = message.to_string();
	

	g_sip_client->raise_call_answered(this/*, msg_str*/);
}
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::call_session__call_disconnected(sip_call_session_t& session, const sip_message_t& message)
{
	LOG_EVENT("trunk", "id:%p : trunk disconnected event : state:%s pass to async call", this, get_sip_trunk_state_name(m_call_state));

	if (m_call_state == DisconnectByeTX)
		return;

	sip_disconnect_info_t* call_info = NEW sip_disconnect_info_t;

	call_info->session = &session;
	call_info->message = NEW sip_message_t(message);

	rtl::async_call_manager_t::callAsync(this, SIP_TRUNK_DISCONNECTED, 0, call_info); // ACMD_DISCONNECT
}
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::call_session__recv_ack(sip_call_session_t& session, const sip_message_t& msg)
{
	LOG_CALL("trunk", "id:%p : session event : trunk-received-ACK : state %s", this, get_sip_trunk_state_name(m_call_state));

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (m_call_state == sip_state_connecting_e)
	{
		m_call_state = sip_state_connected_e;
		g_sip_client->raise_call_connected(this);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::call_session__ack_sent(sip_call_session_t& session)
{
	LOG_CALL("trunk", "id:%p : session event : trunk-ACK-sent : state %s", this, get_sip_trunk_state_name(m_call_state));

	if (m_call_state >= sip_state_calling_e && m_call_state <= sip_state_connecting_e)
	{
		m_call_state = sip_state_connected_e;
		g_sip_client->raise_call_connected(this);
	}
}
//--------------------------------------
//
//--------------------------------------
reinvite_response_t  sip_trunk_t::call_session__recv_reinvite(sip_call_session_t& session, const sip_message_t& offer)
{
	LOG_EVENT("trunk", "id:%p : session event : trunk-reinvite", this);

	return reinvite_response_ok_e;
}
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::call_session__call_reestablished(sip_call_session_t& session, const sip_message_t& message)
{
	LOG_EVENT("trunk", "id:%p : session event : trunk-reconnected", this);

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	// �������� �� 488!

	if (message.get_response_code() == sip_488_NotAcceptableHere)
	{
		LOG_CALL("trunk", "id:%p : trunk-reconnected : reinvite canceled!", this);
		m_call_state = sip_state_connected_e;
	}
	else if (message.get_response_code() == sip_491_RequestPending)
	{
		LOG_CALL("trunk", "id:%p : trunk-reconnected : reinvite pending!", this);

		// Send invite asynchroniusly
		rtl::async_call_manager_t::callAsync(this, 1002, 100, 0); // ACMD_REINVITE
	}
	else if (m_call_state == sip_state_connected_e)
	{
		LOG_CALL("trunk", "id:%p : trunk-reconnected : reinvite accepted!", this);
	}
}
//--------------------------------------
//
//--------------------------------------
bool sip_trunk_t::call_session__apply_sdp_offer(sip_call_session_t& session, const sip_message_t& offer)
{
	LOG_EVENT("trunk", "id:%p : session event : trunk-incoming-sdp-offer : session %p/%p", this, m_call_session, &session);

	if (m_call_session == nullptr)
	{
		LOG_ERROR("trunk", "id:%p : session event : trunk-incoming-sdp-offer : failed : call session is null", this);
		return false;
	}

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (!create_media_session())
	{
		LOG_ERROR("trunk", "id:%p : session event : trunk-incoming-sdp-offer : failed : media session invalid state", this);
		return false;
	}

	if (!m_media_session->set_remote_sdp((const char*)offer.get_body()))
	{
		destroy_media_session();
		return false;
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool sip_trunk_t::call_session__apply_sdp_answer(sip_call_session_t& session, const sip_message_t& answer)
{
	LOG_EVENT("trunk", "id:%p : session event : trunk-incoming-sdp-answer", this);

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	// ����������� ����� �� ���� ������������ ���� ��� ������ �� ��������� �������� �����!
	if (m_media_session == nullptr)
	{
		LOG_ERROR("trunk", "id:%p : session event : trunk-incoming-sdp-answer : failed : media session invalid state", this);
		return false;
	}

	if (!m_media_session->set_remote_sdp((const char*)answer.get_body()))
	{
		destroy_media_session();
		return false;
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool sip_trunk_t::call_session__prepare_sdp_offer(sip_call_session_t& session, sip_message_t& offer)
{
	LOG_EVENT("trunk", "id:%p : session event : trunk-require-sdp-offer : session %p/%p", this, m_call_session, &session);
	
	if (m_call_session == nullptr)
	{
		LOG_ERROR("trunk", "id:%p : trunk-require-sdp-offer : failed : call session is null", this);
		return false;
	}

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (!create_media_session())
	{
		LOG_ERROR("trunk", "id:%p : trunk-require-sdp-offer : failed : media session invalid state", this);
		return false;
	}
	
	rtl::String local_sdp;
	// ���������� ����������� � �������� �����
	if (!m_media_session->get_local_sdp(local_sdp))
	{
		destroy_media_session();
		return false;
	}

	offer.make_Content_Type_value()->assign("application/sdp");
	offer.set_body(local_sdp);

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool sip_trunk_t::call_session__prepare_sdp_answer(sip_call_session_t& session, sip_message_t& answer)
{
	LOG_EVENT("trunk", "id:%p : session event : trunk-require-sdp-answer", this);

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (m_media_session == nullptr)
	{
		LOG_ERROR("trunk", "id:%p : session event : trunk-require-sdp-answer : failed : media session invalid state", this);
		return false;
	}

	rtl::String local_sdp;

	// ��������. ������� ������� � �������� ����� ???? ���� ������� ����� ����� ��������������!!!!
	if (!m_media_session->get_local_sdp(local_sdp))
	{
		destroy_media_session();
		return false;
	}

	// write sdp to answer message
	answer.make_Content_Type_value()->assign("application/sdp");
	answer.set_body(local_sdp);

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool sip_trunk_t::call_session__apply_sdp_reoffer(sip_call_session_t& session, const sip_message_t& offer)
{
	LOG_EVENT("trunk", "id:%p : session event : trunk-incoming-sdp-offer-reinvite...", this);

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (m_media_session == nullptr)
	{
		LOG_ERROR("trunk", "id:%p : session event : trunk-incoming-sdp-offer-reinvite : failed : media session invalid state", this);
		return false;
	}

	return m_media_session->set_remote_sdp((const char*)offer.get_body());
}
//--------------------------------------
//
//--------------------------------------
bool sip_trunk_t::call_session__apply_sdp_reanswer(sip_call_session_t& session, const sip_message_t& answer)
{
	LOG_EVENT("trunk", "id:%p : session event : trunk-incoming-sdp-answer-reinvite...", this);

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (m_media_session == nullptr)
	{
		LOG_ERROR("trunk", "id:%p : session event : trunk-incoming-sdp-answer-reinvite : failed : media session invalid state", this);
		return false;
	}

	return m_media_session->reset_remote_sdp((const char*)answer.get_body());
}
//--------------------------------------
//
//--------------------------------------
bool sip_trunk_t::call_session__prepare_sdp_reoffer(sip_call_session_t& session, sip_message_t& offer, bool hold)
{
	LOG_EVENT("trunk", "id:%p : session event : trunk-require-sdp-offer-reinvite...", this);

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (m_media_session == nullptr)
	{
		LOG_ERROR("trunk", "id:%p : session event : trunk-require-sdp-offer-reinvite : failed : media session invalid state", this);
		return false;
	}

	rtl::String local_sdp;
	bool res = m_media_session->reget_local_sdp(hold, local_sdp);

	// ���������� ����������� � �������� �����
	if (res)
	{
		offer.make_Content_Type_value()->assign("application/sdp");
		offer.set_body(local_sdp);
	}

	return res;
}
//--------------------------------------
//
//--------------------------------------
bool sip_trunk_t::call_session__prepare_sdp_reanswer(sip_call_session_t& session, sip_message_t& answer)
{
	LOG_EVENT("trunk", "id:%p : session event : trunk-require-sdp-answer-reinvite...", this);
	
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (m_media_session == nullptr)
	{
		LOG_ERROR("trunk", "id:%p : session event : trunk-require-sdp-offer-reinvite : failed : media session invalid state", this);
		return false;
	}

	rtl::String local_sdp;
	bool res = false;

	// ���������� ����������� � �������� �����
	if (res = m_media_session->reget_local_sdp(false, local_sdp))
	{
		answer.make_Content_Type_value()->assign("application/sdp");
		answer.set_body(local_sdp);
	}

	return res;
}
//--------------------------------------
//
//--------------------------------------
bool sip_trunk_t::call_session__recv_ni_request(sip_call_session_t& session, const sip_message_t& request, sip_message_t& response)
{
	sip_method_type_t method = request.get_request_method();

	const rtl::String& smethod = request.get_request_method_name();
	rtl::String sline = request.get_request_uri().to_string();

	LOG_EVENT("trunk", "id:%p : session event : trunk-recv-ni-request : %s", this, (const char*)smethod, (const char*)sline);

	switch (method)
	{
	case sip_INFO_e:
		process_info_request(request, response);
		break;
	case sip_REFER_e:
		process_refer_request(request, response);
		break;
	case sip_NOTIFY_e:
		process_notify_request(request, response);
		break;
	default:
		LOG_EVENT("trunk", "id:%p : session event : trunk-recv-ni-request : not processed -- response 200 ok", this);
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::call_session__recv_ni_response(sip_call_session_t& session, const sip_message_t& response)
{
	const rtl::String& method = response.CSeq_Method();

	if (method == "REFER")
	{
		process_refer_response(response);
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::call_session__failure(sip_call_session_t& session)
{
	process_call_disconnected_event(false);
}
//--------------------------------------
//
//--------------------------------------
bool sip_trunk_t::create_media_session()
{
	if (m_media_session != nullptr)
		return false;


	in_addr local = m_profile.get_interface();
	m_media_session = rtx_phone::context.make_rtp_term(local);

	if (m_media_session != nullptr)
	{
		PayloadSet set;
		m_account.get_audio_set(set);
		m_media_session->set_allowed_codecs(set);
	}

	return m_media_session != nullptr;
}
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::destroy_media_session()
{
	if (m_media_session != nullptr)
	{
		rtx_phone::context.free_rtp_term(m_media_session);
		m_media_session = nullptr;
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::async_session_call_disconnected(sip_disconnect_info_t* info)
{
	LOG_EVENT("trunk", "id:%p : session event : trunk-disconnected : state %s", this, get_sip_trunk_state_name(m_call_state));

	try
	{
		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		if (m_call_state != sip_state_new_e)
		{
			int callEndReason = info->session->GetCallEndReason();
			bool isFromRemote = false;

			switch (callEndReason)
			{
			case sip_call_session_t::ICT_Recv3xx:
			case sip_call_session_t::ICT_Recv4xx:
			case sip_call_session_t::ICT_Recv5xx:
			case sip_call_session_t::ICT_Recv6xx:
				m_call_end_reason = DisconnectRejectRX;
				isFromRemote = true;
				break;
			case sip_call_session_t::IST_3xxSent:
			case sip_call_session_t::IST_4xxSent:
			case sip_call_session_t::IST_5xxSent:
			case sip_call_session_t::IST_6xxSent:
				m_call_end_reason = DisconnectRejectTX;
				isFromRemote = false;
				break;
			case sip_call_session_t::NICT_ByeSent:
				m_call_end_reason = DisconnectByeTX;
				isFromRemote = false;
				break;
			case sip_call_session_t::NICT_CancelSent:
				m_call_end_reason = DisconnectCancelTX;
				isFromRemote = false;
				break;
			case sip_call_session_t::NIST_RecvBye:
				m_call_end_reason = DisconnectByeRX;
				isFromRemote = true;
				break;
			case sip_call_session_t::NIST_RecvCancel:
				m_call_end_reason = DisconnectCancelRX;
				isFromRemote = true;
				break;
			}

			if (isFromRemote)
			{
				if (info->message->IsValid() && !info->message->Is2xx() && info->message->CSeq_Method().toUpper() == "INVITE")
				{
					process_call_rejected_event(info->message->get_response_code(),
						info->message->get_response_reason());
				}
			}

			process_call_disconnected_event(false);

		}
		else
		{
			LOG_WARN("trunk", "id:%p : trunk-disconnected : invalid state", this);
		}
	}
	catch (rtl::Exception& ex)
	{
		LOG_ERROR("trunk", "id:%p : trunk-disconnected : failed with exception : %s", this, ex.get_message());
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::process_info_request(const sip_message_t& info, sip_message_t& response)
{
	// �� ������������ � ���������� ������
	//rtl::String digit;
	//int duration = 0;

	//if (info.get_Content_Type()->to_string() *= "application/dtmf-relay")
	//{
	//	rtl::String body((const char*)info.get_body(), info.get_body_length());
	//	rtl::StringList params;
	//	body.split(params, "\n");
	//	if (params.getCount() > 0)
	//	{
	//		for (int i = 0; i < params.getCount(); i++)
	//		{
	//			rtl::StringList param;
	//			params[i].split(param, "=");
	//			if (param.getCount() == 2)
	//			{
	//				if (param[0].trim() *= "Signal")
	//					digit = param[1].trim();
	//				else if (param[0].trim() *= "Duration")
	//					duration = strtoul(param[1].trim(), nullptr, 10);
	//			}
	//		}

	//		OnReceivedDTMF(digit, duration);
	//	}
	//}
	//else if (info.get_Content_Type()->to_string() *= "application/dtmf")
	//{
	//	rtl::String param((const char*)info.get_body(), info.get_body_length());
	//	if (!param.isEmpty())
	//	{
	//		digit = param;
	//		OnReceivedDTMF(digit, 160);
	//	}
	//}
}
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::process_refer_request(const sip_message_t& refer, sip_message_t& response)
{
	LOG_CALL("trunk", "id:%p : processing REFER request", this);

	/// event type
	const sip_value_t* r_event = refer.get_Event_value();

	if (r_event == nullptr || r_event->value() != "refer")
	{
		response.set_response_line(sip_400_BadRequest);
		response.make_Reason_value()->assign("Request has no 'Event' header or has invalid package name");
		return;
	}

	/// refer-to
	const sip_value_uri_t* refer_to = refer.get_Refer_To_value();

	if (refer_to == nullptr)
	{
		response.set_response_line(sip_400_BadRequest);
		response.make_Reason_value()->assign("Request has no 'Refer-To' header");
		return;
	}

	if (refer.get_Refer_To()->get_value_count() > 1)
	{
		response.set_response_line(sip_400_BadRequest);
		response.make_Reason_value()->assign("Request has multiply 'Refer-To' headers");
		return;
	}

	/// + optional 'Referred-By' header

	m_account.process_refer_request(this, refer_to, refer.get_Referred_By_value());
}
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::process_refer_response(const sip_message_t& response)
{
	LOG_CALL("trunk", "id:%p : processing REFER response", this);

	m_account.process_refer_response(this, response.get_response_code(), response.get_response_reason());
}
//--------------------------------------
//
//--------------------------------------
void sip_trunk_t::process_notify_request(const sip_message_t& notify, sip_message_t& response)
{
	LOG_CALL("trunk", "id:%p : processing NOTIFY request", this);
	
	const sip_value_t* n_event = notify.get_Event_value();
	
	if (n_event == nullptr)
	{
		response.set_response_line(sip_489_BadEvent);
		response.make_Reason_value()->assign("Request has no 'Event' header");

		LOG_CALL("trunk", "id:%p : NOTIFY request has no 'Event' header field! Ignored", this);
		return;
	}

	if (n_event->value() != "refer")
	{
		response.set_response_line(sip_489_BadEvent);
		response.make_Reason_value()->assign("Packet has invalid package name in 'Event' header");

		LOG_CALL("trunk", "id:%p : NOTIFY request has invalid package name '%s' in 'Event' header", this, (const char*)n_event->value());
		return;
	}
		
	/// get the subscription state
	const sip_value_t* state = notify.get_Subscription_State_value();

	if (state == nullptr)
	{
		response.set_response_line(sip_489_BadEvent);
		response.make_Reason_value()->assign("Request has no 'Subscribe-state' header");
		return;
	}

	rtl::String state_type = state->value(), state_param;

	if (state_type == "active")
	{
		state_param = state->param_get("expires");
	}
	else if (state->value() == "terminated")
	{
		state_param = state->param_get("reason");
	}
	else
	{
		response.set_response_line(sip_489_BadEvent);
		response.make_Reason_value()->assign("Request has unknown 'Subscribe-state' header value");
		return;
	}

	const sip_value_t* contentType = notify.get_Content_Type_value();

	if (contentType == nullptr || contentType->value() != "message/sipfrag")
	{
		response.set_response_line(sip_489_BadEvent);
		response.make_Reason_value()->assign("Request has no 'message/sipfrag' body");
		return;
	}

	/// get the SIP fragment from the body
	rtl::String sip_frag((const char*)notify.get_body(), notify.get_body_length());

	if (!m_account.process_notify_request(this, state_type, state_param, sip_frag))
	{
		response.set_response_line(sip_481_TransactionDoesNotExist, "Subscription does not exist");
	}
}
//--------------------------------------
