/* RTX H.248 Media Gate. SIP Softphone
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_client.h"
#include "sip_account.h"
#include "sip_trunk.h"
#include "phone_media_engine.h"
//--------------------------------------
// ��������� ������� ��� ������������
//--------------------------------------
void raise_account_registered(uintptr_t account_id);
void raise_account_unregistered(uintptr_t account_id, bool by_user);
void raise_account_incoming_call(uintptr_t account_id, uintptr_t trunk_id, const char* called_id, const char* called_name, const char* caller_id, const char* caller_name/*, const char* invite*/);
void raise_account_named_event(uintptr_t account_id, const char* event_name, const char* event_parameter);
void raise_call_ringing(uintptr_t trunk_id, uint16_t code, const char* reason, bool with_media);
void raise_call_answered(uintptr_t trunk_id/*, const char* sip_message*/);
void raise_call_rejected(uintptr_t trunk_id, uint16_t code, const char* reason);
void raise_call_connected(uintptr_t trunk_id);
void raise_call_disconnected(uintptr_t trunk_id, enum DisconnectReason reason);
void raise_call_refer_notify(uintptr_t trunk, ReferNotifyType ntype, const char* message);
void raise_call_replaced_by_refer(uintptr_t trunk_old, uintptr_t trunk_new, const char* called_id, const char* called_name, const char* caller_id, const char* caller_name);
void raise_call_notify_received(uintptr_t trunk_id, const char* event_type, const char* event_param, const char* sipfrag);
void raise_call_play_stopped(uintptr_t trunk_id);

//--------------------------------------
//
//--------------------------------------
sip_client_t::sip_client_t() : m_sync("sip-client"), m_initialized(false), m_call_manager(nullptr), m_reg_manager(nullptr)
{
	m_default_listener_port = 0;
}
//--------------------------------------
//
//--------------------------------------
sip_client_t::~sip_client_t()
{
	destroy();
}
//--------------------------------------
//
//--------------------------------------
bool sip_client_t::initialize()
{
	in_addr if_address;
	char if_address_str[MAX_PATH];
	uint32_t if_port = 0;
	//bool enable_tcp;

	if_address.s_addr = INADDR_ANY;
	memset(if_address_str, 0, sizeof(if_address_str));

	if (read_config_string(STDKEY_SIP_LISTENER_INTERFACE, if_address_str, MAX_PATH))
	{

		if_address = net_parse(if_address_str);

		if (if_address.s_addr == INADDR_NONE)
			if_address.s_addr = INADDR_ANY;
	}
	else
	{
		if_address.s_addr = INADDR_ANY;
	}

	// �� ��������� 5080
	// ���� ����� �� ���� �� ������� ������������ ��� (���� �� �������� ����� ���������)
	// ���� ���� �� ����� ���������

	if (read_config_integer(STDKEY_SIP_LISTENER_PORT, if_port))
	{
		m_default_listener_port = if_port;
	}

	LOG_CALL("client", "initialize SIP client with parameters:\r\n\
\t\t\tinterface:       %s\r\n\
\t\t\tport:            %u\r\n\
\t\t\tenable-tcp:      %s", inet_ntoa(if_address), m_default_listener_port, STR_BOOL(false));

	m_call_manager = NEW sip_call_manager_t();
	m_reg_manager = NEW sip_reg_manager_t();

	sip_user_agent_t::initialize(4, 4);

	if (m_default_listener_port == 0)
	{
		net_socket_t sock;
		if (!sock.open(AF_INET, SOCK_DGRAM, IPPROTO_UDP))
		{
			int last_error = std_get_error;
			LOG_ERR_MSG("ua", last_error, "opening default local port failed");
			return false;
		}

		sockaddr_in addr;
		memset(&addr, 0, sizeof addr);
		addr.sin_family = AF_INET;
		addr.sin_addr.s_addr = INADDR_ANY;
		addr.sin_port = 0;

		bool result = sock.bind((sockaddr*)&addr, sizeof addr);

		if (!result)
		{
			int last_error = std_get_error;
			LOG_ERR_MSG("ua", last_error, "binding default local port failed");
			return false;
		}

		sockaddrlen_t len = sizeof addr;
		sock.get_interface((sockaddr*)&addr, &len);
		
		sock.close();

		m_default_listener_port = ntohs(addr.sin_port);

		LOG_CALL("client", "default listen port is %s:%u", inet_ntoa(addr.sin_addr), m_default_listener_port);
	}

	sip_user_agent_t::add_transport(sip_transport_udp_e, if_address, m_default_listener_port);
	sip_user_agent_t::add_transport(sip_transport_tcp_e, if_address, m_default_listener_port);

	char tmp[255] = "RTX test phone";
	rtl::String name;

	if (!read_config_string(STDKEY_SIP_UA_NAME, tmp, 255))
	{
		name << "RTX test phone " << API_VERSION_MAJOR << '.' << API_VERSION_MINOR << '.' << API_VERSION_TEST << '.' << API_VERSION_BUILD << ' ' << __DATE__;
	}
	else
	{
		name << tmp << " RTX " << API_VERSION_MAJOR << '.' << API_VERSION_MINOR << '.' << API_VERSION_TEST << '.' << API_VERSION_BUILD << ' ' << __DATE__;
	}

	set_UA_name(name);

	m_sip_stack.begin_work();

	return m_initialized = true;
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::terminate()
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	for (int i = 0; i < m_account_list.getCount(); i++)
	{
		sip_account_t* account = m_account_list[i];
		account->destroy();
		DELETEO(account);
	}

	m_account_list.clear();

	sip_user_agent_t::destroy();

	if (m_call_manager != nullptr)
	{
		m_call_manager->stop();
		DELETEO(m_call_manager);
		m_call_manager = nullptr;
	}

	if (m_reg_manager != nullptr)
	{
		m_reg_manager->stop();
		DELETEO(m_reg_manager);
		m_reg_manager = nullptr;
	}
}
//--------------------------------------
//
//--------------------------------------
uintptr_t sip_client_t::create_account(const char* username, const char* domain)
{
	LOG_CALL("sip", "create account %s@%s...", username, domain);

	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	for (int i = 0; i < m_account_list.getCount(); i++)
	{
		sip_account_t* account = m_account_list[i];

		if (::strcmp(username, account->get_username()) == 0 && std_stricmp(domain, account->get_domain()) == 0)
		{
			LOG_WARN("sip", "account already exist!");
			// ����� ������ ��� ������!
			return (uintptr_t)account;
		}
	}

	sip_account_t* new_account = NEW sip_account_t(username, domain);
	
	m_account_list.add(new_account);

	uintptr_t account_id = (uintptr_t)new_account;

	LOG_CALL("sip", "account created id:%p", account_id);

	return account_id;
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::destroy_account(uintptr_t account_id)
{
	LOG_CALL("sip", "destroy account id:%p...", account_id);

	sip_account_t* found = nullptr;

	{
		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);
		for (int i = 0; i < m_account_list.getCount(); i++)
		{
			sip_account_t* account = m_account_list[i];

			if (account_id == (uintptr_t)account)
			{
				found = account;
				m_account_list.removeAt(i);
				break;
			}
		}
	}

	// ���� ����� �� ������� ��� ��� � ��� ��������
	if (found != nullptr)
	{
		found->destroy();
		DELETEO(found);

		LOG_CALL("sip", "account destroyed");
	}
	else
	{
		LOG_WARN("sip", "account NOT found!");
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::set_local_port(uintptr_t account_id, uint16_t port)
{
	LOG_CALL("sip", "set account local port id:%p localport:%u...", account_id, port);

	sip_account_t* account = get_account(account_id);

	if (account != nullptr)
	{
		account->set_local_port(port);

		LOG_CALL("sip", "account proxy address set");
	}
	else
	{
		LOG_WARN("sip", "invalid account id!");
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::set_proxy_address(uintptr_t account_id, const char* address, uint16_t port, sip_transport_type_t type)
{
	LOG_CALL("sip", "set account proxy address id:%p address:%s:%u %s...", account_id, address, port, TransportType_toString(type));

	sip_account_t* account = get_account(account_id);

	if (account != nullptr)
	{
		account->set_proxy_address(address, port, type);
	}
	LOG_CALL("sip", "account proxy address set");
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::set_keepalive(uintptr_t account_id, int keepalive_interval)
{
	LOG_CALL("sip", "set account keep-alive id:%p enable:%s interval %d sec...", account_id, STR_BOOL(keepalive_interval > 0), keepalive_interval);

	sip_account_t* account = get_account(account_id);

	if (account != nullptr)
	{
		account->set_keepalive(keepalive_interval);
	}

	LOG_CALL("sip", "account proxy address set");
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::set_authentication(uintptr_t account_id, const char* auth_id, const char* auth_pwd)
{
	LOG_CALL("sip", "set account authentication id:%p auth_id:%s auth_pwd:*** ...", account_id, auth_id);

	sip_account_t* account = get_account(account_id);

	if (account != nullptr)
	{
		account->set_authentication(auth_id, auth_pwd);
	}

	LOG_CALL("sip", "account authentication set");
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::set_displayname(uintptr_t account_id, const char* displayname)
{
	LOG_CALL("sip", "set account displayname id:%p name:%s...", account_id, displayname);

	sip_account_t* account = get_account(account_id);

	if (account != nullptr)
	{
		account->set_displayname(displayname);
	}

	LOG_CALL("sip", "account authentication set");
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::set_expires(uintptr_t account_id, int expires)
{
	LOG_CALL("sip", "set account expires time id:%p expires:%u...", account_id, expires);

	sip_account_t* account = get_account(account_id);

	if (account != nullptr)
	{
		account->set_expires(expires);
	}
	LOG_CALL("sip", "account expires time set");
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::start_registration(uintptr_t account_id)
{
	LOG_CALL("sip", "start account registration id:%p...", account_id);

	sip_account_t* account = get_account(account_id);

	if (account != nullptr)
	{
		account->start_registration();
	}
	LOG_CALL("sip", "account registration started");
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::start_unregistration(uintptr_t account_id)
{
	LOG_CALL("sip", "start account unregistration id:%p...", account_id);

	sip_account_t* account = get_account(account_id);

	if (account != nullptr)
	{
		account->start_unregistration();
	}
	LOG_CALL("sip", "account unregistration started");
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::set_payload_set(uintptr_t account_id, const PayloadSet& payloads)
{
	LOG_CALL("sip", "set account audio codec list id:%p...", account_id);

	sip_account_t* account = get_account(account_id);

	if (account != nullptr)
	{
		account->set_audio_set(payloads);
	}

	LOG_CALL("sip", "account audio codec list set");
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::send_text_message(uintptr_t account_id, const char* to_username, const char* to_displayname, const char* content_type, const char* message_text)
{
	LOG_CALL("sip", "sending text message from id:%p to %s %s %s...", account_id, to_username, to_displayname, content_type);

	sip_account_t* account = get_account(account_id);

	if (account != nullptr)
	{
		account->send_text_message(to_username, to_displayname, content_type, message_text);
	}
	LOG_CALL("sip", "text message sent");
}
//--------------------------------------
//
//--------------------------------------
uintptr_t sip_client_t::create_trunk(uintptr_t account_id)
{
	LOG_CALL("sip", "create call for acount id:%p...", account_id);

	sip_trunk_t* trunk = nullptr;
	sip_account_t* account = get_account(account_id);

	if (account != nullptr)
	{
		if ((trunk = account->create_trunk()) != nullptr)
		{
			add_trunk_ptr(trunk);
		}
	}

	uintptr_t trunk_id = (uintptr_t)trunk;

	LOG_CALL("sip", "call id:%p created", trunk_id);

	return trunk_id;
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::destroy_trunk(uintptr_t trunk_id)
{
	LOG_CALL("sip", "destroy call id:%p...", trunk_id);

	sip_trunk_t* found = nullptr;
	
	{
		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		for (int i = m_trunk_list.getCount() - 1; i >= 0; i--)
		{
			sip_trunk_t* trunk = m_trunk_list[i];

			if (trunk_id == (uintptr_t)trunk)
			{
				found = trunk;
				m_trunk_list.removeAt(i);
				break;
			}
		}
	}

	if (found != nullptr)
	{
		found->get_account().remove_trunk(found);
	}

	LOG_CALL("sip", "call destroyed");
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::add_trunk_ptr(sip_trunk_t* trunk)
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);
	m_trunk_list.add(trunk);
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::remove_trunk_ptr(sip_trunk_t** trunk_list, int count)
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	int removed = 0;

	for (int i = m_trunk_list.getCount() - 1; i >= 0; i--)
	{
		sip_trunk_t* comparer = m_trunk_list[i];

		for (int j = 0; j < count; j++)
		{
			const sip_trunk_t* trunk = trunk_list[j];

			if (comparer == trunk)
			{
				m_trunk_list.removeAt(i);
				removed++;
				break;
			}
		}

		if (removed == count)
		{
			// ������� ���
			break;
		}
	}
}
//--------------------------------------
//
//--------------------------------------
bool sip_client_t::trunk_call_to(uintptr_t trunk_id, const char* number, const char* caller_id, const char* caller_name)
{
	LOG_CALL("sip", "call to id:%p number:%s caller-id:%s caller-name:%s...", trunk_id, number, caller_id, caller_name);

	sip_trunk_t* trunk = get_trunk(trunk_id);

	bool result = trunk != nullptr ? trunk->call_to(number, caller_id, caller_name) : false;

	LOG_CALL("sip", "call result %s", STR_BOOL(result));

	return result;
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::trunk_disconnect(uintptr_t trunk_id)
{
	LOG_CALL("sip", "disconnect call id:%p...", trunk_id);

	sip_trunk_t* trunk = get_trunk(trunk_id);

	if (trunk != nullptr)
	{
		trunk->disconnect();
	}
	LOG_CALL("sip", "call disconnected");
}
//--------------------------------------
//
//--------------------------------------
bool sip_client_t::trunk_answer(uintptr_t trunk_id, bool ringing)
{
	const char* wcs_answer = ringing ? "ring" : "answer";

	LOG_CALL("sip", "%s call id:%p...", wcs_answer, trunk_id);

	sip_trunk_t* trunk = get_trunk(trunk_id);

	LOG_CALL("sip", "call %sed", wcs_answer);

	return trunk != nullptr ? trunk->answer(ringing) : false;
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::trunk_reject(uintptr_t trunk_id, uint16_t code, const char* reason)
{
	LOG_CALL("sip", "reject call id:%p code:%u reason:%s...", trunk_id, code, reason);

	sip_trunk_t* trunk = get_trunk(trunk_id);

	if (trunk != nullptr)
	{
		trunk->reject(code, reason);
	}

	LOG_CALL("sip", "call rejected");
}
//--------------------------------------
//
//--------------------------------------
bool sip_client_t::get_call_header(uintptr_t trunk_id, const char* sip_header_name, int index, rtl::String& value)
{
	LOG_CALL("sip", "get call header id:%p name:%s index:%d...", trunk_id, sip_header_name, index);

	sip_trunk_t* trunk = get_trunk(trunk_id);
	bool result = false;
	
	if (trunk != nullptr)
	{
		value = trunk->get_sip_header(sip_header_name, index);

	}

	LOG_CALL("sip", "get call header value %s -> %s", (const char*)value, STR_BOOL(result));

	return result;
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::trunk_hold(uintptr_t trunk_id, bool state)
{
	LOG_CALL("sip", "Hold call id:%p state:%s...", trunk_id, STR_BOOL(state));

	sip_trunk_t* trunk = get_trunk(trunk_id);

	if (trunk != nullptr)
	{
		trunk->hold(state);
	}

	LOG_CALL("sip", "call Hold done");
}
//--------------------------------------
//
//--------------------------------------
//void sip_client_t::trunk_unhold(uintptr_t trunk_id)
//{
//	LOG_CALL("sip", "Unhold call id:%p...", trunk_id);
//
//	sip_trunk_t* trunk = get_trunk(trunk_id);
//
//	if (trunk != nullptr)
//	{
//		trunk->unhold();
//	}
//	LOG_CALL("sip", "call unholded");
//}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::trunk_refer_to(uintptr_t trunk_id, const char* number, uintptr_t replace_to)
{
	LOG_CALL("sip", "Refer to tid:%p -> (%s) -> rid:%p...", trunk_id, number, replace_to);

	sip_trunk_t* trunk = get_trunk(trunk_id);
	sip_trunk_t* replace_to_trunk = get_trunk(replace_to);

	if (trunk != nullptr)
	{
		trunk->get_account().refer_to(trunk, number, replace_to_trunk);
		LOG_CALL("sip", "call Referring");
	}
	else
	{
		LOG_CALL("sip", "Refer failed : trunk not found!");
	}
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::trunk_mute(uintptr_t trunk_id, bool state)
{
	LOG_CALL("sip", "Mute call id:%p state:%s...", trunk_id, STR_BOOL(state));

	sip_trunk_t* trunk = get_trunk(trunk_id);

	if (trunk != nullptr)
	{
		trunk->mute(state);
	}
	LOG_CALL("sip", "call Mute done");
}
//--------------------------------------
//
//--------------------------------------
//void sip_client_t::trunk_unmute(uintptr_t trunk_id)
//{
//	LOG_CALL("sip", "Unmute call id:%p...", trunk_id);
//
//	sip_trunk_t* trunk = get_trunk(trunk_id);
//
//	if (trunk != nullptr)
//	{
//		trunk->unmute();
//	}
//	LOG_CALL("sip", "call unmuted");
//}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::trunk_send_dtmf(uintptr_t trunk_id, char tone)
{
	LOG_CALL("sip", "send dtmf id:%p tone:%C...", trunk_id, tone);

	sip_trunk_t* trunk = get_trunk(trunk_id);

	if (trunk != nullptr)
	{
		trunk->send_dtmf(tone);
	}
	LOG_CALL("sip", "dtmf sent");
}
//--------------------------------------
//
//--------------------------------------
//void sip_client_t::trunk_start_video_session(uintptr_t trunk_id, int width, int height)
//{
//	sip_trunk_t* trunk = get_trunk(trunk_id);
//
//	if (trunk != nullptr)
//	{
//		trunk->start_video_session(width, height);
//	}
//}
////--------------------------------------
////
////--------------------------------------
//void sip_client_t::trunk_stop_video_session(uintptr_t trunk_id)
//{
//	sip_trunk_t* trunk = get_trunk(trunk_id);
//
//	if (trunk != nullptr)
//	{
//		trunk->stop_video_session();
//	}
//}
////--------------------------------------
////
////--------------------------------------
//void sip_client_t::trunk_send_video_frame(uintptr_t trunk_id, uintptr_t frame_bitmap)
//{
//	sip_trunk_t* trunk = get_trunk(trunk_id);
//
//	if (trunk != nullptr)
//	{
//		trunk->send_video_frame((HBITMAP)frame_bitmap);
//	}
//}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::trunk_start_playing(uintptr_t trunk_id, const char* file_path, bool in_cycle)
{
	LOG_CALL("sip", "call id:%p start playing %s...", trunk_id, file_path);

	sip_trunk_t* trunk = get_trunk(trunk_id);

	if (trunk != nullptr)
	{
		trunk->start_playing(file_path, in_cycle);
	}

	LOG_CALL("sip", "call play started");
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::trunk_stop_playing(uintptr_t trunk_id)
{
	LOG_CALL("sip", "call id:%p stop playing...", trunk_id);

	sip_trunk_t* trunk = get_trunk(trunk_id);

	if (trunk != nullptr)
	{
		trunk->stop_playing();
	}

	LOG_CALL("sip", "call play stopped");
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::trunk_start_recording(uintptr_t trunk_id, const char* file_path)
{
	LOG_CALL("sip", "call id:%p start recording %s...", trunk_id, file_path);

	sip_trunk_t* trunk = get_trunk(trunk_id);

	if (trunk != nullptr)
	{
		trunk->start_recording(file_path);
	}
	LOG_CALL("sip", "call record started");
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::trunk_stop_recording(uintptr_t trunk_id)
{
	LOG_CALL("sip", "call id:%p stop recording...", trunk_id);

	sip_trunk_t* trunk = get_trunk(trunk_id);

	if (trunk != nullptr)
	{
		trunk->stop_recording();
	}

	LOG_CALL("sip", "call record stopped");
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::set_devices(const char* device_out, const char* device_in)
{
	LOG_CALL("sip", "set audio devices to wave-out:%s wave-in:%s...", device_out, device_in);
	rtx_phone::context.set_devices(device_out, device_in);
	LOG_CALL("sip", "audio devices set");
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::get_output(char* buffer, int size)
{
	LOG_CALL("sip", "get audio output...");
	const char* output = rtx_phone::context.get_output();

	if (output != nullptr && output[0] != 0)
	{
		strncpy(buffer, output, size-1);
		buffer[size-1] = 0;
	}
	LOG_CALL("sip", "get audio output '%s'", buffer);
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::set_output(const char* device_out)
{
	LOG_CALL("sip", "set audio output to '%s'...", device_out);
	rtx_phone::context.set_output(device_out);
	LOG_CALL("sip", "audio output set");
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::get_input(char* buffer, int size)
{
	LOG_CALL("sip", "get audio input...");
	const char* input = rtx_phone::context.get_input();

	if (input != nullptr && input[0] != 0)
	{
		strncpy(buffer, input, size-1);
		buffer[size-1] = 0;
	}
	LOG_CALL("sip", "get audio input '%s'", buffer);
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::set_input(const char* device_in)
{
	LOG_CALL("sip", "set audio input to '%s'...", device_in);
	rtx_phone::context.set_input(device_in);
	LOG_CALL("sip", "audio input set");
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::mute_all(bool mute)
{
	LOG_CALL("sip", "mute audio input '%s'...", STR_BOOL(mute));
	rtx_phone::context.mute(mute);
	LOG_CALL("sip", "audio input %s", mute ? L"muted" : L"unmuted");
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::start_recording(const char* file_path)
{
	LOG_CALL("sip", "SIP_StartRecording -- start recording to '%s'...", file_path);
	rtx_phone::context.start_recording(file_path);
	LOG_CALL("sip", "SIP_StartRecording -- ok.");
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::stop_recording()
{
	LOG_CALL("sip", "SIP_StopRecording -- stop recording...");
	rtx_phone::context.stop_recording();
	LOG_CALL("sip", "SIP_StopRecording -- ok.");
}
//--------------------------------------
//
//--------------------------------------
sip_account_t* sip_client_t::get_account(uintptr_t account_id)
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	for (int i = 0; i < m_account_list.getCount(); i++)
	{
		if (account_id == (uintptr_t)m_account_list[i])
			return m_account_list[i];
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
sip_trunk_t* sip_client_t::get_trunk(uintptr_t trunk_id)
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	for (int i = 0; i < m_trunk_list.getCount(); i++)
	{
		if (trunk_id == (uintptr_t)m_trunk_list[i])
			return m_trunk_list[i];
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::check_accounts(const sip_transport_point_t& point)
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	for (int i = 0; i < m_account_list.getCount(); i++)
	{
		sip_account_t* account = m_account_list[i];
		
		if (account->get_reg_state() == reg_state_t::unregistered)
			LOG_CALL("client", "client:check_accounts -> start_registration()");
			account->start_registration();
	}
}
//--------------------------------------
// ����� ������� �� ��������� ������
//--------------------------------------
sip_account_t* sip_client_t::find_account(const sip_message_t& request)
{
	// �������� ����������� � ��� ������������

	in_addr remoteAddress = request.get_remote_address();
	uint16_t remotePort = request.get_remote_port();
	const sip_uri_t& uri = request.To_URI();

	for (int i = 0; i < m_account_list.getCount(); i++)
	{
		sip_account_t* account = m_account_list[i];
		
		if (account->check_request(remoteAddress, remotePort, uri.get_user(), uri.get_host()))
		{
			return account;
		}
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
//void sip_client_t::set_video_settings(int width, int height, int framerate, int bitrate, int quality, bool multithreading)
//{
//	m_video_settings.dec_in_width = m_video_settings.dec_out_width = width;
//	m_video_settings.dec_in_height = m_video_settings.dec_out_height = height;
//
//	m_video_settings.enc_in_width = m_video_settings.enc_out_width = width;
//	m_video_settings.enc_in_height = m_video_settings.enc_out_height = height;
//
//	m_video_settings.enc_framerate = framerate;
//	m_video_settings.enc_bitrate = bitrate;
//	m_video_settings.enc_quality = (VideoSettings::Quality)quality;
//	m_video_settings.parallel_encoding = multithreading;
//}
//--------------------------------------
//
//--------------------------------------
void sip_client_t::process_sip_stack_event(sip_stack_event_t* sip_event)
{
	if (!m_initialized)
		return;

	LOG_EVENT("client", "Process SIP Event %s", (const char*)sip_event->to_string());

	if (sip_event->getType() == sip_stack_event_t::sip_event_transport_failure_e)
	{
		sip_event_transport_failure_t* failure_event = (sip_event_transport_failure_t*)sip_event;
		// ������� ��� ����
		const sip_transport_route_t& route = failure_event->getRoute();
		m_registrar.process_transport_disconnected(route);
		m_reg_manager->process_stack_event(route, sip_event);
		m_call_manager->process_stack_event(route, sip_event);

		return;
	}

	if (sip_event->getType() == sip_stack_event_t::sip_event_transport_disconnected_e)
	{
		sip_event_transport_disconnected_t* disconnect_event = (sip_event_transport_disconnected_t*)sip_event;
		// ������� ��� ����
		const sip_transport_route_t& route = disconnect_event->getRoute();
		m_reg_manager->process_stack_event(route, sip_event);
		m_call_manager->process_stack_event(route, sip_event);

		return;
	}

	if (sip_event->getType() == sip_stack_event_t::sip_event_transport_new_interface_e)
	{
		sip_event_transport_new_interface_t* new_event = (sip_event_transport_new_interface_t*)sip_event;
		// ������� ��� ����
		check_accounts(new_event->get_point());
		return;
	}

	const trans_id& transactionId = sip_event->get_transaction_id();
	rtl::String method = transactionId.GetMethod().toUpper();
	rtl::String callId = transactionId.GetCallId();

	//LOG_EVENT("ua", "Process SIP Event %S -- %S", (const char*)method, (const char*)callId);

	if (method *= "REGISTER")
	{
		m_reg_manager->process_stack_event(sip_event);
	}
	else
	{
		m_call_manager->process_stack_event(sip_event);
	}
}
//--------------------------------------
//
//--------------------------------------
bool sip_client_t::add_transport(sip_transport_type_t type, in_addr if_address, uint16_t if_port)
{
	return sip_user_agent_t::add_transport(type, if_address, if_port);
}
//--------------------------------------
/// account events
//--------------------------------------
void sip_client_t::raise_account_registered(sip_account_t* account)
{
	::raise_account_registered((uintptr_t)account);
}
void sip_client_t::raise_account_unregistered(sip_account_t* account, bool by_user)
{
	::raise_account_unregistered((uintptr_t)account, by_user);
}
void sip_client_t::raise_account_named_event(sip_account_t* account, const char* event_name, const char* event_parameter)
{
	::raise_account_named_event((uintptr_t)account, event_name, event_parameter);
}
void sip_client_t::raise_account_incoming_call(sip_account_t* account, sip_trunk_t* trunk, const char* called_id, const char* called_name, const char* caller_id, const char* caller_name/*, const char* invite*/)
{
	::raise_account_incoming_call((uintptr_t)account, (uintptr_t)trunk,
		called_id, called_name,
		caller_id, caller_name/*,
		invite*/);
}
/// trunk events
void sip_client_t::raise_call_ringing(sip_trunk_t* trunk, uint16_t code, const char* reason, bool with_media)
{
	::raise_call_ringing((uintptr_t)trunk, code, reason, with_media);
}
void sip_client_t::raise_call_answered(sip_trunk_t* trunk/*, const char* sip_message*/)
{
	::raise_call_answered((uintptr_t)trunk/*, sip_message*/);
}
void sip_client_t::raise_call_rejected(sip_trunk_t* trunk, uint16_t code, const char* reason)
{
	::raise_call_rejected((uintptr_t)trunk, code, reason);
}
void sip_client_t::raise_call_connected(sip_trunk_t* trunk)
{
	::raise_call_connected((uintptr_t)trunk);
}
void sip_client_t::raise_call_disconnected(sip_trunk_t* trunk, enum DisconnectReason reason)
{
	::raise_call_disconnected((uintptr_t)trunk, reason);
}
void sip_client_t::raise_call_play_stopped(sip_trunk_t* trunk_id)
{
	::raise_call_play_stopped((uintptr_t)trunk_id);
}
void sip_client_t::raise_call_refer_notified(sip_trunk_t* trunk, ReferNotifyType ntype, const char* message)
{
	::raise_call_refer_notify((uintptr_t)trunk, ntype, message);
}
void sip_client_t::raise_call_replaced_by_refer(sip_trunk_t* trunk_old, sip_trunk_t* trunk_new, const rtl::String& called_id, const rtl::String& called_name, const rtl::String& caller_id, const rtl::String& caller_name)
{
	::raise_call_replaced_by_refer((uintptr_t)trunk_old, (uintptr_t)trunk_new, caller_id, caller_name, called_id, called_name);
}
//--------------------------------------
