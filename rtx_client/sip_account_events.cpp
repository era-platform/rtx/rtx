/* RTX H.248 Media Gate. SIP Softphone
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "sip_client.h"
#include "sip_account.h"
#include "sip_trunk.h"
//--------------------------------------
//
//--------------------------------------
void sip_account_t::sip_reg_session__accepted(sip_reg_session_client_t& session, const sip_message_t& request)
{
	sip_transport_route_t* route_ptr = NEW sip_transport_route_t;

	*route_ptr = request.getRoute();
	raise_event(SIP_ASYNC_BACK_REGISTERED_EVENT, 0, route_ptr);
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::sip_reg_session__rejected(sip_reg_session_client_t& session, const sip_message_t& request)
{
	raise_event(SIP_ASYNC_BACK_UNREGISTERED_EVENT, 0, &session);
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::sip_reg_session__timeout(sip_reg_session_client_t& session)
{
	//LOG_EVENT("account", "%s : REGISTRATION TIMEOUT EVENT RAISED", m_username);

	raise_event(SIP_ASYNC_BACK_TIMEOUT_EVENT, 0, &session);
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::sip_reg_session__options_request(sip_reg_session_client_t& session, const sip_message_t& request, sip_message_t& response)
{
	LOG_EVENT("account", "%s : OPTION REQUEST EVENT (IGNORED)", (const char*)m_profile.get_username());
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::sip_reg_session__options_response(sip_reg_session_client_t& session, const sip_message_t& response)
{
	LOG_EVENT("account", "%s : OPTION RESPONSE EVENT (IGNORED)", (const char*)m_profile.get_username());
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::sip_reg_session__destroying(sip_reg_session_client_t& session)
{
	raise_event(SIP_ASYNC_BACK_DESTROYED_EVENT, 0, &session);
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::sip_reg_session__transport_failure(sip_reg_session_client_t& session)
{
	LOG_EVENT("account", "%s : TRANSPORT FAILURE EVENT %p", (const char*)m_profile.get_username(), &session);
	raise_event(SIP_ASYNC_BACK_TRANSP_FAILURE_EVENT, 0, &session);
}
//--------------------------------------
//
//--------------------------------------
bool sip_account_t::process_refer_request(sip_trunk_t* trunk, const sip_value_uri_t* referTo, const sip_value_uri_t* referredBy)
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (m_refer_trunk != nullptr)
	{
		// unexpected state! ignore
		return false;
	}

	m_refer_trunk = trunk;
	m_refer_to = *referTo;
	if (referredBy != nullptr)
		m_referred_by = *referredBy;
	else
		m_referred_by.cleanup();

	m_refer_message = referTo->to_string();
	raise_event(SIP_ASYNC_TRUNK_REFER_INCOME);

	return true;
}
//--------------------------------------
//
//--------------------------------------
void sip_account_t::process_refer_response(sip_trunk_t* trunk, sip_status_t code, const rtl::String& reason)
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (trunk != m_refer_trunk)
	{
		// unexpected response! ignore
		LOG_WARN("account", "%s: process REFER response : unexpected state : referrer trunk(%p) != REFER answer trunk (%p)", m_aor, trunk, m_refer_trunk);
		return;
	}

	m_refer_code = code;
	m_refer_message = reason;

	if (code >= 100 && code < 200)
	{
		rtl::Timer::setTimer(this, SIP_REFER_TIMER_ID, SIP_REFER_TIMEOUT, false);
		LOG_CALL("account", "%s: process REFER response : %u %s : do noting", m_aor, code, (const char*)reason);
		// do nothing
	}
	else if (code >= 200 && code < 300)
	{
		// accepted
		rtl::Timer::setTimer(this, SIP_REFER_TIMER_ID, SIP_REFER_TIMEOUT, false);
		LOG_CALL("account", "%s: process REFER response : %u %s : switch to async processor", m_aor, code, (const char*)reason);
		raise_event(SIP_ASYNC_TRUNK_REFER_ANSWERED, code);
	}
	else if (code >= 300)
	{
		rtl::Timer::stopTimer(this, SIP_REFER_TIMER_ID);
		LOG_CALL("account", "%s: process REFER response : %u %s : stop processing", m_aor, code, (const char*)reason);
		raise_event(SIP_ASYNC_TRUNK_REFER_ANSWERED, code);
	}
	else
	{
		LOG_WARN("account", "%s: process refer response : unexpected state : %u %s", m_aor, code, (const char*)reason);
	}
}
//--------------------------------------
//
//--------------------------------------
bool sip_account_t::process_notify_request(sip_trunk_t* trunk, const rtl::String& state, const rtl::String& state_value, const rtl::String& sipfrag)
{
	rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

	if (trunk != m_refer_trunk)
	{
		LOG_WARN("account", "%s: process NOTIFY request : unexpected state : referrer trunk(%p) != REFER answer trunk (%p)", m_aor, trunk, m_refer_trunk);

		return false;
	}

	if (strncmp(sipfrag, "SIP/2.0 ", 8) == 0)
	{
		m_refer_code = (sip_status_t)strtoul(((const char*)sipfrag) + 8, nullptr, 10);
	}
	else
	{
		m_refer_code = sip_000;
	}

	if (state == "active")
	{
		// we must update refer subscribe timer with new time
		uint32_t timeout = strtoul(state_value, nullptr, 10);
		if (timeout == 0)
		{
			rtl::Timer::stopTimer(this, SIP_REFER_TIMER_ID);
			m_refer_message = sip_get_reason_text(sip_408_RequestTimeout);
			LOG_CALL("account", "%s: process NOTIFY timeout : stop processing", m_aor);
			raise_event(SIP_ASYNC_TRUNK_REFER_ANSWERED, sip_408_RequestTimeout);
		}
		else
		{
			if (timeout > 1800)
				timeout = 1800;
			rtl::Timer::setTimer(this, SIP_REFER_TIMER_ID, timeout * 1000, false);
			m_refer_message = sipfrag;
			LOG_CALL("account", "%s: process NOTIFY : raise event", m_aor);
			raise_event(SIP_ASYNC_TRUNK_REFER_NOTIFIED);
		}
	}
	else if (state == "terminated")
	{
		rtl::Timer::stopTimer(this, SIP_REFER_TIMER_ID);
		m_refer_message = sipfrag;
		LOG_CALL("account", "%s: process NOTIFY : raise 'terminated' event", m_aor);
		raise_event(SIP_ASYNC_TRUNK_REFER_NOTIFIED);
	}
	else
	{
		// we will ignore unexpected states
		LOG_WARN("account", "%s: process NOTIFY request : unexpected state : %s = %s  sipfrag %s", m_aor, (const char*)state,
			(const char*)state_value, (const char*)sipfrag);
	}

	return true;
}
//--------------------------------------
