/* RTX H.248 Media Gate. SIP Softphone
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_enums.h"
//-----------------------------------------------
//
//-----------------------------------------------
#define	SDP_MEDIA_PROFILE_RTP		"RTP/AVP"
#define	SDP_MEDIA_TRANSPORT_UDP		"UDP"
#define TELEPHONE_EVENT				"telephone-event"
//-----------------------------------------------
//
//-----------------------------------------------
class sdp_session_t;
//-----------------------------------------------
//
//-----------------------------------------------
enum sdp_direction_type_t
{
	sdp_direction_inactive_e	= 0,
	sdp_direction_recvonly_e	= 1,
	sdp_direction_sendonly_e	= 2,
	sdp_direction_sendrecv_e	= sdp_direction_recvonly_e | sdp_direction_sendonly_e,
	sdp_direction_undefined_e	= 0x80,
};
const char* sdp_get_direction_name(sdp_direction_type_t dir);
//-----------------------------------------------
//
//-----------------------------------------------
enum sdp_media_description_type_t
{
	sdp_media_audio_e,
	sdp_media_video_e,
	sdp_media_unknown_e,
};
const char* sdp_get_media_name(sdp_media_description_type_t media);
//-----------------------------------------------
//
//-----------------------------------------------
class sdp_media_description_t
{
	rtl::Logger& m_log;
	sdp_session_t* m_session;				// owner session
	sdp_media_description_type_t m_media_type;			// type of media
	char m_media[16];						// name of media

	uint16_t m_port_count;					// port count in sequense
	uint32_t m_port;						// port begins sequense
	in_addr m_address;						// address
	char m_transport[32];					// transport/profile

	sdp_direction_type_t m_direction;		// flow direction
	media::PayloadSet m_formats;				// described formats
	int m_packet_time;						// ptime attribute, in milliseconds
	
private:
	void decode_transport(const char* transport);
	void decode_attribute(const char* attribute);
	void decode_candidate_attribute(const char* value);
	void decode_rtcp_attribute(const char* value);
	void decode_ssrc_attribute(const char* value);

	int encode_media(char* buffer, int size);
	void encode_media(rtl::String& stream);
	int encode_rtp_formats(char* buffer, int size);
	void encode_rtp_formats(rtl::String& stream);

public:
	sdp_media_description_t(const sdp_media_description_t& description);
	sdp_media_description_t(sdp_session_t* session, sdp_media_description_type_t mediaType = sdp_media_unknown_e, const char* mediaName = nullptr);
	~sdp_media_description_t();

	int encode(char* buffer, int size);
	void encode(rtl::String& stream);

	bool decode(const char* str);
	bool decode_line(char name, const char* value);

	/// common properties
	sdp_session_t* get_session() { return m_session; }

	/// 'm' line parameters
	sdp_media_description_type_t get_media_type() { return m_media_type; }
	const char* get_media_name() { return m_media; }
	const char* get_transport() const { return m_transport; }
	void set_transport(const char* transport) { STR_COPY(m_transport, transport); }
	in_addr get_address() const;
	in_addr get_address(in_addr mask) const;
	void set_address(in_addr address) { m_address = address; }
	uint16_t get_port() const { return (uint16_t)m_port; }
	void set_port(uint16_t port) { m_port = port; }
	void set_port_choose() { m_port = MG_PORT_CHOOSE; }
	/// media format attributes fmtp rtpmap
	const PayloadSet& get_media_formats() const { return m_formats; }
	PayloadSet& get_media_formats() { return m_formats; }
	const PayloadFormat& get_top_format() const { return m_formats.getTopFormat(); }
	void set_media_formats(const PayloadSet& mediaFormats);
	int add_media_format(const PayloadFormat& mediaFormat);
	void remove_media_formats();

	/// direction attribute
	void set_direction(sdp_direction_type_t direction) { m_direction = direction; }
	sdp_direction_type_t get_direction() const;

	/// ptime attribute
	int get_packet_time () const { return m_packet_time; }
	void set_packet_time (int milliseconds) { m_packet_time = milliseconds; }
};
//-----------------------------------------------
// 
//-----------------------------------------------
typedef rtl::ArrayT<sdp_media_description_t*> sdp_media_list_t;
//-----------------------------------------------
