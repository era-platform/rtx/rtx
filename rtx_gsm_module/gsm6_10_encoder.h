﻿/* RTX H.248 Media Gate. GSM610 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 //------------------------------------------------------------------------------
 // Copyright 1992 by Jutta Degener and Carsten Bormann, Technische
 // Universitaet Berlin.  See the accompanying file "COPYRIGHT" for
 // details.  THERE IS ABSOLUTELY NO WARRANTY FOR THIS SOFTWARE.
 //------------------------------------------------------------------------------
#pragma once
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "rtx_audio_codec.h"
#include "std_logger.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
extern int g_rtx_gsm_encoder_counter;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
struct gsm_state;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class rtx_gsm_codec_encoder : public mg_audio_encoder_t
{
	rtx_gsm_codec_encoder(const rtx_gsm_codec_encoder&);
	rtx_gsm_codec_encoder&		operator=(const rtx_gsm_codec_encoder&);
public:
	rtx_gsm_codec_encoder(rtl::Logger* log);
	virtual ~rtx_gsm_codec_encoder();

	bool initialize(const media::PayloadFormat& format);
	void destroy();

	virtual const char* getEncoding();

	virtual uint32_t encode(const uint8_t* buff_from, uint32_t size_from, uint8_t* buff_to, uint32_t size_to);

	virtual uint32_t packetize(rtp_packet* packet, const uint8_t* buff, uint32_t buff_size);

	virtual uint32_t get_frame_size();

private:
	uint32_t encode_msgsm(const uint8_t* from, uint32_t from_length, uint8_t* to, uint32_t to_length);

	//uint8_t coding_element(gsm_state* s, const short* source, uint8_t* c);

private:
	rtl::Logger* m_log;
	bool m_msgsm;
	gsm_state* m_gsm_encoder;
	uint32_t m_frame_size;
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
