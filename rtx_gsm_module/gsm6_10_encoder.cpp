﻿/* RTX H.248 Media Gate. GSM610 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 //------------------------------------------------------------------------------
 // Copyright 1992 by Jutta Degener and Carsten Bormann, Technische
 // Universitaet Berlin.  See the accompanying file "COPYRIGHT" for
 // details.  THERE IS ABSOLUTELY NO WARRANTY FOR THIS SOFTWARE.
 //------------------------------------------------------------------------------

#include "stdafx.h"

#include <gsm.h>
#include "gsm6_10_encoder.h"
#include "rtp_packet.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
extern int g_rtx_gsm_encoder_counter = -1;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtx_gsm_codec_encoder::rtx_gsm_codec_encoder(rtl::Logger* log) :
	m_log(log)
{
	m_gsm_encoder = nullptr;
	m_msgsm = false;

	rtl::res_counter_t::add_ref(g_rtx_gsm_encoder_counter);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtx_gsm_codec_encoder::~rtx_gsm_codec_encoder()
{
	destroy();

	rtl::res_counter_t::release(g_rtx_gsm_encoder_counter);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtx_gsm_codec_encoder::initialize(const media::PayloadFormat& format)
{
	if (m_gsm_encoder == nullptr)
	{
		m_gsm_encoder = gsm_create();
		rtl::String param_str(format.getFmtp());
		m_msgsm = rtl::String::compare("msgsm", param_str, false) == 0;
		
		m_frame_size = (m_msgsm) ? 320 : 160;

		return true;
	}

	return false;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t rtx_gsm_codec_encoder::get_frame_size()
{
	return m_frame_size * sizeof(uint16_t);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtx_gsm_codec_encoder::destroy()
{
	if (m_gsm_encoder != nullptr)
	{
		gsm_destroy(m_gsm_encoder);
		m_gsm_encoder = nullptr;
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const char* rtx_gsm_codec_encoder::getEncoding()
{
	return "gsm";
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// пїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅ 320 пїЅпїЅпїЅпїЅ пїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅ пїЅ 33 пїЅпїЅпїЅпїЅпїЅ.
// пїЅпїЅпїЅпїЅ msgsm пїЅпїЅ:
// пїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅ 640 пїЅпїЅпїЅпїЅ пїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅ 65 пїЅпїЅпїЅпїЅ.
//------------------------------------------------------------------------------
uint32_t rtx_gsm_codec_encoder::encode(const uint8_t* buff_from, uint32_t size_from, uint8_t* buff_to, uint32_t size_to)
{
	if (m_gsm_encoder == nullptr)
	{
		PLOG_MEDIA_ERROR("GSM", "gsm6_10_codec.encode -- gsm encoder is null");
		return 0;
	}

	if (m_msgsm)
	{
		return encode_msgsm(buff_from, size_from, buff_to, size_to);
	}

	//uSamples = 2 пїЅпїЅпїЅпїЅпїЅ

	uint32_t pcmCount = size_from / 320;
	uint32_t gsmSize = 0;

	for (uint32_t i = 0; i < pcmCount; i++)
	{
		if (gsmSize + 33 > size_to)
		{
			PLOG_MEDIA_ERROR("GSM","gsm6_10_codec.encode -- not enough space for encode");
			return gsmSize;
		}

		gsm_encode(m_gsm_encoder, (const short*)buff_from, buff_to);

		buff_to += 33;
		buff_from += 320;
		gsmSize += 33;
	}

	return gsmSize;
}
//------------------------------------------------------------------------------
// пїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅ 640 пїЅпїЅпїЅпїЅ пїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅ 65 пїЅпїЅпїЅпїЅ.
//------------------------------------------------------------------------------
uint32_t rtx_gsm_codec_encoder::encode_msgsm(const uint8_t* from, uint32_t from_length, uint8_t* to, uint32_t to_length)
{
	uint32_t pcmCount = from_length / 640;
	uint32_t gsmSize = 0;
	const short* fromPCM = (const short*)from;

	int opt = 1;
	gsm_option(m_gsm_encoder, GSM_OPT_WAV49, &opt);

	for (uint32_t i = 0; i < pcmCount; i++)
	{
		if (gsmSize + 65 > to_length)
		{
			PLOG_MEDIA_ERROR("GSM", "gsm6_10_codec.encode_msgsm -- not enough space for encode");
			return gsmSize;
		}

		gsm_encode(m_gsm_encoder, fromPCM, to);
		gsm_encode(m_gsm_encoder, fromPCM + 160, to + 32);

		to += 65;
		fromPCM += 320;
		gsmSize += 65;
	}

	return gsmSize;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//uint8_t	rtx_gsm_codec_encoder::coding_element(gsm_state* s, const short* source, uint8_t* c)
//{
//	short LARc[8], Nc[4], Mc[4], bc[4], xmaxc[4], xmc[13 * 4];
//
//	Gsm_Coder(s, source, LARc, Nc, bc, Mc, xmaxc, xmc);
//
//	/* field  field name  bits  field  field name  bits
//	________________________________________________
//	1      LARc[0]     6     39     xmc[22]     3
//	2      LARc[1]     6     40     xmc[23]     3
//	3      LARc[2]     5     41     xmc[24]     3
//	4      LARc[3]     5     42     xmc[25]     3
//	5      LARc[4]     4     43     Nc[2]       7
//	6      LARc[5]     4     44     bc[2]       2
//	7      LARc[6]     3     45     Mc[2]       2
//	8      LARc[7]     3     46     xmaxc[2]    6
//	9      Nc[0]       7     47     xmc[26]     3
//	10     bc[0]       2     48     xmc[27]     3
//	11     Mc[0]       2     49     xmc[28]     3
//	12     xmaxc[0]    6     50     xmc[29]     3
//	13     xmc[0]      3     51     xmc[30]     3
//	14     xmc[1]      3     52     xmc[31]     3
//	15     xmc[2]      3     53     xmc[32]     3
//	16     xmc[3]      3     54     xmc[33]     3
//	17     xmc[4]      3     55     xmc[34]     3
//	18     xmc[5]      3     56     xmc[35]     3
//	19     xmc[6]      3     57     xmc[36]     3
//	20     xmc[7]      3     58     xmc[37]     3
//	21     xmc[8]      3     59     xmc[38]     3
//	22     xmc[9]      3     60     Nc[3]       7
//	23     xmc[10]     3     61     bc[3]       2
//	24     xmc[11]     3     62     Mc[3]       2
//	25     xmc[12]     3     63     xmaxc[3]    6
//	26     Nc[1]       7     64     xmc[39]     3
//	27     bc[1]       2     65     xmc[40]     3
//	28     Mc[1]       2     66     xmc[41]     3
//	29     xmaxc[1]    6     67     xmc[42]     3
//	30     xmc[13]     3     68     xmc[43]     3
//	31     xmc[14]     3     69     xmc[44]     3
//	32     xmc[15]     3     70     xmc[45]     3
//	33     xmc[16]     3     71     xmc[46]     3
//	34     xmc[17]     3     72     xmc[47]     3
//	35     xmc[18]     3     73     xmc[48]     3
//	36     xmc[19]     3     74     xmc[49]     3
//	37     xmc[20]     3     75     xmc[50]     3
//	38     xmc[21]     3     76     xmc[51]     3 */
//
//	if (s->wav_fmt)
//	{
//		s->frame_index = !s->frame_index;
//		if (s->frame_index)
//		{
//			uint16_t sr;
//
//			sr = LARc[0] << 10;
//			sr = sr >> 6 | LARc[1] << 10;
//			*c++ = uint8_t(sr >> 4);
//			sr = sr >> 5 | LARc[2] << 11;
//			*c++ = uint8_t(sr >> 7);
//			sr = sr >> 5 | LARc[3] << 11;
//			sr = sr >> 4 | LARc[4] << 12;
//			*c++ = uint8_t(sr >> 6);
//			sr = sr >> 4 | LARc[5] << 12;
//			sr = sr >> 3 | LARc[6] << 13;
//			*c++ = uint8_t(sr >> 7);
//			sr = sr >> 3 | LARc[7] << 13;
//			sr = sr >> 7 | Nc[0] << 9;
//			*c++ = uint8_t(sr >> 5);
//			sr = sr >> 2 | bc[0] << 14;
//			sr = sr >> 2 | Mc[0] << 14;
//			sr = sr >> 6 | xmaxc[0] << 10;
//			*c++ = uint8_t(sr >> 3);
//			sr = sr >> 3 | xmc[0] << 13;
//			*c++ = uint8_t(sr >> 8);
//			sr = sr >> 3 | xmc[1] << 13;
//			sr = sr >> 3 | xmc[2] << 13;
//			sr = sr >> 3 | xmc[3] << 13;
//			*c++ = uint8_t(sr >> 7);
//			sr = sr >> 3 | xmc[4] << 13;
//			sr = sr >> 3 | xmc[5] << 13;
//			sr = sr >> 3 | xmc[6] << 13;
//			*c++ = uint8_t(sr >> 6);
//			sr = sr >> 3 | xmc[7] << 13;
//			sr = sr >> 3 | xmc[8] << 13;
//			*c++ = uint8_t(sr >> 8);
//			sr = sr >> 3 | xmc[9] << 13;
//			sr = sr >> 3 | xmc[10] << 13;
//			sr = sr >> 3 | xmc[11] << 13;
//			*c++ = uint8_t(sr >> 7);
//			sr = sr >> 3 | xmc[12] << 13;
//			sr = sr >> 7 | Nc[1] << 9;
//			*c++ = uint8_t(sr >> 5);
//			sr = sr >> 2 | bc[1] << 14;
//			sr = sr >> 2 | Mc[1] << 14;
//			sr = sr >> 6 | xmaxc[1] << 10;
//			*c++ = uint8_t(sr >> 3);
//			sr = sr >> 3 | xmc[13] << 13;
//			*c++ = uint8_t(sr >> 8);
//			sr = sr >> 3 | xmc[14] << 13;
//			sr = sr >> 3 | xmc[15] << 13;
//			sr = sr >> 3 | xmc[16] << 13;
//			*c++ = uint8_t(sr >> 7);
//			sr = sr >> 3 | xmc[17] << 13;
//			sr = sr >> 3 | xmc[18] << 13;
//			sr = sr >> 3 | xmc[19] << 13;
//			*c++ = uint8_t(sr >> 6);
//			sr = sr >> 3 | xmc[20] << 13;
//			sr = sr >> 3 | xmc[21] << 13;
//			*c++ = uint8_t(sr >> 8);
//			sr = sr >> 3 | xmc[22] << 13;
//			sr = sr >> 3 | xmc[23] << 13;
//			sr = sr >> 3 | xmc[24] << 13;
//			*c++ = uint8_t(sr >> 7);
//			sr = sr >> 3 | xmc[25] << 13;
//			sr = sr >> 7 | Nc[2] << 9;
//			*c++ = uint8_t(sr >> 5);
//			sr = sr >> 2 | bc[2] << 14;
//			sr = sr >> 2 | Mc[2] << 14;
//			sr = sr >> 6 | xmaxc[2] << 10;
//			*c++ = uint8_t(sr >> 3);
//			sr = sr >> 3 | xmc[26] << 13;
//			*c++ = uint8_t(sr >> 8);
//			sr = sr >> 3 | xmc[27] << 13;
//			sr = sr >> 3 | xmc[28] << 13;
//			sr = sr >> 3 | xmc[29] << 13;
//			*c++ = uint8_t(sr >> 7);
//			sr = sr >> 3 | xmc[30] << 13;
//			sr = sr >> 3 | xmc[31] << 13;
//			sr = sr >> 3 | xmc[32] << 13;
//			*c++ = uint8_t(sr >> 6);
//			sr = sr >> 3 | xmc[33] << 13;
//			sr = sr >> 3 | xmc[34] << 13;
//			*c++ = uint8_t(sr >> 8);
//			sr = sr >> 3 | xmc[35] << 13;
//			sr = sr >> 3 | xmc[36] << 13;
//			sr = sr >> 3 | xmc[37] << 13;
//			*c++ = uint8_t(sr >> 7);
//			sr = sr >> 3 | xmc[38] << 13;
//			sr = sr >> 7 | Nc[3] << 9;
//			*c++ = uint8_t(sr >> 5);
//			sr = sr >> 2 | bc[3] << 14;
//			sr = sr >> 2 | Mc[3] << 14;
//			sr = sr >> 6 | xmaxc[3] << 10;
//			*c++ = uint8_t(sr >> 3);
//			sr = sr >> 3 | xmc[39] << 13;
//			*c++ = uint8_t(sr >> 8);
//			sr = sr >> 3 | xmc[40] << 13;
//			sr = sr >> 3 | xmc[41] << 13;
//			sr = sr >> 3 | xmc[42] << 13;
//			*c++ = uint8_t(sr >> 7);
//			sr = sr >> 3 | xmc[43] << 13;
//			sr = sr >> 3 | xmc[44] << 13;
//			sr = sr >> 3 | xmc[45] << 13;
//			*c++ = uint8_t(sr >> 6);
//			sr = sr >> 3 | xmc[46] << 13;
//			sr = sr >> 3 | xmc[47] << 13;
//			*c++ = uint8_t(sr >> 8);
//			sr = sr >> 3 | xmc[48] << 13;
//			sr = sr >> 3 | xmc[49] << 13;
//			sr = sr >> 3 | xmc[50] << 13;
//			*c++ = uint8_t(sr >> 7);
//			sr = sr >> 3 | xmc[51] << 13;
//			sr = sr >> 4;
//			*c = sr >> 8;
//			s->frame_chain = *c;
//		}
//		else
//		{
//			uint16_t sr;
//
//			sr = s->frame_chain << 12;
//			sr = sr >> 6 | LARc[0] << 10;
//			*c++ = uint8_t(sr >> 6);
//			sr = sr >> 6 | LARc[1] << 10;
//			*c++ = uint8_t(sr >> 8);
//			sr = sr >> 5 | LARc[2] << 11;
//			sr = sr >> 5 | LARc[3] << 11;
//			*c++ = uint8_t(sr >> 6);
//			sr = sr >> 4 | LARc[4] << 12;
//			sr = sr >> 4 | LARc[5] << 12;
//			*c++ = uint8_t(sr >> 6);
//			sr = sr >> 3 | LARc[6] << 13;
//			sr = sr >> 3 | LARc[7] << 13;
//			*c++ = uint8_t(sr >> 8);
//			sr = sr >> 7 | Nc[0] << 9;
//			sr = sr >> 2 | bc[0] << 14;
//			*c++ = uint8_t(sr >> 7);
//			sr = sr >> 2 | Mc[0] << 14;
//			sr = sr >> 6 | xmaxc[0] << 10;
//			*c++ = uint8_t(sr >> 7);
//			sr = sr >> 3 | xmc[0] << 13;
//			sr = sr >> 3 | xmc[1] << 13;
//			sr = sr >> 3 | xmc[2] << 13;
//			*c++ = uint8_t(sr >> 6);
//			sr = sr >> 3 | xmc[3] << 13;
//			sr = sr >> 3 | xmc[4] << 13;
//			*c++ = uint8_t(sr >> 8);
//			sr = sr >> 3 | xmc[5] << 13;
//			sr = sr >> 3 | xmc[6] << 13;
//			sr = sr >> 3 | xmc[7] << 13;
//			*c++ = uint8_t(sr >> 7);
//			sr = sr >> 3 | xmc[8] << 13;
//			sr = sr >> 3 | xmc[9] << 13;
//			sr = sr >> 3 | xmc[10] << 13;
//			*c++ = uint8_t(sr >> 6);
//			sr = sr >> 3 | xmc[11] << 13;
//			sr = sr >> 3 | xmc[12] << 13;
//			*c++ = uint8_t(sr >> 8);
//			sr = sr >> 7 | Nc[1] << 9;
//			sr = sr >> 2 | bc[1] << 14;
//			*c++ = uint8_t(sr >> 7);
//			sr = sr >> 2 | Mc[1] << 14;
//			sr = sr >> 6 | xmaxc[1] << 10;
//			*c++ = uint8_t(sr >> 7);
//			sr = sr >> 3 | xmc[13] << 13;
//			sr = sr >> 3 | xmc[14] << 13;
//			sr = sr >> 3 | xmc[15] << 13;
//			*c++ = uint8_t(sr >> 6);
//			sr = sr >> 3 | xmc[16] << 13;
//			sr = sr >> 3 | xmc[17] << 13;
//			*c++ = uint8_t(sr >> 8);
//			sr = sr >> 3 | xmc[18] << 13;
//			sr = sr >> 3 | xmc[19] << 13;
//			sr = sr >> 3 | xmc[20] << 13;
//			*c++ = uint8_t(sr >> 7);
//			sr = sr >> 3 | xmc[21] << 13;
//			sr = sr >> 3 | xmc[22] << 13;
//			sr = sr >> 3 | xmc[23] << 13;
//			*c++ = uint8_t(sr >> 6);
//			sr = sr >> 3 | xmc[24] << 13;
//			sr = sr >> 3 | xmc[25] << 13;
//			*c++ = uint8_t(sr >> 8);
//			sr = sr >> 7 | Nc[2] << 9;
//			sr = sr >> 2 | bc[2] << 14;
//			*c++ = uint8_t(sr >> 7);
//			sr = sr >> 2 | Mc[2] << 14;
//			sr = sr >> 6 | xmaxc[2] << 10;
//			*c++ = uint8_t(sr >> 7);
//			sr = sr >> 3 | xmc[26] << 13;
//			sr = sr >> 3 | xmc[27] << 13;
//			sr = sr >> 3 | xmc[28] << 13;
//			*c++ = uint8_t(sr >> 6);
//			sr = sr >> 3 | xmc[29] << 13;
//			sr = sr >> 3 | xmc[30] << 13;
//			*c++ = uint8_t(sr >> 8);
//			sr = sr >> 3 | xmc[31] << 13;
//			sr = sr >> 3 | xmc[32] << 13;
//			sr = sr >> 3 | xmc[33] << 13;
//			*c++ = uint8_t(sr >> 7);
//			sr = sr >> 3 | xmc[34] << 13;
//			sr = sr >> 3 | xmc[35] << 13;
//			sr = sr >> 3 | xmc[36] << 13;
//			*c++ = uint8_t(sr >> 6);
//			sr = sr >> 3 | xmc[37] << 13;
//			sr = sr >> 3 | xmc[38] << 13;
//			*c++ = uint8_t(sr >> 8);
//			sr = sr >> 7 | Nc[3] << 9;
//			sr = sr >> 2 | bc[3] << 14;
//			*c++ = uint8_t(sr >> 7);
//			sr = sr >> 2 | Mc[3] << 14;
//			sr = sr >> 6 | xmaxc[3] << 10;
//			*c++ = uint8_t(sr >> 7);
//			sr = sr >> 3 | xmc[39] << 13;
//			sr = sr >> 3 | xmc[40] << 13;
//			sr = sr >> 3 | xmc[41] << 13;
//			*c++ = uint8_t(sr >> 6);
//			sr = sr >> 3 | xmc[42] << 13;
//			sr = sr >> 3 | xmc[43] << 13;
//			*c++ = uint8_t(sr >> 8);
//			sr = sr >> 3 | xmc[44] << 13;
//			sr = sr >> 3 | xmc[45] << 13;
//			sr = sr >> 3 | xmc[46] << 13;
//			*c++ = uint8_t(sr >> 7);
//			sr = sr >> 3 | xmc[47] << 13;
//			sr = sr >> 3 | xmc[48] << 13;
//			sr = sr >> 3 | xmc[49] << 13;
//			*c++ = uint8_t(sr >> 6);
//			sr = sr >> 3 | xmc[50] << 13;
//			sr = sr >> 3 | xmc[51] << 13;
//			*c++ = uint8_t(sr >> 8);
//		}
//	}
//	else
//	{
//		*c++ = ((GSM_MAGIC & 0xF) << 4)		/* 1 */
//			| ((LARc[0] >> 2) & 0xF);
//		*c++ = ((LARc[0] & 0x3) << 6)
//			| (LARc[1] & 0x3F);
//		*c++ = ((LARc[2] & 0x1F) << 3)
//			| ((LARc[3] >> 2) & 0x7);
//		*c++ = ((LARc[3] & 0x3) << 6)
//			| ((LARc[4] & 0xF) << 2)
//			| ((LARc[5] >> 2) & 0x3);
//		*c++ = ((LARc[5] & 0x3) << 6)
//			| ((LARc[6] & 0x7) << 3)
//			| (LARc[7] & 0x7);
//		*c++ = ((Nc[0] & 0x7F) << 1)
//			| ((bc[0] >> 1) & 0x1);
//		*c++ = ((bc[0] & 0x1) << 7)
//			| ((Mc[0] & 0x3) << 5)
//			| ((xmaxc[0] >> 1) & 0x1F);
//		*c++ = ((xmaxc[0] & 0x1) << 7)
//			| ((xmc[0] & 0x7) << 4)
//			| ((xmc[1] & 0x7) << 1)
//			| ((xmc[2] >> 2) & 0x1);
//		*c++ = ((xmc[2] & 0x3) << 6)
//			| ((xmc[3] & 0x7) << 3)
//			| (xmc[4] & 0x7);
//		*c++ = ((xmc[5] & 0x7) << 5)			/* 10 */
//			| ((xmc[6] & 0x7) << 2)
//			| ((xmc[7] >> 1) & 0x3);
//		*c++ = ((xmc[7] & 0x1) << 7)
//			| ((xmc[8] & 0x7) << 4)
//			| ((xmc[9] & 0x7) << 1)
//			| ((xmc[10] >> 2) & 0x1);
//		*c++ = ((xmc[10] & 0x3) << 6)
//			| ((xmc[11] & 0x7) << 3)
//			| (xmc[12] & 0x7);
//		*c++ = ((Nc[1] & 0x7F) << 1)
//			| ((bc[1] >> 1) & 0x1);
//		*c++ = ((bc[1] & 0x1) << 7)
//			| ((Mc[1] & 0x3) << 5)
//			| ((xmaxc[1] >> 1) & 0x1F);
//		*c++ = ((xmaxc[1] & 0x1) << 7)
//			| ((xmc[13] & 0x7) << 4)
//			| ((xmc[14] & 0x7) << 1)
//			| ((xmc[15] >> 2) & 0x1);
//		*c++ = ((xmc[15] & 0x3) << 6)
//			| ((xmc[16] & 0x7) << 3)
//			| (xmc[17] & 0x7);
//		*c++ = ((xmc[18] & 0x7) << 5)
//			| ((xmc[19] & 0x7) << 2)
//			| ((xmc[20] >> 1) & 0x3);
//		*c++ = ((xmc[20] & 0x1) << 7)
//			| ((xmc[21] & 0x7) << 4)
//			| ((xmc[22] & 0x7) << 1)
//			| ((xmc[23] >> 2) & 0x1);
//		*c++ = ((xmc[23] & 0x3) << 6)
//			| ((xmc[24] & 0x7) << 3)
//			| (xmc[25] & 0x7);
//		*c++ = ((Nc[2] & 0x7F) << 1)			/* 20 */
//			| ((bc[2] >> 1) & 0x1);
//		*c++ = ((bc[2] & 0x1) << 7)
//			| ((Mc[2] & 0x3) << 5)
//			| ((xmaxc[2] >> 1) & 0x1F);
//		*c++ = ((xmaxc[2] & 0x1) << 7)
//			| ((xmc[26] & 0x7) << 4)
//			| ((xmc[27] & 0x7) << 1)
//			| ((xmc[28] >> 2) & 0x1);
//		*c++ = ((xmc[28] & 0x3) << 6)
//			| ((xmc[29] & 0x7) << 3)
//			| (xmc[30] & 0x7);
//		*c++ = ((xmc[31] & 0x7) << 5)
//			| ((xmc[32] & 0x7) << 2)
//			| ((xmc[33] >> 1) & 0x3);
//		*c++ = ((xmc[33] & 0x1) << 7)
//			| ((xmc[34] & 0x7) << 4)
//			| ((xmc[35] & 0x7) << 1)
//			| ((xmc[36] >> 2) & 0x1);
//		*c++ = ((xmc[36] & 0x3) << 6)
//			| ((xmc[37] & 0x7) << 3)
//			| (xmc[38] & 0x7);
//		*c++ = ((Nc[3] & 0x7F) << 1)
//			| ((bc[3] >> 1) & 0x1);
//		*c++ = ((bc[3] & 0x1) << 7)
//			| ((Mc[3] & 0x3) << 5)
//			| ((xmaxc[3] >> 1) & 0x1F);
//		*c++ = ((xmaxc[3] & 0x1) << 7)
//			| ((xmc[39] & 0x7) << 4)
//			| ((xmc[40] & 0x7) << 1)
//			| ((xmc[41] >> 2) & 0x1);
//		*c++ = ((xmc[41] & 0x3) << 6)			/* 30 */
//			| ((xmc[42] & 0x7) << 3)
//			| (xmc[43] & 0x7);
//		*c++ = ((xmc[44] & 0x7) << 5)
//			| ((xmc[45] & 0x7) << 2)
//			| ((xmc[46] >> 1) & 0x3);
//		*c++ = ((xmc[46] & 0x1) << 7)
//			| ((xmc[47] & 0x7) << 4)
//			| ((xmc[48] & 0x7) << 1)
//			| ((xmc[49] >> 2) & 0x1);
//		*c++ = ((xmc[49] & 0x3) << 6)
//			| ((xmc[50] & 0x7) << 3)
//			| (xmc[51] & 0x7);
//
//	}
//	return 0;
//}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t rtx_gsm_codec_encoder::packetize(rtp_packet* packet, const uint8_t* buff, uint32_t buff_size)
{
	if (packet == nullptr)
	{
		return 0;
	}

	if (buff == nullptr)
	{
		return 0;
	}

	if (buff_size > rtp_packet::PAYLOAD_BUFFER_SIZE)
	{
		return 0;
	}

	if (packet->set_payload(buff, buff_size) == 0)
	{
		return 0;
	}

	packet->set_payload_type(3);
	
	if (m_msgsm)
	{
		packet->set_samples(320);
	}
	else
	{
		packet->set_samples(160);
	}

	return buff_size;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
