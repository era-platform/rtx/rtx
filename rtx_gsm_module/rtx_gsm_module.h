﻿/* RTX H.248 Media Gate. GSM610 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#if defined (TARGET_OS_WINDOWS)
#ifdef RTX_GSM_MODULE_EXPORTS
#define RTX_GSM_MODULE_API __declspec(dllexport)
#else
#define RTX_GSM_MODULE_API __declspec(dllimport)
#endif
#else
#define RTX_GSM_MODULE_API
#endif
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "std_basedef.h"
#include "std_typedef.h"
#include "std_funcdef.h"

#include "rtx_codec.h"
#include "rtx_audio_codec.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
extern "C"
{
	//------------------------------------------------------------------------------
	RTX_GSM_MODULE_API bool initlib();
	//------------------------------------------------------------------------------
	RTX_GSM_MODULE_API void freelib();
	//------------------------------------------------------------------------------
	RTX_GSM_MODULE_API const char* get_codec_list();
	//------------------------------------------------------------------------------
	RTX_GSM_MODULE_API bool get_available_audio_payloads(media::PayloadSet& set);
	//------------------------------------------------------------------------------
	RTX_GSM_MODULE_API mg_audio_decoder_t* create_audio_decoder(const media::PayloadFormat& format);
	//------------------------------------------------------------------------------
	RTX_GSM_MODULE_API void release_audio_decoder(mg_audio_decoder_t* decoder);
	//------------------------------------------------------------------------------
	RTX_GSM_MODULE_API mg_audio_encoder_t* create_audio_encoder(const media::PayloadFormat& format);
	//------------------------------------------------------------------------------
	RTX_GSM_MODULE_API void release_audio_encoder(mg_audio_encoder_t* encoder);
}
//------------------------------------------------------------------------------
