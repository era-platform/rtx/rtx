﻿/* RTX H.248 Media Gate. GSM610 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 //------------------------------------------------------------------------------
 // Copyright 1992 by Jutta Degener and Carsten Bormann, Technische
 // Universitaet Berlin.  See the accompanying file "COPYRIGHT" for
 // details.  THERE IS ABSOLUTELY NO WARRANTY FOR THIS SOFTWARE.
 //------------------------------------------------------------------------------
#include "stdafx.h"
#include "gsm6_10_decoder.h"
#include <gsm.h>
#include "rtp_packet.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
extern int g_rtx_gsm_decoder_counter = -1;
#define GSM_FRAME_SIZE	33
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtx_gsm_codec_decoder::rtx_gsm_codec_decoder(rtl::Logger* log) :
	m_log(log)
{
	m_gsm_decoder = nullptr;
	m_msgsm = false;

	rtl::res_counter_t::add_ref(g_rtx_gsm_decoder_counter);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtx_gsm_codec_decoder::~rtx_gsm_codec_decoder()
{
	destroy();

	rtl::res_counter_t::release(g_rtx_gsm_decoder_counter);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtx_gsm_codec_decoder::initialize(const media::PayloadFormat& format)
{
	if (m_gsm_decoder == nullptr)
	{
		m_gsm_decoder = gsm_create();
		m_msgsm = false;
		rtl::String param_str(format.getEncoding());
		if (rtl::String::compare("msgsm", param_str, false) == 0)
		{
			m_msgsm = true;
		}
		
		m_frame_size = (m_msgsm) ? 320 : 160;

		return true;
	}

	return false;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t rtx_gsm_codec_decoder::get_frame_size_pcm()
{
	return m_frame_size * sizeof(uint16_t);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t rtx_gsm_codec_decoder::get_frame_size_cod()
{
	return m_msgsm ? 65 : GSM_FRAME_SIZE;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtx_gsm_codec_decoder::destroy()
{
	if (m_gsm_decoder != nullptr)
	{
		gsm_destroy(m_gsm_decoder);
		m_gsm_decoder = nullptr;
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const char* rtx_gsm_codec_decoder::getEncoding()
{
	return "gsm";
}
//------------------------------------------------------------------------------
// Читаем по 33 байта и декодируем их в 320 байт.
// если msgsm то:
// Читаем по 65 байт и декодируем в 640 байт.
//------------------------------------------------------------------------------
uint32_t rtx_gsm_codec_decoder::decode(const uint8_t* buff_from, uint32_t size_from, uint8_t* buff_to, uint32_t size_to)
{
	if (m_gsm_decoder == nullptr)
	{
		PLOG_MEDIA_ERROR("GSM", "gsm6_10_codec.decode -- gsm decoder is null");
		return 0;
	}

	if (m_msgsm)
	{
		return decode_msgsm(buff_from, size_from, buff_to, size_to);
	}

	uint32_t gsmCount = size_from / GSM_FRAME_SIZE;
	uint32_t pcmSize = 0;

	for (uint32_t i = 0; i < gsmCount; i++)
	{
		if (pcmSize + 320 > size_to)
		{
			PLOG_MEDIA_ERROR("GSM", "gsm6_10_codec.decode -- not enough space for decode");
			return pcmSize;
		}

		gsm_decode(m_gsm_decoder, buff_from, (short*)buff_to);

		buff_from += GSM_FRAME_SIZE;
		buff_to += 320;
		pcmSize += 320;
	}

	return pcmSize;
}
//------------------------------------------------------------------------------
// Читаем по 65 байт и декодируем в 640 байт.
//------------------------------------------------------------------------------
uint32_t rtx_gsm_codec_decoder::decode_msgsm(const uint8_t* from, uint32_t from_length, uint8_t* to, uint32_t to_length)
{
	uint32_t gsmCount = from_length / 65;
	uint32_t pcmSize = 0;

	for (uint32_t i = 0; i < gsmCount; i++)
	{
		if (pcmSize + 640 > to_length)
		{
			PLOG_MEDIA_ERROR("GSM", "gsm6_10_codec.decode_msgsm -- not enough space for decode");
			return pcmSize;
		}

		gsm_decode(m_gsm_decoder, from, (short*)to);
		gsm_decode(m_gsm_decoder, from + GSM_FRAME_SIZE, (short*)to + 160);

		from += 65;
		to += 320;
		pcmSize += 640;
	}

	return pcmSize;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//int rtx_gsm_codec_decoder::decode_element(gsm_state* s, const uint8_t* c, short* target)
//{
//	short LARc[8], Nc[4], Mc[4], bc[4], xmaxc[4], xmc[13 * 4];
//
//	if (s->wav_fmt)
//	{
//		uint16_t sr = 0;
//
//		s->frame_index = !s->frame_index;
//		if (s->frame_index)
//		{
//			sr = *c++;
//			LARc[0] = sr & 0x3f;  sr >>= 6;
//			sr |= (uint16_t)*c++ << 2;
//			LARc[1] = sr & 0x3f;  sr >>= 6;
//			sr |= (uint16_t)*c++ << 4;
//			LARc[2] = sr & 0x1f;  sr >>= 5;
//			LARc[3] = sr & 0x1f;  sr >>= 5;
//			sr |= (uint16_t)*c++ << 2;
//			LARc[4] = sr & 0xf;  sr >>= 4;
//			LARc[5] = sr & 0xf;  sr >>= 4;
//			sr |= (uint16_t)*c++ << 2;			/* 5 */
//			LARc[6] = sr & 0x7;  sr >>= 3;
//			LARc[7] = sr & 0x7;  sr >>= 3;
//			sr |= (uint16_t)*c++ << 4;
//			Nc[0] = sr & 0x7f;  sr >>= 7;
//			bc[0] = sr & 0x3;  sr >>= 2;
//			Mc[0] = sr & 0x3;  sr >>= 2;
//			sr |= (uint16_t)*c++ << 1;
//			xmaxc[0] = sr & 0x3f;  sr >>= 6;
//			xmc[0] = sr & 0x7;  sr >>= 3;
//			sr = *c++;
//			xmc[1] = sr & 0x7;  sr >>= 3;
//			xmc[2] = sr & 0x7;  sr >>= 3;
//			sr |= (uint16_t)*c++ << 2;
//			xmc[3] = sr & 0x7;  sr >>= 3;
//			xmc[4] = sr & 0x7;  sr >>= 3;
//			xmc[5] = sr & 0x7;  sr >>= 3;
//			sr |= (uint16_t)*c++ << 1;			/* 10 */
//			xmc[6] = sr & 0x7;  sr >>= 3;
//			xmc[7] = sr & 0x7;  sr >>= 3;
//			xmc[8] = sr & 0x7;  sr >>= 3;
//			sr = *c++;
//			xmc[9] = sr & 0x7;  sr >>= 3;
//			xmc[10] = sr & 0x7;  sr >>= 3;
//			sr |= (uint16_t)*c++ << 2;
//			xmc[11] = sr & 0x7;  sr >>= 3;
//			xmc[12] = sr & 0x7;  sr >>= 3;
//			sr |= (uint16_t)*c++ << 4;
//			Nc[1] = sr & 0x7f;  sr >>= 7;
//			bc[1] = sr & 0x3;  sr >>= 2;
//			Mc[1] = sr & 0x3;  sr >>= 2;
//			sr |= (uint16_t)*c++ << 1;
//			xmaxc[1] = sr & 0x3f;  sr >>= 6;
//			xmc[13] = sr & 0x7;  sr >>= 3;
//			sr = *c++;				/* 15 */
//			xmc[14] = sr & 0x7;  sr >>= 3;
//			xmc[15] = sr & 0x7;  sr >>= 3;
//			sr |= (uint16_t)*c++ << 2;
//			xmc[16] = sr & 0x7;  sr >>= 3;
//			xmc[17] = sr & 0x7;  sr >>= 3;
//			xmc[18] = sr & 0x7;  sr >>= 3;
//			sr |= (uint16_t)*c++ << 1;
//			xmc[19] = sr & 0x7;  sr >>= 3;
//			xmc[20] = sr & 0x7;  sr >>= 3;
//			xmc[21] = sr & 0x7;  sr >>= 3;
//			sr = *c++;
//			xmc[22] = sr & 0x7;  sr >>= 3;
//			xmc[23] = sr & 0x7;  sr >>= 3;
//			sr |= (uint16_t)*c++ << 2;
//			xmc[24] = sr & 0x7;  sr >>= 3;
//			xmc[25] = sr & 0x7;  sr >>= 3;
//			sr |= (uint16_t)*c++ << 4;			/* 20 */
//			Nc[2] = sr & 0x7f;  sr >>= 7;
//			bc[2] = sr & 0x3;  sr >>= 2;
//			Mc[2] = sr & 0x3;  sr >>= 2;
//			sr |= (uint16_t)*c++ << 1;
//			xmaxc[2] = sr & 0x3f;  sr >>= 6;
//			xmc[26] = sr & 0x7;  sr >>= 3;
//			sr = *c++;
//			xmc[27] = sr & 0x7;  sr >>= 3;
//			xmc[28] = sr & 0x7;  sr >>= 3;
//			sr |= (uint16_t)*c++ << 2;
//			xmc[29] = sr & 0x7;  sr >>= 3;
//			xmc[30] = sr & 0x7;  sr >>= 3;
//			xmc[31] = sr & 0x7;  sr >>= 3;
//			sr |= (uint16_t)*c++ << 1;
//			xmc[32] = sr & 0x7;  sr >>= 3;
//			xmc[33] = sr & 0x7;  sr >>= 3;
//			xmc[34] = sr & 0x7;  sr >>= 3;
//			sr = *c++;				/* 25 */
//			xmc[35] = sr & 0x7;  sr >>= 3;
//			xmc[36] = sr & 0x7;  sr >>= 3;
//			sr |= (uint16_t)*c++ << 2;
//			xmc[37] = sr & 0x7;  sr >>= 3;
//			xmc[38] = sr & 0x7;  sr >>= 3;
//			sr |= (uint16_t)*c++ << 4;
//			Nc[3] = sr & 0x7f;  sr >>= 7;
//			bc[3] = sr & 0x3;  sr >>= 2;
//			Mc[3] = sr & 0x3;  sr >>= 2;
//			sr |= (uint16_t)*c++ << 1;
//			xmaxc[3] = sr & 0x3f;  sr >>= 6;
//			xmc[39] = sr & 0x7;  sr >>= 3;
//			sr = *c++;
//			xmc[40] = sr & 0x7;  sr >>= 3;
//			xmc[41] = sr & 0x7;  sr >>= 3;
//			sr |= (uint16_t)*c++ << 2;			/* 30 */
//			xmc[42] = sr & 0x7;  sr >>= 3;
//			xmc[43] = sr & 0x7;  sr >>= 3;
//			xmc[44] = sr & 0x7;  sr >>= 3;
//			sr |= (uint16_t)*c++ << 1;
//			xmc[45] = sr & 0x7;  sr >>= 3;
//			xmc[46] = sr & 0x7;  sr >>= 3;
//			xmc[47] = sr & 0x7;  sr >>= 3;
//			sr = *c++;
//			xmc[48] = sr & 0x7;  sr >>= 3;
//			xmc[49] = sr & 0x7;  sr >>= 3;
//			sr |= (uint16_t)*c++ << 2;
//			xmc[50] = sr & 0x7;  sr >>= 3;
//			xmc[51] = sr & 0x7;  sr >>= 3;
//
//			s->frame_chain = sr & 0xf;
//		}
//		else
//		{
//			sr = s->frame_chain;
//			sr |= (uint16_t)*c++ << 4;			/* 1 */
//			LARc[0] = sr & 0x3f;  sr >>= 6;
//			LARc[1] = sr & 0x3f;  sr >>= 6;
//			sr = *c++;
//			LARc[2] = sr & 0x1f;  sr >>= 5;
//			sr |= (uint16_t)*c++ << 3;
//			LARc[3] = sr & 0x1f;  sr >>= 5;
//			LARc[4] = sr & 0xf;  sr >>= 4;
//			sr |= (uint16_t)*c++ << 2;
//			LARc[5] = sr & 0xf;  sr >>= 4;
//			LARc[6] = sr & 0x7;  sr >>= 3;
//			LARc[7] = sr & 0x7;  sr >>= 3;
//			sr = *c++;				/* 5 */
//			Nc[0] = sr & 0x7f;  sr >>= 7;
//			sr |= (uint16_t)*c++ << 1;
//			bc[0] = sr & 0x3;  sr >>= 2;
//			Mc[0] = sr & 0x3;  sr >>= 2;
//			sr |= (uint16_t)*c++ << 5;
//			xmaxc[0] = sr & 0x3f;  sr >>= 6;
//			xmc[0] = sr & 0x7;  sr >>= 3;
//			xmc[1] = sr & 0x7;  sr >>= 3;
//			sr |= (uint16_t)*c++ << 1;
//			xmc[2] = sr & 0x7;  sr >>= 3;
//			xmc[3] = sr & 0x7;  sr >>= 3;
//			xmc[4] = sr & 0x7;  sr >>= 3;
//			sr = *c++;
//			xmc[5] = sr & 0x7;  sr >>= 3;
//			xmc[6] = sr & 0x7;  sr >>= 3;
//			sr |= (uint16_t)*c++ << 2;			/* 10 */
//			xmc[7] = sr & 0x7;  sr >>= 3;
//			xmc[8] = sr & 0x7;  sr >>= 3;
//			xmc[9] = sr & 0x7;  sr >>= 3;
//			sr |= (uint16_t)*c++ << 1;
//			xmc[10] = sr & 0x7;  sr >>= 3;
//			xmc[11] = sr & 0x7;  sr >>= 3;
//			xmc[12] = sr & 0x7;  sr >>= 3;
//			sr = *c++;
//			Nc[1] = sr & 0x7f;  sr >>= 7;
//			sr |= (uint16_t)*c++ << 1;
//			bc[1] = sr & 0x3;  sr >>= 2;
//			Mc[1] = sr & 0x3;  sr >>= 2;
//			sr |= (uint16_t)*c++ << 5;
//			xmaxc[1] = sr & 0x3f;  sr >>= 6;
//			xmc[13] = sr & 0x7;  sr >>= 3;
//			xmc[14] = sr & 0x7;  sr >>= 3;
//			sr |= (uint16_t)*c++ << 1;			/* 15 */
//			xmc[15] = sr & 0x7;  sr >>= 3;
//			xmc[16] = sr & 0x7;  sr >>= 3;
//			xmc[17] = sr & 0x7;  sr >>= 3;
//			sr = *c++;
//			xmc[18] = sr & 0x7;  sr >>= 3;
//			xmc[19] = sr & 0x7;  sr >>= 3;
//			sr |= (uint16_t)*c++ << 2;
//			xmc[20] = sr & 0x7;  sr >>= 3;
//			xmc[21] = sr & 0x7;  sr >>= 3;
//			xmc[22] = sr & 0x7;  sr >>= 3;
//			sr |= (uint16_t)*c++ << 1;
//			xmc[23] = sr & 0x7;  sr >>= 3;
//			xmc[24] = sr & 0x7;  sr >>= 3;
//			xmc[25] = sr & 0x7;  sr >>= 3;
//			sr = *c++;
//			Nc[2] = sr & 0x7f;  sr >>= 7;
//			sr |= (uint16_t)*c++ << 1;			/* 20 */
//			bc[2] = sr & 0x3;  sr >>= 2;
//			Mc[2] = sr & 0x3;  sr >>= 2;
//			sr |= (uint16_t)*c++ << 5;
//			xmaxc[2] = sr & 0x3f;  sr >>= 6;
//			xmc[26] = sr & 0x7;  sr >>= 3;
//			xmc[27] = sr & 0x7;  sr >>= 3;
//			sr |= (uint16_t)*c++ << 1;
//			xmc[28] = sr & 0x7;  sr >>= 3;
//			xmc[29] = sr & 0x7;  sr >>= 3;
//			xmc[30] = sr & 0x7;  sr >>= 3;
//			sr = *c++;
//			xmc[31] = sr & 0x7;  sr >>= 3;
//			xmc[32] = sr & 0x7;  sr >>= 3;
//			sr |= (uint16_t)*c++ << 2;
//			xmc[33] = sr & 0x7;  sr >>= 3;
//			xmc[34] = sr & 0x7;  sr >>= 3;
//			xmc[35] = sr & 0x7;  sr >>= 3;
//			sr |= (uint16_t)*c++ << 1;			/* 25 */
//			xmc[36] = sr & 0x7;  sr >>= 3;
//			xmc[37] = sr & 0x7;  sr >>= 3;
//			xmc[38] = sr & 0x7;  sr >>= 3;
//			sr = *c++;
//			Nc[3] = sr & 0x7f;  sr >>= 7;
//			sr |= (uint16_t)*c++ << 1;
//			bc[3] = sr & 0x3;  sr >>= 2;
//			Mc[3] = sr & 0x3;  sr >>= 2;
//			sr |= (uint16_t)*c++ << 5;
//			xmaxc[3] = sr & 0x3f;  sr >>= 6;
//			xmc[39] = sr & 0x7;  sr >>= 3;
//			xmc[40] = sr & 0x7;  sr >>= 3;
//			sr |= (uint16_t)*c++ << 1;
//			xmc[41] = sr & 0x7;  sr >>= 3;
//			xmc[42] = sr & 0x7;  sr >>= 3;
//			xmc[43] = sr & 0x7;  sr >>= 3;
//			sr = *c++;				/* 30 */
//			xmc[44] = sr & 0x7;  sr >>= 3;
//			xmc[45] = sr & 0x7;  sr >>= 3;
//			sr |= (uint16_t)*c++ << 2;
//			xmc[46] = sr & 0x7;  sr >>= 3;
//			xmc[47] = sr & 0x7;  sr >>= 3;
//			xmc[48] = sr & 0x7;  sr >>= 3;
//			sr |= (uint16_t)*c++ << 1;
//			xmc[49] = sr & 0x7;  sr >>= 3;
//			xmc[50] = sr & 0x7;  sr >>= 3;
//			xmc[51] = sr & 0x7;  sr >>= 3;
//		}
//	}
//	else
//	{
//		/* GSM_MAGIC  = (*c >> 4) & 0xF; */
//
//		if (((*c >> 4) & 0x0F) != GSM_MAGIC)
//		{
//			return -1;
//		}
//
//		LARc[0] = (*c++ & 0xF) << 2;		/* 1 */
//		LARc[0] |= (*c >> 6) & 0x3;
//		LARc[1] = *c++ & 0x3F;
//		LARc[2] = (*c >> 3) & 0x1F;
//		LARc[3] = (*c++ & 0x7) << 2;
//		LARc[3] |= (*c >> 6) & 0x3;
//		LARc[4] = (*c >> 2) & 0xF;
//		LARc[5] = (*c++ & 0x3) << 2;
//		LARc[5] |= (*c >> 6) & 0x3;
//		LARc[6] = (*c >> 3) & 0x7;
//		LARc[7] = *c++ & 0x7;
//		Nc[0] = (*c >> 1) & 0x7F;
//		bc[0] = (*c++ & 0x1) << 1;
//		bc[0] |= (*c >> 7) & 0x1;
//		Mc[0] = (*c >> 5) & 0x3;
//		xmaxc[0] = (*c++ & 0x1F) << 1;
//		xmaxc[0] |= (*c >> 7) & 0x1;
//		xmc[0] = (*c >> 4) & 0x7;
//		xmc[1] = (*c >> 1) & 0x7;
//		xmc[2] = (*c++ & 0x1) << 2;
//		xmc[2] |= (*c >> 6) & 0x3;
//		xmc[3] = (*c >> 3) & 0x7;
//		xmc[4] = *c++ & 0x7;
//		xmc[5] = (*c >> 5) & 0x7;
//		xmc[6] = (*c >> 2) & 0x7;
//		xmc[7] = (*c++ & 0x3) << 1;		/* 10 */
//		xmc[7] |= (*c >> 7) & 0x1;
//		xmc[8] = (*c >> 4) & 0x7;
//		xmc[9] = (*c >> 1) & 0x7;
//		xmc[10] = (*c++ & 0x1) << 2;
//		xmc[10] |= (*c >> 6) & 0x3;
//		xmc[11] = (*c >> 3) & 0x7;
//		xmc[12] = *c++ & 0x7;
//		Nc[1] = (*c >> 1) & 0x7F;
//		bc[1] = (*c++ & 0x1) << 1;
//		bc[1] |= (*c >> 7) & 0x1;
//		Mc[1] = (*c >> 5) & 0x3;
//		xmaxc[1] = (*c++ & 0x1F) << 1;
//		xmaxc[1] |= (*c >> 7) & 0x1;
//		xmc[13] = (*c >> 4) & 0x7;
//		xmc[14] = (*c >> 1) & 0x7;
//		xmc[15] = (*c++ & 0x1) << 2;
//		xmc[15] |= (*c >> 6) & 0x3;
//		xmc[16] = (*c >> 3) & 0x7;
//		xmc[17] = *c++ & 0x7;
//		xmc[18] = (*c >> 5) & 0x7;
//		xmc[19] = (*c >> 2) & 0x7;
//		xmc[20] = (*c++ & 0x3) << 1;
//		xmc[20] |= (*c >> 7) & 0x1;
//		xmc[21] = (*c >> 4) & 0x7;
//		xmc[22] = (*c >> 1) & 0x7;
//		xmc[23] = (*c++ & 0x1) << 2;
//		xmc[23] |= (*c >> 6) & 0x3;
//		xmc[24] = (*c >> 3) & 0x7;
//		xmc[25] = *c++ & 0x7;
//		Nc[2] = (*c >> 1) & 0x7F;
//		bc[2] = (*c++ & 0x1) << 1;		/* 20 */
//		bc[2] |= (*c >> 7) & 0x1;
//		Mc[2] = (*c >> 5) & 0x3;
//		xmaxc[2] = (*c++ & 0x1F) << 1;
//		xmaxc[2] |= (*c >> 7) & 0x1;
//		xmc[26] = (*c >> 4) & 0x7;
//		xmc[27] = (*c >> 1) & 0x7;
//		xmc[28] = (*c++ & 0x1) << 2;
//		xmc[28] |= (*c >> 6) & 0x3;
//		xmc[29] = (*c >> 3) & 0x7;
//		xmc[30] = *c++ & 0x7;
//		xmc[31] = (*c >> 5) & 0x7;
//		xmc[32] = (*c >> 2) & 0x7;
//		xmc[33] = (*c++ & 0x3) << 1;
//		xmc[33] |= (*c >> 7) & 0x1;
//		xmc[34] = (*c >> 4) & 0x7;
//		xmc[35] = (*c >> 1) & 0x7;
//		xmc[36] = (*c++ & 0x1) << 2;
//		xmc[36] |= (*c >> 6) & 0x3;
//		xmc[37] = (*c >> 3) & 0x7;
//		xmc[38] = *c++ & 0x7;
//		Nc[3] = (*c >> 1) & 0x7F;
//		bc[3] = (*c++ & 0x1) << 1;
//		bc[3] |= (*c >> 7) & 0x1;
//		Mc[3] = (*c >> 5) & 0x3;
//		xmaxc[3] = (*c++ & 0x1F) << 1;
//		xmaxc[3] |= (*c >> 7) & 0x1;
//		xmc[39] = (*c >> 4) & 0x7;
//		xmc[40] = (*c >> 1) & 0x7;
//		xmc[41] = (*c++ & 0x1) << 2;
//		xmc[41] |= (*c >> 6) & 0x3;
//		xmc[42] = (*c >> 3) & 0x7;
//		xmc[43] = *c++ & 0x7;			/* 30  */
//		xmc[44] = (*c >> 5) & 0x7;
//		xmc[45] = (*c >> 2) & 0x7;
//		xmc[46] = (*c++ & 0x3) << 1;
//		xmc[46] |= (*c >> 7) & 0x1;
//		xmc[47] = (*c >> 4) & 0x7;
//		xmc[48] = (*c >> 1) & 0x7;
//		xmc[49] = (*c++ & 0x1) << 2;
//		xmc[49] |= (*c >> 6) & 0x3;
//		xmc[50] = (*c >> 3) & 0x7;
//		xmc[51] = *c & 0x7;			/* 33 */
//	}
//
//	Gsm_Decoder(s, LARc, Nc, bc, Mc, xmaxc, xmc, target);
//
//	return 0;
//}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t rtx_gsm_codec_decoder::depacketize(const rtp_packet* packet, uint8_t* buff, uint32_t buff_size)
{
	if (packet == nullptr)
	{
		return 0;
	}

	if (buff == nullptr)
	{
		return 0;
	}

	if (buff_size < packet->get_payload_length())
	{
		return 0;
	}

	uint32_t payload_length = packet->get_payload_length();

	memcpy(buff, packet->get_payload(), payload_length);

	return payload_length;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
