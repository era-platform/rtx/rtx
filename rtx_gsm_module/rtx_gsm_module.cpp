﻿/* RTX H.248 Media Gate. GSM610 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 //------------------------------------------------------------------------------
 // Copyright 1992 by Jutta Degener and Carsten Bormann, Technische
 // Universitaet Berlin.  See the accompanying file "COPYRIGHT" for
 // details.  THERE IS ABSOLUTELY NO WARRANTY FOR THIS SOFTWARE.
 //------------------------------------------------------------------------------
#include "stdafx.h"

#include "rtx_gsm_module.h"
#include "std_logger.h"

#include "gsm6_10_decoder.h"
#include "gsm6_10_encoder.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#if defined (DEBUG_MEMORY)
void* operator new (size_t size, char* file, int line)
{
	return std_mem_checker_malloc(size, file, line, rc_malloc_new_e);
}
void* operator new[](size_t size, char* file, int line)
{
	return std_mem_checker_malloc(size, file, line, rc_malloc_newar_e);
}
void operator delete (void* mem_ptr)
{
	std_mem_checker_free(mem_ptr, rc_malloc_delete_e);
}
void operator delete[](void* mem_ptr)
{
	std_mem_checker_free(mem_ptr, rc_malloc_deletear_e);
}
#endif
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_GSM_MODULE_API const char* get_codec_list()
{
	return "GSM";
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_GSM_MODULE_API bool get_available_audio_payloads(media::PayloadSet& set)
{
	set.addStandardFormat(media::PayloadId::GSM610);

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_GSM_MODULE_API bool initlib()
{
	g_rtx_gsm_decoder_counter = rtl::res_counter_t::add("mge_gsm_decoder");
	g_rtx_gsm_encoder_counter = rtl::res_counter_t::add("mge_gsm_encoder");

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_GSM_MODULE_API void freelib()
{
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_GSM_MODULE_API mg_audio_decoder_t* create_audio_decoder(const media::PayloadFormat& format)
{
	rtl::String codec_name(format.getEncoding());
	if (codec_name.isEmpty())
	{
		return nullptr;
	}
	codec_name.trim();
	codec_name.toLower();

	if (rtl::String::compare("gsm", codec_name, false) == 0)
	{
		rtx_gsm_codec_decoder* decoder = NEW rtx_gsm_codec_decoder(&Err);
		if (decoder->initialize(format))
		{
			return decoder;
		}

		DELETEO(decoder);
	}

	return nullptr;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_GSM_MODULE_API void release_audio_decoder(mg_audio_decoder_t* decoder)
{
	if (decoder == nullptr)
	{
		return;
	}

	rtl::String type = decoder->getEncoding();

	type.toLower();

	if (rtl::String::compare("gsm", type, false) == 0)
	{
		rtx_gsm_codec_decoder* gsm_decoder = (rtx_gsm_codec_decoder*)decoder;
		if (gsm_decoder != nullptr)
		{
			gsm_decoder->destroy();
			DELETEO(gsm_decoder);
			gsm_decoder = nullptr;
		}
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_GSM_MODULE_API mg_audio_encoder_t* create_audio_encoder(const media::PayloadFormat& format)
{
	rtl::String codec_name(format.getEncoding());
	if (codec_name.isEmpty())
	{
		return nullptr;
	}
	codec_name.trim();
	codec_name.toLower();

	if (rtl::String::compare("gsm", codec_name, false) == 0)
	{
		rtx_gsm_codec_encoder* encoder = NEW rtx_gsm_codec_encoder(&Err);
		if (encoder->initialize(format))
		{
			return encoder;
		}

		DELETEO(encoder);
	}

	return nullptr;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_GSM_MODULE_API void release_audio_encoder(mg_audio_encoder_t* encoder)
{
	if (encoder == nullptr)
	{
		return;
	}

	rtl::String type(encoder->getEncoding());
	type.toLower();

	if (rtl::String::compare("gsm", type, false) == 0)
	{
		rtx_gsm_codec_encoder* gsm_encoder = (rtx_gsm_codec_encoder*)encoder;
		if (gsm_encoder != nullptr)
		{
			gsm_encoder->destroy();
			DELETEO(gsm_encoder);
			gsm_encoder = nullptr;
		}
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
