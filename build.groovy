import groovy.json.JsonSlurperClassic

def build()
{
    try
    {
        build_and_test()

        stage('rtx: successx')
        {
            sh (script: "./build.sh finalize")
        }
    }
    catch (ex)
    {
        echo "rtx pipeline crashed: ${ex}"
        throw ex
    }
    finally
    {
        sh (script: "cp build/*.xml _test/ || true") // '|| true' will prevent crash when no files matched the glob.
    }
}

def build_and_test()
{
    stage('rtx: initial clean')
    {
        sh (script: "./build.sh initial_clean")
    }

    stage('rtx: build rtx')
    {
        sh (script: "./build.sh build_rtx")
    }

    stage('rtx: test rtx_media_tools')
    {
        sh (script: "./build.sh test_rtx_media_tools")
    }

    stage('rtx: test rtx_megaco')
    {
        sh (script: "./build.sh test_rtx_megaco")
    }

    stage('rtx: test rtx_media_engine')
    {
        sh (script: "./build.sh test_rtx_media_engine")
    }

    stage('rtx: test rtx_mixer')
    {
        sh (script: "./build.sh test_rtx_mixer")
    }
}

// called by pull_requests pipeline
def validate()
{
    build_and_test()
}

return this;
