= Сервис Медийного шлюза
:toc-title: Содержание
:toc:

== Описание

=== Параметры командной строки

Для запуска медиа сервиса используется единственный параметр -- путь к файлу конфигурации. Все настройки и параметры работы сервиса определяются только в конфигурационном файле.

=== Параметры файла конфигурации

Дополнительно к общим парметрам конфигурационного файла сервис медиа шлюза использует еще 2 набора параметров:

* параметры H.248 (MEGACO).
* параметры медиашлюза.

.table Параметры H.248 (MEGACO)
[width="100%",cols="2a,1a,6a",options="header"]
|====
|Ключ|Тип|Описание
|`megaco-mid-type`| `_string_`  | Тип ключ связки MGC - MG.
|`megaco-mid-postfix`| `_string_`  | Ключ связки MGC - MG.
|||
|`megaco-message-pretty`| `_boolean_`|Формирование пакетов H.248 в читаемом виде.
|`megaco-message-pretty-indent-tab`| `_boolean_`|Использовать табуляцию для отступов.
|`megaco-message-pretty-indent`| `_integer_`  | Количество пробелов если не используются табуляция для отступов
| | |
|`megaco-def-text-port`| `_integer_`  |Сетевой порт для слушателя протокола H.248. Порт по умолчанию *2947*.
|`megaco-def-text-interface`| `_string_`  |Сетевой интерфейс для слушателя протокола H.248. Интерфейс по умолчанию *eth0*.
|`megaco-media-gate-lib`| `_string_`  | Имя модуля реализующего медиа движок. Обязательное поле с единственно возможным значением `rtx_media_engine.dll` для Windows и `librtx_media_engine.so` для Linux.
|`megaco-control-list`| `_string_`  | Список MGC для подключения вида:
[source]
----
tcp 192.168.0.10:2944; udp 192.168.0.110:2944
----
где элемнтом списка является строки формата `протокол адрес:порт` разделенные символом `';'`
|====

.table Параметры медиашлюза
[width="100%",cols="2a,1a,6a",options="header"]
|====
|Ключ|Тип|Описание
|`mg-path-count` | `_integer_` | Количество элементов `mg-path-xx` для чтения.
|`mg-path-__xx__`    | `_string_`  | Локальный путь к корневому ключу. Например: +
 `:SYNC/c:/rostell-er/site1/media/` +
 _xx_ - порядковый номер читаемого пути. Начинается с еденицы.

|`mg-lazy-record-time`| `_integer_` | Частота сброса буферов RTP трафика на диск во время записи разговоров. +
 По умолчанию 5000 миллисекунд.
|mg-dtmf| `_boolean_`  |Включение DTMF детектора. При обнаружении в трафике _RTP_ пакетов объявленные как _DTMF_ генерируется событие в *MGC*
| | |
|mg-term-prefix-count| `_integer_` | Количество сетевых интерфейсов используемых для передачи медиа потоков.
|mg-term-prefix-*xx*| `_string_`  | Описатель интерфейса задействованного для _RTP_ трафика. *xx* -- порядковый номер интерфейса. Начинается с еденицы. +
[source]
----
  tgw 192.168.0.32
----
где `tgw` имя интерфейса, а  `192.168.0.32` физический адрес.
|mg-term-rtp-range-count| `_integer_` |Количество областей портов UDP резервируемых для медиа данных.
|mg-term-rtp-range-*xx*| `_string_`  |Резервирование области портов для интерфейса. *xx* -- порядковый номер интерфейса. Начинается с еденицы. +
[source]
----
  tgw 20100:3000
----
где `tgw` имя интерфейса, `20100` номер первого порта в области а `3000` количество резервируемых портов.
|====

=== Пример файла конфигурации

[source]
----
;logger settings
log-root-path = c:/_rostell-er/Site1/rtx_log
log-trace = CALL EVENT TRANS ERROR WARNING PROTO RTP RTP-FLOW FLAG8 MEDIA-FLOW STAT FLAG7 FLAG8 VAD FLAG4

;log parts in megabytes
log-max-size = 0
log-part-size = 200

; async caller settings
async-thread-initial = 4
async-thread-max = 16

mg-path-count = 4
mg-path-1 = :SYNC/c:/_rostell-er/site1/media/
mg-path-2 = :RECORD/c:/_rostell-er/site1/call_records/
mg-path-3 = :SITESHARE/c:/_rostell-er/site1/share
mg-path-4 = :GLOBALSHARE/c:/_rostell-er/globals

mg-lazy-record-time = 5000

megaco-def-text-port = 2947
megaco-def-text-interface = eth0
mg-dtmf = rfc2833
; megaco settings
megaco-media-gate-lib = rtx_media_engine.dll
; transport1 type address:port [';' transport2 type address:port]
;megaco-control-list = tcp 192.168.0.12:2944
megaco-control-list = tcp 192.168.0.10:2944

; transport type address:port

; MId types : ipv4 ipv6 domain device (mtp not used)
; domain -> megaco-mid-domain = <domain.org>
; device -> will be used format rtx_mg_127.0.0.1
; ipv4 -> [ipv4]:port
; ipv6 -> [ipv6]:port
megaco-mid-type = device
megaco-mid-postfix = def_123
; megaco message pretty output : long names, separate lines, indent nested tokens
; megaco message concise output : short names, one line, no indent
megaco-message-pretty = true;
megaco-message-pretty-indent-tab = true;
megaco-message-pretty-indent = 4;

; named interfaces aka prefixes
; count of legal prefixes -- default 10
mg-term-prefix-count = 1
; format: prefix ' ' ipv4 | ipv6
mg-term-prefix-1 = tgw 192.168.0.10
; count of port ranges -- default 10
mg-term-rtp-range-count = 1
; format: prefix ' ' port_begin ':' port_count
mg-term-rtp-range-1 = tgw 10000:1000
----
