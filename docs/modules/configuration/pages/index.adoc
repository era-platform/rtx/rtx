:toc-title: Оглавление
:toc:

** xref:configuration:common.adoc[Общие правила]
** xref:configuration:rtx_mg3.adoc[Сервис Медийного шлюза]
** xref:configuration:rtx_mixer.adoc[Микшер аудио/видео файлов]
** xref:configuration:rtx_client.adoc[SIP клиент]

