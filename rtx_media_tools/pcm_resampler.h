﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "std_typedef.h"
#include "std_logger.h"
#include "std_mutex.h"

#include "mmt_resampler.h"

//-----------------------------------------------
//
//-----------------------------------------------
extern int g_rtx_pcm_resampler_counter;

namespace media {
	//-----------------------------------------------
	//
	//-----------------------------------------------
	class pcm_resampler : public media::Resampler
	{
		pcm_resampler(const pcm_resampler&);
		pcm_resampler& operator=(const pcm_resampler&);

	public:
		pcm_resampler(rtl::Logger* log);
		virtual ~pcm_resampler();

		virtual bool init(int in_rate, int in_channels, int out_rate, int out_channels);
		virtual void dispose();

		virtual int resample(const short* src, int src_length, int* src_used, short* dst_buffer, int dst_buffer_size);
		virtual bool isSimple() { return true; }
	private:
		rtl::Logger* m_log;
		uint32_t m_in_rate;
		uint32_t m_out_rate;
		int32_t m_state[8];
	};
}
//-----------------------------------------------
