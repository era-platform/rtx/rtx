﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "mmt_lazy_writer_rtp.h"

namespace media
{
	//--------------------------------------
	//
	//--------------------------------------
	LazyMediaWriterRTP::LazyMediaWriterRTP(rtl::Logger* log) : LazyMediaWriter(log),
		m_timestamp_flag(true), m_timestamp_begin(0), m_timestamp_end(0), m_timestamp_last_pack(0), m_format_changed(false)
	{
		memset(&m_file_header, 0, sizeof m_file_header);
	}
	//--------------------------------------
	//
	//--------------------------------------
	LazyMediaWriterRTP::~LazyMediaWriterRTP()
	{
		destroy();
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool LazyMediaWriterRTP::create(const char* path)
	{
		PLOG_CALL("rtp-rec", "Create file...'%s'", path);

		if (!m_file.createNew(path))
		{
			PLOG_WARNING("rtp-rec", "Can't create file for recording : last error %d", std_get_error);
			return false;
		}

		// запишем шапку в файл
		memset(&m_file_header, 0, sizeof m_file_header);

		size_t path_l = strlen(path);

		if (std_stricmp(&path[path_l - 5], ".rtpv") == 0)
			m_file_header.sign = MG_REC_VIDEO_SIGNATURE;
		else
			m_file_header.sign = MG_REC_AUDIO_SIGNATURE;

		m_file_header.rec_start_time = rtl::DateTime::getCurrentGTS();
		m_file_header.rec_start_timestamp = rtl::DateTime::getTicks();

		PLOG_CALL("rtp-rec", "File created. write header infotmation...");

		int toWrite = sizeof(RTPFileHeader);

		if (!m_file.write(&m_file_header, toWrite))
		{
			close();
			PLOG_WARNING("rtp-rec", "Can't write header to file! Last error %d", std_get_error);
			return false;
		}

		LazyMediaWriter::initialize();

		PLOG_CALL("rtp-rec", "File created and updated.");

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool LazyMediaWriterRTP::create(const char* path, const media::PayloadSet& formats)
	{
		bool result = create(path);

		if (result)
			update_format(formats);

		return result;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void LazyMediaWriterRTP::close()
	{
		LazyMediaWriter::destroy();

		if (m_file.isOpened())
		{
			PLOG_CALL("rtp-rec", "Closeing file...");
			// обновим заголовок файла
			m_file_header.rec_stop_timestamp = rtl::DateTime::getTicks();

			int toWrite = sizeof(RTPFileHeader);

			m_file.setPosition(0);
			m_file.write(&m_file_header, toWrite);
			m_file.seekEnd(0);

			m_file.close();
			PLOG_CALL("rtp-rec", "File closed.");
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool LazyMediaWriterRTP::write_packet(const rtp_packet* packet)
	{
		if (packet == nullptr)
		{
			m_file_header.dropped_net_packets++;
			return false;
		}

		if (m_started)
		{
			//PLOG_CALL("rtp-rec", "Write packet (length %d) to cache...", packet->get_payload_length());

			m_write = true;

			if (m_format_changed)
			{
				write_format(m_additional_formats);
				m_format_changed = false;
			}

			int len = packet->get_full_length();// +rtp_packet::FIXED_RTP_HEADER_LENGTH;
			int blockLen = RTP_FILE_PACKET_HEADER_LENGTH + len;
			// получим блок кеша для записи пакета + заголовка
			LazyBlock* block = get_current_block(blockLen);

			m_written_to_buffer += blockLen;

			if (block != nullptr)
			{
				// запишем информацию о пакете в заголовок
				RTPFilePacketHeader* rec_pack = (RTPFilePacketHeader*)block->rec_pos;
				m_file_header.rec_stop_timestamp = rec_pack->timestamp = rtl::DateTime::getTicks();
				rec_pack->type = RTYPE_RTP;
				rec_pack->length = len;

				// скопируем сам пакет
				int writen = packet->write_packet(rec_pack->buffer, sizeof(rec_pack->buffer));
				if (writen != len)
				{
					PLOG_WARNING("rtp-rec", "Write packet to cache failed : writen (%d) != len (%d)", writen, len);
				}

				// сдвинем указатель на свободный буфер в блоке
				block->rec_pos = ((uint8_t*)rec_pack) + blockLen;
				block->rec_length += blockLen;

				m_file_header.packets_rx++;

				m_file_header.media_length += packet->get_payload_length();

				// созраним первый таймштамп
				if (m_timestamp_flag)
				{
					m_file_header.rx_start_timestamp = m_timestamp_begin = packet->getTimestamp();
					m_timestamp_flag = false;
				}

				// последний таймштамп всегда сохраняем
				m_file_header.rx_stop_timestamp = packet->getTimestamp();
				m_timestamp_end = packet->getTimestamp();
				m_timestamp_last_pack = rec_pack->timestamp;
			}
			else
			{
				PLOG_WARNING("rtp-rec", "Write packet to cache failed : cache is full -- packet dropped");
				m_file_header.dropped_rec_packets++;
				//m_rtp_test_flag = false;
				m_lost_in_buffer += blockLen;
			}

			m_write = false;
		}

		// всегда возвращаем истину 
		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void LazyMediaWriterRTP::update_format(const media::PayloadSet& formats)
	{
		if (!m_started)
		{
			rec_write_format(formats);
		}
		else
		{
			m_additional_formats = formats;
			m_format_changed = true;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void LazyMediaWriterRTP::rec_file_started()
	{
		m_timestamp_flag = true;
		m_timestamp_end = m_timestamp_begin = 0;
		m_timestamp_last_pack = rtl::DateTime::getTicks();
	}
	//--------------------------------------
	//
	//--------------------------------------
	void LazyMediaWriterRTP::rec_file_stopped()
	{
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool LazyMediaWriterRTP::rec_file_write(void* dataBlock, uint32_t length, uint32_t* written)
	{
		if (!m_file.isOpened())
		{
			PLOG_WARNING("rtp-rec", "Write block failed : file not open!");
			return false;
		}

		PLOG_CALL("rtp-rec", "Write block size %u to file...", length);

		*written = (uint32_t)m_file.write(dataBlock, length);

		PLOG_CALL("rtp-rec", "Block written size %u", *written);

		return *written == length;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void LazyMediaWriterRTP::write_format(const media::PayloadSet& formats)
	{
		PLOG_CALL("rtp-rec", "Write format to cache...");

		if (formats.getCount() == 0)
		{
			PLOG_WARNING("rtp-rec", "Write format to cache failed : formats is full");
			return;
		}

		int len = sizeof(RTPFileMediaFormat) * formats.getCount();
		// получим блок кеша для записи пакета
		LazyBlock* block = get_current_block(len);

		if (len > int(rtp_packet::PAYLOAD_BUFFER_SIZE + rtp_packet::FIXED_RTP_HEADER_LENGTH))
		{
			PLOG_WARNING("rtp-rec", "Write format to cache failed : buffer small.");
			return;
		}

		if (block != nullptr)
		{
			// запишем информацию о пакете в заголовок
			RTPFilePacketHeader* rec_pack = (RTPFilePacketHeader*)block->rec_pos;
			rec_pack->timestamp = rtl::DateTime::getTicks();
			rec_pack->type = RTYPE_FORMAT;
			rec_pack->length = len;

			for (int i = 0; i < formats.getCount(); i++)
			{
				uint32_t index = i;
				const media::PayloadFormat& fmt_from_list = formats.getAt(i);
				if (fmt_from_list.isEmpty())
				{
					index = ((i - 1) < 0) ? 0 : (i - 1);
					continue;
				}

				// скопируем форматы
				RTPFileMediaFormat& fmt = rec_pack->format[index];

				strncpy(fmt.encoding, fmt_from_list.getEncoding(), RTP_FILE_FMT_ENCODING_SIZE - 1);
				fmt.encoding[RTP_FILE_FMT_ENCODING_SIZE - 1] = 0;

				fmt.payload_id = fmt_from_list.getId_u8();
				fmt.clock_rate = fmt_from_list.getFrequency();
				fmt.channels = fmt_from_list.getChannelCount();
				const rtl::String& params = fmt_from_list.getParams();
				if (!params.isEmpty())
				{
					strncpy(fmt.params, params, RTP_FILE_FMT_PARAMS_SIZE - 1);
					fmt.params[RTP_FILE_FMT_PARAMS_SIZE - 1] = 0;
				}
				const rtl::String& fmtp = fmt_from_list.getFmtp();
				if (!fmtp.isEmpty())
				{
					strncpy(fmt.fmtp, fmtp, RTP_FILE_FMT_FMTP_SIZE - 1);
					fmt.fmtp[RTP_FILE_FMT_FMTP_SIZE - 1] = 0;
				}
			}

			int blockLen = RTP_FILE_PACKET_HEADER_LENGTH + len;

			// сдвинем указатель на свободный буфер в блоке
			block->rec_pos = ((uint8_t*)rec_pack) + blockLen;
			block->rec_length += blockLen;

			// подготовим счетчик семплов
			m_timestamp_flag = true;
			m_timestamp_end = m_timestamp_begin = 0;
		}
		else
		{
			PLOG_WARNING("rtp-rec", "Write format to cache failed : cache is full");
			//m_rtp_test_flag = false;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void LazyMediaWriterRTP::payload_to_rtpfmt(const media::PayloadFormat& pl_fmt, RTPFileMediaFormat* rtp_fmt)
	{
		rtp_fmt->payload_id = pl_fmt.getId_u8();
		rtp_fmt->clock_rate = pl_fmt.getFrequency();
		rtp_fmt->channels = pl_fmt.getChannelCount();
		strncpy(rtp_fmt->encoding, pl_fmt.getEncoding(), 32);
		rtp_fmt->encoding[31] = 0;
		strncpy(rtp_fmt->params, pl_fmt.getParams(), 32);
		rtp_fmt->params[31] = 0;
		strncpy(rtp_fmt->fmtp, pl_fmt.getFmtp(), 64);
		rtp_fmt->fmtp[63] = 0;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void LazyMediaWriterRTP::rec_write_format(const media::PayloadSet& formats)
	{
		RTPFilePacketHeader packet;
		packet.type = RTYPE_FORMAT;
		packet.timestamp = rtl::DateTime::getTicks();
		packet.length = uint16_t(formats.getCount() * sizeof(RTPFileMediaFormat));

		for (int i = 0; i < formats.getCount(); i++)
		{
			payload_to_rtpfmt(formats.getAt(i), &packet.format[i]);
		}

		m_file.write(&packet, packet.length + RTP_FILE_PACKET_HEADER_LENGTH);
	}
}
//--------------------------------------
