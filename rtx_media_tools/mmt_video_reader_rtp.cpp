/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_video_reader_rtp.h"

namespace media
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VideoReaderRTP::VideoReaderRTP(rtl::Logger* log) : VideoReader(log),
		m_jitter(&Log),
		m_newStream(true),
		m_work(nullptr), m_wait(nullptr),
		m_workPtr(0),
		m_thirdBuffer(nullptr)
	{
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VideoReaderRTP::~VideoReaderRTP()
	{
		cleanup();
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoReaderRTP::initialize()
	{
		rtl::MutexLock lock(m_sync);

		m_work = NEW RTPSorter;
		memset(m_work, 0, sizeof(*m_work));

		m_wait = NEW RTPSorter;
		memset(m_wait, 0, sizeof(*m_wait));

		m_thirdBuffer = NEW RTPFilePacketHeader;
		memset(m_thirdBuffer, 0, sizeof(*m_thirdBuffer));
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoReaderRTP::cleanup()
	{
		rtl::MutexLock lock(m_sync);

		if (m_work)
		{
			DELETEO(m_work);
			m_work = nullptr;
		}

		if (m_wait)
		{
			DELETEO(m_wait);
			m_wait = nullptr;
		}

		if (m_thirdBuffer)
		{
			DELETEO(m_thirdBuffer);
			m_thirdBuffer = nullptr;
		}

		cleanupDecoders();
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoReaderRTP::cleanupDecoders()
	{
		for (int i = 0; i < m_decoderList.getCount(); i++)
		{
			DecoderInfo& info = m_decoderList[i];

			if (info.decoder != nullptr)
			{
				rtx_codec__release_video_decoder(info.decoder);
				info.decoder = nullptr;
			}
		}

		m_decoderList.clear();
	}
	//-----------------------------------------------
	/// reader interface
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool VideoReaderRTP::openSource(const char* path)
	{
		// ������� ������ ����
		close();

		initialize();

		// ��������� ������� ����
		if (!m_file.open(path))
		{
			LOG_ERROR("lrtp-src", "Can't open file '%s' : last error %d", path, errno);

			cleanup();

			return false;
		}

		m_GTTStartTime = m_file.get_header().rec_start_time;
		m_filePath = path;

		rtl::String ppath = path;

		//m_out.create(ppath + ".raw");

		return true;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoReaderRTP::closeSource()
	{
		m_file.close();

		m_filePath = rtl::String::empty;

		cleanup();

		//m_out.close();
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool VideoReaderRTP::readImage(VideoImage* image)
	{
		rtl::MutexLock lock(m_sync);

		return readFrame(image);
	}
	//-----------------------------------------------
	/// pcm fill functions
	//-----------------------------------------------
	// reading pcm using cache
	//-----------------------------------------------
	bool VideoReaderRTP::readFrame(VideoImage* image)
	{
		if (!fillJitterBuffer())
			return false;

		// jitter state is ready!
		VideoJitterBuffer frame;
		m_jitter.getReadyFrame(frame);

		return decodeFrame(image, &frame);
	}
	//-----------------------------------------------
	// called when cache is empty!
	//-----------------------------------------------
	bool VideoReaderRTP::fillJitterBuffer()
	{
		rtp_packet packet;

		while (getNextPacket(&packet))
		{
			m_jitter.pushPacket(&packet);

			if (m_jitter.hasReadyFrame())
				return true;
		}

		return false;
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	bool VideoReaderRTP::getNextPacket(rtp_packet* rtpPacket)
	{
		if (m_work->packet_count == 0)
		{
			if (!fillRTPBuffers())
				return false;
		}


		for (;;)
		{
			if (m_workPtr < m_work->packet_count)
			{
				RTPFilePacketHeader& packet = m_work->packets[m_workPtr];

				if (packet.type == RTYPE_FORMAT)
				{
					updateDecoders(packet.format, packet.length / sizeof(RTPFileMediaFormat));
				}
				else if (packet.type == RTYPE_RTP)
				{
					rtpPacket->read_packet(packet.buffer, packet.length);
					return true;
				}
				m_workPtr++;
			}
			else
			{
				updateRTPBuffers();

				if (!fillRTPBuffers())
					return false;
			}
		}

		return nullptr;
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	bool VideoReaderRTP::decodeFrame(VideoImage* image, VideoJitterBuffer* frame)
	{
		bool result = false;

		/*DecoderInfo* dec = findDecoder(frame->getAt(0)->get_payload_type());

		for (int i = 0; i < frame->getCount(); i++)
		{
			rtp_packet* out_packet = frame->getAt(i);

			if (result = dec->decoder->decode(out_packet, image))
			{
				break;
			}
		}

		m_jitter.releaseFrame();*/

		return result;
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	bool VideoReaderRTP::fillRTPBuffers()
	{
		const RTPFilePacketHeader* packet;

		while ((packet = m_file.readNext()) != nullptr)
		{
			if (packet->type == RTYPE_FORMAT)
			{
				updateDecoders(packet->format, packet->length / sizeof(RTPFileMediaFormat));
				continue;
			}

			m_statPackets++;

			rtp_header_t* rtp_hdr = (rtp_header_t*)packet->buffer;
			uint16_t seqNo = ntohs(rtp_hdr->seq);

			if (m_work->packet_count == 0)
			{
				memcpy(m_work->packets, packet, packet->length + RTP_FILE_PACKET_HEADER_LENGTH);

				m_workPtr = 0;
				m_work->seqBegin = seqNo;
				m_work->seqEnd = m_work->seqBegin + RTPSorter::PAKET_PER_SECOND - 1;
				m_work->packet_count = 1;
				m_wait->seqBegin = seqNo + RTPSorter::PAKET_PER_SECOND;
				m_wait->seqEnd = seqNo + (RTPSorter::PAKET_PER_SECOND * 2) - 1;
			}
			else if (seqNo < m_work->seqBegin)
			{
				// LOG late old packets. but now let's drop it
				continue;
			}
			else if (seqNo <= m_work->seqEnd) // working buffer;
			{
				int index = seqNo - m_work->seqBegin;
				// duplicate packets overwrite old ones
				m_work->packet_count++;
				memcpy(m_work->packets + index, packet, packet->length + RTP_FILE_PACKET_HEADER_LENGTH);
			}
			else if (seqNo <= m_wait->seqEnd) // waiting buffer
			{
				int index = seqNo - m_wait->seqBegin;
				// duplicate packets overwrite old ones
				m_wait->packet_count++;
				memcpy(m_wait->packets + index, packet, packet->length + RTP_FILE_PACKET_HEADER_LENGTH);
			}
			else // time to flush working buffer to pcm cache because we get the packet from the third second
			{
				//store for next update
				memcpy(m_thirdBuffer, packet, packet->length + RTP_FILE_PACKET_HEADER_LENGTH);
				return true;
			}
		}

		return m_file.eof();
	}
	//-----------------------------------------------
	// after processing work buffer!
	//-----------------------------------------------
	void VideoReaderRTP::updateRTPBuffers()
	{
		RTPSorter* t = m_work;
		m_work = m_wait;
		m_wait = t;

		memset(m_wait->packets, 0, sizeof(m_wait->packets));
		m_wait->seqBegin = m_work->seqEnd + 1;
		m_wait->seqEnd = m_wait->seqBegin + RTPSorter::PAKET_PER_SECOND - 1;
		m_wait->packet_count = 0;

		if (m_thirdBuffer->type != RTYPE_EMPTY)
		{
			rtp_header_t* rtp_hdr = (rtp_header_t*)m_thirdBuffer->buffer;
			uint16_t seqNo = ntohs(rtp_hdr->seq);
			if (seqNo <= m_wait->seqEnd)
			{
				int index = seqNo - m_wait->seqBegin;
				memcpy(m_wait->packets + index, m_thirdBuffer, m_thirdBuffer->length + RTP_FILE_PACKET_HEADER_LENGTH);
				m_thirdBuffer->type = RTYPE_EMPTY;
				m_wait->packet_count = 1;
			}
		}

		m_workPtr = 0;
	}
	//-----------------------------------------------
	// rtp_packet sorter functions
	//-----------------------------------------------

	////-----------------------------------------------
	///// rtp_packet sorter functions
	////-----------------------------------------------
	////
	////-----------------------------------------------
	//void VideoReaderRTP::processPacket(const RTPFilePacketHeader& packet)
	//{
	//	switch (packet.type)
	//	{
	//	case RTYPE_FORMAT:
	//		updateDecoders(packet.format, packet.length / sizeof(RTPFileMediaFormat));
	//		break;
	//	case RTYPE_RTP:
	//		decodeRTP((rtp_header_t*)packet.buffer, packet.length);
	//		break;
	//	case RTYPE_EMPTY:
	//		decodeEmpty();
	//		break;
	//	default: // invalid!
	//		break;
	//	}
	//	// RTP

	//}
	////-----------------------------------------------
	////
	////-----------------------------------------------
	//void VideoReaderRTP::decodeRTP(rtp_header_t* pack, int length)
	//{
	//	DecoderInfo* info = findDecoder(pack->pt);

	//	if (info == nullptr)
	//		return;

	//	rtp_packet packet;

	//	packet.read_packet(pack, length);

	//	if (pack->pt == 11) // do not decode packet because it is already in PCM format
	//	{
	//		m_decSize = packet.get_payload_length() / sizeof(short);
	//		memcpy(m_decBuffer, packet.get_payload(), packet.get_payload_length());
	//	}
	//	else // decoder must be initialized except 11 payload (mono PCM)
	//	{
	//		m_decSize = info->decoder->decode(packet.get_payload(), packet.get_payload_length(),
	//			(uint8_t*)m_decBuffer, MAX_PCM_BUFFER * sizeof(short)) / sizeof(short);
	//	}

	//	checkCacheSize(m_decSize);

	//	if (info->resampler != nullptr)
	//	{
	//		int len = info->resampler->resample(m_decBuffer, m_decSize, nullptr, m_pcmCache + m_pcmLength, m_pcmSize - m_pcmLength);
	//		m_pcmLength += len;

	//		m_statSamples += len;
	//	}
	//	else
	//	{
	//		memcpy(m_pcmCache + m_pcmLength, m_decBuffer, m_decSize * sizeof(short));
	//		m_pcmLength += m_decSize;

	//		m_statSamples += m_decSize;
	//	}
	//}
	////-----------------------------------------------
	////
	////-----------------------------------------------
	//void VideoReaderRTP::decodeEmpty()
	//{
	//	// 20 ms for 8Kz mono 16-bit
	//	checkCacheSize(COMFORT_NOISE_20);
	//	memset(m_pcmCache + m_pcmLength, 0, COMFORT_NOISE_20 * sizeof(short));
	//	m_pcmLength += COMFORT_NOISE_20;
	//	m_statSamples += COMFORT_NOISE_20;
	//}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoReaderRTP::updateDecoders(const RTPFileMediaFormat* formats, int count)
	{
		rtl::MutexLock lock(m_sync);

		for (int i = 0; i < count; i++)
		{
			const RTPFileMediaFormat* fmt = formats + i;
			DecoderInfo* found = findDecoder(fmt->payload_id);
			if (found != nullptr && std_stricmp(found->format.encoding, fmt->encoding) == 0)
				continue;
			addDecoder(fmt);
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VideoReaderRTP::DecoderInfo* VideoReaderRTP::findDecoder(int payloadId)
	{
		rtl::MutexLock lock(m_sync);

		for (int i = 0; i < m_decoderList.getCount(); i++)
		{
			if (m_decoderList[i].format.payload_id == payloadId)
				return &m_decoderList[i];
		}

		return nullptr;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoReaderRTP::addDecoder(const RTPFileMediaFormat* fmt)
	{
		LOG_FLAG1("rtp-src", "changing format:\n\
\t\tencoding:	\'%s\'\n\
\t\tparams:		\'%s\'\n\
\t\tfmtp:		\'%s\'\n\
\t\tpayload id: %d\n\
\t\tclockrate:	%d\n\
\t\tchannels:	%d",
			fmt->encoding, fmt->params, fmt->fmtp, fmt->payload_id, fmt->clock_rate, fmt->channels);

		PayloadFormat format(fmt->encoding, (PayloadId)fmt->payload_id, fmt->clock_rate, fmt->channels, fmt->fmtp);

		if (format.getId() == PayloadId::G722)
		{
			format.setFrequency(16000);
		}

		DecoderInfo info{ *fmt, nullptr };

		info.decoder = rtx_codec__create_video_decoder(fmt->encoding, &format);

		if (info.decoder == nullptr)
		{
			// LOG
			return;
		}

		m_decoderList.add(info);
	}
}
//-----------------------------------------------
