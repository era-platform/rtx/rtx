/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_video_jitter.h"

#define LOG_PREFIX "v-jit"

namespace media
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VideoJitter::VideoJitter(rtl::Logger* log) : m_log(log), m_current(nullptr), m_next(nullptr)
	{
		m_current = &m_frame1;
		m_next = &m_frame2;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VideoJitter::~VideoJitter()
	{
		m_frame1.clear();
		m_frame2.clear();
		m_current = nullptr;
		m_next = nullptr;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoJitter::pushPacket(const rtp_packet* packet)
	{
		if (!pushToCurrent(packet))
		{
			pushToNext(packet);
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool VideoJitter::pushToCurrent(const rtp_packet* packet)
	{
		uint32_t ts = packet->getTimestamp();
		int res = ts - m_current->getTimestamp();

		if (m_current->getState() == VideoJitterState::Empty || res == 0)
		{
			m_current->push(packet);
			return true;
		}

		return false;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoJitter::pushToNext(const rtp_packet* packet)
	{

		uint32_t ts = packet->getTimestamp();
		int res = ts - m_next->getTimestamp();

		if (m_next->getState() == VideoJitterState::Empty || res == 0)
		{
			m_next->push(packet);
		}
		else if (res > 0)
		{
			dropCurrent();
			pushPacket(packet);
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoJitter::dropCurrent()
	{
		m_current->clear();
		VideoJitterFrame* t = m_current;
		m_current = m_next;
		m_next = t;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool VideoJitter::hasReadyFrame()
	{
		return m_current->getState() == VideoJitterState::Ready;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool VideoJitter::getReadyFrame(VideoJitterBuffer& frame)
	{
		if (m_current->getState() == VideoJitterState::Ready)
		{
			// copy
			frame.assign(*m_current->getFrame());
			return true;
		}

		return false;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoJitter::releaseFrame()
	{
		dropCurrent();
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool VideoJitterFrame::push(const rtp_packet* packet)
	{
		switch (m_state)
		{
		case VideoJitterState::Empty:
			m_ts = packet->getTimestamp();
			m_seq = packet->get_sequence_number();
			m_rts = rtl::DateTime::getTicks();
			m_state = VideoJitterState::Wait;
			return addPacket(packet);

		case VideoJitterState::Wait:
			return addPacket(packet);

		default:
			return false;
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoJitterFrame::clear()
	{
		for (int i = 0; i < m_buffer.getCount(); i++)
		{
			rtp_packet* jp = m_buffer[i];
			DELETEO(jp);
		}

		m_buffer.clear();

		m_state = VideoJitterState::Empty;
		m_ts = 0;
		m_seq = 0;
		m_rts = 0;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool VideoJitterFrame::addPacket(const rtp_packet* packet)
	{
		int idx;
		uint16_t seq = packet->get_sequence_number();

		if (packet->getTimestamp() != m_ts)
		{
			return false;
		}

		for (idx = 0; idx < m_buffer.getCount(); idx++)
		{
			rtp_packet* jp = m_buffer[idx];

			short diff = seq - jp->get_sequence_number();

			if (diff < 0)
			{
				break;
			}

			if (diff == 0)
			{
				return false;
			}
		}

		rtp_packet* own_packet = NEW rtp_packet();
		own_packet->copy_from(packet);
		m_buffer.insert(idx, own_packet);

		// ������ ���������� ������ �� ������ (� ��������)

		if (packet->get_marker())
		{
			if (idx != m_buffer.getCount() - 1)
				return false;
		}

		bool all_filled = ((m_buffer.getLast()->get_sequence_number() - m_seq) + 1) == m_buffer.getCount();

		if (m_buffer.getLast()->get_marker() && all_filled)
		{
			m_state = VideoJitterState::Ready;
		}

		return true;
	}
}
