/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_video_frame.h"
#include "mmt_video_image.h"
#include "mmt_video_text.h"

#define NON_VIDEO_STREAM 0
#define JSON_FIELDS "fields"
#define JSON_FRAME "frame"
#define JSON_VAD "vad"
#define JSON_BACKCOLOR "backgroundColor"
#define JSON_FORECOLOR "foregroundColor"
#define JSON_FACENAME "faceName"
#define JSON_FONTHEIGHT "fontHeight"
//
#define JSON_WIDTH "width"
#define JSON_HEIGHT "height"

namespace media
{
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	VideoFrameGenerator::VideoFrameGenerator() :
		m_drawer(m_canvas),
		m_size({ 0, 0 }),
		m_backColor({ 0 }),
		m_foreColor({ 0 }),
		m_useVadDetector(false),
		m_fontHeight(16),
		m_defaultFont(nullptr)
	{
		m_imageStorage[NoVideo] = nullptr;
		m_imageStorage[NoSignal] = nullptr;

		m_backColors[NoVideo] = { 128, 128, 128 };
		m_backColors[NoSignal] = { 192, 192, 192 };
#if TRACE_VIDEO
		openTrace();
#endif
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	VideoFrameGenerator::~VideoFrameGenerator()
	{
		destroy();

		if (m_imageStorage[NoVideo] != nullptr)
			DELETEO(m_imageStorage[NoVideo]);
		if (m_imageStorage[NoSignal] != nullptr)
			DELETEO(m_imageStorage[NoSignal]);

#if TRACE_VIDEO
		closeTrace();
#endif

	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	bool VideoFrameGenerator::initialize(const char* text)
	{
		rtl::MutexLock lock(m_sync);

#if TRACE_VIDEO
		openTrace();
		trace("initial JSONL:\r\n%s\r\n", text);
#endif

		JsonParser parser;

		int len = text != nullptr ? (int)strlen(text) : 0;

		if (!len || !parser.start(text))
			return false;

		destroy();

		JsonState state;

		while ((state = parser.read()) != JsonState::Eof)
		{
			switch (state)
			{
			case JsonState::Object:
				//...
				break;
			case JsonState::Member:
				readParams(parser);
				break;
			case JsonState::ObjectEnd:
				//...
				break;
			case JsonState::Error:
				return false;
			}
		}
#if defined _DEBUG
		VideoRawElement rawElement;
		rawElement.type = VideoElementType::VideoStream;
		rtl::strupdate(rawElement.id, "VADStream1");
		rawElement.left = 300, rawElement.top = 100, rawElement.width = 300, rawElement.height = 200;
		rawElement.backColor = { 0,0,0,0 };
		rawElement.foreColor = { 0,255,255,0 };
		rawElement.stringLength = 0;
		rawElement.stringValue = nullptr;
		rawElement.videoMode = VideoStreamMode::Vad;
		rawElement.zOrder = 3;

		addElement(rawElement);


		rawElement.clean();

		rawElement.type = VideoElementType::Text;
		rtl::strupdate(rawElement.id, "text1");
		rawElement.left = 300, rawElement.top = 100, rawElement.width = 3000, rawElement.height = 30;
		rawElement.backColor = { 0,0,0,0 };
		rawElement.foreColor = { 0,255,255,0 };
		rawElement.stringLength = rtl::strupdatel(rawElement.stringValue, "Video Conference #123");
		rawElement.textMode = TextBackMode::Contoured;
		rawElement.zOrder = 1;

		addElement(rawElement);

		rawElement.clean();

		rawElement.type = VideoElementType::Time;
		rtl::strupdate(rawElement.id, "time1");
		rawElement.left = m_size.width-120, rawElement.top = m_size.height - 40, rawElement.width = 110, rawElement.height = 30;
		rawElement.backColor = { 0,0,0,0 };
		rawElement.foreColor = { 0,255,255,0 };
		rawElement.stringLength = rtl::strupdatel(rawElement.stringValue, "%H:%M:%S");
		rawElement.textMode = TextBackMode::Opaque;
		rawElement.timezone = 0;
		rawElement.zOrder = 4;

		addElement(rawElement);
#endif
#if TRACE_VIDEO
		trace("generator initialized: el(%d/%d)\r\n", m_elements.getCount(), m_zOrder.getCount());
#endif
		resetBitmap();

		m_defaultFont = FontManager::createFont(m_faceName, m_fontHeight); // , (uint32_t)TextBackMode::Opaque

		return true;
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	void VideoFrameGenerator::destroy()
	{
		rtl::MutexLock lock(m_sync);
#if TRACE_VIDEO
		trace("destroy generator: el(%d)\r\n", m_elements.getCount());
#endif
		for (int i = 0; i < m_elements.getCount(); i++)
		{
			VideoFrameElement* el = m_elements[i];
			DELETEO(el);
		}

		m_elements.clear();
		m_bindedStreams.clear();
		m_vadStreams.clear();
		m_freeStreams.clear();
		m_unassignedTerms.clear();
		m_zOrder.clear();

		m_canvas.clear();

		FontManager::releaseFont(m_defaultFont);
		m_defaultFont = nullptr;

#if TRACE_VIDEO
		trace("generator destroyed: el(%d/%d)\r\n", m_elements.getCount(), m_zOrder.getCount());
#endif
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	void VideoFrameGenerator::updateVideoStream(uint64_t bindId, const VideoImage* image)
	{
		rtl::MutexLock lock(m_sync);

#if TRACE_VIDEO
		trace("search for %016llx (c:%d)->", bindId, m_bindedStreams.getCount());
#endif
		VideoFrameStream* stream = findVideoStream(bindId);

		if (stream != nullptr)
		{
			stream->updateImage(image);
#if TRACE_VIDEO
			trace("%s/%016llx\r\n", stream->getId(), stream->getVideoTermId());
#endif
		}
#if TRACE_VIDEO
		else
		{
			trace("Invalid video term ID %016llx\r\n", bindId);
		}
#endif

		const VAD& vad = m_currentVad;

#if TRACE_VIDEO
		trace("VAD stream %016llx vs %016llx\r\n");
#endif
		if (vad.bindId == bindId || (vad.bindId & 0xFFFFFFFF) == (bindId & 0xFFFFFFFF))
		{
			for (int i = 0; i < m_vadStreams.getCount(); i++)
			{
				m_vadStreams[i]->updateImage(image);
			}
#if TRACE_VIDEO
			trace("VAD stream updated\r\n");
#endif
		}
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	void VideoFrameGenerator::updateVideoStreamVAD(uint64_t bindId, bool vadDetected)
	{
		rtl::MutexLock lock(m_sync);

		VideoFrameStream* stream = findVideoStream(bindId);
#if TRACE_VIDEO
		trace("update VAD state(%s) for ID %016llx\r\n", STR_BOOL(vadDetected), bindId);
#endif
		if (stream != nullptr)
		{
			stream->updateVAD(vadDetected);
		}

		if (vadDetected)
		{
			if (m_currentVad.bindId == 0)
			{
				m_currentVad.bindId = bindId;
				m_currentVad.vadPower = 100;
			}
			else if (m_currentVad.bindId == bindId)
			{
				m_currentVad.vadPower = 100;
			}
			else if (m_currentVad.vadPower < 20)
			{
				m_currentVad.bindId = bindId;
				m_currentVad.vadPower = 100;
			}
		}
#if TRACE_VIDEO
		trace("update VAD result -> bindId %016llx vadPower %d\r\n", m_currentVad.bindId, m_currentVad.vadPower);
#endif
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	uint64_t VideoFrameGenerator::bindVideoStream(uint32_t termId, uint32_t videoId)
	{
		rtl::MutexLock lock(m_sync);

		uint64_t bindId = 0;

		if (videoId == 0)
		{
			bindId =  bindToFree(termId);
		}
		else
		{
			// direct binding 
			VideoFrameStream* found = findVideoStream(videoId);
			if (found != nullptr)
			{
				bindId = found->bindToTermimation(termId);
			}
		}
#if TRACE_VIDEO
		trace("bindVideoStream to term ID %08x | %08x -> %016llx\r\n", termId, videoId, bindId);
#endif
		return bindId;
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	uint64_t VideoFrameGenerator::bindToFree(uint64_t termId)
	{
		uint64_t bindId = 0;
		VideoFrameStream* stream = m_freeStreams.pop();

		if (stream != nullptr)
		{
			// always success
			bindId = stream->bindToTermimation(termId);
			m_bindedStreams.add(stream);
#if TRACE_VIDEO
			trace("bindToFree %016llxx -> %016llx\r\n", termId, termId, bindId);
#endif
		}

		return bindId;
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	void VideoFrameGenerator::unbindVideoStream(uint64_t bindId)
	{
		rtl::MutexLock lock(m_sync);

		int index;
		VideoFrameStream* stream = findVideoStream(bindId, &index);
		bool result = stream != nullptr;

		if (result)
		{
			switch (stream->getMode())
			{
			case VideoStreamMode::Term:
				stream->stopStream();
				break;
			case VideoStreamMode::Auto:
				stream->stopStream();
				m_bindedStreams.removeAt(index);
				m_freeStreams.push(stream);
				break;
			}
		}
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	const Bitmap* VideoFrameGenerator::getPicture()
	{
		rtl::MutexLock lock(m_sync);

		//m_canvas.clear();
#if TRACE_VIDEO
		trace("----> getPicture(%d)\r\n", m_elements.getCount());
#endif
		m_drawer.setPenColor(m_backColor);

#if TRACE_VIDEO
		trace("elements:\r\n");
#endif
		for (int i = 0; i < m_zOrder.getCount(); i++)
		{
			VideoFrameElement* e = m_zOrder[i];
			//if (e->isUpdated())
			e->isUpdated();
			e->drawOn(m_drawer);
		}

		m_currentVad.vadPower--;
#if TRACE_VIDEO
		trace("<---- getPicture()\r\n");
#endif
		return &m_canvas;
	}
	//-----------------------------------------------
	// ���������� ��������
	//-----------------------------------------------
	void VideoFrameGenerator::addZOrder(VideoFrameElement* element)
	{
		int zOrder = element->getZOrder();
		int count = m_zOrder.getCount();
		int index = count;

		for (int i = 0; i < count; i++)
		{
			if (m_zOrder[i]->getZOrder() > zOrder)
			{
				index = i;
				break;
			}
		}

		m_zOrder.insert(index, element);
	}
	//-----------------------------------------------
	// ������������ ������� � �����
	//-----------------------------------------------
	void VideoFrameGenerator::updateAreaFor(VideoFrameElement* element)
	{
		const Rectangle& rect = element->getBounds();
		
		int count = m_zOrder.getCount();
		int zOrder = element->getZOrder();

		for (int i = 0; i < count; i++)
		{
			VideoFrameElement* el = m_zOrder[i];
			
			if (el->getZOrder() >= zOrder)
			{
				Rectangle result;
				if (el->getBounds().intersect(rect, result))
				{
					el->drawOn(m_drawer);
				}
			}
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoFrameGenerator::updateArea(const Rectangle& rect)
	{
		int count = m_zOrder.getCount();

		for (int i = 0; i < count; i++)
		{
			VideoFrameElement* el = m_zOrder[i];
			Rectangle result;
			if (el->getBounds().intersect(rect, result))
			{
				el->drawOn(m_drawer);
			}
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoFrameGenerator::drawNoSignalImage(const Rectangle& rect)
	{
		static int cnt = 0;
		const Bitmap* noVideo = m_imageStorage[NoSignal];

		if (noVideo == nullptr)
		{
			m_drawer.setPenColor(m_backColors[NoSignal]);
			m_drawer.fillRectangle(rect);
			rtl::String text = "No Signal ";
			text << ++cnt;
			m_drawer.drawText(rect, text, TextFormatMiddleH | TextFormatMiddleV);
		}
		else
		{
			Bitmap toDraw;
			noVideo->transformTo(toDraw, rect.getWidth(), rect.getHeight(), Transform::Stretch);
			m_drawer.drawImage(rect.left, rect.top, toDraw);
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoFrameGenerator::drawNoVideoImage(const Rectangle& rect)
	{
		const Bitmap* noVideo = m_imageStorage[NoVideo];

		if (noVideo == nullptr)
		{
			m_drawer.setPenColor(m_backColors[NoVideo]);
			m_drawer.fillRectangle(rect);
			m_drawer.drawText(rect, "No Video", TextFormatMiddleH | TextFormatMiddleV);
		}
		else
		{
			Bitmap toDraw;
			noVideo->transformTo(toDraw, rect.getWidth(), rect.getHeight(), Transform::Stretch);
			m_drawer.drawImage(rect.left, rect.top, toDraw);
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
#if TRACE_VIDEO
	void VideoFrameGenerator::trace(const char* fmt, ...)
	{
		rtl::MutexLock lock(m_sync);

		va_list args;
		va_start(args, fmt);

		m_trace.writeFormatArgs(fmt, args);
	}
#endif
	//-----------------------------------------------
	//
	//-----------------------------------------------
#if TRACE_VIDEO
	void VideoFrameGenerator::trace(VideoFrameElement* element)
	{
		rtl::MutexLock lock(m_sync);

		const Rectangle& r = element->getBounds();
		m_trace.writeFormat("id:%s z:%d type:%s bounds:%d,%d,%d,%d -- ",
			element->getId(), element->getZOrder(), VideoElementType_toString(element->getType()), r.left, r.top, r.right, r.bottom);
	}
#endif
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	bool VideoFrameGenerator::readParams(JsonParser& parser)
	{
		const rtl::String& fieldName = parser.getFieldName();

		if (::strcmp(fieldName, JSON_VAD) == 0)
		{
			m_useVadDetector = parser.getBoolean();
		}
		else if (::strcmp(fieldName, JSON_FIELDS) == 0)
		{
			if (!readElements(parser))
				return false;
		}
		else if (::strcmp(fieldName, JSON_FRAME) == 0)
		{
			if (!readFrameParams(parser))
				return false;
		}

		return true;
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	bool VideoFrameGenerator::readFrameParams(JsonParser& parser)
	{
		JsonState state;

		while ((state = parser.read()) != JsonState::Eof)
		{
			switch (state)
			{
			case JsonState::Member:
				readFrameElement(parser);
				break;
			case JsonState::ObjectEnd:
				return true;
			case JsonState::Error:
				return false;
			}
		}

		return true;
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	bool VideoFrameGenerator::readFrameElement(JsonParser& parser)
	{
		const rtl::String& fieldName = parser.getFieldName();

		if (fieldName.getLength() == 5 && ::strcmp(fieldName, JSON_WIDTH) == 0)
		{
			m_size.width = (int)parser.getInteger();
		}
		else if (::strcmp(fieldName, JSON_HEIGHT) == 0)
		{
			m_size.height = (int)parser.getInteger();
		}
		else if(::strcmp(fieldName, JSON_BACKCOLOR) == 0)
		{
			m_backColor = hexToColor(parser.getString());
		}
		else if (::strcmp(fieldName, JSON_FORECOLOR) == 0)
		{
			m_foreColor = hexToColor(parser.getString());
		}
		else if (::strcmp(fieldName, JSON_FACENAME) == 0)
		{
			m_faceName = parser.getString();
		}
		else if (::strcmp(fieldName, JSON_FONTHEIGHT) == 0)
		{
			m_fontHeight = (int)parser.getInteger();
		}
		
		return true;
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	bool VideoFrameGenerator::readElements(JsonParser& parser)
	{
		JsonState state;

		while ((state = parser.read()) != JsonState::Eof)
		{
			switch (state)
			{
			case JsonState::Array:
				//...
				break;
			case JsonState::ArrayValue:
				if (!readElement(parser))
					return false;
				break;
			case JsonState::ArrayEnd:
				//...
				return true;
			case JsonState::Error:
				return false;
			}
		}

		return true;
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------	
	bool VideoFrameGenerator::readElement(JsonParser& parser)
	{
		JsonState state;

		VideoRawElement element;

		while ((state = parser.read()) != JsonState::Eof)
		{
			switch (state)
			{
			case JsonState::Object:
				//...
				break;
			case JsonState::Member:
				element.read(parser);
				break;
			case JsonState::ObjectEnd:
				addElement(element);
				return true;
			case JsonState::Error:
				return false;
			}
		}

		return true;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool VideoFrameGenerator::readTemplates(JsonParser& parser)
	{
		JsonState state;

		while ((state = parser.read()) != JsonState::Eof)
		{
			switch (state)
			{
			case JsonState::Array:
				//...
				break;
			case JsonState::ArrayValue:
				if (!readTemplate(parser))
					return false;
				break;
			case JsonState::ArrayEnd:
				//...
				return true;
			case JsonState::Error:
				return false;
			}
		}

		return true;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool VideoFrameGenerator::readTemplate(JsonParser& parser)
	{
		JsonState state;

		rtl::StringList propList;

		while ((state = parser.read()) != JsonState::Eof)
		{
			switch (state)
			{
			case JsonState::Object:
				//...
				break;
			case JsonState::Member:
				propList.add(parser.getFieldName());
				propList.add(parser.getValueAsString());
				break;
			case JsonState::ObjectEnd:
				addTemplate(propList);
				return true;
			case JsonState::Error:
				return false;
			}
		}

		return true;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool VideoFrameGenerator::addElement(const VideoRawElement& element)
	{
		VideoFrameElement* vfe = nullptr;
		VideoFrameStream* vfs = nullptr;
		switch (element.type)
		{
		case VideoElementType::Text:
			vfe = NEW VideoFrameText(*this, element.id);
			break;
		case VideoElementType::Time:
			vfe = NEW VideoFrameTimer(*this, element.id);
			break;
		case VideoElementType::Bitmap:
			vfe = NEW VideoFrameImage(*this, element.id);
			break;
		case VideoElementType::VideoStream:
			vfe = vfs = NEW VideoFrameStream(*this, element.id);
			if (element.videoMode == VideoStreamMode::Term)
				m_bindedStreams.add(vfs);
			else if (element.videoMode == VideoStreamMode::Auto)
				m_freeStreams.push(vfs);
			else if (element.videoMode == VideoStreamMode::Vad)
				m_vadStreams.add(vfs);
			break;
		}

		vfe->assign(element);

		if (vfe != nullptr)
		{
			m_elements.add(vfe);
			addZOrder(vfe);
			

			return true;
		}

		return false;
	}
	//--------------------------------------------------
	//
	//--------------------------------------------------
	bool VideoFrameGenerator::addTemplate(const rtl::StringList& propList)
	{
		// �������: "name": "NoSignal", "backcolor": "FFC080", "bitmap": "base64"
		rtl::String name, backcolor, bitmap;
		for (int i = 0; i < propList.getCount(); i+=2)
		{
			if (propList[i] == "name")
			{
				name = propList[i + 1];
			}
			else if (propList[i] == "backcolor")
			{
				backcolor = propList[i + 1];
			}
			else if (propList[i] == "bitmap")
			{
				bitmap = propList[i + 1];
			}
			int index = -1;
			Color defColor;
			if (rtl::String::compare(name, "NoSignal", true) == 0)
			{
				index = NoSignal;
				defColor = { 192,192,192 };
			}
			else if (rtl::String::compare(name, "NoVideo", true) == 0)
			{
				index = NoVideo;
				defColor = { 128,128,128 };
			}

			if (!bitmap.isEmpty())
			{
				if (m_imageStorage[index] == nullptr)
				{
					m_imageStorage[index] = NEW Bitmap;
				}

				m_imageStorage[index]->loadBitmapBase64(bitmap, bitmap.getLength());
			}
			else 
			{
				if (m_imageStorage[index] != nullptr)
				{
					DELETEO(m_imageStorage[index]);
					m_imageStorage[index] = nullptr;
				}

				m_backColors[index] = !backcolor.isEmpty() ? hexToColor(backcolor) : defColor;
			}
		}
		return true;
	}
	//--------------------------------------------------
	//
	//--------------------------------------------------
	void VideoFrameGenerator::resetBitmap()
	{
		rtl::MutexLock lock(m_sync);

#if TRACE_VIDEO
		trace("----> resetBitmap(%d)\r\n", m_elements.getCount());
#endif
		m_canvas.createBitmap(m_size.width, m_size.height);
		m_canvas.fill(m_backColor);
		m_drawer.setClipRect({ 0, 0, m_size.width, m_size.height });
		Rectangle r;
		m_drawer.getClipRect(r);
		m_drawer.setPenColor(m_foreColor);
		int count = m_zOrder.getCount();
#if TRACE_VIDEO		
		trace("elements:\r\n");
#endif		
		for (int i = 0; i < count; i++)
		{
			VideoFrameElement* el = m_zOrder[i];
			el->isUpdated();
			el->drawOn(m_drawer);
		}
#if TRACE_VIDEO		
		trace("<---- resetBitmap()\r\n");
#endif
	}
	//--------------------------------------------------
	//
	//--------------------------------------------------
	void VideoFrameGenerator::recalculateVad()
	{
		//...
	}
	//--------------------------------------------------
	//
	//--------------------------------------------------
#if TRACE_VIDEO
	void VideoFrameGenerator::openTrace()
	{
		rtl::MutexLock lock(m_sync);

		closeTrace();

		if (m_trace.create_or_append("c:\\test\\trace5.txt"))
		{
			m_trace.writeFormat("-------- start tracing --------\r\n");
		}
	}
	//--------------------------------------------------
	//
	//--------------------------------------------------
	void VideoFrameGenerator::closeTrace()
	{
		rtl::MutexLock lock(m_sync);

		if (m_trace.isOpened())
		{
			m_trace.writeFormat("-------- end tracing --------\r\n");
			m_trace.close();
		}
	}
#endif
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VideoFrameStream* VideoFrameGenerator::findVideoStream(uint64_t bindId)
	{
		for (int i = 0; i < m_bindedStreams.getCount(); i++)
		{
			VideoFrameStream* stream = m_bindedStreams[i];
#if TRACE_VIDEO		
			trace("find[%d] -> %llx\r\n", i, stream->getVideoTermId());
#endif
			if (stream->getVideoTermId() == bindId)
			{
				return stream;
			}

			if ((stream->getVideoTermId() & 0xFFFFFFFF) == (bindId & 0xFFFFFFFF))
			{
				return stream;
			}
		}

		return nullptr;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VideoFrameStream* VideoFrameGenerator::findVideoStream(uint64_t bindId, int* index)
	{
		for (int i = 0; i < m_bindedStreams.getCount(); i++)
		{
			VideoFrameStream* stream = m_bindedStreams[i];

			if (stream->getVideoTermId() == bindId)
			{
				*index = i;
				return stream;
			}
		}

		return nullptr;
	}
}
//-----------------------------------------------
