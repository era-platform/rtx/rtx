﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_video_image.h"

namespace media
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VideoFrameImage::~VideoFrameImage()
	{
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoFrameImage::assign(const VideoRawElement& data)
	{
		VideoFrameElement::assign(data);

		Bitmap original;

		if (!original.loadBitmapBase64(data.stringValue, data.stringLength))
			return;
		
		if (original.getWidth() != m_bounds.getWidth() || original.getHeight() != m_bounds.getHeight())
		{
			Bitmap::transformImage(original, m_picture, m_bounds.getWidth(), m_bounds.getHeight());
		}
		else // copy
		{
			m_picture = original;
		}

		m_updated = true;

		//LOG_CALL("VFS", "VideoFrameElement::videoStreamId : %d", m_videoTermId);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoFrameImage::drawOn(ImageDrawer& draw)
	{
#if TRACE_VIDEO		
		m_frame.trace(this);
		m_frame.trace("--\r\n");
#endif
		//m_frame.trace("drawImage(%d, %d)\r\n", m_bounds.left, m_bounds.top);
		draw.drawImage(m_bounds.left, m_bounds.top, m_picture);
		draw.setPenColor({ 0,0,255 });
		//m_frame.trace("drawRectangle(m_bounds)\r\n");
		draw.drawRectangle(m_bounds);

		m_updated = false;
	}
}
//-----------------------------------------------
