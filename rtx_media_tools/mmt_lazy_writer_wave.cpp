﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "mmt_lazy_writer_wave.h"

namespace media
{

	static media::WAVFormat Format_PCM = { media::WAVFormatTagPCM, 1, 8000, 16000, 2, 16, 0 };
	static media::WAVFormatGSM610 Format_GSM610 = { media::WAVFormatTagGSM610, 1, 8000, 1625, 65, 0, 2, 320 };
	static media::WAVFormat Format_G711A = { media::WAVFormatTagALAW, 1, 8000, 8000, 1, 8, 0 };
	static media::WAVFormat Format_G711U = { media::WAVFormatTagULAW, 1, 8000, 8000, 1, 8, 0 };
	//--------------------------------------
	//
	//--------------------------------------
	LazyMediaWriterWAVE::LazyMediaWriterWAVE(rtl::Logger* log) : LazyMediaWriter(log), m_file(log), m_pcm_format(false)
	{
	}
	//--------------------------------------
	//
	//--------------------------------------
	LazyMediaWriterWAVE::~LazyMediaWriterWAVE()
	{
		destroy();
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool LazyMediaWriterWAVE::create(const char* path, const media::PayloadFormat& format)
	{
		PLOG_CALL("wav-rec", "Create file...");

		if (format.isEmpty())
		{
			PLOG_WARNING("wav-rec", "wave recorder : format is null.");
			return false;
		}

		// создадим файл
		media::WAVFormat* waveFormat;

		switch (format.getId())
		{
		case media::PayloadId::PCMU:
			waveFormat = &Format_G711U;
			break;
		case media::PayloadId::PCMA:
			waveFormat = &Format_G711A;
			break;
		case media::PayloadId::GSM610:
			waveFormat = &Format_GSM610.format;
			break;
		default:
			waveFormat = &Format_PCM;
			m_pcm_format = true;
			break;
		}

		if (!m_file.create(path, waveFormat))
		{
			PLOG_WARNING("wav-rec", "wave recorder : Create wave file failed.");
			return false;
		}

		PLOG_CALL("wav-rec", "File created and updated");

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void LazyMediaWriterWAVE::close()
	{
		PLOG_CALL("wav-rec", "Closing file...");

		m_file.close();

		PLOG_CALL("wav-rec", "File closed");
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool LazyMediaWriterWAVE::write_samples(short* samples, int samplesCount)
	{
		if (samples == nullptr || samplesCount == 0)
		{
			PLOG_WARNING("wav-rec", "wave recorder : WritePacket -- empty data!");
			return false;
		}

		if (m_started)
		{
			//PLOG_CALL("wav-rec", "Write block (length %d) to cache...", samplesCount);

			int pack_len = 0;
			const uint8_t* pack_data = nullptr;

			if (m_pcm_format)
			{
				pack_data = (const uint8_t*)samples;
				pack_len = samplesCount * 2;
			}
			else
			{
				return false;
			}

			// получим блок кеша для записи пакета
			LazyBlock* block = get_current_block(1);

			if (block != nullptr)
			{
				//PLOG_CALL("wav-rec", "Write packet to cache...");
				// запишем информацию о пакете в заголовок
				uint8_t* rec_buffer = block->rec_pos;
				int rec_size = CACHE_BLOCK_SIZE - block->rec_length;
				// скопируем сам пакет
				int copy_len = rec_size < pack_len ? rec_size : pack_len;

				memcpy(rec_buffer, pack_data, copy_len);

				if (copy_len == pack_len)
				{
					// запись завершена
					// сдвинем указатель на свободный буфер в блоке
					block->rec_pos += copy_len;
					block->rec_length += copy_len;
				}
				else
				{
					// допишем в следующий блок

					// запомним смещение недописанных данных
					int offset = copy_len;
					copy_len = pack_len - copy_len;

					block = get_current_block(copy_len);
					if (block != nullptr)
					{
						//PLOG_CALL("wav-rec", "Write packet to cache...");
						// запишем информацию о пакете в заголовок
						rec_buffer = block->rec_pos;
						rec_size = CACHE_BLOCK_SIZE - block->rec_length;

						// проверим от греха подальше
						if (rec_size < copy_len)
							copy_len = rec_size;

						// скопируем сам пакет
						memcpy(rec_buffer, pack_data + offset, copy_len);

						// запись завершена
						// сдвинем указатель на свободный буфер в блоке
						block->rec_pos += copy_len;
						block->rec_length += copy_len;
					}
				}
			}
			else
			{
				PLOG_WARNING("wav-rec", "Write packet to cache failed : cache is full -- packet dropped");
			}
		}

		// всегда возвращаем истину 
		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void LazyMediaWriterWAVE::rec_file_started()
	{
	}
	//--------------------------------------
	//
	//--------------------------------------
	void LazyMediaWriterWAVE::rec_file_stopped()
	{
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool LazyMediaWriterWAVE::rec_file_write(void* dataBlock, uint32_t length, uint32_t* written)
	{
		PLOG_CALL("wav-rec", "Write block (len:%d) to file...", length);

		if (m_file.write(dataBlock, length) > 0)
		{
			*written = length;
			PLOG_CALL("wav-rec", "Block written (len:%d) to file...", *written);
			return true;
		}

		*written = 0;
		PLOG_WARNING("wav-rec", "Can't write header to file!");

		return false;
	}
}
//--------------------------------------
