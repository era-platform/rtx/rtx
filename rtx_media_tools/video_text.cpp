/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_video_text.h"

namespace media
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VideoFrameTextBase::~VideoFrameTextBase()
	{
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoFrameTextBase::assign(const VideoRawElement& data)
	{
		VideoFrameElement::assign(data);

		m_faceName = data.faceName;
		m_textMode = data.textMode;
		m_formatFlags = data.textFlags;
		m_foreColor = data.foreColor;

		// in text mode version 1 is stored in the font class
		m_font = FontManager::createFont(m_faceName, m_bounds.getHeight()); // , m_textMode
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoFrameTextBase::drawOn(ImageDrawer& draw)
	{
#if TRACE_VIDEO		
		m_frame.trace(this);
		m_frame.trace("mode:%s text:\"%s\"\r\n", TextBackMode_toString(m_textMode), m_text);
#endif
		draw.setTextBackColor(m_backColor);
		draw.setTextForeColor(m_foreColor);
		draw.setTextBackMode(m_textMode);
		draw.drawText(m_bounds, m_text, TextFormatLeft |TextFormatMiddleV);

		//m_frame.trace("drawText(m_bounds, %s)\r\n", m_text);

		m_updated = false;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VideoFrameText::~VideoFrameText()
	{
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoFrameText::assign(const VideoRawElement& data)
	{
		VideoFrameTextBase::assign(data);

		m_text.assign(data.stringValue, data.stringLength);

		m_updated = true;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VideoFrameTimer::~VideoFrameTimer()
	{
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoFrameTimer::assign(const VideoRawElement& data)
	{
		VideoFrameTextBase::assign(data);

		m_timeZone = data.timezone;
		m_format = data.stringValue;
		
		m_lastTick = ::time(nullptr);

		printTime(m_lastTick);

		m_updated = true;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool VideoFrameTimer::isUpdated()
	{
		time_t t = ::time(nullptr);

		if (t == m_lastTick)
			return false;

		m_lastTick = t;
		printTime(m_lastTick);

		return true;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoFrameTimer::printTime(time_t ts)
	{
		char tmp[128];
		struct tm* t = localtime(&ts);
		strftime(tmp, 128, m_format, t);
		m_text = tmp;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoFrameTimer::drawOn(ImageDrawer& draw)
	{
		LOG_CALL("123", "draw on the ...");
		draw.setTextBackColor(m_backColor); 
		draw.setTextForeColor(m_foreColor);
		draw.setTextBackMode(m_textMode);
		draw.drawText(m_bounds, m_text, TextFormatMiddleH | TextFormatMiddleV);

		m_updated = false;
	}
}
//-----------------------------------------------
