/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_video_font_man.h"

namespace media
{
	//-----------------------------------------------
	// Font manager static private variables
	//-----------------------------------------------
	static rtl::Mutex s_fontSync;
	static rtl::ArrayT<Font*> s_fontList;
	static rtl::ArrayT<FontFile*> s_fontFileList;
	static FontInfo* s_defFontInfo;
	static Font* s_defFont;
	//-----------------------------------------------
	// default font 11 pix
	//-----------------------------------------------
	static const uint8_t s_defFontStream[] = {
#include "font/deffont.txt"
	};
	//-----------------------------------------------
	//
	//-----------------------------------------------
	static Font* findFont(const char* facename, int pixHeight);
	static const FontInfo* findFontInfo(const char* facename, int pixHeight);
	static void initDefaultFont();
	//-----------------------------------------------
	// load *.fnt or *.fon files
	//-----------------------------------------------
	bool FontManager::loadFontDirectory(const char* path)
	{
		initDefaultFont();

		rtl::ArrayT<fs_entry_info_t> fileList;
		rtl::String fontPath = path;

		if (
#if defined TARGET_OS_WINDOWS
			fontPath.last() != '\\' &&
#endif
			fontPath.last() != '/')
		{
			fontPath += FS_PATH_DELIMITER;
		}
		rtl::String maskedPath = fontPath + "*.fnt";
		
		int count = fs_directory_get_entries(path, fileList);
		
		for (int i = 0; i < count; i++)
		{
			loadFont(fontPath + fileList[i].fs_filename);
		}

		return false;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool FontManager::loadFont(const char* path)
	{
		initDefaultFont();

		FontFile* font = NEW FontFile;

		if (font->load2(path))
		{
			rtl::MutexLock lock(s_fontSync);
			s_fontFileList.add(font);
			return true;
		}
		
		DELETEO(font);

		return false;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void FontManager::destroy()
	{
		rtl::MutexLock lock(s_fontSync);

		for (int i = 0; i < s_fontList.getCount(); i++)
		{
			s_fontList[i]->release();
		}

		s_fontList.clear();

		for (int i = 0; i < s_fontFileList.getCount(); i++)
		{
			DELETEO(s_fontFileList[i]);
		}

		s_fontFileList.clear();

		if (s_defFontInfo != nullptr)
		{
			DELETEO(s_defFontInfo);
			s_defFontInfo = nullptr;
		}

	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	const Font* FontManager::createFont(const char* facename, int pixHeight)
	{
		initDefaultFont();

		// ����� �� ����� (��� ����� ��������) � ��������� �������
		rtl::MutexLock lock(s_fontSync);

		// 1. ������� ��������� ������������ ������
		Font* font = findFont(facename, pixHeight);

		if (font == nullptr)
		{
			// ���� � ����������� ������� (�������)
			const FontInfo* fontInfo = findFontInfo(facename, pixHeight);

			if (fontInfo == nullptr)
			{
				// ����� �� ���������. ������ ���� � �������
				return s_defFont;
			}

			font = NEW Font();
			font->createFont(facename, pixHeight, fontInfo);
			s_fontList.add(font);
		}

		if (font != nullptr)
			font->addRef();

		return font;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void FontManager::releaseFont(const Font* constRef)
	{
		Font* font = (Font*)constRef;

		if (font != nullptr && font != s_defFont)
		{
			int ref = font->release();

			if (ref == 1)
			{
				rtl::MutexLock lock(s_fontSync);

				s_fontList.remove(font);

				font->release();
			}
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	const Font* FontManager::getDefaultFont()
	{
		initDefaultFont();

		return s_defFont;
	}
	//-----------------------------------------------
	// ������ ��� �����
	//-----------------------------------------------
	static Font* findFont(const char* facename, int pixHeight)
	{
		int count = s_fontList.getCount();
		for (int i = 0; i < count; i++)
		{
			Font* fnt = s_fontList[i];
			if (std_stricmp(facename, fnt->getFaceName()) == 0 && pixHeight == fnt->getHeight())
				return fnt;
		}

		return nullptr;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	static const FontInfo* findFontInfo(const char* facename, int pixHeight)
	{
		int count = s_fontFileList.getCount();
		for (int i = 0; i < count; i++)
		{
			FontFile* fontFile = s_fontFileList[i];
			if (std_stricmp(facename, fontFile->getFaceName()) == 0)
			{
				const FontInfo* fontInfo = fontFile->getFontByHeight(pixHeight);
				
				if (fontInfo != nullptr)
					return fontInfo;
			}
		}

		return nullptr;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	static void initDefaultFont()
	{
		if (s_defFontInfo)
			return;

		rtl::MutexLock lock(s_fontSync);
		if (s_defFontInfo == nullptr)
		{
			s_defFontInfo = NEW FontInfo;
			rtl::MemoryReader stream(s_defFontStream, sizeof(s_defFontStream));
			s_defFontInfo->load(&stream);

			s_defFont = NEW Font();

			s_defFont->createFont(s_defFontInfo->getFaceName(), s_defFontInfo->getHeader()->dfPixHeight, s_defFontInfo);
		}
	}

	//-----------------------------------------------
	//
	//-----------------------------------------------
	static void printBits(uint8_t val)
	{
		char text[9];
		text[0] = val & 0x80 ? '0' : ' ';
		text[1] = val & 0x40 ? '0' : ' ';
		text[2] = val & 0x20 ? '0' : ' ';
		text[3] = val & 0x10 ? '0' : ' ';
		text[4] = val & 0x08 ? '0' : ' ';
		text[5] = val & 0x04 ? '0' : ' ';
		text[6] = val & 0x02 ? '0' : ' ';
		text[7] = val & 0x01 ? '0' : ' ';
		text[8] = '\0';

		printf(text);
	}

	void printDefFontInfo()
	{
		rtl::MemoryReader mr;

		mr.initialize(s_defFontStream, sizeof(s_defFontStream));

		FontInfo font;

		font.load(&mr);

		const FontHeader& fh = *font.getHeader();

		printf("\tread fileheader size: %d\n", fh.dfSize);
		printf("\t--------------------------\n");
		printf("\t\tdfVersion:           %d\n", fh.dfVersion);
		printf("\t\tdfSize:              %d\n", fh.dfSize);
		printf("\t\tdfCopyright:         %s\n", fh.dfCopyright);
		printf("\t\tdfType:              %d\n", fh.dfType);
		printf("\t\tdfPoints:            %d\n", fh.dfPoints);
		printf("\t\tdfVertRes:           %d\n", fh.dfVertRes);
		printf("\t\tdfHorizRes:          %d\n", fh.dfHorizRes);
		printf("\t\tdfdfAscent:          %d\n", fh.dfAscent);
		printf("\t\tdfdfInternalLeading: %d\n", fh.dfInternalLeading);
		printf("\t\tdfExternalLeading:   %d\n", fh.dfExternalLeading);
		printf("\t\tdfItalic:            %s\n", STR_BOOL(fh.dfItalic));
		printf("\t\tdfUnderline:         %s\n", STR_BOOL(fh.dfUnderline));
		printf("\t\tdfStrikeOut:         %s\n", STR_BOOL(fh.dfStrikeOut));
		printf("\t\tdfWeight:            %d\n", fh.dfWeight);
		printf("\t\tdfCharSet:           %d\n", fh.dfCharSet);
		printf("\t\tdfPixWidth:          %d\n", fh.dfPixWidth);
		printf("\t\tdfPixHeight:         %d\n", fh.dfPixHeight);
		printf("\t\tdfPitchAndFamily:    %d\n", fh.dfPitchAndFamily);
		printf("\t\tdfAvgWidth:          %d\n", fh.dfAvgWidth);
		printf("\t\tdfMaxWidth:          %d\n", fh.dfMaxWidth);
		printf("\t\tdfFirstChar:        '%c'\n", fh.dfFirstChar);
		printf("\t\tdfLastChar:         '%c'\n", fh.dfLastChar);
		printf("\t\tdfDefaultChar:      '%c'\n", fh.dfDefaultChar);
		printf("\t\tdfBreakChar:        '%c'\n", fh.dfBreakChar);
		printf("\t\tdfWidthBytes:        %d\n", fh.dfWidthBytes);
		printf("\t\tdfDevice:            %u\n", fh.dfDevice);
		printf("\t\tdfFaceDevice:        %u\n", fh.dfFace);
		printf("\t\tdfxxx:               %u\n", fh.dfBitsPointer);
		printf("\t\tdfxxx:               %u\n", fh.dfBitsOffset);
		printf("\t\tdfReserved:          %u\n", fh.dfReserved);
		printf("\t\t----------------------------\n");

		uint32_t first = font.getHeader()->dfFirstChar;
		uint32_t last = font.getHeader()->dfLastChar;

		for (uint32_t i = first; i <= last; i++)
		{
			const GlyphInfo& glyph = font.getGlyph(i);
			printf("\t\tGlyph[%d]:           %u, %u, %u, %p\n", i, glyph.width, glyph.height, glyph.rowWidth, glyph.bitstream);
			const uint8_t * ptr = glyph.bitstream;
			for (int k = 0; k < glyph.height; k++)
			{
				for (int j = 0; j < glyph.rowWidth; j++)
				{
					printBits(*(ptr + (j*glyph.height) + k));
				}
				printf("\n");
			}
			printf("\n");
		}
	}
	
	void printFontInfo(const FontInfo& font)
	{
		const FontHeader& fh = *font.getHeader();

		printf("\tread fileheader size: %d\n", fh.dfSize);
		printf("\t--------------------------\n");
		printf("\t\tdfVersion:           %d\n", fh.dfVersion);
		printf("\t\tdfSize:              %d\n", fh.dfSize);
		printf("\t\tdfCopyright:         %s\n", fh.dfCopyright);
		printf("\t\tdfType:              %d\n", fh.dfType);
		printf("\t\tdfPoints:            %d\n", fh.dfPoints);
		printf("\t\tdfVertRes:           %d\n", fh.dfVertRes);
		printf("\t\tdfHorizRes:          %d\n", fh.dfHorizRes);
		printf("\t\tdfdfAscent:          %d\n", fh.dfAscent);
		printf("\t\tdfdfInternalLeading: %d\n", fh.dfInternalLeading);
		printf("\t\tdfExternalLeading:   %d\n", fh.dfExternalLeading);
		printf("\t\tdfItalic:            %s\n", STR_BOOL(fh.dfItalic));
		printf("\t\tdfUnderline:         %s\n", STR_BOOL(fh.dfUnderline));
		printf("\t\tdfStrikeOut:         %s\n", STR_BOOL(fh.dfStrikeOut));
		printf("\t\tdfWeight:            %d\n", fh.dfWeight);
		printf("\t\tdfCharSet:           %d\n", fh.dfCharSet);
		printf("\t\tdfPixWidth:          %d\n", fh.dfPixWidth);
		printf("\t\tdfPixHeight:         %d\n", fh.dfPixHeight);
		printf("\t\tdfPitchAndFamily:    %d\n", fh.dfPitchAndFamily);
		printf("\t\tdfAvgWidth:          %d\n", fh.dfAvgWidth);
		printf("\t\tdfMaxWidth:          %d\n", fh.dfMaxWidth);
		printf("\t\tdfFirstChar:        '%c'\n", fh.dfFirstChar);
		printf("\t\tdfLastChar:         '%c'\n", fh.dfLastChar);
		printf("\t\tdfDefaultChar:      '%c'\n", fh.dfDefaultChar);
		printf("\t\tdfBreakChar:        '%c'\n", fh.dfBreakChar);
		printf("\t\tdfWidthBytes:        %d\n", fh.dfWidthBytes);
		printf("\t\tdfDevice:            %u\n", fh.dfDevice);
		printf("\t\tdfFaceDevice:        %u\n", fh.dfFace);
		printf("\t\tdfxxx:               %u\n", fh.dfBitsPointer);
		printf("\t\tdfxxx:               %u\n", fh.dfBitsOffset);
		printf("\t\tdfReserved:          %u\n", fh.dfReserved);
		printf("\t\t----------------------------\n");

		uint32_t first = font.getHeader()->dfFirstChar;
		uint32_t last = font.getHeader()->dfLastChar;

		for (uint32_t i = first; i <= last; i++)
		{
			const GlyphInfo& glyph = font.getGlyph(i);
			printf("\t\tGlyph[%d]:           %u, %u, %u, %p\n", i, glyph.width, glyph.height, glyph.rowWidth, glyph.bitstream);
			const uint8_t * ptr = glyph.bitstream;
			for (int k = 0; k < glyph.height; k++)
			{
				for (int j = 0; j < glyph.rowWidth; j++)
				{
					printBits(*(ptr + (j*glyph.height) + k));
				}
				printf("\n");
			}
			printf("\n");
		}
	}

	void printFont(const char* filepath)
	{
		FontFile ffile;

		ffile.load2(filepath);

		for (int i = 0; i < ffile.getFontCount(); i++)
		{
			const FontInfo* finfo = ffile.getFontAt(i);

			printFontInfo(*finfo);
		}
	}

	void testBitmapChars(const char* bmpPath)
	{
		Bitmap bmp;

		FontManager::loadFont("c:\\temp\\MSSansSerif-16.fnt");

		if (bmp.loadBitmapFile(bmpPath))
		{
			ImageDrawer id(bmp);

			id.setFont("MS Sans Serif", 20);

			int x = 12, y = 12;

			id.drawChar(x, y, 'H');
			id.drawChar(x + 15, y, 'e');
			id.drawChar(x + 30, y, 'l');
			id.drawChar(x + 45, y, 'l');
			id.drawChar( x + 60, y, '0');
			
			rtl::String out = bmpPath;

			out.replace("test", "test1");

			bmp.saveBitmapFile(out);
		}
	}

	Color ContraColor2(const Color& color)
	{
		HSV hsv = { 0.0, 0.0, 0.0 };
		
		RGBtoHSV(color, hsv);

		hsv.H = 360.0F - hsv.H;

		Color result;

		HSVtoRGB(result, hsv);

		return result;
	}

	void testBitmapText(const char* bmpPath)
	{
		Bitmap bmp;

		//FontManager::loadFont("c:\\temp\\MSSansSerif-19.fnt");
		FontManager::loadFont("c:\\temp\\sseriffr.fon");

		if (bmp.loadBitmapFile(bmpPath))
		{
			ImageDrawer id(bmp);

			int height[4] = { 16,19,24,32 };
			TextBackMode mode[3] = { TextBackMode::Contoured, TextBackMode::Opaque, TextBackMode::Transparent };
			Color colors[8] = { {0,0,128}, {0,128,0}, {128,0,0}, {0, 128, 128}, {128, 128, 0}, {128, 0, 128}, {0, 0, 0}, {255, 255, 255} };
			int x = 100;
			int y = 100;
			int k = 0;
			char text[128];
			
			for (int i = 0; i < 4; i++)
			{
				id.setFont("MS Sans Serif", height[i]);

				for (int j = 0; j < 3; j++)
				{
					id.setTextBackMode(mode[j]);
					Color c = colors[k++];
					if (k == 8)
						k = 0;
					id.setTextForeColor(c);
					id.setTextBackColor(ContraColor(c));
					sprintf(text, "Hello World! %d pix %s", height[i], TextBackMode_toString(mode[j]));
					id.drawString(x, y, text);

					y += height[i] + 4;
				}

				x += 200;
			}

			rtl::String out = bmpPath;

			out.replace("test", "test1");

			bmp.saveBitmapFile(out);
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void testBitmapText2(const char* bmpPath)
	{
		Bitmap bmp;

		//FontManager::loadFont("c:\\temp\\MSSansSerif-19.fnt");
		FontManager::loadFont("c:\\temp\\sseriffr.fon");

		if (bmp.loadBitmapFile(bmpPath))
		{
			ImageDrawer id(bmp);

			int height[4] = { 16,19,24,32 };
			TextBackMode mode[3] = { TextBackMode::Contoured, TextBackMode::Opaque, TextBackMode::Transparent };
			Color colors[8] = { {0,0,128}, {0,128,0}, {128,0,0}, {0, 128, 128}, {128, 128, 0}, {128, 0, 128}, {0, 0, 0}, {255, 255, 255} };
			int x = 20;
			int y = 80;
			int k = 0;
			char text[128];

			TextFormatFlags valign[] = { TextFormatTop, TextFormatMiddleV, TextFormatBottom, TextFormatTop };
			TextFormatFlags halign[] = { TextFormatLeft, TextFormatMiddleH, TextFormatRight };

			for (int i = 0; i < 4; i++)
			{
				id.setFont("MS Sans Serif", height[i]);

				y = 80;

				for (int j = 0; j < 3; j++)
				{
					id.setTextBackMode(mode[j]);
					Color c = colors[k++];
					if (k == 8)
						k = 0;
					id.setTextForeColor(c);
					id.setTextBackColor(ContraColor(c));
					sprintf(text, "Hello World! %d pix %s", height[i], TextBackMode_toString(mode[j]));
					id.drawText(x, y, 300, 60, text, valign[i] | halign[j]);
					id.setPenColor({0,0,255});
					id.drawRectangle(x, y, x+300, y+60);
					y += 60;
				}

				x += 300;
			}

			rtl::String out = bmpPath;

			out.replace("test", "test2");

			bmp.saveBitmapFile(out);
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	static void printBits1(uint8_t val)
	{
		char text[9];
		text[0] = val & 0x80 ? '0' : ' ';
		text[1] = val & 0x40 ? '0' : ' ';
		text[2] = val & 0x20 ? '0' : ' ';
		text[3] = val & 0x10 ? '0' : ' ';
		text[4] = val & 0x08 ? '0' : ' ';
		text[5] = val & 0x04 ? '0' : ' ';
		text[6] = val & 0x02 ? '0' : ' ';
		text[7] = val & 0x01 ? '0' : ' ';
		text[8] = '\0';

		printf(text);
	}
	//-----------------------------------------------
	// �������� �������
	//-----------------------------------------------
	void printFontInfo(const char* filepath)
	{
		rtl::FileStream fd;

		fd.open(filepath);

		if (!fd.isOpened())
		{
			printf("create file failed with error\n");
			return;
		}

		FontInfo font;

		font.load(&fd);

		const FontHeader& fh = *font.getHeader();

		printf("\tread fileheader size: %d\n", fh.dfSize);
		printf("\t--------------------------\n");
		printf("\t\tdfVersion:           %d\n", fh.dfVersion);
		printf("\t\tdfSize:              %d\n", fh.dfSize);
		printf("\t\tdfCopyright:         %s\n", fh.dfCopyright);
		printf("\t\tdfType:              %d\n", fh.dfType);
		printf("\t\tdfPoints:            %d\n", fh.dfPoints);
		printf("\t\tdfVertRes:           %d\n", fh.dfVertRes);
		printf("\t\tdfHorizRes:          %d\n", fh.dfHorizRes);
		printf("\t\tdfdfAscent:          %d\n", fh.dfAscent);
		printf("\t\tdfdfInternalLeading: %d\n", fh.dfInternalLeading);
		printf("\t\tdfExternalLeading:   %d\n", fh.dfExternalLeading);
		printf("\t\tdfItalic:            %s\n", STR_BOOL(fh.dfItalic));
		printf("\t\tdfUnderline:         %s\n", STR_BOOL(fh.dfUnderline));
		printf("\t\tdfStrikeOut:         %s\n", STR_BOOL(fh.dfStrikeOut));
		printf("\t\tdfWeight:            %d\n", fh.dfWeight);
		printf("\t\tdfCharSet:           %d\n", fh.dfCharSet);
		printf("\t\tdfPixWidth:          %d\n", fh.dfPixWidth);
		printf("\t\tdfPixHeight:         %d\n", fh.dfPixHeight);
		printf("\t\tdfPitchAndFamily:    %d\n", fh.dfPitchAndFamily);
		printf("\t\tdfAvgWidth:          %d\n", fh.dfAvgWidth);
		printf("\t\tdfMaxWidth:          %d\n", fh.dfMaxWidth);
		printf("\t\tdfFirstChar:        '%c'\n", fh.dfFirstChar);
		printf("\t\tdfLastChar:         '%c'\n", fh.dfLastChar);
		printf("\t\tdfDefaultChar:      '%c'\n", fh.dfDefaultChar);
		printf("\t\tdfBreakChar:        '%c'\n", fh.dfBreakChar);
		printf("\t\tdfWidthBytes:        %d\n", fh.dfWidthBytes);
		printf("\t\tdfDevice:            %u\n", fh.dfDevice);
		printf("\t\tdfFaceDevice:        %u\n", fh.dfFace);
		printf("\t\tdfxxx:               %u\n", fh.dfBitsPointer);
		printf("\t\tdfxxx:               %u\n", fh.dfBitsOffset);
		printf("\t\tdfReserved:          %u\n", fh.dfReserved);
		printf("\t\t----------------------------\n");

		uint32_t first = font.getHeader()->dfFirstChar;
		uint32_t last = font.getHeader()->dfLastChar;

		for (uint32_t i = first; i <= last; i++)
		{
			const GlyphInfo& glyph = font.getGlyph(i);
			printf("\t\tGlyph[%d]:           %u, %u, %u, %p\n", i, glyph.width, glyph.height, glyph.rowWidth, glyph.bitstream);
			const uint8_t * ptr = glyph.bitstream;
			for (int k = 0; k < glyph.height; k++)
			{
				for (int j = 0; j < glyph.rowWidth; j++)
				{
					printBits1(*(ptr + (j*glyph.height) + k));
				}
				printf("\n");
			}
			printf("\n");
		}
	}
}

//-----------------------------------------------
