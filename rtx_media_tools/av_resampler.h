/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mmt_resampler.h"

//-----------------------------------------------
//
//-----------------------------------------------
struct AVResampleContext;

namespace media
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	class AVResampler : public Resampler
	{
	public:
		AVResampler(rtl::Logger* log);
		virtual ~AVResampler();

		virtual bool init(int in_rate, int in_channels, int out_rate, int out_channels);
		virtual void dispose();
		virtual int	resample(const short* src, int src_samples, int* src_used, short* dst, int dst_samples);
		virtual bool isSimple();

	private:
		rtl::Logger* m_log;
		AVResampleContext* m_context;
	};
}
//-----------------------------------------------
