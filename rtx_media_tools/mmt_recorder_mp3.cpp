﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_recorder_mp3.h"

namespace media
{
	//--------------------------------------
	//
	//--------------------------------------
	RecorderMonoMp3::~RecorderMonoMp3()
	{
		close();
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool RecorderMonoMp3::create(const char* filePath, const char* outEncoding, int outFrequency, const char* outFMTP)
	{
		close();

		int bitrate = outFMTP != nullptr ? strtoul(outFMTP, nullptr, 10) : 1024;

		return m_file.create(filePath, m_frequency, 1, bitrate);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void RecorderMonoMp3::close()
	{
		m_file.close();
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool RecorderMonoMp3::flush()
	{
		return m_file.flush();
	}
	//--------------------------------------
	//
	//--------------------------------------
	int RecorderMonoMp3::write(const short* samples, int count)
	{
		return m_file.write(samples, nullptr, count);
	}
	//--------------------------------------
	//
	//--------------------------------------
	RecorderStereoMp3::~RecorderStereoMp3()
	{
		close();
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool RecorderStereoMp3::create(const char* filePath, const char* outEncoding, int outFrequency, const char* outFMTP)
	{
		close();

		int bitrate = outFMTP != nullptr ? strtoul(outFMTP, nullptr, 10) : 1024;

		return m_file.create(filePath, m_frequency, 2, bitrate);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void RecorderStereoMp3::close()
	{
		m_file.close();
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool RecorderStereoMp3::flush()
	{
		return m_file.flush();
	}
	//--------------------------------------
	//
	//--------------------------------------
	int RecorderStereoMp3::write(const short* leftChannel, const short* rightChannel, int samplesPerChannel)
	{
		return m_file.write(leftChannel, rightChannel, samplesPerChannel);
	}
}
//--------------------------------------
