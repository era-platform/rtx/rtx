﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "mmt_audio_reader_rtp.h"
//--------------------------------------
// ...
//--------------------------------------
#define RAW_BUFFER_LEN	16 * 4096
#define PCM_BUFFER_LEN	4096//32000
#define JITTER_DEPTH	4

namespace media
{
	//--------------------------------------
	//
	//--------------------------------------
	struct JitterRecord
	{
		uint32_t size;
		RTPFilePacketHeader* packet;
	};
	//--------------------------------------
	// конструктор
	//--------------------------------------
	AudioReaderRTP::AudioReaderRTP(rtl::Logger* log) : AudioReader(log), m_last_pid(-1), m_lock("rtp-source")
	{
		memset(&m_header, 0, sizeof m_header);

		m_isVideo = false;

		m_frameTime = 20;

		m_raw_size = RAW_BUFFER_LEN;
		m_raw_cache = (uint8_t*)MALLOC(m_raw_size);
		LOG_FLAG1("rtp-src", "allocated raw cache %p, size = %d", m_raw_cache, m_raw_size);
		m_raw_used = 0;
		m_raw_length = 0;

		m_pcm_size = PCM_BUFFER_LEN;
		m_pcm_cache = (uint8_t*)MALLOC(m_pcm_size);
		LOG_FLAG1("rtp-src", "allocated pcm cache %p, size = %d", m_pcm_cache, m_pcm_size);
		m_pcm_used = 0;
		m_pcm_length = 0;

		m_disposed = false;

		//выделяем память под несколько RTPFilePacketHeader'ов
		m_current_time = 50;
		m_current_packet = nullptr;

		m_jitter_depth = JITTER_DEPTH;
		m_jitter = (JitterRecord*)MALLOC(m_jitter_depth * sizeof(JitterRecord));
		for (int i = 0; i < m_jitter_depth; ++i)
		{
			m_jitter[i].size = sizeof(RTPFilePacketHeader);
			m_jitter[i].packet = (RTPFilePacketHeader*)MALLOC(sizeof(RTPFilePacketHeader));

			LOG_FLAG1("rtp-src", "allocated: memblock %p, .ptr = %p, .size = %d", (m_jitter + i), m_jitter[i].packet, m_jitter[i].size);
		}

		m_jitter_ready = false;

#ifdef TEST_WRITE_TO_FILE
		m_waveFile = NEW WaveRecorder(m_log);
#endif
	}
	//--------------------------------------
	// деструктор
	//--------------------------------------
	AudioReaderRTP::~AudioReaderRTP()
	{
		close();

		try
		{
			rtl::MutexWatchLock lock(m_lock, __FUNCTION__);

			m_disposed = true;

			LOG_FLAG1("rtp-src", "deallocating raw cache %p", m_raw_cache);
			if (m_raw_cache)
			{
				FREE(m_raw_cache);
			}

			LOG_FLAG1("rtp-src", "deallocating pcm cache %p", m_pcm_cache);
			if (m_pcm_cache)
			{
				FREE(m_pcm_cache);
			}

			if (m_jitter)
			{
				for (int i = 0; i < m_jitter_count; ++i)
				{
					LOG_FLAG1("rtp-src", "deallocating: memblock %p, .ptr = %p, .size = %d", (m_jitter + i), m_jitter[i].packet, m_jitter[i].size);
					if (m_jitter[i].packet)
					{
						FREE(m_jitter[i].packet);
					}
				}
				FREE(m_jitter);
			}

#ifdef TEST_WRITE_TO_FILE
			if (m_waveFile)
				DELETEO(m_waveFile);
#endif
		}
		catch (rtl::Exception& ex)
		{
			LOG_ERROR("rtp-src", "destructor ~RTP_MediaSource failed with exception %s", ex.get_message());
		}
	}
	//--------------------------------------
	// открываем файл. в данный момент формат потока не известен. но известа его (медиа потока) длина в семплах и байтах!
	//--------------------------------------
	bool AudioReaderRTP::openSource(const char* fileName)
	{
		// закроем старый файл
		close();

		// попробуем открыть файл
		if (!m_file.open(fileName))
		{
			LOG_ERROR("rtp-src", "Can't open file '%s' : last error %d", fileName, errno);
			return false;
		}

		//читаем заголовок
		uint32_t read = 0, toRead = sizeof m_header;
		if ((read = (uint32_t)m_file.read(&m_header, toRead)) != toRead)
		{
			close();
			LOG_ERROR("rtp-src", "file read error or invalid length : last error %d, length %d", errno, read);
			return false;
		}

		if (m_header.sign != MG_REC_AUDIO_SIGNATURE)
		{
			close();
			LOG_ERROR("rtp-src", "file has invalid signature");
			return false;
		}

#ifdef TEST_WRITE_TO_FILE
		if (m_waveFile)
			m_waveFile->Create(L"c:\\temp\\rec\\rawtest.wav", &Format_PCM, sizeof Format_PCM);
#endif

		m_GTTStartTime = m_header.rec_start_time;
		m_filePath = fileName;

		return true;
	}
	//--------------------------------------
	// закрываем файл
	//--------------------------------------
	void AudioReaderRTP::closeSource()
	{
		reset();

		if (m_file.isOpened())
		{
			m_file.close();
		}

		m_filePath = rtl::String::empty;

#ifdef TEST_WRITE_TO_FILE
		if (m_waveFile)
			m_waveFile->Close();
#endif
	}
	//--------------------------------------
	// возврат на начало файла (используется в цикличном воспроизведении файла
	//--------------------------------------
	void AudioReaderRTP::reset()
	{
		//на позицию за заголовком
		if (m_file.isOpened())
		{
			m_file.setPosition(0);
		}

		m_pcm_length = 0;
		m_pcm_used = 0;
		m_raw_length = 0;
		m_raw_used = 0;

		m_current_time = 50;//50ms для того чтобы немного заполнить джитер
		m_jitter_count = 0;
		m_jitter_reader = 0;
		m_jitter_writer = 0;
		m_jitter_ready = false;
		m_current_packet = nullptr;
		m_eof = false;

		for (int i = 0; i < m_decoder_list.getCount(); i++)
		{
			decoder_info_t& info = m_decoder_list[i];
			if (info.decoder != nullptr)
			{
				DELETEO(info.decoder);
				info.decoder = nullptr;
			}
			if (info.resampler != nullptr)
			{
				DELETEO(info.resampler);
				info.resampler = nullptr;
			}
		}

		m_decoder_list.clear();
	}
	//--------------------------------------
	//
	//--------------------------------------
	AudioReaderRTP::decoder_info_t* AudioReaderRTP::get_decoder(uint8_t payload_type)
	{
		for (int i = 0; i < m_decoder_list.getCount(); i++)
		{
			decoder_info_t& info = m_decoder_list[i];

			if (info.payload_id == payload_type)
			{
				return &info;
			}
		}

		return nullptr;
	}
	//--------------------------------------
	// четние следующей порции медийных данных
	//--------------------------------------
	int AudioReaderRTP::readRawPCM(short* samples, int size)
	{
		int res = 0;

		try
		{
			rtl::MutexWatchLock lock(m_lock, __FUNCTION__);

			if (!m_disposed)
			{
				if (!m_isVideo)
				{
					m_current_time += m_frameTime;
					res = read_next_pcm(samples, size);
				}
			}
		}
		catch (rtl::Exception& ex)
		{
			LOG_ERROR("rtp-src", "EXCEPTION in ReadNext():\n%s", ex.get_message());
		}

		return res;
	}
	//--------------------------------------
	// 
	//--------------------------------------
	uint32_t AudioReaderRTP::read_next_pcm(short* samples, int size)
	{
		uint32_t needLen = m_frameTime * 16;
		uint32_t pcm_remain = (m_pcm_length > m_pcm_used) ? (m_pcm_length - m_pcm_used) : 0;

		//данные кончились?
		if (m_eof && !m_jitter_count && m_current_packet == nullptr && pcm_remain == 0)
		{
			LOG_FLAG1("rtp-src", "end of data!");
			return 0;
		}

		fill_jitter();

		//заполняем pcm-кэш данными
		while (true)
		{
			pcm_remain = (m_pcm_length > m_pcm_used) ? (m_pcm_length - m_pcm_used) : 0;

			//в pcm-кэше хватает на текущий пакет?
			if (pcm_remain >= needLen)
			{
				LOG_FLAG1("rtp-src", "reading: full pcm %d bytes. (pcm len = %d, used = %d)", needLen, m_pcm_length, m_pcm_used);

				memcpy(samples, (const short*)(m_pcm_cache + m_pcm_used), needLen);
				m_pcm_used += needLen;
				if (m_pcm_used >= m_pcm_length)
				{
					m_pcm_length = 0;
					m_pcm_used = 0;
				}
				break;
			}

			//пытаемся пополнить pcm-кэш из джитера
			if (decode_rtp_to_pcm() > 0)
			{
				continue;
			}

			//вдруг пакетов в файле было очень мало, и джиттер нам еще не успел разрешить использовать себя
			//в таком случае сами себе разрешаем 
			else if (m_eof && !m_jitter_ready)
			{
				if (m_jitter_count > 0)	// данные все-таки были прочитаны, разрешаем их использовать
				{
					m_jitter_ready = true;
					continue;
				}
				else
				{
					//данных вообще не было
					return 0;
				}
			}

			//в джитере данные кончились.
			//то что осталось дополняем пустотой и выходим.

			//перемещаем неиспользованные данные в начало
			if (m_pcm_length > m_pcm_used)
			{
				if (m_pcm_used > 0)
				{
					uint32_t toMove = m_pcm_length - m_pcm_used;
					memmove(m_pcm_cache, m_pcm_cache + m_pcm_used, toMove);
					m_pcm_length = toMove;
					m_pcm_used = 0;
				}
			}
			else
			{
				m_pcm_length = 0;
				m_pcm_used = 0;
			}

			//дополняем то чего не хватает тишиной
			uint32_t toZero = needLen - m_pcm_length;

			LOG_FLAG1("rtp-src", "reading: pcm %d bytes + silence %d bytes. (pcm len = %d, used = %d)", m_pcm_length, toZero, m_pcm_length, m_pcm_used);

			if (toZero)
			{
				memset(m_pcm_cache + m_pcm_length, 0, toZero);
			}

			memcpy(samples, (const short*)m_pcm_cache, needLen);
			/*uint32_t toZero = needLen - pcmRest;
			LOG_FLAG1("rtp-src", "reading: pcm %d bytes + silence %d bytes. (pcm len = %d, used = %d)",
				pcmRest, toZero, m_pcm_length, m_pcm_used);
			ZeroMemory(m_pcm_cache + m_pcm_used, toZero);
			packet->SetSamples((const short*)(m_pcm_cache + m_pcm_used), needLen/2);*/
			m_pcm_used = 0;
			m_pcm_length = 0;
			break;
		}

		return needLen / 2;
	}
	//--------------------------------------
	// заполнение джиттера пакетами, ориентируясь на текущее время m_current_time
	//--------------------------------------
	void AudioReaderRTP::fill_jitter()
	{
		/*
		Достаем очередной ртп-пакет. Если он получен раньше текущего времени,
		кидаем его в джиттер. Повторяем.
		*/

		while (true)
		{
			//данных больше нет?
			if (m_eof && m_current_packet == nullptr)
				break;

			//попробуем достать следующий пакет
			if (m_current_packet == nullptr && !get_next_rtp())
			{
				m_eof = true;
			}

			if (m_current_packet != nullptr)
			{
				//вычисляем время пакета относительно старта записи
				uint32_t ts = 0;

				if (m_current_packet->timestamp < m_header.rec_start_timestamp)
				{
					//если вдруг timestamp перешагнул порог и начался с 0
					ts = UINT32_MAX - m_header.rec_start_timestamp + m_current_packet->timestamp + 1;

					//16.12.10
					//проверка на кривой пакет. если ts получается сильно большим, например больше половины uint32_t,
					//откидываем его
					if (ts >= UINT32_MAX / 2)
					{
						m_current_packet = nullptr;
						continue;
					}
				}
				else
				{
					ts = m_current_packet->timestamp - m_header.rec_start_timestamp;
				}

				LOG_FLAG1("rtp-src", "processing packet. ts: %d, cur_time: %d, jbcount: %d", ts, m_current_time, m_jitter_count);

				//если пакет еще рано добавлять, на этом останавливаемся
				if (ts >= m_current_time)
				{
					break;
				}

				//копируем в джиттер
				push_to_jitter(m_current_packet);
				m_current_packet = nullptr;
			}
		}
	}
	//--------------------------------------
	// декодирование пакета из джиттера в кэш pcm
	//--------------------------------------
	uint32_t AudioReaderRTP::decode_rtp_to_pcm()
	{
		if (!m_jitter_ready)
		{
			LOG_FLAG1("rtp-src", "unable to decode - jitter buffer not filled yet");
			return 0;
		}

		/*RTPFilePacketHeader* pPacket = pop_from_jitter();
		if (pPacket == nullptr)
		{
			LOG_FLAG1("rtp-src", "unable to decode - empty jitter buffer");
			return 0;
		}

		uint32_t len = 0;
		rtp_packet rtp;

		rtp.read_packet(pPacket->buffer, pPacket->length);
		int pl_id = rtp.get_payload_type();

		decoder_info_t* dec_info = get_decoder(pl_id);
	*/

		decoder_info_t* dec_info = nullptr;
		//	RTPFilePacketHeader* pPacket = nullptr;
		int pl_id;
		uint32_t len = 0;
		rtp_packet rtp;

		do
		{

			RTPFilePacketHeader* pPacket = pop_from_jitter();
			if (pPacket == nullptr)
			{
				LOG_FLAG1("rtp-src", "unable to decode - empty jitter buffer");
				return 0;
			}

			len = 0;

			rtp.read_packet(pPacket->buffer, pPacket->length);
			pl_id = rtp.get_payload_type();

		}
		while ((dec_info = get_decoder(pl_id)) == nullptr);

		mg_audio_decoder_t* decoder = dec_info->decoder;
		short pcm_frame[960], r_frame[960];
		short* pcm_ptr;
		int pcm_frame_length = 0;

		if (decoder != nullptr)
		{
			pcm_frame_length = decoder->decode(rtp.get_payload(), rtp.get_payload_length(), (uint8_t*)pcm_frame, sizeof(pcm_frame));
		}
		else if (pl_id == 11 || pl_id == 10)
		{
			// ресамплинг если нужно
			pcm_frame_length = rtp.get_payload_length();
			memcpy(pcm_frame, rtp.get_payload(), pcm_frame_length);
		}
		else // also if (cn == 13)
		{
			memset(pcm_frame, 0, 320);
			pcm_frame_length = 320;
		}

		// проверить частоту и и если нужно то ресамплим данные
		if (m_sourceFrequency != dec_info->freq && dec_info->resampler != nullptr)
		{
			int used = 0;
			pcm_frame_length = dec_info->resampler->resample(pcm_frame, pcm_frame_length / 2, &used, r_frame, 640) * 2;
			pcm_ptr = r_frame;
		}
		else
		{
			pcm_ptr = pcm_frame;
		}

		if (pcm_frame_length > 0)
		{
			len = (uint32_t)pcm_frame_length;
			//на всякий случай перестраховка
			if (m_pcm_length + len > m_pcm_size)
			{
				LOG_WARN("rtp-src", "PCM CACHE OVERFLOW!");
				m_pcm_length = 0;
				m_pcm_used = 0;

				if (len > m_pcm_size)
				{
					LOG_WARN("rtp-src", "TOO LARGE RTP PACKET! PCM CACHE OVERFLOW!");
					len = m_pcm_size;
				}
			}

			memcpy(m_pcm_cache + m_pcm_length, pcm_ptr, len);
			m_pcm_length += len;
		}

		LOG_FLAG1("rtp-src", "packet decoded: %d bytes, available pcm: %d bytes (len = %d, used = %d)", len, m_pcm_length - m_pcm_used, m_pcm_length, m_pcm_used);

		return len;
	}
	//--------------------------------------
	// получить RTPFilePacketHeader из raw кэша
	//--------------------------------------
	bool AudioReaderRTP::get_next_rtp()
	{
		m_current_packet = nullptr;

		//пока не попадется ртп пакет
		while (read_rtp_packet(true))
		{
			//есть место для пакета?
			if (m_current_packet->type == RTYPE_RTP)
			{
				//m_src_channels = m_current_packet->format->channels;
				//m_sourceFrequency = m_current_packet->format->clock_rate;
				//m_src_encoding = m_current_packet->format->encoding;
				return true;
			}

			if (m_current_packet->type == RTYPE_FORMAT)
			{
				update_decoder_list(m_current_packet->format, m_current_packet->length / sizeof(RTPFileMediaFormat));
			}

			// остальные типы игнорируем
		}

		return false;
	}
	//--------------------------------------
	// проверка количества оставшихся raw данных.
	// если необходимо, подкачивает из файла.
	// Возвращает true, если данных в буфере хватает для целого RTPFilePacketHeader.
	//--------------------------------------
	bool AudioReaderRTP::read_rtp_packet(bool allowSwap)
	{
		m_current_packet = (RTPFilePacketHeader*)(m_raw_cache + m_raw_used);

		//сколько сырых данных в буфере осталось
		uint32_t remain = 0;

		if (m_raw_used < m_raw_length)
		{
			remain = m_raw_length - m_raw_used;
		}

		//данных в буфере достаточно ?
		uint32_t packet_length = m_current_packet->length + RTP_FILE_PACKET_HEADER_LENGTH;

		//если данных на пакет хватает, возвращаем
		if (remain >= RTP_FILE_PACKET_HEADER_LENGTH && remain >= packet_length)
		{
			m_raw_used += packet_length;
			return true;
		}
		else
		{
			m_current_packet = nullptr;
		}

		//если подкачивать нельзя, данных не хватит
		if (!allowSwap)
		{
			return false;
		}

		//в итоге, если данных не хватает на целый RTPFilePacketHeader, подкачиваем
		read_raw_data();

		return read_rtp_packet(false);
	}
	//--------------------------------------
	// чтение из файла потока ртп пакетов
	//--------------------------------------
	void AudioReaderRTP::read_raw_data()
	{
		LOG_FLAG1("rtp-src", "swapping raw data (raw_len = %d, raw_used = %d)", m_raw_length, m_raw_used);

		//если в конце буфера есть неиспользованные данные, перемещаем их в начало
		if (m_raw_used < m_raw_length)
		{
			uint32_t to_move = m_raw_length - m_raw_used;
			if (m_raw_used < to_move)
			{
				// если области перекрываются! то используем версию с учетом перекрытия (медленную)
				memmove(m_raw_cache, m_raw_cache + m_raw_used, to_move);
			}
			else
			{
				// если области НЕ перекрываются то используем быструю функцию копирования
				memcpy(m_raw_cache, m_raw_cache + m_raw_used, to_move);
			}

			m_raw_length = to_move;
			m_raw_used = 0;
		}
		else
		{
			// просто сбрасываем указатели
			m_raw_length = 0;
			m_raw_used = 0;
		}

		uint32_t to_read = m_raw_size - m_raw_length;
		uint32_t was_read = 0;

		if ((was_read = (uint32_t)m_file.read(m_raw_cache + m_raw_length, to_read)) == 0)
		{
			LOG_FLAG1("rtp-src", "end of data");
		}

		m_raw_length += was_read;
	}
	//--------------------------------------
	// смена текущего кодека
	//--------------------------------------
	void AudioReaderRTP::update_decoder_list(RTPFileMediaFormat* fmt_list, int fmt_count)
	{
		for (int i = 0; i < fmt_count; i++)
		{
			RTPFileMediaFormat& fmt = fmt_list[i];

			LOG_FLAG1("rtp-src", "changing format:\n\
\t\tencoding:	\'%s\'\n\
\t\tparams:		\'%s\'\n\
\t\tfmtp:		\'%s\'\n\
\t\tpayload id: %d\n\
\t\tclockrate:	%d\n\
\t\tchannels:	%d",
fmt.encoding,
fmt.params,
fmt.fmtp,
fmt.payload_id,
fmt.clock_rate,
fmt.channels);

			if (fmt.payload_id == 9)
			{
				fmt.clock_rate = 16000;
			}

			PayloadFormat format(fmt.encoding, (PayloadId)fmt.payload_id, fmt.clock_rate, fmt.channels, fmt.fmtp);


			decoder_info_t info;
			memset(&info, 0, sizeof(info));

			info.payload_id = fmt.payload_id;
			info.channels = fmt.channels;
			info.freq = fmt.clock_rate;
			strncpy(info.encoding, fmt.encoding, 32);
			info.decoder = rtx_codec__create_audio_decoder(info.encoding, &format);

			if (info.freq != m_sourceFrequency)
			{
				info.resampler = createResampler(info.freq, info.channels, m_sourceFrequency, m_sourceChannels);
			}

			if (info.decoder != nullptr || fmt.payload_id == 11)
			{
				m_decoder_list.add(info);
			}
		}
	}
	//--------------------------------------
	// проверка размера выделенной памяти.
	// если необходимо, выделяет больше места.
	//--------------------------------------
	void AudioReaderRTP::resize_jitter(int index, uint32_t packet_size)
	{
		if (index >= m_jitter_depth)
		{
			return;
		}

		JitterRecord* record = m_jitter + index;

		if (record->size >= packet_size)
		{
			return; // OK
		}

		RTPFilePacketHeader* new_packet = (RTPFilePacketHeader*)MALLOC(packet_size);

		LOG_FLAG1("rtp-src", "reallocating: memblock %p, .ptr = %p, .size = %d, New .ptr = %p, New .size = %d", record, record->packet, record->size, new_packet, packet_size);

		if (record->packet)
		{
			FREE(record->packet);
		}

		record->packet = new_packet;
		record->size = packet_size;
	}
	//--------------------------------------
	// jitter push
	//--------------------------------------
	void AudioReaderRTP::push_to_jitter(RTPFilePacketHeader* packet)
	{
		if (packet == nullptr) //?
			return;

		uint32_t len = packet->length + RTP_FILE_PACKET_HEADER_LENGTH;
		resize_jitter(m_jitter_writer, len);
		JitterRecord* record = m_jitter + m_jitter_writer;
		memcpy(record->packet, packet, len);

		if (m_jitter_count < m_jitter_depth)
		{
			if (++m_jitter_writer >= m_jitter_depth)
			{
				m_jitter_writer = 0;
			}
			++m_jitter_count;
		}
		else
		{
			if (++m_jitter_writer >= m_jitter_depth)
			{
				m_jitter_writer = 0;
			}
			m_jitter_reader = m_jitter_writer;
		}

		//если больше половины джиттера заполнено, разрешаем читать из него
		if (m_jitter_count > m_jitter_depth / 2)
		{
			m_jitter_ready = true;
		}
	}
	//--------------------------------------
	// jitter push
	//--------------------------------------
	RTPFilePacketHeader* AudioReaderRTP::pop_from_jitter()
	{
		if (m_jitter_count == 0)
		{
			return nullptr;
		}

		JitterRecord* record = m_jitter + m_jitter_reader;

		if (++m_jitter_reader >= m_jitter_depth)
		{
			m_jitter_reader = 0;
		}

		--m_jitter_count;

		return record->packet;
	}
}
