﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "pcm_resampler.h"
//-----------------------------------------------
//
//-----------------------------------------------
DECLARE int g_rtx_pcm_resampler_counter = -1;

namespace media
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	pcm_resampler::pcm_resampler(rtl::Logger* log)
	{
		m_log = log;

		rtl::res_counter_t::add_ref(g_rtx_pcm_resampler_counter);

		memset(m_state, 0, sizeof(m_state));
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	pcm_resampler::~pcm_resampler()
	{
		dispose();

		m_log = nullptr;

		rtl::res_counter_t::release(g_rtx_pcm_resampler_counter);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool pcm_resampler::init(int in_rate, int in_channels, int out_rate, int out_channels)
	{
		dispose();

		if (in_rate <= 0 || out_rate <= 0)
		{
			return false;
		}

		m_in_rate = in_rate;
		m_out_rate = out_rate;

		memset(m_state, 0, sizeof(m_state));

		return true;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void pcm_resampler::dispose()
	{
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	static inline short normalize_sample(int value)
	{
		if (value > SHRT_MAX)
			return SHRT_MAX;
		if (value < SHRT_MIN)
			return SHRT_MIN;

		return value;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	static void WebRtcSpl_DownsampleBy2(const int16_t* in, int len, int16_t* out, int32_t* filtState);
	static void WebRtcSpl_UpsampleBy2(const int16_t* in, int len, int16_t* out, int32_t* filtState);
	//-----------------------------------------------
	//
	//-----------------------------------------------
	int	pcm_resampler::resample(const short* src, int src_length, int* src_used, short* dst_buffer, int dst_buffer_size)
	{
		int dst_length = 0;

		if (m_in_rate > m_out_rate)
		{
			WebRtcSpl_DownsampleBy2(src, src_length, dst_buffer, m_state);
			dst_length = src_length / 2;
			*src_used = src_length;
		}
		else
		{
			int to_use = MIN(src_length, dst_buffer_size / 2);
			WebRtcSpl_UpsampleBy2(src, to_use, dst_buffer, m_state);
			*src_used = to_use;
			dst_length = to_use * 2;
		}

		return dst_length;
	}
	//-----------------------------------------------
	// allpass filter coefficients.
	//-----------------------------------------------
	static const uint16_t kResampleAllpass1[3] = { 3284, 24441, 49528 };
	static const uint16_t kResampleAllpass2[3] = { 12199, 37471, 60255 };
	//-----------------------------------------------
	// C + the 32 most significant bits of A * B
	//-----------------------------------------------
#define WEBRTC_SPL_SCALEDIFF32(A, B, C) \
    (C + (B >> 16) * A + (((uint32_t)(0x0000FFFF & B) * A) >> 16))
//-----------------------------------------------
// Multiply a 32-bit value with a 16-bit value and accumulate to another input:
//-----------------------------------------------
#define MUL_ACCUM_1(a, b, c) WEBRTC_SPL_SCALEDIFF32(a, b, c)
#define MUL_ACCUM_2(a, b, c) WEBRTC_SPL_SCALEDIFF32(a, b, c)
//-----------------------------------------------
//
//-----------------------------------------------
	static __inline int16_t WebRtcSpl_SatW32ToW16(int32_t value32)
	{
		int16_t out16 = (int16_t)value32;

		if (value32 > 32767)
			out16 = 32767;
		else if (value32 < -32768)
			out16 = -32768;

		return out16;
	}
	//-----------------------------------------------
	// decimator
	//-----------------------------------------------
	static void WebRtcSpl_DownsampleBy2(const int16_t* in, int len, int16_t* out, int32_t* filtState)
	{
		int tmp1, tmp2, diff, in32, out32;
		int i;

		register int state0 = filtState[0];
		register int state1 = filtState[1];
		register int state2 = filtState[2];
		register int state3 = filtState[3];
		register int state4 = filtState[4];
		register int state5 = filtState[5];
		register int state6 = filtState[6];
		register int state7 = filtState[7];

		for (i = (len >> 1); i > 0; i--) {
			// lower allpass filter
			in32 = (int)(*in++) << 10;
			diff = in32 - state1;
			tmp1 = MUL_ACCUM_1(kResampleAllpass2[0], diff, state0);
			state0 = in32;
			diff = tmp1 - state2;
			tmp2 = MUL_ACCUM_2(kResampleAllpass2[1], diff, state1);
			state1 = tmp1;
			diff = tmp2 - state3;
			state3 = MUL_ACCUM_2(kResampleAllpass2[2], diff, state2);
			state2 = tmp2;

			// upper allpass filter
			in32 = (int)(*in++) << 10;
			diff = in32 - state5;
			tmp1 = MUL_ACCUM_1(kResampleAllpass1[0], diff, state4);
			state4 = in32;
			diff = tmp1 - state6;
			tmp2 = MUL_ACCUM_1(kResampleAllpass1[1], diff, state5);
			state5 = tmp1;
			diff = tmp2 - state7;
			state7 = MUL_ACCUM_2(kResampleAllpass1[2], diff, state6);
			state6 = tmp2;

			// add two allpass outputs, divide by two and round
			out32 = (state3 + state7 + 1024) >> 11;

			// limit amplitude to prevent wrap-around, and write to output array
			*out++ = WebRtcSpl_SatW32ToW16(out32);
		}

		filtState[0] = state0;
		filtState[1] = state1;
		filtState[2] = state2;
		filtState[3] = state3;
		filtState[4] = state4;
		filtState[5] = state5;
		filtState[6] = state6;
		filtState[7] = state7;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	static void WebRtcSpl_UpsampleBy2(const int16_t* in, int len, int16_t* out, int32_t* filtState)
	{
		int32_t tmp1, tmp2, diff, in32, out32;
		int16_t i;

		register int32_t state0 = filtState[0];
		register int32_t state1 = filtState[1];
		register int32_t state2 = filtState[2];
		register int32_t state3 = filtState[3];
		register int32_t state4 = filtState[4];
		register int32_t state5 = filtState[5];
		register int32_t state6 = filtState[6];
		register int32_t state7 = filtState[7];

		for (i = len; i > 0; i--) {
			// lower allpass filter
			in32 = (int32_t)(*in++) << 10;
			diff = in32 - state1;
			tmp1 = MUL_ACCUM_1(kResampleAllpass1[0], diff, state0);
			state0 = in32;
			diff = tmp1 - state2;
			tmp2 = MUL_ACCUM_1(kResampleAllpass1[1], diff, state1);
			state1 = tmp1;
			diff = tmp2 - state3;
			state3 = MUL_ACCUM_2(kResampleAllpass1[2], diff, state2);
			state2 = tmp2;

			// round; limit amplitude to prevent wrap-around; write to output array
			out32 = (state3 + 512) >> 10;
			*out++ = WebRtcSpl_SatW32ToW16(out32);

			// upper allpass filter
			diff = in32 - state5;
			tmp1 = MUL_ACCUM_1(kResampleAllpass2[0], diff, state4);
			state4 = in32;
			diff = tmp1 - state6;
			tmp2 = MUL_ACCUM_2(kResampleAllpass2[1], diff, state5);
			state5 = tmp1;
			diff = tmp2 - state7;
			state7 = MUL_ACCUM_2(kResampleAllpass2[2], diff, state6);
			state6 = tmp2;

			// round; limit amplitude to prevent wrap-around; write to output array
			out32 = (state7 + 512) >> 10;
			*out++ = WebRtcSpl_SatW32ToW16(out32);
		}

		filtState[0] = state0;
		filtState[1] = state1;
		filtState[2] = state2;
		filtState[3] = state3;
		filtState[4] = state4;
		filtState[5] = state5;
		filtState[6] = state6;
		filtState[7] = state7;
	}
}
//-----------------------------------------------
