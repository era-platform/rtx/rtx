﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_file_rtp.h"

namespace media
{
	//--------------------------------------
	//
	//--------------------------------------
	FileReaderRTP::FileReaderRTP() : m_buffer(nullptr), m_length(0), m_readPtr(0)
	{
		memset(&m_header, 0, sizeof m_header);
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool FileReaderRTP::eof()
	{
		return m_readPtr >= m_length;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool FileReaderRTP::open(const char* filepath)
	{
		close();

		rtl::FileStream file;

		if (!file.open(filepath))
		{
			return false;
		}

		size_t filelength = file.getLength();

		if (filelength < sizeof(RTPFileHeader))
		{
			return false;
		}

		if (!file.read(&m_header, sizeof(m_header)))
		{
			return false;
		}

		// проверка RTP файла
		if (m_header.sign != MG_REC_AUDIO_SIGNATURE)
		{
			return false;
		}

		if (m_header.media_length == 0 || m_header.packets_rx == 0)
		{
			return false;
		}

		m_length = int(filelength - sizeof(RTPFileHeader));
		m_buffer = (uint8_t*)MALLOC(m_length);

		if (m_buffer == nullptr)
		{
			m_length = 0;
			return false;
		}

		if (file.read(m_buffer, m_length) != (size_t)m_length)
		{
			FREE(m_buffer);
			m_buffer = nullptr;
			m_length = 0;
			return 0;
		}

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void FileReaderRTP::close()
	{
		if (m_buffer != nullptr)
		{
			FREE(m_buffer);
			m_buffer = nullptr;
		}

		m_length = m_readPtr = 0;
	}
	//--------------------------------------
	//
	//--------------------------------------
	const RTPFilePacketHeader* FileReaderRTP::readNext()
	{
		if (m_buffer == nullptr || m_readPtr >= m_length)
		{
			return nullptr;
		}

		RTPFilePacketHeader* rtp_record = (RTPFilePacketHeader*)(m_buffer + m_readPtr);

		int next_step = rtp_record->length + RTP_FILE_PACKET_HEADER_LENGTH;

		if (next_step > m_length - m_readPtr)
			return nullptr;

		m_readPtr += next_step;
		return rtp_record;
	}
}
//--------------------------------------
