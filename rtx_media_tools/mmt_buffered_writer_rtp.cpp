/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "mmt_buffered_writer_rtp.h"

namespace media
{
	//--------------------------------------
   //
   //--------------------------------------
	BufferedMediaWriterRTP::BufferedMediaWriterRTP(rtl::Logger* log) : BufferedMediaWriter(log),
		m_timestamp_flag(true), m_timestamp_begin(0), m_timestamp_end(0), m_timestamp_last_pack(0), m_format_changed(false)
	{
		memset(&m_file_header, 0, sizeof m_file_header);
	}
	//--------------------------------------
	//
	//--------------------------------------
	BufferedMediaWriterRTP::~BufferedMediaWriterRTP()
	{
		destroy();
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool BufferedMediaWriterRTP::create(const char* path, int bufsize_ms)
	{
		PLOG_CALL("rtp-rec", "Create file...'%s'", path);

		if (!m_file.createNew(path))
		{
			PLOG_WARNING("rtp-rec", "Can't create file for recording : last error %d", std_get_error);
			return false;
		}

		// ������� ����� � ����
		memset(&m_file_header, 0, sizeof m_file_header);

		size_t path_l = strlen(path);

		if (std_stricmp(&path[path_l - 5], ".rtpv") == 0)
			m_file_header.sign = MG_REC_VIDEO_SIGNATURE;
		else
			m_file_header.sign = MG_REC_AUDIO_SIGNATURE;

		m_file_header.rec_start_time = rtl::DateTime::getCurrentGTS();
		m_file_header.rec_start_timestamp = rtl::DateTime::getTicks();

		PLOG_CALL("rtp-rec", "File created. write header infotmation...");

		int toWrite = sizeof(RTPFileHeader);

		if (!m_file.write(&m_file_header, toWrite))
		{
			close();
			PLOG_WARNING("rtp-rec", "Can't write header to file! Last error %d", std_get_error);
			return false;
		}

		rtl::String ppath = path;

		ppath += "_d";

		m_dout.createNew(ppath);

		BufferedMediaWriter::initialize(bufsize_ms);

		PLOG_CALL("rtp-rec", "File created and updated.");

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool BufferedMediaWriterRTP::create(const char* path, int bufsize_ms, const media::PayloadSet& formats)
	{
		bool result = create(path, bufsize_ms);

		if (result)
			update_format(formats);

		return result;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void BufferedMediaWriterRTP::close()
	{
		BufferedMediaWriter::destroy();

		if (m_file.isOpened())
		{
			PLOG_CALL("rtp-rec", "Closeing file...");
			// ������� ��������� �����
			m_file_header.rec_stop_timestamp = rtl::DateTime::getTicks();

			flush(true);

			int toWrite = sizeof(RTPFileHeader);

			m_file.setPosition(0);
			m_file.write(&m_file_header, toWrite);
			m_file.seekEnd(0);

			m_file.close();

			m_dout.close();

			PLOG_CALL("rtp-rec", "File closed.");
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool BufferedMediaWriterRTP::write_packet(const rtp_packet* packet)
	{
		if (packet == nullptr)
		{
			m_file_header.dropped_net_packets++;
			return false;
		}

		if (m_started)
		{
			if (m_format_changed)
			{
				write_format(m_additional_formats);
				m_format_changed = false;
			}

			int len = packet->get_full_length();// +rtp_packet::FIXED_RTP_HEADER_LENGTH;
			int blockLen = RTP_FILE_PACKET_HEADER_LENGTH + len;

			RTPFilePacketHeader rec_pack;
			m_file_header.rec_stop_timestamp = rec_pack.timestamp = rtl::DateTime::getTicks();
			rec_pack.type = RTYPE_RTP;
			rec_pack.length = len;

			// ��������� ��� �����
			int written = packet->write_packet(rec_pack.buffer, sizeof(rec_pack.buffer));

			m_dout.write(packet->get_payload(), packet->get_payload_length());

			if (written != len)
			{
				PLOG_WARNING("rtp-rec", "Write packet to buffer failed : writen (%d) != len (%d)", written, len);
			}

			written = m_cache.write((uint8_t*)&rec_pack, blockLen);

			if (written != blockLen)
			{
				PLOG_WARNING("rtp-rec", "Write packet to cache failed : writen (%d) != len (%d)", written, blockLen);
			}
			//else
			//{
			//	PLOG_CALL("rtp-rec", "Packet written to cache : block size %d", blockLen);
			//}

			m_file_header.packets_rx++;

			m_file_header.media_length += packet->get_payload_length();

			// �������� ������ ���������
			if (m_timestamp_flag)
			{
				m_file_header.rx_start_timestamp = m_timestamp_begin = packet->getTimestamp();
				m_timestamp_flag = false;
			}

			// ��������� ��������� ������ ���������
			m_file_header.rx_stop_timestamp = packet->getTimestamp();
			m_timestamp_end = packet->getTimestamp();
			m_timestamp_last_pack = rec_pack.timestamp;
		}

		// ������ ���������� ������ 
		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void BufferedMediaWriterRTP::update_format(const media::PayloadSet& formats)
	{
		if (!m_started)
		{
			rec_write_format(formats);
		}
		else
		{
			m_additional_formats = formats;
			m_format_changed = true;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void BufferedMediaWriterRTP::recFileStarted()
	{
		m_timestamp_flag = true;
		m_timestamp_end = m_timestamp_begin = 0;
		m_timestamp_last_pack = rtl::DateTime::getTicks();
	}
	//--------------------------------------
	//
	//--------------------------------------
	void BufferedMediaWriterRTP::recFileStopped()
	{
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool BufferedMediaWriterRTP::recFileWrite(void* dataBlock, uint32_t length, uint32_t* written)
	{
		if (!m_file.isOpened())
		{
			PLOG_WARNING("rtp-rec", "Write block failed : file not open!");
			return false;
		}

		if ((*written = (uint32_t)m_file.write(dataBlock, length)) == length)
		{
			//PLOG_CALL("rtp-rec", "block written size %u", *written);
			return true;
		}

		PLOG_WARNING("rtp-rec", "Can't write to file! %d -> %d", length, *written);

		return false;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void BufferedMediaWriterRTP::write_format(const media::PayloadSet& formats)
	{
		PLOG_CALL("rtp-rec", "Write format to cache...");

		if (formats.getCount() == 0)
		{
			PLOG_WARNING("rtp-rec", "Write format to cache failed : formats is full");
			return;
		}

		int len = sizeof(RTPFileMediaFormat) * formats.getCount();

		if (len > int(rtp_packet::PAYLOAD_BUFFER_SIZE + rtp_packet::FIXED_RTP_HEADER_LENGTH))
		{
			PLOG_WARNING("rtp-rec", "Write format to cache failed : buffer small.");
			return;
		}

		// ������� ���������� � ������ � ���������
		RTPFilePacketHeader rec_pack;
		rec_pack.timestamp = rtl::DateTime::getTicks();
		rec_pack.type = RTYPE_FORMAT;
		rec_pack.length = len;

		for (int i = 0; i < formats.getCount(); i++)
		{
			uint32_t index = i;
			const media::PayloadFormat& fmt_from_list = formats.getAt(i);
			if (fmt_from_list.isEmpty())
			{
				index = ((i - 1) < 0) ? 0 : (i - 1);
				continue;
			}

			// ��������� �������
			RTPFileMediaFormat& fmt = rec_pack.format[index];

			strncpy(fmt.encoding, fmt_from_list.getEncoding(), RTP_FILE_FMT_ENCODING_SIZE - 1);
			fmt.encoding[RTP_FILE_FMT_ENCODING_SIZE - 1] = 0;

			fmt.payload_id = fmt_from_list.getId_u8();
			fmt.clock_rate = fmt_from_list.getFrequency();
			fmt.channels = fmt_from_list.getChannelCount();

			const rtl::String& params = fmt_from_list.getParams();
			if (!params.isEmpty())
			{
				strncpy(fmt.params, params, RTP_FILE_FMT_PARAMS_SIZE - 1);
				fmt.params[RTP_FILE_FMT_PARAMS_SIZE - 1] = 0;
			}

			const rtl::String& fmtp = fmt_from_list.getFmtp();
			if (!fmtp.isEmpty())
			{
				strncpy(fmt.fmtp, fmtp, RTP_FILE_FMT_FMTP_SIZE - 1);
				fmt.fmtp[RTP_FILE_FMT_FMTP_SIZE - 1] = 0;
			}
		}

		int blockLen = RTP_FILE_PACKET_HEADER_LENGTH + len;

		// ������� ��������� �� ��������� ����� � �����
		m_cache.write((uint8_t*)&rec_pack, blockLen);

		// ���������� ������� �������
		m_timestamp_flag = true;
		m_timestamp_end = m_timestamp_begin = 0;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void BufferedMediaWriterRTP::payload_to_rtpfmt(const media::PayloadFormat& pl_fmt, RTPFileMediaFormat* rtp_fmt)
	{
		rtp_fmt->payload_id = pl_fmt.getId_u8();
		rtp_fmt->clock_rate = pl_fmt.getFrequency();
		rtp_fmt->channels = pl_fmt.getChannelCount();
		strncpy(rtp_fmt->encoding, pl_fmt.getEncoding(), 32);
		rtp_fmt->encoding[31] = 0;
		strncpy(rtp_fmt->params, pl_fmt.getParams(), 32);
		rtp_fmt->params[31] = 0;
		strncpy(rtp_fmt->fmtp, pl_fmt.getFmtp(), 64);
		rtp_fmt->fmtp[63] = 0;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void BufferedMediaWriterRTP::rec_write_format(const media::PayloadSet& formats)
	{
		RTPFilePacketHeader packet;
		packet.type = RTYPE_FORMAT;
		packet.timestamp = rtl::DateTime::getTicks();
		packet.length = uint16_t(formats.getCount() * sizeof(RTPFileMediaFormat));

		for (int i = 0; i < formats.getCount(); i++)
		{
			payload_to_rtpfmt(formats.getAt(i), &packet.format[i]);
		}

		m_file.write(&packet, packet.length + RTP_FILE_PACKET_HEADER_LENGTH);
	}
}
//--------------------------------------
