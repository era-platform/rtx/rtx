﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_payload_set.h"

namespace media
{
	//--------------------------------------
	// поиск по идентификатору формата
	//--------------------------------------
	int PayloadSet::find(PayloadId payloadId) const
	{
		for (int i = 0; i < m_set.getCount(); i++)
		{
			if (m_set[i]->getId() == payloadId)
				return i;
		}

		return BAD_INDEX;
	}
	//--------------------------------------
	// поиск по имени формата
	//--------------------------------------
	int PayloadSet::find(const char* name) const
	{
		for (int i = 0; i < m_set.getCount(); i++)
		{
			if (std_stricmp(m_set[i]->getEncoding(), name) == 0)
				return i;
		}

		return BAD_INDEX;
	}
	//--------------------------------------
	// щзукфещк присвоения
	//--------------------------------------
	PayloadSet&  PayloadSet::assign(const PayloadSet& set)
	{
		removeAll();

		for (int i = 0; i < set.getCount(); i++)
		{
			addFormat(set.getAt(i));
		}

		return *this;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int PayloadSet::getDtmfCount() const
	{
		int count = 0;

		for (int i = 0; i < m_set.getCount(); i++)
			if (m_set[i]->isDtmf())
				count++;

		return count;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int PayloadSet::getMediaCount() const
	{
		int count = 0;

		for (int i = 0; i < m_set.getCount(); i++)
			if (!m_set[i]->isDtmf())
				count++;

		return count;
	}
	//--------------------------------------
	// добавление аудио формата
	//--------------------------------------
	bool PayloadSet::addFormat(const char* name, PayloadId payloadId, int clockRate, int channels, const char* fmtp, bool top)
	{
		if (find(name) >= 0 || (payloadId != PayloadId::Dynamic && find(payloadId) >= 0))
			return false;

		int idx = BAD_INDEX;

		PayloadFormat* fmt = NEW PayloadFormat(name, payloadId, clockRate, channels, fmtp);
		if (top && m_set.getCount() > 0)
		{
			idx = m_set.insert(0, fmt);
		}
		else
		{
			idx = m_set.add(fmt);
		}

		if (idx == BAD_INDEX)
		{
			DELETEO(fmt);
		}

		return idx != BAD_INDEX;
	}
	//--------------------------------------
	// добавление формата со строковыми параметрами 
	//--------------------------------------
	bool PayloadSet::addFormat(const char* name, PayloadId payloadId, int clockRate, const char* params, const char* fmtp, bool top)
	{
		if (find(name) >= 0 || (payloadId != PayloadId::Dynamic && find(payloadId) >= 0))
			return false;

		PayloadFormat* fmt = NEW PayloadFormat(name, payloadId, clockRate, params, fmtp);

		int idx = BAD_INDEX;

		if (top && m_set.getCount() > 0)
		{
			idx = m_set.insert(0, fmt);
		}
		else
		{
			idx = m_set.add(fmt);
		}

		if (idx == BAD_INDEX)
		{
			DELETEO(fmt);
		}

		return idx != BAD_INDEX;
	}
	//--------------------------------------
	// добавление существующего формата
	//--------------------------------------
	bool PayloadSet::addFormat(const PayloadFormat& format, bool top)
	{
		PayloadId payloadId = format.getId();

		if (find(format.getEncoding()) >= 0 || (payloadId != PayloadId::Dynamic && find(payloadId) >= 0))
			return false;

		PayloadFormat* fmt = NEW PayloadFormat(format);
		int idx = BAD_INDEX;

		if (top && m_set.getCount() > 0)
		{
			idx = m_set.insert(0, fmt);
		}
		else
		{
			idx = m_set.add(fmt);
		}

		if (idx == BAD_INDEX)
		{
			DELETEO(fmt);
		}

		return idx != BAD_INDEX;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool PayloadSet::addStandardFormat(PayloadId payloadId)
	{
		return isStandardId(payloadId) ? addFormat(getStandardFormat(payloadId)) : false;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void PayloadSet::setPreferredFormat(int preferredIndex)
	{
		PayloadFormat* fmt = m_set[preferredIndex];
		if (fmt != nullptr)
		{
			m_set.removeAt(preferredIndex);
			m_set.insert(0, fmt);
		}
	}
	//--------------------------------------
	// удаление формата по индексу
	//--------------------------------------
	void PayloadSet::removeAt(int index)
	{
		if (index >= 0 && index < m_set.getCount())
		{
			PayloadFormat* fmt = m_set[index];
			DELETEO(fmt);
			m_set.removeAt(index);
		}
	}
	//--------------------------------------
	// удаление всех форматов из набора
	//--------------------------------------
	void PayloadSet::removeAll()
	{
		for (int i = 0; i < m_set.getCount(); i++)
		{
			PayloadFormat* fmt = m_set[i];
			DELETEO(fmt);
		}

		m_set.clear();
	}
	//--------------------------------------
	// Пересечение двух наборов
	//--------------------------------------
	struct fmt_t
	{
		const PayloadFormat*	fmt;
		float weight;
	};
	int PayloadSet::intersect(const PayloadSet& left, const PayloadSet& right, PayloadSet& result)
	{
		bool found = false;
		const PayloadFormat* left_fmt, *right_fmt;

		rtl::ArrayT<fmt_t> res;

		// добавляем в результирующий массив только совпадающие
		int i, j;
		for (i = 0; i < left.getCount(); i++)
		{
			found = false;

			left_fmt = (PayloadFormat*)left.m_set.getAt(i);
			for (j = 0; j < right.getCount(); j++)
			{
				if (left_fmt->isDtmf())
					continue;
				// проверяем на совпадение
				right_fmt = (PayloadFormat*)right.m_set.getAt(j);

				if (right_fmt->isDtmf())
					continue;

				const char* s1 = left_fmt->getEncoding();
				const char* s2 = right_fmt->getEncoding();

				int sl1 = std_strlen(s1);
				int sl2 = std_strlen(s2);

				if (std_strnicmp(s1, s2, MIN(sl1, sl2)) == 0)
				{
					found = true;
					break;
				}
			}

			if (found)
			{
				fmt_t f = { left_fmt, float(i + j) / 2.0f };
				res.add(f);
				continue;
			}
		}

		result.removeAll();

		int sort_cnt;
		const PayloadFormat* t;
		float ft;

		do
		{
			sort_cnt = 0;

			for (int i = 0; i < (int)res.getCount() - 1; i++)
			{
				if (res[i].weight > res[i + 1].weight)
				{
					t = res[i].fmt;
					ft = res[i].weight;
					res[i].fmt = res[i + 1].fmt;
					res[i].weight = res[i + 1].weight;
					res[i + 1].fmt = t;
					res[i + 1].weight = ft;

					sort_cnt++;
				}
			}
		} while (sort_cnt);

		for (int i = 0; i < res.getCount(); i++)
		{
			if (res[i].fmt != nullptr)
				result.addFormat(*res[i].fmt);
		}

		return (int)result.getCount();
	}
	//--------------------------------------
	//
	//--------------------------------------
	static const PayloadFormat s_PCMU("PCMU", PayloadId::PCMU, 8000, 1);
	static const PayloadFormat s_GSM("GSM", PayloadId::GSM610, 8000, 1);
	static const PayloadFormat s_G723("G723", PayloadId::G723, 8000, 1);
	static const PayloadFormat s_PCMA("PCMA", PayloadId::PCMA, 8000, 1);
	static const PayloadFormat s_G722("G722", PayloadId::G722, 16000, 1);
	static const PayloadFormat s_CN("CN", PayloadId::CN, 8000, 1);
	static const PayloadFormat s_G728("G728", PayloadId::G728, 8000, 1);
	static const PayloadFormat s_G729("G729", PayloadId::G729, 8000, 1);
	//--------------------------------------
	//
	//--------------------------------------
	const PayloadFormat& PayloadSet::getStandardFormat(PayloadId payloadId)
	{
		switch (payloadId)
		{
		case PayloadId::PCMU:	return s_PCMU;
		case PayloadId::GSM610:	return s_GSM;
		case PayloadId::G723:	return s_G723;
		case PayloadId::PCMA:	return s_PCMA;
		case PayloadId::G722:	return s_G722;
		case PayloadId::CN:		return s_CN;
		case PayloadId::G728:	return s_G728;
		case PayloadId::G729:	return s_G729;
		}

		return PayloadFormat::Empty;
	}
	//--------------------------------------
	//
	//--------------------------------------
	const PayloadFormat& PayloadSet::getStandardFormat(const char* encoding)
	{
		rtl::String enc = encoding;

		enc.toUpper();

		if (enc == "PCMU")
			return s_PCMU;
		else if (enc == "PCMA")
			return s_PCMA;
		else if (enc == "GSM")
			return s_GSM;
		else if (enc == "G723")
			return s_G723;
		else if (enc == "G722")
			return s_G722;
		else if (enc == "CN")
			return s_CN;
		else if (enc == "G728")
			return s_G728;
		else if (enc == "G729" || enc == "G729A")
			return s_G729;

		return PayloadFormat::Empty;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool PayloadSet::isStandardId(PayloadId payloadId)
	{
		bool result = false;

		switch (payloadId)
		{
		case PayloadId::PCMU:
		case PayloadId::GSM610:
		case PayloadId::G723:
		case PayloadId::PCMA:
		case PayloadId::G722:
		case PayloadId::CN:
		case PayloadId::G728:
		case PayloadId::G729:
			result = true;
			break;
		default:
			result = false;
			break; // for dummy compilers
		}

		return result;
	}
}
//--------------------------------------
