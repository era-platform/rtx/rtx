/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_drawer.h"
#include "mmt_video_font_man.h"

namespace media
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool Rectangle::intersect(const Rectangle& rect, Rectangle& result) const
	{
		result.left = left > rect.left ? left : rect.left;
		result.top = top > rect.top ? top : rect.top;
		result.right = right < rect.right ? right : rect.right;
		result.bottom = bottom < rect.bottom ? bottom : rect.bottom;

		return result.right > 0 && result.bottom > 0;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	ImageDrawer::ImageDrawer(Bitmap& image) : m_image(image), m_penWidth(1), m_penColor(Color::Black),
		m_backColor(Color::White), m_foreColor(Color::Black), m_backMode(TextBackMode::Opaque),
		m_marginLeft(2), m_marginTop(2), m_marginRight(2), m_marginBottom(2)
	{
		m_font = FontManager::getDefaultFont();
		m_clipRect = { 0, 0, image.getWidth(), image.getHeight() };
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void ImageDrawer::setClipRect(const Rectangle& clipRect)
	{
		if (clipRect.left >= m_image.getWidth() ||
			clipRect.top >= m_image.getHeight() ||
			clipRect.right <= 0 ||
			clipRect.bottom <= 0) 
			return;

		m_clipRect = clipRect;

		if (m_clipRect.left < 0)
			m_clipRect.left = 0;
		if (m_clipRect.top < 0)
			m_clipRect.top = 0;
		if (m_clipRect.right >= m_image.getWidth())
			m_clipRect.right = m_image.getWidth();
		if (m_clipRect.bottom >= m_image.getWidth())
			m_clipRect.bottom = m_image.getHeight();
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void ImageDrawer::drawImage(int x, int y, const Bitmap& image)
	{
		int cx = x + image.getWidth();
		int cy = y + image.getHeight();

		if (cx >= m_image.getWidth())
			cx = m_image.getWidth();

		if (cy >= m_image.getHeight())
			cy = m_image.getHeight();

		for (int i = x; i < cx; i++)
			for (int j = y; j < cy; j++)
				m_image.setPixel(i, j, image.getPixel(i - x, j - y));

		/* This algorithm does not clip picture with clipRect
		for (int j = y; j < cy; j++)
		{
			uint8_t* dst_row = m_image.getRowData(j) + x * m_image.BytesPerPixel;
			const uint8_t* src_row = image.getRowData(j - y);
			memcpy(dst_row, src_row, (cx - x) * image.BytesPerPixel);
		}*/
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void ImageDrawer::drawRectangle(int x1, int y1, int x2, int y2)
	{
		drawLineHorz(x1, x2-1, y1);
		drawLineVert(y1, y2-1, x2-1);
		drawLineHorz(x1, x2-1, y2-1);
		drawLineVert(y1, y2-1, x1);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void ImageDrawer::drawRectangle(const Rectangle& rect)
	{
		drawLineHorz(rect.left, rect.right-1, rect.top);
		drawLineVert(rect.top, rect.bottom-1, rect.right-1);
		drawLineHorz(rect.left, rect.right-1, rect.bottom-1);
		drawLineVert(rect.top, rect.bottom-1, rect.left);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void ImageDrawer::fillRectangle(int x1, int y1, int x2, int y2)
	{
		if (y1 > y2)
			swap(y1, y2);

		for (int y = y1; y <= y2; y += 1)
		{
			drawLine(x1, y, x2, y);
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void ImageDrawer::fillRectangle(const Rectangle& rect)
	{
		int y1 = rect.top, y2 = rect.bottom;

		if (y1 > y2)
			swap(y1, y2);

		for (int y = y1; y < y2; y += 1)
		{
			drawLineHorz(rect.left, rect.right, y);
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void ImageDrawer::drawTriangle(int x1, int y1, int x2, int y2, int x3, int y3)
	{
		drawLine(x1, y1, x2, y2);
		drawLine(x2, y2, x3, y3);
		drawLine(x3, y3, x1, y1);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void ImageDrawer::drawTriangle(const Point& p1, const Point& p2, const Point& p3)
	{
		drawLine(p1, p2);
		drawLine(p2, p3);
		drawLine(p3, p1);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void ImageDrawer::drawQuadix(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4)
	{
		drawLine(x1, y1, x2, y2);
		drawLine(x2, y2, x3, y3);
		drawLine(x3, y3, x4, y4);
		drawLine(x4, y4, x1, y1);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void ImageDrawer::drawQuadix(const Point& p1, const Point& p2, const Point& p3, const Point& p4)
	{
		drawLine(p1, p2);
		drawLine(p2, p3);
		drawLine(p3, p4);
		drawLine(p4, p1);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void ImageDrawer::drawLine(int x1, int y1, int x2, int y2)
	{
		int steep = 0;
		int sx = ((x2 - x1) > 0) ? 1 : -1;
		int sy = ((y2 - y1) > 0) ? 1 : -1;
		int dx = abs(x2 - x1);
		int dy = abs(y2 - y1);

		if (dy > dx)
		{
			swap(x1, y1);
			swap(dx, dy);
			swap(sx, sy);

			steep = 1;
		}

		int e = 2 * dy - dx;

		for (int i = 0; i < dx; ++i)
		{
			if (steep)
				drawDot(y1, x1);
			else
				drawDot(x1, y1);

			while (e >= 0)
			{
				y1 += sy;
				e -= (dx << 1);
			}

			x1 += sx;
			e += (dy << 1);
		}

		drawDot(x2, y2);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void ImageDrawer::drawLineHorz(int x1, int x2, int y)
	{
		if (x1 > x2)
		{
			swap(x1, x2);
		}

		for (int x = x1; x < x2; x++)
		{
			drawDot(x, y);
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void ImageDrawer::drawLineVert(int y1, int y2, int x)
	{
		if (y1 > y2)
		{
			swap(y1, y2);
		}

		for (int y = y1; y < y2; y++)
		{
			drawDot(x, y);
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void ImageDrawer::drawEllipse(int centerx, int centery, int a, int b)
	{
		int t1 = a * a;
		int t2 = t1 << 1;
		int t3 = t2 << 1;
		int t4 = b * b;
		int t5 = t4 << 1;
		int t6 = t5 << 1;
		int t7 = a * t5;
		int t8 = t7 << 1;
		int t9 = 0;

		int d1 = t2 - t7 + (t4 >> 1);
		int d2 = (t1 >> 1) - t8 + t5;
		int x = a;
		int y = 0;

		int negative_tx = centerx - x;
		int positive_tx = centerx + x;
		int negative_ty = centery - y;
		int positive_ty = centery + y;

		while (d2 < 0)
		{
			drawDot(positive_tx, positive_ty);
			drawDot(positive_tx, negative_ty);
			drawDot(negative_tx, positive_ty);
			drawDot(negative_tx, negative_ty);

			++y;

			t9 = t9 + t3;

			if (d1 < 0)
			{
				d1 = d1 + t9 + t2;
				d2 = d2 + t9;
			}
			else
			{
				x--;
				t8 = t8 - t6;
				d1 = d1 + (t9 + t2 - t8);
				d2 = d2 + (t9 + t5 - t8);
				negative_tx = centerx - x;
				positive_tx = centerx + x;
			}

			negative_ty = centery - y;
			positive_ty = centery + y;
		}

		do
		{
			drawDot(positive_tx, positive_ty);
			drawDot(positive_tx, negative_ty);
			drawDot(negative_tx, positive_ty);
			drawDot(negative_tx, negative_ty);

			x--;
			t8 = t8 - t6;

			if (d2 < 0)
			{
				++y;
				t9 = t9 + t3;
				d2 = d2 + (t9 + t5 - t8);
				negative_ty = centery - y;
				positive_ty = centery + y;
			}
			else
				d2 = d2 + (t5 - t8);

			negative_tx = centerx - x;
			positive_tx = centerx + x;
		} while (x >= 0);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void ImageDrawer::drawCircle(int centerx, int centery, int radius)
	{
		int x = 0;
		int d = (1 - radius) << 1;

		while (radius >= 0)
		{
			drawDot(centerx + x, centery + radius);
			drawDot(centerx + x, centery - radius);
			drawDot(centerx - x, centery + radius);
			drawDot(centerx - x, centery - radius);

			if ((d + radius) > 0)
				d -= ((--radius) << 1) - 1;
			if (x > d)
				d += ((++x) << 1) + 1;
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void ImageDrawer::drawDot(int x, int y)
	{
		switch (m_penWidth)
		{
		case 1: setPixel(x, y);
			break;

		case 2: {
			setPixel(x, y);
			setPixel(x + 1, y);
			setPixel(x + 1, y + 1);
			setPixel(x, y + 1);
		}
				break;

		case  3: {
			setPixel(x, y - 1);
			setPixel(x - 1, y - 1);
			setPixel(x + 1, y - 1);

			setPixel(x, y);
			setPixel(x - 1, y);
			setPixel(x + 1, y);

			setPixel(x, y + 1);
			setPixel(x - 1, y + 1);
			setPixel(x + 1, y + 1);
		}
				 break;

		default: setPixel(x, y);
			break;
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void ImageDrawer::setTextMargins(int left, int top, int right, int bottom)
	{
		m_marginLeft = left;
		m_marginRight = right;
		m_marginTop = top;
		m_marginBottom = bottom;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void ImageDrawer::setFont(const char* facename, int height)
	{
		FontManager::releaseFont(m_font);

		m_font = FontManager::createFont(facename, height);

		if (m_font == nullptr)
		{
			m_font = FontManager::getDefaultFont();
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	int ImageDrawer::drawText(int x, int y, int width, int height, const char* zstr, uint32_t flags)
	{
		Rectangle oldClip;
		getClipRect(oldClip);
		setClipRect({ x, y, x + width, y + height });

		int dy = calculateVertAlign(flags);
		if (dy <= 0)
			return 0;

		int dx = calculateHorzAlign(zstr, flags);
		if (dx <= 0)
			return 0;

		int drawWidth = 0;

		switch (m_backMode)
		{
		case TextBackMode::Contoured:
			drawWidth = drawTextContoured(dx, dy, zstr);
			break;
		case TextBackMode::Opaque:
		{
			Color t = m_penColor;
			m_penColor = m_backColor;
			fillRectangle(m_clipRect);
			m_penColor = t;
			drawWidth = drawTextTransparent(dx, dy, zstr);
		}
			break;
		case TextBackMode::Transparent:
			drawWidth = drawTextTransparent(dx, dy, zstr);
			break;
		}

		setClipRect(oldClip);

		return drawWidth;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	int ImageDrawer::drawText(const Rectangle& bounds, const char* zstr, uint32_t flags)
	{
		Rectangle oldClip;
		getClipRect(oldClip);
		setClipRect(bounds);

		int dy = calculateVertAlign(flags);
		if (dy <= 0)
			return 0;

		int dx = calculateHorzAlign(zstr, flags);
		if (dx <= 0)
			return 0;

		int drawWidth = 0;
		
		switch (m_backMode)
		{
		case TextBackMode::Contoured:
			drawWidth = drawTextContoured(dx, dy, zstr);
			break;
		case TextBackMode::Opaque:
		{
			Color t = m_penColor;
			m_penColor = m_backColor;
			fillRectangle(m_clipRect);
			m_penColor = t;
			drawWidth = drawTextTransparent(dx, dy, zstr);
		}
			break;
		case TextBackMode::Transparent:
			drawWidth = drawTextTransparent(dx, dy, zstr);
			break;
		}

		setClipRect(oldClip);

		return drawWidth;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	int ImageDrawer::drawText(int x, int y, const char* zstr)
	{
		int drawWidth = 0;

		switch (m_backMode)
		{
		case TextBackMode::Contoured:
			drawWidth = drawTextContoured(x, y, zstr);
			break;
		case TextBackMode::Opaque:
			drawWidth = drawTextOpaque(x, y, zstr);
			break;
		case TextBackMode::Transparent:
			drawWidth = drawTextTransparent(x, y, zstr);
			break;
		}

		return drawWidth;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	Size ImageDrawer::drawString(int x, int y, const char* zstr)
	{
		int drawWidth = 0;

		switch (m_backMode)
		{
		case TextBackMode::Contoured:
			drawWidth = drawTextContoured(x, y, zstr);
			break;
		case TextBackMode::Opaque:
			drawWidth = drawTextOpaque(x, y, zstr);
			break;
		case TextBackMode::Transparent:
			drawWidth = drawTextTransparent(x, y, zstr);
			break;
		}

		if (drawWidth)
		{
			return { drawWidth, m_font->getHeight() };
		}

		return { 0,0 };
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	Size ImageDrawer::drawChar(int x, int y, char ch)
	{
		const PixelMap* pmap = m_font->getCharPixelMap(ch);
		if (pmap)
		{
			switch (m_backMode)
			{
			case TextBackMode::Contoured:
				drawCharContoured(x, y, pmap);
				break;
			case TextBackMode::Opaque:
				drawCharOpaque(x, y, pmap);
				break;
			case TextBackMode::Transparent:
				drawCharTransparent(x, y, pmap);
				break;
			}

			return { pmap->getWidth(), pmap->getHeight() };
		}

		return {0,0};
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	int ImageDrawer::drawTextContoured(int x, int y, const char* text)
	{
		int x1 = x;

		while (*text)
		{
			const PixelMap* pmap = m_font->getCharPixelMap(*text++);
			if (pmap)
			{
				drawCharContoured(x1, y, pmap);
				x1 += pmap->getWidth() + 1;
			}
		}

		return x1 - x;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	int ImageDrawer::drawTextOpaque(int x, int y, const char* text)
	{
		int x1 = x;

		while (*text)
		{
			const PixelMap* pmap = m_font->getCharPixelMap(*text++);
			if (pmap)
			{
				drawCharOpaque(x1, y, pmap);
				x1 += pmap->getWidth() + 1;
			}
		}

		return x1 - x;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	int ImageDrawer::drawTextTransparent(int x, int y, const char* text)
	{
		int x1 = x;

		while (*text)
		{
			const PixelMap* pmap = m_font->getCharPixelMap(*text++);
			if (pmap)
			{
				drawCharTransparent(x1, y, pmap);
				x1 += pmap->getWidth() + 1;
			}
		}

		return x1 - x;
	}

#define SET_PIXEL(xx, yy) \
	if (!pmap->getPixelState((xx), (yy))) \
		setPixel(x + (xx), y + (yy), m_backColor)

	//----------------------------------------------
	//
	//----------------------------------------------
	void ImageDrawer::drawCharContoured(int x, int y, const PixelMap* pmap)
	{
		for (int k = 0; k < pmap->getHeight(); k++)
		{
			for (int j = 0; j < pmap->getWidth(); j++)
			{
				if (pmap->getPixelState(j, k))
				{
					setPixel(x + j, y + k, m_foreColor);
		
					int x1 = j - 1, x2 = j, x3 = j + 1,
						y1 = k - 1, y2 = k, y3 = k + 1;
					SET_PIXEL(x1, y1);
					SET_PIXEL(x2, y1);
					SET_PIXEL(x3, y1);
					SET_PIXEL(x1, y2);
					SET_PIXEL(x3, y2);
					SET_PIXEL(x1, y3);
					SET_PIXEL(x2, y3);
					SET_PIXEL(x3, y3);
				}
			}
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void ImageDrawer::drawCharOpaque(int x, int y, const PixelMap* pmap)
	{
		for (int k = 0; k < pmap->getHeight(); k++)
		{
			for (int j = 0; j < pmap->getWidth(); j++)
			{
				setPixel(x + j, y + k, pmap->getPixelState(j, k) ? m_foreColor : m_backColor);
			}
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void ImageDrawer::drawCharTransparent(int x, int y, const PixelMap* pmap)
	{
		for (int k = 0; k < pmap->getHeight(); k++)
		{
			for (int j = 0; j < pmap->getWidth(); j++)
			{
				if (pmap->getPixelState(j, k))
				{
					setPixel(x + j, y + k, m_foreColor);
				}
			}
		}
	}

	int ImageDrawer::getTextWidth(const char* zstr)
	{
		return m_font->getTextWidth(zstr);
	}

	int ImageDrawer::calculateVertAlign(uint32_t flags)
	{
		// 1. vertical alignment
		int fh = m_font->getHeight();

		if (flags & TextFormatBottom)
		{
			return m_clipRect.bottom - m_marginBottom - fh;
		}
		
		if (flags & TextFormatMiddleV)
		{
			return m_clipRect.top + (m_clipRect.getHeight() / 2 - fh / 2);
		}

		//if (flags & TextFormatTop)
		return m_clipRect.top + m_marginTop;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	int ImageDrawer::calculateHorzAlign(const char* text, uint32_t flags)
	{
		// 1. vertical alignment
		int tw = m_font->getTextWidth(text);
		int fh = m_font->getHeight();

		if (flags & TextFormatRight)
		{
			return m_clipRect.right - m_marginRight - tw;
		}

		if (flags & TextFormatMiddleH)
		{
			return m_clipRect.left + (m_clipRect.getWidth() / 2 - tw / 2);
		}

		return m_clipRect.left + m_marginTop;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	const char* TextBackMode_toString(TextBackMode mode)
	{
		const char* names[] = { "Transparent", "Contoured", "Opaque" };
		int index = (int)mode;
		return index >= 0 && index < sizeof(names) / sizeof(const char*) ? names[index] : "Unknown";
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	TextBackMode TextBackMode_parse(const char* mode)
	{
		if (std_stricmp(mode, "contoured"))
			return TextBackMode::Contoured;
		if (std_stricmp(mode, "transparent"))
			return TextBackMode::Transparent;
		// it's default mode // if (std_stricmp(mode, "opaque"))
		return TextBackMode::Opaque;

	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	rtl::String TextFormatFlags_toString(TextFormatFlags flags)
	{
		rtl::String result;

		if (flags & TextFormatMiddleH)
			result += "MiddleH";
		else if (flags & TextFormatRight)
			result += "Right";
		else
			result += "Left";

		result += ", ";

		if (flags & TextFormatMiddleV)
			result += "MiddleV, ";
		else if (flags & TextFormatBottom)
			result += "Bottom, ";
		else
			result += "Top, ";

		result += ", ";

		if (flags & TextFormatMultiline)
			result += "MultiLine ";
		else 
			result += "SingleLine";

		return result;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	TextFormatFlags TextFormatFlags_parseItem(const char* flagsStr)
	{
		if (std_stricmp(flagsStr, "right") == 0)
			return TextFormatRight;
		if (std_stricmp(flagsStr, "middlehorz") == 0)
			return TextFormatMiddleH;
		if (std_stricmp(flagsStr, "left") == 0)
			return TextFormatLeft;
		if (std_stricmp(flagsStr, "top") == 0)
			return TextFormatTop;
		if (std_stricmp(flagsStr, "middlevert") == 0)
			return TextFormatMiddleV;
		if (std_stricmp(flagsStr, "bottom") == 0)
			return TextFormatBottom;
		if (std_stricmp(flagsStr, "multiline") == 0)
			return TextFormatMultiline;
		if (std_stricmp(flagsStr, "singleline") == 0)
			return TextFormatSingleLine;

		return TextFormatLeft;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	TextFormatFlags TextFormatFlags_parse(const char* flagsStr)
	{
		uint32_t flags = 0;
		rtl::String fstr = flagsStr;
		rtl::StringList items;

		fstr.split(items, ',');

		for (int i = 0; i < items.getCount(); i++)
		{
			flags |= TextFormatFlags_parseItem(items[i]);
		}

		return (TextFormatFlags)flags;
	}
}
//-----------------------------------------------
