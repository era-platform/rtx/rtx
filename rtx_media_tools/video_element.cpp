/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_video_frame.h"

#define NON_VIDEO_STREAM 0

#define JSON_ID "id"
#define JSON_TYPE "type"
#define JSON_LEFT "left"
#define JSON_TOP "top"
#define JSON_WIDTH "width"
#define JSON_HEIGHT "height"
#define JSON_FORMAT "format"
#define JSON_TEXT "text"
#define JSON_BITMAP "bitmap"
#define JSON_TERMID "termId"
#define JSON_TIMEZONE "timezone"
#define JSON_MODE "mode"
#define JSON_TITLEHEIGHT "titleHeight"
#define JSON_ZORDER "zOrder"
#define JSON_ZINDEX "z-index"
#define JSON_BACKCOLOR "backgroundColor"
#define JSON_FORECOLOR "foregroundColor"
#define JSON_FACENAME "faceName"
#define JSON_TEXTMODE "textMode"
#define JSON_TEXTFLAGS "textFlags"

namespace media
{

	using namespace rtl;
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	VideoFrameElement::VideoFrameElement(VideoFrameGenerator& frame, VideoElementType type, const rtl::String& id) :
		m_id(id), m_frame(frame), m_type(type),
		m_backColor({ 128,192,204,0 }),
		m_z(0),
		m_updated(true)
	{
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	VideoFrameElement::~VideoFrameElement()
	{
		// �����
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------

	void VideoFrameElement::assign(const VideoRawElement& data)
	{
		m_bounds.left = data.left;
		m_bounds.top = data.top;
		m_bounds.right = data.left + data.width;
		m_bounds.bottom = data.top + data.height;
		m_z = data.zOrder;
		m_backColor = data.backColor;
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	void VideoFrameElement::drawOn(ImageDrawer& draw)
	{
#if TRACE_VIDEO
		m_frame.trace(this);
#endif
		m_updated = false;
	}
	//-----------------------------------------------
	// normal elements
	//-----------------------------------------------
	bool VideoFrameElement::isUpdated()
	{
		return m_updated;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VideoRawElement::VideoRawElement() : id(nullptr),
		stringValue(nullptr),
		faceName(nullptr),
		left(0), width(0), top(0), height(0),
		backColor({ 0,0,0,0 }),
		foreColor({ 255,255,255,0 }),
		videoTermId(VideoFrameStream::EmptyId),
		videoMode(VideoStreamMode::Auto),
		titleHeight(0),
		textMode(TextBackMode::Opaque),
		textFlags(TextFormatLeft),
		timezone(0)
	{
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoRawElement::read(JsonParser& parser)
	{
		const rtl::String& name = parser.getFieldName();

		if (::strcmp(name, JSON_ID) == 0)
			id = ::strdup(parser.getString());
		else if (::strcmp(name, JSON_TYPE) == 0)
			type = VideoElementType_parse(parser.getString());
		else if (::strcmp(name, JSON_LEFT) == 0)
			left = (int)parser.getInteger();
		else if (::strcmp(name, JSON_TOP) == 0)
			top = (int)parser.getInteger();
		else if (::strcmp(name, JSON_WIDTH) == 0)
			width = (int)parser.getInteger();
		else if (::strcmp(name, JSON_HEIGHT) == 0)
			height = (int)parser.getInteger();
		else if (::strcmp(name, JSON_TEXT) == 0 || ::strcmp(name, JSON_BITMAP) == 0 || ::strcmp(name, JSON_FORMAT) == 0)
		{
			stringValue = ::strdup(parser.getString());
			stringLength = parser.getString().getLength();
		}
		else if (::strcmp(name, JSON_TERMID) == 0)
			videoTermId = (int)parser.getInteger();
		else if (::strcmp(name, JSON_TIMEZONE) == 0)
			timezone = (int)parser.getInteger();
		else if (::strcmp(name, JSON_MODE) == 0)
			videoMode = VideoStreamMode_parse(parser.getString());
//#if defined _DEBUG
//			videoMode = VideoStreamMode::Auto;
//#else
//			videoMode = VideoStreamMode_parse(parser.getString());
//#endif
		else if (::strcmp(name, JSON_TITLEHEIGHT) == 0)
			titleHeight = (int)parser.getInteger();
		else if (::strcmp(name, JSON_ZORDER) == 0 || ::strcmp(name, JSON_ZINDEX) == 0)
			zOrder = (int)parser.getInteger();
		else if (::strcmp(name, JSON_FACENAME) == 0)
			faceName = ::strdup(parser.getString());
		else if (::strcmp(name, JSON_TEXTFLAGS) == 0)
			textFlags = TextFormatFlags_parse(parser.getString());
		else if (::strcmp(name, JSON_TEXTMODE) == 0)
			textMode = TextBackMode_parse(parser.getString());
		else if (::strcmp(name, JSON_BACKCOLOR) == 0)
			backColor = hexToColor(parser.getString());
		else if (::strcmp(name, JSON_FORECOLOR) == 0)
			foreColor = hexToColor(parser.getString());
	}
	//--------------------------------------------------
	//
	//--------------------------------------------------
	void VideoRawElement::clean()
	{
		if (id != nullptr)
		{
			FREE(id);
			id = nullptr;
		}

		if (stringValue != nullptr)
		{
			FREE(stringValue);
			stringValue = nullptr;
		}

		if (faceName != nullptr)
		{
			FREE(faceName);
			faceName = nullptr;
		}
	}
	//--------------------------------------------------
	//
	//--------------------------------------------------
	VideoElementType VideoElementType_parse(const char* text)
	{
		if (::strcmp(text, "text") == 0)
			return VideoElementType::Text;

		if (::strcmp(text, "bitmap") == 0)
			return VideoElementType::Bitmap;

		if (::strcmp(text, "time") == 0)
			return VideoElementType::Time;

		if (::strcmp(text, "video") == 0)
			return VideoElementType::VideoStream;

		return VideoElementType::Custom;
	}
	//--------------------------------------------------
	//
	//--------------------------------------------------
	const char* VideoElementType_toString(VideoElementType type)
	{
		static const char* names[] = { "text", "bitmap", "time", "video" };
		int index = (int)type;
		return index >= 0 && index < 4 ? names[index] : "unknown";
	}
	//--------------------------------------------------
	//
	//--------------------------------------------------
	VideoStreamMode VideoStreamMode_parse(const char* text)
	{
		if (::strcmp(text, "term") == 0)
			return VideoStreamMode::Term;

		if (::strcmp(text, "auto") == 0)
			return VideoStreamMode::Auto;

		if (::strcmp(text, "vad") == 0)
			return VideoStreamMode::Vad;

		return VideoStreamMode::Error;
	}
	//--------------------------------------------------
	//
	//--------------------------------------------------
	const char* VideoStreamMode_toString(VideoStreamMode type)
	{
		static const char* names[] = { "Auto", "Term", "Vad" };
		int index = (int)type;
		return index >= 0 && index < 4 ? names[index] : "Error";
	}
}
//--------------------------------------------------
