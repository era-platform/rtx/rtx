﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_video_stream.h"
#include "mmt_video_frame.h"

namespace media
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VideoFrameStream::~VideoFrameStream()
	{
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoFrameStream::assign(const VideoRawElement& data)
	{
		VideoFrameElement::assign(data);

		m_mode = data.videoMode;
		m_bindId = data.videoTermId;
		m_titleHeight = data.titleHeight;
		m_videoTitle = data.stringValue;

		m_picture.createBitmap(m_bounds.getWidth(), m_bounds.getHeight());
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoFrameStream::drawOn(ImageDrawer& draw)
	{
		rtl::MutexLock lock(m_sync);

		rtl::String text = m_videoTitle;

#if TRACE_VIDEO		
		m_frame.trace(this);
		m_frame.trace("bindId:%llx mode:%s title:%s\r\n", m_bindId, VideoStreamMode_toString(m_mode), m_videoTitle);
#endif
		if (m_hasStream)
		{
			//m_frame.trace("drawImage(x:%d, y:%d <- w:%d, h:%d)\r\n", m_bounds.left, m_bounds.top, m_picture.getWidth(), m_picture.getHeight());
			draw.drawImage(m_bounds.left, m_bounds.top, m_picture);
#if defined _DEBUG
			draw.setPenColor({ 255,255,0 });
			draw.drawRectangle(m_bounds);
#endif
		}
		else
		{
			m_frame.drawNoSignalImage(m_bounds);
#if defined _DEBUG
			draw.setPenColor({ 0,255,255 }); // временно
			draw.drawRectangle(m_bounds);
#endif
		}

		if (text.isEmpty())
		{
			text << "TermId: " << m_bindId;
		}

		draw.setTextBackColor(ContraColor(m_frame.getForeColor())); // 
		draw.setTextForeColor(m_frame.getForeColor());
		draw.setTextBackMode(TextBackMode::Contoured);
		int textWidth = draw.getTextWidth(text);
		int x = m_bounds.left + (m_bounds.getWidth() - (textWidth + 4)) / 2;
			int y = m_bounds.bottom - (draw.getFontHeight() + 4);

		draw.drawText(x, y, textWidth + 4, draw.getFontHeight() + 4, text, TextFormatMiddleH | TextFormatMiddleV);
		//m_frame.trace("drawText(%d, %d, %d, %d, \"%s\")\r\n", x, y, textWidth + 4, draw.getFontHeight() + 4, text);
#if defined _DEBUG
		char mode[128];
		uint32_t termId = (uint32_t)(m_bindId >> 32);
		uint32_t videoId = (uint32_t)(m_bindId & 0xFFFFFFFF);
		std_snprintf(mode, 128, "id:%s | mode: %s | termId: %u|%u | vl: %s | uc: %s(%d)", (const char*)m_id, VideoStreamMode_toString(m_mode), termId, videoId, STR_BOOL(m_vad), STR_BOOL(m_hasStream), m_updateCounter);
		x = m_bounds.left + 10;
		y = m_bounds.top + 10;
		textWidth = draw.getTextWidth(mode);
		draw.setTextForeColor(Color::Black);
		draw.drawText(x, y, textWidth + 4, draw.getFontHeight() + 4, mode, TextFormatLeft | TextFormatMiddleV);
		std_snprintf(mode, 128, "bounds : %d, %d, %d, %d", m_bounds.left, m_bounds.top, m_bounds.right, m_bounds.bottom);
		x = m_bounds.left + 10;
		y = m_bounds.top + 40;
		textWidth = draw.getTextWidth(mode);
		draw.setTextForeColor(Color::Black);
		draw.drawText(x, y, textWidth + 4, draw.getFontHeight() + 4, mode, TextFormatLeft | TextFormatMiddleV);
#endif

		drawVadBar(draw);

		m_updated = false;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool VideoFrameStream::isUpdated()
	{
		return m_updated;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoFrameStream::drawVadBar(ImageDrawer& draw)
	{
		if (!m_frame.useVadDetector() || !m_vad) // m_vadLevel == 0
			return;

		uint8_t p = 255; // m_vadLevel * 5;
		Rectangle r = m_bounds;
		draw.setPenColor({p, p, 0});
		draw.drawRectangle(r);
		r.inflate(-1, -1);
		draw.drawRectangle(r);
		r.inflate(-1, -1);
		draw.drawRectangle(r);
		r.inflate(-1, -1);
		draw.drawRectangle(r);

		//m_frame.trace("drawVadBar(m_bounds)\r\n");
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoFrameStream::updateImage(const media::VideoImage* image)
	{
		Bitmap original, scaled;
		image->exportARGB(original);
		Bitmap::transformImage(original, scaled, m_bounds.getWidth(), m_bounds.getHeight()); // m_picture
		
		{
			rtl::MutexLock lock(m_sync);

			ImageDrawer drawer(m_picture);
			drawer.drawImage(0, 0, scaled);
			m_updateCounter++;
			m_hasStream = true;
			m_updated = true;
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoFrameStream::updateVAD(bool vadDetected)
	{
		m_vad = vadDetected;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoFrameStream::stopStream()
	{
		LOG_CALL("VFS", "Stop video stream ID %lld rect:%d,%d,%d,%d", m_bindId, m_bounds.left, m_bounds.top, m_bounds.right, m_bounds.bottom);
			
		m_picture.clear();

		m_hasStream = false;
		m_updated = true;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	uint64_t VideoFrameStream::bindToTermimation(uint64_t termId)
	{
		return m_bindId |= termId << 32;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VideoFrameVadStream::~VideoFrameVadStream()
	{
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoFrameVadStream::assign(const VideoRawElement& data)
	{
		VideoFrameElement::assign(data);

		m_picture.createBitmap(m_bounds.getWidth(), m_bounds.getHeight());

		LOG_CALL("VFS", "VideoFrameVADStream: bounds %d,%d,%d,%d",
			m_bounds.left,
			m_bounds.top,
			m_bounds.right,
			m_bounds.bottom);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoFrameVadStream::drawOn(ImageDrawer& draw)
	{
		rtl::MutexLock lock(m_sync);
#if TRACE_VIDEO		
		m_frame.trace(this);
#endif
		if (m_hasStream)
		{
			if (m_updated)
			{
#if TRACE_VIDEO		
				m_frame.trace("drawImage(x:%d, y:%d <- w:%d, h:%d)\r\n", m_bounds.left, m_bounds.top, m_picture.getWidth(), m_picture.getHeight());
#endif
				draw.drawImage(m_bounds.left, m_bounds.top, m_picture);
				draw.setPenColor(Color::Blue);
				draw.drawRectangle(m_bounds);
			}
		}
		else
		{
			m_frame.drawNoVideoImage(m_bounds);
			draw.setPenColor(Color::Red);
			draw.drawRectangle(m_bounds);
		}

		m_updated = false;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool VideoFrameVadStream::isUpdated()
	{
		return m_updated;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoFrameVadStream::updateImage(const media::VideoImage* image)
	{
		rtl::MutexLock lock(m_sync);

		if (image)
		{
			Bitmap original, scaled;
			image->exportARGB(original);
			Bitmap::transformImage(original, scaled, m_bounds.getWidth(), m_bounds.getHeight()); // m_picture
			ImageDrawer drawer(m_picture);
			drawer.drawImage(0, 0, scaled);

			m_updateCounter++;
			m_hasStream = true;
			m_updated = true;
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoFrameVadStream::updateVAD(bool vadDetected)
	{
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoFrameVadStream::stopStream()
	{
	}
}
//-----------------------------------------------
