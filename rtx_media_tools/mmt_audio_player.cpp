﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <random>

#include "stdafx.h"
#include "mmt_audio_player.h"
#include "std_filesystem.h"
#include <random>

namespace media
{
	namespace
	{
		int GenerateRandom(int min, int max)
		{
			std::random_device device;
			std::default_random_engine generator{device()};
			std::uniform_int_distribution<int> distribution(min, max);
			return distribution(generator);
		}
	}

	const int16_t AudioPlayerStopEvent = 0;
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	AudioPlayer::AudioPlayer(rtl::Logger* log) : Metronome(*log), m_log(log)
	{
		m_reader = nullptr;

		m_started = false;
		m_work = false;

		m_fileList.clear();

		m_volume = 0;
		m_loop = false;
		m_random = false;
		m_indexCurrentFile = 0;

		m_startAt = 0;
		m_stopAt = 0;
		m_alreadyRead = 0;

		m_ticksPeriod = 20;
		m_sampleRate = 8000;
		m_channelCount = 1;

		m_handler = nullptr;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	AudioPlayer::~AudioPlayer()
	{
		destroy();
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool AudioPlayer::init(int ticksPeriod, int sampleRate, int channelCount, IPlayerEventHandler* handler)
	{
		m_ticksPeriod = ticksPeriod;
		m_sampleRate = sampleRate;
		m_channelCount = channelCount;
		m_handler = handler;

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void AudioPlayer::destroy()
	{
		stopPlay();

		if (m_reader != nullptr)
		{
			m_reader->close();
			DELETEO(m_reader);
			m_reader = nullptr;
		}

		m_fileList.clear();

		m_volume = 0;
		m_loop = false;
		m_random = false;

		m_indexCurrentFile = 0;
		m_startAt = 0;
		m_stopAt = 0;
		m_alreadyRead = 0;

		m_ticksPeriod = 20;
		m_sampleRate = 8000;
		m_channelCount = 1;

		m_handler = nullptr;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void AudioPlayer::setStartAt(int startAt)
	{
		m_startAt = startAt;
	}
	//------------------------------------------------------------------------------
	void AudioPlayer::setStopAt(int stopAt)
	{
		m_stopAt = stopAt;
	}
	//------------------------------------------------------------------------------
	void AudioPlayer::setLoop(bool loop)
	{
		m_loop = loop;
	}
	//------------------------------------------------------------------------------
	void AudioPlayer::setRandom(bool random)
	{
		m_random = random;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool AudioPlayer::startPlay()
	{
		if (m_started)
		{
			return false;
		}

		m_report.stopTime = 0;
		m_report.errorFiles.clear();

		if (m_fileList.getCount() == 0)
		{
			return false;
		}

		m_indexCurrentFile = 0;
		if (m_random)
		{
			if (!openRandomFile())
			{
				return false;
			}
		}
		else if (!openFile(m_indexCurrentFile))
		{
			return false;
		}

		if (m_startAt > 0)
		{
			int need_read = m_startAt * m_sampleRate / 1000;
			while (need_read > 0)
			{
				const int read = 2048;
				short buff[read] = { 0 };

				if (need_read > read)
				{
					if (m_reader->readNext(buff, read) != read)
					{
						return false;
					}
					need_read -= read;
					m_alreadyRead += read;
				}
				else
				{
					if (m_reader->readNext(buff, need_read) != need_read)
					{
						return false;
					}
					need_read = 0;
					m_alreadyRead += need_read;
				}
			}
		}

		m_started = metronomeStart(m_ticksPeriod, false);

		return m_started;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	PlayerReport* AudioPlayer::stopPlay()
	{
		if (m_started)
		{
			m_started = false;

			int time = 0;
			while (m_work)
			{
				rtl::Thread::sleep(2);
				time += 2;

				if (time > m_ticksPeriod + 2)
				{
					break;
				}
			}

			metronomeStop();

			if (m_reader != nullptr)
			{
				m_reader->close();
			}

			rtl::String filename = (m_fileList.getCount() <= m_indexCurrentFile) ? rtl::String::empty : m_fileList.getAt(m_indexCurrentFile);
			if (filename.isEmpty())
			{
				m_report.stopTime = 0;
			}
			else
			{
				m_report.errorFiles.add(filename);
				m_report.stopTime = (m_alreadyRead == 0) ? 0 : m_alreadyRead * 1000 / m_sampleRate;
			}
		}

		return &m_report;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void AudioPlayer::metronomeTicks()
	{
		/*if (m_asyncSend)
		{
			return;
		}*/

		if (!m_started)
		{
			return;
		}

		if (m_reader == nullptr)
		{
			//raiseEndplayEvent();
			return;
		}

		if (m_handler == nullptr)
		{
			return;
		}

		m_work = true;
		{
			int need_read = m_ticksPeriod * m_sampleRate / 1000;
			if (m_stopAt != 0)
			{
				int stop_after = m_stopAt * m_sampleRate / 1000;
				if (m_alreadyRead >= stop_after)
				{
					need_read = 0;
				}
				else
				{
					int diff = stop_after - m_alreadyRead;
					if (diff < need_read)
					{
						need_read = diff;
					}
				}
			}
			if (need_read > 2048)
			{
				need_read = 2048;
			}
			short buff[2048] = { 0 };

			int samples_read = m_reader->readNext(buff, need_read);

			if (samples_read < need_read)
			{
				if (openNextFile())
				{
					m_alreadyRead = 0;
					int diff = need_read - samples_read;
					samples_read += m_reader->readNext(buff + samples_read, diff);
				}
				else
				{
					samples_read = 0;
				}
			}

			if (samples_read == 0)
			{
				m_work = false;
				raiseEndplayEvent();
				return;
			}
			else
			{
				m_alreadyRead += samples_read;

				if (m_volume > 0)
				{
					// сделаем тише.
					for (int k = 0; k < samples_read; k++)
					{
						buff[k] >>= m_volume;
					}
				}

				m_handler->eventDataReady((char*)buff, (samples_read * sizeof(short)));
			}
		}
		m_work = false;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool AudioPlayer::addFile(const char* filePath)
	{
		PLOG_CALL("m-play", "add_file %s", filePath);

		if (filePath == nullptr)
		{
			PLOG_WARNING("m-play", "add_file -> empty path!");
			return false;
		}

		if (!fs_is_file_exist(filePath))
		{
			PLOG_WARNING("m-play", "add_file -> file does not exist!");
			return false;
		}

		m_fileList.add(filePath);

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool AudioPlayer::addDir(const char* dir)
	{
		PLOG_CALL("m-play", "add_dir %s", dir);

		if (dir == nullptr)
		{
			PLOG_WARNING("m-play", "add_dir -> empty path!");
			return false;
		}

		if (!fs_directory_exists(dir))
		{
			PLOG_WARNING("m-play", "add_file -> directory does not exist!");
			return false;
		}

		rtl::ArrayT<fs_entry_info_t> fileList;
		rtl::String mask(dir);
		if (mask.indexOf("*") == BAD_INDEX)
		{
			if (mask[mask.getLength() - 1] != FS_PATH_DELIMITER)
			{
				mask.append(FS_PATH_DELIMITER);
				mask.append("*");
			}
		}

		fs_directory_get_entries(mask, fileList);

		PLOG_CALL("m-play", "dir entries %d", fileList.getCount());

		for (int i = 0; i < fileList.getCount(); i++)
		{
			fs_entry_info_t& fsEntry = fileList[i];

			PLOG_CALL("m-play", "dir entrie %s", fsEntry.fs_filename);

			if (fsEntry.fs_directory)
			{
				continue;
			}

			rtl::String fullPath(dir);
			if ((fullPath[fullPath.getLength() - 1] != '\\') && (fullPath[fullPath.getLength() - 1] != '/'))
			{
				fullPath << FS_PATH_DELIMITER;
			}
			fullPath << fsEntry.fs_filename;

			if (!addFile(fullPath))
			{
				return false;
			}
		}

		PLOG_CALL("m-play", "add_dir %d files added", m_fileList.getCount());

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void AudioPlayer::setVolume(int volume)
	{
		m_volume = volume;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool AudioPlayer::openFile(int index)
	{
		if (m_reader != nullptr)
		{
			m_reader->close();
			DELETEO(m_reader);
			m_reader = nullptr;
		}

		if (index < 0)
		{
			return false;
		}

		if (index >= m_fileList.getCount())
		{
			return false;
		}

		rtl::String filePath = m_fileList.getAt(index);
		if (filePath.isEmpty())
		{
			return false;
		}

		m_reader = AudioReader::create(filePath, m_sampleRate, m_log);
		if (m_reader == nullptr)
		{
			if (!m_loop)
			{
				m_report.errorFiles.add(filePath);
			}
			return false;
		}

		return true;
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool AudioPlayer::openNextFile()
	{
		if (m_reader != nullptr)
		{
			m_reader->close();
			DELETEO(m_reader);
			m_reader = nullptr;
		}

		if (m_random)
		{
			if (!m_loop)
			{
				return false;
			}

			return openRandomFile();
		}

		m_indexCurrentFile++;
		if (m_indexCurrentFile >= m_fileList.getCount())
		{
			if (!m_loop)
			{
				return false;
			}

			m_indexCurrentFile = 0;
		}

		return openFile(m_indexCurrentFile);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	bool AudioPlayer::openRandomFile()
	{
		if (m_reader != nullptr)
		{
			m_reader->close();
			DELETEO(m_reader);
			m_reader = nullptr;
		}

		m_indexCurrentFile = GenerateRandom(0, m_fileList.getCount() - 1);

		return openFile(m_indexCurrentFile);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	void AudioPlayer::async_handle_call(void* userData, uint16_t callId, uintptr_t intParam, void* ptrParam)
	{
		IPlayerEventHandler* handler = (IPlayerEventHandler*)userData;

		try
		{
			if (callId == AudioPlayerStopEvent && handler != nullptr)
			{
				PlayerReport* report = (PlayerReport*)ptrParam;
				handler->eventEndPlay(report);
				DELETEO(report);
			}
		}
		catch (rtl::Exception& ex)
		{
			LOG_ERROR("m-play", "EndPlay event raising failed! %s", ex.get_message());
		}
	}
	void AudioPlayer::raiseEndplayEvent()
	{
		PlayerReport* report = NEW PlayerReport;
		report->stopTime = 0; // (m_alreadyRead == 0) ? 0 : m_alreadyRead * 1000 / m_sampleRate;
		rtl::async_call_manager_t::callAsync(async_handle_call, m_handler, AudioPlayerStopEvent, 0, report);
	}
}
//------------------------------------------------------------------------------
