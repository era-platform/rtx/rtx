﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_audio_mixer.h"
#include "mmt_audio_reader.h"
#include "mmt_audio_reader_wave.h"
#include "mmt_audio_reader_lrtp.h"
#include "mmt_audio_reader_rtp.h"
#include "mmt_audio_reader_mp3.h"
//--------------------------------------
//
//--------------------------------------
#define CVT_CACHE_SIZE			(1024 * 64)
#define RESAMPLER_CACHE_SIZE	(1024 * 64)
#define RESAMPLER_CACHE_MIN		(1024 * 4)
//--------------------------------------
//
//--------------------------------------
static bool	is_mp3_file(const char* filePath);

namespace media
{
	static bool linearReaderType = false;
	//--------------------------------------
	//
	//--------------------------------------
	AudioReader::AudioReader(rtl::Logger* log) :
		m_eof(true), m_readingStarted(false),
		m_GTTStartTime(0),
		m_log(log),
		m_outputFrequency(8000),
		m_sourceFrequency(8000),
		m_sourceChannels(1),
		// resampler
		m_resampler(nullptr),
		m_resamplerCache(nullptr),
		m_resamplerCachePos(0),
		m_resamplerCacheLength(0),
		m_resamplerOutput(nullptr),
		m_resamplerOutputSize(0),
		m_resamplerOutputPos(0),
		// channels converter
		m_cvtMono(false),
		m_cvtPCM(nullptr)
	{
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_MMT_API AudioReader::~AudioReader()
	{
		resetReader();
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_MMT_API void AudioReader::close()
	{
		closeSource();

		resetReader();
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_MMT_API bool AudioReader::setupReader(int clock_rate)
	{
//		PLOG_WRITE("Reader", "Setup media reader - out frequency is %d", clock_rate);

		resetReader();

		// если уже началось или завершилось чтение то на выход
		if (m_eof || m_readingStarted)
		{
			//PLOG_WRITE("Reader", "The reader already started");
			return false;
		}

		m_outputFrequency = clock_rate;

		// перестраиваем трансформацию исходного PCM в PCM пользователя
		if (m_sourceChannels == 2)
		{
			//PLOG_WRITE("Reader", "make stereo to mono converter");

			// converter mono/stereo
			m_cvtMono = true;

			m_cvtPCM = (short*)MALLOC(CVT_CACHE_SIZE * sizeof(short));
			m_cvtPCMRead = 0;
			m_cvtPCMPos = 0;
		}
		else
		{
			m_cvtMono = false;
		}

		if (m_outputFrequency != m_sourceFrequency)
		{
			//PLOG_WRITE("Reader", "make frequency rsampler");
			// resampler
			m_resampler = createResampler(m_sourceFrequency, 1, m_outputFrequency, 1);
			m_resamplerCache = (short*)MALLOC(RESAMPLER_CACHE_SIZE * sizeof(short));
			m_resamplerCachePos = m_resamplerCacheLength = 0;
			m_resamplerOutput = (short*)MALLOC(RESAMPLER_CACHE_SIZE * sizeof(short));
			m_resamplerOutputSize = m_resamplerOutputPos = 0;
		}

		m_statSamples = 0;
		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int AudioReader::readNext(short* buffer, int size)
	{
		//PLOG_WRITE("Reader", "read next data buffer_size %d bytes...", size);

		int read = readResampled(buffer, size);

		//PLOG_WRITE("Reader", "read %d bytes", read);

		return read;
	}
	//--------------------------------------
	// функция определения типа источника данных и открытия
	//--------------------------------------
	AudioReader* AudioReader::create(const char* path, rtl::Logger* log)
	{
		AudioReader* source = nullptr;

		// проверка на mp3
		if (is_mp3_file(path))
		{
			source = NEW AudioReaderMP3(log);
		}
		else
		{
			// откроем файл
			rtl::FileStream file;

			if (!file.open(path))
			{
				if (log)
					log->log_error("m-rdr", "Can't open file '%s' last error %d", path, errno);
				return nullptr;
			}

			// считаем первый блок
			uint32_t riff, read;

			//AudioReader* source = nullptr;

			if ((read = (uint32_t)file.read(&riff, sizeof(uint32_t))) != sizeof(uint32_t))
			{
				if (log)
					log->log_error("m-rdr", "Can't read file '%s' last error %d", path, errno);
			}
			else if (riff == MG_MAKEFOURCC('R', 'I', 'F', 'F'))
			{
				source = NEW AudioReaderWave(log);
			}
			else if (riff == MG_REC_AUDIO_SIGNATURE)
			{
				if (linearReaderType)
					source = NEW AudioReaderLinearRTP(log);
				else
					source = NEW AudioReaderRTP(log);
			}

			file.close();
		}

		if (source != nullptr)
		{
			if (!source->open(path))
			{
				source->close();
				DELETEO(source);
				source = nullptr;
			}
		}

		return source;
	}
	//--------------------------------------
	// функция определения типа источника данных и открытия
	//--------------------------------------
	AudioReader* AudioReader::create(const char* path, int freq, rtl::Logger* log)
	{
		AudioReader* source = create(path, log);

		if (source != nullptr)
		{
			source->setupReader(freq);
		}

		return source;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool AudioReader::setRTPReaderType(bool linear)
	{
		bool prevValue = linearReaderType;
		linearReaderType = linear;
		return prevValue;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool AudioReader::open(const char* path)
	{
		m_filePath = path;
		m_GTTStartTime = 0;

		// должны быть выставленны m_src_xxx поля
		if (!openSource(path))
			return false;

		m_eof = false;
		m_readingStarted = false;

		return setupReader(m_sourceFrequency);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void AudioReader::resetReader()
	{
		//PLOG_WRITE("Reader", "resetting parameters");

		if (m_resampler != nullptr)
		{
			destroyResampler(m_resampler);
			m_resampler = nullptr;
		}

		if (m_resamplerCache != nullptr)
		{
			FREE(m_resamplerCache);
			m_resamplerCache = nullptr;
			m_resamplerCacheLength = m_resamplerCachePos = 0;
		}

		if (m_resamplerOutput != nullptr)
		{
			FREE(m_resamplerOutput);
			m_resamplerOutput = nullptr;
			m_resamplerOutputSize = m_resamplerOutputPos = 0;
		}

		if (m_cvtPCM != nullptr)
		{
			FREE(m_cvtPCM);
			m_cvtPCM = nullptr;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	int AudioReader::readResampled(short* buffer, int size)
	{
		if (m_resampler == nullptr)
		{
			return readConverted(buffer, size);
		}

		int written = 0;

		while (written < size)
		{
			if (m_resamplerOutputSize - m_resamplerOutputPos < RESAMPLER_CACHE_MIN)
			{
				if (!fillResamplerCache())
					break;
			}

			// записываем в пользовательский буфер
			int to_copy = MIN(m_resamplerOutputSize - m_resamplerOutputPos, size - written);

			memcpy(buffer + written, m_resamplerOutput + m_resamplerOutputPos, to_copy * sizeof(short));

			written += to_copy;
			m_resamplerOutputPos += to_copy;
		}

		return written;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool AudioReader::fillResamplerCache()
	{
		// сначала готовим буфер


		if (m_resamplerOutputPos > 0 && m_resamplerOutputPos < m_resamplerOutputSize)
		{
			// если есть остатки готовых семплов то копируем в начало
			memmove(m_resamplerOutput, m_resamplerOutput + m_resamplerOutputPos, (m_resamplerOutputSize - m_resamplerOutputPos) * sizeof(short));
			m_resamplerOutputSize -= m_resamplerOutputPos;
			m_resamplerOutputPos = 0;
		}
		else
		{
			// если все считанно до конца то нужно привести в исходное состояние
			m_resamplerOutputPos = 0;
			m_resamplerOutputSize = 0;
		}

		if (m_resamplerCachePos > 0 && m_resamplerCachePos < m_resamplerCacheLength)
		{
			// если есть остатки сырых семплов то копируем в начало
			memmove(m_resamplerCache, m_resamplerCache + m_resamplerCachePos, (m_resamplerCacheLength - m_resamplerCachePos) * sizeof(short));
			m_resamplerCacheLength -= m_resamplerCachePos;
			m_resamplerCachePos = 0;
		}
		else
		{
			// если все считанно до конца то нужно привести в исходное состояние
			m_resamplerCacheLength = 0;
			m_resamplerCachePos = 0;
		}

		m_resamplerCacheLength += readConverted(m_resamplerCache + m_resamplerCacheLength, RESAMPLER_CACHE_SIZE - m_resamplerCacheLength);

		if (m_resamplerCacheLength == 0)
		{
			return m_resamplerOutputSize > 0;
		}

		int src_used = 0;
		int length = m_resampler->resample(
			m_resamplerCache,
			m_resamplerCacheLength,
			&src_used,
			m_resamplerOutput + m_resamplerOutputSize, RESAMPLER_CACHE_SIZE - m_resamplerOutputSize);

		if (length == 0)
		{
			return m_resamplerOutputSize > 0;
		}

		m_resamplerOutputSize += length;
		m_resamplerCachePos += src_used;

		return m_resamplerOutputSize > 0;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int AudioReader::readConverted(short* buffer, int size)
	{
		if (!m_cvtMono)
		{
			return readRawPCM(buffer, size);
		}

		// преобразуем каналы потока

		int written = 0;

		while (written < size)
		{
			if (m_cvtPCMPos == m_cvtPCMRead)
			{
				if ((m_cvtPCMRead = readRawPCM(m_cvtPCM, CVT_CACHE_SIZE)) == 0)
				{
					break;
				}
				m_cvtPCMPos = 0;
			}

			int to_cvt = MIN((size - written), (m_cvtPCMRead - m_cvtPCMPos) / 2);

			mixer__stereo_to_mono_i(m_cvtPCM + m_cvtPCMPos, buffer + written, to_cvt);

			m_cvtPCMPos += to_cvt * 2;
			written += to_cvt;
		}

		return written;
	}
}
//--------------------------------------
//
//--------------------------------------
static bool	is_mp3_file(const char* filePath)
{
	const char* cp = strrchr(filePath, '.');
	return cp != nullptr && std_stricmp(cp, ".mp3") == 0;
}
//--------------------------------------
