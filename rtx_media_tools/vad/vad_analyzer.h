﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_media_tools.h"

namespace media
{
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	struct VAD_Analyzer
	{
		RTX_MMT_API virtual ~VAD_Analyzer();

		virtual bool create(int samplerate, int threshold, int vadDuration, int silenceDuration) = 0;
		virtual void destroy() = 0;
		virtual bool process(int samplesLeft, bool& vad, int& msec) = 0;
		virtual bool process(const short* samples, int length, bool& vad, int& msec) = 0;

		RTX_MMT_API static VAD_Analyzer* createInstance(const char* engineName, rtl::Logger* log);
		RTX_MMT_API static void deleteInstance(VAD_Analyzer* engine);
	};
}
//-----------------------------------------------
