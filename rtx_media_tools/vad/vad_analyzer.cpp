﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "vad_analyzer.h"
#include "spandsp.h"
#include "spandsp/power_meter.h"

#define MLOG_VAD if (m_log != nullptr && rtl::Logger::check_trace(TRF_VAD)) m_log->log


namespace media
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VAD_Analyzer::~VAD_Analyzer()
	{
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	class VAD_AnalyzerMax : public VAD_Analyzer
	{
		double m_threshold;
		rtl::Logger* m_log;

	public:
		VAD_AnalyzerMax(rtl::Logger* log);
		virtual ~VAD_AnalyzerMax();

		virtual bool create(int frequesncy, int threshold, int vad_duration, int silence_duration) override;
		virtual void destroy() override;
		virtual bool process(int samples_left, bool& vad, int& msec) override;
		virtual bool process(const short* samples, int length, bool& vad, int& msec) override;
	};
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	class VAD_AnalyzerSpandsp : public VAD_Analyzer
	{
		::power_surge_detector_state_t m_core;
		int m_state;
		rtl::Logger* m_log;

		bool m_prev_signal_present;
		int m_last_silence_offset;
		int m_last_vad_offset;
		int m_last_evt_offset;
		int m_samples;

		int m_frequency;
		int m_channels;
		int m_threshold;

		int m_vad_duration_s;
		int m_silence_duration_s;

	public:
		VAD_AnalyzerSpandsp(rtl::Logger* log);
		virtual ~VAD_AnalyzerSpandsp();

		virtual bool create(int frequency, int threshold, int vad_duration, int silence_duration) override;
		virtual void destroy() override;
		virtual bool process(int samples_left, bool& vad, int& msec) override;
		virtual bool process(const short* samples, int length, bool& vad, int& msec) override;
	};
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	RTX_MMT_API VAD_Analyzer* VAD_Analyzer::createInstance(const char* engine_name, rtl::Logger* log)
	{
		if (strcmp(engine_name, "maxval") == 0)
		{
			return NEW VAD_AnalyzerMax(log);
		}
		else if (strcmp(engine_name, "spandsp") == 0)
		{
			return NEW VAD_AnalyzerSpandsp(log);
		}

		return nullptr;
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	RTX_MMT_API void VAD_Analyzer::deleteInstance(VAD_Analyzer* engine)
	{
		DELETEO(engine);
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	VAD_AnalyzerMax::VAD_AnalyzerMax(rtl::Logger* log) : m_log(log), m_threshold(0.0)
	{
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VAD_AnalyzerMax::~VAD_AnalyzerMax()
	{
		destroy();
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool VAD_AnalyzerMax::create(int samplerate, int threshold, int vad_duration, int silence_duration)
	{
		m_threshold = threshold;

		return true;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VAD_AnalyzerMax::destroy()
	{
		m_threshold = 0.0;
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	bool VAD_AnalyzerMax::process(int samples_left, bool& vad, int& msec)
	{
		return false;
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	bool VAD_AnalyzerMax::process(const short* samples, int length, bool& vad, int& msec)
	{
		//double max = 0.0;

		//for (int i = 0; i < length; i++)
		//{
		//	double value = (double)*(samples + i);

		//	if (fabs(value) > fabs(max))
		//	{
		//		max = value;
		//	}
		//}

		return false;// max;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VAD_AnalyzerSpandsp::VAD_AnalyzerSpandsp(rtl::Logger* log) : m_log(log)
	{
		memset(&m_core, 0, sizeof(m_core));

		m_prev_signal_present = false;
		m_last_silence_offset = 0;
		m_last_vad_offset = 0;
		m_last_evt_offset = 0;
		m_samples = 0;
		m_state = 0;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VAD_AnalyzerSpandsp::~VAD_AnalyzerSpandsp()
	{
		power_surge_detector_release(&m_core);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------

#define SAMPLES_OF(duration, freq) (duration * (freq/1000))
#define SAMPLES_MS(freq) (freq/1000)

	bool VAD_AnalyzerSpandsp::create(int frequency, int threshold, int vad_duration, int silence_duration)
	{
		// theare must be used threshold
		power_surge_detector_init(&m_core, -((float)threshold), 6.0f);

		// internal; states
		m_prev_signal_present = false;
		m_last_silence_offset = 0;
		m_last_vad_offset = 0;
		m_last_evt_offset = 0;
		m_samples = 0;

		// parameters
		m_frequency = frequency;
		m_threshold = threshold;
		m_vad_duration_s = SAMPLES_OF(vad_duration, frequency);
		m_silence_duration_s = SAMPLES_OF(silence_duration, frequency);

		MLOG_VAD("span-vad", "create for freq:%d threshold:%d vad:%d ms silence: %d ms", frequency, threshold, vad_duration, silence_duration);

		return true;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VAD_AnalyzerSpandsp::destroy()
	{
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	bool VAD_AnalyzerSpandsp::process(int samples_left, bool& vad, int& msec)
	{
		bool has_event = false;

		MLOG_VAD("span-vad", "....cn received. samples_left:%d", samples_left);

		int signal_offset = m_samples + samples_left;
		int signal_level = 0;
		bool signal_present = (signal_level != 0);

		if (m_prev_signal_present != signal_present)
		{
			float prev = (float)(signal_offset - m_last_evt_offset) / (float)m_frequency;

			MLOG_VAD("span-vad", "..Off by CN signal at %7.3f | %7.3f", (float)signal_offset / (float)m_frequency, prev);

			m_last_silence_offset = m_last_evt_offset = signal_offset;
			m_last_vad_offset = 0;
			m_prev_signal_present = signal_present;
		}

		if (m_last_silence_offset)
		{
			int offset = signal_offset - m_last_silence_offset;
			if (offset >= m_silence_duration_s && m_state) // 400 ms
			{
				m_last_silence_offset = 0;
				m_state = 0;
				//m_last_vad_offset = 0;

				MLOG_VAD("span-vad", "--------> CN -> VAD down %7.3f", (float)offset / m_frequency); // seconds
				vad = false;
				msec = offset / 8; // mseconds
				has_event = true;
			}
		}

		m_samples += samples_left;

		return has_event;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool VAD_AnalyzerSpandsp::process(const short* samples, int length, bool& vad, int& msec)
	{
		bool has_event = false;

		//	MLOG_VAD("span-vad", "....data received. length:%d", length);

		for (int i = 0; i < length; i++)
		{
			int signal_offset = m_samples + i;

			int signal_level = power_surge_detector(&m_core, samples[i]);
			bool signal_present = (signal_level != 0);

			if (m_prev_signal_present != signal_present)
			{
				float signal_power = power_surge_detector_current_dbm0(&m_core);

				if (signal_present)
				{
					float prev = (float)(signal_offset - m_last_evt_offset) / (float)m_frequency;
					MLOG_VAD("span-vad", "..On  at %7.3f | %7.3f (%6d -> %9d -> %03.3f dBm0)",
						(float)signal_offset / (float)m_frequency, prev, samples[i], signal_level, signal_power);
					m_last_vad_offset = m_last_evt_offset = signal_offset;
					m_last_silence_offset = 0;
				}
				else
				{
					float prev = (float)(signal_offset - m_last_evt_offset) / (float)m_frequency;
					MLOG_VAD("span-vad", "..Off at %7.3f | %7.3f (%6d -> %9d -> %03.3f dBm0)",
						(float)signal_offset / (float)m_frequency, prev, samples[i], signal_level, signal_power);

					m_last_silence_offset = m_last_evt_offset = signal_offset;
					m_last_vad_offset = 0;
				}

				m_prev_signal_present = signal_present;
			}

			if (m_last_silence_offset)
			{
				int offset = signal_offset - m_last_silence_offset;
				if (offset >= m_silence_duration_s && m_state) // 400 ms
				{
					m_last_silence_offset = 0;
					m_state = 0;
					//m_last_vad_offset = 0;

					MLOG_VAD("span-vad", "--------> SL -> VAD down %7.3f", (float)offset / m_frequency); // seconds
					vad = false;
					msec = offset / 8; // mseconds
					has_event = true;
				}
			}

			if (m_last_vad_offset)
			{
				int offset = signal_offset - m_last_vad_offset;
				if (offset >= m_vad_duration_s && !m_state) // 50 ms
				{
					m_last_vad_offset = 0;
					m_state = 1;
					//m_last_silence_offset = 0;

					MLOG_VAD("span-vad", "--------> VAD up %7.3f", offset / 8000.0);
					vad = true;
					msec = offset / 8;
					has_event = true;
				}
			}
		}

		m_samples += length;

		return has_event;
	}
}
//-----------------------------------------------

