﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_file_wave.h"
#include "mmt_lazy_writer.h"
//--------------------------------------
//
//--------------------------------------
#define RIFF_TAG MG_MAKEFOURCC('R', 'I', 'F', 'F')
#define WAVE_TAG MG_MAKEFOURCC('W', 'A', 'V', 'E')
#define FMT_TAG MG_MAKEFOURCC('f', 'm', 't', ' ')
#define DATA_TAG MG_MAKEFOURCC('d', 'a', 't', 'a')
#define WAVE_FORMAT_SIZE	16
#define WAVE_FORMAT_EX_SIZE	18
#define WAVE_CACHE_SIZE (128 * 1024)
//--------------------------------------
//
//--------------------------------------
struct wave_file_header_t
{
	uint32_t       riff_tag;
	uint32_t       file_size;
	uint32_t       wave_tag;
	uint32_t       format_tag;
	uint32_t       format_size;
};

namespace media
{
	//--------------------------------------
	//
	//--------------------------------------
	FileReaderWAV::FileReaderWAV() :
		m_format(nullptr), m_formatExtra(nullptr),
		m_audioBuffer(nullptr), m_audioBufferPtr(0), m_audioBufferWritten(0),
		m_audioDataBegin(0), m_audioRead(0), m_audioLength(0)
	{
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool FileReaderWAV::eof()
	{
		return m_audioRead == m_audioLength;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool FileReaderWAV::open(const char* path)
	{
		close();

		if (!m_file.open(path))
		{
			return false;
		}

		wave_file_header_t header = { 0 };

		size_t size = m_file.read(&header, sizeof header);

		if (size != sizeof(header))
		{
			close();
			return false;
		}

		if (header.riff_tag != RIFF_TAG)
		{
			close();
			return false;
		}

		if (header.wave_tag != WAVE_TAG)
		{
			close();
			return false;
		}

		if (header.format_tag != FMT_TAG)
		{
			close();
			return false;
		}

		if (header.format_size < WAVE_FORMAT_SIZE)
		{
			close();
			return false;
		}

		m_format = (WAVFormat*)MALLOC(header.format_size);

		size = m_file.read(m_format, header.format_size);

		if (size != header.format_size)
		{
			close();
			return false;
		}

		if (size > WAVE_FORMAT_SIZE)
		{
			m_formatExtra = ((uint8_t*)m_format) + WAVE_FORMAT_SIZE + m_format->extraSize;
		}
		else
		{
			m_formatExtra = nullptr;
		}

		uint32_t next_block[2] = { 0, 0 };
		uint32_t __data_tag_ = DATA_TAG;

		do
		{
			if (next_block != 0)
			{
				m_file.seekCurrent(next_block[1]);
			}

			size = m_file.read(next_block, 2 * sizeof(uint32_t));

			if (size < sizeof(uint32_t))
			{
				close();
				return false;
			}
		} while (next_block[0] != __data_tag_);

		m_audioLength = next_block[1];

		if (m_audioLength == 0)
		{
			close();
			return false;
		}

		m_audioDataBegin = (uint32_t)m_file.getPosition();

		m_audioBuffer = (uint8_t*)MALLOC(WAVE_CACHE_SIZE);

		fillCache();

		m_audioRead = 0;

		return m_audioBufferWritten > 0;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void FileReaderWAV::close()
	{
		m_file.close();

		if (m_format != nullptr)
		{
			FREE(m_format);
			m_format = nullptr;
			m_formatExtra = nullptr;
		}

		if (m_audioBuffer != nullptr)
		{
			FREE(m_audioBuffer);
			m_audioBuffer = nullptr;
			m_audioBufferPtr = 0;
			m_audioBufferWritten = 0;
			m_audioRead = 0;
			m_audioLength = 0;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void FileReaderWAV::reset()
	{
		m_audioRead = 0;
		m_audioBufferPtr = 0;
		m_audioBufferWritten = 0;

		m_file.setPosition(m_audioDataBegin);

		fillCache();
	}
	//--------------------------------------
	//
	//--------------------------------------
	int FileReaderWAV::readAudioData(void* buffer, int size)
	{
		// проверим сколько сможем считать!
		if (m_audioRead + size > m_audioLength)
		{
			size = m_audioLength - m_audioRead;
		}

		if (size == 0)
			return 0;

		int copied = 0;
		uint8_t* audio_buffer = (uint8_t*)buffer;
		int normalized_size;

		if (m_format->blockAlign == 1)
			normalized_size = size;
		else if (m_format->blockAlign == 2)
			normalized_size = size & 0xFFFFFFFE;
		else
			normalized_size = (size / m_format->blockAlign) * m_format->blockAlign;

		int normalized_cache_size;
		if (m_format->blockAlign < 3)
			normalized_cache_size = WAVE_CACHE_SIZE;
		else
			normalized_cache_size = (WAVE_CACHE_SIZE / m_format->blockAlign) * m_format->blockAlign;

		while (copied < normalized_size)
		{
			int to_copy = MIN(normalized_size, normalized_cache_size);

			int length = copyBlock(audio_buffer + copied, to_copy);

			if (length == 0)
				break;

			copied += length;
		}

		m_audioRead += copied;

		return copied;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void FileReaderWAV::fillCache()
	{
		// buffer empty or buffer fully read
		if (m_audioBufferWritten == m_audioBufferPtr)
		{
			m_audioBufferWritten = (uint32_t)m_file.read(m_audioBuffer, WAVE_CACHE_SIZE);
			m_audioBufferPtr = 0;
			return;
		}

		// buffer full & not used!
		if (m_audioBufferPtr == 0)
		{
			return;
		}

		// refill buffer
		int remain = m_audioBufferWritten - m_audioBufferPtr;
		memmove(m_audioBuffer, m_audioBuffer + m_audioBufferPtr, remain);
		uint32_t read = (uint32_t)m_file.read(m_audioBuffer + remain, m_audioBufferPtr);

		if (read < m_audioBufferPtr)
		{
			m_audioBufferWritten -= (m_audioBufferPtr - read);
		}

		m_audioBufferPtr = 0;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int FileReaderWAV::copyBlock(uint8_t* buffer, int size)
	{
		int remain = m_audioBufferWritten - m_audioBufferPtr;

		// если данных не хватает то дочитываем
		if (size > remain)
			fillCache();

		remain = m_audioBufferWritten - m_audioBufferPtr;

		if (size > remain)
			size = remain;

		memcpy(buffer, m_audioBuffer + m_audioBufferPtr, size);
		m_audioBufferPtr += size;

		return size;
	}
	//--------------------------------------
	//
	//--------------------------------------
	FileWriterWAV::FileWriterWAV(rtl::Logger* log) :
		m_fileSizeOffset(4), m_audioSizeOffset(0), m_audioLength(0), m_factSizeOffset(-1),
		m_audioBuffer(nullptr), m_audioBufferSize(0), m_audioBufferPtr(0)
	{
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool FileWriterWAV::create(const char* fileName, const WAVFormat* format)
	{
		//uint32_t written = 0;

		LOG_CALL("wav-rec", "Create recorder %s", fileName);

		close();

		if (format == nullptr)
		{
			LOG_ERROR("wav-rec", "wav format is null.");
			return false;
		}

		if (!m_file.createNew(fileName))
		{
			LOG_ERROR("wav-rec", "Can't create file : io error");
			return false;
		}

		wave_file_header_t header;
		header.riff_tag = RIFF_TAG;
		header.file_size = 0;
		header.wave_tag = WAVE_TAG;
		header.format_tag = FMT_TAG;
		header.format_size = WAVE_FORMAT_SIZE + (format->extraSize == 0 ? 0 : format->extraSize + sizeof(uint16_t));

		size_t size = m_file.write(&header, sizeof header);

		if (size != sizeof header)
		{
			close();
			return false;
		}

		size = m_file.write(format, header.format_size);

		if (size != header.format_size)
		{
			close();
			return false;
		}

		if (format->tag ==WAVFormatTagGSM610)
		{
			uint32_t fact = MG_MAKEFOURCC('f', 'a', 'c', 't');
			size = m_file.write(&fact, sizeof(uint32_t));
			if (size != sizeof(uint32_t))
			{
				close();
				return false;
			}
			fact = 4;
			size = m_file.write(&fact, sizeof(uint32_t));
			if (size != sizeof(uint32_t))
			{
				close();
				return false;
			}

			m_factSizeOffset = (uint32_t)m_file.getPosition();
			fact = 0;
			size = m_file.write(&fact, sizeof(uint32_t));
			if (size != sizeof(uint32_t))
			{
				close();
				return false;
			}
		}

		uint32_t data_tag = DATA_TAG;
		m_audioLength = 0; // cbData
		size = m_file.write(&data_tag, sizeof(uint32_t));
		if (size != sizeof(uint32_t))
		{
			close();
			return false;
		}

		m_audioSizeOffset = (uint32_t)m_file.getPosition();

		size = m_file.write(&m_audioLength, sizeof(uint32_t));
		if (size != sizeof(uint32_t))
		{
			close();
			return false;
		}

		m_audioBufferSize = (WAVE_CACHE_SIZE / format->blockAlign) * format->blockAlign;

		m_audioBuffer = (uint8_t*)MALLOC(m_audioBufferSize);

		m_audioBufferPtr = 0;

		LOG_CALL("wav-rec", "Recorder created");

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void FileWriterWAV::close()
	{
		if (m_file.isOpened())
		{
			flush();
			m_file.close();
		}

		if (m_audioBuffer != nullptr)
		{
			FREE(m_audioBuffer);
			m_audioBuffer = nullptr;
			m_audioBufferPtr = 0;
		}

		m_audioLength = 0;
		m_audioSizeOffset = 0;
		m_fileSizeOffset = 4;
	}
	//--------------------------------------
	// сброс кеша на диск
	//--------------------------------------
	bool FileWriterWAV::flush()
	{
		uint32_t written = 0;

		if (m_audioBufferPtr > 0)
		{
			written = (uint32_t)m_file.write(m_audioBuffer, m_audioBufferPtr);

			if (written < m_audioBufferPtr)
			{
				memmove(m_audioBuffer, m_audioBuffer + written, m_audioBufferPtr - written);
			}

			m_audioBufferPtr -= written;
		}

		if (written == 0)
		{
			return true;
		}

		m_audioLength += written;

		// запишем длину файла и длину данных в файл
		size_t len = m_file.getLength() - 8;
		m_file.setPosition(m_fileSizeOffset);
		if (m_file.write(&len, sizeof(uint32_t)) == 0)
		{
			LOG_ERROR("wav-rec", "Flush failed with error %d", std_get_error);
			return false;
		}

		m_file.setPosition(m_audioSizeOffset);
		if (m_file.write(&m_audioLength, sizeof(uint32_t)) == 0)
		{
			LOG_ERROR("wav-rec", "Flush failed with error %d", std_get_error);
			return false;
		}

		if (m_factSizeOffset != (uint32_t)(-1))
		{
			m_file.setPosition(m_factSizeOffset);
			uint32_t fact_len = (m_audioLength / 65) * 320;
			if (m_file.write(&fact_len, sizeof(uint32_t)) == 0)
			{
				LOG_ERROR("wav-rec", "Flush failed with error %d", std_get_error);
				return false;
			}
		}
		// востановим указатель файла
		m_file.seekEnd(0);

		return true;
	}
	//--------------------------------------
	// запись медийных данных любого размера
	//--------------------------------------
	int FileWriterWAV::write(const void* data, int size)
	{
		int written = 0;
		const uint8_t* audio_data = (const uint8_t*)data;

		if (!m_file.isOpened() || data == nullptr || size <= 0)
		{
			return 0;
		}

		while (written < size)
		{
			int length = writeBlock(audio_data + written, size - written);

			if (length == 0)
			{
				break;
			}

			written += length;
		}

		return written;
	}
	//--------------------------------------
	// запись блоков которые не превышают размер кеша
	//--------------------------------------
	int FileWriterWAV::writeBlock(const uint8_t* audio_data, int length)
	{
		if (m_audioBufferPtr + length > m_audioBufferSize)
		{
			if (!flush())
			{
				return 0;
			}
		}

		int to_copy = MIN(length, int(m_audioBufferSize - m_audioBufferPtr));

		memcpy(m_audioBuffer + m_audioBufferPtr, audio_data, to_copy);
		m_audioBufferPtr += to_copy;

		if (m_audioBufferPtr == m_audioBufferSize)
		{
			flush();
		}

		return to_copy;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_MMT_API WAVFormat* getWAVFormat(const char* encoding, int freq, int channels, const char* fmtp)
	{
		WAVFormat* format = nullptr;

		if (std_stricmp(encoding, "GSM") == 0)
		{
			if (freq != 8000 || channels != 1)
				return nullptr;

			media::WAVFormatGSM610* gsm_format = NEW media::WAVFormatGSM610;
			gsm_format->format.tag = WAVFormatTagGSM610;
			gsm_format->format.samplesRate = 8000;
			gsm_format->format.channels = 1;
			gsm_format->format.blockAlign = 65;
			gsm_format->format.bytesPerSecond = 1625;
			gsm_format->format.bitsPerSample = 0;
			gsm_format->format.extraSize = 2;
			gsm_format->samplesPerBlock = 320;

			format = &gsm_format->format;
		}
		else if (std_stricmp(encoding, "PCMA") == 0)
		{
			if (freq != 8000 || channels != 1)
				return nullptr;

			format = NEW WAVFormat;
			format->tag = WAVFormatTagALAW;
			format->samplesRate = 8000;
			format->channels = 1;
			format->blockAlign = 1;
			format->bytesPerSecond = 8000;
			format->bitsPerSample = 8;
			format->extraSize = 0;
		}
		else if (std_stricmp(encoding, "PCMU") == 0)
		{
			if (freq != 8000 || channels != 1)
				return nullptr;

			format = NEW WAVFormat;
			format->tag = WAVFormatTagULAW;
			format->samplesRate = 8000;
			format->channels = 1;
			format->blockAlign = 1;
			format->bytesPerSecond = 8000;
			format->bitsPerSample = 8;
			format->extraSize = 0;
		}
		else if (std_stricmp(encoding, "L16") == 0)
		{
			format = NEW WAVFormat;
			format->tag = WAVFormatTagPCM;
			format->samplesRate = freq;
			format->channels = channels;
			format->bitsPerSample = 16;
			format->blockAlign = 2 * channels;
			format->bytesPerSecond = freq * 2 * channels;
			format->extraSize = 0;
		}

		return format;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_MMT_API WAVFormat* getWAVFormat(const char* encoding)
	{
		WAVFormat* format = nullptr;

		if (std_stricmp(encoding, "GSM") == 0)
		{
			media::WAVFormatGSM610* gsm_format = NEW media::WAVFormatGSM610;
			gsm_format->format.tag = WAVFormatTagGSM610;
			gsm_format->format.samplesRate = 8000;
			gsm_format->format.channels = 1;
			gsm_format->format.blockAlign = 65;
			gsm_format->format.bytesPerSecond = 1625;
			gsm_format->format.bitsPerSample = 0;
			gsm_format->format.extraSize = 2;
			gsm_format->samplesPerBlock = 320;

			format = &gsm_format->format;
		}
		else if (std_stricmp(encoding, "PCMA") == 0)
		{
			format = NEW WAVFormat;
			format->tag = WAVFormatTagALAW;
			format->samplesRate = 8000;
			format->channels = 1;
			format->blockAlign = 1;
			format->bytesPerSecond = 8000;
			format->bitsPerSample = 8;
			format->extraSize = 0;
		}
		else if (std_stricmp(encoding, "PCMU") == 0)
		{
			format = NEW WAVFormat;
			format->tag = WAVFormatTagULAW;
			format->samplesRate = 8000;
			format->channels = 1;
			format->blockAlign = 1;
			format->bytesPerSecond = 8000;
			format->bitsPerSample = 8;
			format->extraSize = 0;
		}
		else if (std_stricmp(encoding, "L16") == 0)
		{
			format = NEW WAVFormat;
			format->tag = WAVFormatTagPCM;
			format->samplesRate = 8000;
			format->channels = 1;
			format->bitsPerSample = 16;
			format->blockAlign = 2;
			format->bytesPerSecond = 16000;
			format->extraSize = 0;
		}

		return format;
	}
}
//--------------------------------------
