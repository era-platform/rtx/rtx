﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_lazy_writer.h"

namespace media
{
	//--------------------------------------
   // конструктор
   //--------------------------------------
	LazyMediaWriter::LazyMediaWriter(rtl::Logger* log) : MediaWriter(log),
		m_created(false),
		m_started(false), m_write(false), m_flash(false),
		m_record_cache(nullptr), m_current_block(0), m_record_block(0)
	{
	}
	//--------------------------------------
	// десруктор
	//--------------------------------------
	LazyMediaWriter::~LazyMediaWriter()
	{
		destroy();
	}
	//--------------------------------------
	// инициализация рекордера и открытие файла записи
	//--------------------------------------
	void LazyMediaWriter::initialize()
	{
		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		if (m_created)
		{
			return;
		}

		// очистим кеш
		m_record_cache = (LazyBlock*)MALLOC(sizeof(LazyBlock) * CACHE_BLOCK_COUNT);
		memset(m_record_cache, 0, sizeof(LazyBlock) * CACHE_BLOCK_COUNT);

		// настроим на запись объявим свободными
		for (int i = 0; i < CACHE_BLOCK_COUNT; i++)
		{
			m_record_cache[i].rec_pos = m_record_cache[i].rec_buffer;
		}

		m_current_block = 0;
		m_record_block = 0;

		m_written_to_buffer = 0;
		m_lost_in_buffer = 0;
		m_written_to_file = 0;
		m_lost_in_file = 0;


		// все готово
		m_created = true;
	}
	//--------------------------------------
	// закрытие файла и освобождение буферов
	//--------------------------------------
	void LazyMediaWriter::destroy()
	{
		PLOG_CALL("rec", "destroy");

		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		if (m_created)
		{
			// остановим запись и удалим себя из менеджера отложенной записи
			if (m_started)
				stop();

			// проверим застрявшие сбросы в файл
			while (m_flash)
			{
				rtl::Thread::sleep(2);
			}

			PLOG_CALL("rec", "final flush...");
			flush(true);

			// очистим кеш
			if (m_record_cache != nullptr)
			{
				FREE(m_record_cache);
				m_record_cache = nullptr;
			}

			m_current_block = 0;
			m_record_block = 0;

			m_created = false;

			PLOG_CALL("rec", "statistics:\n\
\t\tbytes written to buffer: %zu\n\
\t\tbytes lost in buffer:    %zu\n\
\t\tbytes written to file:   %zu\n\
\t\tbytes lost in file:      %zu\n", m_written_to_buffer, m_lost_in_buffer, m_written_to_file, m_lost_in_file);

			PLOG_CALL("rec", "destroyed");
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool LazyMediaWriter::start()
	{
		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		if (m_created)
		{
			if (!m_started)
			{
				PLOG_CALL("rec", "Start recording...");

				memset(m_record_cache, 0, sizeof(LazyBlock) * CACHE_BLOCK_COUNT);

				// настроим на запись объявим свободными
				for (int i = 0; i < CACHE_BLOCK_COUNT; i++)
				{
					m_record_cache[i].rec_pos = m_record_cache[i].rec_buffer;
				}

				m_current_block = 0;
				m_record_block = 0;

				rec_file_started();

				startTimer();

				m_started = true;

				PLOG_CALL("rec", "Recording started");
			}
		}
		else
		{
			PLOG_WARNING("rec", "can't start - not created!");
		}

		return m_started;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void LazyMediaWriter::stop()
	{
		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		if (m_created && m_started)
		{
			PLOG_CALL("rec", "Stopping recording...");

			stopTimer();

			PLOG_CALL("rec", "Recording stopped");

			m_started = false;

			// ждем освобождения
			while (m_write)
			{
				rtl::Thread::sleep(2);
			}

			// рекордер свободен
			rec_file_stopped();
		}
	}
	//--------------------------------------
	// получение блока кеша для записи пакетов
	//--------------------------------------
	LazyBlock* LazyMediaWriter::get_current_block(int blockSize)
	{
		//PLOG_CALL("rec", "Get current block, packet %d", blockSize);

		if (!m_created || m_record_cache == nullptr)
		{
			PLOG_WARNING("rec", "Recorder NOT created(%d) or cache NOT allocated(%p)!", m_created, m_record_cache);
			return nullptr;
		}

		// проверим: переполнен ли кеш?
		if (m_record_cache[m_current_block].rec_state == CACHE_BLOCK_READY)
		{
			print_cache_statistics();
			return nullptr;
		}

		LazyBlock* current = &m_record_cache[m_current_block];
		// размер блока для записи в файл
		if (current->rec_state == CACHE_BLOCK_FREE)
		{
			current->rec_pos = current->rec_buffer;
			current->rec_length = 0;
			current->rec_start_timestamp = rtl::DateTime::getTicks();
			current->rec_state = CACHE_BLOCK_RECORD;

			return current;
		}

		uint32_t current_timestamp = rtl::DateTime::getTicks();

		// если еще есть место для записи в текущий блок?
		// и не пришло время перехода на следующий диск
		if ((current->rec_length + blockSize <= CACHE_BLOCK_SIZE) &&
			(current_timestamp - current->rec_start_timestamp) < getRecordMaxTime())
		{
			//PLOG_CALL("rec", "Return block #%d, remain %d", m_current_block, CACHE_BLOCK_SIZE - m_record_cache[m_current_block].rec_length);
			return current;
		}

		// отметим заполненный блок как готовый для записи в файл
		current->rec_state = CACHE_BLOCK_READY;
		PLOG_CALL("rec", "Move to next block...");

		// перейдем на следующий блок
		if (++m_current_block >= CACHE_BLOCK_COUNT)
			m_current_block = 0;

		current = &m_record_cache[m_current_block];
		// проверим следующий блок и если он свободен начнем запись в него
		if (current->rec_state == CACHE_BLOCK_FREE)
		{
			current->rec_pos = current->rec_buffer;
			current->rec_length = 0;
			current->rec_start_timestamp = rtl::DateTime::getTicks();
			current->rec_state = CACHE_BLOCK_RECORD;

			PLOG_CALL("rec", "Return block #%d, remain %d", m_current_block, CACHE_BLOCK_SIZE - m_record_cache[m_current_block].rec_length);
			return current;
		}

		// нет свободных блоков!
		// отменим в статистике и в логе

		print_cache_statistics();

		return nullptr;
	}
	//--------------------------------------
	// сброс буферов в файл
	//--------------------------------------
	void LazyMediaWriter::flush(bool close)
	{
		if (m_created)
		{
			m_flash = true;

			long endState = close ? CACHE_BLOCK_READY | CACHE_BLOCK_RECORD : CACHE_BLOCK_READY;

			//PLOG_CALL("rec", "Flush recorded data (end state %d)...", endState);

			LazyBlock* to_record = &m_record_cache[m_record_block];
			// сбросим на диск все готовые блоки
			while ((to_record->rec_state & endState) != 0)
			{
				PLOG_CALL("rec", "Flush cache block #%d...", m_record_block);

				uint8_t* ptr = to_record->rec_buffer;
				uint32_t len = to_record->rec_length;
				uint32_t written = 0;

				rec_file_write(ptr, len, &written);

				// отметим записанный блок как свободный для заполнения
				to_record->rec_length = 0;
				to_record->rec_pos = to_record->rec_buffer;
				to_record->rec_state = CACHE_BLOCK_FREE;

				// перейдем на следующий блок
				if (++m_record_block >= CACHE_BLOCK_COUNT)
					m_record_block = 0;

				to_record = &m_record_cache[m_record_block];
			}

			m_flash = false;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	static const char* get_rec_state_name(int state)
	{
		static const char* names[] = { "FREE", "READY", "RECORD" };

		return state >= 0 && state < int(sizeof(names) / sizeof(const char*)) ? names[state] : "UNKNOWN";
	}
	//--------------------------------------
	//
	//--------------------------------------
	void LazyMediaWriter::print_cache_statistics()
	{
		char* text = (char*)MALLOC(4000);
		memset(text, 0, 4000);

		int len = std_snprintf(text, 4000, "\t\tCACHE STATISTICS: cache block %d file block %d\n", m_current_block, m_record_block);

		for (int i = 0; i < CACHE_BLOCK_COUNT; i++)
		{
			LazyBlock& block = m_record_cache[i];
			int state = block.rec_state;
			int length = block.rec_length;
			int64_t pos = block.rec_pos - block.rec_buffer;

			len += std_snprintf(text + len, 4000 - len, "\t\t\tblock(%d) : state %s(%d) length %d pos %" PRIu64, i, get_rec_state_name(state), state, length, pos);

			if (i < CACHE_BLOCK_COUNT - 1)
			{
				text[len++] = '\r';
				text[len++] = '\n';
			}
		}

		text[len] = 0;

		PLOG_WARNING("rec", "CACHE IS FULL :\n%s", text);

		FREE(text);
	}
}
//--------------------------------------
