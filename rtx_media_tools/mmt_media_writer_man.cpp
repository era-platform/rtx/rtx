﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_media_writer_man.h"

namespace media
{
	//--------------------------------------
	// конструктор
	//--------------------------------------
	MediaWriterManager::MediaWriterManager() :
		m_timer(nullptr),
		m_timerEnter(false),
		m_timerLastTick(0),
		m_recordingMaxTime(MAX_RECORD_TIME),
		m_linkFirst(nullptr),
		m_linkLast(nullptr)
	{
	}
	//--------------------------------------
	// деструктор
	//--------------------------------------
	MediaWriterManager::~MediaWriterManager()
	{
		stop();
	}
	//--------------------------------------
	// старт манагера (таймер отключен)
	//--------------------------------------
	bool MediaWriterManager::start()
	{
		LOG_EVENT("rec-man", "Start.....enter");

		rtl::MutexLock lock(m_sync);

		if (m_timer == nullptr)
		{
			m_timerEnter = false;
			m_timer = NEW rtl::Timer(false, this, Log, "m-rec-man");
			bool res = m_timer->start(100);
			m_timerLastTick = rtl::DateTime::getTicks();

			LOG_EVENT("rec-man", "Start.....timer started %s %d", m_timer->getName(), res);
		}
		else
		{
			LOG_EVENT("rec-man", "Start.....Already started!");
		}

		LOG_EVENT("rec-man", "Start.....leave");

		return true;
	}
	//--------------------------------------
	// останов манагера
	//--------------------------------------
	void MediaWriterManager::stop()
	{
		LOG_EVENT("rec-man", "MediaWriterManager::Stop.....enter");

		rtl::MutexLock lock(m_sync);

		if (m_timer != nullptr)
		{
			m_linkFirst = m_linkLast = nullptr;
			m_timer->stop();
			DELETEO(m_timer);

			LOG_EVENT("rec-man", "MediaWriterManager::Stop.....timer stopped");

			m_timer = nullptr;
		}

		LOG_EVENT("rec-man", "MediaWriterManager::Stop.....leave");
	}
	//--------------------------------------
	// добавление рекордера (старт таймера)
	//--------------------------------------
	bool MediaWriterManager::add(MediaWriter* recorder)
	{
		LOG_EVENT("rec-man", "MediaWriterManager::Add.....enter");

		rtl::MutexLock lock(m_sync);

		if (m_linkLast == nullptr)
		{
			m_linkFirst = m_linkLast = recorder;

			start();
		}
		else
		{
			m_linkLast->m_nextLink = recorder;
			m_linkLast = recorder;
		}

		recorder->m_nextLink = nullptr;
		recorder->m_timerLastTick = rtl::DateTime::getTicks();

		LOG_EVENT("rec-man", "MediaWriterManager::Add.....leave");

		return true;
	}
	//--------------------------------------
	// отключение рекордера от таймера (останов таймера если больше нет рекордеров)
	//--------------------------------------
	void MediaWriterManager::remove(MediaWriter* recorder)
	{
		LOG_EVENT("rec-man", "MediaWriterManager::Remove.....enter.");

		rtl::MutexLock lock(m_sync);

		MediaWriter* current = m_linkFirst;
		MediaWriter* prev = nullptr;

		while (current != nullptr && current != recorder)
		{
			prev = current;
			current = current->m_nextLink;
		}

		if (current != nullptr)
		{
			if (prev != nullptr)
			{
				prev->m_nextLink = current->m_nextLink;

				// если был последним то обновим ссылку на последнего
				if (current == m_linkLast)
					m_linkLast = prev;
			}
			else
			{
				m_linkFirst = current->m_nextLink;

				// если единственный то ссылка на последнего тоже обнуляется
				if (m_linkFirst == nullptr)
				{
					m_linkLast = nullptr;
				}
			}

			LOG_EVENT("rec-man", "MediaWriterManager::Remove.....removed");
		}
		else
			LOG_EVENT("rec-man", "MediaWriterManager::Remove.....not found.");

		// ждем окончания работы таймера
		LOG_EVENT("rec-man", "MediaWriterManager::Remove.....wait timer.");
		while (m_timerEnter)
		{
			//SwitchToThread();
			rtl::Thread::sleep(5);
		}

		if (m_linkFirst == nullptr)
		{
			stop();
		}
		// else сессии нет в списке

		recorder->m_nextLink = nullptr;

		LOG_EVENT("rec-man", "MediaWriterManager::Remove.....leave");
	}
	//--------------------------------------
	// обход рекордеров
	//--------------------------------------
	void MediaWriterManager::timer_elapsed_event(rtl::Timer* timer, int tid)
	{
		m_timerEnter = true;

		uint32_t currentTime = rtl::DateTime::getTicks();

		int cycle_time = currentTime - m_timerLastTick;

		if (cycle_time > 500)
		{
			// сообщим на сколько опаздал таймер!
			LOG_WARN("rec-man", "Timer tick late for %d msec!", cycle_time - 100);
		}

		MediaWriter* current = m_linkFirst;

		while (current != nullptr)
		{
			//LOG_EVENT("rec-man", "MediaWriterManager::OnTimerTicks........duration %d current %d",
			//	currentTime - current->m_timerLastTick, current->m_timerPeriod - 10);
			// вызываем флеш с опережением на 10 мсек
			if (currentTime - current->m_timerLastTick >= current->m_timerPeriod - 10)
			{
				current->flush();
				current->m_timerLastTick = currentTime;
			}

			current = current->m_nextLink;
		}

		cycle_time = currentTime - rtl::DateTime::getTicks();

		// сообщим длительность записи если потраченное время > 500 мс
		if (cycle_time > 500)
		{
			LOG_WARN("rec-man", "Recording time is %d msec!", cycle_time);
		}

		m_timerLastTick = currentTime;
		m_timerEnter = false;
	}
}
//--------------------------------------
