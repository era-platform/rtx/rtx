/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "pcm_resampler.h"
#include "av_resampler.h"

namespace media
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	Resampler* createResampler(int in_rate, int in_channels, int out_rate, int out_channels)
	{
		if (g_rtx_pcm_resampler_counter < 0)
		{
			g_rtx_pcm_resampler_counter = rtl::res_counter_t::add("mge_pcm_resampler");
		}

		bool simple_resampler = false;

		if (in_rate > out_rate)
		{
			simple_resampler = ((in_rate / out_rate) == 2) && ((in_rate % out_rate) == 0);
		}
		else
		{
			simple_resampler = ((out_rate / in_rate) == 2) && ((out_rate % in_rate) == 0);
		}

		Resampler* resampler = nullptr;

		if (simple_resampler)
		{
			resampler = NEW pcm_resampler(&Log);
		}
		else
		{
			resampler = NEW AVResampler(&Log);
		}

		if (!resampler->init(in_rate, in_channels, out_rate, out_channels))
		{
			DELETEO(resampler);
			resampler = nullptr;
		}

		return resampler;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void destroyResampler(Resampler* resampler)
	{
		if (resampler == nullptr)
		{
			return;
		}

		resampler->dispose();

		if (resampler->isSimple())
		{
			pcm_resampler* re = (pcm_resampler*)resampler;
			DELETEO(re);
		}
		else
		{
			AVResampler* re = (AVResampler*)resampler;
			DELETEO(re);
		}
	}
}
