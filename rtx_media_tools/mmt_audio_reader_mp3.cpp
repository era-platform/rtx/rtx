/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_audio_reader_mp3.h"
#include "mmt_audio_mixer.h"

namespace media
{
	//--------------------------------------
	//
	//--------------------------------------
#define MP3_CACHE_SIZE			(1024 * 64)
//--------------------------------------
//
//--------------------------------------
	AudioReaderMP3::AudioReaderMP3(rtl::Logger* log) : AudioReader(log)
	{
		m_stereo_cvt_pos = 0;
		m_stereo_cvt_length = 0;
		m_stereo_cvt_left = nullptr;
		m_stereo_cvt_right = nullptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	AudioReaderMP3::~AudioReaderMP3()
	{
		close();
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool AudioReaderMP3::openSource(const char* path)
	{
		if (!m_file.open(path))
			return false;


		m_sourceChannels = m_file.getChannelCount();
		m_sourceFrequency = m_file.getFrequency();

		// �������� �������� ��3
		if (m_sourceChannels == 2)
		{
			m_stereo_cvt_left = (short*)MALLOC(MP3_CACHE_SIZE * sizeof(short));
			m_stereo_cvt_right = (short*)MALLOC(MP3_CACHE_SIZE * sizeof(short));
		}

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void AudioReaderMP3::closeSource()
	{
		m_file.close();

		// Decoder
		if (m_stereo_cvt_left != nullptr)
		{
			FREE(m_stereo_cvt_left);
			m_stereo_cvt_left = nullptr;
		}
		if (m_stereo_cvt_right != nullptr)
		{
			FREE(m_stereo_cvt_right);
			m_stereo_cvt_right = nullptr;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	int AudioReaderMP3::readRawPCM(short* buffer, int size)
	{
		if (m_file.getChannelCount() == 1)
			return m_file.readNext(buffer, size);

		// if mp3 is stereo convert it to interlaced

		int written = 0;

		while (written < size)
		{
			// ���� ���� ������ � ������ ����������� �� ������ ��
			int cache_length = m_stereo_cvt_length - m_stereo_cvt_pos;
			if (cache_length > 1000)
			{
				int to_copy = MIN((size - written) / 2, m_stereo_cvt_length - m_stereo_cvt_pos);

				if ((to_copy & 1) == 1)
					to_copy--; // ������ �����!

				mixer__stereo_d_to_i(m_stereo_cvt_left + m_stereo_cvt_pos, m_stereo_cvt_right + m_stereo_cvt_pos, buffer + written, to_copy);
				written += to_copy * 2;
				m_stereo_cvt_pos += to_copy;
			}
			else // �������� ����� �����������
			{
				if (!fillCache())
					break;
			}
		}

		return written;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool AudioReaderMP3::fillCache()
	{
		if (m_stereo_cvt_pos > 0 && m_stereo_cvt_length - m_stereo_cvt_pos > 0)
		{
			int to_move = m_stereo_cvt_length - m_stereo_cvt_pos;
			memmove(m_stereo_cvt_left, m_stereo_cvt_left + m_stereo_cvt_pos, to_move);
			memmove(m_stereo_cvt_right, m_stereo_cvt_right + m_stereo_cvt_pos, to_move);

			m_stereo_cvt_length -= m_stereo_cvt_pos;
			m_stereo_cvt_pos = 0;
		}

		if (m_stereo_cvt_length == m_stereo_cvt_pos)
		{
			m_stereo_cvt_length = m_stereo_cvt_pos = 0;
		}

		if (m_stereo_cvt_length < MP3_CACHE_SIZE)
		{
			int len = m_file.readNext(m_stereo_cvt_left + m_stereo_cvt_pos, m_stereo_cvt_right + m_stereo_cvt_pos, MP3_CACHE_SIZE - m_stereo_cvt_length);

			if (len == 0)
			{
				// end of file or error
				m_eof = true;
				return false;
			}
			else
			{
				m_stereo_cvt_length += len;
			}
		}

		return true;
	}
}
//--------------------------------------
