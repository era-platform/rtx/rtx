/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_buffered_writer.h"
#include "mmt_media_writer_man.h"

namespace media
{
	//--------------------------------------
	// �����������
	//--------------------------------------
	BufferedMediaWriter::BufferedMediaWriter(rtl::Logger* log) : MediaWriter(log),
		m_created(false), m_started(false)
	{
	}
	//--------------------------------------
	// ���������
	//--------------------------------------
	BufferedMediaWriter::~BufferedMediaWriter()
	{
		destroy();
	}
	//--------------------------------------
	// ������������� ��������� � �������� ����� ������
	//--------------------------------------
	void BufferedMediaWriter::initialize(int bufsize_ms)
	{
		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		if (!m_created)
		{
			// ������� ���
			m_cache.create(BUFFER_SIZE);
			// ��� ������
			m_created = true;
		}

		if (bufsize_ms < 100 || bufsize_ms > 1000)
		{
			bufsize_ms = 250;
		}

		m_written_to_buffer = 0;
		m_lost_in_buffer = 0;
		m_written_to_file = 0;
		m_lost_in_file = 0;

		setTimerPeriod(bufsize_ms);
	}
	//--------------------------------------
	// �������� ����� � ������������ �������
	//--------------------------------------
	void BufferedMediaWriter::destroy()
	{
		PLOG_CALL("rec", "destroy");

		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		if (m_created)
		{
			// ��������� ������ � ������ ���� �� ��������� ���������� ������
			if (m_started)
				stop();

			// �������� ���������� ������ � ����
			while (m_flash)
			{
				rtl::Thread::sleep(2);
			}

			PLOG_CALL("rec", "final flush...");
			flush(true);

			// ������� ���
			m_cache.reset();

			m_created = false;

			PLOG_CALL("rec", "statistics:\n\
\t\tbytes written to buffer: %zu\n\
\t\tbytes lost in buffer:    %zu\n\
\t\tbytes written to file:   %zu\n\
\t\tbytes lost in file:      %zu\n", m_written_to_buffer, m_lost_in_buffer, m_written_to_file, m_lost_in_file);

			PLOG_CALL("rec", "destroyed");
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool BufferedMediaWriter::start()
	{
		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		if (m_created)
		{
			if (!m_started)
			{
				PLOG_CALL("rec", "Start recording...");

				m_cache.reset();

				recFileStarted();

				startTimer();

				PLOG_CALL("rec", "Recording started");

				m_started = true;
			}
		}
		else
		{
			PLOG_WARNING("rec", "can't start - not created!");
		}

		return m_started;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void BufferedMediaWriter::stop()
	{
		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		if (m_created && m_started)
		{
			PLOG_CALL("rec", "Stopping recording...");

			stopTimer();

			PLOG_CALL("rec", "Recording stopped");

			m_started = false;

			// �������� ��������
			recFileStopped();
		}
	}
	//--------------------------------------
	// ����� ������� � ����
	//--------------------------------------
	void BufferedMediaWriter::flush(bool close)
	{
		if (m_created)
		{
			m_flash = true;

			//PLOG_CALL("rec", "Flush recorded data (%d bytes)...", m_cache.getWritten());

			uint8_t buffer[1024];
			int size;
			uint32_t written = 0;
			// ������� �� ���� ��� ������� �����
			while ((size = m_cache.read(buffer, 1024)) > 0)
			{
				recFileWrite(buffer, size, &written);
			}

			m_flash = false;
		}
	}
}
//--------------------------------------
