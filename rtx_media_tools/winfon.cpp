/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#ifdef TARGET_OS_WINDOWS
#include <io.h>
#include <sys\stat.h>
#endif

#include <fcntl.h>
#include "mmt_winfon.h"

namespace media
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	FontInfo::FontInfo() :
		m_header(nullptr),
		m_faceName(nullptr),
		m_deviceName(nullptr),
		m_glyphCount(0),
		m_dataLength(0),
		m_data(nullptr)
	{
		memset(m_glyphes, 0, sizeof(m_glyphes));
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	FontInfo::~FontInfo()
	{
		FREE(m_data);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool FontInfo::load(rtl::Stream* fnt)
	{
		size_t base = fnt->getPosition();
		uint16_t version = fnt->readUInt16();
		m_dataLength = fnt->readUInt32();

		fnt->setPosition(base);

		m_data = (uint8_t*)MALLOC(m_dataLength);

		if (fnt->read(m_data, m_dataLength) != m_dataLength)
		{
			return false;
		}

		m_header = (FontHeader*)m_data;

		// version checking
		if (m_header->dfVersion != 0x200 && m_header->dfVersion != 0x300)
		{
			return false;
		}

		// type checking -- must be 0 (raster from file)
		if (m_header->dfType & (FNT_TYPE_VECTOR | FNT_TYPE_MEMORY | FNT_TYPE_DEVICE))
		{
			return false;
		}

		m_header->dfCopyright[59] = 0;
		rtl::strtrim(m_header->dfCopyright, " \t\r\n");

		// version 3.x
		if (m_header->dfVersion == 0x300)
		{
			// unknown checking
			if (m_header->dfFlags & (FNT_FLAGS_ABCFIXED | FNT_FLAGS_ABCPROP | FNT_FLAGS_16COLOR | FNT_FLAGS_256COLOR | FNT_FLAGS_RGBCOLOR))
			{
				return false;
			}
		}

		m_glyphCount = (m_header->dfLastChar - m_header->dfFirstChar) + 2;
		int firstChar = m_header->dfFirstChar;

		uint16_t* glyphes = (uint16_t*)(m_data + (version == 0x200 ? FNT_HEADER_v2_LEN : FNT_HEADER_v3_LEN));

		for (int i = 0; i < m_glyphCount; i++)
		{
			GlyphInfo& glyph = m_glyphes[i + firstChar];

			glyph.width = *glyphes++;
			glyph.height = m_header->dfPixHeight;
			glyph.rowWidth = glyph.width / 8 + (glyph.width % 8 ? 1 : 0);
			uint32_t offset;
			if (version == 0x200)
			{
				offset = *glyphes++;
			}
			else
			{
				offset = *((uint32_t*)glyphes);
				glyphes += 2;
			}
			glyph.bitstream = m_data + offset;
		}

		if (m_header->dfDevice != 0)
		{
			m_deviceName = (const char*)(m_data + m_header->dfDevice);
		}

		if (m_header->dfFace != 0)
		{
			m_faceName = (const char*)(m_data + m_header->dfFace);
		}

		return true;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
#define FON_MZ_MAGIC	0x5A4D
	struct winmz_header
	{
		uint16_t magic;
		uint16_t skip[29];
		uint32_t lfanew;
	};
	//-----------------------------------------------
	//
	//-----------------------------------------------
#define FON_NE_MAGIC	0x454E
	struct winne_header
	{
		uint16_t magic;
		uint8_t skip[34];
		uint16_t resource_tab_offset;
		uint16_t rname_tab_offset;
	};
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool FontFile::load(const char* filepath)
	{
		rtl::FileStream fon;

		fon.open(m_fileName = filepath);

		if (!fon.isOpened())
		{
			return false;
		}

		int magic = fon.readUInt16();

		fon.setPosition(0);

		if (magic != 0x200 && magic != 0x300 && magic != FON_MZ_MAGIC)
		{
			fon.close();
			printf("Bad magic number. This does not appear to be a Windows FNT for FON file");
			return false;
		}

		if (magic == 0x200 || magic == 0x300)
		{
			FontInfo* fontInfo = NEW FontInfo();
			if (fontInfo->load(&fon))
			{
				m_fontList.add(fontInfo);
				return true;
			}

			DELETEO(fontInfo);
			return false;
		}

		uint32_t neoffset, recoffset, recend;

		/* Ok, we know it begins with a mz header (whatever that is) */
		/* all we care about is the offset to the ne header (whatever that is) */
		fon.setPosition(30 * sizeof(uint16_t));
		neoffset = fon.readUInt32();
		fon.setPosition(neoffset);
		magic = fon.readUInt16();
		
		if (magic != FON_NE_MAGIC)
		{
			fon.close();
			return false;
		}

		for (int i = 0; i < 34; ++i)
		{
			fon.readByte();
		}

		recoffset = neoffset + fon.readUInt16();
		recend = neoffset + fon.readUInt16();
		fon.setPosition(recoffset);
		int shift_size = fon.readUInt16();
		int font_count = 0;
			
		while (fon.getPosition() < recend)
		{
			int id, count;
			id = fon.readUInt16();
			if (id == 0)
				break;
			count = fon.readUInt16();
			if (id == 0x8008)
			{
				/* id==0x8007 seems to point to a FontFile Dir Entry which is almost */
				/*  a copy of the fntheader */
				font_count = count;
				fon.readUInt32();
				break;
			}
			fon.setPosition(4 + count * 12);
		}

		for (int i = 0; i < font_count; ++i)
		{
			/* FontDirEntries need a +4 here */
			size_t here = fon.getPosition();
			uint32_t offset = fon.readUInt16() << shift_size;
			fon.setPosition(offset);
			FontInfo* fontInfo = NEW FontInfo();

			if (!fontInfo->load(&fon))
			{
				DELETEO(fontInfo);
				fon.close();
				return false;
			}
			
			m_fontList.add(fontInfo);
			fon.setPosition(here + 12);
		}

		fon.close();

		return true;
	}
	bool FontFile::load2(const char *filename)
	{
		rtl::FileStream fon;
		int magic, i, shift_size;
		
		uint32_t neoffset, recoffset, recend;
		int font_count;

		if (!fon.open(filename))
			return false;

		magic = fon.readUInt16();

		fon.setPosition(0);

		if (magic != 0x200 && magic != 0x300 && magic != FON_MZ_MAGIC)
		{
			fon.close();
			printf("Bad magic number : This does not appear to be a Windows FNT for FON file");
			return false;
		}

		if (magic == 0x200 || magic == 0x300)
		{
			FontInfo* fontInfo = NEW FontInfo();
			if (fontInfo->load(&fon))
			{
				m_fontList.add(fontInfo);
				return true;
			}

			DELETEO(fontInfo);
			return false;
		}
		else
		{
			/* Ok, we know it begins with a mz header (whatever that is) */
			/* all we care about is the offset to the ne header (whatever that is) */
			fon.setPosition(30 * sizeof(uint16_t));
			neoffset = fon.readUInt32();
			fon.setPosition(neoffset);
			magic = fon.readUInt16();
			
			if (magic != FON_NE_MAGIC)
			{
				fon.close();
				return false;
			}

			fon.seek(rtl::SeekOrigin::Current, 34);

			recoffset = neoffset + fon.readUInt16();
			recend = neoffset + fon.readUInt16();

			fon.seek(rtl::SeekOrigin::Begin, recoffset);
			shift_size = fon.readUInt16();
			font_count = 0;
			
			while (fon.getPosition() < recend)
			{
				int id, count;
				id = fon.readUInt16();
				if (id == 0)
					break;

				count = fon.readUInt16();
				if (id == 0x8008)
				{
					/* id==0x8007 seems to point to a Font Dir Entry which is almost */
					/*  a copy of the fntheader */
					font_count = count;
					fon.readUInt32();
					break;
				}
				fon.seek(rtl::SeekOrigin::Current, 4 + count * 12);
			}
			
			for (i = 0; i < font_count; ++i)
			{
				size_t here = fon.getPosition();
				uint32_t offset = fon.readUInt16() << shift_size;
				fon.seek(rtl::SeekOrigin::Begin, offset);		/* FontDirEntries need a +4 here */
				FontInfo* fontInfo = NEW FontInfo();
				if (fontInfo->load(&fon))
				{
					m_fontList.add(fontInfo);
				}
				else
				{
					DELETEO(fontInfo);
				}

				fon.setPosition(here + 12);
			}
		}
		
		fon.close();

		return m_fontList.getCount() > 0;
	}

	//-----------------------------------------------
	//
	//-----------------------------------------------
	void FontFile::free()
	{
		for (int i = 0; i < m_fontList.getCount(); i++)
		{
			DELETEO(m_fontList[i]);
		}

		m_fileName.setEmpty();
		m_fontList.clear();
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	const char* FontFile::getFaceName() const
	{
		return m_fontList.getCount() > 0 ?
			m_fontList[0]->getFaceName() :
			nullptr;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	const char* FontFile::getDeviceName() const
	{
		return m_fontList.getCount() > 0 ?
			m_fontList[0]->getDeviceName() :
			nullptr;
	}
	//-----------------------------------------------
	// Height must be within 20% -> / 5
	//-----------------------------------------------
	const FontInfo* FontFile::getFontByHeight(int height) const
	{
		int smallerIndex = -1;
		int largerIndex = -1;
		int smaller = height - (height / 5);
		int larger = height + (height / 5);

		for (int i = 0; i < m_fontList.getCount(); i++)
		{
			const FontInfo* info = m_fontList[i];
			int pixHeight = info->getHeader()->dfPixHeight;

			if (pixHeight == height)
			{
				return info;
			}

			if (pixHeight < height && pixHeight > smaller)
			{
				smaller = pixHeight;
				smallerIndex = i;
			}
			else if (pixHeight > height && pixHeight < larger)
			{
				larger = pixHeight;
				largerIndex = i;
			}
		}

		if (larger != -1 && smaller != -1)
		{
			return ((larger - height) < (height - smaller)) ?
				m_fontList[largerIndex] :
				m_fontList[smallerIndex];
		}

		return nullptr;
	}
}
//-----------------------------------------------
