﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mmt_media_writer.h"

 //--------------------------------------
 // запись в файл
 //--------------------------------------
#define MAX_RECORD_TIME 5000

namespace media
{
	//--------------------------------------
	// класс для управления фильтрами записи в файл
	//--------------------------------------
	class MediaWriterManager : rtl::ITimerEventHandler
	{
		rtl::Mutex m_sync;						// синхронизация
		rtl::Timer* m_timer;				// таймер
		volatile bool m_timerEnter;			// признак входа в функцию таймера
		volatile uint32_t m_timerLastTick;	// время последнего тика!

		uint32_t m_recordingMaxTime;		// максимальное время записи в буфер

		MediaWriter* m_linkFirst;			// первый линк на рекордеры (для обхода при удалении или таймировании)
		MediaWriter* m_linkLast;			// последний линк на рекордеры (для добавления)

		bool start();
		void stop();

	public:
		MediaWriterManager();
		~MediaWriterManager();

		bool add(MediaWriter* recorder);
		void remove(MediaWriter* recorder);

		void set_record_max_time(uint32_t max_time) { m_recordingMaxTime = max_time; }
		uint32_t getRecordMaxTime() { return m_recordingMaxTime; }

	private:
		/// <timer_event_handler_t>
		virtual void timer_elapsed_event(rtl::Timer* timer, int tid);
	};
}
//--------------------------------------
