/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_audio_reader_lrtp.h"

namespace media
{

	//-----------------------------------------------
	//
	//-----------------------------------------------
	AudioReaderLinearRTP::AudioReaderLinearRTP(rtl::Logger* log) : AudioReader(log),
		m_newStream(true), m_work(nullptr), m_wait(nullptr), m_third_second(nullptr),
		m_pcmCache(nullptr), m_pcmLength(0), m_pcmSize(0), m_pcmUsed(0),
		m_decSize(0), m_decBuffer(nullptr)
	{
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	AudioReaderLinearRTP::~AudioReaderLinearRTP()
	{
		cleanup();
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void AudioReaderLinearRTP::initialize()
	{
		rtl::MutexLock lock(m_sync);

		m_work = NEW RTPSorter;
		memset(m_work, 0, sizeof(*m_work));

		m_wait = NEW RTPSorter;
		memset(m_wait, 0, sizeof(*m_wait));

		m_third_second = NEW RTPFilePacketHeader;
		memset(m_third_second, 0, sizeof(*m_third_second));

		// starting with 1 second of 8Kz mono 16-bit PCM
		m_pcmCache = NEW short[m_pcmSize = INITIAL_PCM_SIZE];
		memset(m_pcmCache, 0, m_pcmSize * sizeof(short));

		m_decBuffer = NEW short[m_decSize = MAX_PCM_BUFFER];
		memset(m_decBuffer, 0, m_decSize * sizeof(short));
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void AudioReaderLinearRTP::cleanup()
	{
		rtl::MutexLock lock(m_sync);

		if (m_work)
		{
			DELETEO(m_work);
			m_work = nullptr;
		}

		if (m_wait)
		{
			DELETEO(m_wait);
			m_wait = nullptr;
		}

		if (m_third_second)
		{
			DELETEO(m_third_second);
			m_third_second = nullptr;
		}

		if (m_pcmCache)
		{
			DELETEAR(m_pcmCache);
			m_pcmCache = nullptr;
		}

		if (m_decBuffer)
		{
			DELETEAR(m_decBuffer);
			m_decBuffer = nullptr;
		}

		cleanupDecoders();
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void AudioReaderLinearRTP::cleanupDecoders()
	{
		for (int i = 0; i < m_decoderList.getCount(); i++)
		{
			DecoderInfo& info = m_decoderList[i];

			if (info.resampler != nullptr)
			{
				destroyResampler(info.resampler);
				info.resampler = nullptr;
			}
			if (info.decoder != nullptr)
			{
				rtx_codec__release_audio_decoder(info.decoder);
				info.decoder = nullptr;
			}
		}

		m_decoderList.clear();
	}
	//-----------------------------------------------
	/// reader interface
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool AudioReaderLinearRTP::openSource(const char* path)
	{
		// ������� ������ ����
		close();

		initialize();

		// ��������� ������� ����
		if (!m_file.open(path))
		{
			LOG_ERROR("lrtp-src", "Can't open file '%s' : last error %d", path, errno);

			cleanup();

			return false;
		}

		m_GTTStartTime = m_file.get_header().rec_start_time;
		m_filePath = path;

		rtl::String ppath = path;

		//m_out.create(ppath + ".raw");

		return true;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void AudioReaderLinearRTP::closeSource()
	{
		m_file.close();

		m_filePath = rtl::String::empty;

		cleanup();

		//m_out.close();
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	int AudioReaderLinearRTP::readRawPCM(short* buffer, int size)
	{
		rtl::MutexLock lock(m_sync);

		int written = 0;

		while (written < size)
		{
			int res = read_pcm(buffer + written, size - written);

			if (res == 0)
				return written;

			written += res;
		}

		return written;
	}
	//-----------------------------------------------
	/// pcm fill functions
	//-----------------------------------------------
	// reading pcm using cache
	//-----------------------------------------------
	int AudioReaderLinearRTP::read_pcm(short* buffer, int size)
	{
		int remain = m_pcmLength - m_pcmUsed;

		if (remain == 0)
		{
			m_pcmLength = m_pcmUsed = 0;
			if ((remain = fill_pcm_buffer()) == 0)
				return 0;
		}

		if (remain >= size)
		{
			// enough
			memcpy(buffer, m_pcmCache + m_pcmUsed, size * sizeof(short));
			m_pcmUsed += size;

			//m_out.write(m_pcmCache + m_pcmUsed, size * sizeof(short));

			return size;
		}

		// copy remain
		memcpy(buffer, m_pcmCache + m_pcmUsed, remain * sizeof(short));
		m_pcmLength = m_pcmUsed = 0;

		//m_out.write(m_pcmCache + m_pcmUsed, remain * sizeof(short));

		return remain;
	}
	//-----------------------------------------------
	// called when cache is empty!
	//-----------------------------------------------
	int AudioReaderLinearRTP::fill_pcm_buffer()
	{
		if (!fill_rtp_buffers())
			return 0;

		// check and add startup silence block = first_packet_ts - record_start_ts
		// we have sorted and filled rtp packets sequence to decode

		appendInitialSilence();

		for (int i = 0; i < m_work->packet_count; i++)
		{
			RTPFilePacketHeader& packet = m_work->packets[i];

			processPacket(packet);
		}

		update_rtp_buffers();

		return m_pcmLength;
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	bool AudioReaderLinearRTP::fill_rtp_buffers()
	{
		const RTPFilePacketHeader* packet;

		while ((packet = m_file.readNext()) != nullptr)
		{
			if (packet->type == RTYPE_FORMAT)
			{
				updateDecoders(packet->format, packet->length / sizeof(RTPFileMediaFormat));
				continue;
			}

			m_statPackets++;

			rtp_header_t* rtp_hdr = (rtp_header_t*)packet->buffer;
			uint16_t seqNo = ntohs(rtp_hdr->seq);

			if (m_work->packet_count == 0)
			{
				memcpy(m_work->packets, packet, packet->length + RTP_FILE_PACKET_HEADER_LENGTH);

				m_work->seqBegin = seqNo;
				m_work->seqEnd = m_work->seqBegin + RTPSorter::PAKET_PER_SECOND - 1;
				m_work->packet_count = 1;
				m_wait->seqBegin = seqNo + RTPSorter::PAKET_PER_SECOND;
				m_wait->seqEnd = seqNo + (RTPSorter::PAKET_PER_SECOND * 2) - 1;
			}
			else if (seqNo < m_work->seqBegin)
			{
				// LOG late old packets. but now let's drop it
				continue;
			}
			else if (seqNo <= m_work->seqEnd) // working buffer;
			{
				int index = seqNo - m_work->seqBegin;
				// duplicate packets overwrite old ones
				m_work->packet_count++;
				memcpy(m_work->packets + index, packet, packet->length + RTP_FILE_PACKET_HEADER_LENGTH);
			}
			else if (seqNo <= m_wait->seqEnd) // waiting buffer
			{
				int index = seqNo - m_wait->seqBegin;
				// duplicate packets overwrite old ones
				m_wait->packet_count++;
				memcpy(m_wait->packets + index, packet, packet->length + RTP_FILE_PACKET_HEADER_LENGTH);
			}
			else // time to flush working buffer to pcm cache because we get the packet from the third second
			{
				//store for next update
				memcpy(m_third_second, packet, packet->length + RTP_FILE_PACKET_HEADER_LENGTH);
				return true;
			}
		}

		return m_file.eof();
	}
	//-----------------------------------------------
	// after processing work buffer!
	//-----------------------------------------------
	void AudioReaderLinearRTP::update_rtp_buffers()
	{
		RTPSorter* t = m_work;
		m_work = m_wait;
		m_wait = t;

		memset(m_wait->packets, 0, sizeof(m_wait->packets));
		m_wait->seqBegin = m_work->seqEnd + 1;
		m_wait->seqEnd = m_wait->seqBegin + RTPSorter::PAKET_PER_SECOND - 1;
		m_wait->packet_count = 0;

		if (m_third_second->type != RTYPE_EMPTY)
		{
			rtp_header_t* rtp_hdr = (rtp_header_t*)m_third_second->buffer;
			uint16_t seqNo = ntohs(rtp_hdr->seq);
			if (seqNo <= m_wait->seqEnd)
			{
				int index = seqNo - m_wait->seqBegin;
				memcpy(m_wait->packets + index, m_third_second, m_third_second->length + RTP_FILE_PACKET_HEADER_LENGTH);
				m_third_second->type = RTYPE_EMPTY;
				m_wait->packet_count = 1;
			}
		}
	}
	//-----------------------------------------------
	/// rtp_packet sorter functions
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void AudioReaderLinearRTP::processPacket(const RTPFilePacketHeader& packet)
	{
		switch (packet.type)
		{
		case RTYPE_FORMAT:
			updateDecoders(packet.format, packet.length / sizeof(RTPFileMediaFormat));
			break;
		case RTYPE_RTP:
			decodeRTP((rtp_header_t*)packet.buffer, packet.length);
			break;
		case RTYPE_EMPTY:
			decodeEmpty();
			break;
		default: // invalid!
			break;
		}
		// RTP

	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void AudioReaderLinearRTP::decodeRTP(rtp_header_t* pack, int length)
	{
		DecoderInfo* info = findDecoder(pack->pt);

		if (info == nullptr)
			return;

		rtp_packet packet;

		packet.read_packet(pack, length);

		if (pack->pt == 11) // do not decode packet because it is already in PCM format
		{
			m_decSize = packet.get_payload_length() / sizeof(short);
			memcpy(m_decBuffer, packet.get_payload(), packet.get_payload_length());
		}
		else // decoder must be initialized except 11 payload (mono PCM)
		{
			m_decSize = info->decoder->decode(packet.get_payload(), packet.get_payload_length(),
				(uint8_t*)m_decBuffer, MAX_PCM_BUFFER * sizeof(short)) / sizeof(short);
		}

		checkCacheSize(m_decSize);

		if (info->resampler != nullptr)
		{
			int len = info->resampler->resample(m_decBuffer, m_decSize, nullptr, m_pcmCache + m_pcmLength, m_pcmSize - m_pcmLength);
			m_pcmLength += len;

			m_statSamples += len;
		}
		else
		{
			memcpy(m_pcmCache + m_pcmLength, m_decBuffer, m_decSize * sizeof(short));
			m_pcmLength += m_decSize;

			m_statSamples += m_decSize;
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void AudioReaderLinearRTP::decodeEmpty()
	{
		// 20 ms for 8Kz mono 16-bit
		checkCacheSize(COMFORT_NOISE_20);
		memset(m_pcmCache + m_pcmLength, 0, COMFORT_NOISE_20 * sizeof(short));
		m_pcmLength += COMFORT_NOISE_20;
		m_statSamples += COMFORT_NOISE_20;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void AudioReaderLinearRTP::updateDecoders(const RTPFileMediaFormat* formats, int count)
	{
		rtl::MutexLock lock(m_sync);

		for (int i = 0; i < count; i++)
		{
			const RTPFileMediaFormat* fmt = formats + i;
			DecoderInfo* found = findDecoder(fmt->payload_id);
			if (found != nullptr && std_stricmp(found->format.encoding, fmt->encoding) == 0)
				continue;
			addDecoder(fmt);
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	AudioReaderLinearRTP::DecoderInfo* AudioReaderLinearRTP::findDecoder(int payloadId)
	{
		rtl::MutexLock lock(m_sync);

		for (int i = 0; i < m_decoderList.getCount(); i++)
		{
			if (m_decoderList[i].format.payload_id == payloadId)
				return &m_decoderList[i];
		}

		return nullptr;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void AudioReaderLinearRTP::addDecoder(const RTPFileMediaFormat* fmt)
	{
		LOG_FLAG1("rtp-src", "changing format:\n\
\t\tencoding:	\'%s\'\n\
\t\tparams:		\'%s\'\n\
\t\tfmtp:		\'%s\'\n\
\t\tpayload id: %d\n\
\t\tclockrate:	%d\n\
\t\tchannels:	%d",
fmt->encoding, fmt->params, fmt->fmtp, fmt->payload_id, fmt->clock_rate, fmt->channels);

		PayloadFormat format(fmt->encoding, (PayloadId)fmt->payload_id, fmt->clock_rate, fmt->channels, fmt->fmtp);

		if (format.getId() == PayloadId::G722)
		{
			format.setFrequency(16000);
		}

		DecoderInfo info{ *fmt, nullptr, nullptr };

		info.decoder = rtx_codec__create_audio_decoder(fmt->encoding, &format);

		if (info.decoder == nullptr && (PayloadId)fmt->payload_id != PayloadId::L16M)
		{
			// LOG
			return;
		}

		if (fmt->clock_rate != m_sourceFrequency)
		{
			info.resampler = createResampler(fmt->clock_rate, fmt->channels, m_sourceFrequency, m_sourceChannels);
		}

		m_decoderList.add(info);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void AudioReaderLinearRTP::checkCacheSize(int needSize)
	{
		int remain = m_pcmSize - m_pcmLength;

		if (remain >= needSize)
		{
			return;
		}

		m_pcmSize = m_pcmLength + needSize;

		m_pcmSize = (m_pcmSize & 0xFFFFC000) + 0x4000;

		short* ptr = NEW short[m_pcmSize];

		memcpy(ptr, m_pcmCache, m_pcmLength);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void AudioReaderLinearRTP::appendInitialSilence()
	{
		if (!m_newStream)
			return;

		m_newStream = false;

		RTPFilePacketHeader* first = nullptr;

		for (int i = 0; i < m_work->packet_count; i++)
		{
			if (m_work->packets[i].type == RTYPE_RTP)
			{
				first = &m_work->packets[i];
				break;
			}
		}

		if (first == nullptr)
		{
			LOG_ERROR("lrpt", "trying mix empty file");
			return;
		}

		int samplesCount = first->timestamp - m_file.get_header().rec_start_timestamp;

		checkCacheSize(samplesCount); // for 8 KHz

		memset(m_pcmCache + m_pcmLength, 0, samplesCount * sizeof(short));

		m_pcmLength += samplesCount;
	}
}
//-----------------------------------------------
