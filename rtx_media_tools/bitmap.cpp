/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "mmt_bitmap.h"

#include "libyuv/scale_argb.h"
#include "libyuv/convert_argb.h"
#include "libyuv/convert_from_argb.h"

#include <algorithm>
#include <cmath>

namespace media
{
	const Color Color::Black = { 0x00, 0x00, 0x00, 0x00 };
	const Color Color::White = { 0xFF, 0xFF, 0xFF, 0x00 };
	
	const Color Color::Green = { 0x00, 0xFF, 0x00, 0x00 };
	const Color Color::Red = { 0x00, 0x00, 0xFF, 0x00 };
	const Color Color::Blue = { 0xFF, 0x00, 0x00, 0x00 };
	const Color Color::Magenta = { 0xFF, 0x00, 0xFF, 0x00 };
	const Color Color::Cyan = { 0xFF, 0xFF, 0x00, 0x00 };
	const Color Color::Yellow = { 0x00, 0xFF, 0xFF, 0x00 };

	const Color Color::None  = { 0xFF, 0xFF, 0xFF, 0xFF };

	/* Author: Jan Winkler */
	/*! \brief Convert RGB to HSV color space

	  Converts a given set of RGB values `r', `g', `b' into HSV
	  coordinates. The input RGB values are in the range [0, 1], and the
	  output HSV values are in the ranges h = [0, 360], and s, v = [0,
	  1], respectively.

	  \param fR Red component, used as input, range: [0, 1]
	  \param fG Green component, used as input, range: [0, 1]
	  \param fB Blue component, used as input, range: [0, 1]
	  \param hsv.H Hue component, used as output, range: [0, 360]
	  \param hsv.S Hue component, used as output, range: [0, 1]
	  \param hsv.V Hue component, used as output, range: [0, 1]

	*/
	void RGBtoHSV(const Color& rgb, HSV& hsv)
	{
		float fR = rgb.red/255.0f;
		float fG = rgb.green/255.0f;
		float fB = rgb.blue/255.0f;

		float fCMax = MAX(MAX(fR, fG), fB);
		float fCMin = MIN(MIN(fR, fG), fB);
		float fDelta = fCMax - fCMin;

		if (fDelta > 0)
		{
			if (fCMax == fR)
			{
				hsv.H = 60.0f * (fmodf(((fG - fB) / fDelta), 6.0f));
			}
			else if (fCMax == fG)
			{
				hsv.H = 60.0f * (((fB - fR) / fDelta) + 2.0f);
			}
			else if (fCMax == fB)
			{
				hsv.H = 60.0f * (((fR - fG) / fDelta) + 4.0f);
			}

			if (fCMax > 0) {
				hsv.S = fDelta / fCMax;
			}
			else {
				hsv.S = 0;
			}

			hsv.V = fCMax;
		}
		else {
			hsv.H = 0;
			hsv.S = 0;
			hsv.V = fCMax;
		}

		if (hsv.H < 0) {
			hsv.H = 360 + hsv.H;
		}
	}


	/*! \brief Convert HSV to RGB color space

	  Converts a given set of HSV values `h', `s', `v' into RGB
	  coordinates. The output RGB values are in the range [0, 1], and
	  the input HSV values are in the ranges h = [0, 360], and s, v =
	  [0, 1], respectively.

	  \param fR Red component, used as output, range: [0, 1]
	  \param fG Green component, used as output, range: [0, 1]
	  \param fB Blue component, used as output, range: [0, 1]
	  \param hsv.H Hue component, used as input, range: [0, 360]
	  \param hsv.S Hue component, used as input, range: [0, 1]
	  \param hsv.V Hue component, used as input, range: [0, 1]

	*/
	void HSVtoRGB(Color& rgb, const HSV& hsv)
	{
		float fC = hsv.V * hsv.S; // Chroma
		float fHPrime = fmodf(hsv.H / 60.0f, 6.0f);
		float fX = fC * (1 - fabsf(fmodf(fHPrime, 2.0f) - 1.0f));
		float fM = hsv.V - fC;

		float fR = 0, fG = 0, fB = 0;

		if (0 <= fHPrime && fHPrime < 1)
		{
			fR = fC;
			fG = fX;
			fB = 0;
		}
		else if (1 <= fHPrime && fHPrime < 2)
		{
			fR = fX;
			fG = fC;
			fB = 0;
		}
		else if (2 <= fHPrime && fHPrime < 3)
		{
			fR = 0;
			fG = fC;
			fB = fX;
		}
		else if (3 <= fHPrime && fHPrime < 4)
		{
			fR = 0;
			fG = fX;
			fB = fC;
		}
		else if (4 <= fHPrime && fHPrime < 5)
		{
			fR = fX;
			fG = 0;
			fB = fC;
		}
		else if (5 <= fHPrime && fHPrime < 6)
		{
			fR = fC;
			fG = 0;
			fB = fX;
		}

		fR += fM;
		fG += fM;
		fB += fM;

		rgb = { (uint8_t)(fB * 255.0), (uint8_t)(fB * 255.0), (uint8_t)(fB * 255.0), 0 };
	}

	//template<class T>
	//static inline void swap(T& l, T& r)
	//{
	//	T t = l;
	//	l = r;
	//	r = t;
	//}

	static inline double clamp(double v, double lower_range, double upper_range)
	{
		return (v < lower_range) ? lower_range : (v > upper_range) ? upper_range : v;
	}

	Bitmap& Bitmap::operator = (const Bitmap& image)
	{
		if (this != &image)
		{
			createBitmap(image.m_width, image.m_height);

			if (m_bitmap != nullptr && m_length > 0)
				memcpy(m_bitmap, image.m_bitmap, m_length * sizeof(Color));
		}

		return *this;
	}

	void Bitmap::destroy()
	{
		if (m_bitmap != nullptr)
		{
			DELETEAR(m_bitmap);
			m_bitmap = nullptr;
			m_length = m_width = m_height = 0;
		}
	}

	void Bitmap::fill(Color x)
	{
		if (m_bitmap != nullptr)
		{
			for (int i = 0; i < m_width; i++)
				m_bitmap[i] = x;

			for (int i = 1; i < m_height; i++)
				memcpy(m_bitmap + i * m_width, m_bitmap, m_width * sizeof(Color));

		}
	}

	bool Bitmap::copyFrom(const Bitmap& image)
	{
		if (this == &image || image.m_height != m_height || image.m_width != m_width)
		{
			return false;
		}

		if (m_bitmap != nullptr)
		{
			memcpy(m_bitmap, image.m_bitmap, m_length * sizeof(Color));
		}

		return true;
	}

	bool Bitmap::copyFrom(const Bitmap& source_image, int x_offset, int y_offset)
	{
		if ((x_offset + source_image.m_width) > m_width || (y_offset + source_image.m_height) > m_height)
		{
			return false;
		}

		for (int y = 0; y < source_image.m_height; ++y)
		{
			Color* itr1 = getRow(y + y_offset) + x_offset;
			const Color* itr2 = source_image.getRow(y);

			memcpy(itr1, itr2, source_image.m_width * sizeof(Color)); // BytesPerPixel
		}

		return true;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void Bitmap::transformTo(Bitmap& image, int width, int height, Transform mode, int params) const
	{
		switch (mode)
		{
		case Transform::Copy:
			transformCopy(image, width, height);
			break;
		case Transform::Stretch:
			transformStretch(image, width, height, params);
			break;
		case Transform::ScaleVert:
			transformScaleVert(image, width, height, params);
			break;
		case Transform::ScaleHorz:
			transformScaleHorz(image, width, height, params);
			break;
		default:
			// do nothing
			break;
		}
	}

	//-----------------------------------------------
	//
	//-----------------------------------------------
	void Bitmap::transformCopy(Bitmap& image, int width, int height) const
	{
		image.copyFrom(*this);
	}
	void Bitmap::transformStretch(Bitmap& image, int width, int height, int params) const
	{
		image.createBitmap(width, height);

		libyuv::ARGBScale(getData(), m_width * BytesPerPixel, m_width, m_height,
			image.getData(), width * BytesPerPixel, width, height, libyuv::FilterMode::kFilterBilinear);
	}

	void Bitmap::transformScaleVert(Bitmap& image, int width, int height, int params) const
	{
		double koef = (double)height / (double)m_height;
		int scaled_width = (int)((double)m_width * koef);

		image.createBitmap(width, height);

		transformTo(image, scaled_width, height, Transform::Stretch);
	}

	void Bitmap::transformScaleHorz(Bitmap& image, int width, int height, int params) const
	{
		double koef = (double)width / (double)m_width;
		int scaled_height = (int)((double)m_height * koef);

		image.createBitmap(width, height);
		transformTo(image, width, scaled_height, Transform::Stretch);
	}

	bool Bitmap::getRegion(int x, int y, int width, int height, Bitmap& dest_image) const
	{
		if ((x + width) > m_width || (y + height) > m_height)
		{
			return false;
		}

		if (dest_image.m_width < m_width || dest_image.m_height < m_height)
		{
			dest_image.resize(width, height);
		}

		for (int r = 0; r < height; ++r)
		{
			const Color* itr1 = getRow(r + y) + x;
			Color* itr2 = dest_image.getRow(r);

			memcpy(itr2, itr1, width * BytesPerPixel);
		}

		return true;
	}

	bool Bitmap::setRegion(int x, int y, int width, int height, uint8_t value)
	{
		if ((x + width) > m_width || (y + height) > m_height)
		{
			return false;
		}

		for (int r = 0; r < height; ++r)
		{
			Color* itr = getRow(r + y) + x;

			memset(itr, value, width * BytesPerPixel);
		}

		return true;
	}

	bool Bitmap::setRegion(int x, int y, int width, int height, ColorPlane color, uint8_t value)
	{
		if ((x + width) > m_width || (y + height) > m_height)
		{
			return false;
		}

		int color_plane_offset = offset(color);

		for (int r = 0; r < height; ++r)
		{
			Color* itr = getRow(r + y) + x;
			Color* itr_end = itr + width;

			while (itr != itr_end)
			{
				switch (color)
				{
				case ColorPlane::Red: itr->red = value; break;
				case ColorPlane::Green: itr->green = value; break;
				case ColorPlane::Blue: itr->blue = value; break;
				}
			}
		}

		return true;
	}

	bool Bitmap::setRegion(int x, int y, int width, int height, Color colour)
	{
		if ((x + width) > m_width || (y + height) > m_height)
		{
			return false;
		}

		for (int r = 0; r < height; ++r)
		{
			Color* itr = getRow(r + y) + x;
			Color* itr_end = itr + (width);

			while (itr != itr_end)
			{
				*(itr++) = colour;
			}
		}

		return true;
	}

	void Bitmap::reflectiveImage(Bitmap& image, bool include_diagnols)
	{
		image.resize(3 * m_width, 3 * m_height, true);
		image.copyFrom(*this, m_width, m_height);

		verticalFlip();

		image.copyFrom(*this, m_width, 0);
		image.copyFrom(*this, m_width, 2 * m_height);

		verticalFlip();
		horizontalFlip();

		image.copyFrom(*this, 0, m_height);
		image.copyFrom(*this, 2 * m_width, m_height);

		horizontalFlip();

		if (include_diagnols)
		{
			Bitmap tile = *this;

			tile.verticalFlip();
			tile.horizontalFlip();

			image.copyFrom(tile, 0, 0);
			image.copyFrom(tile, 2 * m_width, 0);
			image.copyFrom(tile, 2 * m_width, 2 * m_height);
			image.copyFrom(tile, 0, 2 * m_height);
		}
	}

	void Bitmap::resize(int width, int height, bool clear)
	{
		if (m_width != width || m_height != height)
		{
			createBitmap(width, height);
		}

		if (clear && m_bitmap != nullptr)
		{
			memset(m_bitmap, 0, sizeof(Color) * m_length);
		}
	}

	void Bitmap::saveBitmapFile(const char* file_name) const
	{
		if (m_bitmap == nullptr || m_length == 0 || m_width == 0 || m_height == 0)
		{
			// invalid state!
			return;
		}

		rtl::FileStream stream;

		if (!stream.createNew(file_name))
		{
			fprintf(stderr, "Bitmap::save_image(): Error - Could not open file %s for writing!\n", file_name);
			return;
		}

		BitmapInformationHeader bih;

		bih.width = m_width;
		bih.height = m_height;
		bih.bit_count = (uint16_t)(BytesPerPixel24 << 3);
		bih.clr_important = 0;
		bih.clr_used = 0;
		bih.compression = 0;
		bih.planes = 1;
		bih.size = sizeof(bih);
		bih.x_pels_per_meter = 0;
		bih.y_pels_per_meter = 0;
		bih.size_image = (((bih.width * BytesPerPixel24) + 3) & 0x0000FFFC) * bih.height;

		BitmapFileHeader bfh;

		bfh.type = 19778;
		bfh.size = sizeof(bfh) + sizeof(bih) + bih.size_image;
		bfh.reserved1 = 0;
		bfh.reserved2 = 0;
		bfh.off_bits = sizeof(bih) + sizeof(bfh);

		writeBitmapFileHeader(stream, bfh);
		writeBitmapInformationHeader(stream, bih);

		int padding = (4 - ((3 * m_width) % 4)) % 4;
		char padding_data[4] = { 0x00, 0x00, 0x00, 0x00 };

		int row_length = m_width * BytesPerPixel24;
		uint8_t* row24 = new uint8_t[row_length];

		for (int i = m_height - 1; i >= 0; i--)
		{
			const Color* r = getRow(i);
			uint8_t* ptr = row24;
			for (int j = 0; j < m_width; j++)
			{
				*ptr++ = r->blue;
				*ptr++ = r->green;
				*ptr++ = r->red;
				r++;
			}

			stream.write(row24, row_length);
			stream.write(padding_data, padding);
		}

		delete[] row24;

		stream.close();
	}

	void Bitmap::setChannel(ColorPlane color, uint8_t value)
	{
		for (uint8_t* itr = (getData() + offset(color)); itr < end(); itr += BytesPerPixel)
		{
			*itr = value;
		}
	}

	void Bitmap::rorChannel(ColorPlane color, int ror)
	{
		for (uint8_t* itr = (getData() + offset(color)); itr < end(); itr += BytesPerPixel)
		{
			*itr = (uint8_t)(((*itr) >> ror) | ((*itr) << (8 - ror)));
		}
	}

	void Bitmap::invertColorPlanes()
	{
		for (uint8_t* itr = getData(); itr < end(); *itr = ~(*itr), ++itr);
	}

	void Bitmap::toGrayscale()
	{
		double r_scaler = 0.299;
		double g_scaler = 0.587;
		double b_scaler = 0.114;

		Color* itr_end = m_bitmap + m_length;
		for (Color* itr = m_bitmap; itr < itr_end; itr++)
		{
			uint8_t gray_value = (uint8_t)((r_scaler * itr->red) + (g_scaler * itr->green) + (b_scaler * itr->blue));

			itr->blue = itr->green = itr->red = gray_value;
		}
	}

	void Bitmap::horizontalFlip()
	{
		for (int y = 0; y < m_height; ++y)
		{
			Color* itr1 = getRow(y);
			Color* itr2 = itr1 + m_width - 1;

			while (itr1 < itr2)
			{
				swap(*itr1, *itr2);

				itr1++;
				itr2--;
			}
		}
	}

	void Bitmap::verticalFlip()
	{
		for (int y = 0; y < (m_height / 2); ++y)
		{
			Color* itr1 = getRow(y);
			Color* itr2 = getRow(m_height - y - 1);

			for (int x = 0; x < m_width; ++x)
			{
				swap(*(itr1 + x), *(itr2 + x));
			}
		}
	}

	void Bitmap::subsample(Bitmap& dest) const
	{
		/*
		   Half sub-sample of original image.
		*/
		int w = 0;
		int h = 0;

		bool odd_width = false;
		bool odd_height = false;

		if (0 == (m_width % 2))
			w = m_width / 2;
		else
		{
			w = 1 + (m_width / 2);
			odd_width = true;
		}

		if (0 == (m_height % 2))
			h = m_height / 2;
		else
		{
			h = 1 + (m_height / 2);
			odd_height = true;
		}

		int horizontal_upper = (odd_width) ? (w - 1) : w;
		int vertical_upper = (odd_height) ? (h - 1) : h;

		dest.resize(w, h, true);

		uint8_t* s_itr[4];
		const uint8_t*  itr1[4];
		const uint8_t*  itr2[4];

		int row_increment_ = m_width * BytesPerPixel;

		s_itr[0] = dest.getData() + 0;
		s_itr[1] = dest.getData() + 1;
		s_itr[2] = dest.getData() + 2;
		s_itr[3] = dest.getData() + 3;

		itr1[0] = getData() + 0;
		itr1[1] = getData() + 1;
		itr1[2] = getData() + 2;
		itr1[3] = getData() + 3;

		itr2[0] = getData() + row_increment_ + 0;
		itr2[1] = getData() + row_increment_ + 1;
		itr2[2] = getData() + row_increment_ + 2;
		itr2[3] = getData() + row_increment_ + 3;

		int total = 0;

		for (int j = 0; j < vertical_upper; ++j)
		{
			for (int i = 0; i < horizontal_upper; ++i)
			{
				for (int k = 0; k < BytesPerPixel; s_itr[k] += BytesPerPixel, ++k)
				{
					total = 0;
					total += *(itr1[k]);
					total += *(itr1[k]);
					total += *(itr2[k]);
					total += *(itr2[k]);

					itr1[k] += BytesPerPixel;
					itr1[k] += BytesPerPixel;
					itr2[k] += BytesPerPixel;
					itr2[k] += BytesPerPixel;

					*(s_itr[k]) = static_cast<uint8_t>(total >> 2);
				}
			}

			if (odd_width)
			{
				for (int k = 0; k < BytesPerPixel; s_itr[k] += BytesPerPixel, ++k)
				{
					total = 0;
					total += *(itr1[k]);
					total += *(itr2[k]);

					itr1[k] += BytesPerPixel;
					itr2[k] += BytesPerPixel;

					*(s_itr[k]) = static_cast<uint8_t>(total >> 1);
				}
			}

			for (int k = 0; k < BytesPerPixel; ++k)
			{
				itr1[k] += row_increment_;
			}

			if (j != (vertical_upper - 1))
			{
				for (int k = 0; k < BytesPerPixel; ++k)
				{
					itr2[k] += row_increment_;
				}
			}
		}

		if (odd_height)
		{
			for (int i = 0; i < horizontal_upper; ++i)
			{
				for (int k = 0; k < BytesPerPixel; s_itr[k] += BytesPerPixel, ++k)
				{
					total = 0;
					total += *(itr1[k]);
					total += *(itr2[k]);

					itr1[k] += BytesPerPixel;
					itr2[k] += BytesPerPixel;

					*(s_itr[k]) = static_cast<uint8_t>(total >> 1);
				}
			}

			if (odd_width)
			{
				for (int k = 0; k < BytesPerPixel; ++k)
				{
					(*(s_itr[k])) = *(itr1[k]);
				}
			}
		}
	}

	void Bitmap::upsample(Bitmap& dest) const
	{
		/*
		   2x up-sample of original image.
		*/

		dest.resize(2 * m_width, 2 * m_height, true);

		const uint8_t* s_itr[4];
		uint8_t*  itr1[4];
		uint8_t*  itr2[4];

		int dest_row_increment_ = dest.getWidth() * BytesPerPixel;

		s_itr[0] = getData() + 0;
		s_itr[1] = getData() + 1;
		s_itr[2] = getData() + 2;
		s_itr[3] = getData() + 3;

		itr1[0] = dest.getData() + 0;
		itr1[1] = dest.getData() + 1;
		itr1[2] = dest.getData() + 2;
		itr1[3] = dest.getData() + 3;

		itr2[0] = dest.getData() + dest_row_increment_ + 0;
		itr2[1] = dest.getData() + dest_row_increment_ + 1;
		itr2[2] = dest.getData() + dest_row_increment_ + 2;
		itr2[3] = dest.getData() + dest_row_increment_ + 3;

		for (int j = 0; j < m_height; ++j)
		{
			for (int i = 0; i < m_width; ++i)
			{
				for (int k = 0; k < BytesPerPixel; s_itr[k] += BytesPerPixel, ++k)
				{
					*(itr1[k]) = *(s_itr[k]); itr1[k] += BytesPerPixel;
					*(itr1[k]) = *(s_itr[k]); itr1[k] += BytesPerPixel;

					*(itr2[k]) = *(s_itr[k]); itr2[k] += BytesPerPixel;
					*(itr2[k]) = *(s_itr[k]); itr2[k] += BytesPerPixel;
				}
			}

			for (int k = 0; k < BytesPerPixel; ++k)
			{
				itr1[k] += dest_row_increment_;
				itr2[k] += dest_row_increment_;
			}
		}
	}

	void Bitmap::readBitmapFileHeader(rtl::Stream& stream, BitmapFileHeader& bfh)
	{
		readFrom(stream, bfh.type);
		readFrom(stream, bfh.size);
		readFrom(stream, bfh.reserved1);
		readFrom(stream, bfh.reserved2);
		readFrom(stream, bfh.off_bits);

		if (isBigEndian())
		{
			bfh.type = flip(bfh.type);
			bfh.size = flip(bfh.size);
			bfh.reserved1 = flip(bfh.reserved1);
			bfh.reserved2 = flip(bfh.reserved2);
			bfh.off_bits = flip(bfh.off_bits);
		}
	}

	void Bitmap::writeBitmapFileHeader(rtl::Stream& stream, const BitmapFileHeader& bfh) const
	{
		if (isBigEndian())
		{
			writeTo(stream, flip(bfh.type));
			writeTo(stream, flip(bfh.size));
			writeTo(stream, flip(bfh.reserved1));
			writeTo(stream, flip(bfh.reserved2));
			writeTo(stream, flip(bfh.off_bits));
		}
		else
		{
			writeTo(stream, bfh.type);
			writeTo(stream, bfh.size);
			writeTo(stream, bfh.reserved1);
			writeTo(stream, bfh.reserved2);
			writeTo(stream, bfh.off_bits);
		}
	}

	void Bitmap::readBitmapInformationHeader(rtl::Stream& stream, BitmapInformationHeader& bih)
	{
		readFrom(stream, bih.size);
		readFrom(stream, bih.width);
		readFrom(stream, bih.height);
		readFrom(stream, bih.planes);
		readFrom(stream, bih.bit_count);
		readFrom(stream, bih.compression);
		readFrom(stream, bih.size_image);
		readFrom(stream, bih.x_pels_per_meter);
		readFrom(stream, bih.y_pels_per_meter);
		readFrom(stream, bih.clr_used);
		readFrom(stream, bih.clr_important);

		if (isBigEndian())
		{
			bih.size = flip(bih.size);
			bih.width = flip(bih.width);
			bih.height = flip(bih.height);
			bih.planes = flip(bih.planes);
			bih.bit_count = flip(bih.bit_count);
			bih.compression = flip(bih.compression);
			bih.size_image = flip(bih.size_image);
			bih.x_pels_per_meter = flip(bih.x_pels_per_meter);
			bih.y_pels_per_meter = flip(bih.y_pels_per_meter);
			bih.clr_used = flip(bih.clr_used);
			bih.clr_important = flip(bih.clr_important);
		}
	}

	void Bitmap::writeBitmapInformationHeader(rtl::Stream& stream, const BitmapInformationHeader& bih) const
	{
		if (isBigEndian())
		{
			writeTo(stream, flip(bih.size));
			writeTo(stream, flip(bih.width));
			writeTo(stream, flip(bih.height));
			writeTo(stream, flip(bih.planes));
			writeTo(stream, flip(bih.bit_count));
			writeTo(stream, flip(bih.compression));
			writeTo(stream, flip(bih.size_image));
			writeTo(stream, flip(bih.x_pels_per_meter));
			writeTo(stream, flip(bih.y_pels_per_meter));
			writeTo(stream, flip(bih.clr_used));
			writeTo(stream, flip(bih.clr_important));
		}
		else
		{
			writeTo(stream, bih.size);
			writeTo(stream, bih.width);
			writeTo(stream, bih.height);
			writeTo(stream, bih.planes);
			writeTo(stream, bih.bit_count);
			writeTo(stream, bih.compression);
			writeTo(stream, bih.size_image);
			writeTo(stream, bih.x_pels_per_meter);
			writeTo(stream, bih.y_pels_per_meter);
			writeTo(stream, bih.clr_used);
			writeTo(stream, bih.clr_important);
		}
	}

	void Bitmap::createBitmap(int width, int height)
	{
		destroy();

		int len = width * height;
		

		if (len > 0)
		{
			m_width = width;
			m_height = height;
			m_length = len;
			m_bitmap = new Color[m_length];

			memset(m_bitmap, 0, len * sizeof(Color));
		}
	}

	bool Bitmap::loadBitmapFile(const char* filename)
	{
		destroy();

		rtl::FileStream stream;

		if (!stream.open(filename))
		{
			fprintf(stderr, "Bitmap::loadBitmapFile() ERROR: Bitmap - file %s not found!", filename);
			return false;
		}

		BitmapFileHeader bfh;
		BitmapInformationHeader bih;

		memset(&bfh, 0, sizeof(bfh));
		memset(&bih, 0, sizeof(bih));

		readBitmapFileHeader(stream, bfh);
		readBitmapInformationHeader(stream, bih);

		if (bfh.type != 19778)
		{
			fprintf(stderr, "Bitmap::loadBitmapFile() ERROR: Bitmap - Invalid type value %d expected 19778.", bfh.type);

			memset(&bfh, 0, sizeof(bfh));
			memset(&bih, 0, sizeof(bih));

			stream.close();

			return false;
		}

		if (bih.bit_count != 24)
		{
			fprintf(stderr, "Bitmap::loadBitmapFile() ERROR: Bitmap - Invalid bit depth %d expected 24.", bih.bit_count);

			bfh.clear();
			bih.clear();

			stream.close();

			return false;
		}

		if (bih.size != sizeof(bih))
		{
			fprintf(stderr, "Bitmap::loadBitmapFile() ERROR: Bitmap - Invalid BIH size %d expected %zd", bih.size, sizeof(bih));

			bfh.clear();
			bih.clear();

			stream.close();

			return false;
		}

		m_width = bih.width;
		m_height = bih.height;

		int padding = (4 - ((3 * bih.width) % 4)) % 4;
		char padding_data[4] = { 0x00, 0x00, 0x00, 0x00 };

		size_t bitmap_file_size = stream.getLength();

		size_t bitmap_logical_size = (bih.height * bih.width * BytesPerPixel24) + (bih.height * padding) + sizeof(bih) + sizeof(bfh);

		if (bitmap_file_size != bitmap_logical_size)
		{
			fprintf(stderr, "Bitmap::loadBitmapFile() ERROR: Bitmap - Mismatch between logical and physical sizes of bitmap.\n\
	Logical: %zd\n\
	Physical: %zd\n", bitmap_logical_size, bitmap_file_size);

			bfh.clear();
			bih.clear();

			stream.close();

			return false;
		}

		createBitmap(bih.width, bih.height);

		uint8_t* row24 = new uint8_t[m_width * BytesPerPixel24];

		for (int i = m_height - 1; i >= 0; i--)
		{
			Color* data_ptr = getRow(i); // read in inverted row order

			stream.read(row24, BytesPerPixel24 * m_width);

			uint8_t* ptr = row24;

			for (int j = 0; j < m_width; j++)
			{
				data_ptr[j] = { *ptr++, *ptr++, *ptr++, 255u };
			}

			stream.read(padding_data, padding);
		}

		delete[] row24;

		return true;
	}

	bool Bitmap::loadBitmapStruct(const uint8_t* bitmap, int length)
	{
		destroy();

		const BitmapFileHeader* bfh = (BitmapFileHeader*)bitmap;
		const BitmapInformationHeader* bih = (BitmapInformationHeader*)(bitmap + sizeof(*bfh));

		int read = sizeof(*bfh) + sizeof(*bih);

		if (read >= length)
		{
			fprintf(stderr, "Bitmap::loadBitmapStruct() ERROR: Bitmap - struct has no data.");
			return false;
		}

		if (bfh->type != 19778)
		{
			fprintf(stderr, "Bitmap::loadBitmapFile() ERROR: Bitmap - Invalid type value %d expected 19778.", bfh->type);
			return false;
		}

		if (bih->bit_count != 24)
		{
			fprintf(stderr, "Bitmap::loadBitmapFile() ERROR: Bitmap - Invalid bit depth %d expected 24.", bih->bit_count);
			return false;
		}

		if (bih->size != sizeof(*bih))
		{
			fprintf(stderr, "Bitmap::loadBitmapFile() ERROR: Bitmap - Invalid BIH size %d expected %zd", bih->size, sizeof(bih));
			return false;
		}

		m_width = bih->width;
		m_height = bih->height;

		int padding = (4 - ((3 * bih->width) % 4)) % 4;
		char padding_data[4] = { 0x00, 0x00, 0x00, 0x00 };

		size_t bitmap_file_size = length;

		size_t bitmap_logical_size = (bih->height * bih->width * BytesPerPixel24) + (bih->height * padding) + sizeof(*bih) + sizeof(*bfh);

		if (bitmap_file_size != bitmap_logical_size)
		{
			fprintf(stderr, "Bitmap::loadBitmapFile() ERROR: Bitmap - Mismatch between logical and physical sizes of bitmap.\n\
	Logical: %zd\n\
	Physical: %zd\n", bitmap_logical_size, bitmap_file_size);
			return false;
		}

		createBitmap(bih->width, bih->height);

		const uint8_t* row24 = bitmap + read;

		for (int i = m_height - 1; i >= 0; i--)
		{
			Color* data_ptr = getRow(i); // read in inverted row order

			const uint8_t* ptr = row24;

			for (int j = 0; j < m_width; j++)
			{
				data_ptr[j] = { *ptr++, *ptr++, *ptr++, 255u };
			}

			row24 += BytesPerPixel24 * m_width;
			row24 += padding;
		}

		return true;
	}

	bool Bitmap::loadBitmapBase64(const char* base64, int length)
	{
		int bitmapLength = Base64_CalcDecodedSize(length);
		uint8_t* bitmap = NEW uint8_t[bitmapLength + 4];
		Base64Decode(base64, length, bitmap, bitmapLength + 4);

		bool result = loadBitmapStruct(bitmap, bitmapLength);

		DELETEAR(bitmap);

		return result;
	}
	
	bool Bitmap::setBitmapBits(int width, int height, const uint8_t* argb, int length)
	{
		createBitmap(width, height);

		if (m_length * sizeof(Color) != length)
		{
			return false;
		}

		memcpy(m_bitmap, argb, length);

		return true;
	}

	bool Bitmap::setBitmapBitsRGB24(int width, int height, const uint8_t* rgb24, int length)
	{
		createBitmap(width, height);

		if (m_length * sizeof(Color) != (length * 4) / 3)
		{
			return false;
		}

		const uint8_t* row24 = rgb24;

		for (int i = m_height - 1; i >= 0; i--)
		{
			Color* data_ptr = getRow(i); // read in inverted row order

			for (int i = 0; i < m_width; i++)
			{
				*data_ptr++ = { *row24++, *row24++, *row24++, 255u };
			}
		}

		return true;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void Bitmap::transformImage(const Bitmap& source, Bitmap& dest, int width, int height, Color emptyColor)
	{
		double aspect_ratio_from = (double)source.getWidth() / (double)source.getHeight();
		double aspect_ratio_to = (double)width / (double)height;

		if (fabs(aspect_ratio_from - aspect_ratio_to) < 0.01)
		{
			// aspect ratio is equal
			// grow or shrink?
			source.transformTo(dest, width, height, media::Transform::Stretch);
			return;
		}

		dest.createBitmap(width, height);
		media::ImageDrawer dc(dest);

		if (aspect_ratio_from < aspect_ratio_to)
		{
			// draw vertical bars
			int icon_height = height;
			int icon_width = (int)round(height * aspect_ratio_from);

			media::Bitmap small;
			source.transformTo(small, icon_width, icon_height, media::Transform::Stretch);

			int bw = (width - icon_width) / 2;

			dc.drawImage(bw, 0, small);
			dc.setPenColor(emptyColor);
			dc.fillRectangle(0, 0, bw, height);
			dc.fillRectangle(bw+icon_width, 0, width, height);
		}
		else
		{
			// draw horizontal bars

			int icon_width = width;
			int icon_height = (int)round(icon_width / aspect_ratio_from);

			media::Bitmap small;
			source.transformTo(small, icon_width, icon_height, media::Transform::Stretch);

			int bh = (height - icon_height) / 2;

			dc.drawImage(0, bh, small);
		}
	}
}
