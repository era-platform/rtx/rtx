/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_video_reader.h"
#include "mmt_video_reader_rtp.h"

namespace media
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VideoReader::VideoReader(rtl::Logger* log) : m_log(log),
		m_eof(false),
		m_readingStarted(false),
		m_GTTStartTime(0),
		m_statFrames(0),
		m_statPackets(0)
	{
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VideoReader::~VideoReader()
	{
	}
	//-----------------------------------------------
	/// �������� �����,
	//-----------------------------------------------
	bool VideoReader::open(const char* filePath)
	{
		return false;
	}
	//-----------------------------------------------
	/// �������� ��������
	//-----------------------------------------------
	void VideoReader::close()
	{
	}
	/// ���������� ����� �������� ����� � �� ������� ������
	bool VideoReader::setupReader(VideoSize frameDimention, Transform transformMode)
	{
		m_dimention = frameDimention;
		m_mode = transformMode;
		return true;
	}
	//-----------------------------------------------
	/// ������ ���������������� ������ -- ���� setup_reader() �� ��������� �� �� ������ ����� ������������ L16
	//-----------------------------------------------
	bool VideoReader::readNext(VideoImage* image)
	{
		if (!readImage(&m_outImage))
			return false;

		m_outImage.transformTo(*image, m_dimention.width, m_dimention.height, m_mode, 0);

		return true;
	}
	//-----------------------------------------------
	/// �������� ��������
	//-----------------------------------------------
	VideoReader* VideoReader::create(const char* path, rtl::Logger* log)
	{
		VideoReader* source = nullptr;

		// �������� �� mp3
		//if (is_webm(path))
		//{
		//	source = NEW VideoReaderWebm();
		//}
		//else
		{
			// ������� ����
			rtl::FileStream file;

			if (!file.open(path))
			{
				LOG_ERROR("m-rdr", "Can't open file '%s' last error %d", path, errno);
				return nullptr;
			}

			// ������� ������ ����
			uint32_t riff, read;

			//AudioReader* source = nullptr;
			if ((read = (uint32_t)file.read(&riff, sizeof(uint32_t))) != sizeof(uint32_t))
			{
				LOG_ERROR("m-rdr", "Can't read file '%s' last error %d", path, errno);
			}
			else if (riff == MG_REC_VIDEO_SIGNATURE)
			{
				source = NEW VideoReaderRTP(log);
			}

			file.close();
		}

		if (source != nullptr)
		{
			if (!source->open(path))
			{
				source->close();
				DELETEO(source);
				source = nullptr;
			}
		}

		return source;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VideoReader* VideoReader::create(const char* path, VideoSize frameDimention, Transform transformMode, rtl::Logger* log)
	{
		VideoReader* reader = create(path, log);

		if (reader != nullptr)
			reader->setupReader(frameDimention, transformMode);

		return reader;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoReader::resetReader()
	{
		// ?
	}
}
//-----------------------------------------------
