﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_file_mp3.h"

//--------------------------------------
//
//--------------------------------------
static void frontend_debugf(const char *format, va_list ap);
static void frontend_msgf(const char *format, va_list ap);
static void frontend_errorf(const char *format, va_list ap);
//static void frontend_print_null(const char *format, va_list ap);
static int check_aid(const unsigned char *header);
static int is_syncword_mp123(const void *const headerptr);
static size_t lenOfId3v2Tag(unsigned char const* buf);

namespace media {
	//--------------------------------------
	//
	//--------------------------------------
	FileReaderMP3::FileReaderMP3() : m_hip(nullptr), m_eof(true),
		m_pcmSkipStart(0), m_pcmSkipEnd(0), m_pcmSampleCount(0),
		m_pcmBufferSize(0), m_pcmBufferPos(0)
	{
		memset(&m_mp3Data, 0, sizeof m_mp3Data);

		m_pcmBufferLeft = nullptr; // (short*)MALLOC(PCM_BUFFER_SIZE * sizeof(short));
		m_pcmBufferRight = nullptr; // (short*)MALLOC(PCM_BUFFER_SIZE * sizeof(short));
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool FileReaderMP3::open(const char* path)
	{
		close();

		if (!m_file.open(path))
		{
			return false;
		}

		int enc_delay = 0, enc_padding = 0;

		if (!openFile(&enc_delay, &enc_padding))
		{
			close();
			return false;
		}

		setSkipStartAndEnd(enc_delay, enc_padding);

		unsigned long n = m_pcmSampleCount;
		if (n != UINT_MAX)
		{
			unsigned long const discard = m_pcmSkipStart + m_pcmSkipEnd;
			m_pcmSampleCount = n > discard ? n - discard : 0;
		}

		m_pcmBufferLeft = (short*)MALLOC(PCM_BUFFER_SIZE * sizeof(short));
		if (m_mp3Data.stereo == 2)
		{
			m_pcmBufferRight = (short*)MALLOC(PCM_BUFFER_SIZE * sizeof(short));
		}

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void FileReaderMP3::close()
	{
		m_file.close();

		if (m_hip != nullptr)
		{
			hip_decode_exit(m_hip);
			m_hip = nullptr;
		}

		if (m_pcmBufferLeft != nullptr)
		{
			FREE(m_pcmBufferLeft);
			m_pcmBufferLeft = nullptr;
		}

		if (m_pcmBufferRight != nullptr)
		{
			FREE(m_pcmBufferRight);
			m_pcmBufferRight = nullptr;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	int FileReaderMP3::readNext(short* mono_buffer, int size)
	{
		// если стерео то читаем только левый буфер!
		int copied = 0;

		while (copied < size)
		{
			// читаем максимальное количество данных
			int pcm_read = readDataPCM(mono_buffer + copied, nullptr, size - copied);

			// если данные закончились или что то пошло не так
			if (pcm_read <= 0)
				break;

			// сколько скопированно всего
			copied += pcm_read;
		}

		return copied;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int FileReaderMP3::readNext(short* pcm_l, short* pcm_r, int size)
	{
		// если моно то правый буфер заполняем данными левого буфера!
		// если стерео то читаем только левый буфер!
		int copied = 0;

		while (copied < size)
		{
			short* pcm_r_ptr = pcm_r != nullptr ? pcm_r + copied : nullptr;
			// читаем максимальное количество данных
			int pcm_read = readDataPCM(pcm_l + copied, pcm_r_ptr, size - copied);

			// если данные закончились или что то пошло не так
			if (pcm_read <= 0)
				break;

			// сколько скопированно всего
			copied += pcm_read;
		}

		return copied;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool FileReaderMP3::openFile(int *enc_delay, int *enc_padding)
	{
		/* set the defaults from info incase we cannot determine them from file */
		m_pcmSampleCount = UINT_MAX;

		if (!readHeaders(enc_delay, enc_padding))
		{
			LOG_ERROR("mp3-rd", "Error reading headers in mp3 input file");

			close();
			return false;
		}

		if (m_mp3Data.stereo <= 0 && m_mp3Data.stereo > 2)
		{
			LOG_ERROR("mp3-rd", "Unsupported number of channels: %d", m_mp3Data.stereo);

			close();
			return false;
		}

		m_pcmSampleCount = m_mp3Data.nsamp;

		if (uint32_t(m_pcmSampleCount) == UINT_MAX)
		{
			double  flen = (double)m_file.getLength(); /* try to figure out num_samples */
			if (flen >= 0)
			{
				/* try file size, assume 2 bytes per sample */
				if (m_mp3Data.bitrate > 0)
				{
					double  totalseconds = (flen * 8.0 / (1000.0 * m_mp3Data.bitrate));
					unsigned long tmp_num_samples = (unsigned long)(totalseconds * m_mp3Data.samplerate);

					m_pcmSampleCount = tmp_num_samples;
					m_mp3Data.nsamp = tmp_num_samples;
				}
			}
		}

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool FileReaderMP3::readHeaders(int* enc_delay, int* enc_padding)
	{
		/*  VBRTAGDATA pTagData; */
		/* int xing_header,len2,num_frames; */
		unsigned char buf[100];
		int     ret;
		size_t  len;
		int     aid_header;
		short int pcm_l[1152], pcm_r[1152];
		int     freeformat = 0;

		memset(&m_mp3Data, 0, sizeof(mp3data_struct));

		m_hip = hip_decode_init();

		hip_set_msgf(m_hip, frontend_msgf);
		hip_set_errorf(m_hip, frontend_errorf);
		hip_set_debugf(m_hip, frontend_debugf);

		len = 4;
		if (m_file.read(buf, len) != len)
		{
			return false;      /* failed */
		}

		size_t in_id3v2_size = 0;
		uint8_t* in_id3v2_tag = nullptr;

		while (buf[0] == 'I' && buf[1] == 'D' && buf[2] == '3')
		{
			len = 6;
			if (m_file.read(&buf[4], len) != len)
			{
				return false;  /* failed */
			}

			len = lenOfId3v2Tag(&buf[6]);
			if (in_id3v2_size < 1)
			{
				in_id3v2_size = 10 + len;
				in_id3v2_tag = (unsigned char*)malloc(in_id3v2_size);
				if (in_id3v2_tag)
				{
					memcpy(in_id3v2_tag, buf, 10);
					if (m_file.read(&in_id3v2_tag[10], len) != len)
					{
						return false;  /* failed */
					}
					len = 0; /* copied, nothing to skip */
				}
				else
				{
					in_id3v2_size = 0;
				}
			}
			m_file.seekCurrent(len);
			len = 4;
			if (m_file.read(&buf, len) != len)
			{
				return false;  /* failed */
			}
		}

		aid_header = check_aid(buf);
		if (aid_header)
		{
			if (m_file.read(buf, 2) != 2)
				return false;  /* failed */

			aid_header = (unsigned char)buf[0] + 256 * (unsigned char)buf[1];
			LOG_CALL("mp3-rd", "Album ID found.  length=%d", aid_header);

			/* skip rest of AID, except for 6 bytes we have already read */
			m_file.seekCurrent(aid_header - 6);

			/* read 4 more bytes to set up buffer for MP3 header check */
			if (m_file.read(buf, len) != len)
				return false;  /* failed */
		}

		len = 4;
		while (!is_syncword_mp123(buf))
		{
			unsigned int i;
			for (i = 0; i < len - 1; i++)
				buf[i] = buf[i + 1];
			if (m_file.read(buf + len - 1, 1) != 1)
				return false;  /* failed */
		}

		if ((buf[2] & 0xf0) == 0)
		{
			LOG_CALL("mp3-rd", "Input file is freeformat");
			freeformat = 1;
		}
		/* now parse the current buffer looking for MP3 headers.    */
		/* (as of 11/00: mpglib modified so that for the first frame where  */
		/* headers are parsed, no data will be decoded.   */
		/* However, for freeformat, we need to decode an entire frame, */
		/* so mp3data->bitrate will be 0 until we have decoded the first */
		/* frame.  Cannot decode first frame here because we are not */
		/* yet prepared to handle the output. */
		ret = hip_decode1_headersB(m_hip, buf, len, pcm_l, pcm_r, &m_mp3Data, enc_delay, enc_padding);

		if (-1 == ret)
			return false;

		/* repeat until we decode a valid mp3 header.  */
		while (!m_mp3Data.header_parsed)
		{
			len = m_file.read(buf, sizeof(buf));

			if (len != sizeof(buf))
				return false;

			ret = hip_decode1_headersB(m_hip, buf, len, pcm_l, pcm_r, &m_mp3Data, enc_delay, enc_padding);

			if (-1 == ret)
				return false;
		}

		if (m_mp3Data.bitrate == 0 && !freeformat)
		{
			LOG_CALL("mp3-rd", "fail to sync...");

			return false; // lame_decode_initfile(fd, mp3data, enc_delay, enc_padding);
		}

		if (m_mp3Data.totalframes > 0)
		{
			/* mpglib found a Xing VBR header and computed nsamp & totalframes */
		}
		else
		{
			/* set as unknown.  Later, we will take a guess based on file size
			* ant bitrate */
			m_mp3Data.nsamp = UINT_MAX;
		}

		/*
		report_printf("ret = %i NEED_MORE=%i \n",ret,MP3_NEED_MORE);
		report_printf("stereo = %i \n",mp.fr.stereo);
		report_printf("samp = %i  \n",freqs[mp.fr.sampling_frequency]);
		report_printf("framesize = %i  \n",framesize);
		report_printf("bitrate = %i  \n",mp3data->bitrate);
		report_printf("num frames = %ui  \n",num_frames);
		report_printf("num samp = %ui  \n",mp3data->nsamp);
		report_printf("mode     = %i  \n",mp.fr.mode);
		*/

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void FileReaderMP3::setSkipStartAndEnd(int enc_delay, int enc_padding)
	{
		int skip_start = 0, skip_end = 0;

		/*if (global_decoder.mp3_delay_set)
			skip_start = global_decoder.mp3_delay;*/

		if (skip_start == 0)
		{
			if (enc_delay > -1 || enc_padding > -1)
			{
				if (enc_delay > -1)
					skip_start = enc_delay + 528 + 1;
				if (enc_padding > -1)
					skip_end = enc_padding - (528 + 1);
			}
			/*else
				skip_start = lame_get_encoder_delay(gfp) + 528 + 1;*/
		}
		else
		{
			/* user specified a value of skip. just add for decoder */
			skip_start += 528 + 1; /* mp3 decoder has a 528 sample delay, plus user supplied "skip" */
		}

		skip_start = skip_start < 0 ? 0 : skip_start;
		skip_end = skip_end < 0 ? 0 : skip_end;
		m_pcmSkipStart = skip_start;
		m_pcmSkipEnd = skip_end;
	}
	//--------------------------------------
	// заполняем буфер данными из mp3 файла
	//--------------------------------------
	bool FileReaderMP3::fillBufferPCM()
	{
		if (m_pcmBufferPos > 0)
		{
			// сдвигаем все вниз и дочитываем из файла

			int to_copy = m_pcmBufferSize - m_pcmBufferPos;

			memmove(m_pcmBufferLeft, m_pcmBufferLeft + m_pcmBufferPos, to_copy * sizeof(short));

			if (m_mp3Data.stereo == 2)
			{
				memmove(m_pcmBufferRight, m_pcmBufferRight + m_pcmBufferPos, to_copy * sizeof(short));
			}

			m_pcmBufferSize -= m_pcmBufferPos;
			m_pcmBufferPos = 0;
		}

		while (PCM_BUFFER_SIZE - m_pcmBufferSize > MP3_FRAME_SIZE)
		{
			short pcm_l[MP3_FRAME_SIZE];
			short pcm_r[MP3_FRAME_SIZE];

			int mp3_read = readFrame(pcm_l, pcm_r);

			if (mp3_read == 0)
			{
				// данных больше нет
				m_eof = true;
				break;
			}
			else if (mp3_read < 0)
			{
				// произошла ошибка
				m_eof = true;
				break;
			}

			memcpy(m_pcmBufferLeft + m_pcmBufferSize, pcm_l, mp3_read * sizeof(short));
			if (m_mp3Data.stereo == 2)
			{
				memcpy(m_pcmBufferRight + m_pcmBufferSize, pcm_r, mp3_read * sizeof(short));
			}

			m_pcmBufferSize += mp3_read;
		}

		// при первом заполнении
		if (m_pcmSkipStart > 0)
		{
			if (m_pcmBufferSize >= m_pcmSkipStart)
			{
				// сдвигаем все вниз и дочитываем из файла

				int to_copy = m_pcmBufferSize - m_pcmSkipStart;

				memmove(m_pcmBufferLeft, m_pcmBufferLeft + m_pcmSkipStart, to_copy * sizeof(short));

				if (m_mp3Data.stereo == 2)
				{
					memmove(m_pcmBufferRight, m_pcmBufferRight + m_pcmSkipStart, to_copy * sizeof(short));
				}

				m_pcmBufferSize -= m_pcmSkipStart;
				m_pcmBufferPos = 0;
				m_pcmSkipStart = 0;
			}
			else
			{
				m_pcmSkipStart -= m_pcmBufferSize;
				m_pcmBufferSize = 0;

				return fillBufferPCM();
			}
		}

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int FileReaderMP3::readFrame(short* pcm_l, short* pcm_r)
	{
		int ret = 0;
		size_t len = 0;
		uint8_t buf[1024];

		/* first see if we still have data buffered in the decoder: */
		ret = hip_decode1_headers(m_hip, buf, len, pcm_l, pcm_r, &m_mp3Data);
		if (ret != 0)
			return ret;


		/* read until we get a valid output frame */
		for (;;)
		{
			len = m_file.read(buf, 1024);
			if (len == 0)
			{
				/* we are done reading the file, but check for buffered data */
				ret = hip_decode1_headers(m_hip, buf, len, pcm_l, pcm_r, &m_mp3Data);

				if (ret <= 0)
				{
					return -1; /* done with file */
				}
				break;
			}

			ret = hip_decode1_headers(m_hip, buf, len, pcm_l, pcm_r, &m_mp3Data);
			if (ret == -1)
			{
				return -1;
			}

			if (ret > 0)
				break;
		}

		return ret;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int FileReaderMP3::readDataPCM(short* pcm_l, short* pcm_r, int size)
	{
		// если буфер пустой то заполним его
		if (m_pcmBufferSize == 0)
		{
			if (!fillBufferPCM())
			{
				return 0;
			}
		}

		// вычислим количество даннных для копирования
		int to_copy = MIN(size, m_pcmBufferSize);

		// копируем сколько есть!
		memcpy(pcm_l, m_pcmBufferLeft + m_pcmBufferPos, to_copy * sizeof(short));

		if (pcm_r != nullptr)
		{
			if (m_mp3Data.stereo == 2)
			{
				memcpy(pcm_r, m_pcmBufferRight + m_pcmBufferPos, to_copy * sizeof(short));
			}
			else
			{
				memcpy(pcm_r, m_pcmBufferLeft + m_pcmBufferPos, to_copy * sizeof(short));
			}
		}

		m_pcmBufferPos += to_copy;
		m_pcmBufferSize -= to_copy;

		if (m_pcmBufferSize == 0)
		{
			m_pcmBufferPos = 0;
		}

		return to_copy;
	}
	//--------------------------------------
	//
	//--------------------------------------
	FileWriterMP3::FileWriterMP3() :
		m_lame(nullptr),
		m_pcmLeftStorage(nullptr),
		m_pcmRightStorage(nullptr),
		m_pcmStorageSize(0),
		m_pcmStored(0),
		m_pcmFrameLength(0),
		m_mp3FrameSize(0),
		m_mp3Frame(nullptr),
		m_isStereo(false),
		m_samplesEncoded(0),
		m_mp3FrameCount(0),
		m_mp3BytesWritten(0)
	{
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool FileWriterMP3::create(const char* filename, int clock_rate, int channels, int bit_rate)
	{
		close();

		if (!m_file.createNew(filename))
			return false;

		m_lame = lame_init();

		if (m_lame == nullptr)
		{
			LOG_ERROR("mp3-rec", "Can't create mp3-encoder!");
			close();
			return false;
		}

		// настраеваем кодек
		initializeEventHandlers();

		lame_set_preset(m_lame, 24);

		//lame_set_num_samples(m_lame, UINT32_MAX);
		lame_set_in_samplerate(m_lame, clock_rate);
		lame_set_num_channels(m_lame, channels);

		//lame_set_VBR(m_lame, vbr_off);
		//lame_set_mode(m_lame, MONO);

		if (lame_init_params(m_lame) == -1)
		{
			LOG_ERROR("mp3-rec", "Can't initialize mp3-encoder parameters!");
			close();
			return false;
		}

		// инициализация переменных
		m_isStereo = channels == 2;
		m_mp3FrameSize = lame_get_framesize(m_lame);
		m_mp3Frame = (uint8_t*)MALLOC(LAME_MAXMP3BUFFER);
		m_pcmFrameLength = m_mp3FrameSize * channels;
		m_pcmStorageSize = m_pcmFrameLength * 4;
		m_pcmLeftStorage = (short*)MALLOC(m_pcmStorageSize * sizeof(short));
		m_pcmRightStorage = (short*)MALLOC(m_pcmStorageSize * sizeof(short));
		m_pcmStored = 0;

		// параметры
		LOG_CALL("mp3-rec", "MP3 recorder parameters:\n\
						   clock rate:      %d Hz\n\
						   channels:        %d\n\
						   frame size:      %d bytes\n\
						   pcm frame length %d samples\n\
						   pcm storage size %d samples", clock_rate, channels, m_mp3FrameSize, m_pcmFrameLength, m_pcmStorageSize);


		// пишем заголовок
		size_t id3v2_size = lame_get_id3v2_tag(m_lame, 0, 0);

		if (id3v2_size > 0)
		{
			uint8_t *id3v2tag = (uint8_t*)MALLOC(id3v2_size);

			if (id3v2tag != 0)
			{
				size_t imp3 = lame_get_id3v2_tag(m_lame, id3v2tag, id3v2_size);
				unsigned long owrite;

				owrite = (uint32_t)m_file.write(id3v2tag, imp3);

				FREE(id3v2tag);

				if (owrite != imp3)
				{
					LOG_ERROR("mp3-rec", "Error writing ID3v2 tag");
					close();
					return false;
				}
			}
		}

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void FileWriterMP3::close()
	{
		if (m_lame != nullptr)
		{
			lame_set_num_samples(m_lame, m_samplesEncoded);
			//запишем шапку файла
			int imp3 = lame_encode_flush(m_lame, m_mp3Frame, LAME_MAXMP3BUFFER);
			uint32_t file_written;

			if (imp3 > 0)
			{
				file_written = (uint32_t)m_file.write(m_mp3Frame, imp3);
				m_mp3BytesWritten += file_written;
			}

			// пропишем заголовок 

			imp3 = (uint32_t)lame_get_lametag_frame(m_lame, m_mp3Frame, LAME_MAXMP3BUFFER);

			m_file.setPosition(0);

			if (imp3 > 0)
			{
				file_written = (uint32_t)m_file.write(m_mp3Frame, imp3);
			}

			lame_close(m_lame);
			m_lame = nullptr;
		}

		flush();

		m_file.close();

		if (m_pcmLeftStorage != nullptr)
		{
			FREE(m_pcmLeftStorage);
			m_pcmLeftStorage = nullptr;
		}
		if (m_pcmRightStorage != nullptr)
		{
			FREE(m_pcmRightStorage);
			m_pcmRightStorage = nullptr;
		}

		m_pcmStorageSize = 0;
		m_pcmStored = 0;

		if (m_mp3Frame != nullptr)
		{
			FREE(m_mp3Frame);
			m_mp3Frame = nullptr;
			m_mp3FrameSize = 0;
		}

		// print statistics

		LOG_CALL("mp3-rec", "mp3 statistics:\n\
\t\tpcm samples:  %d\n\
\t\tmp3 frames:   %d\n\
\t\tmp3 written:  %d\n", m_samplesEncoded, m_mp3FrameCount, m_mp3BytesWritten);
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool FileWriterMP3::flush()
	{
		if (m_file.isOpened())
		{
			return true;
		}

		return false;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int FileWriterMP3::write(const short* left_channel, const short* right_channel, int samples_per_channel)
	{
		/// копируем в буфер после чего отправляем в лейм

		if (left_channel == nullptr || samples_per_channel == 0)
			return 0;

		if (!m_file.isOpened() || m_lame == nullptr)
		{
			return -1;
		}

		if (!addSamplesToStorage(left_channel, right_channel, samples_per_channel))
		{
			return -1;
		}

		while (m_pcmStored >= m_pcmFrameLength)
		{
			int mp3_written = lame_encode_buffer(m_lame, m_pcmLeftStorage, m_isStereo ? m_pcmRightStorage : nullptr, m_pcmFrameLength, m_mp3Frame, LAME_MAXMP3BUFFER);

			if (mp3_written > 0)
			{
				uint32_t file_written;

				if ((file_written = (uint32_t)m_file.write(m_mp3Frame, mp3_written)) == 0)
				{
					// ???
					return 0;
				}

				m_samplesEncoded += m_pcmFrameLength;
				m_mp3FrameCount++;
				m_mp3BytesWritten += file_written;

				resetStorage(m_pcmFrameLength);

				return m_pcmFrameLength;
			}
		}

		return 0;
	}
	//--------------------------------------
	// for all other
	//--------------------------------------
	int FileWriterMP3::write(const short* mono_samples, int samples_count)
	{
		return 0;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void FileWriterMP3::initializeEventHandlers()
	{
		lame_set_msgf(m_lame, &frontend_msgf);
		lame_set_errorf(m_lame, &frontend_errorf);
		lame_set_debugf(m_lame, &frontend_debugf);
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool FileWriterMP3::addSamplesToStorage(const short* left_channel, const short* right_channel, int samples_per_channel)
	{
		if (m_pcmStored + samples_per_channel > m_pcmStorageSize)
		{
			LOG_ERROR("mp3-rec", "write : fatal error : writing %d samples count greater than storage free size %d/%d/%d!",
				samples_per_channel, m_pcmStorageSize, m_pcmStored, m_pcmStorageSize - m_pcmStored);
			return false;
		}

		memcpy(m_pcmLeftStorage + m_pcmStored, left_channel, samples_per_channel * sizeof(short));
		if (m_isStereo)
		{
			memcpy(m_pcmRightStorage + m_pcmStored, right_channel != nullptr ? right_channel : left_channel, samples_per_channel * sizeof(short));
		}

		m_pcmStored += samples_per_channel;

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void FileWriterMP3::resetStorage(int samples_written)
	{
		if (samples_written > 0)
		{
			if (samples_written < m_pcmStored)
			{
				// сдвинем остаток в начало для последующей записи
				m_pcmStored -= samples_written;
				memmove(m_pcmLeftStorage, m_pcmLeftStorage + samples_written, m_pcmStored * sizeof(short));
				if (m_isStereo)
				{
					memmove(m_pcmRightStorage, m_pcmRightStorage + samples_written, m_pcmStored * sizeof(short));
				}

			}
			else if (samples_written == m_pcmStored)
			{
				// очистим хранилище
				m_pcmStored = 0;
			}
		}
	}
}
//--------------------------------------
//
//--------------------------------------
void frontend_debugf(const char *format, va_list ap)
{
	if (rtl::Logger::check_trace(TRF_CALL))
	{
		Log.log_format("mp3-lame", format, ap);
	}
}
//--------------------------------------
//
//--------------------------------------
void frontend_msgf(const char *format, va_list ap)
{
	if (rtl::Logger::check_trace(TRF_CALL))
	{
		Log.log_format("mp3-lame", format, ap);
	}
}
//--------------------------------------
//
//--------------------------------------
void frontend_errorf(const char *format, va_list ap)
{
	if (rtl::Logger::check_trace(TRF_CALL))
	{
		Log.log_format("mp3-lame", format, ap);
	}
}
//--------------------------------------
//
//--------------------------------------
// void frontend_print_null(const char *format, va_list ap)
// {
// 	if (rtl::Logger::check_trace(TRF_CALL))
// 	{
// 		Log.log_format("mp3-lame", format, ap);
// 	}
// }
//--------------------------------------
//
//--------------------------------------
static int check_aid(const unsigned char *header)
{
	return 0 == memcmp(header, "AiD\1", 4);
}
//--------------------------------------
//
//--------------------------------------
static int is_syncword_mp123(const void *const headerptr)
{
	const unsigned char *const p = (const unsigned char *)headerptr;
	static const char abl2[16] = { 0, 7, 7, 7, 0, 7, 0, 0, 0, 0, 0, 8, 8, 8, 8, 8 };

	if ((p[0] & 0xFF) != 0xFF)
		return 0;       /* first 8 bits must be '1' */
	if ((p[1] & 0xE0) != 0xE0)
		return 0;       /* next 3 bits are also */
	if ((p[1] & 0x18) == 0x08)
		return 0;       /* no MPEG-1, -2 or -2.5 */

	//switch (p[1] & 0x06)
	//{
	//default:
	//case 0x00:         /* illegal Layer */
	//	return 0;
	//case 0x02:         /* Layer3 */
	//	if (global_reader.input_format != sf_mp3 && global_reader.input_format != sf_mp123) {
	//		return 0;
	//	}
	//	global_reader.input_format = sf_mp3;
	//	break;

	//case 0x04:         /* Layer2 */
	//	if (global_reader.input_format != sf_mp2 && global_reader.input_format != sf_mp123) {
	//		return 0;
	//	}
	//	global_reader.input_format = sf_mp2;
	//	break;

	//case 0x06:         /* Layer1 */
	//	if (global_reader.input_format != sf_mp1 && global_reader.input_format != sf_mp123) {
	//		return 0;
	//	}
	//	global_reader.input_format = sf_mp1;
	//	break;
	//}
	if ((p[1] & 0x06) == 0x00)
		return 0;       /* no Layer I, II and III */
	if ((p[2] & 0xF0) == 0xF0)
		return 0;       /* bad bitrate */
	if ((p[2] & 0x0C) == 0x0C)
		return 0;       /* no sample frequency with (32,44.1,48)/(1,2,4)     */
	if ((p[1] & 0x18) == 0x18 && (p[1] & 0x06) == 0x04 && abl2[p[2] >> 4] & (1 << (p[3] >> 6)))
		return 0;
	if ((p[3] & 3) == 2)
		return 0;       /* reserved enphasis mode */
	return 1;
}
//--------------------------------------
//
//--------------------------------------
static size_t lenOfId3v2Tag(unsigned char const* buf)
{
	unsigned int b0 = buf[0] & 127;
	unsigned int b1 = buf[1] & 127;
	unsigned int b2 = buf[2] & 127;
	unsigned int b3 = buf[3] & 127;
	return (((((b0 << 7) + b1) << 7) + b2) << 7) + b3;
}
//--------------------------------------
