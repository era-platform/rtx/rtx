/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "libyuv.h"
#include "mmt_yuv_image.h"

namespace media
{
	static int getUOffset(int width, int height);
	static int getVOffset(int width, int height);
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoImage::makeImage(int width, int height)
	{
		destroyImage();

		m_ownData = true;
		m_width = width;
		m_height = height;

		m_imageSize = calcImageSize(width, height);
		m_YData = (uint8_t*)MALLOC(m_imageSize);
		int u_offset = getUOffset(width, height);
		memset(m_YData, 0x10, u_offset);
		m_UData = m_YData + u_offset;
		memset(m_UData, 0x80, u_offset / 4);
		m_VData = m_YData + getVOffset(width, height);
		memset(m_VData, 0x80, u_offset / 4);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoImage::makeImage(int width, int height, const uint8_t* image_data, int size)
	{
		makeImage(width, height);

		memcpy(m_YData, image_data, size);

		m_imageSize = size;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoImage::storeImage(int width, int height, const uint8_t* image_data, int size)
	{
		destroyImage();

		m_width = width;
		m_height = height;

		m_ownData = false;

		m_YData = (uint8_t*)image_data;
		m_UData = m_YData + getUOffset(m_width, m_height);
		m_VData = m_YData + getVOffset(m_width, m_height);

		m_imageSize = size;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoImage::copyImage(const VideoImage& image)
	{
		makeImage(image.m_width, image.m_height);

		memcpy(m_YData, image.m_YData, image.m_imageSize);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoImage::destroyImage()
	{
		if (m_ownData && m_YData != nullptr)
		{
			FREE(m_YData);
		}

		m_YData = m_UData = m_VData = nullptr;
		m_imageSize = 0;
		m_width = m_height = 0;
	}
	void VideoImage::importARGB(const Bitmap& argb)
	{
		makeImage(argb.getWidth(), argb.getHeight());
		libyuv::ARGBToI420(argb.getData(), argb.getWidth() * argb.BytesPerPixel, m_YData, m_width, m_UData, m_width / 2, m_VData, m_width / 2, m_width, m_height);

	}
	void VideoImage::exportARGB(Bitmap& argb) const
	{
		argb.resize(m_width, m_height);
		libyuv::I420ToARGB(m_YData, m_width, m_UData, m_width / 2, m_VData, m_width / 2, argb.getData(), m_width * argb.BytesPerPixel, m_width, m_height);

	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	static const int Sign = 0xABCDEF01;

	bool VideoImage::loadImageFile(const char* filepath)
	{
		rtl::FileStream stream;

		char buffer[9] = { 0 };

		if (!stream.open(filepath))
			return false;

		int width, height, sign;

		if (!stream.read(buffer, 8))
			return false;

		sign = strtol(buffer, nullptr, 16);
		if (sign != Sign)
			return false;

		if (!stream.read(buffer, 8))
			return false;

		width = strtol(buffer, nullptr, 16);

		if (!stream.read(buffer, 8))
			return false;

		height = strtol(buffer, nullptr, 16);

		makeImage(width, height);

		stream.read(m_YData, m_width * m_height);
		stream.read(m_UData, m_width * m_height / 4);
		stream.read(m_VData, m_width * m_height / 4);

		stream.close();

		return true;

	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoImage::saveImageFile(const char* filepath, bool raw)
	{
		rtl::FileStream stream;

		if (stream.createNew(filepath))
		{
			//if (!raw)
			{
				char sign[256];
				int length = std_snprintf(sign, 256, "%08x%08x%08x", Sign, m_width, m_height);
				stream.write(sign, length);
			}

			stream.write(m_YData, m_width * m_height);
			stream.write(m_UData, m_width * m_height / 4);
			stream.write(m_VData, m_width * m_height / 4);

			stream.close();
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoImage::transformTo(VideoImage& image, int width, int height, Transform mode, int params) const
	{
		switch (mode)
		{
		case Transform::Copy:
			transform_copy(image, width, height);
			break;
		case Transform::Stretch:
			transform_stretch(image, width, height, params);
			break;
		case Transform::ScaleVert:
			transform_scale_v(image, width, height, params);
			break;
		case Transform::ScaleHorz:
			transform_scale_h(image, width, height, params);
			break;
		default:
			// do nothing
			break;
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VideoPixel VideoImage::getPixel(int x, int y) const
	{
		VideoPixel p = { 0 };

		if (m_YData == nullptr || x >= m_width || y >= m_height)
			return p;

		p.cc[0] = *(m_YData + (m_width * y + x));
		p.cc[1] = *(m_UData + ((m_width * y) / 4 + x / 2));
		p.cc[2] = *(m_VData + ((m_width * y) / 4 + x / 2));

		return p;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoImage::setPixel(int x, int y, VideoPixel pixel)
	{
		if (m_YData == nullptr || x >= m_width || y >= m_height)
			return;

		*(m_YData + (m_width * y + x)) = pixel.cc[0];
		*(m_UData + ((m_width * y) / 4 + x / 2)) = pixel.cc[1];
		*(m_VData + ((m_width * y) / 4 + x / 2)) = pixel.cc[2];

	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VideoImage::transform_copy(VideoImage& image, int width, int height) const
	{
		image.makeImage(width, height);

		int sL = m_width <= width ? 0 : (m_width - width) / 2;
		int sT = m_height <= height ? 0 : (m_height - height) / 2;
		int dL = m_width <= width ? (width - m_width) / 2 : 0;
		int dT = m_height <= height ? (height - m_height) / 2 : 0;

		int copy_w = MIN(width, m_width);
		int copy_h = MIN(height, m_height);

		for (int i = 0; i < copy_h; i++)
		{
			// Y
			memcpy(image.m_YData + ((dT + i) * image.m_width) + dL, m_YData + ((sT + i)*m_width) + sL, copy_w);
			if ((i & 1) == 0)
			{
				// U
				memcpy(image.m_UData + ((dT + i) / 2 * image.m_width / 2) + dL / 2, m_UData + ((sT + i) / 2 * m_width / 2) + sL / 2, copy_w / 2);
				// V
				memcpy(image.m_VData + ((dT + i) / 2 * image.m_width / 2) + dL / 2, m_VData + ((sT + i) / 2 * m_width / 2) + sL / 2, copy_w / 2);
			}
		}
	}
	void VideoImage::transform_stretch(VideoImage& image, int width, int height, int params) const
	{
		image.makeImage(width, height);

		libyuv::I420Scale(m_YData, m_width, m_UData, m_width / 2, m_VData, m_width / 2, m_width, m_height,
			image.m_YData, width, image.m_UData, width / 2, image.m_VData, width / 2, width, height, libyuv::FilterMode::kFilterBox);
	}
	void VideoImage::transform_scale_v(VideoImage& image, int width, int height, int params) const
	{
		image.makeImage(width, height);

		double koef = (double)height / (double)m_height;

		int scaled_width = (int)((double)m_width * koef);

		VideoImage scaled;

		transformTo(scaled, scaled_width, height, Transform::Stretch);

		scaled.transformTo(image, width, height, Transform::Copy);
	}
	void VideoImage::transform_scale_h(VideoImage& image, int width, int height, int params) const
	{
		image.makeImage(width, height);

		double koef = (double)width / (double)m_width;

		int scaled_height = (int)((double)m_height * koef);

		VideoImage scaled;

		transformTo(scaled, width, scaled_height, Transform::Stretch);

		scaled.transformTo(image, width, height, Transform::Copy);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	struct IntFMTP
	{
		const char* name;
		VideoStandard pref_vs;
		bool cif_family;
		VideoSize dimention;
	};
	//-----------------------------------------------
	//
	//-----------------------------------------------
	static const IntFMTP fmtp_sizes[] =
	{
		{ "SQCIF", VideoStandard::SQCIF, true, { 128, 96 }},
		{ "QCIF", VideoStandard::QCIF, true, { 176, 144 }},
		{ "CIF", VideoStandard::CIF, true, { 352, 288 }},
		{ "4CIF", VideoStandard::CIF4, true, { 704, 576 }},
		{ "16CIF", VideoStandard::CIF16, true, { 1408, 1152 }},

		{ "QVGA",VideoStandard::QVGA, false, { 320, 240 }},
		{ "VGA", VideoStandard::VGA, false, { 640, 480 }},
		{ "SVGA", VideoStandard::SVGA, false, { 800, 600 }},
		{ "HVGA", VideoStandard::HVGA, false, { 480, 320 }},

		{ "480P", VideoStandard::HDTV480p, false, { 852, 480 }},
		{ "720P", VideoStandard::HDTV720p, false, { 1280, 720 }},
		{ "1080P", VideoStandard::HDTV1080p, false, { 1920, 1080 }},


		{ "CUSTOM", VideoStandard::Custom, false, { 0, 0 }},
	};
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VideoSize videoStandardDimention(VideoStandard pref_vs)
	{
		int index = (int)pref_vs;

		if (index >= 0 && index < sizeof(fmtp_sizes) / sizeof(IntFMTP))
		{
			return fmtp_sizes[index].dimention;
		}

		return { 0,0 };
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	int getNearestCIF(const VideoSize& img_size, VideoSize& cif)
	{
		const VideoSize s_cif[] = { { 128, 96 }, { 176, 144 }, { 352, 288 }, { 704, 576 }, { 1408, 1152 } };

		for (size_t i = 1; i < sizeof(s_cif) / sizeof(VideoSize); i++)
		{
			if (s_cif[i].width > img_size.width)
			{
				cif = s_cif[i - 1];
				return 1;
			}
		}

		return 0;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VideoSize parseVideoFMTP(const char* fmtp, VideoStandard pref_vs, int* fps)
	{
		if (!fmtp || !fps)
		{
			//LOG_ERROR("Invalid parameter");
			return { 0,0 };
		}

		// set default values
		VideoSize size = videoStandardDimention(pref_vs);
		*fps = 15;

		const char* next_param = fmtp;

		while (*next_param != 0)
		{
			// set real values
			const char* cp = strpbrk(next_param, "=;");

			if (cp == nullptr)
				break;

			if (*cp == ';')
			{
				next_param = cp + 1;
				continue;
			}

			for (size_t i = 0; i < sizeof(fmtp_sizes) / sizeof(fmtp_sizes[0]); i++)
			{
				if ((int)pref_vs >= (int)fmtp_sizes[i].pref_vs && std_strnicmp(fmtp_sizes[i].name, next_param, strlen(fmtp_sizes[i].name)))
				{
					size = fmtp_sizes[i].dimention;
					*fps = atoi(cp + 1);
					*fps = *fps ? 30 / (*fps) : 15;

					return size;
				}
			}
		}

		return { 0,0 };
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VideoSize parseVideoImageAttr(const char* imageattr, VideoStandard pref_vs)
	{
		return { 0, 0 };
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	rtl::String getVideoFMTP(VideoStandard pref_vs)
	{
		rtl::String fmtp;

		for (size_t i = 0; i < sizeof(fmtp_sizes) / sizeof(fmtp_sizes[0]); ++i)
		{
			if (fmtp_sizes[i].cif_family && fmtp_sizes[i].pref_vs <= pref_vs)
			{
				fmtp << fmtp_sizes[i].name << "=2;";
			}
		}

		if (fmtp[fmtp.getLength() - 1] == ';')
			fmtp.remove(fmtp.getLength() - 1);

		return fmtp;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	int calcImageSize(int width, int height)
	{
		return width * height * 2;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	int calcImageSize(const VideoSize& imageSize)
	{
		return calcImageSize(imageSize.width, imageSize.height);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	int getDefaultsVideoMotionRank()
	{
		return 2;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	int getVideoBandwidthKbps(int width, int height, int fps, int motion_rank)
	{
		return (int)round((width * height * fps * motion_rank * 0.07) / 1024.0);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	int getVideoBandwidthKbps2(int width, int height, int fps)
	{
		return getVideoBandwidthKbps(width, height, fps, getDefaultsVideoMotionRank());
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	static int getUOffset(int width, int height)
	{
		return width * height;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	static int getVOffset(int width, int height)
	{
		int numberOfPixels = width * height;
		return 	numberOfPixels + (numberOfPixels >> 2);
	}
	//--------------------------------------
	// ������� ������� �������� �����
	//--------------------------------------
	void VideoImageQueue::create(int queueDepth)
	{
		reset();

		m_queueSize = queueDepth;
		m_queue = (QueueImage*)MALLOC(m_queueSize * sizeof(QueueImage));
		memset(m_queue, 0, m_queueSize * sizeof(QueueImage));
		m_writeIdx = m_readIdx = 0;
		m_count = 0;
	}
	//--------------------------------------
	// ������� �������. �������� ������������
	//--------------------------------------
	void VideoImageQueue::reset()
	{
		if (m_queue != nullptr)
		{
			for (int i = 0; i < m_queueSize; i++)
			{
				if (m_queue[i].image != nullptr)
					DELETEO(m_queue[i].image);
			}

			FREE(m_queue);
			m_queue = nullptr;
		}

		m_queueSize = m_writeIdx = m_readIdx = m_count = 0;
	}
	//--------------------------------------
	// ��������� ���������� ����� ��� ������
	//--------------------------------------
	VideoImageQueue::QueueImage* VideoImageQueue::getWritingImage()
	{
		return !m_queue[m_writeIdx].ready ? m_queue + m_writeIdx : nullptr;
	}
	//--------------------------------------
	// ������� ���� ��� ����������
	//--------------------------------------
	void VideoImageQueue::imageWritten()
	{
		if (!m_queue[m_writeIdx].ready)
		{
			m_queue[m_writeIdx].ready = true;
			if (++m_writeIdx >= m_queueSize)
			{
				m_writeIdx = 0;
			}
			std_interlocked_inc(&m_count);
		}
	}
	//--------------------------------------
	// ��������� ������������ ����� ��� ������
	//--------------------------------------
	VideoImageQueue::QueueImage* VideoImageQueue::getReadingImage()
	{
		return m_queue[m_readIdx].ready ? m_queue + m_readIdx : nullptr;
	}
	//--------------------------------------
	// ������� ���� ��� �����������
	//--------------------------------------
	void VideoImageQueue::imageRead()
	{
		if (m_queue[m_readIdx].ready)
		{
			m_queue[m_readIdx].ready = false;
			if (++m_readIdx >= m_queueSize)
			{
				m_readIdx = 0;
			}
			std_interlocked_dec(&m_count);
		}
	}
	//--------------------------------------
	// ������ � �������. ���� ��� ����� �� �������� ���������
	//--------------------------------------
	void VideoImageQueue::push(VideoImage* image)
	{
		QueueImage* queueImage;
		int offset = 0;

		if ((queueImage = getWritingImage()) != nullptr)
		{
			queueImage->image = image;

			imageWritten();
		}
		else
		{
			DELETEO(image);
		}
	}
	//--------------------------------------
	// ��������� �������� �� �������.
	//--------------------------------------
	VideoImage* VideoImageQueue::pop()
	{
		VideoImage* image = nullptr;
		QueueImage* queueImage;

		if ((queueImage = getReadingImage()) != nullptr)
		{
			image = queueImage->image;
			queueImage->image = nullptr;
			imageRead();
		}

		return image;
	}
}
//-----------------------------------------------

