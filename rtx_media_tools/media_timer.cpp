﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_timer.h"
#include <math.h>
#include "std_mutex.h"
#include "std_sys_env.h"

namespace media {
	//--------------------------------------
	// физический таймер для обслуживания мультимедийных инструментов
	//--------------------------------------
	class MetronomeDistributor : rtl::ITimerEventHandler
	{
		rtl::MutexWatch m_sync;
		rtl::Timer* m_timer;
		int m_logCounter;
		int m_maxProcessingTime;
		bool m_firstTick;
		uint32_t m_tickPeriod;

		// список
		volatile uint32_t m_linkCount;
		Metronome* m_firstLink;
		Metronome* m_lastLink;

		bool m_individual;

	private:
		void checkAndStop();
		// rtl::ITimerEventHandler interface
		virtual void timer_elapsed_event(rtl::Timer* sender, int tid);

	public:
		MetronomeDistributor(uint32_t ticks_period, bool individual);
		~MetronomeDistributor();

		uint32_t getMethronomeCount() { return m_linkCount; }
		void addMethronome(Metronome* handler);
		void removeMethronome(Metronome* handler);
		uint32_t getTicksPeriod() { return m_tickPeriod; }

		bool individual() { return m_individual; }
	};
	typedef rtl::ArrayT<MetronomeDistributor*> MetronomeDistributorList;
	//--------------------------------------
	// менеджер мультимедия таймеров
	//--------------------------------------
	class MetronomeService
	{
		static bool s_initialized;
		static rtl::MutexWatch s_lock;
		static MetronomeDistributorList s_mdList;	// список распределителей времени
		static MetronomeDistributorList s_imdList;
		static int s_maxThreads;				// количество процессоров
		static int s_redLine;				// красная линия после перехода которой создается новый плеер

	private:
		static MetronomeDistributor* findById(uintptr_t timerId, int& index);
		static MetronomeDistributor* findIndividualById(uintptr_t timerId, int& index);
		static MetronomeDistributor* getFreeMetronome(uint32_t ticksPeriod);

	public:
		static void initialize();
		static void destroy();

		static uintptr_t add(Metronome* handler, uint32_t ticks_period);
		static uintptr_t addIndividual(Metronome* handler, uint32_t ticks_period);
		static void remove(uintptr_t timerId, Metronome* handler);
	};
	//--------------------------------------
	//
	//--------------------------------------
	bool MetronomeService::s_initialized = false;
	rtl::MutexWatch MetronomeService::s_lock("mm-timer-man");
	MetronomeDistributorList MetronomeService::s_mdList;
	MetronomeDistributorList MetronomeService::s_imdList;
	int MetronomeService::s_maxThreads = 1;
	int MetronomeService::s_redLine = 0;
	//--------------------------------------
	//
	//--------------------------------------
	void MetronomeService::initialize()
	{
		if (s_initialized)
			return;

		s_maxThreads = std_get_processors_count();

		//	// если количество процессоров больше 12 то принудительно выставим максимальное
		//	// количество таймеров в 12 так как мультимедийные таймеры ограниченны 16 таймерами
		//	// и могут использоватся в других частях процесса
		//

		s_maxThreads *= 2;

		if (s_maxThreads > 24)
			s_maxThreads = 24;

		// выставим красную линию в 8 если количество таймеров больше 1
		if (s_maxThreads > 1)
			s_redLine = 8;

		// инициализируем глобальный таймер на проверку состояния мультимедийных таймеров
		//rtl::Timer::setTimer(timer_check_proc, nullptr, 0, 5000, true);

		MetronomeDistributor* timer = NEW MetronomeDistributor(10, false);
		s_mdList.add(timer);
		LOG_TIMER("MMTIME", "Create New timer %p freq 10", timer);

		timer = NEW MetronomeDistributor(20, false);
		s_mdList.add(timer);
		LOG_TIMER("MMTIME", "Create New timer %p freq 20", timer);

		timer = NEW MetronomeDistributor(30, false);
		s_mdList.add(timer);
		LOG_TIMER("MMTIME", "Create New timer %p freq 30", timer);

		timer = NEW MetronomeDistributor(40, false);
		s_mdList.add(timer);
		LOG_TIMER("MMTIME", "Create New timer %p freq 40", timer);

		s_initialized = true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void MetronomeService::destroy()
	{
		if (!s_initialized)
			return;

		rtl::MutexWatchLock lock(s_lock, __FUNCTION__);

		for (int i = 0; i < s_mdList.getCount(); i++)
		{
			MetronomeDistributor* timer = s_mdList[i];
			DELETEO(timer);
		}

		s_mdList.clear();

		for (int i = 0; i < s_imdList.getCount(); i++)
		{
			MetronomeDistributor* timer = s_imdList[i];
			DELETEO(timer);
		}

		s_imdList.clear();
	}
	//--------------------------------------
	// поиск по идентификатору инстанса
	//--------------------------------------
	MetronomeDistributor* MetronomeService::findById(uintptr_t timerId, int& index)
	{
		MetronomeDistributor* timer = nullptr;
		index = -1;

		//rtl::MutexLock lock(s_lock);
		rtl::MutexWatchLock lock(s_lock, __FUNCTION__);

		for (int i = 0; i < s_mdList.getCount(); i++)
		{
			if (s_mdList[i] == (MetronomeDistributor*)timerId)
			{
				index = i;
				timer = s_mdList[i];
			}
		}

		return timer;
	}
	//--------------------------------------
	// поиск по идентификатору инстанса
	//--------------------------------------
	MetronomeDistributor* MetronomeService::findIndividualById(uintptr_t timerId, int& index)
	{
		MetronomeDistributor* timer = nullptr;
		index = -1;

		//rtl::MutexLock lock(s_lock);
		rtl::MutexWatchLock lock(s_lock, __FUNCTION__);

		for (int i = 0; i < s_imdList.getCount(); i++)
		{
			if (s_imdList[i] == (MetronomeDistributor*)timerId)
			{
				index = i;
				timer = s_imdList[i];
			}
		}

		return timer;
	}
	//--------------------------------------
	// поиск подходящего таймера
	//--------------------------------------
	MetronomeDistributor* MetronomeService::getFreeMetronome(uint32_t ticks_period)
	{
		// первый цикл на простой перебор
		MetronomeDistributor* timer = nullptr;

		try
		{
			//rtl::MutexLock lock(s_lock);
			rtl::MutexWatchLock lock(s_lock, __FUNCTION__);

			for (int i = s_mdList.getCount() - 1; i >= 0; i--)
			{
				if (s_mdList[i]->individual())
				{
					continue;
				}

				timer = s_mdList[i];

				// проверим частоту таймера
				if (timer->getTicksPeriod() != ticks_period)
					continue;

				// проверим красную линию
				if (s_redLine == 0 || int(timer->getMethronomeCount()) < s_redLine)
				{
					goto theEnd;
				}
			}

			// если еще не достигли максимума для количества таймеров то создадим новый таймер
			if (s_mdList.getCount() < s_maxThreads)
			{
				timer = NEW MetronomeDistributor(ticks_period, false);
				s_mdList.add(timer);
				LOG_TIMER("MMTIME", "Create New timer %p freq %u", timer, ticks_period); // PLAY_FREQUENCY

				goto theEnd;
			}

			// второй цикл на поиск наименее нагруженного таймера
			uint32_t min_count = 0xFFFFFFFF;
			int index = -1;

			for (int i = s_mdList.getCount() - 1; i >= 0; i--)
			{
				if (s_mdList[i]->individual())
				{
					continue;
				}

				timer = s_mdList[i];
				if (timer->getMethronomeCount() < min_count)
				{
					min_count = timer->getMethronomeCount();
					index = i;
				}
			}

			// если не был найден таймер с нужной частотой в пределах max_timers то создадим НОВЫЙ таймер!
			// ВАЖНО: такой таймер будет один на все плееры с такой частотой пока не освободится место на
			// каком либо процессоре.

			if (index < 0 || index >= s_mdList.getCount())
				index = 0;

			for (int i = index; i < s_mdList.getCount(); i++)
			{
				timer = s_mdList[i];
				if (!timer->individual())
				{
					break;
				}
			}
		}
		catch (rtl::Exception& ex)
		{
			LOG_ERROR("MMTIME", "Searching free timer failed with exception %s", ex.get_message());
		}

	theEnd:
		if (timer == nullptr)
		{
			return nullptr;
		}

		if (timer->individual())
		{
			return nullptr;
		}

		return timer;
	}
	//--------------------------------------
	//
	//--------------------------------------
	uintptr_t MetronomeService::add(Metronome* handler, uint32_t ticks_period)
	{
		LOG_TIMER("MMTIME", "Adding timer handler %p...", handler);

		MetronomeDistributor* timer = getFreeMetronome(ticks_period);

		if (timer != nullptr)
		{
			timer->addMethronome(handler);
			LOG_TIMER("MMTIME", "timer handler %p added to timer %p", handler, timer);
		}
		else
			LOG_WARN("MMTIME", "TIMER IS nullptr WHILE ADDING TIMER USER!");


		return (uintptr_t)timer;
	}
	//--------------------------------------
	//
	//--------------------------------------
	uintptr_t MetronomeService::addIndividual(Metronome* handler, uint32_t ticks_period)
	{
		LOG_TIMER("MMTIME", "Adding timer handler %p...", handler);

		// первый цикл на простой перебор
		MetronomeDistributor* timer = nullptr;

		try
		{
			rtl::MutexWatchLock lock(s_lock, __FUNCTION__);

			for (int i = s_imdList.getCount() - 1; i >= 0; i--)
			{
				timer = s_imdList[i];
				// проверим частоту таймера
				if (timer->getTicksPeriod() != ticks_period)
					continue;

				if (timer->getMethronomeCount() == 0)
				{
					timer->addMethronome(handler);
					LOG_TIMER("MMTIME", "timer handler %p added to timer %p", handler, timer);
					return (uintptr_t)timer;
				}
			}

			timer = NEW MetronomeDistributor(ticks_period, true);
			s_imdList.add(timer);
			LOG_TIMER("MMTIME", "Create New timer %p freq %u", timer, ticks_period); // PLAY_FREQUENCY

			timer->addMethronome(handler);
			LOG_TIMER("MMTIME", "timer handler %p added to timer %p", handler, timer);
			return (uintptr_t)timer;
		}
		catch (rtl::Exception& ex)
		{
			LOG_ERROR("MMTIME", "Searching free timer failed with exception %s", ex.get_message());
			timer = nullptr;
		}

		return (uintptr_t)timer;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void MetronomeService::remove(uintptr_t timerId, Metronome* handler)
	{
		LOG_TIMER("MMTIME", "Remove timer handler %p timer id %p...", handler, timerId);

		int index = -1;
		MetronomeDistributor* timer = findById(timerId, index);

		if (timer != nullptr && index != -1)
		{
			timer->removeMethronome(handler);
			LOG_TIMER("MMTIME", "timer handler %p removed", handler);
		}
		else
		{
			timer = findIndividualById(timerId, index);
			if (timer != nullptr && index != -1)
			{
				timer->removeMethronome(handler);
				LOG_TIMER("MMTIME", "timer handler %p removed", handler);
			}
			else
			{
				LOG_WARN("MMTIME", "Invalid timer id %p", timerId);
			}
		}

	}
	//--------------------------------------
	//
	//--------------------------------------
	MetronomeDistributor::MetronomeDistributor(uint32_t ticks_period, bool individual) :
		m_sync("mm-timer"),
		m_timer(nullptr),
		m_firstTick(true),
		m_tickPeriod(ticks_period),
		m_linkCount(0),
		m_firstLink(nullptr),
		m_lastLink(nullptr)
	{
		m_logCounter = 0;
		m_maxProcessingTime = 0;
		m_individual = individual;
	}
	//--------------------------------------
	//
	//--------------------------------------
	MetronomeDistributor::~MetronomeDistributor()
	{
		checkAndStop();
	}
	//--------------------------------------
	//
	//--------------------------------------
	void MetronomeDistributor::addMethronome(Metronome* handler)
	{
		LOG_TIMER("MMTIME", "timer %p : Add handler %p...", this, handler);

		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		if (m_lastLink == nullptr)
		{
			m_firstLink = m_lastLink = handler;

			if (m_timer == nullptr)
			{
				m_timer = NEW rtl::Timer(true, this, Log, "m-timer");
			}
		}
		else
		{
			m_lastLink->m_mt_nextLink = handler;
			m_lastLink = handler;
		}

		m_linkCount++;

		if (!m_timer->isStarted())
			m_timer->start(PLAY_FREQUENCY);

		LOG_TIMER("MMTIME", "timer %p : handler added", this);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void MetronomeDistributor::removeMethronome(Metronome* handler)
	{
		LOG_TIMER("MMTIME", "timer %p : Remove handler %p...", this, handler);

		{
			rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

			Metronome* current = m_firstLink;
			Metronome* prev = nullptr;

			while (current != nullptr && current != handler)
			{
				prev = current;
				current = current->m_mt_nextLink;
			}

			if (current != nullptr)
			{
				if (prev != nullptr)
				{
					prev->m_mt_nextLink = current->m_mt_nextLink;

					// если был последним то обновим ссылку на последнего
					if (current == m_lastLink)
						m_lastLink = prev;
				}
				else
				{
					m_firstLink = current->m_mt_nextLink;

					// если единственный то ссылка на последнего тоже обнуляется
					if (m_firstLink == nullptr)
						m_lastLink = nullptr;
				}

				// У current специально не обнуляем ссылку, чтобы таймер не остановился,
				// дойдя до него в параллельном процессе в текущее время

				m_linkCount--;
			}
			// else сессии нет в списке
		}

		checkAndStop();

		LOG_TIMER("MMTIME", "timer %p : handler removed", this, handler);
	}
	//--------------------------------------
	// обработчик таймера
	//--------------------------------------
	void MetronomeDistributor::timer_elapsed_event(rtl::Timer* sender, int tid)
	{
		//LOG_TIMER("MMTIME", "timer %p : Tick timer", this);

		if (m_linkCount == 0)
			return;

		if (m_firstTick)
		{
			m_firstTick = false;

			LOG_TIMER("MMTIME", "timer %p : Tick timer", this);
		}

		uint32_t start = rtl::DateTime::getTicks();

		Metronome* current = m_firstLink;

		while (current != nullptr)
		{
			if (!current->isMetronomePaused() || (current->isMetronomeStopping()))
			{
				current->timerElapsed();
			}
			current = current->m_mt_nextLink;
		}

		int diff = start - rtl::DateTime::getTicks();

		if (diff > m_maxProcessingTime)
		{
			m_maxProcessingTime = diff;

			LOG_TIMER("MMTIME", "timer %p : -----> Max Processing time is %f ms for %d users", this, m_maxProcessingTime, m_linkCount);
		}
		else if (++m_logCounter > 125 && (m_logCounter % 125) == 0)
		{
			LOG_TIMER("MMTIME", "timer %p : -----> Current Processing time is %f ms for %d users", this, diff, m_linkCount);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void MetronomeDistributor::checkAndStop()
	{
		rtl::Timer* timer = nullptr;

		{
			rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

			if (m_linkCount == 0 && m_timer != nullptr)
			{
				timer = m_timer;
				m_timer = nullptr;

				LOG_TIMER("MMTIME", "timer %p : timer removed", this);
			}

		}

		if (timer != nullptr)
		{
			timer->stop();
			DELETEO(timer);
			timer = nullptr;

			LOG_TIMER("MMTIME", "timer %p : timer deleted", this, timer);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	Metronome::Metronome(rtl::Logger& log) :
		m_timerId(0),
		m_mt_nextLink(nullptr),
		m_timerPaused(false),
		m_timerStopping(false),
		m_timerStopped(true),
		m_timerLog(log)
	{
		MetronomeService::initialize();
	}
	//--------------------------------------
	//
	//--------------------------------------
	Metronome::~Metronome()
	{
		metronomeStop();
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Metronome::metronomeStart(uint32_t ticks_period, bool individual)
	{
		if (m_timerId != 0)
		{
			MetronomeService::remove(m_timerId, this);
		}

		LOG_TIMER("MMTIME", "timer %p : Starting with period %u ms...", this, ticks_period);

		m_timerStopping = false;
		m_timerPaused = false;
		if (individual)
		{
			m_timerId = MetronomeService::addIndividual(this, ticks_period);
		}
		else
		{
			m_timerId = MetronomeService::add(this, ticks_period);
		}
		m_timerStopped = (m_timerId == 0);

		LOG_TIMER("MMTIME", "timer %p : Started", this);

		return m_timerId != 0;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Metronome::metronomeStop()
	{
		if (m_timerId != 0)
		{
			LOG_TIMER("MMTIME", "timer %p : Stopping...", this);

			m_timerStopping = true;

			uint32_t started = rtl::DateTime::getTicks();
			uint32_t delay = 0;

			while (!m_timerStopped)
			{
				//Peter. 
				// С учетом большой нагрузки на ядро QueryPerformanceCounter
				// можно и обычный GetTickCount использовать здесь, не шибко критичное значение 2000.
				if ((delay = rtl::DateTime::getTicks() - started) > 2000)
					break;

				rtl::Thread::sleep(5);
			}

			if (delay > 2000)
			{
				LOG_TIMER("MMTIME", "timer %p : Stop data pumping delay(%u) > 2000 ms.", this, delay);
			}

			MetronomeService::remove(m_timerId, this);

			m_timerId = 0;
			m_mt_nextLink = nullptr;

			LOG_TIMER("MMTIME", "timer %p : Stopped", this);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Metronome::metronomePause()
	{
		if (m_timerId != 0)
		{
			m_timerPaused = true;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Metronome::metronomeResume()
	{
		if (m_timerId != 0)
		{
			m_timerPaused = false;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Metronome::initializeMetronomeManager()
	{
		MetronomeService::initialize();
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Metronome::destroyMetronomeManager()
	{
		MetronomeService::destroy();
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Metronome::timerElapsed()
	{
		// сначала проверим на прекращение использования таймера
		if (m_timerStopping)
		{
			// подтверждаем останов
			m_timerStopped = true;
			return;
		}

		// таймер остановлен?
		if (m_timerPaused)
		{
			// таймер временно остановлен
			return;
		}

		// вызываем обработчик
		metronomeTicks();
	}
}
//--------------------------------------
