/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_buffered_writer_raw.h"

namespace media
{
	//--------------------------------------
	//
	//--------------------------------------
	BufferedMediaWriterRaw::BufferedMediaWriterRaw(rtl::Logger* log) : BufferedMediaWriter(log)
	{
	}
	//--------------------------------------
	//
	//--------------------------------------
	BufferedMediaWriterRaw::~BufferedMediaWriterRaw()
	{
		destroy();
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool BufferedMediaWriterRaw::create(const char* path, int bufsize)
	{
		PLOG_CALL("raw-rec", "Create file : cache %d ms, path:%s", bufsize, path);

		if (!m_file.createNew(path))
		{
			PLOG_WARNING("raw-rec", "raw recorder : Create raw file failed.");
			return false;
		}

		PLOG_CALL("raw-rec", "File created and updated");

		//#ifdef _DEBUG
		rtl::String ppath = path;
		ppath += "_d";
		m_dout.createNew(ppath);
		//#endif

		BufferedMediaWriter::initialize(bufsize);

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void BufferedMediaWriterRaw::close()
	{
		PLOG_CALL("raw-rec", "Closing file...");

		flush(true);

		m_file.close();
		//#ifdef _DEBUG
		m_dout.close();
		//#endif
		PLOG_CALL("raw-rec", "File closed");
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool BufferedMediaWriterRaw::write_packet(const rtp_packet* packet)
	{
		if (packet == nullptr)
		{
			return false;
		}


		if (m_started)
		{
			int payloadLength = packet->get_payload_length();
			const uint8_t* payload = packet->get_payload();

			int written = m_cache.write(payload, payloadLength);

			if (written != payloadLength)
			{
				PLOG_WARNING("rtp-rec", "Write packet to cache failed : writen (%d) != len (%d)", written, payloadLength);
				m_lost_in_buffer += payloadLength - written;
			}
			else
			{
				PLOG_FLAG4("rtp-rec", "Packet(%d) written to cache : block size %d bytes", packet->get_payload_type(), payloadLength);
			}

			m_written_to_buffer += written;

			//#ifdef _DEBUG
			m_dout.write(payload, payloadLength);
			//#endif
		}

		// ������ ���������� ������ 
		return true;

	}
	//--------------------------------------
	//
	//--------------------------------------
	void BufferedMediaWriterRaw::recFileStarted()
	{
	}
	//--------------------------------------
	//
	//--------------------------------------
	void BufferedMediaWriterRaw::recFileStopped()
	{
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool BufferedMediaWriterRaw::recFileWrite(void* dataBlock, uint32_t length, uint32_t* written)
	{
		if (!m_file.isOpened())
		{
			PLOG_WARNING("raw-rec", "Write block failed : file not open!");
			return false;
		}

		if ((*written = (uint32_t)m_file.write(dataBlock, length)) == length)
		{
			PLOG_FLAG4("raw-rec", "block written size %u bytes", *written);
			m_written_to_file += *written;
			return true;
		}

		m_written_to_file += *written;
		m_lost_in_file += length - *written;

		PLOG_WARNING("raw-rec", "Can't write to file! %d -> %d", length, *written);

		return false;
	}
}
//--------------------------------------
