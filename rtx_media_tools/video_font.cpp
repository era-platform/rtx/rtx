/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_video_font.h"

namespace media
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void PixelMap::load(const GlyphInfo& glyph)
	{
		clean();

		m_width = glyph.width;
		m_height = glyph.height;

		m_map = (uint8_t*)MALLOC(m_width * m_height);
		const uint8_t* bits = glyph.bitstream;
		const uint8_t* end = glyph.bitstream + glyph.rowWidth * glyph.height;
		int x = 0;
		int y = 0;
		while (bits < end)
		{
			writeByte(x, y, *bits++);
			y++;
			if (y == m_height)
			{
				x += 8; // next column
				y = 0;
			}
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void PixelMap::clean()
	{
		if (m_map != nullptr)
		{
			FREE(m_map);
			m_map = nullptr;
		}

		m_height = m_width = 0;
		m_map_length = 0;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void PixelMap::writeByte(int x, int y, uint8_t bits)
	{
		int index = x + y * m_width;

		uint8_t mask = 0x80;
		int cycles = x + 8 < m_width ? 8 : m_width - x;
		while (cycles--)
		{
			m_map[index++] = (bits & mask) > 0;
			mask >>= 1;
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void Font::createFont(const char* faceName, int height, const FontInfo* info)
	{
		m_faceName = faceName;
		m_fontHeight = height;
		m_info = info;
		m_firstChar = info->getFirstChar();

		for (int i = m_firstChar; i <= info->GetLastChar(); i++)
		{
			PixelMap* pixmap = NEW PixelMap();
			pixmap->load(info->getGlyph(i));
			m_pixmap.add(pixmap);
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void Font::destroy()
	{
		for (int i = 0; i < m_pixmap.getCount(); i++)
		{
			DELETEO(m_pixmap[i]);
		}

		m_pixmap.clear();
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	int Font::addRef()
	{
		return std_interlocked_inc(&m_refCount);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	int Font::release()
	{
		return std_interlocked_dec(&m_refCount);
	}

	int Font::getTextWidth(const char* zstr) const
	{
		int width = 0;
		const PixelMap* pmap;

		while (*zstr)
		{
			if (pmap = getCharPixelMap(*zstr++))
			{
				width +=  pmap->getWidth() + 1;
			}
		}

		return width;
	}
}
//-----------------------------------------------
