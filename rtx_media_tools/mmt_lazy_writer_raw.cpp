/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_lazy_writer_raw.h"

namespace media
{
	//--------------------------------------
   //
   //--------------------------------------
	LazyMediaWriterRaw::LazyMediaWriterRaw(rtl::Logger* log) : LazyMediaWriter(log)
	{
	}
	//--------------------------------------
	//
	//--------------------------------------
	LazyMediaWriterRaw::~LazyMediaWriterRaw()
	{
		destroy();
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool LazyMediaWriterRaw::create(const char* path, int bufsize)
	{
		PLOG_CALL("raw-rec", "Create file...");

		if (bufsize < 100 || bufsize > 5000)
		{
			bufsize = 500;
		}

		if (!m_file.createNew(path))
		{
			PLOG_WARNING("raw-rec", "raw recorder : Create raw file failed.");
			return false;
		}

		PLOG_CALL("raw-rec", "File created and updated");

		LazyMediaWriter::initialize();

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void LazyMediaWriterRaw::close()
	{
		PLOG_CALL("raw-rec", "Closing file...");

		m_file.close();

		PLOG_CALL("raw-rec", "File closed");
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool LazyMediaWriterRaw::write_packet(const rtp_packet* packet)
	{
		if (packet == nullptr)
		{
			return false;
		}


		if (m_started)
		{
			//PLOG_CALL("raw-rec", "Write packet (length %d) to cache...", packet->get_payload_length());

			m_write = true;

			int payloadLength = packet->get_payload_length();
			// ������� ���� ���� ��� ������ ������ + ���������
			LazyBlock* block = get_current_block(payloadLength);

			if (block != nullptr)
			{
				// ������� ���������� � ������ � ���������
				const uint8_t* payload = packet->get_payload();

				uint8_t* rec_buffer = block->rec_pos;
				int rec_size = CACHE_BLOCK_SIZE - block->rec_length;

				memcpy(rec_buffer, payload, payloadLength);

				// ������� ��������� �� ��������� ����� � �����
				block->rec_pos += payloadLength;
				block->rec_length += payloadLength;
			}
			else
			{
				PLOG_WARNING("raw-rec", "Write packet to cache failed : cache is full -- packet dropped");
			}

			m_write = false;
		}

		// ������ ���������� ������ 
		return true;

	}
	//--------------------------------------
	//
	//--------------------------------------
	void LazyMediaWriterRaw::rec_file_started()
	{
	}
	//--------------------------------------
	//
	//--------------------------------------
	void LazyMediaWriterRaw::rec_file_stopped()
	{
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool LazyMediaWriterRaw::rec_file_write(void* dataBlock, uint32_t length, uint32_t* written)
	{
		PLOG_CALL("raw-rec", "Write block (len:%d) to file...", length);

		if (m_file.write(dataBlock, length) > 0)
		{
			*written = length;
			PLOG_CALL("raw-rec", "Block written (len:%d) to file...", *written);
			return true;
		}

		*written = 0;
		PLOG_WARNING("raw-rec", "Can't write header to file!");

		return false;
	}
}
//--------------------------------------
