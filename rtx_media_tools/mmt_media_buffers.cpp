﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_media_buffers.h"

namespace media
{
	//--------------------------------------
	// буфер для псм данных (очередь FIFO) с произвольной длиной чтения/записи
	//--------------------------------------
	bool SamplesBuffer::create(int depth)
	{
		if (m_buffer != NULL)
		{
			FREE(m_buffer);
		}

		m_head = m_tail = m_length = 0;
		m_buffer = (short*)MALLOC((m_size = depth) * sizeof(short));
		memset(m_buffer, 0, m_size * sizeof(short));

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void SamplesBuffer::reset()
	{
		m_head = m_tail = m_length = 0;

		if (m_buffer != NULL)
		{
			memset(m_buffer, 0, m_size * sizeof(short));
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	//int SamplesBuffer::GetSamplesCount();
	//--------------------------------------
	// запись звука.
	// можно записывать блоками не превышающих размер самого буфера
	//--------------------------------------
	int SamplesBuffer::write(const short* samples, int count)
	{
		if (count > m_size)
		{
			// слишком много данных
			return 0;
		}

		//LOG_CALL("MB", "---> WRITE %d samples", count);

		if (m_head + count < m_size)
		{
			// 1 копируем весь буфер
			memcpy(m_buffer + m_head, samples, count * sizeof(short));
			m_head += count;
		}
		else
		{
			// 2 копируем данные в два приема
			int part2 = (m_head + count) - m_size;
			int part1 = count - part2;

			memcpy(m_buffer + m_head, samples, part1 * sizeof(short));

			// вычисление новой головы
			if (part2 > 0)
			{
				memcpy(m_buffer, samples + part1, part2 * sizeof(short));
				m_head = part2;
			}
			else
			{
				m_head = 0;
			}
		}

		// вычисление длины
		m_length += count;
		if (m_length > m_size)
		{
			// если превысили размер буфера то длину установим в размер буфера (заполнен)
			m_length = m_size;
		}

		// корректируем указатель хвоста
		if (m_length == m_size)
		{
			// если данные затираются то подвинем хвост
			m_tail = m_head;
		}

		//LOG_CALL("MB", "---> WRITE head %d tail %d length %d", m_head, m_tail, m_length);

		return count;
	}
	//--------------------------------------
	// чтение данных.
	// возврат: количество возвращаемых семплов в буфере 
	//--------------------------------------
	int SamplesBuffer::read(short* buffer, int count)
	{
		// исправляем возможную длину!
		if (count > m_length)
		{
			count = m_length;
		}

		//LOG_CALL("MB", "---> READ  %d samples", count);

		if (count > 0)
		{
			if (m_tail + count < m_size)
			{
				// 1 копируем весь буфер
				memcpy(buffer, m_buffer + m_tail, count * sizeof(short));
				// вычисление нового хвоста
				m_tail += count;
			}
			else
			{
				// 2 копируем данные в два приема
				int part2 = (m_tail + count) - m_size;
				int part1 = count - part2;

				memcpy(buffer, m_buffer + m_tail, part1 * sizeof(short));
				if (part2 > 0)
				{
					memcpy(buffer + part1, m_buffer, part2 * sizeof(short));
					// вычисление нового хвоста
					m_tail = part2;

					if (m_tail > m_size)
						m_tail -= m_size;

				}
				else
					m_tail = 0;
			}

			// вычисляем длину (уменьшаем)
			m_length -= count;

			// хвост и голову не надо корректировать
			// так как сразу была скорректированна длина буфера!

			if (m_length == 0)
				m_head = m_tail = 0;

			//LOG_CALL("MB", "<--- READ  head %d tail %d length %d", m_head, m_tail, m_length);
		}

		return count;
	}
	//--------------------------------------
	// буфер для псм данных (очередь FIFO) с произвольной длиной чтения/записи
	//--------------------------------------
	bool AudioBuffer::create(int depth)
	{
		if (m_buffer != NULL)
		{
			FREE(m_buffer);
		}
		m_head = m_tail = m_length = 0;
		m_buffer = (uint8_t*)MALLOC(m_size = depth);
		memset(m_buffer, 0, m_size);

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void AudioBuffer::reset()
	{
		m_head = m_tail = m_length = 0;

		if (m_buffer != NULL)
		{
			memset(m_buffer, 0, m_size);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	//int AudioBuffer::GetAudioDataLength();
	//--------------------------------------
	// запись звука.
	// можно записывать блоками не превышающих размер самого буфера
	//--------------------------------------
	int AudioBuffer::write(const uint8_t* audioData, int length)
	{
		if (length > m_size)
		{
			return 0;
		}

		if (m_head + length < m_size)
		{
			// 1 копируем весь буфер
			memcpy(m_buffer + m_head, audioData, length);
			m_head += length;
		}
		else
		{
			// 2 копируем данные в два приема
			int part2 = (m_head + length) - m_size;
			int part1 = length - part2;

			memcpy(m_buffer + m_head, audioData, part1);

			// вычисление новой головы
			if (part2 > 0)
			{
				memcpy(m_buffer, audioData + part1, part2);
				m_head = part2;
			}
			else
				m_head = 0;

		}

		// вычисление длины
		m_length += length;
		if (m_length > m_size)
		{
			// если превысили размер буфера то длину установим в размер буфера (заполнен)
			m_length = m_size;
		}

		// корректируем указатель хвоста
		if (m_length == m_size)
		{
			// если данные затираются то подвинем хвост
			m_tail = m_head;
		}

		return length;
	}
	//--------------------------------------
	// чтение данных.
	// возврат: количество возвращаемых семплов в буфере 
	//--------------------------------------
	int AudioBuffer::read(uint8_t* buffer, int size)
	{
		// исправляем возможную длину!
		if (size > m_length)
		{
			size = m_length;
		}

		if (size > 0)
		{
			if (m_tail + size < m_size)
			{
				// 1 копируем весь буфер
				memcpy(buffer, m_buffer + m_tail, size);
				// вычисление нового хвоста
				m_tail += size;
			}
			else
			{
				// 2 копируем данные в два приема
				int part2 = (m_tail + size) - m_size;
				int part1 = size - part2;

				memcpy(buffer, m_buffer + m_tail, part1);
				if (part2 > 0)
				{
					memcpy(buffer + part1, m_buffer, part2);
					// вычисление нового хвоста
					m_tail = part2;

					if (m_tail > m_size)
						m_tail -= m_size;
				}
				else
					m_tail = 0;
			}

			// вычисляем длину (уменьшаем)
			m_length -= size;

			if (m_length == 0)
				m_tail = m_head = 0;

			// хвост и голову не надо корректировать
			// так как сразу была скорректированна длина буфера!
		}

		return size;
	}
	//--------------------------------------
	// создаем очередь заданной длины
	//--------------------------------------
	void SamplesQueue::create(int queue_length)
	{
		if (m_queue != NULL)
		{
			FREE(m_queue);
		}

		m_queueSize = queue_length / SAMPLES_QUEUE_BSIZE;
		m_queue = (QueueBlock*)MALLOC(m_queueSize * sizeof(QueueBlock));
		memset(m_queue, 0, m_queueSize * sizeof(QueueBlock));
		m_writeIdx = m_readIdx = 0;
		m_data_count = 0;
	}
	//--------------------------------------
	// сброс блоков
	//--------------------------------------
	void SamplesQueue::reset()
	{
		if (m_queue != NULL)
		{
			FREE(m_queue);
			m_queue = NULL;
		}

		m_queueSize = m_writeIdx = m_readIdx = 0;
		m_data_count = 0;
	}
	//--------------------------------------
	// получение свободного блока для записи:
	// если текущий блок свободен то вернем его
	//--------------------------------------
	//SamplesQueue::BLOCK* SamplesQueue::GetNextWriteBlock();
	//--------------------------------------
	// отметим блок как записанный
	//--------------------------------------
	void SamplesQueue::blockWritten()
	{
		if (!m_queue[m_writeIdx].ready)
		{
			m_queue[m_writeIdx++].ready = true;
			if (m_writeIdx >= m_queueSize)
			{
				m_writeIdx = 0;
			}
			std_interlocked_inc(&m_data_count);
		}
	}
	//--------------------------------------
	// получение заполненного блока для чтения:
	// если текущий блок свободен то вернем его
	//--------------------------------------
	//SamplesQueue::BLOCK* SamplesQueue::GetNextReadBlock();
	//--------------------------------------
	// отметим блок как прочитанный
	//--------------------------------------
	void SamplesQueue::blockRead()
	{
		if (m_queue[m_readIdx].ready)
		{
			m_queue[m_readIdx++].ready = false;
			if (m_readIdx >= m_queueSize)
			{
				m_readIdx = 0;
			}
			std_interlocked_dec(&m_data_count);
		}
	}
	//--------------------------------------
	// запись в очередь. если данные не кратны 80(SAMPLES_QUEUE_BSIZE) байтам то лишние данные отбрасываются
	//--------------------------------------
	int SamplesQueue::write(const short* audioData, int length)
	{
		QueueBlock* block;
		int offset = 0;

		while ((block = getWritingBlock()) != nullptr)
		{
			if (length - offset < SAMPLES_QUEUE_BSIZE)
				break;

			memcpy(block->buffer, audioData + offset, SAMPLES_QUEUE_BSIZE * sizeof(short));
			offset += SAMPLES_QUEUE_BSIZE;

			blockWritten();
		}

		return offset;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int SamplesQueue::read(short* audioData, int size)
	{
		QueueBlock* block;
		int offset = 0;

		while ((block = getReadingBlock()) != nullptr)
		{
			if (size - offset < SAMPLES_QUEUE_BSIZE)
				break;

			memcpy(audioData + offset, block->buffer, SAMPLES_QUEUE_BSIZE * sizeof(short));
			offset += SAMPLES_QUEUE_BSIZE;

			blockRead();
		}

		return offset;
	}
	//--------------------------------------
	// создаем очередь заданной длины
	//--------------------------------------
	void AudioQueue::create(int queue_length)
	{
		if (m_queue != NULL)
		{
			FREE(m_queue);
		}

		m_queueSize = queue_length / SAMPLES_QUEUE_BSIZE;
		m_queue = (QueueBlock*)MALLOC(m_queueSize * sizeof(QueueBlock));
		memset(m_queue, 0, m_queueSize * sizeof(QueueBlock));
		m_writeIdx = m_readIdx = 0;
		m_blockCount = 0;
	}
	//--------------------------------------
	// сброс блоков
	//--------------------------------------
	void AudioQueue::reset()
	{
		if (m_queue != NULL)
		{
			FREE(m_queue);
			m_queue = NULL;
		}

		m_queueSize = m_writeIdx = m_readIdx = 0;
		m_blockCount = 0;
	}
	//--------------------------------------
	// получение свободного блока для записи:
	// если текущий блок свободен то вернем его
	//--------------------------------------
	//AudioQueue::BLOCK* AudioQueue::GetNextWriteBlock();
	//--------------------------------------
	// отметим блок как записанный
	//--------------------------------------
	void AudioQueue::blockWritten()
	{
		if (!m_queue[m_writeIdx].ready)
		{
			m_queue[m_writeIdx++].ready = true;
			if (m_writeIdx >= m_queueSize)
			{
				m_writeIdx = 0;
			}
			std_interlocked_inc(&m_blockCount);
		}
	}
	//--------------------------------------
	// получение заполненного блока для чтения:
	// если текущий блок свободен то вернем его
	//--------------------------------------
	//AudioQueue::BLOCK* AudioQueue::GetNextReadBlock();
	//--------------------------------------
	// отметим блок как прочитанный
	//--------------------------------------
	void AudioQueue::blockRead()
	{
		if (m_queue[m_readIdx].ready)
		{
			m_queue[m_readIdx++].ready = false;
			if (m_readIdx >= m_queueSize)
			{
				m_readIdx = 0;
			}
			std_interlocked_dec(&m_blockCount);
		}
	}
	//--------------------------------------
	// запись в очередь. если данные не кратны 80 байтам то лишние данные отбрасываются
	//--------------------------------------
	int AudioQueue::write(const uint8_t* audioData, int length)
	{
		QueueBlock* block;
		int offset = 0;

		while ((block = getWritingBlock()) != nullptr)
		{
			if (length - offset < SAMPLES_QUEUE_BSIZE)
				break;

			memcpy(block->buffer, audioData + offset, SAMPLES_QUEUE_BSIZE);
			offset += SAMPLES_QUEUE_BSIZE;

			blockWritten();
		}

		return offset;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int AudioQueue::read(uint8_t* audioData, int size)
	{
		QueueBlock* block;
		int offset = 0;

		while ((block = getReadingBlock()) != nullptr)
		{
			if (size - offset < SAMPLES_QUEUE_BSIZE)
				break;

			memcpy(audioData + offset, block->buffer, SAMPLES_QUEUE_BSIZE);
			offset += SAMPLES_QUEUE_BSIZE;

			blockRead();
		}

		return offset;
	}
	//--------------------------------------
	// могут дать ошибку если запросит поток читатель
	//--------------------------------------
	//int AudioQueue::GetAvailableLength();
	//--------------------------------------
	// могут дать ошибку если запросит поток писатель
	//--------------------------------------
	//int AudioQueue::GetFreeSize();
	//--------------------------------------
	// конструктор
	//--------------------------------------
	JitterQueue::JitterQueue(rtl::Logger& log) : m_log(log), m_new(true), m_numberOut(0), m_numberIn(0),
		m_depthPackets(0), m_depthMedia(0), m_inPacketSize(160), m_inMediaDepth(0),
		m_outPacketCount(0), m_outIndex(0)
	{
		memset(m_inList, 0, sizeof m_inList);
		memset(m_outList, 0, sizeof m_outList);
	}
	//--------------------------------------
	//
	//--------------------------------------
	JitterQueue::~JitterQueue()
	{
		reset();
	}
	//--------------------------------------
	//
	//--------------------------------------
	void JitterQueue::setPacketDepth(int depth)
	{
		if (m_new)
		{
			m_depthPackets = depth != 0 ?
				depth > MAX_JITTER_SIZE ?
				MAX_JITTER_SIZE :
				depth < MIN_JITTER_SIZE ?
				MIN_JITTER_SIZE :
				depth : 0;

			m_depthMedia = m_depthPackets * m_inPacketSize; // 160;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	JitterState JitterQueue::push(JitterPacket* packet)
	{
		if (m_depthPackets == 0)
			return JitterState::Pass;

		uint32_t current_time = rtl::DateTime::getTicks();

		if (m_new)
		{
			// если пакет новый то сразу возвращаем с запоминанием номера последовательности
			m_numberOut = m_numberIn = packet->seqNo;
			m_new = false;
			m_inPacketSize = packet->count;
			m_inMediaDepth = 0;
			m_lastPacketTime = current_time;

			return JitterState::Pass;
		}

		// остальные пакеты проверяем на пропуски в порядке поступления

		uint16_t seqNo = packet->seqNo;

		short seq_diff = short(seqNo - m_numberOut);

		// если пакет пришел с опозданием --> а может это новая последовательность?
		if (seq_diff < 0)
		{
			//LOG_VOICE(L"jitter", L"drop: n=%d, nout=%d", seqNo, m_numberOut);

			// проверим время пакета с последним успешным пакетом
			if (current_time - m_lastPacketTime > 160) // 20 * 8
			{
				//...
				reset();

				m_numberOut = m_numberIn = seqNo;
				m_inMediaDepth = 0;
				m_lastPacketTime = current_time;
				return JitterState::Pass;
			}

			// если пакет пришел с опозданием --> отбрасываем его
			return JitterState::Drop;
		}

		int samples_count = packet->count;

		// джиттер пустой
		if (m_numberIn == m_numberOut)
		{
			// пакет пришел в правильной последовательности --> сразу пропускаем
			// 09.05.2011 Peter. После 65535 0 капчурится, а не пропускается. Видимо из-за нешортовости операции сложения.
			if (seqNo == (uint16_t)(m_numberOut + 1))
			{
				m_numberOut = m_numberIn = seqNo;
				m_inMediaDepth = 0;
				m_lastPacketTime = current_time;
				return JitterState::Pass;
			}

			// пакет пришел с опережением --> начинаем заполнять джиттер
			// OK
			m_numberIn = seqNo;
			m_numberOut++;

			short pos = seqNo - m_numberOut;

			if (pos >= m_depthPackets)
			{
				// сдвигаем джиттер!
				pos = m_depthPackets - 1;
				m_numberOut = m_numberIn - pos - 1;//peter '+= number_in - pos' -> '= number_in - pos - 1'
			}


			m_inMediaDepth = (pos * m_inPacketSize) + samples_count;

			if (pos >= 0 && pos < m_depthPackets)
			{
				//packet->AddRef();
				m_inList[pos] = packet;
			}
			else
				LOG_WARN("jitter", "write position is out of index (1)!");

			//LOG_VOICE(L"jitter", L"captured: nout=%d, nin=%d", m_numberOut, m_numberIn);
			m_lastPacketTime = current_time;
			return JitterState::Captured;
		}

		if (short(seqNo - m_numberIn) < 0)
		{
			// пакет успел в джиттер
			short pos = seqNo - m_numberOut;
			if (pos >= 0 && pos < m_depthPackets)
			{
				//packet->AddRef();
				m_inList[pos] = packet;
			}
			else
				LOG_WARN("jitter", "write position is out of index (2)!");
		}
		else if (seqNo < m_numberOut + m_depthPackets && m_inMediaDepth + samples_count <= m_depthMedia)
		{
			short pos = seqNo - m_numberOut;

			if (pos >= 0 && pos < m_depthPackets)
			{
				//packet->AddRef();
				m_inList[pos] = packet;
			}
			else
				LOG_WARN("jitter", "write position is out of index (3)!");

			short diff = seqNo - m_numberIn;
			if (diff > 0)
			{
				m_inMediaDepth += m_inPacketSize * diff;
				m_numberIn = seqNo;
			}
			m_inMediaDepth += samples_count;
		}
		////08.05.2011 Peter
		////  Существовало для проверки наличия бага с отрицательным count в ShiftToOuter()
		////  Логирование номеров пакетов показало, что рубить не надо, надо правильно считать в ushort а не в uint32_t.
		////  А отрицательные count подтвердились.
		//else if ( seqNo - m_numberIn < 0 )
		//{
		//	LOG_WARN(L"jitter", L"Negative count got pushed to outer! %d, %d, %d, %d", seqNo, m_outPacketCount, m_numberOut, m_numberIn);
		//	return JTB_DROP;
		//}
		else
		{
			// пакет выталкивает из джиттера в исходящий буфер n пакетов!
			// ???
			// может не поместится в медия

			//08.05.2011 Peter
			//uint32_t shift = seqNo - m_numberIn;
			uint16_t shift = seqNo - m_numberIn;
			shiftToOuter(shift);

			// выясняем наскольно рано пришел пакет!
			short pos = seqNo - m_numberOut;

			if (pos >= m_depthPackets)//Peter '>' -> '>='
			{
				m_numberIn = seqNo;
				pos = m_depthPackets - 1;
				m_numberOut = m_numberIn - pos - 1;//peter '+= number_in - pos' -> '= number_in - pos - 1'
			}

			m_numberIn = seqNo;
			m_inMediaDepth += samples_count;

			if (pos >= 0 && pos < m_depthPackets)
			{
				//packet->AddRef();
				m_inList[pos] = packet;
			}
			else
				LOG_WARN("jitter", "write position is out of index (4)! %d, %d, %d, %d", seqNo, pos, m_numberOut, m_numberIn);
		}

		// проверим джиттер на готовый пакеты и перенесем их в исходящий буфер

		int i = 0;

		for (; i < m_depthPackets; i++)
		{
			if (m_inList[i] == NULL)
				break;
		}

		if (i > 0)
			shiftToOuter(i);

		//LOG_VOICE(L"jitter", L"passed: nout=%d, nin=%d", m_numberOut, m_numberIn);
		// вернем READY если он не пустой.

		m_lastPacketTime = current_time;

		return m_outPacketCount > 0 ? JitterState::Ready : JitterState::Captured;
	}
	//--------------------------------------
	//
	//--------------------------------------
	JitterPacket* JitterQueue::getFirst()
	{
		m_outIndex = 0;

		JitterPacket* packet = m_outList[m_outIndex];
		m_outList[m_outIndex++] = NULL;

		return packet;
	}
	//--------------------------------------
	//
	//--------------------------------------
	JitterPacket* JitterQueue::getNext()
	{
		if (m_outIndex >= MAX_JITTER_SIZE)
			return NULL;

		JitterPacket* packet = m_outList[m_outIndex];

		m_outList[m_outIndex++] = NULL;

		return packet;
	}
	//--------------------------------------
	// сдвиг готовых пакетов в исходящий буфер
	//--------------------------------------
	void JitterQueue::shiftToOuter(int count)
	{
		// ищем последний пакет
		for (m_outPacketCount = MAX_JITTER_SIZE - 1; m_outPacketCount >= 0; m_outPacketCount--)
		{
			if (m_outList[m_outPacketCount] != NULL)
				break;
		}

		// получим количество элементов
		m_outPacketCount++;

		////08.05.2011 Peter. VideoClient Exception в memmove.
		////  Существовало для проверки наличия бага с отрицательным count в ShiftToOuter()
		////  Логирование номеров пакетов показало, что рубить не надо, надо правильно считать в ushort а не в uint32_t.
		////  А отрицательные count подтвердились. 
		if (count < 0)
		{
			LOG_WARN("jitter", "ShiftToOuter got negative count! %d, %d, %d, %d", count, m_outPacketCount, m_numberOut, m_numberIn);
			//??? что бы сделать тут защитного? 
			// пока так, но вообще в функции PushPacket провел анализ на отрицательность, сюда не должны попасть
			count = m_depthPackets;
			count = 0;
		}
		// если нужно сдвинуть весь буфер или даже больше то просто чистим его
		else if ((m_outPacketCount + count > MAX_JITTER_SIZE * 2) || (count >= MAX_JITTER_SIZE))
		{
			for (int i = 0; i < m_outPacketCount; i++)
			{
				if (m_outList[i] != NULL)
					//m_outList[i]->release();
					DELETEO(m_outList[i]);
			}

			memset(m_outList, 0, MAX_JITTER_SIZE * sizeof(rtp_packet*));
			m_outPacketCount = 0;
		}
		// если в исходящем буфере нет места то освобождаем пакеты от начала
		else if (m_outPacketCount + count > MAX_JITTER_SIZE)
		{
			int to_drop = (m_outPacketCount + count) - MAX_JITTER_SIZE;

			for (int i = 0; i < to_drop; i++)
			{
				if (m_outList[i] != NULL)
					//m_outList[i]->release();
					DELETEO(m_outList[i]);
			}

			// сдвигаем в начало
			int to_move = (m_outPacketCount - to_drop);
			memmove(m_outList, m_outList + to_drop, to_move * sizeof(rtp_packet*));

			m_outPacketCount = to_move;

			// очистим задние ряды после сдвига
			memset(m_outList + to_move, 0, to_drop * sizeof(rtp_packet*));
		}

		// записываем сразу за последним пакетом
		int sz = count;//Peter' sz
		if (sz > MAX_JITTER_SIZE)
			sz = MAX_JITTER_SIZE;

		for (int i = 0; i < sz; i++)
		{
			if (m_inList[i] != NULL)
				m_outList[m_outPacketCount++] = m_inList[i];
		}

		// теперь нужно сдвинуть и входящий буфер
		if (m_depthPackets - count > 0)
		{
			memmove(m_inList, m_inList + count, (m_depthPackets - count) * sizeof(rtp_packet*));
			memset(m_inList + (m_depthPackets - count), 0, count * sizeof(rtp_packet*));
		}
		else
		{
			//Peter
			memset(m_inList, 0, m_depthPackets * sizeof(rtp_packet*));
		}

		m_numberOut += count;
		if (m_numberOut > m_numberIn)
			m_numberOut = m_numberIn;

		// пересчитаем длину в семплах
		m_inMediaDepth = 0;

		for (int i = 0; i < m_depthPackets; i++)
		{
			if (m_inList[i] != NULL)
			{
				m_inMediaDepth += m_inList[i]->count;
			}
			else
			{
				m_inMediaDepth += m_inPacketSize;
			}
		}
	}
	//--------------------------------------
	// сброс джиттера
	//--------------------------------------
	void JitterQueue::reset()
	{
		for (int i = 0; i < 16; i++)
		{
			// освобождаем пакеты если они есть!
			if (m_inList[i] != NULL)
				//m_inList[i]->release();
				DELETEO(m_inList[i]);

			if (m_outList[i] != NULL)
				//m_outList[i]->release();
				DELETEO(m_outList[i]);
		}

		memset(m_inList, 0, sizeof m_inList);
		memset(m_outList, 0, sizeof m_outList);

		m_new = true;
		m_numberOut = m_numberIn = 0;
		m_outPacketCount = 0;
	}
}
//--------------------------------------
