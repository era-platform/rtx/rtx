/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include "stdafx.h"

#include "av_resampler.h"
//-----------------------------------------------
//
//-----------------------------------------------
#define __STDC_CONSTANT_MACROS
//-----------------------------------------------
//
//-----------------------------------------------
extern "C"
{
#include <libavcodec/avcodec.h>
}

namespace media
{
	//-----------------------------------------------
	//
	//-----------------------------------------------
	AVResampler::AVResampler(rtl::Logger* log) : m_log(log)
	{
		m_context = NULL;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	AVResampler::~AVResampler()
	{
		dispose();
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool AVResampler::init(int in_rate, int in_channels, int out_rate, int out_channels)
	{
		dispose();

		if (in_rate <= 0 || out_rate <= 0)
			return false;

		m_context = av_resample_init(out_rate, in_rate, 8, 0, 1, 1.0);

		if (!m_context)
		{
			PLOG_ERROR("AVR", "Init -- failed, av_resample_init() return NULL!");
			return false;
		}

		return true;
	}
	void AVResampler::dispose()
	{
		if (m_context)
		{
			av_resample_close(m_context);
			m_context = NULL;
		}
	}
	int AVResampler::resample(const short* src, int srcSamples, int* srcUsed, short* pDst, int dstSamples)
	{
		if (!m_context)
		{
			PLOG_ERROR("RSMP", "Resample -- failed, not initialized!");
		}

		/**
		* Resample an array of samples using a previously configured context.
		* @param src an array of unconsumed samples
		* @param consumed the number of samples of src which have been consumed are returned here
		* @param src_size the number of unconsumed samples available
		* @param dst_size the amount of space in samples available in dst
		* @param update_ctx If this is 0 then the context will not be modified, that way several channels can be resampled with the same context.
		* @return the number of samples written in dst or -1 if an error occurred
		*/
		*srcUsed = 0;
		int res = av_resample(m_context, pDst, (short*)src, srcUsed, srcSamples, dstSamples, TRUE);

		//PLOG_MEDIA_WRITE("RSMP", "Resample -- in size: %d samples. out size: %d samples. res size: %d samples", srcSamples, dstSamples, res);

		return res;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool AVResampler::isSimple()
	{
		return false;
	}
}
//-----------------------------------------------
