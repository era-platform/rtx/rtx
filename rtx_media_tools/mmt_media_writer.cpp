/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_media_writer.h"
#include "mmt_media_writer_man.h"

namespace media
{
	//--------------------------------------
	//
	//--------------------------------------
	DECLARE int g_rtx_recorder_counter = -1;
	//--------------------------------------
	// ������ ���������� ��������� ������ � ����
	//--------------------------------------
	static MediaWriterManager g_mg_recorder_manager;
	//--------------------------------------
	// �����������
	//--------------------------------------
	MediaWriter::MediaWriter(rtl::Logger* log) :
		m_log(log), m_sync("media_writer"),
		m_nextLink(nullptr), m_timerPeriod(250), m_timerLastTick(0)
	{
		rtl::res_counter_t::add_ref(g_rtx_recorder_counter);
	}
	//--------------------------------------
	// ���������
	//--------------------------------------
	MediaWriter::~MediaWriter()
	{
		rtl::res_counter_t::release(g_rtx_recorder_counter);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void MediaWriter::startTimer()
	{
		g_mg_recorder_manager.add(this);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void MediaWriter::stopTimer()
	{
		g_mg_recorder_manager.remove(this);

		m_nextLink = nullptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void MediaWriter::set_record_max_time(uint32_t max_time)
	{
		uint32_t time_to_set;
		if (max_time < 1000)
			time_to_set = 1000;
		else if (max_time > 30000)
			time_to_set = 30000;
		else
			time_to_set = max_time;


	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_MMT_API uint32_t MediaWriter::getRecordMaxTime()
	{
		return g_mg_recorder_manager.getRecordMaxTime();
	}
}
//--------------------------------------
