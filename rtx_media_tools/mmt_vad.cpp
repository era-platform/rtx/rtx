/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_vad.h"
#include "vad/vad_analyzer.h"

namespace media
{
	static double percentage(double all, double part);
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VAD_Detecter::VAD_Detecter(rtl::Logger* log) : m_log(log), m_vadAnalyzer(nullptr)
	{
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	VAD_Detecter::~VAD_Detecter()
	{
		destroy();
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool VAD_Detecter::create(int frequency, int threshold, int vad_duration, int silence_duration)
	{
		destroy();

		m_vadAnalyzer = VAD_Analyzer::createInstance("spandsp", m_log); // "webrtc"
		m_vadAnalyzer->create(frequency, threshold, vad_duration, silence_duration);

		return true;
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	void VAD_Detecter::destroy()
	{
		if (m_vadAnalyzer != nullptr)
		{
			m_vadAnalyzer->destroy();
			VAD_Analyzer::deleteInstance(m_vadAnalyzer);
			m_vadAnalyzer = nullptr;
		}
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool VAD_Detecter::addSilence(int samples_left, bool& vad, int& duration)
	{
		return m_vadAnalyzer->process(samples_left, vad, duration);
	}
	//-----------------------------------------------
	//
	//-----------------------------------------------
	bool VAD_Detecter::addPacket(const short* samples, int length, bool& vad, int& duration)
	{
		return m_vadAnalyzer->process(samples, length, vad, duration);
	}
}
//-----------------------------------------------
