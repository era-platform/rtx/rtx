﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_payload_format.h"

namespace media
{
	//--------------------------------------
	// пустой формат -- используется вместо нуля
	//--------------------------------------
	RTX_MMT_API PayloadFormat PayloadFormat::Empty;
	//--------------------------------------
	//
	//--------------------------------------
	RTX_MMT_API PayloadFormat& PayloadFormat::assign(const PayloadFormat& format)
	{
		m_dtmf = format.m_dtmf;
		m_encoding = format.m_encoding;
		m_id = format.m_id;
		m_freq = format.m_freq;
		m_channels = format.m_channels;
		m_params = format.m_params;
		m_fmtp = format.m_fmtp;

		return *this;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void PayloadFormat::assign(const char* encoding, PayloadId payloadId, int clockRate, int channels, const char* fmtp)
	{
		m_dtmf = false;
		m_encoding = encoding;
		m_id = payloadId;
		m_freq = clockRate;
		m_channels = channels;
		m_params.setEmpty();
		m_fmtp = fmtp;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void PayloadFormat::assign(const char* encoding, PayloadId payloadId, int clockRate, const char* params, const char* fmtp)
	{
		m_dtmf = false;
		m_encoding = encoding;
		m_id = payloadId;
		m_freq = clockRate;
		m_channels = 1;
		m_params = params;
		m_fmtp = fmtp;
	}
	//--------------------------------------
	// сравнение на равенство
	//--------------------------------------
	bool PayloadFormat::isEqual(const PayloadFormat& format) const
	{
		if (m_id != format.m_id || m_dtmf != format.m_dtmf)
			return false;

		// так многие телефоны добавляют в название кодировки всякую хрень
		// то проверяем на совпадение по длине меньшего!
		int len = m_encoding.getLength();
		int f_len = format.m_encoding.getLength();
		int cmd_len = len < f_len ? len : f_len;

		if (std_strnicmp(m_encoding, format.m_encoding, cmd_len) != 0)
			return false;

		if (m_freq != format.m_freq)
			return false;

		if (m_channels != format.m_channels)
			return false;

		if (!m_params.isEmpty() && !format.m_params.isEmpty() && std_stricmp(m_params, format.m_params) != 0)
			return false;

		return true;
	}
	//--------------------------------------
	// очистим формат
	//--------------------------------------
	void PayloadFormat::clear()
	{
		m_dtmf = false;
		m_encoding.setEmpty();
		m_id = PayloadId::Error;
		m_freq = 8000;
		m_channels = 1;
		m_params.setEmpty();
		m_fmtp.setEmpty();
	}
}
//--------------------------------------
