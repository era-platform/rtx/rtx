﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_recorder_wave.h"
//--------------------------------------
//
//--------------------------------------

namespace media
{
	RecorderMonoWave::RecorderMonoWave(int pcmFreq) : RecorderMono(pcmFreq),
		m_file(nullptr), m_fileFormat(nullptr), m_fileFormatSize(0)
	{
		m_pcmBuffer = nullptr;
		m_pcmWritePos = 0;

		m_resampler = nullptr;
		m_resampleBuffer = nullptr;

		// декодирование
		m_enc = nullptr;
		m_encBuffer = nullptr;
		m_encBufferSize = 0;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RecorderMonoWave::~RecorderMonoWave()
	{
		close();
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool RecorderMonoWave::create(const char* filePath, const char* outEncoding, int outFrequency, const char* outFMTP)
	{
		m_fileFormat = getWAVFormat(outEncoding, outFrequency, 1, outFMTP);

		if (m_fileFormat == nullptr)
		{
			return false;
		}

		m_fileFormatSize = sizeof(WAVFormat) + m_fileFormat->extraSize;

		if (!m_file.create(filePath, m_fileFormat))
			return false;

		if (m_frequency != (int)m_fileFormat->samplesRate)
		{
			// resampler
			if ((m_resampler = createResampler(m_frequency, 1, m_fileFormat->samplesRate, m_fileFormat->channels)) == nullptr)
				return false;

			m_resampleBuffer = (short*)MALLOC(m_fileFormat->samplesRate * sizeof(short));
		}

		m_pcmBuffer = (short*)MALLOC(m_frequency * sizeof(short));
		m_pcmWritePos = 0;

		bool result = false;

		switch (m_fileFormat->tag)
		{
		case WAVFormatTagPCM:
			result = true;
			break;
		case WAVFormatTagALAW:
		case WAVFormatTagULAW:
			result = setupG711(outEncoding);
			break;
		case WAVFormatTagGSM610:
			result = setupGSM610();
			break;
		}

		m_stat_PCMWritten = 0;

		return result;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void RecorderMonoWave::close()
	{
		flush();

		m_file.close();

		if (m_pcmBuffer != nullptr)
		{
			if (m_pcmWritePos > 0)
			{
				resampleBlock(m_pcmBuffer, m_pcmWritePos);
			}

			FREE(m_pcmBuffer);
			m_pcmBuffer = nullptr;
			m_pcmWritePos = 0;
		}

		if (m_resampler != nullptr)
		{
			destroyResampler(m_resampler);
			m_resampler = nullptr;
		}

		if (m_resampleBuffer != nullptr)
		{
			FREE(m_resampleBuffer);
			m_resampleBuffer = nullptr;
		}

		if (m_enc != nullptr)
		{
			rtx_codec__release_audio_encoder(m_enc);
			m_enc = nullptr;
		}

		if (m_encBuffer != nullptr)
		{
			FREE(m_encBuffer);
			m_encBuffer = nullptr;
			m_encBufferSize = 0;
		}

		if (m_fileFormat != nullptr)
		{
			FREE(m_fileFormat);
			m_fileFormat = nullptr;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool RecorderMonoWave::flush()
	{
		if (m_pcmWritePos > 0)
		{
			resampleBlock(m_pcmBuffer, m_pcmWritePos);
			m_pcmWritePos = 0;
		}

		return m_file.flush();
	}
	//--------------------------------------
	//
	//--------------------------------------
	int RecorderMonoWave::write(const short* samples, int count)
	{
		int written = 0;

		while (written < count)
		{
			int remain = count - written;

			// 1 если в буфере есть остатки то допишем и отправим секунду
			if (m_pcmWritePos > 0)
			{
				int to_copy = MIN(remain, m_frequency - m_pcmWritePos);
				memcpy(m_pcmBuffer + m_pcmWritePos, samples + written, to_copy * sizeof(short));
				written += to_copy;
				m_pcmWritePos += to_copy;

				if (m_pcmWritePos < m_frequency)
				{
					return written;
				}

				resampleBlock(m_pcmBuffer, m_frequency);
				m_pcmWritePos = 0;

				// буфер полностью сброшен, а данные еще остались -- передаем на след. итерацию
			}
			// если данных больше чем буфер а буфер пустой то отправим секунду напрямую без копирования в буфер
			else if (remain >= m_frequency)
			{
				resampleBlock(samples + written, m_frequency);
				written += m_frequency;
			}
			// остаток меньше чем буфер -- запишем
			else
			{
				memcpy(m_pcmBuffer, samples + written, remain * sizeof(short));
				written += remain;
				m_pcmWritePos = remain;
			}
		}

		return written;
	}
	//--------------------------------------
	// размер входных данных всегда секунда
	//--------------------------------------
	int RecorderMonoWave::resampleBlock(const short* samples, int count)
	{
		if (m_resampler == nullptr)
		{
			return encodeBlock(samples, count);
		}

		int block_count = count / m_frequency;
		int block_remain = count % m_frequency;
		const short* it = samples;

		for (int i = 0; i < block_count; i++)
		{
			int used = 0;
			int result = m_resampler->resample(it, m_frequency, &used, m_resampleBuffer, m_fileFormat->samplesRate);

			if (result > 0)
			{
				encodeBlock(m_resampleBuffer, result);
			}

			it += m_frequency;
		}

		if (block_remain > 0)
		{
			int used = 0;
			int result = m_resampler->resample(it, block_remain, &used, m_resampleBuffer, m_fileFormat->samplesRate);

			if (result > 0)
			{
				encodeBlock(m_resampleBuffer, result);
			}
		}

		return count;
	}
	//--------------------------------------
	// размер входных данных всегда секунда
	//--------------------------------------
	int RecorderMonoWave::encodeBlock(const short* samples, int count)
	{
		if (m_enc == nullptr)
		{
			m_stat_PCMWritten += count;
			return m_file.write(samples, count * sizeof(short)) / sizeof(short);
		}

		memset(m_encBuffer, 0, m_encBufferSize);
		int result = m_enc->encode((const uint8_t*)samples, count * sizeof(short), m_encBuffer, m_encBufferSize);
		m_stat_PCMWritten += result / sizeof(short);
		return m_file.write(m_encBuffer, result);
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool RecorderMonoWave::setupG711(const char* outEncoding)
	{
		PayloadFormat g711(outEncoding, std_stricmp(outEncoding, "PCMA") ? PayloadId::PCMA : PayloadId::PCMU, 8000, 1, nullptr);

		if ((m_enc = rtx_codec__create_audio_encoder(g711.getEncoding(), &g711)) == nullptr)
			return false;

		m_encBuffer = (uint8_t*)MALLOC(m_encBufferSize = 8000);

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool RecorderMonoWave::setupGSM610()
	{
		// for file writing only. 49 - ms wav number. decoder know it
		PayloadFormat gsm("GSM", (PayloadId)WAVFormatTagGSM610, 8000, 1, "msgsm");

		if ((m_enc = rtx_codec__create_audio_encoder(gsm.getEncoding(), &gsm)) == nullptr)
			return false;

		m_encBuffer = (uint8_t*)MALLOC(m_encBufferSize = 1625);

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RecorderStereoWave::RecorderStereoWave(int outFrequency) : RecorderStereo(outFrequency), m_file(nullptr)
	{
		memset(&m_fileFormat, 0, sizeof(WAVFormat));
		m_resampler = 0;
		m_resampleLeft = nullptr;
		m_resampleRight = nullptr;
		m_writeBuffer = nullptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RecorderStereoWave::~RecorderStereoWave()
	{
		close();
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool RecorderStereoWave::create(const char* filePath, const char* outEncoding, int outFrequency, const char* outFMTP)
	{
		if (std_stricmp(outEncoding, "L16") != 0)
			return false;

		m_fileFormat.tag = WAVFormatTagPCM;
		m_fileFormat.samplesRate = outFrequency;
		m_fileFormat.blockAlign = 4;
		m_fileFormat.channels = 2;
		m_fileFormat.bitsPerSample = 16;
		m_fileFormat.bytesPerSecond = m_frequency * 4;
		m_fileFormat.extraSize = 0;

		if (!m_file.create(filePath, &m_fileFormat))
			return false;

		if (m_frequency != (int)m_fileFormat.samplesRate)
		{
			// resampler
			if ((m_resampler = createResampler(m_frequency, 2, m_fileFormat.samplesRate, m_fileFormat.channels)) == nullptr)
				return false;

			int size = m_fileFormat.samplesRate * sizeof(short);
			m_resampleLeft = (short*)MALLOC(size);
			m_resampleRight = (short*)MALLOC(size);
		}

		m_writeBuffer = (short*)MALLOC(m_fileFormat.samplesRate * sizeof(short) * 2);

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void RecorderStereoWave::close()
	{
		m_file.close();

		if (m_resampler != nullptr)
		{
			destroyResampler(m_resampler);
			m_resampler = nullptr;
		}

		if (m_resampleLeft != nullptr)
		{
			FREE(m_resampleLeft);
			m_resampleLeft = nullptr;
		}

		if (m_resampleRight != nullptr)
		{
			FREE(m_resampleRight);
			m_resampleRight = nullptr;
		}

		if (m_writeBuffer != nullptr)
		{
			FREE(m_writeBuffer);
			m_writeBuffer = nullptr;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool RecorderStereoWave::flush()
	{
		return m_file.flush();
	}
	//--------------------------------------
	//
	//--------------------------------------
	int RecorderStereoWave::write(const short* leftChannel, const short* rightChannel, int samplesPerChannel)
	{
		return resample(leftChannel, rightChannel, samplesPerChannel);
	}
	//--------------------------------------
	//
	//--------------------------------------
	int RecorderStereoWave::resample(const short* leftChannel, const short* rightChannel, int samplesPerChannel)
	{
		if (m_resampler == nullptr)
		{
			return transform(leftChannel, rightChannel, samplesPerChannel);
		}

		//int used = 0;
		//int l_res = m_resampler->resample(leftChannel, samplesPerChannel, &used, m_resampleLeft, m_fileFormat.samplesRate);
		//used = 0;
		//int r_res = m_resampler->resample(rightChannel, samplesPerChannel, &used, m_resampleRight, m_fileFormat.samplesRate);

		transform(m_resampleLeft, m_resampleRight, m_fileFormat.samplesRate);

		return samplesPerChannel;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int RecorderStereoWave::transform(const short* leftChannel, const short* rightChannel, int samplesPerChannel)
	{
		for (int i = 0; i < samplesPerChannel; i++)
		{
			m_writeBuffer[i * 2] = leftChannel[i];
			m_writeBuffer[i * 2 + 1] = rightChannel[i];
		}

		return m_file.write(m_writeBuffer, samplesPerChannel * 4) / 4;
	}
}
//--------------------------------------
