﻿/* RTX H.248 Media Gate Media Processing Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_audio_reader_wave.h"

namespace media
{

#define GSM_BUFFER_SIZE			(1625)
#define GSM_BLOCK_SIZE			(65)
#define G711_BUFFER_SIZE		(8000)
#define G711_BLOCK_SIZE			(160)
	//--------------------------------------
	//
	//--------------------------------------
	AudioReaderWave::AudioReaderWave(rtl::Logger* log) : AudioReader(log),
		m_decoder(nullptr),
		m_dec_buffer(nullptr),
		m_dec_buffer_size(0),
		m_dec_buffer_pos(0),
		m_dec_buffer_read(0),
		m_dec_block_size(0)
	{
		m_bytes_read = 0;
	}
	//--------------------------------------
	//
	//--------------------------------------
	AudioReaderWave::~AudioReaderWave()
	{
		close();
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool AudioReaderWave::openSource(const char* path)
	{
		closeSource();

		if (!m_file.open(path))
			return false;

		PLOG_WRITE("Wave", "open file '%s'...", path);

		const WAVFormat* wave_fmt = m_file.getFormat();

		m_sourceChannels = wave_fmt->channels;
		m_sourceFrequency = wave_fmt->samplesRate;

		if (wave_fmt->tag == WAVFormatTagPCM)
		{
			// as is
			return true;
		}

		if (wave_fmt->tag == WAVFormatTagGSM610)
		{
			setupGSM610(wave_fmt);
			return true;
		}

		if (wave_fmt->tag == WAVFormatTagULAW || wave_fmt->tag == WAVFormatTagALAW)
		{
			setupG711(wave_fmt);
			return true;
		}

		close();
		return false;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void AudioReaderWave::closeSource()
	{
		m_file.close();

		// Decoder

		if (m_decoder != nullptr)
		{
			rtx_codec__release_audio_decoder(m_decoder);
			m_decoder = nullptr;
		}

		if (m_dec_buffer != nullptr)
		{
			FREE(m_dec_buffer);
			m_dec_buffer = nullptr;
			m_dec_buffer_size = 0;
			m_dec_buffer_read = m_dec_buffer_pos = 0;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	int AudioReaderWave::readRawPCM(short* buffer, int size)
	{
		if (m_decoder == nullptr)
		{
			return readRaw(buffer, size * sizeof(short)) / sizeof(short);
		}

		// читаем кратный блок данных в локальный буфер по секунде (m_raw_read_size)
		// декодируем поблочно в переданный буфер (m_raw_block_size и m_raw_read_pos)

		int written = 0;

		while (written < size)
		{
			// если буфер пуст то читаем из файла
			if (m_dec_buffer_pos == m_dec_buffer_read)
			{
				if ((m_dec_buffer_read = readRaw(m_dec_buffer, m_dec_buffer_size)) == 0)
				{
					m_eof = true;
					break;
				}
				m_dec_buffer_pos = 0;
			}

			// декодируем один блок
			int mean = m_dec_buffer_read - m_dec_buffer_pos;
			int blocksize = mean < m_dec_block_size ? mean : m_dec_block_size;
			int len = m_decoder->decode(m_dec_buffer + m_dec_buffer_pos, blocksize, (uint8_t*)(buffer + written), (size - written) * sizeof(short));

			if (len > 0)
			{
				// если успешно то увеличиваем позиции в буферах
				written += len / sizeof(short);
				m_dec_buffer_pos += blocksize;
			}
			else
			{
				// иначе прерываем
				// для буфера декодирования оставляем позицию на месте
				// для PCM возвращаем размер декодированных данных
				break;
			}
		}

		// в семплах

		PLOG_WRITE("Wave", "::readRawPCM return %d bytes", written);

		return written;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int AudioReaderWave::readRaw(void* buffer, int size_in_bytes)
	{
		int res = m_file.readAudioData(buffer, size_in_bytes);

		if (res == 0)
			return 0;

		m_bytes_read += res;

		return res;
	}
	//--------------------------------------
	// декодер gsm
	//--------------------------------------
	void AudioReaderWave::setupGSM610(const WAVFormat* fmt)
	{
		PayloadFormat pl("GSM", PayloadId::GSM610, 8000, 1, "ms");

		m_decoder = rtx_codec__create_audio_decoder(pl.getEncoding(), &pl);

		m_dec_buffer = (uint8_t*)MALLOC(GSM_BUFFER_SIZE);
		m_dec_buffer_size = GSM_BUFFER_SIZE;
		m_dec_block_size = GSM_BLOCK_SIZE;
	}
	//--------------------------------------
	// декодер g711
	//--------------------------------------
	void AudioReaderWave::setupG711(const WAVFormat* fmt)
	{
		PLOG_WRITE("Wave", "setup G711 file...");
		
		PayloadFormat pl;

		if (fmt->tag == WAVFormatTagALAW)
			pl.assign("PCMA", PayloadId::PCMA, 8000, 1);
		else // if (fmt->tag == WAVFormatTagALAW)
			pl.assign("PCMU", PayloadId::PCMU, 8000, 1);

		m_decoder = rtx_codec__create_audio_decoder(pl.getEncoding(), &pl);

		m_dec_buffer = (uint8_t*)MALLOC(G711_BUFFER_SIZE);
		m_dec_buffer_size = G711_BUFFER_SIZE;
		m_dec_block_size = G711_BLOCK_SIZE;

		PLOG_WRITE("Wave", "file format is {%d, %s, %d, %d} extra size %d",
			pl.getId(), pl.getEncoding(), pl.getFrequency(), pl.getChannelCount(), fmt->extraSize);
	}
}
//--------------------------------------
