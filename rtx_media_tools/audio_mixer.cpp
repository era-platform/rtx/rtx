/* RTX H.248 Media Gate. G.726 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_audio_mixer.h"

namespace media
{
	//-----------------------------------------------
	// interlaced streo to mono
	//-----------------------------------------------
	void mixer__stereo_to_mono_i(const short* stereo_i, short* mono_buffer, int samples_count)
	{
		if (stereo_i == nullptr || mono_buffer == nullptr || samples_count <= 0)
			return;

		int sample;

		while (samples_count)
		{
			//���������� � ����� �������
			sample = *stereo_i++;
			sample += *stereo_i++;
			sample >>= 1;

			if (sample > SHRT_MAX)
			{
				*mono_buffer = SHRT_MAX;
			}
			else if (sample < SHRT_MIN)
			{
				*mono_buffer = SHRT_MIN;
			}
			else
			{
				*mono_buffer = (short)sample;
			}

			++mono_buffer;
			--samples_count;
		}
	}
	//-----------------------------------------------
	// different streo to mono
	//-----------------------------------------------
	void mixer__stereo_to_mono_d(const short* stereo_left, const short* stereo_right, short* mono_buffer, int samples_count)
	{
		if (stereo_left == nullptr || stereo_right == nullptr || mono_buffer == nullptr || samples_count <= 0)
			return;

		int sample;

		while (samples_count)
		{
			//���������� � ����� �������
			sample = *stereo_left++;
			sample += *stereo_right++;
			sample >>= 1;

			if (sample > SHRT_MAX)
			{
				*mono_buffer = SHRT_MAX;
			}
			else if (sample < SHRT_MIN)
			{
				*mono_buffer = SHRT_MIN;
			}
			else
			{
				*mono_buffer = (short)sample;
			}

			++mono_buffer;
			--samples_count;
		}
	}
	//-----------------------------------------------
	// mono to interlaced streo
	//-----------------------------------------------
	void mixer__mono_to_stereo_i(const short* mono, short* stereo_i_buffer, int samples_count)
	{
		if (mono == nullptr || stereo_i_buffer == nullptr || samples_count <= 0)
			return;

		while (samples_count > 0)
		{
			*stereo_i_buffer++ = *mono;
			*stereo_i_buffer++ = *mono++;

			--samples_count;
		}
	}
	//-----------------------------------------------
	// mono to different streo
	//-----------------------------------------------
	void mixer__mono_to_stereo_d(const short* mono, short* stereo_left_buffer, short* stereo_right_buffer, int samples_count)
	{
		if (mono == nullptr || stereo_left_buffer == nullptr || stereo_right_buffer == nullptr || samples_count <= 0)
			return;

		while (samples_count > 0)
		{
			*stereo_left_buffer++ = *mono;
			*stereo_right_buffer++ = *mono++;

			--samples_count;
		}
	}
	//-----------------------------------------------
	// different channels stereo to interlaced stereo
	//-----------------------------------------------
	void mixer__stereo_d_to_i(const short* stereo_left, const short* stereo_right, short* stereo_i_buffer, int samples_count)
	{
		if (stereo_left == nullptr || stereo_right == nullptr || stereo_i_buffer == nullptr || samples_count <= 0)
			return;

		while (samples_count > 0)
		{
			*stereo_i_buffer++ = *stereo_left++;
			*stereo_i_buffer++ = *stereo_right++;

			--samples_count;
		}
	}
	//-----------------------------------------------
	// interlaced stereo to different channels stereo
	//-----------------------------------------------
	void mixer__stereo_i_to_d(const short* stereo_i, short* stereo_left_buffer, short* stereo_right_buffer, int samples_count)
	{
		if (stereo_i == nullptr || stereo_left_buffer == nullptr || stereo_right_buffer == nullptr || samples_count <= 0)
			return;

		while (samples_count > 0)
		{
			*stereo_left_buffer++ = *stereo_i++;
			*stereo_right_buffer++ = *stereo_i++;

			--samples_count;
		}
	}
}
