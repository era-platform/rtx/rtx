﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_transaction_manager.h"
#include "mg_transaction.h"

namespace h248
{
	//--------------------------------------
	//
	//--------------------------------------
#define DEF_LONG_TIMER_VALUE 30000
#define DEF_INITIAL_RETRANSMISSION_VALUE 500
#define DEF_MAX_RETRANSMISSION_COUNT 7
#define ASYNC_CHECK_TRANSACTIONS 1002
//--------------------------------------
//
//--------------------------------------
	rtl::MutexWatch TransactionManager::s_sync("trn-man");
	rtl::SortedArrayT<Transaction*, TransactionID> TransactionManager::s_timerList(compareObj, compareKey);
	rtl::SortedArrayT<Transaction*, TransactionID> TransactionManager::s_clientList(compareObj, compareKey);
	rtl::SortedArrayT<Transaction*, TransactionID> TransactionManager::s_serverList(compareObj, compareKey);

	int TransactionManager::s_longTimerValue = DEF_LONG_TIMER_VALUE;
	int TransactionManager::s_initialRetransmissionValue = DEF_INITIAL_RETRANSMISSION_VALUE;
	int TransactionManager::s_maxRetransmissionCount = DEF_MAX_RETRANSMISSION_COUNT;
	volatile bool TransactionManager::s_stopped = false;
	volatile uint32_t TransactionManager::s_activeTransactions = 0;
	h248::mg_out_mid_type_t TransactionManager::m_midType = h248::mg_out_mid_ipv4_e;
	rtl::String TransactionManager::m_midDomain;

	//--------------------------------------
	// менеджмент транзакциями
	//--------------------------------------
	int TransactionManager::compareObj(const TransactionPtr& left, const TransactionPtr& right)
	{
		int64_t result = left->getTranId().id - right->getTranId().id;
		return (int)result;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int TransactionManager::compareKey(const TransactionID& left, const TransactionPtr& right)
	{
		int64_t result = left.id - right->getTranId().id;
		return (int)result;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransactionManager::startLongTimer(Transaction* trn)
	{
		//rtl::MutexLock lock(s_sync);
		rtl::MutexWatchLock lock(s_sync, __FUNCTION__);

		s_timerList.add(trn);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransactionManager::addClientTransaction(Transaction* trn)
	{
		//rtl::MutexLock lock(s_sync);
		rtl::MutexWatchLock lock(s_sync, __FUNCTION__);

		s_clientList.add(trn);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransactionManager::addServerTransaction(Transaction* trn)
	{
		//rtl::MutexLock lock(s_sync);
		rtl::MutexWatchLock lock(s_sync, __FUNCTION__);

		s_serverList.add(trn);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransactionManager::removeTransaction(Transaction* trn)
	{
		//rtl::MutexLock lock(s_sync);
		rtl::MutexWatchLock lock(s_sync, __FUNCTION__);

		s_timerList.remove(trn);

		if (trn->getType() == Transaction::Client)
		{
			s_clientList.remove(trn);
		}
		else if (trn->getType() == Transaction::Server)
		{
			s_serverList.remove(trn);
		}
	}
	//--------------------------------------
	// управление временем жизни транзакций
	//--------------------------------------
	void TransactionManager::timerHandler(rtl::Timer* sender, uint32_t elapsed, void* user_data, int tid)
	{
		rtl::async_call_manager_t::callAsync(asyncHandler, nullptr, ASYNC_CHECK_TRANSACTIONS, 0, nullptr);
	}
	//--------------------------------------
	// терминатор для транзакций
	//--------------------------------------
	void TransactionManager::asyncHandler(void* userdata, uint16_t call_id, uintptr_t int_param, void* ptr_param)
	{
		switch (call_id)
		{
		case ASYNC_CHECK_TRANSACTIONS:
		{
			checkTransactionTimes();
			break;
		}
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	Transaction* TransactionManager::findClientTransaction(TransactionID trn_key)
	{
		//rtl::MutexLock lock(s_sync);
		rtl::MutexWatchLock lock(s_sync, __FUNCTION__);

		TransactionPtr* ptr = s_clientList.find(trn_key);

		return ptr != nullptr ? *ptr : nullptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	Transaction* TransactionManager::findServerTransaction(TransactionID trn_key)
	{
		//rtl::MutexLock lock(s_sync);
		rtl::MutexWatchLock lock(s_sync, __FUNCTION__);

		TransactionPtr* ptr = s_serverList.find(trn_key);

		return ptr != nullptr ? *ptr : nullptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransactionManager::checkTransactionTimes()
	{
		uint32_t timestamp = rtl::DateTime::getTicks();

		try
		{
			rtl::MutexWatchLock lock(s_sync, __FUNCTION__);

			// обратный порядок так как при проверке таймеров возможны удаления транзакций
			for (int i = s_timerList.getCount() - 1; i >= 0; i--)
			{
				Transaction* transaction = s_timerList[i];
				if (!transaction->checkTimer(timestamp))
				{
					s_timerList.removeAt(i);
					LOG_TRANS("trn-man", "LONG-TIMER elapsed for %s", transaction->getName());
					terminateTransaction(transaction);
				}
			}
		}
		catch (rtl::Exception& eh)
		{
			LOG_TRANS("trn-man", "timer catches exception\n%s", eh.get_message());
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransactionManager::terminateTransaction(Transaction* transaction)
	{
		LOG_TRANS("trn-man", "terminate transaction %s...", transaction->getName());

		//rtl::MutexWatchLock lock(s_sync, __FUNCTION__);
		//s_timerList.remove(transaction);
		//bool found_and_removed = false;

		if (transaction->getType() == Transaction::Client)
		{
			int index = s_clientList.indexOf(transaction);
			if (index != BAD_INDEX)
			{
				s_clientList.removeAt(index);
			}
		}
		else
		{
			int index = s_serverList.indexOf(transaction);
			if (index != BAD_INDEX)
			{
				s_serverList.removeAt(index);
			}
		}

		// we can delete it
		DELETEO(transaction);

		LOG_TRANS("trn-man", "transaction terminated");
	}
	//--------------------------------------
	// инициализация глобальных переменных
	//--------------------------------------
	bool TransactionManager::create(h248::mg_out_mid_type_t mid_type, const char* mid_domain)
	{
		LOG_TRANS("trn-man", "create transaction manager...");

		rtl::Timer::setTimer(timerHandler, nullptr, 0, 400, true);

		LOG_TRANS("trn-man", "transaction manager created");

		m_midType = mid_type;
		m_midDomain = mid_domain;

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransactionManager::stop()
	{
		LOG_TRANS("trn-man", "stop transaction manager...");

		s_stopped = true;

		rtl::Timer::stopTimer(timerHandler, nullptr, 0);

		// освободим все клиентские транзакции
		{
			//rtl::MutexLock lock(s_sync);
			rtl::MutexWatchLock lock(s_sync, __FUNCTION__);

			for (int i = 0; i < s_clientList.getCount(); i++)
			{
				s_clientList[i]->cancel();
			}
		}

		// нужно дожидатся завершения всех серверных транзакций и попутно заретить создавать новые!
		while (s_activeTransactions > 0)
		{
			// ждем
			rtl::Thread::sleep(5);
		}

		LOG_TRANS("trn-man", "transaction manager stopped");
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransactionManager::destroy()
	{
		LOG_TRANS("trn-man", "destroy transaction manager...");

		try
		{

			//rtl::MutexLock lock(s_sync);
			rtl::MutexWatchLock lock(s_sync, __FUNCTION__);

			// удаление всех транзакций
			for (int i = 0; i < s_timerList.getCount(); i++)
			{
				Transaction* trn = s_timerList[i];
				DELETEO(trn);
			}
			s_timerList.clear();

			for (int i = 0; i < s_clientList.getCount(); i++)
			{
				Transaction* trn = s_clientList[i];
				DELETEO(trn);
			}

			s_clientList.clear();

			for (int i = 0; i < s_serverList.getCount(); i++)
			{
				Transaction* trn = s_serverList[i];
				DELETEO(trn);
			}

			s_serverList.clear();

			LOG_TRANS("trn-man", "transaction manager destroyed");
		}
		catch (rtl::Exception& eh)
		{
			LOG_ERROR("trn-man", "transaction manager destroy catches exception\n%s", eh.get_message());
		}


	}
	//--------------------------------------
	// Transaction
	//--------------------------------------
	bool TransactionManager::startClientTransaction(ITransactionUser* user, TransactionID tid, int mgc_version, const Field* request, const Route& route)
	{
		if (s_stopped)
			return false;

		TransactionManager::addActiveCounter();

		LOG_TRANS("trn-man", "starting client transaction %u v:%d", tid._tid, mgc_version);

		Transaction* trn = NEW Transaction(user, tid, mgc_version);

		bool result = trn->txRequest(request, route);

		if (!result)
		{
			// безопасно. в случае неудачи транзакция никуда не попала
			DELETEO(trn);

			LOG_TRANS("trn-man", "starting client transaction failed");
		}
		else
		{
			LOG_TRANS("trn-man", "client transaction %s started", trn->getName());
		}

		TransactionManager::subActiveCounter();

		return result;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransactionManager::cancelMgcTransactions(uint32_t mgc_id)
	{
		//rtl::MutexLock lock(s_sync);
		rtl::MutexWatchLock lock(s_sync, __FUNCTION__);

		for (int i = s_clientList.getCount() - 1; i >= 0; i--)
		{
			Transaction* trn = s_clientList[i];

			if (trn->getTranId()._cid == mgc_id)
			{
				s_clientList.removeAt(i);
				trn->cancel();
			}
		}

		for (int i = s_serverList.getCount() - 1; i >= 0; i--)
		{
			Transaction* trn = s_serverList[i];

			if (trn->getTranId()._cid == mgc_id)
			{
				s_serverList.removeAt(i);
				trn->cancel();
			}
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransactionManager::sendReply(TransactionID tid, Field* reply)
	{
		// отправляем ответ на транспорт
		Transaction* trn = findServerTransaction(tid);

		if (trn != nullptr)
		{
			trn->txReply(reply);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool TransactionManager::sendBackError(TransactionID tid, ErrorCode error_code, const char* reason)
	{
		// отправляем сообщение с ошибкой на транспорт
		Transaction* trn = findClientTransaction(tid);

		if (trn == nullptr)
			trn = findServerTransaction(tid);

		if (trn != nullptr)
		{
			return trn->sendBackError(error_code, reason);
		}

		return false;
	}
	//--------------------------------------
	// Transaction
	//--------------------------------------
	bool TransactionManager::processServerTransaction(ITransactionUser* user, TransactionID tid, int mgc_version, const Field* request,
		const Route& route) // Field* reply, 
	{
		if (s_stopped)
			return false;

		TransactionManager::addActiveCounter();

		LOG_TRANS("trn-man", "process server transaction %u", tid._tid);

		Transaction* transaction = TransactionManager::findServerTransaction(tid);

		if (transaction == nullptr)
		{
			LOG_TRANS("trn-man", "create new transaction v:%d", mgc_version);

			transaction = NEW Transaction(user, tid, mgc_version);
		}

		bool result = transaction->rxRequest(request, route); // reply, 

		LOG_TRANS("trn-man", "server transaction %s processing %s", transaction->getName(), result ? "done" : "failed");

		TransactionManager::subActiveCounter();

		return result;
	}
	//--------------------------------------
	// Reply or Pending
	//--------------------------------------
	bool TransactionManager::processReply(TransactionID trn_key, const Field* reply)
	{
		if (s_stopped)
			return false;

		TransactionManager::addActiveCounter();

		LOG_TRANS("trn-man", "process client transaction %u response", trn_key._tid);

		bool result = false;

		Transaction* transaction = TransactionManager::findClientTransaction(trn_key);

		if (transaction != nullptr)
		{
			LOG_TRANS("trn-man", "client transaction found %s", transaction->getName());
			// загадка природы. Ответ: обемен номером транзакции
			if (reply->getType() == Token::Reply)
			{
				LOG_TRANS("trn-man", "process client transaction reply");
				result = transaction->rxReply(reply);
			}
			else
			{
				LOG_TRANS("trn-man", "process client transaction pending");
				result = transaction->rxPending(reply);
			}
		}

		LOG_TRANS("trn-man", "process client transaction %s", result ? "done" : "failed");

		TransactionManager::subActiveCounter();

		return result;
	}
	//--------------------------------------
	// TransactionResponseAck
	//--------------------------------------
	bool TransactionManager::processAck(TransactionID trn_key)
	{
		if (s_stopped)
			return false;

		TransactionManager::addActiveCounter();

		LOG_TRANS("trn-man", "process server transaction %u ack", trn_key._tid);

		Transaction* transaction = TransactionManager::findServerTransaction(trn_key);

		if (transaction != nullptr)
		{
			LOG_TRANS("trn-man", "server transaction found %s", transaction->getName());
			transaction->rxAck();
		}

		TransactionManager::subActiveCounter();

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	const char* TransactionManager::makeMid(char* buffer, int size, in_addr addr, uint16_t port)
	{
		int len;
		const char* mid_dev_postfix = nullptr;

		switch (m_midType)
		{
		case mg_out_mid_device_e:
			mid_dev_postfix = Config.get_config_value("megaco-mid-postfix");
			if (mid_dev_postfix != nullptr && mid_dev_postfix[0] != 0)
			{
				len = std_snprintf(buffer, size, "rtx_mg_%s_%s", inet_ntoa(addr), mid_dev_postfix);
			}
			else
			{
				len = std_snprintf(buffer, size, "rtx_mg_%s", inet_ntoa(addr));
			}
			buffer[len] = 0;
			break;
		case mg_out_mid_domain_e:
			if (port != 0)
			{
				len = std_snprintf(buffer, size, "<%s>:%u", (const char*)m_midDomain, port);
			}
			else
			{
				len = std_snprintf(buffer, size, "<%s>", (const char*)m_midDomain);
			}
			buffer[len] = 0;
			break;
		case h248::mg_out_mid_ipv4_e:
		case mg_out_mid_ipv6_e:
		default:
			if (port != 0)
			{
				len = std_snprintf(buffer, size, "[%s]:%u", inet_ntoa(addr), port);
			}
			else
			{
				len = std_snprintf(buffer, size, "[%s]", inet_ntoa(addr));
			}
			break;
		}

		return buffer;
	}
	//--------------------------------------
	//
	//--------------------------------------
	h248::mg_out_mid_type_t parse_mg_out_mid_type(const char* text, int len)
	{
		if (len == -1)
		{
			len = std_strlen(text);
		}

		if (len == 4)
		{
			if (strncmp(text, "ipv4", 4) == 0)
				return h248::mg_out_mid_ipv4_e;

			if (strncmp(text, "ipv6", 4) == 0)
				return mg_out_mid_ipv6_e;

		}
		else if (len == 6)
		{
			if (strncmp(text, "domain", 6) == 0)
				return mg_out_mid_domain_e;
			if (strncmp(text, "device", 6) == 0)
				return mg_out_mid_device_e;
		}

		return mg_out_mid_unknown_e;
	}
}
//--------------------------------------
