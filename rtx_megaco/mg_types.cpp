﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_interface.h"

namespace h248
{
	//--------------------------------------
	// парсинг полного имени терминейшена : тип/адрес/ид
	// term_info.iface через strdup 
	//--------------------------------------
	bool TerminationID::parse(const char* term_id)
	{
		if (term_id == nullptr || term_id[0] == 0)
			return false;

		/*
		prefix//rtp//353
		prefix2//webrtc//1034
		'*'
		public2//rtp//$
		'*'//rtp//'*'
		ROOT
		$
		*/

		// если одиночный символ то
		if (term_id[1] == 0)
		{
			if (term_id[0] == '*')
			{
				m_type = TerminationType::All;
				rtl::strupdate(m_interface, "*");
				m_id = MG_TERM_ALL;
				return true;
			}
			else if (term_id[0] == '$')
			{
				m_type = TerminationType::Choose;
				rtl::strupdate(m_interface, "$");
				m_id = MG_TERM_CHOOSE;
				return true;
			}
			// остальные варианты не приемлемы!
			return false;
		}

		// пройдемся по частям
		const char* iface = term_id;
		const char* type = strchr(iface, '/');
		const char* id = iface != nullptr ? strchr(type + 1, '/') : nullptr;


		if (type == nullptr || id == nullptr)
		{
			// только полное наименование!
			// остальные варианты не приемлемы!
			return false;
		}

		rtl::strupdate(m_interface, iface, PTR_DIFF(type, iface));

		type++;

		int type_len = PTR_DIFF(id, type);

		if (type_len == 1 && type[0] == '*')
		{
			m_type = TerminationType::All;
		}
		else if (type_len == 1 && type[0] == '$')
		{
			m_type = TerminationType::Choose;
		}
		else if (type_len == 3 && std_strnicmp(type, "rtp", 3) == 0)
		{
			m_type = TerminationType::RTP;
		}
		else if (type_len == 4 && std_strnicmp(type, "srtp", 4) == 0)
		{
			m_type = TerminationType::SRTP;
		}
		else if (type_len == 6 && std_strnicmp(type, "webrtc", 6) == 0)
		{
			m_type = TerminationType::WebRTC;
		}
		else if ((type_len == 4 && std_strnicmp(type, "ivrp", 4) == 0) || (type_len == 3 && std_strnicmp(type, "ivr", 3) == 0))
		{
			m_type = TerminationType::IVR_Player;
		}
		else if (type_len == 4 && std_strnicmp(type, "ivrr", 4) == 0)
		{
			m_type = TerminationType::IVR_Record;
		}
		else if (type_len == 3 && std_strnicmp(type, "t30", 3) == 0)
		{
			m_type = TerminationType::Fax_T30;
		}
		else if (type_len == 3 && std_strnicmp(type, "t38", 3) == 0)
		{
			m_type = TerminationType::Fax_T38;
		}
		else
		{
			return false;
		}

		id++;

		if (id[0] == '*')
			m_id = MG_TERM_ALL;
		else if (id[0] == '$')
			m_id = MG_TERM_CHOOSE;
		else
			m_id = strtoul(id, nullptr, 10);


		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	const char* TerminationID::toString(char* buffer, int size) const
	{
		static const char* type_names[] = { "root", "srtp", "rtp", "webrtc", "*", "$", "ivrp", "ivrr", "t30", "t38", "device" };

		const char* type_name = m_type >= TerminationType::Root && m_type <= TerminationType::Fax_T38 ? type_names[(int)m_type] : "unknown";
		const char* prefix = m_interface != nullptr ? m_interface : "_";

		char num_buf[16];
		std_snprintf(num_buf, 16, "%u", m_id);

		const char* id = m_id == MG_TERM_ALL ? "*" : m_id == MG_TERM_CHOOSE ? "$" : m_id == MG_TERM_ROOT ? "root" : num_buf;


		int len = std_snprintf(buffer, size - 1, "%s/%s/%s", prefix, type_name, id);

		buffer[len] = 0;

		return buffer;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool TerminationID::operator == (const TerminationID& id) const
	{
		bool type_equal = m_type == id.m_type || (m_type == TerminationType::All || id.m_type == TerminationType::All);
		bool id_equal = m_id == id.m_id || (m_id == MG_TERM_ALL || id.m_id == MG_TERM_ALL);

		if (m_interface == nullptr || id.m_interface == nullptr || m_interface[0] == '*' || id.m_interface[0] == '*')
		{
			return type_equal && id_equal;
		}

		return std_stricmp(m_interface, id.m_interface) == 0 && id_equal && type_equal;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool TerminationID::operator != (const TerminationID& id) const
	{
		if (m_interface == nullptr || id.m_interface == nullptr)
		{
			return m_type != id.m_type || m_interface != id.m_interface || m_id != id.m_id;
		}

		return m_type != id.m_type || std_stricmp(m_interface, id.m_interface) != 0 || m_id != id.m_id;
	}
	/*
	AddToken = ("Add" / "A")
	AndAUDITSelectToken = ("ANDLgc")
	AuditToken = ("Audit" / "AT")
	AuditCapToken = ("AuditCapability" / "AC")
	AuditValueToken = ("AuditValue" / "AV")
	AuthToken = ("Authentication" / "AU")
	BothToken = ("Both" / "B")
	BothwayToken = ("Bothway" / "BW")
	BriefToken = ("Brief" / "BR")
	BufferToken = ("Buffer" / "BF")
	CtxToken = ("Context" / "C")
	ContextAuditToken = ("ContextAudit" / "CA")
	ContextAttrToken = ("ContextAttr" / "CT")
	ContextListToken = ("ContextList" / "CLT")
	DigitMapToken = ("DigitMap" / "DM")
	DirectionToken = ("SPADirection" / "SPADI")
	DisconnectedToken = ("Disconnected" / "DC")
	DelayToken = ("Delay" / "DL")
	DurationToken = ("Duration" / "DR")
	EmbedToken = ("Embed" / "EM")
	EmergencyToken = ("Emergency" / "EG")
	EmergencyOffToken = ("EmergencyOff" / "EGO")
	EmergencyValueToken = ("EmergencyValue" / "EGV")
	ErrorToken = ("Error" / "ER")
	EventBufferToken = ("EventBuffer" / "EB")
	EventsToken = ("Events" / "E")
	ExternalToken = ("External" / "EX")
	FailoverToken = ("Failover" / "FL")
	ForcedToken = ("Forced" / "FO")
	GracefulToken = ("Graceful" / "GR")
	H221Token = ("H221")
	H223Token = ("H223")
	H226Token = ("H226")
	HandOffToken = ("HandOff" / "HO")
	IEPSToken = ("IEPSCall" / "IEPS")
	ImmAckRequiredToken = ("ImmAckRequired" / "IA")
	InactiveToken = ("Inactive" / "IN")
	InternalToken = ("Internal" / "IT")
	IntsigDelayToken = ("Intersignal" / "SPAIS")
	IsolateToken = ("Isolate" / "IS")
	InSvcToken = ("InService" / "IV")
	InterruptByEventToken = ("IntByEvent" / "IBE")
	InterruptByNewSignalsDescrToken = ("IntBySigDescr" / "IBS")
	IterationToken = ("Iteration" / "IR")
	KeepActiveToken = ("KeepActive" / "KA")
	LocalToken = ("Local" / "L")
	LocalControlToken = ("LocalControl" / "O")
	LockStepToken = ("LockStep" / "SP")
	LoopbackToken = ("Loopback" / "LB")
	MediaToken = ("Media" / "M")
	MegacopToken = ("MEGACO" / "!")
	MessageSegmentToken = ("Segment" / "SM")
	MethodToken = ("Method" / "MT")
	MgcIdToken = ("MgcIdToTry" / "MG")
	ModeToken = ("Mode" / "MO")
	ModifyToken = ("Modify" / "MF")
	ModemToken = ("Modem" / "MD")
	MoveToken = ("Move" / "MV")
	MTPToken = ("MTP")
	MuxToken = ("Mux" / "MX")
	NeverNotifyToken = ("NeverNotify" / "NBNN")
	NotifyToken = ("Notify" / "N")
	NotifyCompletionToken = ("NotifyCompletion" / "NC")
	NotifyImmediateToken = ("ImmediateNotify" / "NBIN")
	NotifyRegulatedToken = ("RegulatedNotify" / "NBRN")
	Nx64kToken = ("Nx64Kservice" / "N64")
	ObservedEventsToken = ("ObservedEvents" / "OE")
	OnewayToken = ("Oneway" / "OW")
	OnewayBothToken = ("OnewayBoth" / "OWB")
	OnewayExternalToken = ("OnewayExternal" / "OWE")
	OnOffToken = ("OnOff" / "OO")
	OrAUDITselectToken = ("ORLgc")
	OtherReasonToken = ("OtherReason" / "OR")
	OutOfSvcToken = ("OutOfService" / "OS")
	PackagesToken = ("Packages" / "PG")
	PendingToken = ("Pending" / "PN")
	PriorityToken = ("Priority" / "PR")
	ProfileToken = ("Profile" / "PF")
	ReasonToken = ("Reason" / "RE")
	RecvonlyToken = ("ReceiveOnly" / "RC")
	ReplyToken = ("Reply" / "P")
	ResetEventsDescriptorToken = ("ResetEventsDescriptor" / "RSE")
	RestartToken = ("Restart" / "RS")
	RemoteToken = ("Remote" / "R")
	RequestIDToken = ("SPARequestID" / "SPARQ")
	ReservedGroupToken = ("ReservedGroup" / "RG")
	ReservedValueToken = ("ReservedValue" / "RV")
	SegmentationCompleteToken= ("END" / "&")
	SendonlyToken = ("SendOnly" / "SO")
	SendrecvToken = ("SendReceive" / "SR")
	ServicesToken = ("Services" / "SV")
	ServiceStatesToken = ("ServiceStates" / "SI")
	ServiceChangeIncompleteToken = ("ServiceChangeInc" / "SIC")
	ServiceChangeToken = ("ServiceChange" / "SC")
	ServiceChangeAddressToken = ("ServiceChangeAddress" / "AD")
	SignalListToken = ("SignalList" / "SL")
	SignalsToken = ("Signals" / "SG")
	SignalTypeToken = ("SignalType" / "SY")
	StatsToken = ("Statistics" / "SA")
	StreamToken = ("Stream" / "ST")
	SubtractToken = ("Subtract" / "S")
	SynchISDNToken = ("SynchISDN" / "SN")
	TerminationStateToken = ("TerminationState" / "TS")
	TestToken = ("Test" / "TE")
	TimeOutToken = ("TimeOut" / "TO")
	TopologyToken = ("Topology" / "TP")
	TransToken = ("Transaction" / "T")
	ResponseAckToken = ("TransactionResponseAck" / "K")
	V18Token = ("V18")
	V22Token = ("V22")
	V22bisToken = ("V22b")
	V32Token = ("V32")
	V32bisToken = ("V32b")
	118 Rec. ITU-T H.248.1 (03/2013)
	V34Token = ("V34")
	V76Token = ("V76")
	V90Token = ("V90")
	V91Token = ("V91")
	VersionToken = ("Version" / "V")
	*/
	//--------------------------------------
	//
	//--------------------------------------
	struct mg_token_info_t
	{
		const char* tok_name;
		Token tok_id;
	};
	//--------------------------------------
	//
	//--------------------------------------
	static int compare_token_func(const void* l, const void* r)
	{
		const char* key = (const char*)l;
		const mg_token_info_t* value = (const mg_token_info_t*)r;

		return strcmp(key, value->tok_name);
	}
	//--------------------------------------
	//
	//--------------------------------------
	Token Token_parse(const char* token, int length)
	{
		static mg_token_info_t megaco_tokens[] =
		{
			// A
			{ "A", Token::Add },
			{ "AC", Token::AuditCapability },
			{ "AD", Token::ServiceChangeAddress },
			{ "ADD", Token::Add },
			{ "ANDLGC", Token::AndLgc },
			{ "AT", Token::Audit },
			{ "AU", Token::Authentication },
			{ "AUDIT", Token::Audit },
			{ "AUDITCAPABILITY", Token::AuditCapability },
			{ "AUDITVALUE", Token::AuditValue },
			{ "AUTHENTICATION", Token::Authentication },
			{ "AV", Token::AuditValue },

			// B
			{ "B", Token::Bothway },
			{ "BF", Token::Buffer },
			{ "BOTH", Token::Both },
			{ "BOTHWAY", Token::Bothway },
			{ "BR", Token::Brief },
			{ "BRIEF", Token::Brief },
			{ "BUFFER", Token::Buffer },
			{ "BW", Token::Bothway },


			// C
			{ "C", Token::Context },
			{ "CA", Token::ContextAudit },
			{ "CLT", Token::ContextList },
			{ "CONTEXT", Token::Context },
			{ "CONTEXTATTR", Token::ContextAttr },
			{ "CONTEXTAUDIT", Token::ContextAudit },
			{ "CONTEXTLIST", Token::ContextList },
			{ "CT", Token::ContextAttr },

			// D
			{ "DC", Token::Disconnected },
			{ "DELAY", Token::Delay },
			{ "DIGITMAP", Token::DigitMap },
			{ "DISCONNECTED", Token::Disconnected },
			{ "DL", Token::Delay },
			{ "DM", Token::DigitMap },
			{ "DR", Token::Duration },
			{ "DURATION", Token::Duration },

			// E
			{ "E", Token::Events },
			{ "EB", Token::EventBuffer },
			{ "EG", Token::Emergency },
			{ "EGO", Token::EmergencyOff },
			{ "EGV", Token::EmergencyValue },
			{ "EM", Token::Embed },
			{ "EMBED", Token::Embed },
			{ "EMERGENCY", Token::Emergency },
			{ "EMERGENCYOFF", Token::EmergencyOff },
			{ "EMERGENCYVALUE", Token::EmergencyValue },
			{ "END", Token::END },
			{ "ER", Token::Error },
			{ "ERROR", Token::Error },
			{ "EVENTBUFFER", Token::EventBuffer },
			{ "EVENTS", Token::Events },
			{ "EX", Token::External },
			{ "EXTERNAL", Token::External },

			// F
			{ "FAILOVER", Token::Failover },
			{ "FL", Token::Failover },
			{ "FO", Token::Forced },
			{ "FORCED", Token::Forced },

			// G
			{ "GR", Token::Graceful },
			{ "GRACEFUL", Token::Graceful },

			// H
			{ "H221", Token::H221 },
			{ "H223", Token::H223 },
			{ "H226", Token::H226 },
			{ "HANDOFF", Token::HandOff },
			{ "HO", Token::HandOff },

			// I
			{ "IA", Token::ImmAckRequired },
			{ "IBE", Token::IntByEvent },
			{ "IBS", Token::IntBySigDescr },
			{ "IEPS", Token::IEPSCall },
			{ "IEPSCALL", Token::IEPSCall },
			{ "IMMACKREQUIRED", Token::ImmAckRequired },
			{ "IMMEDIATENOTIFY", Token::ImmediateNotify },
			{ "IN", Token::Inactive },
			{ "INACTIVE", Token::Inactive },
			{ "INSERVICE", Token::InService },
			{ "INTBYEVENT", Token::IntByEvent },
			{ "INTBYSIGDESCR", Token::IntBySigDescr },
			{ "INTERNAL", Token::Internal },
			{ "INTERSIGNAL", Token::Intersignal },
			{ "IR", Token::Iteration },
			{ "IS", Token::Isolate },
			{ "ISOLATE", Token::Isolate },
			{ "IT", Token::Internal },
			{ "ITERATION", Token::Iteration },
			{ "IV", Token::InService },

			// K
			{ "K", Token::TransactionResponseAck },
			{ "KA", Token::KeepActive },
			{ "KEEPACTIVE", Token::KeepActive },

			// L
			{ "L", Token::Local },
			{ "LB", Token::Loopback },
			{ "LOCAL", Token::Local },
			{ "LOCALCONTROL", Token::LocalControl },
			{ "LOCKSTEP", Token::LockStep },
			{ "LOOPBACK", Token::Loopback },

			// M
			{ "M", Token::Media },
			{ "MD", Token::Modem },
			{ "MEDIA", Token::Media },
			{ "MEGACO", Token::MEGACO },
			{ "METHOD", Token::Method },
			{ "MF", Token::Modify },
			{ "MG", Token::MgcIdToTry },
			{ "MGCIDTOTRY", Token::MgcIdToTry },
			{ "MO", Token::Mode },
			{ "MODE", Token::Mode },
			{ "MODEM", Token::Modem },
			{ "MODIFY", Token::Modify },
			{ "MOVE", Token::Move },
			{ "MT", Token::Method },
			{ "MTP", Token::MTP },
			{ "MUX", Token::Mux },
			{ "MV", Token::Move },
			{ "MX", Token::Mux },

			// N
			{ "N", Token::Notify },
			{ "N64", Token::Nx64Kservice },
			{ "NBIN", Token::ImmediateNotify },
			{ "NBNN", Token::NeverNotify },
			{ "NBRN", Token::RegulatedNotify },
			{ "NC", Token::NotifyCompletion },
			{ "NEVERNOTIFY", Token::NeverNotify },
			{ "NOTIFY", Token::Notify },
			{ "NOTIFYCOMPLETION", Token::NotifyCompletion },
			{ "NX64KSERVICE", Token::Nx64Kservice },

			// O
			{ "O", Token::LocalControl },
			{ "OBSERVEDEVENTS", Token::ObservedEvents },
			{ "OE", Token::ObservedEvents },
			{ "ONEWAY", Token::Oneway },
			{ "ONEWAYBOTH", Token::OnewayBoth },
			{ "ONEWAYEXTERNAL", Token::OnewayExternal },
			{ "ONOFF", Token::OnOff },
			{ "OO", Token::OnOff },
			{ "OR", Token::OtherReason },
			{ "ORLGC", Token::ORLgc },
			{ "OS", Token::OutOfService },
			{ "OTHERREASON", Token::OtherReason },
			{ "OUTOFSERVICE", Token::OutOfService },
			{ "OW", Token::Oneway },
			{ "OWB", Token::OnewayBoth },
			{ "OWE", Token::OnewayExternal },


			// P
			{ "P", Token::Reply },
			{ "PACKAGES", Token::Packages },
			{ "PENDING", Token::Pending },
			{ "PF", Token::Profile },
			{ "PG", Token::Packages },
			{ "PN", Token::Pending },
			{ "PR", Token::Priority },
			{ "PRIORITY", Token::Priority },
			{ "PROFILE", Token::Profile },

			// R
			{ "R", Token::Remote },
			{ "RC", Token::ReceiveOnly },
			{ "RE", Token::Reason },
			{ "REASON", Token::Reason },
			{ "RECEIVEONLY", Token::ReceiveOnly },
			{ "REGULATEDNOTIFY", Token::RegulatedNotify },
			{ "REMOTE", Token::Remote },
			{ "REPLY", Token::Reply },
			{ "RESERVEDGROUP", Token::ReservedGroup },
			{ "RESERVEDVALUE", Token::ReservedValue },
			{ "RESETEVENTSDESCRIPTOR", Token::ResetEventsDescriptor },
			{ "RESTART", Token::Restart },
			{ "RG", Token::ReservedGroup },
			{ "RS", Token::Restart },
			{ "RSE", Token::ResetEventsDescriptor },
			{ "RV", Token::ReservedValue },

			// S
			{ "S", Token::Subtract },
			{ "SA", Token::Statistics },
			{ "SC", Token::ServiceChange },
			{ "SEGMENT", Token::Segment },
			{ "SENDONLY", Token::SendOnly },
			{ "SENDRECEIVE", Token::SendReceive },
			{ "SERVICECHANGE", Token::ServiceChange },
			{ "SERVICECHANGEADDRESS", Token::ServiceChangeAddress },
			{ "SERVICECHANGEINC", Token::ServiceChangeInc },
			{ "SERVICES", Token::Services },
			{ "SERVICESTATES", Token::ServiceStates },
			{ "SG", Token::Signals },
			{ "SI", Token::ServiceStates },
			{ "SIC", Token::ServiceChangeInc },
			{ "SIGNALLIST", Token::SignalList },
			{ "SIGNALS", Token::Signals },
			{ "SIGNALTYPE", Token::SignalType },
			{ "SL", Token::SignalList },
			{ "SM", Token::Segment },
			{ "SN", Token::SynchISDN },
			{ "SO", Token::SendOnly },
			{ "SP", Token::LockStep },

			{ "SPADI", Token::SPADirection },
			{ "SPADIRECTION", Token::SPADirection },
			{ "SPAIS", Token::Intersignal },
			{ "SPAREQUESTID", Token::SPARequestID },
			{ "SPARQ", Token::SPARequestID },
			{ "SR", Token::SendReceive },
			{ "ST", Token::Stream },
			{ "STATISTICS", Token::Statistics },
			{ "STREAM", Token::Stream },
			{ "SUBTRACT", Token::Subtract },
			{ "SV", Token::Services },
			{ "SY", Token::SignalType },
			{ "SYNCHISDN", Token::SynchISDN },

			// T	
			{ "T", Token::Transaction },
			{ "TE", Token::Test },
			{ "TERMINATIONSTATE", Token::TerminationState },
			{ "TEST", Token::Test },
			{ "TIMEOUT", Token::TimeOut },
			{ "TO", Token::TimeOut },
			{ "TOPOLOGY", Token::Topology },
			{ "TP", Token::Topology },
			{ "TRANSACTION", Token::Transaction },
			{ "TRANSACTIONRESPONSEACK", Token::TransactionResponseAck },
			{ "TS", Token::TerminationState },

			// V
			{ "V", Token::Version },
			{ "V18", Token::V18 },
			{ "V22", Token::V22 },
			{ "V22B", Token::V22b },
			{ "V32", Token::V32 },
			{ "V32B", Token::V32b },
			{ "V34", Token::V34 },
			{ "V76", Token::V76 },
			{ "V90", Token::V90 },
			{ "V91", Token::V91 },
			{ "VERSION", Token::Version },
		};

		if (*token < '!' && *(token+1) == 0)
		{
			return Token::MEGACO;
		}
		else if (*token == '&' && *(token + 1) == 0)
		{
			return Token::END;
		}

		char buff[128];

		if (length > 0)
		{
			int to_copy = MIN(127, length);
			strncpy(buff, token, to_copy);
			buff[to_copy] = 0;
		}
		else
		{
			strncpy(buff, token, 127);
			buff[127] = 0;
		}
	
		rtl::strupr(buff);

		mg_token_info_t* result = (mg_token_info_t*)bsearch(buff, megaco_tokens, sizeof(megaco_tokens) / sizeof(mg_token_info_t), sizeof(mg_token_info_t), compare_token_func);

		return result != nullptr ? result->tok_id : Token::_Custom;
	}
	//--------------------------------------
	//
	//--------------------------------------
	const char* Token_toStringLong(Token token)
	{
		static const char* l_megaco_tokens[] =
		{
			"Add",								// "Add"					AddToken
			"ANDLgc",							// "ANDLgc"					AndAUDITSelectToken
			"Audit",							// "Audit"					AuditToken
			"AuditCapability",					// "AuditCapability"		AuditCapToken
			"AuditValue",						// "AuditValue"				AuditValueToken
			"Authentication",					// "Authentication"			AuthToken 

			"Both", 							// "Both"					BothToken 
			"Bothway",							// "Bothway"				BothwayToken 
			"Brief",							// "Brief"					BriefToken 
			"Buffer",							// "Buffer"					BufferToken 

			"Context",							// "Context"				CtxToken 
			"ContextAttr",						// "ContextAudit"			ContextAuditToken
			"ContextAudit",						// "ContextAttr"			ContextAttrToken 
			"ContextList",						// "ContextList"			ContextListToken 

			"Delay",							// "Delay"					DelayToken 
			"DigitMap",							// "DigitMap"				DigitMapToken 
			"Disconnected",						// "Disconnected"			DisconnectedToken 
			"Duration",							// "Duration"				DurationToken 

			"Embed",							// "Embed"					EmbedToken 
			"Emergency",						// "Emergency"				EmergencyToken 
			"EmergencyOff",						// "EmergencyOff"			EmergencyOffToken 
			"EmergencyValue",					// "EmergencyValue"			EmergencyValueToken
			"END",								// "END"					SegmentationCompleteToken
			"Error",							// "Error"					ErrorToken 
			"EventBuffer",						// "EventBuffer"			EventBufferToken 
			"Events",							// "Events"					EventsToken 
			"External",							// "External"				ExternalToken 

			"Failover",							// "Failover"				FailoverToken
			"Forced",							// "Forced"					ForcedToken 

			"Graceful",							// "Graceful"				GracefulToken

			"H221",								// "H221"					H221Token 
			"H223",								// "H223"					H223Token 
			"H226",								// "H226"					H226Token 
			"HandOff",							// "HandOff"				HandOffToken

			"IEPSCall",							// "IEPSCall"				IEPSToken 
			"ImmAckRequired",					// "ImmAckRequired"			ImmAckRequiredToken
			"ImmediateNotify",					// "ImmediateNotify"		NotifyImmediateToken 
			"Inactive",							// "Inactive"				InactiveToken 
			"InService",						// "InService"				InSvcToken 
			"IntByEvent",						// "IntByEvent"				InterruptByEventToken 
			"IntBySigDescr",					// "IntBySigDescr"			InterruptByNewSignalsDescrToken 
			"Internal",							// "Internal"				InternalToken 
			"Intersignal",						// "Intersignal"			IntsigDelayToken 
			"Isolate",							// "Isolate"				IsolateToken 
			"Iteration",						// "Iteration"				IterationToken 

			"KeepActive",						// "KeepActive"				KeepActiveToken 

			"Local",							// "Local" ,				LocalToken 
			"LocalControl",						// "LocalControl" ,			LocalControlToken
			"LockStep",							// "LockStep" ,				LockStepToken 
			"Loopback",							// "Loopback" ,				LoopbackToken 

			"Media",							// "Media" ,				MediaToken 
			"MEGACO",							// "MEGACO" ,				MegacopToken 
			"Method",							// "Method" ,				MethodToken 
			"MgcIdToTry",						// "MgcIdToTry" ,			MgcIdToken 
			"Mode",								// "Mode" ,					ModeToken 
			"Modem",							// "Modem" ,				ModemToken 
			"Modify",							// "Modify" ,				ModifyToken 
			"Move",								// "Move" ,					MoveToken 
			"MTP",								// "MTP",					MTPToken 
			"Mux",								// "Mux" ,					MuxToken 

			"NeverNotify",						// "NeverNotify"			NeverNotifyToken 
			"Notify",							// "Notify"					NotifyToken 
			"NotifyCompletion",					// "NotifyCompletion"		NotifyCompletionToken
			"Nx64Kservice",						// "Nx64Kservice"			Nx64kToken 

			"ObservedEvents",					// "ObservedEvents"			ObservedEventsToken
			"Oneway",							// "Oneway" ,				OnewayToken 
			"OnewayBoth",						// "OnewayBoth"				OnewayBothToken 
			"OnewayExternal",					// "OnewayExternal"			OnewayExternalToken
			"OnOff",							// "OnOff"					OnOffToken 
			"ORLgc",							// "ORLgc"					OrAUDITselectToken 
			"OtherReason",						// "OtherReason"			OtherReasonToken 
			"OutOfService",						// "OutOfService"			OutOfSvcToken 

			"Packages",							// "Packages"				PackagesToke
			"Pending",							// "Pending"				PendingToken 
			"Priority",							// "Priority"				PriorityToke
			"Profile",							// "Profile"				ProfileToken 

			"Reason",							// "Reason"					ReasonToken 
			"ReceiveOnly",						// "ReceiveOnly"			RecvonlyToken
			"RegulatedNotify",					// "RegulatedNotify"		NotifyRegulatedToken 
			"Remote",							// "Remote"					RemoteToken 
			"Reply",							// "Reply"					ReplyToken 
			"ReservedGroup",					// "ReservedGroup"			ReservedGroupToken 
			"ReservedValue",					// "ReservedValue"			ReservedValueToken 
			"ResetEventsDescriptor",			// "ResetEventsDescriptor"	ResetEventsDescriptorToken
			"Restart",							// "Restart"				RestartToken 

			"Segment",							// "Segment" ,				MessageSegmentToken
			"SendOnly",							// "SendOnly"				SendonlyToken 
			"SendReceive",						// "SendReceive"			SendrecvToken 
			"Services",							// "Services"				ServicesToken 
			"ServiceStates",					// "ServiceStates"			ServiceStatesToken 
			"ServiceChange",					// "ServiceChange"			ServiceChangeToken 
			"ServiceChangeAddress",				// "ServiceChangeAddress"	ServiceChangeAddressToken 
			"ServiceChangeInc",					// "ServiceChangeInc"		ServiceChangeIncompleteToken 
			"SignalList",						// "SignalList"				SignalListToken 
			"Signals",							// "Signals"				SignalsToken 
			"SignalType",						// "SignalType"				SignalTypeToken 
			"SPADirection",						// "SPADirection",			DirectionToken
			"SPARequestID",						// "SPARequestID"			RequestIDToken 
			"Statistics",						// "Statistics"				StatsToken 
			"Stream",							// "Stream"					StreamToken 
			"Subtract",							// "Subtract"				SubtractToken 
			"SynchISDN",						// "SynchISDN"				SynchISDNToken 

			"TerminationState",					// "TerminationState"		TerminationStateToken 
			"Test",								// "Test"					TestToken 
			"TimeOut",							// "TimeOut"				TimeOutToken 
			"Topology",							// "Topology"				TopologyToken 
			"Transaction",						// "Transaction"			TransToken 
			"TransactionResponseAck",			// "TransactionResponseAck"	ResponseAckToken

			"V18",								// "V18"					V18Token 
			"V22",								// "V22"					V22Token 
			"V22b",								// "V22b"					V22bisToken 
			"V32",								// "V32"					V32Token 
			"V32b",								// "V32b"					V32bisToken 
			"V34",								// "V34"					V34Token 
			"V76",								// "V76"					V76Token 
			"V90",								// "V90"					V90Token 
			"V91",								// "V91"					V91Token 
			"Version",							// "Version"				VersionToken
		};

		int index = (int)token;

		return (index >= 0 && token < Token::_Last) ? l_megaco_tokens[index] : "";
	}
	//--------------------------------------
	//
	//--------------------------------------
	const char* Token_toStringShort(Token token)
	{
		static const char* megaco_tokens[] =
		{
			"A",					// "Add"					AddToken
			"ANDLgc",				// "ANDLgc"					AndAUDITSelectToken
			"AT",					// "Audit"					AuditToken
			"AC",					// "AuditCapability"		AuditCapToken
			"AV",					// "AuditValue"				AuditValueToken
			"AU",					// "Authentication"			AuthToken 

			"B", 					// "Both"					BothToken 
			"BW",					// "Bothway"				BothwayToken 
			"BR",					// "Brief"					BriefToken 
			"BF",					// "Buffer"					BufferToken 

			"C",					// "Context"				CtxToken 
			"CT",					// "ContextAudit"			ContextAuditToken
			"CA",					// "ContextAttr"			ContextAttrToken 
			"CLI",					// "ContextList"			ContextListToken 

			"DL",					// "Delay"					DelayToken 
			"DM",					// "DigitMap"				DigitMapToken 
			"DC",					// "Disconnected"			DisconnectedToken 
			"DR",					// "Duration"				DurationToken 

			"EM",					// "Embed"					EmbedToken 
			"EG",					// "Emergency"				EmergencyToken 
			"EGO",					// "EmergencyOff"			EmergencyOffToken 
			"EGV",					// "EmergencyValue"			EmergencyValueToken
			"&",					// "END"					SegmentationCompleteToken
			"ER",					// "Error"					ErrorToken 
			"EB",					// "EventBuffer"			EventBufferToken 
			"E",					// "Events"					EventsToken 
			"EX",					// "External"				ExternalToken 

			"FL",					// "Failover"				FailoverToken
			"FO",					// "Forced"					ForcedToken 

			"GR",					// "Graceful"				GracefulToken

			"H221",					// "H221"					H221Token 
			"H223",					// "H223"					H223Token 
			"H226",					// "H226"					H226Token 
			"HO",					// "HandOff"				HandOffToken

			"IEPS",					// "IEPSCall"				IEPSToken 
			"IA",					// "ImmAckRequired"			ImmAckRequiredToken
			"NBIN",					// "ImmediateNotify"		NotifyImmediateToken 
			"IN",					// "Inactive"				InactiveToken 
			"IV",					// "InService"				InSvcToken 
			"IBE",					// "IntByEvent"				InterruptByEventToken 
			"IBS",					// "IntBySigDescr"			InterruptByNewSignalsDescrToken 
			"IT",					// "Internal"				InternalToken 
			"SPAIS",				// "Intersignal"			IntsigDelayToken 
			"IS",					// "Isolate"				IsolateToken 
			"IR",					// "Iteration"				IterationToken 

			"KA",					// "KeepActive"				KeepActiveToken 

			"L",					// "Local" ,				LocalToken 
			"O",					// "LocalControl" ,			LocalControlToken
			"SP",					// "LockStep" ,				LockStepToken 
			"LB",					// "Loopback" ,				LoopbackToken 

			"M",					// "Media" ,				MediaToken 
			"!",					// "MEGACO" ,				MegacopToken 
			"MT",					// "Method" ,				MethodToken 
			"MG",					// "MgcIdToTry" ,			MgcIdToken 
			"MO",					// "Mode" ,					ModeToken 
			"MD",					// "Modem" ,				ModemToken 
			"MF",					// "Modify" ,				ModifyToken 
			"MV",					// "Move" ,					MoveToken 
			"MTP",					// "MTP",					MTPToken 
			"MX",					// "Mux" ,					MuxToken 

			"NBNN",					// "NeverNotify"			NeverNotifyToken 
			"N",					// "Notify"					NotifyToken 
			"NC",					// "NotifyCompletion"		NotifyCompletionToken
			"N64",					// "Nx64Kservice"			Nx64kToken 

			"OE",					// "ObservedEvents"			ObservedEventsToken
			"OW",					// "Oneway" ,				OnewayToken 
			"OWB",					// "OnewayBoth"				OnewayBothToken 
			"OWE",					// "OnewayExternal"			OnewayExternalToken
			"OO",					// "OnOff"					OnOffToken 
			"ORLgc",				// "ORLgc"					OrAUDITselectToken 
			"OR",					// "OtherReason"			OtherReasonToken 
			"OS",					// "OutOfService"			OutOfSvcToken 

			"PG",					// "Packages"				PackagesToke
			"PN",					// "Pending"				PendingToken 
			"PR",					// "Priority"				PriorityToke
			"PF",					// "Profile"				ProfileToken 

			"RE",					// "Reason"					ReasonToken 
			"RC",					// "ReceiveOnly"			RecvonlyToken
			"NBRN",					// "RegulatedNotify"		NotifyRegulatedToken 
			"R",					// "Remote"					RemoteToken 
			"P",					// "Reply"					ReplyToken 
			"RG",					// "ReservedGroup"			ReservedGroupToken 
			"RV",					// "ReservedValue"			ReservedValueToken 
			"RSE",					// "ResetEventsDescriptor"	ResetEventsDescriptorToken
			"RS",					// "Restart"				RestartToken 

			"SM",					// "Segment" ,				MessageSegmentToken
			"SO",					// "SendOnly"				SendonlyToken 
			"SR",					// "SendReceive"			SendrecvToken 
			"SV",					// "Services"				ServicesToken 
			"SI",					// "ServiceStates"			ServiceStatesToken 
			"SC",					// "ServiceChange"			ServiceChangeToken 
			"AD",					// "ServiceChangeAddress"	ServiceChangeAddressToken 
			"SIC",					// "ServiceChangeInc"		ServiceChangeIncompleteToken 
			"SL",					// "SignalList"				SignalListToken 
			"SG",					// "Signals"				SignalsToken 
			"SY",					// "SignalType"				SignalTypeToken 
			"SPADI",				// "SPADirection",			DirectionToken
			"SPARQ",				// "SPARequestID"			RequestIDToken 
			"SA",					// "Statistics"				StatsToken 
			"ST",					// "Stream"					StreamToken 
			"S",					// "Subtract"				SubtractToken 
			"SNN",					// "SynchISDN"				SynchISDNToken 

			"TS",					// "TerminationState"		TerminationStateToken 
			"TE",					// "Test"					TestToken 
			"TO",					// "TimeOut"				TimeOutToken 
			"TP",					// "Topology"				TopologyToken 
			"T",					// "Transaction"			TransToken 
			"K",					// "TransactionResponseAck"	ResponseAckToken

			"V18",					// "V18"					V18Token 
			"V22",					// "V22"					V22Token 
			"V22b",					// "V22b"					V22bisToken 
			"V32",					// "V32"					V32Token 
			"V32b",					// "V32b"					V32bisToken 
			"V34",					// "V34"					V34Token 
			"V76",					// "V76"					V76Token 
			"V90",					// "V90"					V90Token 
			"V91",					// "V91"					V91Token 
			"V",					// "Version"				VersionToken
		};

		int index = (int)token;

		return (index >= 0 && token < Token::_Last) ? megaco_tokens[index] : "";
	}
	//--------------------------------------
	//
	//--------------------------------------
	static bool g_token_names_mode = false;
	//--------------------------------------
	//
	//--------------------------------------
	void setTokenNameMode(bool long_names)
	{
		g_token_names_mode = long_names;
	}
	//--------------------------------------
	//
	//--------------------------------------
	const char* Token_toString(Token token)
	{
		return g_token_names_mode ? Token_toStringLong(token) : Token_toStringShort(token);
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool isValidCommandToken(Token token)
	{
		switch (token)
		{
		case Token::Add:
		case Token::Modify:
		case Token::Move:
		case Token::Subtract:
		case Token::AuditCapability:
		case Token::AuditValue:
		case Token::ServiceChange:
		case Token::Notify:
			return true;
		default:
			break;
		}

		return false;
	}
	//--------------------------------------
	//
	//--------------------------------------
	struct mg_error_info_t
	{
		uint16_t code;
		const char* text;
	};
	static int compare_error_code(const void* left, const void* right)
	{
		mg_error_info_t* l = (mg_error_info_t*)left;
		mg_error_info_t* r = (mg_error_info_t*)right;
		return l->code - r->code;
	}
	const char* ErrorCode_toString(ErrorCode code)
	{
		/*
		Error code #: 400
		Name: Syntax error in message

		code #: 401
		Name: Protocol error

		Error code #: 402
		Name: Unauthorized

		Error code #: 403
		Name: Syntax error in TransactionRequest

		4.2.5 Error code #: 406
		Name: Version not supported

		4.2.6 Error code #: 410
		Name: Incorrect identifier

		4.2.7 Error code #: 411
		Name: The transaction refers to an unknown ContextID

		4.2.8 Error code #: 412
		Name: No ContextIDs available

		4.2.9 Error code #: 413
		Name: Number of transactions in message exceeds maximum

		4.2.10 Error code #: 421
		Name: Unknown action or illegal combination of actions

		4.2.11 Error code #: 422
		Name: Syntax error in action

		4.2.12 Error code #: 430
		Name: Unknown TerminationID

		4.2.13 Error code #: 431
		Name: No TerminationID matched a wildcard

		4.2.14 Error code #: 432
		Name: Out of TerminationIDs or no TerminationID available

		4.2.15 Error code #: 433
		Name: TerminationID is already in a context

		4.2.16 Error code #: 434
		Name: Max number of terminations in a context exceeded

		4.2.17 Error code #: 435
		Name: Termination ID is not in specified context

		4.2.18 Error code #: 440
		Name: Unsupported or unknown package

		4.2.19 Error code #: 441
		Name: Missing remote or local descriptor

		4.2.20 Error code #: 442
		Name: Syntax error in command

		4.2.21 Error code #: 443
		Name: Unsupported or unknown command

		4.2.22 Error code #: 444
		Name: Unsupported or unknown descriptor

		4.2.23 Error code #: 445
		Name: Unsupported or unknown property

		4.2.24 Error code #: 446
		Name: Unsupported or unknown parameter

		4.2.25 Error code #: 447
		Name: Descriptor not legal in this command

		4.2.26 Error code #: 448
		Name: Descriptor appears twice in a command

		4.2.27 Error code #: 449
		Name: Unsupported or unknown parameter or property value

		4.2.28 Error code #: 450
		Name: No such property in this package

		4.2.29 Error code #: 451
		Name: No such event in this package

		4.2.30 Error code #: 452
		Name: No such signal in this package

		4.2.31 Error code #: 453
		Name: No such statistic in this package

		4.2.32 Error code #: 454
		Name: No such parameter value in this package

		4.2.33 Error code #: 455
		Name: Property illegal in this descriptor

		4.2.34 Error code #: 456
		Name: Property appears twice in this descriptor

		4.2.35 Error code #: 457
		Name: Missing parameter in signal or event

		4.2.36 Error code #: 458
		Name: Unexpected event/request ID

		4.2.37 Error code #: 460
		Name: Unable to set statistic on stream

		4.2.38 Error code #: 471
		Name: Implied add for multiplex failure

		4.2.39 Error code #: 472
		Name: Required information missing

		4.2.40 Error code #: 473
		Name: Conflicting property values

		4.2.41 Error code #: 500
		Name: Internal software failure in the MG or MGC

		4.2.42 Error code #: 501
		Name: Not implemented

		4.2.43 Error code #: 502
		Name: Not ready

		4.2.44 Error code #: 503
		Name: Service unavailable

		4.2.45 Error code #: 504
		Name: Command received from unauthorized entity

		4.2.46 Error code #: 505
		Name: Transaction request received before a ServiceChange reply has been received

		4.2.47 Error code #: 506
		Name: Number of TransactionPendings exceeded

		4.2.48 Error code #: 507
		Name: Unknown Control Association

		4.2.49 Error code #: 510
		Name: Insufficient resources

		4.2.50 Error code #: 511
		Name: Temporarily busy

		4.2.51 Error code #: 512
		Name: Media gateway unequipped to detect requested event

		4.2.52 Error code #: 513
		Name: Media gateway unequipped to generate requested signals

		4.2.53 Error code #: 514
		Name: Media gateway cannot send the specified announcement

		4.2.54 Error code #: 515
		Name: Unsupported media type

		4.2.55 Error code #: 517
		Name: Unsupported or invalid mode

		4.2.56 Error code #: 518
		Name: Event buffer full

		4.2.57 Error code #: 519
		Name: Out of space to store digit map

		4.2.58 Error code #: 520
		Name: Digit map undefined in the MG

		4.2.59 Error code #: 521
		Name: Termination is "Service Changing"

		4.2.60 Error code #: 522
		Name: Functionality requested in topology triple not supported

		4.2.61 Error code #: 526
		Name: Insufficient bandwidth

		4.2.62 Error code #: 529
		Name: Internal hardware failure in MG

		4.2.63 Error code #: 530
		Name: Temporary network failure

		4.2.64 Error code #: 531
		Name: Permanent network failure

		4.2.65 Error code #: 532
		Name: Audited property, statistic, event or signal does not exist

		4.2.66 Error code #: 533
		Name: Response exceeds maximum transport PDU size

		4.2.67 Error code #: 534
		Name: Illegal write or read-only property

		4.2.68 Error code #: 542
		Name: Command is not allowed on this termination

		4.2.69 Error code #: 543
		Name: MGC requested event detection timestamp not supported

		4.2.70 Error code #: 581
		Name: Does not exist
		*/

		static mg_error_info_t names[] =
		{
			{ 400, "Syntax error in message" },
			{ 401, "Protocol error" },
			{ 402, "Unauthorized" },
			{ 403, "Syntax error in TransactionRequest" },
			{ 406, "Version not supported" },
			{ 410, "Incorrect identifier" },
			{ 411, "The transaction refers to an unknown ContextID" },
			{ 412, "No ContextIDs available" },
			{ 413, "Number of transactions in message exceeds maximum" },
			{ 421, "Unknown action or illegal combination of actions" },
			{ 422, "Syntax error in action" },
			{ 430, "Unknown TerminationID" },
			{ 431, "No TerminationID matched a wildcard" },
			{ 432, "Out of TerminationIDs or no TerminationID available" },
			{ 433, "TerminationID is already in a context" },
			{ 434, "Max number of terminations in a context exceeded" },
			{ 435, "Termination ID is not in specified context" },
			{ 440, "Unsupported or unknown package" },
			{ 441, "Missing remote or local descriptor" },
			{ 442, "Syntax error in command" },
			{ 443, "Unsupported or unknown command" },
			{ 444, "Unsupported or unknown descriptor" },
			{ 445, "Unsupported or unknown property" },
			{ 446, "Unsupported or unknown parameter" },
			{ 447, "Descriptor not legal in this command" },
			{ 448, "Descriptor appears twice in a command" },
			{ 449, "Unsupported or unknown parameter or property value" },
			{ 450, "No such property in this package" },
			{ 451, "No such event in this package" },
			{ 452, "No such signal in this package" },
			{ 453, "No such statistic in this package" },
			{ 454, "No such parameter value in this package" },
			{ 455, "Property illegal in this descriptor" },
			{ 456, "Property appears twice in this descriptor" },
			{ 457, "Missing parameter in signal or event" },
			{ 458, "Unexpected event/request ID" },
			{ 460, "Unable to set statistic on stream" },
			{ 471, "Implied add for multiplex failure" },
			{ 472, "Required information missing" },
			{ 473, "Conflicting property values" },
			{ 500, "Internal software failure in the MG or MGC" },
			{ 501, "Not implemented" },
			{ 502, "Not ready" },
			{ 503, "Service unavailable" },
			{ 504, "Command received from unauthorized entity" },
			{ 505, "Transaction request received before a ServiceChange reply has been received" },
			{ 506, "Number of TransactionPendings exceeded" },
			{ 507, "Unknown Control Association" },
			{ 510, "Insufficient resources" },
			{ 511, "Temporarily busy" },
			{ 512, "Media gateway unequipped to detect requested event" },
			{ 513, "Media gateway unequipped to generate requested signals" },
			{ 514, "Media gateway cannot send the specified announcement" },
			{ 515, "Unsupported media type" },
			{ 517, "Unsupported or invalid mode" },
			{ 518, "Event buffer full" },
			{ 519, "Out of space to store digit map" },
			{ 520, "Digit map undefined in the MG" },
			{ 521, "Termination is 'Service Changing'" },
			{ 522, "Functionality requested in topology triple not supported" },
			{ 526, "Insufficient bandwidth" },
			{ 529, "Internal hardware failure in MG" },
			{ 530, "Temporary network failure" },
			{ 531, "Permanent network failure" },
			{ 532, "Audited property, statistic, event or signal does not exist" },
			{ 533, "Response exceeds maximum transport PDU size" },
			{ 534, "Illegal write or read-only property" },
			{ 542, "Command is not allowed on this termination" },
			{ 543, "MGC requested event detection timestamp not supported" },
			{ 581, "Does not exist" },
		};

		mg_error_info_t key = { (uint16_t)code, nullptr };

		mg_error_info_t* result = (mg_error_info_t*)bsearch(&key, names, sizeof(names) / sizeof(mg_error_info_t), sizeof(mg_error_info_t), compare_error_code);

		return result != nullptr ? result->text : "Unexpected error";
	}
	//--------------------------------------
	//
	//--------------------------------------
	const char* Reason_toString(Reason code)
	{
		/*
		5.2.1 Reason #: 900
		Name: Service restored

		5.2.2 Reason #: 901
		Name: Cold boot

		Comment: This reason code only applies for TerminationID root.
		5.2.3 Reason #: 902
		Name: Warm boot

		5.2.4 Reason #: 903
		Name: MGC directed change

		5.2.5 Reason #: 904
		Name: Termination malfunctioning

		5.2.6 Reason #: 905
		Name: Termination taken out of service

		5.2.7 Reason #: 906
		Name: Loss of lower layer connectivity (e.g., downstream sync)

		5.2.8 Reason #: 907
		Name: Transmission failure

		5.2.9 Reason #: 908
		Name: MG impending failure

		5.2.10 Reason #: 909
		Name: MGC impending failure

		5.2.11 Reason #: 910
		Name: Media capability failure

		5.2.12 Reason #: 911
		Name: Modem capability failure

		5.2.13 Reason #: 912
		Name: MUX capability failure

		5.2.14 Reason #: 913
		Name: Signal capability failure

		5.2.15 Reason #: 914
		Name: Event capability failure

		5.2.16 Reason #: 915
		Name: State loss

		5.2.17 Reason #: 916
		Name: Packages change

		5.2.18 Reason #: 917
		Name: Capabilities change

		5.2.19 Reason #: 918
		Name: Cancel graceful

		5.2.20 Reason #: 919
		Name: Warm failover

		5.2.21 Reason #: 920
		Name: Cold failover
		*/

		static const char* names[] =
		{
			"Service restored",
			"Cold boot",
			"Warm boot",
			"MGC directed change",
			"Termination malfunctioning",
			"Termination taken out of service",
			"Loss of lower layer connectivity",
			"Transmission failure",
			"MG impending failure",
			"MGC impending failure",
			"Media capability failure",
			"Modem capability failure",
			"MUX capability failure",
			"Signal capability failure",
			"Event capability failure",
			"State loss",
			"Packages change",
			"Capabilities change",
			"Cancel graceful",
			"Warm failover",
			"Cold failover",
		};

		int index = (int)code;

		return index >= 900 && index <= 920 ? names[index-900] : nullptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	Reason Reason_parse(const char* reason, rtl::String& text)
	{
		char code_buff[16];

		if (reason == nullptr || reason[0] != '"')
			return Reason::_Error;

		const char* sp = strchr(reason, ' ');
		const char* end_quote = strchr(reason + 1, '"');

		if (end_quote == nullptr)
			return Reason::_Error;

		if (sp != nullptr)
		{
			text.assign(sp + 1, PTR_DIFF(end_quote, (sp + 1)));

			int to_copy = MIN(15, PTR_DIFF(sp, (reason + 1)));
			strncpy(code_buff, reason + 1, to_copy);
			code_buff[to_copy] = 0;
		}
		else
		{
			int to_copy = MIN(15, PTR_DIFF(end_quote, (reason + 1)));
			strncpy(code_buff, reason + 1, to_copy);
			code_buff[to_copy] = 0;
		}

		return (Reason)strtoul(code_buff, nullptr, 10);
	}
	//--------------------------------------
	// ;The values 0x0, 0xFFFFFFFE and 0xFFFFFFFF are reserved.
	// ; '-' is used for NULL context. '*' is ALL. '$' is CHOOSE.
	// ContextID = (UINT32 / "*" / "-" / "$")
	//--------------------------------------
	bool parse_context_id(const char* text, uint32_t& id)
	{
		if (text == nullptr || text[0] == 0)
			return false;

		text = rtl::skip_SEP(text);

		if (isdigit(text[0]))
		{
			// numeric id
			id = strtoul(text, nullptr, 10);

			// как быть с зарезервированными номерами -- никак пришли и пришли ? нет нуж отшивать!

			if (id == MG_CONTEXT_NULL || id == MG_CONTEXT_ALL || id == MG_CONTEXT_CHOOSE)
			{
				return false;
			}

			return true;
		}
		else if (text[1] == 0)
		{
			// wildcards and NULL
			if (text[0] == '$')
			{
				id = MG_CONTEXT_CHOOSE;
			}
			else if (text[0] == '*')
			{
				id = MG_CONTEXT_ALL;
			}
			else if (text[0] == '-')
			{
				id = MG_CONTEXT_NULL;
			}
			else
			{
				return false;
			}

			return true;
		}

		return false;
	}
	//--------------------------------------
	//
	//--------------------------------------
	TerminationStreamMode TerminationStreamMode_parse(const char* text)
	{
		return (TerminationStreamMode)Token_parse(text);
	}
	//--------------------------------------
	//
	//--------------------------------------
	const char* TerminationStreamMode_toString(TerminationStreamMode mode)
	{
		return Token_toString((Token)mode);
	}
	//------------------------------------------------------------------------------
	//
	//------------------------------------------------------------------------------
	const char* TerminationServiceState_toString(TerminationServiceState state)
	{
		return Token_toString((Token)state);
	}
	//------------------------------------------------------------------------------
	const char* TerminationType_toString(TerminationType type)
	{
		switch (type)
		{
		case TerminationType::Root:
			return "root";
		case TerminationType::SRTP:
			return "srtp";
		case TerminationType::RTP:
			return "rtp";
		case TerminationType::WebRTC:
			return "webrtc";
		case TerminationType::All:
			return "all";
		case TerminationType::Choose:
			return "choose";
		case TerminationType::IVR_Player:
			return "ivr_player";
		case TerminationType::IVR_Record:
			return "ivr_recorder";
		case TerminationType::Device:
			return "device";
		}

		return "Unknown";
	}
	//--------------------------------------
	//
	//--------------------------------------
	TopologyDirection TopologyDirection_parse(const char* text)
	{
		return (TopologyDirection)Token_parse(text);
	}
	//--------------------------------------
	//
	//--------------------------------------
	const char* TopologyDirection_toString(TopologyDirection direction)
	{
		return Token_toString((Token)direction);
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_MG_API EventParams::~EventParams()
	{
		//...
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_MG_API IVR_EventParams::~IVR_EventParams()
	{
		m_fileList.clear();
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_MG_API Field* ObservedEventParams::makeEvent()
	{
		char buffer[128];
		getTermId().toString(buffer, 128);

		Field* action = NEW Field(Token::Context, getContextId());
		Field* notify = action->addField(Token::Notify, buffer);
		Field* observedEvents = notify->addField(Token::ObservedEvents, m_requestId);

		rtl::DateTime dt;
		char event_text[128];
		std_snprintf(event_text, 128, "%02u%02u%02uT%02u%02u%02u%02u:/%s",
			dt.getYear(),
			dt.getMonth(),
			dt.getDay(),
			dt.getHour(),
			dt.getMinute(),
			dt.setSecond(),
			dt.getMilliseconds() / 10,
			m_eventName);

		m_eventNode = observedEvents->addField(event_text);

		return action;
	}

	RTX_MG_API Field* DTMF_EventParams::makeEvent()
	{
		Field* action = ObservedEventParams::makeEvent();
	
		char ch[2] = { m_dtmf, 0 };

		//if (dtmf == '*')
		//	ch[0] = 'E';
		//else
		// in H.248 # simbol must be changed to F simbol!!!
		if (ch[0] == '#')
			ch[0] = 'F';

		m_eventNode->addField("ds", ch);

		return action;
	}

	RTX_MG_API Field* VAD_EventParams::makeEvent()
	{
		Field* action = ObservedEventParams::makeEvent();

		m_eventNode->addField("state", m_state ? "up" : "down");

		return action;
	}

	RTX_MG_API Field* IVR_EventParams::makeEvent()
	{
		Field* action = ObservedEventParams::makeEvent();

		int count = m_fileList.getCount();

		if (count > 1)
		{
			if (m_stopTime == 0)
			{
				if (count == 0)
				{
					m_eventNode->addField("filesfound", "0");
				}
				else
				{
					for (int i = 0; i < count; i++)
					{
						m_eventNode->addField("fileerror", m_fileList[i]);
					}
				}
			}
			else
			{
				for (int i = 0; i < count; i++)
				{
					if (i == (count - 1))
					{
						m_eventNode->addField("filestop", m_fileList[i]);
					}
					else
					{
						m_eventNode->addField("fileerror", m_fileList[i]);
					}
				}
				m_eventNode->addField("stoptime", m_stopTime);
			}
		}
		else
		{
			if (m_stopTime == 0)
			{
				m_eventNode->addField("endplay", "stop");
			}
			else
			{
				m_eventNode->addField("filestop", m_fileList.getFirst());
				m_eventNode->addField("stoptime", m_stopTime);
			}
		}

		return action;
	}

	//--------------------------------------
	//
	//--------------------------------------
	void IVR_EventParams::copyFileList(const rtl::StringList& fileList)
	{
		for (int i = 0; i < fileList.getCount(); i++)
		{
			const rtl::String& filename = fileList[i];
			int index = filename.lastIndexOf(FS_PATH_DELIMITER);
			rtl::String newfilename = '\"';
			newfilename << (index == -1 ? filename : filename.substring(index + 1)) << '\"';
			newfilename.replace(FS_PATH_DELIMITER, '/');
			m_fileList.add(newfilename);
		}
	}


	RTX_MG_API Field* FAX_EventParams::makeEvent()
	{
		Field* action = ObservedEventParams::makeEvent();

		m_eventNode->addField("result", m_error ? "error" : "done");

		return action;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_MG_API Field* ServiceChangedEventParams::makeEvent()
	{
		// готовим сообщение...
		char buffer[128];

		getTermId().toString(buffer, 128);

		Field* action = NEW Field(Token::Context, getContextId());
		Field* service_change = action->addField(Token::ServiceChange, buffer);
		m_services = service_change->addField(Token::Services);
		m_services->addField(Token::Method, Token_toString(m_method));
		rtl::String reason;
		reason << '"' << (uint32_t)m_reason << ' ' << Reason_toString(m_reason) << '"';
		m_services->addField(Token::Reason, reason);

		return action;
	}
	//--------------------------------------
	//
	//--------------------------------------
	RTX_MG_API Field* NoVoiceEventParams::makeEvent()
	{
		Field* action = ServiceChangedEventParams::makeEvent();
		// X-SC = 1* (NAME EQUAL parmValue [COMMA])
		// extension = extensionParameter parmValue
		// extensionParameter = "X" ("-" / "+") 1*6(ALPHA / DIGIT)
		m_services->addField("X-time", m_xtime);

		return action;
	}
}
//--------------------------------------
