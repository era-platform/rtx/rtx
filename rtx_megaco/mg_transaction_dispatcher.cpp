﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_transaction.h"
#include "mg_transaction_dispatcher.h"
#include "mg_transport_manager.h"

namespace h248
{
	//-------------------------------------
	//
	//-------------------------------------
	TransactionDispatcher::TransactionDispatcher() :
		m_tranSync("mg-trans-disp"), m_requestList(compareRequestByObj, compareRequestByKey)
	{
		m_id = 0;
		m_requestReceiver = nullptr;
		srand((int)time(nullptr));
		m_tranIdGen = (uint32_t)rand();
		m_mgcVersion = 3;
	}
	//-------------------------------------
	//
	//-------------------------------------
	TransactionDispatcher::~TransactionDispatcher()
	{
		destroy();
	}
	//-------------------------------------
	//
	//-------------------------------------
	void TransactionDispatcher::initialize(uint32_t id, IRequestReceiver* receiver, int mgcVersion)
	{
		LOG_TRANS("trn-d", "cid:%u : initialize : with id:%u mgc-version:%u...", id, id, mgcVersion);
		m_id = id;
		m_requestReceiver = receiver;
		m_mgcVersion = mgcVersion;

		LOG_TRANS("trn-d", "cid:%u : initialize : done", id);
	}
	//-------------------------------------
	//
	//-------------------------------------
	void TransactionDispatcher::destroy()
	{
		LOG_TRANS("trn-d", "cid:%u : destroy...none", m_id);

		stop();
	}
	//--------------------------------------
	// 
	//--------------------------------------
	void TransactionDispatcher::stop()
	{
		LOG_TRANS("trn-d", "cid:%u : stop...none", m_id);

		// отменяем все связанные с маршрутом транзакции!
		TransactionManager::cancelMgcTransactions(m_id);

	}
	//--------------------------------------
	// 
	//--------------------------------------
	void TransactionDispatcher::handle_Disconnected(const Route& route)
	{
		// убить все транзакции связанные с данным маршрутом. в общем случае всех
		LOG_TRANS("trn-d", "cid:%u : handle disconnected for %s:%u...", m_id, inet_ntoa(route.remoteAddress), route.remotePort);

		m_requestReceiver->IRequestReceiver_transportDisconnected(route);
	}
	//--------------------------------------
	// 
	//--------------------------------------
	void TransactionDispatcher::handle_PacketArrival(const Message* message, const Route& route)
	{
		LOG_TRANS("trn-d", "cid:%u : handle packet arrival from %s:%u v:%d...", m_id, inet_ntoa(route.remoteAddress), route.remotePort, message->getVersion());

		int trn_count = message->getTopFieldCount();

		for (int i = 0; i < trn_count; i++)
		{
			const Field* trn = message->getTopFieldAt(i);

			if (trn == nullptr)
			{
				LOG_ERROR("trn-d", "cid:%u : NULL transaction object received!");
			}

			Token trn_type = trn->getType();
			switch (trn_type)
			{
			case Token::Error:
				handle_Error(trn, route);
				break;
			case Token::Transaction:
				handle_Request(trn, route, message->getVersion());
				break;
			case Token::Reply:
			case Token::Pending:
				handle_Reply(trn, route);
				break;
			case Token::TransactionResponseAck:
				handle_Ack(trn, route);
				break;
			case Token::Segment:
				// TODO: Not Implemented!
				break;
			default:
				LOG_WARN("trn-d", "cid:%u : handle packet : unknown node %s ", m_id, Token_toStringLong(trn->getType()));
				break;
			}
		}

		LOG_TRANS("trn-d", "cid:%u : handle packet : done", m_id);
	}
	//--------------------------------------
	// 
	//--------------------------------------
	void TransactionDispatcher::handle_NetFailureEvent(int net_error, const Route& route)
	{
		// тоже самое что и дисконнект
		// убить все транзакции связанные с данным маршрутом. в общем случае всех

		LOG_TRANS("trn-d", "cid:%u : handle net failure for %s:%u", m_id, inet_ntoa(route.remoteAddress), route.remotePort);

		m_requestReceiver->IRequestReceiver_transportFailure(net_error, route);
	}
	//--------------------------------------
	// 
	//--------------------------------------
	void TransactionDispatcher::handle_ParserErrorEvent(const Route& route)
	{
		// тоже самое что и дисконнект
		// убить все транзакции связанные с данным маршрутом. в общем случае всех
		LOG_TRANS("trn-d", "cid:%u : handle parser error for %s:%u...", m_id, inet_ntoa(route.remoteAddress), route.remotePort);
	}
	//-------------------------------------
	//
	//-------------------------------------
	bool TransactionDispatcher::sendRequest(Field* request, IRequestSender* handler, TransactionID tid)
	{
		// каков порядок!
		// кто создает транзакции?
		// пользователи? нет
		// диспетчер ? да, но кто создает ноду транзакции?
		// ноду должны создавать пользователи без номера транзакции
		// а номер уже присваевает диспетчер!

		RequestSender record;

		record.tid = tid;
		record.sender = handler;

		request->setValue(record.tid._tid);

		LOG_TRANS("trn-d", "cid:%u : send request to %s:%u...", m_id, inet_ntoa(m_route.remoteAddress), m_route.remotePort);

		bool result = TransactionManager::startClientTransaction(this, record.tid, m_mgcVersion, request, m_route);

		LOG_TRANS("trn-d", "cid:%u : send request : %s", m_id, result ? "done" : "failed");

		if (result)
		{
			addSender(record);
		}

		return result;
	}
	//-------------------------------------
	//
	//-------------------------------------
	RTX_MG_API void TransactionDispatcher::sendReply(Field* response, TransactionID tid)
	{
		TransactionManager::sendReply(tid, response);
	}
	//-------------------------------------
	//
	//-------------------------------------
	RTX_MG_API void TransactionDispatcher::sendError(TransactionID tid, ErrorCode error_code, const char* reason)
	{
		TransactionManager::sendBackError(tid, error_code, reason);
	}
	//-------------------------------------
	//
	//-------------------------------------
	uint32_t TransactionDispatcher::generateTranId()
	{
		uint32_t id;

		while ((id = std_interlocked_inc(&m_tranIdGen)) == 0);

		return id;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransactionDispatcher::generateTranId(TransactionID& tid)
	{
		tid._cid = m_id; tid._tid = generateTranId();
	}
	//--------------------------------------
	// разбор входящего запроса
	//--------------------------------------
	void TransactionDispatcher::handle_Request(const Field* request, const Route& route, int version)
	{
		TransactionID trn_key;
		trn_key._cid = m_id;
		trn_key._tid = strtoul(request->getValue(), nullptr, 10);

		LOG_TRANS("trn-d", "cid:%u : handing request from %s:%u transaction-id %u", m_id, inet_ntoa(route.remoteAddress), route.remotePort, trn_key._tid);

		//Field reply(Token::Reply, request->getValue());

		TransactionManager::processServerTransaction(this, trn_key, version, request, route); // &reply, 
	}
	//--------------------------------------
	// разбор входящего ответа на запрос
	// об ошибках не сообщаем
	//--------------------------------------
	void TransactionDispatcher::handle_Reply(const Field* reply, const Route& route)
	{
		TransactionID trn_key;
		trn_key._cid = m_id;
		trn_key._tid = strtoul(reply->getValue(), nullptr, 10);

		LOG_TRANS("trn-d", "cid:%u : handing reply from %s:%u transaction-id %u", m_id, inet_ntoa(route.remoteAddress), route.remotePort, trn_key._tid);

		TransactionManager::processReply(trn_key, reply);
	}
	//--------------------------------------
	// разбор входящего подтверждения в получении ответа
	// об ошибках не сообщаем
	//--------------------------------------
	void TransactionDispatcher::handle_Ack(const Field* ack, const Route& route)
	{
		LOG_TRANS("trn-d", "cid:%u : handing ack from %s:%u", m_id, inet_ntoa(route.remoteAddress), route.remotePort);

		for (int i = 0; i < ack->getFieldCount(); i++)
		{
			const Field* trn_id = ack->getFieldAt(i);

			const char* trn_id_str = trn_id->getName();
			const char* trn_delim = strchr(trn_id_str, '-');

			if (trn_delim != nullptr)
			{
				uint32_t trn_begin = strtoul(trn_id_str, nullptr, 10);
				uint32_t trn_end = strtoul(trn_delim + 1, nullptr, 10);

				if (trn_end == 0 || trn_begin == 0 || trn_end < trn_begin)
				{
					// no error reporting
					continue;
				}

				TransactionID trn_key;
				trn_key._cid = m_id;

				for (uint32_t trn_it = trn_begin; trn_it <= trn_end; trn_it++)
				{
					trn_key._tid = trn_it;
					LOG_TRANS("trn-d", "cid:%u : handing ack tid %u", m_id, trn_key._tid);
					TransactionManager::processAck(trn_key);
				}
			}
			else
			{
				TransactionID trn_key;
				trn_key._cid = m_id;
				trn_key._tid = strtoul(trn_id_str, nullptr, 10);
				LOG_TRANS("trn-d", "cid:%u : handing ack tid %u", m_id, trn_key._tid);
				TransactionManager::processAck(trn_key);
			}
		}
	}
	//--------------------------------------
	// разбор входящего сообщения об ошибке
	//--------------------------------------
	void TransactionDispatcher::handle_Error(const Field* errorDescription, const Route& route)
	{
		// TODO: обработка ошибок
		// пока не понятно как реагировать

		LOG_TRANS("trn-d", "cid:%u : handle error received from %s:%u...", m_id, inet_ntoa(route.remoteAddress), route.remotePort);
	}
	//--------------------------------------
	// transaction_user_interface
	//--------------------------------------
	// client transaction events
	//--------------------------------------
	// получен ответ на запрос
	//--------------------------------------
	bool TransactionDispatcher::TransactionUser_responseReceived(Transaction* sender, const Field* response)
	{
		// ищем отправителя и передаем ответ ему
		RequestSender handler = { 0 };

		LOG_TRANS("trn-d", "cid:%u : event -> response received...", m_id);

		if (!findSender(sender->getTranId(), handler) || handler.sender == nullptr)
		{
			// broken transaction!
			LOG_TRANS("trn-d", "cid:%u : event -> response_received : failed : handler not found", m_id);
			//  ERR:		транзакция существует а обработчик нет
			//	решение:	транзакция должна быть помеченна как ошибочная и уничтожится		
			return false;
		}

		bool result = handler.sender->RequestSender_responseReceived(sender->getTranId(), response);

		releaseSender(sender->getTranId());

		LOG_TRANS("trn-d", "cid:%u : event -> response received : done", m_id);

		return result;
	}
	//--------------------------------------
	// получено сообщение о зарержке выполнения запроса
	//--------------------------------------
	bool TransactionDispatcher::TransactionUser_pendingReceived(Transaction* sender)
	{
		// ищем отправителя и передаем ответ ему
		RequestSender handler = { 0 };

		LOG_TRANS("trn-d", "cid:%u : event -> pending received...", m_id);

		if (!findSender(sender->getTranId(), handler) || handler.sender == nullptr)
		{
			// broken transaction!
			LOG_TRANS("trn-d", "cid:%u : event -> pending_received : failed : handler not found", m_id);
			//  ERR:		транзакция существует а обработчик нет
			//	решение:	транзакция должна быть помеченна как ошибочная и уничтожится
			return false;
		}

		handler.sender->RequestSender_responseReceived(sender->getTranId(), nullptr);

		LOG_TRANS("trn-d", "cid:%u : event -> pending received : done", m_id);

		return true;
	}
	//--------------------------------------
	// вышло время ожидания ответа на запрос 
	//--------------------------------------
	void TransactionDispatcher::TransactionUser_requestTimeout(Transaction* sender)
	{
		LOG_TRANS("trn-d", "cid:%u : event -> request timeout...", m_id);
		// обработчики теже
		// + сервис должен подключится и определить что же именно случилось
		RequestSender handler = { 0 };

		if (!findSender(sender->getTranId(), handler) || handler.sender == nullptr)
		{
			// broken transaction!
			//  ERR:		транзакция существует а обработчик нет
			//	решение:	транзакция должна быть помеченна как ошибочная и уничтожится
			LOG_TRANS("trn-d", "cid:%u : event -> request timeout : failed : handler not found", m_id);
			return;
		}

		handler.sender->RequestSender_responseTimeout(handler.tid);

		releaseSender(sender->getTranId());

		LOG_TRANS("trn-d", "cid:%u : event -> request timeout : done", m_id);
	}
	//--------------------------------------
	// common transaction events
	//--------------------------------------
	// полученно сообщение об ошибке (на запрос, на ответ)
	//--------------------------------------
	void TransactionDispatcher::TransactionUser_errorReceived(Transaction* sender, const Field* error)
	{
		// обработчики теже
		// + сервис должен подключится и определить что же именно случилось
		RequestSender handler = { 0 };

		LOG_TRANS("trn-d", "cid:%u : event -> error received...", m_id);

		if (!findSender(sender->getTranId(), handler) || handler.sender == nullptr)
		{
			// broken transaction!
			//  ERR:		транзакция существует а обработчик нет
			//	решение:	транзакция должна быть помеченна как ошибочная и уничтожится
			LOG_TRANS("trn-d", "cid:%u : event -> error received : failed : handler not found", m_id);
			return;
		}

		handler.sender->RequestSender_errorReceived(handler.tid, error);

		releaseSender(sender->getTranId());

		LOG_TRANS("trn-d", "cid:%u : event -> error received : done", m_id);
	}
	//--------------------------------------
	// транзакция прекратила существование нужно ее удалить
	//--------------------------------------
	void TransactionDispatcher::TransactionUser_terminated(Transaction* sender)
	{
		// тупо удаляем связку транзакции и пользователей если еще они остались
		LOG_TRANS("trn-d", "cid:%u : event -> transaction terminated...", m_id);
		releaseSender(sender->getTranId());
		LOG_TRANS("trn-d", "cid:%u : event -> transaction terminated : done", m_id);
	}
	//--------------------------------------
	// server transaction events
	//--------------------------------------
	// получен запрос
	//--------------------------------------
	void TransactionDispatcher::TransactionUser_requestReceived(Transaction* sender, const Field* request) // , Field* reply
	{
		LOG_TRANS("trn-d", "cid:%u : event -> request received...", m_id);

		m_requestReceiver->IRequestReceiver_requestReceived(sender->getTranId(), request); // , reply
	}
	//--------------------------------------
	// 
	//--------------------------------------
	void TransactionDispatcher::addSender(RequestSender& handler)
	{
		//rtl::MutexLock lock(m_tranSync);
		rtl::MutexWatchLock lock(m_tranSync, __FUNCTION__);

		m_requestList.add(handler);
	}
	//--------------------------------------
	// 
	//--------------------------------------
	bool TransactionDispatcher::findSender(TransactionID tid, RequestSender& handler)
	{
		//rtl::MutexLock lock(m_tranSync);
		rtl::MutexWatchLock lock(m_tranSync, __FUNCTION__);

		RequestSender* ptr = m_requestList.find(tid);

		if (ptr != nullptr)
		{
			handler = *ptr;
		}

		return ptr != nullptr;
	}
	//--------------------------------------
	// 
	//--------------------------------------
	void TransactionDispatcher::releaseSender(TransactionID tid)
	{
		//rtl::MutexLock lock(m_tranSync);
		rtl::MutexWatchLock lock(m_tranSync, __FUNCTION__);

		int index;
		RequestSender* ptr = m_requestList.find(tid, index);

		if (ptr != nullptr)
		{
			m_requestList.removeAt(index);
		}
	}
	//--------------------------------------
	// 
	//--------------------------------------
	int TransactionDispatcher::compareRequestByObj(const RequestSender& left, const RequestSender& right)
	{
		return left.tid.id < right.tid.id ? -1 : left.tid.id > right.tid.id ? 1 : 0;
	}
	//--------------------------------------
	// 
	//--------------------------------------
	int TransactionDispatcher::compareRequestByKey(const TransactionID& left, const RequestSender& right)
	{
		return left.id < right.tid.id ? -1 : left.id > right.tid.id ? 1 : 0;
	}
}
//-------------------------------------
