﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_transport_reader.h"

namespace h248
{
	//--------------------------------------
	//
	//--------------------------------------
#define MAKE_KEY64(a, p, t) uint64_t((uint64_t(a.s_addr) << 32) + (uint64_t(p) << 16) + (int)t)
	//--------------------------------------
	// соединение к удаленному серверу
	//--------------------------------------
	class Transport : public TransportObject
	{
	public:
		enum State
		{
			New,
			Connecting,
			Connected,
			Disconnecting,
			Disconnected,
			Invalid = -1,
		};

	protected:
		Route m_route;
		uint64_t m_key;
		State m_state;
		uint32_t m_deadTime;

	public:
		Transport();
		virtual ~Transport();

		// инициализация транспорта (или или)
		virtual bool connect(const EndPoint& remoteAddress, int timeout) = 0;
		virtual bool start() = 0;

		// отключение
		virtual void disconnect() = 0;

		// отправка сообщений
		virtual int sendMessage(const void* message, int length) = 0;

		State getState() { return m_state; }
		const Route& getRoute() { return m_route; }
		TransportType getType() { return m_route.type; }
		in_addr getInterfaceAddress() { return m_route.localAddress; }
		uint16_t getInterfacePort() { return m_route.localPort; }
		in_addr getRemoteAddress() { return m_route.remoteAddress; }
		uint16_t getRemotePort() { return m_route.remotePort; }
		uint64_t getKey() { return m_key != 0 ? m_key : m_key = MAKE_KEY64(m_route.remoteAddress, m_route.remotePort, m_route.type); }
		uint32_t getDeadTime() { return m_deadTime; }
		void setDeadTime() { m_deadTime = rtl::DateTime::getTicks(); }

		bool isReliable() { return m_route.type >= h248::TransportType::TCP; }

		// internal interface
		virtual void destroy();

	protected:
		void raiseTransportFailure(int netError, const char* function);
		void raiseTransportDisconnected(const char* function);
		void raiseParserError(const char* function);
		void raisePacketArrival(const uint8_t* packet, int length);

		static const char* State_toString(State state);
	};
	//--------------------------------------
	// соединение к удаленному серверу TCP
	//--------------------------------------
	class TransportTCP : public Transport
	{
		net_socket_t m_socket;
		rtl::MutexWatch m_sync;
		uint8_t* m_rxBuffer; // default = 4 Kb then resizing for bigger data
		int m_rxBufferSize;
		int m_rxBufferWritten;
		bool m_rxReadNewPacket;	

	public:
		TransportTCP();
		virtual ~TransportTCP();

		virtual bool connect(const EndPoint& remote_address, int timeout);
		virtual bool start();
		virtual void disconnect();
		virtual int sendMessage(const void* message, int length);

		// internal interface
		bool accept(SOCKET socket, const Route& route);

		// megaco_transport interface
		virtual void destroy();

	private:
		// TransportObject interface
		virtual void dataReady();

		bool prepareBufferRX(int length);
		bool prepareMegacoPacket(const uint8_t* packet, int length);
		void readPackets();
	};
	//--------------------------------------
	// соединение к удаленному серверу UDP
	//--------------------------------------
	class TransportUDP : public Transport
	{
		net_socket_t* m_socket_ptr;

	public:
		TransportUDP();
		virtual ~TransportUDP();

		virtual bool connect(const EndPoint& remote_address, int timeout);
		virtual bool start();
		virtual void disconnect();
		virtual int sendMessage(const void* message, int length);

		// megaco_transport interface
		virtual void destroy();

		bool accept(net_socket_t* socket, const Route& route);
		void rxPacket(const uint8_t* packet, int packet_length, const EndPoint& remoteAddress);
		void rxError(int net_error, const EndPoint& remoteAddress);
	};
}
//--------------------------------------
