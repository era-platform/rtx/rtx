﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_transport_reader.h"

namespace h248
{
	//--------------------------------------
	//
	//--------------------------------------
	volatile uint32_t TransportObject::m_idGen = 0;
	//--------------------------------------
	//
	//--------------------------------------
	void TransportObject::generateId(char* buffer, const char* prefix)
	{
		uint32_t id = std_interlocked_inc(&m_idGen);
		int len = std_snprintf(buffer, 15, "%s(%u)", prefix, id);
		buffer[len] = 0;
	}
	//--------------------------------------
	//
	//--------------------------------------
	TransportObject::TransportObject() :
		m_nextLink(nullptr),
		m_reader(nullptr),
		m_readable(false),
		m_socketRef(nullptr)
	{
		memset(m_id, 0, sizeof m_id);
	}
	//--------------------------------------
	//
	//--------------------------------------
	TransportObject::~TransportObject()
	{
		LOG_NET("net-obj", "%s : --------> deleting object...", m_id);

		// !!! останов и закрытие сокетов прероготива наследников !!!
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransportObject::startReading(TransportReader* reader)
	{
		m_reader = reader;
		m_readable = m_reader != nullptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransportObject::stopReading()
	{
		if (m_reader != nullptr)
		{
			m_readable = false;
			m_reader->removeObject(this);
			m_reader = nullptr;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransportObject::dataReady()
	{
		// implemetations
	}
	//--------------------------------------
	//
	//--------------------------------------
	volatile uint32_t TransportReader::m_idGen = 0;
	//--------------------------------------
	//
	//--------------------------------------
	TransportReader::TransportReader() : m_sync("net-rd")
	{
		m_first = m_last = nullptr;
		m_count = 0;
		m_threadEnter = false;

		m_id = std_interlocked_inc(&m_idGen);
	}
	//--------------------------------------
	//
	//--------------------------------------
	TransportReader::~TransportReader()
	{
		if (m_thread.isStarted())
		{
			stop();
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool TransportReader::start()
	{
		LOG_NET("net-rd", "%d : starting reader thread...", m_id);
		// уже стартовал? выходим с истиной
		if (m_thread.isStarted())
			return true;

		if (!m_threadWakeUpEvent.create(true, false))
		{
			LOG_NET_ERROR("net-rd", "%d : Start failed -- create event return %d", m_id, std_get_error);
			return false;
		}

		if (!m_threadStartedEvent.create(true, false))
		{
			LOG_NET_ERROR("net-rd", "%d : Start failed -- create start event return %d", m_id, std_get_error);

			m_threadWakeUpEvent.close();

			return false;
		}

		//	m_thread.set_name("net-listen");

		if (!m_thread.start(this))
		{
			LOG_NET_ERROR("net-rd", "%d : Start failed -- create thread return %d", m_id, std_get_error);

			m_threadWakeUpEvent.close();
			m_threadStartedEvent.close();

			return false;
		}

		m_threadStartedEvent.wait(INFINITE);
		m_threadStartedEvent.close();

		LOG_NET("net-rd", "%d : reader thread started", m_id);

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransportReader::stop()
	{
		LOG_NET("net-rd", "%d : stopping reader (%d)...", m_id, m_thread.get_id());

		m_thread.stop();
		m_threadWakeUpEvent.signal();
		m_thread.wait();

		//rtl::MutexLock lock(m_sync);
		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		LOG_NET("net-rd", "%d : stopping reader's objects...", m_id);

		TransportObject* current = (TransportObject*)m_first;

		while (current != nullptr)
		{
			current->m_readable = false;
			current->m_reader = nullptr;
			current = current->m_nextLink;
		}

		LOG_NET("net-rd", "%d : cleaning reader...", m_id);

		m_first = m_last = nullptr;
		m_count = 0;

		LOG_NET("net-rd", "%d : m_threadWakeUpEvent.close()", m_id);

		m_threadWakeUpEvent.close();

		LOG_NET("net-rd", "%d : m_threadStartedEvent.close()", m_id);

		m_threadStartedEvent.close();

		LOG_NET("net-rd", "%d : cleanup m_readList", m_id);

		m_readList.clear();

		LOG_NET("net-rd", "%d : reader stopped", m_id);
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool TransportReader::addObject(TransportObject* object)
	{
		LOG_NET("net-rd", "%d : add object %s...", m_id, object->getId());

		if (m_count >= 64)
		{
			LOG_NET("net-rd", "%d : adding object %s failed : object count %d >= 64", m_id, object->getId(), m_count);
			return false;
		}

		//rtl::MutexLock lock(m_sync);
		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		object->startReading(this);

		if (m_last == nullptr)
		{
			m_first = m_last = object;

			if (!start())
			{
				LOG_NET("net-rd", "%d : adding object %s failed : start() return false", m_id, object->getId());
				return false;
			}
		}
		else
		{
			m_last->m_nextLink = object;
			m_last = object;
		}

		m_count++;

		m_threadWakeUpEvent.signal();

		LOG_NET("net-rd", "%d : object %s added", m_id, object->getId());

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransportReader::removeObject(TransportObject* object)
	{
		LOG_NET("net-rd", "%d : remove object %s...", m_id, object->getId());

		//rtl::MutexLock lock(m_sync);
		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		volatile TransportObject* current = m_first;
		volatile TransportObject* prev = nullptr;

		object->m_readable = false;

		// ищем линк
		while (current != nullptr && current != object)
		{
			prev = current;
			current = current->m_nextLink;
		}

		if (current == nullptr)
		{
			LOG_NET("net-rd", "%d : object %s not found", m_id, object->getId());
			return;
		}

		// если нашли то удаляем из списка
		if (prev != nullptr)
		{
			prev->m_nextLink = current->m_nextLink;

			// если был последним то обновим ссылку на последнего
			if (current == m_last)
				m_last = prev;
		}
		else
		{
			m_first = current->m_nextLink;

			// если единственный то ссылка на последнего тоже обнуляется
			if (m_first == nullptr)
			{
				m_last = nullptr;
			}
		}

		object->m_reader = nullptr;
		object->m_nextLink = nullptr;

		m_count--;

		// ждем окончания работы таймера

		uint32_t start_ticks = rtl::DateTime::getTicks();

		// ждем 1 секунду после чего ругаемся
		while (m_threadEnter)
		{
			rtl::Thread::sleep(5);

			uint32_t delay = rtl::DateTime::getTicks() - start_ticks;

			if (delay > 1000)
			{
				LOG_ERROR("net-rd", "%d : object %s removed delay %u", m_id, object->getId(), delay);
				break;
			}
		}

		LOG_NET("net-rd", "%d : object %s removed", m_id, object->getId());
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool TransportReader::prepareSelection()
	{
		m_threadEnter = true;

		LOG_FLAG5("net-rd", "preparing selection...");

		m_readList.clear();

		// получим порты для слушания!
		TransportObject* current = (TransportObject*)m_first;

		while (current != nullptr)
		{
			LOG_FLAG5("net-rd", "preparing selection : current %s readable %s", current->getId(), STR_BOOL(current->m_readable));
			if (current->m_readable)
			{
				net_socket_t* sock = current->m_socketRef;
				m_readList.add(sock);
			}
			current = current->m_nextLink;
		}

		m_threadEnter = false;

		LOG_FLAG5("net-rd", "preparing selection : count %d", m_readList.getCount());

		return m_readList.getCount() > 0;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransportReader::processReadSockets()
	{
		TransportObject* current = nullptr;
		net_socket_t* sock = nullptr;

		// бежим по списку готовых и вызываем
		LOG_FLAG5("net-rd", "process read sockets : count %d", m_readList.getCount());
		for (int i = 0; i < m_readList.getCount(); i++)
		{
			sock = m_readList[i];

			if (sock != nullptr)
			{
				current = (TransportObject*)sock->get_tag();

				// проверяем готовность читать
				if (current != nullptr)
				{
					LOG_FLAG5("net-rd", "process read sockets : current %s readfable %s", current->getId(), STR_BOOL(current->m_readable));

					if (current->m_socketRef == sock && current->m_readable)
					{
						current->dataReady();
					}
				}
			}
		}

		m_readList.clear();
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransportReader::thread_run(rtl::Thread* thread)
	{
		int result = 0;

		m_threadStartedEvent.signal();

		LOG_NET("net-rd", "%d : reader thread run", m_id);

		do
		{
			m_threadWakeUpEvent.wait(INFINITE);
			m_threadWakeUpEvent.reset();

			while (!m_thread.get_stopping_flag() && prepareSelection())
			{
				// соберем сокеты для слушания
				LOG_FLAG5("net-rd", "%d : waiting select with %d sockets", m_id, m_readList.getCount());
				result = net_socket_t::select(m_readList, 250);
				LOG_FLAG5("net-rd", "%d : select result %d", m_id, result);

				if (result > 0)
				{
					m_threadEnter = true;

					if (result > 0)
					{
						processReadSockets();
					}

					m_threadEnter = false;
				}
				else if (result < 0)
				{
					int last_error = std_get_error;
					LOG_NET_ERROR("net-rd", "reader's select() function failed(%d)", m_id, last_error);
				}
			}
		} while (!m_thread.get_stopping_flag());

		LOG_NET("net-rd", "%d : reader thread exit", m_id);
	}
}
//--------------------------------------
