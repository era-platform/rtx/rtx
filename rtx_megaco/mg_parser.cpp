﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_parser.h"

namespace h248
{
	//--------------------------------------
	//
	//--------------------------------------
	Parser::Parser() :
		m_packet(nullptr), m_readPointer(nullptr),
		m_state(ParserState::EndOfFile),
		m_tokenNameSize(0), m_tokenName(nullptr),
		m_tokenRelation(Relation::Assignment),
		m_tokenValueSize(0), m_tokenValue(nullptr),
		m_hasGroup(false),
		m_groupLevelCounter(0)
	{
	}
	//--------------------------------------
	//
	//--------------------------------------
	Parser::~Parser()
	{
		close();
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Parser::startReading(const char* packet)
	{
		close();

		m_packet = packet;
		m_readPointer = packet;
		m_state = ParserState::Begin;

		// небольшое ускорение!
		m_tokenName = (char*)MALLOC(m_tokenNameSize = 64);
		memset(m_tokenName, 0, m_tokenNameSize);
		m_tokenValue = (char*)MALLOC(m_tokenValueSize = 64);
		memset(m_tokenValue, 0, m_tokenValueSize);
		m_hasGroup = false;
		m_groupLevelCounter = 0;

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Parser::close()
	{
		m_packet = nullptr;
		m_readPointer = nullptr;
		m_state = ParserState::EndOfFile;

		if (m_tokenName != nullptr)
		{
			FREE(m_tokenName);
			m_tokenName = nullptr;
		}

		if (m_tokenValue != nullptr)
		{
			FREE(m_tokenValue);
			m_tokenValue = nullptr;
		}

		m_hasGroup = false;
		m_groupLevelCounter = 0;
	}
	//--------------------------------------
	// AH & start line must parsed first!
	//--------------------------------------
	ParserState Parser::readNext()
	{
		if (m_state == ParserState::Error || m_state == ParserState::EndOfFile)
		{
			return m_state;
		}

		if (m_packet == nullptr)
		{
			return ParserState::EndOfFile;
		}

		const char* current = rtl::skip_LWSP(m_readPointer);

		if (*current == 0)
		{
			return m_state = ParserState::EndOfFile;
		}

		// очистим параметры предыдущего токена
		setTokenName(nullptr, 0);
		m_tokenRelation = Relation::Assignment;
		setTokenValue(nullptr, 0);

		// проверка на заголовок аутенитификации
		if (m_state == ParserState::Begin)
		{
			if (readAuth(current) == ParserState::False)
			{
				return readStartLine(current);
			}
		}
		else if (m_state == ParserState::Auth)
		{
			return readStartLine(current);
		}

		// смотрим на '}' (end group)
		if (*current == '}')
		{
			m_readPointer = current + 1;
			return m_state = ParserState::EndGroup;
		}
		if (*current == ',')
		{
			current = rtl::skip_SEP(current + 1);
			//return m_state = ParserState::EndGroup;
		}

		// читаем токен!
		return readToken(current);
	}
	//--------------------------------------
	//
	//--------------------------------------
	ParserState Parser::readTokenGroup()
	{
		if (m_state != ParserState::Token)
		{
			return ParserState::False;
		}

		m_readPointer = rtl::skip_SEP(m_readPointer);

		// если нет группы то возвращаем фальш
		if (!m_hasGroup)
		{
			return ParserState::False;
		}

		// ищем конец группы и возвращаем все как есть!

		const char* delim = strchr(m_readPointer, '}');

		while (delim != nullptr && *(delim - 1) == '\\')
		{
			delim = strchr(delim + 1, '}');
		}

		if (delim == nullptr)
		{
			close();
			return m_state = ParserState::Error;
		}

		setTokenName(nullptr, 0);
		m_tokenRelation = Relation::Assignment;
		setTokenValue(m_readPointer, PTR_DIFF(delim, m_readPointer));

		m_readPointer = delim + 1;

		//m_groupLevelCounter--;
		return m_state = ParserState::EndGroup;
	}
	//--------------------------------------
	//
	//--------------------------------------
	ParserState Parser::readAuth(const char* stream)
	{
		if (std_strnicmp(stream, "Authentication", 14) == 0)
		{
			setTokenName("Authentication", 14);

			//AuthToken EQUAL SecurityParmIndex COLON SequenceNum COLON AuthData SEP message
			const char* eq = rtl::skip_SEP(stream + 14);

			if (*eq != '=')
			{
				close();
				return m_state = ParserState::Error;
			}

			const char* values = rtl::skip_SEP(eq + 1);
			const char* end = rtl::next_SEP(values);
			setTokenValue(values, PTR_DIFF(end, values));
			m_readPointer = end;

			return m_state = ParserState::Auth;
		}

		return ParserState::False;
	}
	//--------------------------------------
	//
	//--------------------------------------
	ParserState Parser::readStartLine(const char* stream)
	{
		// проверка на стартовую линию
		if (std_strnicmp(stream, "MEGACO", 6) == 0 || *stream == '!')
		{
			const char* sep = rtl::next_SEP(stream);
			if (sep == nullptr)
			{
				close();
				return m_state = ParserState::Error;
			}

			setTokenName(stream, PTR_DIFF(sep, stream));

			//MegacopToken SLASH Version SEP mId SEP messageBody
			const char* mid = rtl::skip_SEP(sep);
			const char* end_mid = rtl::next_SEP(mid);
			if (end_mid == nullptr)
			{
				close();
				return m_state = ParserState::Error;
			}

			setTokenValue(mid, PTR_DIFF(end_mid, mid));
			m_readPointer = end_mid;

			return m_state = ParserState::Startline;
		}

		return ParserState::False;
	}
	//--------------------------------------
	//
	//--------------------------------------
	ParserState Parser::readToken(const char* stream)
	{
		// разбор токенов
		/*
		token
		token = value
		token = "value"
		token = [v1, v2, v3]
		token # value
		token > value
		token < value
		*/

		const char* delim = readTokenName(stream);

		// если нет то ошибка! 
		if (delim == nullptr)
		{
			close();
			return m_state = ParserState::Error;
		}

		// если есть значение
		if (*delim == '=')
		{
			// обработка значений!
			const char* value = rtl::skip_SEP(delim + 1);
			if (*value == '"')
			{
				delim = readTokenValue(value, '"');
			}
			else if (*value == '[')
			{
				delim = readTokenValue(value, ']');
			}
			else // all others
			{
				delim = readTokenValue(value);
			}

			if (delim == nullptr)
			{
				close();
				return m_state = ParserState::Error;
			}

			m_hasGroup = *delim == '{';

			if (*delim == '{' || *delim == ',')
			{
				delim++;
			}
		}
		else if (*delim == '#' || *delim == '<' || *delim == '>')
		{
			m_tokenRelation = (Relation)*delim;

			const char* value = rtl::skip_SEP(delim + 1);
			delim = readTokenValue(value);

			if (delim == nullptr)
			{
				close();
				return m_state = ParserState::Error;
			}

			m_hasGroup = *delim == '{';

			if (*delim == '{' || *delim == ',')
			{
				delim++;
			}
		}
		else
		{
			m_hasGroup = *delim == '{';

			if (*delim == '{' || *delim == ',')
			{
				delim++;
			}
		}

		m_readPointer = delim;

		return m_state = ParserState::Token;
	}

	//--------------------------------------
	//
	//--------------------------------------
	const char* Parser::readTokenValue(const char* stream, char end_sign)
	{
		const char* end = strchr(stream + 1, end_sign);

		if (end == nullptr)
			return nullptr;

		//const char* sep = rtl::next_SEP(end);

		int to_copy = PTR_DIFF(end, stream) + 1;
		setTokenValue(stream, to_copy);

		return end + 1;
	}
	//--------------------------------------
	//
	//--------------------------------------
	const char* Parser::readTokenValue(const char* stream)
	{
		const char* end = rtl::next_SEP(stream);
		const char* delim = strpbrk(stream, "{},");

		if (end == nullptr || delim == nullptr)
			return nullptr;

		if (*stream == '(')
		{
			const char* rbrkt = strchr(stream, ')');

			if (rbrkt == nullptr)
				return nullptr;

			int to_copy = PTR_DIFF(rbrkt, stream);
			setTokenValue(stream, to_copy);
			delim = rbrkt + 1;
		}
		else
		{
			int to_copy = end < delim ? PTR_DIFF(end, stream) : PTR_DIFF(delim, stream);
			setTokenValue(stream, to_copy);
		}

		return delim;// end < delim ? end : delim;
	}
	//--------------------------------------
	//
	//--------------------------------------
	const char* Parser::readTokenName(const char* stream)
	{
		const char* delim = strpbrk(stream, "{},=#<>");
		// если нет то ошибка! 
		if (delim == nullptr)
		{
			return nullptr;
		}

		// возможны скобки!
		if (*stream == '(')
		{
			const char* rbrkt = strchr(stream, ')');
			if (rbrkt == nullptr)
				return nullptr;

			int to_copy = PTR_DIFF(rbrkt, stream) + 1;
			setTokenName(stream, to_copy);
			delim = rbrkt + 1;
		}
		else
		{
			const char* sep = rtl::next_SEP(stream);
			// без значения
			// name{
			// name {
			int to_copy = sep < delim ? PTR_DIFF(sep, stream) : PTR_DIFF(delim, stream);

			setTokenName(stream, to_copy);
		}

		return delim;
	}
	//--------------------------------------
	// length must be equal or greater than zero
	//--------------------------------------
	void Parser::setTokenName(const char* name, int length)
	{
		if (length < 0)
			length = 0;

		if (length >= m_tokenNameSize)
		{
			if (m_tokenName != nullptr)
			{
				FREE(m_tokenName);
				m_tokenName = nullptr;
			}

			if (length > 0)
			{
				m_tokenName = (char*)MALLOC(m_tokenNameSize = length + 1);
			}
		}

		if (length > 0)
		{
			strncpy(m_tokenName, name, length);
			m_tokenName[length] = 0;
		}
		else if (m_tokenName != nullptr)
		{
			m_tokenName[0] = 0;
		}
	}
	//--------------------------------------
	// remove leading and trailing whitespaces in square group like [    a,     b,    f   ] 
	//--------------------------------------
	static void pack_value(char* value, int length)
	{
		if (*value != '[' || value[length - 1] != ']')
			return;

		char* ptr = value + 1;

		while (*ptr == ' ' || *ptr == '\t' || *ptr == '\r' || *ptr == '\n') ptr++;

		// ptr points to non WS symbol
		if (ptr > value)
		{
			*(ptr - 1) = '[';
			*value = ' ';
		}

		char* end = value + (length - 1);
		ptr = end - 1;

		while (*ptr == ' ' || *ptr == '\t' || *ptr == '\r' || *ptr == '\n') ptr--;

		if (ptr < end)
		{
			*end = ' ';
			*(ptr + 1) = ']';
		}

		rtl::strtrim(value, " \t\n\r");
	}
	//--------------------------------------
	// length must be equal or greater than zero
	//--------------------------------------
	void Parser::setTokenValue(const char* value, int length)
	{
		if (length > 1024 * 1024 * 8)
		{
			LOG_ERROR("PARSER", "PARSER ERROR size for malloc %d", length);
		}

		if (length < 0)
			length = 0;

		if (length >= m_tokenValueSize)
		{
			if (m_tokenValue != nullptr)
			{
				FREE(m_tokenValue);
				m_tokenValue = nullptr;
			}

			if (length > 0)
			{
				m_tokenValueSize = length + 2;
				m_tokenValue = (char*)MALLOC(m_tokenValueSize);
			}
		}

		if (length > 0)
		{
			strncpy(m_tokenValue, value, length);
			m_tokenValue[length] = 0;

			pack_value(m_tokenValue, length);
		}
		else if (m_tokenValue != nullptr)
		{
			m_tokenValue[0] = 0;
		}
	}
}
//--------------------------------------
