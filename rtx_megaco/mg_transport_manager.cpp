﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_transport_manager.h"
#include "mg_transport_listener.h"
#include "mg_transport.h"

namespace h248
{
//--------------------------------------
//
//--------------------------------------
rtl::MutexWatch TransportManager::m_listenerSync("mg-listener");
rtl::ArrayT<TransportListener*> TransportManager::m_listenerList;

rtl::MutexWatch TransportManager::m_transportSync("mg-transport");
rtl::SortedArrayT<TransportManager::TransportPtr, uint64_t> TransportManager::m_transportList(compareTransportObj, compareTransportKey);
rtl::SortedArrayT<TransportManager::TransportPtr, uint64_t> TransportManager::m_connectingList(compareTransportObj, compareTransportKey);

rtl::ArrayT<Transport*> TransportManager::m_purgatory;
rtl::MutexWatch TransportManager::m_purgatorySync("mg-transport-purgatory");

rtl::MutexWatch TransportManager::m_readerSync("mg-reader");
rtl::ArrayT<TransportReader*> TransportManager::m_readerList;

ITransportUser* TransportManager::m_newRouteHandler = nullptr;

static in_addr s_defaultInterface = { 0 };
rtl::ArrayT<in_addr> s_interfaceTable;

#define DEADTIME 30000
#define PURGATORY_TIMER 1001
//--------------------------------------
// подъем сети
//--------------------------------------
bool TransportManager::create(ITransportUser* eventHandler)
{
	if (eventHandler == nullptr)
	{
		LOG_NET_ERROR("net-man", "starting : failed : bad parameter : null event handler");
		return false;
	}

	m_newRouteHandler = eventHandler;

	LOG_NET("net-man", "starting megaco transport...");

#if defined (TARGET_OS_WINDOWS)
	WSAData d;
	int net_result = WSAStartup(MAKEWORD(2, 0), &d);

	if (net_result != 0)
	{
		LOG_NET_ERROR("net-man", "starting : failed : WSAStartup() failed %d", std_get_error);
		return false;
	}
#endif

	MegacoAsyncCaller.create(2, 4);
	MegacoAsyncCaller.start(rtl::ThreadPriority::Highest);

	s_interfaceTable.clear();

	addrinfo hints = { 0 };
	addrinfo* if_table;
	addrinfo* result = nullptr;

	hints.ai_family = AF_INET;
	hints.ai_flags = AI_PASSIVE;
	//hints.ai_flags = AI_NUMERICHOST; // | AI_PASSIVE

	const char* node = nullptr;

#if defined (TARGET_OS_WINDOWS)
	node = "..localmachine";
#endif

	int res = getaddrinfo(node, nullptr, &hints, &if_table); // "..localmachine"

	if (res == 0 && if_table != nullptr)
	{
		result = if_table;

		while (result != nullptr)
		{
			sockaddr_in* saddr = (sockaddr_in*)result->ai_addr;

			if (saddr != nullptr)
			{
				s_interfaceTable.add(saddr->sin_addr);
			}

			result = result->ai_next;
		}

		freeaddrinfo(if_table);
	}
	else if (res != 0)
	{
		LOG_NET_ERROR("net-man", "starting : failed : getaddrinfo('%s') returns %d", node, std_get_error);
	}

	s_defaultInterface.s_addr = INADDR_NONE;

	const char* def_iface_str = Config.get_config_value(CONF_MEGACO_DEF_TEXT_INTERFACE);

	if (def_iface_str != nullptr && def_iface_str[0] != 0)
	{
		s_defaultInterface.s_addr = inet_addr(def_iface_str);
	}

	if (s_defaultInterface.s_addr == INADDR_NONE)
	{
		s_defaultInterface.s_addr = s_interfaceTable.getCount() > 0 ? s_interfaceTable[0].s_addr : htonl(INADDR_LOOPBACK);
	}

	LOG_NET("net-man", "starting : done");

	rtl::Timer::setTimer(timerEventCallback, nullptr, 0, 1000, true);

	return true;
}
//--------------------------------------
// разрушение всех соединений и останов всех листенеров
//--------------------------------------
void TransportManager::stop()
{
	LOG_NET("net-man", "stopping megaco transport...");

	// disconnect all transports
	{
		LOG_NET("net-man", "stopping : destroying transports...");

		//rtl::MutexLock lock(m_transportSync);
		rtl::MutexWatchLock lock(m_transportSync, __FUNCTION__);

		for (int i = 0; i < m_transportList.getCount(); i++)
		{
			Transport* transport = m_transportList[i];

			transport->disconnect();
		}
	}

	// stop all listeners
	{
		LOG_NET("net-man", "stoping : destroying listeners...");

		//rtl::MutexLock lock(m_listenerSync);
		rtl::MutexWatchLock lock(m_listenerSync, __FUNCTION__);

		for (int i = 0; i < m_listenerList.getCount(); i++)
		{
			TransportListener* listener = m_listenerList[i];

			listener->stopListen();
		}
	}

	// stop all reading
	{
		LOG_NET("net-man", "stopping : destroy transport readers...");
		//rtl::MutexLock lock(m_readerSync);
		rtl::MutexWatchLock lock(m_readerSync, __FUNCTION__);

		for (int i = 0; i < m_readerList.getCount(); i++)
		{
			TransportReader* reader = m_readerList[i];
			reader->stop();
		}
	}

	rtl::Timer::stopTimer(timerEventCallback, nullptr, 0);

	for (int i = m_purgatory.getCount() - 1; i >= 0; i--)
	{
		Transport* transport = m_purgatory.getAt(i);
		if (transport == nullptr)
		{
			continue;
		}

		transport->destroy();
		DELETEO(transport);
	}

	m_purgatory.clear();

	MegacoAsyncCaller.destroy();

	LOG_NET("net-man", "stopping : done");
}
//--------------------------------------
// разрушение всех соединений и останов всех листенеров
//--------------------------------------
void TransportManager::destroy()
{
	LOG_NET("net-man", "destroying megaco transport...");

	// remove all transports
	{
		LOG_NET("net-man", "destroying transports...");

		//rtl::MutexLock lock(m_transportSync);
		rtl::MutexWatchLock lock(m_transportSync, __FUNCTION__);

		for (int i = 0; i < m_transportList.getCount(); i++)
		{
			Transport* transport = m_transportList[i];

			transport->destroy();
			DELETEO(transport);
		}

		m_transportList.clear();
	}

	// remove all listeners
	{
		LOG_NET("net-man", "destroying listeners...");

		//rtl::MutexLock lock(m_listenerSync);
		rtl::MutexWatchLock lock(m_listenerSync, __FUNCTION__);

		for (int i = 0; i < m_listenerList.getCount(); i++)
		{
			TransportListener* listener = m_listenerList[i];

			listener->destroy();
			DELETEO(listener);
		}

		m_listenerList.clear();
	}

	// destroy readers
	{
		LOG_NET("net-man", "destroy transport readers...");
		//rtl::MutexLock lock(m_readerSync);
		rtl::MutexWatchLock lock(m_readerSync, __FUNCTION__);

		for (int i = 0; i < m_readerList.getCount(); i++)
		{
			TransportReader* reader = m_readerList[i];
			DELETEO(reader);
		}

		m_readerList.clear();
	}

	rtl::Timer::stopTimer(timerEventCallback, nullptr, 0);

	for (int i = m_purgatory.getCount() - 1; i >= 0; i--)
	{
		Transport* transport = m_purgatory.getAt(i);
		if (transport == nullptr)
		{
			continue;
		}

		transport->destroy();
		DELETEO(transport);
	}

	m_purgatory.clear();

	s_interfaceTable.clear();

	LOG_NET("net-man", "destroy : done");
}
//--------------------------------------
// подключение слушателя новых соединений TCP или пакетов UDP
//--------------------------------------
bool TransportManager::addListener(const EndPoint& listen_point)
{
	LOG_NET("net-man", "add listener at %s %s:%u", TransportType_toString(listen_point.type), inet_ntoa(listen_point.address), listen_point.port);
	
	return getListener(listen_point) != nullptr;
}
//--------------------------------------
//
//--------------------------------------
bool TransportManager::removeListener(const EndPoint& listen_point)
{
	LOG_NET("net-man", "remove listener at %s %s:%u", TransportType_toString(listen_point.type), inet_ntoa(listen_point.address), listen_point.port);

	int index = -1;
	TransportListener* listener = findListener(listen_point, &index);

	if (listener == nullptr)
		return false;

	{
		//rtl::MutexLock lock(m_listenerSync);
		rtl::MutexWatchLock lock(m_listenerSync, __FUNCTION__);

		m_listenerList.removeAt(index);
	}

	listener->stopListen();
	listener->destroy();
	DELETEO(listener);

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool TransportManager::isRouteToHimself (const Route& route)
{
	rtl::MutexWatchLock lock(m_listenerSync, __FUNCTION__);

	for (int i = 0; i < m_listenerList.getCount(); i++)
	{
		TransportListener* listener = m_listenerList[i];

		if ((listener->getInterfaceAddress().s_addr == route.remoteAddress.s_addr || listener->getInterfaceAddress().s_addr == INADDR_ANY) &&
			listener->getInterfacePort() == route.remotePort)
			return true;
	}

	return false;
}
//--------------------------------------
//
//--------------------------------------
bool TransportManager::createRoute(Route& route, int timeout)
{
	LOG_NET("net-man", "create route to %s %s:%u", TransportType_toString(route.type), inet_ntoa(route.remoteAddress), route.remotePort);

	// проверим попытку подключится к себе!
	if (isRouteToHimself(route))
	{
		LOG_NET_WARN("net-man", "create route failed: this route to himself!");
		return false;
	}

	Transport* transport = nullptr; // findTransport(route);

	if (route.type == h248::TransportType::TCP)
	{
		// we do not need listener. we will connect by own socket
		transport = NEW TransportTCP();
	}
	else if (route.type == h248::TransportType::UDP)
	{
		// we do not need listener. we will connect by own socket
		transport = NEW TransportUDP();
	}

	pushConnecting(transport);

	EndPoint remote_point;
	remote_point.type = route.type;
	remote_point.address = route.remoteAddress;
	remote_point.port = route.remotePort;
		
	if (transport->connect(remote_point, timeout))
	{
		if (moveConnected(transport))
		{
			transport->start();

			route.localAddress = transport->getInterfaceAddress();
			route.localPort = transport->getInterfacePort();

			LOG_NET("net-man", "create route : over interface %s %s:%u", TransportType_toString(route.type), inet_ntoa(route.localAddress), route.localPort);
		}
		else
		{
			LOG_NET("net-man", "create route : connection canceled!");
			transport = nullptr;
		}
	}
	else
	{
		dropConnecting(transport);
		transport->destroy();
		DELETEO(transport);
		transport = nullptr;

		LOG_NET("net-man", "create route : connection failed or canceled");

	}

	return transport != nullptr;
}
//--------------------------------------
//
//--------------------------------------
bool TransportManager::removeRoute(const Route& route, bool force)
{
	LOG_NET("net-man", "remove route to %s %s:%u", TransportType_toString(route.type), inet_ntoa(route.remoteAddress), route.remotePort);

	Transport* transport = findTransport(route);
	
	if (transport != nullptr)
	{
		transport->disconnect();

		if (force)
		{
			transport->destroy();
			removeTransport(transport);
		}
	}
	else if ((transport = popConnecting(route)) != nullptr)
	{
		transport->destroy();
		//DELETEO(transport);
		//transport = nullptr;

		LOG_NET("net-man", "remove route : connecting aborted");
	}

	// удаление будет в событии disconnected!
	LOG_NET("net-man", "remove route : done");

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool TransportManager::sendMessage(const Route& route, const void* message, int length)
{
	LOG_NET("net-man", "send message to %s %s:%u", TransportType_toString(route.type), inet_ntoa(route.remoteAddress), route.remotePort);

	Transport* transport = findTransport(route);
	bool result = false;

	if (transport != nullptr)
	{
		if (rtl::Logger::check_trace(TRF_PROTO))
		{
			uint8_t* a = (uint8_t*)&route.remoteAddress;
			uint8_t* b = (uint8_t*)&route.localAddress;

			LogProto.log("SEND", "%s %d Bytes TO %d.%d.%d.%d:%u IFACE %d.%d.%d.%d:%u\r\n%s\r\n",
				TransportType_toString(route.type), length,
				a[0], a[1], a[2], a[3], route.remotePort, b[0], b[1], b[2], b[3], route.localPort, message);
		}

		result = transport->sendMessage(message, length) == length;
	}

	LOG_NET("net-man", "send message : %s => %p", result ? "done" : "failed", transport);

	return result;
}
//--------------------------------------
// internal interface
//--------------------------------------
void TransportManager::raiseAsyncEvent(int event_id, Transport* sender, uintptr_t int_param, void* ptr_param)
{
	MegacoAsyncCall(pfn_asyncCallback, sender, event_id, int_param, ptr_param);
}
//--------------------------------------
//
//--------------------------------------
bool TransportManager::startReading(TransportObject* transport)
{
	LOG_NET("net-man", "start reading net object %s", transport->getId());

	//rtl::MutexLock lock(m_readerSync);
	rtl::MutexWatchLock lock(m_readerSync, __FUNCTION__);

	TransportReader* reader = nullptr;

	for (int i = 0; i < m_readerList.getCount(); i++)
	{
		TransportReader* it = m_readerList.getAt(i);

		if (it->getFreeSlots() < 64)
		{
			LOG_NET("net-man", "start reading : reader found");
			reader = it;
			break;
		}
	}

	if (reader == nullptr)
	{
		LOG_NET("net-man", "start reading : new reader started");
		reader = NEW TransportReader;
		m_readerList.add(reader);
	}

	if (!reader->addObject(transport))
	{
		// системная ошибка -- что можно сделать кроме перезагрузки сервиса неизвестно
		LOG_NET_ERROR("net-man", "start reading : reader failed");
		return false;
	}

	LOG_NET("net-man", "start reading : done");
	
	return true;
}
//--------------------------------------
//
//--------------------------------------
bool TransportManager::isAddressBanned(in_addr remoteAddress)
{
	return false;
}
//--------------------------------------
// mg_transport_event_raise_handler_t
//--------------------------------------
//
//--------------------------------------
void TransportManager::async_newConnection(Transport* sender)
{
	LOG_NET_EVENT("net-man", "new connected : sender %s", sender->getId());

	{
		//rtl::MutexLock lock(m_transportSync);
		rtl::MutexWatchLock lock(m_transportSync, __FUNCTION__);
		m_transportList.add(sender);
	}

	sender->start();
}
//--------------------------------------
//
//--------------------------------------
void TransportManager::async_disconnected(Transport* sender)
{
	LOG_NET_EVENT("net-man", "async disconnected : sender %s", sender->getId());

	if (isTransportValid(sender))
	{
		if (sender->getState() == Transport::Disconnecting || sender->getState() == Transport::Disconnected)
		{
			LOG_NET_EVENT("net-man", "notify mg_servide_t::TransportUser_disconnected() : sender %s", sender->getId());
			m_newRouteHandler->TransportUser_disconnected(sender->getRoute());
		}

		removeTransport(sender);
	}
	else
	{
		LOG_NET_WARN("net-man", "async disconnected : sender not valid!");
	}
}
//--------------------------------------
// вызов обработчика без лока!
//--------------------------------------
void TransportManager::async_packetArrival(Transport* sender, RawPacket* packet)
{
	LOG_NET_EVENT("net-man", "packet arrival : sender %s", sender->getId());

	uint32_t ts_1 = packet->ts_1;
	uint32_t ts_2 = rtl::DateTime::getTicks();

	if (rtl::Logger::check_trace(TRF_PROTO))
	{
		uint8_t* a = (uint8_t*)&packet->route.remoteAddress.s_addr;
		uint8_t* b = (uint8_t*)&packet->route.localAddress.s_addr;

		LogProto.log("RECV", "%s %d bytes FROM %d.%d.%d.%d:%u IFACE %d.%d.%d.%d:%u\r\n%s\r\n",
				TransportType_toString(packet->route.type), packet->length,
				a[0], a[1], a[2], a[3], packet->route.remotePort, b[0], b[1], b[2], b[3], packet->route.localPort, packet->packet);
	}

	if (!isTransportValid(sender))
	{
		LOG_NET_WARN("net-man", "packet arrival : sender not valid!");

		FREE(packet);
		rtl::res_counter_t::release(g_raw_packet_counter);

		return;
	}

	Message message;

	if (!message.read(packet->packet))
	{
		LOG_NET_WARN("net-man", "packet arrival : packet not valid! - dropped");
		async_parserError(sender);
		FREE(packet);
		rtl::res_counter_t::release(g_raw_packet_counter);

		return;
	}

	FREE(packet);
	rtl::res_counter_t::release(g_raw_packet_counter);

	m_newRouteHandler->TransportUser_packetArrival(sender->getRoute(), &message);
	
	uint32_t ts_3 = rtl::DateTime::getTicks();

	// проверим тайминги!

	if (ts_3 - ts_1 > 300)
	{
		LOG_WARN("net-man", "incoming packet processed too long : ts_1:%u ts_2:%u (%d) ts_3:%u (%d %d ms)", ts_1, ts_2, ts_2 - ts_1, ts_3, ts_3 - ts_1, ts_3 - ts_2);
	}
}
//--------------------------------------
//
//--------------------------------------
void TransportManager::async_failure(Transport* sender, int net_error)
{
	LOG_NET_EVENT("net-man", "transport failure : sender %s net error %d", sender->getId(), net_error);

	if (isTransportValid(sender))
	{
		m_newRouteHandler->TransportUser_netFailure(sender->getRoute(), net_error);

		removeTransport(sender);
	}
	else
	{
		LOG_NET_WARN("net-man", "transport failure : sender not valid!");
	}
}
//--------------------------------------
//
//--------------------------------------
void TransportManager::async_parserError(Transport* sender)
{
	LOG_NET_EVENT("net-man", "parser error : sender %s", sender->getId());

	if (isTransportValid(sender))
	{
		m_newRouteHandler->TransportUser_parserError(sender->getRoute());

		removeTransport(sender);
	}
	else
	{
		LOG_NET_WARN("net-man", "parser error : sender not valid!");
	}
}
//--------------------------------------
//
//--------------------------------------
void TransportManager::pfn_asyncCallback(void* userdata, uint16_t call_id, uintptr_t int_param, void* ptr_param)
{
	switch (call_id)
	{
	case TransportEvent_NewConnection:
		async_newConnection((Transport*)userdata);
		break;
	case TransportEvent_Disconnected:
		async_disconnected((Transport*)userdata);
		break;
	case TransportEvent_PacketArrival:
		async_packetArrival((Transport*)userdata, (RawPacket*)ptr_param);
		break;
	case TransportEvent_Failure:
		async_failure((Transport*)userdata, (int)int_param);
		break;
	case TransportEvent_Invalid:
		async_parserError((Transport*)userdata);
		break;
	case PURGATORY_TIMER:
		clearPurgatory();
		break;
	}
}
//--------------------------------------
//
//--------------------------------------
bool TransportManager::addTransport(Transport* transport)
{
	//rtl::MutexLock lock(m_transportSync);
	rtl::MutexWatchLock lock(m_transportSync, __FUNCTION__);

	int index = m_transportList.indexOf(transport);
	
	if (index != BAD_INDEX)
	{
		m_transportList.removeAt(index);

		LOG_WARN("net-man", "route transport already exist. old dropped!");
	}

	m_transportList.add(transport);
	return true;
}
//--------------------------------------
//
//--------------------------------------
bool TransportManager::removeTransport(Transport* transport)
{
	if (transport == nullptr)
		return false;

	bool result = false;
	{
		//rtl::MutexLock lock(m_transportSync);
		rtl::MutexWatchLock lock(m_transportSync, __FUNCTION__);

		int index = m_transportList.indexOf(transport);
	
		if (index != BAD_INDEX)
		{
			transport = m_transportList.getAt(index);
			m_transportList.removeAt(index);
			result = true;
		}
	}

	transport->destroy();
	//DELETEO(transport);
	pushToPurgatory(transport);

	return result;
}
//--------------------------------------
//
//--------------------------------------
bool TransportManager::isTransportValid(Transport* transport)
{
	//rtl::MutexLock lock(m_transportSync);
	rtl::MutexWatchLock lock(m_transportSync, __FUNCTION__);

	return m_transportList.indexOf(transport) != BAD_INDEX;
}
//--------------------------------------
//
//--------------------------------------
Transport* TransportManager::findTransport(const Route& route)
{
	uint64_t key = MAKE_KEY64(route.remoteAddress, route.remotePort, route.type);

	//rtl::MutexLock lock(m_transportSync);
	rtl::MutexWatchLock lock(m_transportSync, __FUNCTION__);

	TransportPtr* ptr = m_transportList.find(key);

	return ptr != nullptr ? *ptr : nullptr;
}
//--------------------------------------
//
//--------------------------------------
int TransportManager::compareTransportObj(const TransportPtr& left, const TransportPtr& right)
{
	uint64_t left_key = left->getKey();
	uint64_t right_key = right->getKey();
	return left_key < right_key ? -1 : left_key > right_key ? 1 : 0;
}
//--------------------------------------
//
//--------------------------------------
int TransportManager::compareTransportKey(const uint64_t& left, const TransportPtr& right)
{
	uint64_t right_key = right->getKey();
	return left < right_key ? -1 : left > right_key ? 1 : 0;
}
//--------------------------------------
//
//--------------------------------------
void TransportManager::pushConnecting(Transport* transport)
{
	rtl::MutexWatchLock lock(m_transportSync, __FUNCTION__);
	m_connectingList.add(transport);
}
//--------------------------------------
//
//--------------------------------------
bool TransportManager::moveConnected(Transport* transport)
{
	rtl::MutexWatchLock lock(m_transportSync, __FUNCTION__);

	int index = m_connectingList.indexOf(transport);
	
	if (index != BAD_INDEX)
	{
		m_connectingList.removeAt(index);

		uint64_t key = transport->getKey();

		TransportPtr* oldtrans = m_transportList.find(key, index);

		if (oldtrans != nullptr && index != BAD_INDEX)
		{
			LOG_WARN("net-man", "transport already exist to route...%p idx:%d", *oldtrans, index);

			m_transportList.removeAt(index);
			Transport* tr = *oldtrans;
			if (tr != nullptr)
			{
				tr->destroy();
				//DELETEO(tr);
				pushToPurgatory(tr);
			}
		}

		m_transportList.add(transport);

		return true;
	}

	return false;
}
//--------------------------------------
//
//--------------------------------------
bool TransportManager::dropConnecting(Transport* transport)
{
	rtl::MutexWatchLock lock(m_transportSync, __FUNCTION__);
	
	int index = m_connectingList.indexOf(transport);

	if (index != BAD_INDEX)
	{
		m_connectingList.removeAt(index);

		return true;
	}

	return false;
}
//--------------------------------------
//
//--------------------------------------
Transport* TransportManager::popConnecting(const Route& route)
{
	rtl::MutexWatchLock lock(m_transportSync, __FUNCTION__);

	uint64_t key = MAKE_KEY64(route.remoteAddress, route.remotePort, route.type);
	int index = BAD_INDEX;

	Transport* transport = nullptr;
	TransportPtr* found = m_connectingList.find(key, index);

	if (found != nullptr)
	{
		transport = *found;
		m_connectingList.removeAt(index);
	}

	return transport;
}
//--------------------------------------
//
//--------------------------------------
void TransportManager::pushToPurgatory(Transport* transport)
{
	rtl::MutexWatchLock lock(m_purgatorySync, __FUNCTION__);

	for (int i = 0; i < m_purgatory.getCount(); i++)
	{
		Transport* tr = m_purgatory.getAt(i);
		if (tr == nullptr)
		{
			continue;
		}

		if (tr == transport)
		{
			return;
		}
	}

	transport->setDeadTime();
	m_purgatory.add(transport);
}
//--------------------------------------
//
//--------------------------------------
void TransportManager::timerEventCallback(rtl::Timer* sender, uint32_t elapsed, void* user_data, int tid)
{
	MegacoAsyncCall(pfn_asyncCallback, sender, PURGATORY_TIMER, 0, 0);
}
//--------------------------------------
//
//--------------------------------------
void TransportManager::clearPurgatory()
{
	rtl::MutexWatchLock lock(m_purgatorySync, __FUNCTION__);

	for (int i = m_purgatory.getCount()-1; i >= 0 ; i--)
	{
		Transport* transport = m_purgatory.getAt(i);
		if (transport == nullptr)
		{
			continue;
		}

		uint32_t tick = rtl::DateTime::getTicks();
		if (tick - transport->getDeadTime() >= DEADTIME)
		{
			transport->destroy();
			DELETEO(transport);
			m_purgatory.removeAt(i);
		}
	}
}

//--------------------------------------
//
//--------------------------------------
TransportListener* TransportManager::findListener(const EndPoint& listen_iface, int* index)
{
	//rtl::MutexLock lock(m_listenerSync);
	rtl::MutexWatchLock lock(m_listenerSync, __FUNCTION__);

	for (int i = 0; i < m_listenerList.getCount(); i++)
	{
		TransportListener* listener = m_listenerList.getAt(i);

		if (listener->getInterfaceAddress().s_addr == listen_iface.address.s_addr &&
			(listen_iface.port == 0 || listener->getInterfacePort() == listen_iface.port) &&
			listener->getTransportType() == listen_iface.type)
		{
			if (index != nullptr)
			{
				*index = i;
			}

			return listener;
		}
	}

	return nullptr;
}
//--------------------------------------
//
//--------------------------------------
TransportListener* TransportManager::getListener(const EndPoint& listen_point)
{
	TransportListener* listener = findListener(listen_point);

	if (listener == nullptr)
	{
		if (listen_point.type == TransportType::UDP)
		{
			listener = NEW TransportListenerUDP();
		}
		else if (listen_point.type == TransportType::TCP)
		{
			listener = NEW TransportListenerTCP();
		}

		if (listener->create(listen_point) && listener->startListen())
		{
			//rtl::MutexLock lock(m_listenerSync);
			rtl::MutexWatchLock lock(m_listenerSync, __FUNCTION__);
			m_listenerList.add(listener);
		}
		else
		{
			listener->destroy();
			DELETEO(listener);
			listener = nullptr;
		}
	}

	return listener;
}
//--------------------------------------
//
//--------------------------------------
bool TransportManager::getBestInterface(in_addr remote_ip, in_addr& local_ip)
{
	uint16_t destPort = 80;

	SOCKET sock = ::socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (INVALID_SOCKET == sock)
	{
		return false;
	}

	sockaddr_in RecvAddr = { 0 };

	RecvAddr.sin_family = AF_INET;
	RecvAddr.sin_port = htons(destPort);
	RecvAddr.sin_addr = remote_ip;

	bool result = false;

	if (::connect(sock, (sockaddr*)&RecvAddr, sizeof(RecvAddr)) == 0)
	{
		sockaddr_in saddr;
		socklen_t slen = sizeof(saddr);

		if (getsockname(sock, (sockaddr*)&saddr, &slen) == 0)
		{
			local_ip = saddr.sin_addr;
			result = true;
		}
	}

	closesocket(sock);

	return result;
}
//--------------------------------------
//
//--------------------------------------
in_addr TransportManager::getDefaultInterface()
{
	return s_defaultInterface;
}
//--------------------------------------
//
//--------------------------------------
uint16_t TransportManager::getDefaultPort(TransportType type)
{
	rtl::MutexWatchLock lock(m_listenerSync, __FUNCTION__);

	for (int i = 0; i < m_listenerList.getCount(); i++)
	{
		TransportListener* listener = m_listenerList.getAt(i);
		if (listener->getTransportType() == type)
			return listener->getInterfacePort();
	}

	return 0;
}
//--------------------------------------
// for UDP transports -> 
//--------------------------------------
bool TransportManager::attachTransportToListenerUDP(const EndPoint& local_point, Transport* transport)
{
	if (local_point.type != h248::TransportType::UDP)
		return false;
	
	TransportUDP* udp_transport = (TransportUDP*)transport;
	TransportListenerUDP* listener = (TransportListenerUDP*)getListener(local_point);

	if (listener == nullptr)
		return false;

	return listener->attachTransport(udp_transport);
}
//--------------------------------------
//
//--------------------------------------
bool TransportManager::detachTransportFromListenerUDP(Transport* transport)
{
	EndPoint local_point = { transport->getType(), transport->getInterfaceAddress(), transport->getInterfacePort() };
	if (local_point.type != h248::TransportType::UDP)
		return false;

	TransportUDP* udp_transport = (TransportUDP*)transport;
	TransportListenerUDP* listener = (TransportListenerUDP*)findListener(local_point);

	if (listener != nullptr)
	{
		listener->detachTransport(udp_transport);

		removeTransport(udp_transport);
	}

	return listener != nullptr;
}
//--------------------------------------
//
//--------------------------------------
const char* TransportType_toString(TransportType type)
{
	static const char* names[] = { "nil", "udp", "tcp", "sctp" };

	return type >= TransportType::Nil && type <= TransportType::SCTP ? names[(int)type] : "error";
}
}
//--------------------------------------
