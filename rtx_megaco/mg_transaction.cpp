﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_transaction.h"
#include "mg_transaction_manager.h"
#include "mg_transport_manager.h"

namespace h248
{
	//--------------------------------------
	//
	//--------------------------------------
#define AUTO_LOCK(t) auto_counter_t<uint32_t> auto__counter__(t)
	//--------------------------------------
	//
	//--------------------------------------
	Transaction::Transaction(ITransactionUser* user, TransactionID tid, int mgc_version) :
		m_usingCount(0),
		m_sync("mg-transaction"),
		m_user(user),
		m_tranId(tid),
		m_mgcVersion(mgc_version),
		m_type(Transaction::Undefined),
		m_state(State_Idle),
		m_longTimerTimestamp(rtl::DateTime::getTicks())
	{
		m_retransmissionEnabled = false;
		m_retransmissionTime = 0;
		m_retransmissionTimestamp = 0;
		m_retransmissionCount = 0;

		std_snprintf(m_id, 16, "N(%u)", m_tranId._tid);

		rtl::res_counter_t::add_ref(g_transaction_counter);
	}
	//--------------------------------------
	//
	//--------------------------------------
	Transaction::~Transaction()
	{
		rtl::res_counter_t::release(g_transaction_counter);
	}
	//--------------------------------------
	// исходящий запрос
	//--------------------------------------
	bool Transaction::txRequest(const Field* request, const Route& route)
	{
		AUTO_LOCK(m_usingCount);

		if (TransactionManager::isStopped())
		{
			return false;
		}

		// менеджером транзакций гарантируется что возможен только один вызов после создания
		// блокировка 
		LOG_TRANS("trn", "id:%s : sending request to %s:%u", m_id, inet_ntoa(route.remoteAddress), route.remotePort);

		m_state = State_Trying;
		m_route = route;

		// клиентский тип транзакции -- исходящий запрос
		m_type = Client;

		makeName();

		// сохраняем запрос для повторных отправок
		m_retransmissionEnabled = true;
		m_retransmissionTimestamp = rtl::DateTime::getTicks();
		m_retransmissionTime = TransactionManager::getInitialRetransmissionValue();
		m_retransmissionCount = 1;
		m_retransmissionMessage = *request;

		// в список транзакций 
		TransactionManager::addClientTransaction(this);

		bool result = sendMessage();

		if (!result)
		{
			LOG_TRANS("trn", "id:%s : sending message failed -- terminate", m_id);
			TransactionManager::removeTransaction(this);
		}
		else
		{
			// таймер запускаем только после удачной отправки сообщения
			LOG_TRANS("trn", "id:%s : sending message success -- start LONG-TIMER", m_id);
			TransactionManager::startLongTimer(this);
		}

		// можно безопасно удалять при фальше
		return result;
	}
	//--------------------------------------
	// входящий ответ
	//--------------------------------------
	bool Transaction::rxReply(const Field* response)
	{
		AUTO_LOCK(m_usingCount);

		bool raise_event = false;

		// под локом изменяем состояния
		{
			//rtl::MutexLock lock(m_sync);
			rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

			LOG_TRANS("trn", "id:%s : reply received : state(%s)", m_id, State_toString(m_state));

			if (m_state == State_Trying ||
				m_state == State_Pending)
			{
				m_state = State_Replied;
				raise_event = true;
			}

			LOG_TRANS("trn", "id:%s : reply received : new state(%s)", m_id, State_toString(m_state));
		}

		// работа с пользователем
		if (raise_event)
		{
			LOG_TRANS("trn", "id:%s : reply reseived : notify user", m_id);
			if (!m_user->TransactionUser_responseReceived(this, response))
			{
				m_state = State_Terminated;
				LOG_TRANS("trn", "id:%s : pending received : broken transaction -> terminated", m_id);
				return true;
			}
		}

		LOG_TRANS("trn", "id:%s : reply reseived : send ack", m_id);

		sendAck();

		LOG_TRANS("trn", "id:%s : reply reseived : done", m_id);

		return true;
	}
	//--------------------------------------
	// входящиее уведомление о задержке
	//--------------------------------------
	bool Transaction::rxPending(const Field* pending)
	{
		AUTO_LOCK(m_usingCount);

		bool raise_event = false;

		// под локом изменяем состояния
		{
			//rtl::MutexLock lock(m_sync);
			rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

			LOG_TRANS("trn", "id:%s : pending received : state(%s)", m_id, State_toString(m_state));

			if (m_state == State_Trying)
			{
				m_state = State_Pending;
				raise_event = true;
			}

			LOG_TRANS("trn", "id:%s : pending received : new state(%s)", m_id, State_toString(m_state));
		}

		// работа с пользователем
		if (raise_event)
		{
			LOG_TRANS("trn", "id:%s : pending received : notify user", m_id);
			if (!m_user->TransactionUser_pendingReceived(this))
			{
				m_state = State_Terminated;
				LOG_TRANS("trn", "id:%s : pending received : broken transaction -> terminated", m_id);

				return true;
			}
		}

		LOG_TRANS("trn", "id:%s : pending received : send ack", m_id);

		sendAck();

		LOG_TRANS("trn", "id:%s : pending received : done", m_id);

		return true;
	}
	//--------------------------------------
	// принудительное завершение работы
	//--------------------------------------
	void Transaction::cancel()
	{
		AUTO_LOCK(m_usingCount);

		LOG_TRANS("trn", "id:%s : cancel client transaction use-count:%d", m_id, m_usingCount);

		//rtl::MutexLock lock(m_sync);
		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		m_state = State_Terminated;
		m_tranId._tid = 0xFFFFFFFF;
		//m_user = nullptr;
		m_retransmissionEnabled = false;
		m_retransmissionMessage.cleanup();
	}
	//--------------------------------------
	// входящий запрос
	//--------------------------------------
	bool Transaction::rxRequest(const Field* request, const Route& route) // Field* reply, 
	{
		AUTO_LOCK(m_usingCount);

		bool send_again = false;
		bool raise_event = false;

		{
			//rtl::MutexLock lock(m_sync);
			rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

			if (m_state == State_Idle)
			{
				// выставляем тип транзакции
				m_state = State_Trying;
				m_route = route;
				// серверный тип транзакции -- входящий запрос
				m_type = Server;
				makeName();

				LOG_TRANS("trn", "id:%s : request received : start LONG-TIMER", m_id);

				// возводим таймеры
				TransactionManager::startLongTimer(this);

				TransactionManager::addServerTransaction(this);

				raise_event = true;
			}
			else if (m_state == State_Replied)
			{
				LOG_TRANS("trn", "id:%s : retransmitted request received : resend last reply", m_id);
				// повторная отправка ответа
				send_again = true;
			}
			else
			{
				LOG_TRANS("trn", "id:%s : request received : invalid state(%s)", m_id, State_toString(m_state));
			}
		}

		if (send_again)
		{
			LOG_TRANS("trn", "id:%s : request received : retransmit reply", m_id);

			sendMessage();
		}
		else if (raise_event)
		{
			LOG_TRANS("trn", "id:%s : request received : notify user", m_id);
			m_user->TransactionUser_requestReceived(this, request);
		}

		LOG_TRANS("trn", "id:%s : request received : done", m_id);

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Transaction::txReply(const Field* response)
	{
		AUTO_LOCK(m_usingCount);

		{
			//rtl::MutexLock lock(m_sync);
			rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

			LOG_TRANS("trn", "id:%s : send reply: state(%s)", m_id, State_toString(m_state));

			if (m_state == State_Replied)
			{
				LOG_ERROR("trn", "id:%s : send reply: failed : invalid state (%s)", m_id, State_toString(m_state));
				return false;
			}

			m_state = State_Replied;
			m_retransmissionMessage = *response;

			LOG_TRANS("trn", "id:%s : send reply : new state(%s)", m_id, State_toString(m_state));
		}

		sendMessage();

		LOG_TRANS("trn", "id:%s : send reply : done", m_id);

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Transaction::txPending()
	{
		AUTO_LOCK(m_usingCount);

		bool send_request = false;

		{
			//rtl::MutexLock lock(m_sync);
			rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

			LOG_TRANS("trn", "id:%s : send pending : state(%s)", m_id, State_toString(m_state));

			if (m_state == State_Trying)
			{
				m_state = State_Pending;
			}
		}

		if (send_request)
		{
			sendPending();
		}

		LOG_TRANS("trn", "id:%s : send pending : done", m_id);

		return true;
	}
	//--------------------------------------
	// уведомление о получении ответа
	//--------------------------------------
	bool Transaction::rxAck()
	{
		AUTO_LOCK(m_usingCount);

		{
			//rtl::MutexLock lock(m_sync);
			rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

			LOG_TRANS("trn", "id:%s : received ack: state(%s)", m_id, State_toString(m_state));

			m_state = State_Confirmed;
		}

		LOG_TRANS("trn", "id:%s : received ack: done", m_id);

		return true;
	}
	//--------------------------------------
	// колбек для таймера
	//--------------------------------------
	bool Transaction::checkTimer(uint32_t current_time)
	{
		// вечная проверка на LONG_TIMER
		int livetime = current_time - m_longTimerTimestamp;

		ITransactionUser* user = nullptr;

		if (m_usingCount > 0)
			return true;

		if (m_state == State_Terminated)
			return false;

		if (livetime >= TransactionManager::getLongTimerValue())
		{
			user = m_user;
		}

		if (user != nullptr)
		{
			user->TransactionUser_terminated(this);
			return false;
		}

		if (m_retransmissionEnabled)
		{
			MegacoAsyncCall(asyncRetransmitMessage, this, 0, 0, nullptr);
		}

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Transaction::retransmitMessage()
	{
		// если есть повторная отправка

		bool send_last_message = false;
		bool timeout = false;
		uint32_t current_time = rtl::DateTime::getTicks();

		{
			//rtl::MutexLock lock(m_sync);
			rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

			if (m_state == State_Trying && m_type == Client)
			{
				// проверяем
				int diff = current_time - m_retransmissionTimestamp;

				if (diff >= m_retransmissionTime)
				{
					m_retransmissionTimestamp = current_time;
					if (m_retransmissionTime < 4000)
					{
						m_retransmissionTime *= 2;
					}

					m_retransmissionCount++;

					send_last_message = m_retransmissionCount <= 7;
					timeout = !send_last_message;
				}
			}
		}

		if (send_last_message)
		{
			LOG_TRANS("trn", "id:%s : retransmit message count:%d", m_id, m_retransmissionCount);

			sendMessage();
		}

		if (timeout)
		{
			if (m_type == Client)
			{
				LOG_TRANS("trn", "id:%s : TIMEOUT elapsed : notify user", m_id);

				m_user->TransactionUser_requestTimeout(this);
			}
		}
	};
	//--------------------------------------
	//
	//--------------------------------------
	bool Transaction::sendBackError(ErrorCode error_code, const char* reason)
	{
		AUTO_LOCK(m_usingCount);

		// отправляем сообщение Error на транспорт
		char message[256];
		char mid[128];
		int length = std_snprintf(message, 256, "%s/%u %s %s = %u { \"%s. TransactionId = %u\" }",
			Token_toString(Token::MEGACO),
			m_mgcVersion,
			TransactionManager::makeMid(mid, 128, m_route.localAddress, m_route.localPort), Token_toString(Token::Error), error_code, reason != nullptr ? reason : ErrorCode_toString(error_code),
			m_tranId._tid);

		return TransportManager::sendMessage(m_route, message, length);
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Transaction::sendMessage()
	{
		AUTO_LOCK(m_usingCount);

		MessageWriterParams params;

		megaco_get_output_mode(&params);

		// отправляем сообщение на транспорт
		rtl::MemoryStream stream;
		char mid[128];
		stream << Token_toString(Token::MEGACO)
			<< '/'
			<< m_mgcVersion
			<< ' '
			<< TransactionManager::makeMid(mid, 128, m_route.localAddress, m_route.localPort)
			<< (params.human_readable ? "\r\n" : " ");

		m_retransmissionMessage.writeTo(stream, &params);

		const char* message_ptr = (const char*)stream.getBuffer();

		int message_len = (int)stream.getPosition();

		return TransportManager::sendMessage(m_route, message_ptr, message_len);
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Transaction::sendAck()
	{
		AUTO_LOCK(m_usingCount);

		// отправляем сообщение TransactionResponseAck на транспорт
		char mid[128];
		char message[256];
		int length = std_snprintf(message, 256, "%s/%u %s %s { %u }",
			Token_toString(Token::MEGACO),
			m_mgcVersion,
			TransactionManager::makeMid(mid, 128, m_route.localAddress, m_route.localPort),
			Token_toString(Token::TransactionResponseAck),
			m_tranId._tid);

		return TransportManager::sendMessage(m_route, message, length);
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Transaction::sendPending()
	{
		AUTO_LOCK(m_usingCount);

		// отправляем сообщение Pending на транспорт
		char mid[128];
		char message[256];
		int length = std_snprintf(message, 256, "%s/%u %s %s = %u { }",
			Token_toString(Token::MEGACO),
			m_mgcVersion,
			TransactionManager::makeMid(mid, 128, m_route.localAddress, m_route.localPort),
			Token_toString(Token::Pending),
			m_tranId._tid);

		return TransportManager::sendMessage(m_route, message, length);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Transaction::makeName()
	{
		std_snprintf(m_id, 16, "%c(%u)", m_type == Client ? 'C' : 'S', m_tranId._tid);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Transaction::asyncRetransmitMessage(void* userdata, uint16_t call_id, uintptr_t int_param, void* ptr_param)
	{
		Transaction* trn = (Transaction*)userdata;
		trn->retransmitMessage();
	}
	//--------------------------------------
	//
	//--------------------------------------
	const char* Transaction::State_toString(Transaction::State state)
	{
		static const char* names[] = {
			"State_Idle",
			"State_Trying",
			"State_Pending",
			"State_Replied",
			"State_Confirmed",
			"State_Terminated"
		};

		return state >= State_Idle && state <= State_Terminated ? names[state] : "State_Unknown";
	}
}
//--------------------------------------
