﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_message.h"

namespace h248
{
	//--------------------------------------
	//
	//--------------------------------------
	Message::Message() : m_ah(nullptr), m_version(0)
	{
		rtl::res_counter_t::add_ref(g_message_counter);
	}
	//--------------------------------------
	//
	//--------------------------------------
	Message::~Message()
	{
		cleanup();

		rtl::res_counter_t::release(g_message_counter);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Message::cleanup()
	{
		if (m_ah != nullptr)
		{
			DELETEO(m_ah);
			m_ah = nullptr;
		}

		m_mid.clear();

		int count = m_fieldList.getCount();

		if (count > 0)
		{
			for (int i = 0; i < count; i++)
			{
				DELETEO(m_fieldList[i]);
			}

			m_fieldList.clear();
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	const Field* Message::findField(const char* path) const
	{
		const char* token = path;
		const char* next = strchr(path, '\\');

		int len = next == nullptr ? -1 : PTR_DIFF(next, token);
		Token type = Token_parse(token, len);
		const Field* node = nullptr;

		if (type == Token::_Custom)
			node = getField(token, len);
		else
			node = getField(type);

		if (next == nullptr)
		{
			return node;
		}

		return node->findField(next + 1);
	}
	//--------------------------------------
	//
	//--------------------------------------
	Field* Message::findField(const char* path)
	{
		const char* token = path;
		const char* next = strchr(path, '\\');

		int len = next == nullptr ? -1 : PTR_DIFF(next, token);
		Token type = Token_parse(token, len);
		Field* node = nullptr;

		if (type == Token::_Custom)
			node = getField(token, len);
		else
			node = getField(type);

		if (next == nullptr)
		{
			return node;
		}

		return node->findField(next + 1);
	}
	//--------------------------------------
	//
	//--------------------------------------
	Field* Message::addField(const char* path, const Field* node)
	{
		if (path == 0 || path[0] == 0)
		{
			Field* clone = NEW Field(*node);
			m_fieldList.add(clone);

			return clone;
		}

		Field* found = findField(path);

		if (!found)
			return nullptr;

		return found->addField(node);
	}
	//--------------------------------------
	//
	//--------------------------------------
	Field* Message::addField(const char* path, Field* node, bool attach)
	{
		if (path == 0 || path[0] == 0)
		{
			Field* added = attach ? node : NEW Field(*node);

			m_fieldList.add(added);

			return added;
		}

		Field* found = findField(path);

		if (!found)
			return nullptr;

		return found->addField(node, attach);
	}
	//--------------------------------------
	//
	//--------------------------------------
	Field* Message::createField(const char* path, const char* name, const char* value)
	{
		if (path == 0 || path[0] == 0)
		{
			Field* added = NEW Field(name, value);

			m_fieldList.add(added);

			return added;
		}

		Field* found = findField(path);

		if (!found)
			return nullptr;

		return found->addField(name, value);
	}
	//--------------------------------------
	//
	//--------------------------------------
	Field* Message::createField(const char* path, Token type, const char* value)
	{
		if (path == 0 || path[0] == 0)
		{
			Field* added = NEW Field(type, value);

			m_fieldList.add(added);

			return added;
		}

		Field* found = findField(path);

		if (!found)
			return nullptr;

		return found->addField(type, value);
	}
	//--------------------------------------
	//
	//--------------------------------------
	Field* Message::createErrorDescriptor(const char* path, int error_code, const char* reason)
	{
		if (path == 0 || path[0] == 0)
		{
			Field* added = NEW Field(Token::_Error);

			added->setValue(error_code);
			added->setRawData(reason);

			m_fieldList.add(added);

			return added;
		}

		Field* found = findField(path);

		if (!found)
			return nullptr;

		return found->addErrorDescriptor(error_code, reason);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Message::removeField(const char* path)
	{
		if (path == 0 || path[0] == 0)
		{
			return;
		}

		Field* found = findField(path);

		if (found)
		{
			Field* parent = found->getParent();

			if (parent != nullptr)
				parent->removeField(found);
			else
				removeTopField(path);
		}
	}
	//--------------------------------------
	// Message = MegacopToken SLASH Version SEP mId SEP messageBody
	//--------------------------------------
	bool Message::read(const char* packet)
	{
		Parser parser;

		if (!parser.startReading(packet))
			return false;

		ParserState state = parser.readNext();

		if (state == ParserState::Auth)
		{
			m_ah = NEW Authentication;
			m_ah->readFrom(parser.getTokenValue());

			state = parser.readNext();
		}

		// читаем заголовок
		if (state != ParserState::Startline)
		{
			return false;
		}

		if (!setStartLine(parser.getTokenName(), parser.getTokenValue()))
		{
			return false;
		}

		while (parser.readNext() == ParserState::Token)
		{
			Field* node = NEW Field();

			if (node->read(parser))
			{
				m_fieldList.add(node);
			}
			else
			{
				DELETEO(node);
				return false;
			}
		}

		return parser.getState() != ParserState::Error;
	}
	//--------------------------------------
	//
	//--------------------------------------
	rtl::MemoryStream& Message::writeTo(rtl::MemoryStream& stream, MessageWriterParams* params)
	{
		writeStartLine(stream, params);

		if (params != nullptr)
		{
			params->level = 0;
		}

		for (int i = 0; i < m_fieldList.getCount(); i++)
		{
			m_fieldList.getAt(i)->writeTo(stream, params);
		}

		return stream;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Message::setStartLine(const char* token, const char* value)
	{
		const char* cp = strchr(token, '/');

		if (cp == nullptr)
			return false;

		m_version = atoi(cp + 1);

		// parsing mid
		// [ipv4]:port
		// [ipv6]:port
		// <dns>:port]

		return m_mid.parse(value);
	}
	//--------------------------------------
	//
	//--------------------------------------
	rtl::MemoryStream& Message::writeStartLine(rtl::MemoryStream& stream, MessageWriterParams* params)
	{
		stream << Token_toString(Token::MEGACO) << '/' << m_version << ' ';

		return m_mid.writeTo(stream, params);
	}
	//--------------------------------------
	//
	//--------------------------------------
	const Field* Message::getField(const char* name, int length) const
	{
		if (length == -1)
		{
			length = std_strlen(name);
		}

		for (int i = 0; i < m_fieldList.getCount(); i++)
		{
			const Field* node = m_fieldList.getAt(i);
			if (std_strnicmp(node->getName(), name, length) == 0)
				return node;
		}

		return nullptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	Field* Message::getField(const char* name, int length)
	{
		if (length == -1)
		{
			length = std_strlen(name);
		}

		for (int i = 0; i < m_fieldList.getCount(); i++)
		{
			Field* node = m_fieldList.getAt(i);
			if (std_strnicmp(node->getName(), name, length) == 0)
				return node;
		}

		return nullptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	const Field* Message::getField(Token type) const
	{
		for (int i = 0; i < m_fieldList.getCount(); i++)
		{
			const Field* node = m_fieldList.getAt(i);
			if (node->getType() == type)
				return node;
		}

		return nullptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	Field* Message::getField(Token type)
	{
		for (int i = 0; i < m_fieldList.getCount(); i++)
		{
			Field* node = m_fieldList.getAt(i);
			if (node->getType() == type)
				return node;
		}

		return nullptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Message::removeTopField(const char* name)
	{
		for (int i = 0; i < m_fieldList.getCount(); i++)
		{
			Field* it = m_fieldList[i];

			if (std_stricmp(name, it->getName()) == 0)
			{
				m_fieldList.removeAt(i);
				DELETEO(it);
				return;
			}
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Message::removeTopField(Token type)
	{
		for (int i = 0; i < m_fieldList.getCount(); i++)
		{
			Field* it = m_fieldList[i];

			if (type == it->getType())
			{
				m_fieldList.removeAt(i);
				DELETEO(it);
				return;
			}
		}
	}
}
//--------------------------------------
