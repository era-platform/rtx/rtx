﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_transaction_manager.h"

namespace h248
{
	//--------------------------------------
	// макрос для генерации ключа поиска транзакции
	//--------------------------------------
#define MAKE_TRN_KEY(cid, tid) ((uint64_t(cid) << 32) | uint64_t(tid))
#define GET_TRN_ID(tid) uint32_t(tid & 0x00000000FFFFFFFF)
#define GET_CTL_ID(tid) uint32_t((tid & 0xFFFFFFFF00000000) >> 32)
	//--------------------------------------
	//
	//--------------------------------------
	class Transaction
	{
		enum State
		{
			State_Idle,			// начальное состояние			входящий	исходящий
			State_Trying,		// запрос получен/отправлен		входящий	исходящий	; выставляется после отправки/приема
			State_Pending,		// запрос в обработке			входящий	исходящий	; выставляется после повторного приема запроса
			State_Replied,		// ответ получен/отправлен		входящий	исходящий	; выставляется после отправки/приема ответа
			State_Confirmed,		// ответ подтвержден			входящий	исходящий	; выставляется после отправки/приема ACK
			State_Terminated,	// транзакция ожидает срабатывания LONG-TIMER
		};
	public:
		enum Type
		{
			Undefined,
			Client,
			Server
		};

	private:
		volatile uint32_t m_usingCount;
		rtl::MutexWatch m_sync;
		ITransactionUser* m_user;				// пользователь транзакций. в данный момент только контроллеры
		char m_id[16];								// человеческое имя транзакции
		TransactionID m_tranId;								// идентификатор транзакции + идентификатор контроллера
		int m_mgcVersion;							// megaco version
		Type m_type;				// 0-unknown, 1-client, 2-server
		Route m_route;				// маршрут по которому транзакция ходит!
		State m_state;				// состояние транзакции
		uint32_t m_longTimerTimestamp;			// срабатывает через 30 сек в не зависимости от состояния
		bool m_retransmissionEnabled;				// таймер повторной передачи
		uint32_t m_retransmissionTimestamp;		// время начала ожидания
		int m_retransmissionTime;					// продолжительность ожидания перед повторной отправкой
		int m_retransmissionCount;					// текущее количество попыток отправки 
		Field m_retransmissionMessage;	// для повторной отправки сообщений

	public:
		// инициализация клиентской транзакции
		Transaction(ITransactionUser* user, TransactionID tid, int mgc_version);

		// client transaction
		bool txRequest(const Field* message, const Route& route);
		bool rxReply(const Field* reply);
		bool rxPending(const Field* pending);
		void cancel();

		// server transaction
		bool rxRequest(const Field* request, const Route& route); // Field* reply, 
		bool txReply(const Field* message);
		bool txPending();
		bool rxAck();

		// колбек для таймера -- false после срабатывания лонг таймера!
		bool checkTimer(uint32_t current_time);
		bool sendBackError(ErrorCode error_code, const char* reason);

		TransactionID getTranId() const { return m_tranId; }
		Type getType() const { return m_type; }
		const char* getName() const { return m_id; }

	private:
		friend class TransactionManager;
		~Transaction(); // менеджером по лонг таймеру

		bool sendMessage();
		bool sendAck();
		bool sendPending();

		void makeName();

		static void asyncRetransmitMessage(void* userdata, uint16_t call_id, uintptr_t int_param, void* ptr_param);
		void retransmitMessage();

		static const char* State_toString(State state);
	};
}
//--------------------------------------
