﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_auth.h"

namespace h248
{
	//--------------------------------------
	//
	//--------------------------------------
	Authentication::Authentication() : m_securityParmIndex(0), m_sequenceNum(0), m_authDataLength(0)
	{
		memset(m_authData, 0, sizeof m_authData);
	}
	//--------------------------------------
	//
	//--------------------------------------
	Authentication::Authentication(uint32_t param_index, uint32_t seq, const uint8_t* data, int length) :
		m_securityParmIndex(param_index), m_sequenceNum(seq), m_authDataLength(0)
	{
		if (data != nullptr && length > 0)
		{
			m_authDataLength = length;

			if (m_authDataLength > MEGACO_AUTH_DATA_MAX)
				m_authDataLength = MEGACO_AUTH_DATA_MAX;
			if (m_authDataLength > MEGACO_AUTH_DATA_MAX)
				m_authDataLength = MEGACO_AUTH_DATA_MAX;

			memcpy(m_authData, data, m_authDataLength);
		}
		else
		{
			memset(m_authData, 0, sizeof m_authData);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	Authentication::~Authentication()
	{
		m_securityParmIndex = 0;
		m_sequenceNum = 0;
		m_authDataLength = 0;

		memset(m_authData, 0, sizeof m_authData);
	}
	//--------------------------------------
	// SecurityParmIndex COLON SequenceNum COLON AuthData
	//--------------------------------------
	int Authentication::readFrom(const char* stream)
	{
		if (stream == nullptr || *stream == 0)
			return 0;

		// 0.......8.......6......4..........
		// 023AF567:48A3EC03:2348FD8C0E47638B

		// SecureParmIndex
		const char* s_ptr = rtl::skip_LWSP(stream);
		const char* c_ptr = strchr(s_ptr, ':');
		if (c_ptr == nullptr)
			return 0;
		m_securityParmIndex = rtl::hex_to_uint(s_ptr, PTR_DIFF(c_ptr, s_ptr));
		s_ptr = c_ptr + 1;

		// SequenceNumber
		c_ptr = strchr(s_ptr, ':');
		if (c_ptr == nullptr)
			return 0;
		m_sequenceNum = rtl::hex_to_uint(s_ptr, PTR_DIFF(c_ptr, s_ptr));
		s_ptr = c_ptr + 1;

		// AuthData
		c_ptr = strpbrk(s_ptr, " \t\n\r");

		int length;

		if (c_ptr == nullptr)
		{
			length = (int)strlen(s_ptr);
		}
		else
		{
			length = PTR_DIFF(c_ptr, s_ptr);
		}

		if (length == 0)
			return 0;

		m_authDataLength = length / 2;

		if ((length & 1) != 0)
		{
			m_authDataLength++;
		}

		if (m_authDataLength > MEGACO_AUTH_DATA_MAX || m_authDataLength < MEGACO_AUTH_DATA_MIN)
		{
			return 0;
		}

		rtl::hex_to_bytes(s_ptr, length, m_authData, m_authDataLength);

		return PTR_DIFF(s_ptr, stream) + length;
	}
	//--------------------------------------
	// AuthToken EQUAL SecurityParmIndex COLON SequenceNum COLON AuthData
	//--------------------------------------
	int Authentication::writeTo(char* buffer, int size)
	{
		int written = std_snprintf(buffer, size, "Authentication=%08X:%08X:", m_securityParmIndex, m_sequenceNum);
		written += rtl::bytes_to_hex(buffer + written, size - written, m_authData, m_authDataLength);

		return written;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Authentication::setValues(uint32_t param_index, uint32_t seq, const uint8_t* data, int length)
	{
		m_securityParmIndex = param_index;
		m_sequenceNum = seq;
		m_authDataLength = length;

		if (length < MEGACO_AUTH_DATA_MIN || length > MEGACO_AUTH_DATA_MAX)
			return false;

		memcpy(m_authData, data, m_authDataLength);

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Authentication::setAuthData(const uint8_t* data, int length)
	{
		m_authDataLength = length;

		if (length < MEGACO_AUTH_DATA_MIN || length > MEGACO_AUTH_DATA_MAX)
			return false;

		memcpy(m_authData, data, m_authDataLength);

		return true;
	}
}
