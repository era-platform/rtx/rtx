/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_mid.h"

namespace h248
{
	//--------------------------------------
	//
	//--------------------------------------
	const in_addr MID::IPv4_Error = { INADDR_ANY };
	const in6_addr MID::IPv6_Error = { INADDR_ANY };
	//--------------------------------------
	//
	//--------------------------------------
	MID& MID::operator = (const MID& mid)
	{
		clear();

		m_type = mid.m_type;

		switch (m_type)
		{
		case IPv4:
			m_ipv4 = mid.m_ipv4;
			break;
		case IPv6:
			m_ipv6 = mid.m_ipv6;
			break;
		case Domain:
		case Device:
		case MTP:
			m_domain = rtl::strdup(mid.m_domain);
			break;
		default:
			break;
		}

		m_port = mid.m_port;

		return *this;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void MID::clear()
	{
		if (m_type >= Domain)
		{
			if (m_domain != nullptr)
				FREE(m_domain);
		}

		memset(&m_ipv6, 0, sizeof(m_ipv6));
		m_port = 0;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool MID::parse(const char* value)
	{
		/*	mId = ((domainAddress / domainName) [":" portNumber]) / mtpAddress / deviceName
		domainName = "<" (ALPHA / DIGIT) *63(ALPHA / DIGIT / "-" / ".") ">"
		domainAddress = "[" (IPv4address / IPv6address) "]"
		; [IETF RFC 2373] contains the definition of IPv6 addresses.
		IPv6address = hexpart[":" IPv4address]
		IPv4address = V4hex DOT V4hex DOT V4hex DOT V4hex
		V4hex = 1 * 3(DIGIT); "0".."255"
		; this production, while occurring in[IETF RFC 2373], is not referenced
		; IPv6prefix = hexpart SLASH 1 * 2DIGIT
		hexpart = hexseq "::"[hexseq] / "::"[hexseq] / hexseq
		hexseq = hex4 *(":" hex4)
		hex4 = 1 * 4HEXDIG
		portNumber = UINT16*/

		const char* port = nullptr;

		if (*value == '[')
		{
			// parsing ip_address_t
			port = strchr(value + 1, ']');

			if (port == nullptr)
			{
				// fatal error: string has no closing square bracket ']'
				return false;
			}

			const char* dot = rtl::strchr(value + 1, '.', PTR_DIFF(port, value) - 2);
			const char* colon = rtl::strchr(value + 1, ':', PTR_DIFF(port, value) - 2);

			if (colon != nullptr)
			{
				// ipv6
				m_type = IPv6;
			}
			else if (dot != nullptr)
			{
				// ipv4
				m_type = IPv4;
			}
			else
			{
				// fatal error: not internet address
				return false;
			}

			char buf[256];
			int len = PTR_DIFF(port, value) - 1;
			strncpy(buf, value + 1, len);
			buf[len] = 0;
			int ret;

			if (m_type == IPv6)
			{
				ret = inet_pton(AF_INET6, buf, &m_ipv6);
			}
			else if (m_type == IPv4)
			{
				ret = inet_pton(AF_INET, buf, &m_ipv4);
			}

			if (ret != 1)
			{
				return false;
			}
		}
		else if (*value == '<')
		{
			// parsing domain_address
			port = strchr(value + 1, '>');

			if (port == nullptr)
			{
				// fatal error
				return false;
			}

			m_type = Domain;
			m_domain = (char*)MALLOC(port - value);
			int copy_to = PTR_DIFF(port, value) - 1;
			strncpy(m_domain, value + 1, copy_to);
			m_domain[copy_to] = 0;
		}
		else if (std_strnicmp(value, "MTP", 3) == 0)
		{
			// MTP { 1234ABCD }
			m_type = MTP;

			const char* begin = strchr(value, '{');
			const char* end = strchr(value, '}');
			int len = PTR_DIFF(end, begin) - 1;
			m_domain = rtl::strdup(begin + 1, len);
			rtl::strtrim(m_domain, " \t");
		}
		else
		{
			// deviceName
			m_type = Device;
			m_domain = rtl::strdup(value);
		}

		if (port != nullptr && *port != 0 && *(port + 1) == ':')
		{
			m_port = (uint16_t)strtoul(port + 2, nullptr, 10);
		}

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	rtl::MemoryStream& MID::writeTo(rtl::MemoryStream& stream, MessageWriterParams* params)
	{
		switch (m_type)
		{
		case IPv4:
			stream << '[' << m_ipv4 << ']';
			if (m_port != 0)
			{
				stream << ':' << m_port;
			}
			break;
		case IPv6:
			stream << '[' << m_ipv6 << ']';
			if (m_port != 0)
			{
				stream << ':' << m_port;
			}
			break;
		case Domain:
			stream << '<' << m_domain << '>';
			if (m_port != 0)
			{
				stream << ':' << m_port;
			}
			break;
		case Device:
			stream << m_domain;
			break;
		case MTP:
			stream << "MTP {" << m_domain << '}';
			break;
		default:
			break;
		}


		if (params != nullptr && params->human_readable)
			stream << "\r\n";
		else
			stream << ' ';

		return stream;
	}
}
//--------------------------------------
