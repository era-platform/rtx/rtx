﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

namespace h248
{
	//--------------------------------------
	// предварительное описание
	//--------------------------------------
	class TransportReader;
	//--------------------------------------
	// объект сети для чтения
	//--------------------------------------
	class TransportObject
	{
		// переменные для используемые потоком читателем
		friend TransportReader;
		TransportObject* m_nextLink;

	protected:
		TransportReader* m_reader;
		volatile bool m_readable;

		char m_id[16];
		static volatile uint32_t m_idGen;
		net_socket_t* m_socketRef;

	protected:
		virtual void dataReady();
		static void generateId(char* buffer, const char* prefix);
		void setSocket(net_socket_t* sock) { m_socketRef = sock; }

	public:
		TransportObject();
		virtual ~TransportObject();

		void startReading(TransportReader* reader);
		void stopReading();

		const char* getId() { return m_id; }
	};
	//--------------------------------------
	// объект поток для слушателя сети
	//--------------------------------------
	class TransportReader : rtl::IThreadUser
	{
		rtl::MutexWatch m_sync;					// синхронизация

		volatile TransportObject* m_first;	// указатель на первый канал
		volatile TransportObject* m_last;	// указатель на последний канал
		volatile int m_count;				// количество каналов в списке

		rtl::ArrayT<net_socket_t*> m_readList;

		rtl::Thread m_thread;							// поток слушателя

		rtl::Event m_threadWakeUpEvent;				// событие на пробуждение потока
		rtl::Event m_threadStartedEvent;				// событие старта потока -- функция Start ждет пока поток не стартанет

		volatile bool m_threadEnter;				// флаг входа потока в селект

		uint32_t m_id;
		static volatile uint32_t m_idGen;

	public:
		TransportReader();
		virtual ~TransportReader();

		void stop();

		int getFreeSlots() { return 64 - m_count; }
		int getCount() { return m_count; }

		bool addObject(TransportObject* object);
		void removeObject(TransportObject* object);

	private:
		bool start();
		bool prepareSelection();
		void processReadSockets();

		virtual void thread_run(rtl::Thread* thread);
	};
}
//--------------------------------------
