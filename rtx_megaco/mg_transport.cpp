﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_transport_manager.h"
#include "mg_transport.h"

namespace h248
{
	//--------------------------------------
	//
	//--------------------------------------
#define MEGACO_PACKET_DEFAULT_SIZE	(4*1024)
#define MEGACO_PACKET_MAX_SIZE		(64 * 1024)
#define MEGACO_PACKET_RECALC_SIZE(v) (((v) & 0xFFFFFC00) + 1024)
/*
A TPKT consists of two parts:  a packet-header and a TPDU.  The
format of the header is constant regardless of the type of packet.
The format of the packet-header is as follows:

0                   1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|      vrsn     |    reserved   |          packet length        |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

where:

vrsn                         8 bits

This field is always 3 for the version of the protocol described in
this memo.

packet length                16 bits (min=7, max=65535)

This field contains the length of entire packet in octets,
including packet-header.  This permits a maximum TPDU size of
65531 octets.  Based on the size of the data transfer (DT) TPDU,
this permits a maximum TSDU size of 65524 octets.

The format of the TPDU is defined in [ISO8073].  Note that only
TPDUs formatted for transport class 0 are exchanged (different
transport classes may use slightly different formats).
*/
#pragma pack(push, 1)
	struct TPKT_header_t
	{
		uint8_t version;		// version current 3
		uint8_t reserved;		// must be 0
		uint16_t packet_length;	// packet length including header
	};
#pragma pack(pop)
	//--------------------------------------
	//
	//--------------------------------------
	Transport::Transport()
	{
		m_state = New;
		memset(&m_route, 0, sizeof m_route);

		rtl::res_counter_t::add_ref(g_transport_counter);
	}
	//--------------------------------------
	//
	//--------------------------------------
	Transport::~Transport()
	{
		// ...do nothing...
		rtl::res_counter_t::release(g_transport_counter);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Transport::destroy()
	{
		m_state = Disconnected;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Transport::raiseTransportFailure(int net_error, const char* function)
	{
		TransportManager::raiseAsyncEvent(TransportEvent_Failure, this, net_error);

		TransportObject::m_readable = false;

		destroy();
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Transport::raiseTransportDisconnected(const char* function)
	{
		if (m_state == Connected)
		{
			// событие пришло от сети первым
			m_state = Disconnecting;
			// глушим отправку
			//m_readable = false;
			disconnect();
		}
		else if (m_state == Disconnecting)
		{
			// событие пришло в ответ ничего не делаем
			m_state = Disconnected;
		}

		TransportObject::m_readable = false;

		TransportManager::raiseAsyncEvent(TransportEvent_Disconnected, this);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Transport::raiseParserError(const char* function)
	{
		TransportManager::raiseAsyncEvent(TransportEvent_Invalid, this);

		TransportObject::m_readable = false;

		destroy();
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Transport::raisePacketArrival(const uint8_t* packet, int length)
	{
		rtl::res_counter_t::add_ref(g_raw_packet_counter);
		RawPacket* raw_packet = (RawPacket*)MALLOC(sizeof(RawPacket) + length);

		raw_packet->ts_1 = rtl::DateTime::getTicks();
		raw_packet->route = m_route;
		raw_packet->length = length;
		memcpy(raw_packet->packet, packet, length);
		raw_packet->packet[length] = 0;

		TransportManager::raiseAsyncEvent(TransportEvent_PacketArrival, this, 0, raw_packet);
	}
	//--------------------------------------
	//
	//--------------------------------------
	TransportTCP::TransportTCP() :
		m_sync("mg-tcp-transport"),
		m_rxBufferSize(MEGACO_PACKET_DEFAULT_SIZE),
		m_rxBufferWritten(0),
		m_rxReadNewPacket(true)
	{
		// default = 4 Kb then resizing for bigger data
		m_rxBuffer = (uint8_t*)MALLOC(m_rxBufferSize);

		generateId(m_id, "tcp");
	}
	//--------------------------------------
	//
	//--------------------------------------
	TransportTCP::~TransportTCP()
	{
		// do not call disconnect
		//disconnect();

		if (m_rxBuffer != nullptr)
		{
			FREE(m_rxBuffer);

			m_rxBuffer = nullptr;
			m_rxBufferSize = 0;
			m_rxBufferWritten = 0;
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool TransportTCP::connect(const EndPoint& remote_address, int timeout)
	{
		if (m_state != New)
		{
			LOG_NET_ERROR(m_id, "connect : transport invalid state %s", State_toString(m_state));
			return false;
		}

		m_state = Connecting;

		LOG_NET(m_id, "connect to %s %s:%u", TransportType_toString(remote_address.type), inet_ntoa(remote_address.address), remote_address.port);

		if (remote_address.type != h248::TransportType::TCP)
		{
			m_state = Invalid;
			return false;
		}

		m_route.remoteAddress = remote_address.address;
		m_route.remotePort = remote_address.port;
		m_route.type = remote_address.type;

		m_key = MAKE_KEY64(m_route.remoteAddress, m_route.remotePort, m_route.type);

		sockaddr_in saddr = { 0 };
		sockaddrlen_t len = sizeof(saddr);

		saddr.sin_family = AF_INET;
		saddr.sin_addr = remote_address.address;
		saddr.sin_port = htons(remote_address.port);

		if (!m_socket.open(AF_INET, SOCK_STREAM, IPPROTO_TCP))
		{
			LOG_NET_ERROR(m_id, "mg_transport: TransportTCP::connect : socket.open() returns %d", std_get_error);
			m_state = Invalid;
			return false;
		}

		int bsize = 64 * 1024;

		m_socket.set_option(SOL_SOCKET, SO_RCVBUF, &bsize, sizeof(int));
		m_socket.set_option(SOL_SOCKET, SO_SNDBUF, &bsize, sizeof(int));

#if defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
		timeval tv = { timeout / 1000, (timeout % 1000) * 1000 };
		if (!m_socket.set_option(SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(tv)))
		{
			LOG_NET_WARN(m_id, "connect : socket.setsockopt(SO_SNDTIMEO) returns %d", std_get_error);
		}
#elif defined (TARGET_OS_WINDOWS)
		if (!m_socket.set_option(SOL_SOCKET, SO_SNDTIMEO, &timeout, sizeof(timeout)))
		{
			LOG_NET_WARN(m_id, "connect : socket.setsockopt(SO_SNDTIMEO) returns %d", std_get_error);
		}
#endif

		if (!m_socket.connect((sockaddr*)&saddr, len))
		{
			LOG_NET_ERROR(m_id, "connect : socket.connect() returns %d", std_get_error);
			m_socket.close();
			m_state = Invalid;
			return false;
		}

		if (!m_socket.get_interface((sockaddr*)&saddr, &len))
		{
			LOG_NET_WARN(m_id, "connect : socket.get_interface() returns %d", std_get_error);
			m_socket.close();
			m_state = Invalid;
			return false;
		}

		m_route.localAddress = saddr.sin_addr;
		m_route.localPort = ntohs(saddr.sin_port);

		m_socket.set_tag(this);

		LOG_NET(m_id, "connect : done");

		m_state = Connected;

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool TransportTCP::start()
	{
		if (m_state != Connected)
		{
			LOG_NET_ERROR(m_id, "start : transport invalid state %s", State_toString(m_state));
			return false;
		}

		LOG_NET(m_id, "start...");

		setSocket(&m_socket);

		bool result = TransportManager::startReading(this);

		LOG_NET(m_id, "start : %s", result ? "done" : "failed");

		return result;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransportTCP::disconnect()
	{
		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		if (m_state != Connected && m_state != Disconnecting)
		{
			LOG_NET_ERROR(m_id, "disconnect : transport invalid state %s", State_toString(m_state));
			return;
		}

		if (m_state == Connected)
		{
			// инициатива на нашей стороне
			LOG_NET(m_id, "disconnect : shutdown sending & waiting reply from peer");

			m_state = Disconnecting;

			if (m_socket.get_handle() != INVALID_SOCKET)
			{
				if (!m_socket.shutdown(SD_SEND))
				{
					destroy();
				}
			}

			LOG_NET(m_id, "disconnect : done");
		}
		else
		{
			// реакция на shutdown из сети

			LOG_NET(m_id, "disconnect : shutdown sending && waiting close");

			if (m_socket.get_handle() != INVALID_SOCKET)
			{
				if (!m_socket.shutdown(SD_SEND))
				{
					destroy();
				}
				m_state = Disconnected;
			}

			LOG_NET(m_id, "disconnecting : done");
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	int TransportTCP::sendMessage(const void* message, int length)
	{
		if (m_state != Connected)
		{
			LOG_NET_ERROR(m_id, "send message : transport invalid state %s", State_toString(m_state));
			return 0;
		}

		LOG_NET(m_id, "send message...");

		//rtl::MutexLock lock(m_sync);
		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		if (length + sizeof(TPKT_header_t) > MEGACO_PACKET_MAX_SIZE)
		{
			LOG_NET(m_id, "send message : failed : packet size too long (%d)", length + sizeof(TPKT_header_t));
			return 0;
		}

		int to_send = length + sizeof(TPKT_header_t);

		uint8_t buffer[1024 * 32];

		TPKT_header_t* header = (TPKT_header_t*)buffer;

		header->version = 0x03;
		header->reserved = 0x00;
		header->packet_length = htons((uint16_t)to_send);

		memcpy(buffer + sizeof(TPKT_header_t), message, length);

		int sent = m_socket.send(buffer, to_send);
		//sent += m_socket.send(message, length);

		LOG_NET(m_id, "send message : sent %d bytes", sent);

		return sent - sizeof(TPKT_header_t);
	}
	//--------------------------------------
	// internal interface
	//--------------------------------------
	bool TransportTCP::accept(SOCKET socket, const Route& route)
	{
		if (m_state != New)
		{
			LOG_NET_ERROR(m_id, "accept : transport invalid state %s", State_toString(m_state));
			return false;
		}

		LOG_NET(m_id, "accept connection from %s %s:%u...", TransportType_toString(route.type), inet_ntoa(route.remoteAddress), route.remotePort);

		m_socket = socket;
		m_route = route;

		m_socket.set_tag(this);

		int bsize = 64 * 1024;

		m_socket.set_option(SOL_SOCKET, SO_RCVBUF, &bsize, sizeof(int));
		m_socket.set_option(SOL_SOCKET, SO_SNDBUF, &bsize, sizeof(int));

		m_key = MAKE_KEY64(m_route.remoteAddress, m_route.remotePort, m_route.type);

		LOG_NET(m_id, "accept connection : done");

		m_state = Connected;

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransportTCP::destroy()
	{
		LOG_NET(m_id, "destroy : state(%s)...", State_toString(m_state));

		rtl::MutexWatchLock lock(m_sync, __FUNCTION__);

		if (m_socket.get_handle() != INVALID_SOCKET)
		{
			LOG_NET(m_id, "destroy : stop reading...");

			stopReading();

			LOG_NET(m_id, "destroy : close socket...");

			m_socket.close();
		}

		LOG_NET(m_id, "destroy : done");
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransportTCP::dataReady()
	{
		int amount;
		int socket_result = m_socket.io_control(FIONREAD, (uint32_t*)&amount);

		if (socket_result == SOCKET_ERROR)
		{
			LOG_NET_ERROR(m_id, "data ready : socket.io_control returns %d", std_get_error);
			raiseTransportFailure(socket_result, "ioctlsocket");
			return;
		}

		if (amount == 0)
		{
			LOG_NET(m_id, "data ready : socket.io_control() returns 0 bytes. receiving shutdowned");

			raiseTransportDisconnected("ioctlsocket");

			return;
		}

		LOG_NET(m_id, "data ready : READING DATA %d bytes. rx_ptr:%d rx_size:%d", amount, m_rxBufferWritten, m_rxBufferSize);

		while (amount > 0)
		{
			int to_read = MIN(amount, MEGACO_PACKET_MAX_SIZE);

			if (!prepareBufferRX(to_read))
			{
				raiseTransportFailure(-1, "prepare_rx_buffer");
				LOG_NET_ERROR(m_id, "data ready : memory preparing exception!");
				return;
			}

			socket_result = m_socket.receive(m_rxBuffer + m_rxBufferWritten, to_read);

			if (socket_result == SOCKET_ERROR)
			{
				raiseTransportFailure(socket_result, "recv");
				LOG_NET_ERROR(m_id, "data ready : socket.receive() returns %d", std_get_error);
				break;
			}
			else if (socket_result == 0)
			{
				LOG_NET(m_id, "data ready : socket.receive() returns 0 bytes. receiving shutdowned");
				raiseTransportDisconnected("recv");
				break;
			}
			else
			{
				m_rxBufferWritten += socket_result;

				readPackets();
			}

			amount -= socket_result;
		}
	}
	//--------------------------------------
	// проверяем размер буфера и если нужно то изменяем размер
	// ошибка если размер читаемых данных больше максимума
	//--------------------------------------
	bool TransportTCP::prepareBufferRX(int length)
	{
		if (m_rxBufferWritten + length <= m_rxBufferSize)
			return true;

		if (m_rxBufferWritten + length > MEGACO_PACKET_MAX_SIZE)
			return false;

		int new_size = MEGACO_PACKET_RECALC_SIZE(m_rxBufferWritten + length);
		uint8_t* new_buffer = (uint8_t*)MALLOC(new_size);

		if (m_rxBufferWritten > 0)
		{
			memcpy(new_buffer, m_rxBuffer, m_rxBufferWritten);
		}

		FREE(m_rxBuffer);

		m_rxBuffer = new_buffer;
		m_rxBufferSize = new_size;

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransportTCP::readPackets()
	{
		int read_ptr = 0;

		while (read_ptr < m_rxBufferWritten)
		{
			uint8_t* ptr = m_rxBuffer + read_ptr;
			TPKT_header_t* header = (TPKT_header_t*)ptr;

			if (header->version != 3)
			{
				raiseParserError("TPKT_header");
				LOG_NET_WARN(m_id, "read packets : TPKT header version not equal to 3");
				return;
			}

			int packet_length = ntohs(header->packet_length);

			if (packet_length <= m_rxBufferWritten - read_ptr)
			{
				// полный пакет!
				//LOG_NET(m_id, "megaco packet read --> raise event");
				raisePacketArrival(ptr + sizeof(TPKT_header_t), packet_length - sizeof(TPKT_header_t));
				read_ptr += packet_length;
			}
			else
			{
				// недочитанный пакет
				// сдвигаем данные в начало
				if (read_ptr > 0)
				{
					int mean = m_rxBufferWritten - read_ptr;
					memmove(m_rxBuffer, m_rxBuffer + read_ptr, mean);
					m_rxBufferWritten = mean;
				}

				return;
			}
		}

		// данные полностью считанны -- но проверку поставим
		if (read_ptr == m_rxBufferWritten)
		{
			m_rxBufferWritten = 0;
		}
		else
		{
			// alarm
			LOG_NET_WARN(m_id, "read packets : data not processed but exit condition triggered!");
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	TransportUDP::TransportUDP() : m_socket_ptr(nullptr)
	{
		generateId(m_id, "udp");
	}
	//--------------------------------------
	//
	//--------------------------------------
	TransportUDP::~TransportUDP()
	{
		// ...do nothing...
		// транспорт буддет удален из юдп листенера менеджером
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool TransportUDP::connect(const EndPoint& remote_address, int timeout)
	{
		if (m_state != New)
		{
			LOG_NET_ERROR(m_id, "connect : transport invalid state %s", State_toString(m_state));
			return false;
		}

		LOG_NET(m_id, "connect to %s %s:%u...", TransportType_toString(remote_address.type), inet_ntoa(remote_address.address), remote_address.port);

		EndPoint local_point;

		if (!TransportManager::getBestInterface(remote_address.address, local_point.address))
		{
			local_point.address = TransportManager::getDefaultInterface();
		}

		local_point.type = h248::TransportType::UDP;
		local_point.port = 0;

		bool result = TransportManager::attachTransportToListenerUDP(local_point, this);

		m_state = result ? Connected : Invalid;

		LOG_NET(m_id, "connect : %s", result ? "done" : "failed");

		return result;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool TransportUDP::start()
	{
		if (m_state != Connected)
		{
			LOG_NET_ERROR(m_id, "start : transport invalid state %s", State_toString(m_state));
			return false;
		}

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransportUDP::disconnect()
	{
		if (m_state != Connected)
		{
			LOG_NET_ERROR(m_id, "disconnect : transport invalid state %s", State_toString(m_state));
			return;
		}

		LOG_NET(m_id, "disconnect...");
		// всегда наша инициатива!
		// транспорт буддет удален из юдп листенера менеджером

		TransportManager::detachTransportFromListenerUDP(this);

		m_state = Disconnected;

		LOG_NET(m_id, "disconnect : done");
	}
	//--------------------------------------
	//
	//--------------------------------------
	int TransportUDP::sendMessage(const void* message, int length)
	{
		if (m_state != New)
		{
			LOG_NET_ERROR(m_id, "send message : transport invalid state %s", State_toString(m_state));
			return false;
		}

		LOG_NET(m_id, "send message...");

		int sent = 0;

		if (m_socket_ptr != nullptr)
		{
			sent = m_socket_ptr->send_to(message, length, m_route.remoteAddress, m_route.remotePort);

			LOG_NET(m_id, "send message : sent %d bytes", sent);
		}
		else
		{
			LOG_NET_WARN(m_id, "send message : failed : socket not ready");
		}

		return sent;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransportUDP::destroy()
	{
		if (m_state == Disconnected)
		{
			return;
		}

		if (m_state != Connected)
		{
			LOG_NET_ERROR(m_id, "destroy : transport invalid state %s", State_toString(m_state));
			return;
		}

		LOG_NET(m_id, "destroy...");

		// транспорт буддет удален из юдп листенера менеджером
		TransportManager::detachTransportFromListenerUDP(this);

		m_state = Disconnected;

		LOG_NET(m_id, "destroy : done");
	}
	//--------------------------------------
	// internal interface
	//--------------------------------------
	bool TransportUDP::accept(net_socket_t* socket, const Route& route)
	{
		LOG_NET(m_id, "accept connection from %s %s:%u", TransportType_toString(route.type), inet_ntoa(route.remoteAddress), route.remotePort);

		if (m_socket_ptr != nullptr)
		{
			// already accepted!
			LOG_NET_WARN(m_id, "accept connection : failed : transport already accepted");
			return false;
		}

		m_socket_ptr = socket;
		m_route = route;

		m_key = MAKE_KEY64(m_route.remoteAddress, m_route.remotePort, m_route.type);

		LOG_NET_WARN(m_id, "accept connection : done");

		m_state = Connected;

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransportUDP::rxPacket(const uint8_t* packet, int packet_length, const EndPoint& remoteAddress)
	{
		raisePacketArrival(packet, packet_length);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransportUDP::rxError(int net_error, const EndPoint& remoteAddress)
	{
		raiseTransportFailure(net_error, "recv");
	}
	//--------------------------------------
	//
	//--------------------------------------
	const char* Transport::State_toString(State state)
	{
		static const char* names[] = {
			"New",
			"Connecting",
			"Connected",
			"Disconnecting",
			"Disconnected",
		};

		return state >= New && state <= Disconnected ? names[state] : "Invalid";
	}
}
//--------------------------------------
