﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtx_megaco.h"

//--------------------------------------
//
//--------------------------------------
#if defined (DEBUG_MEMORY)
void* operator new (size_t size, char* file, int line)
{
	return std_mem_checker_malloc(size, file, line, rc_malloc_new_e);
}
void* operator new[](size_t size, char* file, int line)
{
	return std_mem_checker_malloc(size, file, line, rc_malloc_newar_e);
}
void operator delete (void* mem_ptr)
{
	std_mem_checker_free(mem_ptr, rc_malloc_delete_e);
}
void operator delete[](void* mem_ptr)
{
	std_mem_checker_free(mem_ptr, rc_malloc_deletear_e);
}
#endif

namespace h248
{
	//--------------------------------------
	//
	//--------------------------------------
	int g_transport_counter = -1;
	int g_transaction_counter = -1;
	int g_message_counter = -1;
	int g_raw_packet_counter = -1;
	//--------------------------------------
	//
	//--------------------------------------
	RTX_MG_API rtl::Logger LogProto;
	static MessageWriterParams g_write_params;
	rtl::async_call_manager_t MegacoAsyncCaller;
	//--------------------------------------
	//
	//--------------------------------------
	void MegacoAsyncCall(rtl::async_call_target_t* target, uint16_t callId, intptr_t intParam, void* ptrParam)
	{
		MegacoAsyncCaller.call(target, callId, intParam, ptrParam);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void MegacoAsyncCall(rtl::pfn_async_callback_t callback, void* user, uint16_t callId, intptr_t intParam, void* ptrParam)
	{
		MegacoAsyncCaller.call(callback, user, callId, intParam, ptrParam);
	}
}

//--------------------------------------
//
//--------------------------------------
RTX_MG_API uint32_t megaco_get_version()
{
	return (API_VERSION_MAJOR << 24) | (API_VERSION_MINOR << 16) | (API_VERSION_TEST << 8) | API_VERSION_BUILD;
}
//--------------------------------------
//
//--------------------------------------
RTX_MG_API void megaco_set_output_mode(const h248::MessageWriterParams* params)
{
	if (params != nullptr)
	{
		h248::g_write_params = *params;
	}
}
//--------------------------------------
//
//--------------------------------------
RTX_MG_API void megaco_get_output_mode(h248::MessageWriterParams* params)
{
	if (params != nullptr)
	{
		*params = h248::g_write_params;
	}
}
//--------------------------------------
// регистрация объектов
//--------------------------------------
RTX_MG_API void megaco_register_counters()
{
	h248::g_transport_counter = rtl::res_counter_t::add("H248_Transport");
	h248::g_transaction_counter = rtl::res_counter_t::add("H248_Transaction");
	h248::g_message_counter = rtl::res_counter_t::add("H248_Message");
	h248::g_raw_packet_counter = rtl::res_counter_t::add("H248_Raw_Packet");
}

//--------------------------------------
