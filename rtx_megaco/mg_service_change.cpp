/* RTX H.248 Media Gate
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_service_change.h"

namespace h248
{
	//--------------------------------------
	//
	//--------------------------------------
	ServiceChangeDescriptor::ServiceChangeDescriptor() :
		m_method(Token::_Last), m_reasonCode(Reason::_Error), m_delay(BAD_INDEX),
		m_midType(Token::_Last), m_port(0), m_timestamp(0), m_version(0), m_SIC(false)
	{
	}
	//--------------------------------------
	//
	//--------------------------------------
	ServiceChangeDescriptor::~ServiceChangeDescriptor()
	{
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool ServiceChangeDescriptor::read(const Field& services_descriptor)
	{
		for (int i = 0; i < services_descriptor.getFieldCount(); i++)
		{
			const Field* param = services_descriptor.getFieldAt(i);

			switch (param->getType())
			{
			case Token::Method:
				m_method = Token_parse(param->getValue());
				break;
			case Token::Reason:
				m_reasonCode = Reason_parse(param->getValue(), m_reasonText);

				if (m_reasonCode == Reason::_Error)
					return false;

				break;
			case Token::Delay:
			{
				const char* value = param->getValue();
				if (value != nullptr && value[0] != 0)
				{
					m_delay = strtoul(value, nullptr, 10);
				}
			}
			break;
			case Token::ServiceChangeAddress:
				// ��������� ������ ��� �������� ����������? ������ ���� ������ ����
				// ������ ������ ����
				if (m_midType != Token::_Error)
				{
					// ������
					return false;
				}
				if (!parse_mid(param->getValue(), Token::ServiceChangeAddress))
				{
					return false;
				}

				break;
			case Token::Profile:
				// �������������� mgc �������
				// ��� �����������!
				m_profile = param->getValue();
				break;
			case Token::Version:
				// ������ ��������� ��� ����� mgc
				m_version = atoi(param->getValue());
				break;
			case Token::MgcIdToTry:
				// ��������������� �����������
				// ������� ����� ���������� 
				if (m_midType != Token::_Error)
				{
					// ������
					return false;
				}

				if (!parse_mid(param->getValue(), Token::MgcIdToTry))
				{
					return false;
				}

				break;
			case Token::ServiceChangeInc:
				// �� �������� �� MG � MGC 
				m_SIC = true;
				break;
			default:
			{
				//case mg_token_Timestamp_e:
				const char* ts = param->getName();

				if (ts == nullptr || !isdigit(ts[0]) || !parse_timestamp(ts))
				{
					return false;
				}
			}
			break;
			}
		}

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool ServiceChangeDescriptor::parse_mid(const char* mid, Token type)
	{
		if (isdigit(mid[0]))
		{
			// portNumber!
			m_mid.clear();
			m_port = (uint16_t)strtoul(mid, nullptr, 10);
			return true;
		}

		m_port = 0;
		bool result = m_mid.parse(mid);
		m_midType = result ? type : Token::_Error;

		return result;
	}
	//--------------------------------------
	// yyyymmddThhmmssss
	//--------------------------------------
	bool ServiceChangeDescriptor::parse_timestamp(const char* timestamp)
	{
		if (timestamp == nullptr || timestamp[0] == 0)
			return false;

		int length = std_strlen(timestamp);

		if (length != 17 || timestamp[8] != 'T')
		{
			return false;
		}

		uint8_t date[8];
		uint8_t time[8];

		for (int i = 0; i < 8; i++)
		{
			char ch = timestamp[i];
			if (!isdigit(ch))
				return false;

			date[i] = ch - '0';
		}

		for (int i = 0; i < 8; i++)
		{
			char ch = timestamp[i + 9];
			if (!isdigit(ch))
				return false;

			time[i] = ch - '0';
		}

		m_timestamp.setYear(date[0] * 1000 + date[1] * 100 + date[2] * 10 + date[3]);
		m_timestamp.setMonth(date[4] * 10 + date[5]);
		m_timestamp.setDay(date[6] * 10 + date[7]);
		m_timestamp.setHour(time[0] * 10 + time[1]);
		m_timestamp.setMinute(time[2] * 10 + time[3]);
		m_timestamp.getSecond(time[4] * 10 + time[5]);
		m_timestamp.setMilliseconds(time[6] * 100 + time[7] * 10);

		return true;
	}
}
//--------------------------------------
