﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_message_node.h"

namespace h248
{
	static void writeMargin(rtl::MemoryStream& stream, MessageWriterParams* params);

	//--------------------------------------
	//
	//--------------------------------------
	Field::Field() :
		m_type(Token::_Last),
		m_relation(Relation::Assignment),
		m_hasRawData(false),
		m_parent(nullptr)
	{
	}
	//--------------------------------------
	//
	//--------------------------------------
	Field::Field(const char* name, const char* value) :
		m_type(Token::_Custom),
		m_name(name),
		m_relation(Relation::Assignment),
		m_value(value),
		m_hasRawData(false),
		m_parent(nullptr)
	{
	}
	//--------------------------------------
	//
	//--------------------------------------
	Field::Field(const char* name, uint32_t value) :
		m_type(Token::_Custom),
		m_name(name),
		m_relation(Relation::Assignment),
		m_hasRawData(false),
		m_parent(nullptr)
	{
		setValue(value);
	}
	//--------------------------------------
	//
	//--------------------------------------
	Field::Field(Token type, const char* value) :
		m_type(Token::_Last),
		m_relation(Relation::Assignment),
		m_value(value),
		m_hasRawData(false),
		m_parent(nullptr)
	{
		setType(type);
	}
	//--------------------------------------
	//
	//--------------------------------------
	Field::Field(Token type, uint32_t value) :
		m_type(Token::_Last),
		m_relation(Relation::Assignment),
		m_hasRawData(false),
		m_parent(nullptr)
	{
		setType(type);
		setValue(value);
	}
	//--------------------------------------
	//
	//--------------------------------------
	Field::Field(const Field& node) :
		m_type(Token::_Last),
		m_relation(Relation::Assignment),
		m_hasRawData(false),
		m_parent(nullptr)
	{
		*this = node;
	}
	//--------------------------------------
	//
	//--------------------------------------
	Field::~Field()
	{
		cleanup();
	}
	//--------------------------------------
	//
	//--------------------------------------
	Field& Field::operator = (const Field& node)
	{
		cleanup();

		m_type = node.m_type;
		m_name = node.m_name;
		m_relation = node.m_relation;
		m_value = node.m_value;
		m_hasRawData = node.m_hasRawData;
		m_rawData = node.m_rawData;

		// парента не копируем!

		for (int i = 0; i < node.m_subList.getCount(); i++)
		{
			//Field* node1 = (Field*)&node;
			//Field** base1 = node1->m_subList.getBase();
			//const Field* const* base = node.m_subList.getBase();

			const Field* orig_node = node.m_subList.getAt(i);
			Field* new_node = NEW Field(*orig_node);

			if (m_subList.add(new_node) == BAD_INDEX)
			{
				DELETEO(new_node);
			}
			else
			{
				new_node->m_parent = this;
			}
		}

		return *this;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool Field::read(Parser& parser)
	{
		setName(parser.getTokenName());
		m_relation = parser.getTokenRelation();
		setValue(parser.getTokenValue());
		m_type = Token_parse(m_name);

		if (m_type == Token::Local || m_type == Token::Remote || m_type == Token::Error)
		{
			m_hasRawData = parser.readTokenGroup() == ParserState::EndGroup;

			if (m_hasRawData)
			{
				setRawData(parser.getTokenValue());
			}
			return true;
		}

		if (parser.hasGroup())
		{
			// читаем ветку!

			while (parser.readNext() == ParserState::Token)
			{
				Field* node = NEW Field();

				if (node->read(parser))
				{
					node->m_parent = this;
					m_subList.add(node);
				}
				else
				{
					DELETEO(node);
					return false;
				}
			}

			return parser.getState() == ParserState::EndGroup;
		}

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Field::cleanup()
	{
		m_name.setEmpty();
		m_value.setEmpty();
		m_rawData.setEmpty();

		int count = m_subList.getCount();

		if (count > 0)
		{
			for (int i = 0; i < count; i++)
			{
				DELETEO(m_subList[i]);
			}

			m_subList.clear();
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	static void write_sdp(rtl::MemoryStream& stream, const char* sdp_text)
	{
		const char* begin = sdp_text;
		const char* end = strchr(begin, '}');

		while (end != nullptr)
		{
			stream.write(begin, end - begin);
			stream << '\\';
			begin = end;
			end = strchr(begin + 1, '}');
		}

		stream << begin;
	}
	//--------------------------------------
	//
	//--------------------------------------
	rtl::MemoryStream& Field::writeTo(rtl::MemoryStream& stream, MessageWriterParams* params) const
	{
		writeMargin(stream, params);

		bool human_readable = params != nullptr && params->human_readable;

		if (m_type == Token::_Custom)
			stream << m_name;
		else
			stream << Token_toString(m_type);

		if (!m_value.isEmpty())
		{
			if (human_readable)
				stream << ' ' << (char)m_relation << ' ' << m_value;
			else
				stream << (char)m_relation << m_value;
		}

		if (m_hasRawData)
		{
			if (human_readable)
			{
				if (m_type != Token::Error)
				{
					stream << " {\r\n";
					//write_margin(stream, params);
					write_sdp(stream, m_rawData);
					stream << "\r\n";
					writeMargin(stream, params);
					stream << '}';
				}
				else
				{
					stream << " { \"" << m_rawData << "\" }";
				}
			}
			else
			{
				if (m_type != Token::Error)
				{
					stream << '{';
					write_sdp(stream, m_rawData);
					stream << '}';
				}
				else
				{
					stream << "{\"" << m_rawData << "\"}";
				}
			}

			return stream;
		}
		else
		{
			int count = m_subList.getCount();

			if (count > 0)
			{
				if (human_readable)
				{
					stream << " {\r\n";
					params->level++;
					for (int i = 0; i < count; i++)
					{
						m_subList[i]->writeTo(stream, params);

						if (i < count - 1)
							stream << ',';

						stream << "\r\n";

					}
					params->level--;
				}
				else
				{
					stream << '{';
					for (int i = 0; i < count; i++)
					{
						m_subList[i]->writeTo(stream, params);

						if (i < count - 1)
							stream << ',';
					}
				}

				writeMargin(stream, params);
				stream << '}';
			}
		}
		return stream;
	}
	//--------------------------------------
	//
	//--------------------------------------
	Field* Field::addField(const Field* node)
	{
		if (node == nullptr)
			return nullptr;

		Field* new_node = NEW Field(*node);

		int idx = m_subList.add(new_node);

		if (idx == BAD_INDEX)
		{
			DELETEO(new_node);
			new_node = nullptr;
		}

		new_node->m_parent = this;

		return new_node;
	}
	//--------------------------------------
	//
	//--------------------------------------
	Field* Field::addField(Field* node, bool attach)
	{
		if (node == nullptr)
		{
			return nullptr;
		}

		Field* added = attach ? node : NEW Field(*node);

		int idx = m_subList.add(added);

		if (idx == BAD_INDEX)
		{
			if (!attach)
			{
				DELETEO(added);
			}

			added = nullptr;
		}
		else
		{
			added->m_parent = this;
		}

		return added;
	}
	//--------------------------------------
	//
	//--------------------------------------
	Field* Field::addField(const char* name, const char* value)
	{
		Field* new_node = NEW Field(name, value);

		int idx = m_subList.add(new_node);

		if (idx == BAD_INDEX)
		{
			DELETEO(new_node);
			new_node = nullptr;
		}
		else
		{
			new_node->m_parent = this;
		}

		return new_node;
	}
	//--------------------------------------
	//
	//--------------------------------------
	Field* Field::addField(const char* name, uint32_t value)
	{
		Field* new_node = NEW Field(name, value);

		int idx = m_subList.add(new_node);

		if (idx == BAD_INDEX)
		{
			DELETEO(new_node);
			new_node = nullptr;
		}
		else
		{
			new_node->m_parent = this;
		}

		return new_node;
	}
	//--------------------------------------
	//
	//--------------------------------------
	Field* Field::addField(Token type, const char* value)
	{
		Field* new_node = NEW Field(type, value);

		int idx = m_subList.add(new_node);

		if (idx == BAD_INDEX)
		{
			DELETEO(new_node);
			new_node = nullptr;
		}
		else
		{
			new_node->m_parent = this;
		}

		return new_node;
	}
	//--------------------------------------
	//
	//--------------------------------------
	Field* Field::addField(Token type, uint32_t value)
	{
		Field* new_node = NEW Field(type, value);

		int idx = m_subList.add(new_node);

		if (idx == BAD_INDEX)
		{
			DELETEO(new_node);
			new_node = nullptr;
		}
		else
		{
			new_node->m_parent = this;
		}

		return new_node;
	}
	//--------------------------------------
	//
	//--------------------------------------
	Field* Field::addErrorDescriptor(int error_code, const char* error_reason)
	{
		Field* new_node = NEW Field(Token::Error);

		// Erlang h.248 implementation does not catch ErrorDescription placed in message tree.
		// It catches only message errors!

		if (m_parent != nullptr)
		{
			return m_parent->addErrorDescriptor(error_code, error_reason);
		}

		removeAllFields();

		int idx = m_subList.add(new_node);

		if (idx == BAD_INDEX)
		{
			DELETEO(new_node);
			new_node = nullptr;
		}
		else
		{
			new_node->m_parent = this;
			new_node->setValue(error_code);
			const char* text = error_reason != nullptr && error_reason[0] != 0 ? error_reason : ErrorCode_toString((ErrorCode)error_code);
			new_node->setRawData(text, true);
		}

		return new_node;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Field::remove(const char* path)
	{

		Field* node = findField(path);

		if (node != nullptr)
		{
			Field* parent = node->getParent();

			if (parent == nullptr)
				return;

			if (parent == this)
			{
				removeField(node);
			}
			else
			{
				parent->removeField(node);
			}
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Field::removeField(const char* name)
	{
		for (int i = 0; i < m_subList.getCount(); i++)
		{
			Field* it = m_subList[i];

			if (std_stricmp(name, it->getName()) == 0)
			{
				m_subList.removeAt(i);
				DELETEO(it);
				return;
			}
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Field::removeField(Token type)
	{
		for (int i = 0; i < m_subList.getCount(); i++)
		{
			Field* it = m_subList[i];

			if (type == it->getType())
			{
				m_subList.removeAt(i);
				DELETEO(it);
				return;
			}
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Field::removeField(Field* node)
	{
		for (int i = 0; i < m_subList.getCount(); i++)
		{
			Field* it = m_subList[i];

			if (node == it)
			{
				m_subList.removeAt(i);
				DELETEO(it);
				return;
			}
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Field::removeFieldAt(int index)
	{
		Field* node = m_subList.getAt(index);

		if (node != nullptr)
		{
			node->cleanup();
			DELETEO(node);
			m_subList.removeAt(index);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Field::removeAllFields()
	{
		for (int i = 0; i < m_subList.getCount(); i++)
		{
			Field* node = m_subList.getAt(i);
			if (node != nullptr)
			{
				node->cleanup();
				DELETEO(node);
			}
		}

		m_subList.clear();
	}
	//--------------------------------------
	//
	//--------------------------------------
	const Field* Field::getField(const char* name, int length) const	
	{
		if (length == -1)
		{
			length = std_strlen(name);
		}

		for (int i = 0; i < m_subList.getCount(); i++)
		{
			const Field* node = m_subList.getAt(i);
			if (std_strnicmp(node->getName(), name, length) == 0)
				return node;
		}

		return nullptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	Field* Field::getField(const char* name, int length)
	{
		if (length == -1)
		{
			length = std_strlen(name);
		}

		for (int i = 0; i < m_subList.getCount(); i++)
		{
			Field* node = m_subList.getAt(i);
			if (std_strnicmp(node->getName(), name, length) == 0)
				return node;
		}

		return nullptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	const Field* Field::getField(Token type) const
	{
		for (int i = 0; i < m_subList.getCount(); i++)
		{
			const Field* node = m_subList.getAt(i);
			if (node->getType() == type)
				return node;
		}

		return nullptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	Field* Field::getField(Token type)
	{
		for (int i = 0; i < m_subList.getCount(); i++)
		{
			Field* node = m_subList.getAt(i);
			if (node->getType() == type)
				return node;
		}

		return nullptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Field::setName(const char* name)
	{
		m_name.setEmpty();
		m_type = Token::_Last;

		if (name != nullptr && name[0] != 0)
		{
			m_name = name;
			m_type = Token_parse(m_name);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Field::setType(Token type)
	{
		m_type = type;
		m_name = Token_toString(m_type);
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Field::setValue(const char* value)
	{
		m_value = value;
		m_value.trim(" \t");
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Field::setValue(uint32_t value)
	{
		m_value.setEmpty();
		m_value << value;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void Field::setRawData(const char* value, bool quoted)
	{
		m_rawData.setEmpty();

		if (value != nullptr && value[0] != 0)
		{
			m_rawData = value;
			m_rawData.replace("\\}", "}");
		}

		m_hasRawData = !m_rawData.isEmpty();
	}
	//--------------------------------------
	// поиск нужной ноды по имени/типу -> "node1/node2/node3"
	//--------------------------------------
	const Field* Field::findField(const char* path) const
	{
		const Field* parent = this;
		const Field* node = nullptr;

		const char* token = path;
		const char* next = strchr(path, '\\');

		if (next == nullptr)
		{
			Token type = Token_parse(path);
			return type == Token::_Custom ? getField(path) : getField(type);
		}

		do
		{
			int len = PTR_DIFF(next, token);
			Token type = Token_parse(token, len);
			node = type == Token::_Custom ? parent->getField(token, len) : parent->getField(type);

			if (node == nullptr)
				return nullptr;

			parent = node;

			token = next + 1;
			next = strchr(token, '\\');
		} while (next != nullptr);

		Token type = Token_parse(token);

		return type == Token::_Custom ? parent->getField(token) : parent->getField(type);
	}
	//--------------------------------------
	// поиск нужной ноды по имени/типу -> "node1/node2/node3"
	//--------------------------------------
	Field* Field::findField(const char* path)
	{
		Field* parent = this;
		Field* node = nullptr;

		const char* token = path;
		const char* next = strchr(path, '\\');

		if (next == nullptr)
		{
			Token type = Token_parse(path);
			return type == Token::_Custom ? getField(path) : getField(type);
		}

		do
		{
			int len = PTR_DIFF(next, token);
			Token type = Token_parse(token, len);
			node = type == Token::_Custom ? parent->getField(token, len) : parent->getField(type);

			if (node == nullptr)
				return nullptr;

			parent = node;

			token = next + 1;
			next = strchr(token, '\\');
		} while (next != nullptr);

		Token type = Token_parse(token);

		return type == Token::_Custom ? parent->getField(token) : parent->getField(type);
	}

	//--------------------------------------
	//
	//--------------------------------------
	static void writeMargin(rtl::MemoryStream& stream, MessageWriterParams* params)
	{
		if (params == nullptr || !params->human_readable)
			return;

		int count = 0;

		char spaces[9] = "        ";

		if (!params->use_tab)
		{
			count = MIN(params->spaces_count, 8);
			spaces[count] = 0;
		}

		for (int i = 0; i < params->level; i++)
		{
			if (params->use_tab)
			{
				stream << '\t';
			}
			else
			{
				stream << spaces;
			}
		}
	}

}
//--------------------------------------
