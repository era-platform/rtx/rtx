﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_transport_manager.h"
#include "mg_transport_listener.h"
#include "mg_transport.h"
#include "net_sock.h"

namespace h248
{
	//--------------------------------------
	//
	//--------------------------------------
	TransportListener::TransportListener()
	{
		memset(&m_interface, 0, sizeof(m_interface));
	}
	//--------------------------------------
	//
	//--------------------------------------
	TransportListener::~TransportListener()
	{
		// ...do nothing...
	}
	//--------------------------------------
	//
	//--------------------------------------
	TransportListenerTCP::TransportListenerTCP()
	{
		generateId(m_id, "tcp-l");
	}
	//--------------------------------------
	//
	//--------------------------------------
	TransportListenerTCP::~TransportListenerTCP()
	{
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool TransportListenerTCP::create(const EndPoint& point)
	{
		LOG_NET(m_id, "create on %s:%u...", inet_ntoa(point.address), point.port);

		m_interface = point;

		if (!m_socket.open(AF_INET, SOCK_STREAM, IPPROTO_TCP))
		{
			int last_error = std_get_error;
			LOG_NET_ERROR(m_id, "mg_transport_listener: TransportListenerTCP::create : failed : socket.open() returns %d", last_error);
			return false;
		}

		sockaddr_in addr;
		memset(&addr, 0, sizeof addr);
		addr.sin_family = AF_INET;
		addr.sin_addr = m_interface.address;
		addr.sin_port = htons(m_interface.port);

		bool result = m_socket.bind((sockaddr*)&addr, sizeof addr);

		if (!result)
		{
			int last_error = std_get_error;
			LOG_NET_ERROR(m_id, "create : failed : socket.bind() returns %d", last_error);
			return false;
		}

		m_socket.set_tag(this);

		int bsize = 64 * 1024;
		m_socket.set_option(SOL_SOCKET, SO_RCVBUF, &bsize, sizeof(int));
		m_socket.set_option(SOL_SOCKET, SO_SNDBUF, &bsize, sizeof(int));

		LOG_NET(m_id, "create : done");

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransportListenerTCP::destroy()
	{
		if (m_socket.get_handle() != INVALID_SOCKET)
		{
			LOG_NET(m_id, "destroy : closing socket");

			stopReading();
			m_socket.close();

			LOG_NET(m_id, "destroy : socket closed");
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool TransportListenerTCP::startListen()
	{
		LOG_NET(m_id, "start listen...");

		bool result = m_socket.listen(SOMAXCONN);

		if (!result)
		{
			int last_error = std_get_error;
			LOG_NET_ERROR(m_id, "start listen : failed : socket.listen() returns %d!", last_error);
			return false;
		}

		setSocket(&m_socket);

		result = TransportManager::startReading(this);

		LOG_NET(m_id, "start listen : %s", result ? "done" : "failed");

		return result;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransportListenerTCP::stopListen()
	{
		LOG_NET(m_id, "stop listen...");

		stopReading();

		LOG_NET(m_id, "stop listen : done");
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransportListenerTCP::dataReady()
	{
		sockaddr_in addr;
		sockaddrlen_t addr_len = sizeof(addr);

		SOCKET handle = m_socket.accept((sockaddr*)&addr, &addr_len);

		if (handle == INVALID_SOCKET)
		{
			LOG_NET_ERROR(m_id, "data ready : failed : socket.accept() returns %d", std_get_error);
			// нужно прочитать на счет ошибок, возможно нужно будет тормозить слушателя!
			//m_manager.sip_listener_stopped(this, last_error);
			return;
		}

		sockaddrlen_t type = 0, typelen = sizeof(type);
		if (::getsockopt(handle, SOL_SOCKET, SO_TYPE, (char*)&type, &typelen) != 0)
		{
			LOG_NET_ERROR(m_id, "data ready : failed : getsockopt returns %d", std_get_error);
			return;
		}

		if (type != SOCK_STREAM)
		{
			LOG_NET_ERROR(m_id, "data ready : failed : socket handle type is not TCP");
			return;
		}

		if (TransportManager::isAddressBanned(addr.sin_addr))
		{
			LOG_NET_WARN(m_id, "data ready : PACKET NOT READ -- ADDRESS %s:%u BANNED!", inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));
			return;
		}

		LOG_EVENT(m_id, "%s : NEW CONNECTION for %s:%u", m_id, inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));

		TransportTCP* transport = NEW TransportTCP(); // this, addr.sin_addr, ntohs(addr.sin_port), m_type, handle);

		// запустим на чтение
		Route route;
		route.type = h248::TransportType::TCP;
		route.localAddress = m_interface.address;
		route.localPort = m_interface.port;
		route.remoteAddress = addr.sin_addr;
		route.remotePort = ntohs(addr.sin_port);

		transport->accept(handle, route);

		TransportManager::raiseAsyncEvent(TransportEvent_NewConnection, transport, 0, nullptr);
	}
	//--------------------------------------
	//
	//--------------------------------------
	TransportListenerUDP::TransportListenerUDP() : m_list_sync("mg-transport"), m_list(compareTransport, compareKey)
	{
		generateId(m_id, "udp-l");
	}
	//--------------------------------------
	//
	//--------------------------------------
	TransportListenerUDP::~TransportListenerUDP()
	{
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool TransportListenerUDP::create(const EndPoint& point)
	{
		LOG_NET(m_id, "create on %s:%u...", inet_ntoa(point.address), point.port);

		m_interface = point;

		if (!m_socket.open(AF_INET, SOCK_DGRAM, IPPROTO_UDP))
		{
			LOG_NET_ERROR(m_id, "create : failed : socket.open() return %d", std_get_error);
			return false;
		}

		sockaddr_in addr;
		memset(&addr, 0, sizeof addr);
		addr.sin_family = AF_INET;
		addr.sin_addr = m_interface.address;
		addr.sin_port = htons(m_interface.port);

		bool result = m_socket.bind((sockaddr*)&addr, sizeof(addr));

		if (!result)
		{
			LOG_NET_ERROR(m_id, "create : failed : socket.bind() return %d", std_get_error);
			return false;
		}

		int bsize = 64 * 1024;
		m_socket.set_option(SOL_SOCKET, SO_RCVBUF, &bsize, sizeof(int));
		m_socket.set_option(SOL_SOCKET, SO_SNDBUF, &bsize, sizeof(int));

		m_socket.set_tag(this);

		LOG_NET(m_id, "create : done");

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransportListenerUDP::destroy()
	{
		if (m_socket.get_handle() != INVALID_SOCKET)
		{
			LOG_NET(m_id, "destroy : closing socket...");

			m_socket.close();

			LOG_NET(m_id, "destroy : socket closed");
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool TransportListenerUDP::startListen()
	{
		LOG_NET(m_id, "start listen...");

		setSocket(&m_socket);

		bool result = TransportManager::startReading(this);

		LOG_NET(m_id, "start listen : %s", result ? "done" : "failed");

		return result;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransportListenerUDP::stopListen()
	{
		LOG_NET(m_id, "stop listen...");

		stopReading();

		LOG_NET(m_id, "stop listen : done");
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool TransportListenerUDP::attachTransport(TransportUDP* udp_transport)
	{
		LOG_NET(m_id, "attach transport %s...", udp_transport->getId());

		Route route;

		route.type = h248::TransportType::UDP;
		route.localAddress = m_interface.address;
		route.localPort = m_interface.port;
		route.remoteAddress = udp_transport->getRemoteAddress();
		route.remotePort = udp_transport->getRemotePort();

		udp_transport->accept(&m_socket, route);

		{
			//rtl::MutexLock lock(m_list_sync);
			rtl::MutexWatchLock lock(m_list_sync, __FUNCTION__);
			m_list.add(udp_transport);
		}

		LOG_NET(m_id, "attach transport : done");

		return true;
	}
	//--------------------------------------
	//
	//--------------------------------------
	bool TransportListenerUDP::detachTransport(TransportUDP* udp_transport)
	{
		LOG_NET(m_id, "detach transport %s...", udp_transport->getId());

		//rtl::MutexLock lock(m_list_sync);
		rtl::MutexWatchLock lock(m_list_sync, __FUNCTION__);

		uint64_t key = udp_transport->getKey();

		int index;
		TransportUDPPtr* found = m_list.find(key, index);

		if (found != nullptr)
		{
			m_list.removeAt(index);
		}

		LOG_NET(m_id, "deatach transport : %s", found != nullptr ? "done" : "not found");

		return found != nullptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransportListenerUDP::dataReady()
	{
		sockaddr_in addr;
		sockaddrlen_t addr_len = sizeof(addr);
		memset(&addr, 0, sizeof(addr));
		uint8_t buffer[4096];

		LOG_NET(m_id, "data ready : READING PACKET...");

		int res = m_socket.receive_from(buffer, 4096, 0, (sockaddr*)&addr, &addr_len);

		EndPoint point = { h248::TransportType::UDP, addr.sin_addr, ntohs(addr.sin_port) };

		if (res < 0)
		{
			pushErrorToTransport(std_get_error, point);
			return;
		}

		if (res == 0 || addr.sin_addr.s_addr == INADDR_ANY)
		{
			// закрытие!
			return;
		}

		if (res < 4)
		{
			LOG_NET_WARN(m_id, "data ready : packet(len:%d) less than 4 bytes! received from %s:%u", res, inet_ntoa(point.address), point.port);
			return;
		}

		if (res == 4 && *(uint32_t*)buffer == 0x0a0d0a0d)
		{
			// это пинг пропустим
			LOG_NET(m_id, "data ready : PING received from %s:%u", inet_ntoa(point.address), point.port);
			return;
		}

		if (m_interface.address.s_addr == addr.sin_addr.s_addr && m_interface.port == ntohs(addr.sin_port))
		{
			LOG_NET_WARN(m_id, "data ready : send loop detected: packet has same send and receive addresses %s:%u", inet_ntoa(point.address), point.port);
			return;
		}

		if (!TransportManager::isAddressBanned(addr.sin_addr))
		{
			if (addr.sin_addr.s_addr != INADDR_ANY && addr.sin_port != 0)
			{
				pushDataToTransport(buffer, res, point);
			}
			else
			{
				LOG_NET_WARN(m_id, "data ready : readed packet (%d) with empty back address!", res);
			}
		}
		else // BANNED ADDRESS
		{
			//if ((g_trace & (TRF_PROTO | TRF_BANNED)) == (TRF_PROTO | TRF_BANNED))
			if (rtl::Logger::check_trace(TRF_PROTO | TRF_BANNED))
			{
				uint8_t* a = (uint8_t*)&point.address.s_addr;
				uint8_t* b = (uint8_t*)&m_interface.address.s_addr;

				LogProto.log("RECV", "data ready : %s %d bytes FROM %d.%d.%d.%d:%u IFACE %d.%d.%d.%d:%u ---- BANNED BY IP!",
					TransportType_toString(m_interface.type), res, a[0], a[1], a[2], a[3], point.port, b[0], b[1], b[2], b[3], m_interface.port);
			}

			LOG_NET_WARN(m_id, "data ready : PACKET NOT PROCESSED -- ADDRESS %s:%u BANNED!", inet_ntoa(point.address), point.port);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	TransportUDP* TransportListenerUDP::getTransport(const EndPoint& remoteAddress)
	{
		//rtl::MutexLock lock(m_list_sync);
		rtl::MutexWatchLock lock(m_list_sync, __FUNCTION__);

		TransportUDP* transport = findTransport(remoteAddress);

		if (transport != nullptr)
		{
			return transport;
		}

		transport = NEW TransportUDP();

		attachTransport(transport);

		TransportManager::raiseAsyncEvent(TransportEvent_NewConnection, transport, 0, nullptr);

		return transport;
	}
	//--------------------------------------
	//
	//--------------------------------------
	TransportUDP* TransportListenerUDP::findTransport(const EndPoint& route)
	{
		//rtl::MutexLock lock(m_list_sync);
		rtl::MutexWatchLock lock(m_list_sync, __FUNCTION__);

		uint64_t key = MAKE_KEY64(route.address, route.port, route.type);

		TransportUDPPtr* found = m_list.find(key);

		return found != nullptr ? *found : nullptr;
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransportListenerUDP::pushDataToTransport(const uint8_t* packet, int packet_length, const EndPoint& remoteAddress)
	{
		// search transport;

		TransportUDP* transport = getTransport(remoteAddress);

		if (transport != nullptr)
		{
			transport->rxPacket(packet, packet_length, remoteAddress);
		}
		else
		{
			LOG_NET_ERROR(m_id, "push data to transport : failed : can't find or create udp transport for incoming packet %s:%u", inet_ntoa(remoteAddress.address), remoteAddress.port);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	void TransportListenerUDP::pushErrorToTransport(int net_error, const EndPoint& remoteAddress)
	{
		// search transport;
		TransportUDP* transport = getTransport(remoteAddress);

		if (transport != nullptr)
		{
			transport->rxError(net_error, remoteAddress);
		}
		else
		{
			LOG_NET_ERROR(m_id, "push error to transport : failed : cant find or create udp transport for net error(%d) hanling %s:%u", net_error, inet_ntoa(remoteAddress.address), remoteAddress.port);
		}
	}
	//--------------------------------------
	//
	//--------------------------------------
	int TransportListenerUDP::compareKey(const uint64_t& left, const TransportUDPPtr& right)
	{
		uint64_t right_key = right->getKey();
		return left < right_key ? -1 : left > right_key ? 1 : 0;
	}
	//--------------------------------------
	//
	//--------------------------------------
	int TransportListenerUDP::compareTransport(const TransportUDPPtr& left, const TransportUDPPtr& right)
	{
		uint64_t left_key = left->getKey();
		uint64_t right_key = right->getKey();
		return left_key < right_key ? -1 : left_key > right_key ? 1 : 0;
	}
}
//--------------------------------------
