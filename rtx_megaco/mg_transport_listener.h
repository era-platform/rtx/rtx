﻿/* RTX H.248 Media Gate Engine
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "mg_transport_reader.h"

namespace h248
{
	//--------------------------------------
	// слушатель новых соединений
	//--------------------------------------
	class TransportListener : protected TransportObject
	{
	protected:
		net_socket_t m_socket;
		EndPoint m_interface;

	public:
		TransportListener();
		~TransportListener();

		virtual bool create(const EndPoint& point) = 0;
		virtual void destroy() = 0;

		virtual bool startListen() = 0;
		virtual void stopListen() = 0;

		in_addr getInterfaceAddress() { return m_interface.address; }
		uint16_t getInterfacePort() { return m_interface.port; }
		TransportType getTransportType() { return m_interface.type; }
	};
	//--------------------------------------
	// слушатель новых соединений TCP
	//--------------------------------------
	class TransportListenerTCP : public TransportListener
	{
	public:
		TransportListenerTCP();
		virtual ~TransportListenerTCP();

		virtual bool create(const EndPoint& point);
		virtual void destroy();

		virtual bool startListen();
		virtual void stopListen();

	private:
		/// TransportObject interface
		virtual void dataReady();

	};
	//--------------------------------------
	// предварительное объявление udp транспорта
	//--------------------------------------
	class TransportUDP;
	//--------------------------------------
	// слушатель новых соединений UDP
	//--------------------------------------
	class TransportListenerUDP : public TransportListener
	{
		rtl::MutexWatch m_list_sync;
		typedef TransportUDP* TransportUDPPtr;
		rtl::SortedArrayT<TransportUDPPtr, uint64_t> m_list;

	public:
		TransportListenerUDP();
		virtual ~TransportListenerUDP();

		virtual bool create(const EndPoint& point);
		virtual void destroy();

		virtual bool startListen();
		virtual void stopListen();

		bool attachTransport(TransportUDP* udp_transport);
		bool detachTransport(TransportUDP* udp_transport);

	private:
		/// TransportObject interface
		virtual void dataReady();

		TransportUDP* getTransport(const EndPoint& route);
		TransportUDP* findTransport(const EndPoint& route);

		void pushDataToTransport(const uint8_t* packet, int packet_length, const EndPoint& remoteAddress);
		void pushErrorToTransport(int net_error, const EndPoint& remoteAddress);

		static int compareKey(const uint64_t& l, const TransportUDPPtr& r);
		static int compareTransport(const TransportUDPPtr& l, const TransportUDPPtr& r);
	};
}
//--------------------------------------
