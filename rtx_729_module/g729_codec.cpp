﻿/* RTX H.248 Media Gate. G.729 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "g729.h"
#include "g729_codec.h"
#include "rtp_packet.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
extern int g_rtx_g729_decoder_counter = -1;
extern int g_rtx_g729_encoder_counter = -1;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtx_g729_codec_decoder::rtx_g729_codec_decoder(rtl::Logger* log) :
	m_log(log)
{
	m_decoder = nullptr;

	rtl::res_counter_t::add_ref(g_rtx_g729_decoder_counter);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtx_g729_codec_decoder::~rtx_g729_codec_decoder()
{
	destroy();

	rtl::res_counter_t::release(g_rtx_g729_decoder_counter);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtx_g729_codec_decoder::initialize(const media::PayloadFormat& format)
{
	m_frame_size = L_FRAME * 2;

	if (m_decoder == nullptr)
	{
		m_decoder = NEW g729();

		g729_InitDecoder(m_decoder);

		return true;
	}

	return false;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t rtx_g729_codec_decoder::get_frame_size_pcm()
{
	return m_frame_size * sizeof(uint16_t);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t rtx_g729_codec_decoder::get_frame_size_cod()
{
	return SERIAL_SIZEBYTES;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtx_g729_codec_decoder::destroy()
{
	if (m_decoder != nullptr)
	{
		DELETEO(m_decoder);
		m_decoder = nullptr;
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const char*rtx_g729_codec_decoder::getEncoding()
{
	return "g729";
}
//------------------------------------------------------------------------------
// Читаем по 10 байт и декодируем их в 160 байт.
//------------------------------------------------------------------------------
uint32_t rtx_g729_codec_decoder::decode(const uint8_t* from, uint32_t from_length, uint8_t* to, uint32_t to_length)
{
	uint32_t pcmSize = 0;
	uint32_t decSize = 0;

	while (pcmSize < to_length)
	{
		if (decSize >= from_length)
		{
			PLOG_MEDIA_ERROR("G729","g729_codec.decode -- not enough space for decode");
			break;
		}

		decode_element(m_decoder, from, (short*)to);

		to += L_FRAME*2;
		from += SERIAL_SIZEBYTES;

		pcmSize += L_FRAME*2;
		decSize += SERIAL_SIZEBYTES;
	}

	return pcmSize;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
uint32_t rtx_g729_codec_decoder::depacketize(const rtp_packet* packet, uint8_t* buff, uint32_t buff_size)
{
	if (packet == nullptr)
	{
		return 0;
	}

	if (buff == nullptr)
	{
		return 0;
	}

	if (buff_size < packet->get_payload_length())
	{
		return 0;
	}

	uint32_t payload_length = packet->get_payload_length();

	memcpy(buff, packet->get_payload(), payload_length);

	return payload_length;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
rtx_g729_codec_encoder::rtx_g729_codec_encoder(rtl::Logger* log) :
	m_log(log)
{
	m_encoder = nullptr;

	rtl::res_counter_t::add_ref(g_rtx_g729_encoder_counter);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtx_g729_codec_encoder::~rtx_g729_codec_encoder()
{
	destroy();

	rtl::res_counter_t::release(g_rtx_g729_encoder_counter);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool		rtx_g729_codec_encoder::initialize(const media::PayloadFormat& format)
{
	m_frame_size = L_FRAME * 2;

	if (m_encoder == nullptr)
	{
		m_encoder = NEW g729();

		g729_InitEncoder(m_encoder);

		return true;
	}

	return false;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t rtx_g729_codec_encoder::get_frame_size()
{
	return m_frame_size * sizeof(uint16_t);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void		rtx_g729_codec_encoder::destroy()
{
	if (m_encoder != nullptr)
	{
		DELETEO(m_encoder);
		m_encoder = nullptr;
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const char* rtx_g729_codec_encoder::getEncoding()
{
	return "g729";
}
//------------------------------------------------------------------------------
// Читаем по 160 байт и кодируем их в 10 байт.
//------------------------------------------------------------------------------
uint32_t rtx_g729_codec_encoder::encode(const uint8_t* from, uint32_t from_length, uint8_t* to, uint32_t to_length)
{
	uint32_t g729Size = 0;
	uint32_t read_Size = 0;

	while (read_Size < from_length)
	{
		if ((g729Size >= to_length))
		{
			PLOG_MEDIA_ERROR("G729","g729_codec.encode -- not enough space for encode");
			break;
		}

		coding_element(m_encoder, (const short*)from, to);

		to += SERIAL_SIZEBYTES;
		from += L_FRAME * 2;

		g729Size += SERIAL_SIZEBYTES;
		read_Size += L_FRAME * 2;
	}

	return g729Size;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t rtx_g729_codec_encoder::packetize(rtp_packet* packet, const uint8_t* buff, uint32_t buff_size)
{
	if (packet == nullptr)
	{
		return 0;
	}

	if (buff == nullptr)
	{
		return 0;
	}

	if (buff_size > rtp_packet::PAYLOAD_BUFFER_SIZE)
	{
		return 0;
	}

	if (packet->set_payload(buff, buff_size) == 0)
	{
		return 0;
	}

	packet->set_payload_type(18);

	packet->set_samples(m_frame_size);

	return buff_size;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
