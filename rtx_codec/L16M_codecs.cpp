/* RTX H.248 Media Gate. Codec Interface Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "buildin_codecs.h"
#include "rtp_packet.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define LOG_PREFIX "L16"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool is_build_in(const char* codec_name)
{
	if (std_stricmp(codec_name, "L16") == 0)
		return true;

	return false;
}
mg_audio_encoder_t* create_buildin_audio_encoder(const media::PayloadFormat& format)
{
	if (std_stricmp(format.getEncoding(), "L16") != 0)
		return nullptr;

	rtx_L16M_encoder* enc = NEW rtx_L16M_encoder();

	if (enc->mg_audio_encoder_initialize(format))
		return enc;

	DELETEO(enc);

	return nullptr;
}
mg_audio_decoder_t* create_buildin_audio_decoder(const media::PayloadFormat& format)
{
	if (std_stricmp(format.getEncoding(), "L16") != 0)
		return nullptr;

	rtx_L16M_decoder* dec = NEW rtx_L16M_decoder();

	if (dec->mg_audio_decoder_initialize(format))
		return dec;

	DELETEO(dec);

	return nullptr;
}
void release_buildin_audio_encoder(mg_audio_encoder_t* encoder)
{
	DELETEO(encoder);
}
void release_buildin_audio_decoder(mg_audio_decoder_t* decoder)
{
	DELETEO(decoder);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtx_L16M_decoder::rtx_L16M_decoder() : m_frame_size(160)
{
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtx_L16M_decoder::~rtx_L16M_decoder()
{
	mg_audio_decoder_destroy();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtx_L16M_decoder::mg_audio_decoder_initialize(const media::PayloadFormat& format)
{
	m_frame_size = (20 * 8000 / 1000);
	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t rtx_L16M_decoder::get_frame_size_pcm()
{
	return m_frame_size * sizeof(uint16_t);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t rtx_L16M_decoder::get_frame_size_cod()
{
	return m_frame_size * sizeof(uint16_t);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtx_L16M_decoder::mg_audio_decoder_destroy()
{

}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const char* rtx_L16M_decoder::getEncoding()
{
	return "L16";
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t rtx_L16M_decoder::decode(const uint8_t* from, uint32_t from_length, uint8_t* to, uint32_t to_length)
{
	int copyto = MIN(from_length, to_length);

	memcpy(to, from, copyto);

	return copyto;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t rtx_L16M_decoder::depacketize(const rtp_packet* packet, uint8_t* buff, uint32_t buff_size)
{
	if (packet == nullptr)
	{
		return 0;
	}

	if (buff == nullptr)
	{
		return 0;
	}

	if (buff_size < packet->get_payload_length())
	{
		return 0;
	}

	uint32_t payload_length = packet->get_payload_length();

	memcpy(buff, packet->get_payload(), payload_length);

	return payload_length;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtx_L16M_encoder::rtx_L16M_encoder()
{
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtx_L16M_encoder::~rtx_L16M_encoder()
{
	mg_audio_encoder_destroy();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtx_L16M_encoder::mg_audio_encoder_initialize(const media::PayloadFormat& format)
{
	m_frame_size = 20 * 8000 / 1000;
	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t rtx_L16M_encoder::get_frame_size()
{
	return m_frame_size * sizeof(uint16_t);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtx_L16M_encoder::mg_audio_encoder_destroy()
{

}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const char* rtx_L16M_encoder::getEncoding()
{
	return "L16";
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t rtx_L16M_encoder::encode(const uint8_t* from, uint32_t from_length, uint8_t* to, uint32_t to_length)
{
	uint32_t copyto = MIN(from_length, to_length);

	memcpy(to, from, copyto);

	return copyto;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t rtx_L16M_encoder::packetize(rtp_packet* packet, const uint8_t* buff, uint32_t buff_size)
{
	if (packet == nullptr)
	{
		return 0;
	}

	if (buff == nullptr)
	{
		return 0;
	}

	if (buff_size > rtp_packet::PAYLOAD_BUFFER_SIZE)
	{
		return 0;
	}

	if (packet->set_payload(buff, buff_size) == 0)
	{
		return 0;
	}

	packet->set_payload_type(11);

	packet->set_samples(buff_size/sizeof(short));

	return buff_size;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
