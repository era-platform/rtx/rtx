/* RTX H.248 Media Gate. Codec Interface Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_audio_codec.h"
#include "std_logger.h"

bool is_build_in(const char* codec_name);
mg_audio_encoder_t* create_buildin_audio_encoder(const media::PayloadFormat& format);
mg_audio_decoder_t* create_buildin_audio_decoder(const media::PayloadFormat& format);
void release_buildin_audio_encoder(mg_audio_encoder_t* encoder);
void release_buildin_audio_decoder(mg_audio_decoder_t* decoder);
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class rtx_L16M_decoder : public mg_audio_decoder_t
{
	rtx_L16M_decoder(const rtx_L16M_decoder&);
	rtx_L16M_decoder& operator=(const rtx_L16M_decoder&);

public:
	rtx_L16M_decoder();
	virtual ~rtx_L16M_decoder();

	bool mg_audio_decoder_initialize(const media::PayloadFormat& format);
	void mg_audio_decoder_destroy();

	virtual const char* getEncoding();

	virtual uint32_t depacketize(const rtp_packet* packet, uint8_t* buff, uint32_t buff_size);

	virtual uint32_t decode(const uint8_t* buff_from, uint32_t size_from, uint8_t* buff_to, uint32_t size_to);

	virtual uint32_t get_frame_size_pcm();
	virtual uint32_t get_frame_size_cod();

private:
	uint32_t m_frame_size;
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class rtx_L16M_encoder : public mg_audio_encoder_t
{
	rtx_L16M_encoder(const rtx_L16M_encoder&);
	rtx_L16M_encoder& operator=(const rtx_L16M_encoder&);

public:
	rtx_L16M_encoder();
	virtual ~rtx_L16M_encoder();

	bool mg_audio_encoder_initialize(const media::PayloadFormat& format);
	void mg_audio_encoder_destroy();

	virtual const char* getEncoding();
	virtual uint32_t encode(const uint8_t* buff_from, uint32_t size_from, uint8_t* buff_to, uint32_t size_to);
	virtual uint32_t packetize(rtp_packet* packet, const uint8_t* buff, uint32_t buff_size);
	virtual uint32_t get_frame_size();

private:
	uint32_t m_frame_size;
};
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
