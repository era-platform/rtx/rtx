﻿/* RTX H.248 Media Gate. Codec Interface Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtx_audio_codec.h"
#include "rtx_video_codec.h"
#include "rtx_codec_module.h"
#include "std_logger.h"
#include "std_sys_env.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#if defined (DEBUG_MEMORY)
void* operator new (size_t size, char* file, int line)
{
	return std_mem_checker_malloc(size, file, line, rc_malloc_new_e);
}
void* operator new[](size_t size, char* file, int line)
{
	return std_mem_checker_malloc(size, file, line, rc_malloc_newar_e);
}
void operator delete (void* mem_ptr)
{
	std_mem_checker_free(mem_ptr, rc_malloc_delete_e);
}
void operator delete[](void* mem_ptr)
{
	std_mem_checker_free(mem_ptr, rc_malloc_deletear_e);
}
#endif
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
static rtl::ArrayT<rtx_codec_module*>* g_audio_codec_module_list = nullptr;
static rtl::ArrayT<rtx_codec_module*>* g_video_codec_module_list = nullptr;
static rtl::Logger* g_codec_module_log = nullptr;
static rtl::StringList g_codec_list;

static bool rtx_codec__load_audio_codecs(const char* folderPath);
static bool rtx_codec__load_video_codecs(const char* folderPath);
static void rtx_codec__set_available_codec_list(const char* codecList);
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtl::String rtx_codec__get_audio_codec_list()
{
	if (g_audio_codec_module_list == nullptr)
	{
		return "";
	}

	rtl::String codecs;

	for (int i = 0; i < g_audio_codec_module_list->getCount(); i++)
	{
		rtx_codec_module* mg_module = g_audio_codec_module_list->getAt(i);
		if (mg_module == nullptr)
		{
			continue;
		}

		rtl::String codec(mg_module->get_codec_list());
		if (codec.isEmpty())
		{
			continue;
		}

		if (i == 0)
		{
			codecs << codec;
		}
		else
		{
			codecs << " " << codec;
		}
	}

	return codecs;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtl::String rtx_codec__get_video_codec_list()
{
	if (g_video_codec_module_list == nullptr)
	{
		return "";
	}

	rtl::String codecs;

	for (int i = 0; i < g_video_codec_module_list->getCount(); i++)
	{
		rtx_codec_module* mg_module = g_video_codec_module_list->getAt(i);
		if (mg_module == nullptr)
		{
			continue;
		}

		rtl::String codec(mg_module->get_codec_list());
		if (codec.isEmpty())
		{
			continue;
		}

		if (i == 0)
		{
			codecs << codec;
		}
		else
		{
			codecs << " " << codec;
		}
	}

	return codecs;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtl::String rtx_codec__get_codec_list()
{
	rtl::String codecs;

	for (int i = 0; i < g_codec_list.getCount(); i++)
		codecs << g_codec_list[i] << ' ';

	return codecs;

	/*
	if (g_audio_codec_module_list != nullptr)
	{

		for (int i = 0; i < g_audio_codec_module_list->getCount(); i++)
		{
			rtx_codec_module* mg_module = g_audio_codec_module_list->getAt(i);

			const char* codec = mg_module->get_codec_list();

			if (codec != nullptr && *codec != '\0')
			{
				codecs << codec << " ";
			}
		}
	}

	if (g_video_codec_module_list != nullptr)
	{
		int vcount = g_video_codec_module_list->getCount();

		for (int i = 0; i < vcount; i++)
		{
			rtx_codec_module* mg_module = g_video_codec_module_list->getAt(i);
			if (mg_module == nullptr)
			{
				continue;
			}

			rtl::String codec(mg_module->get_codec_list());
			if (codec.isEmpty())
			{
				continue;
			}

			codecs << codec;

			if (i < vcount - 1)
				codecs << ' ';
		}
	}

	return codecs;*/
}
//------------------------------------------------------------------------------
// Проверка на известные форматы
//------------------------------------------------------------------------------
bool rtx_codec__isKnownFormat(const char* encoding)
{
	//return true;

	if (std_stricmp(encoding, "telephone-event") == 0 || std_stricmp(encoding, "conf_out") == 0 || std_stricmp(encoding, "conf_in") == 0 ||
		std_stricmp(encoding, "ivr") == 0 || std_stricmp(encoding, "t30") == 0)
		return true;


	for (int i = 0; i < g_codec_list.getCount(); i++)
	{
		if (std_stricmp(encoding, g_codec_list[i]) == 0)
			return true;
	}

	return false;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtx_codec__get_available_audio_codecs(media::PayloadSet& set)
{
	for (int i = 0; i < g_audio_codec_module_list->getCount(); i++)
	{
		rtx_codec_module* mg_module = g_audio_codec_module_list->getAt(i);

		mg_module->get_available_audio_payloads(set);
	}

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtx_codec__get_available_video_codecs(media::PayloadSet& set)
{
	return false;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtx_codec__load_all_codecs(const char* folderPath, const char* codecList)
{
	rtx_codec__load_audio_codecs(folderPath);
	rtx_codec__load_video_codecs(folderPath);
	rtx_codec__set_available_codec_list(codecList);

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
static bool rtx_codec__load_audio_codecs(const char* param)
{
	LOG_CALL("CODEC", "rtx_codec__load_audio_codecs('%s')", param);

	if (g_audio_codec_module_list == nullptr)
	{
		g_audio_codec_module_list = NEW rtl::ArrayT<rtx_codec_module*>();
	}

	char apppath[256];
	rtl::String path;
	rtl::String mask;

	if (param != nullptr && *param != 0)
	{
		path = param;
	}
	else
	{
		if (std_get_application_startup_path(apppath, 256))
		{
			path = apppath;
		}
		else
		{
			Err.log("RTXCODEC", "rtx_codec__load_audio_codecs : get startup path returns error. no codecs loaded!");
			return false;
		}
	}


#if defined(TARGET_OS_WINDOWS)
	mask = path + "\\rtx_codec_audio_*.dll";
#else
	mask = path + "/librtx_codec_audio_*.so";
#endif
	
	rtl::ArrayT<fs_entry_info_t> filelist;

	fs_directory_get_entries(mask, filelist);

	LOG_CALL("CODEC", "rtx_codec__load_audio_codecs : %d codecs found", filelist.getCount());

	for (int i = 0; i < filelist.getCount(); i++)
	{
		fs_entry_info_t& params = filelist.getAt(i);
		
		mask = path + FS_PATH_DELIMITER + params.fs_filename;

		rtx_codec_module* mg_module = NEW rtx_codec_module(rtx_audio_module);

		if (mg_module->init(mask))
		{
			g_audio_codec_module_list->add(mg_module);
		}
		else
		{
			DELETEO(mg_module);

			LOG_CALL("CODEC", "rtx_codec__load_audio_codecs : module %s is invalid", (const char*)mask);
		}
	}

	LOG_CALL("CODEC", "rtx_codec__load_audio_codecs : %d codecs added", g_audio_codec_module_list->getCount());

	return g_audio_codec_module_list->getCount() > 0;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
static bool rtx_codec__load_video_codecs(const char* param)
{
	LOG_CALL("CODEC", "rtx_codec__load_video_codecs('%s')", param);

	if (g_video_codec_module_list == nullptr)
	{
		g_video_codec_module_list = NEW rtl::ArrayT<rtx_codec_module*>();
	}

	char apppath[256];
	rtl::String path;
	rtl::String mask;

	if (param != nullptr && *param != 0)
	{
		path = param;
	}
	else
	{
		if (std_get_application_startup_path(apppath, 256))
		{
			path = apppath;
		}
		else
		{
			Err.log("RTXCODEC", "rtx_codec__load_video_codecs : get startup path returns error. no codecs loaded!");
			return false;
		}
	}


#if defined(TARGET_OS_WINDOWS)
	mask = path + "\\rtx_codec_video_*.dll";
#else
	mask = path + "/librtx_codec_video_*.so";
#endif

	rtl::ArrayT<fs_entry_info_t> filelist;

	fs_directory_get_entries(mask, filelist);

	LOG_CALL("CODEC", "rtx_codec__load_video_codecs : %d codecs found", filelist.getCount());

	for (int i = 0; i < filelist.getCount(); i++)
	{
		fs_entry_info_t& params = filelist.getAt(i);

		mask = path + FS_PATH_DELIMITER + params.fs_filename;

		rtx_codec_module* mg_module = NEW rtx_codec_module(rtx_video_module);

		if (mg_module->init(mask))
		{
			g_video_codec_module_list->add(mg_module);
		}
		else
		{
			DELETEO(mg_module);

			LOG_CALL("CODEC", "rtx_codec__load_video_codecs : module %s is invalid", (const char*)mask);
		}
	}

	LOG_CALL("CODEC", "rtx_codec__load_video_codecs : %d codecs added", g_audio_codec_module_list->getCount());

	return g_video_codec_module_list->getCount() > 0;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
static void rtx_codec__set_available_codec_list(const char* codecList)
{
	LOG_CALL("CODEC", "rtx_codec__set_available_codec_list : %s", codecList);

	rtl::String loaded = rtx_codec__get_audio_codec_list();
	loaded << ' ' << rtx_codec__get_video_codec_list();

	rtl::String allowed = codecList;

	if (loaded.isEmpty())
		return;

	loaded.split(g_codec_list, ' ');

	if (allowed.isEmpty())
		return;

	rtl::StringList allowedList;

	allowed.split(allowedList, ' ');

	// intersect codec sets
	for (int i = g_codec_list.getCount() - 1; i >= 0; i--)
	{
		if (!allowedList.contains(g_codec_list[i], true))
			g_codec_list.removeAt(i);
	}

	if (g_codec_list.getCount() == 0)
	{
		g_codec_list.add("PCMA");
		g_codec_list.add("PCMU");
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtx_codec__load_audio_codec(const char* codec_path)
{
	if (g_audio_codec_module_list == nullptr)
	{
		g_audio_codec_module_list = NEW rtl::ArrayT<rtx_codec_module*>();
	}

	rtx_codec_module* mg_module = NEW rtx_codec_module(rtx_audio_module);

	if (mg_module->init(codec_path))
	{
		g_audio_codec_module_list->add(mg_module);
		return true;
	}

	DELETEO(mg_module);

	return false;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtx_codec__load_video_codec(const char* codec_path)
{
	if (g_video_codec_module_list == nullptr)
	{
		g_video_codec_module_list = NEW rtl::ArrayT<rtx_codec_module*>();
	}

	rtx_codec_module* mg_module = NEW rtx_codec_module(rtx_video_module);

	if (mg_module->init(codec_path))
	{
		g_video_codec_module_list->add(mg_module);
		return true;
	}

	DELETEO(mg_module);

	return false;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtx_codec__unload_codecs()
{
	if (g_audio_codec_module_list == nullptr)
	{
		return;
	}

	for (int i = 0; i < g_audio_codec_module_list->getCount(); i++)
	{
		rtx_codec_module* mg_module = g_audio_codec_module_list->getAt(i);
		if (mg_module == nullptr)
		{
			continue;
		}

		DELETEO(mg_module);
		mg_module = nullptr;
	}

	g_audio_codec_module_list->clear();

	DELETEO(g_audio_codec_module_list);
	g_audio_codec_module_list = nullptr;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtx_codec_module* find_audio_module(const char* codec_name)
{
	if (g_audio_codec_module_list == nullptr)
	{
		return nullptr;
	}

	for (int i = 0; i < g_audio_codec_module_list->getCount(); i++)
	{
		rtx_codec_module* mg_module = g_audio_codec_module_list->getAt(i);
		if (mg_module == nullptr)
		{
			continue;
		}

		rtl::String codec(mg_module->get_codec_list());
		if (codec.isEmpty())
		{
			continue;
		}

		rtl::StringList codecs;
		codec.split(codecs, ' ');

		for (int j = 0; j < codecs.getCount(); j++)
		{
			rtl::String codec_at_list = codecs.getAt(j);
			codec_at_list.trim();
			codec_at_list.toLower();

			rtl::String codec_name_str(codec_name);
			codec_name_str.trim();
			codec_name_str.toLower();

			if (rtl::String::compare(codec_name_str, codec_at_list, false) == 0)
			{
				return mg_module;
			}
		}
	}

	return nullptr;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtx_codec_module* find_video_module(const char* codec_name)
{
	if (g_video_codec_module_list == nullptr)
	{
		return nullptr;
	}

	for (int i = 0; i < g_video_codec_module_list->getCount(); i++)
	{
		rtx_codec_module* mg_module = g_video_codec_module_list->getAt(i);
		if (mg_module == nullptr)
		{
			continue;
		}

		rtl::String codec(mg_module->get_codec_list());
		if (codec.isEmpty())
		{
			continue;
		}

		rtl::StringList codecs;
		codec.split(codecs, ' ');

		for (int j = 0; j < codecs.getCount(); j++)
		{
			rtl::String codec_at_list = codecs.getAt(j);
			codec_at_list.trim();
			codec_at_list.toLower();

			rtl::String codec_name_str(codec_name);
			codec_name_str.trim();
			codec_name_str.toLower();

			if (rtl::String::compare(codec_name_str, codec_at_list, false) == 0)
			{
				return mg_module;
			}
		}
	}

	return nullptr;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
mg_audio_decoder_t* rtx_codec__create_audio_decoder(const char* encoding, const media::PayloadFormat* format)
{
	rtx_codec_module* mg_module = find_audio_module(format->getEncoding());

	if (mg_module != nullptr)
	{
		return mg_module->create_audio_decoder(encoding, format);
	}

	LOG_WARN("CODEC", "module for audio codec '%s' not found", (const char*)format->getEncoding());

	return nullptr;
}
//------------------------------------------------------------------------------
void rtx_codec__release_audio_decoder(mg_audio_decoder_t* decoder)
{
	if (decoder != nullptr)
	{
		rtx_codec_module* mg_module = find_audio_module(decoder->getEncoding());
		if (mg_module != nullptr)
		{
			mg_module->release_audio_decoder(decoder);
		}
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
mg_audio_encoder_t* rtx_codec__create_audio_encoder(const char* encoding, const media::PayloadFormat* format)
{
	rtx_codec_module* mg_module = find_audio_module(format->getEncoding());
	
	if (mg_module != nullptr)
	{
		return mg_module->create_audio_encoder(encoding, format);
	}

	LOG_WARN("CODEC", "module for audio codec '%s' not found", (const char*)format->getEncoding());

	return nullptr;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtx_codec__release_audio_encoder(mg_audio_encoder_t* encoder)
{
	if (encoder != nullptr)
	{
		rtx_codec_module* mg_module = find_audio_module(encoder->getEncoding());
		if (mg_module != nullptr)
		{
			mg_module->release_audio_encoder(encoder);
		}
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_CODEC_API mg_video_decoder_t* rtx_codec__create_video_decoder(const char* encoding, const media::PayloadFormat* format)
{
	rtx_codec_module* mg_module = find_video_module(encoding);

	if (mg_module != nullptr)
	{
		return mg_module->create_video_decoder(encoding, format);
	}
	
	LOG_WARN("CODEC", "module for video codec '%s' not found", (const char*)format->getEncoding());

	return nullptr;
}
//------------------------------------------------------------------------------
RTX_CODEC_API void rtx_codec__release_video_decoder(mg_video_decoder_t* decoder)
{
	if (decoder != nullptr)
	{
		rtx_codec_module* mg_module = find_video_module(decoder->getEncoding());
		if (mg_module != nullptr)
		{
			mg_module->release_video_decoder(decoder);
		}
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_CODEC_API mg_video_encoder_t* rtx_codec__create_video_encoder(const char* encoding, const media::PayloadFormat* format,
	rtx_video_encoder_cb_t cb, void* usr)
{
	rtx_codec_module* mg_module = find_video_module(format->getEncoding());

	if (mg_module != nullptr)
	{
		return mg_module->create_video_encoder(encoding, format, cb, usr);
	}
	
	LOG_WARN("CODEC", "module for video codec '%s' not found", (const char*)format->getEncoding());

	return nullptr;
}
//------------------------------------------------------------------------------
RTX_CODEC_API void rtx_codec__release_video_encoder(mg_video_encoder_t* encoder)
{
	if (encoder != nullptr)
	{
		rtx_codec_module* mg_module = find_video_module(encoder->getEncoding());
		if (mg_module != nullptr)
		{
			mg_module->release_video_encoder(encoder);
		}
	}
}
//------------------------------------------------------------------------------
