﻿/* RTX H.248 Media Gate. Codec Interface Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#include "std_module.h"
#include "rtx_video_codec.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
struct mg_audio_decoder_t;
struct mg_audio_encoder_t;
struct mg_video_decoder_t;
struct mg_video_encoder_t;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
typedef	bool(*pfn_rtx_codec__initlib)();
typedef	void(*pfn_rtx_codec__freelib)();
typedef	const char* (*pfn_rtx_codec__get_codec_list)();
typedef	bool (*pfn_rtx_codec__get_available_audio_payloads)(media::PayloadSet& set);
typedef	mg_audio_decoder_t* (*pfn_rtx_codec__create_audio_decoder)(const media::PayloadFormat* format);
typedef	void(*pfn_rtx_codec__release_audio_decoder)(mg_audio_decoder_t* decoder);
typedef	mg_audio_encoder_t* (*pfn_rtx_codec__create_audio_encoder)(const media::PayloadFormat* format);
typedef	void(*pfn_rtx_codec__release_audio_encoder)(mg_audio_encoder_t* encoder);
typedef	mg_video_decoder_t* (*pfn_rtx_codec__create_video_decoder)(const media::PayloadFormat* format);
typedef	void(*pfn_rtx_codec__release_video_decoder)(mg_video_decoder_t* decoder);
typedef	mg_video_encoder_t* (*pfn_rtx_codec__create_video_encoder)(const media::PayloadFormat* format, rtx_video_encoder_cb_t cb, void* usr);
typedef	void(*pfn_rtx_codec__release_video_encoder)(mg_video_encoder_t* encoder);

enum rtx_module_type_t { rtx_audio_module, rtx_video_module, rtx_unknown_module };
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class rtx_codec_module
{
	rtx_codec_module(const rtx_codec_module&);
	rtx_codec_module& operator=(const rtx_codec_module&);

public:
	rtx_codec_module(rtx_module_type_t type);
	virtual	~rtx_codec_module();

	bool init(const char* module_path);
	void destroy();

	const char* get_codec_list();
	bool get_available_audio_payloads(media::PayloadSet& set);

	bool is_audio_set() { return m_iface_type == rtx_module_type_t::rtx_audio_module; }
	bool is_video_set() { return m_iface_type == rtx_module_type_t::rtx_video_module; }
	bool is_bad_module() { return m_iface_type == rtx_module_type_t::rtx_video_module; }
	rtx_module_type_t type() { return m_iface_type; }

	mg_audio_decoder_t* create_audio_decoder(const char* encoding, const media::PayloadFormat* format);
	void release_audio_decoder(mg_audio_decoder_t* decoder);

	mg_audio_encoder_t* create_audio_encoder(const char* encoding, const media::PayloadFormat* format);
	void release_audio_encoder(mg_audio_encoder_t* encoder);

//	bool get_video_codec_info(const PayloadFormat* format, mmt_image_t* info);

	mg_video_decoder_t* create_video_decoder(const char* encoding, const media::PayloadFormat* format);
	void release_video_decoder(mg_video_decoder_t* decoder);

	mg_video_encoder_t* create_video_encoder(const char* encoding, const media::PayloadFormat* format, rtx_video_encoder_cb_t cb, void* usr);
	void release_video_encoder(mg_video_encoder_t* encoder);

private:
	pfn_rtx_codec__initlib m_initlib_func;
	pfn_rtx_codec__freelib m_freelib_func;
	pfn_rtx_codec__get_codec_list m_get_codec_list_func;
	pfn_rtx_codec__get_available_audio_payloads m_get_available_audio_payloads_func;

	pfn_rtx_codec__create_audio_decoder m_create_audio_decoder_func;
	pfn_rtx_codec__release_audio_decoder m_release_audio_decoder_func;
	pfn_rtx_codec__create_audio_encoder m_create_audio_encoder_func;
	pfn_rtx_codec__release_audio_encoder m_release_audio_encoder_func;

//	pfn_rtx_codec__get_video_codec_info m_get_video_codec_info_func;
	pfn_rtx_codec__create_video_decoder m_create_video_decoder_func;
	pfn_rtx_codec__release_video_decoder m_release_video_decoder_func;
	pfn_rtx_codec__create_video_encoder m_create_video_encoder_func;
	pfn_rtx_codec__release_video_encoder m_release_video_encoder_func;

private:
	rtx_module_type_t m_iface_type;
	rtl::Module m_module;
};
//------------------------------------------------------------------------------
