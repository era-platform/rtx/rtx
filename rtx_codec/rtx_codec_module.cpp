﻿/* RTX H.248 Media Gate. Codec Interface Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtx_codec_module.h"
#include "rtx_codec.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtx_codec_module::rtx_codec_module(rtx_module_type_t type)
{
	m_iface_type = type;
	m_get_codec_list_func = nullptr;
	m_get_available_audio_payloads_func = nullptr;
	m_initlib_func = nullptr;
	m_freelib_func = nullptr;
	m_create_audio_decoder_func = nullptr;
	m_release_audio_decoder_func = nullptr;
	m_create_audio_encoder_func = nullptr;
	m_release_audio_encoder_func = nullptr;
	m_create_video_decoder_func = nullptr;
	m_release_video_decoder_func = nullptr;
	m_create_video_encoder_func = nullptr;
	m_release_video_encoder_func = nullptr;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtx_codec_module::~rtx_codec_module()
{
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtx_codec_module::init(const char* module_path)
{
	if (module_path == nullptr || *module_path == 0)
	{
		return false;
	}

	if (!m_module.load(module_path))
	{
		return false;
	}

	m_initlib_func = (pfn_rtx_codec__initlib)m_module.bindSymbol("initlib");

	if (m_initlib_func == nullptr || !m_initlib_func())
	{
		m_module.unload();
		return false;
	}
	
	m_freelib_func = (pfn_rtx_codec__freelib)m_module.bindSymbol("freelib");
	if (m_freelib_func == nullptr)
	{
		destroy();
		return false;
	}
	
	m_get_codec_list_func = (pfn_rtx_codec__get_codec_list)m_module.bindSymbol("get_codec_list");
	if (m_get_codec_list_func == nullptr)
	{
		destroy();
		return false;
	}

	if (m_iface_type == rtx_audio_module)
	{
		m_get_available_audio_payloads_func = (pfn_rtx_codec__get_available_audio_payloads)m_module.bindSymbol("get_available_audio_payloads");
		if (m_get_available_audio_payloads_func == nullptr)
		{
			destroy();
			return false;
		}

		m_create_audio_decoder_func = (pfn_rtx_codec__create_audio_decoder)m_module.bindSymbol("create_audio_decoder");
		if (m_create_audio_decoder_func == nullptr)
		{
			destroy();
			return false;
		}
		m_release_audio_decoder_func = (pfn_rtx_codec__release_audio_decoder)m_module.bindSymbol("release_audio_decoder");
		if (m_release_audio_decoder_func == nullptr)
		{
			destroy();
			return false;
		}
		m_create_audio_encoder_func = (pfn_rtx_codec__create_audio_encoder)m_module.bindSymbol("create_audio_encoder");
		if (m_create_audio_encoder_func == nullptr)
		{
			destroy();
			return false;
		}
		m_release_audio_encoder_func = (pfn_rtx_codec__release_audio_encoder)m_module.bindSymbol("release_audio_encoder");
		if (m_release_audio_encoder_func == nullptr)
		{
			destroy();
			return false;
		}
	}
	else
	{
		m_create_video_decoder_func = (pfn_rtx_codec__create_video_decoder)m_module.bindSymbol("create_video_decoder");
		if (m_create_video_decoder_func == nullptr)
		{
			destroy();
			return false;
		}
		m_release_video_decoder_func = (pfn_rtx_codec__release_video_decoder)m_module.bindSymbol("release_video_decoder");
		if (m_release_video_decoder_func == nullptr)
		{
			destroy();
			return false;
		}
		m_create_video_encoder_func = (pfn_rtx_codec__create_video_encoder)m_module.bindSymbol("create_video_encoder");
		if (m_create_video_encoder_func == nullptr)
		{
			destroy();
			return false;
		}
		m_release_video_encoder_func = (pfn_rtx_codec__release_video_encoder)m_module.bindSymbol("release_video_encoder");
		if (m_release_video_encoder_func == nullptr)
		{
			destroy();
			return false;
		}
	}

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtx_codec_module::destroy()
{
	if (m_freelib_func != nullptr)
	{
		m_freelib_func();

		m_module.unload();

		m_initlib_func = nullptr;
		m_freelib_func = nullptr;
		m_get_codec_list_func = nullptr;
		m_get_available_audio_payloads_func = nullptr;

		m_create_audio_decoder_func = nullptr;
		m_release_audio_decoder_func = nullptr;
		m_create_audio_encoder_func = nullptr;
		m_release_audio_encoder_func = nullptr;

		m_create_video_decoder_func = nullptr;
		m_release_video_decoder_func = nullptr;
		m_create_video_encoder_func = nullptr;
		m_release_video_encoder_func = nullptr;
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const char* rtx_codec_module::get_codec_list()
{
	return m_get_codec_list_func != nullptr ?
		m_get_codec_list_func() :
		"";
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtx_codec_module::get_available_audio_payloads(media::PayloadSet& set)
{
	return m_get_available_audio_payloads_func != nullptr ? 
		m_get_available_audio_payloads_func(set) :
		false;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//bool rtx_codec_module::create_log(const char* log_path)
//{
//	if (m_create_log_func == nullptr)
//	{
//		return false;
//	}
//
//	return m_create_log_func(log_path);
//}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//void rtx_codec_module::destroy_log()
//{
//	if (m_destroy_log_func == nullptr)
//	{
//		return;
//	}
//
//	m_destroy_log_func();
//}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
mg_audio_decoder_t* rtx_codec_module::create_audio_decoder(const char* encoding, const media::PayloadFormat* format)
{
	if (m_create_audio_decoder_func == nullptr)
	{
		return nullptr;
	}

	return m_create_audio_decoder_func(format);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtx_codec_module::release_audio_decoder(mg_audio_decoder_t* decoder)
{
	if (m_release_audio_decoder_func == nullptr)
	{
		return;
	}

	return m_release_audio_decoder_func(decoder);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
mg_audio_encoder_t* rtx_codec_module::create_audio_encoder(const char* encoding, const media::PayloadFormat* format)
{
	if (m_create_audio_encoder_func == nullptr)
	{
		return nullptr;
	}

	return m_create_audio_encoder_func(format);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtx_codec_module::release_audio_encoder(mg_audio_encoder_t* encoder)
{
	if (m_release_audio_encoder_func == nullptr)
	{
		return;
	}

	return m_release_audio_encoder_func(encoder);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//bool rtx_codec_module::get_video_codec_info(const PayloadFormat* format, mmt_image_t* info)
//{
//	if (m_get_video_codec_info_func == nullptr)
//	{
//		return nullptr;
//	}
//
//	return m_get_video_codec_info_func(format, info);
//}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
mg_video_decoder_t* rtx_codec_module::create_video_decoder(const char* encoding, const media::PayloadFormat* format)
{
	if (m_create_video_decoder_func == nullptr)
	{
		return nullptr;
	}

	return m_create_video_decoder_func(format);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtx_codec_module::release_video_decoder(mg_video_decoder_t* decoder)
{
	if (m_release_video_decoder_func == nullptr)
	{
		return;
	}

	return m_release_video_decoder_func(decoder);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
mg_video_encoder_t* rtx_codec_module::create_video_encoder(const char* encoding, const media::PayloadFormat* format, rtx_video_encoder_cb_t cb, void* usr)
{
	return m_create_video_encoder_func != nullptr ?
		m_create_video_encoder_func(format, cb, usr) :
		nullptr;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtx_codec_module::release_video_encoder(mg_video_encoder_t* encoder)
{
	if (m_release_video_encoder_func != nullptr)
	{
		m_release_video_encoder_func(encoder);
	}
}
//------------------------------------------------------------------------------
