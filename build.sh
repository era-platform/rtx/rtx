#!/usr/bin/env bash

set -e -x
export SHELLOPTS

cd `dirname $0`

UNAME_S=$(uname -s)

#### steps ####

initial_clean()
{
    git clean -dfx
}

build_rtx()
{
    if [ $UNAME_S = Linux ]; then
        make
    elif [ $UNAME_S = FreeBSD ]; then
        gmake
    fi
}

test_rtx_media_tools()
{
    mkdir -p _test
    (cd build && LD_LIBRARY_PATH=. ./rtx_media_tools_test toutput=../_test tlog=logs/ vecpath=../../rtx_assets/test_data)
}

test_rtx_megaco()
{
    mkdir -p _test
    (cd build && LD_LIBRARY_PATH=. ./rtx_megaco_test toutput=../_test tlog=logs/ vecpath=../../rtx_assets/test_data)
}

test_rtx_media_engine()
{
    mkdir -p _test
    (cd build && LD_LIBRARY_PATH=. ./rtx_media_engine_test toutput=../_test tiface=127.0.0.1 tlog=logs/ vecpath=../../rtx_assets/test_data)
}

test_rtx_mixer()
{
    mkdir -p _test
    # rtx_mixer_test ignores the 'tlog=' setting and just writes to the current dir, so: run it from the build/logs directory!
    (cd build/logs && LD_LIBRARY_PATH=.. ../rtx_mixer_test -t --'gtest_filter=test_file_mixer*.*' toutput=../../_test tlog=. vecpath=../../../rtx_assets/test_data)
}

finalize()
{
    rm -rf _build && mkdir -p _build && cp -rp build/* _build
    rm -rf _build/logs _build/*.xml _build/libgmock.so _build/libgtest.so _build/*_test
    (cd _build &&
        for binlib in *.so rtx_mg3 rtx_mixer; do
            echo ---- $binlib ----
            LD_LIBRARY_PATH=. ldd $binlib || true
            #SYS_LIBS=`LD_LIBRARY_PATH=. ldd $binlib | grep ' /lib/' | sed -e 's|^.*/lib/|/lib/|' -e 's/ .*$//'`
            #SYS_LIBS=`LD_LIBRARY_PATH=. ldd $binlib | grep -E ' /lib/.*lib(crypto|ssl|lzma)\.' | sed -e 's|^.*/lib/|/lib/|' -e 's/ .*$//'`
            USR_LIBS=`LD_LIBRARY_PATH=. ldd $binlib | grep ' /usr/lib/' | sed -e 's|^.*/usr/lib/|/usr/lib/|' -e 's/ .*$//'`
            LOCAL_LIBS=`LD_LIBRARY_PATH=. ldd $binlib | grep ' /usr/local/lib/' | sed -e 's|^.*/usr/local/lib/|/usr/local/lib/|' -e 's/ .*$//'`
            for lib in $SYS_LIBS $USR_LIBS $LOCAL_LIBS; do
                cp -p $lib .
            done
        done
    )
    rm -rf _build/libvpx.so.3
    set -x
    (cd _build && chmod -x *.so *.so.* && chmod +x rtx_mg3 rtx_mixer)
}

#### processing ####

func_exists()
{
    if [ -n "`LC_ALL=C type -t $1`" -a "`LC_ALL=C type -t $1`" = function ]; then
        return 0
    fi
    return 1
}

if [ x"$2" != x ]; then
    echo "Only one argument is expected!"
    exit 2
fi

if [ $UNAME_S = FreeBSD ]; then
    # temporarily skip tests on FreeBSD (until they are working).
    SKIP_RTX_TESTS=1
fi

if [ x"$1" = x"build" ]; then
    build_rtx
elif [ x"$1" = x"test" ]; then
    test_rtx_media_tools
    test_rtx_megaco
    test_rtx_media_engine
    test_rtx_mixer
elif [ x"$1" = x"dist" ]; then
    finalize
elif [ x"$1" = x ]; then
    #initial_clean
    build_rtx
    if [ x"$SKIP_RTX_TESTS" = x"" ]; then
        test_rtx_media_tools
        test_rtx_megaco
        test_rtx_media_engine
        test_rtx_mixer
    else
        echo "==== Skipping RTX tests. ===="
    fi
    finalize
else
    if func_exists $1; then
        $1
    else
        echo "Function/step '$1' does not exist."
        exit 1
    fi
fi
