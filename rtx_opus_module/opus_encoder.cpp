/* RTX H.248 Media Gate Opus codec wrapper
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "opus_encoder.h"
//--------------------------------------
//
//--------------------------------------
opus_encoder_t::opus_encoder_t() : m_ctx(nullptr), m_frame_size(0), m_dynamic_payload_id(96)
{
	m_last_ts = 0;
}
//--------------------------------------
//
//--------------------------------------
opus_encoder_t::~opus_encoder_t()
{
	destroy();
}
//--------------------------------------
//
//--------------------------------------
bool opus_encoder_t::initialize(const media::PayloadFormat& format)
{
	int result = 0; 

	m_last_ts = 0;

	destroy();

	m_dynamic_payload_id = format.getId_u8();
	m_frame_size = (format.getFrequency() / 1000) * 20;

	// useinbandfec = 1;
	// usedtx = 1;
	// maxaveragebitrate = 64000

	m_ctx = opus_encoder_create(format.getFrequency(), format.getChannelCount(), OPUS_APPLICATION_VOIP, &result);

	if (result != OPUS_OK || m_ctx == nullptr)
	{
		opus_encoder_destroy(m_ctx);
		return false;
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
void opus_encoder_t::destroy()
{
	if (m_ctx != nullptr)
	{
		opus_encoder_destroy(m_ctx);
		m_ctx = nullptr;
	}
}
//--------------------------------------
//
//--------------------------------------
const char* opus_encoder_t::getEncoding()
{
	return "opus";
}
//--------------------------------------
//
//--------------------------------------
uint32_t opus_encoder_t::encode(const uint8_t* buff_from, uint32_t size_from, uint8_t* buff_to, uint32_t size_to)
{
	int res = opus_encode(m_ctx, (short*)buff_from, size_from/sizeof(short), buff_to, size_to);

	if (res <= 0)
	{
		// process error
		return 0;
	}

	return res;
}
//--------------------------------------
//
//--------------------------------------
uint32_t opus_encoder_t::packetize(rtp_packet* packet, const uint8_t* buff, uint32_t buff_size)
{
	if (packet == nullptr)
	{
		return 0;
	}

	if (buff == nullptr)
	{
		return 0;
	}

	if (buff_size > rtp_packet::PAYLOAD_BUFFER_SIZE)
	{
		return 0;
	}

	if (packet->set_payload(buff, buff_size) == 0)
	{
		return 0;
	}

	packet->set_payload_type(m_dynamic_payload_id);

	packet->set_samples(960);

	if (m_last_ts == 0)
	{
		m_last_ts = packet->getTimestamp();
	}
	m_last_ts += 960;
	
	packet->set_timestamp(m_last_ts);

	return buff_size;
}
//--------------------------------------
//
//--------------------------------------
uint32_t opus_encoder_t::get_frame_size()
{
	return m_frame_size * sizeof(short);
}
//--------------------------------------
//
//--------------------------------------
//void opus_encoder_t::setFmtp(const char* fmtp)
//{
	//if (fmtp == nullptr)
	//	return;

	//const char* ptr = fmtp;
	//const char* cp;

	//char param[256];

	//while ((cp = strchr(ptr, ';')) != nullptr)
	//{
	//	size_t idx = cp - ptr;
	//	if (idx >= 265) idx = 255;
	//	strncpy(param, ptr, idx);
	//	param[idx] = 0;
	//	set_parameter(param);
	//	ptr = cp + 1;
	//}

	//// last value? may be
	//if (ptr != nullptr && *ptr != 0)
	//{
	//	size_t idx = strlen(ptr);
	//	if (idx >= 265) idx = 255;
	//	strncpy(param, ptr, idx);
	//	param[idx] = 0;
	//	set_parameter(param);
	//}
//}
//--------------------------------------
//
//--------------------------------------
void opus_encoder_t::set_parameter(char* param)
{
	char* eq = strchr(param, '=');

	if (eq)
	{
		// param with value
		*eq++ = 0;
	}

	set_parameter(param, eq);
}
//--------------------------------------
//
//--------------------------------------
void opus_encoder_t::set_parameter(char* name, char* value)
{
	uint32_t val_int = (uint32_t)strtoul(value, nullptr, 10);

	//if (std_stricmp(name, "maxplaybackrate") == 0)
	//{
	//	//opus_decoder_ctl(m_ctx, )
	//}
	//else if (std_stricmp(name, "sprop-maxcapturerate") == 0)
	//{

	//}
	//else
	if (std_stricmp(name, "maxaveragebitrate") == 0)
	{
		opus_encoder_ctl(m_ctx, OPUS_SET_MAX_BANDWIDTH(val_int)); // _REQUEST, _atoi
	}
	//else if (std_stricmp(name, "stereo") == 0)
	//{

	//}
	//else if (std_stricmp(name, "sprop-stereo") == 0)
	//{

	//}
	//else if (std_stricmp(name, "cbr") == 0)
	//{

	//}
	else if (std_stricmp(name, "useinbandfec") == 0)
	{
		opus_encoder_ctl(m_ctx, OPUS_SET_INBAND_FEC(val_int)); // _REQUEST, atoi
	}
	else if (std_stricmp(name, "usedtx") == 0)
	{
		opus_encoder_ctl(m_ctx, OPUS_SET_DTX(val_int)); // _REQUEST, atoi
	}
}
//--------------------------------------
