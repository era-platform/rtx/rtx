/* RTX H.248 Media Gate Opus codec wrapper
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "opus_decoder.h"

opus_decoder_t::opus_decoder_t() : m_ctx(nullptr)
{
}

opus_decoder_t::~opus_decoder_t()
{
	destroy();
}

bool opus_decoder_t::initialize(const media::PayloadFormat& format)
{
	int result = 0;

	m_frame_size = (format.getFrequency() / 1000) * 20;
	m_dynamic_payload_id = format.getId_u8();

	m_ctx = opus_decoder_create(format.getFrequency(), format.getChannelCount(), &result);

	if (result != OPUS_OK || m_ctx == nullptr)
	{
		opus_decoder_destroy(m_ctx);
		return false;
	}

	//parse_format(format.getFmtp());

	return true;
}

//void opus_decoder_t::parse_format(const char* fmtp)
//{
//	// ...
//}

void opus_decoder_t::destroy()
{
	if (m_ctx != nullptr)
	{
		opus_decoder_destroy(m_ctx);
		m_ctx = nullptr;	
	}
}

const char* opus_decoder_t::getEncoding()
{
	return "opus";
}

uint32_t opus_decoder_t::depacketize(const rtp_packet* packet, uint8_t* buff, uint32_t buff_size)
{
	if (packet == nullptr)
	{
		return 0;
	}

	if (buff == nullptr)
	{
		return 0;
	}

	if (buff_size < packet->get_payload_length())
	{
		return 0;
	}

	uint32_t payload_length = packet->get_payload_length();

	memcpy(buff, packet->get_payload(), payload_length);

	return payload_length;
}

uint32_t opus_decoder_t::decode(const uint8_t* buff_from, uint32_t size_from, uint8_t* buff_to, uint32_t size_to)
{
	return opus_decode(m_ctx, buff_from, size_from, (short*)buff_to, size_to/sizeof(short), 0) * sizeof(short);
}

uint32_t opus_decoder_t::get_frame_size_pcm()
{
	return m_frame_size * sizeof(short);
}

uint32_t opus_decoder_t::get_frame_size_cod()
{
	return 0;
}
