/* RTX H.248 Media Gate Opus codec wrapper
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtx_opus_module.h"
#include "std_logger.h"
#include "opus_encoder.h"
#include "opus_decoder.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#if defined (DEBUG_MEMORY)
void* operator new (size_t size, char* file, int line)
{
	return std_mem_checker_malloc(size, file, line, rc_malloc_new_e);
}
void* operator new[](size_t size, char* file, int line)
{
	return std_mem_checker_malloc(size, file, line, rc_malloc_newar_e);
}
void operator delete (void* mem_ptr)
{
	std_mem_checker_free(mem_ptr, rc_malloc_delete_e);
}
void operator delete[](void* mem_ptr)
{
	std_mem_checker_free(mem_ptr, rc_malloc_deletear_e);
}
#endif
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_OPUS_MODULE_API const char* get_codec_list()
{
	return "opus";
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_OPUS_MODULE_API bool get_available_audio_payloads(media::PayloadSet& set)
{
	set.addFormat("opus", media::PayloadId::Dynamic, 8000, 1);

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_OPUS_MODULE_API bool initlib()
{
	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_OPUS_MODULE_API void freelib()
{
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_OPUS_MODULE_API mg_audio_decoder_t* create_audio_decoder(const media::PayloadFormat& format)
{
	rtl::String codec_name(format.getEncoding());
	if (codec_name.isEmpty())
	{
		return nullptr;
	}
	codec_name.trim();
	codec_name.toLower();

	if (rtl::String::compare("opus", codec_name, false) == 0)
	{
		opus_decoder_t* decoder = NEW opus_decoder_t();
		if (decoder->initialize(format))
		{
			return decoder;
		}

		DELETEO(decoder);
	}

	return nullptr;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_OPUS_MODULE_API void release_audio_decoder(mg_audio_decoder_t* decoder)
{
	if (decoder == nullptr)
	{
		return;
	}

	rtl::String type(decoder->getEncoding());
	type.toLower();

	if (rtl::String::compare("opus", type, false) == 0)
	{
		opus_decoder_t* opus_decoder = (opus_decoder_t*)decoder;
		if (opus_decoder != nullptr)
		{
			opus_decoder->destroy();
			DELETEO(opus_decoder);
		}
	}

}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_OPUS_MODULE_API mg_audio_encoder_t* create_audio_encoder(const media::PayloadFormat& format)
{
	/*if (param == nullptr)
	{
	return nullptr;
	}*/

	rtl::String codec_name(format.getEncoding());
	if (codec_name.isEmpty())
	{
		return nullptr;
	}
	codec_name.trim();
	codec_name.toLower();

	if (rtl::String::compare("opus", codec_name, false) == 0)
	{
		opus_encoder_t* encoder = NEW opus_encoder_t();
		if (encoder->initialize(format))
		{
			return encoder;
		}
	}

	return nullptr;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_OPUS_MODULE_API void release_audio_encoder(mg_audio_encoder_t* encoder)
{
	if (encoder == nullptr)
	{
		return;
	}

	rtl::String type(encoder->getEncoding());
	type.toLower();

	if (rtl::String::compare("opus", type, false) == 0)
	{
		opus_encoder_t* opus_encoder = (opus_encoder_t*)encoder;
		if (opus_encoder != nullptr)
		{
			opus_encoder->destroy();
			DELETEO(opus_encoder);
		}
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
