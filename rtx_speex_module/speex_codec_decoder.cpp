﻿/* RTX H.248 Media Gate Speex Codec
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "speex_codec_decoder.h"
#include "speex/speex.h"
#include "speex/speex_header.h"
#include "speex/speex_stereo.h"
#include "speex/speex_callbacks.h"
//------------------------------------------------------------------------------
// This explains the values that need to be set prior to initialization in
// order to control various encoding parameters.
//
// Channels
//     Speex only supports mono or stereo, so channels must be set to 1 or 2.
//
// Sample Rate / Encoding Mode
//     Speex has 3 modes, each of which uses a specific sample rate.
//         narrowband     :  8 kHz
//         wideband       : 16 kHz
//         ultra-wideband : 32 kHz
//     sample_rate must be set to one of these 3 values.  This will be
//     used to set the encoding mode.
//
// Variable bit-rate (VBR)
//	   allows a codec to change its bit-rate dynamically to adapt to the ``difficulty''
//	   of the audio being encoded. In the example of Speex, sounds like vowels and 
//	   high-energy transients require a higher bit-rate to achieve good quality, while 
//	   fricatives (e.g. s,f sounds) can be coded adequately with less bits. For this 
//	   reason, VBR can achive lower bit-rate for the same quality, or a better quality
//	   for a certain bit-rate. Despite its advantages, VBR has two main drawbacks: 
//	   first, by only specifying quality, there's no guaranty about the final average 
//	   bit-rate. Second, for some real-time applications like voice over IP (VoIP), 
//	   what counts is the maximum bit-rate, which must be low enough for the 
//	   communication channel.
//
// Complexity
//     Encoding complexity is controlled by setting avctx->compression_level.
//     The valid range is 0 to 10.  A higher setting gives generally better
//     quality at the expense of encoding speed.  This does not affect the
//     bit rate.
//
// Frames-per-Packet
//     The encoder defaults to using 1 frame-per-packet.  However, it is
//     sometimes desirable to use multiple frames-per-packet to reduce the
//     amount of container overhead.  This can be done by setting the
//     'frames_per_packet' option to a value 1 to 8.
//
//
// Optional features
// Speex encoder supports several optional features, which can be useful
// for some conditions.
//
// Voice Activity Detection
//     When enabled, voice activity detection detects whether the audio
//     being encoded is speech or silence/background noise. VAD is always
//     implicitly activated when encoding in VBR, so the option is only useful
//     in non-VBR operation. In this case, Speex detects non-speech periods and
//     encodes them with just enough bits to reproduce the background noise.
//
// Discontinuous Transmission (DTX)
//     DTX is an addition to VAD/VBR operation, that allows to stop transmitting
//     completely when the background noise is stationary.
//     In file-based operation only 5 bits are used for such frames.
//------------------------------------------------------------------------------
#define MAX_BLOCK_SIZE 320
//------------------------------------------------------------------------------
#define LOG_PREFIX "SPEEX"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
DECLARE int g_rtx_speex_decoder_counter = -1;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtx_speex_codec_decoder::rtx_speex_codec_decoder(rtl::Logger* log) :
	m_log(log)
{
	m_decoder = nullptr;
	m_bits = nullptr;

	m_channels = 0;
	m_sample_rate = 0;
	//m_bit_rate = 0;

	m_skip_group_delay = 0;

	rtl::res_counter_t::add_ref(g_rtx_speex_decoder_counter);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtx_speex_codec_decoder::~rtx_speex_codec_decoder()
{
	destroy();

	rtl::res_counter_t::release(g_rtx_speex_decoder_counter);
}
//------------------------------------------------------------------------------
// Channels :		1 or 2.
// Sample Rate :	8, 16, 32 kHz
// Bitrate Range: 
//					8 kHz	: 2400 - 25600 bps
//					16 kHz	: 4000 - 43200 bps
//					32 kHz	: 4400 - 45200 bps
//------------------------------------------------------------------------------
bool rtx_speex_codec_decoder::initialize(const media::PayloadFormat& format)
{
	/*if (!parse_param(format))
	{
		PLOG_MEDIA_ERROR(LOG_PREFIX, "mg_audio_decoder_initialize -- wrong param.");
		return false;
	}*/
	m_dynamic_payload_id = format.getId_u8();
	m_channels = format.getChannelCount();
	m_sample_rate = format.getFrequency();

	if (m_sample_rate == 8000)
	{
		if (!init(SPEEX_MODEID_NB, m_sample_rate/*, m_bit_rate, (m_vbr) ? 1 : 0, 1, 1*/))
		{
			PLOG_MEDIA_ERROR(LOG_PREFIX, "mg_audio_decoder_initialize -- init_nb fail");
			destroy();
			return false;
		}
	}
	else if (m_sample_rate == 16000)
	{
		if (!init(SPEEX_MODEID_WB, m_sample_rate/*, m_bit_rate, (m_vbr) ? 1 : 0, 1, 3*/))
		{
			PLOG_MEDIA_ERROR(LOG_PREFIX, "mg_audio_decoder_initialize -- init_wb fail");
			destroy();
			return false;
		}
	}
	else if (m_sample_rate == 32000)
	{
		if (!init(SPEEX_MODEID_UWB, m_sample_rate/*, m_bit_rate, (m_vbr) ? 1 : 0, 0, 1*/))
		{
			PLOG_MEDIA_ERROR(LOG_PREFIX, "mg_audio_decoder_initialize -- init_uwb fail");
			destroy();
			return false;
		}
	}
	else
	{
		PLOG_MEDIA_ERROR(LOG_PREFIX, "mg_audio_decoder_initialize -- wrong sample rate value %d", m_sample_rate);
		destroy();
		return false;
	}

	m_frame_size = 20 * m_sample_rate / 1000;

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t rtx_speex_codec_decoder::get_frame_size_pcm()
{
	return m_frame_size * sizeof(uint16_t);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t rtx_speex_codec_decoder::get_frame_size_cod()
{
	return m_sample_rate == 8000 ?
		20 :
		m_sample_rate == 16000 ?
			70 :
			74;
}
//------------------------------------------------------------------------------
//пример в SDP:
//m=audio 8088 RTP/AVP 97 98
//a=rtmap:97 speex/16000
//a=fmtp:97 mode="10,any"
//a=rtmap:98 speex/8000
//a=fmtp:98 mode="7,any"
//------------------------------------------------------------------------------
//bool rtx_speex_codec_decoder::parse_param(const PayloadFormat& format)
//{
//	return true;
//}
//------------------------------------------------------------------------------
// пример: 97, 1, 8000, 16000, false
//------------------------------------------------------------------------------
//bool rtx_speex_codec_decoder::parse_param(const char* param)
//{
//	m_dynamic_payload_id = 97;
//	m_channels = 1;
//	m_sample_rate = 8000;
//	m_bit_rate = 15000;
//	m_vbr = false;
//
//	if (param == nullptr)
//	{
//		return true;
//	}
//
//	rtl::String str_param(param);
//	if (str_param.isEmpty())
//	{
//		return true;
//	}
//
//	uint32_t count_param = 1;
//	int last_index = 0;
//	while (count_param <= 5)
//	{
//		rtl::String item;
//		int index = str_param.indexOf(',', last_index);
//		if (index == BAD_INDEX)
//		{
//			if (count_param == 5)
//			{
//				item = str_param.substring(last_index);
//			}
//			else
//			{
//				PLOG_MEDIA_ERROR(LOG_PREFIX, "parse_param -- wrong param count.");
//				return false;
//			}
//		}
//		else
//		{
//			item = str_param.substring(last_index, index - last_index);
//			last_index = index + 1;
//			if (str_param[last_index] == ' ')
//			{
//				last_index++;
//			}
//		}
//
//		if (item.isEmpty())
//		{
//			PLOG_MEDIA_ERROR(LOG_PREFIX, "parse_param -- wrong params.");
//			return false;
//		}
//
//		item.trim();
//
//		if (count_param == 1)
//		{
//			m_dynamic_payload_id = (uint16_t)strtoul(item, nullptr, 10);
//		}
//		else if (count_param == 2)
//		{
//			m_channels = (uint16_t)strtoul(item, nullptr, 10);
//		}
//		else if (count_param == 3)
//		{
//			m_sample_rate = (uint16_t)strtoul(item, nullptr, 10);
//		}
//		else if (count_param == 4)
//		{
//			m_bit_rate = (uint16_t)strtoul(item, nullptr, 10);
//		}
//		else if (count_param == 5)
//		{
//			item.toLower();
//			m_vbr = rtl::String::compare("true", item, false) == 0;
//		}
//
//		count_param++;
//	}
//
//
//	if ((m_channels != 1) && (m_channels != 2))
//	{
//		PLOG_MEDIA_ERROR(LOG_PREFIX, "parse_param -- wrong channels");
//		return false;
//	}
//
//	if (m_sample_rate == 8000)
//	{
//		if ((m_bit_rate < 2400) || (m_bit_rate > 25600))
//		{
//			PLOG_MEDIA_ERROR(LOG_PREFIX, "parse_param -- wrong bitrate for sample_rate: %d", m_sample_rate);
//			return false;
//		}
//	}
//	else if (m_sample_rate == 16000)
//	{
//		if ((m_bit_rate < 4000) || (m_bit_rate > 43200))
//		{
//			PLOG_MEDIA_ERROR(LOG_PREFIX, "parse_param -- wrong bitrate for sample_rate: %d", m_sample_rate);
//			return false;
//		}
//	}
//	else if (m_sample_rate == 32000)
//	{
//		if ((m_bit_rate < 4400) || (m_bit_rate > 45200))
//		{
//			PLOG_MEDIA_ERROR(LOG_PREFIX, "parse_param -- wrong bitrate for sample_rate: %d", m_sample_rate);
//			return false;
//		}
//	}
//	else
//	{
//		PLOG_MEDIA_ERROR(LOG_PREFIX, "parse_param -- wrong sample_rate");
//		return false;
//	}
//
//	return true;
//}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void rtx_speex_codec_decoder::destroy()
{
	if (m_decoder != nullptr)
	{
		speex_decoder_destroy(m_decoder);
		m_decoder = nullptr;
	}
	if (m_bits != nullptr)
	{
		speex_bits_destroy(m_bits);
		DELETEO(m_bits);
		m_bits = nullptr;
	}

	m_channels = 0;
	m_sample_rate = 0;
	/*m_bit_rate = 0;
	m_vbr = false;*/

	m_skip_group_delay = 0;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const char* rtx_speex_codec_decoder::getEncoding()
{
	return "speex";
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t rtx_speex_codec_decoder::depacketize(const rtp_packet* packet, uint8_t* buff, uint32_t buff_size)
{
	if (packet == nullptr)
	{
		return 0;
	}

	if (buff == nullptr)
	{
		return 0;
	}

	uint32_t payload_length = packet->get_payload_length();
	if (buff_size < payload_length)
	{
		return 0;
	}
	
	memcpy(buff, packet->get_payload(), payload_length);

	return payload_length;
}
//------------------------------------------------------------------------------
// size_to 
// для 8000 Hz	: не меньше 320
// для 16000 Hz : не меньше 640
// для 32000 Hz : не меньше 1280
//------------------------------------------------------------------------------
uint32_t rtx_speex_codec_decoder::decode(const uint8_t* buff_from, uint32_t size_from, uint8_t* buff_to, uint32_t size_to)
{
	if ((buff_from == nullptr) || (buff_to == nullptr) || (size_from == 0) || (size_to == 0))
	{
		PLOG_MEDIA_ERROR(LOG_PREFIX, "mg_audio_decode-- wrong parameters");
		return 0;
	}

	if (m_decoder == nullptr)
	{
		PLOG_MEDIA_ERROR(LOG_PREFIX, "mg_audio_decode -- not initialized m_decoder");
		return 0;
	}

	uint32_t frame_size = 0;
	speex_decoder_ctl(m_decoder, SPEEX_GET_FRAME_SIZE, &frame_size);
	if (frame_size * sizeof(short) > size_to)
	{
		PLOG_MEDIA_ERROR(LOG_PREFIX, "speex_codec.decode -- to_length small");
		return 0;
	}

	speex_bits_read_from(m_bits, (char*)buff_from, size_from);
	short out[640] = {0}; // максимальный буфер для 32000 Hz

	speex_decode_int(m_decoder, m_bits, out);

	uint32_t size_left = frame_size - m_skip_group_delay;

	//memcpy((short*)to, out + m_skip_group_delay, size_left);
	for (uint32_t i = 0; i < size_left; i ++)
	{
		short s = out[i + m_skip_group_delay];
		*(((short*)buff_to) + i) = s;
	}
	
	m_skip_group_delay = 0;

	return size_left * sizeof(short);
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool rtx_speex_codec_decoder::init(int mode_id, uint32_t sample_rate/*, uint32_t bit_rate, int vbr, int ehn, int complexity*/)
{
	const SpeexMode* mode = speex_lib_get_mode(mode_id);
	if (mode == nullptr)
	{
		return false;
	}

	m_decoder = speex_decoder_init(mode);
	if (m_decoder == nullptr)
	{
		return false;
	}

	speex_decoder_ctl(m_decoder, SPEEX_SET_SAMPLING_RATE, &sample_rate);

	int ehn = 1;
	speex_decoder_ctl(m_decoder, SPEEX_SET_ENH, &ehn);

	int temp = 0;
	speex_decoder_ctl(m_decoder, SPEEX_GET_LOOKAHEAD, &temp);
	m_skip_group_delay += temp;

	//temp = 1;
	//speex_decoder_ctl(m_decoder, SPEEX_SET_HIGHPASS, &temp);

	if (m_bits == nullptr)
	{
		m_bits = NEW SpeexBits();
	}
	speex_bits_init(m_bits);
	if (m_bits == nullptr)
	{
		return false;
	}

	return true;
}
//------------------------------------------------------------------------------
