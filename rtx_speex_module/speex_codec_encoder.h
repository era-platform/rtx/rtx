﻿/* RTX H.248 Media Gate Speex Codec
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_audio_codec.h"
#include "std_logger.h"

 //------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
extern int g_rtx_speex_encoder_counter;
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
class rtx_speex_codec_encoder : public mg_audio_encoder_t
{
	rtx_speex_codec_encoder(const rtx_speex_codec_encoder&);
	rtx_speex_codec_encoder& operator=(const rtx_speex_codec_encoder&);

public:
	rtx_speex_codec_encoder(rtl::Logger* log);
	virtual	~rtx_speex_codec_encoder();

	// payload_id, channels, sample_rate, bit_rate, vbr
	// пример: 97, 1, 8000, 16000, false
	bool initialize(const media::PayloadFormat& format);
	void destroy();

	virtual const char* getEncoding();
	virtual uint32_t encode(const uint8_t* buff_from, uint32_t size_from, uint8_t* buff_to, uint32_t size_to);
	virtual uint32_t packetize(rtp_packet* packet, const uint8_t* buff, uint32_t buff_size);
	virtual uint32_t get_frame_size();

private:
	void				parse_mode(const rtl::String& value);
	void				parse_vbr(const rtl::String& value);
	void				parse_cng(const rtl::String& value);
	void				parse_param(const rtl::String& param);
	bool				parse_fmtp(const media::PayloadFormat& format);
	//bool				parse_fmtp(const char* param);
	bool				init(int mode_id/*, uint32_t sample_rate, uint32_t bit_rate, int vbr, int ehn*/, int complexity);

private:
	rtl::Logger*				m_log;

	void*				m_encoder;
	struct SpeexBits*	m_bits;

	uint32_t			m_channels;
	uint32_t			m_sample_rate;
	uint32_t			m_frame_size;
	uint32_t			m_bit_rate;
	int					m_vbr;	// variable bit rate
	int					m_vad;	// silence detector
	int					m_cng;	// comfort noise

	uint32_t			m_dynamic_payload_id;

	uint32_t			m_skip_group_delay;
};
//------------------------------------------------------------------------------
