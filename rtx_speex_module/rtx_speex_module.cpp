﻿/* RTX H.248 Media Gate Speex Codec
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtx_speex_module.h"
#include "std_logger.h"
#include "speex_codec_decoder.h"
#include "speex_codec_encoder.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#if defined (DEBUG_MEMORY)
void* operator new (size_t size, char* file, int line)
{
	return std_mem_checker_malloc(size, file, line, rc_malloc_new_e);
}
void* operator new[](size_t size, char* file, int line)
{
	return std_mem_checker_malloc(size, file, line, rc_malloc_newar_e);
}
void operator delete (void* mem_ptr)
{
	std_mem_checker_free(mem_ptr, rc_malloc_delete_e);
}
void operator delete[](void* mem_ptr)
{
	std_mem_checker_free(mem_ptr, rc_malloc_deletear_e);
}
#endif
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_SPEEX_MODULE_API const char* get_codec_list()
{
	return "speex";
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_SPEEX_MODULE_API bool get_available_audio_payloads(media::PayloadSet& set)
{
	set.addFormat("speex", media::PayloadId::Dynamic, 8000, 1);
	set.addFormat("speex", media::PayloadId::Dynamic, 16000, 1);
	set.addFormat("speex", media::PayloadId::Dynamic, 32000, 1);

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_SPEEX_MODULE_API bool initlib()
{
	g_rtx_speex_decoder_counter = rtl::res_counter_t::add("mge_speex_decoder");
	g_rtx_speex_encoder_counter = rtl::res_counter_t::add("mge_speex_encoder");

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_SPEEX_MODULE_API void freelib()
{
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_SPEEX_MODULE_API mg_audio_decoder_t* create_audio_decoder(const media::PayloadFormat& format)
{
	rtl::String codec_name(format.getEncoding());
	if (codec_name.isEmpty())
	{
		return nullptr;
	}
	codec_name.trim();
	codec_name.toLower();

	if (rtl::String::compare("speex", codec_name, false) == 0)
	{
		rtx_speex_codec_decoder* decoder = NEW rtx_speex_codec_decoder(&Err);
		if (decoder->initialize(format))
		{
			return decoder;
		}

		DELETEO(decoder);
	}

	return nullptr;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_SPEEX_MODULE_API void release_audio_decoder(mg_audio_decoder_t* decoder)
{
	if (decoder == nullptr)
	{
		return;
	}

	rtl::String type(decoder->getEncoding());
	type.toLower();

	if (rtl::String::compare("speex", type, false) == 0)
	{
		rtx_speex_codec_decoder* speex_decoder = (rtx_speex_codec_decoder*)decoder;
		if (speex_decoder != nullptr)
		{
			speex_decoder->destroy();
			DELETEO(speex_decoder);
			speex_decoder = nullptr;
		}
	}
	
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_SPEEX_MODULE_API mg_audio_encoder_t* create_audio_encoder(const media::PayloadFormat& format)
{
	rtl::String codec_name(format.getEncoding());
	if (codec_name.isEmpty())
	{
		return nullptr;
	}
	codec_name.trim();
	codec_name.toLower();

	if (rtl::String::compare("speex", codec_name, false) == 0)
	{
		rtx_speex_codec_encoder* encoder = NEW rtx_speex_codec_encoder(&Err);
		if (encoder->initialize(format))
		{
			return encoder;
		}
	}

	return nullptr;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_SPEEX_MODULE_API void release_audio_encoder(mg_audio_encoder_t* encoder)
{
	if (encoder == nullptr)
	{
		return;
	}

	rtl::String type(encoder->getEncoding());
	type.toLower();

	if (rtl::String::compare("speex", type, false) == 0)
	{
		rtx_speex_codec_encoder* speex_encoder = (rtx_speex_codec_encoder*)encoder;
		if (speex_encoder != nullptr)
		{
			speex_encoder->destroy();
			DELETEO(speex_encoder);
			speex_encoder = nullptr;
		}
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
