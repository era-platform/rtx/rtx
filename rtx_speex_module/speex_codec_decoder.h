﻿/* RTX H.248 Media Gate Speex Codec
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_audio_codec.h"
#include "std_logger.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
extern int g_rtx_speex_decoder_counter;
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
class rtx_speex_codec_decoder : public mg_audio_decoder_t
{
	rtx_speex_codec_decoder(const rtx_speex_codec_decoder&);
	rtx_speex_codec_decoder& operator=(const rtx_speex_codec_decoder&);

public:
	rtx_speex_codec_decoder(rtl::Logger* log);
	virtual	~rtx_speex_codec_decoder();

	// payload_id, channels, sample_rate, bit_rate, vbr
	// пример: 97, 1, 8000, 16000, false
	bool initialize(const media::PayloadFormat& format);
	void destroy();

	virtual const char* getEncoding();
	virtual uint32_t depacketize(const rtp_packet* packet, uint8_t* buff, uint32_t buff_size);
	virtual uint32_t decode(const uint8_t* buff_from, uint32_t size_from, uint8_t* buff_to, uint32_t size_to);
	virtual uint32_t get_frame_size_pcm();
	virtual uint32_t get_frame_size_cod();

private:
	//bool				parse_param(const PayloadFormat& format);
	//bool				parse_param(const char* param);
	bool				init(int mode_id, uint32_t sample_rate/*, uint32_t bit_rate, int vbr, int ehn, int complexity*/);

private:
	rtl::Logger*				m_log;

	void*				m_decoder;
	struct SpeexBits*	m_bits;

	uint32_t			m_channels;
	uint32_t			m_sample_rate;
	uint32_t			m_frame_size;
	//uint32_t			m_bit_rate;
	//bool				m_vbr;

	uint32_t			m_dynamic_payload_id;

	uint32_t			m_skip_group_delay;
};
//------------------------------------------------------------------------------
