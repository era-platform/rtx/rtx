﻿/* RTX H.248 Media Gate. Internal tester
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "std_sys_env.h"
#include "std_string.h"
#include "std_module.h"
#include "rtx_stdlib.h"
#include "sip/sip_uri.h"
#include "sip/sip_value_uri.h"
#include "std_json.h"
#include "mmt_video_frame.h"
#include "mmt_video_font_man.h"

using namespace rtl;
//---------------------------------------------------------------------
//
//---------------------------------------------------------------------
#if defined (DEBUG_MEMORY)
void* operator new (size_t size, char* file, int line)
{
	return std_mem_checker_malloc(size, file, line, rc_malloc_new_e);
}
void* operator new[](size_t size, char* file, int line)
{
	return std_mem_checker_malloc(size, file, line, rc_malloc_newar_e);
}
void operator delete (void* mem_ptr)
{
	std_mem_checker_free(mem_ptr, rc_malloc_delete_e);
}
void operator delete[](void* mem_ptr)
{
	std_mem_checker_free(mem_ptr, rc_malloc_deletear_e);
}
#endif
//---------------------------------------------------------------------
//
//---------------------------------------------------------------------
static int run_modile_tests(int argc, char* argv[]);
static int run_json_test();
static int run_frame_gen_test();

static void SuperPuperFunction()
{
	const char* name = "999";

#define PRINT(fmt, ...) printf("id(%s) : " __FUNCTION__ " : " fmt, name, __VA_ARGS__)

	PRINT("arg: %s %s%c\r\n", "Hello", "World", '!');

}

class ClassABC
{
public:
	ClassABC() { }
	void PRINTF()
	{
		printf("called '%s' function...\n", __func__);
		printf("called '%s' function...\n", __FUNCTION__);
	}
};

//---------------------------------------------------------------------
//
//---------------------------------------------------------------------
int main(int argc, char* argv[])
{
	//rtl::FileStream bmp;

	ClassABC obj;

	obj.PRINTF();

	//SuperPuperFunction();

	//bmp.open("c:\\temp\\bitmap_test\\step_4.bmp");
	//int file_length = bmp.getLength();
	//uint8_t* buffer = NEW uint8_t[file_length];
	//file_length = bmp.read(buffer, file_length);
	//int string_length = Base64_CalcEncodedSize(file_length);
	//char* string = NEW char[string_length + 2];
	//string_length = Base64Encode(buffer, file_length, string, string_length);
	//rtl::FileStream text;
	//text.create("c:\\temp\\bitmap_test\\step_4_64.txt");
	//text.write(string, string_length);

	//bmp.close();
	//text.close();

	//return 0;
	//return run_json_test();

	//media::printDefFont();
	//media::printDefFontInfo();
	//media::printFont(argv[1]);
	//media::testBitmapText("c:\\temp\\test.bmp");
	//media::testBitmapText2("c:\\temp\\test.bmp");
	//media::testBitmapChars("c:\\temp\\test.bmp");
	//run_frame_gen_test();
	// return run_modile_tests(argc, argv);

	return 0;
}
//---------------------------------------------------------------------
//
//---------------------------------------------------------------------
typedef	int(*pfn_tests_run)(int argc, char* argv[]);
//---------------------------------------------------------------------
//
//---------------------------------------------------------------------
struct tests_module_t
{
	rtl::Module module;
	rtl::String module_name;
	pfn_tests_run run_fun;
};
//---------------------------------------------------------------------
//
//---------------------------------------------------------------------
static int run_modile_tests(int argc, char* argv[])
{
	printf("RTX TEST STARTED\n");

	char apppath[256] = { 0 };

	rtl::String path;
	rtl::String mask;

	if (std_get_application_startup_path(apppath, 256))
	{
		path = apppath;
	}
	else
	{
		printf("RTX TEST WRONG path\n");
		return -1;
	}

	if (argc > 7)
	{
		printf("RTX TEST WRONG argc\n");
		return -2;
	}

#if defined(TARGET_OS_WINDOWS)
	// mask = path + "\\rtx_*.dll"; TODO test
	mask = path + "\\rtx_*.dll";
#else
	mask = path + "/librtx_*.so";
#endif
	rtl::ArrayT<fs_entry_info_t> filelist;

	bool output = false;
	int newArgc = 0;
	char* newArgs[] = { nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr };
	for (int i = 0; i < argc; i++)
	{
		rtl::String str(argv[i]);
		if (str.indexOf("--gtest_output=xml:") != BAD_INDEX)
		{
			output = true;
		}
		newArgs[newArgc] = argv[i];
		newArgc++;
	}

	if (!output)
	{
		char newParam[512] = { 0 };
		rtl::String testResultFile = "toutput=" + path;
		strcpy(newParam, testResultFile);
		newArgs[newArgc] = newParam;
		newArgc++;
	}

	printf("RTX TEST mask: %s\n", (const char*)mask);
	fs_directory_get_entries(mask, filelist);

	for (int i = 0; i < filelist.getCount(); i++)
	{
		fs_entry_info_t& params = filelist.getAt(i);

#if defined(TARGET_OS_WINDOWS)
		mask = path + FS_PATH_DELIMITER + params.fs_filename;
#else
		mask = params.fs_filename;
#endif
		printf("RTX TEST module load: %s\n", (const char*)mask);

		if (mask.indexOf("rtx_") == BAD_INDEX ||
#if defined TARGET_OS_WINDOWS
			mask.indexOf(".dll") == BAD_INDEX)
#else
			mask.indexOf(".so") == BAD_INDEX)
#endif
		{
			continue;
		}

		tests_module_t mg_module;
		mg_module.module_name = params.fs_filename;
		if (!mg_module.module.load(mask))
		{
			printf("RTX TEST error module load: %s\n", (const char*)mask);
			continue;
		}
		mg_module.run_fun = (pfn_tests_run)mg_module.module.bindSymbol("run_tests");
		if (mg_module.run_fun == nullptr)
		{
			printf("RTX TEST module has no run_test() entry point\n");
			mg_module.module.unload();
			continue;
		}

		int r = mg_module.run_fun(newArgc, newArgs);

		if (r < 0)
		{
			printf("RTX TEST run_tests return error %d\n", r);
		}

		mg_module.module.unload();

		if (r == 1)
		{
			printf("RTX TESTS FAILED\n");
			return -1;
		}
	}

	printf("RTX TEST STOPED\n");

	return 0;
}
//---------------------------------------------------------------------
//
//---------------------------------------------------------------------

int g_level = 0;



void printMargin(int l)
{
	char m[128];
	int cnt = MIN(l * 4, 127), i;
	
	for (i = 0; i < cnt; i++)
		m[i] = ' ';
	m[i] = '\0';

	 	printf(m);
}

const char* jsonTest = "{\r\n\
  \"frame-index\": 5,\r\n\
  \"frame\": {\r\n\
    \"width\": 1280,\r\n\
    \"height\": 720,\r\n\
    \"backgroundColor\": \"fe4d13\",\r\n\
    \"foregroundColor\": \"D00000\"\r\n\
  },\r\n\
  \"fields\": [\r\n\
    {\r\n\
      \"id\" : \"t13\",\r\n\
      \"zOrder\" : \"1\",\r\n\
      \"left\": 190,\r\n\
      \"top\": 10,\r\n\
      \"width\": 880,\r\n\
      \"height\": 30,\r\n\
      \"type\": \"text\",\r\n\
      \"text\": \"(content)\"\r\n\
    },\r\n\
    {\r\n\
      \"id\" : \"b001\",\r\n\
      \"zOrder\" : \"1\",\r\n\
      \"left\":10,\r\n\
      \"top\" : 10,\r\n\
      \"width\" : 150,\r\n\
      \"height\" : 130,\r\n\
      \"type\" : \"bitmap\",\r\n\
      \"bitmap\" : \"(base64)\"\r\n\
    },\r\n\
    {\r\n\
      \"id\" : \"aa123\",\r\n\
      \"zOrder\" : \"0\",\r\n\
      \"left\":1070,\r\n\
      \"top\" : 10,\r\n\
      \"width\" : 200,\r\n\
      \"height\" : 30,\r\n\
      \"type\" : \"time\",\r\n\
      \"format\" : \"yyyy-MM-dd HH:mm:ss\",\r\n\
      \"timezone\" : 3.5\r\n\
    },\r\n\
    {\r\n\
      \"id\" : \"00103\",\r\n\
      \"zOrder\" : \"2\",\r\n\
      \"left\":280,\r\n\
      \"top\" : 90,\r\n\
      \"width\" : 960,\r\n\
      \"height\" : 540,\r\n\
      \"titleHeight\" : 30,\r\n\
      \"type\" : \"video\",\r\n\
      \"mode\" : \"vad\"\r\n\
    },\r\n\
    {\r\n\
      \"id\" : \"00120\",\r\n\
      \"zOrder\" : \"1\",\r\n\
      \"left\":10,\r\n\
      \"top\" : 50,\r\n\
      \"width\" : 240,\r\n\
      \"height\" : 135,\r\n\
      \"titleHeight\" : 25,\r\n\
      \"type\" : \"video\",\r\n\
      \"mode\" : \"term\",\r\n\
      \"termId\" : \"(bindId)\"\r\n\
    },\r\n\
    {\r\n\
      \"id\" : \"00123\",\r\n\
      \"left\" : 10,\r\n\
      \"top\" : 215,\r\n\
      \"width\" : 240,\r\n\
      \"height\" : 135,\r\n\
      \"type\" : \"video\",\r\n\
      \"mode\" : \"auto\"\r\n\
    }\r\n\
  ],\r\n\
  \"vad\":true\r\n\
}";

static void printValue(JsonParser& parser)
{
	//printMargin(g_level);

	switch (parser.getFieldType())
	{
	case JsonTypes::Boolean:
		printf("%s,", STR_BOOL(parser.getBoolean()));
		break;
	case JsonTypes::Float:
		printf("%f,", parser.getFloat());
		break;
	case JsonTypes::Integer:
		printf("%zd,", parser.getInteger());
		break;
	case JsonTypes::String:
		printf("\"%s\",", (const char*)parser.getString());
		break;
	case JsonTypes::Null:
		printf("null,");
		break;
	case JsonTypes::Array:
	case JsonTypes::Object:
		break;
	}
}

static void printMember(JsonParser& parser)
{
	printMargin(g_level);

	printf("\"%s\":", (const char*)parser.getFieldName());

	printValue(parser);

	printf("\r\n");
}

static void printArrayValue(JsonParser& parser)
{
	printMargin(g_level);

	printValue(parser);

	printf("\r\n");
}

static int run_json_test()
{
	//printf("%s\r\n", jsonTest);

	JsonParser parser;

	g_level = 0;

	parser.start(jsonTest);

	JsonState state;

	do
	{
		state = parser.read();

		switch (state)
		{
		case JsonState::Array:
			printMargin(g_level);
			printf("[\r\n");
			g_level++;
			break;
		case JsonState::ArrayEnd:
			g_level--;
			printMargin(g_level);
			printf("],\r\n");
			break;
		case JsonState::Object:
			printMargin(g_level);
			printf("{\r\n");
			g_level++;
			break;
		case JsonState::ObjectEnd:
			g_level--;
			printMargin(g_level);
			printf("},\r\n");
			break;
		case JsonState::Member:
			printMember(parser);
			break;
		case JsonState::ArrayValue:
			printValue(parser);
			break;
		case JsonState::Error:
			printf("Error\r\n");
			state = JsonState::Eof;
			break;
		case JsonState::Eof:
			printf("Eof\r\n");
			break;
		}
	}
	while (state != JsonState::Eof);
	
	printf("Eof\r\n");

	return 0;
}

static int run_frame_gen_test()
{
	media::VideoFrameGenerator vfg;
	rtl::FileStream text;
	text.open("c:\\test\\video_conf6.json");
	size_t len = text.getLength();
	char* jsonTest2 = NEW char[len + 1];
	len = text.read(jsonTest2, len);
	jsonTest2[len] = 0;

	vfg.initialize(jsonTest2);

	const media::Bitmap* bmp = vfg.getPicture();
	if (bmp)
	{
		bmp->saveBitmapFile("c:\\test\\vfg_test6.bmp");
	}

	return 0;
}
//---------------------------------------------------------------------
