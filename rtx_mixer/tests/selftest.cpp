/* RTX H.248 Media Gate. Media mixer tool
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#ifndef RTX_TESTING
int start_selftesting(int argc, char* argv[])
{
	fprintf(stderr, "ERROR: selftest disabled\n\n");

	return -2;
}
#else 
//-----------------------------------------------
//
//-----------------------------------------------
#include "gtest/gtest.h"
#include "gmock/gmock.h"

int start_selftesting(int argc, char* argv[])
{
	printf(" - RTX MIXER TEST tools run_tests\n");

	rtl::String toutput, vec_path;
	printf(" - RTX MIXER TEST tools argc: %d\n", argc);
	for (int i = 0; i < argc; i++)
	{
		printf(" - RTX MIXER TEST tools check arg[%d]=%s\n", i, argv[i]);
		rtl::String str(argv[i]);
		if (str.indexOf("toutput=") != BAD_INDEX)
		{
			toutput.append(argv[i] + 8);
		}
		else if (str.indexOf("vecpath=") != BAD_INDEX)
		{
			vec_path = argv[i] + 8;
		}
	}

	printf(" - RTX MIXER TEST tools check vecpath\n");
	if (!vec_path.isEmpty())
	{
		Config.set_config_value("vecpath", vec_path);
	}
	printf(" - RTX MIXER TEST tools vecpath = %s\n", (const char*)vec_path);

	printf(" - RTX MIXER TEST tools check toutput\n");
	if (!toutput.isEmpty())
	{
		rtl::String path("xml:");
		path << toutput << FS_PATH_DELIMITER << "rtx_mixer_result.xml";
		::testing::GTEST_FLAG(output) = (const char*)path;
	}

	printf(" - RTX MIXER TEST tools InitGoogleTest\n");
	::testing::InitGoogleTest(&argc, argv);

	printf(" - RTX MIXER TEST tools RUN_ALL_TESTS\n");
	int Res = RUN_ALL_TESTS();
	printf(" - RTX MIXER TEST tools end(%d)\n", Res);
	
	return Res;
}
#endif		
