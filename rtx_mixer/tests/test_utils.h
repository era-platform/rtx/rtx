/* RTX H.248 Media Gate. Media mixer tool
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

//-----------------------------------------------
//
//-----------------------------------------------
#ifdef RTX_TESTING
namespace rtx_testing
{
	enum mixer_result_t
	{
		mixer_result_ok_e,
		mixer_result_none_e,
		mixer_result_invalid_e,
		mixer_result_source_error_e,
		mixer_result_target_error_e,
	};
	const char* get_mixer_result_text(mixer_result_t val);

	rtl::String get_path_to_mixer_data();
	int read_data_from_file(const char* path, uint8_t* buff, int buff_size);
	bool load_test_vector(const char* name, uint8_t** dataptr, int* datasize);
	rtl::String save_test_vector(const char* name, const void* dataptr, int datasize);
	mixer_result_t check_result(const char* answer, int& err);
	void test_compare_files(const char* dst_path, const char* ref_path);
}
#endif // RTX_TESTING
//-----------------------------------------------
