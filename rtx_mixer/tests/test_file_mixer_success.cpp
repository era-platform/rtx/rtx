/* RTX H.248 Media Gate. Media mixer tool
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

//-----------------------------------------------
//
//-----------------------------------------------
#ifdef RTX_TESTING

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "test_utils.h"

extern int file_mixer(const char* batch);

namespace rtx_testing
{

	TEST(test_file_mixer, rtp1_pcm_mono)
	{
		rtl::String mix_path = get_path_to_mixer_data();
		rtl::String batch;

		batch << "BEGIN MIX 123\r\n" <<
			"  SOURCE " << mix_path << FS_PATH_DELIMITER << "rtp_2.rtp\r\n" <<
			"  TARGET " << mix_path << FS_PATH_DELIMITER << "rtp_pcm_mono.wav\r\n" <<
			"  FORMAT L16 8000 1\r\nEND\r\n";

		rtl::String batch_path = save_test_vector("rtp_pcm_mono.batch", (const char*)batch, batch.getLength());

		if (file_mixer(batch_path) == 0)
		{
			rtl::String res = mix_path + FS_PATH_DELIMITER + "rtp_pcm_mono.wav";
			rtl::String ref = mix_path + FS_PATH_DELIMITER + "rtp_pcm_mono_ref.wav";
			
			test_compare_files(res, ref);

			fs_delete_file(batch_path);
			rtl::String batch_path_answer = mix_path + FS_PATH_DELIMITER + "rtp_pcm_mono.batch_answer";
			fs_delete_file(batch_path_answer);
			fs_delete_file(res);
		}
		else
		{
			ASSERT_TRUE(false);
		}
	}
	TEST(test_file_mixer, rtp1_rtp2_pcm_mono)
	{
		rtl::String mix_path = get_path_to_mixer_data();
		rtl::String batch;

		batch << "BEGIN MIX 123\r\n" <<
			"  SOURCE " << mix_path << FS_PATH_DELIMITER << "rtp_1.rtp\r\n" <<
			"  SOURCE " << mix_path << FS_PATH_DELIMITER << "rtp_2.rtp\r\n" <<
			"  TARGET " << mix_path << FS_PATH_DELIMITER << "rtp2_pcm_mono.wav\r\n" <<
			"  FORMAT L16 8000 1\r\nEND\r\n";

		rtl::String batch_path = save_test_vector("rtp2_pcm_mono.batch", (const char*)batch, batch.getLength());

		if (file_mixer(batch_path) == 0)
		{
			rtl::String res = mix_path + FS_PATH_DELIMITER + "rtp2_pcm_mono.wav";
			rtl::String ref = mix_path + FS_PATH_DELIMITER + "rtp2_pcm_mono_ref.wav";

			test_compare_files(res, ref);

			fs_delete_file(batch_path);
			rtl::String batch_path_answer = mix_path + FS_PATH_DELIMITER + "rtp2_pcm_mono.batch_answer";
			fs_delete_file(batch_path_answer);
			fs_delete_file(res);
		}
		else
		{
			ASSERT_TRUE(false);
		}
	}
	TEST(test_file_mixer, rtp1_rtp2_g711a_mono)
	{
		rtl::String mix_path = get_path_to_mixer_data();
		rtl::String batch;

		batch << "BEGIN MIX 123\r\n" <<
			"  SOURCE " << mix_path << FS_PATH_DELIMITER << "rtp_1.rtp\r\n" <<
			"  SOURCE " << mix_path << FS_PATH_DELIMITER << "rtp_2.rtp\r\n" <<
			"  TARGET " << mix_path << FS_PATH_DELIMITER << "rtp2_g711a_mono.wav\r\n" <<
			"  FORMAT PCMA 8000 1\r\nEND\r\n";

		rtl::String batch_path = save_test_vector("rtp2_g711a_mono.batch", (const char*)batch, batch.getLength());

		if (file_mixer(batch_path) == 0)
		{
			rtl::String res = mix_path + FS_PATH_DELIMITER + "rtp2_g711a_mono.wav";
			rtl::String ref = mix_path + FS_PATH_DELIMITER + "rtp2_g711a_mono_ref.wav";

			test_compare_files(res, ref);

			fs_delete_file(batch_path);
			rtl::String batch_path_answer = mix_path + FS_PATH_DELIMITER + "rtp2_g711a_mono.batch_answer";
			fs_delete_file(batch_path_answer);
			fs_delete_file(res);
		}
		else
		{
			ASSERT_TRUE(false);
		}
	}
	TEST(test_file_mixer, rtp1_rtp2_pcm_stereo)
	{
		rtl::String mix_path = get_path_to_mixer_data();
		rtl::String batch;

		batch << "BEGIN MIX 123\r\n" <<
			"  SOURCE " << mix_path << FS_PATH_DELIMITER << "rtp_1.rtp\r\n" <<
			"  SOURCE " << mix_path << FS_PATH_DELIMITER << "rtp_2.rtp\r\n" <<
			"  TARGET " << mix_path << FS_PATH_DELIMITER << "rtp2_pcm_stereo.wav\r\n" <<
			"  FORMAT L16 8000 2\r\nEND\r\n";

		rtl::String batch_path = save_test_vector("rtp2_pcm_stereo.batch", (const char*)batch, batch.getLength());

		if (file_mixer(batch_path) == 0)
		{
			rtl::String res = mix_path + FS_PATH_DELIMITER + "rtp2_pcm_stereo.wav";
			rtl::String ref = mix_path + FS_PATH_DELIMITER + "rtp2_pcm_stereo_ref.wav";

			test_compare_files(res, ref);

			fs_delete_file(batch_path);
			rtl::String batch_path_answer = mix_path + FS_PATH_DELIMITER + "rtp2_pcm_stereo.batch_answer";
			fs_delete_file(batch_path_answer);
			fs_delete_file(res);
		}
		else
		{
			ASSERT_TRUE(false);
		}
	}
	/*TEST(test_file_mixer, rtp1_mp3_mono)
	{
		rtl::String mix_path = get_path_to_mixer_data();
		rtl::String batch;

		batch << "BEGIN MIX 123\r\n" <<
			"  SOURCE " << mix_path << FS_PATH_DELIMITER << "rtp_1.rtp\r\n" <<
			"  TARGET " << mix_path << FS_PATH_DELIMITER << "rtp_mp3_mono.mp3\r\n" <<
			"  FORMAT MP3 8000 1\r\nEND\r\n";

		rtl::String batch_path = save_test_vector("rtp_mp3_mono.batch", (const char*)batch, batch.getLength());

		if (file_mixer(batch_path) == 0)
		{
			rtl::String res = mix_path + FS_PATH_DELIMITER + "rtp_mp3_mono.mp3";
			rtl::String ref = mix_path + FS_PATH_DELIMITER + "rtp_mp3_mono_ref.mp3";

			test_compare_files(res, ref);

			fs_delete_file(batch_path);
			rtl::String batch_path_answer = mix_path + FS_PATH_DELIMITER + "rtp_mp3_mono.batch_answer";
			fs_delete_file(batch_path_answer);
			fs_delete_file(res);
		}
		else
		{
			ASSERT_TRUE(false);
		}
	}*/
	/*TEST(test_file_mixer, rtp1_rtp2_mp3_mono)
	{
		rtl::String mix_path = get_path_to_mixer_data();
		rtl::String batch;

		batch << "BEGIN MIX 123\r\n" <<
			"  SOURCE " << mix_path << FS_PATH_DELIMITER << "rtp_1.rtp\r\n" <<
			"  SOURCE " << mix_path << FS_PATH_DELIMITER << "rtp_2.rtp\r\n" <<
			"  TARGET " << mix_path << FS_PATH_DELIMITER << "rtp2_mp3_mono.mp3\r\n" <<
			"  FORMAT mp3 8000 1\r\nEND\r\n";

		rtl::String batch_path = save_test_vector("rtp2_mp3_mono.batch", (const char*)batch, batch.getLength());

		if (file_mixer(batch_path) == 0)
		{
			rtl::String res = mix_path + FS_PATH_DELIMITER + "rtp2_mp3_mono.mp3";
			rtl::String ref = mix_path + FS_PATH_DELIMITER + "rtp2_mp3_mono_ref.mp3";

			test_compare_files(res, ref);

			fs_delete_file(batch_path);
			rtl::String batch_path_answer = mix_path + FS_PATH_DELIMITER + "rtp2_mp3_mono.batch_answer";
			fs_delete_file(batch_path_answer);
			fs_delete_file(res);
		}
		else
		{
			ASSERT_TRUE(false);
		}
	}*/
	/*TEST(test_file_mixer, rtp1_rtp2_mp3_stereo)
	{
		rtl::String mix_path = get_path_to_mixer_data();
		rtl::String batch;

		batch << "BEGIN MIX 123\r\n" <<
			"  SOURCE " << mix_path << FS_PATH_DELIMITER << "rtp_1.rtp\r\n" <<
			"  SOURCE " << mix_path << FS_PATH_DELIMITER << "rtp_2.rtp\r\n" <<
			"  TARGET " << mix_path << FS_PATH_DELIMITER << "rtp2_mp3_stereo.mp3\r\n" <<
			"  FORMAT mp3 8000 2\r\nEND\r\n";

		rtl::String batch_path = save_test_vector("rtp2_mp3_stereo.batch", (const char*)batch, batch.getLength());

		if (file_mixer(batch_path) == 0)
		{
			rtl::String res = mix_path + FS_PATH_DELIMITER + "rtp2_mp3_stereo.mp3";
			rtl::String ref = mix_path + FS_PATH_DELIMITER + "rtp2_mp3_stereo_ref.mp3";

			test_compare_files(res, ref);

			fs_delete_file(batch_path);
			rtl::String batch_path_answer = mix_path + FS_PATH_DELIMITER + "rtp2_mp3_stereo.batch_answer";
			fs_delete_file(batch_path_answer);
			fs_delete_file(res);
		}
		else
		{
			ASSERT_TRUE(false);
		}
	}*/
	TEST(test_file_mixer, rtp1_gsm_mono)
	{
		rtl::String mix_path = get_path_to_mixer_data();
		rtl::String batch;

		batch << "BEGIN MIX 123\r\n" <<
			"  SOURCE " << mix_path << FS_PATH_DELIMITER << "rtp_1.rtp\r\n" <<
			"  TARGET " << mix_path << FS_PATH_DELIMITER << "rtp_gsm_mono.wav\r\n" <<
			"  FORMAT GSM 8000 1\r\nEND\r\n";

		rtl::String batch_path = save_test_vector("rtp_gsm_mono.batch", (const char*)batch, batch.getLength());

		if (file_mixer(batch_path) == 0)
		{
			rtl::String res = mix_path + FS_PATH_DELIMITER + "rtp_gsm_mono.wav";
			rtl::String ref = mix_path + FS_PATH_DELIMITER + "rtp_gsm_mono_ref.wav";

			test_compare_files(res, ref);

			fs_delete_file(batch_path);
			rtl::String batch_path_answer = mix_path + FS_PATH_DELIMITER + "rtp_gsm_mono.batch_answer";
			fs_delete_file(batch_path_answer);
			fs_delete_file(res);
		}
		else
		{
			ASSERT_TRUE(false);
		}
	}
	TEST(test_file_mixer, rtp1_g711a_mono)
	{
		rtl::String mix_path = get_path_to_mixer_data();
		rtl::String batch;

		batch << "BEGIN MIX 123\r\n" <<
			"  SOURCE " << mix_path << FS_PATH_DELIMITER << "rtp_1.rtp\r\n" <<
			"  TARGET " << mix_path << FS_PATH_DELIMITER << "rtp_g711a_mono.wav\r\n" <<
			"  FORMAT PCMA 8000 1\r\nEND\r\n";

		rtl::String batch_path = save_test_vector("rtp_g711a_mono.batch", (const char*)batch, batch.getLength());

		if (file_mixer(batch_path) == 0)
		{
			rtl::String res = mix_path + FS_PATH_DELIMITER + "rtp_g711a_mono.wav";
			rtl::String ref = mix_path + FS_PATH_DELIMITER + "rtp_g711a_mono_ref.wav";

			test_compare_files(res, ref);

			fs_delete_file(batch_path);
			rtl::String batch_path_answer = mix_path + FS_PATH_DELIMITER + "rtp_g711a_mono.batch_answer";
			fs_delete_file(batch_path_answer);
			fs_delete_file(res);
		}
		else
		{
			ASSERT_TRUE(false);
		}
	}
	TEST(test_file_mixer, g711a_mp3_mono)
	{
		rtl::String mix_path = get_path_to_mixer_data();
		rtl::String batch;

		batch << "BEGIN MIX 123\r\n" <<
			"  SOURCE " << mix_path << FS_PATH_DELIMITER << "g711a.wav\r\n" <<
			"  TARGET " << mix_path << FS_PATH_DELIMITER << "g711a_mp3_mono.wav\r\n" <<
			"  FORMAT PCMA 8000 1\r\nEND\r\n";

		rtl::String batch_path = save_test_vector("g711a_mp3_mono.batch", (const char*)batch, batch.getLength());

		if (file_mixer(batch_path) == 0)
		{
			rtl::String res = mix_path + FS_PATH_DELIMITER + "g711a_mp3_mono.wav";
			rtl::String ref = mix_path + FS_PATH_DELIMITER + "g711a_mp3_mono_ref.wav";

			test_compare_files(res, ref);

			fs_delete_file(batch_path);
			rtl::String batch_path_answer = mix_path + FS_PATH_DELIMITER + "g711a_mp3_mono.batch_answer";
			fs_delete_file(batch_path_answer);
			fs_delete_file(res);
		}
		else
		{
			ASSERT_TRUE(false);
		}
	}
	/*TEST(test_file_mixer, gsm_pcm_mp3_mono)
	{
		rtl::String mix_path = get_path_to_mixer_data();
		rtl::String batch;

		batch << "BEGIN MIX 123\r\n" <<
			"  SOURCE " << mix_path << FS_PATH_DELIMITER << "gsm.wav\r\n" <<
			"  SOURCE " << mix_path << FS_PATH_DELIMITER << "pcm.wav\r\n" <<
			"  TARGET " << mix_path << FS_PATH_DELIMITER << "gsm_pcm_mp3_mono.mp3\r\n" <<
			"  FORMAT mp3 8000 1\r\nEND\r\n";

		rtl::String batch_path = save_test_vector("gsm_pcm_mp3_mono.batch", (const char*)batch, batch.getLength());

		if (file_mixer(batch_path) == 0)
		{
			rtl::String res = mix_path + FS_PATH_DELIMITER + "gsm_pcm_mp3_mono.mp3";
			rtl::String ref = mix_path + FS_PATH_DELIMITER + "gsm_pcm_mp3_mono_ref.mp3";

			test_compare_files(res, ref);

			fs_delete_file(batch_path);
			rtl::String batch_path_answer = mix_path + FS_PATH_DELIMITER + "gsm_pcm_mp3_mono.batch_answer";
			fs_delete_file(batch_path_answer);
			fs_delete_file(res);
		}
		else
		{
			ASSERT_TRUE(false);
		}
	}*/
	/*TEST(test_file_mixer, mp3_pcm_mono)
	{
		rtl::String mix_path = get_path_to_mixer_data();
		rtl::String batch;

		batch << "BEGIN MIX 123\r\n" <<
			"  SOURCE " << mix_path << FS_PATH_DELIMITER << "mp3.mp3\r\n" <<
			"  TARGET " << mix_path << FS_PATH_DELIMITER << "mp3_pcm_mono.wav\r\n" <<
			"  FORMAT L16 8000 1\r\nEND\r\n";

		rtl::String batch_path = save_test_vector("mp3_pcm_mono.batch", (const char*)batch, batch.getLength());

		if (file_mixer(batch_path) == 0)
		{
			rtl::String res = mix_path + FS_PATH_DELIMITER + "mp3_pcm_mono.wav";
			rtl::String ref = mix_path + FS_PATH_DELIMITER + "mp3_pcm_mono_ref.wav";

			test_compare_files(res, ref);

			fs_delete_file(batch_path);
			rtl::String batch_path_answer = mix_path + FS_PATH_DELIMITER + "mp3_pcm_mono.batch_answer";
			fs_delete_file(batch_path_answer);
			fs_delete_file(res);
		}
		else
		{
			ASSERT_TRUE(false);
		}
	}*/
}
#endif // RTX_TESTING
