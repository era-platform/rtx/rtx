/* RTX H.248 Media Gate. Media mixer tool
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

//-----------------------------------------------
//
//-----------------------------------------------
#ifdef RTX_TESTING

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "test_rtx_mixer.h"

extern int file_mixer(const char* batch);

namespace rtx_testing
{

	TEST(test_file_mixer_failures, source1_invalid)
	{
		rtl::String mix_path = get_path_to_mixer_data();
		rtl::String batch;

		batch << "BEGIN MIX 123\r\n" <<
			"  SOURCE " << mix_path << FS_PATH_DELIMITER << "rtp_invalid.rtp\r\n" <<
			"  TARGET " << mix_path << FS_PATH_DELIMITER << "rtp_invalid.wav\r\n" <<
			"  FORMAT L16 8000 1\r\nEND\r\n";

		rtl::String batch_path = save_test_vector("rtp_invalid.batch", (const char*)batch, batch.getLength());
		rtl::String batch_path_answer = mix_path + FS_PATH_DELIMITER + "rtp_invalid.batch_answer";

		if (file_mixer(batch_path) == 0)
		{
			
			int err = 0;
			mixer_result_t res = check_result(batch_path_answer, err);
			mixer_result_t ref_val = mixer_result_source_error_e;

			fs_delete_file(batch_path);
			fs_delete_file(batch_path_answer);

			PRINTF("checking result : %s, %s\n", get_mixer_result_text(res), get_mixer_result_text(ref_val));
			ASSERT_EQ(res, ref_val);
		}
		else
		{
			fs_delete_file(batch_path);
			fs_delete_file(batch_path_answer);

			ASSERT_TRUE(false);
		}
	}
	TEST(test_file_mixer_failures, source2_invalid)
	{
		rtl::String mix_path = get_path_to_mixer_data();
		rtl::String batch;

		batch << "BEGIN MIX 123\r\n" <<
			"  SOURCE " << mix_path << FS_PATH_DELIMITER << "rtp_invalid1.rtp\r\n" <<
			"  SOURCE " << mix_path << FS_PATH_DELIMITER << "rtp_invalid2.rtp\r\n" <<
			"  TARGET " << mix_path << FS_PATH_DELIMITER << "rtp_invalid.mp3\r\n" <<
			"  FORMAT MP3 8000 1\r\nEND\r\n";

		rtl::String batch_path = save_test_vector("rtp_invalid.batch", (const char*)batch, batch.getLength());
		rtl::String batch_path_answer = mix_path + FS_PATH_DELIMITER + "rtp_invalid.batch_answer";

		if (file_mixer(batch_path) == 0)
		{
			
			int err = 0;
			mixer_result_t res = check_result(batch_path_answer, err);
			mixer_result_t ref_val = mixer_result_source_error_e;

			fs_delete_file(batch_path);
			fs_delete_file(batch_path_answer);

			PRINTF("checking result : %s, %s\n", get_mixer_result_text(res), get_mixer_result_text(ref_val));
			ASSERT_EQ(res, ref_val);
		}
		else
		{
			fs_delete_file(batch_path);
			fs_delete_file(batch_path_answer);

			ASSERT_TRUE(false);
		}
	}
	TEST(test_file_mixer_failures, format_invalid)
	{
		rtl::String mix_path = get_path_to_mixer_data();
		rtl::String batch;

		batch << "BEGIN MIX 123\r\n" <<
			"  SOURCE " << mix_path << FS_PATH_DELIMITER << "rtp_1.rtp\r\n" <<
			"  SOURCE " << mix_path << FS_PATH_DELIMITER << "rtp_2.rtp\r\n" <<
			"  TARGET " << mix_path << FS_PATH_DELIMITER << "rtp_invalid.mp3\r\n" <<
			"  FORMAT OPU 16000 1\r\nEND\r\n";

		rtl::String batch_path = save_test_vector("rtp_invalid.batch", (const char*)batch, batch.getLength());
		rtl::String batch_path_answer = mix_path + FS_PATH_DELIMITER + "rtp_invalid.batch_answer";

		if (file_mixer(batch_path) == 0)
		{
			
			int err = 0;
			mixer_result_t res = check_result(batch_path_answer, err);
			mixer_result_t ref_val = mixer_result_invalid_e;

			fs_delete_file(batch_path);
			fs_delete_file(batch_path_answer);

			PRINTF("checking result : %s, %s\n", get_mixer_result_text(res), get_mixer_result_text(ref_val));
			ASSERT_EQ(res, ref_val);
		}
		else
		{
			fs_delete_file(batch_path);
			fs_delete_file(batch_path_answer);

			ASSERT_TRUE(false);
		}
	}
	/*TEST(test_file_mixer_failures, target_invalid)
	{
		rtl::String mix_path = get_path_to_mixer_data();
		rtl::String batch;

		batch << "BEGIN MIX 123\r\n" <<
			"  SOURCE " << mix_path << FS_PATH_DELIMITER << "rtp_1.rtp\r\n" <<
			"  SOURCE " << mix_path << FS_PATH_DELIMITER << "rtp_2.rtp\r\n" <<
			"  TARGET " << mix_path << FS_PATH_DELIMITER << "super_folder/qwerty/rtp_invalid.mp3\r\n" <<
			"  FORMAT MP3 8000 1\r\nEND\r\n";

		rtl::String batch_path = save_test_vector("rtp_invalid.batch", (const char*)batch, batch.getLength());
		rtl::String batch_path_answer = mix_path + FS_PATH_DELIMITER + "rtp_invalid.batch_answer";

		if (file_mixer(batch_path) == 0)
		{

			int err = 0;
			mixer_result_t res = check_result(batch_path_answer, err);
			mixer_result_t ref_val = mixer_result_target_error_e;

			fs_delete_file(batch_path);
			fs_delete_file(batch_path_answer);

			PRINTF("checking result : %s, %s\n", get_mixer_result_text(res), get_mixer_result_text(ref_val));
			ASSERT_EQ(res, ref_val);
		}
		else
		{
			fs_delete_file(batch_path);
			fs_delete_file(batch_path_answer);

			ASSERT_TRUE(false);
		}
	}*/
}
#endif // RTX_TESTING
