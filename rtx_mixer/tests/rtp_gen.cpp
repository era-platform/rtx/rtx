﻿/* RTX H.248 Media Gate. Media mixer tool
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_file_rtp.h"
//--------------------------------------
//
//--------------------------------------
class shuffler_t
{
	media::RTPFileHeader m_header;
	rtl::FileStream m_file;
	struct ShufflingPacket { int times; media::RTPFilePacketHeader* packet; };
	rtl::ArrayT<ShufflingPacket> m_shuffle_stack;
	int m_roll_count;
public:
	shuffler_t() : m_roll_count(0) { }
	~shuffler_t() { close(); }

	bool create(const char* path);
	void close();

	void write_header(const media::RTPFileHeader& header);
	bool write_rtp_packet(const media::RTPFilePacketHeader* packet);

private:
	int roll_the_dice(int min, int max);
	void push_to_stack(const media::RTPFilePacketHeader* packet, int times);
	void free_stack();
	void write_packet(media::RTPFilePacketHeader* packet);
};
//--------------------------------------
// генерация rtp файла со спутанным порядком
//--------------------------------------
void shuffle_rtp_file(const char* original, const char* out)
{
	media::FileReaderRTP reader;
	shuffler_t shuffler;

	if (!reader.open(original))
	{
		printf("Error: original rtp file not found!\n");
		return;
	}

	rtl::String outname = out;

	if (outname.isEmpty())
	{
		outname = original;
		outname += ".shf";
	}

	if (!shuffler.create(outname))
	{
		printf("Error: unable create target shuffled rtp file!\n");
		return;
	}

	shuffler.write_header(reader.get_header());

	const media::RTPFilePacketHeader* packet;

	while ((packet = reader.readNext()) != nullptr)
	{
		if (!shuffler.write_rtp_packet(packet))
		{
			break;
		}
	}

	reader.close();
	shuffler.close();
}

bool shuffler_t::create(const char* path)
{
	memset(&m_header, 0, sizeof(m_header));
	m_roll_count = 0;
	return m_file.createNew(path);
}

void shuffler_t::close()
{
	while (m_shuffle_stack.getCount() > 0)
	{
		m_roll_count++;
		free_stack();
	}

	m_file.close();
}

void shuffler_t::write_header(const media::RTPFileHeader& header)
{
	m_header = header;

	m_file.write(&header, sizeof(header));
}

bool shuffler_t::write_rtp_packet(const media::RTPFilePacketHeader* packet)
{
	m_roll_count++;

	if (!m_file.isOpened() || m_header.sign != MG_REC_AUDIO_SIGNATURE)
	{
		printf("Error: attempt to write to not created file!\n");
		return false;
	}

	int times = roll_the_dice(2, 0x3F);

	if (times > 0)
		push_to_stack(packet, times);
	else
		m_file.write(packet, packet->length + RTP_FILE_PACKET_HEADER_LENGTH);

	free_stack();

	return true;
}

int shuffler_t::roll_the_dice(int min, int max)
{
	return (m_roll_count % 10) == 0 ? rand() & 0x3F : 0;
}

void shuffler_t::push_to_stack(const media::RTPFilePacketHeader* packet, int times)
{
	media::RTPFilePacketHeader* buffer = (media::RTPFilePacketHeader*)malloc(packet->length + RTP_FILE_PACKET_HEADER_LENGTH);
	memcpy(buffer, packet, packet->length + RTP_FILE_PACKET_HEADER_LENGTH);
	ShufflingPacket item = { times, buffer };

	m_shuffle_stack.add(item);
}

void shuffler_t::free_stack()
{
	for (int i = m_shuffle_stack.getCount() - 1; i >= 0; i--)
	{
		ShufflingPacket& item = m_shuffle_stack.getAt(i);

		if (--item.times == 0)
		{
			m_file.write(item.packet, item.packet->length + RTP_FILE_PACKET_HEADER_LENGTH);
			free(item.packet);
			m_shuffle_stack.removeAt(i);
		}
	}
}
//--------------------------------------
