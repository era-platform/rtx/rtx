/* RTX H.248 Media Gate. Media mixer tool
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "std_sys_env.h"
#include "std_file_stream.h"
//-----------------------------------------------
//
//-----------------------------------------------
#ifdef RTX_TESTING

#include "test_rtx_mixer.h"

namespace rtx_testing
{
	static int copy_file(const char* src_path, const char* dst_path);

	rtl::String get_path_to_mixer_data()
	{
		rtl::String vec_path = Config.get_config_value("vecpath");

		if (vec_path.isEmpty())
		{
			char buffer[256];

			vec_path = std_get_application_startup_path(buffer, 256) ? buffer : (char*)nullptr;
		}

		return vec_path + FS_PATH_DELIMITER + "mixer";
	}
	int read_data_from_file(const char* path, uint8_t* buff, int buff_size)
	{
		rtl::FileStream file;
		if (!file.open(path))
		{
			return 0;
		}
		int read = int(file.read(buff, buff_size));
		file.close();
		return read;
	}
	bool load_test_vector(const char* name, uint8_t** dataptr, int* datasize)
	{
		rtl::String path = get_path_to_mixer_data();
		path << FS_PATH_DELIMITER << name;

		rtl::FileStream file;
		if (!file.open(path))
		{
			return false;
		}

		*datasize = (int)file.getLength();
		*dataptr = (uint8_t*)MALLOC(*datasize);
		int read = (int)file.read(*dataptr, *datasize);
		file.close();
		if (read != *datasize)
		{
			FREE(*dataptr);
			*datasize = 0;
		}
		return *datasize != 0;
	}
	rtl::String save_test_vector(const char* name, const void* dataptr, int datasize)
	{
		rtl::String path = get_path_to_mixer_data();
		path << FS_PATH_DELIMITER << name;

		rtl::FileStream file;

		if (file.createNew(path))
		{
			file.write(dataptr, datasize);
			file.close();

			return path;
		}

		return rtl::String::empty;
	}
	void test_compare_files(const char* dst_path, const char* ref_path)
	{
		rtl::FileStream dst, ref;
		int cycles = 0;

		uint8_t dst_buf[2048], ref_buf[2048];

		if (!dst.open(dst_path))
		{
			ASSERT_TRUE(false);
			return;
		}

		if (!ref.open(ref_path))
		{
			copy_file(dst_path, ref_path);
			ASSERT_TRUE(false);
			return;
		}

		bool res;

		while (true)
		{
			size_t dst_read = dst.read(dst_buf, 2048);
			size_t ref_read = ref.read(ref_buf, 2048);
			bool is_eq_1 = dst_read == ref_read;

			if (dst_read == 0 && ref_read == 0)
			{
				// OK;
				return;
			}

			if (dst_read == 0 || ref_read == 0)
			{
				res = dst_read > 0 && ref_read > 0;
				ASSERT_TRUE(res);
				return;
			}

			if (!is_eq_1)
			{
				res = dst_read == ref_read;
				ASSERT_TRUE(res);
				return;
			}

			res = memcmp(dst_buf, ref_buf, dst_read) == 0;

			if (!res)
			{
				ASSERT_TRUE(res);
				return;
			}

			cycles++;
		}

		PRINTF("comparing: %d packets processed\n", cycles);
	}
	mixer_result_t check_result(const char* answer, int& err)
	{
		rtl::FileStream file;

		err = 0;

		if (!file.open(answer))
		{
			return mixer_result_none_e;
		}

		size_t f_len = file.getLength();
		char* text = (char*)MALLOC(f_len + 1);
		if (file.read(text, f_len) != f_len)
		{
			FREE(text);
			return mixer_result_invalid_e;
		}
		text[f_len] = 0;

		char* src_tag = strstr(text, "SOURCE ");

		while (src_tag != nullptr)
		{
			src_tag += 7;
			if (strncmp(src_tag, "FAILED ", 7) == 0)
			{
				src_tag += 7;
				err = atoi(src_tag);
				FREE(text);
				return mixer_result_source_error_e;
			}

			src_tag = strstr(src_tag, "SOURCE ");
		}

 		char* dst_tag = strstr(text, "TARGET ");

		if (dst_tag != nullptr)
		{
			dst_tag += 7;
			if (strncmp(dst_tag, "OK", 2) == 0)
			{
				FREE(text);
				return mixer_result_t::mixer_result_ok_e;
			}
			else if (strncmp(dst_tag, "FAILED ", 7) == 0)
			{
				dst_tag += 7;
				err = atoi(dst_tag);
				FREE(text);
				return mixer_result_target_error_e;
			}
		}

		dst_tag = strstr(text, "FAILED ");

		if (dst_tag != nullptr)
		{
			dst_tag += 7;
			err = atoi(dst_tag);
			FREE(text);
			return mixer_result_invalid_e;
		}
		
		FREE(text);

		return mixer_result_t::mixer_result_invalid_e;
	}
	static const char* s_mixer_result[] = 
	{
		"mixer_result_ok_e",
		"mixer_result_none_e",
		"mixer_result_invalid_e",
		"mixer_result_source_error_e",
		"mixer_result_target_error_e",
	};
	const char* get_mixer_result_text(mixer_result_t val)
	{
		return val > 0 && val <= mixer_result_target_error_e ? s_mixer_result[val] : "unknown_result";
	}
	int copy_file(const char* src_path, const char* dst_path)
	{
		rtl::FileStream src;
		rtl::FileStream dst;

		if (!src.open(src_path))
			return 0;
		
		if (!dst.createNew(dst_path))
			return 0;

		uint8_t buffer[1024];
		size_t bsize, fsize = 0;
		
		while ((bsize = src.read(buffer, 1024)) > 0)
		{
			fsize += bsize;
			dst.write(buffer, bsize);
		}

		src.close();
		dst.close();

		printf("---- copy_file %zu bytes ----", fsize);

		return 0;
	}
}
#endif // RTX_TESTING
//-----------------------------------------------
