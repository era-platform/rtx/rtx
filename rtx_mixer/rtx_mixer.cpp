﻿/* RTX H.248 Media Gate. Media mixer tool
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "task_list.h"
#include "file_mixer_mono.h"
#include "file_mixer_stereo.h"
//-----------------------------------------------
//
//-----------------------------------------------
#if defined (DEBUG_MEMORY)
void* operator new (size_t size, char* file, int line)
{
	return std_mem_checker_malloc(size, file, line, rc_malloc_new_e);
}
void* operator new[](size_t size, char* file, int line)
{
	return std_mem_checker_malloc(size, file, line, rc_malloc_newar_e);
}
void operator delete (void* mem_ptr)
{
	std_mem_checker_free(mem_ptr, rc_malloc_delete_e);
}
void operator delete[](void* mem_ptr)
{
	std_mem_checker_free(mem_ptr, rc_malloc_deletear_e);
}
#endif

//bool g_use_linear_rtp_reader = false;

static void print_info()
{
	fprintf(stdout, "Usage: rtx_mixer [-c configfile] batch_file_name\n");
	fprintf(stdout, "       rtx_mixer [-c configfile] -r rtp_file_name\n");
	fprintf(stdout, "       rtx_mixer [-c configfile] -t [google_params]\n");
	fprintf(stdout, "       rtx_mixer [-c configfile] -s:rtp_in [rtp_out]\n");
	fprintf(stdout, "           -c  -- path to configuration file\n");
	fprintf(stdout, "           -l  -- use a linear RTP reader. By default,\n");
	fprintf(stdout, "                  an RTP reader with a jitter buffer will be used.");
	fprintf(stdout, "           -r  -- print rtp file statistics\n");
	fprintf(stdout, "           -t  -- launch google tests. MUST be compiled with self-testing keys\n");
	fprintf(stdout, " google_params -- google test system options\n");
	fprintf(stdout, "    -s:rtp_in  -- rtp file to shuffle\n");
	fprintf(stdout, "      rtp_out  -- rtp file to write shuffle result\n");
}
//-----------------------------------------------
//
//-----------------------------------------------
int main(int argc, char* argv[])
{
	if (argc < 2)
	{
		print_info();
		return 0;
	}

	rtl::String cfg_file = "./rtx_mixer.cfg";

	int res = -1;

	bool r = false,  // rtp dump
		 t = false,	 // start tests
		 l = false,  // linear rtp reader
		 s = false;	 // shuffle rtp file

	rtl::String rtp_in, rtp_out;

	while ((res = getopt(argc, argv, "c:lrts:")) != EOF)
	{
		switch (res)
		{
		case 'c': // path to config file
			cfg_file = optarg;
			break;
		case 'l':
			printf("using linear rtp reader...\n");
			media::AudioReader::setRTPReaderType(true);
			break;
		case 'r':
			r = true;
			break;
		case 't':
			t = true;
			break;
		case 's':
			s = true;
			rtp_in = optarg;
			break;
		case '?':
			printf("Warnng: unknown option %s. ignored\n", argv[optind]);
			break;
		case ':':
			printf("Warnng: unknown option %s. ignored\n", argv[optind]);
			break;
		}
	}

	printf("config path %s\n", (const char*)cfg_file);

	Config.read(cfg_file);

	//const char* log_path = Config.get_config_value("log-root-path");
	//printf("log path %s\n", log_path);

	////TErr..log("out", "log path = %s", log_path);

	//const char* cfg_value = Config.get_config_value(CONF_LOG_MAX_SIZE);
	//uint64_t max_size = cfg_value != nullptr ? strtoull(cfg_value, nullptr, 10) : 10;
	//cfg_value = Config.get_config_value(CONF_LOG_PART_SIZE);
	//uint64_t part_size = cfg_value != nullptr ? strtoull(cfg_value, nullptr, 10) : 10;
	//cfg_value = Config.get_config_value(CONF_LOG_PART_COUNT);
	//uint64_t part_count = cfg_value != nullptr ? strtoull(cfg_value, nullptr, 10) : 10;

	//rtl::Logger::set_log_sizes(max_size, part_size, part_count);

	//Log.create(log_path, "mix", LOG_APPEND);
	//rtl::Logger::set_trace_flags(Config.get_config_value("log-trace"));

	//Log.log("out", "config path = %s", (const char*)cfg_file);

	if (!rtx_codec__load_all_codecs(nullptr, nullptr))
	{
		fprintf(stderr, "error : rtx_codec__load_all_codecs FAIL.\n");
		return -1;
	}

	if (r)
	{
		printf("start rtp info...\n");
		res = print_rtp_file(argc, argv);
	}
	else if (t)
	{
		printf("start self tesing...\n");
		res = start_selftesting(argc, argv);
	}
	else if (s)
	{
		if (rtp_in.isEmpty())
		{
			printf("Error: mising rtp input file!\n");
			print_info();
			exit(-1);
		}

		if (optind < argc)
		{
			rtp_out = argv[optind + 1];
		}

		shuffle_rtp_file(rtp_in, rtp_out);
	}
	else
	{
		rtl::String batch_file;
		if (optind < argc)
		{
			printf("start file mixer...\n");
			res = file_mixer(argv[optind]);
		}
		else
		{
			printf("Error: mising batch file!\n");
			print_info();
			exit(-1);
		}

	}

	Log.destroy();

	return res;
}
//-----------------------------------------------
//
//-----------------------------------------------
//media::Reader* open_rtp_reader(const char* path, int freq)
//{
//	media::Reader* reader = nullptr;
//
//	if (g_use_linear_rtp_reader)
//	{
//		reader = NEW media::ReaderLinearRTP();
//	}
//	else
//	{
//		reader = NEW media::ReaderRTP();
//	}
//
//	if (reader->open(path))
//	{
//		reader->setupReader(freq);
//	}
//	else
//	{
//		reader->close();
//		DELETEO(reader);
//		reader = nullptr;
//	}
//
//	return reader;
//}
////-----------------------------------------------
////
////-----------------------------------------------
//media::Reader* create_rtp_reader()
//{
//	if (g_use_linear_rtp_reader)
//	{
//		return NEW media::ReaderLinearRTP();
//	}
//	else
//	{
//		return NEW media::ReaderRTP();
//	}
//}
//-----------------------------------------------
