﻿/* RTX H.248 Media Gate. Media mixer tool
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "task_list.h"
#include "mmt_wave_format.h"
#include "mmt_audio_reader_lrtp.h"
#include "mmt_recorder.h"
//--------------------------------------
// микшируем по 20 мс!
//--------------------------------------
class file_mixer_stereo_t
{
	rtl::String m_out_encoding;	// кодирование результируещего файла
	int m_out_freq;				// частота результируещего файла
	int m_out_channels;			// всегда стерео(2)
	rtl::String m_out_fmtp;		// для mp3 битрейт или индекс сжатия

	int m_in_freq;				// наибольшая частота среди исходных файлов. Для RTP невозможно определить поэтому всегда 8000
	int m_in_channels;			// всегда моно(1), так как каждый файл пишется в один из каналов

	media::AudioReader* m_source_left;
	media::AudioReader* m_source_right;
	media::RecorderStereo* m_target_file;

	int m_start_delay_left;
	int m_start_delay_right;

	int m_samples_per_ms;		// количество семплов в одном канале в миллисекунду
	int m_frame_size;			// размер фрейма чтения/записи в семплах (количество семплов в 20 миллисекунд)
	short* m_channel_left;
	short* m_channel_right;

	result_t* m_result; // сборщик результатов работв

public:
	file_mixer_stereo_t();
	~file_mixer_stereo_t();

	result_t* run(task_t* task);

private:
	bool open_sources(const source_info_t* source_left, const source_info_t* source_right);
	void close_sources();
	bool create_target(const char* target);
	void close_target();
	bool mix_files();
	void check_offsets();
	// Заполняем буфер либо данными, либо тишиной.
	// Возвращает true если закончили чтение данных.
	bool fill_channel(media::AudioReader* source, short* buffer, int& timeshift);
};
//--------------------------------------------------------------------------------------------------
// 
//--------------------------------------------------------------------------------------------------
