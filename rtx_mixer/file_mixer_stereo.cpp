﻿/* RTX H.248 Media Gate. Media mixer tool
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "file_mixer_stereo.h"
//-----------------------------------------------
//
//-----------------------------------------------
//#define LOG_PREFIX "mix-str"
// проверка временнОго сдвига файле
// если разница больше чем час, не используем
#define CHECK_TIMESHIFT(sub) (int)((sub <= 0 || sub > 3600000) ? 0 : sub)
//-----------------------------------------------
//
//-----------------------------------------------
file_mixer_stereo_t::file_mixer_stereo_t()
{
	m_out_freq = 8000;
	m_out_channels = 2;
	m_in_freq = 8000;
	m_in_channels = 1;

	m_source_left = nullptr;
	m_source_right = nullptr;
	m_target_file = nullptr;

	m_start_delay_left = 0;
	m_start_delay_right = 0;
	
	m_samples_per_ms = 160;
	m_frame_size = 0;
	m_channel_left = nullptr;
	m_channel_right = nullptr;
	m_result = nullptr;
}
//-----------------------------------------------
//
//-----------------------------------------------
file_mixer_stereo_t::~file_mixer_stereo_t()
{
	close_target();
	close_sources();

	FREE(m_channel_left);
	m_channel_left = nullptr;

	FREE(m_channel_right);
	m_channel_right = nullptr;

	// m_result must be deleted by run() user
}
//-----------------------------------------------
//
//-----------------------------------------------
result_t* file_mixer_stereo_t::run(task_t* task)
{
	m_result = NEW result_t(task->get_id());

	m_out_encoding = task->getEncoding();
	m_out_freq = task->get_samples_rate();
	m_out_channels = task->getChannelCount();
	m_out_fmtp = task->get_params();

	m_samples_per_ms = m_out_freq / 1000;
	m_frame_size = m_samples_per_ms * 20;

	m_channel_left = (short*)MALLOC(m_frame_size * sizeof(short) * 2);
	m_channel_right = (short*)MALLOC(m_frame_size * sizeof(short) * 2);

	if (!open_sources(task->get_source(0), task->get_source(1)))
	{
		//LOG_ERROR(LOG_PREFIX, "Unable to open source files!");
		fprintf(stderr, "Unable to open source files!\n");
		return m_result;
	}

	if (!create_target(task->get_target()))
	{
		//LOG_ERROR(LOG_PREFIX, "Unable to create target file!");
		fprintf(stderr, "Unable to create target file!\n");
		return m_result;
	}

	mix_files();

	close_target();
	close_sources();

	return m_result;
}
//-----------------------------------------------
//
//-----------------------------------------------s
bool file_mixer_stereo_t::open_sources(const source_info_t* source_left, const source_info_t* source_right)
{
	//LOG_MIXER(LOG_PREFIX, "Source files opening...");

	close_sources();

	if (source_left == nullptr || source_left->filename == nullptr || source_left->filename[0] == 0)
	{
		//LOG_ERROR(LOG_PREFIX, "Left channel file has empty name!");
		fprintf(stderr, "Left channel file has empty name!\n");
		m_result->add_error(-1, "first channel file has invalid name");
		return false;
	}

	if (source_right == nullptr || source_right->filename == nullptr || source_right->filename[0] == 0)
	{
		//LOG_ERROR(LOG_PREFIX, "Right channel file has empty name!");
		fprintf(stderr, "Right channel file has empty name!\n");
		m_result->add_error(-1, "Second channel file has invalid name");
		return false;
	}

	//перераспределяем каналы (право лево)
	if (source_left->flags == 2 || source_right->flags == 1)
	{
		if (source_left->flags == 0 || source_left->flags > source_right->flags)
		{
			const source_info_t* tmp = source_left;
			source_left = source_right;
			source_right = tmp;
		}
	}


	//открываем файлы
	//LOG_MIXER(LOG_PREFIX, "File \'%s\' opened", source_left);

	m_source_left = media::AudioReader::create(source_left->filename, nullptr);

	if (m_source_left == nullptr)
	{
		//LOG_ERROR(LOG_PREFIX, "Unable to open file for left channel");
		fprintf(stderr, "Unable to open file for left channel\n");
		m_result->add_error(-1, source_left->filename);
	}

	//LOG_MIXER(LOG_PREFIX, "File \'%s\' opened", source_right);

	m_source_right = media::AudioReader::create(source_right->filename, nullptr);

	if (m_source_right == nullptr)
	{
		//LOG_ERROR(LOG_PREFIX, "Unable to open file for right channel");
		fprintf(stderr, "Unable to open file for right channel\n");
		m_result->add_error(-1, source_right->filename);
		
		if (m_source_left == nullptr)
			return false;
	}

	m_source_left->setupReader(m_in_freq);
	m_source_right->setupReader(m_in_freq);

	//определяем смещения файлов
	check_offsets();

	return TRUE;
}
//-----------------------------------------------
//
//-----------------------------------------------
void file_mixer_stereo_t::close_sources()
{
	//LOG_MIXER(LOG_PREFIX, "Source files closing...");

	if (m_source_left != nullptr)
	{
		m_source_left->close();
		DELETEO(m_source_left);
		m_source_left = nullptr;
	}

	if (m_source_right != nullptr)
	{
		m_source_right->close();
		DELETEO(m_source_right);
		m_source_right = nullptr;
	}

	m_start_delay_left = 0;
	m_start_delay_right = 0;

	//LOG_MIXER(LOG_PREFIX, "Source files closing... ok.");
}
//-----------------------------------------------
//
//-----------------------------------------------
bool file_mixer_stereo_t::create_target(const char* target)
{
	close_target();

	if (!target || (target[0] == 0))
	{
		//LOG_ERROR(LOG_PREFIX, "Unable to create MP3 target file!");
		fprintf(stderr, "Unable to create MP3 target file!\n");
		m_result->set_target_error(-1);
		return false;
	}

	m_target_file = media::RecorderStereo::create(m_in_freq, m_out_encoding);

	if (m_target_file != nullptr)
	{
		if (!m_target_file->create(target, m_out_encoding, m_out_freq, m_out_fmtp))
		{
			m_result->set_target_error(errno);
			DELETEO(m_target_file);
			m_target_file = nullptr;
			return false;
		}
	}

	//LOG_MIXER(LOG_PREFIX, "Target file creating: ok.");

	return true;
}
//-----------------------------------------------
//
//-----------------------------------------------
void file_mixer_stereo_t::close_target()
{
	if (m_target_file != nullptr)
	{
		m_target_file->close();
		DELETEO(m_target_file);
		m_target_file = nullptr;
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
bool file_mixer_stereo_t::mix_files()
{
	//LOG_MIXER(LOG_PREFIX, "Begin mix...");

	try
	{
		for (;;)
		{
			// заполняем буферы данными либо тишиной
			bool read_left_end = fill_channel(m_source_left, m_channel_left, m_start_delay_left);
			bool read_rigth_end = fill_channel(m_source_right, m_channel_right, m_start_delay_right);

			if (read_left_end && read_rigth_end)
			{
				// прочли оба ресурса до конца
				break;
			}

			// записываем в выходной файл.
			m_target_file->write((const short*)m_channel_left, (const short*)m_channel_right, m_frame_size);
		}
	}
	catch (rtl::Exception& ex)
	{
		//LOG_ERROR(LOG_PREFIX, "EXCEPTION in mix_files():\n%s", ex.get_message());
		fprintf(stderr, "EXCEPTION in mix_files():\n%s\n", ex.get_message());
	}

	//LOG_MIXER(LOG_PREFIX, "Mix complete...");
	return true;
}
//-----------------------------------------------
//
//-----------------------------------------------
void file_mixer_stereo_t::check_offsets()
{
	m_start_delay_left = 0;
	m_start_delay_right = 0;

	uint64_t stCurLeft = m_source_left->getStartTime();
	uint64_t stCurRight = m_source_right->getStartTime();

	if (stCurLeft == 0 || stCurRight == 0)
	{
		return;
	}

	int64_t sub = stCurLeft - stCurRight; //в миллисекундах

	// смещение в семплах!
	if (sub > 0)
	{
		m_start_delay_left = CHECK_TIMESHIFT(sub) * m_samples_per_ms;

		if (m_start_delay_left < m_frame_size)
		{
			m_start_delay_left = m_start_delay_left < m_frame_size / 2 ? 0 : m_frame_size;
		}
		else
		{
			m_start_delay_left = (m_start_delay_left / m_frame_size) * m_frame_size;
		}
	}
	else if (sub < 0)
	{
		sub = sub * (-1);

		m_start_delay_right = CHECK_TIMESHIFT(sub) * m_samples_per_ms;

		if (m_start_delay_right < m_frame_size)
		{
			m_start_delay_right = m_start_delay_right < m_frame_size / 2 ? 0 : m_frame_size;
		}
		else
		{
			m_start_delay_right = (m_start_delay_right / m_frame_size) * m_frame_size;
		}
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
bool file_mixer_stereo_t::fill_channel(media::AudioReader* source, short* buffer, int& timeshift)
{
	// если файл уже прочитан!
	if (source->eof())
	{
		memset(buffer, 0, m_frame_size * sizeof(short));
		return true;
	}

	// если сдвиг больше или равен размеру фрейма чтения
	if (timeshift >= m_frame_size)
	{
		memset(buffer, 0, m_frame_size * sizeof(short));
		timeshift -= m_frame_size;

		if (timeshift < 0)
			timeshift = 0;

		// не конец файла!
		return false;
	}

	// нормальная ситуация -- сдвиг уже пройден
	int read = source->readNext(buffer, m_frame_size);

	if (read == m_frame_size)
	{
		// данные еще есть!
		return false;
	}

	return true;
}
//-----------------------------------------------
