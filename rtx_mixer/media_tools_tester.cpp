/* RTX H.248 Media Gate. Media mixer tool
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_resampler.h"
#include "mmt_audio_mixer.h"
#include "mmt_audio_reader.h"
#include "mmt_file_mp3.h"
#include "mmt_recorder_wave.h"

static void test_mp3_reader(const char* infile, const char* outfile, int freq, int channels);
static void test_mp3_file(const char* infile, const char* outfile, int freq, int channels);
static void test_mp3_file_clean(const char* infile, const char* outfile, int freq, int channels);
//--------------------------------------
// ������������ ����� ������������
//--------------------------------------
void test_media_readers(int argc, char* argv[])
{
	int channels = 0;
	int freq = 0;
	rtl::String fmt;
	const char* in_file_name = nullptr;
	const char* out_file_name = nullptr;

	// ������ ����������
	for (int i = 2; i < argc; i++)
	{
		const char* arg = argv[i];

		if (strncmp(arg, "-F:", 3) == 0)
		{
			fmt.assign(arg + 3);
		}
		else if (strncmp(arg, "-C:", 3) == 0)
		{
			if (isdigit(*(arg + 3)))
			{
				channels = atoi(arg + 3);
			}
			else
			{
				channels = strcmp(arg + 3, "stereo") == 0 ? 2 : strcmp(arg + 3, "mono") == 0 ? 1 : 0;
			}
		}
		else if (strncmp(arg, "-R:", 3) == 0)
		{
			freq = atoi(arg + 3);
		}
		else if (in_file_name == nullptr)
		{
			in_file_name = arg;
		}
		else 
		{
			out_file_name = arg;
		}
	}

	// �������� ����������
	if (in_file_name == nullptr || in_file_name[0] == 0)
	{
		printf("error: incorrect parameters -- missing source file name\n");
		return;
	}

	test_mp3_reader(in_file_name, out_file_name, freq, channels);

	//test_mp3_file(in_file_name, out_file_name, freq, channels);

	//test_mp3_file_clean(in_file_name, out_file_name, freq, channels);
}

static void test_mp3_reader(const char* infile, const char* outfile, int freq, int channels)
{
	media::AudioReader* reader = nullptr;

	if (freq == 0)
		reader = media::AudioReader::create(infile, nullptr);
	else
		reader = media::AudioReader::create(infile, freq, nullptr);

	if (reader == nullptr)
	{
		printf("error: incorrect parameters -- bad source file or incorrect format\n");
		return;
	}

	// �������� ������ � wav

	media::FileWriterWAV wave(&Log);

	media::WAVFormat format;

	format.tag = media::WAVFormatTagPCM;
	format.samplesRate = reader->getOutputFrequency();
	format.channels = 1;
	format.bitsPerSample = 16;
	format.blockAlign = sizeof(short) * format.channels;
	format.bytesPerSecond = format.blockAlign * format.samplesRate;
	format.extraSize = 0;

	rtl::String outfile2 = outfile;

	int idx = outfile2.indexOf('.');
	outfile2.insert(idx, "_std");


	if (!wave.create(outfile2, &format))
	{
		reader->close();
		DELETEO(reader);
		printf("error: incorrect parameters -- bad target filename or incorrect format");
		return;
	}

	
	// ������ � ������ �� ������! -- 1 �������
	int pcm_buffer_size = 160;// format.bytesPerSecond / 2;
	short* pcm_buffer = (short*)MALLOC(pcm_buffer_size * sizeof(short));

	int pcm_read; // ������� ��������

	while ((pcm_read = reader->readNext(pcm_buffer, pcm_buffer_size)) > 0)
	{
		wave.write(pcm_buffer, pcm_read * sizeof(short));
	}

	reader->close();
	DELETEO(reader);

	wave.close();
}

static void test_mp3_file(const char* infile, const char* outfile, int freq, int channels)
{
	media::FileReaderMP3* reader = nullptr;

	reader = NEW media::FileReaderMP3();

	if (!reader->open(infile))
	{
		printf("error: incorrect parameters -- bad source file or incorrect format\n");
		return;
	}

	// �������� ������ � wav

	media::FileWriterWAV wave(&Log);
	media::FileWriterWAV wave2(&Log);

	media::WAVFormat format;

	format.tag = media::WAVFormatTagPCM;
	format.samplesRate = freq;// reader->get_freq();
	format.channels = 1; // reader->getChannelCount();
	format.bitsPerSample = 16;
	format.blockAlign = sizeof(short) * format.channels;
	format.bytesPerSecond = format.blockAlign * format.samplesRate;
	format.extraSize = 0;

	rtl::String outfile2 = outfile;

	int idx = outfile2.indexOf('.');
	outfile2.insert(idx, "_dir");

	if (!wave.create(outfile2, &format))
	{
		reader->close();
		DELETEO(reader);
		printf("error: incorrect parameters -- bad target filename or incorrect format");
		return;
	}

	media::WAVFormat format2;

	format2.tag = media::WAVFormatTagPCM;
	format2.samplesRate = reader->getFrequency();
	format2.channels = 1; // reader->getChannelCount();
	format2.bitsPerSample = 16;
	format2.blockAlign = sizeof(short) * format.channels;
	format2.bytesPerSecond = format.blockAlign * format.samplesRate;
	format2.extraSize = 0;

	rtl::String outfile3 = outfile;

	int idx3 = outfile3.indexOf('.');

	outfile3.insert(idx3, "_mono");

	if (!wave2.create(outfile3, &format2))
	{
		reader->close();
		wave.close();
		DELETEO(reader);
		printf("error: incorrect parameters -- bad target filename or incorrect format");
		return;
	}

	
	int read_buffer_size = 100000;
	
	short* pcm_buffer_left = (short*)MALLOC(read_buffer_size * sizeof(short));
	short* pcm_buffer_right = (short*)MALLOC(read_buffer_size * sizeof(short));
	short* pcm_buffer_mix = (short*)MALLOC(read_buffer_size * sizeof(short));
	short* pcm_buffer_out = (short*)MALLOC(read_buffer_size * sizeof(short));

	int pcm_read; // ������� ��������

	media::Resampler* resampler = media::createResampler(reader->getFrequency(), reader->getChannelCount(), format.samplesRate, format.channels);

	while ((pcm_read = reader->readNext(pcm_buffer_left, pcm_buffer_right, read_buffer_size)) > 0)
	{
		media::mixer__stereo_to_mono_d(pcm_buffer_left, pcm_buffer_right, pcm_buffer_mix, pcm_read);

		wave2.write(pcm_buffer_mix, pcm_read * sizeof(short));

		int src_used = 0;
		int dst_filled = resampler->resample(pcm_buffer_mix, pcm_read, &src_used, pcm_buffer_out, read_buffer_size);
		wave.write(pcm_buffer_out, dst_filled * sizeof(short));
	}

	reader->close();
	DELETEO(reader);

	wave.close();
	wave2.close();

	FREE(pcm_buffer_left);
	FREE(pcm_buffer_right);
	FREE(pcm_buffer_mix);
	FREE(pcm_buffer_out);
}

static short normalize_sample(int value)
{
	if (value > SHRT_MAX)
		return SHRT_MAX;
	if (value < SHRT_MIN)
		return SHRT_MIN;

	return value;
}

static void test_mp3_file_clean(const char* infile, const char* outfile, int freq, int channels)
{
	media::FileReaderMP3* reader = nullptr;

	reader = NEW media::FileReaderMP3();

	if (!reader->open(infile))
	{
		printf("error: incorrect parameters -- bad source file or incorrect format\n");
		return;
	}

	// �������� ������ � wav

	media::FileWriterWAV wave(&Log);

	media::WAVFormat format;

	format.tag = media::WAVFormatTagPCM;
	format.samplesRate = reader->getFrequency()/2;
	format.channels = 1;
	format.bitsPerSample = 16;
	format.blockAlign = sizeof(short) * format.channels;
	format.bytesPerSecond = format.blockAlign * format.samplesRate;
	format.extraSize = 0;

	rtl::String outfile2 = outfile;

	int idx = outfile2.indexOf('.');
	outfile2.insert(idx, "_clean");


	if (!wave.create(outfile2, &format))
	{
		reader->close();
		DELETEO(reader);
		printf("error: incorrect parameters -- bad target filename or incorrect format");
		return;
	}

	int read_buffer_size = 100000;
//	int pcm_buffer_size = read_buffer_size;// format.bytesPerSecond / 2;

	short* pcm_buffer_left = (short*)MALLOC(read_buffer_size * sizeof(short));
	short* pcm_buffer_right = (short*)MALLOC(read_buffer_size * sizeof(short));
	short* pcm_buffer_out = (short*)MALLOC(read_buffer_size * sizeof(short));
	short* pcm_buffer_re = (short*)MALLOC(read_buffer_size * sizeof(short));


	int pcm_read; // ������� ��������

	while ((pcm_read = reader->readNext(pcm_buffer_left, pcm_buffer_right, read_buffer_size)) > 0)
	{
		media::mixer__stereo_to_mono_d(pcm_buffer_left, pcm_buffer_right, pcm_buffer_out, pcm_read);
		
		int to_write = pcm_read / 2;
		
		for (int i = 0; i < to_write; i++)
		{
			int sample = pcm_buffer_out[i * 2];
				sample += pcm_buffer_out[(i * 2) + 1];
			
				pcm_buffer_re[i] = normalize_sample(sample / 2);
		}

		wave.write(pcm_buffer_re, to_write * sizeof(short));
	}

	reader->close();
	DELETEO(reader);

	wave.close();
}
