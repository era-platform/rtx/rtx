﻿/* RTX H.248 Media Gate. Media mixer tool
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mmt_file_rtp.h"
//--------------------------------------
//
//--------------------------------------
static int print_rtp_info(const char* rtp_file_path, FILE* out);
static void add_times(const rtl::DateTime& time, uint32_t milliseconds, rtl::DateTime& result);
//--------------------------------------
//
//--------------------------------------
static uint16_t __htons(uint16_t val)
{
	return ((val & 0xFF00) >> 8) | ((val & 0x00FF) << 8);
}
//--------------------------------------
//
//--------------------------------------
static uint32_t __htonl(uint32_t val)
{
	uint32_t l = __htons((val & 0x0000FFFF)) << 16;
	uint32_t h = __htons((val >> 16) & 0x0000FFFF);
	return  h | l;

}
#define __ntohs __htons
#define __ntohl __htonl
//--------------------------------------
//
//--------------------------------------
// static const char* gtt_to_string(uint64_t gtt, char* buffer, int size)
// {
// 	rtl::DateTime dt = rtl::DateTime::parse_gtt(gtt);
// 	int len = std_snprintf(buffer, size, "%04u.%02u.%02u %02u:%02u:%02u:%03u", dt.getYear(), dt.getMonth(), dt.getDay(), dt.getHour(), dt.getMinute(), dt.setSecond(), dt.getMilliseconds());
// 	if (len > 0)
// 		buffer[len] = 0;
// 	else
// 		buffer[0] = 0;

// 	return buffer;
// }
//--------------------------------------
//
//--------------------------------------
static const char* rtp_packet_type(uint32_t tp)
{
	static const char* names[] = {"DAT", "FMT", "RTP"};

	return tp < 3 ? names[tp] : "INV";
}
//--------------------------------------
//
//--------------------------------------
int print_rtp_file(int arc, char* argv[])
{
	rtl::String info = argv[2];

	int idx = info.lastIndexOf('.');

	if (idx != -1)
	{
		info.remove(idx, -1);
		info += ".txt";
	}
	else
	{
		info += ".txt";
	}

	FILE* out = fopen(info, "w");

	int res = print_rtp_info(argv[2], out != nullptr ? out : stdout);

	if (out != nullptr)
		fclose(out);

	return res;
}
//--------------------------------------
//
//--------------------------------------
static int print_rtp_info(const char* rtp_file_path, FILE* out)
{
	media::FileReaderRTP rtp_file;

	if (!rtp_file.open(rtp_file_path))
	{
		fprintf(stderr, "error: invalid file or file not found\n");
		return -1;
	}

	const media::RTPFileHeader& header = rtp_file.get_header();

	char sign[5];

	strncpy(sign, (char*)&header.sign, 4);
	sign[4] = 0;

	fprintf(out, "---------------------------------------------\nRTP Header\n---------------------------------------------\n");
	fprintf(out, "\tsign              : %s\n", sign);
	fprintf(out, "\tpack written      : %d\n", header.packets_rx);
	fprintf(out, "\tpack rec dropped  : %d\n", header.dropped_rec_packets);
	fprintf(out, "\tpack net dropped  : %d\n", header.dropped_net_packets);
	fprintf(out, "\tpack dropped      : %d\n", header.dropped_invalid_packets);
	fprintf(out, "---------------------------------------------\n");
	rtl::DateTime base_time = rtl::DateTime::parseGTS(header.rec_start_time);
	fprintf(out, "\trec start time    : %4d.%02d.%02d %02d:%02d:%02d:%03d\n",
		base_time.getYear(), base_time.getMonth(), base_time.getDay(),
		base_time.getHour(), base_time.getMinute(), base_time.setSecond(), base_time.getMilliseconds());
	fprintf(out, "\trec start ts      : %d\n", header.rec_start_timestamp);
	fprintf(out, "\trec stop ts       : %d\n", header.rec_stop_timestamp);
	rtl::DateTime ptime = 0;
	add_times(base_time, header.rec_stop_timestamp - header.rec_start_timestamp, ptime);
	fprintf(out, "\trec stop time     : %02d:%02d:%02d:%03d\n", ptime.getHour(), ptime.getMinute(), ptime.setSecond(), ptime.getMilliseconds());
	fprintf(out, "---------------------------------------------\n");
	fprintf(out, "\trecv start ts     : %08X\n", header.rx_start_timestamp);
	fprintf(out, "\trecv stop ts      : %08X\n", header.rx_stop_timestamp);
	fprintf(out, "---------------------------------------------\n");
	fprintf(out, "\tmedia length      : %d bytes\n", header.media_length);
	fprintf(out, "---------------------------------------------\nRTP Packets\n");

	const media::RTPFilePacketHeader* packet = nullptr;
	int pack_counter = 0;

	uint32_t last = header.rec_start_timestamp;
	uint32_t lastTS = 0;
	uint16_t last_seqn = 0;
	uint32_t current_ssrc = 0;
	
	fprintf(out, "+--------------+----------+-----+------+---------------+-----+---------+------------+--------+------------+--------+\n");
	fprintf(out, "| time         |        # | typ |  len |  v-p-x-cc-m   |  pt |     seq | media ts   |  shift |  rx ts     |  delay |\n");
	fprintf(out, "+--------------+----------+-----+------+---------------+-----+---------+------------+--------+------------+--------+\n");

	while ((packet = rtp_file.readNext()) != nullptr)
	{
		++pack_counter;
		add_times(base_time, packet->timestamp - header.rec_start_timestamp, ptime);

		if (packet->type == RTYPE_FORMAT)
		{
			int count = packet->length / sizeof(media::RTPFileMediaFormat);
			                                                                     //+-----+----------+------------+--------+------------+--------+
			fprintf(out, "| %02d:%02d:%02d:%03d | % 8d | %s | % 4d |               |     |         |            |        |            |        |\n",
				ptime.getHour(), ptime.getMinute(), ptime.setSecond(), ptime.getMilliseconds(),
				pack_counter, rtp_packet_type(packet->type), packet->length);

			for (int i = 0; i < count; i++)
			{
				const media::RTPFileMediaFormat& fmt = packet->format[i];
				fprintf(out, "+--------------+----------+-----+------+---------------+-----+---------+------------+--------+------------+--------+\n");
				
				fprintf(out, "|              | %s pt:%u freq:%u channels:%u params:%s fmtp:%s\n",
					fmt.encoding,
					fmt.payload_id,
					fmt.clock_rate,
					fmt.channels,
					fmt.params[0] != 0 ? fmt.params : "none",
					fmt.fmtp[0] != 0 ? fmt.fmtp : "none");
			}
			fprintf(out, "+--------------+----------+-----+------+---------------+-----+---------+------------+--------+------------+--------+\n");

		}
		else
		{
			const rtp_header_t* hdr = (const rtp_header_t*)packet->buffer;
			uint16_t seqn = __ntohs(hdr->seq);
			uint32_t ts = __ntohl(hdr->ts);
			short seq_diff = seqn - last_seqn;
			last_seqn = seqn;

			if (hdr->ssrc != current_ssrc)
			{
				
				fprintf(out, "|              | new SSRC %08X\n", __ntohl(hdr->ssrc));
				
				current_ssrc = hdr->ssrc;
			}

			fprintf(out, "| %02u:%02u:%02u:%03u | % 8d | %s | % 4u | %02u-%u-%u-%04u-%u | % 3u | %c % 5u | % 10u | % 6d | % 10u | % 6d |\n",
				ptime.getHour(), ptime.getMinute(), ptime.setSecond(), ptime.getMilliseconds(),
				pack_counter,
				rtp_packet_type(packet->type),
				packet->length,
				hdr->ver, hdr->pad, hdr->ext, hdr->cc, hdr->m,
				hdr->pt,
				seq_diff != 1 ? '*' : ' ', seqn,
				ts,
				lastTS != 0 ? ts - lastTS : 0,
				packet->timestamp,
				int(packet->timestamp - last));

			lastTS = ts;
			last = packet->timestamp;
		}
	}

	fprintf(out, "+--------------+----------+-----+------+---------------+-----+---------+------------+--------+------------+--------+\n\n	");

	return 0;
}

static void add_times(const rtl::DateTime& time, uint32_t milliseconds, rtl::DateTime& result)
{
	uint64_t gtt = time.getGTS() + milliseconds;

	result = rtl::DateTime::parseGTS(gtt);
}
