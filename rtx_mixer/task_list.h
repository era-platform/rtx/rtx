﻿/* RTX H.248 Media Gate. Media mixer tool
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//--------------------------------------
//
//--------------------------------------
enum task_command_t
{
	task_command_mix_e,
	task_command_join_e,
	task_command_unknown_e,
};
//--------------------------------------
//
//--------------------------------------
struct parse_result_t
{
	bool result;
	const char* reason; // only const literals
};
//--------------------------------------
//
//--------------------------------------
struct source_info_t
{
	const char* filename;
	uint32_t shift;
	uint32_t flags; // 0 - default, 1 - left, 2 - right
};
#define SOURCE_DEFAULT_CHANNEL 0
#define SOURCE_LEFT_CHANNEL 1
#define SOURCE_RIGHT_CHANNEL 2
//--------------------------------------
//
//--------------------------------------
class task_t
{
	task_command_t m_command;
	rtl::String m_id;
	rtl::ArrayT<source_info_t> m_source_list;
	rtl::String m_target;

	rtl::String m_enc_format;
	int m_enc_samples_rate;
	int m_enc_channels;
	rtl::String m_enc_fmtp;

public:
	task_t(task_command_t command, rtl::String id);
	~task_t() { cleanup(); }

	void add_source(const char* src, int length, uint32_t shift = 0, uint32_t stereo_flag = SOURCE_DEFAULT_CHANNEL);
	void set_target(const char* dst, int length) { m_target.assign(dst, length); }
	void set_format(const char* enc, int enc_len, int samples_rate, int channels, const char* fmtp, int fmtp_len);

	task_command_t get_command() { return m_command; }
	const rtl::String& get_id() { return m_id; }
	int get_source_count() { return m_source_list.getCount(); }
	const source_info_t* get_source(int index) { return &m_source_list[index]; }
	const rtl::String& get_target() { return m_target; }

	const rtl::String& getEncoding() { return m_enc_format; }
	const rtl::String& get_params() { return m_enc_fmtp; }
	int get_samples_rate() { return m_enc_samples_rate; }
	int getChannelCount() { return m_enc_channels; }

	void cleanup();
};
//--------------------------------------
//
//--------------------------------------
class task_list_t
{
	rtl::ArrayT<task_t*> m_task_list;
	parse_result_t m_parse_result;
public:
	task_list_t() { }
	~task_list_t() { clear(); }

	bool load_task_list(FILE* batch_file);

	int get_task_count() { return m_task_list.getCount(); }
	task_t* get_task(int index) { return m_task_list.getAt(index); }
	const char* get_error_reason() { return m_parse_result.result ? "" : m_parse_result.reason; }
	void clear();
};
//--------------------------------------
//
//--------------------------------------
class result_t
{
	rtl::String m_id;
	rtl::String m_failure_reason;
	rtl::ArrayT<int> m_errno_list;
	rtl::StringList m_file_list;
	int m_target_errno;

public:
	result_t(const char* id) : m_id(id), m_target_errno(0) { }
	~result_t() { }

	const rtl::String& get_id() { return m_id; }
	void set_failure(const char* text) { m_failure_reason = text; }
	void add_error(int error, const char* filename);
	void set_target_error(int error) { m_target_errno = error; }

	const rtl::String& get_failure() { return m_failure_reason; }
	int get_error_count() { return m_errno_list.getCount(); }
	int get_error_at(int index) { return m_errno_list.getAt(index); }
	rtl::String get_file_at(int index) { return m_file_list.getAt(index); }
	int get_target_error() { return m_target_errno; }
};
//--------------------------------------
//
//--------------------------------------
class result_list_t
{
	rtl::ArrayT<result_t*> m_result_list;
public:
	result_list_t() { }
	~result_list_t() { clear(); }

	void add_result(result_t* result) { m_result_list.add(result); }
	bool write_to_file(FILE* answer_file);

	void clear();
};
//--------------------------------------
