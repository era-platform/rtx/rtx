/* RTX H.248 Media Gate. Media mixer tool
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "task_list.h"
#include "file_mixer_mono.h"
#include "file_mixer_stereo.h"
//-----------------------------------------------
//
//-----------------------------------------------
int file_mixer(const char* batch)
{
	FILE* batch_file = fopen(batch, "r");

	if (batch_file == nullptr)
	{
		fprintf(stderr, "error: opening batch file filed. errno %d path:%s\n", errno, batch);
		return false;
	}

	rtl::String answer_filepath = batch;
	answer_filepath += "_answer";
	FILE* answer_file = fopen(answer_filepath, "w");

	if (answer_file == nullptr)
	{
		fprintf(stderr, "warning: creating answer file failed. errno %d path:%s\n", errno, (const char*)answer_filepath);
	}
	else
	{
		fprintf(stdout, "creating answer file ok: %s\n", (const char*)answer_filepath);
	}

	task_list_t task_list;
	result_list_t result_list;

	if (!task_list.load_task_list(batch_file))
	{
		fprintf(stderr, "error: batch file parse error %s\n", task_list.get_error_reason());
		return -1;
	}

	fclose(batch_file);
	batch_file = nullptr;

	for (int i = 0; i < task_list.get_task_count(); i++)
	{
		task_t* task = task_list.get_task(i);
		result_t* result = nullptr;

		if (task->get_command() == task_command_mix_e)
		{
			if (task->getChannelCount() == 2 && task->get_source_count() == 2)
			{
				file_mixer_stereo_t mixer;
				result = mixer.run(task);
			}
			else
			{
				file_mixer_mono_t mixer;
				result = mixer.run(task);
			}
		}
		else if (task->get_command() == task_command_join_e)
		{
			file_mixer_mono_t mixer;
			result = mixer.run(task);
		}
		else
		{
			// ������ ����� ����� ����� ��������� INTERNAL_ERROR
			result = NEW result_t(task->get_id());
			result->set_failure("unknown command");
		}

		result_list.add_result(result);
	}

	if (answer_file != nullptr)
	{
		result_list.write_to_file(answer_file);
		fclose(answer_file);
		answer_file = nullptr;
	}

	printf("mix done\n\n");

	return 0;
}
//-----------------------------------------------
