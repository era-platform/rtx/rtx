﻿/* RTX H.248 Media Gate. Media mixer tool
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "task_list.h"
#include "mmt_wave_format.h"
#include "mmt_audio_reader_lrtp.h"
#include "mmt_recorder.h"
//--------------------------------------
//
//--------------------------------------
struct mix_buffer_t
{
	short samples[640];
	int count;
};
//--------------------------------------
//
//--------------------------------------
class file_mixer_mono_t
{
	rtl::String m_out_encoding;
	int m_out_freq;
	int m_out_channels;		// если стерео и файлов источников 2 то пишем по разным каналам для mp3 или pcm
	rtl::String m_out_fmtp;	// для mp3 битрейт или индекс сжатия

	rtl::ArrayT<media::AudioReader*> m_sources;
	media::RecorderMono* m_target_file;

	//задержка начала записи i-того файла относительно самого первого файла
	rtl::ArrayT<uint32_t> m_start_delay;
	mix_buffer_t m_mix_buffer_1;
	mix_buffer_t m_mix_buffer_2;

	task_command_t m_cmd;
	result_t* m_result;

public:
	file_mixer_mono_t();
	~file_mixer_mono_t();

	//void set_out_format(const char* encoding, int freq, int channels, const char* fmtp);
	result_t* run(task_t* task);

private:
	bool open_sources(task_t* task);
	void close_sources();
	bool create_target(const char* target);
	void close_target();
	bool mix_files();
	bool join_files();
	void check_mix_offsets();
	void check_join_offsets();
	void mix_samples(short* destination, const short* sources, int count);
};
//--------------------------------------
