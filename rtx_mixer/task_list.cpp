﻿/* RTX H.248 Media Gate. Media mixer tool
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "task_list.h"
//--------------------------------------
//
//--------------------------------------
task_t::task_t(task_command_t command, rtl::String id) :
m_command(command), m_id(id)
{
	m_enc_format = "L16";
	m_enc_samples_rate = 8000;
	m_enc_channels = 1;
}
//--------------------------------------
//
//--------------------------------------
void task_t::cleanup()
{
	for (int i = 0; i < m_source_list.getCount(); i++)
	{
		char* fp = (char*)m_source_list[i].filename;
		FREE(fp);
	}

	m_source_list.clear();
}
//--------------------------------------
//
//--------------------------------------
void task_t::add_source(const char* src, int length, uint32_t shift, uint32_t flags)
{
	source_info_t src_info = {rtl::strdup(src,length),shift,flags};
	m_source_list.add(src_info);

	//LOG_CALL("task", "add source %s", src_info.filename);
}
//--------------------------------------
//
//--------------------------------------
void task_t::set_format(const char* enc, int enc_len, int samples_rate, int channels, const char* params, int params_len)
{
	m_enc_format.assign(enc, enc_len);
	m_enc_samples_rate = samples_rate;
	m_enc_channels = channels;
	m_enc_fmtp.assign(params, params_len);

	//LOG_CALL("task", "set format %s %d %d %s", (const char*)m_enc_format, m_enc_samples_rate, m_enc_channels, (const char*)m_enc_fmtp);
}
//--------------------------------------
//
//--------------------------------------
static char* skip_ws(char* text)
{
	while (*text == ' ' || *text == '\t') text++;
	return text;
}
//--------------------------------------
//
//--------------------------------------
static const char* skip_ws(const char* text)
{
	while (*text == ' ' || *text == '\t') text++;
	return text;
}
//--------------------------------------
//
//--------------------------------------
static parse_result_t parse_block(const char* text, task_command_t& cmd, rtl::String& id)
{
	const char* ptr = skip_ws(text);

	if (std_strnicmp(ptr, "MIX", 3) == 0)
	{
		cmd = task_command_mix_e;
		ptr += 3;
	}
	else if (std_strnicmp(ptr, "JOIN", 4) == 0)
	{
		cmd = task_command_join_e;
		ptr += 4;
	}
	else
	{
		return { false, "unknown command!" };
	}

	//LOG_CALL("mix", "block %s", text);

	ptr = skip_ws(ptr);

	const char* end = strpbrk(ptr, " \t\n\r");

	if (end == nullptr)
		id.assign(ptr);
	else
		id.assign(ptr, PTR_DIFF(end, ptr));

	if (!id.isEmpty())
	{
		return { true, nullptr };
	}
	else
	{
		return{ false, "empty task id!" };
	}
}
#define KEYWORD_SOURCE	1
#define KEYWORD_TARGET	2
#define KEYWORD_FORMAT	3
#define KEYWORD_DELETE	4
#define KEYWORD_END		5
#define KEYWORD_INVALID	-1
//--------------------------------------
//
//--------------------------------------
static int get_key(const char* line, const char*& params)
{
	if (std_strnicmp(line, "SOURCE", 6) == 0)
	{
		params = line + 6;
		return KEYWORD_SOURCE;
	}

	if (std_strnicmp(line, "TARGET", 6) == 0)
	{
		params = line + 6;
		return KEYWORD_TARGET;
	}

	if (std_strnicmp(line, "FORMAT", 6) == 0)
	{
		params = line + 6;
		return KEYWORD_FORMAT;
	}

	if (std_strnicmp(line, "DELETE", 6) == 0)
	{
		params = line + 6;
		return KEYWORD_DELETE;
	}

	if (std_strnicmp(line, "END", 3) == 0)
	{
		params = line + 3;
		return KEYWORD_END;
	}

	return KEYWORD_INVALID;
}
//--------------------------------------
//
//--------------------------------------
static parse_result_t parse_source(const char* params, task_t* task, bool parse_shift)
{
	// join sample: "13980 /home/rec/1923/source1.rtp ; comments"
	// mix sample: "/home/rec/1923/source1.rtp   ; comment"

	int shift = 0;
	int length = -1;
	const char* filepath = nullptr;
	int stereo_flag = 0; // default

	if (parse_shift)
	{
		params = skip_ws(params);
		filepath = strpbrk(params, " \t");

		if (filepath == nullptr)
		{
			// delimeter must be presend!
			return { false, "empty source path!" };
		}

		shift = strtol(params, nullptr, 10);

		//LOG_CALL("mix", "shift %d", shift);

		filepath = skip_ws(filepath);
	}
	else
	{

		params = skip_ws(params);

		if (std_strnicmp(params, "left ", 5) == 0)
		{
			params += 5;
			stereo_flag = 1; // LEFT
		}
		else if ((params[0] == 'l' || params[0] == 'L') && params[1] == ' ')
		{
			params += 2;
			stereo_flag = 1; // LEFT
		}
		else if (std_strnicmp(params, "right ", 6) == 0)
		{
			params += 6;
			stereo_flag = 2; // RIGHT
		}
		else if ((params[0] == 'r' || params[0] == 'R') && params[1] == ' ')
		{
			params += 2;
			stereo_flag = 2; // RIGHT
		}

		filepath = skip_ws(params);
	}

	//LOG_CALL("mix", "source file (%s) path %s", filepath, stereo_flag == 1 ? "left" : stereo_flag == 2 ? "right" : "default");
	
	const char* delim = strpbrk(filepath, " \t\n\r");
	if (delim != nullptr)
	{
		length = PTR_DIFF(delim, filepath);
	}

	task->add_source(filepath, length, shift, stereo_flag);
	
	return { true, nullptr };
}
//--------------------------------------
//
//--------------------------------------
static parse_result_t parse_target(const char* params, task_t* task)
{
	int length = -1;
	const char* filepath = skip_ws(params);
	const char* delim = strpbrk(filepath, " \t\n\r");

	if (delim != nullptr)
	{
		length = PTR_DIFF(delim, filepath);
	}
	if (length > 0)
	{
		task->set_target(filepath, length);
		return { true, nullptr };
	}

	return { false, "empty target path!" };
}
//--------------------------------------
//
//--------------------------------------
static parse_result_t parse_format(const char* params, task_t* task)
{
	params = skip_ws(params);

	//       |        .           .        .
	// FORMAT encoding sample_rate channels [params]

	int samples_rate, channels;
	const char* ptr, *encoding, *fmtp = nullptr;
	int encoding_len = -1, fmtp_len = -1;

	encoding = params;

	if ((ptr = strpbrk(params, " \t")) == nullptr)
	{
		return{ false, "FORMAT has no encoding name parameter!" };
	}

	encoding_len = PTR_DIFF(ptr, encoding);

	ptr = skip_ws(ptr);
	samples_rate = strtol(ptr, nullptr, 10);

	if ((ptr = strpbrk(ptr, " \t")) == nullptr)
	{
		return{ false, "FORMAT has no sample rate parameter!" };
	}

	ptr = skip_ws(ptr);
	
	if (isdigit(*ptr))
	{
		channels = strtol(ptr, nullptr, 10);
	}
	else
	{
		if (std_strnicmp(ptr, "mono", 4) == 0)
			channels = 1;
		else if (std_strnicmp(ptr, "stereo", 4) == 0)
			channels = 2;
		else
		{
			return { false, "unknown audio channel parameter!" };
		}
	}

	if ((ptr = strpbrk(ptr, " \t")) != nullptr)
	{
		fmtp = skip_ws(ptr);
		if ((ptr = strpbrk(ptr, " \t")) != nullptr)
		{
			fmtp_len = PTR_DIFF(ptr, fmtp);
		}
	}

	task->set_format(encoding, encoding_len, samples_rate, channels, fmtp, fmtp_len);

	return { true, nullptr };
}
//--------------------------------------
//
//--------------------------------------
/*
; формат файла заданий
BEGIN MIX id1         							; начало блока микшера. Уникальный идентификатор блока для связывания с результатом
SOURCE rtp_src_filepath1						; полный путь к источнику 1
SOURCE rtp_src_filepath2
...
SOURCE rtp_src_filepathN						; полный путь к источнику N
TARGET dst_filepath								; полный путь к результату
FORMAT encoding sample_rate channels [params]	; название кодировки (PCM, PCMA, PCMU, MP3, GSM, ...)
; частота 8000-192xxx для PCM и MP3
; 1 - mono, 2 - stereo можно указывать цифры или слова stereo mono
; доп. параметры для кодека
END
; comment
BEGIN JOIN id2	    							; начало блока склейки файлов
SOURCE m src_filepath1							; обязательное смещение в миллисекундах.
SOURCE k src_filepath2
...
SOURCE 0 src_filepathN							; Ноль указывает что файл будет микшироватся сразу
TARGET dst_filepath								; полный путь к результату
FORMAT encoding sample_rate channels [params]
END

формат файла результата

; формат файла результата
RESULT id1										; начало блока результата
SOURCE FAILED errno filepath2 					; код ошибки ненормального файла2
SOURCE FAILED errno filepath3 					; код ошибки ненормального файла3
TARGET OK										; результат работы
END												; конец блока
; comment
RESULT id2										; начало блока результата
TARGET FAILED errno								; код ошибки
END												; конец блока
*/
bool task_list_t::load_task_list(FILE* batch_file)
{
	char line[256];
	task_t* task = nullptr;
	task_command_t cmd;
	rtl::String id;

	while (fgets(line, 256, batch_file) != nullptr)
	{
		//LOG_CALL("mix", "line read %s", line);
		char* text = skip_ws(line);

		if (text[0] == '\r' || text[0] == '\n' || text[0] == ';')
			continue;

		//LOG_CALL("mix", "line processing..");

		if (task != nullptr)
		{
			const char* params = nullptr;
			int keyword = get_key(text, params);

			//LOG_CALL("mix", "task key %d", keyword);

			switch (keyword)
			{
			case KEYWORD_SOURCE:
				m_parse_result = parse_source(params, task, cmd == task_command_join_e);
				break;
			case KEYWORD_TARGET:
				m_parse_result = parse_target(params, task);
				break;
			case KEYWORD_FORMAT:
				m_parse_result = parse_format(params, task);
				break;
			case KEYWORD_END:
				m_task_list.add(task);
				task = nullptr;
				break;
			default:
				m_parse_result.result = false;
				m_parse_result.reason = "invalid keyword";
				break;
			}

			if (!m_parse_result.result)
			{
				//LOG_ERROR("mix", "parser error : %s", m_parse_result.reason);
				fprintf(stderr, "parser error : '%s'\n", m_parse_result.reason);
					DELETEO(task);
				return false;
			}
		}
		else if (std_strnicmp(text, "BEGIN", 5) == 0)
		{
			// блок с заданием!
			m_parse_result = parse_block(text + 5, cmd, id);
			if (!m_parse_result.result)
			{
				//LOG_ERROR("mix", "parser error : %s", m_parse_result.reason);
				fprintf(stderr, "parser error : '%s'", m_parse_result.reason);
				return false;
			}
			
			task = NEW task_t(cmd, id);
		}
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
void task_list_t::clear()
{
	for (int i = 0; i < m_task_list.getCount(); i++)
	{
		task_t* task = m_task_list[i];
		DELETEO(task);
	}

	m_task_list.clear();
}
//--------------------------------------
//
//--------------------------------------
void result_t::add_error(int error, const char* filename)
{
	m_errno_list.add(error);
	m_file_list.add(filename);
}
//--------------------------------------
//
//--------------------------------------
bool result_list_t::write_to_file(FILE* answer_file)
{
	if (answer_file == nullptr)
		return true;

	for (int i = 0; i < m_result_list.getCount(); i++)
	{
		result_t* result = m_result_list[i];

		fprintf(answer_file, "RESULT %s\r\n", (const char*)result->get_id());

		const rtl::String failure = result->get_failure();
		if (!failure.isEmpty())
		{
			fprintf(answer_file, "FAILED %s\r\n", (const char*)failure);
		}
		else
		{
			for (int j = 0; j < result->get_error_count(); j++)
			{
				fprintf(answer_file, "SOURCE FAILED %d %s\r\n", result->get_error_at(j), (const char*)result->get_file_at(j));
			}

			if (result->get_target_error() > 0)
			{
				fprintf(answer_file, "TARGET FAILED %d\r\n", result->get_target_error());
			}
			else
			{
				fprintf(answer_file, "TARGET OK\r\n");
			}
		}
		fprintf(answer_file, "END\r\n");
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
void result_list_t::clear()
{
	for (int i = 0; i < m_result_list.getCount(); i++)
	{
		result_t* result = m_result_list[i];
		DELETEO(result);
	}

	m_result_list.clear();
}
//--------------------------------------
