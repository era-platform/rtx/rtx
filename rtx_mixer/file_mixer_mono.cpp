﻿/* RTX H.248 Media Gate. Media mixer tool
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include <errno.h>
#include "file_mixer_mono.h"
//-----------------------------------------------
//
//-----------------------------------------------
//#define LOG_PREFIX "mix-mn"
#define MIX_MONO_FREQ 8000
//-----------------------------------------------
//
//-----------------------------------------------
file_mixer_mono_t::file_mixer_mono_t() : m_out_freq(0), m_out_channels(0), m_target_file(nullptr)
{
	memset(&m_mix_buffer_1, 0, sizeof m_mix_buffer_1);
	memset(&m_mix_buffer_2, 0, sizeof m_mix_buffer_2);
}
//--------------------------------------
//
//--------------------------------------
file_mixer_mono_t::~file_mixer_mono_t()
{
	close_target();
	close_sources();
}
//--------------------------------------
//
//--------------------------------------
result_t* file_mixer_mono_t::run(task_t* task)
{
	m_result = NEW result_t(task->get_id());
	m_cmd = task->get_command();

	m_out_encoding = task->getEncoding();
	m_out_freq = task->get_samples_rate();
	m_out_channels = task->getChannelCount();
	m_out_fmtp = task->get_params();

	if (!open_sources(task))
	{
		//LOG_ERROR(LOG_PREFIX, "Unable to open source files!");
		fprintf(stderr, "Unable to open source files!\n");
		return m_result;
	}

	if (!create_target(task->get_target()))
	{
		//LOG_ERROR(LOG_PREFIX, "Unable to create target file!");
		fprintf(stderr, "Unable to create target file!\n");
		return m_result;
	}

	bool res;

	if (m_cmd == task_command_mix_e)
	{
		// standard mixing or joining with external shifts
		res = mix_files();
	}
	else
	{
		// simple join
		res = join_files();
	}

	/*if (!res)
	{
		LOG_ERROR(LOG_PREFIX, "MixFiles() failed!");
	}*/

	close_target();

	close_sources();

	return m_result;
}
//--------------------------------------
//
//--------------------------------------
bool file_mixer_mono_t::open_sources(task_t* task)
{
	close_sources();

	//LOG_MIXER(LOG_PREFIX, "Source files opening...");

	if (task->get_source_count() <= 0)
	{
		//LOG_ERROR(LOG_PREFIX, "No files to open!");
		fprintf(stderr, "No source files to open!\n");
		m_result->add_error(-1, nullptr);
		return false;
	}

	//открываем файлы
	for (int i = 0; i < task->get_source_count(); ++i)
	{
		const source_info_t* src = task->get_source(i);
		media::AudioReader* source = media::AudioReader::create(src->filename, 8000, nullptr);

		if (source != nullptr) // ->open(sourcepath)
		{
			//LOG_MIXER(LOG_PREFIX, "File \'%s\' opened", (const char*)src->filename);
			m_sources.add(source);
			if (m_cmd == task_command_join_e)
			{
				m_start_delay.add(src->shift);
			}
		}
		else
		{
			//LOG_MIXER(LOG_PREFIX, "Unable to open file \'%s\'", src->filename);
			int err = errno;
			m_result->add_error(err != 0 ? err : ENOENT, src->filename);
		}
	}

	if (!m_sources.getCount())
	{
		return false;
	}

	for (int i = 0; i < m_sources.getCount(); ++i)
	{
		m_sources[i]->setupReader(m_out_freq);
	}

	//определяем смещения файлов
	if (m_cmd == task_command_mix_e)
		check_mix_offsets();
	else
		check_join_offsets();

	return m_result->get_error_count() == 0;
}
//--------------------------------------
//
//--------------------------------------
void file_mixer_mono_t::close_sources()
{
	if (m_sources.getCount())
	{
		//LOG_MIXER(LOG_PREFIX, "Source files closing...");
		for (int i = 0; i < m_sources.getCount(); ++i)
		{
			media::AudioReader* source = m_sources[i];
			source->close();
			DELETEO(source);
		}
	}

	m_sources.clear();
	m_start_delay.clear();
}
//--------------------------------------
//
//--------------------------------------
bool file_mixer_mono_t::create_target(const char* target)
{
	if (target == nullptr || target[0] == 0)
	{
		m_result->set_target_error(-1);
		return false;
	}

	m_target_file = media::RecorderMono::create(MIX_MONO_FREQ, m_out_encoding);

	if (m_target_file != nullptr)
	{
		errno = 0;
		if (!m_target_file->create(target, m_out_encoding, m_out_freq, m_out_fmtp))
		{
			int err = errno;
			if (err != 0)
			{
				fprintf(stderr, "error: target file error %d", err);
				m_result->set_target_error(err);
			}
			else
				m_result->set_failure("target file create error (encoder)");

			DELETEO(m_target_file);
			m_target_file = nullptr;
		}
	}

	return m_target_file != nullptr;
}
//--------------------------------------
//
//--------------------------------------
void file_mixer_mono_t::close_target()
{
	if (m_target_file != nullptr)
	{
		m_target_file->close();
		DELETEO(m_target_file);
		m_target_file = nullptr;
	}
}
//--------------------------------------
//
//--------------------------------------
bool file_mixer_mono_t::mix_files()
{
	//LOG_MIXER(LOG_PREFIX, "Begin mix...");

	mix_buffer_t mix1 = { 0 };
	mix_buffer_t mix2 = { 0 };
//	mix_buffer_t mixGsm = { 0 };
//	mix_buffer_t bufEnc = { 0 };
	short silence[160] = { 0 };

	mix_buffer_t* pMix1 = &mix1;
	mix_buffer_t* pMix2 = &mix2;

	short pcm_buffer[160] = { 0 };

	short* pcm = nullptr;
	int pcm_read = 0;
	int pcm_read_sum = 0;
	int pcm_written = 0;
	int samples_sum = 0, packets_sum = 0;
	try
	{
		//пока не прочитаем все файлы до конца
		while (m_sources.getCount())
		{
			pMix1 = (pMix1 == &mix1) ? &mix2 : &mix1;
			pMix2 = (pMix2 == &mix2) ? &mix1 : &mix2;

			//микшируем следующие 20 мс каждого файла
			int i = 0;

			while (m_sources.getCount() && (i < m_sources.getCount()))
			{
				//файл начинаем читать если только "подошло его время"
				//(таким образом выравниваем файлы по времени начала записи)
				int delay = m_start_delay[i];
				if (delay >= 20)
				{
					//отсчет времени уменьшаем и "читаем" тишину
					delay -= 20;
					m_start_delay[i] = delay;
					delay = 0;
					pcm = silence;//заполняем тишиной
					pcm_read = 160;
				}
				else
				{
					delay *= 8;//преобразуем в кол-во сэмплов
					//если файл кончился, закрываем его
					media::AudioReader* source = m_sources[i];
					if (!(pcm_read = source->readNext(pcm_buffer, 160)))
					{
						//LOG_MIXER(LOG_PREFIX, "Closing source file: \'%s\'", (const char*)source->getFilePath());
						samples_sum += source->statSamplesRead();
						packets_sum += source->statPacketsRead();
						source->close();
						DELETEO(source);
						m_sources.removeAt(i);
						m_start_delay.removeAt(i);
						continue;
					}
					pcm = pcm_buffer;
					pcm_read_sum += pcm_read;
				}

				//текущий пакет может не влезть полностью, если он пишется в буфер не с начала буфера (т.е. со смещением)
				//поэтому пишем его по частям

				//сколько сэмплов пишем за первый заход
				int copySamples = MIN(/*кол-во сэмплов до заполнения буфера
									  (точнее его середины, т.к. используем только половину его)*/160 - delay,
									  /*кол-во имеющихся в наличии сэмплов*/pcm_read);
				if (copySamples)
				{
					mix_samples(pMix1->samples + delay, pcm, copySamples);
				}

				int writtenSamples = delay + copySamples;//сколько в итоге сэмплов текущего пакета в буфере
				if (pMix1->count < writtenSamples)
					pMix1->count = writtenSamples;//реальных данных в первом буфере

				//оставшиеся сэмплы докопируем в начало второго буфера
				if (int restSamples = pcm_read - copySamples)
				{
					mix_samples(pMix2->samples, pcm + copySamples, restSamples);
					if (pMix2->count < restSamples)
						pMix2->count = restSamples;//реальных данных во втором буфере
				}

				++i;
			}

			//теперь смикшированное кодируем и отправляем в файл
			//if (m_container == MIX_CONTAINER_WAV)
			//{
			//	if (m_codec == MIX_CODEC_GSM610)
			//	{
			//		//если данные были, то значит это уже вторая часть,
			//		//и поэтому далее запишем их в файл
			//		bool flush = mixGsm.length > 0;
			//		//добавляем новые данные к старым
			//		memcpy(mixGsm.buffer + mixGsm.length, pMix1->buffer, pMix1->length);
			//		mixGsm.length += pMix1->length;
			//		//кодируем и отправляем в файл
			//		if (flush)
			//		{
			//			bufEnc.length = pCodec->encode(bufEnc.buffer, sizeof bufEnc.buffer, (const short*)mixGsm.buffer, mixGsm.length / 2);
			//			m_target_file->write(bufEnc.buffer, bufEnc.length);
			//			mixGsm.length = 0;
			//		}
			//	}
			//	else if (pMix1->length > 0)
			//	{
			//		if (pCodec)
			//		{
			//			bufEnc.length = pCodec->encode(bufEnc.buffer, sizeof bufEnc.buffer, (const short*)pMix1->buffer, pMix1->length / 2);
			//			m_target_file->write(bufEnc.buffer, bufEnc.length);
			//		}
			//		else
			//		{
			//			m_target_file->write((short*)pMix1->buffer, nullptr, pMix1->length / sizeof(short));
			//		}
			//	}
			//}
			//else if (m_container == MIX_CONTAINER_MP3)
			//{
			//	m_target_file->write((const short*)pMix1->buffer, nullptr, pMix1->length / sizeof(short));
			//}
			//else
			//{
			//	//другое?..
			//}

			pcm_written += pMix1->count;
			m_target_file->write(pMix1->samples, pMix1->count);

			memset(pMix1, 0, sizeof(mix_buffer_t));
		}

		//скидываем в файл остатки
		//if (m_container == MIX_CONTAINER_WAV)
		//{
		//	if (m_codec == MIX_CODEC_GSM610)
		//	{
		//		//добавляем остатки к старым данным
		//		if (pMix2->length)
		//		{
		//			memcpy(mixGsm.buffer + mixGsm.length, pMix2->buffer, pMix2->length);
		//			mixGsm.length += pMix2->length;
		//		}
		//		//кодируем и отправляем в файл
		//		if (mixGsm.length)
		//		{
		//			bufEnc.length = pCodec->encode(bufEnc.buffer, sizeof bufEnc.buffer, (const short*)mixGsm.buffer, mixGsm.length / 2);
		//			m_target_file->write(bufEnc.buffer, bufEnc.length);
		//		}
		//	}
		//	else if (pMix2->length)
		//	{
		//		if (pCodec)
		//		{
		//			bufEnc.length = pCodec->encode(bufEnc.buffer, sizeof bufEnc.buffer, (const short*)pMix2->buffer, pMix2->length / 2);
		//			m_target_file->write(bufEnc.buffer, bufEnc.length);
		//		}
		//		else
		//			m_target_file->write((const short*)pMix2->buffer, nullptr, pMix2->length / sizeof(short));
		//	}
		//}
		//else if (m_container == MIX_CONTAINER_MP3)
		//{
		//	m_target_file->write((const short*)pMix2->buffer, nullptr, pMix2->length / sizeof(short));
		//}
		//else
		//{
		//	//другое?..
		//}

		pcm_written += pMix2->count;
		m_target_file->write(pMix2->samples, pMix2->count);

		m_target_file->flush();
	}
	catch (rtl::Exception& ex)
	{
		//LOG_ERROR(LOG_PREFIX, "EXCEPTION in MixFiles():\n%s", ex.get_message());
		fprintf(stderr, "EXCEPTION in MixFiles():\n%s\n", ex.get_message());
	}

	//LOG_MIXER(LOG_PREFIX, "Mix complete... samples (%d/%d) packets (%d) -> %d/%d samples written", pcm_read_sum, samples_sum, packets_sum, pcm_written, m_target_file->statSamplesWritten());

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool file_mixer_mono_t::join_files()
{
	//LOG_MIXER(LOG_PREFIX, "Begin join...");

//	mix_buffer_t bufGsm = { 0 };
//	mix_buffer_t bufEnc = { 0 };

	try
	{
		while (m_sources.getCount())
		{
			media::AudioReader* source = m_sources[0];
			short pcm[160];
			int len = 0;
			
			while (true)
			{
				if ((len = source->readNext(pcm, 160)) == 0)
				{
					source->close();
					DELETEO(source);
					m_sources.removeAt(0);
					m_start_delay.removeAt(0);
					break;
				}

				//в wav
				m_target_file->write(pcm, len);
			}
		}
	}
	catch (rtl::Exception& ex)
	{
		//LOG_ERROR(LOG_PREFIX, "EXCEPTION in JoinFiles():\n%s", ex.get_message());
		fprintf(stderr, "EXCEPTION in JoinFiles():\n%s\n", ex.get_message());
	}

	//LOG_MIXER(LOG_PREFIX, "Join complete.");

	return true;
}
//--------------------------------------
//
//--------------------------------------
void file_mixer_mono_t::mix_samples(short* destination, const short* source, int count)
{
	if (destination && source)
	{
		int sample = 0;

		for (int i = 0; i < count; ++i)
		{
			sample = destination[i] + source[i];
			if (sample > 32767)
				sample = 32767;
			else if (sample < -32767)
				sample = -32767;

			destination[i] = (short)sample;
		}
	}
}
//--------------------------------------
//
//--------------------------------------
void file_mixer_mono_t::check_mix_offsets()
{
	rtl::ArrayT<uint64_t> times;	// массив с отметками времени каждого файла
	uint64_t min_shift = 0;		// нет отметок
	int shift_count = 0;

	//извлекаем отметки из каждого файла, заодно определяя наименьшую
	for (int i = 0; i < m_sources.getCount(); ++i)
	{
		uint64_t cur_shift = m_sources[i]->getStartTime();

		times.add(cur_shift);

		if (cur_shift == 0)
			continue;
		
		if (min_shift == 0)
			min_shift = cur_shift;
		else if (min_shift > cur_shift)
			min_shift = cur_shift;

		shift_count++;
	}

	if (shift_count == 1)
	{
		for (int i = 0; i < m_sources.getCount(); ++i)
		{
			m_start_delay.add(0);
		}

		return;
	}

	//в массив m_startDelay заносим только относительное время
	for (int i = 0; i < times.getCount(); ++i)
	{
		uint64_t cur_shift = times[i];

		if (cur_shift == 0)
		{
			m_start_delay.add(0);
		}
		else
		{
			// ввел новый формат миллисекунды : struct timeval -> миллисекунды с 1 января 1970 года
			int64_t sub = cur_shift - min_shift;

			//на всякий случай, если разница больше чем час, не используем
			if (sub >= 3600000)
				sub = 0;

			int value = (int)sub;
			m_start_delay.add(value);
		}
	}
}
//--------------------------------------
//
//--------------------------------------
void file_mixer_mono_t::check_join_offsets()
{
	/*
	Если все смещения 0 то простая склейка.
	Иначе выставляем всем сдвиги и делаем микс.
	*/

	for (int i = 0; i < m_start_delay.getCount(); i++)
	{
		if (m_start_delay[i] > 0)
		{
			m_cmd = task_command_mix_e;
			return;
		}
	}
}
//--------------------------------------
