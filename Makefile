LIBS=\
rtx_711_module/librtx_codec_audio_g711.so \
rtx_729_module/librtx_codec_audio_g729.so \
rtx_G722_module/librtx_codec_audio_g722.so \
rtx_G726_module/librtx_codec_audio_g726.so \
rtx_codec_video_h263/librtx_codec_video_h263.so \
rtx_codec_video_h264/librtx_codec_video_h264.so \
rtx_codec_video_vpx/librtx_codec_video_vpx.so \
rtx_codec/librtx_codec.so \
rtx_crypto/librtx_crypto.so \
rtx_gsm_module/librtx_codec_audio_gsm.so \
rtx_media_engine/librtx_media_engine.so \
rtx_media_tools/librtx_media_tools.so \
rtx_megaco/librtx_megaco.so \
rtx_netlib/librtx_netlib.so \
rtx_opus_module/librtx_codec_audio_opus.so \
rtx_rtplib/librtx_rtplib.so \
rtx_speex_module/librtx_codec_audio_speex.so \
rtx_stdlib/librtx_stdlib.so \
rtx_sip/librtx_sip.so \
rtx_aux/librtx_aux.so \
rtx_client/librtx_client.so

DIRS=\
rtx_711_module \
rtx_729_module \
rtx_G722_module \
rtx_G726_module \
rtx_aux \
rtx_client \
rtx_codec \
rtx_codec_video_h263 \
rtx_codec_video_h264 \
rtx_codec_video_vpx \
rtx_crypto \
rtx_gsm_module \
rtx_media_engine \
rtx_media_engine_test \
rtx_media_tools \
rtx_media_tools_test \
rtx_megaco \
rtx_megaco_test \
rtx_netlib \
rtx_opus_module \
rtx_rtplib \
rtx_sip \
rtx_speex_module \
rtx_stdlib \
rtx_mg3 \
rtx_mixer

UNAME_S := $(shell uname -s)

TARGETS = \
	build/rtx_mg3 \
	build/rtx_mixer
ifeq ($(UNAME_S),Linux)
TARGETS += \
	build/rtx_mixer_test \
	build/rtx_megaco_test \
	build/rtx_media_engine_test \
	build/rtx_media_tools_test
endif

TARGET_LIBS = \
	build/librtx_codec_audio_g711.so \
	build/librtx_codec_audio_g729.so \
	build/librtx_codec_audio_g722.so \
	build/librtx_codec_audio_g726.so \
	build/librtx_codec_video_h263.so \
	build/librtx_codec_video_h264.so \
	build/librtx_codec_video_vpx.so \
	build/librtx_codec.so \
	build/librtx_crypto.so \
	build/librtx_codec_audio_gsm.so \
	build/librtx_media_engine.so \
	build/librtx_media_tools.so \
	build/librtx_megaco.so \
	build/librtx_netlib.so \
	build/librtx_codec_audio_opus.so \
	build/librtx_rtplib.so \
	build/librtx_codec_audio_speex.so \
	build/librtx_stdlib.so \
	build/librtx_sip.so \
	build/librtx_aux.so \
	build/librtx_client.so \

.PHONY: all build clean distclean

all: $(TARGETS)

.SUFFIXES:

# target files in build/

$(TARGETS): $(TARGET_LIBS) | build
$(TARGET_LIBS): | build

build:
	mkdir -p build

build/rtx_mg3: rtx_mg3/rtx_mg3
	cp -p $< $@
build/rtx_mixer: rtx_mixer/rtx_mixer
	cp -p $< $@
build/rtx_mixer_test: rtx_mixer/rtx_mixer_test gtests
	cp -p $< $@
build/rtx_megaco_test: rtx_megaco_test/rtx_megaco_test gtests
	cp -p $< $@
build/rtx_media_engine_test: rtx_media_engine_test/rtx_media_engine_test gtests
	cp -p $< $@
build/rtx_media_tools_test: rtx_media_tools_test/rtx_media_tools_test gtests
	cp -p $< $@

ifeq ($(UNAME_S),Linux)
build/libgtest.so: | build
	cp /usr/src/gmock/gtest/libgtest.so $@
build/libgmock.so: | build
	cp /usr/src/gmock/libgmock.so $@
gtests: build/libgtest.so build/libgmock.so
endif
ifeq ($(UNAME_S),FreeBSD)
gtests:
endif

build/librtx_codec_audio_g711.so: rtx_711_module/librtx_codec_audio_g711.so
	cp -p $< $@
build/librtx_codec_audio_g729.so: rtx_729_module/librtx_codec_audio_g729.so
	cp -p $< $@
build/librtx_codec_audio_g722.so: rtx_G722_module/librtx_codec_audio_g722.so
	cp -p $< $@
build/librtx_codec_audio_g726.so: rtx_G726_module/librtx_codec_audio_g726.so
	cp -p $< $@
build/librtx_codec_video_h263.so: rtx_codec_video_h263/librtx_codec_video_h263.so
	cp -p $< $@
build/librtx_codec_video_h264.so: rtx_codec_video_h264/librtx_codec_video_h264.so
	cp -p $< $@
build/librtx_codec_video_vpx.so: rtx_codec_video_vpx/librtx_codec_video_vpx.so
	cp -p $< $@
build/librtx_codec.so: rtx_codec/librtx_codec.so
	cp -p $< $@
build/librtx_crypto.so: rtx_crypto/librtx_crypto.so
	cp -p $< $@
build/librtx_codec_audio_gsm.so: rtx_gsm_module/librtx_codec_audio_gsm.so
	cp -p $< $@
build/librtx_media_engine.so: rtx_media_engine/librtx_media_engine.so
	cp -p $< $@
build/librtx_media_tools.so: rtx_media_tools/librtx_media_tools.so
	cp -p $< $@
build/librtx_megaco.so: rtx_megaco/librtx_megaco.so
	cp -p $< $@
build/librtx_netlib.so: rtx_netlib/librtx_netlib.so
	cp -p $< $@
build/librtx_codec_audio_opus.so: rtx_opus_module/librtx_codec_audio_opus.so
	cp -p $< $@
build/librtx_rtplib.so: rtx_rtplib/librtx_rtplib.so
	cp -p $< $@
build/librtx_codec_audio_speex.so: rtx_speex_module/librtx_codec_audio_speex.so
	cp -p $< $@
build/librtx_stdlib.so: rtx_stdlib/librtx_stdlib.so
	cp -p $< $@
build/librtx_sip.so: rtx_sip/librtx_sip.so
	cp -p $< $@
build/librtx_aux.so: rtx_aux/librtx_aux.so
	cp -p $< $@
build/librtx_client.so: rtx_client/librtx_client.so
	cp -p $< $@

# executables in subdirs

rtx_mg3/rtx_mg3: $(LIBS)
	cd rtx_mg3 && $(MAKE)
rtx_mixer/rtx_mixer: rtx_stdlib/librtx_stdlib.so rtx_codec/librtx_codec.so rtx_media_tools/librtx_media_tools.so
	cd rtx_mixer && $(MAKE) rtx_mixer
rtx_mixer/rtx_mixer_test: rtx_stdlib/librtx_stdlib.so rtx_codec/librtx_codec.so rtx_media_tools/librtx_media_tools.so
	cd rtx_mixer && $(MAKE) rtx_mixer_test

rtx_megaco_test/rtx_megaco_test: rtx_netlib/librtx_netlib.so
	cd rtx_megaco_test && $(MAKE)
rtx_media_engine_test/rtx_media_engine_test: rtx_media_tools/librtx_media_tools.so rtx_megaco/librtx_megaco.so
	cd rtx_media_engine_test && $(MAKE)
rtx_media_tools_test/rtx_media_tools_test: rtx_media_tools/librtx_media_tools.so
	cd rtx_media_tools_test && $(MAKE)

# libraries in subdirs

rtx_stdlib/librtx_stdlib.so:
	cd rtx_stdlib && $(MAKE)
rtx_netlib/librtx_netlib.so: rtx_stdlib/librtx_stdlib.so
	cd rtx_netlib && $(MAKE)
rtx_crypto/librtx_crypto.so: rtx_netlib/librtx_netlib.so
	cd rtx_crypto && $(MAKE)
rtx_rtplib/librtx_rtplib.so: rtx_crypto/librtx_crypto.so
	cd rtx_rtplib && $(MAKE)
rtx_codec/librtx_codec.so: rtx_rtplib/librtx_rtplib.so
	cd rtx_codec && $(MAKE)
rtx_media_tools/librtx_media_tools.so: rtx_codec/librtx_codec.so
	cd rtx_media_tools && $(MAKE)
rtx_sip/librtx_sip.so: rtx_netlib/librtx_netlib.so
	cd rtx_sip && $(MAKE)
rtx_aux/librtx_aux.so: rtx_stdlib/librtx_stdlib.so
	cd rtx_aux && $(MAKE)
rtx_client/librtx_client.so: rtx_sip/librtx_sip.so rtx_aux/librtx_aux.so
	cd rtx_client && $(MAKE)
rtx_711_module/librtx_codec_audio_g711.so: rtx_media_tools/librtx_media_tools.so
	cd rtx_711_module && $(MAKE)
rtx_729_module/librtx_codec_audio_g729.so: rtx_media_tools/librtx_media_tools.so
	cd rtx_729_module && $(MAKE)
rtx_G722_module/librtx_codec_audio_g722.so: rtx_media_tools/librtx_media_tools.so
	cd rtx_G722_module && $(MAKE)
rtx_G726_module/librtx_codec_audio_g726.so: rtx_media_tools/librtx_media_tools.so
	cd rtx_G726_module && $(MAKE)
rtx_codec_video_h263/librtx_codec_video_h263.so: rtx_media_tools/librtx_media_tools.so
	cd rtx_codec_video_h263 && $(MAKE)
rtx_codec_video_h264/librtx_codec_video_h264.so: rtx_media_tools/librtx_media_tools.so
	cd rtx_codec_video_h264 && $(MAKE)
rtx_codec_video_vpx/librtx_codec_video_vpx.so: rtx_media_tools/librtx_media_tools.so
	cd rtx_codec_video_vpx && $(MAKE)
rtx_gsm_module/librtx_codec_audio_gsm.so: rtx_media_tools/librtx_media_tools.so
	cd rtx_gsm_module && $(MAKE)
rtx_opus_module/librtx_codec_audio_opus.so: rtx_media_tools/librtx_media_tools.so
	cd rtx_opus_module && $(MAKE)
rtx_speex_module/librtx_codec_audio_speex.so: rtx_media_tools/librtx_media_tools.so
	cd rtx_speex_module && $(MAKE)
rtx_megaco/librtx_megaco.so: rtx_netlib/librtx_netlib.so
	cd rtx_megaco && $(MAKE)
rtx_media_engine/librtx_media_engine.so: rtx_media_tools/librtx_media_tools.so rtx_megaco/librtx_megaco.so
	cd rtx_media_engine && $(MAKE)

clean:
	for dir in $(DIRS); do $(MAKE) -C $$dir clean; done
	rm -f build/librtx*.so
	rm -f build/libgmock.so build/libgtest.so
	rm -f build/rtx_*

distclean: clean
	rm -rf _build build _test
