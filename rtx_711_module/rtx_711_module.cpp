﻿/* RTX H.248 Media Gate. G.711 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "rtx_711_module.h"
#include "std_logger.h"

#include "g711a_codec.h"
#include "g711u_codec.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#if defined (DEBUG_MEMORY)
void* operator new (size_t size, char* file, int line)
{
	return std_mem_checker_malloc(size, file, line, rc_malloc_new_e);
}
void* operator new[](size_t size, char* file, int line)
{
	return std_mem_checker_malloc(size, file, line, rc_malloc_newar_e);
}
void operator delete (void* mem_ptr)
{
	std_mem_checker_free(mem_ptr, rc_malloc_delete_e);
}
void operator delete[](void* mem_ptr)
{
	std_mem_checker_free(mem_ptr, rc_malloc_deletear_e);
}
#endif
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_711_MODULE_API const char* get_codec_list()
{
	return "PCMA PCMU";
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_711_MODULE_API bool get_available_audio_payloads(media::PayloadSet& set)
{
	set.addStandardFormat(media::PayloadId::PCMU);
	set.addStandardFormat(media::PayloadId::PCMA);

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_711_MODULE_API bool initlib()
{
	g_rtx_pcma_decoder_counter = rtl::res_counter_t::add("mge_pcma_decoder");
	g_rtx_pcmu_decoder_counter = rtl::res_counter_t::add("mge_pcmu_decoder");
	g_rtx_pcma_encoder_counter = rtl::res_counter_t::add("mge_pcma_encoder");
	g_rtx_pcmu_encoder_counter = rtl::res_counter_t::add("mge_pcmu_encoder");

	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_711_MODULE_API void freelib()
{
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
RTX_711_MODULE_API mg_audio_decoder_t* create_audio_decoder(const media::PayloadFormat& format)
{
	rtl::String codec_name(format.getEncoding());
	if (codec_name.isEmpty())
	{
		return nullptr;
	}

	codec_name.trim();
	codec_name.toLower();
	
	if (rtl::String::compare("pcma", codec_name, false) == 0)
	{
		rtx_g711a_codec_decoder* decoder = NEW rtx_g711a_codec_decoder(&Err);
		if (decoder->initialize(format))
		{
			return decoder;
		}

		DELETEO(decoder);
	}
	else if (rtl::String::compare("pcmu", codec_name, false) == 0)
	{
		rtx_g711u_codec_decoder* decoder = NEW rtx_g711u_codec_decoder(&Err);
		if (decoder->initialize(format))
		{
			return decoder;
		}
		DELETEO(decoder);
	}

	return nullptr;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void release_audio_decoder(mg_audio_decoder_t* decoder)
{
	if (decoder == nullptr)
	{
		return;
	}

	rtl::String type(decoder->getEncoding());
	type.toLower();

	if (rtl::String::compare("pcma", type, false) == 0)
	{
		rtx_g711a_codec_decoder* g711a_decoder = (rtx_g711a_codec_decoder*)decoder;
		if (g711a_decoder != nullptr)
		{
			DELETEO(g711a_decoder);
			g711a_decoder = nullptr;
		}
	}
	else if (rtl::String::compare("pcmu", type, false) == 0)
	{
		rtx_g711u_codec_decoder* g711u_decoder = (rtx_g711u_codec_decoder*)decoder;
		if (g711u_decoder != nullptr)
		{
			DELETEO(g711u_decoder);
			g711u_decoder = nullptr;
		}
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
mg_audio_encoder_t* create_audio_encoder(const media::PayloadFormat& format)
{
	/*if (param == nullptr)
	{
		return nullptr;
	}*/

	rtl::String codec_name(format.getEncoding());
	if (codec_name.isEmpty())
	{
		return nullptr;
	}
	codec_name.trim();
	codec_name.toLower();

	if (rtl::String::compare("PCMA", codec_name, true) == 0)
	{
		rtx_g711a_codec_encoder* encoder = NEW rtx_g711a_codec_encoder(&Err);
		if (encoder->initialize(format))
		{
			return encoder;
		}

		DELETEO(encoder);
	}
	else if (rtl::String::compare("PCMU", codec_name, true) == 0)
	{
		rtx_g711u_codec_encoder* encoder = NEW rtx_g711u_codec_encoder(&Err);
		if (encoder->initialize(format))
		{
			return encoder;
		}

		DELETEO(encoder);
	}

	return nullptr;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void release_audio_encoder(mg_audio_encoder_t* encoder)
{
	if (encoder == nullptr)
	{
		return;
	}

	rtl::String type(encoder->getEncoding());
	type.toLower();

	if (rtl::String::compare("PCMA", type, true) == 0)
	{
		rtx_g711a_codec_encoder* g711a_encoder = (rtx_g711a_codec_encoder*)encoder;
		if (g711a_encoder != nullptr)
		{
			DELETEO(g711a_encoder);
			g711a_encoder = nullptr;
		}
	}
	else if (rtl::String::compare("PCMU", type, true) == 0)
	{
		rtx_g711u_codec_encoder* g711u_encoder = (rtx_g711u_codec_encoder*)encoder;
		if (g711u_encoder != nullptr)
		{
			DELETEO(g711u_encoder);
			g711u_encoder = nullptr;
		}
	}
}
//------------------------------------------------------------------------------
