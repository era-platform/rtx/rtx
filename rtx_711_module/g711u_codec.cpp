﻿/* RTX H.248 Media Gate. G.711 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "g711u_codec.h"
#include "rtp_packet.h"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define BIAS_VALUE 33
//------------------------------------------------------------------------------
#define LOG_PREFIX "PCMU"
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
extern int g_rtx_pcmu_decoder_counter = -1;
extern int g_rtx_pcmu_encoder_counter = -1;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtx_g711u_codec_decoder::rtx_g711u_codec_decoder(rtl::Logger* log) :
	m_log(log)
{
	rtl::res_counter_t::add_ref(g_rtx_pcmu_decoder_counter);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtx_g711u_codec_decoder::~rtx_g711u_codec_decoder()
{
	destroy();

	rtl::res_counter_t::release(g_rtx_pcmu_decoder_counter);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtx_g711u_codec_decoder::initialize(const media::PayloadFormat& format)
{
	m_frame_size = 20 * 8000 / 1000;
	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t rtx_g711u_codec_decoder::get_frame_size_pcm()
{
	return m_frame_size * sizeof(uint16_t);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t rtx_g711u_codec_decoder::get_frame_size_cod()
{
	return m_frame_size * sizeof(uint8_t);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtx_g711u_codec_decoder::destroy()
{

}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const char* rtx_g711u_codec_decoder::getEncoding()
{
	return "pcmu";
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t rtx_g711u_codec_decoder::decode(const uint8_t* from, uint32_t from_length, uint8_t* to, uint32_t to_length)
{
	uint32_t size = 0;

	for (uint32_t i = 0; i < from_length; i ++)
	{
		if (size >= to_length)
		{
			PLOG_MEDIA_ERROR(LOG_PREFIX, "g711u_codec.decode -- not enough space for decode");
			break;
		}

		uint8_t in = *(from + i);

		uint16_t out = decode_element(in);

		uint16_t* ch = (uint16_t*)to;

		memcpy(ch + i, &out, sizeof(out));

		size += sizeof(out);
	}

	return size;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint16_t			rtx_g711u_codec_decoder::decode_element(uint8_t in)
{
	// Complement to obtain normal u-law value.
	uint8_t u_val = ~in;

	//Extract and bias the quantization bits. Then
	//shift up by the segment number and subtract out the bias.

	uint16_t temp = ((u_val & (0xf)) << 3) + (BIAS_VALUE << 2);//(0x84);
	temp <<= (u_val & (0x70)) >> (4);

	return ((u_val & (0x80)) ? ((BIAS_VALUE << 2) - temp) : (temp - (BIAS_VALUE << 2)));
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t rtx_g711u_codec_decoder::depacketize(const rtp_packet* packet, uint8_t* buff, uint32_t buff_size)
{
	if (packet == nullptr)
	{
		return 0;
	}

	if (buff == nullptr)
	{
		return 0;
	}

	if (buff_size < packet->get_payload_length())
	{
		return 0;
	}

	uint32_t payload_length = packet->get_payload_length();

	memcpy(buff, packet->get_payload(), payload_length);

	return payload_length;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtx_g711u_codec_encoder::rtx_g711u_codec_encoder(rtl::Logger* log) :
	m_log(log)
{
	rtl::res_counter_t::add_ref(g_rtx_pcmu_encoder_counter);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
rtx_g711u_codec_encoder::~rtx_g711u_codec_encoder()
{
	destroy();

	rtl::res_counter_t::release(g_rtx_pcmu_encoder_counter);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool rtx_g711u_codec_encoder::initialize(const media::PayloadFormat& format)
{
	m_frame_size = 20 * 8000 / 1000;
	return true;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t rtx_g711u_codec_encoder::get_frame_size()
{
	return m_frame_size * sizeof(uint16_t);
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtx_g711u_codec_encoder::destroy()
{

}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const char* rtx_g711u_codec_encoder::getEncoding()
{
	return "pcmu";
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t			rtx_g711u_codec_encoder::encode(const uint8_t* from, uint32_t from_length, uint8_t* to, uint32_t to_length)
{
	uint32_t size = 0;
	short in;

	for (uint32_t i = 0; i < from_length; i += sizeof(in))
	{
		if (size >= to_length)
		{
			PLOG_MEDIA_ERROR(LOG_PREFIX, "g711u_codec.encode -- not enough space for encode");
			break;
		}

		short* ch = (short*)(from + i);
		in = *ch;

		if (i + sizeof(in) > from_length)
		{
			break;
		}

		uint8_t out = coding_element(in);

		memcpy(to + size, &out, sizeof(out));

		size += sizeof(out);
	}

	return size;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint8_t				rtx_g711u_codec_encoder::coding_element(short in)
{
	uint16_t mask;
	uint16_t seg = 8;
	uint16_t uval;

	// Get the sign and the magnitude of the value. 
	short pcm_val = in >> 2;
	if (pcm_val < 0)
	{
		pcm_val = -pcm_val;
		mask = 0x7F;
	}
	else
	{
		mask = 0xFF;
	}

	if (pcm_val > 8159) //clip the magnitude
	{
		pcm_val = 8159;
	}

	pcm_val += BIAS_VALUE;

	if (pcm_val <= 0x1FF)
	{
		if (pcm_val <= 0x7F)
		{
			seg = pcm_val <= 0x3F ? 0 : 1;
		}
		else
		{
			seg = pcm_val <= 0xFF ? 2 : 3;
		}
	}
	else if (pcm_val <= 0x1FFF)
	{
		if (pcm_val <= 0x7FF)
		{
			seg = pcm_val <= 0x3FF ? 4 : 5;
		}
		else
		{
			seg = pcm_val <= 0xFFF ? 6 : 7;
		}
	}

	//Combine the sign, segment, quantization bits;
	//and complement the code word.

	if (seg >= 8)		// out of range, return maximum value.
	{
		return uint8_t(0x7F ^ mask);
	}
	else
	{
		uval = (seg << 4) | ((pcm_val >> (seg + 1)) & 0xF);
		return uint8_t(uval ^ mask);
	}
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
uint32_t rtx_g711u_codec_encoder::packetize(rtp_packet* packet, const uint8_t* buff, uint32_t buff_size)
{
	if (packet == nullptr)
	{
		return 0;
	}

	if (buff == nullptr)
	{
		return 0;
	}

	if (buff_size > rtp_packet::PAYLOAD_BUFFER_SIZE)
	{
		return 0;
	}

	if (packet->set_payload(buff, buff_size) == 0)
	{
		return 0;
	}

	packet->set_payload_type(0);

	packet->set_samples(buff_size);

	return buff_size;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
