/* RTX H.248 Media Gate. Audio Driver Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "audio_device_out.h"
#include "audio_mixer.h"

#if defined(TARGET_OS_WINDOWS)
//--------------------------------------
//
//--------------------------------------
aux_device_out_wave_t::aux_device_out_wave_t(aux_device_out_handler_t *handler) : aux_device_out_t("aux-out", handler),
																				  m_device_index(0), m_device_out(nullptr), m_headers(nullptr), m_buffer(nullptr)
{
}
//--------------------------------------
//
//--------------------------------------
aux_device_out_wave_t::~aux_device_out_wave_t()
{
	aux_close_device();
}
//--------------------------------------
//
//--------------------------------------
bool aux_device_out_wave_t::aux_open_device(const char *deviceName)
{
	MMRESULT res;

	rtl::MutexLock lock(m_sync);

	if (m_opened_flag)
		aux_close_device();

	LOG_CALL(m_tag, "open device : name:%s clock rate %d, channels %d, block times %d ms, block count %d, block size %d bytes...",
			 deviceName, m_clock_rate, m_channels, m_block_times, m_block_count, m_block_size);

	m_headers = (WAVEHDR *)MALLOC(sizeof(WAVEHDR) * m_block_count);
	m_buffer = (uint8_t *)MALLOC(m_block_size * (m_block_count));

	memset(m_headers, 0, sizeof(WAVEHDR) * (m_block_count));
	memset(m_buffer, 0, m_block_size * (m_block_count));

	if (!aux_start_thread())
	{
		LOG_WARN(m_tag, "open device : start thread failed");
		return false;
	}

	m_device_name = deviceName;

	if (m_device_name.isEmpty())
	{

		m_device_index = 0;
	}
	else
	{
		m_device_index = audio_mixer_t::device_out_from_name(m_device_name);
	}

	WAVEFORMATEX pcm_format;

	pcm_format.wFormatTag = WAVE_FORMAT_PCM;
	pcm_format.nChannels = m_channels;
	pcm_format.nSamplesPerSec = m_clock_rate;
	pcm_format.nAvgBytesPerSec = m_clock_rate * sizeof(short) * m_channels;
	pcm_format.nBlockAlign = (WORD)(sizeof(short) * m_channels);
	pcm_format.wBitsPerSample = sizeof(short) * 8;
	pcm_format.cbSize = 0;

	if ((res = waveOutOpen(&m_device_out, m_device_index, &pcm_format, (DWORD_PTR)m_thread.get_id(), 0, CALLBACK_THREAD)) != MMSYSERR_NOERROR)
	{
		LOG_WARN(m_tag, "open device : waveOutOpen() failed with code %d", res);
		aux_close_device();

		return false;
	}

	for (int i = 0; i < m_block_count; i++)
	{
		m_headers[i].dwBufferLength = m_block_size;
		m_headers[i].lpData = (LPSTR)(m_buffer + (m_block_size * i));

		LOG_CALL(m_tag, "open device : preparing wave out header #%d, %p:%p length:%d bytes", i, &m_headers[i], m_headers[i].lpData, m_headers[i].dwBufferLength);

		if ((res = waveOutPrepareHeader(m_device_out, m_headers + i, sizeof(WAVEHDR))) != MMSYSERR_NOERROR)
		{
			LOG_ERROR(m_tag, "open device : waveOutPrepareHeader(%d) failed with code %d", i, res);
			aux_close_device();

			return false;
		}
	}

	LOG_CALL(m_tag, "open device: success");

	return m_opened_flag = true;
}
//--------------------------------------
//
//--------------------------------------
void aux_device_out_wave_t::aux_close_device()
{
	rtl::MutexLock lock(m_sync);

	// �������� ���� �������� ����������
	m_opened_flag = false;

	aux_stop_thread();

	// ��������� ���������������
	aux_stop_device();

	// ������� ����������
	if (m_device_out != nullptr)
	{
		LOG_CALL(m_tag, "close device : closing...");

		// ����������� ��������� ���������������
		for (int i = 0; i < m_block_count; i++)
		{
			waveOutUnprepareHeader(m_device_out, m_headers + i, sizeof(WAVEHDR));
		}

		// ��������� ����� �����
		waveOutClose(m_device_out);
		m_device_out = nullptr;

		LOG_CALL(m_tag, "close device : closed");
	}

	// ������� ����������
	if (m_headers != nullptr)
	{
		FREE(m_headers);
		m_headers = nullptr;
	}

	if (m_buffer != nullptr)
	{
		FREE(m_buffer);
		m_buffer = nullptr;
	}
}
//--------------------------------------
// ����� ���������� -- ��������� ������ � ��������� ������ �� ���������������
//--------------------------------------
bool aux_device_out_wave_t::aux_start_device()
{
	rtl::MutexLock lock(m_sync);

	if (!m_opened_flag || m_started_flag)
		return false;

	// ��������� ������ � ��������� �� ���������������
	short *buffer;

	LOG_CALL(m_tag, "start device : starting with %d headers...", m_block_count);

	for (int i = 0; i < m_block_count; i++)
	{
		buffer = (short *)m_headers[i].lpData;
		m_headers[i].dwBufferLength = m_handler->aux_out_get_data(buffer, m_block_size / 2) * 2;

		LOG_CALL(m_tag, "start device : playing initial media block by header #%d", i);

		waveOutWrite(m_device_out, m_headers + i, sizeof WAVEHDR);
	}

	LOG_CALL(m_tag, "start device: started");

	return m_started_flag = true;
}
//--------------------------------------
//
//--------------------------------------
void aux_device_out_wave_t::aux_stop_device()
{
	rtl::MutexLock lock(m_sync);

	if (m_started_flag && m_device_out != nullptr)
	{
		LOG_CALL(m_tag, "stop device : stopping...");

		// ������� ����������
		waveOutReset(m_device_out);
		m_started_flag = false;

		LOG_CALL(m_tag, "stop device : stopped");
	}
}
//--------------------------------------
//
//--------------------------------------
void aux_device_out_wave_t::aux_set_paused(bool pause)
{
	rtl::MutexLock lock(m_sync);

	if (m_started_flag)
	{
		LOG_CALL(m_tag, "pause device : %s", pause ? "set pause" : " continue playing");

		if (pause)
		{
			waveOutPause(m_device_out);
		}
		else
		{
			waveOutRestart(m_device_out);
		}
	}
}
void aux_device_out_wave_t::post_stop_thread()
{
	PostThreadMessage(m_thread.get_id(), WM_CMD_SHUTDOWN, 0, 0);
}
//--------------------------------------
//
//--------------------------------------
void aux_device_out_wave_t::thread_run(rtl::Thread *thread)
{
	uint32_t res;
	WAVEHDR *header;

	LOG_CALL(m_tag, "thread : enter");

	m_thread_stop_flag = false;

	MSG msg;
	PeekMessageA(&msg, nullptr, 0, 0, PM_NOREMOVE);

	if (m_thread_started_event != nullptr)
		SetEvent(m_thread_started_event);

	while (GetMessage(&msg, nullptr, 0, 0) && !m_thread_stop_flag)
	{
		try
		{
			switch (msg.message)
			{
			case WM_CMD_SHUTDOWN:
				LOG_CALL(m_tag, "thread : exit : by WM_SHUTDOWN");
				return;

			case MM_WOM_DONE:
				// �������� ��������� �� ����
				if ((header = (WAVEHDR *)msg.lParam) == nullptr)
				{
					LOG_ERROR(m_tag, "thread : playing receive nullptr. continue playing");
					break;
				}

				// ���������� ����� ��� ����������
				header->dwBufferLength = m_handler->aux_out_get_data((short *)header->lpData, m_block_size / 2) * 2;

				if ((int)header->dwBufferLength < m_block_size)
				{
					memset(header->lpData + header->dwBufferLength, 0, m_block_size - header->dwBufferLength);
					header->dwBufferLength = m_block_size;
				}

				// �������� ���� �� ������������
				if ((res = waveOutWrite(m_device_out, header, sizeof(WAVEHDR))) != MMSYSERR_NOERROR)
				{
					LOG_ERROR(m_tag, "thread : waveOutWrite() failed with code %d. continue playing", res);
				}

				break;
			}
		}
		catch (rtl::Exception &ex)
		{
			LOG_ERROR(m_tag, "thread : play catches exception :\n%s", ex.get_message());
			ex.raise_notify(__FILE__, __FUNCSIG__);
		}
	}

	LOG_CALL(m_tag, "thread : exit : by %s.", m_thread_stop_flag ? "STOP_FLAG" : "WM_QUIT");
}
#endif // TARGET_OS_*
//--------------------------------------
