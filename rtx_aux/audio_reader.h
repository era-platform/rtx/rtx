﻿/* RTX H.248 Media Gate. G.726 Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_media_tools.h"
#include "mmt_file_wave.h"
#include "mmt_file_mp3.h"

#define WAVE_FRAME_SIZE		16000 // bytes

typedef void (AUX_CALL *PFN_FILEDONE)();
typedef void (AUX_CALL *PFN_FRAME_DONE)(double ms);

//-----------------------------------------------
//
//-----------------------------------------------
struct audio_data_reader_t
{
	virtual void reset() = 0;
	virtual uint32_t read_next_block(void* buffer) = 0;		// size = 16000 bytes
	virtual uint32_t read_first_block(void* buffer) = 0;	// size = 16000 bytes
	virtual void close() = 0;

	virtual uint32_t get_format_channels() = 0;
	virtual uint32_t get_format_rate() = 0;
};
//-----------------------------------------------
//
//-----------------------------------------------
class audio_data_reader_wave_file_t : public audio_data_reader_t
{
	media::FileReaderWAV m_file;

	bool prepare();
	void decode();

public:
	audio_data_reader_wave_file_t() { }
	virtual ~audio_data_reader_wave_file_t();

	bool open(const char* file_path);
	bool open(void* data, int size);
	
	virtual void close();

	virtual void reset();
	virtual uint32_t read_next_block(void* buffer);		// size = 16000 bytes
	virtual uint32_t read_first_block(void* buffer);	// size = 16000 bytes

	uint32_t virtual get_format_channels() { return m_file.getChannelCount(); }
	uint32_t virtual get_format_rate() { return m_file.getFrequency(); }
};

//-----------------------------------------------
//
//-----------------------------------------------
class audio_data_reader_wave_buffer_t : public audio_data_reader_t
{
	uint8_t* m_buffer;			// сам звук
	uint32_t m_buffer_size;		// длина звук
	uint32_t m_pointer;			// указатель на новые данные

public:
	audio_data_reader_wave_buffer_t();
	virtual ~audio_data_reader_wave_buffer_t();

	bool open(void* data, int size);
	virtual void close();

	virtual void reset();
	virtual uint32_t read_next_block(void* buffer);		// size = 16000 bytes
	virtual uint32_t read_first_block(void* buffer);	// size = 16000 bytes

	virtual uint32_t get_format_channels() { return 1; }
	virtual uint32_t get_format_rate() { return 8000; }
};
//-----------------------------------------------
//
//-----------------------------------------------
class audio_data_reader_mp3_file_t : public audio_data_reader_t
{
	media::FileReaderMP3 m_file;

public:
	audio_data_reader_mp3_file_t();
	virtual ~audio_data_reader_mp3_file_t();

	bool open(const char* file_path);
	virtual void close();

	bool is_opened();

	virtual void reset();
	virtual uint32_t read_next_block(void* buffer);		// size = 16000 bytes
	virtual uint32_t read_first_block(void* buffer);	// size = 16000 bytes

	virtual uint32_t get_format_channels();
	virtual uint32_t get_format_rate();
};
//-----------------------------------------------
