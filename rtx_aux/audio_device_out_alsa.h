﻿/* RTX H.248 Media Gate. Audio Driver Module
 *
 * Copyright @ 2020 George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "audio_device.h"
//--------------------------------------
// аудио выход
//--------------------------------------
#if defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
class aux_device_out_alsa_t : public aux_device_out_t
{
	// общие поля
	rtl::String m_device_index;	// индекс устройства
	snd_pcm_t * m_device_out;	// описатель устройства

public:
	aux_device_out_alsa_t(aux_device_out_handler_t* handler);
	virtual ~aux_device_out_alsa_t();

	virtual bool aux_open_device(const char* deviceName);
	virtual void aux_close_device();
	virtual bool aux_start_device();
	virtual void aux_stop_device();
	virtual void aux_set_paused(bool pause);

private:
	virtual void post_stop_thread();
	virtual void thread_run(rtl::Thread* thread);
};
#endif // TARGET_OS_*
//--------------------------------------
