﻿/* RTX H.248 Media Gate. Audio Driver Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include <math.h>
#include "audio_signal_player.h"
#include "audio_mixer.h"
//--------------------------------------
//
//--------------------------------------
#define DTMF_DURATION			2000					// ms
#define DTMF_SAMPLES_COUNT		DTMF_DURATION * 8		// samples
#define SIGNAL_DURATION			2000					// ms
#define SIGNAL_SAMPLES_COUNT	SIGNAL_DURATION * 8		// samples
#define LONG_DURATION			1000					// ms
#define LONG_SAMPLES_COUNT		LONG_DURATION * 8		// samples
#define LONG_FRAME_LENGTH		LONG_SAMPLES_COUNT * 5
#define BUSY_DURATION			400						// ms
#define BUSY_SAMPLES_COUNT		BUSY_DURATION * 8		// samples
#define BUSY_FRAME_LENGTH		BUSY_SAMPLES_COUNT * 10
#define TONE_VOLUME				15						// попугаев
//--------------------------------------
// таблица генерации тонов DTMF
//--------------------------------------
#define FREQ_ROW1				697
#define FREQ_ROW2				770
#define FREQ_ROW3				852
#define FREQ_ROW4				941

#define FREQ_COL1				1209
#define FREQ_COL2				1336
#define FREQ_COL3				1477
#define FREQ_COL4				1633

#define FREQ_TONE1				420
#define FREQ_TONE2				370
#define FREQ_TONE3				440

#define FREQ_CLICK				1206

static int Frequency[STD_DTMF_COUNT][2] =
{
	{ FREQ_COL1, FREQ_ROW1 },	// SND_DTMF_1
	{ FREQ_COL2, FREQ_ROW1 },	// SND_DTMF_2
	{ FREQ_COL3, FREQ_ROW1 },	// SND_DTMF_3
	{ FREQ_COL4, FREQ_ROW1 },	// SND_DTMF_A
	{ FREQ_COL1, FREQ_ROW2 },	// SND_DTMF_4
	{ FREQ_COL2, FREQ_ROW2 },	// SND_DTMF_5
	{ FREQ_COL3, FREQ_ROW2 },	// SND_DTMF_6
	{ FREQ_COL4, FREQ_ROW2 },	// SND_DTMF_B
	{ FREQ_COL1, FREQ_ROW3 },	// SND_DTMF_7
	{ FREQ_COL2, FREQ_ROW3 },	// SND_DTMF_8
	{ FREQ_COL3, FREQ_ROW3 },	// SND_DTMF_9
	{ FREQ_COL4, FREQ_ROW3 },	// SND_DTMF_C
	{ FREQ_COL1, FREQ_ROW4 },	// SND_DTMF_ASTERISK
	{ FREQ_COL2, FREQ_ROW4 },	// SND_DTMF_0
	{ FREQ_COL3, FREQ_ROW4 },	// SND_DTMF_MESH
	{ FREQ_COL4, FREQ_ROW4 },	// SND_DTMF_D
};

bool audio_signal_player_t::s_initialized = false;

signal_samples_t audio_signal_player_t::s_signals[STD_SIGNAL_COUNT] = { 0 };
//--------------------------------------
//
//--------------------------------------
audio_signal_player_t::audio_signal_player_t() : m_player(true, "dtmf", 16000)
{
	if (!s_initialized)
	{
		generate_std_signals(15);
	}
}
//--------------------------------------
//
//--------------------------------------
audio_signal_player_t::~audio_signal_player_t()
{
	stop_playing();
}
//--------------------------------------
//
//--------------------------------------
bool audio_signal_player_t::start_playing(const char* device_name, PhoneTones tone, int duration)
{
	// если уже генерируется сигнал то прервем его!
	stop_playing();

	// откроем устройство
	m_reader.close();

	int index = (int)tone;

	m_reader.open(s_signals[index].samples, s_signals[index].count * sizeof(short));
	
	m_player.start_play(device_name, &m_reader, true, nullptr, nullptr);

	return true;
}
//--------------------------------------
//
//--------------------------------------
void audio_signal_player_t::stop_playing()
{
	if (m_player.is_playing())
	{
		LOG_CALL("dtmf-p", "stopping play...");

		m_player.stop_play();
		m_reader.close();

		LOG_CALL("dtmf-p", "play stopped.");
	}
}
//--------------------------------------
//
//--------------------------------------
//static WAVFormat format_PCM = { WAVFormatTagPCM, 1, 8000, 16000, 2, 16,  0 };
//--------------------------------------
//
//--------------------------------------
bool audio_signal_player_t::generate_std_signals(uint16_t volume)
{
	if (s_initialized)
		return true;

	LOG_CALL("tone-gen", "generate tone samples : VOLUME value %u", volume);
	
	if (volume > 100)
		volume = 100;

	volume = (uint16_t)((float)volume * 327.67);
	
	LOG_CALL("tone_gen", "volume recalculated to %u", volume);

	//wchar_t s_file[MAX_PATH];
	//wave_file_recorder_t rec(Log);

	// генерация стандартных тонов DTMF
	// буферы должны быть достаточно большими для воспроизведения на стандартном плеере?

	for (int i = 0; i < STD_DTMF_COUNT; i++)
	{
		int index = i + PhoneTones_DTMF_0;

		s_signals[index].count = DTMF_SAMPLES_COUNT;
		s_signals[index].samples = (short*)MALLOC(DTMF_SAMPLES_COUNT * sizeof(short));
		
		memset(s_signals[index].samples, 0, DTMF_SAMPLES_COUNT * sizeof(short));
		// i -> zero based index in frequency table!
		audio_mixer_t::generate_std_signal(Frequency[i][0], Frequency[i][1], DTMF_DURATION, s_signals[index].samples, DTMF_SAMPLES_COUNT, volume);

		//_snwprintf(s_file, MAX_PATH, L"file_%S.wav", get_std_signal_name(MM_PhoneTones(index)));

		//if (rec.create(s_file, &format_PCM, sizeof WAVEFORMATEX))
		//{
		//	rec.write(s_signals[index].samples, s_signals[index].count * sizeof(short));

		//	rec.close();
		//}
	}

	s_signals[PhoneTones_Click].count = DTMF_SAMPLES_COUNT;
	s_signals[PhoneTones_Click].samples = (short*)MALLOC((DTMF_SAMPLES_COUNT)*sizeof(short));
	audio_mixer_t::generate_std_signal(FREQ_CLICK, 0, DTMF_DURATION, s_signals[PhoneTones_Click].samples, DTMF_SAMPLES_COUNT, volume);

	//if (rec.create(L"file_Click.wav", &format_PCM, sizeof WAVEFORMATEX))
	//{
	//	rec.write(s_signals[MM_PhoneTones_Click].samples, s_signals[MM_PhoneTones_Click].count * sizeof(short));

	//	rec.close();
	//}

	// генерация тона атс
	s_signals[PhoneTones_PBX].count = SIGNAL_SAMPLES_COUNT;
	s_signals[PhoneTones_PBX].samples = (short*)MALLOC(SIGNAL_SAMPLES_COUNT * sizeof(short));
	audio_mixer_t::generate_std_signal(FREQ_TONE2, FREQ_TONE3, SIGNAL_DURATION, s_signals[PhoneTones_PBX].samples, SIGNAL_SAMPLES_COUNT, volume);

	//if (rec.create(L"file_PBX.wav", &format_PCM, sizeof WAVEFORMATEX))
	//{
	//	rec.write(s_signals[MM_PhoneTones_PBX].samples, s_signals[MM_PhoneTones_PBX].count * sizeof(short));

	//	rec.close();
	//}

	// генерация тона занято
	s_signals[PhoneTones_Busy].count = BUSY_FRAME_LENGTH; // tone + silence
	s_signals[PhoneTones_Busy].samples = (short*)MALLOC(BUSY_FRAME_LENGTH * sizeof(short));
	audio_mixer_t::generate_std_signal(FREQ_TONE1, 0, BUSY_DURATION, s_signals[PhoneTones_Busy].samples, BUSY_SAMPLES_COUNT, volume);
	memset(s_signals[PhoneTones_Busy].samples + BUSY_SAMPLES_COUNT, 0, BUSY_SAMPLES_COUNT * sizeof(short));
	for (int i = 1; i <= 4; i++)
	{
		memcpy(s_signals[PhoneTones_Busy].samples + (BUSY_SAMPLES_COUNT * 2 * i),  s_signals[PhoneTones_Busy].samples, BUSY_SAMPLES_COUNT * 2 * sizeof(short));
	}

	//if (rec.create(L"file_Busy.wav", &format_PCM, sizeof WAVEFORMATEX))
	//{
	//	rec.write(s_signals[MM_PhoneTones_Busy].samples, s_signals[MM_PhoneTones_Busy].count * sizeof(short));

	//	rec.close();
	//}

	// генерация тона город
	s_signals[PhoneTones_DialTone].count = SIGNAL_SAMPLES_COUNT;
	s_signals[PhoneTones_DialTone].samples = (short*)MALLOC(SIGNAL_SAMPLES_COUNT * sizeof(short));
	audio_mixer_t::generate_std_signal(FREQ_TONE1, 0, SIGNAL_DURATION, s_signals[PhoneTones_DialTone].samples, SIGNAL_SAMPLES_COUNT, volume);

	//if (rec.create(L"file_DialTone.wav", &format_PCM, sizeof WAVEFORMATEX))
	//{
	//	rec.write(s_signals[MM_PhoneTones_DialTone].samples, s_signals[MM_PhoneTones_DialTone].count * sizeof(short));

	//	rec.close();
	//}

	// генерация тона длинный
	s_signals[PhoneTones_Long].count = LONG_FRAME_LENGTH; // 1 сек
	s_signals[PhoneTones_Long].samples = (short*)MALLOC(LONG_SAMPLES_COUNT * 5 * sizeof(short)); // + 4 сек
	audio_mixer_t::generate_std_signal(FREQ_TONE1, 0, LONG_DURATION, s_signals[PhoneTones_Long].samples, LONG_SAMPLES_COUNT, volume);
	memset(s_signals[PhoneTones_Long].samples + LONG_SAMPLES_COUNT, 0, LONG_SAMPLES_COUNT * 4 * sizeof(short));

	//if (rec.create(L"file_Long.wav", &format_PCM, sizeof WAVEFORMATEX))
	//{
	//	rec.write(s_signals[MM_PhoneTones_Long].samples, s_signals[MM_PhoneTones_Long].count * sizeof(short));

	//	rec.close();
	//}

	// генерация тишины
	s_signals[PhoneTones_Silence].count = SIGNAL_SAMPLES_COUNT; // 2 сек
	s_signals[PhoneTones_Silence].samples = (short*)MALLOC(SIGNAL_SAMPLES_COUNT * sizeof(short));
	memset(s_signals[PhoneTones_Silence].samples, 0, SIGNAL_SAMPLES_COUNT * sizeof(short));

	return (s_initialized = true);
}
//--------------------------------------
//
//--------------------------------------
void audio_signal_player_t::cleanup_std_tomes()
{
	if (s_initialized)
	{
		for (int i = 0; i < STD_SIGNAL_COUNT; i++)
		{
			if (s_signals[i].samples != nullptr)
			{
				FREE(s_signals[i].samples);
				s_signals[i].samples = nullptr;
				s_signals[i].count = 0;
			}
		}
	}
}
//--------------------------------------

