﻿/* RTX H.248 Media Gate. Audio Driver Module
 *
 * Copyright @ 2020 George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "audio_device_in_alsa.h"
#include "audio_mixer.h"

#if defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
//--------------------------------------
//
//--------------------------------------
aux_device_in_alsa_t::aux_device_in_alsa_t(aux_device_in_handler_t* handler) : aux_device_in_t("aux-in", handler),
	m_device_index("default"), m_device_in(nullptr)
{
}
//--------------------------------------
//
//--------------------------------------
aux_device_in_alsa_t::~aux_device_in_alsa_t()
{
	aux_close_device();
}
//--------------------------------------
//
//--------------------------------------
bool aux_device_in_alsa_t::aux_open_device(const char* deviceName)
{
	rtl::MutexLock lock(m_sync);

	if (m_opened_flag)
	{
		aux_close_device();
	}

	m_device_name = deviceName;
	m_device_name.trim();

	// string_t defname = "default";//audio_mixer_t::device_in_from_name(m_device_name);

	// if (m_device_name.is_empty() || m_device_name == "default")
	// {
	// 	m_device_index = "default";
	// }
	// else
	// {
	// 	m_device_index = defname;
	// }

	m_device_index = "default";

	LOG_CALL(m_tag, "open device : name:%s clock-rate %d, channels %d, block-times %d ms, block-count %d, block-size %d bytes",
		(const char*)m_device_index, m_clock_rate, m_channels, m_block_times, m_block_count, m_block_size);

	int err;
	snd_pcm_hw_params_t *hw_params;

	if ((err = snd_pcm_open(&m_device_in, m_device_index, SND_PCM_STREAM_CAPTURE, 0)) < 0)
	{
		LOG_ERROR(m_tag, "open device : snd_pcm_open() failed with error : '%s'", snd_strerror(err));
		aux_close_device();
		return false;
	}

    if ((err = snd_pcm_set_params(m_device_in, SND_PCM_FORMAT_S16_LE, SND_PCM_ACCESS_RW_INTERLEAVED,
		m_channels, m_clock_rate, 0, m_block_times)) < 0) //   * m_block_count * 1000
	{
		/* 0.16 sec */ // 160000 = 8 * 20 * 1000
		LOG_ERROR(m_tag, "open device : setup failed with error: %s", snd_strerror(err));
    	return false;
	}
	//-----
	// if ((err = snd_pcm_hw_params_malloc (&hw_params)) < 0)
	// {
	// 	LOG_ERROR(m_tag, "open device : cannot allocate hardware parameter structure (%s)", snd_strerror (err));
	// 	aux_close_device();
	// 	return false;
	// }
			 
	// if ((err = snd_pcm_hw_params_any (m_device_in, hw_params)) < 0)
	// {
	// 	LOG_ERROR(m_tag, "open device : cannot initialize hardware parameter structure (%s)", snd_strerror (err));
	// 	aux_close_device();
	// 	return false;
	// }
	
	// if ((err = snd_pcm_hw_params_set_access (m_device_in, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0)
	// {
	// 	LOG_ERROR(m_tag, "open device : cannot set access type (%s)", snd_strerror (err));
	// 	aux_close_device();
	// 	return false;
	// }

	// if ((err = snd_pcm_hw_params_set_format (m_device_in, hw_params, SND_PCM_FORMAT_S16_LE)) < 0)
	// {
	// 	LOG_ERROR(m_tag, "open device : cannot set sample format (%s)", snd_strerror (err));
	// 	aux_close_device();
	// 	return false;
	// }

	// unsigned freq = m_clock_rate;
	// if ((err = snd_pcm_hw_params_set_rate_near (m_device_in, hw_params, &freq, 0)) < 0)
	// {
	// 	LOG_ERROR(m_tag, "open device : cannot set sample rate (%s)",  snd_strerror (err));
	// 	aux_close_device();
	// 	return false;
	// }

	// if ((err = snd_pcm_hw_params_set_channels (m_device_in, hw_params, 1)) < 0)
	// {
	// 	LOG_ERROR(m_tag, "open device : cannot set channel count (%s)", snd_strerror (err));
	// 	aux_close_device();
	// 	return false;
	// }

	// if ((err = snd_pcm_hw_params (m_device_in, hw_params)) < 0)
	// {
	// 	LOG_ERROR(m_tag, "open device : cannot set parameters (%s)",  snd_strerror (err));
	// 	aux_close_device();
	// 	return false;
	// }

	// snd_pcm_hw_params_free (hw_params);

	// if ((err = snd_pcm_prepare (m_device_in)) < 0)
	//  {
	// 	LOG_ERROR(m_tag, "open device : cannot prepare audio interface for use (%s)", snd_strerror(err));
	// 	aux_close_device();
	// 	return false;
	// }
	//-----
	if (!aux_start_thread())
	{
		LOG_WARN(m_tag, "open device : start thread failed");
		aux_close_device();
		return false;
	}

	LOG_CALL(m_tag, "open device : success");

	return m_opened_flag = true;
}
//--------------------------------------
//
//--------------------------------------
void aux_device_in_alsa_t::aux_close_device()
{
	rtl::MutexLock lock(m_sync);

	// остановим воспроизведение
	aux_stop_device();

	aux_stop_thread();

	// закроем устройство
	if (m_device_in != nullptr)
	{
		LOG_CALL(m_tag, "close device : closing...");

		// закрываем аудио выход
		snd_pcm_close(m_device_in);
		m_device_in = nullptr;

		LOG_CALL(m_tag, "close device : closed");
	}
}
//--------------------------------------
//
//--------------------------------------
bool aux_device_in_alsa_t::aux_start_device()
{
	rtl::MutexLock lock(m_sync);

	if (m_opened_flag && !m_started_flag)
	{
		LOG_CALL(m_tag, "start device : starting...");

		m_paused_flag = false;
		m_started_flag = true;

		LOG_CALL(m_tag, "start device : started");
	}

	return m_started_flag;
}
//--------------------------------------
//
//--------------------------------------
void aux_device_in_alsa_t::aux_stop_device()
{
	rtl::MutexLock lock(m_sync);

	if (m_started_flag && m_device_in)
	{
		LOG_CALL(m_tag, "stop device : stopping...");

		m_started_flag = false;

		// сбросим устройство
		snd_pcm_drop(m_device_in);

		m_paused_flag = false;
	}
}
//--------------------------------------
//
//--------------------------------------
void aux_device_in_alsa_t::aux_set_paused(bool pause)
{
	rtl::MutexLock lock(m_sync);

	if (m_started_flag)
	{
		LOG_CALL(m_tag, "pause device : %s (%d/%d)", pause ? "set pause" : " continue recording", m_paused_flag, pause);
		snd_pcm_pause(m_device_in, m_paused_flag = pause);
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
void aux_device_in_alsa_t::post_stop_thread()
{
	m_thread_stop_flag = true;
}
//-----------------------------------------------
//
//-----------------------------------------------
void aux_device_in_alsa_t::thread_run(rtl::Thread* thread)
{
	LOG_CALL(m_tag, "thread : enter");

	m_thread_stop_flag = false;
	
	LOG_CALL(m_tag, "thread : before signaling");

	m_thread_started_event.signal();

	LOG_CALL(m_tag, "thread : start signaled");

	m_out.createNew("/home/george/develop/rtxsip/record_in.raw");

	while (!m_thread_stop_flag)
	{
		try
		{
			int res = snd_pcm_wait(m_device_in, 20);

			int ready = 0;
			short buffer[1024];

			if ((ready = snd_pcm_readi(m_device_in, buffer, 160)) < 0)
			{
				LOG_ERROR(m_tag, "read from audio interface failed %d (%s)", ready, snd_strerror(ready));
				break;
			}
			// отправим буфер получателю
			LOG_CALL(m_tag, "thread : pcm capture read %d samples wait:%d", ready, res);
			size_t wr = m_out.write(buffer, ready*sizeof(short));
			LOG_CALL(m_tag, "thread : pcm capture written %lld bytes", wr);
			if (m_handler != nullptr && !m_paused_flag && ready > 0)
			{
				m_handler->aux_in_data_ready(buffer, ready);
			}
		}
		catch (rtl::Exception& ex)
		{
			LOG_ERROR(m_tag, "thread : recording catches exception : %s", ex.get_message());
			ex.raise_notify(__FILE__, __FUNCTION__);
		}
	}

	m_out.close();
	LOG_CALL(m_tag, "thread : exit");
}
#endif // TARGET_OS_*
//-----------------------------------------------
