﻿/* RTX H.248 Media Gate. Audio Driver Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

//--------------------------------------
//
//--------------------------------------
class audio_mixer_t
{
public:
	static void set_output_volume(const char* device_name, uint16_t volume);
	static uint16_t get_output_volume(const char* device_name);
	static void set_input_volume(const char* device_name, uint16_t volume);
	static uint16_t get_input_volume(const char* device_name);
#if defined (TARGET_OS_WINDOWS)
	static int32_t device_in_from_name(const char* device_name);
	static int32_t device_out_from_name(const char* device_name);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	static rtl::String device_in_from_name(const char* device_name);
	static rtl::String device_out_from_name(const char* device_name);
#endif
	static bool generate_std_signal(int freq1, int freq2, int duration, short* buffer, int size, uint16_t volume);
	static bool generate_std_signal2(int freq1, int freq2, int duration, short* buffer, int size, uint16_t volume);
};
//--------------------------------------
