﻿/* RTX H.248 Media Gate. Audio Driver Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
//--------------------------------------
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the AUXLIB_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// AUXLIB_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
//--------------------------------------
#include "std_basedef.h"
#include "std_typedef.h"
#include "std_funcdef.h"

#if defined (TARGET_OS_WINDOWS)
	#ifdef AUXLIB_EXPORTS
	#define AUX_API __declspec(dllexport)
	#else
	#define AUX_API __declspec(dllimport)
	#endif
	#define AUX_CALL __stdcall
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	#define AUX_API
	#define AUX_CALL
#endif
//--------------------------------------
// константы генератора тонов
//--------------------------------------
enum PhoneTones
{
	PhoneTones_Busy,			// стандартный тон с перерывами (400 Х 400 мс)
	PhoneTones_Long,			// стандартный тон с перерывами (1000 Х 4000 мс)
	PhoneTones_DialTone,		// стандартный тон без пустот
	PhoneTones_PBX,			// не стандартный тон без переывов
	PhoneTones_Click,		// нестандартный тон
	PhoneTones_Silence,		// тишина (именно выводится!)
	//------------------
	PhoneTones_DTMF_0,
	PhoneTones_DTMF_1,
	PhoneTones_DTMF_2,
	PhoneTones_DTMF_3,
	PhoneTones_DTMF_4,
	PhoneTones_DTMF_5,
	PhoneTones_DTMF_6,
	PhoneTones_DTMF_7,
	PhoneTones_DTMF_8,
	PhoneTones_DTMF_9,
	PhoneTones_DTMF_A,
	PhoneTones_DTMF_B,
	PhoneTones_DTMF_C,
	PhoneTones_DTMF_D,
	PhoneTones_DTMF_Asterisk,
	PhoneTones_DTMF_NumberSign,
};
//--------------------------------------
// прототипы функций обратного вызова
//--------------------------------------
/// <summary>
/// событие останова воспроизведения по окончании данных
/// при принудительном останове не генерируется
/// </summary>
typedef void (AUX_CALL *MM_PlayStoppedHandler)();
//--------------------------------------
// набор функций обратного вызова для передачи событий из библиотеки
//--------------------------------------
struct MM_CallbackInterface
{
	MM_PlayStoppedHandler PlayStopped;
};
//--------------------------------------
// Генерация событий для пользователя
//--------------------------------------
//void raise_play_stopped();
//--------------------------------------
// интерфейс к аудио и телефонных USB устройств
//--------------------------------------
extern "C"
{
/// <summary>
/// инициализация auxlib.dll
/// </summary>
/// <param name="callback_info">список функций обработчики событий от библиотеки</param>
/// <returns>True</returns>
AUX_API bool AUX_CALL MM_InitLib(MM_CallbackInterface& callback_info);
/// <summary>
/// завершение работы auxlib.dll
/// </summary>
AUX_API void AUX_CALL MM_FreeLib();
/// <summary>
/// Получение количества устройств воспроизведения
/// </summary>
/// <returns>количества устройств воспроизведения</returns>
AUX_API int AUX_CALL MM_AudioGetOutputCount();
/// <summary>
/// получение имени устройства воспроизведения по индексу
/// </summary>
/// <param name="index">индекс</param>
/// <param name="buffer">буфер</param>
/// <param name="size">размер буфера</param>
/// <returns></returns>
AUX_API bool AUX_CALL MM_AudioGetOutputName(int index, char* buffer, int size);
/// <summary>
/// Получение количества устройств записи
/// </summary>
/// <returns>количество устройств записи</returns>
AUX_API int AUX_CALL MM_AudioGetInputCount();
/// <summary>
/// получение имени устройства записи по индексу
/// </summary>
/// <param name="index">индекс</param>
/// <param name="buffer">буфер</param>
/// <param name="size">размер буфера</param>
/// <returns></returns>
AUX_API bool AUX_CALL MM_AudioGetInputName(int index, char* buffer, int size);
//---------------------------------------------------------------------
/// <summary>
/// Установка всех девайсов
/// </summary>
/// <param name="ringer">имя устройства Звонок</param>
/// <param name="speaker">имя устройства Спикер</param>
/// <param name="microphone">имя устройства Микрофон</param>
/// <param name="direct_sound">использовать DirectSound (true) или waveform(false)</param>
AUX_API void AUX_CALL MM_AudioSetDevices(const char* ringer, const char* speaker, const char* microphone, bool direct_sound);
/// <summary>
/// Установка устройства вывода для звонков
/// 'BEEP' - проигрываются сигналы на стандартном устройстве c помощью функции Beep()
/// </summary>
/// <param name="device_name">устройство вывода</param>
AUX_API void AUX_CALL MM_AudioSetRingDevice(const char* device_name);
/// <summary>
/// Получить текущее устройство вывода для звонков
/// </summary>
/// <param name="device_name">буфер</param>
/// <param name="size">размер буфера</param>
AUX_API void AUX_CALL MM_AudioGetRingDevice(char* device_name, int size);
/// <summary>
/// Установка устройства вывода для воспроизведения файлов или тонов
/// </summary>
/// <param name="device_name">имя устройства</param>
AUX_API void AUX_CALL MM_AudioSetSpeakerDevice(const char* device_name);
/// <summary>
/// Получить текущее устройство вывода для воспроизведения файлов или тонов
/// </summary>
/// <param name="device_name">буфер</param>
/// <param name="size">размер буфера</param>
AUX_API void AUX_CALL MM_AudioGetSpeakerDevice(char* device_name, int size);
/// <summary>
/// Установка устройства ввода
/// Пока используется только для выставления громкости
/// </summary>
/// <param name="device_name">имя устройства</param>
AUX_API void AUX_CALL MM_AudioSetMicrophoneDevice(const char* device_name);
/// <summary>
/// Получить текущее устройство ввода
/// Пока используется только для выставления громкости
/// </summary>
/// <param name="device_name">буфер</param>
/// <param name="size">размер буфера</param>
AUX_API void AUX_CALL MM_AudioGetMicrophoneDevice(char* device_name, int size);
/// <summary>
/// получение флага использования библиотеки DirectSound
/// </summary>
/// <returns>значение флага</returns>
AUX_API bool AUX_CALL MM_AudioGetDirectSoundFlag();
/// <summary>
/// переключение на библиотеку DirectSound или waveform
/// </summary>
/// <param name="enable">true - DirectSound, false - waveform</param>
AUX_API void AUX_CALL MM_AudioSetDirectSoundFlag(bool enable);
//---------------------------------------------------------------------
/// <summary>
/// получить громкость устройста воспроизведения Ring
/// </summary>
/// <returns>громкость или -1 в случае неудачи</returns>
AUX_API int AUX_CALL MM_AudioGetRingVolume();
/// <summary>
/// выставить громкость устройста воспроизведения Ring
/// </summary>
/// <param name="volume">громкость воспроизведения</param>
AUX_API void AUX_CALL MM_AudioSetRingVolume(int volume);
/// <summary>
/// получить громкость устройста воспроизведения Speaker
/// </summary>
/// <returns>громкость или -1 в случае неудачи</returns>
AUX_API int AUX_CALL MM_AudioGetSpeakerVolume();
/// <summary>
/// выставить громкость устройста воспроизведения Speaker
/// </summary>
/// <param name="volume">громкость воспроизведения</param>
AUX_API void AUX_CALL MM_AudioSetSpeakerVolume(int volume);
/// <summary>
/// получить громкость устройста записи Microphone
/// </summary>
/// <returns>громкость или -1 в случае неудачи</returns>
AUX_API int AUX_CALL MM_AudioGetMicrophoneVolume();
/// <summary>
/// выставить громкость устройста записи Microphone
/// </summary>
/// <param name="volume">громкость записи</param>
AUX_API void AUX_CALL MM_AudioSetMicrophoneVolume(int volume);
//---------------------------------------------------------------------
/// <summary>
/// начать воспроизведение файла в Speaker
/// Только окончании данных для воспроизведения генерируется событие PlayStopped
/// </summary>
/// <param name="filepath">путь к файлу</param>
/// <param name="in_cycle">воспризводить файл в цикле</param>
AUX_API bool AUX_CALL MM_AudioStartPlayFile(const char* filepath, bool in_cycle);
/// <summary>
/// установить файл для воспроизведения звонка в устройство Ring
/// null - при звонке воспроизводить файл из ресурса дллки
/// если имя устройства BEEP то файл не воспроизводится. Вместо него воспроизводится сигналы на с помощью функции Beep() 
/// </summary>
/// <param name="filepath">путь к файлу</param>
AUX_API void AUX_CALL MM_AudioSetRingFile(const char* filepath);
/// <summary>
/// Получить текущей файл воспроизведения звонка
/// </summary>
/// <param name="filepath_buffer">буфер</param>
/// <param name="size">размер буфера</param>
AUX_API void AUX_CALL MM_AudioGetRingFile(char* filepath_buffer, int size);
/// <summary>
/// воспроизвести звонок в Ring
/// если RingDevice == "имя существующего устройства" то выводим на него,
/// если "BEEP" то в стандартный спикер компьютера
/// если null то воспроизводим файл из ресурса дллки
/// </summary>
AUX_API void AUX_CALL MM_AudioRing();
/// <summary>
/// прекращение воспроизведения на всех устройствах
/// </summary>
AUX_API void AUX_CALL MM_AudioAbort();
} // extern "C"
//--------------------------------------
