/* RTX H.248 Media Gate. Audio Driver Module
 *
 * Copyright @ 2020 George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "audio_device_out_alsa.h"
#include "audio_mixer.h"

#if defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
//--------------------------------------
//
//--------------------------------------
aux_device_out_alsa_t::aux_device_out_alsa_t(aux_device_out_handler_t *handler) : aux_device_out_t("aux-out", handler),
	m_device_index("default"), m_device_out(nullptr)
{
}
//--------------------------------------
//
//--------------------------------------
aux_device_out_alsa_t::~aux_device_out_alsa_t()
{
}
//--------------------------------------
//
//--------------------------------------
bool aux_device_out_alsa_t::aux_open_device(const char *deviceName)
{
	rtl::MutexLock lock(m_sync);

	if (m_opened_flag)
	{
		aux_close_device();
	}

	m_device_name = deviceName;

	if (m_device_name.isEmpty() || m_device_name == "default")
	{
		m_device_index = "default";
	}
	else
	{
		m_device_index = audio_mixer_t::device_out_from_name(m_device_name);
	}

	LOG_CALL(m_tag, "open device : name:%s clock rate %d, channels %d, block times %d ms, block count %d, block size %d bytes...",
		(const char*)m_device_index, m_clock_rate, m_channels, m_block_times, m_block_count, m_block_size);

	int err;

	if ((err = snd_pcm_open(&m_device_out, m_device_index , SND_PCM_STREAM_PLAYBACK, 0)) < 0)
	{
		LOG_ERROR(m_tag, "open device : snd_pcm_open() failed with error : '%s'", snd_strerror(err));
		aux_close_device();

		return false;
	}

    if ((err = snd_pcm_set_params(m_device_out, SND_PCM_FORMAT_S16_LE, SND_PCM_ACCESS_RW_INTERLEAVED,
		m_channels, m_clock_rate, 1, m_block_times * m_block_count * 1000)) < 0)
	{
		/* 0.16 sec */ // 160000 = 8 * 20 * 1000
        LOG_ERROR(m_tag, "open device : setup failed with error: %s", snd_strerror(err));
        return false;
    }

	if (!aux_start_thread())
	{
		LOG_WARN(m_tag, "open device : start thread failed");
		return false;
	}

	LOG_CALL(m_tag, "open device : success");

	return m_opened_flag = true;
}
//--------------------------------------
//
//--------------------------------------
void aux_device_out_alsa_t::aux_close_device()
{
	rtl::MutexLock lock(m_sync);

	// �������� ���� �������� ����������
	m_opened_flag = false;

	aux_stop_thread();

	// ��������� ���������������
	aux_stop_device();

	// ������� ����������
	if (m_device_out != nullptr)
	{
		LOG_CALL(m_tag, "close device : closing...");

		// ��������� ����� �����
		snd_pcm_close(m_device_out);
		m_device_out = nullptr;

		LOG_CALL(m_tag, "close device : closed");
	}
}
//--------------------------------------
// ����� ���������� -- ��������� ������ � ��������� ������ �� ���������������
//--------------------------------------
bool aux_device_out_alsa_t::aux_start_device()
{
	rtl::MutexLock lock(m_sync);

	if (!m_opened_flag || m_started_flag)
		return false;

	// ��������� ������ � ��������� �� ���������������
	LOG_CALL(m_tag, "start device : starting with %d headers...", m_block_count);

	//...

	LOG_CALL(m_tag, "start device: started");

	return m_started_flag = true;
}
//--------------------------------------
//
//--------------------------------------
void aux_device_out_alsa_t::aux_stop_device()
{
	rtl::MutexLock lock(m_sync);

	if (m_started_flag && m_device_out != nullptr)
	{
		LOG_CALL(m_tag, "stop device : stopping...");

		// ������� ����������
		snd_pcm_drop(m_device_out);

		m_started_flag = false;

		LOG_CALL(m_tag, "stop device : stopped");
	}
}
//--------------------------------------
//
//--------------------------------------
void aux_device_out_alsa_t::aux_set_paused(bool pause)
{
	rtl::MutexLock lock(m_sync);

	if (m_started_flag)
	{
		LOG_CALL(m_tag, "pause device : %s", pause ? "set pause" : " continue playing");

		snd_pcm_pause(m_device_out, pause);
	}
}

void aux_device_out_alsa_t::post_stop_thread()
{
	m_thread_stop_flag = true;
}
//--------------------------------------
//
//--------------------------------------
static int xrun_recovery(snd_pcm_t *handle, int err)
{
   	if (err == -EPIPE)
	{
		/* under-run */
		err = snd_pcm_prepare(handle);
        if (err < 0)
            LOG_ERROR("aux", "Can't recovery from underrun, prepare failed: %s", snd_strerror(err));
		return 0;
    }
	else if (err == -ESTRPIPE)
	{
		while ((err = snd_pcm_resume(handle)) == -EAGAIN)
            rtl::Thread::sleep(1);       /* wait until the suspend flag is released */
		if (err < 0)
		{
        	err = snd_pcm_prepare(handle);
            if (err < 0)
				printf("Can't recovery from suspend, prepare failed: %s\n", snd_strerror(err));
        }
        return 0;
    }
    return err;
}
//-----
void aux_device_out_alsa_t::thread_run(rtl::Thread *thread)
{
	LOG_CALL(m_tag, "thread : enter");

	m_thread_stop_flag = false;
	m_thread_started_event.signal();

	LOG_CALL(m_tag, "thread : start signaled");

	int err;
	short buffer[m_block_size/2];

	//file_stream_t out;
	
	//out.create("~/develop/rtxsip/play_1.raw");

	rtl::Thread::sleep(40); // wait for 2 rtp packets

	while (!m_thread_stop_flag)
	{
		try
		{
			int written = m_handler->aux_out_get_data(buffer, m_block_size/2); // m_block_size/2

			LOG_CALL(m_tag, "thread : read for %u samples --> returns %u samples", m_block_size/2, written);

			if (written == 0)
			{
				LOG_CALL(m_tag, "thread : wait for data 40 ms");
				rtl::Thread::sleep(40); // wait for 2 rtp packets
				continue;
			}
			err = snd_pcm_writei(m_device_out, buffer, written);
				
			if (err == -EAGAIN)
			{
				continue;
			}

			if (err < 0)
			{
				if (xrun_recovery(m_device_out, err) < 0)
				{
					LOG_ERROR(m_tag, "Write error: %d(%s)", err, snd_strerror(err));
					break;
				}
			}

			// // ���������� ����� ��� ����������
			// int frames_to_deliver = written;
			// // �������� ���� �� ������������
			// // LOG_CALL(m_tag, "thread : check playback buffers...");
			// // if ((frames_to_deliver = snd_pcm_avail_update(m_device_out)) < 0)
			// // {
			// // 	if (frames_to_deliver == -EPIPE)
			// // 	{
			// // 		LOG_ERROR(m_tag, "an xrun occured!");
			// // 		break;
			// // 	}
			// // 	else
			// // 	{
			// // 		LOG_ERROR(m_tag, "unknown ALSA avail update return value (%d)", (int)frames_to_deliver);
			// // 		break;
			// // 	}
			// // }
			// LOG_CALL(m_tag, "thread : playback buffer size %d", frames_to_deliver);
			// /* deliver the data */
			// out.write(buffer, written * sizeof(short));
			// if ((written = snd_pcm_writei(m_device_out, buffer, written)) < 0)
			// {
			// 	LOG_ERROR(m_tag, "write failed %d(%s)", written, snd_strerror(written));
			// 	break;
			// }

			// LOG_CALL(m_tag, "thread : wait playback for %u ms", 160);

			// if ((err = snd_pcm_wait(m_device_out, 20)) < 0)
			// {
			// 	LOG_ERROR(m_tag, "poll failed %d(%s)", err, strerror (errno));
			// 	break;
			// }
		}
		catch (rtl::Exception &ex)
		{
			LOG_ERROR(m_tag, "thread : play catches exception :\n%s", ex.get_message());
			ex.raise_notify(__FILE__, __FUNCTION__);
		}
	}

	//out.close();

	LOG_CALL(m_tag, "thread : exit : by %s.", m_thread_stop_flag ? "STOP_FLAG" : "WM_QUIT");
}
#endif // TARGET_OS_*
//--------------------------------------
