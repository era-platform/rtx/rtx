﻿/* RTX H.248 Media Gate. Audio Driver Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtx_aux.h"
#include "audio_player.h"
//#include "audio_signal_player.h"
//#include "beep_player.h"
#include "audio_mixer.h"
//--------------------------------------
//
//--------------------------------------
//extern aux_device_t g_aux;

static rtl::Mutex s_aux_sync;

static volatile bool s_aux_initialized = false;						// была проведена инициализация (генерация сигналов, загрузка стандартного звонка из ресурса...)
static volatile int s_aux_entries = 0;								// текущее количество входов функций в библиотеку 
static volatile aux_device_state_t s_aux_state = aux_state_idle_e;	// текущее состояние

static rtl::String s_aux_speaker_device_name;							// воспроизведение файлов, тонов и выставления громкости
static rtl::String s_aux_microphone_device_name;						// для выставления громкости
static rtl::String s_aux_ringer_device_name;							// для звонков
static rtl::String s_aux_ringer_file_path;								// файл для звонков

struct device_info_t
{
	char name[64];
	char description[64];
};

static rtl::ArrayT<device_info_t> s_aux_out_devices;
static rtl::ArrayT<device_info_t> s_aux_in_devices;

static aux_ringer_type_t s_aux_ringer_type = ring_std_file_e;		// тип звонка
static audio_player_wave_t* s_aux_ring_player = nullptr;			// воспроизведение файлов звонков
static audio_data_reader_t* s_aux_ring_reader = nullptr;			// для воспроизведения звонка из файла

//static bool s_aux_file_play_in_cycle;
static audio_data_reader_t* s_aux_file_reader = nullptr;			// источник аудио данных
static audio_player_wave_t* s_aux_file_player = nullptr;			// воспроизведение в спикер из m_file_reader

static MM_PlayStoppedHandler s_aux_pfn_play_stopped = nullptr;		// функция обратного вызова для уведомления об окончании воспроизведения файла

static bool s_aux_set_to_default_devices();
static void s_aux_set_ringer_name(const char* device_name_ptr);
static void s_aux_set_speaker_name(const char* device_name_ptr);
static void s_aux_set_microphone_name(const char* device_name_ptr);
static audio_data_reader_t* s_aux_create_file_reader(const char* filepath);
static void s_aux_start_ringing();
static void s_aux_abort();
static const char* get_device_play_state(aux_device_state_t state);
static void s_aux_stop_ringing();
static void s_aux_reset_file_playing();
static void s_aux_stop_file_playing();
static void s_aux_reset_tone_playing();
static void s_aux_stop_tone_playing();
static void s_aux_raise_play_stopped();
static int s_aux_update_out_devices_list();
static int s_aux_update_in_devices_list();
//--------------------------------------
// макрос блокировки и проверки на возможность работы !
//--------------------------------------
#define CHECK_AND_LOCK_AUX \
	if (!s_aux_initialized) \
		return; \
	rtl::MutexLock lock(s_aux_sync); \
	if (!s_aux_initialized) \
		return;
//--------------------------------------
// макрос блокировки и проверки на возможность работы !
// параметр определяет значение для неуспешного возврата из функции
//--------------------------------------
#define CHECK_AND_LOCK_AUX_RV(rv) \
	if (!s_aux_initialized) \
		return rv; \
	rtl::MutexLock lock(s_aux_sync); \
	if (!s_aux_initialized) \
		return rv;
//--------------------------------------
//
//--------------------------------------
AUX_API bool AUX_CALL MM_InitLib(MM_CallbackInterface& callback_info)
{
	bool result = false;

	EXCEPTION_ENTER;

	try
	{
		//g_aux.initialize();

		rtl::MutexLock lock(s_aux_sync);

		if (!s_aux_initialized)
		{
			LOG_CALL("auxlib", "initialize...");

			s_aux_pfn_play_stopped = callback_info.PlayStopped;

			s_aux_set_to_default_devices();

			s_aux_initialized = true;

			LOG_CALL("auxlib", "initialize...done");

			result = true;
		}
	}
	catch (rtl::Exception& ex)
	{
		LOG_ERROR("auxlib", "Multimedia library initializing failed with rtl::Exception %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	EXCEPTION_LEAVE;

	return result;
}
//--------------------------------------
//
//--------------------------------------
AUX_API void AUX_CALL MM_FreeLib()
{
	if (!s_aux_initialized)
		return;

	EXCEPTION_ENTER;

	try
	{
		//g_aux.destroy();

		// специально чтобы остальные функции даже не пробовали зайти!

		s_aux_initialized = false;

		rtl::MutexLock lock(s_aux_sync);

		LOG_CALL("auxlib", "destroy...");

		if (s_aux_ring_player != nullptr)
		{
			DELETEO(s_aux_ring_player);
			s_aux_ring_player = nullptr;
		}

		if (s_aux_ring_reader != nullptr)
		{
			DELETEO(s_aux_ring_reader);
			s_aux_ring_reader = nullptr;
		}

		/*if (s_aux_ring_beeper != nullptr)
		{
			DELETEO(s_aux_ring_beeper);
			s_aux_ring_beeper = nullptr;
		}

		if (s_aux_dtmf_player != nullptr)
		{
			DELETEO(s_aux_dtmf_player);
			s_aux_dtmf_player = nullptr;
		}*/

		if (s_aux_file_reader != nullptr)
		{
			DELETEO(s_aux_file_reader);
			s_aux_file_reader = nullptr;
		}

		if (s_aux_file_player != nullptr)
		{
			DELETEO(s_aux_file_player);
			s_aux_file_player = nullptr;
		}

		//audio_signal_player_t::cleanup_std_tomes();

		LOG_CALL("auxlib", "destroy...done");

		s_aux_pfn_play_stopped = nullptr;
	}
	catch (rtl::Exception& ex)
	{
		LOG_ERROR("auxlib", "Multimedia library destroy failed with rtl::Exception %s", ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	EXCEPTION_LEAVE;
}
//--------------------------------------
//
//--------------------------------------
AUX_API int AUX_CALL MM_AudioGetOutputCount()
{
	return s_aux_out_devices.getCount();
}
//--------------------------------------
//
//--------------------------------------
AUX_API bool AUX_CALL MM_AudioGetOutputName(int index, char* buffer, int size)
{
	if (index >= 0 && index < s_aux_out_devices.getCount())
	{
		device_info_t& info = s_aux_out_devices.getAt(index);
		
		buffer[0] = 0;

		if (info.name[0] != 0)
		{
			int len = std_snprintf(buffer, size-1, "%s (%s)", info.name, info.description);
			buffer[len] = 0;
		}

		return true;
	}

	return false;
}
//--------------------------------------
//
//--------------------------------------
AUX_API int AUX_CALL MM_AudioGetInputCount()
{
	return s_aux_in_devices.getCount();
}
//--------------------------------------
//
//--------------------------------------
AUX_API bool AUX_CALL MM_AudioGetInputName(int index, char* buffer, int size)
{
	if (index >= 0 && index < s_aux_in_devices.getCount())
	{
		device_info_t& info = s_aux_in_devices.getAt(index);
		
		buffer[0] = 0;

		if (info.name[0] != 0)
		{
			int len = std_snprintf(buffer, size-1, "%s (%s)", info.name, info.description);
			buffer[len] = 0;
		}

		return true;
	}

	return false;
}
//--------------------------------------
//
//--------------------------------------
AUX_API void AUX_CALL MM_AudioSetDevices(const char* ringer, const char* speaker, const char* microphone)
{
	EXCEPTION_ENTER;

	try
	{
		CHECK_AND_LOCK_AUX;

		LOG_CALL("auxlib", "set all devices to ringer:%s speaker:%s microphone:%s...", ringer, speaker, microphone);

		s_aux_set_ringer_name(ringer);
		s_aux_set_speaker_name(speaker);
		s_aux_set_microphone_name(microphone);

		LOG_CALL("auxlib", "set all devices...done");
	}
	catch (rtl::Exception& ex)
	{
		LOG_ERROR("auxlib", "%s failed with rtl::Exception %s", __FUNCTION__, ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	EXCEPTION_LEAVE;

}
//--------------------------------------
//
//--------------------------------------
AUX_API void AUX_CALL MM_AudioSetRingDevice(const char* device_name)
{
	EXCEPTION_ENTER;

	try
	{
		CHECK_AND_LOCK_AUX;

		LOG_CALL("auxlib", "set ringer name to %s...", device_name);

		s_aux_set_ringer_name(device_name);

		LOG_CALL("auxlib", "set ringer name...done", device_name);

	}
	catch (rtl::Exception& ex)
	{
		LOG_ERROR("auxlib", "%s failed with rtl::Exception %s", __FUNCTION__, ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	EXCEPTION_LEAVE;
}
//--------------------------------------
//
//--------------------------------------
AUX_API void AUX_CALL MM_AudioGetRingDevice(char* buffer, int size)
{
	EXCEPTION_ENTER;

	try
	{
		CHECK_AND_LOCK_AUX;

		LOG_CALL("auxlib", "get ringer name...");

		if (buffer != nullptr && size > 0)
		{
			if (s_aux_ringer_device_name.getLength() > 0)
			{
				memcpy(buffer, s_aux_ringer_device_name, size - 1);
				buffer[size - 1] = 0;
			}
			else
			{
				buffer[0] = 0;
			}
		}

		LOG_CALL("auxlib", "get ringer name...return %s", buffer);
	}
	catch (rtl::Exception& ex)
	{
		LOG_ERROR("auxlib", "%s failed with rtl::Exception %s", __FUNCTION__, ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	EXCEPTION_LEAVE;
}
//--------------------------------------
//
//--------------------------------------
AUX_API void AUX_CALL MM_AudioSetSpeakerDevice(const char* device_name)
{
	EXCEPTION_ENTER;

	try
	{
		CHECK_AND_LOCK_AUX;

		LOG_CALL("auxlib", "set speaker name to %s...", device_name);

		s_aux_set_speaker_name(device_name);

		LOG_CALL("auxlib", "set speaker name...done");

	}
	catch (rtl::Exception& ex)
	{
		LOG_ERROR("auxlib", "%s failed with rtl::Exception %s", __FUNCTION__, ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	EXCEPTION_LEAVE;
}
//--------------------------------------
//
//--------------------------------------
AUX_API void AUX_CALL MM_AudioGetSpeakerDevice(char* buffer, int size)
{

	EXCEPTION_ENTER;

	try
	{
		CHECK_AND_LOCK_AUX;

		LOG_CALL("auxlib", "get speaker name...");

		if (buffer != nullptr && size > 0)
		{
			if (s_aux_speaker_device_name.getLength() > 0)
			{
				memcpy(buffer, s_aux_speaker_device_name, size - 1);
				buffer[size - 1] = 0;
			}
			else
			{
				buffer[0] = 0;
			}
		}

		LOG_CALL("auxlib", "get speaker name...returns %s", buffer);
	}
	catch (rtl::Exception& ex)
	{
		LOG_ERROR("auxlib", "%s failed with rtl::Exception %s", __FUNCTION__, ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	EXCEPTION_LEAVE;
}
//--------------------------------------
//
//--------------------------------------
AUX_API void AUX_CALL MM_AudioSetMicrophoneDevice(const char* device_name)
{

	EXCEPTION_ENTER;

	try
	{
		CHECK_AND_LOCK_AUX;

		LOG_CALL("auxlib", "set microphone name to %s...", device_name);

		s_aux_set_microphone_name(device_name);

		LOG_CALL("auxlib", "set microphone name...done");
	}
	catch (rtl::Exception& ex)
	{
		LOG_ERROR("auxlib", "%s failed with rtl::Exception %s", __FUNCTION__, ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	EXCEPTION_LEAVE;
}
//--------------------------------------
//
//--------------------------------------
AUX_API void AUX_CALL MM_AudioGetMicrophoneDevice(char* buffer, int size)
{
	EXCEPTION_ENTER;

	try
	{
		CHECK_AND_LOCK_AUX;

		LOG_CALL("auxlib", "get microphone name...");

		if (buffer != nullptr && size > 0)
		{
			if (s_aux_microphone_device_name.getLength() > 0)
			{
				memcpy(buffer, s_aux_microphone_device_name, size - 1);
				buffer[size - 1] = 0;
			}
			else
			{
				buffer[0] = 0;
			}
		}

		LOG_CALL("auxlib", "get microphone name...return %s", buffer);
	}
	catch (rtl::Exception& ex)
	{
		LOG_ERROR("auxlib", "%s failed with rtl::Exception %s", __FUNCTION__, ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	EXCEPTION_LEAVE;
}
//--------------------------------------
//
//--------------------------------------
AUX_API int AUX_CALL MM_AudioGetRingVolume()
{
	int result = -1;

	EXCEPTION_ENTER;

	try
	{
		CHECK_AND_LOCK_AUX_RV(-1);

		LOG_CALL("auxlib", "get ringer volume...");

		result = audio_mixer_t::get_output_volume(s_aux_ringer_device_name);

		LOG_CALL("auxlib", "get ringer volume...returns %d", result);
	}
	catch (rtl::Exception& ex)
	{
		LOG_ERROR("auxlib", "%s failed with rtl::Exception %s", __FUNCTION__, ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	EXCEPTION_LEAVE;

	return result;
}
//--------------------------------------
//
//--------------------------------------
AUX_API void AUX_CALL MM_AudioSetRingVolume(int volume)
{
	EXCEPTION_ENTER;

	try
	{
		CHECK_AND_LOCK_AUX;

		LOG_CALL("auxlib", "set ringer volume to %d...", volume);

		audio_mixer_t::set_output_volume(s_aux_ringer_device_name, volume);

		LOG_CALL("auxlib", "set ringer volume...done");
	}
	catch (rtl::Exception& ex)
	{
		LOG_ERROR("auxlib", "%s failed with rtl::Exception %s", __FUNCTION__, ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	EXCEPTION_LEAVE;
}
//--------------------------------------
//
//--------------------------------------
AUX_API int AUX_CALL MM_AudioGetSpeakerVolume()
{
	int result = 0;

	EXCEPTION_ENTER;

	try
	{
		CHECK_AND_LOCK_AUX_RV(-1);

		LOG_CALL("auxlib", "get speaker volume...");

		result = audio_mixer_t::get_output_volume(s_aux_speaker_device_name);

		LOG_CALL("auxlib", "get speaker volume...return %d", result);
	}
	catch (rtl::Exception& ex)
	{
		LOG_ERROR("auxlib", "%s failed with rtl::Exception %s", __FUNCTION__, ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	EXCEPTION_LEAVE;

	return result;
}
//--------------------------------------
//
//--------------------------------------
AUX_API void AUX_CALL MM_AudioSetSpeakerVolume(int volume)
{
	EXCEPTION_ENTER;

	try
	{
		CHECK_AND_LOCK_AUX;

		LOG_CALL("auxlib", "set speaker volume to %d...", volume);

		audio_mixer_t::set_output_volume(s_aux_speaker_device_name, volume);

		LOG_CALL("auxlib", "set speaker volume...done");
	}
	catch (rtl::Exception& ex)
	{
		LOG_ERROR("auxlib", "%s failed with rtl::Exception %s", __FUNCTION__, ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	EXCEPTION_LEAVE;
}
//--------------------------------------
//
//--------------------------------------
AUX_API int AUX_CALL MM_AudioGetMicrophoneVolume()
{
	int result = -1;

	EXCEPTION_ENTER;

	try
	{
		CHECK_AND_LOCK_AUX_RV(-1);

		LOG_CALL("auxlib", "get microphone volume...");

		result = audio_mixer_t::get_input_volume(s_aux_microphone_device_name);

		LOG_CALL("auxlib", "get microphone volume...return %d", result);
	}
	catch (rtl::Exception& ex)
	{
		LOG_ERROR("auxlib", "%s failed with rtl::Exception %s", __FUNCTION__, ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	EXCEPTION_LEAVE;

	return result;
}
//--------------------------------------
//
//--------------------------------------
AUX_API void AUX_CALL MM_AudioSetMicrophoneVolume(int volume)
{
	EXCEPTION_ENTER;

	try
	{
		//g_aux.set_microphone_volume(volume);

		CHECK_AND_LOCK_AUX;

		LOG_CALL("auxlib", "set microphone volume to %d...", volume);

		audio_mixer_t::set_input_volume(s_aux_microphone_device_name, volume);

		LOG_CALL("auxlib", "set microphone volume...done");
	}
	catch (rtl::Exception& ex)
	{
		LOG_ERROR("auxlib", "%s failed with rtl::Exception %s", __FUNCTION__, ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	EXCEPTION_LEAVE;
}
//--------------------------------------
//
//--------------------------------------
AUX_API bool AUX_CALL MM_AudioStartPlayFile(const char* filepath, bool in_cycle)
{
	bool result = false;

	EXCEPTION_ENTER;

	try
	{
		CHECK_AND_LOCK_AUX_RV(false);

		LOG_CALL("auxlib", "start play file loop %s path %s...", WCS_BOOL(in_cycle), filepath);

		s_aux_abort();

		s_aux_file_reader = s_aux_create_file_reader(filepath);

		if (s_aux_file_reader != nullptr)
		{
			s_aux_file_player = NEW audio_player_wave_t(false, "player");

			rtl::String str;
			if (s_aux_speaker_device_name.isEmpty())
			{
				char buffer[33] = { 0 };
				MM_AudioGetOutputName(0, buffer, 33);
				str.append(buffer);
				str.trim();
			}
			else
			{
				str.append(s_aux_speaker_device_name);
			}

			result = s_aux_file_player->start_play(str, s_aux_file_reader, in_cycle, s_aux_raise_play_stopped, nullptr);

			if (result)
			{
				s_aux_state = aux_state_file_playing_e;

				LOG_CALL("auxlib", "start play file...done");
			}
			else
			{
				DELETEO(s_aux_file_reader);
				s_aux_file_reader = nullptr;

				DELETEO(s_aux_file_player);
				s_aux_file_player = nullptr;

				LOG_CALL("auxlib", "start play file...failed");
			}
		}
	}
	catch (rtl::Exception& ex)
	{
		LOG_ERROR("auxlib", "%s failed with rtl::Exception %s", __FUNCTION__, ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	EXCEPTION_LEAVE;

	return result;
}
//--------------------------------------
//
//--------------------------------------
AUX_API void AUX_CALL MM_AudioSetRingFile(const char* filepath)
{
	EXCEPTION_ENTER;

	try
	{
		CHECK_AND_LOCK_AUX;

		LOG_CALL("auxlib", "set ringer file to %s...", filepath);

		if (s_aux_ringer_file_path == filepath)
		{
			LOG_CALL("auxlib", "set ringer file...already done");
			return;
		}

		if (filepath == nullptr || filepath[0] == 0)
		{
			if (s_aux_state == aux_state_ringing_e)
			{
				s_aux_stop_ringing();
				s_aux_ringer_file_path.setEmpty();
				s_aux_ringer_type = ring_std_file_e;
				s_aux_start_ringing();
			}
			else
			{
				s_aux_ringer_file_path.setEmpty();
				s_aux_ringer_type = ring_std_file_e;
			}
		}
		else
		{
			s_aux_ringer_file_path = filepath;

			if (s_aux_state == aux_state_ringing_e)
			{
				s_aux_stop_ringing();
				s_aux_ringer_type = ring_custom_file_e;
				s_aux_start_ringing();
			}
			else
			{
				s_aux_ringer_type = ring_custom_file_e;
			}
		}

		LOG_CALL("auxlib", "set ringer file...done");
	}
	catch (rtl::Exception& ex)
	{
		LOG_ERROR("auxlib", "%s failed with rtl::Exception %s", __FUNCTION__, ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	EXCEPTION_LEAVE;
}
//--------------------------------------
//
//--------------------------------------
AUX_API void AUX_CALL MM_AudioGetRingFile(char* buffer, int size)
{
	EXCEPTION_ENTER;

	try
	{
		CHECK_AND_LOCK_AUX;

		LOG_CALL("auxlib", "get ringer file...");

		if (buffer != nullptr && size > 0)
		{
			if (s_aux_ringer_file_path.getLength() > 0)
			{
				memcpy(buffer, s_aux_ringer_file_path, size - 1);
				buffer[size - 1] = 0;
			}
			else
			{
				buffer[0] = 0;
			}
		}

		LOG_CALL("auxlib", "get ringer file...returns %s", buffer);
	}
	catch (rtl::Exception& ex)
	{
		LOG_ERROR("auxlib", "%s failed with rtl::Exception %s", __FUNCTION__, ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	EXCEPTION_LEAVE;
}
//--------------------------------------
//
//--------------------------------------
AUX_API void AUX_CALL MM_AudioRing()
{
	EXCEPTION_ENTER;

	try
	{
		//g_aux.start_ringing();

		CHECK_AND_LOCK_AUX;

		LOG_CALL("auxlib", "start ringing...");

		s_aux_start_ringing();

		LOG_CALL("auxlib", "start ringing...done");
	}
	catch (rtl::Exception& ex)
	{
		LOG_ERROR("auxlib", "%s failed with rtl::Exception %s", __FUNCTION__, ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	EXCEPTION_LEAVE;
}
//--------------------------------------
//
//--------------------------------------
AUX_API void AUX_CALL MM_AudioAbort()
{
	EXCEPTION_ENTER;

	try
	{
		//g_aux.abort();

		CHECK_AND_LOCK_AUX

		LOG_CALL("auxlib", "abort players state(%s)...", get_device_play_state(s_aux_state));

		s_aux_abort();

		LOG_CALL("auxlib", "abort players...done");
	}
	catch (rtl::Exception& ex)
	{
		LOG_ERROR("auxlib", "%s failed with rtl::Exception %s", __FUNCTION__, ex.get_message());
		ex.raise_notify(__FILE__, __FUNCTION__);
	}

	EXCEPTION_LEAVE;
}
//--------------------------------------
//
//--------------------------------------
static bool s_aux_set_to_default_devices()
{
	s_aux_speaker_device_name.setEmpty();
	s_aux_ringer_device_name.setEmpty();
	s_aux_microphone_device_name.setEmpty();

	if (s_aux_update_out_devices_list() > 0)
	{
		s_aux_speaker_device_name << s_aux_out_devices[0].name << " (" << s_aux_out_devices[0].description + ')';
		s_aux_ringer_device_name << s_aux_out_devices[0].name << " (" << s_aux_out_devices[0].description + ')';
	}
	
	if (s_aux_update_in_devices_list() > 0)
	{
		s_aux_microphone_device_name << s_aux_in_devices[0].name << " (" << s_aux_in_devices[0].description + ')';
	}

	return true;
}
//--------------------------------------
//
//--------------------------------------
void s_aux_set_ringer_name(const char* device_name_ptr)
{
	rtl::String device_name = device_name_ptr;

	device_name.trim();

	if (s_aux_ringer_device_name == device_name)
	{
		return;
	}

	if (device_name *= "default")
	{
		s_aux_ringer_device_name.setEmpty();
	}
	else
	{
		s_aux_ringer_device_name = device_name;
	}

	if (!s_aux_ringer_device_name.isEmpty() && std_strnicmp(s_aux_ringer_device_name, "BEEP", 4) == 0)
	{
		s_aux_ringer_device_name = s_aux_speaker_device_name;
		s_aux_ringer_type = ring_std_file_e;
	}

	// если звенит звонок то перезапустим его
	if (s_aux_state == aux_state_ringing_e)
	{
		s_aux_stop_ringing();

		s_aux_start_ringing();
	}
}
//--------------------------------------
//
//--------------------------------------
static void s_aux_set_speaker_name(const char* device_name_ptr)
{
	rtl::String device_name = device_name_ptr;

	device_name.trim();

	if (s_aux_speaker_device_name == device_name)
	{
		return;
	}

	if (device_name == nullptr || std_stricmp(device_name, "default") == 0)
	{
		s_aux_speaker_device_name.setEmpty();
	}
	else
	{
		s_aux_speaker_device_name = device_name;
	}

	if (s_aux_state == aux_state_file_playing_e)
	{
		s_aux_reset_file_playing();
	}
	else if (s_aux_state == aux_state_tone_playing_e)
	{
		s_aux_reset_tone_playing();
	}
}
//--------------------------------------
//
//--------------------------------------
static void s_aux_set_microphone_name(const char* device_name_ptr)
{
	rtl::String device_name = device_name_ptr;

	device_name.trim();

	if (s_aux_microphone_device_name != device_name)
	{
		if (device_name == nullptr || std_stricmp(device_name, "default") == 0)
		{
			s_aux_microphone_device_name.setEmpty();
		}
		else
		{
			s_aux_microphone_device_name = device_name;
		}
	}
}
//--------------------------------------
//
//--------------------------------------
bool s_aux_is_filepath_mp3(const char* filepath)
{
	const char* ext = strchr(filepath, '.');

	return ext != nullptr ? std_stricmp(ext, ".mp3") == 0 : false;
}
//--------------------------------------
//
//--------------------------------------
static audio_data_reader_t* s_aux_create_file_reader(const char* filepath)
{
	audio_data_reader_t* reader = nullptr;

	if (s_aux_is_filepath_mp3(filepath))
	{
		audio_data_reader_mp3_file_t* mp3_reader = NEW audio_data_reader_mp3_file_t;

		if (!mp3_reader->open(filepath))
		{
			DELETEO(mp3_reader);
		}
		else
		{
			reader = mp3_reader;
		}
	}
	else
	{
		audio_data_reader_wave_file_t* wave_reader = NEW audio_data_reader_wave_file_t;

		if (!wave_reader->open(filepath))
		{
			DELETEO(wave_reader);
		}
		else
		{
			reader = wave_reader;
		}
	}

	return reader;
}
//--------------------------------------
//
//--------------------------------------
static void s_aux_raise_play_stopped()
{
	if (s_aux_pfn_play_stopped != nullptr)
	{
		s_aux_pfn_play_stopped();
	}
}
//--------------------------------------
//
//--------------------------------------
static void s_aux_start_ringing()
{
	s_aux_abort();

	if (s_aux_ringer_type == ring_beeper_e)
	{
		/*s_aux_ring_beeper = NEW audio_player_beeper_t;

		const wchar_t* params = s_aux_ringer_device_name;

		s_aux_ring_beeper->set_parameters(params + 4);
		s_aux_ring_beeper->play();*/
	}
	else
	{
		if (s_aux_ringer_type == ring_custom_file_e)
		{
			s_aux_ring_reader = s_aux_create_file_reader(s_aux_ringer_file_path);
		}

		if (s_aux_ring_reader != nullptr)
		{
			s_aux_ring_player = NEW audio_player_wave_t(false, "ringer");

			rtl::String str;
			if (s_aux_ringer_device_name.isEmpty())
			{
				char buffer[33];
				MM_AudioGetOutputName(0, buffer, 33);
				str.append(buffer);
				str.trim();
			}
			else
			{
				str.append(s_aux_ringer_device_name);
			}

			if (s_aux_ring_player->start_play(str, s_aux_ring_reader, true, nullptr, nullptr))
			{
				s_aux_state = aux_state_ringing_e;
			}
			else
			{
				DELETEO(s_aux_ring_player);
				s_aux_ring_player = nullptr;

				s_aux_ring_reader->close();
				DELETEO(s_aux_ring_reader);
				s_aux_ring_reader = nullptr;
			}
		}
	}
}
//--------------------------------------
//
//--------------------------------------
static void s_aux_abort()
{
	switch (s_aux_state)
	{
	case aux_state_ringing_e:
		s_aux_stop_ringing();
		break;
	case aux_state_file_playing_e:
		s_aux_stop_file_playing();
		break;
	case aux_state_tone_playing_e:
		s_aux_stop_tone_playing();
		break;
	default:
		//
		break;
	}
}
//--------------------------------------
//
//--------------------------------------
static void s_aux_stop_ringing()
{
	if (s_aux_ringer_type == ring_beeper_e)
	{
		/*s_aux_ring_beeper->stop();
		DELETEO(s_aux_ring_beeper);
		s_aux_ring_beeper = nullptr;*/
	}
	else // ring_std_file_e & ring_custom_file_e
	{
		s_aux_ring_player->stop_play();
		DELETEO(s_aux_ring_player);
		s_aux_ring_player = nullptr;

		s_aux_ring_reader->close();
		DELETEO(s_aux_ring_reader);
		s_aux_ring_reader = nullptr;
	}

	s_aux_state = aux_state_idle_e;
}
//--------------------------------------
//
//--------------------------------------
static void s_aux_reset_file_playing()
{
	s_aux_file_player->set_output(s_aux_speaker_device_name);
}
//--------------------------------------
//
//--------------------------------------
static void s_aux_stop_file_playing()
{
	if (s_aux_file_player != nullptr)
	{
		s_aux_file_player->stop_play();
		DELETEO(s_aux_file_player);
		s_aux_file_player = nullptr;
	}

	if (s_aux_file_reader)
	{
		s_aux_file_reader->close();
		DELETEO(s_aux_file_reader);
		s_aux_file_reader = nullptr;
	}

	s_aux_state = aux_state_idle_e;
}
//--------------------------------------
//
//--------------------------------------
static void s_aux_reset_tone_playing()
{
	/*if (s_aux_dtmf_player != nullptr && s_aux_dtmf_player->is_playing())
	{
		s_aux_dtmf_player->start_playing(s_aux_speaker_device_name, s_aux_last_tone);
	}*/
}
//--------------------------------------
//
//--------------------------------------
static void s_aux_stop_tone_playing()
{
	/*if (s_aux_dtmf_player != nullptr)
	{
		s_aux_dtmf_player->stop_playing();
		DELETEO(s_aux_dtmf_player);
		s_aux_dtmf_player = nullptr;
	}*/
}
//--------------------------------------
// функции помошники
//--------------------------------------
int get_output_by_name(const char* device_name)
{
#if defined (TARGET_OS_WINDOWS)
	int woc_count = waveOutGetNumDevs();

	if (device_name == nullptr || device_name[0] == 0 || woc_count == 0)
		return -1;

	WAVEOUTCAPSA woc;

	for (int i = 0; i < woc_count; i++)
	{
		memset(&woc, 0, sizeof woc);
		
		if (waveOutGetDevCapsA(i, &woc, sizeof woc) == MMSYSERR_NOERROR)
		{
			woc.szPname[MAXPNAMELEN-1] = 0;
			rtl::String str(woc.szPname);
			str.trim(" ");
			if (_stricmp(str, device_name) == 0)
			{
				return i;
			}
		}
	}
#endif
	return -1;
}
//--------------------------------------
// функции помошники
//--------------------------------------
int get_input_by_name(const char* device_name)
{
#if defined (TARGET_OS_WINDOWS)
	int wic_count = waveOutGetNumDevs();

	if (device_name == nullptr || device_name[0] == 0 || wic_count == 0)
		return -1;

	WAVEINCAPSA wic;

	for (int i = 0; i < wic_count; i++)
	{
		memset(&wic, 0, sizeof wic);
		
		if (waveInGetDevCapsA(i, &wic, sizeof wic) == MMSYSERR_NOERROR)
		{
			wic.szPname[MAXPNAMELEN-1] = 0;
			rtl::String str(wic.szPname);
			str.trim(" ");
			if (_stricmp(str, device_name) == 0)
			{
				return i;
			}
		}
	}
#endif
	return -1;
}
//--------------------------------------
//
//--------------------------------------
static const char* get_device_play_state(aux_device_state_t state)
{
	const char* names[] = { "aux_idle_e", "aux_ringing_e", "aux_file_playing_e", "aux_tone_playing_e" };

	return (state >= aux_state_idle_e && state <= aux_state_tone_playing_e) ? names[state] : "aux_unknown_state_e";
}
//--------------------------------------
//
//--------------------------------------
static int s_aux_update_out_devices_list()
{
	s_aux_out_devices.clear();

#if defined (TARGET_OS_WINDOWS)
	int count = waveOutGetNumDevs();

	if (count > 0)
	{
		WAVEOUTCAPSW woc;

		for (int i = 0; i < count; i++)
		{
			memset(&woc, 0, sizeof woc);
		
			if (waveOutGetDevCapsW(i, &woc, sizeof woc) == MMSYSERR_NOERROR)
			{
				device_info_t info;
				woc.szPname[MAXPNAMELEN-1] = 0;
				rtl::wcs_to_utf8(info.name, sizeof(info.name) - 1, woc.szPname, sizeof(woc.szPname));
				info.name[sizeof(info.name) - 1] = 0;
				info.description[0] = 0;

				s_aux_out_devices.add(info);
			}
		}
	}
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	snd_ctl_t *handle;
	int card, err, dev;//, idx;
	snd_ctl_card_info_t *info;
	snd_pcm_info_t *pcminfo;
	snd_ctl_card_info_alloca(&info);
	snd_pcm_info_alloca(&pcminfo);
	static snd_pcm_stream_t stream = SND_PCM_STREAM_PLAYBACK;

	card = -1;
	if (snd_card_next(&card) < 0 || card < 0)
	{
		LOG_ERROR("AUX", "no soundcards found...");
		return 0;
	}
	
	LOG_CALL("AUX", "**** List of %s Hardware Devices ****", snd_pcm_stream_name(stream));
	
	while (card >= 0)
	{
		char name[32];
		sprintf(name, "hw:%d", card);
		
		if ((err = snd_ctl_open(&handle, name, 0)) < 0)
		{
			LOG_ERROR("AUX", "control open (%i): %s", card, snd_strerror(err));
			goto next_card;
		}
		
		if ((err = snd_ctl_card_info(handle, info)) < 0)
		{
			LOG_ERROR("AUX", "control hardware info (%i): %s", card, snd_strerror(err));
			snd_ctl_close(handle);
			goto next_card;
		}
		
		dev = -1;
		
		while (1)
		{
			if (snd_ctl_pcm_next_device(handle, &dev)<0)
				LOG_ERROR("AUX", "snd_ctl_pcm_next_device");
			if (dev < 0)
				break;
			snd_pcm_info_set_device(pcminfo, dev);
			snd_pcm_info_set_subdevice(pcminfo, 0);
			snd_pcm_info_set_stream(pcminfo, stream);

			if ((err = snd_ctl_pcm_info(handle, pcminfo)) < 0)
			{
				if (err != -ENOENT)
					LOG_ERROR("AUX", "control digital audio info (%i): %s", card, snd_strerror(err));
				continue;
			}

			LOG_CALL("AUX", "card %i: %s [%s], device %i: %s [%s]",
				card, snd_ctl_card_info_get_id(info), snd_ctl_card_info_get_name(info), dev,
				snd_pcm_info_get_id(pcminfo), snd_pcm_info_get_name(pcminfo));

			device_info_t dev_info;
			STR_COPY(dev_info.name, snd_ctl_card_info_get_name(info));
			STR_COPY(dev_info.description, snd_pcm_info_get_name(pcminfo));

			s_aux_out_devices.add(dev_info);
		}
		snd_ctl_close(handle);
	next_card:
		if (snd_card_next(&card) < 0)
		{
			LOG_CALL("AUX", "snd_card_next");
			break;
		}
	}

#endif
	return s_aux_out_devices.getCount();
}
//--------------------------------------
//
//--------------------------------------
static int s_aux_update_in_devices_list()
{
	s_aux_in_devices.clear();

#if defined (TARGET_OS_WINDOWS)
	int count = waveInGetNumDevs();

	if (count > 0)
	{
		WAVEINCAPSW wic;

		for (int i = 0; i < count; i++)
		{
			memset(&wic, 0, sizeof wic);
		
			if (waveInGetDevCapsW(i, &wic, sizeof wic) == MMSYSERR_NOERROR)
			{
				device_info_t info;
				wic.szPname[MAXPNAMELEN-1] = 0;
				rtl::wcs_to_utf8(info.name, sizeof(info.name)-1, wic.szPname, sizeof(wic.szPname));
				info.name[sizeof(info.name) - 1] = 0;
				info.description[0] = 0;

				s_aux_in_devices.add(info);
			}
		}
	}
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	snd_ctl_t *handle;
	int card, err, dev;
	snd_ctl_card_info_t *info;
	snd_pcm_info_t *pcminfo;
	snd_ctl_card_info_alloca(&info);
	snd_pcm_info_alloca(&pcminfo);
	static snd_pcm_stream_t stream = SND_PCM_STREAM_CAPTURE;

	card = -1;
	if (snd_card_next(&card) < 0 || card < 0)
	{
		LOG_ERROR("AUX", "no soundcards found...");
		return 0;
	}
	
	LOG_CALL("AUX", "**** List of %s Hardware Devices ****", snd_pcm_stream_name(stream));
	
	while (card >= 0)
	{
		char name[32];
		sprintf(name, "hw:%d", card);
		
		if ((err = snd_ctl_open(&handle, name, 0)) < 0)
		{
			LOG_ERROR("AUX", "control open (%i): %s", card, snd_strerror(err));
			goto next_card;
		}
		
		if ((err = snd_ctl_card_info(handle, info)) < 0)
		{
			LOG_ERROR("AUX", "control hardware info (%i): %s", card, snd_strerror(err));
			snd_ctl_close(handle);
			goto next_card;
		}
		
		dev = -1;
		
		while (1)
		{
			if (snd_ctl_pcm_next_device(handle, &dev)<0)
				LOG_ERROR("AUX", "snd_ctl_pcm_next_device");
			if (dev < 0)
				break;
			snd_pcm_info_set_device(pcminfo, dev);
			snd_pcm_info_set_subdevice(pcminfo, 0);
			snd_pcm_info_set_stream(pcminfo, stream);

			if ((err = snd_ctl_pcm_info(handle, pcminfo)) < 0)
			{
				if (err != -ENOENT)
					LOG_ERROR("AUX", "control digital audio info (%i): %s", card, snd_strerror(err));
				continue;
			}

			LOG_CALL("AUX", "card %i: %s [%s], device %i: %s [%s]",
				card, snd_ctl_card_info_get_id(info), snd_ctl_card_info_get_name(info), dev,
				snd_pcm_info_get_id(pcminfo), snd_pcm_info_get_name(pcminfo));

			device_info_t dev_info;
			STR_COPY(dev_info.name, snd_ctl_card_info_get_name(info));
			STR_COPY(dev_info.description, snd_pcm_info_get_name(pcminfo));

			s_aux_in_devices.add(dev_info);
		}
		snd_ctl_close(handle);
	next_card:
		if (snd_card_next(&card) < 0)
		{
			LOG_CALL("AUX", "snd_card_next");
			break;
		}
	}

#endif
	return s_aux_out_devices.getCount();
}
//--------------------------------------
