﻿/* RTX H.248 Media Gate. Audio Driver Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "audio_mixer.h"
//-----------------------------------------------
// utilites: parse "DeviceName (DeviceDescr)" form to 2 strings "DeviceName" & "DeviceDescr" 
// may be "Device (Descr)", "Device", "default", "" or nullptr
//-----------------------------------------------
static bool get_default_capture_device(rtl::String& name, rtl::String& descr)
{
	name = "default";
	descr.setEmpty();
	return true;
}
static bool get_default_playback_device(rtl::String& name, rtl::String& descr)
{
	name = "default";
	descr.setEmpty();
	return true;
}
static bool parse_capture_device_name(const char* device_name, rtl::String& name, rtl::String& descr)
{
	rtl::String devname = device_name;
	devname.trim();
	if (devname == nullptr || devname[0] == 0 || std_stricmp(devname, "default") == 0)
		return get_default_capture_device(name, descr);

	const char* op = strchr(devname, '(');

	if (op == nullptr)
	{
		descr.setEmpty();
		name = devname;
		return true;
	}

	return true;
}
static bool parse_playback_device_name(const char* device_name, rtl::String& name, rtl::String& descr);
#if defined (TARGET_OS_WINDOWS)
//-----------------------------------------------
//
//-----------------------------------------------
void audio_mixer_t::set_output_volume(const char* deviceName, uint16_t volume)
{
	int index = device_out_from_name(deviceName);
	// выставим мастер громкость на максимум
	HMIXER mix = nullptr;
	MIXERLINE WOLine = {0};
	WOLine.cbStruct = sizeof(WOLine);
	MIXERLINECONTROLS WOCtls = {0};
	MIXERCONTROL WOVolCtl = {0};
	WOCtls.cbStruct = sizeof (WOCtls);
	MIXERCONTROLDETAILS WOVolDet ={0};
	MIXERCONTROLDETAILS_UNSIGNED WOVolVal = {0};

	MMRESULT mm = mixerOpen(&mix, index, 0, 0, MIXER_OBJECTF_WAVEOUT);
	
	if (mm == MMSYSERR_NOERROR)
	{
		WOLine.dwComponentType = MIXERLINE_COMPONENTTYPE_SRC_PCSPEAKER;
		if ((mm = mixerGetLineInfo(HMIXEROBJ(mix), &WOLine, MIXER_GETLINECONTROLSF_ALL)) != MMSYSERR_NOERROR)
		{
			LOG_ERROR("aux-mix", "set output volume failed : mixerGetLineInfo() returns %u.", mm);
			goto TheEnd;
		}

		WOCtls.dwLineID = WOLine.dwLineID;
		WOCtls.dwControlType = MIXERCONTROL_CONTROLTYPE_VOLUME;
		WOCtls.cControls = 1;
		WOCtls.cbmxctrl = sizeof (WOVolCtl);
		WOCtls.pamxctrl = &WOVolCtl;

		if ((mm = mixerGetLineControls(HMIXEROBJ(mix), &WOCtls, MIXER_GETLINECONTROLSF_ONEBYTYPE)) != MMSYSERR_NOERROR)
		{
			LOG_ERROR("aux-mix", "set output volume failed : mixerGetLineControls() returns %u.", mm);
			goto TheEnd;
		}
		WOVolVal.dwValue = volume;
		WOVolDet.cbStruct = sizeof (WOVolDet);
		WOVolDet.dwControlID = WOVolCtl.dwControlID;
		WOVolDet.cChannels = 1;
		WOVolDet.cMultipleItems = 0;
		WOVolDet.cbDetails = sizeof (WOVolVal);
		WOVolDet.paDetails = &WOVolVal;

		if ((mm = mixerSetControlDetails(HMIXEROBJ(mix), &WOVolDet, MIXER_GETCONTROLDETAILSF_VALUE)) != MMSYSERR_NOERROR)
		{
			LOG_ERROR("aux-mix", "set output volume failed : mixerSetControlDetails() returns %u.", mm);
		}
TheEnd:
   		mixerClose(mix);
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
uint16_t audio_mixer_t::get_output_volume(const char* deviceName)
{
	int index = device_out_from_name(deviceName);
	uint16_t volume = 0;
	// выставим мастер громкость на максимум
	HMIXER mix = nullptr;
	MIXERLINE WOLine = {0};
	WOLine.cbStruct = sizeof(WOLine);
	MIXERLINECONTROLS WOCtls = {0};
	MIXERCONTROL WOVolCtl = {0};
	WOCtls.cbStruct = sizeof (WOCtls);
	MIXERCONTROLDETAILS WOVolDet ={0};
	MIXERCONTROLDETAILS_UNSIGNED WOVolVal = {0};

	MMRESULT mm = mixerOpen(&mix, index, 0, 0, MIXER_OBJECTF_WAVEOUT);
	
	if (mm == MMSYSERR_NOERROR)
	{
		WOLine.dwComponentType = MIXERLINE_COMPONENTTYPE_SRC_PCSPEAKER;
		if ((mm = mixerGetLineInfo(HMIXEROBJ(mix), &WOLine, MIXER_GETLINECONTROLSF_ALL)) != MMSYSERR_NOERROR)
		{
			LOG_ERROR("aux-mix", "get output volume failed : mixerGetLineInfo() returns %u.", mm);
			goto TheEnd;
		}

		WOCtls.dwLineID = WOLine.dwLineID;
		WOCtls.dwControlType = MIXERCONTROL_CONTROLTYPE_VOLUME;
		WOCtls.cControls = 1;
		WOCtls.cbmxctrl = sizeof (WOVolCtl);
		WOCtls.pamxctrl = &WOVolCtl;

		if ((mm = mixerGetLineControls(HMIXEROBJ(mix), &WOCtls, MIXER_GETLINECONTROLSF_ONEBYTYPE)) != MMSYSERR_NOERROR)
		{
			LOG_ERROR("aux-mix", "get output volume failed : mixerGetLineControls() returns %u.", mm);
			goto TheEnd;
		}
		
		WOVolVal.dwValue = 0;
		WOVolDet.cbStruct = sizeof (WOVolDet);
		WOVolDet.dwControlID = WOVolCtl.dwControlID;
		WOVolDet.cChannels = 1;
		WOVolDet.cMultipleItems = 0;
		WOVolDet.cbDetails = sizeof (WOVolVal);
		WOVolDet.paDetails = &WOVolVal;

		if ((mm = mixerGetControlDetails((HMIXEROBJ)mix, &WOVolDet, MIXER_GETCONTROLDETAILSF_VALUE)) != MMSYSERR_NOERROR)
		{
			LOG_ERROR("aux-mix", "get output volume failed : mixerGetControlDetails() returns %u.", mm);
			goto TheEnd;
		}

		volume = (uint16_t)WOVolVal.dwValue;

TheEnd:
   		mixerClose(mix);
	}
	/*
MMRESULT mixerGetControlDetails(
  HMIXEROBJ hmxobj,
  LPMIXERCONTROLDETAILS pmxcd,
  DWORD fdwDetails
);*/
	return volume;
}
//-----------------------------------------------
//
//-----------------------------------------------
void audio_mixer_t::set_input_volume(const char* deviceName, uint16_t volume)
{
	int index = device_in_from_name(deviceName);
	// выставим мастер громкость на максимум
	HMIXER mix = nullptr;
	MIXERLINE WOLine = {0};
	WOLine.cbStruct = sizeof(WOLine);
	MIXERLINECONTROLS WOCtls = {0};
	MIXERCONTROL WOVolCtl = {0};
	WOCtls.cbStruct = sizeof (WOCtls);
	MIXERCONTROLDETAILS WOVolDet ={0};
	MIXERCONTROLDETAILS_UNSIGNED WOVolVal = {0};

	MMRESULT mm = mixerOpen(&mix, index, 0, 0, MIXER_OBJECTF_WAVEIN);
	
	if (mm == MMSYSERR_NOERROR)
	{
		WOLine.dwComponentType = MIXERLINE_COMPONENTTYPE_SRC_MICROPHONE;
		if ((mm = mixerGetLineInfo(HMIXEROBJ(mix), &WOLine, MIXER_GETLINECONTROLSF_ALL)) != MMSYSERR_NOERROR)
		{
			LOG_ERROR("aux-mix", "set input volume failed : mixerGetLineInfo() returns %u.", mm);
			goto TheEnd;
		}

		WOCtls.dwLineID = WOLine.dwLineID;
		WOCtls.dwControlType = MIXERCONTROL_CONTROLTYPE_VOLUME;
		WOCtls.cControls = 1;
		WOCtls.cbmxctrl = sizeof (WOVolCtl);
		WOCtls.pamxctrl = &WOVolCtl;

		if ((mm = mixerGetLineControls(HMIXEROBJ(mix), &WOCtls, MIXER_GETLINECONTROLSF_ONEBYTYPE)) != MMSYSERR_NOERROR)
		{
			LOG_ERROR("aux-mix", "set input volume failed : mixerGetLineControls() returns %u.", mm);
			goto TheEnd;
		}

		WOVolVal.dwValue = volume;
		WOVolDet.cbStruct = sizeof (WOVolDet);
		WOVolDet.dwControlID = WOVolCtl.dwControlID;
		WOVolDet.cChannels = 1;
		WOVolDet.cMultipleItems = 0;
		WOVolDet.cbDetails = sizeof (WOVolVal);
		WOVolDet.paDetails = &WOVolVal;

		if ((mm = mixerSetControlDetails(HMIXEROBJ(mix), &WOVolDet, MIXER_GETCONTROLDETAILSF_VALUE)) != MMSYSERR_NOERROR)
		{
			LOG_ERROR("aux-mix", "set input volume failed : mixerSetControlDetails() returns %u.", mm);
		}
TheEnd:
   		mixerClose(mix);
	}
}
//-----------------------------------------------
//
//-----------------------------------------------
uint16_t audio_mixer_t::get_input_volume(const char* deviceName)
{
	int index = device_in_from_name(deviceName);
	// выставим мастер громкость на максимум
	uint16_t volume = 0;
	HMIXER mix = nullptr;
	MIXERLINE WOLine = {0};
	WOLine.cbStruct = sizeof(WOLine);
	MIXERLINECONTROLS WOCtls = {0};
	MIXERCONTROL WOVolCtl = {0};
	WOCtls.cbStruct = sizeof (WOCtls);
	MIXERCONTROLDETAILS WOVolDet ={0};
	MIXERCONTROLDETAILS_UNSIGNED WOVolVal = {0};

	MMRESULT mm = mixerOpen(&mix, index, 0, 0, MIXER_OBJECTF_WAVEIN);
	
	if (mm == MMSYSERR_NOERROR)
	{
		WOLine.dwComponentType = MIXERLINE_COMPONENTTYPE_SRC_MICROPHONE;
		if ((mm = mixerGetLineInfo(HMIXEROBJ(mix), &WOLine, MIXER_GETLINECONTROLSF_ALL)) != MMSYSERR_NOERROR)
		{
			LOG_ERROR("aux-mix", "get input volume failed : mixerGetLineInfo() returns %u.", mm);
			goto TheEnd;
		}

		WOCtls.dwLineID = WOLine.dwLineID;
		WOCtls.dwControlType = MIXERCONTROL_CONTROLTYPE_VOLUME;
		WOCtls.cControls = 1;
		WOCtls.cbmxctrl = sizeof (WOVolCtl);
		WOCtls.pamxctrl = &WOVolCtl;

		if ((mm = mixerGetLineControls(HMIXEROBJ(mix), &WOCtls, MIXER_GETLINECONTROLSF_ONEBYTYPE)) != MMSYSERR_NOERROR)
		{
			LOG_ERROR("aux-mix", "get input volume failed : mixerGetLineControls() returns %u.", mm);
			goto TheEnd;
		}

		WOVolVal.dwValue = volume;
		WOVolDet.cbStruct = sizeof (WOVolDet);
		WOVolDet.dwControlID = WOVolCtl.dwControlID;
		WOVolDet.cChannels = 1;
		WOVolDet.cMultipleItems = 0;
		WOVolDet.cbDetails = sizeof (WOVolVal);
		WOVolDet.paDetails = &WOVolVal;

		if ((mm = mixerGetControlDetails(HMIXEROBJ(mix), &WOVolDet, MIXER_GETCONTROLDETAILSF_VALUE)) != MMSYSERR_NOERROR)
		{
			LOG_ERROR("aux-mix", "get input volume failed : mixerGetControlDetails() returns %u.", mm);
		}

		volume = (uint16_t)WOVolVal.dwValue;
TheEnd:
   		mixerClose(mix);
	}
	
	return volume;
}
//-----------------------------------------------
//
//-----------------------------------------------
int32_t audio_mixer_t::device_in_from_name(const char* device_name)
{
	int wic_count = waveOutGetNumDevs();

	if (device_name == nullptr || device_name[0] == 0)
		return wic_count == 0 ? -1 : 0;

	WAVEINCAPSA wic;

	for (int i = 0; i < wic_count; i++)
	{
		memset(&wic, 0, sizeof wic);
		
		if (waveInGetDevCapsA(i, &wic, sizeof wic) == MMSYSERR_NOERROR)
		{
			wic.szPname[MAXPNAMELEN-1] = 0;
			rtl::String str(wic.szPname);
			str.trim(" ");
			if (_stricmp(str, device_name) == 0)
			{
				return i;
			}
		}
	}

	return -1;
}
int32_t audio_mixer_t::device_out_from_name(const char* deviceName)
{
	int woc_count = waveOutGetNumDevs();

	if (deviceName == nullptr || deviceName[0] == 0)
		return woc_count == 0 ? -1 : 0;

	WAVEOUTCAPSA woc;

	for (int i = 0; i < woc_count; i++)
	{
		memset(&woc, 0, sizeof woc);
		
		if (waveOutGetDevCapsA(i, &woc, sizeof woc) == MMSYSERR_NOERROR)
		{
			woc.szPname[MAXPNAMELEN-1] = 0;
			rtl::String str(woc.szPname);
			str.trim(" ");
			if (std_stricmp(str, deviceName) == 0)
			{
				return i;
			}
		}
	}

	return -1;	
}
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
void set_output_volume(const char* device_name, uint16_t volume)
{
	return;
}
uint16_t audio_mixer_t::get_output_volume(const char* device_name)
{
	return 0;
}
void audio_mixer_t::set_input_volume(const char* device_name, uint16_t volume)
{
	return;
}
uint16_t audio_mixer_t::get_input_volume(const char* device_name)
{
	return 0;
}
rtl::String audio_mixer_t::device_in_from_name(const char* device_name)
{
	return rtl::String("default");
}
rtl::String audio_mixer_t::device_out_from_name(const char* device_name)
{
	return rtl::String("default");
}
#endif // TARGET_OS_xxx

#define _USE_MATH_DEFINES
#include <math.h>
//-----------------------------------------------
//
//-----------------------------------------------
#define SAMPLE_RATE 8000 // 8000, 11025, 22050, or 44100  
//-----------------------------------------------
//
//-----------------------------------------------
static inline int math_round(double d)
{
	return int(d >= 0.0 ? d+0.5 : d-0.5);
}
//-----------------------------------------------
//
//-----------------------------------------------
bool audio_mixer_t::generate_std_signal(int freq1, int freq2, int duration, short* buffer, int size, uint16_t volume)
{
	int i, samples;
	double w1 = 2 * M_PI * double(freq1); // omega ( 2 * pi * frequency)
	double w2 = 2 * M_PI * double(freq2);

	// Calculate samples count
	samples = duration * SAMPLE_RATE / 1000;
	
	if (samples > size)
		return false;

	// calculate and write out the tone signal
    for (i = 0; i < samples; i++)
	{
		int ival = math_round(volume * (sin(i * w1 / SAMPLE_RATE) + sin(i * w2 / SAMPLE_RATE)));

		if (ival < -32768)
			ival = -32768;
		else if (ival > 32767) 
			ival = 32767;

		buffer[i] = (short)ival;
	}

	return true;
}
//-----------------------------------------------
//
//-----------------------------------------------
#define TWO32 4294967296.0  /* 2^32 */
//-----------------------------------------------
//
//-----------------------------------------------
static double amptab[2] = { 8191.75, 16383.5 };
//-----------------------------------------------
//
//-----------------------------------------------
static inline int phinc(double f)
{ 
	return math_round(TWO32 * f / (double)SAMPLE_RATE);
}
//-----------------------------------------------
//
//-----------------------------------------------
bool audio_mixer_t::generate_std_signal2(int freq1, int freq2, int duration, short* buffer, int size, uint16_t volume)
{
	int ak = 0;
	double amp = amptab[ak];
	int phinc1 = phinc(freq1);
	int phinc2 = phinc(freq2);

	int ns = duration * (SAMPLE_RATE/1000);
	uint32_t ptr1 = 0, ptr2 = 0;

	for (int n = 0; n < ns; n++)
	{
		double val = amp * (sin((double)ptr1) + sin((double)ptr2));

		int ival = math_round(val);
		if (ival < -32768)
			ival = -32768;
		else if (val > 32767) 
			ival = 32767;

		buffer[n] = (short)ival;

		ptr1 += phinc1; 
		ptr2 += phinc2;
	}

	return true;
}

//--------------------------------------
