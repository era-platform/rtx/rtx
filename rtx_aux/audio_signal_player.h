﻿/* RTX H.248 Media Gate. Audio Driver Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "audio_player.h"
#include "audio_reader.h"
//--------------------------------------
//
//--------------------------------------
#define STD_SIGNAL_COUNT		22
#define STD_DTMF_COUNT			16
//--------------------------------------
// представление файла в PCM
//--------------------------------------
struct signal_samples_t
{
	short* samples;
	int count;
};
//--------------------------------------
//
//--------------------------------------
class audio_signal_player_t
{
	static bool s_initialized;
	static signal_samples_t s_signals[STD_SIGNAL_COUNT];	// аудио данные для стандартных сигналов

	audio_data_reader_wave_buffer_t m_reader;
	audio_player_wave_t m_player;
	PhoneTones m_current_tone;

public:
	audio_signal_player_t();
	~audio_signal_player_t();

	bool is_playing() { return m_player.is_playing(); }

	/// продолжительность сигнала мин 100 мс, -1 - в цикле
	bool start_playing(const char* device_name, PhoneTones tone, int duration = 240);
	void stop_playing();

	static bool generate_std_signals(uint16_t volume);
	static void cleanup_std_tomes();
};
