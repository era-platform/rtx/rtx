﻿/* RTX H.248 Media Gate. Audio Driver Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"
#include "audio_player.h"
#include "audio_mixer.h"
//#include <math.h>
//--------------------------------------
//
//--------------------------------------
#if defined (TARGET_OS_WINDOWS)
static WAVEFORMATEX format_PCM = { WAVE_FORMAT_PCM,		1, 8000, 16000, 2, 16,  0 };
//--------------------------------------
//
//--------------------------------------
audio_player_wave_t::audio_player_wave_t(bool stop_thread_after_device, const char* name, int frame_size) :
	m_lock("wav-play-lock"), m_wave_index(0), m_wave(nullptr),
	m_stop_flag(false), m_frame_size(frame_size), m_loop(false), m_file_done(nullptr), m_reader(nullptr),
	m_stop_thread_after_device(stop_thread_after_device), m_pos_changed(nullptr)
{
	memset(m_header, 0, sizeof(m_header));
	m_data = (uint8_t*)MALLOC(m_frame_size * 2);
	if (name != nullptr && name[0] != 0)
	{
		strncpy(m_name, name, 63);
		m_name[63] = 0;
	}
	else
	{
		strcpy(m_name, "wave-player");
	}
}
//--------------------------------------
//
//--------------------------------------
audio_player_wave_t::~audio_player_wave_t()
{
	stop_play();

	FREE(m_data);
	m_data = nullptr;
}
//--------------------------------------
//
//--------------------------------------
void audio_player_wave_t::set_output(const char* deviceName)
{
	MMRESULT mm;

	LOG_CALL("wav-play", "player %S : selecting device %s...", m_name, deviceName);

	rtl::MutexWatchLock lock(m_lock, __FUNCSIG__);

	if (m_wave == nullptr || deviceName == nullptr)
		return;

	close();

	if (!open(deviceName) || m_reader == nullptr)
	{
		LOG_CALL("wav-play", "player %S : selecting device : open device 0 or reader(%p) is nullptr", m_name, m_reader);
		return;
	}

	// Read the waveform-audio data subchunk. 
	if ((m_header[0].dwBufferLength = m_reader->read_next_block((short*)m_data)) < 1)
	{
		LOG_CALL("wav-play", "player %S : selecting device : buffer length(%d) < 1 ", m_name, m_header[0].dwBufferLength);
		return;
	}

	if ((mm = waveOutWrite(m_wave, &m_header[0], sizeof(WAVEHDR))) != MMSYSERR_NOERROR)
	{
		LOG_ERROR("wav-play", "player %S : selecting device : failed. waveOutWrite() returns %u.", m_name, mm);
		return;
	}

	// Read the waveform-audio data subchunk. 
	if ((m_header[1].dwBufferLength = m_reader->read_next_block((short*)(m_data + m_frame_size))) > 0)
	{
		if((mm = waveOutWrite(m_wave, &m_header[1], sizeof(WAVEHDR))) != MMSYSERR_NOERROR)
		{
			LOG_ERROR("wav-play", "player %S : selecting device : failed. waveOutWrite() returns %u.", m_name, mm);
		}
	}

	LOG_CALL("wav-play", "player %S : device selected", m_name);
}
//--------------------------------------
//
//--------------------------------------
bool audio_player_wave_t::start_play(const char* deviceName, audio_data_reader_t* reader, bool loop, PFN_FILEDONE done_callback, PFN_FRAME_DONE pos_callback)
{
	MMRESULT mm;
	
	LOG_CALL("wav-play", "player %S : start playing...", m_name);

	rtl::MutexWatchLock lock(m_lock, __FUNCSIG__);

	stop_play();

	m_loop = loop;
	m_file_done = done_callback;
	m_pos_changed = pos_callback;
	m_reader = reader;

	if (!open(deviceName))
	{
		LOG_ERROR("wav-play", "player %S : play failed : open device", m_name);
		return false;
	}

	// Read the waveform-audio data subchunk.
	if ((m_header[0].dwBufferLength = m_reader->read_next_block((short*)m_data)) == 0)
	{
		LOG_ERROR("wav-play", "player %S : play failed : no data", m_name);
		return false;
	}

	if ((mm = waveOutWrite(m_wave, &m_header[0], sizeof(WAVEHDR))) != MMSYSERR_NOERROR)
	{
		LOG_ERROR("wav-play", "player %S : play failed : waveOutWrite(0) returns %u.", m_name, mm);
		return false;
	}

	// Read the waveform-audio data subchunk. 
	if ((m_header[1].dwBufferLength = m_reader->read_next_block((short*)(m_data + m_frame_size))) > 0)
	{
		if((mm = waveOutWrite(m_wave, &m_header[1], sizeof(WAVEHDR))) != MMSYSERR_NOERROR)
		{
			LOG_ERROR("wav-play", "player %S : play failed : waveOutWrite(1) returns %u.", m_name, mm);
			return false;
		}
	}

	LOG_CALL("wav-play", "player %S : playing started", m_name);

	return true;
}
//--------------------------------------
//
//--------------------------------------
void audio_player_wave_t::stop_play()
{
	rtl::MutexWatchLock lock(m_lock, __FUNCSIG__);

	if (!m_thread.isStarted())
		return;

	LOG_CALL("wav-play", "player %S : stopping...", m_name);

	close();

	m_file_done = nullptr;
	m_pos_changed = nullptr;
	m_loop = false;
	m_reader = nullptr;

	LOG_CALL("wav-play", "player %S : stopped", m_name);
}
//--------------------------------------
//
//--------------------------------------
bool audio_player_wave_t::open(const char* waveOutName)
{
	MMRESULT mm;

	LOG_CALL("wav-play", "player %S : opening device %s...", m_name, waveOutName);

	if (waveOutName == nullptr || waveOutName[0] == 0)
	{
		m_wave_index = 0;
	}
	else
	{
		m_wave_index = audio_mixer_t::device_out_from_name(waveOutName);
	}
	
	if (m_wave_index == -1)
	{
		// ...
		m_wave_index = 0;
	}

	// событие от запускаемого потока о его готовности к работе
	if (!m_thread_ready_event.create(true, false))
	{
		LOG_ERROR("wav-play", "player %S : opening device failed : CreateEvent() returned %s", m_name, GetLastError());
		return false;
	}

	m_stop_flag = false;

	if (!m_thread.start(this))
	{
		LOG_ERROR("wav-play", "player %S : opening device failed : _beginthreadex() returns %u", m_name, GetLastError());
		return false;
	}

	// ждем когда поток будет готов.
	if (!m_thread_ready_event.wait(INFINITE))
	{
		LOG_ERROR("wav-play", "player %S : opening device failed : WaitForSingleObject() returned %s", m_name, GetLastError());
		return false;
	}

	int rate = m_reader->get_format_rate();
	int channels = m_reader->get_format_channels();

	WAVEFORMATEX format = 
	{
		(WORD)WAVE_FORMAT_PCM,
		(WORD)channels,
		(DWORD)rate,
		(DWORD)(rate * channels * 2),
		(WORD)(channels * 2),
		(WORD)16,
		(WORD)0
	};

	if ((mm = waveOutOpen(&m_wave, m_wave_index, &format, m_thread.get_id(), 0, CALLBACK_THREAD)) != 0)
	{
		LOG_ERROR("wav-play", "player %S : opening device failed : waveOutOpen() returns %u.", m_name, mm);
		return false;
	}

	m_header[0].lpData = (LPSTR)m_data;
	m_header[0].dwFlags = 0;
	m_header[0].dwBufferLength = m_frame_size;

	if ((mm = waveOutPrepareHeader(m_wave, &m_header[0], sizeof(WAVEHDR))) != 0)
	{
		LOG_ERROR("wav-play", "player %S : opening device failed : waveOutPrepareHeader() returns %u.", m_name, mm);
		return false;
	}

	m_header[1].lpData = (LPSTR)(m_data + m_frame_size);
	m_header[1].dwFlags = 0;
	m_header[1].dwBufferLength = m_frame_size;

	if ((mm = waveOutPrepareHeader(m_wave, &m_header[1], sizeof(WAVEHDR))) != 0)
	{
		LOG_ERROR("wav-play", "player %S : opening device failed : waveOutPrepareHeader() returns %u.", m_name, mm);
		return false;
	}

	LOG_CALL("wav-play", "player %S : device opened", m_name);

	return true;
}
//--------------------------------------
//
//--------------------------------------
void audio_player_wave_t::close()
{
	if (!m_thread_ready_event.is_valid())
		return;

	LOG_CALL("wav-play", "player %S : closing device", m_name);

	// для остановки потока ставим флаг
	m_stop_flag = true;

	if (m_stop_thread_after_device)
	{
		if (m_wave != nullptr)
		{
			LOG_CALL("wav-play", "player %S : closing device : waveOutReset...", m_name);
			waveOutReset(m_wave);
			LOG_CALL("wav-play", "player %S : closing device : waveOutUnprepareHeader 0 ...", m_name);
			waveOutUnprepareHeader(m_wave, &m_header[0], sizeof(WAVEHDR));
			LOG_CALL("wav-play", "player %S : closing device : waveOutUnprepareHeader 1 ...", m_name);
			waveOutUnprepareHeader(m_wave, &m_header[1], sizeof(WAVEHDR));
			LOG_CALL("wav-play", "player %S : closing device : waveOutClose ...", m_name);
			waveOutClose(m_wave);
			m_wave = nullptr;
		}

		if (m_thread.isStarted())
		{
			LOG_CALL("wav-play", "player %S : closing device : waiting stop thread ...", m_name);
			m_thread.wait();
		}
	}
	else
	{
		// остановим поток
		if (m_thread.isStarted())
		{
			LOG_CALL("wav-play", "player %S : closing device : waiting stop thread ...", m_name);
			m_thread.wait();
		}

		// для висты - сначало остановим поток потом устройство!
		if (m_wave != nullptr)
		{
			LOG_CALL("wav-play", "player %S : closing device : waveOutReset ...", m_name);
			waveOutReset(m_wave);
			LOG_CALL("wav-play", "player %S : closing device : waveOutUnprepareHeader 0 ...", m_name);
			waveOutUnprepareHeader(m_wave, &m_header[0], sizeof(WAVEHDR));
			LOG_CALL("wav-play", "player %S : closing device : waveOutUnprepareHeader 1 ...", m_name);
			waveOutUnprepareHeader(m_wave, &m_header[1], sizeof(WAVEHDR));
			LOG_CALL("wav-play", "player %S : closing device : waveOutClose ...", m_name);
			waveOutClose(m_wave);
			m_wave = nullptr;
		}
	}

	m_thread_ready_event.close();

	LOG_CALL("wav-play", "player %S : device closed", m_name);
}
//--------------------------------------
// получение и проигрывание следующего блока данных
//--------------------------------------
bool audio_player_wave_t::process_play_done(WAVEHDR* waveHeader)
{
	PFN_FRAME_DONE pos_callback = m_pos_changed;

	if (pos_callback != nullptr)
	{
		// размер фрейма продолжительностью в 1 секунду
		unsigned sec_size = m_reader->get_format_rate() * 2/*байта*/ * m_reader->get_format_channels();
		// продолжительность фрейма (в мс) размером в m_frame_size
		double frame_size = 1000.0 * m_frame_size / sec_size;

		try
		{
			pos_callback(frame_size);
		}
		catch (rtl::SEH_Exception ex)
		{
			LOG_ERROR("wav-play", "EXCEPTION in OnPlayDone():\n%s", ex.get_message());
		}
	}

	// Read the waveform-audio data subchunk. 
	bool		res = true;
	uint32_t	oppositePlaying = false;

	if (m_stop_flag)
	{
		return false;
	}

	// заполняем первый буфер
	if (waveHeader == &m_header[0])
	{
		oppositePlaying = (m_header[1].dwFlags & WHDR_INQUEUE) != 0;
	}
	else if (waveHeader == &m_header[1])	// второй буфер
	{
		oppositePlaying = (m_header[0].dwFlags & WHDR_INQUEUE) != 0;
	}

	// читаем след. порцию из файла
	if (waveHeader->dwBufferLength = m_reader->read_next_block((short*)waveHeader->lpData) > 0)
	{
		// отправим данные в драйвер
		waveOutWrite(m_wave, waveHeader, sizeof(WAVEHDR));
	}
	else
	{
		// файл закончен
		if (m_loop && !m_stop_flag)
		{
			waveHeader->dwBufferLength = m_reader->read_first_block((short*)waveHeader->lpData);
			// отправим данные в драйвер
			waveOutWrite(m_wave, waveHeader, sizeof(WAVEHDR));
		}
		else if (!oppositePlaying || m_stop_flag)
		{
			// здесь надо прерывать?
			PFN_FILEDONE done_callback = m_file_done;
			if (done_callback != nullptr)
            {
				done_callback();
			}

			res = false;
		}
	}

	return res;
}
//--------------------------------------
//
//--------------------------------------
void audio_player_wave_t::process_play()
{
	LOG_CALL("wav-play", "player %S : processor thread started", m_name);

	try
	{
		MSG msg;
		PeekMessage(&msg, nullptr, 0, 0, PM_NOREMOVE);

		// уведомляем поток, который сейчас сидит в open(), что мы стартанули и готовы к работе.
		if (!m_thread_ready_event.is_valid())
		{
			LOG_WARN("wav-play", "player %S : SetEvent() returned %u", m_name, GetLastError());
		}
		else
		{
			m_thread_ready_event.signal();
		}

		while (GetMessage(&msg, nullptr, 0, 0))
		{
			// если надо выгружатся безусловно
			if (m_stop_flag)
			{
				LOG_CALL("wav-play", "player %S : processor thread stoped by StopFlag.", m_name);
				return;
			}
			// проверим сообщение
			switch (msg.message)
			{
			case MM_WOM_OPEN:
				break;
			case MM_WOM_CLOSE:
				break;
			case MM_WOM_DONE:
				if (!process_play_done((WAVEHDR*)msg.lParam))
				{
					LOG_CALL("wav-play", "player %S : processor thread stoped by ending data.", m_name);
					return;
				}
				break;
			case WM_CMD_SHUTDOWN:
				LOG_CALL("wav-play", "player %S : processor thread stoped by WM_CMD_SHUTDOWN.", m_name);
				return;
			}
		}
		//
		LOG_CALL("wav-play", "player %S : processor thread stopped by WM_QUIT.", m_name);
	}
	catch (rtl::Exception& ex)
	{
		LOG_ERROR("wav-play", "player %S : processor thread failed with exception :\n%s", m_name, ex.get_message());
		ex.raise_notify(__FILE__, __FUNCSIG__);
	}

	LOG_CALL("wav-play", "player %S : processor thread exit", m_name);
}
//--------------------------------------
//
//--------------------------------------
void audio_player_wave_t::thread_run(rtl::Thread* thread)
{
	audio_player_wave_t* player = (audio_player_wave_t*)thread;
	rtl::SEH_Exception::start_handling();
	SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_TIME_CRITICAL);

	player->process_play();

	SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_NORMAL);
	rtl::SEH_Exception::stop_handling();
	_endthreadex(0);
}
#endif // TARGET_OS_WINDOWS
//--------------------------------------
