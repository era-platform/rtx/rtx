﻿/* RTX H.248 Media Gate. Audio Driver Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "audio_device.h"
#if defined (TARGET_OS_WINDOWS)
#include "audio_device_in.h"
#include "audio_device_out.h"
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
#include "audio_device_in_alsa.h"
#include "audio_device_out_alsa.h"
#endif
//-----------------------------------------------
// получить номер девайса по имени
//-----------------------------------------------
aux_device_t::aux_device_t(const char* name) : m_opened_flag(false), m_started_flag(false),
	m_thread_stop_flag(true), m_clock_rate(8000), m_channels(1), m_block_times(40), m_block_count(2), m_block_size(0)
{
	if (name != nullptr && name[0] != 0)
	{
		STR_COPY(m_tag, name);
	}
	else
	{
		strcpy(m_tag, "AUX");
	}
}
//--------------------------------------
//
//--------------------------------------
aux_device_t::~aux_device_t()
{
}
//--------------------------------------
//
//--------------------------------------
bool aux_device_t::aux_setup_device(int clockRate, int channels, int blockTimes, int blockCount)
{
	rtl::MutexLock lock(m_sync);

	aux_close_device();

	m_clock_rate = clockRate;
	m_channels = channels;

	m_block_times = blockTimes < AUX_MIN_BLOCK_TIMES ?
		AUX_MIN_BLOCK_TIMES :
		blockTimes > AUX_MAX_BLOCK_TIMES ?
			AUX_MAX_BLOCK_TIMES :
			blockTimes;
		
	m_block_count = blockCount < AUX_MIN_BLOCK_COUNT ?
		AUX_MIN_BLOCK_COUNT :
		blockCount > AUX_MAX_BLOCK_COUNT ?
			AUX_MAX_BLOCK_COUNT :
			blockCount;

	// вычислим размер блока
	m_block_size = (m_clock_rate / 1000) * sizeof(short) * m_channels * m_block_times;

	LOG_CALL(m_tag, "setup device : clock-rate:%d channels:%d block-time:%d block-count:%d block-size:%d",
		m_clock_rate, m_channels, m_block_times, m_block_count, m_block_size);

	return true;
}
//--------------------------------------
//
//--------------------------------------
bool aux_device_t::aux_start_thread()
{
	rtl::MutexLock lock(m_sync);

	if (m_thread.isStarted())
	{
		m_thread.stop();
	}
	
	LOG_CALL(m_tag, "start thread : strarting...");

	if (!m_thread_started_event.create(true, false))
	{
		LOG_ERROR(m_tag, "start thread : start-event not created");
		aux_stop_thread();
		return false;
	}

	m_thread.set_priority(rtl::ThreadPriority::Highest);

	if (!m_thread.start(this))
	{
		LOG_ERROR(m_tag, "start thread : thread not started");
		aux_stop_thread();
		return false;
	}

	if (!m_thread_started_event.wait(INFINITE))
	{
		LOG_ERROR(m_tag, "start thread : start-event wait failed");
		aux_stop_thread();
		return false;
	}

	m_thread_started_event.close();

	LOG_CALL(m_tag, "start thread : sucessfully started");

	return true;
}
//--------------------------------------
//
//--------------------------------------
void aux_device_t::aux_stop_thread()
{
	bool wait_thread = false;

	{
		rtl::MutexLock lock(m_sync);

		// дождемся завершения потока
		if (m_thread.isStarted())
		{
			LOG_CALL(m_tag, "stop thread : stopping...");
			// остановим поток
			m_thread_stop_flag = true;
			post_stop_thread();
			wait_thread = true;
		}
	}

	if (wait_thread)
	{
		m_thread.stop();
		LOG_CALL(m_tag, "stop thread : stoped");
	}
}
aux_device_out_t* aux_device_t::create_output(aux_device_out_handler_t* handler)
{
#if defined (TARGET_OS_WINDOWS)
	return NEW aux_device_out_wave_t(handler);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	return NEW aux_device_out_alsa_t(handler);
#else
	return nullptr;
#endif
}
void aux_device_t::free_output(aux_device_out_t* output)
{
#if defined (TARGET_OS_WINDOWS)
	DELETEO((aux_device_out_wave_t*)output);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	DELETEO((aux_device_out_alsa_t*)output);
#endif
}
aux_device_in_t* aux_device_t::create_input(aux_device_in_handler_t* handler)
{
#if defined (TARGET_OS_WINDOWS)
	return NEW aux_device_in_wave_t(handler);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	return NEW aux_device_in_alsa_t(handler);
#else
	return nullptr;
#endif

}
void aux_device_t::free_input(aux_device_in_t* input)
{
#if defined (TARGET_OS_WINDOWS)
	DELETEO((aux_device_in_wave_t*)input);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	DELETEO((aux_device_in_alsa_t*)input);
#endif
}
////////////////////////////////////////
//--------------------------------------
//
//--------------------------------------
aux_device_out_t::~aux_device_out_t()
{
}
//--------------------------------------
//
//--------------------------------------
aux_device_in_t::~aux_device_in_t()
{
}
//--------------------------------------
