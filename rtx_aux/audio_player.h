﻿/* RTX H.248 Media Gate. Audio Driver Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_stdlib.h"
#include "audio_reader.h"
//--------------------------------------
//
//--------------------------------------
#define WAVE_FRAME_SIZE		16000
//--------------------------------------
//
//--------------------------------------
typedef void(AUX_CALL *PFN_FILEDONE)();
typedef void(AUX_CALL *PFN_FRAME_DONE)(double ms);
//--------------------------------------
//
//--------------------------------------
class audio_player_wave_t : rtl::IThreadUser
{
	rtl::MutexWatch m_lock;
	bool m_stop_thread_after_device;
	char m_name[64];
	rtl::Thread m_thread;
	volatile bool m_stop_flag;				// флаг останова проигрывания
	rtl::Event m_thread_ready_event;

#if defined (TARGET_OS_WINDOWS)
	uint32_t m_wave_index;
	HWAVEOUT m_wave;						// дескриптор устройства
	WAVEHDR m_header[2];					// заголовки данных (2)
	int m_frame_size;
	uint8_t* m_data;						// буфера для данных (2)
	bool m_loop;							// флаг циклического проигрывания
	PFN_FILEDONE m_file_done;				// событие окнчания проигрывания
	audio_data_reader_t* m_reader;
	PFN_FRAME_DONE m_pos_changed;			// событие о том, что очередной фрейм проигрался
#endif // TARGET_OS_WINDOWS

public:
	audio_player_wave_t(bool stop_thread_after_device, const char* name, int frame_size = 16000);
	~audio_player_wave_t();

	void set_output(const char* deviceName);
	bool start_play(const char* waveOutName, audio_data_reader_t* reader, bool loop, PFN_FILEDONE done_callback, PFN_FRAME_DONE pos_callback = NULL);
	void stop_play();

#if defined (TARGET_OS_WINDOWS)
	bool is_playing() { return m_wave != nullptr; }
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	bool is_playing() { return false; }
#endif

private:
	bool open(const char* waveOutName);
	void close();
#if defined (TARGET_OS_WINDOWS)
	bool process_play_done(WAVEHDR* header);
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
	bool process_play_done(void* header);
#endif
	void process_play();

private:
	virtual void thread_run(rtl::Thread* thread);
};
//--------------------------------------


