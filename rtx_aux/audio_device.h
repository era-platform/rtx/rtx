﻿/* RTX H.248 Media Gate. Audio Driver Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_aux.h"
//-----------------------------------------------
// константы 
//-----------------------------------------------
#define AUX_MAX_BLOCK_COUNT			8
#define AUX_MAX_BLOCK_TIMES			40
#define AUX_MIN_BLOCK_COUNT			2
#define AUX_MIN_BLOCK_TIMES			10
//-----------------------------------------------
// новый интефейс для аудио
// интерфейс обратного вызова выхода
//-----------------------------------------------
struct aux_device_out_handler_t
{
	virtual int aux_out_get_data(short* buffer, int size) = 0;
};
//-----------------------------------------------
// интерфейс обратного вызова входа
//-----------------------------------------------
struct aux_device_in_handler_t
{
	virtual void aux_in_data_ready(const short* samples, int length) = 0;
};
//-----------------------------------------------
// декларация устройств ввода/вывода
//-----------------------------------------------
class aux_device_out_t;
class aux_device_in_t;
//-----------------------------------------------
// аудио устройство
//-----------------------------------------------
class aux_device_t : public rtl::IThreadUser
{
protected:
	rtl::Mutex m_sync;

	volatile bool m_opened_flag;			// флаг успешного открытия
	volatile bool m_started_flag;			// флаг успешного старта

	rtl::Thread m_thread;						// описатель потока обработки
	rtl::Event m_thread_started_event;			// событие синхронизации старта потока
	volatile bool m_thread_stop_flag;		// флаг для останова потока

	rtl::String m_device_name;					// имя устройства

	// буфер воспроизведения
	int m_clock_rate;						// частота воспроизведения
	int m_channels;							// количество каналов
	int m_block_times;						// размер блока в миллисекундах
	int m_block_count;						// кол-во описателей в работе
	int m_block_size;						// размер блока в байтах

	char m_tag[16];							// имя для логирования

public:
	aux_device_t(const char* name);
	virtual ~aux_device_t();

	AUX_API bool aux_setup_device(int clockRate, int channels, int blockTimes, int blockCount);

	virtual bool aux_open_device(const char* deviceName) = 0;
	virtual void aux_close_device() = 0;
	virtual bool aux_start_device() = 0;
	virtual void aux_stop_device() = 0;
	virtual void aux_set_paused(bool pause) = 0;

	bool aux_is_opened() { return m_opened_flag; }
	const char* aux_get_device_name() { return m_device_name; }
	int aux_get_block_size() { return m_block_size; }
	int aux_get_block_count() { return m_block_count; }

	AUX_API static aux_device_out_t* create_output(aux_device_out_handler_t* handler);
	AUX_API static void free_output(aux_device_out_t* output);
	AUX_API static aux_device_in_t* create_input(aux_device_in_handler_t* handler);
	AUX_API static void free_input(aux_device_in_t* input);

protected:
	bool aux_start_thread();
	void aux_stop_thread();

	virtual void post_stop_thread() = 0;
	virtual void thread_run(rtl::Thread* thread) = 0;
};
//-----------------------------------------------
// аудио выход
//-----------------------------------------------
class aux_device_out_t : public aux_device_t
{
protected:
	aux_device_out_handler_t* m_handler;			// источние данных

public:
	aux_device_out_t(const char* name, aux_device_out_handler_t* handler) : aux_device_t(name), m_handler(handler) { }
	virtual ~aux_device_out_t();
};
//-----------------------------------------------
// аудио вход
//-----------------------------------------------
class aux_device_in_t : public aux_device_t
{
protected:
	aux_device_in_handler_t* m_handler;			// получатель данных
	volatile bool m_paused_flag;

public:
	aux_device_in_t(const char* name, aux_device_in_handler_t* handler) : aux_device_t(name), m_handler(handler) { }
	virtual ~aux_device_in_t();

	bool is_paused() { return m_paused_flag; }
};
//-----------------------------------------------
