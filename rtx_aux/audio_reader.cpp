﻿/* RTX H.248 Media Gate. Audio Driver Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "audio_reader.h"
//--------------------------------------
//
//--------------------------------------
//static WAVFormat format_PCM = { WAVFormatTagPCM, 1, 8000, 16000, 2, 16, 0 };
//--------------------------------------
//
//--------------------------------------
audio_data_reader_wave_file_t::~audio_data_reader_wave_file_t()
{
	close();
}
//--------------------------------------
//
//--------------------------------------
bool audio_data_reader_wave_file_t::open(const char* file_pame)
{
	close();

	return m_file.open(file_pame) && prepare();
}
//--------------------------------------
//
//--------------------------------------
bool audio_data_reader_wave_file_t::prepare()
{
	return true;
}
//--------------------------------------
//
//--------------------------------------
void audio_data_reader_wave_file_t::close()
{
	m_file.close();
}

void audio_data_reader_wave_file_t::decode()
{
}
//--------------------------------------
//
//--------------------------------------
void audio_data_reader_wave_file_t::reset()
{
	m_file.reset();
}
//--------------------------------------
//
//--------------------------------------
uint32_t audio_data_reader_wave_file_t::read_next_block(void* buffer)
{
	return m_file.readAudioData((uint8_t*)buffer, WAVE_FRAME_SIZE);
}
//--------------------------------------
//
//--------------------------------------
uint32_t audio_data_reader_wave_file_t::read_first_block(void* buffer)
{
	m_file.reset();
	return m_file.readAudioData((uint8_t*)buffer, WAVE_FRAME_SIZE);
}
//--------------------------------------
//
//--------------------------------------
audio_data_reader_wave_buffer_t::audio_data_reader_wave_buffer_t() : m_buffer(nullptr), m_buffer_size(0), m_pointer(0)
{

}
//--------------------------------------
//
//--------------------------------------
audio_data_reader_wave_buffer_t::~audio_data_reader_wave_buffer_t()
{
	close();
}
//--------------------------------------
//
//--------------------------------------
bool audio_data_reader_wave_buffer_t::open(void* data, int size)
{
	close();

	m_buffer = (uint8_t*)data;
	m_buffer_size = size;
	m_pointer = 0;

	return true;
}
//--------------------------------------
//
//--------------------------------------
void audio_data_reader_wave_buffer_t::close()
{
	m_buffer = nullptr;
	m_buffer_size = 0;
	m_pointer = 0;
}
//--------------------------------------
//
//--------------------------------------
void audio_data_reader_wave_buffer_t::reset()
{
	m_pointer = 0;
}
//--------------------------------------
//
//--------------------------------------
uint32_t audio_data_reader_wave_buffer_t::read_next_block(void* buffer)
{
	uint32_t len = 0;

	if (m_pointer < m_buffer_size)
	{
		uint32_t size = m_buffer_size - m_pointer;
		len = size < WAVE_FRAME_SIZE ? size : WAVE_FRAME_SIZE;
		memcpy(buffer, m_buffer + m_pointer, len);
		m_pointer += len;

		if (len < WAVE_FRAME_SIZE)
			memset((uint8_t*)buffer + len, 0, WAVE_FRAME_SIZE - len);
	}

	return len;
}
//--------------------------------------
//
//--------------------------------------
uint32_t audio_data_reader_wave_buffer_t::read_first_block(void* buffer)
{
	m_pointer = 0;
	return read_next_block(buffer);
}
//--------------------------------------
//
//--------------------------------------
audio_data_reader_mp3_file_t::audio_data_reader_mp3_file_t()
{
}
//--------------------------------------
//
//--------------------------------------
audio_data_reader_mp3_file_t::~audio_data_reader_mp3_file_t()
{
	close();
}
//--------------------------------------
//
//--------------------------------------
bool audio_data_reader_mp3_file_t::open(const char* fileName)
{
	return m_file.open(fileName);
}
//--------------------------------------
//
//--------------------------------------
bool audio_data_reader_mp3_file_t::is_opened()
{
	return !m_file.eof();
}
//--------------------------------------
//
//--------------------------------------
void audio_data_reader_mp3_file_t::close()
{
	m_file.close();
}
//--------------------------------------
//
//--------------------------------------
void audio_data_reader_mp3_file_t::reset()
{
}
//--------------------------------------
//
//--------------------------------------
uint32_t audio_data_reader_mp3_file_t::read_next_block(void* buffer)
{
	return m_file.readNext((short*)buffer, WAVE_FRAME_SIZE);
}
//--------------------------------------
//
//--------------------------------------
uint32_t audio_data_reader_mp3_file_t::read_first_block(void* buffer)
{
	reset();
	
	return read_next_block(buffer);
}
//--------------------------------------
//
//--------------------------------------
uint32_t audio_data_reader_mp3_file_t::get_format_channels()
{
	return m_file.getChannelCount();
}
//--------------------------------------
//
//--------------------------------------
uint32_t audio_data_reader_mp3_file_t::get_format_rate()
{
	return m_file.getFrequency();
}
//--------------------------------------
