/* RTX H.248 Media Gate. VPX Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

//-----------------------------------------------
// 
//-----------------------------------------------
struct vpx_codec_ctx;

//-----------------------------------------------
// 
//-----------------------------------------------
namespace vpx
{
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	class VpxDecoder
	{
	public:
		VpxDecoder();
		~VpxDecoder();

		void reset();
		bool decode(const uint8_t* frame, uint32_t length);
		bool hasImage();
		int getFrameWidth() const { return m_width; }
		int getFrameHeight() const { return m_height; }
		const uint8_t* getData() const { return m_buffer + m_bufferPos; }
		int getDataLength() const { return m_lastSize; }

	private:
		uint8_t* m_buffer;
		int m_bufferLength;
		int m_bufferPos;
		int m_lastSize;
		int m_count_res_pos;
		vpx_codec_ctx* m_ctx;
		int m_width;
		int m_height;
		
		void* m_iter; // ??
	};
}
