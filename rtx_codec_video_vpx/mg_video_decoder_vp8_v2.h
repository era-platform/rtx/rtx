/* RTX H.248 Media Gate. VPX Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#define VIDEO_TEST 1

namespace vpx
{

	//-----------------------------------------------
	// 
	//-----------------------------------------------
	typedef struct { const uint8_t* ptr; uint32_t len; } t_video_nal_data;
	typedef rtl::ArrayT<t_video_nal_data> t_video_nal_array;

	//-----------------------------------------------
	// 
	//-----------------------------------------------
	class Vp8Depacketizer
	{
		static const uint32_t MAX_SIZE_PAYLOAD = 32 * 1024;

		t_video_nal_array m_current_nals;

		uint8_t* m_buffer;
		uint32_t m_buffer_pos;
		uint32_t m_buffer_size;

		uint32_t m_last_timestamp;

		uint8_t m_descriptor_S;
		uint8_t m_descriptor_PartID;

		bool m_is_keyframe;
		bool m_is_interframe;
	
		uint32_t m_part_size;

		bool m_m;

	public:
		Vp8Depacketizer();
		~Vp8Depacketizer();

		void reset();

		bool depacketize(const uint8_t* data, uint32_t length, bool marker, uint32_t timestamp);
		bool isReady() const { return m_current_nals.getCount() > 0; }
		const t_video_nal_array& current_nals() const { return m_current_nals; }

	private:
		int check_timestamp(const uint8_t* from, uint32_t from_len, unsigned timestamp);
		uint32_t parse_descriptor(const uint8_t* from, uint32_t from_len);
		bool write_patrition(const uint8_t* from, uint32_t from_len);
	};
	//-----------------------------------------------
	//
	//-----------------------------------------------
	class VP8Parser
	{
		uint8_t* m_parsed_frame;
		uint32_t m_parsed_size;

	public:
		VP8Parser() : m_parsed_frame(nullptr), m_parsed_size(0) { }
		~VP8Parser() { reset(); }

		void reset();
		uint32_t parse(const uint8_t* data, uint32_t len);
		const uint8_t* parsed_frame() const { return m_parsed_frame; }
		uint32_t parsed_frame_size() const { return m_parsed_size; }
	};
	//-----------------------------------------------
	//
	//-----------------------------------------------
	class Decoder : public mg_video_decoder_t
	{
		rtl::Logger& m_log;

		Vp8Depacketizer m_depack;
		VP8Parser m_parser;
		VpxDecoder m_dec;
		bool m_startDecode;

#if VIDEO_TEST
		rtl::FileStream m_out;
#endif

	private:
		bool initialize();
		bool sdp_att_match(const char* attr_name, const char* attr_value);

	public:
		Decoder(rtl::Logger& log);
		virtual ~Decoder();

		bool initialize(const media::PayloadFormat* fmt);
		void destroy();

		virtual const char*	getEncoding();
		virtual bool init();
		virtual media::VideoImage* decode(const rtp_packet* rtp_pack);
	};
}
