/* RTX H.248 Media Gate. VPX Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_video_decoder_vp8_v2.h"
//-----------------------------------------------
// 
//-----------------------------------------------
#define LOG_PREFIX "VP8D"

namespace vpx
{
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	Decoder::Decoder(rtl::Logger& log) : m_log(log)
	{
		m_startDecode = false;

#if VIDEO_TEST
		m_out.createNew("c:\\temp\\vconf_test\\vp8_dec_rgb.raw");
#endif
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	Decoder::~Decoder()
	{
		destroy();

#if VIDEO_TEST
		m_out.close();
#endif

	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	bool Decoder::initialize(const media::PayloadFormat* fmt)
	{
		destroy();

		m_dec.reset();
		m_depack.reset();
		m_parser.reset();

		return true;
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	void Decoder::destroy()
	{
		m_dec.reset();
		m_depack.reset();
		m_parser.reset();
		m_startDecode = false;
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	const char*	Decoder::getEncoding()
	{
		return "VP8";
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	bool Decoder::init()
	{
		return true;
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	media::VideoImage* Decoder::decode(const rtp_packet* rtp_pack)
	{
		if (m_depack.depacketize(rtp_pack->get_payload(), rtp_pack->get_payload_length(), rtp_pack->get_marker(), rtp_pack->getTimestamp()))
		{
			const t_video_nal_array& nals = m_depack.current_nals();

			for (int i = 0; i < nals.getCount(); i++)
			{
				const t_video_nal_data& nal = nals[i];
			
				uint32_t processed = 0;

				while (processed < nal.len)
				{
					processed += m_parser.parse(nal.ptr + processed, nal.len - processed);
					if (m_parser.parsed_frame() == nullptr)
						continue;

					if (!m_dec.decode(m_parser.parsed_frame(), m_parser.parsed_frame_size()))
					{
						LOG_WARN(LOG_PREFIX, "DecoderProc -- error while decoding!");
						continue; // break;
					}

					if (!m_dec.hasImage())
					{
						continue;
					}

					const uint8_t* data = m_dec.getData();
					int len = m_dec.getDataLength();
					int w = m_dec.getFrameWidth();
					int h = m_dec.getFrameHeight();

					LOG_CALL(LOG_PREFIX, "image writing %dx%d %p %d bytes", w, h, data, len);
					
#if VIDEO_TEST
					m_out.write(data, len);
#endif

					media::Bitmap bmp;
					bmp.setBitmapBits(w, h, m_dec.getData(), m_dec.getDataLength());
					media::VideoImage* image = NEW media::VideoImage;
					image->exportARGB(bmp);

					return image;
				}
			}
		}

		//LOG_WARN(LOG_PREFIX, "DecoderProc -- depackeize failed!");

		return nullptr;
	}

	//-----------------------------------------------
	// 
	//-----------------------------------------------
#define LOG_PREFIX_D			L"RTP>VP8"
	//-----------------------------------------------
	//	X : Extended control bits present.  When set to one, the extension
	//octet MUST be provided immediately after the mandatory first
	//octet.If the bit is zero, all optional fields MUST be omitted.
	//	R : Bit reserved for future use.MUST be set to zero and MUST be
	//ignored by the receiver.
	//	N : Non - reference frame.When set to one, the frame can be discarded
	//without affecting any other future or past frames.If the
	//reference status of the frame is unknown, this bit SHOULD be set
	//to zero to avoid discarding frames needed for reference.
	//	Informative note : This document does not describe how to
	//	determine if an encoded frame is non - reference.The reference
	//	status of an encoded frame is preferably provided from the
	//	encoder implementation.
	//	S : Start of VP8 partition.SHOULD be set to 1 when the first payload
	//octet of the RTP packet is the beginning of a new VP8 partition,
	//and MUST NOT be 1 otherwise.The S bit MUST be set to 1 for the
	//first packet of each encoded frame.
	//	PID : Partition index.Denotes which VP8 partition the first payload
	//octet of the packet belongs to.The first VP8 partition
	//(containing modes and motion vectors) MUST be labeled with PID =
	//0.  PID SHOULD be incremented for each subsequent partition, but
	//MAY be kept at 0 for all packets.PID MUST NOT be larger than 7.
	//If more than one packet in an encoded frame contains the same PID,
	//the S bit MUST NOT be set for any other packet than the first
	//packet with that PID.
	//-----------------------------------------------
	//	 0 1 2 3 4 5 6 7 
	//	+-+-+-+-+-+-+-+-+
	//	|X|R|N|S|R| PID | (REQUIRED)
	//	+-+-+-+-+-+-+-+-+
	//-----------------------------------------------
	struct vp8_payload_descriptor_head
	{
		uint8_t pid : 3, r1 : 1, s : 1, n : 1, r2 : 1, x : 1;
	};
	//-----------------------------------------------
	// I: PictureID present.When set to one, the OPTIONAL PictureID MUST
	//	be present after the extension bit field and specified as below.
	//	Otherwise, PictureID MUST NOT be present.
	//
	// L : TL0PICIDX present.When set to one, the OPTIONAL TL0PICIDX MUST
	//	be present and specified as below, and the T bit MUST be set to 1.
	//	Otherwise, TL0PICIDX MUST NOT be present.
	//
	// T : TID present.When set to one, the OPTIONAL TID / KEYIDX octet MUST
	//	be present.The TID | Y part of the octet MUST be specified as
	//	below.If K(below) is set to one but T is set to zero, the TID /
	//	KEYIDX octet MUST be present, but the TID | Y field MUST be ignored.
	//	If neither T nor K is set to one, the TID / KEYIDX octet MUST NOT be
	//	present.
	//
	// K : KEYIDX present.When set to one, the OPTIONAL TID / KEYIDX octet
	//	MUST be present.The KEYIDX part of the octet MUST be specified
	//	as below.If T(above) is set to one but K is set to zero, the
	//	TID / KEYIDX octet MUST be present, but the KEYIDX field MUST be
	//	ignored.If neither T nor K is set to one, the TID / KEYIDX octet
	//	MUST NOT be present.
	//
	// RSV : Bits reserved for future use.MUST be set to zero and MUST be
	//	  ignored by the receiver.
	//-----------------------------------------------
	//     +-+-+-+-+-+-+-+-+
	//	X: |I|L|T|K|  RSV  | (OPTIONAL)
	//	   +-+-+-+-+-+-+-+-+
	//-----------------------------------------------
	struct vp8_payload_descriptor_X
	{
		uint8_t rsv : 4, k : 1, t : 1, l : 1, i : 1;
	};
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	Vp8Depacketizer::Vp8Depacketizer() : m_buffer_size(0), m_buffer_pos(0), m_buffer(nullptr),
		m_last_timestamp(0)
	{
		m_descriptor_PartID = 0;
		m_descriptor_S = 0;
		m_m = false;
		m_is_interframe = false;
		m_is_keyframe = false;
		m_part_size = 0;
		m_buffer_size = MAX_SIZE_PAYLOAD;
		m_buffer = NEW uint8_t[m_buffer_size];

	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	Vp8Depacketizer::~Vp8Depacketizer()
	{
		reset();

		DELETEAR(m_buffer);
		m_buffer = nullptr;
		m_buffer_size = 0;
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	void Vp8Depacketizer::reset()
	{
		m_current_nals.clear();

		memset(m_buffer, 0, m_buffer_size);
		m_buffer_pos = 0;
		m_last_timestamp = 0;
		m_m = false;
		m_is_interframe = false;
		m_is_keyframe = false;
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	bool Vp8Depacketizer::depacketize(const uint8_t* data, uint32_t length, bool marker, uint32_t timestamp)
	{
		if ((data == nullptr) || (length == 0))
		{
			LOG_WARN(LOG_PREFIX, "Vp8Depacketizer::depacketize -> invalid params");
			return false;
		}

		m_current_nals.clear();
		m_m = marker;

		const uint8_t* from = (uint8_t*)data;
		uint32_t from_len = length;

		uint32_t size_desc = parse_descriptor(from, from_len);
		if (size_desc == 0)
		{
			LOG_WARN(LOG_PREFIX, "Vp8Depacketizer::depacketize -> parser error");
			return false;
		}

		int size_hdr = check_timestamp(from + size_desc, from_len - size_desc, timestamp);
		if ((size_hdr < 0) || (from_len <= (size_desc + size_hdr)))
		{
			LOG_WARN(LOG_PREFIX, "Vp8Depacketizer::depacketize -> timestamp error");
			m_buffer_pos = 0;
			return false;
		}

		if (!m_is_keyframe)
		{
			LOG_CALL(LOG_PREFIX, "Vp8Depacketizer::depacketize -> no keyframe");
			m_buffer_pos = 0;
			return false;
		}

		uint32_t size_res = from_len - (size_desc);
		if (!write_patrition(from + size_desc, size_res))
		{
			LOG_WARN(LOG_PREFIX, "Vp8Depacketizer::depacketize -> assembly error");
			return false;
		}

		if (m_m)
		{
			const uint8_t* pay_ptr = (const uint8_t*)m_buffer;
			const uint32_t pay_size = m_buffer_pos;

			m_buffer_pos = 0;

			if (pay_size < m_part_size)
			{
				LOG_WARN(LOG_PREFIX, "Vp8Depacketizer::depacketize -> internal error");
				return false;
			}

			m_current_nals.add({ (const uint8_t*)pay_ptr, pay_size });

			return true;
		}

		LOG_WARN(LOG_PREFIX, "Vp8Depacketizer::depacketize -> continue...");

		return false;
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	uint32_t Vp8Depacketizer::parse_descriptor(const uint8_t* from, uint32_t from_len)
	{
		uint32_t size = 0;
		const uint8_t* pdata = from;

		vp8_payload_descriptor_head* vp8_pd = (vp8_payload_descriptor_head*)pdata;
		if ((vp8_pd->r1) || (vp8_pd->r2))
		{
			return 0;
		}

		m_descriptor_S = (*pdata & 0x10) >> 4;
		m_descriptor_PartID = (*pdata & 0x0F);

		if (m_descriptor_S)
		{
			m_buffer_pos = 0;
		}

		size++;
		if (size >= from_len)
		{
			return 0;
		}

		/*When the X bit is set to 1 in the first octet, the extension bit
		field octet MUST be provided as the second octet.  If the X bit is 0,
		the extension bit field octet MUST NOT be present, and all bits below
		MUST be implicitly interpreted as 0.*/
		if (vp8_pd->x)
		{
			vp8_payload_descriptor_X* vp8_pd_X = (vp8_payload_descriptor_X*)(pdata + size);
			size++;
			if (size >= from_len)
			{
				return 0;
			}

			if (vp8_pd_X->i)
			{
				uint8_t* I = (uint8_t*)(pdata + size);
				uint8_t M = (*I & 0x80);
				/*M: The most significant bit of the first octet is an extension flag.
				The field MUST be present if the I bit is equal to one.If set
				the PictureID field MUST contain 16 bits else it MUST contain 8
				bits including this MSB, see PictureID.*/
				if (M)
				{
					// PictureID 16 bits
					size += 2;
					if (size >= from_len)
					{
						return 0;
					}
				}
				else
				{
					// PictureID 8 bits
					size++;
					if (size >= from_len)
					{
						return 0;
					}
				}
			}

			if (vp8_pd_X->l)
			{
				size++;
				if (size >= from_len)
				{
					return 0;
				}
			}

			if ((vp8_pd_X->t) || (vp8_pd_X->k))
			{
				size++;
				if (size >= from_len)
				{
					return 0;
				}
			}
		}

		return size;
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	int Vp8Depacketizer::check_timestamp(const uint8_t* from, uint32_t from_len, unsigned timestamp)
	{
		const uint8_t* pdata = from;
		int size = 0;

		// New frame ?
		if (m_last_timestamp != timestamp)
		{
			/* 4.3.  VP8 Payload Header
			Note that the header is present only in packets
			which have the S bit equal to one and the PartID equal to zero in the
			payload descriptor.  Subsequent packets for the same frame do not
			carry the payload header.
			0 1 2 3 4 5 6 7
			+-+-+-+-+-+-+-+-+
			|Size0|H| VER |P|
			+-+-+-+-+-+-+-+-+
			|     Size1     |
			+-+-+-+-+-+-+-+-+
			|     Size2     |
			+-+-+-+-+-+-+-+-+
			| Bytes 4..N of |
			| VP8 payload   |
			:               :
			+-+-+-+-+-+-+-+-+
			| OPTIONAL RTP  |
			| padding       |
			:               :
			+-+-+-+-+-+-+-+-+
			P: Inverse key frame flag.  When set to 0 the current frame is a key
			frame.  When set to 1 the current frame is an interframe.  Defined
			in [RFC6386]
			*/

			m_buffer_pos = 0;

			// Make sure the header is present
			if ((m_descriptor_S != 1) || (m_descriptor_PartID != 0) || (from_len < 3))
			{
				return size;
			}

			/* SizeN:  The size of the first partition in bytes is calculated from
			the 19 bits in Size0, Size1, and Size2 as 1stPartitionSize = Size0
			+ 8 * Size1 + 2048 * Size2.  [RFC6386]. */
			m_part_size = ((pdata[0] >> 5) & 0xFF) + 8 * pdata[1] + 2048 * pdata[2];
			m_is_interframe = (pdata[0] & 0x01);
			if (!m_is_keyframe)
			{
				m_is_keyframe = !m_is_interframe;
			}

			size = 3;
			m_last_timestamp = timestamp;
		}

		return size;
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	bool Vp8Depacketizer::write_patrition(const uint8_t* from, uint32_t from_len)
	{
		if (m_buffer_size < from_len + m_buffer_pos)
		{
			uint32_t new_size = from_len + m_buffer_pos;
			uint8_t* buff = NEW uint8_t[new_size];
			if (m_buffer != nullptr)
			{
				memcpy(buff, m_buffer, m_buffer_size);
				DELETEAR(m_buffer);
			}
			m_buffer_size = new_size;
			m_buffer = buff;
		}

		memcpy(m_buffer + m_buffer_pos, from, from_len);
		m_buffer_pos += from_len;

		return true;
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	void VP8Parser::reset()
	{
		if (m_parsed_frame != nullptr)
		{
			delete[] m_parsed_frame;
			m_parsed_frame = nullptr;
		}

		m_parsed_size = 0;
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	uint32_t VP8Parser::parse(const uint8_t* data, uint32_t len)
	{
		if (m_parsed_size < len)
		{
			delete[] m_parsed_frame;
			m_parsed_frame = new uint8_t[len];
		}

		memcpy(m_parsed_frame, data, len);
		m_parsed_size = len;

		return len;
	}
}
//-----------------------------------------------
