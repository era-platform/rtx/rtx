/* RTX H.248 Media Gate. VPX Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_video_decoder_vp8.h"

#if defined VIDEO_TEST
static void print_vp8_header(FILE* out, const rtp_packet* packet);
#endif

mg_video_decoder_vp8_t::mg_video_decoder_vp8_t(rtl::Logger& log) : m_log(log)
{
	memset(&m_cfg, 0, sizeof(vpx_codec_dec_cfg_t));
	m_initialized = false;
	memset(&m_context, 0, sizeof(vpx_codec_ctx_t));

	m_accumulator = nullptr;
	m_accumulator_pos = 0;
	m_accumulator_size = 0;
	m_first_part_size = 0;
	m_last_timestamp = 0;
	m_idr = false;;
	m_corrupted = false;;

#if defined (VIDEO_TEST)
	m_outfile = nullptr;
#endif
}

mg_video_decoder_vp8_t::~mg_video_decoder_vp8_t()
{
#if defined(VIDEO_TEST)
	if (m_outfile)
	{
		fclose(m_outfile);
		m_outfile = nullptr;
	}
#endif
}

bool mg_video_decoder_vp8_t::initialize(const media::PayloadFormat* fmt)
{
	if (!initialize())
		return false;


#if defined(VIDEO_TEST)

	static int nnn = 1;
	char fname[128];
	std_snprintf(fname, 128, "c:\\temp\\outfile%d.txt", nnn++);
	m_outfile = fopen(fname, "wb");

	bool b = (m_outfile != nullptr);

	//MLOG_CALL("VP8-DEC", "open video raw file %s", STR_BOOL(b));

#endif
	//MLOG_CALL("VP8-DEC", "initialization ok");
	return true;
}

void mg_video_decoder_vp8_t::destroy()
{
	if (m_initialized)
	{
		vpx_codec_destroy(&m_context);
		m_initialized = false;
	}

	if (m_accumulator)
	{
		FREE(m_accumulator);
		m_accumulator = nullptr;
	}

	m_accumulator_size = 0;
	m_accumulator_pos = 0;

#if defined(VIDEO_TEST)
	if (m_outfile)
	{
		fclose(m_outfile);
		m_outfile = nullptr;
	}
#endif
}

const char*	mg_video_decoder_vp8_t::getEncoding()
{
	return "VP8";
}

media::VideoImage* mg_video_decoder_vp8_t::decode(const rtp_packet* rtp_pack)
{
#if defined VIDEO_TEST
	print_vp8_header(m_outfile, rtp_pack);
#endif

	//MLOG_CALL("VP8-DEC", "Decode rtp_packet: seq:%d ts:%08x M:%s %d bytes",
		//rtp_pack->get_sequence_number(),
		//rtp_pack->getTimestamp(),
		//STR_BOOL(rtp_pack->get_marker()),
		//rtp_pack->get_payload_length());



	if (rtp_pack->get_payload_length() < 1 || !m_initialized)
	{
		MLOG_ERROR("VP8-D", "Invalid parameter");
		return nullptr;
	}

	if (!depacketize(rtp_pack))
	{
		return nullptr;
	}

	media::VideoImage* image = nullptr;

	// Decode the frame if we have a marker or the first partition is complete and not corrupted
	if (rtp_pack->get_marker())
	{

		// in all cases: reset accumulator position
		int pay_size = m_accumulator_pos;
		m_accumulator_pos = 0;

		if (pay_size < m_first_part_size)
		{
			MLOG_WARN("VP8-D", "[VP8] No enough bytes for the first part: %u < %u", pay_size, m_first_part_size);
			// Not a fatal error
			return nullptr;
		}

		vpx_codec_err_t vpx_ret = vpx_codec_decode(&m_context, m_accumulator, (int)pay_size, nullptr, 0);

#if defined VIDEO_TEST
		fprintf(m_outfile, "DEC:ASize:%9d PSize:%9d -> %d %s\n", pay_size, m_first_part_size, vpx_ret,
			vpx_ret ? vpx_codec_err_to_string(vpx_ret) : "Success");
#endif

		if (vpx_ret != VPX_CODEC_OK)
		{
			MLOG_WARN("VP8-D", "vpx_codec_decode (asize:%d psize:%d) failed with error =%s", pay_size, m_first_part_size, vpx_codec_err_to_string(vpx_ret));
			if (vpx_ret == VPX_CODEC_UNSUP_BITSTREAM)
			{
				LOG_BINARY("VP8-D", rtp_pack->get_payload(), rtp_pack->get_payload_length(), "corrupted packet!");
			}
			return nullptr;
		}

		//if (m_idr)
		//{
			//MLOG_WARN("VP8-D", "Decoded VP8 IDR");
		//}

		// copy decoded data
		int ret = 0;
		vpx_image_t* img;
		vpx_codec_iter_t iter = nullptr;

		while ((img = vpx_codec_get_frame(&m_context, &iter)))
		{
			// update sizes
			int xsize = (img->d_w * img->d_h * 3) >> 1;

			// allocate destination buffer
			if (image == nullptr)
			{
				image = NEW media::VideoImage(img->d_w, img->d_h);
			}
			else
			{
				image->makeImage(img->d_w, img->d_h);
			}

			uint8_t* out_data = image->getImageBuffer();

			// layout picture
			for (int plane = 0; plane < 3; plane++)
			{
				uint8_t* buf = img->planes[plane];

				for (int y = 0; y < (int)img->d_h >> (plane ? 1 : 0); y++)
				{
					int w_count = img->d_w >> (plane ? 1 : 0);

					if ((ret + w_count) > xsize)
					{
						MLOG_ERROR("VP8-D", "BufferOverflow");
						return nullptr;
					}

					memcpy(out_data + ret, buf, w_count);
					ret += w_count;
					buf += img->stride[plane];
				}
			}
			//#if defined(VIDEO_TEST)
			//				fwrite(out_data, xsize, 1, m_outfile);
			//#endif
		}
	}

	return image; // ret > 0;
}

bool mg_video_decoder_vp8_t::initialize()
{
	if (m_initialized)
	{
		return true;
	}

	m_cfg.w = 640;
	m_cfg.h = 480;

#if defined TARGET_OS_WINDOWS
	{
		SYSTEM_INFO SystemInfo;
		GetSystemInfo(&SystemInfo);
		m_cfg.threads = SystemInfo.dwNumberOfProcessors;
	}
#endif

	vpx_codec_flags_t dec_flags = 0;
	vpx_codec_caps_t dec_caps = vpx_codec_get_caps(&vpx_codec_vp8_dx_algo);

	if (dec_caps & VPX_CODEC_CAP_POSTPROC)
	{
		dec_flags |= VPX_CODEC_USE_POSTPROC;
	}

#if defined(VPX_CODEC_CAP_ERROR_CONCEALMENT)
	if (dec_caps & VPX_CODEC_CAP_ERROR_CONCEALMENT)
	{
		dec_flags |= VPX_CODEC_USE_ERROR_CONCEALMENT;
	}
#endif

	vpx_codec_err_t vpx_ret;

	if ((vpx_ret = vpx_codec_dec_init(&m_context, vpx_codec_vp8_dx(), &m_cfg, dec_flags)) != VPX_CODEC_OK)
	{
		MLOG_ERROR("vp8", "vpx_codec_dec_init failed with error =%s", vpx_codec_err_to_string(vpx_ret));
		return false;
	}

	static vp8_postproc_cfg_t __pp = { VP8_DEBLOCK | VP8_DEMACROBLOCK, 4, 0 };

	if ((vpx_ret = vpx_codec_control(&m_context, VP8_SET_POSTPROC, &__pp)))
	{
		MLOG_WARN("vp8", "vpx_codec_dec_init failed with error =%s", vpx_codec_err_to_string(vpx_ret));
	}

	return m_initialized = true;
}

bool mg_video_decoder_vp8_t::depacketize(const rtp_packet* rtp_pack)
{
	const uint8_t* in_data = rtp_pack->get_payload();
	int in_size = rtp_pack->get_payload_length();
	const uint8_t* pdata = in_data;
	const uint8_t* pdata_end = (pdata + in_size);

	static const size_t xmax_size = (3840 * 2160 * 3) >> 3; // >>3 instead of >>1 (not an error)

	/* 4.2. VP8 Payload Descriptor */

	bool X = *pdata & 0x80;
	bool N = *pdata & 0x20;
	bool S = *pdata & 0x10;
	uint8_t PartID = *pdata & 0x0F;

	// skip "REQUIRED" header
	if (++pdata >= pdata_end)
	{
		MLOG_ERROR("VP8-D", "Too short (REQUIRED)");
		return false;
	}

	// check "OPTIONAL" headers
	if (X)
	{
		bool I = (*pdata & 0x80);
		bool L = (*pdata & 0x40);
		bool T = (*pdata & 0x20);
		bool K = (*pdata & 0x10);

		if (++pdata >= pdata_end)
		{
			MLOG_ERROR("VP8-D", "Too short (OPTIONAL)");
			return false;
		}

		if (I)
		{
			if (*pdata & 0x80)
			{
				// M
				// PictureID on 16bits
				if ((pdata += 2) >= pdata_end)
				{
					MLOG_ERROR("VP8-D", "Too short (PictureID 16)");
					return false;
				}
			}
			else
			{
				// PictureID on 8bits
				if (++pdata >= pdata_end)
				{
					MLOG_ERROR("VP8-D", "Too short (PictureID 8)");
					return false;
				}
			}
		}

		if (L)
		{
			if (++pdata >= pdata_end)
			{
				MLOG_ERROR("VP8-D", "Too short (L)");
				return false;
			}
		}

		if (T || K)
		{
			if (++pdata >= pdata_end)
			{
				MLOG_ERROR("VP8-D", "Too short (T|K");
				return false;
			}
		}
	}

	in_size = (int)(pdata_end - pdata);

	// New frame ?
	if (m_last_timestamp != rtp_pack->getTimestamp())
	{
		/* 4.3.  VP8 Payload Header
		Note that the header is present only in packets
		which have the S bit equal to one and the PartID equal to zero in the
		payload descriptor.  Subsequent packets for the same frame do not
		carry the payload header.
		0 1 2 3 4 5 6 7
		+-+-+-+-+-+-+-+-+
		|Size0|H| VER |P|
		+-+-+-+-+-+-+-+-+
		|     Size1     |
		+-+-+-+-+-+-+-+-+
		|     Size2     |
		+-+-+-+-+-+-+-+-+
		| Bytes 4..N of |
		| VP8 payload   |
		:               :
		+-+-+-+-+-+-+-+-+
		| OPTIONAL RTP  |
		| padding       |
		:               :
		+-+-+-+-+-+-+-+-+
		P: Inverse key frame flag.  When set to 0 the current frame is a key
		frame.  When set to 1 the current frame is an interframe.  Defined
		in [RFC6386]
		*/

		// Reset accumulator position
		m_accumulator_pos = 0;

		// Make sure the header is present
		if (!S || PartID != 0 || in_size < 3)
		{
			MLOG_WARN("VP8-D", "VP8 payload header is missing");
			return false;
		}

		/* SizeN:  The size of the first partition in bytes is calculated from
		   the 19 bits in Size0, Size1, and Size2 as 1stPartitionSize = Size0
		   + 8 * Size1 + 2048 * Size2.  [RFC6386]. */
		m_first_part_size = ((pdata[0] >> 5) & 0xFF) + 8 * pdata[1] + 2048 * pdata[2];

		// Starting new frame...reset "corrupted" value
		m_corrupted = false;

		// Key frame?
		m_idr = !(pdata[0] & 0x01);

		// Update timestamp
		m_last_timestamp = rtp_pack->getTimestamp();
	}

	if (in_size > xmax_size)
	{
		m_accumulator_pos = 0;
		MLOG_ERROR("VP8-D", "%u too big to contain valid encoded data. xmax_size=%u", (unsigned)in_size, (unsigned)xmax_size);
		return false;
	}

	// start-accumulator
	if (m_accumulator == nullptr)
	{
		if (!(m_accumulator = (uint8_t*)MALLOC(in_size)))
		{
			MLOG_ERROR("VP8-D", "Failed to allocated new buffer");
			return false;
		}
		m_accumulator_size = in_size;
	}

	if ((m_accumulator_pos + in_size) >= xmax_size)
	{
		MLOG_ERROR("VP8-D", "BufferOverflow");
		m_accumulator_pos = 0;
		return false;
	}

	if ((m_accumulator_pos + in_size) > m_accumulator_size)
	{
		if (!(m_accumulator = (uint8_t*)REALLOC(m_accumulator, (m_accumulator_pos + in_size))))
		{
			MLOG_ERROR("VP8-D", "Failed to reallocated new buffer");
			m_accumulator_pos = 0;
			m_accumulator_size = 0;
			return false;
		}
		m_accumulator_size = (m_accumulator_pos + in_size);
	}

	memcpy(m_accumulator + m_accumulator_pos, pdata, in_size);
	m_accumulator_pos += in_size;

	return true;
}

#if defined VIDEO_TEST
static uint16_t swap2(uint16_t v)
{
	return (v << 8) | (v >> 8);
}
static void print_vp8_header(FILE* out, const rtp_packet* packet)
{
	fprintf(out, "-----\nRTP:%5d, %08X, M:%d L:%5d", packet->get_sequence_number(), packet->getTimestamp(),
		packet->get_marker() ? 1: 0, packet->get_payload_length());

	const uint8_t* vp8 = packet->get_payload();
	const uint8_t* bin = packet->get_payload();
	int length = packet->get_payload_length();

	uint8_t X = (*vp8 & 0x80) >> 7;
	uint8_t N = (*vp8 & 0x20) >> 5;
	uint8_t S = (*vp8 & 0x10) >> 4;
	uint8_t PID = *vp8 & 0x07;

	fprintf(out, ", (%02X) X:%d N:%d S:%d PID:% 3d", *vp8, X, N, S, PID);

	if (X)
	{
		vp8++;
		
		uint8_t I = (*vp8 & 0x80) >> 7;
		uint8_t L = (*vp8 & 0x40) >> 6;
		uint8_t T = (*vp8 & 0x20) >> 5;
		uint8_t K = (*vp8 & 0x10) >> 4;

		fprintf(out, ", (%02X) I:%d L:%d T:%d K:%d", *vp8, I, L, T, K);

		if (I)
		{
			vp8++;

			uint8_t M = (*vp8 & 0x80) >> 7;
			uint16_t PictureId = *vp8 & 0x7F;
			if (M)
			{
				PictureId <<= 8;
				PictureId |= *(++vp8);
			}

			fprintf(out, ", (%02X) M:%d PictId:% 5d", *vp8, M, PictureId);
		}

		if (L)
		{
			vp8++;

			fprintf(out, ", (%02X) TL0PICIDX:% 3d", *vp8, *vp8);
		}

		if (T || K)
		{
			vp8++;
			
			uint8_t TID = (*vp8 & 0xC0) >> 6;
			uint8_t Y = (*vp8 & 0x20) >> 5;
			uint8_t KEYIDX = *vp8 & 0x1F;

			fprintf(out, ", (%02X) TID: 3%d Y:%d KEYIDX:% 2d", *vp8, TID, Y, KEYIDX);
		}
	}

	// fragment assembling
	if (S && PID == 0)
	{
		vp8++;
		uint8_t b1 = *vp8;
		uint32_t S0 = (*vp8 & 0xD0) >> 5;
		uint8_t H = (*vp8 & 0x10) >> 4;
		uint8_t VER = (*vp8 & 0x0D) >> 1;
		uint8_t P = *vp8 & 0x01;
		uint32_t S1 = *(++vp8);
		uint32_t S2 = *(++vp8);

		uint32_t Size = S0 + 8 * S1 + 2048 * S2;
		uint32_t Size1 = S0 | (S2 << 3) | (S2 << 11);

		fprintf(out, ", (%02X) VP8: S0:1%d H:%d VER:%d P:%c (%02X) S1:%3d (%02X) S2:%3d Size:%d/%d",
			b1, S0, H, VER, P ? 'I' : 'K',
			S1, S1, S2, S2, Size, Size1);

		if (!P)
		{
			const uint8_t *c = ++vp8;

			// vet via sync code
			if (c[0] != 0x9d || c[1] != 0x01 || c[2] != 0x2a)
			{
				fprintf(out, " Corrupted frame! %02x %02x %02x", c[0], c[1], c[2]);
			}
			else
			{
				int Width = (*(uint16_t*)(c + 3)) & 0x3fff;
				int horiz_scale = (*(uint16_t*)(c + 3)) >> 14;
				int Height = (*(uint16_t*)(c + 5)) & 0x3fff;
				int vert_scale = (*(uint16_t*)(c + 5)) >> 14;

				fprintf(out, ", W:%5d HS:%5d H:%5d VS:%5d", Width, horiz_scale, Height, vert_scale);
			}
		}
	}

	char hex[256];

	int len = std_bytes_to_hex(hex, 256, bin, MIN(30, length));
	hex[len] = 0;

	fprintf(out, "\nBIN:%d %s\n", len, hex);
}
#endif
