/* RTX H.248 Media Gate. VPX Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_video_encoder_vp8.h"

#if !defined(TDAV_VP8_DISABLE_EXTENSION)
#   define TDAV_VP8_DISABLE_EXTENSION       1 /* Set X fied value to zero */
#endif

#if TDAV_VP8_DISABLE_EXTENSION
#   define TDAV_VP8_PAY_DESC_SIZE			1
#else
#   define TDAV_VP8_PAY_DESC_SIZE			4
#endif
#define TDAV_SYSTEM_CORES_COUNT				0
#define TDAV_VP8_GOP_SIZE_IN_SECONDS		60
#define TDAV_VP8_RTP_PAYLOAD_MAX_SIZE		1050
#if !defined(TDAV_VP8_MAX_BANDWIDTH_KB)
#	define TDAV_VP8_MAX_BANDWIDTH_KB		6000
#endif
#if !defined(TDAV_VP8_MIN_BANDWIDTH_KB)
#	define TDAV_VP8_MIN_BANDWIDTH_KB		100
#endif


mg_video_encoder_vp8_t::mg_video_encoder_vp8_t(rtl::Logger& log, rtx_video_encoder_cb_t cb, void* usr) : m_log(log)
{
	m_width = 640;
	m_height = 480;
	m_fps = 30;
	m_max_br = 0;
	m_max_mbps = 0;
	m_chroma = media::VideoChroma::YUV420p;
	m_flip = false;

	m_last_clock = rtl::DateTime::getTicks();
	m_last_ts = (rand() ^ 0x1B5D) & 0x000FFFFF;

	m_callback = cb;
	m_user = usr;

	//! preferred video size
	m_pref_size = media::VideoStandard::HDTV480p;

	memset(&m_cfg, 0, sizeof(m_cfg));
	m_initialized = 0;
	m_pts = 0;
	memset(&m_context, 0, sizeof(m_context));//
	
	m_pic_id = 0;
	m_frame_count = 0;
	m_force_idr = 0;
	m_rotation = 0;

	m_rtp = { nullptr, 0 };

#if defined (VIDEO_TEST)
	m_outfile = nullptr;
#endif

}

mg_video_encoder_vp8_t::~mg_video_encoder_vp8_t()
{
}

bool mg_video_encoder_vp8_t::initialize(const media::PayloadFormat* fmt)
{
	vpx_codec_err_t vpx_ret;
	vpx_enc_frame_flags_t enc_flags = 0; // VPX_EFLAG_XXX

	if (m_initialized)
	{
		MLOG_CALL("VP8-ENC", "VP8 encoder already inialized");
		return false;
	}

	if ((vpx_ret = vpx_codec_enc_config_default(vpx_codec_vp8_cx(), &m_cfg, 0)) != VPX_CODEC_OK) {
		MLOG_CALL("VP8-ENC","vpx_codec_enc_config_default failed with error =%s", vpx_codec_err_to_string(vpx_ret));
		return false;
	}
	m_cfg.g_timebase.num = 1;
	m_cfg.g_timebase.den = m_fps;
	m_cfg.rc_target_bitrate = VIDEO_CLAMP(
		1,
		media::getVideoBandwidthKbps2(m_width, m_height, m_fps),
		INT32_MAX //m_bandwidth_max_upload
		);
	m_cfg.g_w = (m_rotation == 90 || m_rotation == 270) ? m_height : m_width;
	m_cfg.g_h = (m_rotation == 90 || m_rotation == 270) ? m_width : m_height;
	m_cfg.kf_mode = VPX_KF_AUTO;
	/*m_cfg.kf_min_dist =*/ m_cfg.kf_max_dist = (TDAV_VP8_GOP_SIZE_IN_SECONDS * m_fps);
#if defined(VPX_ERROR_RESILIENT_DEFAULT)
	m_cfg.g_error_resilient = VPX_ERROR_RESILIENT_DEFAULT;
#else
	m_cfg.g_error_resilient = 1;
#endif
#if defined(VPX_ERROR_RESILIENT_PARTITIONS)
	m_cfg.g_error_resilient |= VPX_ERROR_RESILIENT_PARTITIONS;
#endif
#if defined(VPX_CODEC_USE_OUTPUT_PARTITION)
	enc_flags |= VPX_CODEC_USE_OUTPUT_PARTITION;
#endif
	m_cfg.g_lag_in_frames = 0;
#if TDAV_UNDER_WINDOWS
	{
		SYSTEM_INFO SystemInfo;
		GetSystemInfo(&SystemInfo);
		m_cfg.g_threads = SystemInfo.dwNumberOfProcessors;
	}
#endif
	m_cfg.rc_end_usage = VPX_CBR;
	m_cfg.g_pass = VPX_RC_ONE_PASS;
#if 0
	m_cfg.rc_dropframe_thresh = 30;
	m_cfg.rc_resize_allowed = 0;
	m_cfg.rc_min_quantizer = 2;
	m_cfg.rc_max_quantizer = 56;
	m_cfg.rc_undershoot_pct = 100;
	m_cfg.rc_overshoot_pct = 15;
	m_cfg.rc_buf_initial_sz = 500;
	m_cfg.rc_buf_optimal_sz = 600;
	m_cfg.rc_buf_sz = 1000;
#endif

	if ((vpx_ret = vpx_codec_enc_init(&m_context, vpx_codec_vp8_cx(), &m_cfg, enc_flags)) != VPX_CODEC_OK)
	{
		MLOG_CALL("VP8-ENC","vpx_codec_enc_init failed with error =%s", vpx_codec_err_to_string(vpx_ret));
		return false;
	}
	m_pic_id = /*(rand() ^ rand()) % 0x7FFF*/0/*Use zero: why do you want to make your life harder?*/;

	/* vpx_codec_control(&m_context, VP8E_SET_STATIC_THRESHOLD, 800); */
#if !TDAV_UNDER_MOBILE /* must not remove: crash on Android for sure and probably on iOS also (all ARM devices ?) */
	vpx_codec_control(&m_context, VP8E_SET_NOISE_SENSITIVITY, 2);
#elif TDAV_UNDER_WINDOWS_CE
	vpx_codec_control(&m_context, VP8E_SET_NOISE_SENSITIVITY, 16);
	vpx_codec_control(&m_context, VP8E_SET_CPUUSED, 16);
	vpx_codec_control(&m_context, VP8E_SET_STATIC_THRESHOLD, 16);
	vpx_codec_control(&m_context, VP8E_SET_SHARPNESS, 16);
#endif

	// Set number of partitions
#if defined(VPX_CODEC_USE_OUTPUT_PARTITION)
	{
		unsigned _s = m_height * m_width;
		if (_s < (352 * 288))
		{
			vpx_codec_control(&m_context, VP8E_SET_TOKEN_PARTITIONS, VP8_ONE_TOKENPARTITION);
		}
		else if (_s < (352 * 288) * 2 * 2)
		{
			vpx_codec_control(&m_context, VP8E_SET_TOKEN_PARTITIONS, VP8_TWO_TOKENPARTITION);
		}
		else if (_s < (352 * 288) * 4 * 4)
		{
			vpx_codec_control(&m_context, VP8E_SET_TOKEN_PARTITIONS, VP8_FOUR_TOKENPARTITION);
		}
		else if (_s < (352 * 288) * 8 * 8)
		{
			vpx_codec_control(&m_context, VP8E_SET_TOKEN_PARTITIONS, VP8_EIGHT_TOKENPARTITION);
		}
	}
#endif

	m_frame_count = 0;
	m_initialized = true;

	MLOG_CALL("VP8-ENC", "[VP8] target_bitrate=%d kbps", m_cfg.rc_target_bitrate);

#if defined(VIDEO_TEST)

	static int nnn = 1;
	char fname[128];
	std_snprintf(fname, 128, "c:\\temp\\outfile%d.vp8", nnn++);
	m_outfile = fopen(fname, "wb");

	bool b = (m_outfile != nullptr);

	MLOG_CALL("VP8-DEC", "open video raw file %s", STR_BOOL(b));

#endif


	return true;
}
void mg_video_encoder_vp8_t::destroy()
{
	MLOG_CALL("VP8-ENC", "vp8_close_encoder(begin)");
	
	if (m_initialized)
	{
		vpx_codec_destroy(&m_context);
		m_initialized = false;
	}

	FREE(m_rtp.ptr);
	m_rtp.size = 0;
	m_rotation = 0; // reset rotation


#if defined(VIDEO_TEST)
	fclose(m_outfile);
	m_outfile = nullptr;
#endif

	MLOG_CALL("VP8-ENC", "tdav_codec_vp8_close_encoder(end)");
}

const char*	mg_video_encoder_vp8_t::getEncoding()
{
	return "VP8";
}
bool mg_video_encoder_vp8_t::init(rtx_video_encoder_cb_t cb, void* user)
{
	m_callback = cb;
	m_user = user;
	return true;
}

void mg_video_encoder_vp8_t::reset()
{
}

bool mg_video_encoder_vp8_t::encode(const media::VideoImage* picture)
{
	vpx_enc_frame_flags_t flags = 0;
	vpx_codec_err_t vpx_ret = VPX_CODEC_OK;
	const vpx_codec_cx_pkt_t *pkt;
	vpx_codec_iter_t iter = nullptr;
	vpx_image_t image = { VPX_IMG_FMT_NONE };

	media::VideoImage w_image;
	const media::VideoImage* p_image;

	if (picture->getWidth() != m_width)
	{
		picture->transformTo(w_image, m_width, m_height, media::Transform::Stretch);
		p_image = &w_image;
	}
	else
	{
		p_image = picture;
	}

	const uint8_t* in_data = p_image->getYData();
	size_t in_size = p_image->getImageSize();


	MLOG_CALL("VP8-ENC", "Encode: -- image(%dx%d len:%d bytes)",
		p_image->getWidth(),
		p_image->getHeight(),
		p_image->getImageSize());

	if (!in_data || !in_size) {
		MLOG_ERROR("VP8-ENC", "Invalid parameter");
		return 0;
	}

	//if (in_size != (m_context.config.enc->g_w * m_context.config.enc->g_h * 3) >> 1)
	//{
	//	MLOG_ERROR("VP8-ENC", "Invalid size");
	//	return 0;
	//}

	// wrap yuv420 buffer
	if (!vpx_img_wrap(&image, VPX_IMG_FMT_I420, m_context.config.enc->g_w, m_context.config.enc->g_h, 1, (unsigned char*)in_data)) {
		MLOG_ERROR("VP8-ENC", "vpx_img_wrap failed");
		return 0;
	}

	// encode data
	++m_pts;
	if (m_force_idr)
	{
		flags |= VPX_EFLAG_FORCE_KF;
		m_force_idr = false;
	}
	
	{
		rtl::MutexLock l1(m_mutex);
		vpx_ret = vpx_codec_encode(&m_context, &image, m_pts, 1, flags, VPX_DL_REALTIME);
	}

	if (vpx_ret != VPX_CODEC_OK)
	{
		MLOG_ERROR("VP8-ENC", "vpx_codec_encode failed with error =%s", vpx_codec_err_to_string(vpx_ret));
		goto bail;
	}

	++m_frame_count;
	++m_pic_id;

	while ((pkt = vpx_codec_get_cx_data(&m_context, &iter)))
	{
		switch (pkt->kind)
		{
		case VPX_CODEC_CX_FRAME_PKT:
			vp8_encap(pkt);
			break;
		case VPX_CODEC_STATS_PKT:		/**< Two-pass statistics for this frame */
		case VPX_CODEC_PSNR_PKT:		/**< PSNR statistics for this frame */
		case VPX_CODEC_CUSTOM_PKT:		/**< Algorithm extensions  */
		default:
			MLOG_CALL("VP8-ENC", "pkt->kind=%d not supported", (int)pkt->kind);
			break;
		}
	}

bail:
	vpx_img_free(&image);
	return 0;

	return false;
}

void mg_video_encoder_vp8_t::vp8_encap(const vpx_codec_cx_pkt_t *pkt)
{
	bool non_ref, is_keyframe, part_start;
	uint8_t *frame_ptr;
	uint32_t part_size, part_ID, pkt_size, index;

	if (!pkt || !pkt->data.frame.buf || !pkt->data.frame.sz)
	{
		MLOG_ERROR("VP8-ENC", "Invalid parameter");
		return;
	}

#if defined(VIDEO_TEST)
	fwrite(pkt->data.frame.buf, pkt->data.frame.sz, 1, m_outfile);
#endif


	index = 0;
	frame_ptr = (uint8_t*)pkt->data.frame.buf;
	pkt_size = (uint32_t)pkt->data.frame.sz;
	non_ref = (pkt->data.frame.flags & VPX_FRAME_IS_DROPPABLE) != 0;
	is_keyframe = (pkt->data.frame.flags & VPX_FRAME_IS_KEY);

	uint32_t current = rtl::DateTime::getTicks();
	int ms_diff = current - m_last_clock;
	m_last_ts += ms_diff * 90;
	m_last_clock = current;


#if defined(VPX_CODEC_USE_OUTPUT_PARTITION)
	part_ID = pkt->data.frame.partition_id;
	part_start = true;
	part_size = pkt_size;

	MLOG_CALL("VP8-ENC", "Send: -- frame %d bytes", pkt_size);

	while (index < part_size)
	{
		uint32_t frag_size = MIN(1200, (part_size - index));

		MLOG_CALL("VP8-ENC", "Send: -- frame fragmet %d bytes", frag_size);

		rtp_callback(
			&frame_ptr[index],
			frag_size,
			part_ID,
			part_start,
			non_ref,
			((pkt->data.frame.flags & VPX_FRAME_IS_FRAGMENT) == 0 && (index + frag_size) == part_size) // RTP marker?
			);
		part_start = false;
		index += frag_size;
	}
#else
	// first partition (contains modes and motion vectors)
	part_ID = 0; // The first VP8 partition(containing modes and motion vectors) MUST be labeled with PartID = 0
	part_start = tsk_true;
	part_size = (frame_ptr[2] << 16) | (frame_ptr[1] << 8) | frame_ptr[0];
	part_size = (part_size >> 5) & 0x7FFFF;
	if (part_size > pkt_size) {
		MLOG_ERROR("VP8-ENC",("part_size is > pkt_size(%u,%u)", part_size, pkt_size);
		return;
	}

	// first,first,....partitions (or fragment if part_size > TDAV_VP8_RTP_PAYLOAD_MAX_SIZE)
	while (index<part_size) {
		uint32_t frag_size = TSK_MIN(TDAV_VP8_RTP_PAYLOAD_MAX_SIZE, (part_size - index));
		tdav_codec_vp8_rtp_callback(self, &frame_ptr[index], frag_size, part_ID, part_start, non_ref, tsk_false);
		part_start = tsk_false;
		index += frag_size;
	}

	// second,third,... partitions (or fragment if part_size > TDAV_VP8_RTP_PAYLOAD_MAX_SIZE)
	// FIXME: low FEC
	part_start = tsk_true;
	while (index<pkt_size) {
		if (part_start) {
			/* PartID SHOULD be incremented for each subsequent partition,
			but MAY be kept at 0 for all packets.  PartID MUST NOT be larger
			than 8.
			*/
			part_ID++;
		}
		part_size = TSK_MIN(TDAV_VP8_RTP_PAYLOAD_MAX_SIZE, (pkt_size - index));

		tdav_codec_vp8_rtp_callback(self, &frame_ptr[index], part_size, part_ID, part_start, non_ref, (index + part_size) == pkt_size);
		index += part_size;
		/*
		If more than one packet in an encoded frame contains the
		same PartID, the S bit MUST NOT be set for any other packet than
		the first packet with that PartID.
		*/
		part_start = tsk_false;
	}
#endif /* VPX_CODEC_USE_OUTPUT_PARTITION */
}

//#define TDAV_VP8_PAY_DESC_SIZE			4

void mg_video_encoder_vp8_t::rtp_callback(const void *data, uint32_t size, uint32_t partID, bool part_start, bool non_ref, bool last)
{
	size_t paydesc_and_hdr_size = TDAV_VP8_PAY_DESC_SIZE;
	bool has_hdr;
	
	/* draft-ietf-payload-vp8-04 - 4.2. VP8 Payload Descriptor
	0 1 2 3 4 5 6 7
	+-+-+-+-+-+-+-+-+
	|X|R|N|S|PartID | (REQUIRED)
	+-+-+-+-+-+-+-+-+
	X:   |I|L|T|K| RSV   | (OPTIONAL)
	+-+-+-+-+-+-+-+-+
	I:   |M| PictureID   | (OPTIONAL)
	+-+-+-+-+-+-+-+-+
	L:   |   TL0PICIDX   | (OPTIONAL)
	+-+-+-+-+-+-+-+-+
	T/K: |TID|Y| KEYIDX  | (OPTIONAL)
	+-+-+-+-+-+-+-+-+

	draft-ietf-payload-vp8-04 - 4.3. VP8 Payload Header
	0 1 2 3 4 5 6 7
	+-+-+-+-+-+-+-+-+
	|Size0|H| VER |P|
	+-+-+-+-+-+-+-+-+
	|     Size1     |
	+-+-+-+-+-+-+-+-+
	|     Size2     |
	+-+-+-+-+-+-+-+-+
	| Bytes 4..N of |
	| VP8 payload   |
	:               :
	+-+-+-+-+-+-+-+-+
	| OPTIONAL RTP  |
	| padding       |
	:               :
	+-+-+-+-+-+-+-+-+
	*/

	/*
	Note that the header is present only in packets which have the S bit equal to one and the
	PartID equal to zero in the payload descriptor.
	*/
	if ((has_hdr = (part_start && partID == 0)))
	{
		has_hdr = true;
		paydesc_and_hdr_size += 0; // encoded data already contains payload header?
	}

	if (!data || !size)
	{
		MLOG_ERROR("VP8-ENC", "Invalid parameter");
		return;
	}
	if (m_rtp.size < (size + paydesc_and_hdr_size))
	{
		if (!(m_rtp.ptr = (uint8_t*)REALLOC(m_rtp.ptr, (size + paydesc_and_hdr_size))))
		{
			MLOG_ERROR("VP8-ENC", "Failed to allocate new buffer");
			return;
		}
		m_rtp.size = (size + paydesc_and_hdr_size);
	}
	
	memcpy((m_rtp.ptr + paydesc_and_hdr_size), data, size);

	/* VP8 Payload Descriptor */
	// |X|R|N|S|PartID|
	m_rtp.ptr[0] = (partID & 0x0F) // PartID
		| ((part_start << 4) & 0x10)// S
		| ((non_ref << 5) & 0x20) // N
		// R = 0
#if TDAV_VP8_DISABLE_EXTENSION
		| (0x00) // X=0
#else
		| (0x80) // X=1
#endif
		;

#if !TDAV_VP8_DISABLE_EXTENSION
	// X:   |I|L|T|K| RSV   |
	m_rtp.ptr[1] = 0x80; // I = 1, L = 0, T = 0, K = 0, RSV = 0
	// I:   |M| PictureID   |
	m_rtp.ptr[2] = (0x80 | ((m_pic_id >> 8) & 0x7F)); // M = 1 (PictureID on 15 bits)
	m_rtp.ptr[3] = (m_pic_id & 0xFF);
#endif

	/* 4.2. VP8 Payload Header */
	//if(has_hdr) {
	// already part of the encoded stream
	//}

	// Send data over the network
	if (m_callback)
	{
		MLOG_CALL("VP8-ENC", "SEND RTP %d bytes, ts:%u m:%s", size + TDAV_VP8_PAY_DESC_SIZE, m_last_ts, last ? "true" : "false");
		m_callback(m_rtp.ptr, size + TDAV_VP8_PAY_DESC_SIZE, m_last_ts, last, m_user);
	}
}

bool mg_video_encoder_vp8_t::sdp_att_match(const char* att_name, const char* att_value)
{
	if (std_stricmp(att_name, "fmtp") == 0)
	{
		int fps;
		media::VideoSize vsize = media::parseVideoFMTP(att_value, m_pref_size, &fps);
		
		if (vsize.width == 0)
		{
			MLOG_CALL("VP8-ENC", "Failed to match fmtp=%s", att_value);
			return false;
		}

		m_width = vsize.width;
		m_height = vsize.height;
		m_fps = fps;
	}
	else if (std_stricmp(att_name, "imageattr") == 0)
	{
		media::VideoSize vsize = media::parseVideoImageAttr(att_value, m_pref_size);

		if (vsize.width == 0)
		{
			return false;
		}

		m_width = vsize.width;
		m_height = vsize.height;
		// clamp the output size to the defined max range
	}

	return true;
}
