/* RTX H.248 Media Gate. VPX Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#define VPX_CODEC_DISABLE_COMPAT 1 /* strict compliance with the latest SDK by disabling some backwards compatibility  */
#include <vpx/vpx_codec.h>
#include <vpx/vpx_encoder.h>
#include <vpx/vpx_decoder.h>
#include <vpx/vp8cx.h>
#include <vpx/vp8dx.h>

//#define VIDEO_TEST

//-----------------------------------------------
//
//-----------------------------------------------
class mg_video_decoder_vp8_t : public mg_video_decoder_t
{
	rtl::Logger& m_log;

	bool m_initialized;
	vpx_codec_dec_cfg_t m_cfg;
	vpx_codec_ctx_t m_context;

	uint8_t* m_accumulator;
	int m_accumulator_pos;
	int m_accumulator_size;
	int m_first_part_size;

	bool m_idr;
	bool m_corrupted;

	uint32_t m_last_timestamp;

#if defined(VIDEO_TEST)
	FILE* m_outfile;
#endif

private:
	bool initialize();
	bool depacketize(const rtp_packet* rtp_pack);
public:
	mg_video_decoder_vp8_t(rtl::Logger& log);
	virtual ~mg_video_decoder_vp8_t();

	bool initialize(const media::PayloadFormat* fmt);
	void destroy();

	virtual const char*	getEncoding() override;
	virtual media::VideoImage* decode(const rtp_packet* rtp_pack) override;
};
//-----------------------------------------------
