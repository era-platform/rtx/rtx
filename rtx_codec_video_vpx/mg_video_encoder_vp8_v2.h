/* RTX H.248 Media Gate. VPX Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "rtx_video_codec.h"

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
typedef struct { uint8_t* ptr; unsigned len; } t_video_nal_data;
typedef rtl::ArrayT<t_video_nal_data> t_video_nal_array;
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
struct i_vp8_codec_encoder
{
	virtual int				init(unsigned int w, unsigned int h, unsigned int bitrate) = 0;
	virtual void			dispose() = 0;

	virtual int				start_encode(const unsigned char* from, unsigned int from_len) = 0;
	virtual int				next_pkt() = 0;

	virtual unsigned int*	get_pointer_last_param() = 0;
	virtual unsigned int	get_size_last_param() = 0;
	virtual unsigned char*	get_pointer_last_pkt() = 0;
};
//---------------------------------------------
//
//---------------------------------------------
struct i_video_rtp_packetizer
{
	struct rtp_pack
	{
		uint8_t* data;
		int	size;
		bool marker;
		uint32_t timestamp;
	};
	typedef rtl::ArrayT<rtp_pack> rtp_packet_array;


	virtual bool	init(unsigned framerate, unsigned rtp_payload_length) = 0;
	virtual void	dispose() = 0;
	virtual bool	packetize_frame(const t_video_nal_array& nal_queue) = 0;
	virtual const rtp_packet_array& current_packets() = 0;
};
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
class vp8_codec_encoder : public mg_video_encoder_t
{
public:
	vp8_codec_encoder(rtl::Logger& log, rtx_video_encoder_cb_t cb, void* usr);
	virtual ~vp8_codec_encoder();

	bool initialize(const media::PayloadFormat* fmt);
	void destroy();

	virtual const char*	getEncoding();
	virtual bool init(rtx_video_encoder_cb_t cb, void* user);
	virtual void reset();
	virtual bool encode(const media::VideoImage* picture);

private:
	bool encode(const char* data, unsigned int length);

	void clear_nal_pool();

private:
	rtl::Logger& m_log;

	bool m_enc_initialized;
	rtx_video_encoder_cb_t m_callback;
	void* m_callback_user;

	i_vp8_codec_encoder* m_enc;
	i_video_rtp_packetizer* m_pack;

	t_video_nal_array m_nal_list;
};
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
class vp8_codec_rtp_packetization : public i_video_rtp_packetizer
{
public:
	vp8_codec_rtp_packetization(rtl::Logger& log);
	virtual ~vp8_codec_rtp_packetization();

	virtual bool init(unsigned framerate, unsigned rtp_payload_length);
	virtual void dispose();

	virtual bool packetize_frame(const t_video_nal_array& nal_queue);
	virtual const rtp_packet_array& current_packets();

private:
	void clear_packet_pool();

	unsigned write_descriptor(const unsigned char* from, unsigned int from_len, unsigned char* to, unsigned int to_len, bool part_start);

	bool parse_param(const unsigned int* param, unsigned int param_len);
	bool write_packet(const unsigned char* to, unsigned int to_len);

private:
	rtl::Logger& m_log;

	rtp_packet_array m_packet_list;

	unsigned char* m_buffer;
	unsigned m_buffer_pos;
	unsigned m_buffer_size;

	unsigned m_payload_length;
	unsigned m_framerate;
	unsigned m_timestamp;

	bool m_non_reference_frame;
	bool m_keyframe;
	unsigned m_part_ID;
	bool m_frame_is_fragment;// FRAME_IS_FRAGMENT

	bool m_marker;
};
//-----------------------------------------------
//
//-----------------------------------------------
class vpx_lib_vp8_codec_encoder : public i_vp8_codec_encoder
{
public:
	vpx_lib_vp8_codec_encoder();
	virtual ~vpx_lib_vp8_codec_encoder();

	virtual int init(uint32_t w, uint32_t h, uint32_t bitrate);
	virtual void dispose();

	virtual int	start_encode(const uint8_t* from, uint32_t from_len);
	virtual int	next_pkt();

	virtual uint32_t* get_pointer_last_param();
	virtual uint32_t get_size_last_param();
	virtual uint8_t* get_pointer_last_pkt();

private:
	bool write_param(uint32_t* param_buf, uint32_t param_buf_len);

private:
	uint32_t m_w;
	uint32_t m_h;

	uint32_t m_pkt_count;

	bool m_keyframe;
	bool m_nonref;
	uint32_t m_part_ID;
	bool m_frame_is_fragment;

	uint8_t* m_enc_buf;
	uint32_t m_enc_buf_len;
	uint32_t m_enc_buf_pos;

	struct vpx_codec_ctx* m_codec;
	struct vpx_image* m_img;
	struct vpx_codec_enc_cfg* m_cfg;

	void* m_iter;

	uint32_t* m_pointer_last_param;
	uint32_t m_size_last_param;
	uint8_t* m_pointer_last_pkt;
};

//-----------------------------------------------
