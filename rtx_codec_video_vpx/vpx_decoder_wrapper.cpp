﻿/* RTX H.248 Media Gate. VPX Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "vpx_decoder_wrapper.h"

#include "vpx/vp8dx.h"
#include "vpx/vpx_decoder.h"
#include "libyuv/convert.h"
#include "libyuv/video_common.h"

#define LOG_PREFIX "V8Dec"

namespace vpx
{	
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	VpxDecoder::VpxDecoder() : m_ctx(nullptr), m_buffer(nullptr)
	{
		m_height = 0;
		m_width = 0;
		m_bufferLength = 0;
		m_bufferPos = 0;
		m_lastSize = 0;
		m_count_res_pos = 0;
		m_iter = nullptr;

		m_ctx = NEW vpx_codec_ctx_t;

	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	VpxDecoder::~VpxDecoder()
	{
		reset();

		DELETEO(m_ctx);
	}
	//-----------------------------------------------
	// 
	//-----------------------------------------------
	void VpxDecoder::reset()
	{
		if (m_buffer != nullptr)
		{
			DELETEAR(m_buffer);
			m_buffer = nullptr;
			m_bufferLength = 0;
			m_bufferPos = 0;
			m_lastSize = 0;
		}

		m_count_res_pos = 0;

		m_width = 0;
		m_height = 0;

		vpx_codec_dec_cfg_t cfg;
		cfg.w = 640;
		cfg.h = 480;

		// инициализируем кодек с размерами окна.
		vpx_codec_err_t	res = vpx_codec_dec_init(m_ctx, vpx_codec_vp8_dx(), &cfg, 0);
		
		if (res != VPX_CODEC_OK)
		{
			return;
		}

		m_width = cfg.w;
		m_height = cfg.h;
		m_bufferLength = m_width * m_height * 3;
		m_buffer = NEW uint8_t[m_bufferLength];
		m_bufferPos = 0;
		m_lastSize = 0;
		m_count_res_pos = 0;
		m_iter = nullptr;

		return;
	}
	//-----------------------------------------------
	// Начало декодирования.
	//-----------------------------------------------
	bool VpxDecoder::decode(const uint8_t* frame, uint32_t length)
	{
		if (frame == nullptr || length == 0)
		{
			LOG_WARN(LOG_PREFIX, "VpxDecoder::decode() -> invalid params");
			return false;
		}

		if (m_ctx == nullptr)
		{
			LOG_WARN(LOG_PREFIX, "VpxDecoder::decode() -> not initialized!");
			return false;
		}

		m_iter = nullptr;
		// передаем данные в библиотеку VPX там происходит декодирование.
		vpx_codec_err_t err = vpx_codec_decode(m_ctx, frame, length, nullptr, 0);

		if (err == VPX_CODEC_OK)
		{
			// если все успешно нужно вызвать метод get_image() для получение картинки в формате vpx
			return true;
		}

		LOG_WARN(LOG_PREFIX, "VpxDecoder::decode() -> internal error %s(%d)", "@#$%", err);

		return false;
	}
	//-----------------------------------------------
	// Получение картинки в vpx_lib формате (по умолчанию I420).
	// если возвратилось 0, то значит закончились картинки во фрейме.
	// Обычно одна картинка на фрейм. Но мало ли.... 
	// По этому реализована как отдельная функция.
	//-----------------------------------------------
	bool VpxDecoder::hasImage()
	{
		if (m_ctx == nullptr)
		{
			return false;
		}

		vpx_codec_iter_t iter = m_iter;
		// получаем картинку из заранее декодированного фрейма.
		vpx_image_t *img = vpx_codec_get_frame(m_ctx, &iter);
		m_iter = &iter;
		if (img == nullptr)
		{
			// больше картинок во фрейме нет.

			return false;
		}

		m_width = img->d_w;
		m_height = img->d_h;

		// размер для картинки rgb... для других форматов может быть по другому.
		m_lastSize = m_width * m_height * 3;

		// остаток буфера..
		int res_size = m_bufferLength - m_bufferPos;

		// если в буфере не хватает места для очередной картинки - создаем новый буфер.
		if (res_size < m_lastSize)
		{
			uint32_t all_size = m_bufferPos + m_lastSize;

			// временный буфер для перезаписи данных
			uint8_t* buff = NEW uint8_t[all_size];
			memcpy(buff, m_buffer, m_bufferPos);

			// тут мы отказываемся от владения старого указателя и указываем новый...
			DELETEAR(m_buffer);
			m_buffer = buff;
			m_bufferLength = all_size;
		}

		// конвертируем полученный формат картинки в rgb
		// (формат картинки можно узнать тут: img->fmt)
		int error = libyuv::ConvertFromI420(
			img->planes[0], img->stride[0],
			img->planes[1], img->stride[1],
			img->planes[2], img->stride[2],
			m_buffer + m_bufferPos, 0, m_width, m_height, libyuv::FOURCC_I420);

		if (error < 0)
		{
			LOG_WARN(LOG_PREFIX, "VpxDecoder::decode() -> libyuv internal error %s(%d)", "@#$%", error);
			return false;
		}

		m_bufferPos += m_lastSize;

		return m_lastSize > 0;
	}

}
//-----------------------------------------------
