/* RTX H.248 Media Gate. VPX Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "mg_video_encoder_vp8_v2.h"

#include <vpx/vpx_encoder.h>
#include <vpx/vp8cx.h>

#define DEFAULT_BITRATE (1024 * 512)
#define LOG_PREFIX "VP8-E"

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
vp8_codec_encoder::vp8_codec_encoder(rtl::Logger& log, rtx_video_encoder_cb_t cb, void* usr) :
	m_log(log),
	m_enc_initialized(false),
	m_callback(cb),
	m_callback_user(usr),
	m_enc(nullptr),
	m_pack(nullptr)
{
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
vp8_codec_encoder::~vp8_codec_encoder()
{
	destroy();
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool vp8_codec_encoder::initialize(const media::PayloadFormat* fmt)
{
	destroy();

	m_enc = NEW vpx_lib_vp8_codec_encoder();
	
	if (m_enc == NULL)
	{
		//MLOG_MEDIA_FLOW(LOG_PREFIX, "  ERROR   : init --> m_enc is NULL");
		return false;
	}

	m_enc->init(1280, 720, DEFAULT_BITRATE);

	m_pack = NEW vp8_codec_rtp_packetization(m_log);

	m_pack->init(15, 1400);

	return true;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void vp8_codec_encoder::destroy()
{
	if (m_enc != NULL)
	{
		m_enc->dispose();
		DELETEO(m_enc);
		m_enc = NULL;
	}

	if (m_pack != nullptr)
	{
		m_pack->dispose();
		DELETEO(m_pack);
		m_pack = NULL;
	}

	clear_nal_pool();
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
const char*	vp8_codec_encoder::getEncoding()
{
	return "VP8";
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool vp8_codec_encoder::init(rtx_video_encoder_cb_t cb, void* user)
{
	m_callback = cb;
	m_callback_user = user;

	return true;
}

void vp8_codec_encoder::reset()
{
}

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool vp8_codec_encoder::encode(const media::VideoImage* picture)
{
	media::VideoImage w_image;
	const media::VideoImage* p_image;

	/*if (picture->getWidth() != 640)
	{
		picture->transformTo(w_image, 640, 480, media::Transform::Stretch);
		p_image = &w_image;
	}
	else*/
	{
		p_image = picture;
	}

	if (!m_enc_initialized)
	{
		m_enc_initialized = true;

		if (!m_enc->init(p_image->getWidth(), p_image->getHeight(), DEFAULT_BITRATE))
		{
			//MLOG_WARN(LOG_PREFIX, "Encoder -- error while initializing encoder!");
			return false;
		}
	}

	const uint8_t* in_data = p_image->getYData();
	size_t in_size = p_image->getImageSize();

	//MLOG_CALL("VP8-ENC", "Encode: -- image(%dx%dx len:%d bytes)",
	//	p_image->getWidth(),
	//	p_image->getHeight(),
	//	p_image->getImageSize());

	if (!in_data || !in_size) {
		//MLOG_ERROR("VP8-ENC", "Invalid parameter");
		return 0;
	}

	if (!encode((const char*)p_image->getYData(), p_image->getImageSize()))
	{
		//MLOG_WARN(LOG_PREFIX, "Encoder -- error while encoding!");
		return false;
	}

	if (!m_pack->packetize_frame(m_nal_list))
	{
		//MLOG_WARN(LOG_PREFIX, "Encoder -- error while packetizing!");
		return false;
	}

	const i_video_rtp_packetizer::rtp_packet_array& array = m_pack->current_packets();

	for (int i = 0; i < array.getCount(); ++i)
	{
		const i_video_rtp_packetizer::rtp_pack& rtp = array[i];
		m_callback(rtp.data, rtp.size, rtp.timestamp, rtp.marker, m_callback_user);
	}

	return true;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool vp8_codec_encoder::encode(const char* data, unsigned len)
{
	clear_nal_pool();

	if ((data == NULL) || (len == 0))
	{
		//MLOG_MEDIA_FLOW(LOG_PREFIX, "  ERROR   : encode --> wrong param.");
		return false;
	}

	if ((m_enc == NULL))
	{
		//MLOG_MEDIA_FLOW(LOG_PREFIX, "  ERROR   : encode --> m_enc is NULL.");
		return false;
	}

	int err = m_enc->start_encode((const unsigned char*)data, len);
	if (err < 0)
	{
		//MLOG_MEDIA_FLOW(LOG_PREFIX, "  ERROR   : encode --> m_enc->start_encode FAIL! error: %d", err);
		return false;
	}

	bool error = false;
	bool go = true;
	while (go)
	{
		int encode_size = m_enc->next_pkt();
		if (encode_size <= 0)
		{
			if (encode_size < 0)
			{
				//MLOG_MEDIA_FLOW(LOG_PREFIX, "  ERROR   : encode --> m_enc->next_pkt FAIL! error: %d", encode_size);
				error = true;
			}
			go = false;
			break;
		}

		m_nal_list.add({ (uint8_t*)m_enc->get_pointer_last_param(), m_enc->get_size_last_param() });
		m_nal_list.add({ (uint8_t*)m_enc->get_pointer_last_pkt(), (uint32_t)encode_size });
	}

	if (error)
	{
		return false;
	}

	return true;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void vp8_codec_encoder::clear_nal_pool()
{
	//for (int i = 0; i < m_nal_list.getCount(); ++i)
	//{
	//	t_video_nal_data& nal = m_nal_list[i];
	//	FREE(nal.ptr);
	//}
	m_nal_list.clear();
}

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
#define LOG_PREFIX_P "VP8>RTP"
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
vp8_codec_rtp_packetization::vp8_codec_rtp_packetization(rtl::Logger& log) :
m_log(log), m_payload_length(0), m_timestamp(0), m_framerate(0),
m_buffer_size(0), m_buffer_pos(0), m_buffer(NULL)
{
	m_non_reference_frame = false;
	m_keyframe = true;
	m_part_ID = 0;
	m_frame_is_fragment = false;
	m_marker = false;

}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
vp8_codec_rtp_packetization::~vp8_codec_rtp_packetization()
{
	dispose();
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool vp8_codec_rtp_packetization::init(unsigned framerate, unsigned rtp_payload_length)
{
	//MLOG_CALL(LOG_PREFIX_P, "init --> initializing...");

	dispose();

	m_framerate = framerate;
	m_payload_length = rtp_payload_length;

	//��������� ��� 100 ��� �������
	m_buffer_size = 100 * m_payload_length;
	m_buffer = NEW uint8_t[m_buffer_size];

	m_non_reference_frame = false;
	m_keyframe = true;
	m_part_ID = 0;
	m_frame_is_fragment = false;
	m_marker = false;

	//MLOG_CALL(LOG_PREFIX_P, "init <-- initialized.");

	return true;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void vp8_codec_rtp_packetization::dispose()
{
	//MLOG_CALL(LOG_PREFIX_P, "dispose --> disposing...");

	clear_packet_pool();

	if (m_buffer != NULL)
	{
		DELETEAR(m_buffer);
		m_buffer = NULL;
	}
	m_buffer_size = 0;
	m_buffer_pos = 0;

	m_payload_length = 0;
	m_framerate = 0;
	m_timestamp = 0;

	m_non_reference_frame = false;
	m_keyframe = true;
	m_part_ID = 0;
	m_frame_is_fragment = false;
	m_marker = false;

	//MLOG_CALL(LOG_PREFIX_P, "dispose <-- disposed.");
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool vp8_codec_rtp_packetization::packetize_frame(const t_video_nal_array& nal_queue)
{
	clear_packet_pool();

	m_buffer_pos = 0;

	//MLOG_MEDIA_FLOW(LOG_PREFIX_P, "packetize_frame --> nals count: %d, current timestamp: %u", nal_queue.getCount(), m_timestamp);

	if ((nal_queue.isEmpty()) || (nal_queue.getCount() < 2))
	{
		return false;
	}

	for (int i = 1; i < nal_queue.getCount(); i += 2)
	{
		const t_video_nal_data& param = nal_queue[i - 1];
		if (!parse_param((const unsigned int*)param.ptr, param.len))
		{
			continue;
		}

		const t_video_nal_data& nal = nal_queue[i];

		uint8_t* frame_ptr = nal.ptr;
		uint32_t pkt_size = nal.len;

		uint32_t part_size = 0;
		bool start_partition = true;
		uint32_t index = 0;
		bool error = false;

		start_partition = true;
		part_size = pkt_size;
		while (index < part_size)
		{
			uint32_t step_size = part_size - index;
			uint32_t frag_size = (step_size > m_payload_length) ? m_payload_length : step_size;
			//((pkt->data.frame.flags & VPX_FRAME_IS_FRAGMENT) == 0 && (index + frag_size) == part_size) // RTP marker?
			m_marker = ((!m_frame_is_fragment) && ((index + frag_size) == part_size));
			unsigned int write_size = write_descriptor(frame_ptr + index, frag_size, m_buffer + m_buffer_pos, m_buffer_size - m_buffer_pos, start_partition);
			if (write_size == 0)
			{
				error = true;
				break;
			}
			m_buffer_pos += write_size;
			start_partition = false;
			index += frag_size;
		}
	}

	//MLOG_MEDIA_FLOW(LOG_PREFIX_P, "packetize_frame <-- packets count: %d", m_packet_list.getCount());

	unsigned ts = (m_framerate == 0) ? 90000 / 15 : 90000 / m_framerate;
	m_timestamp += ts;

	return true;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
const i_video_rtp_packetizer::rtp_packet_array& vp8_codec_rtp_packetization::current_packets()
{
	return m_packet_list;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void vp8_codec_rtp_packetization::clear_packet_pool()
{
	m_packet_list.clear();
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
unsigned vp8_codec_rtp_packetization::write_descriptor(const unsigned char* from, unsigned int from_len, unsigned char* to, unsigned int to_len, bool part_start)
{
	unsigned int hdr_size = 1;
	bool has_hdr = false;

	if (part_start && m_part_ID == 0)
	{
		has_hdr = true;
		//hdr_size = 4;
	}

	if (to_len < (from_len + hdr_size))
	{
		return false;
	}
	memcpy((to + hdr_size), from, from_len);
	to[0] = 0;
	// |X|R|N|S|PartID|
	//to[0] = (m_part_ID & 0x0F) | ((part_start << 4) & 0x10) | ((m_non_reference_frame << 5) & 0x20) | (0x00);

	if (part_start)
	{
		to[0] |= 0x10;
	}
	if (!m_keyframe)
	{
		to[0] |= 0x20;
	}

	/*if (has_hdr)
	{
	to[1] = 0x00;
	memcpy((to + 2), &from_len, 2);
	}*/

	if (!write_packet(to, from_len + hdr_size))
	{
		return 0;
	}

	return from_len + hdr_size;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool vp8_codec_rtp_packetization::parse_param(const unsigned int* param, unsigned int param_len)
{
	m_non_reference_frame = false;
	m_keyframe = false;

	if ((param == NULL) || (param_len == 0))
	{
		return false;
	}

	if (param_len < 4)
	{
		return false;
	}

	m_non_reference_frame = param[0] == 1 ? true : false;
	m_keyframe = param[1] == 1 ? true : false;
	m_part_ID = param[2];
	m_frame_is_fragment = param[3] == 1 ? true : false;

	return true;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool vp8_codec_rtp_packetization::write_packet(const unsigned char* to, unsigned int to_len)
{
	m_packet_list.add({ (uint8_t*)to, (int)to_len, m_marker, m_timestamp });

	return true;
}

#include <vpx/vpx_encoder.h>
#include <vpx/vp8cx.h>
//#include "vpx_ports\mem_ops.h"
//#include "vpx\internal\vpx_codec_internal.h"

#include "libyuv/convert.h"
#include "libyuv/video_common.h"
//-----------------------------------------------
// 
//-----------------------------------------------
vpx_lib_vp8_codec_encoder::vpx_lib_vp8_codec_encoder()
{
	m_codec = NULL;
	m_img = NULL;
	m_cfg = NULL;

	m_pkt_count = 0;

	m_keyframe = true;
	m_nonref = true;
	m_part_ID = 0;
	m_frame_is_fragment = false;

	m_enc_buf = NULL;
	m_enc_buf_len = 0;
	m_enc_buf_pos = 0;

	m_iter = NULL;

	m_pointer_last_param = NULL;
	m_size_last_param = 0;
	m_pointer_last_pkt = NULL;
	//	m_size_last_pkt = 0;
}
//-----------------------------------------------
// 
//-----------------------------------------------
vpx_lib_vp8_codec_encoder::~vpx_lib_vp8_codec_encoder()
{
	dispose();
}
//-----------------------------------------------
// 
//-----------------------------------------------
int vpx_lib_vp8_codec_encoder::init(uint32_t w, uint32_t h, uint32_t bitrate)
{
	dispose();

	m_enc_buf_pos = 0;
	m_enc_buf_len = w * h * 3;
	m_enc_buf = new uint8_t[m_enc_buf_len];

	m_cfg = new vpx_codec_enc_cfg_t();

	vpx_codec_err_t err = vpx_codec_enc_config_default(vpx_codec_vp8_cx(), m_cfg, 0);

	if (err != VPX_CODEC_OK)
	{
		return -1;
	}

	m_cfg->g_w = w;
	m_cfg->g_h = h;
	m_cfg->g_timebase.num = 1;
	m_cfg->g_timebase.den = 20; //max - fr = 30
	m_cfg->rc_target_bitrate = bitrate;
	m_cfg->g_lag_in_frames = 0;
	m_cfg->rc_end_usage = VPX_CBR;
	m_cfg->g_pass = VPX_RC_ONE_PASS;
	m_cfg->kf_mode = VPX_KF_AUTO;
	m_cfg->kf_min_dist = 0;
	m_cfg->kf_max_dist = 50;

	/*m_cfg->rc_dropframe_thresh = 30;
	m_cfg->rc_resize_allowed = 0;
	m_cfg->rc_min_quantizer = 2;
	m_cfg->rc_max_quantizer = 56;
	m_cfg->rc_undershoot_pct = 100;
	m_cfg->rc_overshoot_pct = 15;
	m_cfg->rc_buf_initial_sz = 500;
	m_cfg->rc_buf_optimal_sz = 600;
	m_cfg->rc_buf_sz = 1000;*/

	vpx_enc_frame_flags_t flags = 0;
	m_codec = NEW vpx_codec_ctx_t;
	err = vpx_codec_enc_init(m_codec, vpx_codec_vp8_cx(), m_cfg, flags);
	if (err != VPX_CODEC_OK)
	{
		return -2;
	}

	m_img = vpx_img_alloc(m_img, VPX_IMG_FMT_I420, m_cfg->g_w, m_cfg->g_h, 0);
	if (m_img == NULL)
	{
		return -3;
	}

	m_pkt_count = 0;

	m_keyframe = true;
	m_nonref = true;
	m_part_ID = 0;
	m_frame_is_fragment = false;

	return 1;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void vpx_lib_vp8_codec_encoder::dispose()
{
	if (m_cfg != NULL)
	{
		delete m_cfg;
		m_cfg = NULL;
	}

	if (m_codec != NULL)
	{
		vpx_codec_destroy(m_codec);
		m_codec = NULL;
	}

	if (m_img != NULL)
	{
		vpx_img_free(m_img);
		m_img = NULL;
	}

	if (m_enc_buf != NULL)
	{
		delete[] m_enc_buf;
		m_enc_buf = NULL;
	}
	m_enc_buf_len = 0;
	m_enc_buf_pos = 0;

	m_pkt_count = 0;

	m_keyframe = true;
	m_nonref = true;
	m_part_ID = 0;
	m_frame_is_fragment = false;

	m_iter = NULL;

	m_pointer_last_param = NULL;
	m_size_last_param = 0;
	m_pointer_last_pkt = NULL;
	//	m_size_last_pkt = 0;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool vpx_lib_vp8_codec_encoder::write_param(uint32_t* param_buf, uint32_t param_buf_len)
{
	if ((param_buf == NULL) || (param_buf_len == 0) || (param_buf_len < 4))
	{
		return false;
	}

	param_buf[0] = (m_nonref) ? 1 : 0;
	param_buf[1] = (m_keyframe) ? 1 : 0;
	param_buf[2] = m_part_ID;
	param_buf[3] = (m_frame_is_fragment) ? 1 : 0;

	return true;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
int vpx_lib_vp8_codec_encoder::start_encode(const uint8_t* from, uint32_t from_len)
{
	if ((from == NULL) || (from_len == 0))
	{
		return -1;
	}

	if (m_codec == NULL)
	{
		return -2;
	}

	if (m_img == NULL)
	{
		return -4;
	}

	/*int error = libyuv::ConvertToI420(from, from_len,
		m_img->planes[0], m_img->stride[0],
		m_img->planes[1], m_img->stride[1],
		m_img->planes[2], m_img->stride[2],
		0, 0,
		m_img->w, m_img->h,
		m_img->w, m_img->h,
		libyuv::RotationMode::kRotate0, libyuv::FOURCC_24BG);

	if (error < 0)
	{
		return -4;
	}*/

	if (!vpx_img_wrap(m_img, VPX_IMG_FMT_I420, m_img->w, m_img->h, 1, (unsigned char*)from))
	{
		LOG_ERROR("VP8-ENC", "vpx_img_wrap failed");
		return 0;
	}

	m_iter = NULL;
	m_enc_buf_pos = 0;

	m_pkt_count++;
	vpx_enc_frame_flags_t flags = 0;

	vpx_codec_err_t res = vpx_codec_encode(m_codec, m_img, m_pkt_count, 1, flags, VPX_DL_REALTIME);
	if (res != VPX_CODEC_OK)
	{
		return -5;
	}

	return 1;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
int vpx_lib_vp8_codec_encoder::next_pkt()
{
	if (m_codec == NULL)
	{
		return -6;
	}

	if (m_img == NULL)
	{
		return -7;
	}

	vpx_codec_iter_t iter = m_iter;
	const vpx_codec_cx_pkt_t *pkt = vpx_codec_get_cx_data(m_codec, &iter);

	if (pkt == NULL)
	{
		//vpx_img_free(m_img);
		//m_img = NULL;
		m_enc_buf_pos = 0;
		return 0;
	}

	m_iter = (void*)iter;

	if (pkt->kind == VPX_CODEC_CX_FRAME_PKT)
	{
		m_size_last_param = 4;

		if ((m_enc_buf_len - m_enc_buf_pos - m_size_last_param) < pkt->data.frame.sz)
		{
			uint32_t size_write = (uint32_t)(pkt->data.frame.sz + m_enc_buf_pos + m_size_last_param);
			uint8_t* buff = new uint8_t[size_write];
			if (m_enc_buf != NULL)
			{
				memcpy(buff, m_enc_buf, m_enc_buf_pos);
				delete[] m_enc_buf;
			}
			m_enc_buf = buff;
			m_enc_buf_len = size_write;
		}

		m_nonref = (pkt->data.frame.flags & VPX_FRAME_IS_DROPPABLE) != 0;
		m_keyframe = (pkt->data.frame.flags & VPX_FRAME_IS_KEY) != 0;
		m_frame_is_fragment = (pkt->data.frame.flags & VPX_FRAME_IS_FRAGMENT) != 0;
		m_part_ID = (pkt->data.frame.partition_id < 0) ? 0 : pkt->data.frame.partition_id;

		m_pointer_last_param = (uint32_t*)(m_enc_buf + m_enc_buf_pos);
		write_param(m_pointer_last_param, m_size_last_param);

		uint32_t size_param = (sizeof(m_size_last_param) * m_size_last_param);
		m_pointer_last_pkt = m_enc_buf + m_enc_buf_pos + size_param;
		int m_size_last_pkt = (uint32_t)pkt->data.frame.sz;

		memcpy(m_pointer_last_pkt, pkt->data.frame.buf, pkt->data.frame.sz);

		m_enc_buf_pos += (size_param + m_size_last_pkt);

		return m_size_last_pkt;
	}

	return -8;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
uint32_t* vpx_lib_vp8_codec_encoder::get_pointer_last_param()
{
	return m_pointer_last_param;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
uint32_t vpx_lib_vp8_codec_encoder::get_size_last_param()
{
	return m_size_last_param;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
uint8_t* vpx_lib_vp8_codec_encoder::get_pointer_last_pkt()
{
	return m_pointer_last_pkt;
}
//------------------------------------------------------------------------------
