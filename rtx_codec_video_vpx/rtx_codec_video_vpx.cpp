/* RTX H.248 Media Gate. VPX Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#include "rtx_codec_video_vpx.h"

//-----------------------------------------------
//
//-----------------------------------------------
//#define USE_V2_DEC	1
#define USE_V2_ENC	1

#if !defined USE_V2_DEC
#include "mg_video_decoder_vp8.h"
#else
#include "mg_video_decoder_vp8_v2.h"
#endif

#if !defined USE_V2_ENC
#include "mg_video_encoder_vp8.h"
#else
#include "mg_video_encoder_vp8_v2.h"
#endif
//-----------------------------------------------
//
//-----------------------------------------------
//rtl::Logger vp8_log;
//-----------------------------------------------
//
//-----------------------------------------------
bool initlib()
{
//	vp8_log.create(Log.get_folder(), "vp8", LOG_APPEND);

	return true;
}
//-----------------------------------------------
//
//-----------------------------------------------
void freelib()
{
//	vp8_log.destroy();
}
//-----------------------------------------------
//
//-----------------------------------------------
const char* get_codec_list()
{
	return "VP8 VP9";
}
//-----------------------------------------------
//
//-----------------------------------------------
//bool get_video_codec_info(const PayloadFormat* format, mmt_image_t* info)
//{
//	return false;
//}
//-----------------------------------------------
//
//-----------------------------------------------
mg_video_decoder_t* create_video_decoder(const media::PayloadFormat* format)
{
#ifdef USE_V2_DEC
	vpx::Decoder* dec = NEW vpx::Decoder(Log);
#else
	mg_video_decoder_vp8_t* dec = NEW mg_video_decoder_vp8_t(Log);
#endif

	if (dec->initialize(format))
	{
		return dec;
	}

	DELETEO(dec);

	return nullptr;
}
//-----------------------------------------------
//
//-----------------------------------------------
void release_video_decoder(mg_video_decoder_t* decoder)
{
	//vp8_log.log("VP8", "release decoder");
#ifdef USE_V2_DEC
	vpx::Decoder* dec = (vpx::Decoder*)decoder;
#else
	mg_video_decoder_vp8_t* dec = (mg_video_decoder_vp8_t*)decoder;
#endif

	DELETEO(dec);
}
//-----------------------------------------------
//
//-----------------------------------------------
mg_video_encoder_t* create_video_encoder(const media::PayloadFormat* format, rtx_video_encoder_cb_t cb, void* usr)
{
#ifdef USE_V2_ENC
	vp8_codec_encoder* enc = NEW vp8_codec_encoder(Log, cb, usr);
#else
	mg_video_encoder_vp8_t* enc = NEW mg_video_encoder_vp8_t(Log, cb, usr);
#endif

	//vp8_log.log("VP8", "create encoder %s(%d) fmtp:%s", (const char*)format->getEncoding(), format->get_id(), (const char*)format->getFmtp());

	if (enc->initialize(format))
	{
		//vp8_log.log("VP8", "create encoder done");
		return enc;
	}

	//vp8_log.log("VP8", "create encoder failed");

	DELETEO(enc);
	return nullptr;
}
//-----------------------------------------------
//
//-----------------------------------------------
void release_video_encoder(mg_video_encoder_t* encoder)
{
	//vp8_log.log("VP8", "release encoder");

#ifdef USE_V2_ENC
	vp8_codec_encoder* enc = (vp8_codec_encoder*)encoder;
#else
	mg_video_encoder_vp8_t* enc = (mg_video_encoder_vp8_t*)encoder;
#endif

	DELETEO(enc);
}
//-----------------------------------------------
