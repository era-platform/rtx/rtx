/* RTX H.248 Media Gate. VPX Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#define VPX_CODEC_DISABLE_COMPAT 1 /* strict compliance with the latest SDK by disabling some backwards compatibility  */
#include <vpx/vpx_encoder.h>
#include <vpx/vpx_decoder.h>
#include <vpx/vp8cx.h>
#include <vpx/vp8dx.h>

//#define VIDEO_TEST

//-----------------------------------------------
//
//-----------------------------------------------
class mg_video_encoder_vp8_t : public mg_video_encoder_t
{
	rtl::Logger& m_log;
	// encoded
	uint32_t m_width;
	uint32_t m_height;
	uint32_t m_fps;
	uint32_t m_max_br;
	uint32_t m_max_mbps;
	media::VideoChroma m_chroma;
	bool m_flip;

	uint32_t m_last_ts;
	uint32_t m_last_clock;

	rtx_video_encoder_cb_t m_callback;
	void* m_user;

	media::VideoStandard m_pref_size; 	//! preferred video size

	vpx_codec_enc_cfg_t m_cfg;
	bool m_initialized;
	vpx_codec_ctx_t m_context;
	
	vpx_codec_pts_t m_pts;
	uint32_t m_pic_id : 15;
	uint64_t m_frame_count;
	bool m_force_idr;
	int m_rotation;

	struct {
		uint8_t* ptr;
		size_t size;
	} m_rtp;

	rtl::Mutex m_mutex;

#if defined(VIDEO_TEST)
	FILE* m_outfile;
#endif


private:
	void vp8_encap(const vpx_codec_cx_pkt_t *pkt);
	void rtp_callback(const void *data, uint32_t size, uint32_t partID, bool part_start, bool non_ref, bool last);
	bool sdp_att_match(const char* att_name, const char* att_value);

public:
	mg_video_encoder_vp8_t(rtl::Logger& log, rtx_video_encoder_cb_t cb, void* usr);
	virtual ~mg_video_encoder_vp8_t();

	bool initialize(const media::PayloadFormat* fmt);
	void destroy();

	virtual const char*	getEncoding();
	virtual bool init(rtx_video_encoder_cb_t cb, void* user);
	virtual void reset();
	virtual bool encode(const media::VideoImage* picture);
};
