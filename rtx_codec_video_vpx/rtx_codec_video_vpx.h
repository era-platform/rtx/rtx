/* RTX H.248 Media Gate. VPX Codec Module
 *
 * Copyright @ 2020 Peter Bukashin, George Makarov, Pavel Abramov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "std_basedef.h"
#include "std_typedef.h"
#include "std_funcdef.h"
//-----------------------------------------------
//
//-----------------------------------------------
#if defined (TARGET_OS_WINDOWS)
#ifdef RTX_CODEC_VIDEO_VPX_EXPORTS
#define RTX_MODULE_API __declspec(dllexport)
#else
#define RTX_MODULE_API __declspec(dllimport)
#endif
#elif defined(TARGET_OS_LINUX) || defined(TARGET_OS_FREEBSD)
#define RTX_MODULE_API
#endif

#include "rtx_codec.h"
#include "rtx_video_codec.h"
//-----------------------------------------------
//
//-----------------------------------------------
extern "C"
{
	RTX_MODULE_API bool initlib();
	//-------------------------------------------
	RTX_MODULE_API void freelib();
	//-------------------------------------------
	RTX_MODULE_API const char* get_codec_list();
	//-------------------------------------------
	RTX_MODULE_API mg_video_decoder_t* create_video_decoder(const media::PayloadFormat* format);
	//-------------------------------------------
	RTX_MODULE_API void release_video_decoder(mg_video_decoder_t* decoder);
	//-------------------------------------------
	RTX_MODULE_API mg_video_encoder_t* create_video_encoder(const media::PayloadFormat* format, rtx_video_encoder_cb_t cb, void* usr);
	//-------------------------------------------
	RTX_MODULE_API void release_video_encoder(mg_video_encoder_t* encoder);
}

//-----------------------------------------------
